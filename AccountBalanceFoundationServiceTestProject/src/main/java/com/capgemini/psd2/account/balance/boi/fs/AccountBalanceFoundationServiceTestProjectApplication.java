package com.capgemini.psd2.account.balance.boi.fs;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.psd2.aisp.platform.domain.PlatformAccountBalanceResponse;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.foundationservice.account.balance.boi.adapter.AccountBalanceFoundationServiceAdapter;


@SpringBootApplication
@ComponentScan("com.capgemini.psd2")
public class AccountBalanceFoundationServiceTestProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(AccountBalanceFoundationServiceTestProjectApplication.class, args);
	}
}

@RestController
@ResponseBody
class TestAccountBalanceFSController{
	
	@Autowired
	AccountBalanceFoundationServiceAdapter adapter;
	
	@RequestMapping("/testAccountBalance")
	public PlatformAccountBalanceResponse getResponse(){
		//Accounts accounts = new Accounts();
		//Accnt accnt = new Accnt();
		//accounts.getAccount().add(accnt);	
		AccountMapping accountMapping = new AccountMapping();
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
		AccountDetails accDet = new AccountDetails();
		accDet.setAccountId("1234");
		accDet.setAccountNSC("903779");
		accDet.setAccountNumber("76528776");
		accDetList.add(accDet);
		accountMapping.setAccountDetails(accDetList);
		accountMapping.setTppCID("test_123");
		accountMapping.setPsuId("BOI999");
		accountMapping.setCorrelationId("12345678");
		HashMap<String, String> params = new HashMap<>();
		//params.put(AccountBalanceFoundationServiceConstants.CHANNEL_ID, "test");
		//params.put("x-channel-id", "BOL");

		 params.put("channelId", "BOL");
		/*    params.put("consentFlowType", "AISP");
		    params.put("x-channel-id", "BOL");
		    params.put("x-user-id", "BOI123");
		    params.put("X-BOI-PLATFORM", "platform");
		    params.put("x-fapi-interaction-id", "12345678"); */
		
		
		// PlatformAccountBalanceResponse bal = adapter.retrieveAccountBalance(accountMapping, params);
		return adapter.retrieveAccountBalance(accountMapping, params);
	}
}