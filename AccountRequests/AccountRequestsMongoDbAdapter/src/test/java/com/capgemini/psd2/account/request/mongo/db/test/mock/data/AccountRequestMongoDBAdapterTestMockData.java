package com.capgemini.psd2.account.request.mongo.db.test.mock.data;

import java.util.ArrayList;
import java.util.List;

import com.capgemini.psd2.aisp.domain.OBReadConsentResponse1Data;
import com.capgemini.psd2.aisp.domain.OBReadConsentResponse1Data.StatusEnum;

public class AccountRequestMongoDBAdapterTestMockData {

	public static OBReadConsentResponse1Data getData() {
		OBReadConsentResponse1Data data = new OBReadConsentResponse1Data();
		List<OBReadConsentResponse1Data.PermissionsEnum> permissions = new ArrayList<>();
		permissions.add(OBReadConsentResponse1Data.PermissionsEnum.READACCOUNTSDETAIL);
		permissions.add(OBReadConsentResponse1Data.PermissionsEnum.READBALANCES);
		data.setCmaVersion("2.0");
		data.setPermissions(permissions);
		data.setExpirationDateTime("2017-05-05T00:00:00-00:00");
		data.setTransactionFromDateTime("2017-05-03T00:00:00-00:00");
		data.setTransactionToDateTime("2017-05-03T00:00:00-00:00");
		data.setConsentId("12345");
		data.setCreationDateTime("2017-05-04T00:00:00-00:00");
		data.setTppCID("12345");
		data.setStatus(OBReadConsentResponse1Data.StatusEnum.AWAITINGAUTHORISATION);
		return data;
	}
	
	public static OBReadConsentResponse1Data getDataResponse() {
		OBReadConsentResponse1Data data = new OBReadConsentResponse1Data();
		List<OBReadConsentResponse1Data.PermissionsEnum> permissions = new ArrayList<>();
		permissions.add(OBReadConsentResponse1Data.PermissionsEnum.READACCOUNTSDETAIL);
		permissions.add(OBReadConsentResponse1Data.PermissionsEnum.READBALANCES);
		data.setCmaVersion("2.0");
		data.setPermissions(permissions);
		data.setExpirationDateTime("2017-05-05T00:00:00-00:00");
		data.setTransactionFromDateTime("2017-05-03T00:00:00-00:00");
		data.setTransactionToDateTime("2017-05-03T00:00:00-00:00");
		data.setAccountRequestId("12345");
		data.setCreationDateTime("2017-05-04T00:00:00-00:00");
		data.setTppCID("12345");
		data.setStatus(StatusEnum.AWAITINGAUTHORISATION);
		return data;
	}
	
	public static OBReadConsentResponse1Data getDataInvalidTppAccess() {
		OBReadConsentResponse1Data data = new OBReadConsentResponse1Data();
		List<OBReadConsentResponse1Data.PermissionsEnum> permissions = new ArrayList<>();
		permissions.add(OBReadConsentResponse1Data.PermissionsEnum.READACCOUNTSDETAIL);
		permissions.add(OBReadConsentResponse1Data.PermissionsEnum.READBALANCES);
		data.setPermissions(permissions);
		data.setExpirationDateTime("2017-05-05T00:00:00-00:00");
		data.setTransactionFromDateTime("2017-05-03T00:00:00-00:00");
		data.setTransactionToDateTime("2017-05-03T00:00:00-00:00");
		data.setConsentId("12345");
		data.setCreationDateTime("2017-05-04T00:00:00-00:00");
		data.setTppCID("12345");
		data.setStatus(OBReadConsentResponse1Data.StatusEnum.AWAITINGAUTHORISATION);
		return data;
	}

}
