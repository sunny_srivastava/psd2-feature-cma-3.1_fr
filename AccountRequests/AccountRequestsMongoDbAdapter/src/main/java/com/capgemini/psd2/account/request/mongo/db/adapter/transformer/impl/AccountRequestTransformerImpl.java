package com.capgemini.psd2.account.request.mongo.db.adapter.transformer.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.capgemini.psd2.aisp.domain.OBReadConsentResponse1Data;
import com.capgemini.psd2.aisp.domain.OBReadConsentResponse1Data.PermissionsEnum;
import com.capgemini.psd2.aisp.domain.OBReadConsentResponse1Data.StatusEnum;
import com.capgemini.psd2.aisp.transformer.AccountRequestTransformer;

@Component
public class AccountRequestTransformerImpl implements AccountRequestTransformer{

	@Override
	public <T> OBReadConsentResponse1Data transformAccountInformation(T source, OBReadConsentResponse1Data destination,
			Map<String, String> params) {
		OBReadConsentResponse1Data accCma2Response = (OBReadConsentResponse1Data) source;
		destination.setCmaVersion(accCma2Response.getCmaVersion());
		destination.setConsentId(accCma2Response.getAccountRequestId());
		destination.setCreationDateTime(accCma2Response.getCreationDateTime());
		destination.setExpirationDateTime(accCma2Response.getExpirationDateTime());
		destination.setStatusUpdateDateTime(accCma2Response.getStatusUpdateDateTime());
		destination.setTransactionFromDateTime(accCma2Response.getTransactionFromDateTime());
		destination.setTransactionToDateTime(accCma2Response.getTransactionToDateTime());
		List<PermissionsEnum> persmissions= new ArrayList<>();
		for (PermissionsEnum permission : accCma2Response.getPermissions()){
			persmissions.add(PermissionsEnum.fromValue(permission.toString()));
		}
		destination.setPermissions(persmissions);
		destination.setStatus(StatusEnum.fromValue(accCma2Response.getStatus().toString()));
		return destination;
	}
}
