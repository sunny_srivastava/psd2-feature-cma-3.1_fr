package com.capgemini.psd2.account.request.mongo.db.adapter.impl;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessResourceFailureException;

import com.capgemini.psd2.account.request.mongo.db.adapter.repository.AccountRequestCMA3Repository;
import com.capgemini.psd2.account.request.mongo.db.adapter.repository.AccountRequestRepository;
import com.capgemini.psd2.aisp.adapter.AccountRequestAdapter;
import com.capgemini.psd2.aisp.consent.adapter.impl.AispConsentAdapterImpl;
import com.capgemini.psd2.aisp.domain.OBReadConsentResponse1;
import com.capgemini.psd2.aisp.domain.OBReadConsentResponse1Data;
import com.capgemini.psd2.aisp.domain.OBReadConsentResponse1Data.StatusEnum;
import com.capgemini.psd2.aisp.transformer.AccountRequestTransformer;
import com.capgemini.psd2.aspect.AdapterJSONUtilities;
import com.capgemini.psd2.consent.domain.AispConsent;
import com.capgemini.psd2.enums.ConsentStatusEnum;
import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.ErrorMapKeys;
import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.InternalServerErrorMessage;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.product.common.CompatibleVersionList;
import com.capgemini.psd2.utilities.DateUtilites;

public class AccountRequestMongoDbAdaptorImpl implements AccountRequestAdapter {

	@Autowired
	private AccountRequestCMA3Repository accountRequestRepository;

	@Autowired
	private AccountRequestRepository accountRequestRepositoryV2;

	@Autowired
	private AispConsentAdapterImpl aispConsentAdapter;

	@Autowired
	private AccountRequestTransformer accountRequestTransformer;
	
	@Autowired
	private CompatibleVersionList compatibleVersionList;
	
	@Autowired
	private RequestHeaderAttributes reqHeaderAtrributes;
	
	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(AccountRequestMongoDbAdaptorImpl.class);
	/** The Constant for error message in logs.  */
	private static final String ERROR_MESSAGE="{\"Creating Consent Data\":\"{}\"}";
	@Override
	public OBReadConsentResponse1 getAccountRequestGETResponse(String consentId) {

		OBReadConsentResponse1 accountRequestGETResponse = new OBReadConsentResponse1();
		OBReadConsentResponse1Data data = fetchData(consentId);

		/* Checking for REVOKED status as well since this status is no longer valid as of v3.1.4 . */
		if (data.getStatus().equals(StatusEnum.REVOKED)) {
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_RESOURCE_NOTFOUND,
					ErrorMapKeys.NO_ACCOUNT_REQUEST_DATA_FOUND));
		}
		
		String cid = reqHeaderAtrributes.getTppCID();
		if (null != cid && !cid.equals(data.getTppCID())) {
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.INVALID_TPP_USER_ACCESS);
		}
		accountRequestGETResponse.setData(data);
		return accountRequestGETResponse;
	}

	@Override
	public OBReadConsentResponse1 updateAccountRequestResponse(String consentId, OBReadConsentResponse1Data.StatusEnum statusEnum) {

		OBReadConsentResponse1 updateAccountRequestResponse = new OBReadConsentResponse1();
		OBReadConsentResponse1Data data = fetchData(consentId);
		
		if (data.getCmaVersion() != null)
			data.setStatusUpdateDateTime(DateUtilites.getCurrentDateInISOFormat());
		data.setStatus(statusEnum);
		try {
			data = accountRequestRepository.save(data);
		} catch (DataAccessResourceFailureException e) {
			LOG.info("DataAccessResourceFailureException in updateAccountRequestResponse(): "+e);
			LOG.error(ERROR_MESSAGE, e);
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
					InternalServerErrorMessage.CONNECTION_ERROR));
		}
		updateAccountRequestResponse.setData(data);
		return updateAccountRequestResponse;
	}

	@Override
	public void removeAccountRequest(String consentId, String cid) {
		OBReadConsentResponse1Data data = fetchData(consentId);
		
		if (null != cid && !cid.equals(data.getTppCID())) {
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.INVALID_TPP_USER_ACCESS);
		}
		
		if (data.getCmaVersion() != null)
			data.setStatusUpdateDateTime(DateUtilites.getCurrentDateInISOFormat());
		
		// Setting status as DELETED since REVOKED is not applicable after v3.1.4
		data.setStatus(StatusEnum.DELETED);
		try {
			revokeConsentAndToken(consentId);
			accountRequestRepository.save(data);
		} catch (DataAccessResourceFailureException e) {
			LOG.info("DataAccessResourceFailureException in removeAccountRequest(): "+e);
			LOG.error(ERROR_MESSAGE, e);
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.INVALID_TPP_USER_ACCESS);
		}
	}

	private OBReadConsentResponse1Data fetchData(String consentId) {
		OBReadConsentResponse1Data data = null;
		OBReadConsentResponse1Data dataV2 = null;
		try {
			data = accountRequestRepository.findByConsentIdAndCmaVersionIn(consentId,compatibleVersionList.fetchVersionList());
			if (data == null){
				dataV2 = accountRequestRepositoryV2.findByAccountRequestIdAndCmaVersionIn(consentId,compatibleVersionList.fetchVersionList());
				if (dataV2 != null) {
					data = accountRequestTransformer.transformAccountInformation(dataV2, data, null);
				}
			}
		} catch (DataAccessResourceFailureException e) {
			LOG.info("DataAccessResourceFailureException in fetchData(): "+e);
			LOG.error(ERROR_MESSAGE, e);
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
					InternalServerErrorMessage.CONNECTION_ERROR));
		}

		/*
		 * Change for v3.1.8, PSU should not be able to access/see DELETED resources.
		 */
		if (null == data || data.getStatus().equals(StatusEnum.DELETED)) {
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_RESOURCE_NOTFOUND,
					ErrorMapKeys.NO_ACCOUNT_REQUEST_DATA_FOUND));
		}
		return data;
	}
	
	private void revokeConsentAndToken(String accountRequestId) {
		// Changing consent status to DELETED irrespective of the status before.
		AispConsent aispConsent = aispConsentAdapter.retrieveConsentByAccountRequestId(accountRequestId);
		if (aispConsent == null)
			return;
		aispConsentAdapter.updateConsentStatus(aispConsent.getConsentId(), ConsentStatusEnum.DELETED);
	}

	@Override
	public OBReadConsentResponse1 createAccountRequestPOSTResponse(
			OBReadConsentResponse1Data data, Map<String, String> params) {

		
		OBReadConsentResponse1 accountRequestPOSTResponse = new OBReadConsentResponse1();
		OBReadConsentResponse1Data responseData;
		try {
			if(LOG.isDebugEnabled()) {
				LOG.debug(ERROR_MESSAGE, AdapterJSONUtilities.getJSONOutPutFromObject(data));
			}
			responseData = accountRequestRepository.save(data);
		} catch (DataAccessResourceFailureException e) {
			LOG.info("DataAccessResourceFailureException in createAccountRequestPOSTResponse(): "+e);
			LOG.error(ERROR_MESSAGE, e);
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
					InternalServerErrorMessage.CONNECTION_ERROR));
		}

		accountRequestPOSTResponse.setData(responseData);
		return accountRequestPOSTResponse;
	
	}

}