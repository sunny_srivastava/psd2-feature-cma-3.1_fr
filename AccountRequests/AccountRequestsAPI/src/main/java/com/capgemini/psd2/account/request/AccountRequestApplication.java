package com.capgemini.psd2.account.request;

import javax.servlet.Filter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.web.client.RestTemplate;

import com.capgemini.psd2.filter.PSD2Filter;

@EnableEurekaClient
@SpringBootApplication
@ComponentScan(basePackages = { "com.capgemini.psd2" })
@EnableMongoRepositories(basePackages = { "com.capgemini.psd2" })
public class AccountRequestApplication {

	static ConfigurableApplicationContext context = null;

	public static void main(String[] args) {
		try {
			context = SpringApplication.run(AccountRequestApplication.class, args);
		} catch (Exception e) {
			if (context != null) {
				SpringApplication.exit(context, () -> 1);
			}
			System.exit(1);
		}
	}

	@Bean(name = "psd2Filter")
	public Filter psd2Filter() {
		return new PSD2Filter();
	}

	@Bean
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}

}
