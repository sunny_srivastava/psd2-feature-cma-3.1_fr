package com.capgemini.psd2.account.request.service.impl;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.capgemini.psd2.account.request.service.AccountRequestService;
import com.capgemini.psd2.aisp.adapter.AccountRequestAdapter;
import com.capgemini.psd2.aisp.domain.Links;
import com.capgemini.psd2.aisp.domain.Meta;
import com.capgemini.psd2.aisp.domain.OBReadConsent1;
import com.capgemini.psd2.aisp.domain.OBReadConsentResponse1;
import com.capgemini.psd2.aisp.domain.OBReadConsentResponse1Data;
import com.capgemini.psd2.aisp.validation.adapter.AISPCustomValidator;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.domain.OBRisk1;
import com.capgemini.psd2.token.Token;
import com.capgemini.psd2.token.TppInformationTokenData;
import com.capgemini.psd2.utilities.DateUtilites;
import com.capgemini.psd2.utilities.JSONUtilities;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Service
public class AccountRequestServiceImpl implements AccountRequestService {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(AccountRequestServiceImpl.class);

	@Autowired
	@Qualifier("accountRequestRoutingAdapter")
	private AccountRequestAdapter accountRequestAdapter;

	@Autowired
	private RequestHeaderAttributes reqHeaderAtrributes;

	@Value("${cmaVersion}")
	private String cmaVersion;

	@SuppressWarnings("rawtypes")
	@Autowired
	@Qualifier("accountsRoutingValidator")
	private AISPCustomValidator accountsCustomValidator;

	@SuppressWarnings("unchecked")
	@Override
	public OBReadConsentResponse1 createAccountRequest(OBReadConsent1 accountRequestPOSTRequest) {

		OBReadConsent1 accountRequestPOSTRequestValidated = (OBReadConsent1) accountsCustomValidator
				.validateRequestParams(accountRequestPOSTRequest);
		
		String str = JSONUtilities.getJSONOutPutFromObject(accountRequestPOSTRequestValidated);
		OBReadConsentResponse1 accountRequestPOSTResponse = JSONUtilities.getObjectFromJSONString(str, OBReadConsentResponse1.class);

		OBReadConsentResponse1Data data = accountRequestPOSTResponse.getData();
		data.setConsentId(UUID.randomUUID().toString());
		data.setStatus(OBReadConsentResponse1Data.StatusEnum.AWAITINGAUTHORISATION);
		data.setCreationDateTime(DateUtilites.getCurrentDateInISOFormat());
		data.setStatusUpdateDateTime(DateUtilites.getCurrentDateInISOFormat());
		data.setCmaVersion(cmaVersion);
		data.setTppCID(reqHeaderAtrributes.getTppCID());
		data.setTenantId(reqHeaderAtrributes.getTenantId());
		Token token = reqHeaderAtrributes.getToken();
		TppInformationTokenData tppInformationTokenData = token.getTppInformation();
		data.setTppLegalEntityName(tppInformationTokenData.getTppLegalEntityName());
		
		Map<String, String> params = reqHeaderAtrributes.getToken().getSeviceParams();
		if(params == null || params.isEmpty()) {
			LOGGER.info("{\"Method\":\"{}\",\"serviceParams\":\"{}\"}",
					"com.capgemini.psd2.account.request.service.impl.AccountRequestServiceImpl.createAccountRequest()", null);
			params = new HashMap<>();
		}
		
		params.put(PSD2Constants.CO_RELATION_ID, reqHeaderAtrributes.getCorrelationId());
		accountRequestPOSTResponse = accountRequestAdapter.createAccountRequestPOSTResponse(data, params);

		if (accountRequestPOSTResponse.getRisk() == null)
			accountRequestPOSTResponse.setRisk(new OBRisk1());
		if (accountRequestPOSTResponse.getLinks() == null)
			accountRequestPOSTResponse.setLinks(new Links());
		if (accountRequestPOSTResponse.getMeta() == null)
			accountRequestPOSTResponse.setMeta(new Meta());
		accountRequestPOSTResponse.getLinks().setSelf(reqHeaderAtrributes.getSelfUrl() + PSD2Constants.SLASH
				+ accountRequestPOSTResponse.getData().getConsentId());
		accountRequestPOSTResponse.getMeta().setTotalPages(1);
		
		accountsCustomValidator.validateResponseParams(accountRequestPOSTResponse);
		return accountRequestPOSTResponse;
	}

	@SuppressWarnings("unchecked")
	@Override
	public OBReadConsentResponse1 retrieveAccountRequest(String consentId) {
		
		accountsCustomValidator.validateUniqueId(consentId);

		OBReadConsentResponse1 accountRequestGETResponse = accountRequestAdapter.getAccountRequestGETResponse(consentId);
		if (accountRequestGETResponse.getRisk() == null)
			accountRequestGETResponse.setRisk(new OBRisk1());
		if (accountRequestGETResponse.getLinks() == null)
			accountRequestGETResponse.setLinks(new Links());
		if (accountRequestGETResponse.getMeta() == null)
			accountRequestGETResponse.setMeta(new Meta());
		accountRequestGETResponse.getLinks().setSelf(reqHeaderAtrributes.getSelfUrl());
		accountRequestGETResponse.getMeta().setTotalPages(1);
		
		if(NullCheckUtils.isNullOrEmpty(accountRequestGETResponse.getData().getStatusUpdateDateTime())) {
			accountRequestGETResponse.getData().setStatusUpdateDateTime(accountRequestGETResponse.getData().getCreationDateTime());
		}
		
		accountsCustomValidator.validateResponseParams(accountRequestGETResponse);
		
		return accountRequestGETResponse;
	}

	@Override
	public void removeAccountRequest(String consentId) {
		
		accountsCustomValidator.validateUniqueId(consentId);

		accountRequestAdapter.removeAccountRequest(consentId, reqHeaderAtrributes.getTppCID());
	}

}
