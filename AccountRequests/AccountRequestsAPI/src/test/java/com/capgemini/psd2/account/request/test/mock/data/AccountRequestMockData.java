package com.capgemini.psd2.account.request.test.mock.data;

import java.util.ArrayList;
import java.util.List;

import com.capgemini.psd2.aisp.domain.Links;
import com.capgemini.psd2.aisp.domain.Meta;
import com.capgemini.psd2.aisp.domain.OBReadConsent1;
import com.capgemini.psd2.aisp.domain.OBReadConsent1Data;
import com.capgemini.psd2.aisp.domain.OBReadConsentResponse1;
import com.capgemini.psd2.aisp.domain.OBReadConsentResponse1Data;
import com.capgemini.psd2.pisp.domain.OBRisk1;
import com.fasterxml.jackson.databind.ObjectMapper;

public class AccountRequestMockData {

	public static OBReadConsent1 getAccountRequestsParamatersMockData() {
		OBReadConsent1 accountRequestsParammeters = new OBReadConsent1();
		OBReadConsent1Data data = new OBReadConsent1Data();
		//Data data = new Data();
		List<OBReadConsent1Data.PermissionsEnum> permissions = new ArrayList<>();
		permissions.add(OBReadConsent1Data.PermissionsEnum.READACCOUNTSDETAIL);
		permissions.add(OBReadConsent1Data.PermissionsEnum.READBALANCES);
		permissions.add(OBReadConsent1Data.PermissionsEnum.READACCOUNTSBASIC);
		permissions.add(OBReadConsent1Data.PermissionsEnum.READTRANSACTIONSBASIC);
		permissions.add(OBReadConsent1Data.PermissionsEnum.READTRANSACTIONSDETAIL);
		data.setPermissions(permissions);
		data.setExpirationDateTime("2019-05-02T00:00:00+00:00");
		data.setTransactionFromDateTime("2017-05-03T00:00:00+00:00");
		data.setTransactionToDateTime("2017-05-03T00:00:00+00:00");

		accountRequestsParammeters.setData(data);
		accountRequestsParammeters.setRisk(null);
		return accountRequestsParammeters;
	}
		
	public static OBReadConsent1 getAccountRequestsParamatersMockDataOrConditions() {
		OBReadConsent1 accountRequestsParammeters = new OBReadConsent1();
		//Data data = new Data();
		OBReadConsent1Data data = new OBReadConsent1Data();

		List<OBReadConsent1Data.PermissionsEnum> permissions = new ArrayList<>();
		permissions.add(OBReadConsent1Data.PermissionsEnum.READACCOUNTSDETAIL);
		permissions.add(OBReadConsent1Data.PermissionsEnum.READBALANCES);
		permissions.add(OBReadConsent1Data.PermissionsEnum.READTRANSACTIONSDEBITS);
		permissions.add(OBReadConsent1Data.PermissionsEnum.READTRANSACTIONSDETAIL);
		permissions.add(OBReadConsent1Data.PermissionsEnum.READTRANSACTIONSBASIC);

		data.setPermissions(permissions);
		data.setExpirationDateTime("2019-05-02T00:00:00+00:00");
		data.setTransactionFromDateTime("2017-05-03T00:00:00+00:00");
		data.setTransactionToDateTime("2017-05-03T00:00:00+00:00");

		accountRequestsParammeters.setData(data);
		accountRequestsParammeters.setRisk(null);
		return accountRequestsParammeters;
	}
	
	public static OBReadConsent1 getAccountRequestsParamatersExceptionMockData() {
		OBReadConsent1 accountRequestsParammeters = new OBReadConsent1();
		//Data data = new Data();
		OBReadConsent1Data data = new OBReadConsent1Data();

		List<OBReadConsent1Data.PermissionsEnum> permissions = new ArrayList<>();
		permissions.add(OBReadConsent1Data.PermissionsEnum.READACCOUNTSBASIC);
		
		data.setPermissions(permissions);
		data.setExpirationDateTime("2019-05-02T00:00:00+00:00");
		data.setTransactionFromDateTime("2017-05-03T00:00:00+00:00");
		data.setTransactionToDateTime("2017-05-03T00:00:00+00:00");

		accountRequestsParammeters.setData(data);
		accountRequestsParammeters.setRisk(null);
		return accountRequestsParammeters;
	}
	
	public static OBReadConsentResponse1 getAccountRequestsMockResponseData() {

		OBReadConsentResponse1 accountRequestPOSTResponse = new OBReadConsentResponse1();

		//Data1 data = new Data1();
		OBReadConsentResponse1Data data = new OBReadConsentResponse1Data();

		List<OBReadConsentResponse1Data.PermissionsEnum> permissions = new ArrayList<>();
		permissions.add(OBReadConsentResponse1Data.PermissionsEnum.READACCOUNTSBASIC);
		permissions.add(OBReadConsentResponse1Data.PermissionsEnum.READACCOUNTSDETAIL);
		permissions.add(OBReadConsentResponse1Data.PermissionsEnum.READBALANCES);

		data.setPermissions(permissions);
		data.setCreationDateTime("2017-05-02T00:00:00+00:00");
		data.setExpirationDateTime("2019-05-02T00:00:00+00:00");
		data.setTransactionFromDateTime("2017-05-03T00:00:00+00:00");
		data.setTransactionToDateTime("2017-12-03T00:00:00+00:00");

		data.setConsentId("af5b90c1-64b5-4a52-ba55-5eed68b2a269");
		data.setStatus(OBReadConsentResponse1Data.StatusEnum.AWAITINGAUTHORISATION);
		
		data.setTppCID("6443e15975554bce8099e35b88b40465");

		accountRequestPOSTResponse.setData(data);
		accountRequestPOSTResponse.setRisk(null);

		return accountRequestPOSTResponse;
	}
	
	public static OBReadConsentResponse1 getAccountRequestsBranchCoverageMockResponseData() {

		OBReadConsentResponse1 accountRequestPOSTResponse = new OBReadConsentResponse1();
		OBReadConsentResponse1Data data = new OBReadConsentResponse1Data();
		//Data1 data = new Data1();

		List<OBReadConsentResponse1Data.PermissionsEnum> permissions = new ArrayList<>();
		permissions.add(OBReadConsentResponse1Data.PermissionsEnum.READACCOUNTSBASIC);
		permissions.add(OBReadConsentResponse1Data.PermissionsEnum.READACCOUNTSDETAIL);
		permissions.add(OBReadConsentResponse1Data.PermissionsEnum.READBALANCES);

		data.setPermissions(permissions);
		data.setCreationDateTime("2017-05-02T00:00:00+00:00");
		data.setExpirationDateTime("2019-05-02T00:00:00+00:00");
		data.setTransactionFromDateTime("2017-05-03T00:00:00+00:00");
		data.setTransactionToDateTime("2017-12-03T00:00:00+00:00");

		data.setConsentId("af5b90c1-64b5-4a52-ba55-5eed68b2a269");
		data.setStatus(OBReadConsentResponse1Data.StatusEnum.AWAITINGAUTHORISATION);
		
		data.setTppCID("6443e15975554bce8099e35b88b40465");
		
		accountRequestPOSTResponse.setData(data);
		OBRisk1 risk=new OBRisk1();
		accountRequestPOSTResponse.setRisk(risk);
		Links links = new Links();
		links.setSelf("http://localhost:8989/account-requests");
		accountRequestPOSTResponse.setLinks(links);
		Meta metaData = new Meta();
		metaData.setTotalPages(1);
		accountRequestPOSTResponse.setMeta(metaData);

		return accountRequestPOSTResponse;
	}
	
	public static String asJsonString(final Object obj) {
		try {
			return new ObjectMapper().writeValueAsString(obj);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

}