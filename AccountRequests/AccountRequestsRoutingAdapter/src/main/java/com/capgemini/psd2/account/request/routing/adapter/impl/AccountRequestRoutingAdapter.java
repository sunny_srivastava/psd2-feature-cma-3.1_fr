package com.capgemini.psd2.account.request.routing.adapter.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.capgemini.psd2.account.request.routing.adapter.routing.AccountRequestAdapterFactory;
import com.capgemini.psd2.aisp.adapter.AccountRequestAdapter;
import com.capgemini.psd2.aisp.domain.OBReadConsentResponse1;
import com.capgemini.psd2.aisp.domain.OBReadConsentResponse1Data;

public class AccountRequestRoutingAdapter implements AccountRequestAdapter {
	
	/** The default adapter. */
	@Value("${app.defaultAdapterAccountReq}")
	private String defaultAdapter;
	
	@Autowired
	private AccountRequestAdapterFactory accountRequestAdapterFactory;

	@Override
	public OBReadConsentResponse1 getAccountRequestGETResponse(String accountRequestId) {

		AccountRequestAdapter accountRequestAdapter = accountRequestAdapterFactory.getAdapterInstance(defaultAdapter);
		return accountRequestAdapter.getAccountRequestGETResponse(accountRequestId);
	}

	@Override
	public OBReadConsentResponse1 updateAccountRequestResponse(String accountRequestId, OBReadConsentResponse1Data.StatusEnum statusEnum) {

		AccountRequestAdapter accountRequestAdapter = accountRequestAdapterFactory.getAdapterInstance(defaultAdapter);
		return accountRequestAdapter.updateAccountRequestResponse(accountRequestId, statusEnum);
	}
	
	@Override
	public void removeAccountRequest(String accountRequestId,String cid) {
		AccountRequestAdapter accountRequestAdapter = accountRequestAdapterFactory.getAdapterInstance(defaultAdapter);
		accountRequestAdapter.removeAccountRequest(accountRequestId,cid);
	}

	@Override
	public OBReadConsentResponse1 createAccountRequestPOSTResponse(
			OBReadConsentResponse1Data data, Map<String, String> params) {
		AccountRequestAdapter accountRequestAdapter = accountRequestAdapterFactory.getAdapterInstance(defaultAdapter);
		return accountRequestAdapter.createAccountRequestPOSTResponse(data, params);
	}
}
