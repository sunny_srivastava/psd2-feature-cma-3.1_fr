/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.adapter.routing.test.adapter;

import java.util.Map;

import com.capgemini.psd2.adapter.routing.test.data.AccountRequestRoutingAdapterTestMockData;
import com.capgemini.psd2.aisp.adapter.AccountRequestAdapter;
import com.capgemini.psd2.aisp.domain.OBReadConsentResponse1;
import com.capgemini.psd2.aisp.domain.OBReadConsentResponse1Data;


public class AccountRequestTestRoutingAdapter implements AccountRequestAdapter {

	@Override
	public OBReadConsentResponse1 getAccountRequestGETResponse(String accountRequestId) {
		return AccountRequestRoutingAdapterTestMockData.getAccountRequestGETResponse(accountRequestId);
	}

	@Override
	public void removeAccountRequest(String accountRequestId, String cid) {

	}

	@Override
	public OBReadConsentResponse1 updateAccountRequestResponse(String accountRequestId,
			OBReadConsentResponse1Data.StatusEnum statusEnum) {
		return AccountRequestRoutingAdapterTestMockData.updateAccountRequestResponse(accountRequestId, statusEnum);
	}
	
	@Override
	public OBReadConsentResponse1 createAccountRequestPOSTResponse(
			OBReadConsentResponse1Data data, Map<String, String> params) {
		return AccountRequestRoutingAdapterTestMockData.postAccountRequestPOSTResponse(data);
	}

}
