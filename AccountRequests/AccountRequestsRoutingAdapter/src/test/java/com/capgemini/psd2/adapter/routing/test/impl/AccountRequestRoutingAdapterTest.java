package com.capgemini.psd2.adapter.routing.test.impl;

import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyString;

import java.util.HashMap;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.account.request.routing.adapter.impl.AccountRequestRoutingAdapter;
import com.capgemini.psd2.account.request.routing.adapter.routing.AccountRequestAdapterFactory;
import com.capgemini.psd2.adapter.routing.test.adapter.AccountRequestTestRoutingAdapter;
import com.capgemini.psd2.adapter.routing.test.data.AccountRequestRoutingAdapterTestMockData;
import com.capgemini.psd2.aisp.adapter.AccountRequestAdapter;
import com.capgemini.psd2.aisp.domain.OBReadConsentResponse1;
import com.capgemini.psd2.aisp.domain.OBReadConsentResponse1Data;


@RunWith(SpringJUnit4ClassRunner.class)
public class AccountRequestRoutingAdapterTest {
	
	@Mock
	private AccountRequestAdapterFactory accountRequestAdapterFactory;
	
	@InjectMocks
	private AccountRequestAdapter accountRequestRountingAdapter = new AccountRequestRoutingAdapter();
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}
	
	
	@Test
	public void testAccountRequestPOSTRequest() {
		AccountRequestAdapter accountRequestAdapter = new AccountRequestTestRoutingAdapter();
		Mockito.when(accountRequestAdapterFactory.getAdapterInstance(anyString())).thenReturn(accountRequestAdapter);
	
		OBReadConsentResponse1Data  data = AccountRequestRoutingAdapterTestMockData.createData();
		OBReadConsentResponse1 accountRequestPOSTResponse = accountRequestRountingAdapter.createAccountRequestPOSTResponse(data, new HashMap<>());
		
		assertTrue(accountRequestPOSTResponse.getData().getStatus().equals(OBReadConsentResponse1Data.StatusEnum.AWAITINGAUTHORISATION));
	}

	
	@Test
	public void testAccountRequestGETRequest() {
		AccountRequestAdapter accountRequestAdapter = new AccountRequestTestRoutingAdapter();
		Mockito.when(accountRequestAdapterFactory.getAdapterInstance(anyString())).thenReturn(accountRequestAdapter);
	
		OBReadConsentResponse1 accountRequestPOSTResponse = accountRequestRountingAdapter.getAccountRequestGETResponse(anyString());
		
		assertTrue(accountRequestPOSTResponse.getData().getStatus().equals(OBReadConsentResponse1Data.StatusEnum.AWAITINGAUTHORISATION));
	}
	
	@Test
	public void testAccountRequestUPDATERequest() {
		AccountRequestAdapter accountRequestAdapter = new AccountRequestTestRoutingAdapter();
		Mockito.when(accountRequestAdapterFactory.getAdapterInstance(anyString())).thenReturn(accountRequestAdapter);
	
		OBReadConsentResponse1 accountRequestPOSTResponse = accountRequestRountingAdapter.updateAccountRequestResponse(anyString(), OBReadConsentResponse1Data.StatusEnum.AUTHORISED);
		
		assertTrue(accountRequestPOSTResponse.getData().getStatus().equals(OBReadConsentResponse1Data.StatusEnum.AUTHORISED));
	}
	
	@Test
	public void testAccountRequestDELETERequest() {
		AccountRequestAdapter accountRequestAdapter = new AccountRequestTestRoutingAdapter();
		Mockito.when(accountRequestAdapterFactory.getAdapterInstance(anyString())).thenReturn(accountRequestAdapter);
	
		accountRequestRountingAdapter.removeAccountRequest(anyString(), null);
		
	}
}
