package com.capgemini.psd2.foundationservice.raml.exceptions;

import java.util.Date;

public class CommonHeader{
  private String originalAPITransactionId;

  private String sourceSystem;

  private String sourceUser;

  private Date timestamp;

  private String correlationId;

  private boolean replayOnFailure;

  private ReplayIntervalType replayInterval;

  private Number replyAfter;

  private int replayThreshold;

  private String originalMessage;

  private ErrorInfo errorInfo;

  public ErrorInfo getErrorInfo() {
	return errorInfo;
}

public void setErrorInfo(ErrorInfo errorInfo) {
	this.errorInfo = errorInfo;
}

public String getOriginalAPITransactionId() {
    return this.originalAPITransactionId;
  }

  public void setOriginalAPITransactionId(String originalAPITransactionId) {
    this.originalAPITransactionId = originalAPITransactionId;
  }

  public String getSourceSystem() {
    return this.sourceSystem;
  }

  public void setSourceSystem(String sourceSystem) {
    this.sourceSystem = sourceSystem;
  }

  public String getSourceUser() {
    return this.sourceUser;
  }

  public void setSourceUser(String sourceUser) {
    this.sourceUser = sourceUser;
  }

  public Date getTimestamp() {
    return this.timestamp;
  }

  public void setTimestamp(Date timestamp) {
    this.timestamp = timestamp;
  }

  public String getCorrelationId() {
    return this.correlationId;
  }

  public void setCorrelationId(String correlationId) {
    this.correlationId = correlationId;
  }

  public boolean getReplayOnFailure() {
    return this.replayOnFailure;
  }

  public void setReplayOnFailure(boolean replayOnFailure) {
    this.replayOnFailure = replayOnFailure;
  }
  
  public Number getReplyAfter() {
    return this.replyAfter;
  }

  public void setReplyAfter(Number replyAfter) {
    this.replyAfter = replyAfter;
  }

  public int getReplayThreshold() {
    return this.replayThreshold;
  }

  public void setReplayThreshold(int replayThreshold) {
    this.replayThreshold = replayThreshold;
  }

  public String getOriginalMessage() {
    return this.originalMessage;
  }

  public void setOriginalMessage(String originalMessage) {
    this.originalMessage = originalMessage;
  }

  public CommonHeader balanceType(ReplayIntervalType replayInterval) {
	    this.replayInterval = replayInterval;
	    return this;
	}
	  public ReplayIntervalType getBalanceType() {
	    return replayInterval;
	  }

	  public void setBalanceType(ReplayIntervalType replayInterval) {
	    this.replayInterval = replayInterval;
	  }
	  
	  
	 

}
