package com.capgemini.psd2.foundationservice.exceptions;

import com.capgemini.psd2.foundationservice.exceptions.ValidationViolation;

public class MockFoundationServiceException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	private final ValidationViolation validationViolation;

	public MockFoundationServiceException(String message, ValidationViolation validationViolation) {
		super(message);
		this.validationViolation = validationViolation;
	}

	public ValidationViolation getErrorInfo() {
		return validationViolation;
	}

	public static MockFoundationServiceException populateMockFoundationServiceException(ErrorCodeEnum errorCodeEnum) {
		ValidationViolation validationViolation = new ValidationViolation();
		validationViolation.setErrorCode(errorCodeEnum.getErrorCode());
		validationViolation.setErrorField(errorCodeEnum.getErrorField());
		validationViolation.setErrorText(errorCodeEnum.getErrorText());
		validationViolation.setErrorValue(errorCodeEnum.getStatus().toString());

		return new MockFoundationServiceException(errorCodeEnum.getErrorText(), validationViolation);
	}

}
