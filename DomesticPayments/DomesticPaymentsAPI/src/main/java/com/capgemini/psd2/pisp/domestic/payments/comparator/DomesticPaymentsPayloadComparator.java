package com.capgemini.psd2.pisp.domestic.payments.comparator;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

import org.springframework.stereotype.Component;

import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.pisp.domain.CustomDPaymentConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomDPaymentConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.CustomDPaymentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.OBDomestic1;
import com.capgemini.psd2.pisp.domain.OBDomestic1InstructedAmount;
import com.capgemini.psd2.pisp.domain.OBPostalAddress6;
import com.capgemini.psd2.pisp.domain.OBRemittanceInformation1;
import com.capgemini.psd2.pisp.domain.OBRisk1DeliveryAddress;
import com.capgemini.psd2.pisp.domain.OBWriteDomesticConsentResponse1;
import com.capgemini.psd2.pisp.domain.PaymentConsentsPlatformResource;
import com.capgemini.psd2.pisp.utilities.PispUtilities;
import com.capgemini.psd2.utilities.JSONUtilities;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Component
public class DomesticPaymentsPayloadComparator implements Comparator<CustomDPaymentConsentsPOSTResponse> {

	@Override
	public int compare(CustomDPaymentConsentsPOSTResponse paymentResponse,
			CustomDPaymentConsentsPOSTResponse adaptedPaymentResponse) {

		int value = 1;

		if (Double.compare(Double.parseDouble(paymentResponse.getData().getInitiation().getInstructedAmount().getAmount()),Double
				.parseDouble(adaptedPaymentResponse.getData().getInitiation().getInstructedAmount().getAmount()))==0) {
			adaptedPaymentResponse.getData().getInitiation().getInstructedAmount()
					.setAmount(paymentResponse.getData().getInitiation().getInstructedAmount().getAmount());
		}

		if (paymentResponse.getData().getInitiation().equals(adaptedPaymentResponse.getData().getInitiation())
				&& paymentResponse.getRisk().equals(adaptedPaymentResponse.getRisk()))
			value = 0;

		return value;
	}

	public int comparePaymentDetails(Object response, Object request,
			PaymentConsentsPlatformResource paymentSetupPlatformResource) {
		CustomDPaymentConsentsPOSTResponse paymentSetupResponse = null;
		CustomDPaymentConsentsPOSTResponse tempPaymentSetupResponse = null;
		CustomDPaymentConsentsPOSTResponse adaptedPaymentResponse = null;
		CustomDPaymentConsentsPOSTRequest paymentSetupRequest = null;
		CustomDPaymentsPOSTRequest paymentSubmissionRequest = null;
		int returnValue = 1;

		if (response instanceof CustomDPaymentConsentsPOSTResponse) {
			paymentSetupResponse = (CustomDPaymentConsentsPOSTResponse) response;
			String creationDateTime = paymentSetupResponse.getData().getCreationDateTime();
			paymentSetupResponse.getData().setCreationDateTime(null);
			String strPaymentResponse = JSONUtilities.getJSONOutPutFromObject(paymentSetupResponse);
			tempPaymentSetupResponse = JSONUtilities.getObjectFromJSONString(PispUtilities.getObjectMapper(),
					strPaymentResponse, CustomDPaymentConsentsPOSTResponse.class);
			paymentSetupResponse.getData().setCreationDateTime(creationDateTime);
		}

		if (request instanceof CustomDPaymentConsentsPOSTRequest) {
			paymentSetupRequest = (CustomDPaymentConsentsPOSTRequest) request;
			String strPaymentRequest = JSONUtilities.getJSONOutPutFromObject(paymentSetupRequest);
			adaptedPaymentResponse = JSONUtilities.getObjectFromJSONString(PispUtilities.getObjectMapper(),
					strPaymentRequest, CustomDPaymentConsentsPOSTResponse.class);
		} else if (request instanceof CustomDPaymentsPOSTRequest) {
			paymentSubmissionRequest = (CustomDPaymentsPOSTRequest) request;
			String strSubmissionRequest = JSONUtilities.getJSONOutPutFromObject(paymentSubmissionRequest);
			adaptedPaymentResponse = JSONUtilities.getObjectFromJSONString(PispUtilities.getObjectMapper(),
					strSubmissionRequest, CustomDPaymentConsentsPOSTResponse.class);
		}

		if (tempPaymentSetupResponse != null && adaptedPaymentResponse != null) {

			if (!validateDebtorDetails(tempPaymentSetupResponse.getData().getInitiation(),
					adaptedPaymentResponse.getData().getInitiation(), paymentSetupPlatformResource))
				return 1;
			compareAmount(tempPaymentSetupResponse.getData().getInitiation().getInstructedAmount(),
					adaptedPaymentResponse.getData().getInitiation().getInstructedAmount());

			/*
			 * Date: 13-Feb-2018 Changes are made for CR-10
			 */
			checkAndModifyEmptyFields(adaptedPaymentResponse, (CustomDPaymentConsentsPOSTResponse) response);
			// End of CR-10

			returnValue = compare(tempPaymentSetupResponse, adaptedPaymentResponse);
		}
		return returnValue;

	}

	public boolean validateDebtorDetails(OBDomestic1 responseInitiation, OBDomestic1 requestInitiation,
			PaymentConsentsPlatformResource paymentSetupPlatformResource) {

		if (Boolean.valueOf(paymentSetupPlatformResource.getTppDebtorDetails())) {

			/*
			 * If DebtorAccount.Name is sent by TPP,tppDebtoNameDetails would be true, then
			 * name will participate in comparison else product will set foundation response
			 * DebtorAccount.Name as null so that it can be passed in comparison. Both
			 * fields would have the value as 'Null'.
			 */
			if (!Boolean.valueOf(paymentSetupPlatformResource.getTppDebtorNameDetails()))
				responseInitiation.getDebtorAccount().setName(null);
			String schemeNameOfResponseInitiation = responseInitiation.getDebtorAccount().getSchemeName();
			String schemeNameOfRequestInitiation = requestInitiation.getDebtorAccount().getSchemeName();
			if(schemeNameOfResponseInitiation!= null && schemeNameOfResponseInitiation.startsWith(PSD2Constants.OB_SCHEME_PREFIX)){
				responseInitiation.getDebtorAccount().setSchemeName(schemeNameOfResponseInitiation.split(PSD2Constants.OB_SCHEME_PREFIX)[1]);
		
			}
			if (schemeNameOfRequestInitiation!= null && schemeNameOfRequestInitiation.startsWith(PSD2Constants.OB_SCHEME_PREFIX)) {
				requestInitiation.getDebtorAccount().setSchemeName(schemeNameOfRequestInitiation.split(PSD2Constants.OB_SCHEME_PREFIX)[1]);
			}
			return Objects.equals(responseInitiation.getDebtorAccount(), requestInitiation.getDebtorAccount());
		}
			
		if (!Boolean.valueOf(paymentSetupPlatformResource.getTppDebtorDetails())
				&& requestInitiation.getDebtorAccount() != null) {
			return Boolean.FALSE;
		}
		if (!Boolean.valueOf(paymentSetupPlatformResource.getTppDebtorDetails())
				&& responseInitiation.getDebtorAccount() != null) {
			responseInitiation.setDebtorAccount(null);
			return Boolean.TRUE;
		}
		return Boolean.TRUE;

	}

	/*
	 * Date: 12-Jan-2018, SIT defect#956 Comparing two float values received from FS
	 * and TPP. Both values must be treated equal mathematically if values are in
	 * below format. i.e 1.2 == 1.20, 0.20000 == 0.20, 0.2 == 00.20000, 0.2 ==
	 * 0.20000, 001.00 == 1.00 etc. If values are equal then set the amount received
	 * from TPP to the amount object of FS response. By doing this, comparison would
	 * be passed in swagger's java file(compare method of this class) and same
	 * amount would be available to TPP also in response of idempotent request.
	 */
	private void compareAmount(OBDomestic1InstructedAmount responseInstructedAmount,
			OBDomestic1InstructedAmount requestInstructedAmount) {
		if (Double.compare(Double.parseDouble(responseInstructedAmount.getAmount()),
				Double.parseDouble(requestInstructedAmount.getAmount())) == 0)
			responseInstructedAmount.setAmount(requestInstructedAmount.getAmount());
	}

	private void checkAndModifyEmptyFields(OBWriteDomesticConsentResponse1 adaptedPaymentResponse,
			CustomDPaymentConsentsPOSTResponse response) {

		checkAndModifyRemittanceInformation(adaptedPaymentResponse.getData().getInitiation(), response);
		checkAndModifyCreditorPostalAddressInformation(adaptedPaymentResponse.getData().getInitiation(), response);

		if (adaptedPaymentResponse.getRisk().getDeliveryAddress() != null) {
			checkAndModifyAddressLine(adaptedPaymentResponse.getRisk().getDeliveryAddress(), response);
		}

		if (adaptedPaymentResponse.getData().getInitiation().getCreditorPostalAddress() != null
				&& adaptedPaymentResponse.getData().getInitiation().getCreditorPostalAddress()
						.getAddressLine() != null) {
			checkAndModifyCreditorPostalAddressLine(
					adaptedPaymentResponse.getData().getInitiation().getCreditorPostalAddress(), response);
		}

	}

	private void checkAndModifyRemittanceInformation(OBDomestic1 responseInitiation,
			CustomDPaymentConsentsPOSTResponse response) {

		OBRemittanceInformation1 remittanceInformation = responseInitiation.getRemittanceInformation();
		if (remittanceInformation != null && response.getData().getInitiation().getRemittanceInformation() == null
				&& NullCheckUtils.isNullOrEmpty(remittanceInformation.getReference())
				&& NullCheckUtils.isNullOrEmpty(remittanceInformation.getUnstructured())) {

			responseInitiation.setRemittanceInformation(null);
		}
	}

	private void checkAndModifyCreditorPostalAddressInformation(OBDomestic1 responseInitiation,
			CustomDPaymentConsentsPOSTResponse response) {

		OBPostalAddress6 crPostaladdInformation = responseInitiation.getCreditorPostalAddress();
		if (crPostaladdInformation != null && response.getData().getInitiation().getCreditorPostalAddress() == null
				&& NullCheckUtils.isNullOrEmpty(crPostaladdInformation.getAddressLine())
				&& NullCheckUtils.isNullOrEmpty(crPostaladdInformation.getAddressType())
				&& NullCheckUtils.isNullOrEmpty(crPostaladdInformation.getDepartment())
				&& NullCheckUtils.isNullOrEmpty(crPostaladdInformation.getSubDepartment())
				&& NullCheckUtils.isNullOrEmpty(crPostaladdInformation.getStreetName())
				&& NullCheckUtils.isNullOrEmpty(crPostaladdInformation.getBuildingNumber())
				&& NullCheckUtils.isNullOrEmpty(crPostaladdInformation.getPostCode())
				&& NullCheckUtils.isNullOrEmpty(crPostaladdInformation.getTownName())
				&& NullCheckUtils.isNullOrEmpty(crPostaladdInformation.getCountrySubDivision())
				&& NullCheckUtils.isNullOrEmpty(crPostaladdInformation.getCountry())) {

			responseInitiation.setCreditorPostalAddress(null);
		}
	}

	private void checkAndModifyAddressLine(OBRisk1DeliveryAddress riskDeliveryAddress,
			CustomDPaymentConsentsPOSTResponse response) {

		if (riskDeliveryAddress.getAddressLine() != null) {
			List<String> addressLineList = new ArrayList<>();
			for (String addressLine : riskDeliveryAddress.getAddressLine()) {
				if (NullCheckUtils.isNullOrEmpty(addressLine))
					continue;
				addressLineList.add(addressLine);
			}
			if (addressLineList.isEmpty() && response.getRisk().getDeliveryAddress().getAddressLine() == null)
				addressLineList = null;

			riskDeliveryAddress.setAddressLine(addressLineList);

		}
	}

	private void checkAndModifyCreditorPostalAddressLine(OBPostalAddress6 obPostalAddress6,
			CustomDPaymentConsentsPOSTResponse response) {
		List<String> addressLineList = new ArrayList<>();
		for (String addressLine : (obPostalAddress6.getAddressLine())) {
			if (NullCheckUtils.isNullOrEmpty(addressLine))
				continue;
			addressLineList.add(addressLine);
		}
		if (addressLineList.isEmpty()
				&& response.getData().getInitiation().getCreditorPostalAddress().getAddressLine() == null)
			addressLineList = null;

		obPostalAddress6.setAddressLine(addressLineList);
	}

}
