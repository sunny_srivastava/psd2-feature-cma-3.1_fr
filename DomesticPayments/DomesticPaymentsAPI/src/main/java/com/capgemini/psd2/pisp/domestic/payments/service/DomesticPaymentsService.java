package com.capgemini.psd2.pisp.domestic.payments.service;

import com.capgemini.psd2.pisp.domain.CustomDPaymentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.PaymentDomesticSubmitPOST201Response;

public interface DomesticPaymentsService {

	// Interfaces for DomesticPayment Payments v3.0 API
	public PaymentDomesticSubmitPOST201Response createDomesticPaymentsResource(
			CustomDPaymentsPOSTRequest paymentSetupRequest);

	public PaymentDomesticSubmitPOST201Response retrieveDomesticPaymentsResource(String submissionId);

}
