package com.capgemini.psd2.pisp.domestic.payments.test.service.impl;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyMap;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.consent.domain.PispConsent;
import com.capgemini.psd2.exceptions.OBPSD2ExceptionUtility;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.adapter.DomesticPaymentsAdapter;
import com.capgemini.psd2.pisp.adapter.PispConsentAdapter;
import com.capgemini.psd2.pisp.domain.CustomDPaymentConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.CustomDPaymentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.OBCashAccountDebtor3;
import com.capgemini.psd2.pisp.domain.OBDomestic1;
import com.capgemini.psd2.pisp.domain.OBExternalStatus2Code;
import com.capgemini.psd2.pisp.domain.OBMultiAuthorisation1;
import com.capgemini.psd2.pisp.domain.OBPostalAddress6;
import com.capgemini.psd2.pisp.domain.OBRemittanceInformation1;
import com.capgemini.psd2.pisp.domain.OBWriteDataDomestic1;
import com.capgemini.psd2.pisp.domain.OBWriteDataDomesticConsentResponse1;
import com.capgemini.psd2.pisp.domain.OBWriteDataDomesticResponse1;
import com.capgemini.psd2.pisp.domain.PaymentDomesticSubmitPOST201Response;
import com.capgemini.psd2.pisp.domain.PaymentsPlatformResource;
import com.capgemini.psd2.pisp.domestic.payments.comparator.DomesticPaymentsPayloadComparator;
import com.capgemini.psd2.pisp.domestic.payments.service.impl.DomesticPaymentsServiceImpl;
import com.capgemini.psd2.pisp.processing.adapter.service.PaymentSubmissionProcessingAdapter;
import com.capgemini.psd2.pisp.stage.operations.adapter.DomesticPaymentStagingAdapter;
import com.capgemini.psd2.pisp.status.PaymentConstants;

@RunWith(SpringJUnit4ClassRunner.class)
public class DomesticPaymentsServiceImplTest {

	PaymentsPlatformResource resource = new PaymentsPlatformResource();

	@InjectMocks
	private DomesticPaymentsServiceImpl domesticPaymentsServiceImpl;

	@Mock
	private DomesticPaymentsPayloadComparator dPaymentComparator;

	@Mock
	private DomesticPaymentStagingAdapter domesticPaymentStagingAdapter;;

	@Mock
	private PaymentSubmissionProcessingAdapter paymentsProcessingAdapter;

	@Mock
	private DomesticPaymentsAdapter domesticPaymentsAdapter;
	
	@Mock
	private PispConsentAdapter pispConsentAdapter;
	
	@Mock
	private RequestHeaderAttributes requestHeaderAttr;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);

		Map<String, String> map = new HashMap<>();

		map.put("SIGNATURE", "Something wrong with x-jws-signature request header");

		Map<String, String> specificErrorMessageMap = new HashMap<>();

		specificErrorMessageMap.put("signature_invalid_content",
				"Invalid content prsent in joseheader of x-jws-signature header");

		specificErrorMessageMap.put("signature_missing", "signature header missing in request");
		OBPSD2ExceptionUtility.genericErrorMessages.putAll(map);
		OBPSD2ExceptionUtility.specificErrorMessages.putAll(specificErrorMessageMap);
	}

	@Test
	public void contextLoads() {
	}

	@Test
	public void testCreateDomesticPaymentsResourceInComplete() {
		CustomDPaymentsPOSTRequest request = new CustomDPaymentsPOSTRequest();

		OBWriteDataDomestic1 data = new OBWriteDataDomestic1();
		data.setConsentId("11112");
		OBDomestic1 initiation = new OBDomestic1();
		initiation.setLocalInstrument("testLocal");
		OBCashAccountDebtor3 debtorAccount = new OBCashAccountDebtor3();
		debtorAccount.setName("");
		initiation.setDebtorAccount(debtorAccount);
		data.setInitiation(initiation);
		request.setData(data);
		initiation.setRemittanceInformation(new OBRemittanceInformation1());
		initiation.setCreditorPostalAddress(new OBPostalAddress6());

		PispConsent pispConsent = new PispConsent();
		pispConsent.setPartyIdentifier("123");
		
		Map<String, Object> map = new HashMap<>();
		Object testObject = new Object();
		map.put("For testing", testObject);

		Mockito.when(paymentsProcessingAdapter.preSubmissionProcessing(anyObject(), anyObject(), any()))
				.thenReturn(map);

		Mockito.when(dPaymentComparator.comparePaymentDetails(anyObject(), anyObject(), anyObject())).thenReturn(0);

		CustomDPaymentConsentsPOSTResponse response = new CustomDPaymentConsentsPOSTResponse();
		response.setFraudScore(new Object());
		response.setData(new OBWriteDataDomesticConsentResponse1());
		response.getData().setInitiation(new OBDomestic1());
		response.getData().getInitiation().setDebtorAccount(new OBCashAccountDebtor3());

		Mockito.when(domesticPaymentStagingAdapter.retrieveStagedDomesticPaymentConsents(anyObject(), anyMap()))
				.thenReturn(response);

		PaymentDomesticSubmitPOST201Response paymentsFoundationResponse = new PaymentDomesticSubmitPOST201Response();
		OBWriteDataDomesticResponse1 data1 = new OBWriteDataDomesticResponse1();
		OBMultiAuthorisation1 multiAuthorisation = new OBMultiAuthorisation1();
		multiAuthorisation.setStatus(OBExternalStatus2Code.AUTHORISED);
		data1.setInitiation(new OBDomestic1());
		data1.getInitiation().setRemittanceInformation(new OBRemittanceInformation1());
		data1.getInitiation().setCreditorPostalAddress(new OBPostalAddress6());
		data1.setMultiAuthorisation(multiAuthorisation);
		paymentsFoundationResponse.setData(data1);

		Mockito.when(domesticPaymentsAdapter.processDomesticPayments(anyObject(), anyMap(), anyMap()))
				.thenReturn(paymentsFoundationResponse);
		Mockito.when(domesticPaymentsAdapter.retrieveStagedDomesticPayments(anyObject(), anyMap()))
				.thenReturn(paymentsFoundationResponse);

		PaymentsPlatformResource paymentsplatformresource = new PaymentsPlatformResource();
		paymentsplatformresource.setCreatedAt("30-10-2018");
		paymentsplatformresource.setProccessState("Incomplete");

		Mockito.when(paymentsProcessingAdapter.createInitialPaymentsPlatformResource(anyString(), anyString(), any()))
				.thenReturn(paymentsplatformresource);

		Mockito.when(paymentsProcessingAdapter.postPaymentProcessFlows(anyMap(), anyObject(), any(), anyString(), any(),
				anyString())).thenReturn(new PaymentDomesticSubmitPOST201Response());

		Mockito.when(pispConsentAdapter.retrieveConsentByPaymentId("11112")).thenReturn(pispConsent);
		domesticPaymentsServiceImpl.createDomesticPaymentsResource(request);
	}

	@Test
	public void testCreateDomesticPaymentsResourceInComplete_DebtorAccountNull() {
		CustomDPaymentsPOSTRequest request = new CustomDPaymentsPOSTRequest();

		OBWriteDataDomestic1 data = new OBWriteDataDomestic1();
		data.setConsentId("11112");
		OBDomestic1 initiation = new OBDomestic1();
		initiation.setLocalInstrument("testLocal");
		initiation.setDebtorAccount(null);
		data.setInitiation(initiation);
		request.setData(data);
		initiation.setRemittanceInformation(new OBRemittanceInformation1());
		initiation.setCreditorPostalAddress(new OBPostalAddress6());

		PispConsent pispConsent = new PispConsent();
		pispConsent.setPartyIdentifier("abc");
		Map<String, Object> map = new HashMap<>();
		Object testObject = new Object();
		map.put("For testing", testObject);

		Mockito.when(paymentsProcessingAdapter.preSubmissionProcessing(anyObject(), anyObject(), any()))
				.thenReturn(map);

		Mockito.when(dPaymentComparator.comparePaymentDetails(anyObject(), anyObject(), anyObject())).thenReturn(0);

		CustomDPaymentConsentsPOSTResponse response = new CustomDPaymentConsentsPOSTResponse();
		response.setFraudScore(new Object());
		response.setData(new OBWriteDataDomesticConsentResponse1());
		response.getData().setInitiation(new OBDomestic1());
		response.getData().getInitiation().setDebtorAccount(new OBCashAccountDebtor3());

		Mockito.when(domesticPaymentStagingAdapter.retrieveStagedDomesticPaymentConsents(anyObject(), anyMap()))
				.thenReturn(response);

		PaymentDomesticSubmitPOST201Response paymentsFoundationResponse = new PaymentDomesticSubmitPOST201Response();
		OBWriteDataDomesticResponse1 data1 = new OBWriteDataDomesticResponse1();
		OBMultiAuthorisation1 multiAuthorisation = new OBMultiAuthorisation1();
		multiAuthorisation.setStatus(OBExternalStatus2Code.AUTHORISED);
		data1.setInitiation(new OBDomestic1());
		data1.getInitiation().setRemittanceInformation(new OBRemittanceInformation1());
		data1.getInitiation().setCreditorPostalAddress(new OBPostalAddress6());
		data1.setMultiAuthorisation(multiAuthorisation);
		paymentsFoundationResponse.setData(data1);

		Mockito.when(domesticPaymentsAdapter.processDomesticPayments(anyObject(), anyMap(), anyMap()))
				.thenReturn(paymentsFoundationResponse);
		Mockito.when(domesticPaymentsAdapter.retrieveStagedDomesticPayments(anyObject(), anyMap()))
				.thenReturn(paymentsFoundationResponse);

		PaymentsPlatformResource paymentsplatformresource = new PaymentsPlatformResource();
		paymentsplatformresource.setCreatedAt("30-10-2018");
		paymentsplatformresource.setProccessState("Incomplete");

		Mockito.when(paymentsProcessingAdapter.createInitialPaymentsPlatformResource(anyString(), anyString(), any()))
				.thenReturn(paymentsplatformresource);

		Mockito.when(paymentsProcessingAdapter.postPaymentProcessFlows(anyMap(), anyObject(), any(), anyString(), any(),
				anyString())).thenReturn(new PaymentDomesticSubmitPOST201Response());

		Mockito.when(pispConsentAdapter.retrieveConsentByPaymentId("11112")).thenReturn(pispConsent);
		
		domesticPaymentsServiceImpl.createDomesticPaymentsResource(request);
	}
	
	@SuppressWarnings("unchecked")
	@Test(expected = PSD2Exception.class)
	public void testCreateDomesticPaymentsResourceWithNullFoundationResource() {
		CustomDPaymentsPOSTRequest request = new CustomDPaymentsPOSTRequest();

		OBWriteDataDomestic1 data = new OBWriteDataDomestic1();
		data.setConsentId("11112");
		OBDomestic1 initiation = new OBDomestic1();
		initiation.setLocalInstrument("testLocal");
		OBCashAccountDebtor3 debtorAccount = new OBCashAccountDebtor3();
		debtorAccount.setName("domesticDebtor");
		initiation.setDebtorAccount(debtorAccount);
		data.setInitiation(initiation);
		request.setData(data);

		Map<String, Object> map = new HashMap<>();
		Object testObject = new Object();
		map.put("InComplete", testObject);

		Mockito.when(paymentsProcessingAdapter.preSubmissionProcessing(anyObject(), anyObject(), any()))
				.thenReturn(map);

		Mockito.when(dPaymentComparator.comparePaymentDetails(anyObject(), anyObject(), anyObject())).thenReturn(0);

		CustomDPaymentConsentsPOSTResponse response = new CustomDPaymentConsentsPOSTResponse();
		response.setFraudScore(new Object());

		Mockito.when(domesticPaymentStagingAdapter.retrieveStagedDomesticPaymentConsents(anyObject(), anyMap()))
				.thenReturn(response);

		PaymentsPlatformResource paymentsplatformresource = new PaymentsPlatformResource();
		paymentsplatformresource.setCreatedAt("30-10-2018");
		paymentsplatformresource.setProccessState("Completed");

		Mockito.when(paymentsProcessingAdapter.createInitialPaymentsPlatformResource(anyString(), anyString(), any()))
				.thenReturn(paymentsplatformresource);

		Object result = domesticPaymentsServiceImpl.createDomesticPaymentsResource(request);
		assertNotNull(result);
	}

	@Test
	public void testRetrieveDomesticPaymentsResourceVersion3() {
		Map<String, PaymentsPlatformResource> map = new HashMap<>();
		PaymentsPlatformResource resource = new PaymentsPlatformResource();
		resource.setSubmissionId("131313");
		resource.setPaymentConsentId("3333");
		map.put("submission", resource);

		Mockito.when(paymentsProcessingAdapter.prePaymentProcessGETFlows(anyString(), anyObject())).thenReturn(map);

		PaymentDomesticSubmitPOST201Response response = new PaymentDomesticSubmitPOST201Response();
		OBWriteDataDomesticResponse1 data = new OBWriteDataDomesticResponse1();
		OBMultiAuthorisation1 multiAuthorisation = new OBMultiAuthorisation1();
		multiAuthorisation.setStatus(OBExternalStatus2Code.AUTHORISED);
		data.setMultiAuthorisation(multiAuthorisation);
		response.setData(data);

		Mockito.when(domesticPaymentsAdapter.retrieveStagedDomesticPayments(anyObject(), anyMap()))
				.thenReturn(response);

		PaymentDomesticSubmitPOST201Response dresponse = new PaymentDomesticSubmitPOST201Response();
		OBWriteDataDomesticResponse1 data1 = new OBWriteDataDomesticResponse1();
		data1.setConsentId("3333");
		dresponse.setData(data1);
		String submissionId = "131313";
		Mockito.when(paymentsProcessingAdapter.postPaymentProcessFlows(anyMap(), anyObject(), any(), anyString(), any(),
				anyString())).thenReturn(dresponse);
		PispConsent consent = new PispConsent();
		consent.setPsuId("12345");
		when(pispConsentAdapter.retrieveConsentByPaymentId(anyObject())).thenReturn(consent);
		assertNotNull(domesticPaymentsServiceImpl.retrieveDomesticPaymentsResource(submissionId));
	}

	@Test
	public void testRetrieveDomesticPaymentsResourceVersion1() {
		Map<String, PaymentsPlatformResource> map = new HashMap<>();
		PaymentsPlatformResource resource = new PaymentsPlatformResource();
		resource.setSubmissionId(null);
		resource.setPaymentId("1111");
		resource.setSubmissionCmaVersion(PaymentConstants.CMA_FIRST_VERSION);
		map.put("submission", resource);
		Mockito.when(paymentsProcessingAdapter.prePaymentProcessGETFlows(anyString(), anyObject())).thenReturn(map);

		PaymentDomesticSubmitPOST201Response response = new PaymentDomesticSubmitPOST201Response();
		OBWriteDataDomesticResponse1 data = new OBWriteDataDomesticResponse1();
		OBMultiAuthorisation1 multiAuthorisation = new OBMultiAuthorisation1();
		multiAuthorisation.setStatus(OBExternalStatus2Code.AUTHORISED);
		data.setMultiAuthorisation(multiAuthorisation);
		response.setData(data);

		Mockito.when(domesticPaymentsAdapter.retrieveStagedDomesticPayments(anyObject(), anyMap()))
				.thenReturn(response);
		String submissionId = "111111";
		PaymentDomesticSubmitPOST201Response dresponse = new PaymentDomesticSubmitPOST201Response();
		OBWriteDataDomesticResponse1 data1 = new OBWriteDataDomesticResponse1();
		data1.setDomesticPaymentId("756");
		dresponse.setData(data1);
		
		PispConsent consent = new PispConsent();
		consent.setPsuId("12345");
		when(pispConsentAdapter.retrieveConsentByPaymentId(anyObject())).thenReturn(consent);

		Mockito.when(paymentsProcessingAdapter.postPaymentProcessFlows(anyMap(), anyObject(), any(), anyString(), any(),
				anyString())).thenReturn(dresponse);
		Object result = domesticPaymentsServiceImpl.retrieveDomesticPaymentsResource(submissionId);
		assertNotNull(result);
	}

	@Test(expected = PSD2Exception.class)
	public void testRetrieveDomesticPaymentsResourceWithNullResource() {
		Map<String, PaymentsPlatformResource> map = new HashMap<>();
		PaymentsPlatformResource resource = new PaymentsPlatformResource();
		resource.setPaymentConsentId("3333");

		map.put("submission", resource);
		Mockito.when(paymentsProcessingAdapter.prePaymentProcessGETFlows(anyString(), any())).thenReturn(map);

		Mockito.when(domesticPaymentsAdapter.retrieveStagedDomesticPayments(anyObject(), anyMap())).thenReturn(null);
		String submissionId = "121212";
		domesticPaymentsServiceImpl.retrieveDomesticPaymentsResource(submissionId);

	}

	@After
	public void tearDown() throws Exception {
		paymentsProcessingAdapter = null;
		domesticPaymentsAdapter = null;
		domesticPaymentStagingAdapter = null;
		dPaymentComparator = null;
		domesticPaymentsServiceImpl = null;
	}
}