package com.capgemini.psd2.pisp.domestic.payments.mongo.db.adapter.impl;

import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Conditional;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.conditions.MongoDbMockCondition;
import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.InternalServerErrorMessage;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.adapter.DomesticPaymentsAdapter;
import com.capgemini.psd2.pisp.domain.CustomDPaymentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.DPaymentsFoundationResource;
import com.capgemini.psd2.pisp.domain.OBCharge1;
import com.capgemini.psd2.pisp.domain.OBChargeBearerType1Code;
import com.capgemini.psd2.pisp.domain.OBExternalStatus2Code;
import com.capgemini.psd2.pisp.domain.OBMultiAuthorisation1;
import com.capgemini.psd2.pisp.domain.OBTransactionIndividualStatus1Code;
import com.capgemini.psd2.pisp.domain.PaymentDomesticSubmitPOST201Response;
import com.capgemini.psd2.pisp.domestic.payments.mongo.db.adapter.repository.DomesticPaymentsFoundationRepository;
import com.capgemini.psd2.pisp.enums.ProcessExecutionStatusEnum;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.pisp.status.PaymentConstants;
import com.capgemini.psd2.pisp.utilities.PispUtilities;
import com.capgemini.psd2.utilities.JSONUtilities;
import com.capgemini.psd2.utilities.NullCheckUtils;
import com.capgemini.psd2.utilities.SandboxValidationUtility;

@Conditional(MongoDbMockCondition.class)
@Component("domesticPaymentsMongoDbAdapter")
public class DomesticPaymentsMongoDbAdapterImpl implements DomesticPaymentsAdapter {

	@Autowired
	private DomesticPaymentsFoundationRepository dPaymentSubmissionFoundationRepository;

	@Autowired
	private SandboxValidationUtility utility;

	@Autowired
	private RequestHeaderAttributes reqAttributes;

	@Value("${app.isSandboxEnabled:false}")
	private boolean isSandboxEnabled;

	@Override
	public PaymentDomesticSubmitPOST201Response processDomesticPayments(CustomDPaymentsPOSTRequest customRequest,
			Map<String, OBTransactionIndividualStatus1Code> paymentStatusMap, Map<String, String> params) {

		String paymentRequest = JSONUtilities.getJSONOutPutFromObject(customRequest);
		DPaymentsFoundationResource bankResponse = JSONUtilities.getObjectFromJSONString(
				PispUtilities.getObjectMapper(), paymentRequest, DPaymentsFoundationResource.class);

		String currency = customRequest.getData().getInitiation().getInstructedAmount().getCurrency();
		String initiationAmount = customRequest.getData().getInitiation().getInstructedAmount().getAmount();
		OBChargeBearerType1Code reqChargeBearer = null;

		String submissionId = null;
		String expectedExecutionDateTime = utility.getMockedExpectedExecDtTm();
		String expectedSettlementDateTime = utility.getMockedExpectedSettlementDtTm();
		List<OBCharge1> charges = utility.getMockedOBChargeList(currency, initiationAmount, reqChargeBearer);
		ProcessExecutionStatusEnum processStatusEnum = utility.getMockedProcessExecStatus(currency, initiationAmount);
		OBMultiAuthorisation1 multiAuthorisation = utility.getMockedMultiAuthBlock(currency, initiationAmount);

		if (processStatusEnum == ProcessExecutionStatusEnum.PASS
				|| processStatusEnum == ProcessExecutionStatusEnum.FAIL
				|| processStatusEnum == ProcessExecutionStatusEnum.INCOMPLETE)
			submissionId = UUID.randomUUID().toString();

		bankResponse.getData().setDomesticPaymentId(submissionId);
		bankResponse.getData().setCreationDateTime(customRequest.getCreatedOn());

		// removing charges block as this is not returned for BOI on domestic payment
		// APIs
		if (!isSandboxEnabled) {
			bankResponse.getData().setCharges(charges);
		}

		bankResponse.getData().setExpectedExecutionDateTime(expectedExecutionDateTime);
		bankResponse.getData().setExpectedSettlementDateTime(expectedSettlementDateTime);
		bankResponse.setProcessExecutionStatus(processStatusEnum);

		bankResponse.getData().setMultiAuthorisation(multiAuthorisation);
		if (submissionId != null) {
			OBTransactionIndividualStatus1Code status = calculateCMAStatus(processStatusEnum, multiAuthorisation,
					paymentStatusMap);
			bankResponse.getData().setStatus(status);
			bankResponse.getData().setStatusUpdateDateTime(customRequest.getCreatedOn());
		}
		try {
			dPaymentSubmissionFoundationRepository.save(bankResponse);
		} catch (DataAccessResourceFailureException exception) {
			throw PSD2Exception.populatePSD2Exception(
					new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR, exception.getMessage()));
		}
		return bankResponse;
	}

	@Override
	public PaymentDomesticSubmitPOST201Response retrieveStagedDomesticPayments(
			CustomPaymentStageIdentifiers customPaymentStageIdentifiers, Map<String, String> params) {
		PaymentDomesticSubmitPOST201Response response = null;
		DPaymentsFoundationResource x = dPaymentSubmissionFoundationRepository
				.findOneByDataDomesticPaymentId(customPaymentStageIdentifiers.getPaymentSubmissionId());
		if (!NullCheckUtils.isNullOrEmpty(x)) {
			response = new PaymentDomesticSubmitPOST201Response();
			response.setData(x.getData());
		}
		String initiationAmount = null;
		if (response != null && response.getData() != null && response.getData().getInitiation() != null
				&& response.getData().getInitiation().getInstructedAmount() != null) {
			initiationAmount = response.getData().getInitiation().getInstructedAmount().getAmount();
		}

		if (reqAttributes.getMethodType().equals("GET")
				&& utility.isValidAmount(initiationAmount, PSD2Constants.SANDBOX_PISP_GET_MOCKING)) {

			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
					InternalServerErrorMessage.SANDBOX_MOCKED_ERROR));
		}
		return response;
	}

	private OBTransactionIndividualStatus1Code calculateCMAStatus(ProcessExecutionStatusEnum processExecutionStatusEnum,
			OBMultiAuthorisation1 multiAuthorisation,
			Map<String, OBTransactionIndividualStatus1Code> paymentStatusMap) {

		OBTransactionIndividualStatus1Code cmaStatus = paymentStatusMap.get(PaymentConstants.INITIAL);

		if (processExecutionStatusEnum == ProcessExecutionStatusEnum.FAIL)
			cmaStatus = paymentStatusMap.get(PaymentConstants.FAIL);
		else if (processExecutionStatusEnum == ProcessExecutionStatusEnum.PASS) {
			if (multiAuthorisation == null || multiAuthorisation.getStatus() == OBExternalStatus2Code.AUTHORISED)
				cmaStatus = paymentStatusMap.get(PaymentConstants.PASS_M_AUTH);
			else if (multiAuthorisation.getStatus() == OBExternalStatus2Code.AWAITINGFURTHERAUTHORISATION)
				cmaStatus = paymentStatusMap.get(PaymentConstants.PASS_M_AWAIT);
			else if (multiAuthorisation.getStatus() == OBExternalStatus2Code.REJECTED)
				cmaStatus = paymentStatusMap.get(PaymentConstants.PASS_M_REJECT);
		}
		return cmaStatus;
	}
}