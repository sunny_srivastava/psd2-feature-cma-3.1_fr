package com.capgemini.psd2.pisp.domestic.payments.mongo.db.test.adapter.impl;

import com.capgemini.psd2.pisp.domain.CustomDPaymentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.DPaymentsFoundationResource;
import com.capgemini.psd2.pisp.domain.OBDomestic1;
import com.capgemini.psd2.pisp.domain.OBDomestic1InstructedAmount;
import com.capgemini.psd2.pisp.domain.OBWriteDataDomestic1;
import com.capgemini.psd2.pisp.domain.OBWriteDataDomesticResponse1;

public class DomesticPaymentsMongoDbAdapterImplTestMockData {

	public static CustomDPaymentsPOSTRequest getRequest() {

		CustomDPaymentsPOSTRequest request = new CustomDPaymentsPOSTRequest();
		OBWriteDataDomestic1 data = new OBWriteDataDomestic1();
		OBDomestic1 initiation =  new OBDomestic1();
		OBDomestic1InstructedAmount instructedAmount = new OBDomestic1InstructedAmount();
		instructedAmount.setAmount("1234.65");
		instructedAmount.setCurrency("EUR");
		initiation.setInstructedAmount(instructedAmount);
		data.setInitiation(initiation);
		request.setData(data);
		return request;

	}

	public static DPaymentsFoundationResource getResource() {

		DPaymentsFoundationResource resource = new DPaymentsFoundationResource();
		
		OBWriteDataDomesticResponse1 data = new OBWriteDataDomesticResponse1();
		OBDomestic1 initiation = new OBDomestic1();
		OBDomestic1InstructedAmount instructedAmount = new OBDomestic1InstructedAmount();
		initiation.setInstructedAmount(instructedAmount);
		instructedAmount.setAmount("245.36");
		data.setInitiation(initiation);
		resource.setData(data);

		return resource;

	}
}
