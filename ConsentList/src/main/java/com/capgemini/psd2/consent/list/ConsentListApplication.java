package com.capgemini.psd2.consent.list;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.web.client.RestTemplate;

import com.capgemini.psd2.account.request.routing.adapter.impl.AccountRequestRoutingAdapter;
import com.capgemini.psd2.aisp.adapter.AccountRequestAdapter;
import com.capgemini.psd2.cisp.adapter.FundsConfirmationConsentAdapter;
import com.capgemini.psd2.funds.confirmation.consent.routing.adapter.impl.FundsConfirmationConsentRoutingAdapter;

@EnableEurekaClient
@SpringBootApplication
@ComponentScan(basePackages = { "com.capgemini.psd2" })
@EnableMongoRepositories(basePackages = { "com.capgemini.psd2" })
public class ConsentListApplication {

	static ConfigurableApplicationContext context = null;

	public static void main(String[] args) {
		try {
			context = SpringApplication.run(ConsentListApplication.class, args);
		} catch (Exception e) {
			if (context != null) {
				SpringApplication.exit(context, () -> 1);
			}
			System.exit(1);
		}
	}

	@Bean(name = "AccountRequestAdapter")
	public AccountRequestAdapter accountRequestRoutingAdapter() {
		return new AccountRequestRoutingAdapter();
	}

	@Bean(name = "FundsConfirmationConsentAdapter")
	public FundsConfirmationConsentAdapter fundsConfirmationConsentAdapter() {
		return new FundsConfirmationConsentRoutingAdapter();
	}

	@Bean
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}

}
