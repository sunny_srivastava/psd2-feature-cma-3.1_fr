
package com.capgemini.psd2.consent.list.data;

import java.util.List;

import com.capgemini.psd2.aisp.domain.OBReadConsentResponse1Data.PermissionsEnum;
import com.capgemini.psd2.consent.domain.AispConsent;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Consent extends AispConsent {

	private String consentCreationDate;

	private List<PermissionsEnum> permissions;

	private String paymentId;
	
	private String cispIntentId;
	
	private ConsentType consentType = null;

	@JsonProperty("Status")
	private String statusName;

	public enum ConsentType {
		AISP, PISP, CISP
	}

	public String getCispIntentId() {
		return cispIntentId;
	}

	public void setCispIntentId(String cispIntentId) {
		this.cispIntentId = cispIntentId;
	}

	public ConsentType getConsentType() {
		return consentType;
	}

	public void setConsentType(ConsentType consentType) {
		this.consentType = consentType;
	}

	public List<PermissionsEnum> getPermissions() {
		return permissions;
	}

	public void setPermissions(List<PermissionsEnum> permissions) {
		this.permissions = permissions;
	}

	public String getConsentCreationDate() {
		return consentCreationDate;
	}

	public void setConsentCreationDate(String consentCreationDate) {
		this.consentCreationDate = consentCreationDate;
	}

	public String getPaymentId() {
		return paymentId;
	}

	public void setPaymentId(String paymentId) {
		this.paymentId = paymentId;
	}

	public String getStatusName() {
		return statusName;
	}

	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}

}
