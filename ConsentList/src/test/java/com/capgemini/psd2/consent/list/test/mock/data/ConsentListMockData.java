package com.capgemini.psd2.consent.list.test.mock.data;

import java.util.ArrayList;
import java.util.List;

import com.capgemini.psd2.aisp.domain.Links;
import com.capgemini.psd2.aisp.domain.Meta;
import com.capgemini.psd2.aisp.domain.OBReadConsentResponse1;
import com.capgemini.psd2.aisp.domain.OBReadConsentResponse1Data;
import com.capgemini.psd2.aisp.domain.OBReadConsentResponse1Data.PermissionsEnum;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmationConsentResponse1;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmationConsentResponse1Data;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AispConsent;
import com.capgemini.psd2.consent.domain.CispConsent;
import com.capgemini.psd2.consent.domain.PispConsent;
import com.capgemini.psd2.consent.list.data.Consent;
import com.capgemini.psd2.consent.list.data.ConsentListResponse;
import com.capgemini.psd2.enums.ConsentStatusEnum;
import com.capgemini.psd2.pisp.domain.PaymentConsentsPlatformResource;
import com.capgemini.psd2.pisp.utilities.PispUtilities;

public class ConsentListMockData {

	public static ConsentListResponse getConsentListResponseMockData() {
		ConsentListResponse consentListResponse = new ConsentListResponse();
		List<Consent> consents = new ArrayList<>();
		Consent consent1 = new Consent();
		consent1.setConsentId("19910488");
		consent1.setPsuId("boi123");
		consent1.setTppLegalEntityName("Moneywise-Wealth");
		consent1.setStartDate("2017-07-24T13:45:44.269Z");
		consent1.setEndDate("2017-05-02T00:00:00.875");
		consent1.setConsentCreationDate("2017-07-24T13:45:44.269Z");
		consent1.setStatusName("ACTIVE");

		List<PermissionsEnum> permissions = new ArrayList<>();
		permissions.add(PermissionsEnum.READACCOUNTSBASIC);
		permissions.add(PermissionsEnum.READACCOUNTSDETAIL);
		permissions.add(PermissionsEnum.READBALANCES);		
		consent1.setPermissions(permissions);
		
		List<AccountDetails> accountDetails = new ArrayList<>();
		AccountDetails detail1 = new AccountDetails();
		detail1.setAccountNumber("76528776");
		detail1.setAccountId("55559717-5ae1-48c2-95bb-c27d8be1df9b");
		detail1.setAccountNSC("903779");
		accountDetails.add(detail1);
		AccountDetails detail2 = new AccountDetails();
		detail2.setAccountNumber("76528776");
		detail2.setAccountId("55559717-5ae1-48c2-95bb-c27d8be1df9b");
		detail2.setAccountNSC("903779");
		accountDetails.add(detail2);
		AccountDetails detail3 = new AccountDetails();
		detail3.setAccountNumber("76528776");
		detail3.setAccountId("55559717-5ae1-48c2-95bb-c27d8be1df9b");
		detail3.setAccountNSC("903779");
		accountDetails.add(detail3);
		consent1.setAccountDetails(accountDetails);		
		consents.add(consent1);
		
		consent1 = new Consent();
		consent1.setConsentId("80947088");
		consent1.setPsuId("boi123");
		consent1.setTppLegalEntityName("Moneywise-Wealth");
		consent1.setStartDate("2017-07-24T13:45:44.269Z");
		consent1.setEndDate("2017-05-02T00:00:00.875");
		consent1.setConsentCreationDate("2017-07-24T13:45:44.269Z");
		consent1.setStatusName("ACTIVE");

		permissions = new ArrayList<>();
		permissions.add(PermissionsEnum.READACCOUNTSBASIC);
		permissions.add(PermissionsEnum.READACCOUNTSDETAIL);
		permissions.add(PermissionsEnum.READBALANCES);		
		consent1.setPermissions(permissions);
		
		accountDetails = new ArrayList<>();
		detail1 = new AccountDetails();
		detail1.setAccountNumber("76528776");
		detail1.setAccountId("55559717-5ae1-48c2-95bb-c27d8be1df9b");
		detail1.setAccountNSC("903779");
		accountDetails.add(detail1);
		detail2 = new AccountDetails();
		detail2.setAccountNumber("76528776");
		detail2.setAccountId("55559717-5ae1-48c2-95bb-c27d8be1df9b");
		detail2.setAccountNSC("903779");
		accountDetails.add(detail2);
		detail3 = new AccountDetails();
		detail3.setAccountNumber("76528776");
		detail3.setAccountId("55559717-5ae1-48c2-95bb-c27d8be1df9b");
		detail3.setAccountNSC("903779");
		accountDetails.add(detail3);
		consent1.setAccountDetails(accountDetails);
		
		consents.add(consent1);
		
		consentListResponse.setData(consents);
			consentListResponse.setLinks(new Links());
			consentListResponse.setMeta(new Meta());
		consentListResponse.getLinks().setSelf("https://localhost:8985/consents/boi123?status=ACTIVE");
		consentListResponse.getMeta().setTotalPages(1);
		
		return consentListResponse;
	}
		
	public static List<AispConsent> getConsentsList() {
		List<AispConsent> consents = new ArrayList<>();
		AispConsent consent1 = new AispConsent();
		consent1.setConsentId("19910488");
		consent1.setPsuId("boi123");
		consent1.setStartDate("2017-07-24T13:45:44.269Z");
		consent1.setEndDate("2017-05-02T00:00:00.875");
		consent1.setStatus(ConsentStatusEnum.AWAITINGAUTHORISATION);
		
		List<AccountDetails> accountDetails = new ArrayList<>();
		AccountDetails detail1 = new AccountDetails();
		detail1.setAccountNumber("76528776");
		detail1.setAccountId("55559717-5ae1-48c2-95bb-c27d8be1df9b");
		detail1.setAccountNSC("903779");
		accountDetails.add(detail1);
		AccountDetails detail2 = new AccountDetails();
		detail2.setAccountNumber("76528776");
		detail2.setAccountId("55559717-5ae1-48c2-95bb-c27d8be1df9b");
		detail2.setAccountNSC("903779");
		accountDetails.add(detail2);
		AccountDetails detail3 = new AccountDetails();
		detail3.setAccountNumber("76528776");
		detail3.setAccountId("55559717-5ae1-48c2-95bb-c27d8be1df9b");
		detail3.setAccountNSC("903779");
		accountDetails.add(detail3);
		consent1.setAccountDetails(accountDetails);		
		consents.add(consent1);
		
		consent1 = new AispConsent();
		consent1.setConsentId("80947088");
		consent1.setPsuId("boi123");
		consent1.setStartDate("2017-07-24T13:45:44.269Z");
		consent1.setEndDate("2017-05-02T00:00:00.875");
		consent1.setStatus(ConsentStatusEnum.AWAITINGAUTHORISATION);
		
		accountDetails = new ArrayList<>();
		detail1 = new AccountDetails();
		detail1.setAccountNumber("76528776");
		detail1.setAccountId("55559717-5ae1-48c2-95bb-c27d8be1df9b");
		detail1.setAccountNSC("903779");
		accountDetails.add(detail1);
		detail2 = new AccountDetails();
		detail2.setAccountNumber("76528776");
		detail2.setAccountId("55559717-5ae1-48c2-95bb-c27d8be1df9b");
		detail2.setAccountNSC("903779");
		accountDetails.add(detail2);
		detail3 = new AccountDetails();
		detail3.setAccountNumber("76528776");
		detail3.setAccountId("55559717-5ae1-48c2-95bb-c27d8be1df9b");
		detail3.setAccountNSC("903779");
		accountDetails.add(detail3);
		consent1.setAccountDetails(accountDetails);
		
		consents.add(consent1);
		return consents;
	}
	
	public static List<PispConsent> getPispConsentsList() {
		List<PispConsent> consents = new ArrayList<>();
		PispConsent consent1 = new PispConsent();
		consent1.setConsentId("19910488");
		consent1.setPsuId("boi123");
		consent1.setStartDate("2017-07-24T13:45:44.269Z");
		consent1.setEndDate("2017-07-25T13:45:44.269Z");
		consent1.setStatus(ConsentStatusEnum.AWAITINGAUTHORISATION);
		
		AccountDetails accountDetails = new AccountDetails();
		accountDetails.setAccountNumber("76528776");
		accountDetails.setAccountId("55559717-5ae1-48c2-95bb-c27d8be1df9b");
		accountDetails.setAccountNSC("903779");
		
		consent1.setAccountDetails(accountDetails);	
		consent1.setTppCId("12345415256152");
		consent1.setPaymentId("12345");
		consents.add(consent1);
		
		consent1 = new PispConsent();
		consent1.setConsentId("80947088");
		consent1.setPsuId("boi123");
		consent1.setStartDate("2017-07-24T13:45:44.269Z");
		consent1.setStatus(ConsentStatusEnum.AWAITINGAUTHORISATION);	
		
		accountDetails = new AccountDetails();
		accountDetails.setAccountNumber("76528776");
		accountDetails.setAccountId("55559717-5ae1-48c2-95bb-c27d8be1df9b");
		accountDetails.setAccountNSC("903779");
		
		consent1.setAccountDetails(accountDetails);
		consent1.setTppCId("12345415256152");
		consent1.setPaymentId("12345");
		consents.add(consent1);
		
		
		consents.add(consent1);
		return consents;
	}
	
	public static List<CispConsent> getCispConsentList(){
		List<CispConsent> consents = new ArrayList<>();
		CispConsent consent1 = new CispConsent();
		consent1.setConsentId("19910488");
		consent1.setPsuId("boi123");
		consent1.setStartDate("2017-07-24T13:45:44.269Z");
		consent1.setEndDate("2017-07-25T13:45:44.269Z");
		consent1.setStatus(ConsentStatusEnum.AWAITINGAUTHORISATION);
		
		AccountDetails accountDetails = new AccountDetails();
		accountDetails.setAccountNumber("76528776");
		accountDetails.setAccountId("55559717-5ae1-48c2-95bb-c27d8be1df9b");
		accountDetails.setAccountNSC("903779");
		
		consent1.setAccountDetails(accountDetails);	
		consent1.setTppCId("12345415256152");
		consent1.setFundsIntentId("12345");
		consents.add(consent1);
		
		consent1 = new CispConsent();
		consent1.setConsentId("80947088");
		consent1.setPsuId("boi123");
		consent1.setStartDate("2017-07-24T13:45:44.269Z");
		consent1.setStatus(ConsentStatusEnum.AWAITINGAUTHORISATION);	
		
		accountDetails = new AccountDetails();
		accountDetails.setAccountNumber("76528776");
		accountDetails.setAccountId("55559717-5ae1-48c2-95bb-c27d8be1df9b");
		accountDetails.setAccountNSC("903779");
		
		consent1.setAccountDetails(accountDetails);
		consent1.setTppCId("12345415256152");
		consent1.setFundsIntentId("12345");
		consents.add(consent1);
		return consents;
	}

	public static OBReadConsentResponse1 getAccountRequestData() {

		OBReadConsentResponse1 accountRequestPOSTResponse = new OBReadConsentResponse1();
		List<PermissionsEnum> permissions = new ArrayList<>();
		permissions.add(PermissionsEnum.READACCOUNTSBASIC);
		permissions.add(PermissionsEnum.READACCOUNTSDETAIL);
		permissions.add(PermissionsEnum.READBALANCES);
		OBReadConsentResponse1Data data = new OBReadConsentResponse1Data();
		accountRequestPOSTResponse.setData(data);
		accountRequestPOSTResponse.getData().setPermissions(permissions);
		return accountRequestPOSTResponse;
	}

	public static PaymentConsentsPlatformResource getPaymentSetupPlatformRepository() {
		PaymentConsentsPlatformResource paymentSetupPlatformResource = new PaymentConsentsPlatformResource();
		paymentSetupPlatformResource.setTppLegalEntityName("MoneyWise.com");
		paymentSetupPlatformResource.setTppCID("12345415256152");
		paymentSetupPlatformResource.setCreatedAt(PispUtilities.getCurrentDateInISOFormat());
		
		return paymentSetupPlatformResource;
	}
	
	public static OBFundsConfirmationConsentResponse1 getFundsCOnfirmationCOnsentResponse() {
		OBFundsConfirmationConsentResponse1 oBFundsConfirmationConsentResponse1=new OBFundsConfirmationConsentResponse1();
		OBFundsConfirmationConsentResponse1Data oBFundsConfirmationConsentDataResponse1=new OBFundsConfirmationConsentResponse1Data();
		oBFundsConfirmationConsentDataResponse1.setConsentId("1233");
		oBFundsConfirmationConsentDataResponse1.setCreationDateTime("date");
		oBFundsConfirmationConsentDataResponse1.setTppLegalEntityName("entityName");
		
		oBFundsConfirmationConsentResponse1.setData(oBFundsConfirmationConsentDataResponse1);
		return oBFundsConfirmationConsentResponse1;
	}

}
