package com.capgemini.psd2.consent.list.test.data;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.aisp.domain.Links;
import com.capgemini.psd2.aisp.domain.Meta;
import com.capgemini.psd2.aisp.domain.OBReadConsentResponse1Data;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.list.data.Consent;
import com.capgemini.psd2.consent.list.data.ConsentListResponse;

@RunWith(SpringJUnit4ClassRunner.class)
public class ConsentListResponseTest {

	@Test
	public void test() {
		ConsentListResponse response = new ConsentListResponse();
		List<Consent> data = new ArrayList<>();
		Consent consent = new Consent();

		consent.setConsentId("19910488");
		consent.setPsuId("boi123");
		consent.setTppLegalEntityName("Moneywise-Wealth");
		consent.setStartDate("2017-07-24T13:45:44.269Z");
		consent.setEndDate("2017-05-02T00:00:00.875");
		consent.setConsentCreationDate("2017-07-24T13:45:44.269Z");
		consent.setStatusName("AWAITINGAUTHENTICATION");

		List<OBReadConsentResponse1Data.PermissionsEnum> permissions = new ArrayList<>();
		permissions.add(OBReadConsentResponse1Data.PermissionsEnum.READACCOUNTSBASIC);
		permissions.add(OBReadConsentResponse1Data.PermissionsEnum.READACCOUNTSDETAIL);
		permissions.add(OBReadConsentResponse1Data.PermissionsEnum.READBALANCES);
		consent.setPermissions(permissions);

		List<AccountDetails> accountDetails = new ArrayList<>();
		AccountDetails detail1 = new AccountDetails();
		detail1.setAccountNumber("76528776");
		detail1.setAccountId("55559717-5ae1-48c2-95bb-c27d8be1df9b");
		detail1.setAccountNSC("903779");
		accountDetails.add(detail1);
		AccountDetails detail2 = new AccountDetails();
		detail2.setAccountNumber("76528776");
		detail2.setAccountId("55559717-5ae1-48c2-95bb-c27d8be1df9b");
		detail2.setAccountNSC("903779");
		accountDetails.add(detail2);
		AccountDetails detail3 = new AccountDetails();
		detail3.setAccountNumber("76528776");
		detail3.setAccountId("55559717-5ae1-48c2-95bb-c27d8be1df9b");
		detail3.setAccountNSC("903779");
		accountDetails.add(detail3);
		consent.setAccountDetails(accountDetails);

		consent.setTppCId("Moneywise-Wealth");

		consent.setTransactionFromDateTime("2015-05-02T00:00:00.875");

		consent.setTransactionToDateTime("2019-05-02T00:00:00.875");

		consent.setAccountRequestId("55559717-5ae1-48c2-95bb-c27d8be1df9d");

		data.add(consent);
		response.setData(data);
		assertTrue(response.getData().equals(data));

		Links links = new Links();
		response.setLinks(links);
		assertTrue(response.getLinks().equals(links));

		Meta meta = new Meta();
		response.setMeta(meta);
		assertTrue(response.getMeta().equals(meta));

		response.data(data);
		response.links(links);
		response.meta(meta);
		
		response.hashCode();
		response.equals(response);
		response.toString();
		response.setData(null);
		response.toString();
		response.equals(null);
		response.equals(consent);
		
	}
	
}
