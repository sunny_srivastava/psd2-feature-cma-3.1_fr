(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".main-container {\n  -ms-flex: 1 0 auto;\n      flex: 1 0 auto;\n  padding: 0;\n  margin: 0;\n  width: 100%;\n}\n\n@media screen and (max-width: 767px) {\n  .header-left,\n  .footer-left,\n  .header-right,\n  .footer-right {\n    text-align: center;\n  }\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYXBwLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxrQkFBYztNQUFkLGNBQWM7RUFDZCxVQUFVO0VBQ1YsU0FBUztFQUNULFdBQVc7QUFDYjs7QUFFQTtFQUNFOzs7O0lBSUUsa0JBQWtCO0VBQ3BCO0FBQ0YiLCJmaWxlIjoic3JjL2FwcC9hcHAuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5tYWluLWNvbnRhaW5lciB7XG4gIGZsZXg6IDEgMCBhdXRvO1xuICBwYWRkaW5nOiAwO1xuICBtYXJnaW46IDA7XG4gIHdpZHRoOiAxMDAlO1xufVxuXG5AbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA3NjdweCkge1xuICAuaGVhZGVyLWxlZnQsXG4gIC5mb290ZXItbGVmdCxcbiAgLmhlYWRlci1yaWdodCxcbiAgLmZvb3Rlci1yaWdodCB7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICB9XG59XG4iXX0= */"

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"main-container\">\n  <app-header></app-header>\n<app-login></app-login>\n<app-loading-spinner *ngIf='showSpinner'></app-loading-spinner>\n<app-modal *ngIf=\"enableModal\" [redirectUri]=\"redirectUri\" (closeModal)=\"handleCloseModal($event)\"></app-modal>\n<app-aisp-consent-details *ngIf=\"consentType==='AISP' && skipConsentFlow===true\"></app-aisp-consent-details>\n<app-pisp-consent-details *ngIf=\"consentType==='PISP' && skipConsentFlow===true\"></app-pisp-consent-details>\n</div>\n\n<app-footer></app-footer>"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_static_content_fetch_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./services/static-content-fetch.service */ "./src/app/services/static-content-fetch.service.ts");
/* harmony import */ var _services_error_handler_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./services/error-handler.service */ "./src/app/services/error-handler.service.ts");
/* harmony import */ var _services_shared_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./services/shared.service */ "./src/app/services/shared.service.ts");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _services_consent_detail_service_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./services/consent-detail-service.service */ "./src/app/services/consent-detail-service.service.ts");







var AppComponent = /** @class */ (function () {
    function AppComponent(fetchService, errorService, modalService, title, consentService) {
        var _this = this;
        this.fetchService = fetchService;
        this.errorService = errorService;
        this.modalService = modalService;
        this.title = title;
        this.consentService = consentService;
        this.fetchService.setCdnBaseUrl(document.querySelector('#cdnBaseURL').value);
        this.fetchService.setSoftwareOnBehalfOfOrg((document.querySelector('#softwareOnBehalfOfOrg').value === "null" || document.querySelector('#softwareOnBehalfOfOrg').value == null
            || document.querySelector('#softwareOnBehalfOfOrg').value.length <= 0) ? 'none' : document.querySelector('#softwareOnBehalfOfOrg').value);
        this.fetchService.setbuildversion(document.querySelector('#buildversion').value);
        this.consentService.channelId = document.querySelector('#channelId').value;
        this.consentService.payeeDetails = document.querySelector('#paymentSetup').value ? JSON.parse(document.querySelector('#paymentSetup').value) : null;
        this.consentService.psuAccounts = document.querySelector('#psuAccounts').value ? JSON.parse(document.querySelector('#psuAccounts').value) : null;
        this.consentService.accRequest = document.querySelector('#account-request').value ? JSON.parse(document.querySelector('#account-request').value) : null;
        this.consentService.consentType = document.querySelector('#consentType').value ? document.querySelector('#consentType').value : null;
        this.consentService.tppInfo = document.querySelector('#tppInfo').value ? JSON.parse(document.querySelector('#tppInfo').value) : null;
        this.consentType = this.consentService.consentType;
        this.skipConsentFlow = document.querySelector('#skipConsentFlow').value;
        this.skipConsentFlow = (this.skipConsentFlow == "true") && ((this.consentType === "AISP" && this.consentService.accRequest)
            || (this.consentType === "PISP" && this.consentService.payeeDetails)) ? true : false;
        this.staticContent = document.querySelector("#staticContent");
        if (document.querySelector("#error") && document.querySelector("#error").value) {
            this.error = document.querySelector("#error").value;
        }
        var secError = null;
        if (document.querySelector("#sec-error") && document.querySelector("#sec-error").value) {
            secError = document.querySelector("#sec-error").value;
        }
        this.error = this.error ? this.error : (secError ? secError : "");
        if (this.staticContent.value) {
            this.staticContent = this.staticContent.value;
            this.staticContent = this.staticContent ? JSON.parse(this.staticContent) : null;
            this.title.setTitle(this.staticContent['APPLICATION_TITLE']);
            if (this.staticContent["ERROR_MESSAGES"]["SHOW_SITE_MAINTAINENCE_ALERT"] === "true")
                this.staticContent["ERROR_MESSAGES"]["SHOW_SITE_MAINTAINENCE_ALERT"] = true;
            else
                this.staticContent["ERROR_MESSAGES"]["SHOW_SITE_MAINTAINENCE_ALERT"] = false;
            if (this.staticContent["ERROR_MESSAGES"]["SHOW_NOTICE"] === "true")
                this.staticContent["ERROR_MESSAGES"]["SHOW_NOTICE"] = true;
            else
                this.staticContent["ERROR_MESSAGES"]["SHOW_NOTICE"] = false;
            this.fetchService.setStaticContent(this.staticContent);
            this.errorService.setErrors(this.staticContent["ERROR_MESSAGES"]);
            if (this.staticContent["ERROR_MESSAGES"]["SHOW_SITE_MAINTAINENCE_ALERT"]) {
                this.errorService.errorArr.push({
                    type: this.staticContent["ERROR_MESSAGES"]["ALERT"],
                    errorMessage: this.staticContent["ERROR_MESSAGES"]["ALERT_MESSAGE"],
                    errorDetails: this.staticContent["ERROR_MESSAGES"]["ALERT_MESSAGE_DETAILS"]
                });
            }
            if (this.staticContent["ERROR_MESSAGES"]["SHOW_NOTICE"]) {
                this.errorService.notice = {
                    type: this.staticContent["ERROR_MESSAGES"]["NOTICE"],
                    errorMessage: this.staticContent["ERROR_MESSAGES"]["NOTICE_MESSAGE"],
                    errorDetails: this.staticContent["ERROR_MESSAGES"]["NOTICE_MESSAGE_DETAILS"]
                };
            }
        }
        if (this.error) {
            this.error = JSON.parse(this.error);
            if (this.error.errorCode === "731") {
                this.enableModal = true;
                this.redirectUri = document.querySelector('#resumePath').value;
                // this.errorService.setError(null);
            }
            else
                this.errorService.setError(this.error);
        }
        this.modalService.showLoadingSpinner$.subscribe(function (showSpinner) {
            _this.showSpinner = showSpinner;
        });
        this.modalService.enableModalPopup$.subscribe(function (data) {
            _this.enableModal = data.enableModal;
            _this.redirectUri = data.redirectUri;
        });
    }
    AppComponent.prototype.handleCloseModal = function (enable) {
        this.enableModal = enable;
    };
    AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_static_content_fetch_service__WEBPACK_IMPORTED_MODULE_2__["StaticContentFetchService"],
            _services_error_handler_service__WEBPACK_IMPORTED_MODULE_3__["ErrorHandlerService"],
            _services_shared_service__WEBPACK_IMPORTED_MODULE_4__["SharedService"],
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_5__["Title"],
            _services_consent_detail_service_service__WEBPACK_IMPORTED_MODULE_6__["ConsentDetailServiceService"]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _components_login_login_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./components/login/login.component */ "./src/app/components/login/login.component.ts");
/* harmony import */ var _components_header_header_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./components/header/header.component */ "./src/app/components/header/header.component.ts");
/* harmony import */ var _components_footer_footer_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./components/footer/footer.component */ "./src/app/components/footer/footer.component.ts");
/* harmony import */ var _components_username_username_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./components/username/username.component */ "./src/app/components/username/username.component.ts");
/* harmony import */ var _services_login_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./services/login.service */ "./src/app/services/login.service.ts");
/* harmony import */ var _services_static_content_fetch_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./services/static-content-fetch.service */ "./src/app/services/static-content-fetch.service.ts");
/* harmony import */ var _services_error_handler_service__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./services/error-handler.service */ "./src/app/services/error-handler.service.ts");
/* harmony import */ var _components_modal_modal_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./components/modal/modal.component */ "./src/app/components/modal/modal.component.ts");
/* harmony import */ var _components_loading_spinner_loading_spinner_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./components/loading-spinner/loading-spinner.component */ "./src/app/components/loading-spinner/loading-spinner.component.ts");
/* harmony import */ var _components_error_error_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./components/error/error.component */ "./src/app/components/error/error.component.ts");
/* harmony import */ var _components_pisp_consent_details_pisp_consent_details_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./components/pisp-consent-details/pisp-consent-details.component */ "./src/app/components/pisp-consent-details/pisp-consent-details.component.ts");
/* harmony import */ var _components_aisp_consent_details_aisp_consent_details_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./components/aisp-consent-details/aisp-consent-details.component */ "./src/app/components/aisp-consent-details/aisp-consent-details.component.ts");
/* harmony import */ var _services_consent_detail_service_service__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./services/consent-detail-service.service */ "./src/app/services/consent-detail-service.service.ts");
/* harmony import */ var _components_permissions_permissions_component__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./components/permissions/permissions.component */ "./src/app/components/permissions/permissions.component.ts");
/* harmony import */ var src_app_pipes_account_digits_filter_pipe__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! src/app/pipes/account-digits-filter.pipe */ "./src/app/pipes/account-digits-filter.pipe.ts");





//component imports





//services imports










//pipes import 

var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_5__["AppComponent"],
                _components_login_login_component__WEBPACK_IMPORTED_MODULE_6__["LoginComponent"],
                _components_header_header_component__WEBPACK_IMPORTED_MODULE_7__["HeaderComponent"],
                _components_footer_footer_component__WEBPACK_IMPORTED_MODULE_8__["FooterComponent"],
                _components_username_username_component__WEBPACK_IMPORTED_MODULE_9__["UsernameComponent"],
                _components_modal_modal_component__WEBPACK_IMPORTED_MODULE_13__["ModalComponent"],
                _components_loading_spinner_loading_spinner_component__WEBPACK_IMPORTED_MODULE_14__["LoadingSpinnerComponent"],
                _components_error_error_component__WEBPACK_IMPORTED_MODULE_15__["ErrorComponent"],
                _components_pisp_consent_details_pisp_consent_details_component__WEBPACK_IMPORTED_MODULE_16__["PispConsentDetailsComponent"],
                _components_aisp_consent_details_aisp_consent_details_component__WEBPACK_IMPORTED_MODULE_17__["AispConsentDetailsComponent"],
                _components_permissions_permissions_component__WEBPACK_IMPORTED_MODULE_19__["PermissionsComponent"],
                src_app_pipes_account_digits_filter_pipe__WEBPACK_IMPORTED_MODULE_20__["AccountDigitsFilterPipe"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"], _angular_common_http__WEBPACK_IMPORTED_MODULE_4__["HttpClientModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["ReactiveFormsModule"]
            ],
            providers: [_services_static_content_fetch_service__WEBPACK_IMPORTED_MODULE_11__["StaticContentFetchService"], _services_login_service__WEBPACK_IMPORTED_MODULE_10__["LoginService"], _services_error_handler_service__WEBPACK_IMPORTED_MODULE_12__["ErrorHandlerService"], _services_consent_detail_service_service__WEBPACK_IMPORTED_MODULE_18__["ConsentDetailServiceService"]],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_5__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/components/aisp-consent-details/aisp-consent-details.component.css":
/*!************************************************************************************!*\
  !*** ./src/app/components/aisp-consent-details/aisp-consent-details.component.css ***!
  \************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "thead,th{\n    background-color: #5d78ff;\n    border:1px solid black!important;\n}\n.table{\n    padding:15px;\n    display:inline-table;\n    margin: 15px;\n    width: 100%; \n}\n.row{\n    -ms-flex-line-pack:center;\n        align-content:center;\n}\ntr,td{\n    border:1px solid black;\n}\n.header-bar-primary{\n    background: #5d78ff;\n    border-radius: 4px;\n    color: #fff;\n    font-size: 18px;\n    letter-spacing: 0;\n    line-height: 1.5;\n    padding: 18px 20px;\n    word-wrap: break-word;\n}\n.table-header-container{\n    background-color:#fff;\n    width:100%;\n    margin:15px;\n}\n.access-date-label{\n    margin-bottom:10px;\n    padding-left:15px;\n}\n.container{\n    font-family:sans-serif;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9haXNwLWNvbnNlbnQtZGV0YWlscy9haXNwLWNvbnNlbnQtZGV0YWlscy5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0kseUJBQXlCO0lBQ3pCLGdDQUFnQztBQUNwQztBQUNBO0lBQ0ksWUFBWTtJQUNaLG9CQUFvQjtJQUNwQixZQUFZO0lBQ1osV0FBVztBQUNmO0FBQ0E7SUFDSSx5QkFBb0I7UUFBcEIsb0JBQW9CO0FBQ3hCO0FBQ0E7SUFDSSxzQkFBc0I7QUFDMUI7QUFDQTtJQUNJLG1CQUFtQjtJQUNuQixrQkFBa0I7SUFDbEIsV0FBVztJQUNYLGVBQWU7SUFDZixpQkFBaUI7SUFDakIsZ0JBQWdCO0lBQ2hCLGtCQUFrQjtJQUNsQixxQkFBcUI7QUFDekI7QUFDQTtJQUNJLHFCQUFxQjtJQUNyQixVQUFVO0lBQ1YsV0FBVztBQUNmO0FBQ0E7SUFDSSxrQkFBa0I7SUFDbEIsaUJBQWlCO0FBQ3JCO0FBQ0E7SUFDSSxzQkFBc0I7QUFDMUIiLCJmaWxlIjoic3JjL2FwcC9jb21wb25lbnRzL2Fpc3AtY29uc2VudC1kZXRhaWxzL2Fpc3AtY29uc2VudC1kZXRhaWxzLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyJ0aGVhZCx0aHtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjNWQ3OGZmO1xuICAgIGJvcmRlcjoxcHggc29saWQgYmxhY2shaW1wb3J0YW50O1xufVxuLnRhYmxle1xuICAgIHBhZGRpbmc6MTVweDtcbiAgICBkaXNwbGF5OmlubGluZS10YWJsZTtcbiAgICBtYXJnaW46IDE1cHg7XG4gICAgd2lkdGg6IDEwMCU7IFxufVxuLnJvd3tcbiAgICBhbGlnbi1jb250ZW50OmNlbnRlcjtcbn1cbnRyLHRke1xuICAgIGJvcmRlcjoxcHggc29saWQgYmxhY2s7XG59XG4uaGVhZGVyLWJhci1wcmltYXJ5e1xuICAgIGJhY2tncm91bmQ6ICM1ZDc4ZmY7XG4gICAgYm9yZGVyLXJhZGl1czogNHB4O1xuICAgIGNvbG9yOiAjZmZmO1xuICAgIGZvbnQtc2l6ZTogMThweDtcbiAgICBsZXR0ZXItc3BhY2luZzogMDtcbiAgICBsaW5lLWhlaWdodDogMS41O1xuICAgIHBhZGRpbmc6IDE4cHggMjBweDtcbiAgICB3b3JkLXdyYXA6IGJyZWFrLXdvcmQ7XG59XG4udGFibGUtaGVhZGVyLWNvbnRhaW5lcntcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiNmZmY7XG4gICAgd2lkdGg6MTAwJTtcbiAgICBtYXJnaW46MTVweDtcbn1cbi5hY2Nlc3MtZGF0ZS1sYWJlbHtcbiAgICBtYXJnaW4tYm90dG9tOjEwcHg7XG4gICAgcGFkZGluZy1sZWZ0OjE1cHg7XG59XG4uY29udGFpbmVye1xuICAgIGZvbnQtZmFtaWx5OnNhbnMtc2VyaWY7XG59Il19 */"

/***/ }),

/***/ "./src/app/components/aisp-consent-details/aisp-consent-details.component.html":
/*!*************************************************************************************!*\
  !*** ./src/app/components/aisp-consent-details/aisp-consent-details.component.html ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"container\">\n<div class=\"row\">\n  <table class=\"table\">\n      <thead>\n      <tr><th>Account nickname</th><th>IBAN</th><th>Currency</th><th> Account type</th></tr>    \n      </thead>\n      <tbody>\n          <tr *ngFor=\"let account of psuAccounts\">\n              <td>{{ account.Nickname }}</td>\n              <td>{{ account.Account[0].Identification | accountDigitsFilter}}</td>\n              <td>{{ account.Currency }}</td>\n              <td>{{ account.AccountSubType }}</td>\n          </tr>\n          </tbody>\n  </table>\n\n  <div class=\"table-header-container\">\n        <div class=\"header-bar-primary\">Requested transaction access</div>\n        <div class=\"row\">\n          <div class=\"col-md-6 col-sm-6\">\n              <div class=\"access-date-label\">\n                  <span class=\"row-label\"> From: </span>\n                  <span class=\"row-value\" [innerHTML]=\"fromDate? (fromDate | date: 'dd MMMM yyyy') : '-' \"></span>\n              </div> \n          </div>\n          <div class=\"col-md-6 col-sm-6\">\n              <div class=\"access-date-label\">\n                  <span class=\"row-label\"> To: </span>\n                  <span class=\"row-value\" [innerHTML]=\"tillDate? (tillDate | date: 'dd MMMM yyyy') : '-' \">\n                      </span>\n              </div> \n          </div>\n        </div>\n </div> \n \n  <!--consent validity STARTS-->\n  <div class=\"table table-header-container\">\n          <div class=\"box-container\">\n              <span class=\"row-value\" [innerHTML]=\"expiryDate ? ('The consent is valid until '+(expiryDate | date: 'dd MMMM yyyy')+' You can revoke access to this service through the third party provider or through 365 online / Business On Line. For security purposes we will ask you to sign in again every 90 days.') : '-'\"></span>\n          </div>\n  </div>\n  <!--consent validity ENDS-->\n  <div class=\"col-md-12\"> \n      <h3>Permission List</h3>\n      <app-permissions></app-permissions>\n  </div>\n\n</div>\n</div>"

/***/ }),

/***/ "./src/app/components/aisp-consent-details/aisp-consent-details.component.ts":
/*!***********************************************************************************!*\
  !*** ./src/app/components/aisp-consent-details/aisp-consent-details.component.ts ***!
  \***********************************************************************************/
/*! exports provided: AispConsentDetailsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AispConsentDetailsComponent", function() { return AispConsentDetailsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_consent_detail_service_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/consent-detail-service.service */ "./src/app/services/consent-detail-service.service.ts");



var AispConsentDetailsComponent = /** @class */ (function () {
    function AispConsentDetailsComponent(consentService) {
        this.consentService = consentService;
    }
    AispConsentDetailsComponent.prototype.ngOnInit = function () {
        this.psuAccounts = this.consentService.psuAccounts.Data.Account;
        this.accountRequest = this.consentService.accRequest.Data;
        this.tillDate = (this.accountRequest.TransactionToDateTime).split('T')[0];
        this.fromDate = (this.accountRequest.TransactionFromDateTime).split('T')[0];
        this.expiryDate = (this.accountRequest.ExpirationDateTime).split('T')[0];
        this.permissionList = this.accountRequest.Permissions;
    };
    AispConsentDetailsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-aisp-consent-details',
            template: __webpack_require__(/*! ./aisp-consent-details.component.html */ "./src/app/components/aisp-consent-details/aisp-consent-details.component.html"),
            styles: [__webpack_require__(/*! ./aisp-consent-details.component.css */ "./src/app/components/aisp-consent-details/aisp-consent-details.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_consent_detail_service_service__WEBPACK_IMPORTED_MODULE_2__["ConsentDetailServiceService"]])
    ], AispConsentDetailsComponent);
    return AispConsentDetailsComponent;
}());



/***/ }),

/***/ "./src/app/components/error/error.component.css":
/*!******************************************************!*\
  !*** ./src/app/components/error/error.component.css ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvZXJyb3IvZXJyb3IuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./src/app/components/error/error.component.html":
/*!*******************************************************!*\
  !*** ./src/app/components/error/error.component.html ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div [class]=\"noticeCSSClass\" *ngIf=\"!this.errorService.errors['SHOW_NOTICE']\">\n    <span>\n        <span class=\"label-error-primary\"><em class=\"logo fa fa-custom-size fa-exclamation-circle\" alt=\"error logo\"></em> &nbsp;&nbsp;{{this.display.type}}</span> : {{this.display.errorMessage}}</span><br>\n        <span *ngIf='this.display.errorDetails != \"\"' [innerHTML]=\"this.display.errorDetails\"></span>\n    \n    </div>\n    \n    <div [class] =\"noticeCSSClass\" *ngIf=\"this.errorService.errors['SHOW_NOTICE']\">\n        <span>\n            <em class=\"notice-logo fa fa-3x fa-exclamation-circle\" alt=\"maintenance logo\"></em>\n            </span><br>\n        <span class=\"label-primary\">{{this.display.errorMessage}}</span><br>\n        <span *ngIf='this.display.errorDetails != \"\"' class=\"label-secondary\" [innerHTML]=\"this.display.errorDetails\"></span>\n    </div>"

/***/ }),

/***/ "./src/app/components/error/error.component.ts":
/*!*****************************************************!*\
  !*** ./src/app/components/error/error.component.ts ***!
  \*****************************************************/
/*! exports provided: ErrorComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ErrorComponent", function() { return ErrorComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_error_handler_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/error-handler.service */ "./src/app/services/error-handler.service.ts");



var ErrorComponent = /** @class */ (function () {
    function ErrorComponent(errorService) {
        this.errorService = errorService;
        this.display = { type: "", errorMessage: "", errorDetails: "" };
    }
    ErrorComponent.prototype.ngOnInit = function () {
        this.display.type = this.notice.type;
        this.display.errorMessage = this.notice.errorMessage;
        this.display.errorDetails = this.notice.errorDetails;
        if (this.notice.type == "Alert")
            this.noticeCSSClass = "alert alert-secondary label-secondary";
        else if (this.notice.type == "Notice") {
            this.noticeCSSClass = "maintenance-notice";
        }
        else {
            this.noticeCSSClass = "alert alert-danger";
        }
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], ErrorComponent.prototype, "notice", void 0);
    ErrorComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-error',
            template: __webpack_require__(/*! ./error.component.html */ "./src/app/components/error/error.component.html"),
            styles: [__webpack_require__(/*! ./error.component.css */ "./src/app/components/error/error.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_error_handler_service__WEBPACK_IMPORTED_MODULE_2__["ErrorHandlerService"]])
    ], ErrorComponent);
    return ErrorComponent;
}());



/***/ }),

/***/ "./src/app/components/footer/footer.component.css":
/*!********************************************************!*\
  !*** ./src/app/components/footer/footer.component.css ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".footer {\n    width: 100%;\n    /* background-color: #e8e8e8; */\n    -ms-flex-negative: 0;\n        flex-shrink: 0;\n    padding: 1%;\n    font-size: small;\n  }\n  \n  .footer-left {\n    text-align: left;\n    /* color: #3d4465;  */\n  }\n  \n  .footer-right {\n    text-align: right;\n    padding-top: 1%;\n  }\n  \n  .footer-right .quick-links {\n    /* margin-top: 10%; */\n    padding: 0;\n    list-style-type: none;\n  }\n  \n  .footer-right .quick-links li {\n    display: inline;\n    padding: 0 2%;\n    }\n  \n  .footer-right .quick-links .link-border{\n    border-right: 1px solid;\n    /* border-right-color: black; */\n  }\n  \n  /* .footer-right .quick-links li a {\n    color: #3d4465;\n  } */\n  \n  @media screen and (max-width: 767px) {\n    .footer-left,\n    .footer-right {\n      text-align: center;\n    }\n  \n  }\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9mb290ZXIvZm9vdGVyLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSxXQUFXO0lBQ1gsK0JBQStCO0lBQy9CLG9CQUFjO1FBQWQsY0FBYztJQUNkLFdBQVc7SUFDWCxnQkFBZ0I7RUFDbEI7O0VBRUE7SUFDRSxnQkFBZ0I7SUFDaEIscUJBQXFCO0VBQ3ZCOztFQUVBO0lBQ0UsaUJBQWlCO0lBQ2pCLGVBQWU7RUFDakI7O0VBRUE7SUFDRSxxQkFBcUI7SUFDckIsVUFBVTtJQUNWLHFCQUFxQjtFQUN2Qjs7RUFFQTtJQUNFLGVBQWU7SUFDZixhQUFhO0lBQ2I7O0VBRUY7SUFDRSx1QkFBdUI7SUFDdkIsK0JBQStCO0VBQ2pDOztFQUVBOztLQUVHOztFQUVIO0lBQ0U7O01BRUUsa0JBQWtCO0lBQ3BCOztFQUVGIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50cy9mb290ZXIvZm9vdGVyLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuZm9vdGVyIHtcbiAgICB3aWR0aDogMTAwJTtcbiAgICAvKiBiYWNrZ3JvdW5kLWNvbG9yOiAjZThlOGU4OyAqL1xuICAgIGZsZXgtc2hyaW5rOiAwO1xuICAgIHBhZGRpbmc6IDElO1xuICAgIGZvbnQtc2l6ZTogc21hbGw7XG4gIH1cbiAgXG4gIC5mb290ZXItbGVmdCB7XG4gICAgdGV4dC1hbGlnbjogbGVmdDtcbiAgICAvKiBjb2xvcjogIzNkNDQ2NTsgICovXG4gIH1cbiAgXG4gIC5mb290ZXItcmlnaHQge1xuICAgIHRleHQtYWxpZ246IHJpZ2h0O1xuICAgIHBhZGRpbmctdG9wOiAxJTtcbiAgfVxuICBcbiAgLmZvb3Rlci1yaWdodCAucXVpY2stbGlua3Mge1xuICAgIC8qIG1hcmdpbi10b3A6IDEwJTsgKi9cbiAgICBwYWRkaW5nOiAwO1xuICAgIGxpc3Qtc3R5bGUtdHlwZTogbm9uZTtcbiAgfVxuICBcbiAgLmZvb3Rlci1yaWdodCAucXVpY2stbGlua3MgbGkge1xuICAgIGRpc3BsYXk6IGlubGluZTtcbiAgICBwYWRkaW5nOiAwIDIlO1xuICAgIH1cblxuICAuZm9vdGVyLXJpZ2h0IC5xdWljay1saW5rcyAubGluay1ib3JkZXJ7XG4gICAgYm9yZGVyLXJpZ2h0OiAxcHggc29saWQ7XG4gICAgLyogYm9yZGVyLXJpZ2h0LWNvbG9yOiBibGFjazsgKi9cbiAgfVxuICBcbiAgLyogLmZvb3Rlci1yaWdodCAucXVpY2stbGlua3MgbGkgYSB7XG4gICAgY29sb3I6ICMzZDQ0NjU7XG4gIH0gKi9cblxuICBAbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA3NjdweCkge1xuICAgIC5mb290ZXItbGVmdCxcbiAgICAuZm9vdGVyLXJpZ2h0IHtcbiAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICB9XG4gIFxuICB9Il19 */"

/***/ }),

/***/ "./src/app/components/footer/footer.component.html":
/*!*********************************************************!*\
  !*** ./src/app/components/footer/footer.component.html ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<footer class=\"footer footer-primary\">\n  <div class=\"container\">\n    <div class=\"row\">\n      <div class=\"footer-right col-12 col-md-5 offset-md-7\">\n        <ul class=\"quick-links\">\n          <li class='link-border'><a [href]=\"this.fetchService.getStaticContent('FOOTER','ABOUT_US_URL')\">{{this.fetchService.getStaticContent('FOOTER','ABOUT_US_LABEL')}}<span class=\"sr-only\" aria-label=\" Link will open in the new window\"></span></a></li>\n          <li class='link-border'><a [href]=\"this.fetchService.getStaticContent('FOOTER','COOKIES_AND_PRIVACY_POLICY_URL')\">{{this.fetchService.getStaticContent('FOOTER','COOKIES_AND_PRIVACY_POLICY_LABEL')}}<span class=\"sr-only\" aria-label=\" Link will open in the new window\"></span></a></li>\n          <li class='link-border'><a [href]=\"this.fetchService.getStaticContent('FOOTER','TERMS_AND_CONDITIONS_URL')\">{{this.fetchService.getStaticContent('FOOTER','TERMS_AND_CONDITIONS_LABEL')}}<span class=\"sr-only\" aria-label=\" Link will open in the new window\"></span></a></li>\n          <li><a [href]=\"this.fetchService.getStaticContent('FOOTER','HELP_URL')\">{{this.fetchService.getStaticContent('FOOTER','HELP_LABEL')}}<span class=\"sr-only\" aria-label=\" Link will open in the new window\"></span></a></li>\n        </ul>\n      </div>\n    </div>\n    <div class=\"row\">\n      <div class=\"footer-left col-12 custom-margin\">\n        <span class=\"label-secondary\" [innerHTML]=\"this.fetchService.getStaticContent('FOOTER','REGULATORY_TEXT')\">\n        </span>\n      </div>\n    </div>\n  </div>\n</footer>"

/***/ }),

/***/ "./src/app/components/footer/footer.component.ts":
/*!*******************************************************!*\
  !*** ./src/app/components/footer/footer.component.ts ***!
  \*******************************************************/
/*! exports provided: FooterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FooterComponent", function() { return FooterComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_static_content_fetch_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/static-content-fetch.service */ "./src/app/services/static-content-fetch.service.ts");



var FooterComponent = /** @class */ (function () {
    function FooterComponent(fetchService) {
        this.fetchService = fetchService;
    }
    FooterComponent.prototype.ngOnInit = function () {
    };
    FooterComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-footer',
            template: __webpack_require__(/*! ./footer.component.html */ "./src/app/components/footer/footer.component.html"),
            styles: [__webpack_require__(/*! ./footer.component.css */ "./src/app/components/footer/footer.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_static_content_fetch_service__WEBPACK_IMPORTED_MODULE_2__["StaticContentFetchService"]])
    ], FooterComponent);
    return FooterComponent;
}());



/***/ }),

/***/ "./src/app/components/header/header.component.css":
/*!********************************************************!*\
  !*** ./src/app/components/header/header.component.css ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".header {\n  /* background-color: #ffffff; */\n  width: 100%;\n  box-shadow: 0px 0px 40px 0px rgba(82, 63, 105, 0.1);\n  padding: 10px 20px;\n}\n.header-left {\n  padding-top: 1.5%;\n}\n.header-right {\n  text-align: right;\n  font-size: smaller;\n  /* color: #3d4465 */\n}\n.header-right .header-right-label {\n  font-size: 2rem;\n}\n@media screen and (max-width: 767px) {\n  .header-left,\n  .header-right {\n    text-align: center;\n  }\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9oZWFkZXIvaGVhZGVyLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSwrQkFBK0I7RUFDL0IsV0FBVztFQUNYLG1EQUFtRDtFQUNuRCxrQkFBa0I7QUFDcEI7QUFDQTtFQUNFLGlCQUFpQjtBQUNuQjtBQUVBO0VBQ0UsaUJBQWlCO0VBQ2pCLGtCQUFrQjtFQUNsQixtQkFBbUI7QUFDckI7QUFFQTtFQUNFLGVBQWU7QUFDakI7QUFFQTtFQUNFOztJQUVFLGtCQUFrQjtFQUNwQjtBQUNGIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50cy9oZWFkZXIvaGVhZGVyLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuaGVhZGVyIHtcbiAgLyogYmFja2dyb3VuZC1jb2xvcjogI2ZmZmZmZjsgKi9cbiAgd2lkdGg6IDEwMCU7XG4gIGJveC1zaGFkb3c6IDBweCAwcHggNDBweCAwcHggcmdiYSg4MiwgNjMsIDEwNSwgMC4xKTtcbiAgcGFkZGluZzogMTBweCAyMHB4O1xufVxuLmhlYWRlci1sZWZ0IHtcbiAgcGFkZGluZy10b3A6IDEuNSU7XG59XG5cbi5oZWFkZXItcmlnaHQge1xuICB0ZXh0LWFsaWduOiByaWdodDtcbiAgZm9udC1zaXplOiBzbWFsbGVyO1xuICAvKiBjb2xvcjogIzNkNDQ2NSAqL1xufVxuXG4uaGVhZGVyLXJpZ2h0IC5oZWFkZXItcmlnaHQtbGFiZWwge1xuICBmb250LXNpemU6IDJyZW07XG59XG5cbkBtZWRpYSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDc2N3B4KSB7XG4gIC5oZWFkZXItbGVmdCxcbiAgLmhlYWRlci1yaWdodCB7XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICB9XG59XG4iXX0= */"

/***/ }),

/***/ "./src/app/components/header/header.component.html":
/*!*********************************************************!*\
  !*** ./src/app/components/header/header.component.html ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<header class=\"header header-primary\">\n  <div class=\"container\">\n    <div class=\"row\">\n      <div class=\"header-left col-12 col-md-4\">\n         <img class=\"brand-logo\" [src]=\"this.fetchService.getCdnBaseUrl()+'/sca-ui/'+this.fetchService.getbuildversion()+'/'+this.fetchService.getStaticContent('HEADER','LOGO_URL')\" [alt]='this.fetchService.getStaticContent(\"HEADER\",\"BANK_LOGO_IMAGE_ALTERNATE_LABEL\")' \n         [title] = 'this.fetchService.getStaticContent(\"HEADER\",\"BANK_LOGO_IMAGE_ALTERNATE_LABEL\")'/>\n      </div>\n      <div class=\"header-right col-12 col-md-8\">\n        <div class=\"header-right-content\">\n          <h1 class=\"header-right-label label-primary\">{{this.fetchService.getStaticContent('HEADER','ACCOUNT_ACCESS_LABEL')}}</h1>\n          <p class=\"subInfo label-secondary\">{{this.fetchService.getStaticContent('HEADER','THIRD_PARTY_LABEL')}}</p>\n        </div>\n      </div>\n    </div>\n  </div>\n</header>"

/***/ }),

/***/ "./src/app/components/header/header.component.ts":
/*!*******************************************************!*\
  !*** ./src/app/components/header/header.component.ts ***!
  \*******************************************************/
/*! exports provided: HeaderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HeaderComponent", function() { return HeaderComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_static_content_fetch_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/static-content-fetch.service */ "./src/app/services/static-content-fetch.service.ts");



var HeaderComponent = /** @class */ (function () {
    function HeaderComponent(fetchService) {
        this.fetchService = fetchService;
    }
    HeaderComponent.prototype.ngOnInit = function () {
        // this.logourl=this.fetchService.getCdnBaseUrl()+'/v2/cmadev/sca-ui/'+this.fetchservice.getbuildversion()+'/'+this.fetchService.getStaticContent('HEADER','LOGO_URL'
    };
    HeaderComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-header',
            template: __webpack_require__(/*! ./header.component.html */ "./src/app/components/header/header.component.html"),
            styles: [__webpack_require__(/*! ./header.component.css */ "./src/app/components/header/header.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_static_content_fetch_service__WEBPACK_IMPORTED_MODULE_2__["StaticContentFetchService"]])
    ], HeaderComponent);
    return HeaderComponent;
}());



/***/ }),

/***/ "./src/app/components/loading-spinner/loading-spinner.component.css":
/*!**************************************************************************!*\
  !*** ./src/app/components/loading-spinner/loading-spinner.component.css ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".loading-spinner-container{\n    display: block;\n    position: fixed;\n    left:0;\n    top: 0;\n    right: 0;\n    bottom:0;\n    background-color: rgba(0,0,0,0.7);\n    text-align: center;\n    z-index: 1;\n}\n\n.loading-spinner {\n    width: 190px;\n    height: 70px;\n    margin: auto;\n    left: 0;\n    right: 0;\n    top: 0;\n    bottom: 0;\n    position: absolute;\n    color: #fff;\n}\n\n.textPara:after {\n    content: \"\";\n    position: absolute;\n    width: 40px;\n    height: 40px;\n    border: 2px solid;\n    top: 30px;\n    border-bottom-color: transparent;\n    left: 80px;\n    border-radius: 100%;\n    -webkit-animation: rotation 2s infinite linear;\n    /* animation: infinite; */\n}\n\n@-webkit-keyframes rotation {\n\tfrom {\n\t\t\t-webkit-transform: rotate(0deg);\n\t}\n\tto {\n\t\t\t-webkit-transform: rotate(359deg);\n\t}\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9sb2FkaW5nLXNwaW5uZXIvbG9hZGluZy1zcGlubmVyLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSxjQUFjO0lBQ2QsZUFBZTtJQUNmLE1BQU07SUFDTixNQUFNO0lBQ04sUUFBUTtJQUNSLFFBQVE7SUFDUixpQ0FBaUM7SUFDakMsa0JBQWtCO0lBQ2xCLFVBQVU7QUFDZDs7QUFFQTtJQUNJLFlBQVk7SUFDWixZQUFZO0lBQ1osWUFBWTtJQUNaLE9BQU87SUFDUCxRQUFRO0lBQ1IsTUFBTTtJQUNOLFNBQVM7SUFDVCxrQkFBa0I7SUFDbEIsV0FBVztBQUNmOztBQUNBO0lBQ0ksV0FBVztJQUNYLGtCQUFrQjtJQUNsQixXQUFXO0lBQ1gsWUFBWTtJQUNaLGlCQUFpQjtJQUNqQixTQUFTO0lBQ1QsZ0NBQWdDO0lBQ2hDLFVBQVU7SUFDVixtQkFBbUI7SUFDbkIsOENBQThDO0lBQzlDLHlCQUF5QjtBQUM3Qjs7QUFDQTtDQUNDO0dBQ0UsK0JBQStCO0NBQ2pDO0NBQ0E7R0FDRSxpQ0FBaUM7Q0FDbkM7QUFDRCIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvbG9hZGluZy1zcGlubmVyL2xvYWRpbmctc3Bpbm5lci5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmxvYWRpbmctc3Bpbm5lci1jb250YWluZXJ7XG4gICAgZGlzcGxheTogYmxvY2s7XG4gICAgcG9zaXRpb246IGZpeGVkO1xuICAgIGxlZnQ6MDtcbiAgICB0b3A6IDA7XG4gICAgcmlnaHQ6IDA7XG4gICAgYm90dG9tOjA7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogcmdiYSgwLDAsMCwwLjcpO1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICB6LWluZGV4OiAxO1xufVxuXG4ubG9hZGluZy1zcGlubmVyIHtcbiAgICB3aWR0aDogMTkwcHg7XG4gICAgaGVpZ2h0OiA3MHB4O1xuICAgIG1hcmdpbjogYXV0bztcbiAgICBsZWZ0OiAwO1xuICAgIHJpZ2h0OiAwO1xuICAgIHRvcDogMDtcbiAgICBib3R0b206IDA7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGNvbG9yOiAjZmZmO1xufVxuLnRleHRQYXJhOmFmdGVyIHtcbiAgICBjb250ZW50OiBcIlwiO1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICB3aWR0aDogNDBweDtcbiAgICBoZWlnaHQ6IDQwcHg7XG4gICAgYm9yZGVyOiAycHggc29saWQ7XG4gICAgdG9wOiAzMHB4O1xuICAgIGJvcmRlci1ib3R0b20tY29sb3I6IHRyYW5zcGFyZW50O1xuICAgIGxlZnQ6IDgwcHg7XG4gICAgYm9yZGVyLXJhZGl1czogMTAwJTtcbiAgICAtd2Via2l0LWFuaW1hdGlvbjogcm90YXRpb24gMnMgaW5maW5pdGUgbGluZWFyO1xuICAgIC8qIGFuaW1hdGlvbjogaW5maW5pdGU7ICovXG59XG5ALXdlYmtpdC1rZXlmcmFtZXMgcm90YXRpb24ge1xuXHRmcm9tIHtcblx0XHRcdC13ZWJraXQtdHJhbnNmb3JtOiByb3RhdGUoMGRlZyk7XG5cdH1cblx0dG8ge1xuXHRcdFx0LXdlYmtpdC10cmFuc2Zvcm06IHJvdGF0ZSgzNTlkZWcpO1xuXHR9XG59Il19 */"

/***/ }),

/***/ "./src/app/components/loading-spinner/loading-spinner.component.html":
/*!***************************************************************************!*\
  !*** ./src/app/components/loading-spinner/loading-spinner.component.html ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"loading-spinner-container\">\n  <div class=\"loading-spinner\">\n    <em class=\"fa fa-spinner fa-pulse fa-3x fa-fw\"></em>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/components/loading-spinner/loading-spinner.component.ts":
/*!*************************************************************************!*\
  !*** ./src/app/components/loading-spinner/loading-spinner.component.ts ***!
  \*************************************************************************/
/*! exports provided: LoadingSpinnerComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoadingSpinnerComponent", function() { return LoadingSpinnerComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var LoadingSpinnerComponent = /** @class */ (function () {
    function LoadingSpinnerComponent() {
    }
    LoadingSpinnerComponent.prototype.ngOnInit = function () {
    };
    LoadingSpinnerComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-loading-spinner',
            template: __webpack_require__(/*! ./loading-spinner.component.html */ "./src/app/components/loading-spinner/loading-spinner.component.html"),
            styles: [__webpack_require__(/*! ./loading-spinner.component.css */ "./src/app/components/loading-spinner/loading-spinner.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], LoadingSpinnerComponent);
    return LoadingSpinnerComponent;
}());



/***/ }),

/***/ "./src/app/components/login/login.component.css":
/*!******************************************************!*\
  !*** ./src/app/components/login/login.component.css ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".content {\n \n  margin: 30px auto;\n  text-align: center;\n}\n.content-box {\n  text-align: left;\n  /* color: #3d4465; */\n  margin: auto;\n  /* background-color: #fff; */\n  box-shadow: 0px 0px 13px 0px rgba(82, 63, 105, 0.05);\n  padding: 20px;\n}\n.alert-box{\n  margin: auto;\n  padding: 0;\n  text-align: left;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9sb2dpbi9sb2dpbi5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBOztFQUVFLGlCQUFpQjtFQUNqQixrQkFBa0I7QUFDcEI7QUFDQTtFQUNFLGdCQUFnQjtFQUNoQixvQkFBb0I7RUFDcEIsWUFBWTtFQUNaLDRCQUE0QjtFQUM1QixvREFBb0Q7RUFDcEQsYUFBYTtBQUNmO0FBRUE7RUFDRSxZQUFZO0VBQ1osVUFBVTtFQUNWLGdCQUFnQjtBQUNsQiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvbG9naW4vbG9naW4uY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5jb250ZW50IHtcbiBcbiAgbWFyZ2luOiAzMHB4IGF1dG87XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cbi5jb250ZW50LWJveCB7XG4gIHRleHQtYWxpZ246IGxlZnQ7XG4gIC8qIGNvbG9yOiAjM2Q0NDY1OyAqL1xuICBtYXJnaW46IGF1dG87XG4gIC8qIGJhY2tncm91bmQtY29sb3I6ICNmZmY7ICovXG4gIGJveC1zaGFkb3c6IDBweCAwcHggMTNweCAwcHggcmdiYSg4MiwgNjMsIDEwNSwgMC4wNSk7XG4gIHBhZGRpbmc6IDIwcHg7XG59XG5cbi5hbGVydC1ib3h7XG4gIG1hcmdpbjogYXV0bztcbiAgcGFkZGluZzogMDtcbiAgdGV4dC1hbGlnbjogbGVmdDtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/components/login/login.component.html":
/*!*******************************************************!*\
  !*** ./src/app/components/login/login.component.html ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<main class=\"content\">\n  <div class=\"container-fluid\">\n    <div class=\"row\">\n      <div class=\"alert-box col-11 \">\n        <div *ngIf=\"!this.errorService.errors['SHOW_NOTICE']\">\n          <app-error *ngFor=\"let notice of this.errorService.errorArr\" [notice] =\"this.notice\"></app-error>\n        </div>\n          <app-error *ngIf=\"this.errorService.errors['SHOW_NOTICE']\" [notice] =\"this.errorService.notice\"></app-error>\n      </div>\n    </div>\n    <div class=\"row\" *ngIf=\"!this.errorService.errors['SHOW_NOTICE']\">  \n      <div class=\"content-box main-secondary label-primary col-10 col-sm-7 col-md-5 col-lg-4\">\n        <app-username></app-username>\n      </div>\n    </div>\n  </div>\n</main>\n"

/***/ }),

/***/ "./src/app/components/login/login.component.ts":
/*!*****************************************************!*\
  !*** ./src/app/components/login/login.component.ts ***!
  \*****************************************************/
/*! exports provided: LoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return LoginComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_error_handler_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/error-handler.service */ "./src/app/services/error-handler.service.ts");



var LoginComponent = /** @class */ (function () {
    function LoginComponent(errorService) {
        this.errorService = errorService;
    }
    LoginComponent.prototype.ngOnInit = function () { };
    LoginComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-login',
            template: __webpack_require__(/*! ./login.component.html */ "./src/app/components/login/login.component.html"),
            styles: [__webpack_require__(/*! ./login.component.css */ "./src/app/components/login/login.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_error_handler_service__WEBPACK_IMPORTED_MODULE_2__["ErrorHandlerService"]])
    ], LoginComponent);
    return LoginComponent;
}());



/***/ }),

/***/ "./src/app/components/modal/modal.component.css":
/*!******************************************************!*\
  !*** ./src/app/components/modal/modal.component.css ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".modal {\n  display: block;\n  /* background-color: rgba(0,0,0,0.4); */\n  text-align: center;\n}\n\n.modal .modal-dialog {\n  margin: 11% auto;\n}\n\n.modal-header{\n    padding: 2% 4%;\n}\n\n.modal-body{\n    font-size: 14px;\n}\n\n.modal-label{\n    font-size: 1.1rem\n}\n\n.modal .modal-content {\n  margin: 100px auto;\n}\n\n.modal .modal-label {\n  text-align: left;\n}\n\n.modal .ok-btn {\n  min-width: 100px;\n  margin: 0px auto;\n}\n\n.close {\n    background-color: transparent;\n    border:none;\n}\n\n.modal-footer{\n    padding: 2%;\n    border: none;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9tb2RhbC9tb2RhbC5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsY0FBYztFQUNkLHVDQUF1QztFQUN2QyxrQkFBa0I7QUFDcEI7O0FBRUE7RUFDRSxnQkFBZ0I7QUFDbEI7O0FBRUE7SUFDSSxjQUFjO0FBQ2xCOztBQUVBO0lBQ0ksZUFBZTtBQUNuQjs7QUFFQTtJQUNJO0FBQ0o7O0FBRUE7RUFDRSxrQkFBa0I7QUFDcEI7O0FBRUE7RUFDRSxnQkFBZ0I7QUFDbEI7O0FBRUE7RUFDRSxnQkFBZ0I7RUFDaEIsZ0JBQWdCO0FBQ2xCOztBQUVBO0lBQ0ksNkJBQTZCO0lBQzdCLFdBQVc7QUFDZjs7QUFFQTtJQUNJLFdBQVc7SUFDWCxZQUFZO0FBQ2hCIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50cy9tb2RhbC9tb2RhbC5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLm1vZGFsIHtcbiAgZGlzcGxheTogYmxvY2s7XG4gIC8qIGJhY2tncm91bmQtY29sb3I6IHJnYmEoMCwwLDAsMC40KTsgKi9cbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuXG4ubW9kYWwgLm1vZGFsLWRpYWxvZyB7XG4gIG1hcmdpbjogMTElIGF1dG87XG59XG5cbi5tb2RhbC1oZWFkZXJ7XG4gICAgcGFkZGluZzogMiUgNCU7XG59XG5cbi5tb2RhbC1ib2R5e1xuICAgIGZvbnQtc2l6ZTogMTRweDtcbn1cblxuLm1vZGFsLWxhYmVse1xuICAgIGZvbnQtc2l6ZTogMS4xcmVtXG59XG5cbi5tb2RhbCAubW9kYWwtY29udGVudCB7XG4gIG1hcmdpbjogMTAwcHggYXV0bztcbn1cblxuLm1vZGFsIC5tb2RhbC1sYWJlbCB7XG4gIHRleHQtYWxpZ246IGxlZnQ7XG59XG5cbi5tb2RhbCAub2stYnRuIHtcbiAgbWluLXdpZHRoOiAxMDBweDtcbiAgbWFyZ2luOiAwcHggYXV0bztcbn1cblxuLmNsb3NlIHtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudDtcbiAgICBib3JkZXI6bm9uZTtcbn1cblxuLm1vZGFsLWZvb3RlcntcbiAgICBwYWRkaW5nOiAyJTtcbiAgICBib3JkZXI6IG5vbmU7XG59Il19 */"

/***/ }),

/***/ "./src/app/components/modal/modal.component.html":
/*!*******************************************************!*\
  !*** ./src/app/components/modal/modal.component.html ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"modal\" id=\"modalPopUpBox\">\n  <div class=\"modal-dialog\">\n    <div class=\"modal-content\">\n      <div class=\"modal-header modal-secondary\">\n        <span class=\"modal-label label-primary\">{{this.fetchService.getStaticContent('SESSION_TIMEOUT_POPUP','SESSION_TIMEOUT_POPUP_HEADER_TITLE')}}</span>\n      </div>\n      <div class=\"modal-body modal-primary label-secondary\">\n        {{this.fetchService.getStaticContent('SESSION_TIMEOUT_POPUP','SESSION_TIMEOUT_POPUP_MESSAGE')}}\n      </div>\n      <div class=\"modal-footer modal-primary\">\n          <input type=\"button\" [value]=\"this.fetchService.getStaticContent('SESSION_TIMEOUT_POPUP','SESSION_POPOUT_BUTTON_LABEL')\" class=\"btn ok-btn bttn-primary\" (click)=\"onClick()\">\n      </div>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/components/modal/modal.component.ts":
/*!*****************************************************!*\
  !*** ./src/app/components/modal/modal.component.ts ***!
  \*****************************************************/
/*! exports provided: ModalComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ModalComponent", function() { return ModalComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_static_content_fetch_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/static-content-fetch.service */ "./src/app/services/static-content-fetch.service.ts");



var ModalComponent = /** @class */ (function () {
    function ModalComponent(fetchService) {
        this.fetchService = fetchService;
        this.redirectUri = '';
        this.closeModal = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
    }
    ModalComponent.prototype.ngOnInit = function () {
    };
    ModalComponent.prototype.onClick = function () {
        window.location.href = this.redirectUri;
    };
    // used for modal popups except session timeout. 
    ModalComponent.prototype.onClose = function () {
        this.redirectUri = '';
        this.closeModal.emit(false);
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
    ], ModalComponent.prototype, "redirectUri", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], ModalComponent.prototype, "closeModal", void 0);
    ModalComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-modal',
            template: __webpack_require__(/*! ./modal.component.html */ "./src/app/components/modal/modal.component.html"),
            styles: [__webpack_require__(/*! ./modal.component.css */ "./src/app/components/modal/modal.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_static_content_fetch_service__WEBPACK_IMPORTED_MODULE_2__["StaticContentFetchService"]])
    ], ModalComponent);
    return ModalComponent;
}());



/***/ }),

/***/ "./src/app/components/permissions/permissions.component.css":
/*!******************************************************************!*\
  !*** ./src/app/components/permissions/permissions.component.css ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".header-bar-primary{\n    background: #5d78ff;\n    border-radius: 4px;\n    color: #fff;\n    font-size: 18px;\n    letter-spacing: 0;\n    line-height: 1.5;\n    padding: 18px 20px;\n    word-wrap: break-word;\n}\n.card-header{\n    padding:0 0;\n}\n.request-permissions .btn-link .icon-plus-minus{\n    background: #5d78ff;\n    border-radius: 100%;\n    display: block;\n    transition-duration: 0.5s;\n    top: 13px;\n    bottom: 0;\n    left: 15px;\n    position: absolute;\n    height: 24px;\n    width: 24px;\n}\n.request-permissions .accordion > .card .card-header button{\n    height: auto;\n    padding: 13px 15px 13px 45px;\n    width: 100%;\n    margin: 0;\n}\n.request-permissions .card-header .btn-link{\n    font-size: 14px;\n    color: #34353a;\n    text-align: left;\n    text-decoration: none;\n    vertical-align: middle;\n}\n.request-permissions .accordion > .card .card-header{\n    background-color: #f1f1f1;\n    border: 0;\n    padding: 1px;\n    padding-bottom: 2px;\n    position: relative;\n}\n.accordion>.card .card-header {\n    margin-bottom: -1px;\n}\n.request-permissions .btn-link .icon-plus-minus:before {\n    color: #fff;\n    font-size: 20px;\n    line-height: 24px;\n    text-align: center;\n    content: \"+\";\n    top: 0;\n    left: 0;\n    position: absolute;\n    height: 24px;\n    width: 24px;\n}\n.request-permissions .btn-link.collapsed .icon-plus-minus:before {\n    content: \"-\";\n    font-size: 30px;\n    line-height: 19px;\n}\n.margin-bottom-20{\n    margin-bottom: 20px;\n}\n.request-permissions{\n    background-color: #fff;\n}\n.details-container {\n    padding: 0 20px;\n}\n.padding-top-bottom-15 {\n    padding-top: 15px;\n    padding-bottom: 15px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9wZXJtaXNzaW9ucy9wZXJtaXNzaW9ucy5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksbUJBQW1CO0lBQ25CLGtCQUFrQjtJQUNsQixXQUFXO0lBQ1gsZUFBZTtJQUNmLGlCQUFpQjtJQUNqQixnQkFBZ0I7SUFDaEIsa0JBQWtCO0lBQ2xCLHFCQUFxQjtBQUN6QjtBQUNBO0lBQ0ksV0FBVztBQUNmO0FBQ0E7SUFDSSxtQkFBbUI7SUFDbkIsbUJBQW1CO0lBQ25CLGNBQWM7SUFDZCx5QkFBeUI7SUFDekIsU0FBUztJQUNULFNBQVM7SUFDVCxVQUFVO0lBQ1Ysa0JBQWtCO0lBQ2xCLFlBQVk7SUFDWixXQUFXO0FBQ2Y7QUFDQTtJQUNJLFlBQVk7SUFDWiw0QkFBNEI7SUFDNUIsV0FBVztJQUNYLFNBQVM7QUFDYjtBQUNBO0lBQ0ksZUFBZTtJQUNmLGNBQWM7SUFDZCxnQkFBZ0I7SUFDaEIscUJBQXFCO0lBQ3JCLHNCQUFzQjtBQUMxQjtBQUNBO0lBQ0kseUJBQXlCO0lBQ3pCLFNBQVM7SUFDVCxZQUFZO0lBQ1osbUJBQW1CO0lBQ25CLGtCQUFrQjtBQUN0QjtBQUNBO0lBQ0ksbUJBQW1CO0FBQ3ZCO0FBQ0E7SUFDSSxXQUFXO0lBQ1gsZUFBZTtJQUNmLGlCQUFpQjtJQUNqQixrQkFBa0I7SUFDbEIsWUFBWTtJQUNaLE1BQU07SUFDTixPQUFPO0lBQ1Asa0JBQWtCO0lBQ2xCLFlBQVk7SUFDWixXQUFXO0FBQ2Y7QUFDQTtJQUNJLFlBQVk7SUFDWixlQUFlO0lBQ2YsaUJBQWlCO0FBQ3JCO0FBQ0E7SUFDSSxtQkFBbUI7QUFDdkI7QUFDQTtJQUNJLHNCQUFzQjtBQUMxQjtBQUNBO0lBQ0ksZUFBZTtBQUNuQjtBQUVBO0lBQ0ksaUJBQWlCO0lBQ2pCLG9CQUFvQjtBQUN4QiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvcGVybWlzc2lvbnMvcGVybWlzc2lvbnMuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5oZWFkZXItYmFyLXByaW1hcnl7XG4gICAgYmFja2dyb3VuZDogIzVkNzhmZjtcbiAgICBib3JkZXItcmFkaXVzOiA0cHg7XG4gICAgY29sb3I6ICNmZmY7XG4gICAgZm9udC1zaXplOiAxOHB4O1xuICAgIGxldHRlci1zcGFjaW5nOiAwO1xuICAgIGxpbmUtaGVpZ2h0OiAxLjU7XG4gICAgcGFkZGluZzogMThweCAyMHB4O1xuICAgIHdvcmQtd3JhcDogYnJlYWstd29yZDtcbn1cbi5jYXJkLWhlYWRlcntcbiAgICBwYWRkaW5nOjAgMDtcbn1cbi5yZXF1ZXN0LXBlcm1pc3Npb25zIC5idG4tbGluayAuaWNvbi1wbHVzLW1pbnVze1xuICAgIGJhY2tncm91bmQ6ICM1ZDc4ZmY7XG4gICAgYm9yZGVyLXJhZGl1czogMTAwJTtcbiAgICBkaXNwbGF5OiBibG9jaztcbiAgICB0cmFuc2l0aW9uLWR1cmF0aW9uOiAwLjVzO1xuICAgIHRvcDogMTNweDtcbiAgICBib3R0b206IDA7XG4gICAgbGVmdDogMTVweDtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgaGVpZ2h0OiAyNHB4O1xuICAgIHdpZHRoOiAyNHB4O1xufVxuLnJlcXVlc3QtcGVybWlzc2lvbnMgLmFjY29yZGlvbiA+IC5jYXJkIC5jYXJkLWhlYWRlciBidXR0b257XG4gICAgaGVpZ2h0OiBhdXRvO1xuICAgIHBhZGRpbmc6IDEzcHggMTVweCAxM3B4IDQ1cHg7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgbWFyZ2luOiAwO1xufVxuLnJlcXVlc3QtcGVybWlzc2lvbnMgLmNhcmQtaGVhZGVyIC5idG4tbGlua3tcbiAgICBmb250LXNpemU6IDE0cHg7XG4gICAgY29sb3I6ICMzNDM1M2E7XG4gICAgdGV4dC1hbGlnbjogbGVmdDtcbiAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XG4gICAgdmVydGljYWwtYWxpZ246IG1pZGRsZTtcbn1cbi5yZXF1ZXN0LXBlcm1pc3Npb25zIC5hY2NvcmRpb24gPiAuY2FyZCAuY2FyZC1oZWFkZXJ7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2YxZjFmMTtcbiAgICBib3JkZXI6IDA7XG4gICAgcGFkZGluZzogMXB4O1xuICAgIHBhZGRpbmctYm90dG9tOiAycHg7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xufVxuLmFjY29yZGlvbj4uY2FyZCAuY2FyZC1oZWFkZXIge1xuICAgIG1hcmdpbi1ib3R0b206IC0xcHg7XG59XG4ucmVxdWVzdC1wZXJtaXNzaW9ucyAuYnRuLWxpbmsgLmljb24tcGx1cy1taW51czpiZWZvcmUge1xuICAgIGNvbG9yOiAjZmZmO1xuICAgIGZvbnQtc2l6ZTogMjBweDtcbiAgICBsaW5lLWhlaWdodDogMjRweDtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgY29udGVudDogXCIrXCI7XG4gICAgdG9wOiAwO1xuICAgIGxlZnQ6IDA7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGhlaWdodDogMjRweDtcbiAgICB3aWR0aDogMjRweDtcbn1cbi5yZXF1ZXN0LXBlcm1pc3Npb25zIC5idG4tbGluay5jb2xsYXBzZWQgLmljb24tcGx1cy1taW51czpiZWZvcmUge1xuICAgIGNvbnRlbnQ6IFwiLVwiO1xuICAgIGZvbnQtc2l6ZTogMzBweDtcbiAgICBsaW5lLWhlaWdodDogMTlweDtcbn1cbi5tYXJnaW4tYm90dG9tLTIwe1xuICAgIG1hcmdpbi1ib3R0b206IDIwcHg7XG59XG4ucmVxdWVzdC1wZXJtaXNzaW9uc3tcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xufVxuLmRldGFpbHMtY29udGFpbmVyIHtcbiAgICBwYWRkaW5nOiAwIDIwcHg7XG59XG5cbi5wYWRkaW5nLXRvcC1ib3R0b20tMTUge1xuICAgIHBhZGRpbmctdG9wOiAxNXB4O1xuICAgIHBhZGRpbmctYm90dG9tOiAxNXB4O1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/components/permissions/permissions.component.html":
/*!*******************************************************************!*\
  !*** ./src/app/components/permissions/permissions.component.html ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"margin-bottom-20 request-permissions\">\n  <div class=\"header-bar-primary\">Requested permissions</div>\n  <div class=\"details-container padding-top-bottom-15\">\n    <div class=\"accordion\" id=\"permissionsReq\">\n      <ng-container *ngFor=\"let i of permissionGroupsKeys\">\n        <div *ngIf=\"permissionGroups[i].data.length\" class=\"card\">\n          <div class=\"card-header\">\n            <button\n              class=\"btn btn-link\"\n              role=\"button\"\n              (click)=\"showDetail(i)\"\n              [ngClass] = \"{'collapsed' : permissionGroups[i].state }\"\n              [attr.aria-expanded]=\"permissionGroups[i].state\">\n              <span class=\"icon-plus-minus\"></span>\n              <span\n                [innerHTML]=\"accountPermissions[i]\"\n              ></span>\n            </button>\n          </div>\n          <div class=\"collapsed show\" *ngIf=\"permissionGroups[i].state\">\n            <div class=\"card-body\">\n              \n                <ul *ngIf=\"\n                      permissionGroups[i].state &&\n                      permissionGroups[i].data.length !== 0\n                    \"\n                    class=\"permission-description\">\n                    <ng-container *ngFor=\"let temp of permissionGroups[i].data\">\n                    <li\n                      [innerHTML]=\"\n                        accountPermissionDescription[\n                          (temp | uppercase) + '_ELEMENTS'\n                        ]\n                      \"\n                    ></li>\n                  </ng-container>\n                </ul>\n            </div>\n          </div>\n        </div>\n      </ng-container>\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/components/permissions/permissions.component.ts":
/*!*****************************************************************!*\
  !*** ./src/app/components/permissions/permissions.component.ts ***!
  \*****************************************************************/
/*! exports provided: PermissionsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PermissionsComponent", function() { return PermissionsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_static_content_fetch_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/static-content-fetch.service */ "./src/app/services/static-content-fetch.service.ts");
/* harmony import */ var src_app_services_consent_detail_service_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/consent-detail-service.service */ "./src/app/services/consent-detail-service.service.ts");




var PermissionsComponent = /** @class */ (function () {
    function PermissionsComponent(fetchService, accountService) {
        this.fetchService = fetchService;
        this.accountService = accountService;
        this.permissionListData = [];
        this.accountDetail = false;
        this.regularPayment = false;
        this.accountTransactions = false;
        this.statements = false;
        this.accountFeature = false;
        this.permissionGroups = {};
        this.permissionGroupsKeys = [];
    }
    PermissionsComponent.prototype.showDetail = function (temp) {
        this.permissionGroups[temp].state = !this.permissionGroups[temp].state;
    };
    PermissionsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.accountPermissionHeader = this.fetchService.getStaticContent(this.accountService.consentType, 'REVIEW_CONFIRM_PAGE', 'PERMISSIONS', 'PERMISSION_HEADER');
        this.accountRequestPermissions = this.accountService.accRequest.Data.Permissions;
        this.accountPermissions = this.fetchService.getStaticContent(this.accountService.consentType, 'REVIEW_CONFIRM_PAGE', 'PERMISSIONS', 'PERMISSION_LABEL');
        this.accountPermissionDescription = this.fetchService.getStaticContent(this.accountService.consentType, 'REVIEW_CONFIRM_PAGE', 'PERMISSIONS', 'PERMISSION_DESCRIPTIONS');
        for (var i = 1; i <= 5; i++) {
            this.permissionGroups['HEADING' + i] = { state: false, data: [] };
        }
        this.permissionGroupsKeys = Object.keys(this.permissionGroups);
        this.accountRequestPermissions.forEach(function (element) {
            if (element !== 'ReadTransactionsCredits' &&
                element !== 'ReadTransactionsDebits') {
                if (element === 'ReadTransactionsBasic' || element === 'ReadTransactionsDetail') {
                    if (_this.accountRequestPermissions.indexOf('ReadTransactionsCredits') !== -1 &&
                        _this.accountRequestPermissions.indexOf('ReadTransactionsDebits') !==
                            -1) {
                        element = 'All' + element;
                    }
                    else if (_this.accountRequestPermissions.indexOf('ReadTransactionsCredits') !== -1) {
                        element = 'ReadTransactionsCreditsBasic';
                    }
                    else {
                        element = 'ReadTransactionsDebitsBasic';
                    }
                }
                _this.permissionListData.push(element);
            }
        });
        this.permissionListData.forEach(function (element1) {
            if (element1 === 'ReadAccountsBasic' ||
                element1 === 'ReadAccountsDetail' ||
                element1 === 'ReadBalances' ||
                element1 === 'ReadPAN') {
                _this.permissionGroups["HEADING1"].data.push(element1);
            }
            else if (element1 === 'ReadBeneficiariesBasic' ||
                element1 === 'ReadBeneficiariesDetail' ||
                element1 === 'ReadStandingOrdersBasic' ||
                element1 === 'ReadStandingOrdersDetail' ||
                element1 === 'ReadDirectDebits' ||
                element1 === 'ReadScheduledPaymentsBasic' ||
                element1 === 'ReadScheduledPaymentsDetail') {
                _this.permissionGroups["HEADING2"].data.push(element1);
            }
            else if (element1 === 'AllReadTransactionsBasic' ||
                element1 === 'ReadTransactionsCreditsBasic' ||
                element1 === 'ReadTransactionsDebitsBasic' ||
                element1 === 'AllReadTransactionsDetail' ||
                element1 === 'ReadTransactionsCreditsDetail' ||
                element1 === 'ReadTransactionsDebitsDetail') {
                _this.permissionGroups["HEADING3"].data.push(element1);
            }
            else if (element1 === 'ReadStatementsBasic' ||
                element1 === 'ReadStatementsDetail') {
                _this.permissionGroups["HEADING4"].data.push(element1);
            }
            else if (element1 === 'ReadProducts') {
                _this.permissionGroups["HEADING5"].data.push(element1);
            }
        });
    };
    PermissionsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-permissions',
            template: __webpack_require__(/*! ./permissions.component.html */ "./src/app/components/permissions/permissions.component.html"),
            styles: [__webpack_require__(/*! ./permissions.component.css */ "./src/app/components/permissions/permissions.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_static_content_fetch_service__WEBPACK_IMPORTED_MODULE_2__["StaticContentFetchService"],
            src_app_services_consent_detail_service_service__WEBPACK_IMPORTED_MODULE_3__["ConsentDetailServiceService"]])
    ], PermissionsComponent);
    return PermissionsComponent;
}());



/***/ }),

/***/ "./src/app/components/pisp-consent-details/pisp-consent-details.component.css":
/*!************************************************************************************!*\
  !*** ./src/app/components/pisp-consent-details/pisp-consent-details.component.css ***!
  \************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".container{\n    font-size:14px;\n    padding-bottom:40px;\n    background-color: #ffffff;\n    margin-bottom:2px;\n}\n.main-div{\n    display: inline-block;\n}\n.header-bar-primary{\n    background: #5d78ff;\n    border-radius: 4px;\n    color: #fff;\n    font-size: 18px;\n    letter-spacing: 0;\n    line-height: 1.5;\n    padding: 18px 20px;\n    word-wrap: break-word;\n}\n.table-header-container{\n    background-color:#fff;\n    width:100%;\n    margin:15px;\n}\nthead{\n    background-color: #5d78ff;\n    border:1px solid black!important;\n}\n.table{\n    padding:15px;\n    display:inline-table;\n    margin: 15px;\n    width: 100%; \n}\nth,tr,td{\n    border:1px solid black!important;\n    text-align: left;\n}\n.payment-section-label{\n    font-weight:700\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9waXNwLWNvbnNlbnQtZGV0YWlscy9waXNwLWNvbnNlbnQtZGV0YWlscy5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksY0FBYztJQUNkLG1CQUFtQjtJQUNuQix5QkFBeUI7SUFDekIsaUJBQWlCO0FBQ3JCO0FBQ0E7SUFDSSxxQkFBcUI7QUFDekI7QUFDQTtJQUNJLG1CQUFtQjtJQUNuQixrQkFBa0I7SUFDbEIsV0FBVztJQUNYLGVBQWU7SUFDZixpQkFBaUI7SUFDakIsZ0JBQWdCO0lBQ2hCLGtCQUFrQjtJQUNsQixxQkFBcUI7QUFDekI7QUFDQTtJQUNJLHFCQUFxQjtJQUNyQixVQUFVO0lBQ1YsV0FBVztBQUNmO0FBQ0E7SUFDSSx5QkFBeUI7SUFDekIsZ0NBQWdDO0FBQ3BDO0FBQ0E7SUFDSSxZQUFZO0lBQ1osb0JBQW9CO0lBQ3BCLFlBQVk7SUFDWixXQUFXO0FBQ2Y7QUFDQTtJQUNJLGdDQUFnQztJQUNoQyxnQkFBZ0I7QUFDcEI7QUFFQTtJQUNJO0FBQ0oiLCJmaWxlIjoic3JjL2FwcC9jb21wb25lbnRzL3Bpc3AtY29uc2VudC1kZXRhaWxzL3Bpc3AtY29uc2VudC1kZXRhaWxzLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuY29udGFpbmVye1xuICAgIGZvbnQtc2l6ZToxNHB4O1xuICAgIHBhZGRpbmctYm90dG9tOjQwcHg7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZmZmZjtcbiAgICBtYXJnaW4tYm90dG9tOjJweDtcbn1cbi5tYWluLWRpdntcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG59XG4uaGVhZGVyLWJhci1wcmltYXJ5e1xuICAgIGJhY2tncm91bmQ6ICM1ZDc4ZmY7XG4gICAgYm9yZGVyLXJhZGl1czogNHB4O1xuICAgIGNvbG9yOiAjZmZmO1xuICAgIGZvbnQtc2l6ZTogMThweDtcbiAgICBsZXR0ZXItc3BhY2luZzogMDtcbiAgICBsaW5lLWhlaWdodDogMS41O1xuICAgIHBhZGRpbmc6IDE4cHggMjBweDtcbiAgICB3b3JkLXdyYXA6IGJyZWFrLXdvcmQ7XG59XG4udGFibGUtaGVhZGVyLWNvbnRhaW5lcntcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiNmZmY7XG4gICAgd2lkdGg6MTAwJTtcbiAgICBtYXJnaW46MTVweDtcbn1cbnRoZWFke1xuICAgIGJhY2tncm91bmQtY29sb3I6ICM1ZDc4ZmY7XG4gICAgYm9yZGVyOjFweCBzb2xpZCBibGFjayFpbXBvcnRhbnQ7XG59XG4udGFibGV7XG4gICAgcGFkZGluZzoxNXB4O1xuICAgIGRpc3BsYXk6aW5saW5lLXRhYmxlO1xuICAgIG1hcmdpbjogMTVweDtcbiAgICB3aWR0aDogMTAwJTsgXG59XG50aCx0cix0ZHtcbiAgICBib3JkZXI6MXB4IHNvbGlkIGJsYWNrIWltcG9ydGFudDtcbiAgICB0ZXh0LWFsaWduOiBsZWZ0O1xufVxuXG4ucGF5bWVudC1zZWN0aW9uLWxhYmVse1xuICAgIGZvbnQtd2VpZ2h0OjcwMFxufSJdfQ== */"

/***/ }),

/***/ "./src/app/components/pisp-consent-details/pisp-consent-details.component.html":
/*!*************************************************************************************!*\
  !*** ./src/app/components/pisp-consent-details/pisp-consent-details.component.html ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<div class=\"container\">\n  <div class=\"row\">\n    <table class=\"table table-striped card-detail-viewer\">\n      <thead class=\"header-bar-primary\">\n      <tr><th colspan=\"2\" >Creditor Details</th></tr>\n      </thead>\n    \n        <tr>\n          <th>Payment Initiated By</th>\n          <td [innerHTML]=\"payInstBy?payInstBy:'-'\"></td>\n        </tr>\n        <tr>\n          <th>BIC/NSC</th>\n          <td [innerHTML]=\"bic?bic:'-'\"></td>\n        </tr>  \n        <tr>\n          <th>Amount</th>\n          <td [innerHTML]=\"amount?(amount | currency:currency):'-'\"></td>\n        </tr>\n        <tr>\n            <th>Payee name</th>\n            <td [innerHTML]=\"payeeName?payeeName:'-'\"></td>\n        </tr>\n        <tr>\n          <th>IBAN</th>\n          <td [innerHTML]=\"accountNo?(accountNo | accountDigitsFilter):'-'\"></td>\n        </tr>\n        <tr>\n          <th>Reference</th>\n          <td [innerHTML]=\"payeeReference?payeeReference:'-'\"></td>\n        </tr> \n      </tbody>\n    </table>\n  </div>\n\n  <div>\n    <span class=\"payment-section-label\">Software On Behalf Of:</span>\n    <span class=\"payment-section-value\" [innerHTML]='this.fetchService.getSoftwareOnBehalfOfOrg()'></span>\n    <p></p>\n  </div>\n\n  <div class=\"debtor-details\">\n    <h4> Debtor Details</h4>\n    <div class=\"payment-inner-row\">\n      <span class=\"payment-section-label\">Debitor Account No:</span>\n      <span class=\"payment-section-value\" [innerHTML]=\"debitorAccountNo?debitorAccountNo:'-'\"></span>\n    </div>\n  </div>\n</div>\n\n\n"

/***/ }),

/***/ "./src/app/components/pisp-consent-details/pisp-consent-details.component.ts":
/*!***********************************************************************************!*\
  !*** ./src/app/components/pisp-consent-details/pisp-consent-details.component.ts ***!
  \***********************************************************************************/
/*! exports provided: PispConsentDetailsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PispConsentDetailsComponent", function() { return PispConsentDetailsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_consent_detail_service_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/consent-detail-service.service */ "./src/app/services/consent-detail-service.service.ts");
/* harmony import */ var src_app_services_static_content_fetch_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/static-content-fetch.service */ "./src/app/services/static-content-fetch.service.ts");




var PispConsentDetailsComponent = /** @class */ (function () {
    function PispConsentDetailsComponent(consentService, fetchService) {
        this.consentService = consentService;
        this.fetchService = fetchService;
    }
    PispConsentDetailsComponent.prototype.ngOnInit = function () {
        if (this.consentService.tppInfo && this.consentService.tppInfo.applicationName && this.consentService.tppInfo.tppName) {
            this.payInstBy = this.consentService.tppInfo.applicationName + '(' + this.consentService.tppInfo.tppName + ')';
        }
        this.payeeDetails = this.consentService.payeeDetails;
        if (this.payeeDetails) {
            this.debitorAccountNo = this.payeeDetails.debtorDetails ? this.payeeDetails.debtorDetails.Identification : '';
            this.payeeName = this.payeeDetails.creditorDetails ? this.payeeDetails.creditorDetails.Name : '';
            if (this.payeeDetails.creditorDetails &&
                this.payeeDetails.creditorDetails.SecondaryIdentification &&
                this.payeeDetails.remittanceDetails &&
                this.payeeDetails.remittanceDetails.Unstructured &&
                this.payeeDetails.remittanceDetails.Reference) {
                this.payeeReference = this.payeeDetails.creditorDetails.SecondaryIdentification;
            }
            else if (this.payeeDetails.creditorDetails && this.payeeDetails.creditorDetails.SecondaryIdentification &&
                this.payeeDetails.remittanceDetails &&
                this.payeeDetails.remittanceDetails.Reference) {
                this.payeeReference = this.payeeDetails.creditorDetails.SecondaryIdentification;
            }
            else if (this.payeeDetails.creditorDetails &&
                this.payeeDetails.creditorDetails.SecondaryIdentification) {
                this.payeeReference = this.payeeDetails.creditorDetails.SecondaryIdentification;
            }
            else if (this.payeeDetails.remittanceDetails &&
                this.payeeDetails.remittanceDetails.Reference &&
                this.payeeDetails.remittanceDetails.Unstructured) {
                this.payeeReference = this.payeeDetails.remittanceDetails.Reference;
            }
            else if (this.payeeDetails.remittanceDetails &&
                this.payeeDetails.remittanceDetails.Reference) {
                this.payeeReference = this.payeeDetails.remittanceDetails.Reference;
            }
            else if (this.payeeDetails.remittanceDetails &&
                this.payeeDetails.remittanceDetails.Unstructured) {
                this.payeeReference = null;
            }
            else {
                this.payeeReference = null;
            }
            if (this.payeeDetails && this.payeeDetails.amountDetails) {
                this.amount = this.payeeDetails.amountDetails.Amount;
                this.currency = this.payeeDetails.amountDetails.Currency;
            }
            if (this.payeeDetails.creditorDetails) {
                switch (this.payeeDetails.creditorDetails.SchemeName) {
                    case "UK.OBIE.SortCodeAccountNumber":
                        if (this.payeeDetails.creditorDetails.Identification) {
                            this.accountNo = this.payeeDetails.creditorDetails.Identification.substr(6);
                            this.bic = this.payeeDetails.creditorDetails.Identification.substr(0, 6);
                        }
                        break;
                    default:
                        if (this.payeeDetails.creditorDetails.Identification) {
                            this.accountNo = this.payeeDetails.creditorDetails.Identification;
                        }
                        break;
                }
            }
        }
        else {
            this.payeeDetails = null;
        }
    };
    PispConsentDetailsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-pisp-consent-details',
            template: __webpack_require__(/*! ./pisp-consent-details.component.html */ "./src/app/components/pisp-consent-details/pisp-consent-details.component.html"),
            styles: [__webpack_require__(/*! ./pisp-consent-details.component.css */ "./src/app/components/pisp-consent-details/pisp-consent-details.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_consent_detail_service_service__WEBPACK_IMPORTED_MODULE_2__["ConsentDetailServiceService"], src_app_services_static_content_fetch_service__WEBPACK_IMPORTED_MODULE_3__["StaticContentFetchService"]])
    ], PispConsentDetailsComponent);
    return PispConsentDetailsComponent;
}());



/***/ }),

/***/ "./src/app/components/username/username.component.css":
/*!************************************************************!*\
  !*** ./src/app/components/username/username.component.css ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".username-box .userInput:focus-within {\n  background-color: #fdfdc5 !important;\n}\n.username-box .userInput {\n  font-style: italic;\n  font-size: small;\n}\n.username-box .username-box-element {\n  margin: 2% auto;\n}\n.username-box .login-label{\n  font-weight: 700;\n}\n.error-message {\n  color: red;\n}\n.btn{\n  min-width: 100px;\n}\n.username-box .login-button {\n  background-color: #5d78ff;\n  color: floralwhite;\n  float: right;\n}\n.username-box .back-to-third-party-button {\n  background-color: rgba(93, 120, 255, 0.1);\n  float: left;\n  color: #3d4465;\n}\n/* @media screen and (min-height: 640px) {\n      .content {\n        padding: 33% 0;\n      }\n    } */\n@media screen and (max-width: 430px) {\n  .back-to-third-party-button,\n  .login-button {\n    width: 100%;\n  }\n}\n/* @media screen and (min-width: 768px) and (max-width: 810px) {\n  .back-to-third-party-button,\n  .login-button {\n    width: 100%;\n  }\n} */\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy91c2VybmFtZS91c2VybmFtZS5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0Usb0NBQW9DO0FBQ3RDO0FBQ0E7RUFDRSxrQkFBa0I7RUFDbEIsZ0JBQWdCO0FBQ2xCO0FBQ0E7RUFDRSxlQUFlO0FBQ2pCO0FBRUE7RUFDRSxnQkFBZ0I7QUFDbEI7QUFFQTtFQUNFLFVBQVU7QUFDWjtBQUVBO0VBQ0UsZ0JBQWdCO0FBQ2xCO0FBRUE7RUFDRSx5QkFBeUI7RUFDekIsa0JBQWtCO0VBQ2xCLFlBQVk7QUFDZDtBQUVBO0VBQ0UseUNBQXlDO0VBQ3pDLFdBQVc7RUFDWCxjQUFjO0FBQ2hCO0FBR0E7Ozs7T0FJTztBQUVQO0VBQ0U7O0lBRUUsV0FBVztFQUNiO0FBQ0Y7QUFFQTs7Ozs7R0FLRyIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvdXNlcm5hbWUvdXNlcm5hbWUuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi51c2VybmFtZS1ib3ggLnVzZXJJbnB1dDpmb2N1cy13aXRoaW4ge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmRmZGM1ICFpbXBvcnRhbnQ7XG59XG4udXNlcm5hbWUtYm94IC51c2VySW5wdXQge1xuICBmb250LXN0eWxlOiBpdGFsaWM7XG4gIGZvbnQtc2l6ZTogc21hbGw7XG59XG4udXNlcm5hbWUtYm94IC51c2VybmFtZS1ib3gtZWxlbWVudCB7XG4gIG1hcmdpbjogMiUgYXV0bztcbn1cblxuLnVzZXJuYW1lLWJveCAubG9naW4tbGFiZWx7XG4gIGZvbnQtd2VpZ2h0OiA3MDA7XG59XG5cbi5lcnJvci1tZXNzYWdlIHtcbiAgY29sb3I6IHJlZDtcbn1cblxuLmJ0bntcbiAgbWluLXdpZHRoOiAxMDBweDtcbn1cblxuLnVzZXJuYW1lLWJveCAubG9naW4tYnV0dG9uIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzVkNzhmZjtcbiAgY29sb3I6IGZsb3JhbHdoaXRlO1xuICBmbG9hdDogcmlnaHQ7XG59XG5cbi51c2VybmFtZS1ib3ggLmJhY2stdG8tdGhpcmQtcGFydHktYnV0dG9uIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogcmdiYSg5MywgMTIwLCAyNTUsIDAuMSk7XG4gIGZsb2F0OiBsZWZ0O1xuICBjb2xvcjogIzNkNDQ2NTtcbn1cblxuXG4vKiBAbWVkaWEgc2NyZWVuIGFuZCAobWluLWhlaWdodDogNjQwcHgpIHtcbiAgICAgIC5jb250ZW50IHtcbiAgICAgICAgcGFkZGluZzogMzMlIDA7XG4gICAgICB9XG4gICAgfSAqL1xuXG5AbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA0MzBweCkge1xuICAuYmFjay10by10aGlyZC1wYXJ0eS1idXR0b24sXG4gIC5sb2dpbi1idXR0b24ge1xuICAgIHdpZHRoOiAxMDAlO1xuICB9XG59XG5cbi8qIEBtZWRpYSBzY3JlZW4gYW5kIChtaW4td2lkdGg6IDc2OHB4KSBhbmQgKG1heC13aWR0aDogODEwcHgpIHtcbiAgLmJhY2stdG8tdGhpcmQtcGFydHktYnV0dG9uLFxuICAubG9naW4tYnV0dG9uIHtcbiAgICB3aWR0aDogMTAwJTtcbiAgfVxufSAqL1xuIl19 */"

/***/ }),

/***/ "./src/app/components/username/username.component.html":
/*!*************************************************************!*\
  !*** ./src/app/components/username/username.component.html ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<form #loginForm=\"ngForm\" method=\"POST\" action=\"login\" (ngSubmit)=\"userLogin()\" autocomplete=\"off\">\n  <div class=\"username-box\">\n    <label for=\"userTxt\" class=\"login-label username-box-element\">\n      {{this.fetchService.getStaticContent('LOGIN_FORM','LOGIN_HEADER')}}</label>\n      <div>\n        <span class=\"payment-section-label\">Software On Behalf Of: </span>\n        <span class=\"payment-section-value\" [innerHTML]='this.fetchService.getSoftwareOnBehalfOfOrg()'></span>\n        <p></p>\n      </div>\n    <input type=\"text\" [(ngModel)]=\"userName\" name=\"username\" id=\"userTxt\" class=\"userInput form-control username-box-element label-secondary\"\n      [placeholder]=\"this.fetchService.getStaticContent('LOGIN_FORM','USER_PLACEHOLDER_LABEL')\" required/>\n    <input type=\"password\" [(ngModel)]=\"password\" name=\"password\" id=\"userPwd\" class=\"userInput form-control username-box-element label-secondary\"\n      [placeholder]=\"this.fetchService.getStaticContent('LOGIN_FORM','PWD_PLACEHOLDER_LABEL')\" required/>\n    <span class='error-message username-box-element label-error-primary' *ngIf='errorFlag && !validateCredentials()'>{{this.errorService.errors['EMPTY_CREDENTIALS']}}</span>\n    <div class=\"button-group\">\n      <input type=\"button\" [value]=\"this.fetchService.getStaticContent('LOGIN_FORM','BUTTONS','BACK_TO_TPP_BUTTON_LABEL')\" class=\"back-to-third-party-button username-box-element btn bttn-secondary\" (click)=\"backToThirdParty()\"/>\n      <input type=\"submit\" id=\"login-btn\" [value]=\"this.fetchService.getStaticContent('LOGIN_FORM','BUTTONS','CONTINUE_BUTTON_LABEL')\" class=\"login-button username-box-element btn bttn-primary\" [disabled]=\"!loginForm.valid || !validateCredentials()\"/>\n    </div>\n  </div>\n </form>"

/***/ }),

/***/ "./src/app/components/username/username.component.ts":
/*!***********************************************************!*\
  !*** ./src/app/components/username/username.component.ts ***!
  \***********************************************************/
/*! exports provided: UsernameComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UsernameComponent", function() { return UsernameComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_static_content_fetch_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/static-content-fetch.service */ "./src/app/services/static-content-fetch.service.ts");
/* harmony import */ var _services_login_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/login.service */ "./src/app/services/login.service.ts");
/* harmony import */ var _services_error_handler_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/error-handler.service */ "./src/app/services/error-handler.service.ts");
/* harmony import */ var _services_shared_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../services/shared.service */ "./src/app/services/shared.service.ts");
/* harmony import */ var src_app_services_consent_detail_service_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/consent-detail-service.service */ "./src/app/services/consent-detail-service.service.ts");







var UsernameComponent = /** @class */ (function () {
    // @ViewChild('loginForm') loginFormToSubmit;
    function UsernameComponent(fetchService, login, errorService, modalService, consentService) {
        this.fetchService = fetchService;
        this.login = login;
        this.errorService = errorService;
        this.modalService = modalService;
        this.consentService = consentService;
        this.errorFlag = false;
    }
    UsernameComponent.prototype.ngOnInit = function () {
        // this.errorFlag=this.errorService.serverError.errorCode?true:false;
    };
    UsernameComponent.prototype.validateUsername = function () {
        if (this.userName) // if incorrect
            return true;
        else
            return false;
    };
    UsernameComponent.prototype.validateCredentials = function () {
        if (/^\s*$/.test(this.userName) || /^\s*$/.test(this.password))
            return false; // if incorrect
        else
            return true;
    };
    UsernameComponent.prototype.userLogin = function () {
        if (this.validateCredentials()) {
            this.errorFlag = false;
            var form = document.querySelector("form");
            var resumePath = document.querySelector('#resumePath');
            var corId = document.querySelector('#correlationId');
            var fsHeaders = document.querySelector('#fsHeaders');
            var psuAccounts = document.querySelector('#psuAccounts');
            var paymentSetup = document.querySelector('#paymentSetup');
            var channelId = document.querySelector('#channelId');
            var accountRequest = document.querySelector('#account-request');
            form.appendChild(resumePath);
            form.appendChild(corId);
            form.appendChild(channelId);
            form.appendChild(fsHeaders);
            form.appendChild(psuAccounts);
            form.appendChild(paymentSetup);
            form.appendChild(accountRequest);
            form.submit();
        }
        else {
            this.errorFlag = true;
        }
    };
    UsernameComponent.prototype.backToThirdParty = function () {
        var _this = this;
        this.modalService.emitShowLoadingSpinner(true);
        var authUrl = document.querySelector('#resumePath').value ? document.querySelector('#resumePath').value : null;
        var reqData = null;
        var serverErrorFlag = document.querySelector('#serverErrorFlag').value ? document.querySelector('#serverErrorFlag').value : null;
        var correlationId = document.querySelector('#correlationId').value ? document.querySelector('#correlationId').value : null;
        var channelId = document.querySelector('#channelId').value ? document.querySelector('#channelId').value : null;
        correlationId = (correlationId) ? correlationId : null;
        this.login.rejectRequest(reqData, authUrl, serverErrorFlag, correlationId, channelId).subscribe(function (res) {
            if (res.status == 200) {
                window.location.href = res.body.model.redirectUri;
            }
        }, function (errorResponse) {
            if (errorResponse.error && errorResponse.error.exception && (typeof errorResponse.error.exception === 'string' || errorResponse.error.exception instanceof String)) {
                if (errorResponse.error.exception.indexOf("errorCode") != -1) {
                    errorResponse.error.exception = JSON.parse(errorResponse.error.exception);
                }
            }
            if (errorResponse.error.exception.errorCode === "731") {
                _this.redirectUri = errorResponse.error.redirectUri;
                // code for session modal popup invocation
                _this.modalService.emitShowLoadingSpinner(false);
                _this.modalService.emitEnableModalPopup({ enableModal: true, redirectUri: _this.redirectUri });
            }
            else {
                var errorCode = errorResponse.error.exception ? errorResponse.error.exception.errorCode : "800";
                _this.errorService.setError(errorCode);
                _this.modalService.emitShowLoadingSpinner(false);
            }
        });
    };
    UsernameComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-username',
            template: __webpack_require__(/*! ./username.component.html */ "./src/app/components/username/username.component.html"),
            styles: [__webpack_require__(/*! ./username.component.css */ "./src/app/components/username/username.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_static_content_fetch_service__WEBPACK_IMPORTED_MODULE_2__["StaticContentFetchService"], _services_login_service__WEBPACK_IMPORTED_MODULE_3__["LoginService"], _services_error_handler_service__WEBPACK_IMPORTED_MODULE_4__["ErrorHandlerService"],
            _services_shared_service__WEBPACK_IMPORTED_MODULE_5__["SharedService"], src_app_services_consent_detail_service_service__WEBPACK_IMPORTED_MODULE_6__["ConsentDetailServiceService"]])
    ], UsernameComponent);
    return UsernameComponent;
}());



/***/ }),

/***/ "./src/app/pipes/account-digits-filter.pipe.ts":
/*!*****************************************************!*\
  !*** ./src/app/pipes/account-digits-filter.pipe.ts ***!
  \*****************************************************/
/*! exports provided: AccountDigitsFilterPipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AccountDigitsFilterPipe", function() { return AccountDigitsFilterPipe; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var AccountDigitsFilterPipe = /** @class */ (function () {
    function AccountDigitsFilterPipe() {
    }
    AccountDigitsFilterPipe.prototype.transform = function (value) {
        return " ~" + value.slice(-4);
    };
    AccountDigitsFilterPipe = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Pipe"])({
            name: 'accountDigitsFilter'
        })
    ], AccountDigitsFilterPipe);
    return AccountDigitsFilterPipe;
}());



/***/ }),

/***/ "./src/app/services/consent-detail-service.service.ts":
/*!************************************************************!*\
  !*** ./src/app/services/consent-detail-service.service.ts ***!
  \************************************************************/
/*! exports provided: ConsentDetailServiceService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConsentDetailServiceService", function() { return ConsentDetailServiceService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var ConsentDetailServiceService = /** @class */ (function () {
    function ConsentDetailServiceService() {
    }
    ConsentDetailServiceService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], ConsentDetailServiceService);
    return ConsentDetailServiceService;
}());



/***/ }),

/***/ "./src/app/services/error-handler.service.ts":
/*!***************************************************!*\
  !*** ./src/app/services/error-handler.service.ts ***!
  \***************************************************/
/*! exports provided: ErrorHandlerService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ErrorHandlerService", function() { return ErrorHandlerService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var ErrorHandlerService = /** @class */ (function () {
    function ErrorHandlerService() {
        this.serverError = { type: "", errorCode: "", errorMessage: "", errorDetails: "" };
        this.errorArr = [];
        this.redirectUri = "";
    }
    ErrorHandlerService.prototype.setErrors = function (errors) {
        this.errors = errors;
    };
    ErrorHandlerService.prototype.setError = function (error) {
        if (typeof error == "string") {
            if (this.errors[error]) {
                this.serverError = {
                    type: this.errors["ERROR"],
                    errorCode: error,
                    errorMessage: this.errors[error],
                    errorDetails: this.errors[error + "_DETAILS"]
                };
            }
            else {
                this.serverError = {
                    type: this.errors["ERROR"],
                    errorCode: error,
                    errorMessage: this.errors["GENERIC_ERROR"],
                    errorDetails: this.errors["GENERIC_ERROR_DETAILS"]
                };
            }
        }
        else {
            if (this.errors[error.errorCode]) {
                this.serverError = {
                    type: this.errors["ERROR"],
                    errorCode: error.errorCode,
                    errorMessage: this.errors[error.errorCode],
                    errorDetails: this.errors[error.errorCode + "_DETAILS"]
                };
            }
            else if (error['errorCode']) {
                this.serverError = {
                    type: this.errors["ERROR"],
                    errorCode: error["errorCode"],
                    errorMessage: error["errorMessage"],
                    errorDetails: error["errorDetails"]
                };
                if (error["errorDetails"] == "null")
                    this.serverError.errorDetails = "";
            }
            else {
                this.serverError = {
                    type: this.errors["ERROR"],
                    errorCode: "800",
                    errorMessage: this.errors["GENERIC_ERROR"],
                    errorDetails: this.errors["GENERIC_ERROR_DETAILS"]
                };
                if (error["errorDetails"] == "null")
                    this.serverError.errorDetails = "";
            }
        }
        this.errorArr.push(this.serverError);
    };
    ErrorHandlerService.prototype.handleError = function () {
        if (this.serverError.errorCode)
            return this.serverError;
    };
    ErrorHandlerService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: "root"
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], ErrorHandlerService);
    return ErrorHandlerService;
}());



/***/ }),

/***/ "./src/app/services/login.service.ts":
/*!*******************************************!*\
  !*** ./src/app/services/login.service.ts ***!
  \*******************************************/
/*! exports provided: LoginService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginService", function() { return LoginService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");



var LoginService = /** @class */ (function () {
    function LoginService(http) {
        this.http = http;
    }
    LoginService.prototype.validateCredentials = function (userName) {
    };
    LoginService.prototype.rejectRequest = function (reqData, authUrl, serverErrorFlag, correlationId, channelId) {
        return this.http.put("./cancel?oAuthUrl=" + authUrl + "&serverErrorFlag=" + serverErrorFlag + "&correlationId=" + correlationId + "&channelId=" + channelId, reqData, { observe: "response" });
        // return  this.http.put(`https://obuitest.free.beeceptor.com/cancel?oAuthUrl=${authUrl}&serverErrorFlag=${serverErrorFlag}&correlationId=${correlationId}`
        // ,reqData,{observe:"response"});
    };
    LoginService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], LoginService);
    return LoginService;
}());



/***/ }),

/***/ "./src/app/services/shared.service.ts":
/*!********************************************!*\
  !*** ./src/app/services/shared.service.ts ***!
  \********************************************/
/*! exports provided: SharedService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SharedService", function() { return SharedService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");



var SharedService = /** @class */ (function () {
    function SharedService() {
        // observable sources
        this.enableModalPopupSource = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        this.showLoadingSpinnerSource = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        // observables
        this.enableModalPopup$ = this.enableModalPopupSource.asObservable();
        this.showLoadingSpinner$ = this.showLoadingSpinnerSource.asObservable();
    }
    // emitter functions
    SharedService.prototype.emitEnableModalPopup = function (change) {
        this.enableModalPopupSource.next(change);
    };
    SharedService.prototype.emitShowLoadingSpinner = function (change) {
        this.showLoadingSpinnerSource.next(change);
    };
    SharedService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: "root"
        })
    ], SharedService);
    return SharedService;
}());



/***/ }),

/***/ "./src/app/services/static-content-fetch.service.ts":
/*!**********************************************************!*\
  !*** ./src/app/services/static-content-fetch.service.ts ***!
  \**********************************************************/
/*! exports provided: StaticContentFetchService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StaticContentFetchService", function() { return StaticContentFetchService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");



var StaticContentFetchService = /** @class */ (function () {
    function StaticContentFetchService(http) {
        this.http = http;
    }
    StaticContentFetchService.prototype.setStaticContent = function (staticContent) {
        this.staticContent = staticContent;
    };
    StaticContentFetchService.prototype.getStaticContent = function () {
        var keys = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            keys[_i] = arguments[_i];
        }
        var i = 0;
        var keyReqd = this.staticContent;
        while (i < keys.length) {
            keyReqd = keyReqd[keys[i]];
            i++;
        }
        return keyReqd;
    };
    StaticContentFetchService.prototype.setCdnBaseUrl = function (baseUrl) {
        this.cdnBaseUrl = baseUrl;
    };
    StaticContentFetchService.prototype.setSoftwareOnBehalfOfOrg = function (id) {
        this.softwareOnBehalfOfOrg = id;
    };
    StaticContentFetchService.prototype.getSoftwareOnBehalfOfOrg = function () {
        return this.softwareOnBehalfOfOrg;
    };
    StaticContentFetchService.prototype.getCdnBaseUrl = function () {
        return this.cdnBaseUrl;
    };
    StaticContentFetchService.prototype.setbuildversion = function (buildversion) {
        this.buildversion = buildversion;
    };
    StaticContentFetchService.prototype.getbuildversion = function () {
        return this.buildversion;
    };
    StaticContentFetchService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: "root"
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], StaticContentFetchService);
    return StaticContentFetchService;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /var/lib/jenkins/workspace/PSD2-cma2-Dev/Saas-mcr-0.7.1-CloudFront-UI-CI/sca/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map