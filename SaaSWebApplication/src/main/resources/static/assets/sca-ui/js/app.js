"use strict";
var consentApp = angular.module("loginApp", ["ui.router", "ui.bootstrap", "ngSanitize", "blockUI", "pascalprecht.translate", "ngCookies", "fraudAnalyzer", "loginPartials"]);

consentApp.value("envVariables", {
    blockChain: false
});

consentApp.run(["$rootScope", "config", "$timeout", "$window", "$fraudAnalyze", function ($rootScope, config, $timeout, $window, $fraudAnalyze) {
    $rootScope.defaultCssTheme = config.BankOfIrelandCssTheme;
    $rootScope.$on("$stateChangeSuccess", function () {
        $timeout(function () {
            document.body.scrollTop = document.documentElement.scrollTop = 0;
        }, 100);
    });
    $fraudAnalyze.loaded.then(function () {
        $fraudAnalyze.init = function () {
            if (!$("#boiukppm").length) {
                $("body").append("<input type='hidden' id='boiukppm' name='boiukppm'>");
                $("body").append("<input type='hidden' id='boiukprefs2' name='boiukprefs2'>");
                var fraudHdmInfoData = angular.fromJson($("#fraudHdmInfo").val());
                var args = [];
                args[0] = decodeURIComponent(fraudHdmInfoData.resUrl); // using decodeURIComponent() for decode the url
                args[1] = decodeURIComponent(fraudHdmInfoData.hdmUrl);
                args[2] = fraudHdmInfoData.hdmInputName;
                $window.boiukns.boiukfn("boiukppm", args);
            }
        };
        $fraudAnalyze.capture = function (dateTime) {
            $window.boiukns.boiukcfn("boiukppm");
            var $form = $("#loginForm");
            $("#boiukppm").attr("name", "device-jsc").appendTo($form);
            $("#boiukprefs2").attr("name", "device-hdim-payload").appendTo($form);
            var $evtTime = $("<input type='hidden' id='event-time' name='event-time'>");
            $evtTime.val(dateTime);
            $evtTime.appendTo($form);
        };
    });    
    $rootScope.cdnBaseURL = $("#cdnBaseURL").val();
}]);

consentApp.config(["$stateProvider", "$urlRouterProvider", "$httpProvider", "blockUIConfig", "$translateProvider",
    "$translatePartialLoaderProvider", "config", "$qProvider", "$fraudAnalyzeProvider",
    function ($stateProvider, $urlRouterProvider, $httpProvider, blockUIConfig, $translateProvider, $translatePartialLoaderProvider, config, $qProvider, $fraudAnalyzeProvider) {

        var staticContent = angular.fromJson($("#staticContent").val());
        $translatePartialLoaderProvider.addPart("index");
        $translateProvider.translations("en", staticContent);
        $translateProvider.preferredLanguage("en");
        $translateProvider.fallbackLanguage("en");
        $translateProvider.useSanitizeValueStrategy("sceParameters");

        blockUIConfig.templateUrl = "/views/loading-spinner.html";
        $qProvider.errorOnUnhandledRejections(false);

        var $jscFilePath = $("#jsc-file-path").val();
        if ($jscFilePath && $jscFilePath.length) {
            $fraudAnalyzeProvider.setJsCollectors([$jscFilePath]);
        }

        $httpProvider.interceptors.push(["$q", "blockUI", "$timeout", function ($q, blockUI, $timeout) {
            return {
                request: function (config) {
                    blockUI.start();
                    return config;
                },
                requestError: function (rejection) {
                    blockUI.stop();
                    return $q.reject(rejection);
                },
                response: function (response) {
                    blockUI.stop();
                    return response;
                },
                responseError: function (rejection) {
                    blockUI.stop();
                    return $q.reject(rejection);
                }
            };
        }]);
        $urlRouterProvider.otherwise("/");
        $stateProvider.state("login", {
            url: "/",
            templateUrl: "/views/login.html",
            controller: "loginCtrl",
            resolve: {
                fontResolved: ["fontloaderService", function (fontloaderService) {
                    return fontloaderService.isFontLoad();
                }]
            }
        });
        $stateProvider.state("login.modal", {
            views: {
                "modalContainer@": {
                    templateUrl: "/views/modal.html"
                }
            },
            onEnter: ["$state", function ($state) {
                // Hitting the ESC key closes the modal
                $(document).on("keyup", function (e) {
                    if (e.keyCode === 27) {
                        $(document).off("keyup");
                        $state.go("login");
                    }
                });

                // Clicking outside of the modal closes it
                $(document).on("click", ".Modal-backdrop, .Modal-holder", function () {
                    $state.go("login");
                });

                // Clickin on the modal or it"s contents doesn"t cause it to close
                $(document).on("click", ".Modal-box, .Modal-box *", function (e) {
                    e.stopPropagation();
                });
            }],
            params: {
                content: null
            },
            abstract: true
        });

        $stateProvider.state("login.modal.modal-1", {
            views: {
                "modal@": {
                    templateUrl: "/views/modal-1.html",
                    controller: "lightboxCtrl"
                }
            }
        });

        $stateProvider.state("login.modal.modal-2", {
            views: {
                "modal@": {
                    templateUrl: "/views/modal-2.html",
                    controller: "lightboxCtrl"
                }
            }
        });

        $stateProvider.state("login.modal.modal-3", {
            views: {
                "modal@": {
                    templateUrl: "/views/modal-3.html",
                    controller: "lightboxCtrl"
                }
            }
        });
    }
]);

"use strict";
consentApp.constant("config", {
    lang: "en",
    BankOfIrelandCssTheme: "bank-of-ireland.css",
    "365Online": "https://www.365online.com",
    modelpopupConfig: {
        open: function() { /**/ },
        "modelpopupTitle": "Title",
        "modelpopupBodyContent": "Content",
        "btn": {
            "okbtn": {
                "visible": true,
                "label": "",
                "action": null
            },
            "cancelbtn": {
                "visible": true,
                "label": "",
                "action": null
            }
        }
    }
});

"use strict";
angular.module("loginApp").controller("loginCtrl", ["$rootScope", "$scope", "LoginService",
    "config", "$translate", "blockUI", "$uibModal", "$timeout", "$state", "$window", "$cookies", "$fraudAnalyze",
    function ($rootScope, $scope, LoginService, config, $translate, blockUI, $uibModal, $timeout, $state, $window, $cookies, $fraudAnalyze, fontResolved) {

        var $ = window.jQuery;
        $scope.errorData = null;
        $scope.maintenanceAlertData = null;
        $scope.alertData = null;
        $scope.placement = {
            selected: "right"
        };
        $scope.userId = "";
        $scope.passcode = "";
        $scope.redirectUri = "";
        $scope.mntError = "";
        $scope.alertErrorFlag = "";

        $scope.init = function () {
            $fraudAnalyze.loaded.then(function () {
                $fraudAnalyze.init();
            });
            $translate(["ERROR_MESSAGES.SHOW_ALERT", "ERROR_MESSAGES.SHOW_NOTICE",
                "HEADER.LOGOURL", "HEADER.BANK_LOGO_IMAGE_ALTERNET_LABEL",
                "CHANNEL_ID", "BRAND_ID",
                "HEADER.ACCOUNT_ACCESS_LABEL",
                "HEADER.THIRD_PARTY_LABEL", "LOGIN_FORM.LOGIN_HEADER", "LOGIN_FORM.USER_LABEL",
                "LOGIN_FORM.USER_PASSWORD_LABEL", "LOGIN_FORM.NOTE_LABEL",
                "LOGIN_FORM.NOTE_DESCRIPTION", "LOGIN_FORM.NEED_HELP_LABEL",
                "LOGIN_FORM.BUTTONS.BACK_TO_TPP_BUTTON_LABEL",
                "LOGIN_FORM.BUTTONS.CONTINUE_BUTTON_LABEL",
                "LOGIN_INFORMATION.FURTHER_INFORMATION_HEADER",
                "LOGIN_INFORMATION.BOI_KEYCODE_HEADER",
                "LOGIN_INFORMATION.BOI_KEYCODE_LABEL", "LOGIN_FORM.USER_PLACEHOLDER_LABEL",
                "LOGIN_FORM.USER_TOOLTIP_MESSAGE", "LOGIN_FORM.PASSWORD_PLACEHOLDER_LABEL",
                "LOGIN_FORM.USER_TOOLTIP_LABEL", "LOGIN_FORM.PASSWORD_TOOLTIP_MESSAGE",
                "LOGIN_FORM.HELP_URL", "LOGIN_FORM.PASSWORD_TOOLTIP_LABEL",
                "LOGIN_INFORMATION.LOGIN_LINK_URL",
                "LOGIN_INFORMATION.KEYCODE_IMAGE_ALTERNATE_LABEL",
                "LOGIN_INFORMATION.KEYCODE_HELP_LABEL",
                "LOGIN_INFORMATION.REGISTER_KEYCODE_HEADER", "LOGIN_INFORMATION.STEP_1_LABEL",
                "LOGIN_INFORMATION.STEP_1_DESCRIPTION", "LOGIN_INFORMATION.STEP_2_LABEL",
                "LOGIN_INFORMATION.STEP_2_DESCRIPTION", "LOGIN_INFORMATION.STEP_3_LABEL",
                "LOGIN_INFORMATION.STEP_3_DESCRIPTION",
                "LOGIN_INFORMATION.NOTE_LABEL", "LOGIN_INFORMATION.NOTE_DESCRIPTION",
                "LOGIN_INFORMATION.LOGIN_LINK_LABEL", "FOOTER.ABOUT_US_LABEL",
                "FOOTER.PRIVACY_POLICY_LABEL", "FOOTER.TNC_LABEL", "FOOTER.HELP_LABEL",
                "FOOTER.REGULATORY_LABEL", "SESSION_TIMEOUT_POPUP.SESSION_TIMEOUT_POPUP_MESSAGE",
                "SESSION_TIMEOUT_POPUP.SESSION_POPOUT_BUTTON_LABEL",
                "LIGHTBOX1.MESSAGE", "LIGHTBOX1.I_DONT_HAVE_BOI_KEYCODE_BUTTON_LABEL",
                "LIGHTBOX1.PRESS_HERE_WILL_CLOSE_POPUP_SCREENREADER_LABEL",
                "LIGHTBOX1.PRESS_HERE_WILL_OPEN_POPUP_SCREENREADER_LABEL",
                "LIGHTBOX1.CLOSE_IMAGE_ALTERNET_LABEL",
                "LIGHTBOX1.POPUP_DISPLAYED_SCREENREADER_LABEL",
                "LIGHTBOX1.I_HAVE_BOI_KEYCODE_BUTTON_LABEL",
                "LIGHTBOX2.MESSAGE", "LIGHTBOX2.CLOSE_IMAGE_ALTERNET_LABEL",
                "LIGHTBOX2.POPUP_DISPLAYED_SCREENREADER_LABEL",
                "LIGHTBOX2.I_DONT_WANT_TO_CONTINUE_LABEL",
                "LIGHTBOX2.PRESS_HERE_WILL_REDIRECT_TPP_SCREENREADER_LABEL",
                "LIGHTBOX2.PRESS_HERE_WILL_OPEN_REGISTER_WINDOW_SCREENREADER_LABEL",
                "LIGHTBOX2.PRESS_HERE_WILL_CLOSE_POPUP_SCREENREADER_LABEL",
                "LIGHTBOX2.RETURN_TO_THIRD_PARTY_BUTTON_LABEL",
                "LIGHTBOX2.I_WANT_TO_REGISTER_KEYCODE_LABEL",
                "LIGHTBOX2.REGISTER_KEYCODE_BUTTON_LABAEL",
                "LIGHTBOX2.I_HAVE_PREVIOUSLY_REGISTER_KEYCODE_LABEL",
                "LIGHTBOX2.CONTINUE_WITH_LOGIN_BUTTON_LABEL",
                "LIGHTBOX3.MESSAGE1", "LIGHTBOX3.MESSAGE2", "LIGHTBOX3.MESSAGE3",
                "LIGHTBOX3.CLOSE_IMAGE_ALTERNET_LABEL",
                "LIGHTBOX3.PRESS_HERE_WILL_BACK_TO_OPTIONS_SCREENREADER_LABEL",
                "LIGHTBOX3.PRESS_HERE_WILL_REDIRECT_TO_LOGIN_SCREENREADER_LABEL",
                "LIGHTBOX3.BACK_TO_OPTIONS_BUTTON_LABEL",
                "LIGHTBOX3.LOGIN_TO_365_ONLINE_BUTTON_LABEL",
                "OPTION1", "OPTION2", "OPTION3", "BUTTON",
                "FOOTER.ABOUT_US_URL", "FOOTER.PRIVACY_POLICY_URL", "FOOTER.TNC_URL",
                "FOOTER.HELP_URL"
            ]).then(function (translations) {
                $scope.alertErrorFlag = translations["ERROR_MESSAGES.SHOW_ALERT"];
                $scope.mntError = translations["ERROR_MESSAGES.SHOW_NOTICE"];
                $scope.logoPath = translations["HEADER.LOGOURL"];
                $scope.logoAltLabel = translations["HEADER.BANK_LOGO_IMAGE_ALTERNET_LABEL"];
                $scope.userPlaceHolderLabel = translations["LOGIN_FORM.USER_PLACEHOLDER_LABEL"];
                $scope.userTooltipMsg = translations["LOGIN_FORM.USER_TOOLTIP_MESSAGE"];
                $scope.userTooltipLbl = translations["LOGIN_FORM.USER_TOOLTIP_LABEL"];
                $scope.passwordPlaceHolderLabel = translations["LOGIN_FORM.PASSWORD_PLACEHOLDER_LABEL"];
                $scope.passwordTooltipMsg = translations["LOGIN_FORM.PASSWORD_TOOLTIP_MESSAGE"];
                $scope.passwordTooltipLbl = translations["LOGIN_FORM.PASSWORD_TOOLTIP_LABEL"];
                $scope.loginHelpUrl = translations["LOGIN_FORM.HELP_URL"];
                $scope.login365Url = translations["LOGIN_INFORMATION.LOGIN_LINK_URL"];
                $scope.aboutUsUrl = translations["FOOTER.ABOUT_US_URL"];
                $scope.privacyPolicyUrl = translations["FOOTER.PRIVACY_POLICY_URL"];
                $scope.tncUrl = translations["FOOTER.TNC_URL"];
                $scope.helpUrl = translations["FOOTER.HELP_URL"];
                $scope.keyImageAlt = translations["LOGIN_INFORMATION.KEYCODE_IMAGE_ALTERNATE_LABEL"];

                $("#channelId").val(translations["CHANNEL_ID"]);
                $("#brandId").val(translations["BRAND_ID"]);
                $scope.accountAccessText = translations["HEADER.ACCOUNT_ACCESS_LABEL"];
                $scope.thirdPartyText = translations["HEADER.THIRD_PARTY_LABEL"];
                $scope.loginHeaderText = translations["LOGIN_FORM.LOGIN_HEADER"];
                $scope.userText = translations["LOGIN_FORM.USER_LABEL"];
                $scope.passwordText = translations["LOGIN_FORM.USER_PASSWORD_LABEL"];
                $scope.noteTitleText = translations["LOGIN_FORM.NOTE_LABEL"];
                $scope.loginNoteDescText = translations["LOGIN_FORM.NOTE_DESCRIPTION"];
                $scope.needHelpText = translations["LOGIN_FORM.NEED_HELP_LABEL"];
                $scope.backtoTPPText = translations["LOGIN_FORM.BUTTONS.BACK_TO_TPP_BUTTON_LABEL"];
                $scope.continueText = translations["LOGIN_FORM.BUTTONS.CONTINUE_BUTTON_LABEL"];
                $scope.furtherText = translations["LOGIN_INFORMATION.FURTHER_INFORMATION_HEADER"];
                $scope.boikeycodeText = translations["LOGIN_INFORMATION.BOI_KEYCODE_HEADER"];
                $scope.boikeycodeAppText = translations["LOGIN_INFORMATION.BOI_KEYCODE_LABEL"];
                $scope.boikeycodeHelpText = translations["LOGIN_INFORMATION.KEYCODE_HELP_LABEL"];
                $scope.registreKeyCodeText = translations["LOGIN_INFORMATION.REGISTER_KEYCODE_HEADER"];
                $scope.step1Text = translations["LOGIN_INFORMATION.STEP_1_LABEL"];
                $scope.step1DescText = translations["LOGIN_INFORMATION.STEP_1_DESCRIPTION"];
                $scope.step2Text = translations["LOGIN_INFORMATION.STEP_2_LABEL"];
                $scope.step2DescText = translations["LOGIN_INFORMATION.STEP_2_DESCRIPTION"];
                $scope.step3Text = translations["LOGIN_INFORMATION.STEP_3_LABEL"];
                $scope.step3DescText = translations["LOGIN_INFORMATION.STEP_3_DESCRIPTION"];
                $scope.noteInfoText = translations["LOGIN_INFORMATION.NOTE_LABEL"];
                $scope.noteDescText = translations["LOGIN_INFORMATION.NOTE_DESCRIPTION"];
                $scope.loginLinkText = translations["LOGIN_INFORMATION.LOGIN_LINK_LABEL"];
                $scope.aboutUsText = translations["FOOTER.ABOUT_US_LABEL"];
                $scope.cookieText = translations["FOOTER.PRIVACY_POLICY_LABEL"];
                $scope.tncText = translations["FOOTER.TNC_LABEL"];
                $scope.helpText = translations["FOOTER.HELP_LABEL"];
                $scope.regulatoryText = translations["FOOTER.REGULATORY_LABEL"];
                $scope.sessionDescText = translations["SESSION_TIMEOUT_POPUP.SESSION_TIMEOUT_POPUP_MESSAGE"];
                $scope.sessionPopupBtnText = translations["SESSION_TIMEOUT_POPUP.SESSION_POPOUT_BUTTON_LABEL"];



                $scope.modelPopUpConf.lightBox1Description = translations["LIGHTBOX1.MESSAGE"];
                $scope.modelPopUpConf.lightBox1IdontHaveBoiKeycodeApp = translations["LIGHTBOX1.I_DONT_HAVE_BOI_KEYCODE_BUTTON_LABEL"];
                $scope.modelPopUpConf.lightBox1HaveBoiKeycodeApp = translations["LIGHTBOX1.I_HAVE_BOI_KEYCODE_BUTTON_LABEL"];
                $scope.modelPopUpConf.lightbox1PopUpDisplayed = translations["LIGHTBOX1.POPUP_DISPLAYED_SCREENREADER_LABEL"];
                $scope.modelPopUpConf.lightbox1CloseImageAlt = translations["LIGHTBOX1.CLOSE_IMAGE_ALTERNET_LABEL"];
                $scope.lightbox1ClosePopUp = translations["LIGHTBOX1.PRESS_HERE_WILL_CLOSE_POPUP_SCREENREADER_LABEL"];
                $scope.openPopUp = translations["LIGHTBOX1.PRESS_HERE_WILL_OPEN_POPUP_SCREENREADER_LABEL"];


                // Light BOX 2
                $scope.lightbox2ClosePopUp = translations["LIGHTBOX1.PRESS_HERE_WILL_CLOSE_POPUP_SCREENREADER_LABEL"];
                $scope.modelPopUpConf.lightbox2PopUpDisplayed = translations["LIGHTBOX2.POPUP_DISPLAYED_SCREENREADER_LABEL"];
                $scope.modelPopUpConf.lightbox2CloseImageAlt = translations["LIGHTBOX2.CLOSE_IMAGE_ALTERNET_LABEL"];
                $scope.redirectToTpp = translations["LIGHTBOX2.PRESS_HERE_WILL_REDIRECT_TPP_SCREENREADER_LABEL"];
                $scope.openRegisterWindow = translations["LIGHTBOX2.PRESS_HERE_WILL_OPEN_REGISTER_WINDOW_SCREENREADER_LABEL"];

                $scope.modelPopUpConf.lightBox2Description = translations["LIGHTBOX2.MESSAGE"];
                $scope.modelPopUpConf.lightBox2IdontWantToContinue = translations["LIGHTBOX2.I_DONT_WANT_TO_CONTINUE_LABEL"];
                $scope.modelPopUpConf.lightBox2ReturnToThirdParty = translations["LIGHTBOX2.RETURN_TO_THIRD_PARTY_BUTTON_LABEL"];
                $scope.modelPopUpConf.lightBox2IwantToRegisterKeycodeApp = translations["LIGHTBOX2.I_WANT_TO_REGISTER_KEYCODE_LABEL"];
                $scope.modelPopUpConf.lightBox2RegisterKeycodeApp = translations["LIGHTBOX2.REGISTER_KEYCODE_BUTTON_LABAEL"];
                $scope.modelPopUpConf.lightBox2IhavePreviouslyRegisterKeycodeApp = translations["LIGHTBOX2.I_HAVE_PREVIOUSLY_REGISTER_KEYCODE_LABEL"];
                $scope.modelPopUpConf.lightBox2ContinueWithLogin = translations["LIGHTBOX2.CONTINUE_WITH_LOGIN_BUTTON_LABEL"];
                $scope.modelPopUpConf.optionLabel1 = translations["OPTION1"];
                $scope.modelPopUpConf.optionLabel2 = translations["OPTION2"];
                $scope.modelPopUpConf.optionLabel3 = translations["OPTION3"];

                // Light BOX 3
                $scope.modelPopUpConf.lightBox3Description1 = translations["LIGHTBOX3.MESSAGE1"];
                $scope.modelPopUpConf.lightBox3Description2 = translations["LIGHTBOX3.MESSAGE2"];
                $scope.modelPopUpConf.lightBox3Description3 = translations["LIGHTBOX3.MESSAGE3"];
                $scope.modelPopUpConf.lightBox3BackToOptions = translations["LIGHTBOX3.BACK_TO_OPTIONS_BUTTON_LABEL"];
                $scope.modelPopUpConf.lightBox3LoginTo365Online = translations["LIGHTBOX3.LOGIN_TO_365_ONLINE_BUTTON_LABEL"];
                $scope.modelPopUpConf.button = translations["BUTTON"];
                $scope.backToOption = translations["LIGHTBOX3.PRESS_HERE_WILL_BACK_TO_OPTIONS_SCREENREADER_LABEL"];
                $scope.redirectToLogin = translations["LIGHTBOX3.PRESS_HERE_WILL_REDIRECT_TO_LOGIN_SCREENREADER_LABEL"];
                $scope.lightbox3ClosePopUp = translations["LIGHTBOX3.PRESS_HERE_WILL_CLOSE_POPUP_SCREENREADER_LABEL"];
                $scope.modelPopUpConf.lightbox3CloseImageAlt = translations["LIGHTBOX3.CLOSE_IMAGE_ALTERNET_LABEL"];
            });

            try {

                // maintenance Alert show or hide code.................
                $timeout(function () {
                    if ($scope.alertErrorFlag === "true") {
                        $scope.alertData = {};
                        $scope.alertData.errorCode = "ALERT_MESSAGE";
                        $scope.alertData.errorCSSClass = "alert";
                        $scope.alertData.prefix = "alert";

                    }
                    if ($scope.mntError === "true") {
                        $scope.maintenanceAlertData = {};
                        $scope.maintenanceAlertData.errorCode = "NOTICE_MESSAGE";
                        $scope.maintenanceAlertData.errorCSSClass = "warning";
                        $scope.maintenanceAlertData.prefix = "notice";
                    }
                });


                if (!navigator.cookieEnabled) {
                    $scope.createCustomErr({ "errorCode": "COOKIE_ENABLE_MSG" });
                } else {
                    var secError = $("#sec-error") ? $("#sec-error").val() : null;
                    var error = $("#error") ? $("#error").val() : null;
                    error = secError ? angular.fromJson(secError) : (error ? angular.fromJson(error) : null);

                    if (error) {
                        if (error.errorCode === "731") {
                            $scope.redirectUri = $("#resumePath").val();
                            $timeout(function () {
                                $scope.sessionOut();
                            });
                        } else {
                            $scope.createCustomErr(error);
                        }
                    } else {

                        $scope.modelPopUpConf = config.modelpopupConfig;
                        $state.go("login.modal.modal-1", { content: $scope.modelPopUpConf });
                    }
                }
            }
            catch (e) {
                $scope.createCustomErr({ "errorCode": "999" });
            }
        };

        $scope.createCustomErr = function (error) {
            $scope.errorData = {};
            $scope.errorData.errorCode = error ? error.errorCode : "800";
            $scope.errorData.correlationId = error ? error.correlationId : null;
            $scope.errorData.errorCSSClass = error ? error.errorCSSClass : null;
        };

        $scope.loginAction = function () {
            var today = new Date();
            var dateTime = today.toISOString();
            try {
                if ($scope.formValidation()) {
                    blockUI.start();
                    var $form = $("#loginForm");
                    $("#resumePath").appendTo($form);
                    $("#correlationId").appendTo($form);
                    $("#channelId").appendTo($form);
                    $("#fsHeaders").appendTo($form);
                    if ($window.boiukns) {
                        $fraudAnalyze.capture(dateTime);
                    }
                    $($form).submit();
                }
            } catch (e) {
                $scope.createCustomErr({ "errorCode": "999" });
            }
        };

        $scope.formValidation = function () {
            if (($scope.userId.length === 0) || ($scope.passcode.length === 0)) {
                $scope.createCustomErr({ "errorCode": "EMPTY_CREDENTIALS" });
                return false;
            } else {
                if (!isNaN($scope.userId) && !isNaN($scope.passcode)) {
                    if (($scope.userId.length === 8 || $scope.userId.length === 6) && ($scope.passcode.length === 6)) {
                        return true;
                    } else {
                        $scope.createCustomErr({ "errorCode": "INVALID_CREDENTIALS" });
                        return false;
                    }
                } else {
                    $scope.createCustomErr({ "errorCode": "INVALID_CREDENTIALS" });
                    return false;
                }

            }

        };

        $scope.sessionOut = function () {
            $scope.modalInstance = $uibModal.open({
                templateUrl: "/views/session-out-popup.html",
                animation: true,
                backdrop: false,
                keyboard: false,
                windowClass: "pop-confirm",
                scope: $scope,
                resolve: {
                    items: function () {
                        return $scope.items;
                    }
                },
                controller: ["$scope", "$uibModalInstance", function ($scope, $uibModalInstance) {
                    $scope.redirectTPP = function () {
                        $uibModalInstance.dismiss("cancel");
                        $window.location.href = $scope.redirectUri;
                    };

                }]
            });
        };

        $scope.backToThirdParty = function () {
            var authUrl = $("#resumePath").val();
            var reqData = null;
            var channelId = $("#channelId").val();
            var serverErrorFlag = $("#serverErrorFlag").val();
            var correlationId = $("#correlationId") ? $("#correlationId").val() : null;
            LoginService.rejectRequest(reqData, authUrl, serverErrorFlag, correlationId, channelId).then(function (resp) {
                if (resp.status === 200) {
                    blockUI.start();
                    $window.location.href = resp.data.model.redirectUri;
                }
            },
                function (error) {
                    if (error.data.exception.errorCode === "731") {
                        $scope.redirectUri = error.data.redirectUri;
                        $scope.sessionOut();
                    } else {
                        $scope.errorData = {};
                        $scope.errorData.errorCode = error.data.exception ? error.data.exception.errorCode : "800";
                        $scope.errorData.correlationId = error.headers("correlationId") || null;
                        blockUI.stop();
                    }
                });
        };
        $scope.focusMainContaint = function () {
            $("#username").focus();
        };
        $scope.filterValue = function ($event) {
            if ($event.which === 13) {
                $scope.loginAction();
            } else {
                if ($event.which !== 8) {// only for mozila browser
                    if ($event.keyCode !== 9) {// only for mozila browser
                        if (isNaN(String.fromCharCode($event.which)) || $event.which === 32) {
                            $event.preventDefault();
                        }
                    }

                }

            }
        };

        $scope.init();
    }
]);

"use strict";
angular.module("loginApp").controller("lightboxCtrl", [
    "$rootScope", "$scope", "config", "LoginService", "$translate", "$uibModal", "$timeout", "$state", "$window", "blockUI",
    function ($rootScope, $scope, config, LoginService, $translate, $uibModal, $timeout, $state, $window, blockUI) {
        $scope.init = function () {
            $scope.modelPopUpConf = $state.params.content;

            $translate(["SESSION_TIMEOUT_POPUP.SESSION_TIMEOUT_POPUP_MESSAGE",
            "SESSION_TIMEOUT_POPUP.SESSION_POPOUT_BUTTON_LABEL"
            ]).then(function (translations) {
                $scope.sessionDescText = translations["SESSION_TIMEOUT_POPUP.SESSION_TIMEOUT_POPUP_MESSAGE"];
                $scope.sessionPopupBtnText = translations["SESSION_TIMEOUT_POPUP.SESSION_POPOUT_BUTTON_LABEL"];
            });
        };

        $scope.goToModal2 = function () {
            $state.go("login.modal.modal-2", { content: $scope.modelPopUpConf });
        };
        $scope.goToModal3 = function () {
            $state.go("login.modal.modal-3", { content: $scope.modelPopUpConf });
        };
        $scope.goToTPP = function () {
            var authUrl = $("#resumePath").val();
            var reqData = null;
            var serverErrorFlag = $("#serverErrorFlag").val();
            $scope.cancelBtnClicked();
            var channelId = $("#channelId").val();
            var correlationId = $("#correlationId") ? $("#correlationId").val() : null;
            LoginService.rejectRequest(reqData, authUrl, serverErrorFlag, correlationId, channelId).then(function (resp) {
                if (resp.status === 200) {
                    blockUI.start();
                    $window.location.href = resp.data.model.redirectUri;
                }
            },
                function (error) {
                    if (error.data.exception.errorCode === "731") {
                        $scope.redirectUri = error.data.redirectUri;
                        $scope.sessionOut();
                    } else {
                        $scope.errorData = {};
                        $scope.errorData.errorCode = error.data.exception ? error.data.exception.errorCode : "800";
                        $scope.errorData.correlationId = error.headers("correlationId") || null;
                        blockUI.stop();
                    }
                });
        };
        $scope.sessionOutClicked = function () {
            $scope.cancelBtnClicked();
        };

        $scope.goTo365 = function () {
            $scope.cancelBtnClicked();
            $scope.url = config["365Online"]; // Redirection URL
            window.open(
                $scope.url,
                "_blank"
            );
        };


        $scope.cancelBtnClicked = function () {
            $scope.modalInstance.dismiss("cancel");
        };
        $scope.init();

    }

]);

"use strict";
angular.module("loginApp").service("LoginService", ["$http", function($http) {
    return {
        // Reject request
        rejectRequest: function(reqData, authUrl, serverErrorFlag, correlationId, channelId) {
                return $http.put("./cancel?oAuthUrl=" + authUrl + "&serverErrorFlag="+ serverErrorFlag + "&correlationId="+ correlationId + "&channelId="+ channelId, reqData);
        }
    };
}]);

"use strict";
angular.module("loginApp").factory("fontloaderService", ["$q", "$window", "blockUI", function ($q, $window, blockUI) {
    return {
        isFontLoad: function () {
            blockUI.start();
            var deferred = $q.defer();
            $window.WebFont.load({
                custom: {
                    families: ["FontAwesome", "OpenSans-Bold", "OpenSans-Italic",
                        "OpenSans-Light", "OpenSans-Regular", "OpenSans-Semibold", "Glyphicons Halflings"]
                },
                active: function (familyName, fvd) {
                    deferred.resolve("font loaded");
                    blockUI.stop();
                }
            });
            return deferred.promise;
        }
    };
}]);

"use strict";
angular.module("loginApp").directive("errorNotice",
    ["$translate", function($translate) {
        return {
            restrict: "E",
            replace: true,
            scope: {
                errorData: "="
            },
            templateUrl: "/views/error-notice.html",
            link: {
                pre:function($scope, el, attrs) {
                    $scope.$watch("errorData", function() {
                        var errorCodeMsg = "ERROR_MESSAGES."+ $scope.errorData.errorCode;
                        var errorCodeMsgDesc = "ERROR_MESSAGES."+ $scope.errorData.errorCode + "_DETAILS";
                        $scope.isErrorMsgTextDescription = false;
                        var correlationCodeMsg ="";
                        if($scope.errorData.correlationId){
                            correlationCodeMsg = "ERROR_MESSAGES."+ $scope.errorData.correlationId;
                        }

                        $translate(["ERROR_MESSAGES.UNABLE_REQUEST_PROCESS",
                        "ERROR_MESSAGES.CORRELATION_LABEL","ERROR_MESSAGES.ALERT",
                        "ERROR_MESSAGES.ERROR","ERROR_MESSAGES.NOTICE",errorCodeMsg,errorCodeMsgDesc,correlationCodeMsg
                            ]).then(function(translations) {
                                if($scope.errorData.prefix === "alert"){
                                    $scope.alertText = translations["ERROR_MESSAGES.ALERT"] + ":";
                                }else if($scope.errorData.prefix === "notice"){
                                    $scope.alertText = translations["ERROR_MESSAGES.NOTICE"] + ":";
                                }else{
                                    $scope.alertText = translations["ERROR_MESSAGES.ERROR"] + ":";
                                }
                                $scope.corRelationText = translations["ERROR_MESSAGES.CORRELATION_LABEL"];
                                $scope.corRelationMsgText = translations[correlationCodeMsg];
                                $scope.errorMsgText = (translations[errorCodeMsg] === errorCodeMsg) ?
                                translations["ERROR_MESSAGES.UNABLE_REQUEST_PROCESS"] : translations[errorCodeMsg];
                                if(translations[errorCodeMsgDesc] !== errorCodeMsgDesc && translations[errorCodeMsgDesc] !== ""){
                                    $scope.isErrorMsgTextDescription = true;
                                    $scope.errorMsgTextDescription = translations[errorCodeMsgDesc];
                                }
                                $scope.errorCssClass= ($scope.errorData.errorCSSClass === "warning") ? "alert-warning" : "alert-danger";
                            });
                     });

                }
            }
        };
    }]);

"use strict";
angular.module("loginApp").directive("modelPopUp", ["$uibModal", "$document", function ($uibModal, $document) {
    return {
        restrict: "E",
        scope: {
            modal: "=",
            cdnBaseUrl: "="
        },
        controller: "loginCtrl",
        link: function ($scope, el, attrs) {
            $scope.modalInstance = $uibModal.open({
                templateUrl: "/views/modalPopUp.html",
                animation: true,
                backdrop: true,
                size: "lg",
                windowClass: "pop-confirm",
                scope: $scope,
                controller: "lightboxCtrl"
            });
            $("header").css({ "z-index": 1065 });
        }
    };
}]);

"use strict";
angular.module("loginApp").filter("isTranslated", function(){
    return function(translatedVal, originalVal){
      return (translatedVal === originalVal) ? false : true;
    };
});
