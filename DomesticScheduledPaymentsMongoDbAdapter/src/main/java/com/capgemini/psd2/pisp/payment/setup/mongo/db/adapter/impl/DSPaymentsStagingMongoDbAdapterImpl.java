package com.capgemini.psd2.pisp.payment.setup.mongo.db.adapter.impl;

import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Conditional;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.conditions.MongoDbMockCondition;
import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.InternalServerErrorMessage;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.adapter.DomesticScheduledPaymentsAdapter;
import com.capgemini.psd2.pisp.domain.CustomDSPaymentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomDSPaymentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.GenericPaymentsResponse;
import com.capgemini.psd2.pisp.domain.OBCharge1;
import com.capgemini.psd2.pisp.domain.OBChargeBearerType1Code;
import com.capgemini.psd2.pisp.domain.OBExternalStatus1Code;
import com.capgemini.psd2.pisp.domain.OBExternalStatus2Code;
import com.capgemini.psd2.pisp.domain.OBMultiAuthorisation1;
import com.capgemini.psd2.pisp.enums.ProcessExecutionStatusEnum;
import com.capgemini.psd2.pisp.payment.setup.mongo.db.adapter.repository.DSPaymentsFoundationRepository;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.pisp.status.PaymentConstants;
import com.capgemini.psd2.pisp.utilities.PispUtilities;
import com.capgemini.psd2.utilities.JSONUtilities;
import com.capgemini.psd2.utilities.SandboxValidationUtility;

@Conditional(MongoDbMockCondition.class)
@Component("dScheduledPaymentsMongoDbAdapter")
public class DSPaymentsStagingMongoDbAdapterImpl implements DomesticScheduledPaymentsAdapter {

	@Autowired
	private DSPaymentsFoundationRepository dsPaymentsFoundationRepository;

	@Autowired
	private SandboxValidationUtility utility;

	@Autowired
	private RequestHeaderAttributes reqAttributes;

	@Value("${app.isSandboxEnabled:false}")
	private boolean isSandboxEnabled;
	
	@Override
	public GenericPaymentsResponse processDSPayments(CustomDSPaymentsPOSTRequest customRequest,
			Map<String, OBExternalStatus1Code> paymentStatusMap, Map<String, String> params) {

		GenericPaymentsResponse genericPaymentsResponse = populateMockedResponse(customRequest);
		populateAndSaveDbResource(customRequest, genericPaymentsResponse, paymentStatusMap);

		return genericPaymentsResponse;
	}

	private GenericPaymentsResponse populateMockedResponse(CustomDSPaymentsPOSTRequest customRequest) {
		GenericPaymentsResponse genericPaymentsResponse = new GenericPaymentsResponse();
		String currency = customRequest.getData().getInitiation().getInstructedAmount().getCurrency();
		String initiationAmount = customRequest.getData().getInitiation().getInstructedAmount().getAmount();
		OBChargeBearerType1Code reqChargeBearer = null;

		String submissionId = null;
		String expectedExecutionDateTime = utility.getMockedExpectedExecDtTm();
		String expectedSettlementDateTime = utility.getMockedExpectedSettlementDtTm();
		
		ProcessExecutionStatusEnum processStatusEnum = utility.getMockedProcessExecStatus(currency, initiationAmount);
		OBMultiAuthorisation1 multiAuthorisation = utility.getMockedMultiAuthBlock(currency, initiationAmount);

		if (processStatusEnum == ProcessExecutionStatusEnum.PASS
				|| processStatusEnum == ProcessExecutionStatusEnum.FAIL
				|| processStatusEnum == ProcessExecutionStatusEnum.INCOMPLETE)
			submissionId = UUID.randomUUID().toString();

		genericPaymentsResponse.setSubmissionId(submissionId);
		
		// removing charges block as this is not returned for BOI on domestic payment APIs
		if(!isSandboxEnabled){
			List<OBCharge1> charges = utility.getMockedOBChargeList(currency, initiationAmount, reqChargeBearer);
			genericPaymentsResponse.setCharges(charges);
		}
		genericPaymentsResponse.setExpectedExecutionDateTime(expectedExecutionDateTime);
		genericPaymentsResponse.setExpectedSettlementDateTime(expectedSettlementDateTime);
		genericPaymentsResponse.setMultiAuthorisation(multiAuthorisation);
		genericPaymentsResponse.setProcessExecutionStatusEnum(processStatusEnum);
		return genericPaymentsResponse;
	}

	private void populateAndSaveDbResource(CustomDSPaymentsPOSTRequest customRequest,
			GenericPaymentsResponse genericPaymentsResponse, Map<String, OBExternalStatus1Code> paymentStatusMap) {

		String paymentRequest = JSONUtilities.getJSONOutPutFromObject(customRequest);
		CustomDSPaymentsPOSTResponse fsDbResource = JSONUtilities.getObjectFromJSONString(
				PispUtilities.getObjectMapper(), paymentRequest, CustomDSPaymentsPOSTResponse.class);

		fsDbResource.getData().setDomesticScheduledPaymentId(genericPaymentsResponse.getSubmissionId());
		fsDbResource.getData().setCreationDateTime(customRequest.getCreatedOn());
		fsDbResource.getData().setCharges(genericPaymentsResponse.getCharges());
		fsDbResource.getData().setExpectedExecutionDateTime(genericPaymentsResponse.getExpectedExecutionDateTime());
		fsDbResource.getData().setExpectedSettlementDateTime(genericPaymentsResponse.getExpectedSettlementDateTime());
		fsDbResource.getData().setMultiAuthorisation(genericPaymentsResponse.getMultiAuthorisation());
		if (genericPaymentsResponse.getSubmissionId() != null) {
			OBExternalStatus1Code status = calculateCMAStatus(genericPaymentsResponse.getProcessExecutionStatusEnum(),
					genericPaymentsResponse.getMultiAuthorisation(), paymentStatusMap);
			fsDbResource.getData().setStatus(status);
			fsDbResource.getData().setStatusUpdateDateTime(customRequest.getCreatedOn());
		}
		try {
			dsPaymentsFoundationRepository.save(fsDbResource);
		} catch (DataAccessResourceFailureException exception) {
			throw PSD2Exception.populatePSD2Exception(
					new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR, exception.getMessage()));
		}
	}

	@Override
	public CustomDSPaymentsPOSTResponse retrieveStagedDSPaymentsResponse(
			CustomPaymentStageIdentifiers customPaymentStageIdentifiers, Map<String, String> params) {
		CustomDSPaymentsPOSTResponse response = new CustomDSPaymentsPOSTResponse();
		CustomDSPaymentsPOSTResponse x = dsPaymentsFoundationRepository
				.findOneByDataDomesticScheduledPaymentId(customPaymentStageIdentifiers.getPaymentSubmissionId());
		response.setData(x.getData());

		String initiationAmount = response.getData().getInitiation().getInstructedAmount().getAmount();
		if (reqAttributes.getMethodType().equals("GET")) {
			if (utility.isValidAmount(initiationAmount, PSD2Constants.SANDBOX_PISP_GET_MOCKING)) {

				throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
						InternalServerErrorMessage.SANDBOX_MOCKED_ERROR));
			}
		}

		return response;
	}

	private OBExternalStatus1Code calculateCMAStatus(ProcessExecutionStatusEnum processExecutionStatusEnum,
			OBMultiAuthorisation1 multiAuthorisation, Map<String, OBExternalStatus1Code> paymentStatusMap) {

		OBExternalStatus1Code cmaStatus = paymentStatusMap.get(PaymentConstants.INITIAL);

		if (processExecutionStatusEnum == ProcessExecutionStatusEnum.FAIL)
			cmaStatus = paymentStatusMap.get(PaymentConstants.FAIL);
		else if (processExecutionStatusEnum == ProcessExecutionStatusEnum.PASS) {
			if (multiAuthorisation == null || multiAuthorisation.getStatus() == OBExternalStatus2Code.AUTHORISED)
				cmaStatus = paymentStatusMap.get(PaymentConstants.PASS_M_AUTH);
			else if (multiAuthorisation.getStatus() == OBExternalStatus2Code.AWAITINGFURTHERAUTHORISATION)
				cmaStatus = paymentStatusMap.get(PaymentConstants.PASS_M_AWAIT);
			else if (multiAuthorisation.getStatus() == OBExternalStatus2Code.REJECTED)
				cmaStatus = paymentStatusMap.get(PaymentConstants.PASS_M_REJECT);
		}
		return cmaStatus;
	}
}
