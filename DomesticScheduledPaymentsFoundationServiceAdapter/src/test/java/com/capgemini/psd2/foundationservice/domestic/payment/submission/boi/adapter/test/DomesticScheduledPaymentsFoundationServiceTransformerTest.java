package com.capgemini.psd2.foundationservice.domestic.payment.submission.boi.adapter.test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.foundationservice.domestic.scheduled.payment.submission.boi.adapter.domain.Amount;
import com.capgemini.psd2.foundationservice.domestic.scheduled.payment.submission.boi.adapter.domain.AuthorisingPartyAccount;
import com.capgemini.psd2.foundationservice.domestic.scheduled.payment.submission.boi.adapter.domain.ChargeBearer;
import com.capgemini.psd2.foundationservice.domestic.scheduled.payment.submission.boi.adapter.domain.Country;
import com.capgemini.psd2.foundationservice.domestic.scheduled.payment.submission.boi.adapter.domain.Currency;
import com.capgemini.psd2.foundationservice.domestic.scheduled.payment.submission.boi.adapter.domain.FinancialEventAmount;
import com.capgemini.psd2.foundationservice.domestic.scheduled.payment.submission.boi.adapter.domain.PaymentInstructionCharge;
import com.capgemini.psd2.foundationservice.domestic.scheduled.payment.submission.boi.adapter.domain.PaymentInstructionPostalAddress;
import com.capgemini.psd2.foundationservice.domestic.scheduled.payment.submission.boi.adapter.domain.PaymentInstructionStatusCode2;
import com.capgemini.psd2.foundationservice.domestic.scheduled.payment.submission.boi.adapter.domain.ProposingPartyAccount;
import com.capgemini.psd2.foundationservice.domestic.scheduled.payment.submission.boi.adapter.domain.ScheduledPaymentInstructionComposite;
import com.capgemini.psd2.foundationservice.domestic.scheduled.payment.submission.boi.adapter.domain.ScheduledPaymentInstructionProposal;
import com.capgemini.psd2.foundationservice.domestic.scheduled.payment.submission.boi.adapter.domain.ScheduledPaymentInstructionProposal2;
import com.capgemini.psd2.foundationservice.domestic.scheduled.payment.submission.boi.adapter.transformer.DomesticScheduledPaymentSubmissionFoundationServiceTransformer;
import com.capgemini.psd2.pisp.domain.CustomDSPaymentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.OBAddressTypeCode;
import com.capgemini.psd2.pisp.domain.OBCashAccountCreditor2;
import com.capgemini.psd2.pisp.domain.OBCashAccountDebtor3;
import com.capgemini.psd2.pisp.domain.OBDomestic1InstructedAmount;
import com.capgemini.psd2.pisp.domain.OBDomesticScheduled1;
import com.capgemini.psd2.pisp.domain.OBExternalPaymentContext1Code;
import com.capgemini.psd2.pisp.domain.OBPostalAddress6;
import com.capgemini.psd2.pisp.domain.OBRemittanceInformation1;
import com.capgemini.psd2.pisp.domain.OBRisk1;
import com.capgemini.psd2.pisp.domain.OBRisk1DeliveryAddress;
import com.capgemini.psd2.pisp.domain.OBWriteDataDomesticScheduled1;
import com.capgemini.psd2.validator.PSD2Validator;

@RunWith(SpringJUnit4ClassRunner.class)
public class DomesticScheduledPaymentsFoundationServiceTransformerTest {
	
	@InjectMocks
	DomesticScheduledPaymentSubmissionFoundationServiceTransformer transformer;
	
	@Mock
	private PSD2Validator psd2Validator;
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * Context loads.
	 */
	@Test
	public void contextLoads() {
	}
	
	@Test
	public void  testtransformDomesticConsentResponseFromFDToAPIForInsert(){
		Map<String, String> params= new HashMap<>();
		
		
		ScheduledPaymentInstructionComposite composite = new ScheduledPaymentInstructionComposite();
		List<PaymentInstructionCharge> charges = new ArrayList<PaymentInstructionCharge>();
		PaymentInstructionCharge charge = new PaymentInstructionCharge();
		Currency currency = new Currency(); 
		
		currency.setIsoAlphaCode("pata nai");
		Amount amount = new Amount();
		amount .setTransactionCurrency(22.00);
		charge.setAmount(amount);
		charge.setChargeBearer(ChargeBearer.BorneByCreditor);
		charge.setCurrency(currency);
		charge.setType("fvfvfv");
		charges.add(charge);
		ScheduledPaymentInstructionProposal paymentInstructionProposal = new ScheduledPaymentInstructionProposal();
		paymentInstructionProposal.setCharges(charges);
		composite.setPaymentInstructionProposal(paymentInstructionProposal );
		ScheduledPaymentInstructionProposal2 paymentInstruction = new ScheduledPaymentInstructionProposal2();
		paymentInstruction.setPaymentInstructionNumber("5562174");
		paymentInstruction.setPaymentInstructionStatusCode(PaymentInstructionStatusCode2.InitiationCompleted);
		composite.setPaymentInstruction(paymentInstruction);
		transformer.transformDomesticScheduledPaymentsFromFDToAPIForInsert(composite);
		
	}
	
	@Test
	public void  testtransformDomesticConsentResponseFromFDToAPIForInsert1(){
		Map<String, String> params= new HashMap<>();
		
		
		ScheduledPaymentInstructionComposite composite = new ScheduledPaymentInstructionComposite();
		List<PaymentInstructionCharge> charges = new ArrayList<PaymentInstructionCharge>();
		PaymentInstructionCharge charge = new PaymentInstructionCharge();
		Currency currency = new Currency(); 
		
		currency.setIsoAlphaCode("pata nai");
		Amount amount = new Amount();
		amount .setTransactionCurrency(22.00);
		charge.setAmount(amount);
		charge.setChargeBearer(ChargeBearer.BorneByCreditor);
		charge.setCurrency(currency);
		charge.setType("fvfvfv");
		charges.add(charge);
		ScheduledPaymentInstructionProposal paymentInstructionProposal = new ScheduledPaymentInstructionProposal();
		paymentInstructionProposal.setCharges(charges);
		composite.setPaymentInstructionProposal(paymentInstructionProposal );
		ScheduledPaymentInstructionProposal2 paymentInstruction = new ScheduledPaymentInstructionProposal2();
		paymentInstruction.setPaymentInstructionNumber("5562174");
		paymentInstruction.setPaymentInstructionStatusCode(PaymentInstructionStatusCode2.InitiationFailed);
		composite.setPaymentInstruction(paymentInstruction);
		transformer.transformDomesticScheduledPaymentsFromFDToAPIForInsert(composite);
		
	}
	@Test
	public void testtransformDomesticPaymentResponse1(){
		CustomDSPaymentsPOSTRequest paymentConsentsRequest = new CustomDSPaymentsPOSTRequest();
		OBWriteDataDomesticScheduled1 data = new OBWriteDataDomesticScheduled1();
		Map<String, String> params= new HashMap<>();
		OBRemittanceInformation1 remittanceInformation = new OBRemittanceInformation1();
		remittanceInformation .setReference("ftgfy");
		remittanceInformation.setUnstructured("unstructured");
		OBCashAccountCreditor2 creditorAccount = new OBCashAccountCreditor2();
		creditorAccount .setIdentification("hgh");
		creditorAccount.setName("vg0");
		creditorAccount.setSchemeName("cfg");
		creditorAccount.setSecondaryIdentification("fgg");
		
		OBCashAccountDebtor3 debtorAccount = new OBCashAccountDebtor3();
		debtorAccount .setIdentification("12323");
		debtorAccount.setName("erer");
		debtorAccount.setSchemeName("wedfdf");
		debtorAccount.setSecondaryIdentification("dfdfd");
		List<String> addressLine = new ArrayList<>();
		addressLine.add(0, "fvd");
		addressLine.add(1, "dcf");
		OBDomestic1InstructedAmount instructedAmount = new OBDomestic1InstructedAmount();
		instructedAmount .setAmount("1234");
		instructedAmount.setCurrency("EUR");
		OBDomesticScheduled1 initiation = new OBDomesticScheduled1();
		initiation .setDebtorAccount(debtorAccount);
		initiation.setInstructedAmount(instructedAmount);
		initiation.setCreditorAccount(creditorAccount);
		initiation.setRemittanceInformation(remittanceInformation);
		initiation.setInstructionIdentification("tuyht");
		initiation.setLocalInstrument("tfh");
		initiation.setEndToEndIdentification(null);
		OBPostalAddress6 creditorPostalAddress = new OBPostalAddress6();
		creditorPostalAddress.setAddressType(OBAddressTypeCode.BUSINESS);
		creditorPostalAddress.setDepartment("wedfdf");
		creditorPostalAddress.setSubDepartment("wedfdf");
		creditorPostalAddress.setStreetName("wedfdf");
		creditorPostalAddress.setBuildingNumber("wedfdf");
		creditorPostalAddress.setPostCode("wedfdf");
		creditorPostalAddress.setTownName("wedfdf");
		creditorPostalAddress.setCountrySubDivision("wedfdf");
		creditorPostalAddress.setCountry("wedfdf");
		creditorPostalAddress.setAddressLine(addressLine);
		initiation.setCreditorPostalAddress(creditorPostalAddress );
		initiation.setRequestedExecutionDateTime("2019-04-07");
		data.setConsentId("df2sd4f1s2");
		data.setInitiation(initiation);
		OBRisk1 obRisk1=new OBRisk1();
		OBRisk1DeliveryAddress deliveryAddress=new OBRisk1DeliveryAddress();
		deliveryAddress.setCountry("India");
		deliveryAddress.setTownName("Pune");
		deliveryAddress.setAddressLine(addressLine);
		obRisk1.setDeliveryAddress(deliveryAddress);
		obRisk1.setMerchantCategoryCode("df2sd4f1s2");
		obRisk1.setMerchantCustomerIdentification("df2sd4f1s2");
		obRisk1.setPaymentContextCode(OBExternalPaymentContext1Code.BILLPAYMENT);
		paymentConsentsRequest.setData(data);;
		paymentConsentsRequest.setRisk(obRisk1);
		params.put("transactioReqHeader", "boi");
		params.put("tenant_id", "BOIUK");
		params.put("tenant_id","BOIROI");
		
		transformer.transformDomesticScheduledPaymentsFromAPIToFDForInsert(paymentConsentsRequest,params);
	}
	@Test
	public void testtransformDomesticPaymentResponse2(){
		CustomDSPaymentsPOSTRequest paymentConsentsRequest = new CustomDSPaymentsPOSTRequest();
		OBWriteDataDomesticScheduled1 data = new OBWriteDataDomesticScheduled1();
		Map<String, String> params= new HashMap<>();
		OBRemittanceInformation1 remittanceInformation = new OBRemittanceInformation1();
		remittanceInformation .setReference("ftgfy");
		remittanceInformation.setUnstructured("unstructured");
		OBCashAccountCreditor2 creditorAccount = new OBCashAccountCreditor2();
		creditorAccount .setIdentification("hgh");
		creditorAccount.setName("vg0");
		creditorAccount.setSchemeName("cfg");
		creditorAccount.setSecondaryIdentification("fgg");
		
		OBCashAccountDebtor3 debtorAccount = new OBCashAccountDebtor3();
		debtorAccount .setIdentification("12323");
		debtorAccount.setName("erer");
		debtorAccount.setSchemeName("wedfdf");
		debtorAccount.setSecondaryIdentification("dfdfd");
		List<String> addressLine = new ArrayList<>();
		addressLine.add(0, "fvd");
		addressLine.add(1, "dcf");
		OBDomestic1InstructedAmount instructedAmount = new OBDomestic1InstructedAmount();
		instructedAmount .setAmount("1234");
		instructedAmount.setCurrency("EUR");
		OBDomesticScheduled1 initiation = new OBDomesticScheduled1();
		initiation .setDebtorAccount(debtorAccount);
		initiation.setInstructedAmount(instructedAmount);
		initiation.setCreditorAccount(creditorAccount);
		initiation.setRemittanceInformation(remittanceInformation);
		initiation.setInstructionIdentification("tuyht");
		initiation.setLocalInstrument("tfh");
		initiation.setEndToEndIdentification("456");
		OBPostalAddress6 creditorPostalAddress = new OBPostalAddress6();
		creditorPostalAddress.setAddressType(OBAddressTypeCode.BUSINESS);
		creditorPostalAddress.setDepartment("wedfdf");
		creditorPostalAddress.setSubDepartment("wedfdf");
		creditorPostalAddress.setStreetName("wedfdf");
		creditorPostalAddress.setBuildingNumber("wedfdf");
		creditorPostalAddress.setPostCode("wedfdf");
		creditorPostalAddress.setTownName("wedfdf");
		creditorPostalAddress.setCountrySubDivision("wedfdf");
		creditorPostalAddress.setCountry("wedfdf");
		creditorPostalAddress.setAddressLine(addressLine);
		initiation.setCreditorPostalAddress(creditorPostalAddress );
		initiation.setRequestedExecutionDateTime("2019-04-07");
		data.setConsentId("df2sd4f1s2");
		data.setInitiation(initiation);
		OBRisk1 obRisk1=new OBRisk1();
		OBRisk1DeliveryAddress deliveryAddress=new OBRisk1DeliveryAddress();
		deliveryAddress.setCountry("India");
		deliveryAddress.setTownName("Pune");
		deliveryAddress.setAddressLine(addressLine);
		obRisk1.setDeliveryAddress(deliveryAddress);
		obRisk1.setMerchantCategoryCode("df2sd4f1s2");
		obRisk1.setMerchantCustomerIdentification("df2sd4f1s2");
		obRisk1.setPaymentContextCode(OBExternalPaymentContext1Code.BILLPAYMENT);
		paymentConsentsRequest.setData(data);;
		paymentConsentsRequest.setRisk(obRisk1);
		params.put("transactioReqHeader", "boi");
		params.put("tenant_id", "BOIUK");
		params.put("tenant_id","BOIROI");
		params.put("X-BOI-CHANNEL", "BoiChannel");
		transformer.transformDomesticScheduledPaymentsFromAPIToFDForInsert(paymentConsentsRequest,params);
	}
	@Test
	public void testtransformDomesticPaymentResponse(){
	ScheduledPaymentInstructionComposite inputBalanceObj = new ScheduledPaymentInstructionComposite();
	ScheduledPaymentInstructionProposal2 paymentInstruction = new ScheduledPaymentInstructionProposal2();
	paymentInstruction.setPaymentInstructionNumber("54461");
	paymentInstruction.setInstructionIssueDate("2018-10-20T00:00:00+01:00");
	paymentInstruction.setPaymentInstructionStatusCode(PaymentInstructionStatusCode2.InitiationCompleted);
	paymentInstruction.setInstructionStatusUpdateDateTime("2018-10-20T00:00:00+01:00");
	inputBalanceObj.setPaymentInstruction(paymentInstruction);
	ScheduledPaymentInstructionProposal paymentInstructionProposal = new ScheduledPaymentInstructionProposal();
	paymentInstructionProposal.setPaymentInstructionProposalId("41xc21s");
	List<PaymentInstructionCharge> charges = new ArrayList<>();
	Currency currency = new Currency(); 	
	currency.setIsoAlphaCode("pata nai");
	Amount amount = new Amount();
	amount .setTransactionCurrency(22.00);
	PaymentInstructionCharge charge= new PaymentInstructionCharge();
	charge.setAmount(amount);
	charge.setChargeBearer(ChargeBearer.BorneByCreditor);
	charge.setCurrency(currency);
	charge.setType("fvfvfv");
	charges.add(charge);
	paymentInstructionProposal.setCharges(charges );
	paymentInstructionProposal.setInstructionReference("sdcscs");
	paymentInstructionProposal.setInstructionEndToEndReference("sscsc");
	paymentInstructionProposal.setInstructionLocalInstrument("scsc");
	paymentInstructionProposal.setRequestedExecutionDateTime("2018-10-20T00:00:00+01:00");
	FinancialEventAmount financialEventAmount = new FinancialEventAmount();
	financialEventAmount.setTransactionCurrency(22.00);
	paymentInstructionProposal.setFinancialEventAmount(financialEventAmount );
	Currency transactionCurrency = new Currency();
	transactionCurrency.setIsoAlphaCode("sdcsdc412");
	paymentInstructionProposal.setTransactionCurrency(transactionCurrency);
	AuthorisingPartyAccount authorisingPartyAccount = new AuthorisingPartyAccount();
	authorisingPartyAccount.setAccountIdentification("sdcsdc412");
	authorisingPartyAccount.setAccountName("sdcsdc412");
	authorisingPartyAccount.setAccountNumber("sdcsdc412");
	authorisingPartyAccount.setSchemeName("sdcsdc412");
	authorisingPartyAccount.setSecondaryIdentification("sdcsdc412");
	paymentInstructionProposal.setAuthorisingPartyAccount(authorisingPartyAccount );
	inputBalanceObj.setPaymentInstructionProposal(paymentInstructionProposal);
	ProposingPartyAccount proposingPartyAccount = new ProposingPartyAccount();
	proposingPartyAccount.setAccountIdentification("sdcsdc412");
	proposingPartyAccount.setAccountName("sdcsdc412");
	proposingPartyAccount.setAccountNumber("sdcsdc412");
	proposingPartyAccount.setSchemeName("sdcsdc412");
	proposingPartyAccount.setSecondaryIdentification("sdcsdc412");
	PaymentInstructionPostalAddress proposingPartyPostalAddress = new PaymentInstructionPostalAddress();
	proposingPartyPostalAddress.setAddressType("sdcsdc412");
	proposingPartyPostalAddress.setDepartment("sdcsdc412");
	proposingPartyPostalAddress.setSubDepartment("sdcsdc412");
	proposingPartyPostalAddress.setGeoCodeBuildingName("sdcsdc412");
	proposingPartyPostalAddress.setGeoCodeBuildingNumber("sdcsdc412");
	proposingPartyPostalAddress.setPostCodeNumber("sdcsdc412");
	proposingPartyPostalAddress.setTownName("sdcsdc412");
	proposingPartyPostalAddress.setCountrySubDivision("sdcsdc412");
	Country addressCountry = new Country();
	addressCountry.setIsoCountryAlphaTwoCode("fgvdfvd");
	proposingPartyPostalAddress.setAddressCountry(addressCountry );
	List<String> addressLine = new ArrayList<>();
	addressLine.add(0, "fvd");
	proposingPartyPostalAddress.setAddressLine(addressLine );
	paymentInstructionProposal.setProposingPartyPostalAddress(proposingPartyPostalAddress );
	paymentInstructionProposal.setProposingPartyAccount(proposingPartyAccount);
	paymentInstructionProposal.setAuthorisingPartyReference("fvdfvsf");
	paymentInstructionProposal.setAuthorisingPartyUnstructuredReference("scdasv");
	Map<String, String> params = new HashMap<>();
	transformer.transformDomesticScheduledPaymentResponse(inputBalanceObj); 
	}
	
	@Test
	public void testtransformDomesticPaymentResponse3(){
	ScheduledPaymentInstructionComposite inputBalanceObj = new ScheduledPaymentInstructionComposite();
	ScheduledPaymentInstructionProposal2 paymentInstruction = new ScheduledPaymentInstructionProposal2();
	paymentInstruction.setPaymentInstructionNumber("54461");
	paymentInstruction.setInstructionIssueDate("2018-10-20T00:00:00+01:00");
	paymentInstruction.setPaymentInstructionStatusCode(PaymentInstructionStatusCode2.InitiationFailed);
	paymentInstruction.setInstructionStatusUpdateDateTime("2018-10-20T00:00:00+01:00");
	inputBalanceObj.setPaymentInstruction(paymentInstruction);
	ScheduledPaymentInstructionProposal paymentInstructionProposal = new ScheduledPaymentInstructionProposal();
	paymentInstructionProposal.setPaymentInstructionProposalId("41xc21s");
	List<PaymentInstructionCharge> charges = new ArrayList<>();
	Currency currency = new Currency(); 	
	currency.setIsoAlphaCode("pata nai");
	Amount amount = new Amount();
	amount .setTransactionCurrency(22.00);
	PaymentInstructionCharge charge= new PaymentInstructionCharge();
	charge.setAmount(amount);
	charge.setChargeBearer(ChargeBearer.BorneByCreditor);
	charge.setCurrency(currency);
	charge.setType("fvfvfv");
	charges.add(charge);
	paymentInstructionProposal.setCharges(charges );
	paymentInstructionProposal.setInstructionReference("sdcscs");
	paymentInstructionProposal.setInstructionEndToEndReference("sscsc");
	paymentInstructionProposal.setInstructionLocalInstrument("scsc");
	paymentInstructionProposal.setRequestedExecutionDateTime("2018-10-20T00:00:00+01:00");
	FinancialEventAmount financialEventAmount = new FinancialEventAmount();
	financialEventAmount.setTransactionCurrency(22.00);
	paymentInstructionProposal.setFinancialEventAmount(financialEventAmount );
	Currency transactionCurrency = new Currency();
	transactionCurrency.setIsoAlphaCode("sdcsdc412");
	paymentInstructionProposal.setTransactionCurrency(transactionCurrency);
	AuthorisingPartyAccount authorisingPartyAccount = new AuthorisingPartyAccount();
	authorisingPartyAccount.setAccountIdentification("sdcsdc412");
	authorisingPartyAccount.setAccountName("sdcsdc412");
	authorisingPartyAccount.setAccountNumber("sdcsdc412");
	authorisingPartyAccount.setSchemeName("sdcsdc412");
	authorisingPartyAccount.setSecondaryIdentification("sdcsdc412");
	paymentInstructionProposal.setAuthorisingPartyAccount(authorisingPartyAccount );
	inputBalanceObj.setPaymentInstructionProposal(paymentInstructionProposal);
	ProposingPartyAccount proposingPartyAccount = new ProposingPartyAccount();
	proposingPartyAccount.setAccountIdentification("sdcsdc412");
	proposingPartyAccount.setAccountName("sdcsdc412");
	proposingPartyAccount.setAccountNumber("sdcsdc412");
	proposingPartyAccount.setSchemeName("sdcsdc412");
	proposingPartyAccount.setSecondaryIdentification("sdcsdc412");
	PaymentInstructionPostalAddress proposingPartyPostalAddress = new PaymentInstructionPostalAddress();
	proposingPartyPostalAddress.setAddressType("sdcsdc412");
	proposingPartyPostalAddress.setDepartment("sdcsdc412");
	proposingPartyPostalAddress.setSubDepartment("sdcsdc412");
	proposingPartyPostalAddress.setGeoCodeBuildingName("sdcsdc412");
	proposingPartyPostalAddress.setGeoCodeBuildingNumber("sdcsdc412");
	proposingPartyPostalAddress.setPostCodeNumber("sdcsdc412");
	proposingPartyPostalAddress.setTownName("sdcsdc412");
	proposingPartyPostalAddress.setCountrySubDivision("sdcsdc412");
	Country addressCountry = new Country();
	addressCountry.setIsoCountryAlphaTwoCode("fgvdfvd");
	proposingPartyPostalAddress.setAddressCountry(addressCountry );
	List<String> addressLine = new ArrayList<>();
	addressLine.add(0, "fvd");
	proposingPartyPostalAddress.setAddressLine(addressLine );
	paymentInstructionProposal.setProposingPartyPostalAddress(proposingPartyPostalAddress );
	paymentInstructionProposal.setProposingPartyAccount(proposingPartyAccount);
	paymentInstructionProposal.setAuthorisingPartyUnstructuredReference("scdasv");
	Map<String, String> params = new HashMap<>();
	transformer.transformDomesticScheduledPaymentResponse(inputBalanceObj); 
	}


}
