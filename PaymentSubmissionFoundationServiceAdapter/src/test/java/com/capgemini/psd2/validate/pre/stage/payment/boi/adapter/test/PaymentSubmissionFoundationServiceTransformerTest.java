package com.capgemini.psd2.validate.pre.stage.payment.boi.adapter.test;

import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.capgemini.psd2.adapter.datetime.utility.TimeZoneDateTimeAdapter;
import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.adapter.fraudnet.domain.FraudServiceResponse;
import com.capgemini.psd2.foundationservice.payment.submission.boi.adapter.constants.PaymentSubmissionFoundationServiceConstants;
import com.capgemini.psd2.foundationservice.payment.submission.boi.adapter.domain.PaymentInstruction;
import com.capgemini.psd2.foundationservice.payment.submission.boi.adapter.domain.ValidationPassed;
import com.capgemini.psd2.foundationservice.payment.submission.boi.adapter.transformer.PaymentSubmissionFoundationServiceTransformer;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.pisp.domain.CreditorAccount;
import com.capgemini.psd2.pisp.domain.CreditorAgent;
import com.capgemini.psd2.pisp.domain.CustomPaymentSubmissionPOSTRequest;
import com.capgemini.psd2.pisp.domain.DebtorAccount;
import com.capgemini.psd2.pisp.domain.DebtorAgent;
import com.capgemini.psd2.pisp.domain.PaymentSetupInitiationInstructedAmount;
import com.capgemini.psd2.pisp.domain.PaymentSetupResponseInitiation;
import com.capgemini.psd2.pisp.domain.PaymentSubmission;
import com.capgemini.psd2.pisp.domain.PaymentSubmissionExecutionResponse;
import com.capgemini.psd2.pisp.domain.RemittanceInformation;
import com.capgemini.psd2.pisp.domain.Risk;
import com.capgemini.psd2.pisp.domain.Risk.PaymentContextCodeEnum;
import com.capgemini.psd2.pisp.domain.RiskDeliveryAddress;

public class PaymentSubmissionFoundationServiceTransformerTest {
	
	@InjectMocks
	private PaymentSubmissionFoundationServiceTransformer transformer;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void contextLoads() {
	}

	@Mock
	private TimeZoneDateTimeAdapter timeZoneDateTimeAdapter;
	
	@Test
	public void transformPaymentSubmissionPOSTRequestTest1(){
		
		CustomPaymentSubmissionPOSTRequest paymentSubmissionPOSTRequest = new CustomPaymentSubmissionPOSTRequest();
		
		PaymentSubmission data = new PaymentSubmission();
		FraudServiceResponse fraudServiceResponse = new FraudServiceResponse();
		fraudServiceResponse.setDecisionType("Decision");
		
		PaymentSetupResponseInitiation initiation = new PaymentSetupResponseInitiation();
	    initiation.setInstructionIdentification("ANSM023");
	    initiation.setEndToEndIdentification("FRESCO.21302.GFX.37");
	    String date = "2016-01-01T17:07:20+0000";
	    paymentSubmissionPOSTRequest.setCreatedOn(date);
	    
	    DebtorAccount debtorAccount = new DebtorAccount();
	    debtorAccount.setIdentification("01234567123654");
	    debtorAccount.setName("Andrea Smith");
	    debtorAccount.setSchemeName(com.capgemini.psd2.pisp.domain.DebtorAccount.SchemeNameEnum.SORTCODEACCOUNTNUMBER);
	    
	    DebtorAgent debtorAgent = new DebtorAgent();
	    debtorAgent.setIdentification("SC112800");
	    debtorAgent.setSchemeName(com.capgemini.psd2.pisp.domain.DebtorAgent.SchemeNameEnum.BICFI);        
	    
	    CreditorAccount creditorAccount = new CreditorAccount();
	    creditorAccount.setIdentification("21325698123654");
	    creditorAccount.setName("Bob Clements");
	    creditorAccount.setSchemeName(com.capgemini.psd2.pisp.domain.CreditorAccount.SchemeNameEnum.SORTCODEACCOUNTNUMBER);
	    
	    CreditorAgent creditorAgent = new CreditorAgent();
	    creditorAgent.setIdentification("080800");// In FS side it is expected to be Integer 
	    creditorAgent.setSchemeName(com.capgemini.psd2.pisp.domain.CreditorAgent.SchemeNameEnum.BICFI);
	    
	    PaymentSetupInitiationInstructedAmount instructedAmount = new PaymentSetupInitiationInstructedAmount();
	    instructedAmount.setAmount("20.00");
	    instructedAmount.setCurrency("GBP");
	    
	    RemittanceInformation remittanceInformation = new RemittanceInformation();
	    remittanceInformation.setReference("FRESCO-037");
	    remittanceInformation.setUnstructured("Internal ops code 5120103");
	    
	    RiskDeliveryAddress riskDeliveryAddress = new RiskDeliveryAddress();
	    List<String> addressLine = new ArrayList<>();
	    addressLine.add("test1");
	    addressLine.add("test2");
	    List<String> countrySubDivision = new ArrayList<>();
	    countrySubDivision.add("test1");
	    countrySubDivision.add("test2");
	    
	    riskDeliveryAddress.setAddressLine(addressLine);
	    riskDeliveryAddress.setCountrySubDivision(countrySubDivision);
	    
	    initiation.setCreditorAccount(creditorAccount);
	    initiation.setCreditorAgent(creditorAgent);
	    initiation.setDebtorAccount(debtorAccount);
	    initiation.setDebtorAgent(debtorAgent);
	    initiation.setInstructedAmount(instructedAmount);
	    initiation.setRemittanceInformation(remittanceInformation);
	    
	    Risk risk = new Risk();
	    risk.setPaymentContextCode(PaymentContextCodeEnum.PERSONTOPERSON);
	    risk.setMerchantCategoryCode("test");
	    risk.setMerchantCustomerIdentification("test");
	    risk.setDeliveryAddress(riskDeliveryAddress);
	    
	    data.setInitiation(initiation);
	    paymentSubmissionPOSTRequest.setFraudSystemResponse(fraudServiceResponse);
	    paymentSubmissionPOSTRequest.setData(data);
	    paymentSubmissionPOSTRequest.setRisk(risk);

	    Map<String, String> params = new HashMap<String, String>();

	    params.put(PaymentSubmissionFoundationServiceConstants.USER_ID, "testUser");
	    params.put(PaymentSubmissionFoundationServiceConstants.CHANNEL_ID, "BOL");
	    params.put(PaymentSubmissionFoundationServiceConstants.CORRELATION_ID, "testCorrelation");
	    params.put(PaymentSubmissionFoundationServiceConstants.PLATFORM_ID, "testPlatform");
	    params.put(PSD2Constants.PAYMENT_CREATED_ON, new DateTime().toString());
	    
	    PaymentInstruction instruction = new PaymentInstruction();
	    
	    instruction = transformer.transformPaymentSubmissionPOSTRequest(paymentSubmissionPOSTRequest, params);
	    assertNotNull(instruction);
	}
	
	@Test
	public void transformPaymentSubmissionPOSTRequestTest2(){
		
		CustomPaymentSubmissionPOSTRequest paymentSubmissionPOSTRequest = new CustomPaymentSubmissionPOSTRequest();
		
		PaymentSubmission data = new PaymentSubmission();
	    
	    PaymentSetupResponseInitiation initiation = new PaymentSetupResponseInitiation();
	    initiation.setInstructionIdentification("ANSM023");
	    initiation.setEndToEndIdentification("FRESCO.21302.GFX.37");
	    String date = "2016-01-01T17:07:20+0000";
	    paymentSubmissionPOSTRequest.setCreatedOn(date);
	    
	    DebtorAccount debtorAccount = new DebtorAccount();
	    debtorAccount.setIdentification("01234567");
	    debtorAccount.setName("Andrea Smith");
	    debtorAccount.setSchemeName(com.capgemini.psd2.pisp.domain.DebtorAccount.SchemeNameEnum.IBAN);

	    CreditorAccount creditorAccount = new CreditorAccount();
	    creditorAccount.setIdentification("21325698");
	    creditorAccount.setName("Bob Clements");
	    creditorAccount.setSchemeName(com.capgemini.psd2.pisp.domain.CreditorAccount.SchemeNameEnum.IBAN);

	    PaymentSetupInitiationInstructedAmount instructedAmount = new PaymentSetupInitiationInstructedAmount();
	    instructedAmount.setAmount("20.00");
	    instructedAmount.setCurrency("GBP");
	    
	    RemittanceInformation remittanceInformation = new RemittanceInformation();
	    remittanceInformation.setReference("FRESCO-037");
	    remittanceInformation.setUnstructured("Internal ops code 5120103");
	    
	    RiskDeliveryAddress riskDeliveryAddress = new RiskDeliveryAddress();
	    List<String> addressLine = new ArrayList<>();
	    addressLine.add("test1");
	    addressLine.add("test2");
	    
	    initiation.setCreditorAccount(creditorAccount);
	    initiation.setDebtorAccount(debtorAccount);
	    initiation.setInstructedAmount(instructedAmount);
	    initiation.setRemittanceInformation(remittanceInformation);
	    
	    Risk risk = new Risk();
	    risk.setPaymentContextCode(PaymentContextCodeEnum.PERSONTOPERSON);
	    risk.setMerchantCategoryCode("test");
	    risk.setMerchantCustomerIdentification("test");
	    risk.setDeliveryAddress(riskDeliveryAddress);
	    
	    data.setInitiation(initiation);
	    paymentSubmissionPOSTRequest.setData(data);
	    paymentSubmissionPOSTRequest.setRisk(risk);

	    Map<String, String> params = new HashMap<String, String>();

	    params.put(PaymentSubmissionFoundationServiceConstants.USER_ID, "testUser");
	    params.put(PaymentSubmissionFoundationServiceConstants.CHANNEL_ID, "BOL");
	    params.put(PaymentSubmissionFoundationServiceConstants.CORRELATION_ID, "testCorrelation");
	    params.put(PaymentSubmissionFoundationServiceConstants.PLATFORM_ID, "testPlatform");
	    params.put(PSD2Constants.PAYMENT_CREATED_ON, new DateTime().toString());
	    PaymentInstruction instruction = new PaymentInstruction();
	    
	    instruction = transformer.transformPaymentSubmissionPOSTRequest(paymentSubmissionPOSTRequest, params);
	    assertNotNull(instruction);
	}
	
	@Test
	public void transformPaymentSubmissionResponseTest1(){
		
		ValidationPassed validationPassed = new ValidationPassed();
		validationPassed.setSuccessCode("testCode");
		validationPassed.setSuccessMessage("testMessage");
		PaymentSubmissionExecutionResponse paymentSubmissionExecutionResponse = new PaymentSubmissionExecutionResponse();
		
		paymentSubmissionExecutionResponse = transformer.transformPaymentSubmissionResponse(validationPassed);
		assertNotNull(paymentSubmissionExecutionResponse);
	}
	
	@Test
	public void transformPaymentSubmissionResponseTest2(){
		
		ValidationPassed validationPassed = new ValidationPassed();
		validationPassed.setSuccessCode("Test");
		validationPassed.setSuccessMessage("Test");
		PaymentSubmissionExecutionResponse paymentSubmissionExecutionResponse = transformer.transformPaymentSubmissionResponse(validationPassed);
		assertNotNull(paymentSubmissionExecutionResponse);
	}
	
	@Test(expected = AdapterException.class)
	public void transformPaymentSubmissionResponseValidationPassedNullCheck(){
		PaymentSubmissionExecutionResponse paymentSubmissionExecutionResponse = transformer.transformPaymentSubmissionResponse(null);
		assertNotNull(paymentSubmissionExecutionResponse);
	}

}
