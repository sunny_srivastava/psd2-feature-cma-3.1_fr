package com.capgemini.psd2.payment.presubmissionvalidation.mock.foundationservice.service;

import com.capgemini.psd2.foundationservice.validator.ValidationPassed;
import com.capgemini.psd2.payment.presubmissionvalidation.mock.foundationservice.domain.PaymentInstruction;

public interface PaymentPreSubmissionValidationService {
	
	public ValidationPassed validatePaymentInstruction(PaymentInstruction paymentInstr);

}
