package com.capgemini.psd2.pisp.dspc.service.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMethod;

import com.capgemini.psd2.exceptions.ErrorMapKeys;
import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.domain.CustomDSPConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomDSPConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.CustomPaymentSetupPlatformDetails;
import com.capgemini.psd2.pisp.domain.GenericPaymentConsentResponse;
import com.capgemini.psd2.pisp.domain.OBExternalConsentStatus1Code;
import com.capgemini.psd2.pisp.domain.OBPostalAddress6;
import com.capgemini.psd2.pisp.domain.OBRemittanceInformation1;
import com.capgemini.psd2.pisp.domain.OBRisk1DeliveryAddress;
import com.capgemini.psd2.pisp.domain.PaymentConsentsPlatformResource;
import com.capgemini.psd2.pisp.domain.PaymentResponseInfo;
import com.capgemini.psd2.pisp.domain.PaymentRetrieveGetRequest;
import com.capgemini.psd2.pisp.dspc.comparator.DSPConsentsPayloadComparator;
import com.capgemini.psd2.pisp.dspc.service.DSPConsentsService;
import com.capgemini.psd2.pisp.enums.PaymentTypeEnum;
import com.capgemini.psd2.pisp.processing.adapter.service.PaymentConsentProcessingAdapter;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.pisp.stage.operations.adapter.DomesticScheduledPaymentStagingAdapter;
import com.capgemini.psd2.pisp.status.PaymentConstants;
import com.capgemini.psd2.pisp.utilities.PispUtilities;
import com.capgemini.psd2.utilities.JSONUtilities;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Service
public class DSPConsentsServiceImpl implements DSPConsentsService {

	@Autowired
	private DSPConsentsPayloadComparator dScheduledPaymentConsentsComparator;

	@Autowired
	@Qualifier("dspConsentsStagingRoutingAdapter")
	private DomesticScheduledPaymentStagingAdapter dspStagingAdapter;

	@Autowired
	private RequestHeaderAttributes requestHeaderAttr;
	
	@Autowired
	private PaymentConsentProcessingAdapter<CustomDSPConsentsPOSTRequest, CustomDSPConsentsPOSTResponse> paymemtRequestAdapter;
	
	@Value("${cmaVersion}")
	private String cmaVersion;

	@Override
	public CustomDSPConsentsPOSTResponse createDomesticScheduledPaymentConsentsResource(
			CustomDSPConsentsPOSTRequest scheduledPaymentRequest) {

		CustomDSPConsentsPOSTResponse paymentConsentsFoundationResponse = null;
		String requestType = PSD2Constants.PAYMENT_RESOURCE_IDEMPOTENT;

		PaymentConsentsPlatformResource paymentConsentsPlatformResponse = paymemtRequestAdapter
				.preConsentProcessFlows(scheduledPaymentRequest, populatePlatformDetails(scheduledPaymentRequest));

		/* Non Idempotent Request */
		if (paymentConsentsPlatformResponse.getPaymentConsentId() == null) {
			requestType = PSD2Constants.PAYMENT_RESOURCE_NON_IDEMPOTENT;
			/* Create stage call to Adapter */
			GenericPaymentConsentResponse consentFoundationGenericCreateResponse = processDomesticScheduleStagingResource(
					scheduledPaymentRequest, paymentConsentsPlatformResponse.getCreatedAt());

			paymentConsentsFoundationResponse = transformIntoTPPResponse(scheduledPaymentRequest,
					consentFoundationGenericCreateResponse, paymentConsentsPlatformResponse);
		}

		/* Idempotent Request */
		else
			paymentConsentsFoundationResponse = retrieveAndCompareDomesticStageResponse(scheduledPaymentRequest,
					paymentConsentsPlatformResponse);

		PaymentResponseInfo stageDetails = populateStageDetails(paymentConsentsFoundationResponse);
		stageDetails.setRequestType(requestType);

		/* Fix for SIT issue #2272 
		 * Tpp providing Blank remittance info, and FS is returning null,
		 * Thus in such case returning same Blank remittance info to TPP.
		 * 27.02.2019 */
		OBRemittanceInformation1 requestRemittanceInfo = scheduledPaymentRequest.getData().getInitiation().getRemittanceInformation();
		OBPostalAddress6 reqCreditorPostalAddress = scheduledPaymentRequest.getData().getInitiation().getCreditorPostalAddress();
		paymentConsentsFoundationResponse.getData().getInitiation().setRemittanceInformation(requestRemittanceInfo);
		paymentConsentsFoundationResponse.getData().getInitiation().setCreditorPostalAddress(reqCreditorPostalAddress);
		OBRisk1DeliveryAddress reqRiskDeliveryAddress = scheduledPaymentRequest.getRisk().getDeliveryAddress();
		paymentConsentsFoundationResponse.getRisk().setDeliveryAddress(reqRiskDeliveryAddress);
		
		/*
		 * Post Process Flows : Update Platform resource(for Non Idempotent), Transform
		 * response, Validate response
		 */
		return paymemtRequestAdapter.postConsentProcessFlows(stageDetails, paymentConsentsFoundationResponse,
				paymentConsentsPlatformResponse, RequestMethod.POST.toString());
	}

	private CustomDSPConsentsPOSTResponse transformIntoTPPResponse(CustomDSPConsentsPOSTRequest scheduledPaymentRequest,
			GenericPaymentConsentResponse consentFoundationGenericCreateResponse,
			PaymentConsentsPlatformResource paymentConsentsPlatformResponse) {
		String request = JSONUtilities.getJSONOutPutFromObject(scheduledPaymentRequest);
		CustomDSPConsentsPOSTResponse paymentConsentsFoundationResponse = JSONUtilities
				.getObjectFromJSONString(PispUtilities.getObjectMapper(), request, CustomDSPConsentsPOSTResponse.class);

		paymentConsentsFoundationResponse.getData().setConsentId(consentFoundationGenericCreateResponse.getConsentId());
		paymentConsentsFoundationResponse.getData().setCreationDateTime(paymentConsentsPlatformResponse.getCreatedAt());
		paymentConsentsFoundationResponse.getData()
				.setExpectedExecutionDateTime(consentFoundationGenericCreateResponse.getExpectedExecutionDateTime());
		paymentConsentsFoundationResponse.getData()
				.setExpectedSettlementDateTime(consentFoundationGenericCreateResponse.getExpectedSettlementDateTime());
		paymentConsentsFoundationResponse.getData().setCharges(consentFoundationGenericCreateResponse.getCharges());
		paymentConsentsFoundationResponse
				.setConsentPorcessStatus(consentFoundationGenericCreateResponse.getProcessConsentStatusEnum());
		paymentConsentsFoundationResponse.getData().setCutOffDateTime(consentFoundationGenericCreateResponse.getCutOffDateTime());
		return paymentConsentsFoundationResponse;
	}

	private CustomPaymentSetupPlatformDetails populatePlatformDetails(
			CustomDSPConsentsPOSTRequest scheduledPaymentRequest) {
		CustomPaymentSetupPlatformDetails standingConsentsPlatformDetails = new CustomPaymentSetupPlatformDetails();
		standingConsentsPlatformDetails.setPaymentType(PaymentTypeEnum.DOMESTIC_SCH_PAY);
		standingConsentsPlatformDetails.setSetupCmaVersion(cmaVersion);
		standingConsentsPlatformDetails = checkDebtorDetails(scheduledPaymentRequest, standingConsentsPlatformDetails);
		return standingConsentsPlatformDetails;
	}

	public CustomPaymentSetupPlatformDetails checkDebtorDetails(CustomDSPConsentsPOSTRequest scheduledPaymentRequest,
			CustomPaymentSetupPlatformDetails platformDetails) {
		String tppDebtorDetailsStatus = String.valueOf(Boolean.FALSE);
		String tppDebtorNameDetailsStatus = String.valueOf(Boolean.FALSE);
		if (scheduledPaymentRequest.getData() != null && scheduledPaymentRequest.getData().getInitiation() != null
				&& scheduledPaymentRequest.getData().getInitiation().getDebtorAccount() != null) {
			tppDebtorDetailsStatus = String.valueOf(Boolean.TRUE);
			if (!NullCheckUtils
					.isNullOrEmpty(scheduledPaymentRequest.getData().getInitiation().getDebtorAccount().getName()))
				tppDebtorNameDetailsStatus = String.valueOf(Boolean.TRUE);
		}

		platformDetails.setTppDebtorDetails(tppDebtorDetailsStatus);
		platformDetails.setTppDebtorNameDetails(tppDebtorNameDetailsStatus);
		return platformDetails;
	}

	private GenericPaymentConsentResponse processDomesticScheduleStagingResource(
			CustomDSPConsentsPOSTRequest scheduledPaymentRequest, String setupCreationDate) {

		/* new fields added */

		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();
		stageIdentifiers.setPaymentSetupVersion(cmaVersion);
		stageIdentifiers.setPaymentTypeEnum(PaymentTypeEnum.DOMESTIC_SCH_PAY);
		scheduledPaymentRequest.setCreatedOn(setupCreationDate);
		Map<String, String> paramMap = new HashMap<String, String>();
		paramMap.put(PSD2Constants.CO_RELATION_ID, requestHeaderAttr.getCorrelationId());
		return dspStagingAdapter.processDomesticScheduledPaymentConsents(scheduledPaymentRequest, stageIdentifiers,
				paramMap, OBExternalConsentStatus1Code.AWAITINGAUTHORISATION, OBExternalConsentStatus1Code.REJECTED);

	}

	public PaymentResponseInfo populateStageDetails(CustomDSPConsentsPOSTResponse paymentConsentsFoundationResponse) {
		PaymentResponseInfo stageDetails = new PaymentResponseInfo();
		stageDetails.setConsentProcessStatus(paymentConsentsFoundationResponse.getConsentPorcessStatus());
		stageDetails.setPaymentConsentId(paymentConsentsFoundationResponse.getData().getConsentId());
		return stageDetails;
	}

	private CustomDSPConsentsPOSTResponse retrieveAndCompareDomesticStageResponse(
			CustomDSPConsentsPOSTRequest scheduledPaymentRequest,
			PaymentConsentsPlatformResource paymentConsentsPlatformResponse) {
		CustomDSPConsentsPOSTResponse paymentConsentsFoundationResponse;

		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();
		stageIdentifiers.setPaymentConsentId(paymentConsentsPlatformResponse.getPaymentConsentId());
		stageIdentifiers.setPaymentTypeEnum(PaymentTypeEnum.DOMESTIC_SCH_PAY);
		stageIdentifiers.setPaymentSetupVersion(paymentConsentsPlatformResponse.getSetupCmaVersion());

		paymentConsentsFoundationResponse = dspStagingAdapter
				.retrieveStagedDomesticScheduledPaymentConsents(stageIdentifiers, null);

		if (dScheduledPaymentConsentsComparator.compareScheduledPaymentDetails(paymentConsentsFoundationResponse,
				scheduledPaymentRequest, paymentConsentsPlatformResponse) > 0)
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_RESOURCE_CONSENTMISMATCH,
					ErrorMapKeys.RESOURCE_CONSENT_MISMATCH));

		return paymentConsentsFoundationResponse;
	}

	@Override
	public CustomDSPConsentsPOSTResponse retrieveDomesticScheduledPaymentConsentsResource(String consentId) {
		PaymentRetrieveGetRequest customRequest = new PaymentRetrieveGetRequest();
		customRequest.setConsentId(consentId);
		customRequest.setPaymentTypeEnum(PaymentTypeEnum.DOMESTIC_SCH_PAY);
		PaymentConsentsPlatformResource paymentConsentsPlatformResponse = paymemtRequestAdapter
				.preConsentProcessGETFlow(customRequest);

		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();
		stageIdentifiers.setPaymentConsentId(customRequest.getConsentId());
		stageIdentifiers.setPaymentTypeEnum(PaymentTypeEnum.DOMESTIC_SCH_PAY);
		if (NullCheckUtils.isNullOrEmpty(paymentConsentsPlatformResponse.getSetupCmaVersion()))
			stageIdentifiers.setPaymentSetupVersion(PaymentConstants.CMA_FIRST_VERSION);
		else
			stageIdentifiers.setPaymentSetupVersion(paymentConsentsPlatformResponse.getSetupCmaVersion());

		CustomDSPConsentsPOSTResponse paymentConsentsFoundationResponse = dspStagingAdapter
				.retrieveStagedDomesticScheduledPaymentConsents(stageIdentifiers, null);

		if (paymentConsentsFoundationResponse == null) {
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_RESOURCE_NOTFOUND,
					ErrorMapKeys.NO_PAYMENT_SETUP_RESOURCES_FOUND));
		}
		
		PaymentResponseInfo stageDetails = new PaymentResponseInfo();
		stageDetails.setRequestType(PSD2Constants.PAYMENT_RESOURCE_IDEMPOTENT);
		return paymemtRequestAdapter.postConsentProcessFlows(stageDetails, paymentConsentsFoundationResponse,
				paymentConsentsPlatformResponse, RequestMethod.GET.toString());
	}

}
