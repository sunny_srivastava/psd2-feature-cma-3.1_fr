package com.capgemini.psd2.pisp.dspc.application;

import javax.servlet.Filter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import com.capgemini.psd2.exceptions.PSD2ExceptionHandler;
import com.capgemini.psd2.filter.PSD2Filter;
import com.capgemini.psd2.pisp.utilities.JsonStringTrimmer;

@SpringBootApplication
@ComponentScan(basePackages = {
		"com.capgemini.psd2" }, excludeFilters = @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, value = PSD2ExceptionHandler.class))
@EnableMongoRepositories(basePackages = { "com.capgemini.psd2" })
@EnableEurekaClient
public class DSPConsentsApplication {

	static ConfigurableApplicationContext context = null;

	public static void main(String[] args) {
		try {
			context = SpringApplication.run(DSPConsentsApplication.class, args);
		} catch (Exception e) {
			if (context != null) {
				SpringApplication.exit(context, () -> 1);
			}
			System.exit(1);
		}
	}

	/*
	 * To trim the white space from the fields of JSON request pay load
	 */
	@Bean
	public JsonStringTrimmer jsonStringTrimmer() {
		return new JsonStringTrimmer();
	}

	@Bean(name = "psd2Filter")
	public Filter psd2Filter() {
		return new PSD2Filter();
	}
}
