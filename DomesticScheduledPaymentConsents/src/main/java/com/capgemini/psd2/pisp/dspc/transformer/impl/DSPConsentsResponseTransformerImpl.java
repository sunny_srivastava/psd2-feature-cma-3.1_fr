package com.capgemini.psd2.pisp.dspc.transformer.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.domain.CustomDSPConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomDSPConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.Links;
import com.capgemini.psd2.pisp.domain.Meta;
import com.capgemini.psd2.pisp.domain.OBExternalConsentStatus1Code;
import com.capgemini.psd2.pisp.domain.PaymentConsentsPlatformResource;
import com.capgemini.psd2.pisp.processing.adapter.PaymentConsentsTransformer;
import com.capgemini.psd2.pisp.status.PaymentStatusCompatibilityMapEnum;
import com.capgemini.psd2.pisp.utilities.PispDateUtility;
import com.capgemini.psd2.pisp.utilities.PispUtilities;
import com.capgemini.psd2.pisp.validation.adapter.constants.PaymentSetupConstants;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Component("domesticSchedulePaymentConsentTransformer")
public class DSPConsentsResponseTransformerImpl
		implements PaymentConsentsTransformer<CustomDSPConsentsPOSTResponse, CustomDSPConsentsPOSTRequest> {

	@Autowired
	private RequestHeaderAttributes reqHeaderAtrributes;

	@Autowired
	private PispDateUtility pispDateUtility;

	@Override
	public CustomDSPConsentsPOSTResponse paymentConsentsResponseTransformer(
			CustomDSPConsentsPOSTResponse domesticConsentsTppResponse,
			PaymentConsentsPlatformResource paymentSetupPlatformResource, String methodType) {

		/*
		 * These fields are expected to already be populated in domesticSetupTppResponse
		 * from Domestic Stage Service : Charges, CutOffDateTime,
		 * ExpectedExecutionDateTime, ExpectedSettlementDateTime
		 */

		/* Fields from platform Resource */
		domesticConsentsTppResponse.getData().setCreationDateTime(paymentSetupPlatformResource.getCreatedAt());
		String setupCompatibleStatus = PaymentStatusCompatibilityMapEnum
				.valueOf(paymentSetupPlatformResource.getStatus().toUpperCase()).getCompatibleStatus();

		domesticConsentsTppResponse.getData().setStatus(OBExternalConsentStatus1Code.fromValue(setupCompatibleStatus));

		domesticConsentsTppResponse.getData().setStatusUpdateDateTime(
				NullCheckUtils.isNullOrEmpty(paymentSetupPlatformResource.getStatusUpdateDateTime())
						? paymentSetupPlatformResource.getUpdatedAt()
						: paymentSetupPlatformResource.getStatusUpdateDateTime());

		/*
		 * If debtorFlag is 'False' then DebtorAccount & DebtorAgent details do not have
		 * to send in response. debtorFlag would be set as Null DebtorAgent is not
		 * available in CMA3
		 */
		if (!Boolean.valueOf(paymentSetupPlatformResource.getTppDebtorDetails()))
			domesticConsentsTppResponse.getData().getInitiation().setDebtorAccount(null);

		/* Adding Prefix uk.obie. in creditor scheme in response */
		String creditorAccountScheme = domesticConsentsTppResponse.getData().getInitiation().getCreditorAccount()
				.getSchemeName();
		if (PaymentSetupConstants.prefixToAddSchemeNameList().contains(creditorAccountScheme)) {
			domesticConsentsTppResponse.getData().getInitiation().getCreditorAccount()
					.setSchemeName(PSD2Constants.OB_SCHEME_PREFIX.concat(creditorAccountScheme));
		}

		/* Adding Prefix uk.obie. in debtor scheme in response */
		if (!NullCheckUtils.isNullOrEmpty(domesticConsentsTppResponse.getData().getInitiation().getDebtorAccount())) {
			String debtorAccountScheme = domesticConsentsTppResponse.getData().getInitiation().getDebtorAccount()
					.getSchemeName();
			if (PaymentSetupConstants.prefixToAddSchemeNameList().contains(debtorAccountScheme)) {
				domesticConsentsTppResponse.getData().getInitiation().getDebtorAccount()
						.setSchemeName(PSD2Constants.OB_SCHEME_PREFIX.concat(debtorAccountScheme));
			}
		}
		
		/*
		 * If tppDebtorNameFlag is 'False' then DebtorAccount.name should not be sent to
		 * TPP.
		 */
		if (Boolean.valueOf(paymentSetupPlatformResource.getTppDebtorDetails())
				&& !Boolean.valueOf(paymentSetupPlatformResource.getTppDebtorNameDetails()))
			domesticConsentsTppResponse.getData().getInitiation().getDebtorAccount().setName(null);

		if (domesticConsentsTppResponse.getLinks() == null)
			domesticConsentsTppResponse.setLinks(new Links());

		domesticConsentsTppResponse.getLinks().setSelf(PispUtilities.populateLinks(
				paymentSetupPlatformResource.getPaymentConsentId(), methodType, reqHeaderAtrributes.getSelfUrl()));

		if (domesticConsentsTppResponse.getMeta() == null)
			domesticConsentsTppResponse.setMeta(new Meta());

		return domesticConsentsTppResponse;
	}

	@Override
	public CustomDSPConsentsPOSTRequest paymentConsentRequestTransformer(
			CustomDSPConsentsPOSTRequest paymentConsentRequest) {
		if (!NullCheckUtils
				.isNullOrEmpty(paymentConsentRequest.getData().getInitiation().getRequestedExecutionDateTime())) {
			paymentConsentRequest.getData().getInitiation()
					.setRequestedExecutionDateTime((pispDateUtility.transformDateTimeInRequest(
							paymentConsentRequest.getData().getInitiation().getRequestedExecutionDateTime())));
		}
		if (!NullCheckUtils.isNullOrEmpty(paymentConsentRequest.getData().getAuthorisation()) && !NullCheckUtils
				.isNullOrEmpty(paymentConsentRequest.getData().getAuthorisation().getCompletionDateTime())) {
			paymentConsentRequest.getData().getAuthorisation()
					.setCompletionDateTime((pispDateUtility.transformDateTimeInRequest(
							paymentConsentRequest.getData().getAuthorisation().getCompletionDateTime())));
		}
		return paymentConsentRequest;
	}

}