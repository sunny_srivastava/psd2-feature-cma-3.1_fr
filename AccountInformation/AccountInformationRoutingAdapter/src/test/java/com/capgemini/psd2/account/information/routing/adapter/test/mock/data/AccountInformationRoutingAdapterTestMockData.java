/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.account.information.routing.adapter.test.mock.data;

import java.util.ArrayList;
import java.util.List;

import com.capgemini.psd2.aisp.domain.OBAccount6;
import com.capgemini.psd2.aisp.domain.OBReadAccount6;
import com.capgemini.psd2.aisp.domain.OBReadAccount6Data;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountInformationResponse;
import com.capgemini.psd2.consent.domain.AccountMapping;

/**
 * The Class AccountInformationRoutingAdapterTestMockData.
 */
public class AccountInformationRoutingAdapterTestMockData {

	/** The mock accounts GET response. */
	public static PlatformAccountInformationResponse mockAccountsGETResponse;

	/** The mock accounts GET response accounts. */
	public static OBAccount6 mockAccountsGETResponseAccounts;

	/** The mock account mapping. */
	public static AccountMapping mockAccountMapping;

	/**
	 * Gets the account GET response.
	 *
	 * @return the account GET response
	 */
	public static PlatformAccountInformationResponse getAccountGETResponse() {

		PlatformAccountInformationResponse platformAccountInformationResponse = new PlatformAccountInformationResponse();
		OBReadAccount6Data obReadAccount2Data = new OBReadAccount6Data();

		List<OBAccount6> accountInfo = new ArrayList<>();
		obReadAccount2Data.setAccount(accountInfo);

		OBReadAccount6 obReadAccount2 = new OBReadAccount6();
		obReadAccount2.setData(obReadAccount2Data);

		platformAccountInformationResponse.setoBReadAccount6(obReadAccount2);

		return platformAccountInformationResponse;

	}

	
}
