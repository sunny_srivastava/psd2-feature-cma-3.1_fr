/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.account.information.routing.adapter.test.impl;

import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyString;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.account.information.routing.adapter.impl.AccountInformationRoutingAdapter;
import com.capgemini.psd2.account.information.routing.adapter.routing.AccountInformationAdapterFactory;
import com.capgemini.psd2.account.information.routing.adapter.test.adapter.AccountInformationTestRoutingAdapter;
import com.capgemini.psd2.aisp.adapter.AccountInformationAdapter;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountInformationResponse;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.token.Token;

/**
 * The Class AccountInformationRoutingAdapterTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class AccountInformationRoutingAdapterTest {

	/** The account information adapter factory. */
	@Mock
	private AccountInformationAdapterFactory accountInformationAdapterFactory;
	
	@Mock
	private RequestHeaderAttributes reqHeaderAtrributes;

	/** The account information routing adapter. */
	@InjectMocks
	private AccountInformationAdapter accountInformationRoutingAdapter = new AccountInformationRoutingAdapter();

	/**
	 * Sets the up.
	 *
	 * @throws Exception
	 *             the exception
	 */
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * Test retrieve account information success flow.
	 */
	@Test
	public void testRetrieveAccountInformationSuccessFlow() {
		AccountInformationAdapter accountInformationAdaptor = new AccountInformationTestRoutingAdapter();

		Mockito.when(accountInformationAdapterFactory.getAdapterInstance(anyString()))
				.thenReturn(accountInformationAdaptor);

		Token token = new Token();
		Map<String, String> map  = new HashMap<>();
		token.setSeviceParams(map);
		Mockito.when(reqHeaderAtrributes.getToken()).thenReturn(token);
		PlatformAccountInformationResponse platformAccountInformationResponse = accountInformationRoutingAdapter
				.retrieveAccountInformation(null, null);
		
		

		assertTrue(platformAccountInformationResponse.getoBReadAccount6().getData().getAccount().isEmpty());

	}

	@Test
	public void testRetrieveMultipleAccountInformationSuccessFlow() {
		AccountInformationAdapter accountInformationAdaptor = new AccountInformationTestRoutingAdapter();
		
		Mockito.when(accountInformationAdapterFactory.getAdapterInstance(anyString()))
				.thenReturn(accountInformationAdaptor);
		
		PlatformAccountInformationResponse platformAccountInformationResponse = accountInformationRoutingAdapter
				.retrieveMultipleAccountsInformation(null, null);
		
		assertTrue(platformAccountInformationResponse.getoBReadAccount6().getData().getAccount().isEmpty());
	}

}
