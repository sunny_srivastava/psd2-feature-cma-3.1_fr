/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.account.information.routing.adapter.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.capgemini.psd2.account.information.routing.adapter.routing.AccountInformationAdapterFactory;
import com.capgemini.psd2.aisp.adapter.AccountInformationAdapter;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountInformationResponse;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.utilities.NullCheckUtils;

/**
 * The Class AccountInformationRoutingAdapter.
 */
public class AccountInformationRoutingAdapter implements AccountInformationAdapter {

	/** The account information adapter factory. */
	@Autowired
	private AccountInformationAdapterFactory accountInformationAdapterFactory;
	
	@Autowired
	private RequestHeaderAttributes reqHeaderAtrributes;

	/** The default adapter. */
	@Value("${app.defaultAccountInformationAdapter}")
	private String defaultAdapter;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.capgemini.psd2.aisp.adapter.AccountInformationAdapter#
	 * retrieveAccountInformation(com.capgemini.psd2.aisp.domain.AccountMapping,
	 * java.util.Map)
	 */
	@Override
	public PlatformAccountInformationResponse retrieveAccountInformation(AccountMapping accountMapping, Map<String, String> params) {
		AccountInformationAdapter accountInformationAdapter = accountInformationAdapterFactory
				.getAdapterInstance(defaultAdapter);
		return accountInformationAdapter.retrieveAccountInformation(accountMapping, addHeaderParams(params));
	}

	@Override
	public PlatformAccountInformationResponse retrieveMultipleAccountsInformation(AccountMapping accountMapping,
			Map<String, String> params) {
		AccountInformationAdapter accountInformationAdapter = accountInformationAdapterFactory
				.getAdapterInstance(defaultAdapter);
		return accountInformationAdapter.retrieveMultipleAccountsInformation(accountMapping, addHeaderParams(params));
	}
	
	private Map<String, String> addHeaderParams(Map<String, String> mapParam) {
		Map<String,String> mapParamResult;
		if (NullCheckUtils.isNullOrEmpty(mapParam))
			mapParamResult = new HashMap<>(); 
		else mapParamResult=mapParam;

		if (null != reqHeaderAtrributes.getToken() && null != reqHeaderAtrributes.getToken().getSeviceParams()) {
			mapParamResult.put(PSD2Constants.CHANNEL_IN_REQ_HEADER,
					reqHeaderAtrributes.getToken().getSeviceParams().get(PSD2Constants.CHANNEL_NAME));
		}
		mapParamResult.put(PSD2Constants.USER_IN_REQ_HEADER, reqHeaderAtrributes.getPsuId());
		mapParamResult.put(PSD2Constants.CORRELATION_REQ_HEADER, reqHeaderAtrributes.getCorrelationId());
		mapParamResult.put(PSD2Constants.TENANT_ID, reqHeaderAtrributes.getTenantId());
		return mapParamResult;
	}
}