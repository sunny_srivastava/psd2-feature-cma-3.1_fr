/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.account.information.service.impl;


import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Service;

import com.capgemini.psd2.account.information.service.AccountInformationService;
import com.capgemini.psd2.aisp.account.mapping.adapter.AccountMappingAdapter;
import com.capgemini.psd2.aisp.adapter.AccountInformationAdapter;
import com.capgemini.psd2.aisp.adapter.AispConsentAdapter;
import com.capgemini.psd2.aisp.domain.Links;
import com.capgemini.psd2.aisp.domain.Meta;
import com.capgemini.psd2.aisp.domain.OBAccount6;
import com.capgemini.psd2.aisp.domain.OBAccount6Account;
import com.capgemini.psd2.aisp.domain.OBReadAccount6;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountInformationResponse;
import com.capgemini.psd2.aisp.validation.adapter.AISPCustomValidator;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.consent.domain.AispConsent;
import com.capgemini.psd2.exceptions.ErrorMapKeys;
import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.LoggerUtils;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.mask.DataMaskImpl;
import com.capgemini.psd2.mask.MaskOutput;
import com.capgemini.psd2.utilities.NullCheckUtils;

/**
 * The Class AccountInformationServiceImpl.
 */
@Service
@ConfigurationProperties(prefix = "app")
@Configuration
@EnableAutoConfiguration
public class AccountInformationServiceImpl implements AccountInformationService {

	/** The req header atrributes. */
	@Autowired
	private RequestHeaderAttributes reqHeaderAtrributes;

	@Autowired
	private AccountMappingAdapter accountMappingAdapter;

	/** The account information adapter. */
	@Autowired
	@Qualifier("accountInformationAdapterImpl")
	private AccountInformationAdapter accountInformationAdapter;

	/** The aisp consent adapter. */
	@Autowired
	private AispConsentAdapter aispConsentAdapter;
	
	@Autowired
	private DataMaskImpl dataMaskImpl;

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(AccountInformationServiceImpl.class);

	/** The logger utils. */
	@Autowired
	private LoggerUtils loggerUtils;

	@Value("${app.readPanPermissionName}")
	private String readPanPermissionName;

	@Value("${app.maskPanIdentification}")
	private Boolean maskPanIdentification;

	private Map<String, String> panMaskRules;
	
	
	@SuppressWarnings("rawtypes")
	@Autowired
	@Qualifier("accountsRoutingValidator")
	private AISPCustomValidator accountsCustomValidator;


	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.capgemini.psd2.account.information.service.AccountInformationService#
	 * retrieveAccountInformation(java.lang.String)
	 */
	@Override
	public OBReadAccount6 retrieveAccountInformation(String accountId) {

		validateAccountId(accountId);
		
		// Get consents by accountId and validate.
		AispConsent aispConsent = aispConsentAdapter.validateAndRetrieveConsentByAccountId(accountId,
				reqHeaderAtrributes.getToken().getConsentTokenData().getConsentId());

		// Get account mapping.
		AccountMapping accountMapping = accountMappingAdapter.retrieveAccountMappingDetails(accountId, aispConsent);

		Map<String, String> params = reqHeaderAtrributes.getToken().getSeviceParams();
		params.put(PSD2Constants.CMAVERSION, aispConsent.getCmaVersion());
		params.put(PSD2Constants.CO_RELATION_ID, reqHeaderAtrributes.getCorrelationId());
		
		// Retrieve account information.
		PlatformAccountInformationResponse platformAccountInformationResponse = accountInformationAdapter
				.retrieveAccountInformation(accountMapping, params);

		// Get CMA2 account information response.
		OBReadAccount6 obReadAccount = platformAccountInformationResponse.getoBReadAccount6();

		// update platform response.
		updatePlatformResponse(obReadAccount);

		// Apply platform and CMA2-swagger validations.
		accountsCustomValidator.validateResponseParams(obReadAccount);
		
		// conditional masking of PAN
		maskPanIdentification(obReadAccount);


		return obReadAccount;
	}

	@Override
	public OBReadAccount6 retrieveMultipleAccountsInformation() {

		// Get consents by accountId and validate.
		AispConsent aispConsent = aispConsentAdapter
				.retrieveConsent(reqHeaderAtrributes.getToken().getConsentTokenData().getConsentId());

		// Get account mapping.
		AccountMapping accountMapping = accountMappingAdapter.retrieveAccountMappingDetails(aispConsent);

		Map<String, String> params = reqHeaderAtrributes.getToken().getSeviceParams();
		params.put(PSD2Constants.CMAVERSION, aispConsent.getCmaVersion());
		params.put(PSD2Constants.CO_RELATION_ID, reqHeaderAtrributes.getCorrelationId());
		
		// Retrieve account information.
		PlatformAccountInformationResponse platformAccountInformationResponse = accountInformationAdapter
				.retrieveMultipleAccountsInformation(accountMapping, params);

		// Get CMA2 account information response.
		OBReadAccount6 obReadAccount = platformAccountInformationResponse.getoBReadAccount6();
		if (obReadAccount == null){
					throw PSD2Exception.populatePSD2Exception(
					new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_RESOURCE_NOTFOUND, ErrorMapKeys.NO_ACCOUNT_REQUEST_DATA_FOUND));
			
		}	
			
		// update platform response.
		updatePlatformResponse(obReadAccount);


		// Apply platform and CMA2-swagger validations.
		accountsCustomValidator.validateResponseParams(obReadAccount);
		
		
		// conditional masking of PAN
		maskPanIdentification(obReadAccount);

		return obReadAccount;
	}

	private void maskPanIdentification(OBReadAccount6 obReadAccount) {
		Set<Object> claims = reqHeaderAtrributes.getToken().getClaims().get("accounts");
		if (maskPanIdentification || !claims.contains(readPanPermissionName)) {
			List<OBAccount6> accountsList = obReadAccount.getData().getAccount();
			accountsList.forEach(acc -> {
				List<OBAccount6Account> schemeAccountsList = acc.getAccount();
				schemeAccountsList.forEach(schemeAcc -> {
					if ("PAN".equals(schemeAcc.getSchemeName()) || ("UK.OBIE.PAN".equals(schemeAcc.getSchemeName()))) {
						dataMaskImpl.mask(schemeAcc, panMaskRules, MaskOutput.UPDATE);
					}
				});
				acc.setAccount(schemeAccountsList);
			});
		}
	}

	private void updatePlatformResponse(OBReadAccount6 obReadAccount) {

		LOG.info("{\"Enter\":\"{}\",\"{}\"}",
				"com.capgemini.psd2.account.product.service.impl.updatePlatformResponse()",
				loggerUtils.populateLoggerData("updatePlatformResponse"));

		if (obReadAccount.getLinks() == null)
			obReadAccount.setLinks(new Links());
		obReadAccount.getLinks().setSelf(reqHeaderAtrributes.getSelfUrl());

		if (obReadAccount.getMeta() == null)
			obReadAccount.setMeta(new Meta());
		obReadAccount.getMeta().setTotalPages(1);

		LOG.info("{\"Exit\":\"{}\",\"{}\"}", "com.capgemini.psd2.account.product.service.impl.updatePlatformResponse()",
				loggerUtils.populateLoggerData("updatePlatformResponse"));
	}

	public Map<String, String> getPanMaskRules() {
		return panMaskRules;
	}

	public void setPanMaskRules(Map<String, String> panMaskRules) {
		this.panMaskRules = panMaskRules;
	}

	
	public void validateAccountId(String accountId) {
		if (!NullCheckUtils.isNullOrEmpty(accountId)) {
			accountsCustomValidator.validateUniqueId(accountId);
		} else {
			throw PSD2Exception.populatePSD2Exception(
					new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_FIELD_MISSING, ErrorMapKeys.NO_ACCOUNT_ID_FOUND));
		}
	}
	
}
