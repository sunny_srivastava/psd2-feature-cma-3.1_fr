/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.account.information.test.controller;


import static org.mockito.Matchers.anyString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.HashMap;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.capgemini.psd2.account.information.controller.AccountInformationController;
import com.capgemini.psd2.account.information.service.impl.AccountInformationServiceImpl;
import com.capgemini.psd2.account.information.test.mock.data.AccountInformationTestMockData;
import com.capgemini.psd2.aisp.domain.OBReadAccount6;
import com.capgemini.psd2.exceptions.OBPSD2ExceptionUtility;

/**
 * The Class AccountInformationControllerTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class AccountInformationControllerTest {

	/** The service. */
	@Mock
	private AccountInformationServiceImpl service;

	/** The mock mvc. */
	private MockMvc mockMvc;

	/** The controller. */
	@InjectMocks
	private AccountInformationController controller;

	/**
	 * Sets the up.
	 *
	 * @throws Exception
	 *             the exception
	 */
	
	/** The product response. */
	OBReadAccount6 productGETResponse = null;
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		this.mockMvc = MockMvcBuilders.standaloneSetup(controller).dispatchOptions(true).build();
		Map<String, String> map=new HashMap<>();
		map.put("SIGNATURE", "Something wrong with x-jws-signature request header");
		Map<String, String> specificErrorMessageMap=new HashMap<>();
		specificErrorMessageMap.put("NO_ACCOUNT_ID_FOUND", "signature_invalid_content");
		OBPSD2ExceptionUtility.genericErrorMessages.putAll(map);
		OBPSD2ExceptionUtility.specificErrorMessages.putAll(specificErrorMessageMap); 
	}

	/**
	 * Test retrieve account information success flow.
	 *
	 * @throws Exception
	 *             the exception
	 */
	@Test
	public void testRetrieveAccountInformationSuccessFlow() throws Exception {

		Mockito.when(service.retrieveAccountInformation(anyString()))
				.thenReturn(productGETResponse);

		this.mockMvc.perform(get("/accounts/{accountId}", "269c3ff5-d7f8-419b-a3b9-7136c5b4611a")
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
	}

	
	/**
	 * Test retrieve multiple accounts information success flow.
	 *
	 * @throws Exception
	 *             the exception
	 */
	@Test
	public void testRetrieveMultipleAccountInformationSuccessFlow() throws Exception {

		Mockito.when(service.retrieveMultipleAccountsInformation())
				.thenReturn(AccountInformationTestMockData.getMockExpectedAccountInformationResponse());

		this.mockMvc.perform(get("/accounts").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
	}

	

	@After
	public void tearDown() throws Exception {
		controller = null;
	}
}
