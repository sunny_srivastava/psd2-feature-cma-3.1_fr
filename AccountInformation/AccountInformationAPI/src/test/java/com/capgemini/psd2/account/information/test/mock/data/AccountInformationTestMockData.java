/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.account.information.test.mock.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.capgemini.psd2.aisp.domain.Links;
import com.capgemini.psd2.aisp.domain.Meta;
import com.capgemini.psd2.aisp.domain.OBAccount6;
import com.capgemini.psd2.aisp.domain.OBAccount6Account;
import com.capgemini.psd2.aisp.domain.OBBranchAndFinancialInstitutionIdentification50;
import com.capgemini.psd2.aisp.domain.OBReadAccount6;
import com.capgemini.psd2.aisp.domain.OBReadAccount6Data;
import com.capgemini.psd2.aisp.mongo.db.adapter.domain.AccountInformationCMA2;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountInformationResponse;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.consent.domain.AispConsent;
import com.capgemini.psd2.enums.ConsentStatusEnum;
import com.capgemini.psd2.logger.LoggerAttribute;
import com.capgemini.psd2.token.ConsentTokenData;
import com.capgemini.psd2.token.Token;

/**
 * The Class AccountInformationTestMockData.
 */
public class AccountInformationTestMockData {

	/** The mock account GET response. */
	public static OBReadAccount6 mockAccountGETResponse;

	/** The mock account GET response data. */
	public static OBAccount6 mockAccount;

	/** The mock account mapping. */
	public static AccountMapping mockAccountMapping;

	/** The mock consent. */
	public static AispConsent mockConsent;

	/** The mock token. */
	public static Token mockToken;

	/**
	 * Gets the token.
	 *
	 * @return the token
	 */
	public static Token getToken() {
		mockToken = new Token();
		ConsentTokenData consentTokenData = new ConsentTokenData();
		consentTokenData.setConsentExpiry("1509348259877L");
		consentTokenData.setConsentId("12345");
		mockToken.setConsentTokenData(consentTokenData);
		Map<String, Set<Object>> claimsMap = new HashMap<>();

		Set<Object> setClaims = new HashSet<>();
		setClaims.add("ReadPAN");
		claimsMap.put("accounts", setClaims);
		mockToken.setClaims(claimsMap);
		Map<String,String> map=new HashMap<>();
		mockToken.setSeviceParams(map);
		return mockToken;
	}

	/**
	 * Gets the account GET response.
	 *
	 * @return the account GET response
	 */
	public static OBReadAccount6 getAccountGETResponse() {
		mockAccountGETResponse = new OBReadAccount6();
		List<OBAccount6> accountsGETResponseAccountsList = new ArrayList<>();
		accountsGETResponseAccountsList.add(getAccount1());
		OBReadAccount6Data mockData = new OBReadAccount6Data();
		mockData.setAccount(accountsGETResponseAccountsList);
		// mockAccountGETResponse.setData(mockData);
		return mockAccountGETResponse;
	}

	/**
	 * Gets the consent mock data.
	 *
	 * @return the consent mock data
	 */
	public static AispConsent getConsentMockData() {
		mockConsent = new AispConsent();
		mockConsent.setTppCId("tpp123");
		mockConsent.setPsuId("user123");
		mockConsent.setConsentId("a1e590ad-73dc-453a-841e-2a2ef055e878");

		List<AccountDetails> selectedAccounts = new ArrayList<>();

		AccountDetails accountRequest = new AccountDetails();
		accountRequest.setAccountId("269c3ff5-d7f8-419b-a3b9-7136c5b4611a");
		accountRequest.setAccountNSC("SC802001");
		accountRequest.setAccountNumber("10203345");
		selectedAccounts.add(accountRequest);
		mockConsent.setAccountDetails(selectedAccounts);

		return mockConsent;
	}

	/**
	 * Gets the account GET response data.
	 *
	 * @return the account GET response data
	 */
	public static OBAccount6 getAccount1() {
		mockAccount = new OBAccount6();
		List<OBAccount6Account> accountList = new ArrayList<>();
		OBAccount6Account acct = new OBAccount6Account();
		acct.setIdentification("10203345");
		acct.setSchemeName("UK.OBIE.IBAN");
		acct.setName("Lorem");
		mockAccount.setAccount(accountList);

		OBBranchAndFinancialInstitutionIdentification50 servicer = new OBBranchAndFinancialInstitutionIdentification50();
		servicer.setIdentification("SC802001");
		servicer.setSchemeName("BICFI");
		mockAccount.setServicer(servicer);
		return mockAccount;
	}

	private static OBAccount6 getAccount2() {
		mockAccount = new OBAccount6();
		List<OBAccount6Account> accountList = new ArrayList<>();
		OBAccount6Account acct = new OBAccount6Account();
		acct.setIdentification("76528776");
		acct.setSchemeName("UK.OBIE.IBAN");
		acct.setName("Mr Kevin");
		mockAccount.setAccount(accountList);

		OBBranchAndFinancialInstitutionIdentification50 servicer = new OBBranchAndFinancialInstitutionIdentification50();
		servicer.setIdentification("SC802001");
		servicer.setSchemeName("BICFI");
		mockAccount.setServicer(servicer);
		return mockAccount;
	}

	private static OBAccount6 getAccount3() {
		mockAccount = new OBAccount6();
		List<OBAccount6Account> accountList = new ArrayList<>();
		OBAccount6Account acct = new OBAccount6Account();
		acct.setIdentification("25369621");
		acct.setSchemeName("UK.OBIE.IBAN");
		acct.setName("Mr Jack");
		mockAccount.setAccount(accountList);

		OBBranchAndFinancialInstitutionIdentification50 servicer = new OBBranchAndFinancialInstitutionIdentification50();
		servicer.setIdentification("SC802001");
		servicer.setSchemeName("BICFI");
		mockAccount.setServicer(servicer);
		return mockAccount;
	}

	private static OBAccount6 getAccount4() {
		mockAccount = new OBAccount6();
		List<OBAccount6Account> accountList = new ArrayList<>();
		OBAccount6Account acct = new OBAccount6Account();
		acct.setIdentification("9999");
		acct.setSchemeName("UK.OBIE.IBAN");
		acct.setName("Mr Sam");
		mockAccount.setAccount(accountList);

		OBBranchAndFinancialInstitutionIdentification50 servicer = new OBBranchAndFinancialInstitutionIdentification50();
		servicer.setIdentification("SC802001");
		servicer.setSchemeName("BICFI");
		mockAccount.setServicer(servicer);
		return mockAccount;
	}

	private static OBAccount6 getAccount5() {
		mockAccount = new OBAccount6();
		List<OBAccount6Account> accountList = new ArrayList<>();
		OBAccount6Account acct = new OBAccount6Account();
		acct.setIdentification("888");
		acct.setSchemeName("UK.OBIE.IBAN");
		acct.setName("Mr Ham");
		mockAccount.setAccount(accountList);

		OBBranchAndFinancialInstitutionIdentification50 servicer = new OBBranchAndFinancialInstitutionIdentification50();
		servicer.setIdentification("SC802001");
		servicer.setSchemeName("BICFI");
		mockAccount.setServicer(servicer);
		return mockAccount;
	}

	/**
	 * Gets the account mapping.
	 *
	 * @return the account mapping
	 */
	public static AccountMapping getAccountMapping() {
		mockAccountMapping = new AccountMapping();
		mockAccountMapping.setTppCID("tpp123");
		mockAccountMapping.setPsuId("user123");
		mockAccountMapping.setCorrelationId("95212678-4d0c-450f-8268-25dcfc95bfa1");
		List<AccountDetails> selectedAccounts = new ArrayList<>();

		AccountDetails accountRequest1 = new AccountDetails();
		accountRequest1.setAccountId("269c3ff5-d7f8-419b-a3b9-7136c5b4611a");
		accountRequest1.setAccountNSC("SC802001");
		accountRequest1.setAccountNumber("10203345");

		AccountDetails accountRequest2 = new AccountDetails();
		accountRequest2.setAccountId("bf3e700e-25c9-475c-ab37-f2069af8b79a");
		accountRequest2.setAccountNSC("SC802001");
		accountRequest2.setAccountNumber("25369621");

		selectedAccounts.add(accountRequest1);
		selectedAccounts.add(accountRequest2);

		mockAccountMapping.setAccountDetails(selectedAccounts);
		return mockAccountMapping;
	}

	public static OBReadAccount6 getMockExpectedAccountInformationResponse() {
		PlatformAccountInformationResponse platformAccountInformationRespnse = new PlatformAccountInformationResponse();
		OBReadAccount6Data obReadAccount2Data = new OBReadAccount6Data();
		OBReadAccount6 obReadAccount2 = new OBReadAccount6();
		List<AccountInformationCMA2> mockAccountInfoList = new ArrayList<>();
		mockAccountInfoList.add(new AccountInformationCMA2());
		mockAccountInfoList.get(0).setAccountId("f4483fda-81be-4873-b4a6-20375b7f55c1");

		List<OBAccount6> obAccount2List = new ArrayList<>();
		obAccount2List.addAll(mockAccountInfoList);

		obReadAccount2Data.setAccount(obAccount2List);
		obReadAccount2.setData(obReadAccount2Data);
		platformAccountInformationRespnse.setoBReadAccount6(obReadAccount2);

		return obReadAccount2;
	}

	public static AispConsent getMockAispConsent() {
		AispConsent aispConsent = new AispConsent();
		aispConsent.setTppCId("6443e15975554bce8099e35b88b40465");
		aispConsent.setPsuId("88888888");
		aispConsent.setConsentId("39032481");
		aispConsent.setAccountRequestId("ae8c4441-f783-4e80-8810-254241bed98c");
		aispConsent.setChannelId("B365");
		aispConsent.setStartDate("2017-08-09T06:44:31.250Z");
		aispConsent.setEndDate("2018-09-02T00:00:00.875");
		aispConsent.setTransactionFromDateTime("2015-05-03T00:00:00.800");
		aispConsent.setTransactionToDateTime("2018-12-03T00:00:00.345");
		aispConsent.setStatus(ConsentStatusEnum.AUTHORISED);

		List<AccountDetails> selectedAispConsentAccountDetails = new ArrayList<>();
		AccountDetails accountRequest = new AccountDetails();

		accountRequest.setAccountId("f4483fda-81be-4873-b4a6-20375b7f55c1");
		accountRequest.setAccountNSC("903779");
		accountRequest.setAccountNumber("76528776");

		selectedAispConsentAccountDetails.add(accountRequest);
		aispConsent.setAccountDetails(selectedAispConsentAccountDetails);

		return aispConsent;
	}

	/**
	 * Gets the mock account mapping.
	 *
	 * @return the mock account mapping
	 */
	public static AccountMapping getMockAccountMapping() {
		AccountMapping mapping = new AccountMapping();
		mapping.setTppCID("6443e15975554bce8099e35b88b40465");
		mapping.setPsuId("88888888");

		List<AccountDetails> selectedAccounts = new ArrayList<>();
		AccountDetails accountRequest = new AccountDetails();

		accountRequest.setAccountId("f4483fda-81be-4873-b4a6-20375b7f55c1");
		accountRequest.setAccountNSC("903779");
		accountRequest.setAccountNumber("76528776");
		selectedAccounts.add(accountRequest);
		mapping.setAccountDetails(selectedAccounts);
		return mapping;
	}

	public static PlatformAccountInformationResponse getMockPlatformAccountInformationResponseWithLinksMeta() {

		PlatformAccountInformationResponse platformAccountInformationRespnse = new PlatformAccountInformationResponse();
		OBReadAccount6Data obReadAccount2Data = new OBReadAccount6Data();

		OBReadAccount6 obReadAccount2 = new OBReadAccount6();
		List<AccountInformationCMA2> mockAccountInfoList = new ArrayList<>();
		mockAccountInfoList.add(new AccountInformationCMA2());
		mockAccountInfoList.get(0).setAccountId("f4483fda-81be-4873-b4a6-20375b7f55c1");
		List<OBAccount6Account> data2accountlist = new ArrayList<>();
		mockAccountInfoList.get(0).setAccount(data2accountlist);
		OBAccount6Account data2account = new OBAccount6Account();
		data2account.setIdentification("12345678");
		data2accountlist.add(data2account);
		mockAccountInfoList.get(0).getAccount().get(0).setSchemeName("UK.OBIE.PAN");

		List<OBAccount6> obAccount2List = new ArrayList<>();
		obAccount2List.addAll(mockAccountInfoList);

		obReadAccount2Data.setAccount(obAccount2List);
		obReadAccount2.setData(obReadAccount2Data);

		Links links = new Links();
		obReadAccount2.setLinks(links);
		Meta meta = new Meta();
		obReadAccount2.setMeta(meta);
		platformAccountInformationRespnse.setoBReadAccount6(obReadAccount2);

		return platformAccountInformationRespnse;

	}
	//
	public static PlatformAccountInformationResponse getMockPlatformAccountInformationResponseWithLinksMetaNull() {

		PlatformAccountInformationResponse platformAccountInformationRespnse = new PlatformAccountInformationResponse();
		OBReadAccount6Data obReadAccount2Data = new OBReadAccount6Data();

		OBReadAccount6 obReadAccount2 = new OBReadAccount6();
		List<AccountInformationCMA2> mockAccountInfoList = new ArrayList<>();
		mockAccountInfoList.add(new AccountInformationCMA2());
		mockAccountInfoList.get(0).setAccountId("f4483fda-81be-4873-b4a6-20375b7f55c1");
		List<OBAccount6Account> data2accountlist = new ArrayList<>();
		mockAccountInfoList.get(0).setAccount(data2accountlist);
		OBAccount6Account data2account = new OBAccount6Account();
		data2account.setIdentification("12345678");
		data2accountlist.add(data2account);
		mockAccountInfoList.get(0).getAccount().get(0).setSchemeName("UK.OBIE.PAN");

		List<OBAccount6> obAccount2List = new ArrayList<>();
		obAccount2List.addAll(mockAccountInfoList);

		obReadAccount2Data.setAccount(obAccount2List);
		obReadAccount2.setData(obReadAccount2Data);

		Links links = null;
		//obReadAccount2.setLinks(links);
		Meta meta = null;
		//obReadAccount2.setMeta(meta);
		platformAccountInformationRespnse.setoBReadAccount6(obReadAccount2);

		return platformAccountInformationRespnse;

	}
	//

	public static LoggerAttribute getMockLoggerData() {
		LoggerAttribute x = new LoggerAttribute();
		x.setApiId("testApiID");
		return x;
	}

	public static Object getMockExpectedAccountInformationResponseWithLinksMeta() {

		OBReadAccount6Data obReadAccount2Data = new OBReadAccount6Data();
		OBReadAccount6 obReadAccount2 = new OBReadAccount6();
		List<AccountInformationCMA2> mockAccountInfoList = new ArrayList<>();

		mockAccountInfoList.add(new AccountInformationCMA2());
		mockAccountInfoList.get(0).setAccountId("f4483fda-81be-4873-b4a6-20375b7f55c1");

		List<OBAccount6> obAccount2List = new ArrayList<>();
		obAccount2List.addAll(mockAccountInfoList);

		obReadAccount2Data.setAccount(obAccount2List);
		obReadAccount2.setData(obReadAccount2Data);
		Links links = new Links();
		Meta meta = new Meta();
		meta.setTotalPages(1);
		obReadAccount2.setLinks(links);
		obReadAccount2.setMeta(meta);

		return obReadAccount2;
	}

	public static PlatformAccountInformationResponse getMockPlatformAccountInformationResponseWithoutLinksMeta() {

		PlatformAccountInformationResponse platformAccountInformationRespnse = new PlatformAccountInformationResponse();
		OBReadAccount6Data obReadAccount2Data = new OBReadAccount6Data();
		OBReadAccount6 obReadAccount2 = new OBReadAccount6();
		List<AccountInformationCMA2> mockAccountInfoList = new ArrayList<>();
		mockAccountInfoList.add(new AccountInformationCMA2());
		mockAccountInfoList.get(0).setAccountId("f4483fda-81be-4873-b4a6-20375b7f55c1");
		List<OBAccount6> obAccount2List = new ArrayList<>();
		obAccount2List.addAll(mockAccountInfoList);
		obReadAccount2Data.setAccount(obAccount2List);
		obReadAccount2.setData(obReadAccount2Data);

		platformAccountInformationRespnse.setoBReadAccount6(obReadAccount2);

		return platformAccountInformationRespnse;

	}

}
