/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.account.information.test.service.impl;

import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.account.information.service.impl.AccountInformationServiceImpl;
import com.capgemini.psd2.account.information.test.mock.data.AccountInformationTestMockData;
import com.capgemini.psd2.aisp.account.mapping.adapter.AccountMappingAdapter;
import com.capgemini.psd2.aisp.adapter.AccountInformationAdapter;
import com.capgemini.psd2.aisp.adapter.AispConsentAdapter;
import com.capgemini.psd2.aisp.domain.OBReadAccount6;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountInformationResponse;
import com.capgemini.psd2.aisp.validation.adapter.impl.AccountInformationValidatorImpl;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.LoggerUtils;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.mask.DataMaskImpl;

/**
 * The Class AccountInformationServiceImplTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class AccountInformationServiceImplTest {

	/** The account information adapter. */
	@Mock
	private DataMaskImpl dataMaskImpl;
	
	@Mock
	private AccountInformationAdapter accountInformationAdapter;

	/** The consent mapping adapter. */
	@Mock
	private AispConsentAdapter aispConsentAdapter;

	/** The request header attributes. */
	@Mock
	private RequestHeaderAttributes requestHeaderAttributes;

	/** The service. */
	@InjectMocks
	private AccountInformationServiceImpl service;

	/** The mapping adapter */
	@Mock
	private AccountMappingAdapter accountMappingAdapter;

	@Mock
	private LoggerUtils loggerUtils;
	
	@Mock
	private AccountInformationValidatorImpl accInformationValidatorImpl;
	

	/**
	 * Sets the up.
	 *
	 * @throws Exception
	 *             the exception
	 */
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		when(requestHeaderAttributes.getToken()).thenReturn(AccountInformationTestMockData.getToken());
		/*
		 * when(aispConsentAdapter.retrieveAccountMapping(anyString()))
		 * .thenReturn(AccountInformationTestMockData.getMockAccountMapping());
		 */

	}

	/**
	 * Test retrieve account information success flow With Links And Data Null.
	 */
	
	@Test
	public void testRetrieveAccountInformationSuccessFlowWithLinksMeta() {
		Mockito.when(aispConsentAdapter.validateAndRetrieveConsentByAccountId(anyString(), anyString()))
				.thenReturn(AccountInformationTestMockData.getMockAispConsent());

		Mockito.when(accountMappingAdapter.retrieveAccountMappingDetails(anyString(), anyObject()))
				.thenReturn(AccountInformationTestMockData.getMockAccountMapping());

		Mockito.when(accountInformationAdapter.retrieveAccountInformation(anyObject(), anyObject()))
				.thenReturn(AccountInformationTestMockData.getMockPlatformAccountInformationResponseWithLinksMeta());

		Mockito.when(loggerUtils.populateLoggerData(anyString()))
				.thenReturn(AccountInformationTestMockData.getMockLoggerData());

		ReflectionTestUtils.setField(service, "maskPanIdentification", false);
		
		accInformationValidatorImpl.validateResponseParams(anyObject());

		OBReadAccount6 actualResponse = service.retrieveAccountInformation("f4483fda-81be-4873-b4a6-20375b7f55c1");

	}

	
	@Test
	public void testRetriveAccountInformationSuccessFlowWithoutLinksMeta() {
		Mockito.when(aispConsentAdapter.validateAndRetrieveConsentByAccountId(anyString(), anyString()))
				.thenReturn(AccountInformationTestMockData.getMockAispConsent());

		Mockito.when(accountMappingAdapter.retrieveAccountMappingDetails(anyString(), anyObject()))
				.thenReturn(AccountInformationTestMockData.getMockAccountMapping());

		Mockito.when(accountInformationAdapter.retrieveAccountInformation(anyObject(), anyObject()))
				.thenReturn(AccountInformationTestMockData.getMockPlatformAccountInformationResponseWithLinksMetaNull());  //getMockPlatformAccountInformationResponseWithLinksMeta()

		Mockito.when(loggerUtils.populateLoggerData(anyString()))
				.thenReturn(AccountInformationTestMockData.getMockLoggerData());
		
		ReflectionTestUtils.setField(service, "maskPanIdentification", false);
		
		accInformationValidatorImpl.validateResponseParams(anyObject());

		OBReadAccount6 actualResponse = service.retrieveAccountInformation("f4483fda-81be-4873-b4a6-20375b7f55c1");

	}
	
	@Test
	public void testRetrieveMutiAccountInfo() {
		Mockito.when(aispConsentAdapter.retrieveConsent(anyString()))
		.thenReturn(AccountInformationTestMockData.getMockAispConsent());

	Mockito.when(accountMappingAdapter.retrieveAccountMappingDetails(anyObject()))
			.thenReturn(AccountInformationTestMockData.getMockAccountMapping());
	
	Mockito.when(accountInformationAdapter.retrieveMultipleAccountsInformation(anyObject(), anyObject()))
	.thenReturn(AccountInformationTestMockData.getMockPlatformAccountInformationResponseWithLinksMeta());      
	
	ReflectionTestUtils.setField(service, "maskPanIdentification", false);
	
	service.retrieveMultipleAccountsInformation();
	}
	
	@Test
	public void testGetterSetters() {
		service.getPanMaskRules();
		Map<String,String> map=new HashMap<>();
		service.setPanMaskRules(map);
	}

	@Test(expected=PSD2Exception.class)
	public void testRetrieveMultiAccForException() {
		
		Mockito.when(aispConsentAdapter.retrieveConsent(anyString()))
		.thenReturn(AccountInformationTestMockData.getMockAispConsent());

		Mockito.when(accountMappingAdapter.retrieveAccountMappingDetails(anyObject()))
				.thenReturn(AccountInformationTestMockData.getMockAccountMapping());
		
		Mockito.when(accountInformationAdapter.retrieveMultipleAccountsInformation(anyObject(), anyObject()))
		.thenReturn(new PlatformAccountInformationResponse());      
		
		service.retrieveMultipleAccountsInformation();
	}
	/**
	 * Tear down.
	 *
	 * @throws Exception
	 *             the exception
	 */
	@After
	public void tearDown() throws Exception {
		service = null;
	}
	
	@Test(expected = PSD2Exception.class)
	public void testValidateAccountIdExceptionFlow() {
				service.validateAccountId(null);
	
	}
}
