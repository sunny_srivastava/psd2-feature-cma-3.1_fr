/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.account.information.mongo.db.adapter.test.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyString;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.account.information.mongo.db.adapter.impl.AccountInformationMongoDbAdaptorImpl;
import com.capgemini.psd2.account.information.mongo.db.adapter.repository.AccountInformationRepository2;
import com.capgemini.psd2.account.information.mongo.db.adapter.test.mock.data.AccountInformationAdapterTestMockData;
import com.capgemini.psd2.aisp.domain.OBAccount6Account;
import com.capgemini.psd2.aisp.domain.OBBranchAndFinancialInstitutionIdentification50;
import com.capgemini.psd2.aisp.mongo.db.adapter.domain.AccountInformationCMA2;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountInformationResponse;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.exceptions.OBPSD2ExceptionUtility;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.LoggerUtils;
import com.capgemini.psd2.utilities.SandboxValidationUtility;
import com.capgemini.psd2.validator.PSD2Validator;

/**
 * The Class AccountInformationMongoDBAdapterImplTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class AccountInformationMongoDBAdapterImplTest {

	/** The account info repo. */
	@Mock
	private AccountInformationRepository2 accountInfoRepo2;

	@Mock
	private PSD2Validator validator;
	
	@Mock
	private SandboxValidationUtility utility;

	/**
	 * Sets the up.
	 *
	 * @throws Exception
	 *             the exception
	 */
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);

Map<String, String> map=new HashMap<>();
		map.put("SIGNATURE", "Something wrong with x-jws-signature request header");
		Map<String, String> specificErrorMessageMap=new HashMap<>();
		specificErrorMessageMap.put("NO_ACCOUNT_ID_FOUND", "signature_invalid_content");
		OBPSD2ExceptionUtility.genericErrorMessages.putAll(map);
		OBPSD2ExceptionUtility.specificErrorMessages.putAll(specificErrorMessageMap);
	}

	/** The account information mongo DB adapter impl. */
	@InjectMocks
	private AccountInformationMongoDbAdaptorImpl accountInformationMongoDBAdapterImpl;

	/**
	 * Test retrieve Single account information success flow.
	 */
	@Mock
	LoggerUtils loggerUtils;

	@Test
	public void testRetrieveAccountInformationSuccessFlow() {

		AccountMapping accountMapping = new AccountMapping();
		List<AccountDetails> accountDetailsList = new ArrayList<>();
		AccountDetails accountDetails = new AccountDetails();
		Map<String, String> params = new HashMap<>();

		accountDetails.setAccountId("f4483fda-81be-4873-b4a6-20375b7f55c1");
		accountDetailsList.add(accountDetails);
		accountMapping.setAccountDetails(accountDetailsList);
		
		List<AccountInformationCMA2> accountInfoList = new ArrayList<>();
		AccountInformationCMA2 obj=new AccountInformationCMA2();
		List<OBAccount6Account> accountList=new ArrayList<>();
		OBAccount6Account account2=new OBAccount6Account();
		account2.setSchemeName("UK.OBIE.SortCodeAccountNumber");
		account2.setIdentification("123");
		account2.setSecondaryIdentification("45457");
		account2.setName("avin");
		accountList.add(account2);
		obj.setAccount(accountList);
		accountInfoList.add(obj);
		
		Mockito.when(utility.isValidPsuId(accountMapping.getPsuId())).thenReturn(false);

		Mockito.when(accountInfoRepo2.findByAccountNumberAndNsc(anyString(), anyString())).thenReturn(accountInfoList);

		Mockito.when(loggerUtils.populateLoggerData(anyString()))
				.thenReturn(AccountInformationAdapterTestMockData.getMockLoggerData());
		ReflectionTestUtils.setField(accountInformationMongoDBAdapterImpl, "maskSortCodeServicerValidation",true);
		
		PlatformAccountInformationResponse acctualAccountInforesponse = accountInformationMongoDBAdapterImpl
				.retrieveAccountInformation(accountMapping, params);


	}

	@Test(expected=PSD2Exception.class)
	public void testRetrieveAccountInformationSuccessFlow2() {

		AccountMapping accountMapping = new AccountMapping();
		List<AccountDetails> accountDetailsList = new ArrayList<>();
		AccountDetails accountDetails = new AccountDetails();
		Map<String, String> params = new HashMap<>();

		accountDetails.setAccountId("f4483fda-81be-4873-b4a6-20375b7f55c1");
		accountDetailsList.add(accountDetails);
		accountMapping.setAccountDetails(accountDetailsList);
		
		List<AccountInformationCMA2> accountInfoList = new ArrayList<>();
		AccountInformationCMA2 obj=new AccountInformationCMA2();
		List<OBAccount6Account> accountList=new ArrayList<>();
		OBAccount6Account account2=new OBAccount6Account();
		account2.setSchemeName("UK.OBIE.IBAN");
		account2.setIdentification("123");
		account2.setSecondaryIdentification("45457");
		account2.setName("avin");
		accountList.add(account2);
		obj.setAccount(accountList);
		accountInfoList.add(obj);
		
		Mockito.when(utility.isValidPsuId(accountMapping.getPsuId())).thenReturn(false);

		Mockito.when(accountInfoRepo2.findByAccountNumberAndNsc(anyString(), anyString())).thenReturn(accountInfoList);

		Mockito.when(loggerUtils.populateLoggerData(anyString()))
				.thenReturn(AccountInformationAdapterTestMockData.getMockLoggerData());
		ReflectionTestUtils.setField(accountInformationMongoDBAdapterImpl, "maskSortCodeServicerValidation",true);
		
		PlatformAccountInformationResponse acctualAccountInforesponse = accountInformationMongoDBAdapterImpl
				.retrieveAccountInformation(accountMapping, params);


	}
	
	@Test(expected=PSD2Exception.class)
	public void testRetrieveAccountInformationSuccessFlow1() {

		AccountMapping accountMapping = new AccountMapping();
		List<AccountDetails> accountDetailsList = new ArrayList<>();
		AccountDetails accountDetails = new AccountDetails();
		Map<String, String> params = new HashMap<>();

		accountDetails.setAccountId("f4483fda-81be-4873-b4a6-20375b7f55c1");
		accountDetailsList.add(accountDetails);
		accountMapping.setAccountDetails(accountDetailsList);
		
		List<AccountInformationCMA2> accountInfoList = new ArrayList<>();
		AccountInformationCMA2 obj=new AccountInformationCMA2();
		List<OBAccount6Account> accountList=new ArrayList<>();
		OBAccount6Account account2=new OBAccount6Account();
		account2.setSchemeName("UK.OBIE.SortCodeAccountNumber");
		account2.setIdentification("123");
		account2.setSecondaryIdentification("45457");
		account2.setName("avin");
		accountList.add(account2);
		obj.setAccount(accountList);
		obj.setServicer(new OBBranchAndFinancialInstitutionIdentification50());
		accountInfoList.add(obj);
		
		Mockito.when(utility.isValidPsuId(accountMapping.getPsuId())).thenReturn(false);

		Mockito.when(accountInfoRepo2.findByAccountNumberAndNsc(anyString(), anyString())).thenReturn(accountInfoList);

		Mockito.when(loggerUtils.populateLoggerData(anyString()))
				.thenReturn(AccountInformationAdapterTestMockData.getMockLoggerData());
		ReflectionTestUtils.setField(accountInformationMongoDBAdapterImpl, "maskSortCodeServicerValidation",true);
		
		PlatformAccountInformationResponse acctualAccountInforesponse = accountInformationMongoDBAdapterImpl
				.retrieveAccountInformation(accountMapping, params);


	}
	@Test
	public void testRetrieveAccountInformationSuccessFlow3() {

		AccountMapping accountMapping = new AccountMapping();
		List<AccountDetails> accountDetailsList = new ArrayList<>();
		AccountDetails accountDetails = new AccountDetails();
		Map<String, String> params = new HashMap<>();

		accountDetails.setAccountId("f4483fda-81be-4873-b4a6-20375b7f55c1");
		accountDetailsList.add(accountDetails);
		accountMapping.setAccountDetails(accountDetailsList);
		
		List<AccountInformationCMA2> accountInfoList = new ArrayList<>();
		AccountInformationCMA2 obj=new AccountInformationCMA2();
		List<OBAccount6Account> accountList=new ArrayList<>();
		OBAccount6Account account2=new OBAccount6Account();
		account2.setSchemeName("SortCodeAccountNumber");
		account2.setIdentification("123");
		account2.setSecondaryIdentification("45457");
		account2.setName("avin");
		accountList.add(account2);
		obj.setAccount(accountList);
		obj.setServicer(null);
		accountInfoList.add(obj);
		
		Mockito.when(utility.isValidPsuId(accountMapping.getPsuId())).thenReturn(false);

		Mockito.when(accountInfoRepo2.findByAccountNumberAndNsc(anyString(), anyString())).thenReturn(accountInfoList);

		Mockito.when(loggerUtils.populateLoggerData(anyString()))
				.thenReturn(AccountInformationAdapterTestMockData.getMockLoggerData());
		ReflectionTestUtils.setField(accountInformationMongoDBAdapterImpl, "maskSortCodeServicerValidation",true);
		
		PlatformAccountInformationResponse acctualAccountInforesponse = accountInformationMongoDBAdapterImpl
				.retrieveAccountInformation(accountMapping, params);


	}
	
	
	@Test
	public void testRetrieveAccountInformationSuccessFlow4() {

		AccountMapping accountMapping = new AccountMapping();
		List<AccountDetails> accountDetailsList = new ArrayList<>();
		AccountDetails accountDetails = new AccountDetails();
		Map<String, String> params = new HashMap<>();

		accountDetails.setAccountId("f4483fda-81be-4873-b4a6-20375b7f55c1");
		accountDetailsList.add(accountDetails);
		accountMapping.setAccountDetails(accountDetailsList);
		
		List<AccountInformationCMA2> accountInfoList = new ArrayList<>();
		AccountInformationCMA2 obj=new AccountInformationCMA2();
		List<OBAccount6Account> accountList=new ArrayList<>();
		OBAccount6Account account2=new OBAccount6Account();
		account2.setSchemeName("IBAN");
		account2.setIdentification("123");
		account2.setSecondaryIdentification("45457");
		account2.setName("avin");
		accountList.add(account2);
		obj.setAccount(accountList);
		obj.setServicer(new OBBranchAndFinancialInstitutionIdentification50());
		accountInfoList.add(obj);
		
		Mockito.when(utility.isValidPsuId(accountMapping.getPsuId())).thenReturn(false);

		Mockito.when(accountInfoRepo2.findByAccountNumberAndNsc(anyString(), anyString())).thenReturn(accountInfoList);

		Mockito.when(loggerUtils.populateLoggerData(anyString()))
				.thenReturn(AccountInformationAdapterTestMockData.getMockLoggerData());
		ReflectionTestUtils.setField(accountInformationMongoDBAdapterImpl, "maskSortCodeServicerValidation",true);
		
		PlatformAccountInformationResponse acctualAccountInforesponse = accountInformationMongoDBAdapterImpl
				.retrieveAccountInformation(accountMapping, params);


	}
	
	
	@Test
	public void testRetrieveAccountInformationSuccessFlow5() {

		AccountMapping accountMapping = new AccountMapping();
		List<AccountDetails> accountDetailsList = new ArrayList<>();
		AccountDetails accountDetails = new AccountDetails();
		Map<String, String> params = new HashMap<>();

		accountDetails.setAccountId("f4483fda-81be-4873-b4a6-20375b7f55c1");
		accountDetailsList.add(accountDetails);
		accountMapping.setAccountDetails(accountDetailsList);
		
		List<AccountInformationCMA2> accountInfoList = new ArrayList<>();
		AccountInformationCMA2 obj=new AccountInformationCMA2();
		List<OBAccount6Account> accountList=new ArrayList<>();
		OBAccount6Account account2=new OBAccount6Account();
		account2.setSchemeName("UK.OBIE.SortCodeAccountNumber");
		account2.setIdentification("123");
		account2.setSecondaryIdentification("45457");
		account2.setName("avin");
		accountList.add(account2);
		obj.setAccount(accountList);
		accountInfoList.add(obj);
		
		Mockito.when(utility.isValidPsuId(accountMapping.getPsuId())).thenReturn(false);

		Mockito.when(accountInfoRepo2.findByAccountNumberAndNsc(anyString(), anyString())).thenReturn(accountInfoList);

		Mockito.when(loggerUtils.populateLoggerData(anyString()))
				.thenReturn(AccountInformationAdapterTestMockData.getMockLoggerData());
		ReflectionTestUtils.setField(accountInformationMongoDBAdapterImpl, "maskSortCodeServicerValidation",false);
		
		PlatformAccountInformationResponse acctualAccountInforesponse = accountInformationMongoDBAdapterImpl
				.retrieveAccountInformation(accountMapping, params);


	}
	
	@Test(expected=PSD2Exception.class)
	public void testRetrieveAccountInformationExceptionFlow() {

		AccountMapping accountMapping = new AccountMapping();
		List<AccountDetails> accountDetailsList = new ArrayList<>();
		AccountDetails accountDetails = new AccountDetails();
		Map<String, String> params = new HashMap<>();

		accountDetails.setAccountId("f4483fda-81be-4873-b4a6-20375b7f55c1");
		accountDetailsList.add(accountDetails);
		accountMapping.setAccountDetails(accountDetailsList);
		
		List<AccountInformationCMA2> accountInfoList = new ArrayList<>();
		AccountInformationCMA2 obj=new AccountInformationCMA2();
		List<OBAccount6Account> accountList=new ArrayList<>();
		OBAccount6Account account2=new OBAccount6Account();
		account2.setSchemeName("IBAN");
		account2.setIdentification("123");
		account2.setSecondaryIdentification("45457");
		account2.setName("avin");
		accountList.add(account2);
		obj.setAccount(accountList);
		obj.setServicer(new OBBranchAndFinancialInstitutionIdentification50());
		accountInfoList.add(obj);
		
		Mockito.when(utility.isValidPsuId(accountMapping.getPsuId())).thenReturn(true);

		Mockito.when(accountInfoRepo2.findByAccountNumberAndNsc(anyString(), anyString())).thenReturn(accountInfoList);

		Mockito.when(loggerUtils.populateLoggerData(anyString()))
				.thenReturn(AccountInformationAdapterTestMockData.getMockLoggerData());
		ReflectionTestUtils.setField(accountInformationMongoDBAdapterImpl, "maskSortCodeServicerValidation",false);
		
		PlatformAccountInformationResponse acctualAccountInforesponse = accountInformationMongoDBAdapterImpl
				.retrieveAccountInformation(accountMapping, params);


	}
	@Test
	public void testRetrieveMultipleAccountInformationSuccessFlow() {

		AccountMapping accountMapping = new AccountMapping();
		List<AccountDetails> accountDetailsList = new ArrayList<>();
		AccountDetails accountDetails = new AccountDetails();
		Map<String, String> params = new HashMap<>();

		accountDetails.setAccountId("f4483fda-81be-4873-b4a6-20375b7f55c1");
		accountDetailsList.add(accountDetails);
		accountMapping.setAccountDetails(accountDetailsList);
		List<AccountInformationCMA2> accountInfoList = new ArrayList<>();
		AccountInformationCMA2 obj=new AccountInformationCMA2();
		List<OBAccount6Account> accountList=new ArrayList<>();
		OBAccount6Account account2=new OBAccount6Account();
		account2.setSchemeName("UK.OBIE.SortCodeAccountNumber");
		account2.setIdentification("123");
		account2.setSecondaryIdentification("45457");
		account2.setName("avin");
		accountList.add(account2);
		obj.setAccount(accountList);
		accountInfoList.add(obj);

		Mockito.when(accountInfoRepo2.findByPsuId(anyString())).thenReturn(accountInfoList);

		Mockito.when(loggerUtils.populateLoggerData(anyString()))
				.thenReturn(AccountInformationAdapterTestMockData.getMockLoggerData());
		ReflectionTestUtils.setField(accountInformationMongoDBAdapterImpl, "maskSortCodeServicerValidation",true);
		
		PlatformAccountInformationResponse acctualAccountInforesponse = accountInformationMongoDBAdapterImpl
				.retrieveMultipleAccountsInformation(accountMapping, params);

	}
	
	
	@Test
	public void testRetrieveMultipleAccountInformationSuccessFlowmaskSortCodeServicerValidation() {

		AccountMapping accountMapping = new AccountMapping();
		List<AccountDetails> accountDetailsList = new ArrayList<>();
		AccountDetails accountDetails = new AccountDetails();
		Map<String, String> params = new HashMap<>();

		accountDetails.setAccountId("f4483fda-81be-4873-b4a6-20375b7f55c1");
		accountDetailsList.add(accountDetails);
		accountMapping.setAccountDetails(accountDetailsList);
		List<AccountInformationCMA2> accountInfoList = new ArrayList<>();
		AccountInformationCMA2 obj=new AccountInformationCMA2();
		List<OBAccount6Account> accountList=new ArrayList<>();
		OBAccount6Account account2=new OBAccount6Account();
		account2.setSchemeName("UK.OBIE.SortCodeAccountNumber");
		account2.setIdentification("123");
		account2.setSecondaryIdentification("45457");
		account2.setName("avin");
		accountList.add(account2);
		obj.setAccount(accountList);
		accountInfoList.add(obj);

		Mockito.when(accountInfoRepo2.findByPsuId(anyString())).thenReturn(accountInfoList);

		Mockito.when(loggerUtils.populateLoggerData(anyString()))
				.thenReturn(AccountInformationAdapterTestMockData.getMockLoggerData());
		ReflectionTestUtils.setField(accountInformationMongoDBAdapterImpl, "maskSortCodeServicerValidation",false);
		
		PlatformAccountInformationResponse acctualAccountInforesponse = accountInformationMongoDBAdapterImpl
				.retrieveMultipleAccountsInformation(accountMapping, params);

	}
	
	@Test
	public void testRetrieveMultipleAccountInformationSuccessFlowSORTAccount() {

		AccountMapping accountMapping = new AccountMapping();
		List<AccountDetails> accountDetailsList = new ArrayList<>();
		AccountDetails accountDetails = new AccountDetails();
		Map<String, String> params = new HashMap<>();

		accountDetails.setAccountId("f4483fda-81be-4873-b4a6-20375b7f55c1");
		accountDetailsList.add(accountDetails);
		accountMapping.setAccountDetails(accountDetailsList);
		List<AccountInformationCMA2> accountInfoList = new ArrayList<>();
		AccountInformationCMA2 obj=new AccountInformationCMA2();
		List<OBAccount6Account> accountList=new ArrayList<>();
		OBAccount6Account account2=new OBAccount6Account();
		account2.setSchemeName("SortCodeAccountNumber");
		account2.setIdentification("123");
		account2.setSecondaryIdentification("45457");
		account2.setName("avin");
		accountList.add(account2);
		obj.setAccount(accountList);
		accountInfoList.add(obj);

		Mockito.when(accountInfoRepo2.findByPsuId(anyString())).thenReturn(accountInfoList);

		Mockito.when(loggerUtils.populateLoggerData(anyString()))
				.thenReturn(AccountInformationAdapterTestMockData.getMockLoggerData());
		ReflectionTestUtils.setField(accountInformationMongoDBAdapterImpl, "maskSortCodeServicerValidation",true);
		
		PlatformAccountInformationResponse acctualAccountInforesponse = accountInformationMongoDBAdapterImpl
				.retrieveMultipleAccountsInformation(accountMapping, params);

	}

	@Test
	public void testRetrieveMultipleAccountInformationSuccessFlowsmaskSortCodeServicerValidation() {

		AccountMapping accountMapping = new AccountMapping();
		List<AccountDetails> accountDetailsList = new ArrayList<>();
		AccountDetails accountDetails = new AccountDetails();
		Map<String, String> params = new HashMap<>();

		accountDetails.setAccountId("f4483fda-81be-4873-b4a6-20375b7f55c1");
		accountDetailsList.add(accountDetails);
		accountMapping.setAccountDetails(accountDetailsList);
		List<AccountInformationCMA2> accountInfoList = new ArrayList<>();
		AccountInformationCMA2 obj=new AccountInformationCMA2();
		List<OBAccount6Account> accountList=new ArrayList<>();
		OBAccount6Account account2=new OBAccount6Account();
		account2.setSchemeName("SortCodeAccountNumber");
		account2.setIdentification("123");
		account2.setSecondaryIdentification("45457");
		account2.setName("avin");
		accountList.add(account2);
		obj.setAccount(accountList);
		accountInfoList.add(obj);

		Mockito.when(accountInfoRepo2.findByPsuId(anyString())).thenReturn(accountInfoList);

		Mockito.when(loggerUtils.populateLoggerData(anyString()))
				.thenReturn(AccountInformationAdapterTestMockData.getMockLoggerData());
		ReflectionTestUtils.setField(accountInformationMongoDBAdapterImpl, "maskSortCodeServicerValidation",false);
		
		PlatformAccountInformationResponse acctualAccountInforesponse = accountInformationMongoDBAdapterImpl
				.retrieveMultipleAccountsInformation(accountMapping, params);

	}
	
	@Test(expected=PSD2Exception.class)
	public void testRetrieveMultipleAccountInformationSuccessFlow3() {

		AccountMapping accountMapping = new AccountMapping();
		List<AccountDetails> accountDetailsList = new ArrayList<>();
		AccountDetails accountDetails = new AccountDetails();
		Map<String, String> params = new HashMap<>();

		accountDetails.setAccountId("f4483fda-81be-4873-b4a6-20375b7f55c1");
		accountDetailsList.add(accountDetails);
		accountMapping.setAccountDetails(accountDetailsList);
		List<AccountInformationCMA2> accountInfoList = new ArrayList<>();
		AccountInformationCMA2 obj=new AccountInformationCMA2();
		List<OBAccount6Account> accountList=new ArrayList<>();
		OBAccount6Account account2=new OBAccount6Account();
		account2.setSchemeName("UK.OBIE.IBAN");
		account2.setIdentification("123");
		account2.setSecondaryIdentification("45457");
		account2.setName("avin");
		accountList.add(account2);
		obj.setAccount(accountList);
		accountInfoList.add(obj);

		Mockito.when(accountInfoRepo2.findByPsuId(anyString())).thenReturn(accountInfoList);

		Mockito.when(loggerUtils.populateLoggerData(anyString()))
				.thenReturn(AccountInformationAdapterTestMockData.getMockLoggerData());
		ReflectionTestUtils.setField(accountInformationMongoDBAdapterImpl, "maskSortCodeServicerValidation",true);
		
		PlatformAccountInformationResponse acctualAccountInforesponse = accountInformationMongoDBAdapterImpl
				.retrieveMultipleAccountsInformation(accountMapping, params);

	}
	
	@Test
	public void testRetrieveMultipleAccountInformationSuccessFlowIBAN3() {

		AccountMapping accountMapping = new AccountMapping();
		List<AccountDetails> accountDetailsList = new ArrayList<>();
		AccountDetails accountDetails = new AccountDetails();
		Map<String, String> params = new HashMap<>();

		accountDetails.setAccountId("f4483fda-81be-4873-b4a6-20375b7f55c1");
		accountDetailsList.add(accountDetails);
		accountMapping.setAccountDetails(accountDetailsList);
		List<AccountInformationCMA2> accountInfoList = new ArrayList<>();
		AccountInformationCMA2 obj=new AccountInformationCMA2();
		List<OBAccount6Account> accountList=new ArrayList<>();
		OBAccount6Account account2=new OBAccount6Account();
		account2.setSchemeName("IBAN");
		account2.setIdentification("123");
		account2.setSecondaryIdentification("45457");
		account2.setName("avin");
		accountList.add(account2);
		obj.setAccount(accountList);
		obj.setServicer(new OBBranchAndFinancialInstitutionIdentification50());
		accountInfoList.add(obj);

		Mockito.when(accountInfoRepo2.findByPsuId(anyString())).thenReturn(accountInfoList);

		Mockito.when(loggerUtils.populateLoggerData(anyString()))
				.thenReturn(AccountInformationAdapterTestMockData.getMockLoggerData());
		ReflectionTestUtils.setField(accountInformationMongoDBAdapterImpl, "maskSortCodeServicerValidation",true);
		
		PlatformAccountInformationResponse acctualAccountInforesponse = accountInformationMongoDBAdapterImpl
				.retrieveMultipleAccountsInformation(accountMapping, params);

	}
	
	@Test(expected=PSD2Exception.class)
	public void testRetrieveMultipleAccountInformationSuccessFlow2() {

		AccountMapping accountMapping = new AccountMapping();
		List<AccountDetails> accountDetailsList = new ArrayList<>();
		AccountDetails accountDetails = new AccountDetails();
		Map<String, String> params = new HashMap<>();

		accountDetails.setAccountId("f4483fda-81be-4873-b4a6-20375b7f55c1");
		accountDetailsList.add(accountDetails);
		accountMapping.setAccountDetails(accountDetailsList);
		List<AccountInformationCMA2> accountInfoList = new ArrayList<>();
		AccountInformationCMA2 obj=new AccountInformationCMA2();
		List<OBAccount6Account> accountList=new ArrayList<>();
		OBAccount6Account account2=new OBAccount6Account();
		account2.setSchemeName("UK.OBIE.SortCodeAccountNumber");
		account2.setIdentification("123");
		account2.setSecondaryIdentification("45457");
		account2.setName("avin");
		accountList.add(account2);
		obj.setAccount(accountList);
		obj.setServicer(new OBBranchAndFinancialInstitutionIdentification50());
		accountInfoList.add(obj);

		Mockito.when(accountInfoRepo2.findByPsuId(anyString())).thenReturn(accountInfoList);

		Mockito.when(loggerUtils.populateLoggerData(anyString()))
				.thenReturn(AccountInformationAdapterTestMockData.getMockLoggerData());
		ReflectionTestUtils.setField(accountInformationMongoDBAdapterImpl, "maskSortCodeServicerValidation",true);
		
		PlatformAccountInformationResponse acctualAccountInforesponse = accountInformationMongoDBAdapterImpl
				.retrieveMultipleAccountsInformation(accountMapping, params);

	}

	/**
	 * Test retrieve Single account information data access resource failure
	 * exception.
	 */

	@Test(expected = PSD2Exception.class)
	public void testRetrieveAccountInformationDataAccessResourceFailureException() {

		AccountMapping accountMapping = new AccountMapping();
		List<AccountDetails> accountDetailsList = new ArrayList<>();
		AccountDetails accountDetails = new AccountDetails();
		Map<String, String> params = new HashMap<>();

		accountDetails.setAccountId("f4483fda-81be-4873-b4a6-20375b7f55c1");
		accountDetailsList.add(accountDetails);
		accountMapping.setAccountDetails(accountDetailsList);
		List<AccountInformationCMA2> accountInfoList = new ArrayList<>();
		accountInfoList.add(new AccountInformationCMA2());
		
		Mockito.when(utility.isValidPsuId(accountMapping.getPsuId())).thenReturn(false);

		Mockito.when(accountInfoRepo2.findByAccountNumberAndNsc(anyString(), anyString()))
				.thenThrow(DataAccessResourceFailureException.class);

		Mockito.when(loggerUtils.populateLoggerData(anyString()))
				.thenReturn(AccountInformationAdapterTestMockData.getMockLoggerData());
		ReflectionTestUtils.setField(accountInformationMongoDBAdapterImpl, "maskSortCodeServicerValidation",false);
		
		PlatformAccountInformationResponse acctualAccountInforesponse = accountInformationMongoDBAdapterImpl
				.retrieveAccountInformation(accountMapping, params);

		assertEquals(AccountInformationAdapterTestMockData.getMockExpectedAccountInformationResponse(),
				acctualAccountInforesponse.getoBReadAccount6());

	}

	/**
	 * Test retrieve Single account information null exception.
	 */

	@Test(expected = PSD2Exception.class)
	public void testRetrieveAccountInformationNullException() {

		AccountMapping accountMapping = new AccountMapping();
		List<AccountDetails> accountDetailsList = new ArrayList<>();
		AccountDetails accountDetails = new AccountDetails();
		Map<String, String> params = new HashMap<>();

		accountDetails.setAccountId("f4483fda-81be-4873-b4a6-20375b7f55c1");
		accountDetailsList.add(accountDetails);
		accountMapping.setAccountDetails(accountDetailsList);
		List<AccountInformationCMA2> accountInfoList = new ArrayList<>();
		accountInfoList.add(new AccountInformationCMA2());
		
		Mockito.when(utility.isValidPsuId(accountMapping.getPsuId())).thenReturn(false);

		Mockito.when(accountInfoRepo2.findByAccountNumberAndNsc(anyString(), anyString())).thenReturn(null);

		Mockito.when(loggerUtils.populateLoggerData(anyString()))
				.thenReturn(AccountInformationAdapterTestMockData.getMockLoggerData());
		
		ReflectionTestUtils.setField(accountInformationMongoDBAdapterImpl, "maskSortCodeServicerValidation",false);
		PlatformAccountInformationResponse acctualAccountInforesponse = accountInformationMongoDBAdapterImpl
				.retrieveAccountInformation(accountMapping, params);

		assertEquals(AccountInformationAdapterTestMockData.getMockExpectedAccountInformationResponse(),
				acctualAccountInforesponse.getoBReadAccount6());

	}

	/**
	 * Test retrieve Multiple accounts information success flow.
	 */

	@Test(expected = PSD2Exception.class)
	public void testRetrieveMultipleAccountInformationEmptyList() {
		List<AccountInformationCMA2> list = new ArrayList<>();
		Mockito.when(accountInfoRepo2.findAll()).thenReturn(list);
		try {
			
			accountInformationMongoDBAdapterImpl.retrieveMultipleAccountsInformation(
					AccountInformationAdapterTestMockData.getAccountMapping(), null);
		} catch (PSD2Exception e) {
			assertEquals("No data found for the requested account", e.getErrorInfo().getErrorMessage());
			throw e;
		}
	}
	
	@Test(expected = PSD2Exception.class)
	public void testRetrieveMultipleAccountInformationNullList() {
		Mockito.when(accountInfoRepo2.findByPsuId(anyString())).thenReturn(null);
		try {
			
			accountInformationMongoDBAdapterImpl.retrieveMultipleAccountsInformation(
					AccountInformationAdapterTestMockData.getAccountMapping(), null);
		} catch (PSD2Exception e) {
			assertEquals("No data found for the requested account", e.getErrorInfo().getErrorMessage());
			throw e;
		}
	}

	
	@Test(expected = PSD2Exception.class)
	public void testRetrieveMultipleAccountInformationEmptyListDataResourceFailure() {
			
			Mockito.when(accountInfoRepo2.findByPsuId(anyString())).thenThrow(new DataAccessResourceFailureException("error"));
			accountInformationMongoDBAdapterImpl.retrieveMultipleAccountsInformation(
					AccountInformationAdapterTestMockData.getAccountMapping(), null);
		
	}
}
