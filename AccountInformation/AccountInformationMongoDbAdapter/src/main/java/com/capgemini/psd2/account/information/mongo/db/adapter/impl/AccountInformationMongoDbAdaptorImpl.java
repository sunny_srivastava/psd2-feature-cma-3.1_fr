/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.account.information.mongo.db.adapter.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.account.information.mongo.db.adapter.repository.AccountInformationRepository2;
import com.capgemini.psd2.aisp.adapter.AccountInformationAdapter;
import com.capgemini.psd2.aisp.domain.OBAccount6;
import com.capgemini.psd2.aisp.domain.OBAccount6Account;
import com.capgemini.psd2.aisp.domain.OBReadAccount6;
import com.capgemini.psd2.aisp.domain.OBReadAccount6Data;
import com.capgemini.psd2.aisp.mongo.db.adapter.domain.AccountInformationCMA2;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountInformationResponse;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.ErrorMapKeys;
import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.InternalServerErrorMessage;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.LoggerUtils;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.utilities.NullCheckUtils;
import com.capgemini.psd2.utilities.SandboxValidationUtility;

/**
 * The Class AccountInformationMongoDbAdaptorImpl.
 */
@Component
public class AccountInformationMongoDbAdaptorImpl implements AccountInformationAdapter {

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(AccountInformationMongoDbAdaptorImpl.class);

	/** The logger utils. */
	@Autowired
	private LoggerUtils loggerUtils;

	@Autowired
	private AccountInformationRepository2 accountInfoRepo2;

	@Autowired
	private SandboxValidationUtility utility;

	@Value("${app.maskSortCodeServicerValidation:#{false}}")
	private Boolean maskSortCodeServicerValidation;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.capgemini.psd2.aisp.adapter.AccountInformationAdapter#
	 * retrieveAccountInformation(com.capgemini.psd2.aisp.domain.AccountMapping,
	 * java.util.Map)
	 */
	@Override
	public PlatformAccountInformationResponse retrieveAccountInformation(AccountMapping accountMapping,
			Map<String, String> params) {

		LOG.info("{\"Enter\":\"{}\",\"{}\"}",
				"com.capgemini.psd2.account.product.mongo.db.adapter.impl.retrieveAccountInformation()",
				loggerUtils.populateLoggerData("retrieveAccountInformation"));

		String accountNumber = accountMapping.getAccountDetails().get(0).getAccountNumber();
		String nsc = accountMapping.getAccountDetails().get(0).getAccountNSC();
		String accountId = accountMapping.getAccountDetails().get(0).getAccountId();
		List<AccountInformationCMA2> mockAccountList = new ArrayList<>();
		LOG.info("TenantId : " + params.get(PSD2Constants.TENANT_ID));

		try {
			if (utility.isValidPsuId(accountMapping.getPsuId())) {

				throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
						InternalServerErrorMessage.SANDBOX_MOCKED_ERROR));
			}

			mockAccountList = accountInfoRepo2.findByAccountNumberAndNsc(accountNumber, nsc);

		} catch (DataAccessResourceFailureException e) {
			LOG.error(e.getMessage(),e);
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
					InternalServerErrorMessage.CONNECTION_ERROR));
		}
		if (NullCheckUtils.isNullOrEmpty(mockAccountList))
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.NO_RECORD_FOUND_FOR_REQUESTED_ACCT);

		OBAccount6Account account = mockAccountList.get(0).getAccount().get(0);

		/*
		 * This sandbox mock response is configurable based on the value of
		 * maskSortCodeServicerValidation that is set in application properties
		 */
		if (maskSortCodeServicerValidation) {
			if (("SortCodeAccountNumber".equals(account.getSchemeName())
					|| "UK.OBIE.SortCodeAccountNumber".equals(account.getSchemeName()))
					&& mockAccountList.get(0).getServicer() != null)
				throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
						ErrorMapKeys.VALIDATION_ERROR_OUTPUT));

			if (("IBAN".equals(account.getSchemeName()) || "UK.OBIE.IBAN".equals(account.getSchemeName()))
					&& mockAccountList.get(0).getServicer() == null)
				throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
						ErrorMapKeys.VALIDATION_ERROR_OUTPUT));
		}

		mockAccountList.get(0).setAccountId(accountId);
		List<OBAccount6> accountList = new ArrayList<>();
		accountList.addAll(mockAccountList);
		OBReadAccount6 obReadAccount = new OBReadAccount6();
		OBReadAccount6Data obReadAccount2Data = new OBReadAccount6Data();
		obReadAccount2Data.setAccount(accountList);
		obReadAccount.setData(obReadAccount2Data);
		PlatformAccountInformationResponse platformAccountInformationResponse = new PlatformAccountInformationResponse();
		platformAccountInformationResponse.setoBReadAccount6(obReadAccount);

		LOG.info("{\"Exit\":\"{}\",\"{}\"}",
				"com.capgemini.psd2.account.product.mongo.db.adapter.impl.retrieveAccountInformation()",
				loggerUtils.populateLoggerData("retrieveAccountInformation"));

		return platformAccountInformationResponse;
	}

	@Override
	public PlatformAccountInformationResponse retrieveMultipleAccountsInformation(AccountMapping accountMapping,
			Map<String, String> seviceParams) {

		LOG.info("{\"Enter\":\"{}\",\"{}\"}",
				"com.capgemini.psd2.account.product.mongo.db.adapter.impl.retrieveMultipleAccountsInformation()",
				loggerUtils.populateLoggerData("retrieveMultipleAccountsInformation"));

		List<AccountInformationCMA2> multiAccountsInfoList = new ArrayList<>();
		try {
			multiAccountsInfoList = accountInfoRepo2.findByPsuId(accountMapping.getPsuId());
		} catch (DataAccessResourceFailureException e) {
			LOG.error(e.getMessage(),e);
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
					InternalServerErrorMessage.CONNECTION_ERROR));
		}

		if (null == multiAccountsInfoList || multiAccountsInfoList.isEmpty())
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.NO_RECORD_FOUND_FOR_REQUESTED_ACCT);

		for (AccountInformationCMA2 accInfoCMA2 : multiAccountsInfoList) {

			/* Please confirm and remove below scheme based code if possible */
			OBAccount6Account data2account = accInfoCMA2.getAccount().get(0);

			if (maskSortCodeServicerValidation) {
				if (("SortCodeAccountNumber".equals(data2account.getSchemeName())
						|| "UK.OBIE.SortCodeAccountNumber".equals(data2account.getSchemeName()))
						&& accInfoCMA2.getServicer() != null)
					throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
							ErrorMapKeys.VALIDATION_ERROR_OUTPUT));

				if (("IBAN".equals(data2account.getSchemeName()) || "UK.OBIE.IBAN".equals(data2account.getSchemeName()))
						&& accInfoCMA2.getServicer() == null)
					throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
							ErrorMapKeys.VALIDATION_ERROR_OUTPUT));
			}
		}
		Map<String, String> accountNumberIdMap = new HashMap<>();
		accountMapping.getAccountDetails()
				.forEach(x -> accountNumberIdMap.put(x.getAccountNumber() + "|" + x.getAccountNSC(), x.getAccountId()));
		Iterator<AccountInformationCMA2> itr = multiAccountsInfoList.iterator();
		while (itr.hasNext()) {
			AccountInformationCMA2 accountInformationCMA2 = itr.next();
			if (accountNumberIdMap
					.get(accountInformationCMA2.getAccountNumber() + "|" + accountInformationCMA2.getNsc()) != null) {
				accountInformationCMA2.setAccountId(accountNumberIdMap
						.get(accountInformationCMA2.getAccountNumber() + "|" + accountInformationCMA2.getNsc()));
			} else {
				itr.remove();
			}
		}

		List<OBAccount6> accountList = new ArrayList<>();

		accountList.addAll(multiAccountsInfoList);
		OBReadAccount6 obReadAccount = new OBReadAccount6();
		OBReadAccount6Data obReadAccountData = new OBReadAccount6Data();
		obReadAccountData.setAccount(accountList);
		obReadAccount.setData(obReadAccountData);
		PlatformAccountInformationResponse platformAccountInformationResponse = new PlatformAccountInformationResponse();
		platformAccountInformationResponse.setoBReadAccount6(obReadAccount);

		LOG.info("{\"Exit\":\"{}\",\"{}\"}",
				"com.capgemini.psd2.account.product.mongo.db.adapter.impl.retrieveMultipleAccountsInformation()",
				loggerUtils.populateLoggerData("retrieveMultipleAccountsInformation"));

		return platformAccountInformationResponse;
	}
}
