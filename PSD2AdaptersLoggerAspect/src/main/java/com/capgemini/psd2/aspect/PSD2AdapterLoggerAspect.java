/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * The Class PSD2Aspect.
 */
@Component
@Aspect
public class PSD2AdapterLoggerAspect {

	@Autowired
	private PSD2AdapterLoggerAspectUtils aspectUtils;

	/**
	 * Arround logger advice service.
	 *
	 * @param proceedingJoinPoint
	 *            the proceeding join point
	 * @return the object
	 */
	@Around("execution(* com.capgemini.psd2..mongo.db.adapter..*.* (..))")
	public Object arroundLoggerAdviceService(ProceedingJoinPoint proceedingJoinPoint) {
		return aspectUtils.methodAdvice(proceedingJoinPoint);
	}

	/**
	 * This method throws {@link PSD2Exception} on posting no body or invalid
	 * body according to swagger in account request POST API and can be used for
	 * any other POST API
	 * 
	 * @param joinPoint
	 * @param error
	 * 
	 * @throws PSD2Exception
	 * 
	 */
	@AfterThrowing(pointcut = "execution(* org.springframework.http.converter.json.AbstractJackson2HttpMessageConverter.read(..))", throwing = "error")
	public void throwExcpetionOnJsonBinding(JoinPoint joinPoint, Throwable error) {
		aspectUtils.throwExceptionOnJsonBinding(joinPoint, error);
	}

}
