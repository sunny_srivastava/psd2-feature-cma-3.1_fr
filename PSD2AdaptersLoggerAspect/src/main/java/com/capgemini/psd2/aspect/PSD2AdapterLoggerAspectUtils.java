package com.capgemini.psd2.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.OBPSD2Exception;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.mask.DataMask;
import com.capgemini.psd2.utilities.JSONUtilities;

@Component
public class PSD2AdapterLoggerAspectUtils {

	@Value("${app.payloadLog:#{false}}")
	private boolean payloadLog;

	/** The mask payload log. */
	@Value("${app.maskPayloadLog:#{false}}")
	private boolean maskPayloadLog;

	/** The mask payload. */
	@Value("${app.maskPayload:#{false}}")
	private boolean maskPayload;

	/** The api id. */
	@Value("${spring.application.name}")
	private String apiId;

	@Autowired
	private DataMask dataMask;

	private static final Logger LOGGER = LoggerFactory.getLogger(PSD2AdapterLoggerAspectUtils.class);

	private static final String ERROR_DETAILS = "{\"Exception\":\"{}.{}()\",\"{}\",\"ErrorDetails\":{}}";

	private static final String ERROR_DETAILS_1 = "{\"Exception\":\"{}.{}()\",\"{}\",\"ErrorDetails\":\"{}\"}";

	public Object methodAdvice(ProceedingJoinPoint proceedingJoinPoint) {
		
		try {
			if(!LOGGER.isDebugEnabled()) {
				return proceedingJoinPoint.proceed();
			}
			
			String arguments = "";
			try{
				if(proceedingJoinPoint.getArgs() != null && proceedingJoinPoint.getArgs().length > 0)
					arguments = AdapterJSONUtilities.getJSONOutPutFromObject(proceedingJoinPoint.getArgs());
			}catch(PSD2Exception e){
				arguments = "";
			}
			
			LOGGER.debug("{\"Enter\":\"{}.{}()\",\"apiId\":\"{}\",\"{}\"}",
					proceedingJoinPoint.getSignature().getDeclaringTypeName(), proceedingJoinPoint.getSignature().getName(), apiId, arguments);
			Object result = proceedingJoinPoint.proceed();
			LOGGER.debug("{\"Exit\":\"{}.{}()\"}", proceedingJoinPoint.getSignature().getDeclaringTypeName(),
					proceedingJoinPoint.getSignature().getName());
			return result;
		} catch (PSD2Exception e) {
			logException(e, proceedingJoinPoint);
			throw e;
		} catch (OBPSD2Exception e) {
			logOBException(e, proceedingJoinPoint);
			throw e;
		} catch (Throwable e) {
			PSD2Exception psd2Exception = PSD2Exception.generateResponseForInternalErrors(e);
			logException(psd2Exception, proceedingJoinPoint);
			throw psd2Exception;
		}
	}

	public Object methodPayloadAdvice(ProceedingJoinPoint proceedingJoinPoint) {
		logArgument(proceedingJoinPoint);
		try {
			Object result = proceedingJoinPoint.proceed();

			checkPayload(proceedingJoinPoint, result);

			checkMaskPayload(proceedingJoinPoint, result);

			return result;
		} catch (PSD2Exception e) {
			logException(e, proceedingJoinPoint);
			throw e;
		} catch (OBPSD2Exception e) {
			logOBException(e, proceedingJoinPoint);
			throw e;
		} catch (Throwable e) {
			PSD2Exception psd2Exception = PSD2Exception.generateResponseForInternalErrors(e);
			logException(psd2Exception, proceedingJoinPoint);
			throw psd2Exception;
		}
	}

	public void throwExceptionOnJsonBinding(JoinPoint joinPoint, Throwable error) {
		PSD2Exception psd2Exception = PSD2Exception.populatePSD2Exception(
				new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_RESOURCE_INVALIDFORMAT, error.toString()));

		LOGGER.error(ERROR_DETAILS, joinPoint.getSignature().getDeclaringTypeName(), joinPoint.getSignature().getName(),
				psd2Exception.getOBErrorResponse1());
		if (LOGGER.isDebugEnabled()) {
			LOGGER.error(ERROR_DETAILS_1, joinPoint.getSignature().getDeclaringTypeName(),
					joinPoint.getSignature().getName(), error.getStackTrace());
		}
		throw psd2Exception;
	}

	private void logException(PSD2Exception e, ProceedingJoinPoint proceedingJoinPoint) {

		if (e.getOBErrorResponse1() != null)
			LOGGER.error(ERROR_DETAILS, proceedingJoinPoint.getSignature().getDeclaringTypeName(),
					proceedingJoinPoint.getSignature().getName(), e.getOBErrorResponse1());
		else
			LOGGER.error(ERROR_DETAILS, proceedingJoinPoint.getSignature().getDeclaringTypeName(),
					proceedingJoinPoint.getSignature().getName(), e.getErrorInfo());
		if (LOGGER.isDebugEnabled()) {
			LOGGER.error(ERROR_DETAILS_1, proceedingJoinPoint.getSignature().getDeclaringTypeName(),
					proceedingJoinPoint.getSignature().getName(), e.getStackTrace());
		}
	}

	private void logOBException(OBPSD2Exception e, ProceedingJoinPoint proceedingJoinPoint) {
		LOGGER.error(ERROR_DETAILS, proceedingJoinPoint.getSignature().getDeclaringTypeName(),
				proceedingJoinPoint.getSignature().getName(), e.getErrorResponse());
		if (LOGGER.isDebugEnabled()) {
			LOGGER.error(ERROR_DETAILS_1, proceedingJoinPoint.getSignature().getDeclaringTypeName(),
					proceedingJoinPoint.getSignature().getName(), e.getStackTrace());
		}
	}

	private void logArgument(ProceedingJoinPoint proceedingJoinPoint) {
		if (maskPayloadLog && payloadLog) {
			LOGGER.info("{\"Enter\":\"{}.{}()\",\"Arguments\":{}}",
					proceedingJoinPoint.getSignature().getDeclaringTypeName(),
					proceedingJoinPoint.getSignature().getName(),
					JSONUtilities.getJSONOutPutFromObject(dataMask.maskRequestLog(proceedingJoinPoint.getArgs(),
							proceedingJoinPoint.getSignature().getName())));

		} else if (!maskPayloadLog && payloadLog) {
			LOGGER.info("{\"Enter\":\"{}.{}()\",\"Arguments\":{}}",
					proceedingJoinPoint.getSignature().getDeclaringTypeName(),
					proceedingJoinPoint.getSignature().getName(),
					JSONUtilities.getJSONOutPutFromObject(dataMask.maskMRequestLog(proceedingJoinPoint.getArgs(),
							proceedingJoinPoint.getSignature().getName())));

		} else {
			LOGGER.info("{\"Enter\":\"{}.{}()\"}", proceedingJoinPoint.getSignature().getDeclaringTypeName(),
					proceedingJoinPoint.getSignature().getName());
		}
	}

	private void checkPayload(ProceedingJoinPoint proceedingJoinPoint, Object result) {
		if (maskPayloadLog && payloadLog) {
			LOGGER.info("{\"Exit\":\"{}.{}()\",\"Payload\":{}}",
					proceedingJoinPoint.getSignature().getDeclaringTypeName(),
					proceedingJoinPoint.getSignature().getName(), JSONUtilities.getJSONOutPutFromObject(
							dataMask.maskResponseLog(result, proceedingJoinPoint.getSignature().getName())));
		} else if (!maskPayloadLog && payloadLog) {
			LOGGER.info("{\"Exit\":\"{}.{}()\",\"Payload\":{}}",
					proceedingJoinPoint.getSignature().getDeclaringTypeName(),
					proceedingJoinPoint.getSignature().getName(), JSONUtilities.getJSONOutPutFromObject(
							dataMask.maskMResponseLog(result, proceedingJoinPoint.getSignature().getName())));
		} else {
			LOGGER.info("{\"Exit\":\"{}.{}()\"}", proceedingJoinPoint.getSignature().getDeclaringTypeName(),
					proceedingJoinPoint.getSignature().getName());
		}
	}

	private void checkMaskPayload(ProceedingJoinPoint proceedingJoinPoint, Object result) {
		if (maskPayload)
			dataMask.maskResponse(result, proceedingJoinPoint.getSignature().getName());
		else
			dataMask.maskMResponse(result, proceedingJoinPoint.getSignature().getName());
	}
}
