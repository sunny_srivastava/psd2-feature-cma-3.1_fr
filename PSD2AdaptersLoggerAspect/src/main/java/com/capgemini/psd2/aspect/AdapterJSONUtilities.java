package com.capgemini.psd2.aspect;


import java.io.IOException;
import java.io.Serializable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.PSD2Constants;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonParser;

public final class AdapterJSONUtilities implements Serializable{

	private static final Logger LOG = LoggerFactory.getLogger(AdapterJSONUtilities.class);
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The mapper. */
	private static ObjectMapper mapper = null;
	
	/**
	 * Instantiates a new JSON utilities.
	 */
	private AdapterJSONUtilities(){
		
	}
	
	static {
		mapper = new ObjectMapper();
	}
	
	/**
	 * Gets the JSON out put from object.
	 *
	 * @param <T> the generic type
	 * @param obj the obj
	 * @return the JSON out put from object
	 */
	public static <T> String getJSONOutPutFromObject(T obj){
		String jsonString = null;
		try{
			jsonString = mapper.writeValueAsString(obj);
		}
		catch(JsonProcessingException je) {
			LOG.error(PSD2Constants.LOGGER_STATEMENT+je);
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
					je.getMessage()));
		}
		return jsonString;
	}
	
	
	
	/**
	 * Gets the object from JSON string.
	 *
	 * @param <T> the generic type
	 * @param jsonString the json string
	 * @param classType the class type
	 * @return the object from JSON string
	 */
	public static <T> T getObjectFromJSONString(String jsonString,Class<T> classType) {
		T obj = null;
		try {
			obj = mapper.readValue(jsonString, classType);
		} catch (IOException e) {
			LOG.error(PSD2Constants.LOGGER_STATEMENT+e);
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
					e.getMessage()));
		}
		return obj; 
	}
	
	/**
	 * Gets the object from JSON string.
	 *
	 * @param <T> the generic type
	 * @param jsonString the json string
	 * @param obj the obj
	 * @return the object from JSON string
	 */
	public static <T> T getObjectFromJSONString(String jsonString, T obj) {
		T jsonObj = null;
		try {
			jsonObj = mapper.readerForUpdating(obj).readValue(jsonString);
		} catch (IOException e) {
			LOG.error(PSD2Constants.LOGGER_STATEMENT+e);
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
					e.getMessage()));
		}
		return jsonObj;
	}
	
	public static <T> T getObjectFromJSONString(ObjectMapper mapper,String jsonString, T obj) {
		T jsonObj = null;
		try {
			jsonObj = mapper.readerForUpdating(obj).readValue(jsonString);
		} catch (IOException e) {
			LOG.error(PSD2Constants.LOGGER_STATEMENT+e);
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
					e.getMessage()));
		}
		return jsonObj;
	}
	
	public static <T> T getObjectFromJSONString(ObjectMapper mapper,String jsonString,Class<T> classType) {
		T obj = null;
		try {
			obj = mapper.readValue(jsonString, classType);
		} catch (IOException e) {
			LOG.error(PSD2Constants.LOGGER_STATEMENT+e);
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
					e.getMessage()));
		}
		return obj; 
	}
	
	
	public static void jsonParser(String jsonString) {
		try {
			JsonParser parser = new JsonParser();
			parser.parse(jsonString);
		} catch (Exception e1) {
			LOG.error(PSD2Constants.LOGGER_STATEMENT+e1);
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_RESOURCE_INVALIDFORMAT, e1.getMessage(), true));
		}
	}
}