package com.capgemini.psd2.integration.controller.test;

import static org.mockito.Matchers.anyString;

import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;

import com.capgemini.psd2.integration.controller.ResponseSigningController;
import com.capgemini.psd2.integration.service.ResponseSigningService;

public class ResponseSigningControllerTest {

	@Mock
	private ResponseSigningService service;

	@InjectMocks
	private ResponseSigningController controller;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testpublicKeyForClient() {
		service.signResponse("responseBody", MediaType.APPLICATION_JSON);
		controller.signResponse("responseBody");
	}

	@Test
	public void testfetchJwksUrlForFiles() {
		byte[] bytesFromFile = "90".getBytes();
		service.signResponse(Arrays.toString(bytesFromFile), MediaType.APPLICATION_PDF);
		controller.signFile(bytesFromFile);
	}

}
