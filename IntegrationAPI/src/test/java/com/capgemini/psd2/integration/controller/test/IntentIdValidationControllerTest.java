package com.capgemini.psd2.integration.controller.test;

import static org.mockito.Mockito.doNothing;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.integration.controller.IntentIdValidationController;
import com.capgemini.psd2.integration.service.IntentIdValidationService;

public class IntentIdValidationControllerTest 
{
	
	
	@Mock
	private IntentIdValidationService intentIdValidationService;
	
	@InjectMocks
	private IntentIdValidationController controller;
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void testValidateIntentId() throws Exception{
		String intentId="123";
		String clientId="456";
		String scope="openid payments";
		
		doNothing().when(intentIdValidationService).validateIntentId(intentId, clientId, scope);
		controller.validateIntentId(intentId, clientId, scope);
		
	}
	
	@Test
	public void testValidateIntentId2() throws Exception{
		ReflectionTestUtils.setField(controller, "byPassIntentValidation", true);
		controller.validateIntentId("123", "6756", "adj");
		
	}



}
