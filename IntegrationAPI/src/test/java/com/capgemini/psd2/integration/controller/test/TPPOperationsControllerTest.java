package com.capgemini.psd2.integration.controller.test;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.integration.controller.TPPOperationsController;
import com.capgemini.psd2.integration.dtos.TPPInformationDTO;
import com.capgemini.psd2.integration.service.TPPInformationService;
import com.capgemini.psd2.token.TPPInformation;


public class TPPOperationsControllerTest {
	@Mock
	private TPPInformationService tppInformationService;
	
	@InjectMocks
	public TPPOperationsController controller;
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);	
	}
	
	@Test(expected = PSD2Exception.class)
	public void findTPPInformationTest() throws Exception
	{
		
		controller.findTPPInformation(null);
	}
	
	
	@Test
	public void findTPPInformationClientIdNotNullTest() throws Exception
	{
		controller.findTPPInformation("122334");
	}
	
	@Test(expected = PSD2Exception.class)
	public void findTPPInformationClientIdNullTest() throws Exception
	{
		controller.findTPPInformation("");
	}
	@Test(expected = PSD2Exception.class)
	public void findTPPInformationClientIdNullTest2() throws Exception
	{
		controller.findTPPInformation(null);
	}
	
	@Test(expected = PSD2Exception.class)
	public void validateTPPTest() throws Exception
	{
		TPPInformationDTO tppInformation=new TPPInformationDTO();
		TPPInformation tppInfo =new TPPInformation();
		tppInformation.setClientId("1234");
		String clientId="1234";
		assertEquals(clientId,tppInformation.getClientId());
		Mockito.when(tppInformationService.findTPPInformation(clientId)).thenReturn(tppInformation);
		//Mockito.when(tppInformationService.findTPPInformation(anyObject())).thenReturn(tppInformation);
		tppInformation.setTppInformation(tppInfo);
		tppInformation.getTppInformation().setTppBlock("true");
		controller.validateTPP("1234");
	}
	
	@Test(expected = PSD2Exception.class)
	public void validateTPPTest2() throws Exception
	{
		ReflectionTestUtils.setField(controller,"byPassTppBlockCheck",Boolean.FALSE);
		TPPInformationDTO tppInformation=new TPPInformationDTO();
		TPPInformation tppInfo =new TPPInformation();
		tppInformation.setClientId("1234");
		String clientId="1234";
		Mockito.when(tppInformationService.findTPPInformation(clientId)).thenReturn(tppInformation);
		//Mockito.when(tppInformationService.findTPPInformation(anyObject())).thenReturn(tppInformation);
		tppInformation.setTppInformation(tppInfo);
		tppInformation.getTppInformation().setTppBlock("true");
		controller.validateTPP("1234");
	}
	@Test
	public void validateTPPTest3() throws Exception
	{
		ReflectionTestUtils.setField(controller,"byPassTppBlockCheck",Boolean.TRUE);
		TPPInformationDTO tppInformation=new TPPInformationDTO();
		TPPInformation tppInfo =new TPPInformation();
		tppInformation.setClientId("1234");
		String clientId="1234";
		Mockito.when(tppInformationService.findTPPInformation(clientId)).thenReturn(tppInformation);
		//Mockito.when(tppInformationService.findTPPInformation(anyObject())).thenReturn(tppInformation);
		tppInformation.setTppInformation(tppInfo);
		tppInformation.getTppInformation().setTppBlock("true");
		controller.validateTPP("1234");
	}
	
	@Test
	public void validateTPPTest4() throws Exception
	{
		
		TPPInformationDTO tppInformation=new TPPInformationDTO();
		TPPInformation tppInfo =new TPPInformation();
		tppInformation.setClientId("1234");
		String clientId="1234";
		Mockito.when(tppInformationService.findTPPInformation(clientId)).thenReturn(tppInformation);
		//Mockito.when(tppInformationService.findTPPInformation(anyObject())).thenReturn(tppInformation);
		tppInformation.setTppInformation(tppInfo);
		tppInformation.getTppInformation().setTppBlock(null);
		controller.validateTPP("1234");
	}
	@Test
	public void validateTPPTest5() throws Exception
	{
		
		TPPInformationDTO tppInformation=new TPPInformationDTO();
		TPPInformation tppInfo =new TPPInformation();
		tppInformation.setClientId("1234");
		String clientId="1234";
		Mockito.when(tppInformationService.findTPPInformation(clientId)).thenReturn(tppInformation);
		//Mockito.when(tppInformationService.findTPPInformation(anyObject())).thenReturn(tppInformation);
		tppInformation.setTppInformation(tppInfo);
		tppInformation.getTppInformation().setTppBlock("");
		controller.validateTPP("1234");
	}

}
