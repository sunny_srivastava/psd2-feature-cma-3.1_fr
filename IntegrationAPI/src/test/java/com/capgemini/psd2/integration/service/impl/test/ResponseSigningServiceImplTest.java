package com.capgemini.psd2.integration.service.impl.test;

import java.security.KeyStore;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.integration.service.impl.ResponseSigningServiceImpl;
import com.capgemini.psd2.logger.RequestHeaderAttributes;

public class ResponseSigningServiceImplTest {

	@Mock
	private RequestHeaderAttributes requestHeaderAttributes;

	@Mock
	private KeyStore keyStore = null;

	@InjectMocks
	ResponseSigningServiceImpl service;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testPopulateKeyStore() {
		String responseInput = "{\r\n" + "    \"errorCode\": \"128\",\r\n"
				+ "    \"errorMessage\": \"Validation error found with the provided ouput\",\r\n"
				+ "    \"detailErrorMessage\": \"instructedAmount.amount : must match \\\"^\\\\d{1,13}\\\\.\\\\d{1,5}$\\\";\"\r\n"
				+ "}";
		List<String> critHeaders = new ArrayList<>();
		critHeaders.add("b64");
		Map<String, Map<String, String>> signingKeyDetails = new HashMap<>();
		Map<String, String> map = new HashMap<>();
		map.put("signingAlgo", "RS256");
		map.put("alias", "6di2de88s8d2zvxgyxm0b2 (open banking test issuing ca)");
		map.put("kid", "yTXZEcT2QV3jHeJtKaOfY0v3ytQ");
		signingKeyDetails.put("tenant1", map);
		ReflectionTestUtils.setField(service, "critData", critHeaders);
		ReflectionTestUtils.setField(service, "keystorePath", "keystore.jks");
		ReflectionTestUtils.setField(service, "keystorePassword", "password");
		ReflectionTestUtils.setField(service, "signingKeyDetails", signingKeyDetails);
		Mockito.when(requestHeaderAttributes.getTenantId()).thenReturn("tenant1");
		service.populateKeyStore();
		service.getSigningKeyDetails();
		service.signResponse(responseInput, MediaType.APPLICATION_JSON_UTF8);
		service.getCritData();
	}
	
	@Test(expected=PSD2Exception.class)
	public void testPopulateKeyStore1() {
		String responseInput = "{\r\n" + "    \"errorCode\": \"128\",\r\n"
				+ "    \"errorMessage\": \"Validation error found with the provided ouput\",\r\n"
				+ "    \"detailErrorMessage\": \"instructedAmount.amount : must match \\\"^\\\\d{1,13}\\\\.\\\\d{1,5}$\\\";\"\r\n"
				+ "}";
		List<String> critHeaders = new ArrayList<>();
		critHeaders.add("b64");
		Map<String, Map<String, String>> signingKeyDetails = new HashMap<>();
		Map<String, String> map = new HashMap<>();
		map.put("signingAlgo", "RS256");
		map.put("alias", "6di2de88s8d2zvxgyxm0b2 (open banking test issuing ca)");
		map.put("kid", "yTXZEcT2QV3jHeJtKaOfY0v3ytQ");
		signingKeyDetails.put("tenant1", map);
		ReflectionTestUtils.setField(service, "critData", critHeaders);
		ReflectionTestUtils.setField(service, "keystorePath", "keystore.jks");
		ReflectionTestUtils.setField(service, "keystorePassword", "password1");
		ReflectionTestUtils.setField(service, "signingKeyDetails", signingKeyDetails);
		Mockito.when(requestHeaderAttributes.getTenantId()).thenReturn("tenant1");
		service.populateKeyStore();
		service.getSigningKeyDetails();
		service.signResponse(responseInput, MediaType.APPLICATION_JSON_UTF8);
		service.getCritData();
	}

	@Test(expected = PSD2Exception.class)
	public void testPopulateKeyStore_InvalidSignAlgo() {
		String responseInput = "{\r\n" + "    \"errorCode\": \"128\",\r\n"
				+ "    \"errorMessage\": \"Validation error found with the provided ouput\",\r\n"
				+ "    \"detailErrorMessage\": \"instructedAmount.amount : must match \\\"^\\\\d{1,13}\\\\.\\\\d{1,5}$\\\";\"\r\n"
				+ "}";
		List<String> critHeaders = new ArrayList<>();
		critHeaders.add("b64");
		Map<String, Map<String, String>> signingKeyDetails = new HashMap<>();
		Map<String, String> map = new HashMap<>();
		map.put("signingAlgo", "HS256");
		map.put("alias", "6di2de88s8d2zvxgyxm0b2 (open banking test issuing ca)");
		map.put("kid", "yTXZEcT2QV3jHeJtKaOfY0v3ytQ");
		signingKeyDetails.put("tenant1", map);
		ReflectionTestUtils.setField(service, "critData", critHeaders);
		ReflectionTestUtils.setField(service, "keystorePath", "keystore.jks");
		ReflectionTestUtils.setField(service, "keystorePassword", "password");
		ReflectionTestUtils.setField(service, "signingKeyDetails", signingKeyDetails);
		Mockito.when(requestHeaderAttributes.getTenantId()).thenReturn("tenant1");
		service.populateKeyStore();
		service.getSigningKeyDetails();
		service.signResponse(responseInput, MediaType.APPLICATION_JSON_UTF8);
		service.getCritData();
	}

}
