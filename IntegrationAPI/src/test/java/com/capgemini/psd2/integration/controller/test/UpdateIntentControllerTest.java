package com.capgemini.psd2.integration.controller.test;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyList;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.capgemini.psd2.account.request.mongo.db.adapter.repository.AccountRequestCMA3Repository;
import com.capgemini.psd2.aisp.adapter.AccountRequestAdapter;
import com.capgemini.psd2.aisp.adapter.AispConsentAdapter;
import com.capgemini.psd2.aisp.domain.OBReadConsentResponse1Data;
import com.capgemini.psd2.cisp.adapter.CispConsentAdapter;
import com.capgemini.psd2.cisp.adapter.FundsConfirmationConsentAdapter;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmationConsentResponse1Data;
import com.capgemini.psd2.consent.domain.AispConsent;
import com.capgemini.psd2.consent.domain.CispConsent;
import com.capgemini.psd2.consent.domain.PispConsent;
import com.capgemini.psd2.enums.ConsentStatusEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.funds.confirmation.consent.mongo.db.adapter.repository.FundsConfirmationConsentRepository;
import com.capgemini.psd2.integration.controller.UpdateIntentController;
import com.capgemini.psd2.logger.LoggerUtils;
import com.capgemini.psd2.pisp.adapter.PispConsentAdapter;
import com.capgemini.psd2.pisp.domain.OBExternalConsentStatus1Code;
import com.capgemini.psd2.pisp.enums.PaymentTypeEnum;
import com.capgemini.psd2.pisp.payment.setup.platform.adapter.PaymentSetupPlatformAdapter;
import com.capgemini.psd2.pisp.sca.consent.operations.adapter.PispScaConsentOperationsAdapter;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageUpdateData;
import com.capgemini.psd2.pisp.status.PaymentConstants;
import com.capgemini.psd2.pisp.utilities.PispUtilities;
import com.capgemini.psd2.product.common.CompatibleVersionList;

public class UpdateIntentControllerTest {
	@Mock
	private LoggerUtils loggerUtils;

	@Mock
	private AispConsentAdapter aispConsentAdapter;

	@Mock
	private PispConsentAdapter pispConsentAdapter;

	@Mock
	private AccountRequestAdapter accountRequestAdapter;

	@Mock
	private AccountRequestCMA3Repository accountRequestRepository;

	@Mock
	private FundsConfirmationConsentRepository fundsConfirmationConsentRepository;

	@Mock
	private FundsConfirmationConsentAdapter fundsConfirmationConsentAdapter;

	@Mock
	private CispConsentAdapter cispConsentAdapter;

	@InjectMocks
	private UpdateIntentController controller;
	
	@Mock CompatibleVersionList compatibleVersionList;
	
	@Mock
	private PaymentSetupPlatformAdapter paymentSetupPlatformAdapter;

	@Mock
	private PispScaConsentOperationsAdapter pispStageOperationsAdapter;
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}

	@Test(expected = PSD2Exception.class)
	public void updateIndentScopeNull() {
		controller.updateIndent("1234", null);
	}

	@Test(expected = PSD2Exception.class)
	public void updateIndentScopeNotNullPISP() {
		controller.updateIndent("123456", "openid payments");
	}

	@Test(expected = PSD2Exception.class)
	public void updateIndentScopeNotNullFundsConfirmation() {
		Mockito.when(compatibleVersionList.fetchVersionList()).thenReturn(fetchVersionList());
		controller.updateIndent("123456", "OPENID FUNDSCONFIRMATIONS");
	}

	@Test(expected = PSD2Exception.class)
	public void updateIndentAispConsentNullTest() {
		AispConsent aispConsent = null;
		Mockito.when(aispConsentAdapter.retrieveConsentByAccountRequestIdAndStatus(anyObject(), anyObject()))
				.thenReturn(aispConsent);
		controller.updateIndent("1234", "OPENID ACCOUNTS");
	}

	@SuppressWarnings("unchecked")
	@Test
	public void updateIndentAispConsentNotNullTest() {
		OBReadConsentResponse1Data data1 = new OBReadConsentResponse1Data();
		data1.setStatus(OBReadConsentResponse1Data.StatusEnum.AUTHORISED);
		when(accountRequestRepository.findByConsentIdAndCmaVersionIn(anyString(), anyList())).thenReturn(data1);
		AispConsent aispConsent = new AispConsent();
		aispConsent.setConsentId("1234");
		aispConsent.setStatus(ConsentStatusEnum.EXPIRED);
		Mockito.when(aispConsentAdapter.retrieveConsentByAccountRequestIdAndStatus(anyObject(), anyObject()))
				.thenReturn(aispConsent);
		controller.updateIndent("1234", "OPENID ACCOUNTS");
	}
	
	
	@SuppressWarnings("unchecked")
	@Test
	public void updateIndentAispConsentAwaitingAuthorisation() {
		OBReadConsentResponse1Data data1 = new OBReadConsentResponse1Data();
		data1.setStatus(OBReadConsentResponse1Data.StatusEnum.AWAITINGAUTHORISATION);
		AispConsent aispConsent = new AispConsent();
		aispConsent.setStatus(ConsentStatusEnum.AUTHORISED);
		Mockito.when(compatibleVersionList.fetchVersionList()).thenReturn(fetchVersionList());
		when(accountRequestRepository.findByConsentIdAndCmaVersionIn(anyString(), anyList())).thenReturn(data1);
		Mockito.when(aispConsentAdapter.retrieveConsentByAccountRequestIdAndStatus(anyObject(), anyObject()))
				.thenReturn(aispConsent);
		controller.updateAispIntent("2345");
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void updateIndentAispConsentAwaitingAuthorisation2() {
		OBReadConsentResponse1Data data1 = new OBReadConsentResponse1Data();
		data1.setStatus(OBReadConsentResponse1Data.StatusEnum.AWAITINGAUTHORISATION);
		AispConsent aispConsent = new AispConsent();
		aispConsent.setStatus(ConsentStatusEnum.AWAITINGAUTHORISATION);
		Mockito.when(compatibleVersionList.fetchVersionList()).thenReturn(fetchVersionList());
		when(accountRequestRepository.findByConsentIdAndCmaVersionIn(anyString(), anyList())).thenReturn(data1);
		Mockito.when(aispConsentAdapter.retrieveConsentByAccountRequestIdAndStatus(anyObject(), anyObject()))
				.thenReturn(aispConsent);
		controller.updateAispIntent("2345");
	}
	
	@SuppressWarnings("unchecked")
	@Test(expected=PSD2Exception.class)
	public void updateIndentAispConsentAwaitingAuthorisation3() {
		OBReadConsentResponse1Data data1 = new OBReadConsentResponse1Data();
		data1.setStatus(OBReadConsentResponse1Data.StatusEnum.REJECTED);
		AispConsent aispConsent = new AispConsent();
		aispConsent.setStatus(ConsentStatusEnum.AWAITINGAUTHORISATION);
		Mockito.when(compatibleVersionList.fetchVersionList()).thenReturn(fetchVersionList());
		when(accountRequestRepository.findByConsentIdAndCmaVersionIn(anyString(), anyList())).thenReturn(data1);
		Mockito.when(aispConsentAdapter.retrieveConsentByAccountRequestIdAndStatus(anyObject(), anyObject()))
				.thenReturn(aispConsent);
		controller.updateAispIntent("2345");
	}

	@Test(expected = PSD2Exception.class)
	public void updateIndentPispConsentNullTest() {
		PispConsent pispConsent = null;
		Mockito.when(pispConsentAdapter.retrieveConsentByPaymentId(anyObject(), anyObject())).thenReturn(pispConsent);
		controller.updateIndent("1234", "OPENID PAYMENTS");
	}

	@Test(expected = PSD2Exception.class)
	public void updateIndentPispConsentNotNullTest() {
		PispConsent pispConsent = new PispConsent();
		String paymentConsentId = "1234";
		CustomPaymentStageIdentifiers customPaymentStageIdentifiers = new CustomPaymentStageIdentifiers();
		customPaymentStageIdentifiers.setPaymentConsentId(paymentConsentId);
		customPaymentStageIdentifiers.setPaymentSetupVersion("3.1");
		customPaymentStageIdentifiers.setPaymentTypeEnum(PaymentTypeEnum.INTERNATIONAL_SCH_PAY);
		
		pispConsent.setConsentId(paymentConsentId);
		pispConsent.setChannelId("12345");
		pispConsent.setPsuId("1234");
		
		CustomPaymentStageUpdateData updateData = new CustomPaymentStageUpdateData();
		updateData.setSetupStatusUpdated(Boolean.TRUE);
		updateData.setSetupStatus(OBExternalConsentStatus1Code.AUTHORISED.toString());
		updateData.setSetupStatusUpdateDateTime(PispUtilities.getCurrentDateInISOFormat());
		
		Map<String, String> params = null;
		
		Mockito.when(pispConsentAdapter.retrieveConsentByPaymentId(anyObject(), anyObject())).thenReturn(pispConsent);
		Mockito.when(paymentSetupPlatformAdapter.populateStageIdentifiers(anyString()))
				.thenReturn(customPaymentStageIdentifiers);
		Mockito.doNothing().when(pispStageOperationsAdapter).updatePaymentStageData(customPaymentStageIdentifiers,
				updateData, params);
		controller.updateIndent("1234", null);
	}

	@Test(expected = PSD2Exception.class)
	public void updateIndentPispChannelIdNullTest() {
		PispConsent pispConsent = new PispConsent();
		pispConsent.setConsentId("1234");
		Mockito.when(pispConsentAdapter.retrieveConsentByPaymentId(anyObject(), anyObject())).thenReturn(pispConsent);
		controller.updateIndent("1234", "OPENID PAYMENTS");
	}

	@Test
	public void updateIndentPispChannelIdNotNullTest() {
		PispConsent pispConsent = new PispConsent();
		CustomPaymentStageIdentifiers customPaymentStageIdentifiers = new CustomPaymentStageIdentifiers();
		pispConsent.setConsentId("1234");
		pispConsent.setChannelId("b365");
		pispConsent.setPsuId("32118465");
		pispConsent.setPartyIdentifier("BOI");
		customPaymentStageIdentifiers.setPaymentSetupVersion(PaymentConstants.CMA_FIRST_VERSION);
		customPaymentStageIdentifiers.setPaymentConsentId("23a2d463-f97e-4f29-a2e6-e624103b8634");
		customPaymentStageIdentifiers.setPaymentTypeEnum(PaymentTypeEnum.DOMESTIC_PAY);
		Mockito.when(paymentSetupPlatformAdapter.populateStageIdentifiers(anyString()))
				.thenReturn(customPaymentStageIdentifiers);
		Mockito.when(pispConsentAdapter.retrieveConsentByPaymentId(anyObject(), anyObject())).thenReturn(pispConsent);
		controller.updateIndent("1234", "OPENID PAYMENTS");
	}

	@SuppressWarnings("unchecked")
	@Test(expected = PSD2Exception.class)
	public void updateCispIntentConsentNull() {
		OBFundsConfirmationConsentResponse1Data fundsConfirmationConsentResponse = new OBFundsConfirmationConsentResponse1Data();
		fundsConfirmationConsentResponse
				.setStatus(OBFundsConfirmationConsentResponse1Data.StatusEnum.AUTHORISED);
		when(fundsConfirmationConsentRepository.findByConsentIdAndCmaVersionIn(anyString(),anyList()))
				.thenReturn(fundsConfirmationConsentResponse);
		Mockito.when(compatibleVersionList.fetchVersionList()).thenReturn(fetchVersionList());
		controller.updateCispIntent(null);
	}

	@SuppressWarnings("unchecked")
	@Test
	public void updateCispIntentAuthorised() {
		OBFundsConfirmationConsentResponse1Data fundsConfirmationConsentResponse = new OBFundsConfirmationConsentResponse1Data();
		fundsConfirmationConsentResponse
				.setStatus(OBFundsConfirmationConsentResponse1Data.StatusEnum.AUTHORISED);
		CispConsent cispConsent = new CispConsent();
		cispConsent.setStatus(ConsentStatusEnum.AWAITINGAUTHORISATION);
		cispConsent.setConsentId("123456");
		when(fundsConfirmationConsentRepository.findByConsentIdAndCmaVersionIn(anyString(),anyList()))
				.thenReturn(fundsConfirmationConsentResponse);
		when(cispConsentAdapter.retrieveConsentByFundsIntentIdAndStatus(anyString(), anyObject()))
				.thenReturn(cispConsent);
		controller.updateCispIntent("123456");
	}
	@SuppressWarnings("unchecked")
	@Test(expected=PSD2Exception.class)
	public void updateCispIntentAuthorised2() {
		OBFundsConfirmationConsentResponse1Data fundsConfirmationConsentResponse = new OBFundsConfirmationConsentResponse1Data();
		fundsConfirmationConsentResponse
				.setStatus(OBFundsConfirmationConsentResponse1Data.StatusEnum.REVOKED);
		CispConsent cispConsent = new CispConsent();
		cispConsent.setStatus(ConsentStatusEnum.AWAITINGAUTHORISATION);
		cispConsent.setConsentId("123456");
		when(fundsConfirmationConsentRepository.findByConsentIdAndCmaVersionIn(anyString(),anyList()))
				.thenReturn(fundsConfirmationConsentResponse);
		when(cispConsentAdapter.retrieveConsentByFundsIntentIdAndStatus(anyString(), anyObject()))
				.thenReturn(cispConsent);
		controller.updateCispIntent("123456");
	}

	@SuppressWarnings("unchecked")
	@Test
	public void updateCispIntentAwaitingAuthorisation() {
		OBFundsConfirmationConsentResponse1Data fundsConfirmationConsentResponse = new OBFundsConfirmationConsentResponse1Data();
		fundsConfirmationConsentResponse
				.setStatus(OBFundsConfirmationConsentResponse1Data.StatusEnum.AWAITINGAUTHORISATION);
		CispConsent cispConsent = new CispConsent();
		cispConsent.setStatus(ConsentStatusEnum.AUTHORISED);
		when(fundsConfirmationConsentRepository.findByConsentIdAndCmaVersionIn(anyString(),any()))
				.thenReturn(fundsConfirmationConsentResponse);
		when(cispConsentAdapter.retrieveConsentByFundsIntentIdAndStatus(anyString(), anyObject()))
				.thenReturn(cispConsent);
		controller.updateCispIntent("123456");
	}
	
	private List<String> fetchVersionList(){
		List<String> versionList = new ArrayList<>();
		versionList.add(null);
		versionList.add("1.0");
		versionList.add("3.0");
		return versionList;
	}
	
	@Test
	public void updateIndentPispConsentV1() {
		PispConsent pispConsent = new PispConsent();
		String paymentConsentId = "1234";
		CustomPaymentStageIdentifiers customPaymentStageIdentifiers = new CustomPaymentStageIdentifiers();
		customPaymentStageIdentifiers.setPaymentConsentId(paymentConsentId);
		customPaymentStageIdentifiers.setPaymentSetupVersion("1.0");
		customPaymentStageIdentifiers.setPaymentTypeEnum(PaymentTypeEnum.DOMESTIC_PAY);
		
		pispConsent.setConsentId(paymentConsentId);
		pispConsent.setChannelId("12345");
		pispConsent.setPsuId("1234");
		
		CustomPaymentStageUpdateData updateData = new CustomPaymentStageUpdateData();
		updateData.setSetupStatusUpdated(Boolean.TRUE);
		updateData.setSetupStatus(OBExternalConsentStatus1Code.AUTHORISED.toString());
		updateData.setSetupStatusUpdateDateTime(PispUtilities.getCurrentDateInISOFormat());
		
		Map<String, String> params = null;
		
		Mockito.when(pispConsentAdapter.retrieveConsentByPaymentId(anyObject(), anyObject())).thenReturn(pispConsent);
		Mockito.when(paymentSetupPlatformAdapter.populateStageIdentifiers(anyString())).thenReturn(customPaymentStageIdentifiers);
		Mockito.doNothing().when(pispStageOperationsAdapter).updatePaymentStageData(customPaymentStageIdentifiers, updateData, params);
		controller.updateIndent("1234", "openid payments");
	}
	
	@Test(expected =PSD2Exception.class)
	public void updateIndentPispConsent2() {
		PispConsent pispConsent = new PispConsent();
		String paymentConsentId = "1234";
		CustomPaymentStageIdentifiers customPaymentStageIdentifiers = new CustomPaymentStageIdentifiers();
		customPaymentStageIdentifiers.setPaymentConsentId(paymentConsentId);
		customPaymentStageIdentifiers.setPaymentSetupVersion("1.0");
		customPaymentStageIdentifiers.setPaymentTypeEnum(PaymentTypeEnum.DOMESTIC_PAY);
		
		pispConsent.setConsentId(paymentConsentId);
		pispConsent.setChannelId("");
		pispConsent.setPsuId("1234");
		
		CustomPaymentStageUpdateData updateData = new CustomPaymentStageUpdateData();
		updateData.setSetupStatusUpdated(Boolean.TRUE);
		updateData.setSetupStatus(OBExternalConsentStatus1Code.AUTHORISED.toString());
		updateData.setSetupStatusUpdateDateTime(PispUtilities.getCurrentDateInISOFormat());
		
		Map<String, String> params = null;
		
		Mockito.when(pispConsentAdapter.retrieveConsentByPaymentId(anyObject(), anyObject())).thenReturn(pispConsent);
		Mockito.when(paymentSetupPlatformAdapter.populateStageIdentifiers(anyString())).thenReturn(customPaymentStageIdentifiers);
		Mockito.doNothing().when(pispStageOperationsAdapter).updatePaymentStageData(customPaymentStageIdentifiers, updateData, params);
		controller.updateIndent("1234", "openid payments");
	}
	
	@Test(expected =PSD2Exception.class)
	public void updateIndentPispConsent3() {
		PispConsent pispConsent = new PispConsent();
		String paymentConsentId = "1234";
		CustomPaymentStageIdentifiers customPaymentStageIdentifiers = new CustomPaymentStageIdentifiers();
		customPaymentStageIdentifiers.setPaymentConsentId(paymentConsentId);
		customPaymentStageIdentifiers.setPaymentSetupVersion("1.0");
		customPaymentStageIdentifiers.setPaymentTypeEnum(PaymentTypeEnum.DOMESTIC_PAY);
		
		pispConsent.setConsentId(paymentConsentId);
		pispConsent.setChannelId("12345");
		pispConsent.setPsuId("");
		
		CustomPaymentStageUpdateData updateData = new CustomPaymentStageUpdateData();
		updateData.setSetupStatusUpdated(Boolean.TRUE);
		updateData.setSetupStatus(OBExternalConsentStatus1Code.AUTHORISED.toString());
		updateData.setSetupStatusUpdateDateTime(PispUtilities.getCurrentDateInISOFormat());
		
		Map<String, String> params = null;
		
		Mockito.when(pispConsentAdapter.retrieveConsentByPaymentId(anyObject(), anyObject())).thenReturn(pispConsent);
		Mockito.when(paymentSetupPlatformAdapter.populateStageIdentifiers(anyString())).thenReturn(customPaymentStageIdentifiers);
		Mockito.doNothing().when(pispStageOperationsAdapter).updatePaymentStageData(customPaymentStageIdentifiers, updateData, params);
		controller.updateIndent("1234", "openid payments");
	}
	
	@Test(expected =PSD2Exception.class)
	public void updateIndentPispConsent4() {
		PispConsent pispConsent = new PispConsent();
		String paymentConsentId = "1234";
		CustomPaymentStageIdentifiers customPaymentStageIdentifiers = new CustomPaymentStageIdentifiers();
		customPaymentStageIdentifiers.setPaymentConsentId(paymentConsentId);
		customPaymentStageIdentifiers.setPaymentSetupVersion("1.0");
		customPaymentStageIdentifiers.setPaymentTypeEnum(PaymentTypeEnum.DOMESTIC_PAY);
		
		pispConsent.setConsentId(paymentConsentId);
		pispConsent.setChannelId("12345");
		pispConsent.setPsuId(null);
		
		CustomPaymentStageUpdateData updateData = new CustomPaymentStageUpdateData();
		updateData.setSetupStatusUpdated(Boolean.TRUE);
		updateData.setSetupStatus(OBExternalConsentStatus1Code.AUTHORISED.toString());
		updateData.setSetupStatusUpdateDateTime(PispUtilities.getCurrentDateInISOFormat());
		
		Map<String, String> params = null;
		
		Mockito.when(pispConsentAdapter.retrieveConsentByPaymentId(anyObject(), anyObject())).thenReturn(pispConsent);
		Mockito.when(paymentSetupPlatformAdapter.populateStageIdentifiers(anyString())).thenReturn(customPaymentStageIdentifiers);
		Mockito.doNothing().when(pispStageOperationsAdapter).updatePaymentStageData(customPaymentStageIdentifiers, updateData, params);
		controller.updateIndent("1234", "openid payments");
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void updateIndentCispChannelIdNotNullTest() {
		/*CispConsent cispConsent = new CispConsent();
		OBFundsConfirmationConsentResponse1Data fundsConfirmationConsentResponseData = new OBFundsConfirmationConsentResponse1Data();
		OBFundsConfirmationConsentResponse1 fundsConfirmationConsentResponse = new OBFundsConfirmationConsentResponse1();
		cispConsent.setConsentId("123456");
//		cispConsent.setChannelId("b365");
//		cispConsent.setPsuId("32118465");
//		cispConsent.setPartyIdentifier("BOI");
		fundsConfirmationConsentResponseData.setCmaVersion("test1");
		fundsConfirmationConsentResponse.setData(fundsConfirmationConsentResponseData);
		Mockito.when(fundsConfirmationConsentAdapter.updateFundsConfirmationConsentResponse(anyString(),
				OBFundsConfirmationConsentResponse1Data.StatusEnum.AUTHORISED))
				.thenReturn(fundsConfirmationConsentResponse);
		Mockito.when(cispConsentAdapter.retrieveConsentByFundsIntentIdAndStatus(anyObject(), anyObject())).thenReturn(cispConsent);
		controller.updateIndent("123456", "OPENID FUNDSCONFIRMATIONS");
		*/
		OBFundsConfirmationConsentResponse1Data data1 = new OBFundsConfirmationConsentResponse1Data();
		data1.setStatus(OBFundsConfirmationConsentResponse1Data.StatusEnum.AUTHORISED);
		when(fundsConfirmationConsentRepository.findByConsentIdAndCmaVersionIn(anyString(), anyList())).thenReturn(data1);
		CispConsent cispConsent = new CispConsent();
		cispConsent.setConsentId("123456");
		cispConsent.setStatus(ConsentStatusEnum.EXPIRED);
		Mockito.when(cispConsentAdapter.retrieveConsentByFundsIntentIdAndStatus(anyObject(), anyObject()))
				.thenReturn(cispConsent);
		controller.updateIndent("123456", "OPENID FUNDSCONFIRMATIONS");
		
	}
}
