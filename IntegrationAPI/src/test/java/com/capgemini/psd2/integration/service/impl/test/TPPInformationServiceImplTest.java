package com.capgemini.psd2.integration.service.impl.test;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;

import java.util.ArrayList;
import java.util.List;

import javax.naming.directory.BasicAttributes;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.integration.adapter.TPPInformationAdaptor;
import com.capgemini.psd2.integration.dtos.TPPInformationDTO;
import com.capgemini.psd2.integration.service.impl.TPPInformationServiceImpl;

@RunWith(SpringJUnit4ClassRunner.class)
public class TPPInformationServiceImplTest

{
@Mock
private TPPInformationAdaptor tppInformationAdaptor;

@InjectMocks
private TPPInformationServiceImpl service;

 @Before
 public void setUp() throws Exception {
    MockitoAnnotations.initMocks(this);
   }

 @Test
	public void findTPPInformationTest() {
	 TPPInformationDTO tppInformationDTO = new TPPInformationDTO();
	 BasicAttributes tppInformationObj = new BasicAttributes();
	 BasicAttributes attr = new BasicAttributes(); 
	 tppInformationObj.put("x-roles", attr );
	 Mockito.when(tppInformationAdaptor.fetchTPPInformation(anyObject())).thenReturn(tppInformationObj);	 
	 service.findTPPInformation("123");	 
	 assertNotNull(tppInformationDTO);
 }  
 @Test
	public void findTPPInformationNullTest() {
	 TPPInformationDTO tppInformationDTO = new TPPInformationDTO();
	 Object tppInformationObj = new Object();
	
	 Mockito.when(tppInformationAdaptor.fetchTPPInformation(anyObject())).thenReturn(tppInformationObj);	 
	 service.findTPPInformation("123");	 
	 assertNotNull(tppInformationDTO);
 }  

	@Test
	public void testfetchClientInformation() {
		BasicAttributes tppInformationObj = new BasicAttributes();
		tppInformationObj.put("attrID", "attribute");
		List<Object> mockArrayList = new ArrayList<>();
		mockArrayList.add(0, tppInformationObj);
		Mockito.when(tppInformationAdaptor.fetchTppAppMappingForClient(anyString())).thenReturn(mockArrayList);
		service.fetchClientInformation("tenant1", "basicAttribute");
	}

	@Test
	public void testfetchJWKSUrlWithRequestURL() {
		BasicAttributes tppInformationObj = new BasicAttributes();
		tppInformationObj.put("softwareJwksEndpoint", "attribute");
		List<Object> mockArrayList = new ArrayList<>();
		mockArrayList.add(0, tppInformationObj);
		Mockito.when(tppInformationAdaptor.fetchTppAppMappingForClient(anyString())).thenReturn(mockArrayList);
		service.fetchJWKSUrl("tenant1");
	}

	@Test
	public void testfetchJWKSUrlssaTokenNullRequestURL() {
		BasicAttributes tppInformationObj = new BasicAttributes();
		tppInformationObj.put("ssaToken",
				"eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOlsiYmFua0FjY291bnRUcmFuc2FjdGlvbnMiXSwiZXhwIjoxNDg1NTkxODY1LCJ1c2VyX25hbWUiOiJkZW1vdXNlciIsImp0aSI6ImI1ZmFiOTI3LWQ3NDItNDc1MC05ZTE5LWFkMjQ5ODY4ZTBlZSIsImNsaWVudF9pZCI6Im1vbmV5d2lzZSIsInNjb3BlIjpbIntcInJlc291cmNlTmFtZVwiOlwiYmFsYW5jZVwiLFwiY2xhaW1zXCI6W3tcImFjY291bnROdW1iZXJcIjpcIkVFMDYyMjAwMjIyMDEwMTkyMjM4XCIsXCJiYWxcIjp0cnVlLFwiYWNjdFwiOntcImNjeVwiOnRydWUsXCJvd25yXCI6dHJ1ZSxcImlkXCI6dHJ1ZX19XX0iLCJ7XCJyZXNvdXJjZU5hbWVcIjpcInRyYW5zYWN0aW9uXCIsXCJjbGFpbXNcIjpbe1wiYWNjb3VudE51bWJlclwiOlwiRUUwNjIyMDAyMjIwMTAxOTIyMzhcIixcInN0YXRlbWVudE1vbnRoc1wiOlwiNlwifV19Il19.SMNi5VkmXCbAjgQXdpPQM-WKb2oVp2eAlPLe_xh29tZamwLbjhsOksb_G-V6redvk-zwU_nBeO6vyMU1LCYjX5cip30fxA6ZA3_LAddKtesSNNbmNVqpwBnD_1wjcVaY9g7fmp-jbzF10CmI417hSb6x5HXPSAZcVioBWGGDclDjLzR4FcdQ-k58nFN3nOWqG-l29UPT4Aj9t305L2D4JiltILSaBNcIVKyTsSlJpELyx2qkDNwTHYQUzJc-fZoPD2E4-_Oweg6VAyRgtUGp7l-yd14rTk7ZX6Sfz4jX0eZZfd9VEqIbyRlrR9pJNyIsfVDPhZi4xHKH9cv5u5anhA");
		List<Object> mockArrayList = new ArrayList<>();
		mockArrayList.add(0, tppInformationObj);
		Mockito.when(tppInformationAdaptor.fetchTppAppMappingForClient(anyString())).thenReturn(mockArrayList);
		service.fetchJWKSUrl("tenant1");
	}
  }