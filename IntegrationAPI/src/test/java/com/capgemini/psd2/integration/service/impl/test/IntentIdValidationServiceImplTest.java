package com.capgemini.psd2.integration.service.impl.test;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.account.request.mongo.db.adapter.repository.AccountRequestCMA3Repository;
import com.capgemini.psd2.aisp.consent.adapter.repository.AispConsentMongoRepository;
import com.capgemini.psd2.aisp.domain.OBReadConsentResponse1Data;
import com.capgemini.psd2.cisp.consent.adapter.repository.CispConsentMongoRepository;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmationConsentResponse1Data;
import com.capgemini.psd2.consent.domain.AispConsent;
import com.capgemini.psd2.consent.domain.CispConsent;
import com.capgemini.psd2.enums.ConsentStatusEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.funds.confirmation.consent.mongo.db.adapter.repository.FundsConfirmationConsentRepository;
import com.capgemini.psd2.integration.service.impl.IntentIdValidationServiceImpl;
import com.capgemini.psd2.pisp.domain.OBExternalConsentStatus1Code;
import com.capgemini.psd2.pisp.domain.PaymentConsentsPlatformResource;
import com.capgemini.psd2.pisp.payment.setup.platform.adapter.PaymentSetupPlatformAdapter;
import com.capgemini.psd2.pisp.status.PaymentStatusEnum;
import com.capgemini.psd2.product.common.CompatibleVersionList;


public class IntentIdValidationServiceImplTest {
	@Mock
	private AccountRequestCMA3Repository accountRequestRepository;
	
	@Mock
	private AispConsentMongoRepository accountConsentRepository;

	@Mock
	private PaymentSetupPlatformAdapter paymentSetupPlatformAdapter;
	
	@Mock
	private FundsConfirmationConsentRepository fundsConfirmationConsentRepository;

	@Mock
	private CispConsentMongoRepository cispConsentRepository;
	
	@Mock CompatibleVersionList compatibleVersionList;	
	
	@Mock 
	private Calendar creationDate;
	
	@InjectMocks
	private IntentIdValidationServiceImpl service;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		ReflectionTestUtils.setField(service, "accountRequestExpiryTime", 24);
	}

	@Test(expected=PSD2Exception.class)
	public void validateAispIntentRevoked(){
		OBReadConsentResponse1Data accountRequestResponse = new OBReadConsentResponse1Data();
		accountRequestResponse.setCreationDateTime("2014-01-19T00:00:00+05:30");
		accountRequestResponse.setTppCID("123456");
		accountRequestResponse.setStatus(OBReadConsentResponse1Data.StatusEnum.REVOKED);
		Mockito.when(compatibleVersionList.fetchVersionList()).thenReturn(fetchVersionList());
		when(accountRequestRepository.findByConsentIdAndCmaVersionIn(anyString(),any())).thenReturn(accountRequestResponse);
		service.validateAispIntent("123456", "123456");
	}
	
	@Test(expected=PSD2Exception.class)
	public void validateAispIntentDeleted(){
		OBReadConsentResponse1Data accountRequestResponse = new OBReadConsentResponse1Data();
		accountRequestResponse.setCreationDateTime("2014-01-19T00:00:00+05:30");
		accountRequestResponse.setTppCID("123456");
		accountRequestResponse.setStatus(OBReadConsentResponse1Data.StatusEnum.DELETED);
		Mockito.when(compatibleVersionList.fetchVersionList()).thenReturn(fetchVersionList());
		when(accountRequestRepository.findByConsentIdAndCmaVersionIn(anyString(),any())).thenReturn(accountRequestResponse);
		service.validateAispIntent("123456", "123456");
	}
	
	@Test(expected=PSD2Exception.class)
	public void validateAispIntentExpirationDate(){
		OBReadConsentResponse1Data accountRequestResponse = new OBReadConsentResponse1Data();
		accountRequestResponse.setCreationDateTime("2014-01-19T00:00:00+05:30");
		accountRequestResponse.setExpirationDateTime("2017-01-19T00:00:00+05:30");
		accountRequestResponse.setTppCID("123456");
		accountRequestResponse.setStatus(OBReadConsentResponse1Data.StatusEnum.AUTHORISED);
		ReflectionTestUtils.setField(service, "accountRequestExpiryTime", 24);
		Mockito.when(compatibleVersionList.fetchVersionList()).thenReturn(fetchVersionList());
		when(accountRequestRepository.findByConsentIdAndCmaVersionIn(anyString(),any())).thenReturn(accountRequestResponse);
		service.validateAispIntent("123456", "123456");
	}
	
	@Test(expected=PSD2Exception.class)
	public void validateAispIntentExpirationDateConsentRejected(){
		OBReadConsentResponse1Data accountRequestResponse = new OBReadConsentResponse1Data();
		accountRequestResponse.setCreationDateTime("2014-01-19T00:00:00+05:30");
		accountRequestResponse.setExpirationDateTime("2019-01-19T00:00:00+05:30");
		accountRequestResponse.setTppCID("123456");
		accountRequestResponse.setStatus(OBReadConsentResponse1Data.StatusEnum.AWAITINGAUTHORISATION);
		ReflectionTestUtils.setField(service, "accountRequestExpiryTime", 24);
		AispConsent aispConsent = new AispConsent();
		aispConsent.setStatus(ConsentStatusEnum.REJECTED);
		Mockito.when(compatibleVersionList.fetchVersionList()).thenReturn(fetchVersionList());
		when(accountRequestRepository.findByConsentIdAndCmaVersionIn(anyString(),any())).thenReturn(accountRequestResponse);
		when(accountConsentRepository.findByAccountRequestIdAndStatusAndCmaVersionIn(anyString(), anyObject(), any())).thenReturn(aispConsent);
		service.validateAispIntent("123456", "123456");
	}
	
	@Test(expected=PSD2Exception.class)
	public void validateAispIntentExpirationDateConsentNull(){
		OBReadConsentResponse1Data accountRequestResponse = new OBReadConsentResponse1Data();
		accountRequestResponse.setCreationDateTime("2014-01-19T00:00:00+05:30");
		accountRequestResponse.setExpirationDateTime("2019-01-19T00:00:00+05:30");
		accountRequestResponse.setTppCID("123456");
		accountRequestResponse.setStatus(OBReadConsentResponse1Data.StatusEnum.AWAITINGAUTHORISATION);
		ReflectionTestUtils.setField(service, "accountRequestExpiryTime", 24);
		Mockito.when(compatibleVersionList.fetchVersionList()).thenReturn(fetchVersionList());
		when(accountRequestRepository.findByConsentIdAndCmaVersionIn(anyString(),any())).thenReturn(accountRequestResponse);
		service.validateAispIntent("123456", "123456");
	}
	
	@Test(expected=PSD2Exception.class)
	public void validateAispIntentDataResponseNull(){
		Mockito.when(compatibleVersionList.fetchVersionList()).thenReturn(fetchVersionList());
		service.validateAispIntent("123456", "123456");
	}
	
	@Test(expected=PSD2Exception.class)
	public void validatePispIntentCreationDateException(){
		PaymentConsentsPlatformResource paymentSetUpResponse = new PaymentConsentsPlatformResource();
		paymentSetUpResponse.setCreatedAt("2014-01-19T00:00:00+05:30");
		paymentSetUpResponse.setTppCID("123456");
		paymentSetUpResponse.setStatus("AcceptedTechnicalValidation");
		ReflectionTestUtils.setField(service, "accountRequestExpiryTime", 24);
		when(paymentSetupPlatformAdapter.retrievePaymentSetupPlatformResource(anyString())).thenReturn(paymentSetUpResponse);
		service.validatePispIntent("123456", "123456");
	}
	
	@Test(expected=PSD2Exception.class)
	public void validatePispIntentResourceNotFound(){
		service.validatePispIntent("123456", "123456");
	}
	
	@Test
	public void validateIntentIdAccountRequestResponseNotNullTest() {
		OBReadConsentResponse1Data accountRequestResponse = new OBReadConsentResponse1Data();
		accountRequestResponse.setCreationDateTime("2014-01-19T00:00:00+05:30");
		accountRequestResponse.setTppCID("123");
		accountRequestResponse.setStatus(OBReadConsentResponse1Data.StatusEnum.AUTHORISED);
		ReflectionTestUtils.setField(service, "accountRequestExpiryTime", 24);
		Mockito.when(compatibleVersionList.fetchVersionList()).thenReturn(fetchVersionList());
		Mockito.when(accountRequestRepository.findByConsentIdAndCmaVersionIn(anyString(),any())).thenReturn(accountRequestResponse);
		Mockito.when(accountConsentRepository.findByAccountRequestIdAndStatusAndCmaVersionIn(anyString(),anyObject(),any())).thenReturn(new AispConsent());
		
		service.validateIntentId("999", "123", "openid accounts");
	}
	
	@Test(expected = PSD2Exception.class)
	public void validateIntentIdAccountRequestResponseNotNullTestWithStatusRevoked() {
		OBReadConsentResponse1Data accountRequestResponse = new OBReadConsentResponse1Data();
		AispConsent aispConsent = new AispConsent();
				
		accountRequestResponse.setCreationDateTime("2014-01-19T00:00:00+05:30");
		accountRequestResponse.setTppCID("123456");
		accountRequestResponse.setStatus(OBReadConsentResponse1Data.StatusEnum.AWAITINGAUTHORISATION);
		ReflectionTestUtils.setField(service, "accountRequestExpiryTime", 24);
		Mockito.when(compatibleVersionList.fetchVersionList()).thenReturn(fetchVersionList());
		Mockito.when(accountRequestRepository.findByConsentIdAndCmaVersionIn(anyString(),any())).thenReturn(accountRequestResponse);
		Mockito.when(accountConsentRepository.findByAccountRequestIdAndStatusAndCmaVersionIn(anyString(),anyObject(),any())).thenReturn(aispConsent);
		
		service.validateIntentId("999", "123456", "openid accounts");
	}
	
	@Test(expected = PSD2Exception.class)
	public void validateIntentIdAccountRequestResponseNotNullTestWithAispConsentNull() {
		OBReadConsentResponse1Data accountRequestResponse = new OBReadConsentResponse1Data();
					
		accountRequestResponse.setCreationDateTime("2014-01-19T00:00:00+05:30");
		accountRequestResponse.setTppCID("123456");
		ReflectionTestUtils.setField(service, "accountRequestExpiryTime", 24);
		Mockito.when(compatibleVersionList.fetchVersionList()).thenReturn(fetchVersionList());
		Mockito.when(accountRequestRepository.findByConsentIdAndCmaVersionIn(anyString(),any())).thenReturn(accountRequestResponse);
		Mockito.when(accountConsentRepository.findByAccountRequestIdAndStatusAndCmaVersionIn(anyString(),anyObject(),any())).thenReturn(null);
		
		accountRequestResponse.setStatus(OBReadConsentResponse1Data.StatusEnum.AWAITINGAUTHORISATION);
		
		
		service.validateIntentId("999", "123456", "openid accounts");
	}
	
	@Test(expected=PSD2Exception.class)
	public void validateIntentIdFundsFlow(){
		Mockito.when(compatibleVersionList.fetchVersionList()).thenReturn(fetchVersionList());
		service.validateIntentId("999", "123", "openid fundsconfirmations");
	}
	
	@Test(expected = PSD2Exception.class)
	public void validateIntentIdAccountRequestResponseNullTest() {
		OBReadConsentResponse1Data accountRequestResponse = null;
		Mockito.when(compatibleVersionList.fetchVersionList()).thenReturn(fetchVersionList());
		Mockito.when(accountRequestRepository.findByConsentIdAndCmaVersionIn(anyObject(),any())).thenReturn(accountRequestResponse);
		service.validateIntentId("999", "123", "openid accounts");
	}

	@Test(expected=PSD2Exception.class)
	public void validateIntentIdPispFlow(){
		service.validateIntentId("999", "123", "openid payments");
	}
	
	@Test(expected = PSD2Exception.class)
	public void validateIntentIdpaymentSetUpResponseNullTest() {
		PaymentConsentsPlatformResource paymentSetUpResponse = new PaymentConsentsPlatformResource();
		paymentSetUpResponse.setCreatedAt("2019-01-19T00:00:00+05:30");
		paymentSetUpResponse.setTppCID("1234");
		Mockito.when(paymentSetupPlatformAdapter.retrievePaymentSetupPlatformResource(anyString(),anyString()))
				.thenReturn(paymentSetUpResponse);
	
		service.validateIntentId("999", "123", "openid payments");
	}

	@Test(expected = PSD2Exception.class)
	public void validateIntentIdpaymentSetUpResponseNullTest1() {
		PaymentConsentsPlatformResource paymentSetUpResponse = null;
		Mockito.when(paymentSetupPlatformAdapter.retrievePaymentSetupPlatformResource(anyObject()))
				.thenReturn(paymentSetUpResponse);
		service.validateIntentId("999", "123", "openid payments");
	}
	
	@Test(expected = PSD2Exception.class)
	public void validateIntentIdpaymentSetUpResponseNullTest2() {
		PaymentConsentsPlatformResource paymentSetUpResponse = new PaymentConsentsPlatformResource();
		paymentSetUpResponse.setCreatedAt("2019-01-19T00:00:00+05:30");
		paymentSetUpResponse.setTppCID("123");
		paymentSetUpResponse.setStatus("authorised");
		ReflectionTestUtils.setField(service, "accountRequestExpiryTime", 24);
		
		Mockito.when(paymentSetupPlatformAdapter.retrievePaymentSetupPlatformResource(anyString(),anyString()))
				.thenReturn(paymentSetUpResponse);
	
		service.validateIntentId("999", "123", "openid payments");
	}
	
	@Test(expected = PSD2Exception.class)
	public void validateIntentIdpaymentSetUpResponseNullTest3() {
		PaymentConsentsPlatformResource paymentSetUpResponse = new PaymentConsentsPlatformResource();
		paymentSetUpResponse.setCreatedAt("2019-01-19T00:00:00+05:30");
		paymentSetUpResponse.setTppCID("123");
		paymentSetUpResponse.setStatus(PaymentStatusEnum.ACCEPTEDTECHNICALVALIDATION.getStatusCode());
		ReflectionTestUtils.setField(service, "accountRequestExpiryTime", 24);
		
		Mockito.when(paymentSetupPlatformAdapter.retrievePaymentSetupPlatformResource(anyString(),anyString()))
				.thenReturn(paymentSetUpResponse);
	
		service.validateIntentId("999", "123", "openid payments");
	}
	
	@Test(expected = PSD2Exception.class)
	public void validateIntentIdpaymentSetUpResponseNullTest4() {
		PaymentConsentsPlatformResource paymentSetUpResponse = new PaymentConsentsPlatformResource();
		paymentSetUpResponse.setCreatedAt("2019-01-19T00:00:00+05:30");
		paymentSetUpResponse.setTppCID("123");
		paymentSetUpResponse.setStatus(OBExternalConsentStatus1Code.AWAITINGAUTHORISATION.toString());
		ReflectionTestUtils.setField(service, "accountRequestExpiryTime", 24);
		
		Mockito.when(paymentSetupPlatformAdapter.retrievePaymentSetupPlatformResource(anyString(),anyString()))
				.thenReturn(paymentSetUpResponse);
	
		service.validateIntentId("999", "123", "openid payments");
	}
	@Test(expected = PSD2Exception.class)
	public void validateIntentIdpaymentSetUpResponseTest() {
		service.validateIntentId("999", "123", "openid");
	}
	
	@Test(expected=PSD2Exception.class)
	public void validateCispIntentStatusRevoked(){
		OBFundsConfirmationConsentResponse1Data fundsConfirmationConsentResponse = new OBFundsConfirmationConsentResponse1Data();
		CispConsent cispConsent = new CispConsent();
		fundsConfirmationConsentResponse.setCreationDateTime("2014-01-19T00:00:00+05:30");
		fundsConfirmationConsentResponse.setTppCID("123456");
		fundsConfirmationConsentResponse.setStatus(OBFundsConfirmationConsentResponse1Data.StatusEnum.REVOKED);
		ReflectionTestUtils.setField(service, "accountRequestExpiryTime", 24);
		Mockito.when(compatibleVersionList.fetchVersionList()).thenReturn(fetchVersionList());
		when(fundsConfirmationConsentRepository.findByConsentIdAndCmaVersionIn(anyString(),any())).thenReturn(fundsConfirmationConsentResponse);
		when(cispConsentRepository.findByFundsIntentIdAndStatusAndCmaVersionIn(anyString(), anyObject(),any())).thenReturn(cispConsent);
		service.validateCispIntent("123456", "123456");
	}
	
	@Test(expected=PSD2Exception.class)
	public void validateCispIntentStatusDeleted(){
		OBFundsConfirmationConsentResponse1Data fundsConfirmationConsentResponse = new OBFundsConfirmationConsentResponse1Data();
		CispConsent cispConsent = new CispConsent();
		fundsConfirmationConsentResponse.setCreationDateTime("2014-01-19T00:00:00+05:30");
		fundsConfirmationConsentResponse.setTppCID("123456");
		fundsConfirmationConsentResponse.setStatus(OBFundsConfirmationConsentResponse1Data.StatusEnum.DELETED);
		ReflectionTestUtils.setField(service, "accountRequestExpiryTime", 24);
		Mockito.when(compatibleVersionList.fetchVersionList()).thenReturn(fetchVersionList());
		when(fundsConfirmationConsentRepository.findByConsentIdAndCmaVersionIn(anyString(),any())).thenReturn(fundsConfirmationConsentResponse);
		when(cispConsentRepository.findByFundsIntentIdAndStatusAndCmaVersionIn(anyString(), anyObject(),any())).thenReturn(cispConsent);
		service.validateCispIntent("123456", "123456");
	}
	
	@Test(expected=PSD2Exception.class)
	public void validateCispIntentExpirationTimeException(){
		OBFundsConfirmationConsentResponse1Data fundsConfirmationConsentResponse = new OBFundsConfirmationConsentResponse1Data();
		CispConsent cispConsent = new CispConsent();
		fundsConfirmationConsentResponse.setCreationDateTime("2014-01-19T00:00:00+05:30");
		fundsConfirmationConsentResponse.setExpirationDateTime("2017-01-19T00:00:00+05:30");
		fundsConfirmationConsentResponse.setTppCID("123456");
		fundsConfirmationConsentResponse.setStatus(OBFundsConfirmationConsentResponse1Data.StatusEnum.AWAITINGAUTHORISATION);
		ReflectionTestUtils.setField(service, "accountRequestExpiryTime", 24);
		Mockito.when(compatibleVersionList.fetchVersionList()).thenReturn(fetchVersionList());
		when(fundsConfirmationConsentRepository.findByConsentIdAndCmaVersionIn(anyString(),any())).thenReturn(fundsConfirmationConsentResponse);
		when(cispConsentRepository.findByFundsIntentIdAndStatusAndCmaVersionIn(anyString(), anyObject(),any())).thenReturn(cispConsent);
		service.validateCispIntent("123456", "123456");
	}
	@Test(expected=PSD2Exception.class)
	public void validateCispIntent(){
		OBFundsConfirmationConsentResponse1Data fundsConfirmationConsentResponse = new OBFundsConfirmationConsentResponse1Data();
		CispConsent cispConsent = new CispConsent();
		fundsConfirmationConsentResponse.setCreationDateTime("2014-01-19T00:00:00+05:30");
		fundsConfirmationConsentResponse.setExpirationDateTime("2017-01-19T00:00:00+05:30");
		fundsConfirmationConsentResponse.setTppCID("434");
		fundsConfirmationConsentResponse.setStatus(OBFundsConfirmationConsentResponse1Data.StatusEnum.AWAITINGAUTHORISATION);
		ReflectionTestUtils.setField(service, "accountRequestExpiryTime", 24);
		Mockito.when(compatibleVersionList.fetchVersionList()).thenReturn(fetchVersionList());
		when(fundsConfirmationConsentRepository.findByConsentIdAndCmaVersionIn(anyString(),any())).thenReturn(fundsConfirmationConsentResponse);
		when(cispConsentRepository.findByFundsIntentIdAndStatusAndCmaVersionIn(anyString(), anyObject(),any())).thenReturn(cispConsent);
		service.validateCispIntent("123456", "123456");
	}
	
	@Test(expected=PSD2Exception.class)
	public void validateCispIntentNotAuthorized(){
		OBFundsConfirmationConsentResponse1Data fundsConfirmationConsentResponse = new OBFundsConfirmationConsentResponse1Data();
		CispConsent cispConsent = new CispConsent();
		fundsConfirmationConsentResponse.setCreationDateTime("2014-01-19T00:00:00+05:30");
		fundsConfirmationConsentResponse.setExpirationDateTime("2019-01-19T00:00:00+05:30");
		fundsConfirmationConsentResponse.setTppCID("123456");
		fundsConfirmationConsentResponse.setStatus(OBFundsConfirmationConsentResponse1Data.StatusEnum.AWAITINGAUTHORISATION);
		ReflectionTestUtils.setField(service, "accountRequestExpiryTime", 24);
		Mockito.when(compatibleVersionList.fetchVersionList()).thenReturn(fetchVersionList());
		when(fundsConfirmationConsentRepository.findByConsentIdAndCmaVersionIn(anyString(),any())).thenReturn(fundsConfirmationConsentResponse);
		when(cispConsentRepository.findByFundsIntentIdAndStatusAndCmaVersionIn(anyString(), anyObject(),any())).thenReturn(cispConsent);
		service.validateCispIntent("123456", "123456");
	}
	
	@Test
	public void validateCispIntentNotAuthorizedNotNull(){
		OBFundsConfirmationConsentResponse1Data fundsConfirmationConsentResponse = new OBFundsConfirmationConsentResponse1Data();
		CispConsent cispConsent = new CispConsent();
		cispConsent.setChannelId("test");
		fundsConfirmationConsentResponse.setCreationDateTime("2014-01-19T00:00:00+05:30");
		fundsConfirmationConsentResponse.setExpirationDateTime("2023-01-19T00:00:00+05:30");
		fundsConfirmationConsentResponse.setTppCID("123456");
		fundsConfirmationConsentResponse.setStatus(OBFundsConfirmationConsentResponse1Data.StatusEnum.AWAITINGAUTHORISATION);
		ReflectionTestUtils.setField(service, "accountRequestExpiryTime", 24);
		Mockito.when(compatibleVersionList.fetchVersionList()).thenReturn(fetchVersionList());
		when(fundsConfirmationConsentRepository.findByConsentIdAndCmaVersionIn(anyString(),any())).thenReturn(fundsConfirmationConsentResponse);
		when(cispConsentRepository.findByFundsIntentIdAndStatusAndCmaVersionIn(anyString(), anyObject(),any())).thenReturn(cispConsent);
		service.validateCispIntent("123456", "123456");
	}
	
	@Test(expected=PSD2Exception.class)
	public void validateCispIntentNotAuthorizedNotNull1(){
		OBFundsConfirmationConsentResponse1Data fundsConfirmationConsentResponse = new OBFundsConfirmationConsentResponse1Data();
		CispConsent cispConsent = new CispConsent();
		cispConsent.setChannelId("test");
		fundsConfirmationConsentResponse.setCreationDateTime("2014-01-19T00:00:00+05:30");
		fundsConfirmationConsentResponse.setExpirationDateTime("2023-01-19T00:00:00+05:30");
		fundsConfirmationConsentResponse.setTppCID("123456");
		fundsConfirmationConsentResponse.setStatus(OBFundsConfirmationConsentResponse1Data.StatusEnum.REJECTED);
		ReflectionTestUtils.setField(service, "accountRequestExpiryTime", 24);
		Mockito.when(compatibleVersionList.fetchVersionList()).thenReturn(fetchVersionList());
		when(fundsConfirmationConsentRepository.findByConsentIdAndCmaVersionIn(anyString(),any())).thenReturn(fundsConfirmationConsentResponse);
		when(cispConsentRepository.findByFundsIntentIdAndStatusAndCmaVersionIn(anyString(), anyObject(),any())).thenReturn(cispConsent);
		service.validateCispIntent("123456", "123456");
	}
	
	@Test(expected=PSD2Exception.class)
	public void validateCispIntentNotAuthorizedNotNull2(){
		OBFundsConfirmationConsentResponse1Data fundsConfirmationConsentResponse = new OBFundsConfirmationConsentResponse1Data();
		CispConsent cispConsent = new CispConsent();
		cispConsent.setChannelId("test");
		fundsConfirmationConsentResponse.setCreationDateTime("2014-01-19T00:00:00+05:30");
		fundsConfirmationConsentResponse.setExpirationDateTime("2023-01-19T00:00:00+05:30");
		fundsConfirmationConsentResponse.setTppCID("123456");
		fundsConfirmationConsentResponse.setStatus(OBFundsConfirmationConsentResponse1Data.StatusEnum.AUTHORISED);
		ReflectionTestUtils.setField(service, "accountRequestExpiryTime", 24);
		Mockito.when(compatibleVersionList.fetchVersionList()).thenReturn(fetchVersionList());
		when(fundsConfirmationConsentRepository.findByConsentIdAndCmaVersionIn(anyString(),any())).thenReturn(fundsConfirmationConsentResponse);
		when(cispConsentRepository.findByFundsIntentIdAndStatusAndCmaVersionIn(anyString(), anyObject(),any())).thenReturn(cispConsent);
		service.validateCispIntent("123456", "123456");
	}

	@Test(expected=PSD2Exception.class)
	public void validateCispIntentWithExpirationDateError(){
		OBFundsConfirmationConsentResponse1Data fundsConfirmationConsentResponse = new OBFundsConfirmationConsentResponse1Data();
		fundsConfirmationConsentResponse.setCreationDateTime("2014-01-19T00:00:00+05:30");
		fundsConfirmationConsentResponse.setExpirationDateTime("2019-01-19T00:00:00+05:30");
		fundsConfirmationConsentResponse.setTppCID("123456");
		fundsConfirmationConsentResponse.setStatus(OBFundsConfirmationConsentResponse1Data.StatusEnum.AWAITINGAUTHORISATION);
		ReflectionTestUtils.setField(service, "accountRequestExpiryTime", 24);
		Mockito.when(compatibleVersionList.fetchVersionList()).thenReturn(fetchVersionList());
		when(fundsConfirmationConsentRepository.findByConsentIdAndCmaVersionIn(anyString(),any())).thenReturn(fundsConfirmationConsentResponse);
		service.validateCispIntent("123456", "123456");
	}
	
	@Test(expected=PSD2Exception.class)
	public void validateCispIntentWithInvalidStatus_ConsentNull(){
		OBFundsConfirmationConsentResponse1Data fundsConfirmationConsentResponse = new OBFundsConfirmationConsentResponse1Data();
		
		fundsConfirmationConsentResponse.setCreationDateTime("2014-01-19T00:00:00+05:30");
		fundsConfirmationConsentResponse.setExpirationDateTime("2019-12-19T00:00:00+05:30");
		fundsConfirmationConsentResponse.setTppCID("123456");
		fundsConfirmationConsentResponse.setStatus(OBFundsConfirmationConsentResponse1Data.StatusEnum.AWAITINGAUTHORISATION);
		ReflectionTestUtils.setField(service, "accountRequestExpiryTime", 24);
		Mockito.when(compatibleVersionList.fetchVersionList()).thenReturn(fetchVersionList());
		when(fundsConfirmationConsentRepository.findByConsentIdAndCmaVersionIn(anyString(),any())).thenReturn(fundsConfirmationConsentResponse);
		service.validateCispIntent("123456", "123456");
	}
	
	@Test(expected=PSD2Exception.class)
	public void validateCispIntentWithInvalidStatus_ConsentNotNull(){
		OBFundsConfirmationConsentResponse1Data fundsConfirmationConsentResponse = new OBFundsConfirmationConsentResponse1Data();
		CispConsent cispConsent = new CispConsent();
		fundsConfirmationConsentResponse.setCreationDateTime("2014-01-19T00:00:00+05:30");
		fundsConfirmationConsentResponse.setExpirationDateTime("2019-12-19T00:00:00+05:30");
		fundsConfirmationConsentResponse.setTppCID("123456");
		fundsConfirmationConsentResponse.setStatus(OBFundsConfirmationConsentResponse1Data.StatusEnum.AUTHORISED);
		ReflectionTestUtils.setField(service, "accountRequestExpiryTime", 24);
		Mockito.when(compatibleVersionList.fetchVersionList()).thenReturn(fetchVersionList());
		when(fundsConfirmationConsentRepository.findByConsentIdAndCmaVersionIn(anyString(),any())).thenReturn(fundsConfirmationConsentResponse);
		when(cispConsentRepository.findByFundsIntentIdAndStatusAndCmaVersionIn(anyString(), anyObject(),any())).thenReturn(cispConsent);
		service.validateCispIntent("123456", "123456");
	}
	
	@Test(expected=PSD2Exception.class)
	public void validateCispIntentFundsNotFound(){
		when(fundsConfirmationConsentRepository.findByConsentIdAndCmaVersionIn(anyString(),any())).thenReturn(null);
		service.validateCispIntent("123456", "123456");
	}
	
	@After
	public void tearDown() throws Exception {
		service = null;
	}

	private List<String> fetchVersionList(){
		List<String> versionList = new ArrayList<>();
		versionList.add(null);
		versionList.add("1.0");
		versionList.add("3.0");
		return versionList;
	}
}
