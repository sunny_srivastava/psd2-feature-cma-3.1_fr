package com.capgemini.psd2.integration.service.impl.test;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.integration.dtos.CPNPEntity;
import com.capgemini.psd2.integration.repository.CPNPRepository;
import com.capgemini.psd2.integration.service.impl.CPNPOperationsServiceImpl;
import com.capgemini.psd2.product.common.CompatibleVersionList;

public class CPNPOperationsServiceImplTest {
	
	@Mock
	private CPNPRepository repository;

	@InjectMocks
	private CPNPOperationsServiceImpl service;
	
	@Mock
	private CompatibleVersionList compatibleVersionList;
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void MaxRequestsAllowedtestGetAndSet() {
		service.setMaxRequestsAllowed(1);
		service.getMaxRequestsAllowed();
	}
	
	@Test
	public void testVerifyClient() {
		List<CPNPEntity> cpnpEntityList=new ArrayList<>();
		ReflectionTestUtils.setField(service, "maxRequestsAllowed", 4);
		Mockito.when(compatibleVersionList.fetchVersionList()).thenReturn(fetchVersionList());
		when(repository.findByPsuIdAndTppIdAndCmaVersionIn(anyString(),anyString(),any())).thenReturn(cpnpEntityList);
		service.verifyClient("001000589JE", "88888888");
	}
	
	@Test(expected=PSD2Exception.class)
	public void testVerifyClientForExceptionheaders() {
		ReflectionTestUtils.setField(service, "maxRequestsAllowed", 4);
		service.verifyClient("001000589JE", null);
	}
	@Test(expected=PSD2Exception.class)
	public void testVerifyClientForExceptionheaders2() {
		ReflectionTestUtils.setField(service, "maxRequestsAllowed", 4);
		service.verifyClient(null, "987779");
	}
	
	@Test(expected=PSD2Exception.class)
	public void testVerifyClientForException() {
		ReflectionTestUtils.setField(service, "maxRequestsAllowed", 1);
		List<CPNPEntity> cpnpEntityList=new ArrayList<>();
		CPNPEntity cpnpEntity1=new CPNPEntity();
		cpnpEntity1.setPsuId("88888888");
		cpnpEntity1.setTppId("0000sd");
		
		CPNPEntity cpnpEntity2=new CPNPEntity();
		cpnpEntity2.setPsuId("88888888");
		cpnpEntity2.setTppId("0000sd");
		
		cpnpEntityList.add(cpnpEntity1);
		cpnpEntityList.add(cpnpEntity2);
		
		Mockito.when(compatibleVersionList.fetchVersionList()).thenReturn(fetchVersionList());
		
		when(repository.findByPsuIdAndTppIdAndCmaVersionIn(anyString(),anyString(),any())).thenReturn(cpnpEntityList);
		when(repository.save(any(CPNPEntity.class))).thenReturn(null);
		service.verifyClient("001000589JE", "88888888");
	}
	
	private List<String> fetchVersionList(){
		List<String> versionList = new ArrayList<>();
		versionList.add(null);
		versionList.add("1.0");
		versionList.add("3.0");
		return versionList;
	}
}
