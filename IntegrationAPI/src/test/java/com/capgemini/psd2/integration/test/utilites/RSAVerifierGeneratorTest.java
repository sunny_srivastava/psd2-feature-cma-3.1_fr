package com.capgemini.psd2.integration.test.utilites;

import java.io.IOException;
import java.io.InputStream;
import java.security.Key;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.HashSet;
import java.util.Set;

import org.junit.Test;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import com.capgemini.psd2.integration.utilities.RSAVerifierGenerator;
import com.nimbusds.jose.Header;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.JWSHeader;

public class RSAVerifierGeneratorTest {

	@Test
	public void testCreateJWSVerifier() {
		try {
			Resource resource = new ClassPathResource("keystore.jks");
			InputStream in = resource.getInputStream();
			KeyStore keystore= KeyStore.getInstance(KeyStore.getDefaultType());
			keystore.load(in, "password".toCharArray());
			Key key=keystore.getCertificate("6di2de88s8d2zvxgyxm0b2 (open banking test issuing ca)").getPublicKey();
			RSAVerifierGenerator generator=new RSAVerifierGenerator();
			Set<String> paramSet=new HashSet<>();
			paramSet.add("b64");
			Header header=new JWSHeader.Builder(JWSAlgorithm.RS256).criticalParams(paramSet).build();
			generator.createJWSVerifier(key, header);
		} catch (IOException | KeyStoreException | NoSuchAlgorithmException | CertificateException e) {
			e.printStackTrace();
		}
		
	}
}
