package com.capgemini.psd2.integration.controller.test;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doNothing;

import java.io.File;

import javax.ws.rs.HttpMethod;

import org.apache.commons.io.FileUtils;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.integration.controller.JWSVerificationController;
import com.capgemini.psd2.integration.service.JWSVerificationService;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;

public class JWSVerificationControllerTest {

	@Mock
	private JWSVerificationService service;
	
	@InjectMocks
	private JWSVerificationController controller;
	
	@Mock
	private RequestHeaderAttributes req;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void testVerifyJWS(){
		
		doNothing().when(service).validateSignature(anyString(), anyString());
		controller.verifyJWS("body", "1OEwYAKIgMtefvOKfSEdAS","POST");
	}
	
	@Test(expected=PSD2Exception.class)
	public void testVerifyJWS_clientIdNull(){
		
		doNothing().when(service).validateSignature(anyString(), anyString());
		controller.verifyJWS("body",null,"POST");
	}
	
	@Test
	public void testVerifyJWS_emptyHeader(){
		req = new RequestHeaderAttributes();
		req.setJwsHeader(PSD2Constants.NOT_DEFINED);
		ReflectionTestUtils.setField(controller, "reqHeaderAttributes", req);
		doNothing().when(service).validateSignature(anyString(), anyString());
		controller.verifyJWS("body","1OEwYAKIgMtefvOKfSEdAS","GET");
	}
	
	@Test
	public void testVerifyJWS_emptyHeader2(){
		req = new RequestHeaderAttributes();
		req.setJwsHeader(PSD2Constants.NOT_DEFINED);
		ReflectionTestUtils.setField(controller, "reqHeaderAttributes", req);
		doNothing().when(service).validateSignature(anyString(), anyString());
		controller.verifyJWS("body","1OEwYAKIgMtefvOKfSEdAS","POST");
	}
	
	@Test
	public void testVerifyJWS_emptyHeader4(){
		req = new RequestHeaderAttributes();
		req.setJwsHeader("jsdfi");
		ReflectionTestUtils.setField(controller, "reqHeaderAttributes", req);
		doNothing().when(service).validateSignature(anyString(), anyString());
		controller.verifyJWS("body","1OEwYAKIgMtefvOKfSEdAS","POST");
	}
	
	@Test(expected=PSD2Exception.class)
	public void testVerifyJWS_emptyHeader5(){
		req = new RequestHeaderAttributes();
		req.setJwsHeader("jsdfi");
		ReflectionTestUtils.setField(controller, "reqHeaderAttributes", req);
		doNothing().when(service).validateSignature(anyString(), anyString());
		controller.verifyJWS("body","1OEwYAKIgMtefvOKfSEdAS",HttpMethod.DELETE.toString());
	}
	
	@Test(expected=PSD2Exception.class)
	public void testVerifyJWS_emptyHeader6(){
		req = new RequestHeaderAttributes();
		req.setJwsHeader("jsdfi");
		ReflectionTestUtils.setField(controller, "reqHeaderAttributes", req);
		doNothing().when(service).validateSignature(anyString(), anyString());
		controller.verifyJWS("body","1OEwYAKIgMtefvOKfSEdAS",HttpMethod.GET.toString());
	}
	@Test(expected=PSD2Exception.class)
	public void testVerifyJWS_emptyHeader7(){
		req = new RequestHeaderAttributes();
		req.setJwsHeader("");
		ReflectionTestUtils.setField(controller, "reqHeaderAttributes", req);
		doNothing().when(service).validateSignature(anyString(), anyString());
		controller.verifyJWS("body","1OEwYAKIgMtefvOKfSEdAS",HttpMethod.DELETE.toString());
	}
	
	
	@Test(expected = PSD2Exception.class)
	public void testVerifyJWS_emptyBodyAndFile(){
		
		doNothing().when(service).validateSignature(anyString(), anyString());
		controller.verifyJWS(null, "1OEwYAKIgMtefvOKfSEdAS", "POST");
	}

	public MockMultipartFile getFile() {
		try{
			Resource resource = new ClassPathResource("/transaction.xml");
			File file = resource.getFile();
			byte[] byteArray = FileUtils.readFileToByteArray(file);
			MockMultipartFile jsonFile = new MockMultipartFile("FileParam", byteArray);
			return jsonFile;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
		
	}
}
