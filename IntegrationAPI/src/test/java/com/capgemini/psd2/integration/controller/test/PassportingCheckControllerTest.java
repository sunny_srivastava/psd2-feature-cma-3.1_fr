package com.capgemini.psd2.integration.controller.test;

import static org.mockito.Matchers.anyString;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.integration.controller.PassportingCheckController;
import com.capgemini.psd2.service.ITppDetailsService;

@RunWith(SpringJUnit4ClassRunner.class)
public class PassportingCheckControllerTest {

	@Mock
	ITppDetailsService iTppDetailsService;

	@InjectMocks
	PassportingCheckController 	controller;
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void controllerTest() {
		
		Mockito.when(iTppDetailsService.passporting(anyString(), anyString(), anyString(), anyString()))
				.thenReturn(true);
		
		controller.passportingCheck("TESTID", "AISP,PISP", "tenant1", "orgid");
	}
	
	@Test(expected=PSD2Exception.class)
	public void controllerTestFail() {
		
		Mockito.when(iTppDetailsService.passporting(anyString(), anyString(), anyString(), anyString()))
				.thenReturn(false);
		
		controller.passportingCheck("TESTID", "AISP,PISP", "tenant1", "orgid");
	}
}
