package com.capgemini.psd2.integration.controller.test;

import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.capgemini.psd2.aisp.adapter.AispConsentAdapter;
import com.capgemini.psd2.cisp.adapter.CispConsentAdapter;
import com.capgemini.psd2.consent.domain.AispConsent;
import com.capgemini.psd2.consent.domain.CispConsent;
import com.capgemini.psd2.consent.domain.PispConsent;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.integration.controller.ConsentValidationController;
import com.capgemini.psd2.pisp.adapter.PispConsentAdapter;

public class ConsentValidationControllerTest {
	@Mock
	private AispConsentAdapter aispConsentAdapter;
	
	@Mock
	private PispConsentAdapter pispConsentAdapter;
	
	@Mock
	private CispConsentAdapter cispConsentAdapter;
	
	@InjectMocks
	private ConsentValidationController controller;
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test(expected = PSD2Exception.class)
	public void transformAISPTokenAispConsentNullTest() {
		AispConsent aispConsent=null;
		Mockito.when(aispConsentAdapter.retrieveConsentByAccountRequestIdAndStatus(anyObject(),anyObject())).thenReturn(aispConsent);
		controller.validateConsent("123","openid accounts",null);	
	}
	@Test(expected = PSD2Exception.class)
	public void transformScopeConsentNullTest() {
		AispConsent aispConsent=null;
		Mockito.when(aispConsentAdapter.retrieveConsentByAccountRequestIdAndStatus(anyObject(),anyObject())).thenReturn(aispConsent);
		controller.validateConsent("123","random","9342");	
	}
	@Test(expected = PSD2Exception.class)
	public void transformAISPTokenAispConsentEmptyTest() {
		AispConsent aispConsent=null;
		Mockito.when(aispConsentAdapter.retrieveConsentByAccountRequestIdAndStatus(anyObject(),anyObject())).thenReturn(aispConsent);
		controller.validateConsent("123","openid accounts","");	
	}

	@Test(expected = PSD2Exception.class)
	public void transformAISPTokenAispConsentEmptyTest1() {
		AispConsent aispConsent=null;
		Mockito.when(aispConsentAdapter.retrieveConsentByAccountRequestIdAndStatus(anyObject(),anyObject())).thenReturn(aispConsent);
		controller.validateConsent("123","openid accounts"," ");	
	}
	@Test
	public void transformAISPTokenAispConsentNotNullTest() {
		AispConsent aispConsent=new AispConsent();
		aispConsent.setEndDate("2030-01-19T00:00:00+05:30");
		aispConsent.setTppCId("1234");
		Mockito.when(aispConsentAdapter.retrieveConsentByAccountRequestIdAndStatus(anyObject(),anyObject())).thenReturn(aispConsent);
		controller.validateConsent("123","openid accounts","1234");	
	}
	
	@Test
	public void transformAISPTokenAispConsentNullTest2() {
		AispConsent aispConsent=new AispConsent();
		aispConsent.setTppCId("1234");
		Mockito.when(aispConsentAdapter.retrieveConsentByAccountRequestIdAndStatus(anyObject(),anyObject())).thenReturn(aispConsent);
		controller.validateConsent("123","openid accounts","1234");	
	}

	@Test
	public void transformAISPTokenAispConsentNotNullTest2() {
		AispConsent aispConsent=new AispConsent();
		aispConsent.setEndDate("2030-01-19T00:00:00+05:30");
		aispConsent.setTppCId("1234");
		Mockito.when(aispConsentAdapter.retrieveConsentByAccountRequestIdAndStatus(anyObject(),anyObject())).thenReturn(aispConsent);
		controller.validateConsent("123","accounts","1234");	
	}
	
	@Test(expected = PSD2Exception.class)
	public void transformAISPTokenAispConsentNullTest3() {
		AispConsent aispConsent=new AispConsent();
		aispConsent.setEndDate("2019-01-19T00:00:00+05:30");
		aispConsent.setTppCId("1234");
		Mockito.when(aispConsentAdapter.retrieveConsentByAccountRequestIdAndStatus(anyObject(),anyObject())).thenReturn(aispConsent);
		controller.validateConsent("123","openid accounts","1234");	
	}
	@Test(expected = PSD2Exception.class)
	public void transformAISPTokenPispConsentNullTest() {
		PispConsent pispConsent=null;
		Mockito.when(pispConsentAdapter.retrieveConsentByPaymentId(anyObject(),anyObject())).thenReturn(pispConsent);
		controller.validateConsent("123","openid payments",null);	
	}
	
	@Test
	public void transformAISPTokenPispConsentNotNullTest() {
		PispConsent pispConsent=new PispConsent();
		pispConsent.setEndDate("2030-11-19T00:00:00+05:30");
		pispConsent.setTppCId("1234");
		Mockito.when(pispConsentAdapter.retrieveConsentByPaymentId(anyObject(),anyObject())).thenReturn(pispConsent);
		controller.validateConsent("123","openid payments","1234");	
	}
	@Test
	public void transformAISPTokenPispConsentNotNullTest2() {
		PispConsent pispConsent=new PispConsent();
		pispConsent.setEndDate("2030-11-19T00:00:00+05:30");
		pispConsent.setTppCId("1234");
		Mockito.when(pispConsentAdapter.retrieveConsentByPaymentId(anyObject(),anyObject())).thenReturn(pispConsent);
		controller.validateConsent("123","payments","1234");	
	}
	
	@Test(expected = PSD2Exception.class)
	public void consentExpiryTest() {
		AispConsent aispConsent=new AispConsent();
		PispConsent pispConsent=new PispConsent();
		Mockito.when(aispConsentAdapter.retrieveConsentByAccountRequestIdAndStatus(anyObject(),anyObject())).thenReturn(aispConsent);
		Mockito.when(pispConsentAdapter.retrieveConsentByPaymentId(anyObject(),anyObject())).thenReturn(pispConsent);
		pispConsent.setEndDate("2017-01-19T00:00:00+05:30");
		controller.validateConsent("123","openid payments",null);	
	}

	@Test(expected=PSD2Exception.class)
	public void validateConsentTestFundsConsentNull(){
		controller.validateConsent("123456", "openid fundsconfirmations", "123456");
	}
	
	@Test(expected=PSD2Exception.class)
	public void validateConsentTestFundsInvalidTppUser(){
		CispConsent cispConsent = new CispConsent();
		cispConsent.setTppCId("abcd");
		when(cispConsentAdapter.retrieveConsentByFundsIntentIdAndStatus(anyString(),anyObject())).thenReturn(cispConsent);
		controller.validateConsent("123456", "openid fundsconfirmations", "123456");
	}
	
	@Test
	public void validateConsentTestFundsSuccess(){
		CispConsent cispConsent = new CispConsent();
		cispConsent.setTppCId("123456");
		cispConsent.setEndDate("2030-11-19T00:00:00+05:30");
		when(cispConsentAdapter.retrieveConsentByFundsIntentIdAndStatus(anyString(),anyObject())).thenReturn(cispConsent);
		controller.validateConsent("123456", "openid fundsconfirmations", "123456");
	}
	@Test
	public void validateConsentTestFundsSuccess2(){
		CispConsent cispConsent = new CispConsent();
		cispConsent.setTppCId("123456");
		cispConsent.setEndDate("2030-11-19T00:00:00+05:30");
		when(cispConsentAdapter.retrieveConsentByFundsIntentIdAndStatus(anyString(),anyObject())).thenReturn(cispConsent);
		controller.validateConsent("123456", "fundsconfirmations", "123456");
	}
}
