package com.capgemini.psd2.integration.service.impl.test;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.jose4j.jwt.NumericDate;
import org.springframework.http.MediaType;

import com.capgemini.psd2.aisp.domain.OBAccount6;
import com.capgemini.psd2.aisp.domain.OBAccount6Account;
import com.capgemini.psd2.aisp.domain.OBBranchAndFinancialInstitutionIdentification50;
import com.capgemini.psd2.aisp.domain.OBReadAccount6;
import com.capgemini.psd2.aisp.domain.OBReadAccount6Data;
import com.capgemini.psd2.aisp.domain.OBReadConsentResponse1;
import com.capgemini.psd2.aisp.domain.OBReadConsentResponse1Data;
import com.capgemini.psd2.consent.domain.CustomerAccountInfo;
import com.capgemini.psd2.consent.domain.PSD2Account;
import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.PSD2Constants;
import com.nimbusds.jose.Header;
import com.nimbusds.jose.JOSEObjectType;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.JWSHeader;

public class IntegrationOperationsAPIMockdata {

	public static OBReadConsentResponse1 mockAccountRequestPOSTResponse;
	public static List<CustomerAccountInfo> mockCustomerAccountInfoList;
	
	public static Header getJWsHeader() {
		
			Set<String> critHeaders = new HashSet<>();

			JWSHeader header = new JWSHeader.Builder(JWSAlgorithm.RS256).criticalParams(critHeaders)
					.customParam(PSD2Constants.JOSE_HEADER_B64, Boolean.FALSE)
					.customParam(PSD2Constants.JOSE_HEADER_ISSUER, "subject")
					.customParam(PSD2Constants.JOSE_HEADER_IAT, NumericDate.now().getValue())
					.type(JOSEObjectType.JOSE)
					.keyID("rtr")
					.contentType(MediaType.APPLICATION_JSON_VALUE).build();
			return header;
		
	}
	
	public static String getRequestBody() {
		String body="{\r\n" + 
				"  \"Data\": {\r\n" + 
				"    \"Initiation\": {\r\n" + 
				"      \"InstructionIdentification\": \"ABDDCF\",\r\n" + 
				"      \"EndToEndIdentification\": \"DEMO USER\",\r\n" + 
				"      \"InstructedAmount\": {\r\n" + 
				"        \"Amount\": \"1.200\",\r\n" + 
				"        \"Currency\": \"EUR\"\r\n" + 
				"      },\r\n" + 
				"      \"CreditorAgent\": {\r\n" + 
				"        \"SchemeName\": \"BICFI\",\r\n" + 
				"        \"Identification\": \"12345\"\r\n" + 
				"      },\r\n" + 
				"      \"CreditorAccount\": {\r\n" + 
				"        \"SchemeName\": \"IBAN\",\r\n" + 
				"        \"Identification\": \"FR1420041010050500013M02606\",\r\n" + 
				"        \"Name\": \"Test user\",\r\n" + 
				"        \"SecondaryIdentification\": \"0002\"\r\n" + 
				"      },\r\n" + 
				"      \"RemittanceInformation\": {\r\n" + 
				"        \"Unstructured\": \"Internal ops code 5120101\",\r\n" + 
				"        \"Reference\": \"FRESCO-101\"\r\n" + 
				"      }\r\n" + 
				"    }\r\n" + 
				"  },\r\n" + 
				"  \"Risk\": {\r\n" + 
				"    \"PaymentContextCode\": \"EcommerceGoods\",\r\n" + 
				"    \"MerchantCategoryCode\": \"5967\",\r\n" + 
				"    \"MerchantCustomerIdentification\": \"053598653254\",\r\n" + 
				"    \"DeliveryAddress\": {\r\n" + 
				"      \"AddressLine\": [\r\n" + 
				"        \"Flat 7\",\r\n" + 
				"        \"Acacia Lodge\"\r\n" + 
				"      ],\r\n" + 
				"      \"StreetName\": \"AcaciaAvenue\",\r\n" + 
				"      \"BuildingNumber\": \"27\",\r\n" + 
				"      \"PostCode\": \"7U31 2ZZ\",\r\n" + 
				"      \"TownName\": \"Sparsholt\",\r\n" + 
				"      \"CountrySubDivision\": [\r\n" + 
				"        \"ABCDABCDABCDABCDAB\",\r\n" + 
				"        \"DEFG\"\r\n" + 
				"      ],\r\n" + 
				"      \"Country\": \"GB\"\r\n" + 
				"    }\r\n" + 
				"  }\r\n" + 
				"}";
		return body;
	}
	
	public static List<String> getCritData(){
		List<String> critData=new ArrayList<>();
		critData.add(PSD2Constants.JOSE_HEADER_B64);
		critData.add(PSD2Constants.JOSE_HEADER_ISSUER);
		critData.add(PSD2Constants.JOSE_HEADER_IAT);
		critData.add(PSD2Constants.JOSE_HEADER_TRUST_ANCHOR);
		return critData;
	}
	
	public static List<String> getSupportedAlgo(){
		List<String> critData=new ArrayList<>();
		critData.add("RS256");
		critData.add("PS256");
		return critData;
	}
	
	public static X509Certificate getCertifcate() {
		CertificateFactory cf = null;
		X509Certificate cert = null;
		String certString = "MIIJqjCCCJKgAwIBAgINQzUUSxyi8Ih6ReqfCjANBgkqhkiG9w0BAQsFADBqMQswCQYDVQQGEwJIVTERMA8GA1UEBwwIQnVkYXBlc3QxFjAUBgNVBAoMDU1pY3Jvc2VjIEx0ZC4xFDASBgNVBAsMC2UtU3ppZ25vIENBMRowGAYDVQQDDBFlLVN6aWdubyBUZXN0IENBMzAeFw0xOTAyMDExMDI5MzlaFw0xOTA1MDIxMDI5MzlaMIHoMQswCQYDVQQGEwJJRTEPMA0GA1UEBwwGRHVibGluMSAwHgYDVQQKDBdDYXBnZW1pbmkgUFNEMiBUZXN0IE9yZzEdMBsGA1UEYQwUUFNESUUtQ0JJLTEwMzQ1MDc4OTAxGjAYBgNVBAsMEUNhcGdlbWluaSBQU0QyIE9VMRcwFQYDVQQDDA5DYXBnZW1pbmkgUFNEMjEpMCcGCSqGSIb3DQEJARYaYWxvay5hLnNpbmdoQGNhcGdlbWluaS5jb20xJzAlBgNVBAUTHjEuMy42LjEuNC4xLjIxNTI4LjIuMi45OS4xMjc5NzCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAMUGUF+ZXzt5C+k9PUW0FNy3s5749souWcEfijY3663BY1TQ+NLL3hegqFLTYiQMN6jvKPItOtkqIbSAe4nULeJm/TZql+/TRik5AgUq4mbhhGn8PfYJEGv2fi7FYoIworbcjXGQVggopE9vePwd1H+ubmrJKm7STz3C+IR86W1WPuUHPCA6103BJjCTjkFe9Fje8Y1vrkTPVOO9W76v4r3FwL0wdhaS8vCwuujmc1vBDWCoOv4fpociO8TCgjDka1tlbDn91/y0jjLXkz+0rNJvhaf+REZuK/xNobhEXIYlkyngLHl3JKACOUuXY3yS5kiw4u4KyrSg6OkkigW1oDMCAwEAAaOCBc4wggXKMA4GA1UdDwEB/wQEAwIGQDCCA38GA1UdIASCA3YwggNyMIIDbgYMKwYBBAGBqBgCAQFkMIIDXDAmBggrBgEFBQcCARYaaHR0cDovL2NwLmUtc3ppZ25vLmh1L3FjcHMwRgYIKwYBBQUHAgIwOgw4VGVzdCBxdWFsaWZpZWQgY2VydGlmaWNhdGUgZm9yIGVsZWN0cm9uaWMgc2VhbCAoQnJvbnplKS4wgaUGCCsGAQUFBwICMIGYDIGVVGhlIHByb3ZpZGVyIHByZXNlcnZlcyByZWdpc3RyYXRpb24gZGF0YSBmb3IgMTAgeWVhcnMgYWZ0ZXIgdGhlIGV4cGlyYXRpb24gb2YgdGhlIGNlcnRpZmljYXRlLiBUaGUgc3ViamVjdCBvZiB0aGUgdGVzdCBjZXJ0aWZpY2F0ZSBpcyBhIGxlZ2FsIHBlcnNvbi4wgZUGCCsGAQUFBwICMIGIDIGFVEVTVCBjZXJ0aWZpY2F0ZSBpc3N1ZWQgb25seSBmb3IgdGVzdGluZyBwdXJwb3Nlcy4gVGhlIGlzc3VlciBpcyBub3QgbGlhYmxlIGZvciBhbnkgZGFtYWdlcyBhcmlzaW5nIGZyb20gdGhlIHVzZSBvZiB0aGlzIGNlcnRpZmljYXRlITBRBggrBgEFBQcCAjBFDENUZXN6dCBlbGVrdHJvbmlrdXMgYsOpbHllZ3rFkSBtaW7FkXPDrXRldHQgdGFuw7pzw610dsOhbnlhIChCcm9ueikuMIGmBggrBgEFBQcCAjCBmQyBlkEgcmVnaXN6dHLDoWNpw7NzIGFkYXRva2F0IGEgc3pvbGfDoWx0YXTDsyBhIHRhbsO6c8OtdHbDoW55IGxlasOhcnTDoXTDs2wgc3rDoW3DrXRvdHQgMTAgw6l2aWcgxZFyemkgbWVnLiBBIHRlc3p0IHRhbsO6c8OtdHbDoW55IGFsYW55YSBqb2dpIHN6ZW3DqWx5LjCBrQYIKwYBBQUHAgIwgaAMgZ1UZXN6dGVsw6lzaSBjw6lscmEga2lhZG90dCBURVNaVCB0YW7DunPDrXR2w6FueS4gQSBoYXN6bsOhbGF0w6F2YWwga2FwY3NvbGF0b3NhbiBmZWxtZXLDvGzFkSBrw6Fyb2vDqXJ0IGEgU3pvbGfDoWx0YXTDsyBzZW1taWx5ZW4gZmVsZWzFkXNzw6lnZXQgbmVtIHbDoWxsYWwhMB0GA1UdDgQWBBSQROsffzI7QlLqG8F83fKDBcbRtDAfBgNVHSMEGDAWgBTc5gIo7zcwj4k+oK0gVfPvNujwzTBABgNVHREEOTA3gRphbG9rLmEuc2luZ2hAY2FwZ2VtaW5pLmNvbaAZBggrBgEFBQcIA6ANMAsGCSsGAQQBgagYAjAyBgNVHR8EKzApMCegJaAjhiFodHRwOi8vdGVzenQuZS1zemlnbm8uaHUvVENBMy5jcmwwbwYIKwYBBQUHAQEEYzBhMDAGCCsGAQUFBzABhiRodHRwOi8vdGVzenQuZS1zemlnbm8uaHUvdGVzdGNhM29jc3AwLQYIKwYBBQUHMAKGIWh0dHA6Ly90ZXN6dC5lLXN6aWduby5odS9UQ0EzLmNydDCCAQwGCCsGAQUFBwEDBIH/MIH8MAgGBgQAjkYBATALBgYEAI5GAQMCAQowUwYGBACORgEFMEkwJBYeaHR0cHM6Ly9jcC5lLXN6aWduby5odS9xY3BzX2VuEwJFTjAhFhtodHRwczovL2NwLmUtc3ppZ25vLmh1L3FjcHMTAkhVMBMGBgQAjkYBBjAJBgcEAI5GAQYCMHkGBgQAgZgnAjBvMEwwEQYHBACBmCcBAQwGUFNQX0FTMBEGBwQAgZgnAQIMBlBTUF9QSTARBgcEAIGYJwEDDAZQU1BfQUkwEQYHBACBmCcBBAwGUFNQX0lDDBdDZW50cmFsIEJhbmsgb2YgSXJlbGFuZAwGSUUtQ0JJMA0GCSqGSIb3DQEBCwUAA4IBAQAV+hLK3PUe/ISBzXErmRcXJmVyKaIqMyD9ZrWzAC98eDq5fQ0m1uNHqgf3gKsKV/cSU30LFNGGx+P3DqBhjM+eKDalHp4yl6NEdA1TIBk3VnJVDMpSboIRzR6ZWUUolXa6UacgrOr8mzdfUcT1/Bo8at+7cTpK9qR8T7GU/EAMwScVrhgut9DmbV6u6C6k9kMSljUfrJ460bQjlFlW+Msml0fzJt43e1OyCxd2Lv1oiL8aMtZBQHJpI8P/orkN2HgFFqD+5iY/v83vwQuQRC97U0Xqozk7Et/L3BWyPbGLqTqdJxFxNDcow7Uq1Yilm1T/gJSTLU7sPGgVBhubbjZU";
		try {
			byte[] arr = Base64.getDecoder().decode(certString);
			InputStream stream = new ByteArrayInputStream(arr);
			cf = CertificateFactory.getInstance(PSD2Constants.X509);
			cert = (X509Certificate) cf.generateCertificate(stream);
		} catch (CertificateException | IllegalArgumentException ie) {
			throw PSD2Exception.populatePSD2Exception(
					ErrorCodeEnum.CERTIFICATE_GENERATION_FAILED.getDetailErrorMessage(),
					ErrorCodeEnum.VALIDATION_ERROR);
		}
		return cert;
	}

	public static X509Certificate getNonOID_Certifcate() {
		CertificateFactory cf = null;
		X509Certificate cert = null;
		String certString="MIIFoTCCBImgAwIBAgIEWWwmMTANBgkqhkiG9w0BAQsFADBmMQswCQYDVQQGEwJHQjEdMBsGA1UEChMUT3BlbiBCYW5raW5nIExpbWl0ZWQxETAPBgNVBAsTCFRlc3QgUEtJMSUwIwYDVQQDExxPcGVuIEJhbmtpbmcgVGVzdCBJc3N1aW5nIENBMB4XDTE4MDQyNjEzMTE0MVoXDTE5MDUyNjEzNDE0MVowajELMAkGA1UEBhMCR0IxHTAbBgNVBAoTFE9wZW4gQmFua2luZyBMaW1pdGVkMRswGQYDVQQLExJuQlJjWUFjQUNuZ2hiR0ZPQmsxHzAdBgNVBAMTFjRQNU5uRXBNM1dFeTFyV3dta1RydWkwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDRrCtcYYiMRVf6wjdyYtZSywI9Bq0vY4XKuyRxQUa0nV85jEH+yc9He0cpvTOk+6XqDSrE0PU0m6rQAi5oHUSMkdLfId7FRMP+vj4JkwgLcY/70AIqq1WRyzmw7+z+7ihwOgq4u0W4Cy1/D5EQH1A8vH25A7wfZEAR83tIX1UYEpYyFWvfK4dvtPf2OL3nJUf+aa8f451nHqb577i/f7m2cTxe5sjEcu5SlRcm+aN823KMl1xQ8+Err3LTP1doFxoNXXQ2tNaWRLtvMo64oFTE+xcNaqwoVbB5XTwRYPyc439GWsp8FL0BQwWRgB1onAkF+d+Kwyrle5UZdtRYBv0fAgMBAAGjggJRMIICTTAOBgNVHQ8BAf8EBAMCBsAwFQYDVR0lBA4wDAYKKwYBBAGCNwoDDDCB4AYDVR0gBIHYMIHVMIHSBgsrBgEEAah1gQYBZDCBwjAqBggrBgEFBQcCARYeaHR0cDovL29iLnRydXN0aXMuY29tL3BvbGljaWVzMIGTBggrBgEFBQcCAjCBhgyBg1VzZSBvZiB0aGlzIENlcnRpZmljYXRlIGNvbnN0aXR1dGVzIGFjY2VwdGFuY2Ugb2YgdGhlIE9wZW5CYW5raW5nIFJvb3QgQ0EgQ2VydGlmaWNhdGlvbiBQb2xpY2llcyBhbmQgQ2VydGlmaWNhdGUgUHJhY3RpY2UgU3RhdGVtZW50MDoGCCsGAQUFBwEBBC4wLDAqBggrBgEFBQcwAYYeaHR0cDovL29idGVzdC50cnVzdGlzLmNvbS9vY3NwMIHEBgNVHR8EgbwwgbkwN6A1oDOGMWh0dHA6Ly9vYnRlc3QudHJ1c3Rpcy5jb20vcGtpL29idGVzdGlzc3VpbmdjYS5jcmwwfqB8oHqkeDB2MQswCQYDVQQGEwJHQjEdMBsGA1UEChMUT3BlbiBCYW5raW5nIExpbWl0ZWQxETAPBgNVBAsTCFRlc3QgUEtJMSUwIwYDVQQDExxPcGVuIEJhbmtpbmcgVGVzdCBJc3N1aW5nIENBMQ4wDAYDVQQDEwVDUkwxMjAfBgNVHSMEGDAWgBQPAcAv6Ghs9yO/jmVuiQ3GpoV3tzAdBgNVHQ4EFgQUuuzJQD+hKv5IGMe4fITgJasxaaIwDQYJKoZIhvcNAQELBQADggEBACcrCNyC9JxQ3e6dhhcrqssLOfe4QN7F4zZr5ywTv54uiPgCOaUylkHxnAT47eRvmt6ss0BJQO8Ypp45akiL/iqMPDq5q9WHMtJxCbichtVhmVQabaXwQSU4oEeR4cR3//LxSElGpa7zFGhhlU4CsX6PPaJk6UIgfI+qEg9YCyZPGuMce0y1IMZEmIVkgv5jGKXAuqPyFEEwwjH82DRJHPF1bWN2d2W8xJBNEQUPW9+pzc0PquU61JC2jVlcyvbXgQa1Eo6/dVVxLp1wnNIeDZeMy/jxoBGSlxm5W1gfEJEH406eOW8DsX3NaCbhr8+qo3IhAGC8kUaLv856jHFrjGg=";
		try {
			byte[] arr = Base64.getDecoder().decode(certString);
			InputStream stream = new ByteArrayInputStream(arr);
			cf = CertificateFactory.getInstance(PSD2Constants.X509);
			cert = (X509Certificate) cf.generateCertificate(stream);
		} catch (CertificateException  | IllegalArgumentException ie) {
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.CERTIFICATE_GENERATION_FAILED.getDetailErrorMessage(),ErrorCodeEnum.VALIDATION_ERROR);
		} 
		return cert;
	}

	public static OBReadConsentResponse1 getAccountRequestPOSTResponse() {
		mockAccountRequestPOSTResponse = new OBReadConsentResponse1();

		OBReadConsentResponse1Data data = new OBReadConsentResponse1Data();
		List<OBReadConsentResponse1Data.PermissionsEnum> permissions = new ArrayList<>();
		permissions.add(OBReadConsentResponse1Data.PermissionsEnum.READACCOUNTSBASIC);
		permissions.add(OBReadConsentResponse1Data.PermissionsEnum.READACCOUNTSDETAIL);
		permissions.add(OBReadConsentResponse1Data.PermissionsEnum.READBALANCES);
		data.setPermissions(permissions);
		data.setCreationDateTime("2018-12-25T00:00:00-00:00");
		data.setExpirationDateTime("2018-05-02T00:00:00-00:00");
		data.setTransactionFromDateTime("2018-05-03T00:00:00-00:00");
		data.setTransactionToDateTime("2018-12-03T00:00:00-00:00");
		data.setConsentId("af5b90c1-64b5-4a52-ba55-5eed68b2a269");
		data.setStatus(OBReadConsentResponse1Data.StatusEnum.AWAITINGAUTHORISATION);
		mockAccountRequestPOSTResponse.setData(data);
		mockAccountRequestPOSTResponse.setRisk(null);
		return mockAccountRequestPOSTResponse;
	}

	public static OBReadConsentResponse1 getAccountRequestPOSTResponseInvalidStatus() {
		OBReadConsentResponse1 mockAccountRequestPOSTResponse = new OBReadConsentResponse1();

		OBReadConsentResponse1Data data = new OBReadConsentResponse1Data();
		List<OBReadConsentResponse1Data.PermissionsEnum> permissions = new ArrayList<>();
		permissions.add(OBReadConsentResponse1Data.PermissionsEnum.READACCOUNTSBASIC);
		permissions.add(OBReadConsentResponse1Data.PermissionsEnum.READACCOUNTSDETAIL);
		permissions.add(OBReadConsentResponse1Data.PermissionsEnum.READBALANCES);
		data.setPermissions(permissions);
		data.setCreationDateTime("2017-12-25T00:00:00-00:00");
		data.setExpirationDateTime("2017-05-02T00:00:00-00:00");
		data.setTransactionFromDateTime("2017-05-03T00:00:00-00:00");
		data.setTransactionToDateTime("2017-12-03T00:00:00-00:00");
		data.setConsentId("af5b90c1-64b5-4a52-ba55-5eed68b2a269");
		data.setStatus(OBReadConsentResponse1Data.StatusEnum.AUTHORISED);
		mockAccountRequestPOSTResponse.setData(data);
		mockAccountRequestPOSTResponse.setRisk(null);
		return mockAccountRequestPOSTResponse;
	}

	public static List<CustomerAccountInfo> getCustomerAccountInfoList() {
		mockCustomerAccountInfoList = new ArrayList<>();
		CustomerAccountInfo customerAccountInfo1 = new CustomerAccountInfo();
		customerAccountInfo1.setAccountName("John Doe");
		customerAccountInfo1.setUserId("1234");
		customerAccountInfo1.setAccountNumber("10203345");
		customerAccountInfo1.setAccountNSC("SC802001");
		customerAccountInfo1.setCurrency("EUR");
		customerAccountInfo1.setNickname("John");
		customerAccountInfo1.setAccountType("checking");

		CustomerAccountInfo customerAccountInfo2 = new CustomerAccountInfo();
		customerAccountInfo2.setAccountName("Tiffany Doe");
		customerAccountInfo2.setUserId("1234");
		customerAccountInfo2.setAccountNumber("10203346");
		customerAccountInfo2.setAccountNSC("SC802002");
		customerAccountInfo2.setCurrency("GRP");
		customerAccountInfo2.setNickname("Tiffany");
		customerAccountInfo2.setAccountType("savings");

		mockCustomerAccountInfoList.add(customerAccountInfo1);
		mockCustomerAccountInfoList.add(customerAccountInfo2);

		return mockCustomerAccountInfoList;
	}

	public static List<PSD2Account> getCustomerAccounts() {
		List<PSD2Account> mockCustomerAccountList = new ArrayList<>();
		PSD2Account customerAccountInfo1 = new PSD2Account();

		List<OBAccount6Account> accountList = new ArrayList<>();
		OBAccount6Account acct = new OBAccount6Account();
		acct.setIdentification("12345");
		acct.setSchemeName("UK.OBIE.IBAN");
		accountList.add(acct);
		OBBranchAndFinancialInstitutionIdentification50 servicer = new OBBranchAndFinancialInstitutionIdentification50();
		servicer.setIdentification("12345");
		customerAccountInfo1.setServicer(servicer);
		customerAccountInfo1.setAccount(accountList);
		customerAccountInfo1.setCurrency("EUR");
		customerAccountInfo1.setNickname("John");
		PSD2Account customerAccountInfo2 = new PSD2Account();

		customerAccountInfo2.setCurrency("EUR");
		customerAccountInfo2.setNickname("John");
		customerAccountInfo2.setAccount(accountList);
		customerAccountInfo2.setServicer(servicer);

		mockCustomerAccountList.add(customerAccountInfo1);
		mockCustomerAccountList.add(customerAccountInfo2);
		return mockCustomerAccountList;
	}

	public static OBReadAccount6 getCustomerAccountInfo() {
		OBReadAccount6 mockOBReadAccount2 = new OBReadAccount6();
		List<OBAccount6> obAccount2 = new ArrayList<>();
		PSD2Account acct = new PSD2Account();
		acct.setAccountId("14556236");
		acct.setCurrency("EUR");
		acct.setNickname("John");
		List<OBAccount6Account> accountList = new ArrayList<>();
		OBAccount6Account accnt = new OBAccount6Account();
		accnt.setIdentification("12345");
		accnt.setSchemeName("UK.OBIE.IBAN");
		accountList.add(accnt);
		OBBranchAndFinancialInstitutionIdentification50 servicer = new OBBranchAndFinancialInstitutionIdentification50();
		servicer.setIdentification("12345");
		acct.setAccount(accountList);
		acct.setServicer(servicer);

		PSD2Account accnt1 = new PSD2Account();
		accnt1.setAccountId("14556236");
		accnt1.setCurrency("EUR");
		accnt1.setNickname("John");
		accnt1.setServicer(servicer);
		accnt1.setAccount(accountList);
		obAccount2.add(accnt1);
		obAccount2.add(acct);
		OBReadAccount6Data data2 = new OBReadAccount6Data();
		data2.setAccount(obAccount2);
		mockOBReadAccount2.setData(data2);
		return mockOBReadAccount2;
	}

	public static OBReadAccount6 getCustomerAccountData() {
		OBReadAccount6 mockOBReadAccount2 = new OBReadAccount6();
		List<OBAccount6> obAccount2List = new ArrayList<>();
		PSD2Account acct = new PSD2Account();
		acct.setAccountId("14556236");
		acct.setCurrency("EUR");
		acct.setNickname("John");
		// acct.setAccountType("savings");
		List<OBAccount6Account> accountList = new ArrayList<>();
		OBAccount6Account acct1 = new OBAccount6Account();
		acct1.setIdentification("12345");
		acct1.setSchemeName("UK.OBIE.IBAN");
		accountList.add(acct1);
		OBBranchAndFinancialInstitutionIdentification50 servicer = new OBBranchAndFinancialInstitutionIdentification50();
		servicer.setIdentification("12345");
		acct.setAccount(accountList);
		acct.setServicer(servicer);

		PSD2Account accnt = new PSD2Account();
		accnt.setAccountId("14556236");
		accnt.setCurrency("EUR");
		accnt.setNickname("John");
		accnt.setServicer(servicer);
		accnt.setAccount(accountList);
		obAccount2List.add(acct);
		obAccount2List.add(accnt);
		OBReadAccount6Data data2 = new OBReadAccount6Data();
		data2.setAccount(obAccount2List);
		mockOBReadAccount2.setData(data2);

		return mockOBReadAccount2;
	}

	public static List<PSD2Account> getCustomerAccountsNew() {
		List<PSD2Account> mockCustomerAccountList = new ArrayList<>();
		PSD2Account customerAccountInfo1 = new PSD2Account();
		List<OBAccount6Account> accountList = new ArrayList<>();
		OBAccount6Account acct = new OBAccount6Account();
		acct.setIdentification("12345");
		acct.setSchemeName("UK.OBIE.IBAN");
		accountList.add(acct);
		OBBranchAndFinancialInstitutionIdentification50 servicer = new OBBranchAndFinancialInstitutionIdentification50();
		servicer.setIdentification("12345");
		customerAccountInfo1.setServicer(servicer);
		customerAccountInfo1.setAccount(accountList);
		customerAccountInfo1.setCurrency("EUR");
		customerAccountInfo1.setNickname("John");
		customerAccountInfo1.setHashedValue("123");
		PSD2Account customerAccountInfo2 = new PSD2Account();

		customerAccountInfo2.setCurrency("EUR");
		customerAccountInfo2.setNickname("John");
		customerAccountInfo2.setAccount(accountList);
		customerAccountInfo2.setHashedValue("123");
		customerAccountInfo2.setServicer(servicer);

		mockCustomerAccountList.add(customerAccountInfo1);
		mockCustomerAccountList.add(customerAccountInfo2);
		return mockCustomerAccountList;
	}

	public static OBReadAccount6 getCustomerAccountInfoNew() {
		OBReadAccount6 mockOBReadAccount2 = new OBReadAccount6();
		List<OBAccount6> accountData = new ArrayList<>();
		PSD2Account acct = new PSD2Account();
		acct.setAccountId("14556236");
		acct.setCurrency("EUR");
		acct.setNickname("John");
		List<OBAccount6Account> accountList = new ArrayList<>();
		OBAccount6Account acct1 = new OBAccount6Account();
		acct1.setIdentification("12345");
		acct1.setSchemeName("UK.OBIE.IBAN");
		accountList.add(acct1);
		OBBranchAndFinancialInstitutionIdentification50 servicer = new OBBranchAndFinancialInstitutionIdentification50();
		servicer.setIdentification("12345");
		acct.setAccount(accountList);
		acct.setHashedValue("123");
		acct.setServicer(servicer);

		PSD2Account accnt = new PSD2Account();
		accnt.setAccountId("14556236");
		accnt.setCurrency("EUR");
		accnt.setNickname("John");
		accnt.setServicer(servicer);
		accnt.setAccount(accountList);
		accnt.setHashedValue("123");
		// accnt.setAccountType("savings");
		accountData.add(acct);
		accountData.add(accnt);
		OBReadAccount6Data data2 = new OBReadAccount6Data();
		data2.setAccount(accountData);
		mockOBReadAccount2.setData(data2);

		return mockOBReadAccount2;
	}

}
