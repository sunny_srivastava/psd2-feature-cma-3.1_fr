package com.capgemini.psd2.integration.service.impl.test;

import static org.junit.Assert.*;

import java.lang.reflect.Constructor;
import java.lang.reflect.Modifier;

import org.junit.Test;
import org.mockito.InjectMocks;
import com.capgemini.psd2.integration.utilities.X509CertificateUtils;

public class X509CertificateUtilsTest {
	
	@InjectMocks
	X509CertificateUtils cert;
	
	@Test
    public void testPrivateConstructor() throws Exception {
		Constructor<X509CertificateUtils> constructor = X509CertificateUtils.class.getDeclaredConstructor();
		  assertTrue(Modifier.isPrivate(constructor.getModifiers()));
		  constructor.setAccessible(true);
		  constructor.newInstance();
    }
}
