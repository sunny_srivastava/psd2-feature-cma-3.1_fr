package com.capgemini.psd2.integration.test.utilites;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpHeaders;

import com.capgemini.psd2.exceptions.OBPSD2ExceptionUtility;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.integration.service.TPPInformationService;
import com.capgemini.psd2.integration.utilities.JWKSGenerator;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.rest.client.sync.RestClientSync;

public class JWKSGeneratorTest {

	@Mock
	private RestClientSync restClient;

	@Mock
	private RequestHeaderAttributes requestHeaderAttributes;
	
	@InjectMocks
	private JWKSGenerator generator;
	
	@Mock
	private TPPInformationService tppInformationService;


	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		
		Map<String, String> map=new HashMap<>();
		map.put("HEADER", "header error message");
		map.put("INTERNAL", "freq ma"); 

		Map<String, String> specificErrorMessageMap=new HashMap<>();
		specificErrorMessageMap.put("signature_missing", "signature header missing in request");
		specificErrorMessageMap.put("unexpected_error", "unexpected error occured");
		OBPSD2ExceptionUtility.genericErrorMessages.putAll(map);
		OBPSD2ExceptionUtility.specificErrorMessages.putAll(specificErrorMessageMap);
		Mockito.when(tppInformationService.fetchJWKSUrl(anyString())).thenReturn("https://keystore.openbankingtest.org.uk/0015800000jfQ9aAAE/5pRXn4FJ7bSbKwb7x5daKu.jwks\r\n" + 
				" \r\n" + 
				"");

	}
	
	@Test
	public void testGetSigningCertifcate() {
		String response="{\"keys\": [{\"e\": \"AQAB\", \"kid\": \"HvWTP7HnV-G86ji_KJXHQ3CZN2w\", \"kty\": \"RSA\", \"n\": \"xwujM8RY3RCzYNRiPwV6alzdHMCS9td57-i1IsVIUl7MpWi11vma9jKR21JLHQvS3fGPHPEbdPp-n0QLtHS5NBPsDrPfem0dkObZOOZOX-r5BH6NpIT1AEpreqrNPtxeiDm7V53DBgncj42Ue9uqK6dKhpjr1kcSpKunvIx-9YrKgGaZ6uTehcYHyRcG7rM_zXZmabgzGo_DDfuVJ6XivklLH3Ndn1O-5s03LFoncTPl8AYqkzP08LQgosOwYte99EhbBmvqx4-v7ZTv_OIlwsaelr2HjSNeeYAd1w47F5R0Vt-wlhC8J3PNLO-3yMCoKYzsgzBNRGd3xZnZkc6efQ\", \"x5t\": \"HvWTP7HnV-G86ji_KJXHQ3CZN2w\", \"x5t#256\": \"WAJ1j3mJ7NEjwNsXj6rnnL16pb9GyR4We8tmZAXZO44\", \"x5u\": \"https://keystore.mit.openbanking.qa/nBRcYAcACnghbGFOBk/HvWTP7HnV-G86ji_KJXHQ3CZN2w.pem\", \"x5c\": [\"MIIFrDCCBJSgAwIBAgIEWWwmMDANBgkqhkiG9w0BAQsFADBmMQswCQYDVQQGEwJHQjEdMBsGA1UEChMUT3BlbiBCYW5raW5nIExpbWl0ZWQxETAPBgNVBAsTCFRlc3QgUEtJMSUwIwYDVQQDExxPcGVuIEJhbmtpbmcgVGVzdCBJc3N1aW5nIENBMB4XDTE4MDQyNjEzMTEzMVoXDTE5MDQyNjEzNDEzMVowajELMAkGA1UEBhMCR0IxHTAbBgNVBAoTFE9wZW4gQmFua2luZyBMaW1pdGVkMRswGQYDVQQLExJuQlJjWUFjQUNuZ2hiR0ZPQmsxHzAdBgNVBAMTFjRQNU5uRXBNM1dFeTFyV3dta1RydWkwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDHC6MzxFjdELNg1GI/BXpqXN0cwJL213nv6LUixUhSXsylaLXW+Zr2MpHbUksdC9Ld8Y8c8Rt0+n6fRAu0dLk0E+wOs996bR2Q5tk45k5f6vkEfo2khPUASmt6qs0+3F6IObtXncMGCdyPjZR726orp0qGmOvWRxKkq6e8jH71isqAZpnq5N6FxgfJFwbusz/NdmZpuDMaj8MN+5UnpeK+SUsfc12fU77mzTcsWidxM+XwBiqTM/TwtCCiw7Bi1730SFsGa+rHj6/tlO/84iXCxp6WvYeNI155gB3XDjsXlHRW37CWELwnc80s77fIwKgpjOyDME1EZ3fFmdmRzp59AgMBAAGjggJcMIICWDAOBgNVHQ8BAf8EBAMCB4AwIAYDVR0lAQH/BBYwFAYIKwYBBQUHAwEGCCsGAQUFBwMCMIHgBgNVHSAEgdgwgdUwgdIGCysGAQQBqHWBBgFkMIHCMCoGCCsGAQUFBwIBFh5odHRwOi8vb2IudHJ1c3Rpcy5jb20vcG9saWNpZXMwgZMGCCsGAQUFBwICMIGGDIGDVXNlIG9mIHRoaXMgQ2VydGlmaWNhdGUgY29uc3RpdHV0ZXMgYWNjZXB0YW5jZSBvZiB0aGUgT3BlbkJhbmtpbmcgUm9vdCBDQSBDZXJ0aWZpY2F0aW9uIFBvbGljaWVzIGFuZCBDZXJ0aWZpY2F0ZSBQcmFjdGljZSBTdGF0ZW1lbnQwOgYIKwYBBQUHAQEELjAsMCoGCCsGAQUFBzABhh5odHRwOi8vb2J0ZXN0LnRydXN0aXMuY29tL29jc3AwgcQGA1UdHwSBvDCBuTA3oDWgM4YxaHR0cDovL29idGVzdC50cnVzdGlzLmNvbS9wa2kvb2J0ZXN0aXNzdWluZ2NhLmNybDB+oHygeqR4MHYxCzAJBgNVBAYTAkdCMR0wGwYDVQQKExRPcGVuIEJhbmtpbmcgTGltaXRlZDERMA8GA1UECxMIVGVzdCBQS0kxJTAjBgNVBAMTHE9wZW4gQmFua2luZyBUZXN0IElzc3VpbmcgQ0ExDjAMBgNVBAMTBUNSTDEyMB8GA1UdIwQYMBaAFA8BwC/oaGz3I7+OZW6JDcamhXe3MB0GA1UdDgQWBBSvtBesNM6NLeqR4TcHiFbyQOIS8zANBgkqhkiG9w0BAQsFAAOCAQEAoTDP8thZrT5awc7JVhbhD5p/L6J3DNuNasZAwBDXNYRI5eNtZw6aqAzAQw0twJ2+H5lGDLDGPhY6WgahEAu+0H399dLMbEpR/WTwdp0nRtK9a2/Ru8UFj4wTzh2THeIIXjJ/slJ1ZSCaQfy13bq0glt02l9c5TgreKk8UERetgGirzXkaH6ghVC4/ULAQmuyFh//TQdPsI5beAn+ZCjlw0VpUg3LrQ7d9QWsGEFnFolhN5lSB4AGgIopW1kkTnryLeP2dsXs4SyoFHLkS0VGSw3uc1Myt+WOau1eTd5LNOF5rVyZz1JEP3R+9jbNm655r1C+ByvSKa0/mFYMV8RW/Q==\"], \"use\": \"enc\"}, {\"e\": \"AQAB\", \"kid\": \"yTXZEcT2QV3jHeJtKaOfY0v3ytQ\", \"kty\": \"RSA\", \"n\": \"0awrXGGIjEVX-sI3cmLWUssCPQatL2OFyrskcUFGtJ1fOYxB_snPR3tHKb0zpPul6g0qxND1NJuq0AIuaB1EjJHS3yHexUTD_r4-CZMIC3GP-9ACKqtVkcs5sO_s_u4ocDoKuLtFuAstfw-REB9QPLx9uQO8H2RAEfN7SF9VGBKWMhVr3yuHb7T39ji95yVH_mmvH-OdZx6m-e-4v3-5tnE8XubIxHLuUpUXJvmjfNtyjJdcUPPhK69y0z9XaBcaDV10NrTWlkS7bzKOuKBUxPsXDWqsKFWweV08EWD8nON_RlrKfBS9AUMFkYAdaJwJBfnfisMq5XuVGXbUWAb9Hw\", \"x5t\": \"yTXZEcT2QV3jHeJtKaOfY0v3ytQ\", \"x5t#256\": \"dxUl8dAr2wROuI1ZOGX5x8QZoqmSAoQ8IXW7pyAVB6s\", \"x5u\": \"https://keystore.mit.openbanking.qa/nBRcYAcACnghbGFOBk/yTXZEcT2QV3jHeJtKaOfY0v3ytQ.pem\", \"x5c\": [\"MIIFoTCCBImgAwIBAgIEWWwmMTANBgkqhkiG9w0BAQsFADBmMQswCQYDVQQGEwJHQjEdMBsGA1UEChMUT3BlbiBCYW5raW5nIExpbWl0ZWQxETAPBgNVBAsTCFRlc3QgUEtJMSUwIwYDVQQDExxPcGVuIEJhbmtpbmcgVGVzdCBJc3N1aW5nIENBMB4XDTE4MDQyNjEzMTE0MVoXDTE5MDUyNjEzNDE0MVowajELMAkGA1UEBhMCR0IxHTAbBgNVBAoTFE9wZW4gQmFua2luZyBMaW1pdGVkMRswGQYDVQQLExJuQlJjWUFjQUNuZ2hiR0ZPQmsxHzAdBgNVBAMTFjRQNU5uRXBNM1dFeTFyV3dta1RydWkwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDRrCtcYYiMRVf6wjdyYtZSywI9Bq0vY4XKuyRxQUa0nV85jEH+yc9He0cpvTOk+6XqDSrE0PU0m6rQAi5oHUSMkdLfId7FRMP+vj4JkwgLcY/70AIqq1WRyzmw7+z+7ihwOgq4u0W4Cy1/D5EQH1A8vH25A7wfZEAR83tIX1UYEpYyFWvfK4dvtPf2OL3nJUf+aa8f451nHqb577i/f7m2cTxe5sjEcu5SlRcm+aN823KMl1xQ8+Err3LTP1doFxoNXXQ2tNaWRLtvMo64oFTE+xcNaqwoVbB5XTwRYPyc439GWsp8FL0BQwWRgB1onAkF+d+Kwyrle5UZdtRYBv0fAgMBAAGjggJRMIICTTAOBgNVHQ8BAf8EBAMCBsAwFQYDVR0lBA4wDAYKKwYBBAGCNwoDDDCB4AYDVR0gBIHYMIHVMIHSBgsrBgEEAah1gQYBZDCBwjAqBggrBgEFBQcCARYeaHR0cDovL29iLnRydXN0aXMuY29tL3BvbGljaWVzMIGTBggrBgEFBQcCAjCBhgyBg1VzZSBvZiB0aGlzIENlcnRpZmljYXRlIGNvbnN0aXR1dGVzIGFjY2VwdGFuY2Ugb2YgdGhlIE9wZW5CYW5raW5nIFJvb3QgQ0EgQ2VydGlmaWNhdGlvbiBQb2xpY2llcyBhbmQgQ2VydGlmaWNhdGUgUHJhY3RpY2UgU3RhdGVtZW50MDoGCCsGAQUFBwEBBC4wLDAqBggrBgEFBQcwAYYeaHR0cDovL29idGVzdC50cnVzdGlzLmNvbS9vY3NwMIHEBgNVHR8EgbwwgbkwN6A1oDOGMWh0dHA6Ly9vYnRlc3QudHJ1c3Rpcy5jb20vcGtpL29idGVzdGlzc3VpbmdjYS5jcmwwfqB8oHqkeDB2MQswCQYDVQQGEwJHQjEdMBsGA1UEChMUT3BlbiBCYW5raW5nIExpbWl0ZWQxETAPBgNVBAsTCFRlc3QgUEtJMSUwIwYDVQQDExxPcGVuIEJhbmtpbmcgVGVzdCBJc3N1aW5nIENBMQ4wDAYDVQQDEwVDUkwxMjAfBgNVHSMEGDAWgBQPAcAv6Ghs9yO/jmVuiQ3GpoV3tzAdBgNVHQ4EFgQUuuzJQD+hKv5IGMe4fITgJasxaaIwDQYJKoZIhvcNAQELBQADggEBACcrCNyC9JxQ3e6dhhcrqssLOfe4QN7F4zZr5ywTv54uiPgCOaUylkHxnAT47eRvmt6ss0BJQO8Ypp45akiL/iqMPDq5q9WHMtJxCbichtVhmVQabaXwQSU4oEeR4cR3//LxSElGpa7zFGhhlU4CsX6PPaJk6UIgfI+qEg9YCyZPGuMce0y1IMZEmIVkgv5jGKXAuqPyFEEwwjH82DRJHPF1bWN2d2W8xJBNEQUPW9+pzc0PquU61JC2jVlcyvbXgQa1Eo6/dVVxLp1wnNIeDZeMy/jxoBGSlxm5W1gfEJEH406eOW8DsX3NaCbhr8+qo3IhAGC8kUaLv856jHFrjGg=\"], \"use\": \"sig\"}]}";
		when(requestHeaderAttributes.getX_ssl_client_ou()).thenReturn("nBRcYAcACnghbGFOBk");
//		ReflectionTestUtils.setField(generator,"jwksDomain","https://keystore.mit.openbanking.qa");
		when(restClient.callForGet(anyObject(),eq(String.class),any(HttpHeaders.class))).thenReturn(response);
		generator.getSigningCertificate("yTXZEcT2QV3jHeJtKaOfY0v3ytQ", "4P5NnEpM3WEy1rWwmkTrui");
	}
	
	@Test(expected=PSD2Exception.class)
	public void testGetSigningCertifcateException() {
		String response=null;
		when(requestHeaderAttributes.getX_ssl_client_ou()).thenReturn("nBRcYAcACnghbGFOBk");
	//	ReflectionTestUtils.setField(generator,"jwksDomain","https://keystore.mit.openbanking.qa");
		when(restClient.callForGet(anyObject(),eq(String.class),any(HttpHeaders.class))).thenReturn(response);
		generator.getSigningCertificate("yTXZEcT2QV3jHeJtKaOfY0v3ytQ", "4P5NnEpM3WEy1rWwmkTrui");
	}
	
	@Test(expected=PSD2Exception.class)
	public void testGetSigningCertifcateException1() {
		String response="{\\\"keys\\\": [{\\\"e\\\": \\\"AQAB\\\", \\\"kid\\\": \\\"HvWTP7HnV-G86ji_KJXHQ3CZN2w\\\", \\\"kty\\\": \\\"RSA\\\", \\\"n\\\": \\\"xwujM8RY3RCzYNRiPwV6alzdHMCS9td57-i1IsVIUl7MpWi11vma9jKR21JLHQvS3fGPHPEbdPp-n0QLtHS5NBPsDrPfem0dkObZOOZOX-r5BH6NpIT1AEpreqrNPtxeiDm7V53DBgncj42Ue9uqK6dKhpjr1kcSpKunvIx-9YrKgGaZ6uTehcYHyRcG7rM_zXZmabgzGo_DDfuVJ6XivklLH3Ndn1O-5s03LFoncTPl8AYqkzP08LQgosOwYte99EhbBmvqx4-v7ZTv_OIlwsaelr2HjSNeeYAd1w47F5R0Vt-wlhC8J3PNLO-3yMCoKYzsgzBNRGd3xZnZkc6efQ\\\", \\\"x5t\\\": \\\"HvWTP7HnV-G86ji_KJXHQ3CZN2w\\\", \\\"x5t#256\\\": \\\"WAJ1j3mJ7NEjwNsXj6rnnL16pb9GyR4We8tmZAXZO44\\\", \\\"x5u\\\": \\\"https://keystore.mit.openbanking.qa/nBRcYAcACnghbGFOBk/HvWTP7HnV-G86ji_KJXHQ3CZN2w.pem\\\", \\\""
				+ "\\\": [\\\"MIIFrDCCBJSgAwIBAgIEWWwmMDANBgkqhkiG9w0BAQsFADBmMQswCQYDVQQGEwJHQjEdMBsGA1UEChMUT3BlbiBCYW5raW5nIExpbWl0ZWQxETAPBgNVBAsTCFRlc3QgUEtJMSUwIwYDVQQDExxPcGVuIEJhbmtpbmcgVGVzdCBJc3N1aW5nIENBMB4XDTE4MDQyNjEzMTEzMVoXDTE5MDQyNjEzNDEzMVowajELMAkGA1UEBhMCR0IxHTAbBgNVBAoTFE9wZW4gQmFua2luZyBMaW1pdGVkMRswGQYDVQQLExJuQlJjWUFjQUNuZ2hiR0ZPQmsxHzAdBgNVBAMTFjRQNU5uRXBNM1dFeTFyV3dta1RydWkwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDHC6MzxFjdELNg1GI/BXpqXN0cwJL213nv6LUixUhSXsylaLXW+Zr2MpHbUksdC9Ld8Y8c8Rt0+n6fRAu0dLk0E+wOs996bR2Q5tk45k5f6vkEfo2khPUASmt6qs0+3F6IObtXncMGCdyPjZR726orp0qGmOvWRxKkq6e8jH71isqAZpnq5N6FxgfJFwbusz/NdmZpuDMaj8MN+5UnpeK+SUsfc12fU77mzTcsWidxM+XwBiqTM/TwtCCiw7Bi1730SFsGa+rHj6/tlO/84iXCxp6WvYeNI155gB3XDjsXlHRW37CWELwnc80s77fIwKgpjOyDME1EZ3fFmdmRzp59AgMBAAGjggJcMIICWDAOBgNVHQ8BAf8EBAMCB4AwIAYDVR0lAQH/BBYwFAYIKwYBBQUHAwEGCCsGAQUFBwMCMIHgBgNVHSAEgdgwgdUwgdIGCysGAQQBqHWBBgFkMIHCMCoGCCsGAQUFBwIBFh5odHRwOi8vb2IudHJ1c3Rpcy5jb20vcG9saWNpZXMwgZMGCCsGAQUFBwICMIGGDIGDVXNlIG9mIHRoaXMgQ2VydGlmaWNhdGUgY29uc3RpdHV0ZXMgYWNjZXB0YW5jZSBvZiB0aGUgT3BlbkJhbmtpbmcgUm9vdCBDQSBDZXJ0aWZpY2F0aW9uIFBvbGljaWVzIGFuZCBDZXJ0aWZpY2F0ZSBQcmFjdGljZSBTdGF0ZW1lbnQwOgYIKwYBBQUHAQEELjAsMCoGCCsGAQUFBzABhh5odHRwOi8vb2J0ZXN0LnRydXN0aXMuY29tL29jc3AwgcQGA1UdHwSBvDCBuTA3oDWgM4YxaHR0cDovL29idGVzdC50cnVzdGlzLmNvbS9wa2kvb2J0ZXN0aXNzdWluZ2NhLmNybDB+oHygeqR4MHYxCzAJBgNVBAYTAkdCMR0wGwYDVQQKExRPcGVuIEJhbmtpbmcgTGltaXRlZDERMA8GA1UECxMIVGVzdCBQS0kxJTAjBgNVBAMTHE9wZW4gQmFua2luZyBUZXN0IElzc3VpbmcgQ0ExDjAMBgNVBAMTBUNSTDEyMB8GA1UdIwQYMBaAFA8BwC/oaGz3I7+OZW6JDcamhXe3MB0GA1UdDgQWBBSvtBesNM6NLeqR4TcHiFbyQOIS8zANBgkqhkiG9w0BAQsFAAOCAQEAoTDP8thZrT5awc7JVhbhD5p/L6J3DNuNasZAwBDXNYRI5eNtZw6aqAzAQw0twJ2+H5lGDLDGPhY6WgahEAu+0H399dLMbEpR/WTwdp0nRtK9a2/Ru8UFj4wTzh2THeIIXjJ/slJ1ZSCaQfy13bq0glt02l9c5TgreKk8UERetgGirzXkaH6ghVC4/ULAQmuyFh//TQdPsI5beAn+ZCjlw0VpUg3LrQ7d9QWsGEFnFolhN5lSB4AGgIopW1kkTnryLeP2dsXs4SyoFHLkS0VGSw3uc1Myt+WOau1eTd5LNOF5rVyZz1JEP3R+9jbNm655r1C+ByvSKa0/mFYMV8RW/Q==\\\"], \\\"use\\\": \\\"enc\\\"}, {\\\"e\\\": \\\"AQAB\\\", \\\"kid\\\": \\\"yTXZEcT2QV3jHeJtKaOfY0v3ytQ\\\", \\\"kty\\\": \\\"RSA\\\", \\\"n\\\": \\\"0awrXGGIjEVX-sI3cmLWUssCPQatL2OFyrskcUFGtJ1fOYxB_snPR3tHKb0zpPul6g0qxND1NJuq0AIuaB1EjJHS3yHexUTD_r4-CZMIC3GP-9ACKqtVkcs5sO_s_u4ocDoKuLtFuAstfw-REB9QPLx9uQO8H2RAEfN7SF9VGBKWMhVr3yuHb7T39ji95yVH_mmvH-OdZx6m-e-4v3-5tnE8XubIxHLuUpUXJvmjfNtyjJdcUPPhK69y0z9XaBcaDV10NrTWlkS7bzKOuKBUxPsXDWqsKFWweV08EWD8nON_RlrKfBS9AUMFkYAdaJwJBfnfisMq5XuVGXbUWAb9Hw\\\", \\\"x5t\\\": \\\"yTXZEcT2QV3jHeJtKaOfY0v3ytQ\\\", \\\"x5t#256\\\": \\\"dxUl8dAr2wROuI1ZOGX5x8QZoqmSAoQ8IXW7pyAVB6s\\\", \\\"x5u\\\": \\\"https://keystore.mit.openbanking.qa/nBRcYAcACnghbGFOBk/yTXZEcT2QV3jHeJtKaOfY0v3ytQ.pem\\\", \\\"x5c\\\": [\\\"MIIFoTCCBImgAwIBAgIEWWwmMTANBgkqhkiG9w0BAQsFADBmMQswCQYDVQQGEwJHQjEdMBsGA1UEChMUT3BlbiBCYW5raW5nIExpbWl0ZWQxETAPBgNVBAsTCFRlc3QgUEtJMSUwIwYDVQQDExxPcGVuIEJhbmtpbmcgVGVzdCBJc3N1aW5nIENBMB4XDTE4MDQyNjEzMTE0MVoXDTE5MDUyNjEzNDE0MVowajELMAkGA1UEBhMCR0IxHTAbBgNVBAoTFE9wZW4gQmFua2luZyBMaW1pdGVkMRswGQYDVQQLExJuQlJjWUFjQUNuZ2hiR0ZPQmsxHzAdBgNVBAMTFjRQNU5uRXBNM1dFeTFyV3dta1RydWkwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDRrCtcYYiMRVf6wjdyYtZSywI9Bq0vY4XKuyRxQUa0nV85jEH+yc9He0cpvTOk+6XqDSrE0PU0m6rQAi5oHUSMkdLfId7FRMP+vj4JkwgLcY/70AIqq1WRyzmw7+z+7ihwOgq4u0W4Cy1/D5EQH1A8vH25A7wfZEAR83tIX1UYEpYyFWvfK4dvtPf2OL3nJUf+aa8f451nHqb577i/f7m2cTxe5sjEcu5SlRcm+aN823KMl1xQ8+Err3LTP1doFxoNXXQ2tNaWRLtvMo64oFTE+xcNaqwoVbB5XTwRYPyc439GWsp8FL0BQwWRgB1onAkF+d+Kwyrle5UZdtRYBv0fAgMBAAGjggJRMIICTTAOBgNVHQ8BAf8EBAMCBsAwFQYDVR0lBA4wDAYKKwYBBAGCNwoDDDCB4AYDVR0gBIHYMIHVMIHSBgsrBgEEAah1gQYBZDCBwjAqBggrBgEFBQcCARYeaHR0cDovL29iLnRydXN0aXMuY29tL3BvbGljaWVzMIGTBggrBgEFBQcCAjCBhgyBg1VzZSBvZiB0aGlzIENlcnRpZmljYXRlIGNvbnN0aXR1dGVzIGFjY2VwdGFuY2Ugb2YgdGhlIE9wZW5CYW5raW5nIFJvb3QgQ0EgQ2VydGlmaWNhdGlvbiBQb2xpY2llcyBhbmQgQ2VydGlmaWNhdGUgUHJhY3RpY2UgU3RhdGVtZW50MDoGCCsGAQUFBwEBBC4wLDAqBggrBgEFBQcwAYYeaHR0cDovL29idGVzdC50cnVzdGlzLmNvbS9vY3NwMIHEBgNVHR8EgbwwgbkwN6A1oDOGMWh0dHA6Ly9vYnRlc3QudHJ1c3Rpcy5jb20vcGtpL29idGVzdGlzc3VpbmdjYS5jcmwwfqB8oHqkeDB2MQswCQYDVQQGEwJHQjEdMBsGA1UEChMUT3BlbiBCYW5raW5nIExpbWl0ZWQxETAPBgNVBAsTCFRlc3QgUEtJMSUwIwYDVQQDExxPcGVuIEJhbmtpbmcgVGVzdCBJc3N1aW5nIENBMQ4wDAYDVQQDEwVDUkwxMjAfBgNVHSMEGDAWgBQPAcAv6Ghs9yO/jmVuiQ3GpoV3tzAdBgNVHQ4EFgQUuuzJQD+hKv5IGMe4fITgJasxaaIwDQYJKoZIhvcNAQELBQADggEBACcrCNyC9JxQ3e6dhhcrqssLOfe4QN7F4zZr5ywTv54uiPgCOaUylkHxnAT47eRvmt6ss0BJQO8Ypp45akiL/iqMPDq5q9WHMtJxCbichtVhmVQabaXwQSU4oEeR4cR3//LxSElGpa7zFGhhlU4CsX6PPaJk6UIgfI+qEg9YCyZPGuMce0y1IMZEmIVkgv5jGKXAuqPyFEEwwjH82DRJHPF1bWN2d2W8xJBNEQUPW9+pzc0PquU61JC2jVlcyvbXgQa1Eo6/dVVxLp1wnNIeDZeMy/jxoBGSlxm5W1gfEJEH406eOW8DsX3NaCbhr8+qo3IhAGC8kUaLv856jHFrjGg=\\\"], \\\"use\\\": \\\"sig\\\"}]}";
		when(requestHeaderAttributes.getX_ssl_client_ou()).thenReturn("nBRcYAcACnghbGFOBk");
//		ReflectionTestUtils.setField(generator,"jwksDomain","https://keystore.mit.openbanking.qa");
		when(restClient.callForGet(anyObject(),eq(String.class),any(HttpHeaders.class))).thenReturn(response);
		generator.getSigningCertificate("yTXZEcT2QV3jHeJtKaOfY0v3ytQ", "4P5NnEpM3WEy1rWwmkTrui");
	}
	
	@Test(expected=PSD2Exception.class)
	public void testGetSigningCertifcateException2() {
		when(requestHeaderAttributes.getX_ssl_client_ou()).thenReturn("nBRcYAcACnghbGFOBk");
		String response="{  \r\n" + 
				"   \"keys\":[  \r\n" + 
				"    \r\n" + 
				"   ]\r\n" + 
				"}";
	//	ReflectionTestUtils.setField(generator,"jwksDomain","https://keystore.mit.openbanking.qa");
		when(restClient.callForGet(anyObject(),eq(String.class),any(HttpHeaders.class))).thenReturn(response);
		generator.getSigningCertificate("yTXZEcT2QV3jHeJtKaOfY0v3ytQ", "4P5NnEpM3WEy1rWwmkTrui");
	}
	
	@Test(expected=PSD2Exception.class)
	public void testGetSigningCertifcateException3() {
		when(requestHeaderAttributes.getX_ssl_client_ou()).thenReturn("nBRcYAcACnghbGFOBk");
		String response="{\"keys\": [{\"e\": \"AQAB\", \"kid\": \"HvWTP7HnV-G86ji_KJXHQ3CZN2w\", \"kty\": \"RSA\", \"n\": \"xwujM8RY3RCzYNRiPwV6alzdHMCS9td57-i1IsVIUl7MpWi11vma9jKR21JLHQvS3fGPHPEbdPp-n0QLtHS5NBPsDrPfem0dkObZOOZOX-r5BH6NpIT1AEpreqrNPtxeiDm7V53DBgncj42Ue9uqK6dKhpjr1kcSpKunvIx-9YrKgGaZ6uTehcYHyRcG7rM_zXZmabgzGo_DDfuVJ6XivklLH3Ndn1O-5s03LFoncTPl8AYqkzP08LQgosOwYte99EhbBmvqx4-v7ZTv_OIlwsaelr2HjSNeeYAd1w47F5R0Vt-wlhC8J3PNLO-3yMCoKYzsgzBNRGd3xZnZkc6efQ\", \"x5t\": \"HvWTP7HnV-G86ji_KJXHQ3CZN2w\", \"x5t#256\": \"WAJ1j3mJ7NEjwNsXj6rnnL16pb9GyR4We8tmZAXZO44\", \"x5u\": \"https://keystore.mit.openbanking.qa/nBRcYAcACnghbGFOBk/HvWTP7HnV-G86ji_KJXHQ3CZN2w.pem\", \"x5c\": [\"MIIFrDCCBJSgAwIBAgIEWWwmMDANBgkqhkiG9w0BAQsFADBmMQswCQYDVQQGEwJHQjEdMBsGA1UEChMUT3BlbiBCYW5raW5nIExpbWl0ZWQxETAPBgNVBAsTCFRlc3QgUEtJMSUwIwYDVQQDExxPcGVuIEJhbmtpbmcgVGVzdCBJc3N1aW5nIENBMB4XDTE4MDQyNjEzMTEzMVoXDTE5MDQyNjEzNDEzMVowajELMAkGA1UEBhMCR0IxHTAbBgNVBAoTFE9wZW4gQmFua2luZyBMaW1pdGVkMRswGQYDVQQLExJuQlJjWUFjQUNuZ2hiR0ZPQmsxHzAdBgNVBAMTFjRQNU5uRXBNM1dFeTFyV3dta1RydWkwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDHC6MzxFjdELNg1GI/BXpqXN0cwJL213nv6LUixUhSXsylaLXW+Zr2MpHbUksdC9Ld8Y8c8Rt0+n6fRAu0dLk0E+wOs996bR2Q5tk45k5f6vkEfo2khPUASmt6qs0+3F6IObtXncMGCdyPjZR726orp0qGmOvWRxKkq6e8jH71isqAZpnq5N6FxgfJFwbusz/NdmZpuDMaj8MN+5UnpeK+SUsfc12fU77mzTcsWidxM+XwBiqTM/TwtCCiw7Bi1730SFsGa+rHj6/tlO/84iXCxp6WvYeNI155gB3XDjsXlHRW37CWELwnc80s77fIwKgpjOyDME1EZ3fFmdmRzp59AgMBAAGjggJcMIICWDAOBgNVHQ8BAf8EBAMCB4AwIAYDVR0lAQH/BBYwFAYIKwYBBQUHAwEGCCsGAQUFBwMCMIHgBgNVHSAEgdgwgdUwgdIGCysGAQQBqHWBBgFkMIHCMCoGCCsGAQUFBwIBFh5odHRwOi8vb2IudHJ1c3Rpcy5jb20vcG9saWNpZXMwgZMGCCsGAQUFBwICMIGGDIGDVXNlIG9mIHRoaXMgQ2VydGlmaWNhdGUgY29uc3RpdHV0ZXMgYWNjZXB0YW5jZSBvZiB0aGUgT3BlbkJhbmtpbmcgUm9vdCBDQSBDZXJ0aWZpY2F0aW9uIFBvbGljaWVzIGFuZCBDZXJ0aWZpY2F0ZSBQcmFjdGljZSBTdGF0ZW1lbnQwOgYIKwYBBQUHAQEELjAsMCoGCCsGAQUFBzABhh5odHRwOi8vb2J0ZXN0LnRydXN0aXMuY29tL29jc3AwgcQGA1UdHwSBvDCBuTA3oDWgM4YxaHR0cDovL29idGVzdC50cnVzdGlzLmNvbS9wa2kvb2J0ZXN0aXNzdWluZ2NhLmNybDB+oHygeqR4MHYxCzAJBgNVBAYTAkdCMR0wGwYDVQQKExRPcGVuIEJhbmtpbmcgTGltaXRlZDERMA8GA1UECxMIVGVzdCBQS0kxJTAjBgNVBAMTHE9wZW4gQmFua2luZyBUZXN0IElzc3VpbmcgQ0ExDjAMBgNVBAMTBUNSTDEyMB8GA1UdIwQYMBaAFA8BwC/oaGz3I7+OZW6JDcamhXe3MB0GA1UdDgQWBBSvtBesNM6NLeqR4TcHiFbyQOIS8zANBgkqhkiG9w0BAQsFAAOCAQEAoTDP8thZrT5awc7JVhbhD5p/L6J3DNuNasZAwBDXNYRI5eNtZw6aqAzAQw0twJ2+H5lGDLDGPhY6WgahEAu+0H399dLMbEpR/WTwdp0nRtK9a2/Ru8UFj4wTzh2THeIIXjJ/slJ1ZSCaQfy13bq0glt02l9c5TgreKk8UERetgGirzXkaH6ghVC4/ULAQmuyFh//TQdPsI5beAn+ZCjlw0VpUg3LrQ7d9QWsGEFnFolhN5lSB4AGgIopW1kkTnryLeP2dsXs4SyoFHLkS0VGSw3uc1Myt+WOau1eTd5LNOF5rVyZz1JEP3R+9jbNm655r1C+ByvSKa0/mFYMV8RW/Q==\"], \"use\": \"enc\"}, {\"e\": \"AQAB\", \"kid\": \"yTXZEcT2QV3jHeJtKaOfY0v3ytQ\", \"kty\": \"RSA\", \"n\": \"0awrXGGIjEVX-sI3cmLWUssCPQatL2OFyrskcUFGtJ1fOYxB_snPR3tHKb0zpPul6g0qxND1NJuq0AIuaB1EjJHS3yHexUTD_r4-CZMIC3GP-9ACKqtVkcs5sO_s_u4ocDoKuLtFuAstfw-REB9QPLx9uQO8H2RAEfN7SF9VGBKWMhVr3yuHb7T39ji95yVH_mmvH-OdZx6m-e-4v3-5tnE8XubIxHLuUpUXJvmjfNtyjJdcUPPhK69y0z9XaBcaDV10NrTWlkS7bzKOuKBUxPsXDWqsKFWweV08EWD8nON_RlrKfBS9AUMFkYAdaJwJBfnfisMq5XuVGXbUWAb9Hw\", \"x5t\": \"yTXZEcT2QV3jHeJtKaOfY0v3ytQ\", \"x5t#256\": \"dxUl8dAr2wROuI1ZOGX5x8QZoqmSAoQ8IXW7pyAVB6s\", \"x5u\": \"https://keystore.mit.openbanking.qa/nBRcYAcACnghbGFOBk/yTXZEcT2QV3jHeJtKaOfY0v3ytQ.pem\", \"x5c\": [\"MIIFoTCCBImgAwIBAgIEWWwmMTANBgkqhkiG9w0BAQsFADBmMQswCQYDVQQGEwJHQjEdMBsGA1UEChMUT3BlbiBCYW5raW5nIExpbWl0ZWQxETAPBgNVBAsTCFRlc3QgUEtJMSUwIwYDVQQDExxPcGVuIEJhbmtpbmcgVGVzdCBJc3N1aW5nIENBMB4XDTE4MDQyNjEzMTE0MVoXDTE5MDUyNjEzNDE0MVowajELMAkGA1UEBhMCR0IxHTAbBgNVBAoTFE9wZW4gQmFua2luZyBMaW1pdGVkMRswGQYDVQQLExJuQlJjWUFjQUNuZ2hiR0ZPQmsxHzAdBgNVBAMTFjRQNU5uRXBNM1dFeTFyV3dta1RydWkwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDRrCtcYYiMRVf6wjdyYtZSywI9Bq0vY4XKuyRxQUa0nV85jEH+yc9He0cpvTOk+6XqDSrE0PU0m6rQAi5oHUSMkdLfId7FRMP+vj4JkwgLcY/70AIqq1WRyzmw7+z+7ihwOgq4u0W4Cy1/D5EQH1A8vH25A7wfZEAR83tIX1UYEpYyFWvfK4dvtPf2OL3nJUf+aa8f451nHqb577i/f7m2cTxe5sjEcu5SlRcm+aN823KMl1xQ8+Err3LTP1doFxoNXXQ2tNaWRLtvMo64oFTE+xcNaqwoVbB5XTwRYPyc439GWsp8FL0BQwWRgB1onAkF+d+Kwyrle5UZdtRYBv0fAgMBAAGjggJRMIICTTAOBgNVHQ8BAf8EBAMCBsAwFQYDVR0lBA4wDAYKKwYBBAGCNwoDDDCB4AYDVR0gBIHYMIHVMIHSBgsrBgEEAah1gQYBZDCBwjAqBggrBgEFBQcCARYeaHR0cDovL29iLnRydXN0aXMuY29tL3BvbGljaWVzMIGTBggrBgEFBQcCAjCBhgyBg1VzZSBvZiB0aGlzIENlcnRpZmljYXRlIGNvbnN0aXR1dGVzIGFjY2VwdGFuY2Ugb2YgdGhlIE9wZW5CYW5raW5nIFJvb3QgQ0EgQ2VydGlmaWNhdGlvbiBQb2xpY2llcyBhbmQgQ2VydGlmaWNhdGUgUHJhY3RpY2UgU3RhdGVtZW50MDoGCCsGAQUFBwEBBC4wLDAqBggrBgEFBQcwAYYeaHR0cDovL29idGVzdC50cnVzdGlzLmNvbS9vY3NwMIHEBgNVHR8EgbwwgbkwN6A1oDOGMWh0dHA6Ly9vYnRlc3QudHJ1c3Rpcy5jb20vcGtpL29idGVzdGlzc3VpbmdjYS5jcmwwfqB8oHqkeDB2MQswCQYDVQQGEwJHQjEdMBsGA1UEChMUT3BlbiBCYW5raW5nIExpbWl0ZWQxETAPBgNVBAsTCFRlc3QgUEtJMSUwIwYDVQQDExxPcGVuIEJhbmtpbmcgVGVzdCBJc3N1aW5nIENBMQ4wDAYDVQQDEwVDUkwxMjAfBgNVHSMEGDAWgBQPAcAv6Ghs9yO/jmVuiQ3GpoV3tzAdBgNVHQ4EFgQUuuzJQD+hKv5IGMe4fITgJasxaaIwDQYJKoZIhvcNAQELBQADggEBACcrCNyC9JxQ3e6dhhcrqssLOfe4QN7F4zZr5ywTv54uiPgCOaUylkHxnAT47eRvmt6ss0BJQO8Ypp45akiL/iqMPDq5q9WHMtJxCbichtVhmVQabaXwQSU4oEeR4cR3//LxSElGpa7zFGhhlU4CsX6PPaJk6UIgfI+qEg9YCyZPGuMce0y1IMZEmIVkgv5jGKXAuqPyFEEwwjH82DRJHPF1bWN2d2W8xJBNEQUPW9+pzc0PquU61JC2jVlcyvbXgQa1Eo6/dVVxLp1wnNIeDZeMy/jxoBGSlxm5W1gfEJEH406eOW8DsX3NaCbhr8+qo3IhAGC8kUaLv856jHFrjGg=\"], \"use\": \"sig\"}]}";
//		ReflectionTestUtils.setField(generator,"jwksDomain","https://keystore.mit.openbanking.qa");
		when(restClient.callForGet(anyObject(),eq(String.class),any(HttpHeaders.class))).thenReturn(response);
		generator.getSigningCertificate("yTXZEcT2QV3jeJtKaOfY0v3ytQ", "4P5NnEpM3WEy1rWwmkTrui");
	}
}
