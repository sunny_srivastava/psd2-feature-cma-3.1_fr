package com.capgemini.psd2.integration.controller.test;

import static org.mockito.Matchers.anyString;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.capgemini.psd2.integration.controller.JWKController;
import com.capgemini.psd2.integration.service.TPPInformationService;

public class JWKControllerTest {

	@Mock
	private TPPInformationService service;

	@InjectMocks
	private JWKController controller;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testpublicKeyForClient() {
		service.fetchClientInformation(anyString(), anyString());
		controller.publicKeyForClient("aspsp", "1OEwYAKIgMtefvOKfSEdAS", "tenant1");
	}

	@Test
	public void testfetchJwksUrlForFiles() {
		service.fetchJWKSUrl(anyString());
		controller.fetchJwksUrlForFiles("1OEwYAKIgMtefvOKfSEdAS");
	}

}
