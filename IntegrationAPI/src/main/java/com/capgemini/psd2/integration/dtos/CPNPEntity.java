
package com.capgemini.psd2.integration.dtos;

import java.util.Date;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "#{@CollectionNameLocator.getCPNPCollectionName()}")
public class CPNPEntity {

	private String psuId;
	private String tppId;
	private String tenantId;
	private String cmaVersion;

	private Date insertedAt = new Date();

	public String getPsuId() {
		return psuId;
	}

	public void setPsuId(String psuId) {
		this.psuId = psuId;
	}

	public String getTppId() {
		return tppId;
	}

	public void setTppId(String tppId) {
		this.tppId = tppId;
	}

	public Date getInsertedAt() {
		return insertedAt;
	}

	public void setInsertedAt(Date insertedAt) {
		this.insertedAt = insertedAt;
	}


	public String getTenantId() {
		return tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}

	public String getCmaVersion() {
		return cmaVersion;
	}

	public void setCmaVersion(String cmaVersion) {
		this.cmaVersion = cmaVersion;
	}

	@Override
	public String toString() {
		return "CPNPEntity [psuId=" + psuId + ", tppId=" + tppId + ", tenantId=" + tenantId + ", cmaVersion="
				+ cmaVersion + ", insertedAt=" + insertedAt + "]";
	}



}
