package com.capgemini.psd2.integration.utilities;

import java.security.Key;
import java.security.interfaces.RSAPublicKey;
import com.capgemini.psd2.cma2.aisp.domain.NoPayloadLogging;
import com.capgemini.psd2.utilities.AbstractJWSVerifier;
import com.nimbusds.jose.Header;
import com.nimbusds.jose.JWSVerifier;
import com.nimbusds.jose.crypto.RSASSAVerifier;
import com.nimbusds.jose.crypto.bc.BouncyCastleProviderSingleton;

public class RSAVerifierGenerator extends AbstractJWSVerifier{
	
	@Override
	@NoPayloadLogging
	public JWSVerifier createJWSVerifier(Key key,Header header) {
		RSASSAVerifier rsaVerifier=new RSASSAVerifier((RSAPublicKey)key, header.getCriticalParams());
		rsaVerifier.getJCAContext().setProvider(BouncyCastleProviderSingleton.getInstance());
		return rsaVerifier;
	}

}
