package com.capgemini.psd2.integration.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Service;

import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.integration.dtos.CPNPEntity;
import com.capgemini.psd2.integration.repository.CPNPRepository;
import com.capgemini.psd2.integration.service.CPNPOperationsService;
import com.capgemini.psd2.product.common.CompatibleVersionList;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Service
@ConfigurationProperties("app")
public class CPNPOperationsServiceImpl implements CPNPOperationsService {

	private int maxRequestsAllowed;
	
	@Autowired
	private CPNPRepository repository; 
	
	@Autowired
	private CompatibleVersionList compatibleVersionList;

	
	public int getMaxRequestsAllowed() {
		return maxRequestsAllowed;
	}


	public void setMaxRequestsAllowed(int maxRequestsAllowed) {
		this.maxRequestsAllowed = maxRequestsAllowed;
	}


	@Override
	public void verifyClient(String tppId, String psuId) {
		CPNPEntity cpnpEntity = new CPNPEntity();
		
		if (NullCheckUtils.isNullOrEmpty(tppId) || NullCheckUtils.isNullOrEmpty(psuId))
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.REQUIRED_HEADERS_MISSING_FROM_MULE);

		cpnpEntity.setPsuId(psuId);
		cpnpEntity.setTppId(tppId);

		List<CPNPEntity> cpnpHitCount = repository.findByPsuIdAndTppIdAndCmaVersionIn(psuId,tppId,compatibleVersionList.fetchVersionList());
		if (cpnpHitCount.size() < maxRequestsAllowed)
			repository.save(cpnpEntity);
		else
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.MAX_REQUESTS_REACHED_UNDER_CPNP);

	}

}
