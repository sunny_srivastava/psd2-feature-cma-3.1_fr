package com.capgemini.psd2.integration.service.impl;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.capgemini.psd2.aisp.adapter.AccountRequestAdapter;
import com.capgemini.psd2.aisp.adapter.AispConsentAdapter;
import com.capgemini.psd2.aisp.domain.OBReadConsentResponse1;
import com.capgemini.psd2.aisp.domain.OBReadConsentResponse1Data;
import com.capgemini.psd2.cisp.adapter.CispConsentAdapter;
import com.capgemini.psd2.consent.domain.AispConsent;
import com.capgemini.psd2.consent.domain.CispConsent;
import com.capgemini.psd2.consent.domain.PispConsent;
import com.capgemini.psd2.enums.ConsentStatusEnum;
import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.integration.service.TokenTransformationService;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.pisp.adapter.PispConsentAdapter;
import com.capgemini.psd2.token.ConsentTokenData;
import com.capgemini.psd2.token.Token;

@Service
public class TokenTransformationServiceImpl implements TokenTransformationService{

	@Autowired
	private AispConsentAdapter aispConsentAdapter;

	@Autowired
	private PispConsentAdapter pispConsentAdapter;
	
	@Autowired
	private CispConsentAdapter cispConsentAdapter;

	@Autowired
	@Qualifier("accountRequestRoutingAdapter")
	private AccountRequestAdapter accountRequestAdapter;
	
	@Override
	public void transformAISPToken(Token token, String accountRequestId) {
		ConsentTokenData consentData;

		Set<Object> claims = new HashSet<>();
		Map<String, Set<Object>> scopeClaims = new HashMap<>();
		AispConsent aispConsent = aispConsentAdapter.retrieveConsentByAccountRequestIdAndStatus(accountRequestId, ConsentStatusEnum.AUTHORISED);
		OBReadConsentResponse1 accountRequest = accountRequestAdapter.getAccountRequestGETResponse(accountRequestId);

		if (aispConsent == null) {
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.CONSENT_DATA_NOT_PRESENT);
		}
		if (accountRequest == null) {
			throw PSD2Exception.populatePSD2Exception("Account request intent does not exist",
					ErrorCodeEnum.INTENT_ID_VALIDATION_ERROR);
		}

		List<OBReadConsentResponse1Data.PermissionsEnum> permissions = accountRequest.getData().getPermissions();
		if (permissions == null || permissions.isEmpty()) {
			throw PSD2Exception.populatePSD2Exception("No permissions associated with intent",
					ErrorCodeEnum.PERMISSIONS_VALIDATION_ERROR);
		}

		for (OBReadConsentResponse1Data.PermissionsEnum permissionEnum : permissions) {
			claims.add(permissionEnum.name());
		}
		scopeClaims.put(PSD2Constants.ACCOUNTS, claims);
		consentData = populateConsentData(aispConsent.getEndDate(), aispConsent.getConsentId());
		token.setConsentType(PSD2Constants.AISP);
		token.setClaims(scopeClaims);
		token.setScope(
				new HashSet<>(Arrays.asList(new String[] { PSD2Constants.OPEN_ID, PSD2Constants.ACCOUNTS })));
		token.setUser_name(aispConsent.getPsuId());
		token.setConsentTokenData(consentData);
	}

	@Override
	public void transformPISPToken(Token token, String paymentRequestId) {
		Map<String, Set<Object>> scopeClaims = new HashMap<>();		
		PispConsent pispConsent = pispConsentAdapter.retrieveConsentByPaymentId(paymentRequestId, ConsentStatusEnum.AUTHORISED);
		if (pispConsent == null) {
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.CONSENT_DATA_NOT_PRESENT);
		}
		ConsentTokenData consentData = populateConsentData(pispConsent.getEndDate(), pispConsent.getConsentId());
		token.setConsentType(PSD2Constants.PISP);
		scopeClaims.put(PSD2Constants.PAYMENTS, new HashSet<Object>());
		token.setClaims(scopeClaims);
		token.setScope(
				new HashSet<>(Arrays.asList(new String[] { PSD2Constants.OPEN_ID, PSD2Constants.PAYMENTS })));
		token.setUser_name(pispConsent.getPsuId());
		token.setConsentTokenData(consentData);
	}
	
	@Override
	public void transformCISPToken(Token token, String fundsConfirmationConsentId) {

		Map<String, Set<Object>> scopeClaims = new HashMap<>();
		CispConsent cispConsent = cispConsentAdapter.retrieveConsentByFundsIntentIdAndStatus(fundsConfirmationConsentId ,ConsentStatusEnum.AUTHORISED);
		if (cispConsent == null) {
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.CONSENT_DATA_NOT_PRESENT);
		}
		ConsentTokenData consentData = populateConsentData(cispConsent.getEndDate(), cispConsent.getConsentId());
		token.setConsentType(PSD2Constants.CISP);
		scopeClaims.put(PSD2Constants.FUNDSCONFIRMATIONS, new HashSet<Object>());
		token.setClaims(scopeClaims);
		token.setScope(
				new HashSet<>(Arrays.asList(new String[] { PSD2Constants.OPEN_ID, PSD2Constants.FUNDSCONFIRMATIONS })));
		token.setUser_name(cispConsent.getPsuId());
		token.setConsentTokenData(consentData);
	
	}
	
	private ConsentTokenData populateConsentData(String expiry, String consentId) {
		ConsentTokenData consentTokenData = new ConsentTokenData();
		consentTokenData.setConsentExpiry(expiry);
		consentTokenData.setConsentId(consentId);
		return consentTokenData;
	}	
}
