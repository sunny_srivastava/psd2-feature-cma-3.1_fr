package com.capgemini.psd2.integration.utilities;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.Base64;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.function.Predicate;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.exceptions.ErrorMapKeys;
import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.integration.service.TPPInformationService;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.RestClientSync;
import com.capgemini.psd2.utilities.CertificateGenerator;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Component
public class JWKSGenerator implements CertificateGenerator {

	@Autowired
	private RestClientSync restClient;
	
	@Autowired
	private TPPInformationService tppInformationService;

	@Override
	public X509Certificate getSigningCertificate(String kid, String clientId) {
		String requestUrl=tppInformationService.fetchJWKSUrl(clientId);
		return retrieveCertifciate(kid, requestUrl);
	}

	private X509Certificate retrieveCertifciate(String kid, String jwksEndpoint) {
		String jwks = retirveJWKSCert(jwksEndpoint);
		if(NullCheckUtils.isNullOrEmpty(jwks)) {
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR, "jwks fetch failed"));
		}
		String certString = retrieveX5c(kid, jwks);
		if (NullCheckUtils.isNullOrEmpty(certString)) {
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR, "invalid certifcate received from OB"));
		}
		return convertStringtoCertificate(certString);
	}

	private String retirveJWKSCert(String obJWKSEndpoint) {
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(obJWKSEndpoint);
		return restClient.callForGet(requestInfo, String.class, null);
	}

	private String retrieveX5c(String kid, String jwks) {
		JSONObject jsonObject = null;
		JSONObject matchedCerts = null; 
		JSONParser jsonParser = new JSONParser();
		try {
			jsonObject = (JSONObject) jsonParser.parse(jwks);
			JSONArray jsonArray = (JSONArray) jsonObject.get(PSD2Constants.KEYS);
			if(jsonArray == null || jsonArray.isEmpty()) {
				throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR, "No keys found in the jwks"));
			}
			Predicate<JSONObject> matchingPredicate = jsonObj -> jsonObj.get(PSD2Constants.KID).equals(kid.trim());		
			matchedCerts = (JSONObject)jsonArray.stream().filter(matchingPredicate).findFirst().get();		
		} catch (NoSuchElementException e) {
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_SIGNATURE_INVALIDCLAIM,ErrorMapKeys.SIGNATURE_KID_INVALID));
		}catch (ParseException e) {
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,"invalid response from OB jwks endpoint"));
		}
		
		return ((List<String>)matchedCerts.get(PSD2Constants.X5C)).get(0);
	}

	private X509Certificate convertStringtoCertificate(String certString) {
		CertificateFactory cf = null;
		X509Certificate cert = null;		
		try {
			byte[] arr = Base64.getDecoder().decode(certString);
			InputStream stream = new ByteArrayInputStream(arr);
			cf = CertificateFactory.getInstance(PSD2Constants.X509);
			cert = (X509Certificate) cf.generateCertificate(stream);
		} catch (CertificateException  | IllegalArgumentException ie) {
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR, "Certifcate Generation failed"));
		} 
		return cert;
	}

}
