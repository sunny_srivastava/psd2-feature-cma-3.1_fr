package com.capgemini.psd2.integration.service.impl;

import java.io.IOException;
import java.io.InputStream;
import java.security.Key;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.jose4j.jwt.NumericDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;

import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.integration.service.ResponseSigningService;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JOSEObjectType;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.JWSHeader;
import com.nimbusds.jose.JWSSigner;
import com.nimbusds.jose.Payload;
import com.nimbusds.jose.crypto.RSASSASigner;
import com.nimbusds.jose.crypto.bc.BouncyCastleProviderSingleton;
import com.nimbusds.jose.util.Base64URL;

@Service
@ConfigurationProperties("app.signature")
public class ResponseSigningServiceImpl implements ResponseSigningService{
	
	@Autowired
	private RequestHeaderAttributes requestHeaderAttributes;
	
	@Value("${server.ssl.key-store}")
	private String keystorePath;

	@Value("${server.ssl.key-store-password}")
	private String keystorePassword;
	
	private KeyStore keyStore = null;

	private List<String> critData = new ArrayList<>();
	
	private Map<String, Map<String, String>> signingKeyDetails=new HashMap<>();

	public List<String> getCritData() {
		return critData;
	}
	
	public Map<String, Map<String, String>> getSigningKeyDetails() {
		return signingKeyDetails;
	}
	
	@PostConstruct
	public void populateKeyStore() {
		
		try {
	 		Resource resource = new ClassPathResource(keystorePath);
			InputStream in = resource.getInputStream();
			keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
			keyStore.load(in, keystorePassword.toCharArray());
		} catch (KeyStoreException | NoSuchAlgorithmException | CertificateException | IOException e) {
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR, e.getMessage()));
		}
	}

	@Override
	public String signResponse(String responseBody, MediaType mediaType) {

		Base64URL signature = null;
		String signingInputString = "";
		try {
			JWSHeader header = populateJWSHeader(mediaType);

			Payload payload = new Payload(responseBody);

			if (header.getCustomParam("b64") == null || (Boolean) header.getCustomParam("b64")) {
				signingInputString = header.toBase64URL().toString() + '.' + payload.toBase64URL().toString();
			} else {
				signingInputString = header.toBase64URL().toString() + '.' + payload.toString();
			}

			PrivateKey key = (PrivateKey) getPrivateKey();
			JWSSigner signer = new RSASSASigner(key);
			signer.getJCAContext().setProvider(BouncyCastleProviderSingleton.getInstance());

			if (!signer.supportedJWSAlgorithms().contains(header.getAlgorithm()))
				throw new JOSEException("The \"" + header.getAlgorithm()
						+ "\" algorithm is not allowed or supported by the JWS signer: Supported algorithms: "
						+ signer.supportedJWSAlgorithms());

			signature = signer.sign(header, signingInputString.getBytes());

		} catch (Exception e) {
			throw PSD2Exception
					.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR, e.getMessage()));
		}

		return signingInputString.split("\\.")[0] + ".." + signature;
	}

	private String getSubjectFromCertificate() {
		X509Certificate certificate = null;
		try {
			certificate = (X509Certificate) keyStore.getCertificate(signingKeyDetails.get(requestHeaderAttributes.getTenantId()).get("alias"));
			return certificate.getSubjectDN().toString();
		} catch (KeyStoreException e) {
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR, e.getMessage()));
		}
	}

	private Key getPrivateKey() {
		try {
			return keyStore.getKey(signingKeyDetails.get(requestHeaderAttributes.getTenantId()).get("alias"),
					keystorePassword.toCharArray());
		} catch (UnrecoverableKeyException | NoSuchAlgorithmException | KeyStoreException e) {
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR, e.getMessage()));
		}
	}

	private JWSHeader populateJWSHeader(MediaType mediaType) {
		Set<String> critHeaders = new HashSet<>();
		critHeaders.addAll(critData);
		
		return new JWSHeader.Builder(JWSAlgorithm.parse(signingKeyDetails.get(requestHeaderAttributes.getTenantId()).get("signingAlgo"))).criticalParams(critHeaders)
				.customParam(PSD2Constants.JOSE_HEADER_B64, Boolean.FALSE)
				.customParam(PSD2Constants.JOSE_HEADER_ISSUER, getSubjectFromCertificate())
				.customParam(PSD2Constants.JOSE_HEADER_IAT, NumericDate.now().getValue())
				.customParam(PSD2Constants.JOSE_HEADER_TRUST_ANCHOR, "openbanking.org.uk")
				.type(JOSEObjectType.JOSE)
				.keyID(signingKeyDetails.get(requestHeaderAttributes.getTenantId()).get("kid"))
				.contentType(mediaType.toString()).build();
	}

}
