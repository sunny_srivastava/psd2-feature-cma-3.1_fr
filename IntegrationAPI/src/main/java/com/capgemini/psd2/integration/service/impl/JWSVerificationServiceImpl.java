package com.capgemini.psd2.integration.service.impl;

import java.security.PublicKey;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

import org.jose4j.jwt.NumericDate;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;

import com.capgemini.psd2.exceptions.ErrorMapKeys;
import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.integration.service.JWSVerificationService;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.utilities.AbstractJWSVerifier;
import com.capgemini.psd2.utilities.CertificateGenerator;
import com.capgemini.psd2.utilities.NullCheckUtils;
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.util.Base64URL;
import com.nimbusds.jwt.SignedJWT;

@Service
@ConfigurationProperties(prefix = "app.signature")
public class JWSVerificationServiceImpl implements JWSVerificationService {

	@Autowired
	private RequestHeaderAttributes requestHeaderAttributes;

	@Autowired
	private CertificateGenerator certificateGenerator;
	
	@Autowired
	private AbstractJWSVerifier verifier;

	private List<String> supportedAlgo = new ArrayList<>();

	private List<String> critData = new ArrayList<>();

	@Value("${app.signature.domainName}")
	private String domainName;

	private List<ExceptionDTO> exceptionData = null;

	@Value("${app.signature.iatLeeway:#{0}}")
	private Integer iatLeeway;
	
	public List<String> getSupportedAlgo() {
		return supportedAlgo;
	}

	public List<String> getCritData() {
		return critData;
	}

	public String getDomainName() {
		return domainName;
	}

	public void setDomainName(String domainName) {
		this.domainName = domainName;
	}

	@Override
	public void validateSignature(String body, String clientId) {
		
		exceptionData = new ArrayList<>();
		Map<String, Object> params = new HashMap<>();

		String jwsHeader = requestHeaderAttributes.getJwsHeader();
		SignedJWT signedJWT;

		if (jwsHeader.equals(PSD2Constants.NOT_DEFINED))
			jwsHeader = null;
		if (NullCheckUtils.isNullOrEmpty(jwsHeader)) {
			throw PSD2Exception.populatePSD2Exception(
					new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_SIGNATURE_MISSING, ErrorMapKeys.SIGNATURE_MISSING));
		} else {
			String[] headerParts = jwsHeader.split("\\.");
			validateHeader(headerParts, params);

		signedJWT = populateSignedJWT(headerParts, body);
			params.put(PSD2Constants.CLIENT_ID, clientId);

			verifySignature(signedJWT, params);
		}
	}

	private void validateHeader(String[] headerParts, Map<String, Object> params) {

		String joseHeaderJsonString = null;
		JSONObject joseHeader = null;
		JSONParser jsonParser = new JSONParser();

		Map<String, Object> joseHeaderClaimsMap = new HashMap<>();

		try {
			if (headerParts == null || headerParts.length != 3)
				throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_SIGNATURE_MALFORMED,
						ErrorMapKeys.SIGNATURE_MALFORMED));
			joseHeaderJsonString = new String(Base64.getDecoder().decode(headerParts[0]));

			if (NullCheckUtils.isNullOrEmpty(joseHeaderJsonString)) {
				throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_SIGNATURE_MALFORMED,
						ErrorMapKeys.SIGNATURE_MALFORMED));
			}
			joseHeader = (JSONObject) jsonParser.parse(joseHeaderJsonString);

			joseHeaderClaimsMap.put(PSD2Constants.JOSE_HEADER_TYP,
					(String) joseHeader.get(PSD2Constants.JOSE_HEADER_TYP));
			joseHeaderClaimsMap.put(PSD2Constants.JOSE_HEADER_CTY,
					(String) joseHeader.get(PSD2Constants.JOSE_HEADER_CTY));
			joseHeaderClaimsMap.put(PSD2Constants.JOSE_HEADER_KID,
					(String) joseHeader.get(PSD2Constants.JOSE_HEADER_KID));
			joseHeaderClaimsMap.put(PSD2Constants.JOSE_HEADER_ALG,
					(String) joseHeader.get(PSD2Constants.JOSE_HEADER_ALG));
			joseHeaderClaimsMap.put(PSD2Constants.JOSE_HEADER_CRITICAL_ARRAY,
					(List) joseHeader.get(PSD2Constants.JOSE_HEADER_CRITICAL_ARRAY));
			joseHeaderClaimsMap.put(PSD2Constants.JOSE_HEADER_IAT,
					(Long) joseHeader.get(PSD2Constants.JOSE_HEADER_IAT));
			joseHeaderClaimsMap.put(PSD2Constants.JOSE_HEADER_B64,
					(Boolean) joseHeader.get(PSD2Constants.JOSE_HEADER_B64));
			joseHeaderClaimsMap.put(PSD2Constants.JOSE_HEADER_ISSUER,
					(String) joseHeader.get(PSD2Constants.JOSE_HEADER_ISSUER));
			joseHeaderClaimsMap.put(PSD2Constants.JOSE_HEADER_TRUST_ANCHOR,
					(String) joseHeader.get(PSD2Constants.JOSE_HEADER_TRUST_ANCHOR));

		} catch (ParseException | IllegalArgumentException ex) {
			throw PSD2Exception.populatePSD2Exception(
					new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_SIGNATURE_MALFORMED, ErrorMapKeys.SIGNATURE_MALFORMED));
		} catch (ClassCastException e) {
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_SIGNATURE_INVALIDCLAIM,
					ErrorMapKeys.SIGNATURE_INVALID_CLAIMS));
		}

		String headerType = (String) joseHeaderClaimsMap.get(PSD2Constants.JOSE_HEADER_TYP);
		if (!NullCheckUtils.isNullOrEmpty(headerType) && !(headerType.equalsIgnoreCase(PSD2Constants.JOSE))) {
			exceptionData.add(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_SIGNATURE_INVALIDCLAIM,
					ErrorMapKeys.SIGNATURE_HEADERTYPE_INVALID));
		}

		String headerContentType = (String) joseHeaderClaimsMap.get(PSD2Constants.JOSE_HEADER_CTY);
		if (!NullCheckUtils.isNullOrEmpty(headerContentType)
				&& !(headerContentType.equals(MediaType.APPLICATION_JSON_VALUE)
						|| (headerContentType.equals(PSD2Constants.JSON)))) {
			exceptionData.add(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_SIGNATURE_INVALIDCLAIM,
					ErrorMapKeys.SIGNATURE_HEADERCONTENTTYPE_INVALID));
		}

		mandatoryClaimsCheck(joseHeaderClaimsMap);

		if (!supportedAlgo.contains((String) joseHeaderClaimsMap.get(PSD2Constants.JOSE_HEADER_ALG))) {
			exceptionData.add(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_SIGNATURE_INVALIDCLAIM,
					ErrorMapKeys.SIGNATURE_ALGO_INVALID));
		}

		List<String> critArray = (List) joseHeader.get(PSD2Constants.JOSE_HEADER_CRITICAL_ARRAY);

		if (critArray.size() != 4 || (!critData.containsAll(critArray)))
			exceptionData.add(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_SIGNATURE_INVALIDCLAIM,
					ErrorMapKeys.SIGNATURE_CRITARRAY_INVALID));

		NumericDate numericDate = NumericDate
				.fromSeconds((Long) joseHeaderClaimsMap.get(PSD2Constants.JOSE_HEADER_IAT));

		numericDate.addSeconds((long) iatLeeway*-1);
		
		if (!numericDate.isBefore(NumericDate.now()))
			exceptionData.add(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_SIGNATURE_INVALIDCLAIM,
					ErrorMapKeys.SIGNATURE_IAT_INVALID));

		if ((Boolean) joseHeaderClaimsMap.get(PSD2Constants.JOSE_HEADER_B64))
			exceptionData.add(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_SIGNATURE_INVALIDCLAIM,
					ErrorMapKeys.SIGNATURE_B64_INVALID));

		if (null == joseHeaderClaimsMap.get(PSD2Constants.JOSE_HEADER_TRUST_ANCHOR))
			exceptionData.add(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_SIGNATURE_INVALIDCLAIM,
					ErrorMapKeys.SIGNATURE_TRUST_ANCHOR_MISSING));
		else if (!joseHeaderClaimsMap.get(PSD2Constants.JOSE_HEADER_TRUST_ANCHOR).toString().equals(domainName))
			exceptionData.add(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_SIGNATURE_INVALIDCLAIM,
					ErrorMapKeys.SIGNATURE_TRUST_ANCHOR_INVALID));
		
		params.put(PSD2Constants.KID, (String) joseHeaderClaimsMap.get(PSD2Constants.JOSE_HEADER_KID));
		params.put(PSD2Constants.JOSE_HEADER_ISSUER,
				(String) joseHeaderClaimsMap.get(PSD2Constants.JOSE_HEADER_ISSUER));
		params.put(PSD2Constants.JOSE_HEADER_TRUST_ANCHOR,
				(String) joseHeaderClaimsMap.get(PSD2Constants.JOSE_HEADER_TRUST_ANCHOR));
	}

	private void mandatoryClaimsCheck(Map<String, Object> joseHeaderClaimsMap) {
		if (NullCheckUtils.isNullOrEmpty(joseHeaderClaimsMap.get(PSD2Constants.JOSE_HEADER_ALG))) {
			exceptionData.add(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_SIGNATURE_MISSINGCLAIM,
					ErrorMapKeys.SIGNATURE_ALGO_MISSING));
		}

		if (joseHeaderClaimsMap.get(PSD2Constants.JOSE_HEADER_CRITICAL_ARRAY) == null) {
			exceptionData.add(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_SIGNATURE_MISSINGCLAIM,
					ErrorMapKeys.SIGNATURE_CRITARRAY_MISSING));
		}

		if (joseHeaderClaimsMap.get(PSD2Constants.JOSE_HEADER_IAT) == null) {
			exceptionData.add(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_SIGNATURE_MISSINGCLAIM,
					ErrorMapKeys.SIGNATURE_IAT_MISSING));
		}

		if (joseHeaderClaimsMap.get(PSD2Constants.JOSE_HEADER_B64) == null)
			exceptionData.add(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_SIGNATURE_MISSINGCLAIM,
					ErrorMapKeys.SIGNATURE_B64_MISSING));

		if (joseHeaderClaimsMap.get(PSD2Constants.JOSE_HEADER_TRUST_ANCHOR) == null)
			exceptionData.add(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_SIGNATURE_MISSINGCLAIM,
					ErrorMapKeys.SIGNATURE_TRUST_ANCHOR_MISSING));

		if (!exceptionData.isEmpty())
			throw PSD2Exception.populatePSD2Exception(exceptionData.toArray(new ExceptionDTO[exceptionData.size()]));
	}

	private void verifySignature(SignedJWT signedJwt, Map<String, Object> params) {
		Boolean result;
		String subjectInRequest;
		String[] ncaIdReq;
		
		for (Map.Entry<String,Object> entry : params.entrySet())  
		
		if (NullCheckUtils.isNullOrEmpty((String) params.get(PSD2Constants.KID))) {
			exceptionData.add(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_SIGNATURE_MISSINGCLAIM,
					ErrorMapKeys.SIGNATURE_KID_MISSING));
		}

		if (NullCheckUtils.isNullOrEmpty((String) params.get(PSD2Constants.JOSE_HEADER_ISSUER))) {
			exceptionData.add(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_SIGNATURE_MISSINGCLAIM,
					ErrorMapKeys.SIGNATURE_ISSUER_MISSING));
		}

		if (!exceptionData.isEmpty())
			throw PSD2Exception.populatePSD2Exception(exceptionData.toArray(new ExceptionDTO[exceptionData.size()]));

		X509Certificate certificate = certificateGenerator.getSigningCertificate((String) params.get(PSD2Constants.KID),
				(String) params.get(PSD2Constants.CLIENT_ID));
		String subject = certificate.getSubjectDN().toString();
		subject = subject.replaceAll("\\s", "");
		
		if (!subject.contains(PSD2Constants.OID_2_5_4_97)) {
			subjectInRequest = (String) params.get(PSD2Constants.JOSE_HEADER_ISSUER);
			subjectInRequest = subjectInRequest.replaceAll("\\s", "");
			
			if (NullCheckUtils.isNullOrEmpty(subject)
					|| !subject.equals(subjectInRequest)) {
				throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(
						OBErrorCodeEnum.UK_OBIE_SIGNATURE_INVALIDCLAIM, ErrorMapKeys.SIGNATURE_ISSUER_INVALID));
			}
		} else {
			subjectInRequest = (String) params.get(PSD2Constants.JOSE_HEADER_ISSUER);
			String[] ncaIdX5c = Arrays.asList(subject.split(",")).stream()
					.filter(obj -> obj.contains(PSD2Constants.OID_2_5_4_97)).findFirst().get().split("=");
			try {
				ncaIdReq = Arrays.asList(subjectInRequest.split(",")).stream()
						.filter(obj -> obj.contains(PSD2Constants.OID_2_5_4_97)).findFirst().get().split("=");
			} catch (NoSuchElementException e) {
				throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(
						OBErrorCodeEnum.UK_OBIE_SIGNATURE_INVALIDCLAIM, ErrorMapKeys.SIGNATURE_ISSUER_INVALID));
			}

			if (ncaIdX5c.length != 2 && (ncaIdX5c[1].isEmpty() || ncaIdX5c[1] == null)) {
				throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
						"Invalid Certificate Received from Trust Anchor"));
			}

			if (ncaIdReq.length != 2 && (ncaIdReq[1].isEmpty() || ncaIdReq[1] == null)) {
				throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(
						OBErrorCodeEnum.UK_OBIE_SIGNATURE_INVALIDCLAIM, ErrorMapKeys.SIGNATURE_ISSUER_INVALID));
			}
			if (!ncaIdReq[1].trim().equals(ncaIdX5c[1].trim())) {
				throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(
						OBErrorCodeEnum.UK_OBIE_SIGNATURE_INVALIDCLAIM, ErrorMapKeys.SIGNATURE_ISSUER_INVALID));
			}
		}

		PublicKey publicKey = certificate.getPublicKey();
		try {
			result = verifier.verifyJWS(publicKey, signedJwt);
		} catch (JOSEException e) {
			throw PSD2Exception.populatePSD2Exception(
					new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_SIGNATURE_INVALID, ErrorMapKeys.SIGNATURE_INVALID));
		}
		if (!result) {
			throw PSD2Exception.populatePSD2Exception(
					new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_SIGNATURE_INVALID, ErrorMapKeys.SIGNATURE_INVALID));
		}
	}

	private SignedJWT populateSignedJWT(String[] headerParts, String body) {
		Base64URL base64url1 = new Base64URL(headerParts[0].trim());
		Base64URL base64url2 = new Base64URL(body);
		Base64URL base64url3 = new Base64URL(headerParts[2].trim());
		try {
			return new SignedJWT(base64url1, base64url2, base64url3);
		} catch (java.text.ParseException e) {
			throw PSD2Exception.populatePSD2Exception(
					new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_SIGNATURE_MALFORMED, ErrorMapKeys.SIGNATURE_MALFORMED));
		}
	}

}
