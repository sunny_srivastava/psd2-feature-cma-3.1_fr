package com.capgemini.psd2.integration.controller;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.psd2.account.request.mongo.db.adapter.repository.AccountRequestCMA3Repository;
import com.capgemini.psd2.aisp.adapter.AccountRequestAdapter;
import com.capgemini.psd2.aisp.adapter.AispConsentAdapter;
import com.capgemini.psd2.aisp.domain.OBReadConsentResponse1Data;
import com.capgemini.psd2.cisp.adapter.CispConsentAdapter;
import com.capgemini.psd2.cisp.adapter.FundsConfirmationConsentAdapter;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmationConsentResponse1Data;
import com.capgemini.psd2.consent.domain.AispConsent;
import com.capgemini.psd2.consent.domain.CispConsent;
import com.capgemini.psd2.consent.domain.PispConsent;
import com.capgemini.psd2.enums.ConsentStatusEnum;
import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.funds.confirmation.consent.mongo.db.adapter.repository.FundsConfirmationConsentRepository;
import com.capgemini.psd2.logger.LoggerUtils;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.pisp.adapter.PispConsentAdapter;
import com.capgemini.psd2.pisp.domain.LogAttributesPlatformResources;
import com.capgemini.psd2.pisp.domain.OBExternalConsentStatus1Code;
import com.capgemini.psd2.pisp.payment.setup.platform.adapter.PaymentSetupPlatformAdapter;
import com.capgemini.psd2.pisp.sca.consent.operations.adapter.PispScaConsentOperationsAdapter;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageUpdateData;
import com.capgemini.psd2.pisp.status.PaymentConstants;
import com.capgemini.psd2.pisp.status.PaymentStatusEnum;
import com.capgemini.psd2.pisp.utilities.PispUtilities;
import com.capgemini.psd2.product.common.CompatibleVersionList;
import com.capgemini.psd2.utilities.JSONUtilities;

@RestController
public class UpdateIntentController {

	private static final Logger LOG = LoggerFactory.getLogger(UpdateIntentController.class);

	@Autowired
	private LoggerUtils loggerUtils;

	@Autowired
	private AispConsentAdapter aispConsentAdapter;

	@Autowired
	private PaymentSetupPlatformAdapter paymentSetupPlatformAdapter;

	@Autowired
	@Qualifier("pispScaConsentOperationsRoutingAdapter")
	private PispScaConsentOperationsAdapter pispStageOperationsAdapter;

	@Autowired
	private PispConsentAdapter pispConsentAdapter;

	@Autowired
	private CispConsentAdapter cispConsentAdapter;

	@Autowired
	private AccountRequestCMA3Repository accountRequestRepository;

	@Autowired
	private FundsConfirmationConsentRepository fundsConfirmationConsentRepository;

	@Autowired
	@Qualifier("accountRequestRoutingAdapter")
	private AccountRequestAdapter accountRequestAdapter;

	@Autowired
	@Qualifier("fundsConfirmationConsentRoutingAdapter")
	private FundsConfirmationConsentAdapter fundsConfirmationConsentAdapter;

	@Autowired
	private CompatibleVersionList compatibleVersionList;

	@RequestMapping(value = "/updateIntent/{intentId}", method = RequestMethod.PUT)
	public Object updateIndent(@PathVariable String intentId, @RequestHeader String scope) {

		if (scope != null && scope.equalsIgnoreCase(PSD2Constants.OPENID_ACCOUNTS)) {

			return updateAispIntent(intentId);

		} else if (scope != null && scope.equalsIgnoreCase(PSD2Constants.OPENID_PAYMENTS)) {

			return updatePispIntent(intentId);

		} else if (scope != null && scope.equalsIgnoreCase(PSD2Constants.OPENID_FUNDSCONFIRMATIONS)) {

			return updateCispIntent(intentId);

		} else {
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.SCOPE_MISSING_IN_REQUEST);
		}
	}

	public Object updateAispIntent(String intentId) {
		AispConsent aispConsent = null;
		OBReadConsentResponse1Data accountRequestResponse = accountRequestRepository.findByConsentIdAndCmaVersionIn(intentId, compatibleVersionList.fetchVersionList());

		if (accountRequestResponse != null
				&& accountRequestResponse.getStatus().equals(OBReadConsentResponse1Data.StatusEnum.AUTHORISED)) {

			aispConsent = aispConsentAdapter.retrieveConsentByAccountRequestIdAndStatus(intentId,
					ConsentStatusEnum.AUTHORISED);
		} else if (accountRequestResponse != null
				&& accountRequestResponse.getStatus().equals(OBReadConsentResponse1Data.StatusEnum.AWAITINGAUTHORISATION)) {
			aispConsent = aispConsentAdapter.retrieveConsentByAccountRequestIdAndStatus(intentId,
					ConsentStatusEnum.AWAITINGAUTHORISATION);
		}

		if (aispConsent == null) {
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.CONSENT_DATA_NOT_PRESENT);
		}
		if (!aispConsent.getStatus().equals(ConsentStatusEnum.AUTHORISED)) {
			/* Toeken generation call */
			/* Log here for reporting : generated new tokens */
			LOG.info("{\"Enter\":\"{}\",\"{}\"}", "com.capgemini.psd2.integration.controller.updateAispIntent()",
					loggerUtils.populateLoggerData("updateAispIntent"));

			aispConsentAdapter.updateConsentStatusWithResponse(aispConsent.getConsentId(),
					ConsentStatusEnum.AUTHORISED);
			aispConsent.setStatus(ConsentStatusEnum.AUTHORISED);
			accountRequestAdapter.updateAccountRequestResponse(intentId, OBReadConsentResponse1Data.StatusEnum.AUTHORISED);

			LOG.info("{\"Exit\":\"{}\",\"{}\",\"authorisedConsent\":{}}",
					"com.capgemini.psd2.integration.controller.updateAispIntent()",
					loggerUtils.populateLoggerData("updateAispIntent"), JSONUtilities.getJSONOutPutFromObject(aispConsent));

			return aispConsent;
		} else {
			return aispConsent;
		}
	}

	public Object updatePispIntent(String intentId) {

		LOG.info("{\"Enter\":\"{}\",\"{}\"}",
				"com.capgemini.psd2.integration.controller.updatePispIntent()",
				loggerUtils.populateLoggerData("updatePispIntent"));

		//This logger is for the MI Report, do not remove this
		LOG.info("{\"Enter\":\"{}\",\"{}\"}",
				"com.capgemini.psd2.pisp.payment.setup.platform.adapter.impl.updatePaymentSetupPlatformResource()",
				loggerUtils.populateLoggerData("updatePaymentSetupPlatformResource"));

		PispConsent pispConsent = pispConsentAdapter.retrieveConsentByPaymentId(intentId,
				ConsentStatusEnum.AWAITINGAUTHORISATION);

		if (pispConsent == null) {
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.CONSENT_DATA_NOT_PRESENT);
		}

		String channelId = pispConsent.getChannelId();
		String psuId = pispConsent.getPsuId();

		if (channelId == null || channelId.trim().isEmpty() || psuId == null || psuId.trim().isEmpty()) {
			throw PSD2Exception.populatePSD2Exception("Either Psu Id or channel id is not present",
					ErrorCodeEnum.VALIDATION_ERROR);
		}

		Map<String, String> paramsMap = new HashMap<>();
		paramsMap.put(PSD2Constants.USER_IN_REQ_HEADER, psuId);
		paramsMap.put(PSD2Constants.CHANNEL_IN_REQ_HEADER, channelId);
		paramsMap.put(PSD2Constants.PARTY_IDENTIFIER, pispConsent.getPartyIdentifier());

		pispConsentAdapter.updateConsentStatusWithResponse(pispConsent.getConsentId(),
				ConsentStatusEnum.AUTHORISED);
		pispConsent.setStatus(ConsentStatusEnum.AUTHORISED);
		/*
		 * CMA3 Setup status modified from ACCEPTEDCUSTOMERPROFILE to AUTHORISED
		 */
		CustomPaymentStageUpdateData updateData = new CustomPaymentStageUpdateData();
		CustomPaymentStageIdentifiers stageIdentifiers = paymentSetupPlatformAdapter.populateStageIdentifiers(intentId);
		updateData.setSetupStatusUpdated(Boolean.TRUE);
		updateData.setSetupStatus(OBExternalConsentStatus1Code.AUTHORISED.toString());
		updateData.setStatus(OBExternalConsentStatus1Code.AUTHORISED.toString());
		if (stageIdentifiers.getPaymentSetupVersion().equals(PaymentConstants.CMA_FIRST_VERSION)) {
			updateData.setSetupStatus(PaymentStatusEnum.ACCEPTEDCUSTOMERPROFILE.getStatusCode());
		}
		updateData.setSetupStatusUpdateDateTime(PispUtilities.getCurrentDateInISOFormat());

		/* Update platform status to Rejected */
		paymentSetupPlatformAdapter.updatePaymentSetupPlatformResource(intentId, updateData);

		/* Removed call for retrieve payment stage details */
		/* Update payment setup status to Rejected */
		pispStageOperationsAdapter.updatePaymentStageData(stageIdentifiers, updateData, paramsMap);

		LogAttributesPlatformResources updatedConsentPlatformResource = new LogAttributesPlatformResources();

		updatedConsentPlatformResource.setStatus(OBExternalConsentStatus1Code.AUTHORISED.toString());
		updatedConsentPlatformResource.setPaymentConsentId(stageIdentifiers.getPaymentConsentId());
		updatedConsentPlatformResource.setPaymentType(stageIdentifiers.getPaymentTypeEnum());


		//This logger is for the MI Report, do not remove this
		LOG.info("{\"Exit\":\"{}\",\"{}\",\"updatedConsentPlatformResource\":{}}",
				"com.capgemini.psd2.pisp.payment.setup.platform.adapter.impl.updatePaymentSetupPlatformResource()",
				loggerUtils.populateLoggerData("updatedConsentPlatformResource"),
				JSONUtilities.getJSONOutPutFromObject(updatedConsentPlatformResource));


		LOG.info("{\"Exit\":\"{}\",\"{}\",\"updatePispData\":{},\"stageIdentifiers\":{}}",
				"com.capgemini.psd2.integration.controller.updatePispIntent()",
				loggerUtils.populateLoggerData("updatePispIntent"),
				JSONUtilities.getJSONOutPutFromObject(updateData), JSONUtilities.getJSONOutPutFromObject(stageIdentifiers));



		return pispConsent;



	}

	public Object updateCispIntent(String intentId) {
		CispConsent cispConsent = null;
		OBFundsConfirmationConsentResponse1Data fundsConfirmationConsentResponse = fundsConfirmationConsentRepository
				.findByConsentIdAndCmaVersionIn(intentId,compatibleVersionList.fetchVersionList());

		if (fundsConfirmationConsentResponse != null && fundsConfirmationConsentResponse.getStatus()
				.equals(OBFundsConfirmationConsentResponse1Data.StatusEnum.AUTHORISED)) {

			cispConsent = cispConsentAdapter.retrieveConsentByFundsIntentIdAndStatus(intentId,
					ConsentStatusEnum.AUTHORISED);
		} else if (fundsConfirmationConsentResponse != null && fundsConfirmationConsentResponse.getStatus()
				.equals(OBFundsConfirmationConsentResponse1Data.StatusEnum.AWAITINGAUTHORISATION)) {
			cispConsent = cispConsentAdapter.retrieveConsentByFundsIntentIdAndStatus(intentId,
					ConsentStatusEnum.AWAITINGAUTHORISATION);
		}

		if (cispConsent == null) {
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.CONSENT_DATA_NOT_PRESENT);
		}
		if (!cispConsent.getStatus().equals(ConsentStatusEnum.AUTHORISED)) {

			/* Token generation call */
			/* Log here for reporting : generated new tokens */
			LOG.info("{\"Enter\":\"{}\",\"{}\"}", "com.capgemini.psd2.integration.controller.updateCispIntent()",
					loggerUtils.populateLoggerData("updateCispIntent"));

			cispConsentAdapter.updateConsentStatusWithResponse(cispConsent.getConsentId(),
					ConsentStatusEnum.AUTHORISED);
			cispConsent.setStatus(ConsentStatusEnum.AUTHORISED);
			fundsConfirmationConsentAdapter.updateFundsConfirmationConsentResponse(intentId,
					OBFundsConfirmationConsentResponse1Data.StatusEnum.AUTHORISED);

			LOG.info("{\"Exit\":\"{}\",\"{}\",\"authorisedConsent\":{}}",
					"com.capgemini.psd2.integration.controller.updateCispIntent()",
					loggerUtils.populateLoggerData("updateCispIntent"), JSONUtilities.getJSONOutPutFromObject(cispConsent));
			return cispConsent;
		} else {
			return cispConsent;
		}
	}
}