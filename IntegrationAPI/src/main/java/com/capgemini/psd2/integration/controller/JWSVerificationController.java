package com.capgemini.psd2.integration.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.psd2.exceptions.ErrorMapKeys;
import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.integration.service.JWSVerificationService;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.utilities.NullCheckUtils;


@RestController
public class JWSVerificationController {

	@Autowired
	private JWSVerificationService service;
	
	@Autowired
	private RequestHeaderAttributes reqHeaderAttributes;

	@RequestMapping(value="/verifyJWS/{clientId}",method=RequestMethod.POST)
	public void verifyJWS(@RequestBody String requestBody,@PathVariable("clientId") String clientId,
			@RequestHeader("requestMethod") String reqMethod) {
		if(NullCheckUtils.isNullOrEmpty(clientId))
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR, "Required data missing for jws verification"));
		if(NullCheckUtils.isNullOrEmpty(requestBody))
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_RESOURCE_INVALIDFORMAT, "invalid payload found during jws verification"));
		if(reqMethod.equals(PSD2Constants.GET) && reqHeaderAttributes.getJwsHeader().equals(PSD2Constants.NOT_DEFINED))
			return;
		if((reqMethod.equals(PSD2Constants.GET) || reqMethod.equals(HttpMethod.DELETE.toString())) && reqHeaderAttributes.getJwsHeader().length()>=0)
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_SIGNATURE_UNEXPECTED, ErrorMapKeys.SIGNATURE_UNEXPECTED));
		
		service.validateSignature(requestBody,clientId);
		
	}
}
