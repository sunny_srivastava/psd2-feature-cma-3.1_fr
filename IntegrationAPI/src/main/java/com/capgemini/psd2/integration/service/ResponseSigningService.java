package com.capgemini.psd2.integration.service;

import org.springframework.http.MediaType;

public interface ResponseSigningService {
	
	public String signResponse(String responseBody, MediaType applicationJson);
}
