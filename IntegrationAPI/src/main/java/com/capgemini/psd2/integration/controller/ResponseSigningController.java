package com.capgemini.psd2.integration.controller;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.psd2.integration.service.ResponseSigningService;

@RestController
public class ResponseSigningController {
	
	@Autowired
	private ResponseSigningService service;

	@PostMapping(path="/sign-response")
	public String signResponse(@RequestBody String responseBody) {
		return service.signResponse(responseBody, MediaType.APPLICATION_JSON);
	}
	
	@PostMapping("/sign-file")
	public String signFile(@RequestBody byte[] bytesFromFile) {
		return service.signResponse(Arrays.toString(bytesFromFile), MediaType.APPLICATION_PDF);
	}
}
