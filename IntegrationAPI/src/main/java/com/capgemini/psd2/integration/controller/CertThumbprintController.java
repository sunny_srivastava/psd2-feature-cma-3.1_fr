package com.capgemini.psd2.integration.controller;

import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Collections;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.integration.utilities.X509CertificateUtils;

@RestController
public class CertThumbprintController {

	private static final Logger LOG = LoggerFactory.getLogger(CertThumbprintController.class);

	@GetMapping(value="/certificate/thumbprint")
	public Map<String,String> computeThumbprint(@RequestHeader(name="X-SSL-Cert") String xsslcert){
		X509Certificate xCert = null;
		String thumbPrint = null;
		try {
			xCert = X509CertificateUtils.buildX509Certificate(xsslcert);
			thumbPrint  = X509CertificateUtils.extractSha256Thumbprint(xCert);			
		} catch (CertificateException | NoSuchAlgorithmException e) {		
			LOG.info("Exception in computing ThumbPrint: Stack Trace{}", e.getMessage());
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.TECHNICAL_ERROR);
			
		}
		return Collections.singletonMap("thumbprint", thumbPrint);
	}
	
}
