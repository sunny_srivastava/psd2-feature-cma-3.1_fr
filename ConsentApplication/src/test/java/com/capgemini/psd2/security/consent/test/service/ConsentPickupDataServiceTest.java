package com.capgemini.psd2.security.consent.test.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.json.simple.parser.ParseException;
import org.junit.Test;
import org.mockito.Mockito;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.scaconsenthelper.constants.OIDCConstants;
import com.capgemini.psd2.security.consent.service.ConsentPickupDataService;

public class ConsentPickupDataServiceTest {
	
	private ConsentPickupDataService consentPickupDataService = new ConsentPickupDataService();
	
	@Test
	public void testPopulateIntentData() throws ParseException{
		String jsonResponse = "{\"claims\":\"SETUP\",\"scope\":\"openid\",\"username\":\"demo\",\"channel_id\":\"APIChannel\",\"client_id\":\"6443e15975554bce8099e35b88b40465\",\"correlationId\":\"12345\"}";
		assertNotNull(consentPickupDataService.populateIntentData(jsonResponse));
	}
	
	@Test
	public void testNullScopePopulateIntentData() throws ParseException{
		String jsonResponse = "{\"claims\":\"SETUP\",\"scope\":\"\",\"username\":\"demo\",\"channel_id\":\"APIChannel\",\"client_id\":\"6443e15975554bce8099e35b88b40465\",\"correlationId\":\"12345\"}";

 
		JSONParser parser = new JSONParser();
		JSONObject jsonObject = (JSONObject) parser.parse(jsonResponse);
 
		String claims = (String) jsonObject.get(OIDCConstants.CLAIMS);
		String scopes = (String) jsonObject.get(OIDCConstants.SCOPE);
		String userName = (String) jsonObject.get(OIDCConstants.USERNAME);
		String channelId = (String) jsonObject.get(OIDCConstants.CHANNEL_ID);
		String clientId = (String) jsonObject.get(OIDCConstants.CLIENT_ID);
		String correlationId =  (String) jsonObject.get(PSD2Constants.CO_RELATION_ID);
		assertNotNull(claims);
		assertEquals(scopes,"");
		assertNotNull(userName);
		assertNotNull(channelId);
		assertNotNull(clientId);
		assertNotNull(correlationId);
	}
	
	@Test
	public void testNullScopePopulateIntentData2() throws ParseException{
		String jsonResponse = "{\"claims\":\"SETUP\",\"username\":\"demo\",\"channel_id\":\"APIChannel\",\"client_id\":\"6443e15975554bce8099e35b88b40465\",\"correlationId\":\"12345\"}";

 
		JSONParser parser = new JSONParser();
		JSONObject jsonObject = (JSONObject) parser.parse(jsonResponse);
 
		String claims = (String) jsonObject.get(OIDCConstants.CLAIMS);
		String scopes = (String) jsonObject.get(OIDCConstants.SCOPE);
		String userName = (String) jsonObject.get(OIDCConstants.USERNAME);
		String channelId = (String) jsonObject.get(OIDCConstants.CHANNEL_ID);
		String clientId = (String) jsonObject.get(OIDCConstants.CLIENT_ID);
		String correlationId =  (String) jsonObject.get(PSD2Constants.CO_RELATION_ID);
		assertNotNull(claims);
		assertNull(scopes);
		assertNotNull(userName);
		assertNotNull(channelId);
		assertNotNull(clientId);
		assertNotNull(correlationId);
	}
	@Test
	public void testTenantIdPopulateIntentData() throws ParseException{
		String jsonResponse = "{\"claims\":\"SETUP\",\"scope\":\"openid\",\"username\":\"demo\",\"channel_id\":\"APIChannel\",\"client_id\":\"6443e15975554bce8099e35b88b40465\",\"correlationId\":\"12345\",\"tenant_id\":\"674335\"}";
		JSONParser parser = new JSONParser();
		JSONObject jsonObject = (JSONObject) parser.parse(jsonResponse);
		assertNotNull(jsonObject.get("tenant_id"));
		assertNotNull(consentPickupDataService.populateIntentData(jsonResponse).getTenant_id());
		assertNotNull(consentPickupDataService.populateIntentData(jsonResponse));
	}
}
