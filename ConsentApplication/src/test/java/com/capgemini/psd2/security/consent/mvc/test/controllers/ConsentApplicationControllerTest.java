package com.capgemini.psd2.security.consent.mvc.test.controllers;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;

import com.capgemini.psd2.exceptions.ErrorInfo;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.payment.setup.platform.adapter.PaymentSetupPlatformAdapter;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.scaconsenthelper.config.helpers.PispConsentCreationDataHelper;
import com.capgemini.psd2.scaconsenthelper.constants.PFConstants;
import com.capgemini.psd2.scaconsenthelper.constants.SCAConsentHelperConstants;
import com.capgemini.psd2.scaconsenthelper.models.IntentTypeEnum;
import com.capgemini.psd2.scaconsenthelper.models.PickupDataModel;
import com.capgemini.psd2.security.consent.aisp.helpers.AispConsentCreationDataHelper;
import com.capgemini.psd2.security.consent.controllers.ConsentApplicationController;
import com.capgemini.psd2.ui.content.utility.controller.UIStaticContentUtilityController;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
public class ConsentApplicationControllerTest {

	@Mock
	private HttpServletRequest httpServletRequest;

	@Mock
	private HttpServletResponse httpServletResponse;

	@Mock
	private RequestHeaderAttributes requestHeaderAttributes;

	@Mock
	private UIStaticContentUtilityController uiController;

	@Mock
	private AispConsentCreationDataHelper consentCreationDataHelper;

	@Mock
	private PaymentSetupPlatformAdapter paymentSetupPlatformAdapter;

	@Mock
	private PispConsentCreationDataHelper pispConsentCreationDataHelper;
	
	@Mock
	private AispConsentCreationDataHelper aispConsentCreationDataHelper;
	
	@Mock
	private PickupDataModel pickUpDataModel;
	
	@Mock
	private ErrorInfo errorInfoMock;
	
	@Mock 
	private Model model= new  ExtendedModelMap();

	private String applicationName = "PSD2";

	@Spy
	@InjectMocks
	private ConsentApplicationController consentApplicationController = new ConsentApplicationController();

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		ReflectionTestUtils.setField(consentApplicationController, "applicationName", applicationName);
	}

	@Test
	public void testHealthUrl() {
		Model model = mock(Model.class);
		assertNotNull(consentApplicationController.hello(model));
	}

	@Test(expected = PSD2Exception.class)
	public void testHandleSessionTimeOutOnCancel() {
		Model model = mock(Model.class);
		PickupDataModel pickUpData = new PickupDataModel();
		pickUpData.setIntentTypeEnum(IntentTypeEnum.AISP_INTENT_TYPE);
		when(httpServletRequest.getAttribute(SCAConsentHelperConstants.INTENT_DATA)).thenReturn(pickUpData);
		when(httpServletRequest.getAttribute((PSD2Constants.CONSENT_FLOW_TYPE))).thenReturn("AISP");
		assertNotNull(consentApplicationController.handleSessionTimeOutOnCancel(model));
		verify(consentApplicationController,times(1)).error(model);
	}

	@Test
	public void testSessionErrors() {
		Map<String, Object> map = new HashMap<>();
		map.put("demo", new Object());
		assertNotNull(consentApplicationController.viewErrors(map));
	}

	@Test(expected = PSD2Exception.class)
	public void testErrors() {
		Model model = mock(Model.class);

		ErrorInfo errorInfo = new ErrorInfo("100", "Error message", "400");
		PickupDataModel pickUpDataModel = new PickupDataModel();
		pickUpDataModel.setIntentId("123");
		pickUpDataModel.setChannelId("aspsp");
		pickUpDataModel.setClientId("6795ee9ca8e3407694f866725303db37");
		pickUpDataModel.setIntentTypeEnum(IntentTypeEnum.AISP_INTENT_TYPE);

		when(httpServletRequest.getAttribute(SCAConsentHelperConstants.INTENT_DATA)).thenReturn(pickUpDataModel);
		when(httpServletRequest.getAttribute(SCAConsentHelperConstants.EXCEPTION)).thenReturn(errorInfo);
		when(httpServletRequest.getAttribute(PFConstants.RESUME_PATH)).thenReturn("https://pfcluster.webservices.com");
		when(requestHeaderAttributes.getCorrelationId()).thenReturn(null);
		when(httpServletRequest.getParameter(PSD2Constants.CO_RELATION_ID)).thenReturn("12345");
		assertNotNull(consentApplicationController.error(model));
		assertNotNull(consentApplicationController.handleSessionTimeOutOnCancel(model));

	}

	
	@Test(expected = PSD2Exception.class)
	public void testPISPErrors() {
		Model model = mock(Model.class);

		ErrorInfo errorInfo = new ErrorInfo("100", "Error message", "400");
		PickupDataModel pickUpDataModel = new PickupDataModel();
		pickUpDataModel.setIntentId("123");
		pickUpDataModel.setChannelId("aspsp");
		pickUpDataModel.setClientId("6795ee9ca8e3407694f866725303db37");
		pickUpDataModel.setIntentTypeEnum(IntentTypeEnum.PISP_INTENT_TYPE);
		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();
		stageIdentifiers.setPaymentSetupVersion(PSD2Constants.CMA3);
		when(httpServletRequest.getAttribute(SCAConsentHelperConstants.INTENT_DATA)).thenReturn(pickUpDataModel);
		when(httpServletRequest.getAttribute(SCAConsentHelperConstants.EXCEPTION)).thenReturn(errorInfo);
		when(httpServletRequest.getAttribute(PFConstants.RESUME_PATH)).thenReturn("https://pfcluster.webservices.com");
		when(requestHeaderAttributes.getCorrelationId()).thenReturn(null);
		when(httpServletRequest.getParameter(PSD2Constants.CO_RELATION_ID)).thenReturn("12345");

		when(paymentSetupPlatformAdapter.populateStageIdentifiers(pickUpDataModel.getIntentId()))
				.thenReturn(stageIdentifiers);
		assertNotNull(consentApplicationController.error(model));
		assertNotNull(consentApplicationController.handleSessionTimeOutOnCancel(model));
	}

	
	@Test(expected = PSD2Exception.class)
	public void testPISPNullErrors() {
		Model model = mock(Model.class);

		ErrorInfo errorInfo = new ErrorInfo("100", "Error message", "400");
		PickupDataModel pickUpDataModel = new PickupDataModel();
		pickUpDataModel.setIntentId("123");
		pickUpDataModel.setChannelId("aspsp");
		pickUpDataModel.setClientId("6795ee9ca8e3407694f866725303db37");
		pickUpDataModel.setIntentTypeEnum(IntentTypeEnum.PISP_INTENT_TYPE);
		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();
		stageIdentifiers.setPaymentSetupVersion(PSD2Constants.CMA3);
		when(httpServletRequest.getAttribute(SCAConsentHelperConstants.INTENT_DATA)).thenReturn(pickUpDataModel);
		when(httpServletRequest.getAttribute(SCAConsentHelperConstants.EXCEPTION)).thenReturn(errorInfo);
		when(httpServletRequest.getAttribute(PFConstants.RESUME_PATH)).thenReturn(null);
		when(requestHeaderAttributes.getCorrelationId()).thenReturn(null);
		when(httpServletRequest.getParameter(PSD2Constants.CO_RELATION_ID)).thenReturn("12345");

		when(paymentSetupPlatformAdapter.populateStageIdentifiers(pickUpDataModel.getIntentId()))
				.thenReturn(stageIdentifiers);
		assertNotNull(consentApplicationController.error(model));
		assertNotNull(consentApplicationController.handleSessionTimeOutOnCancel(model));
	}

	@Test(expected = PSD2Exception.class)
	public void testErrorNullCondition() {
		Model model = mock(Model.class);
		PickupDataModel pickUpData = new PickupDataModel();
		pickUpData.setIntentTypeEnum(IntentTypeEnum.AISP_INTENT_TYPE);
		when(requestHeaderAttributes.getCorrelationId()).thenReturn("12345");
		when(httpServletRequest.getAttribute(SCAConsentHelperConstants.EXCEPTION)).thenReturn(null);
		when(httpServletRequest.getAttribute(SCAConsentHelperConstants.INTENT_DATA)).thenReturn(pickUpData);
		when(httpServletRequest.getAttribute(PFConstants.RESUME_PATH)).thenReturn(null);
		when(httpServletRequest.getParameter(PFConstants.RESUME_PATH)).thenReturn("https://pfcluster.webservices.com");
		when(httpServletRequest.getParameter(SCAConsentHelperConstants.OAUTH_URL_PARAM)).thenReturn("oAuth.apidev.com");
		assertNotNull(consentApplicationController.error(model));
	}

	@Test(expected = PSD2Exception.class)
	public void testErrorOAuthUrl() {
		Model model = mock(Model.class);
		PickupDataModel pickUpData = new PickupDataModel();
		pickUpData.setIntentTypeEnum(IntentTypeEnum.AISP_INTENT_TYPE);
		when(httpServletRequest.getAttribute(SCAConsentHelperConstants.INTENT_DATA)).thenReturn(pickUpData);
		when(requestHeaderAttributes.getCorrelationId()).thenReturn("12345");
		when(httpServletRequest.getAttribute(SCAConsentHelperConstants.EXCEPTION)).thenReturn(null);
		when(httpServletRequest.getAttribute(PFConstants.RESUME_PATH)).thenReturn(null);
		when(httpServletRequest.getAttribute((PSD2Constants.CONSENT_FLOW_TYPE))).thenReturn("AISP");
		when(httpServletRequest.getParameter(SCAConsentHelperConstants.OAUTH_URL_PARAM)).thenReturn("oAuth.apidev.com");
		assertNotNull(consentApplicationController.error(model));
	}

	@Test(expected = PSD2Exception.class)
	public void handleSessionTimeOutOnCancelTest() {
		Model model = mock(Model.class);
		PickupDataModel pickUpData = new PickupDataModel();
		pickUpData.setIntentTypeEnum(IntentTypeEnum.AISP_INTENT_TYPE);
		when(httpServletRequest.getAttribute(SCAConsentHelperConstants.INTENT_DATA)).thenReturn(pickUpData);
		when(httpServletRequest.getAttribute((PSD2Constants.CONSENT_FLOW_TYPE))).thenReturn("AISP");
		consentApplicationController.handleSessionTimeOutOnCancel(model);
	}
	
	@Test(expected=PSD2Exception.class)
	public void testErrors2(){
		//Model model = new  ExtendedModelMap();
		String intentId ="123";
		String channelId ="BOI";
		String clientId="6795ee9ca8e3407694f866725303db37";
		IntentTypeEnum enumType =IntentTypeEnum.AISP_INTENT_TYPE;
		
		
		pickUpDataModel.setIntentId(intentId);
		
		pickUpDataModel.setChannelId(channelId);
		
		pickUpDataModel.setClientId(clientId);
		
		pickUpDataModel.setIntentTypeEnum(enumType);
		
		when(pickUpDataModel.getClientId()).thenReturn(clientId);
		when(pickUpDataModel.getChannelId()).thenReturn(channelId);
		when(pickUpDataModel.getIntentId()).thenReturn(intentId);
		when(pickUpDataModel.getIntentTypeEnum()).thenReturn(enumType);
		
		httpServletRequest.setAttribute(SCAConsentHelperConstants.INTENT_DATA, pickUpDataModel);
		when(httpServletRequest.getAttribute(SCAConsentHelperConstants.INTENT_DATA)).thenReturn(pickUpDataModel);
		PickupDataModel mdelpick =(PickupDataModel) httpServletRequest.getAttribute(SCAConsentHelperConstants.INTENT_DATA);
		assertNotNull(mdelpick.getIntentId());
		
		
		errorInfoMock.setStatusCode("400");
		errorInfoMock.setErrorCode("230");
		when(errorInfoMock.getStatusCode()).thenReturn("400");
		when(errorInfoMock.getErrorCode()).thenReturn("230");
		httpServletRequest.setAttribute(SCAConsentHelperConstants.EXCEPTION,errorInfoMock);
		when(httpServletRequest.getAttribute(SCAConsentHelperConstants.EXCEPTION)).thenReturn(errorInfoMock);
		ErrorInfo infoEmdl=(ErrorInfo) httpServletRequest.getAttribute(SCAConsentHelperConstants.EXCEPTION);
		assertNotNull(infoEmdl.getStatusCode());
		String resumePathMock ="https://pfcluster.apiboidev.com";
		httpServletRequest.setAttribute(PFConstants.RESUME_PATH,resumePathMock);
		
		
		
		when(httpServletRequest.getAttribute(PFConstants.RESUME_PATH)).thenReturn(resumePathMock);
		when(httpServletRequest.getParameter(anyString())).thenReturn("sdjf");
		when(model.addAttribute(any(),any())).thenReturn(model);
		
       
		when(aispConsentCreationDataHelper.getCMAVersion(anyString())).thenReturn("3.1");
		String version = aispConsentCreationDataHelper.getCMAVersion(intentId);
		assertNotNull(version);
		assertTrue(version.equals("3.1"));
	
		assertNotNull(consentApplicationController.error(model));
		assertNotNull(consentApplicationController.handleSessionTimeOutOnCancel(model));
	}
	
	
	@Test(expected=PSD2Exception.class)
	public void testErrorsEmpty2(){
		//Model model = new  ExtendedModelMap();
		String intentId ="123";
		String channelId ="BOI";
		String clientId="6795ee9ca8e3407694f866725303db37";
		IntentTypeEnum enumType =IntentTypeEnum.AISP_INTENT_TYPE;
		
		
		pickUpDataModel.setIntentId(intentId);
		
		pickUpDataModel.setChannelId(channelId);
		
		pickUpDataModel.setClientId(clientId);
		
		pickUpDataModel.setIntentTypeEnum(enumType);
		
		when(pickUpDataModel.getClientId()).thenReturn(clientId);
		when(pickUpDataModel.getChannelId()).thenReturn(channelId);
		when(pickUpDataModel.getIntentId()).thenReturn(intentId);
		when(pickUpDataModel.getIntentTypeEnum()).thenReturn(enumType);
		
		httpServletRequest.setAttribute(SCAConsentHelperConstants.INTENT_DATA, pickUpDataModel);
		when(httpServletRequest.getAttribute(SCAConsentHelperConstants.INTENT_DATA)).thenReturn(pickUpDataModel);
		PickupDataModel mdelpick =(PickupDataModel) httpServletRequest.getAttribute(SCAConsentHelperConstants.INTENT_DATA);
		assertNotNull(mdelpick.getIntentId());
		
		
		errorInfoMock.setStatusCode("");
		errorInfoMock.setErrorCode("");
		when(errorInfoMock.getStatusCode()).thenReturn("");
		when(errorInfoMock.getErrorCode()).thenReturn("");
		httpServletRequest.setAttribute(SCAConsentHelperConstants.EXCEPTION,errorInfoMock);
		when(httpServletRequest.getAttribute(SCAConsentHelperConstants.EXCEPTION)).thenReturn(errorInfoMock);
		ErrorInfo infoEmdl=(ErrorInfo) httpServletRequest.getAttribute(SCAConsentHelperConstants.EXCEPTION);
		assertNotNull(infoEmdl.getStatusCode());
		String resumePathMock ="";
		httpServletRequest.setAttribute(PFConstants.RESUME_PATH,resumePathMock);
		
		when(requestHeaderAttributes.getCorrelationId()).thenReturn("");
		
		when(httpServletRequest.getAttribute(PFConstants.RESUME_PATH)).thenReturn(resumePathMock);
		when(httpServletRequest.getParameter(anyString())).thenReturn("sdjf");
		when(model.addAttribute(any(),any())).thenReturn(model);
		
       
		when(aispConsentCreationDataHelper.getCMAVersion(anyString())).thenReturn("3.1");
		String version = aispConsentCreationDataHelper.getCMAVersion(intentId);
		assertNotNull(version);
		assertTrue(version.equals("3.1"));
		when(consentApplicationController.error(model)).thenReturn("dsfsdf");
		assertNotNull(consentApplicationController.error(model));
		assertNotNull(consentApplicationController.handleSessionTimeOutOnCancel(model));
		verify(consentApplicationController,times(1)).error(model);
	}
}

