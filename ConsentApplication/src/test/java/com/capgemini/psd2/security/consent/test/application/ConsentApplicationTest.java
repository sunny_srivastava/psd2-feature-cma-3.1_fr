package com.capgemini.psd2.security.consent.test.application;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.security.consent.application.ConsentApplication;

@Configuration
@RunWith(SpringJUnit4ClassRunner.class)
public class ConsentApplicationTest {
	
	@InjectMocks
	ConsentApplication app;
	@Mock
	FilterRegistrationBean filterRegBean;
	@Test
	public void test() {
		assertTrue(true);
	}
	
	@Test
	public void testConsentFilter(){
		
		List<String> urlPatterns = new ArrayList<>();
        urlPatterns.add("/index.jsp");
        urlPatterns.add("/aisp/customerconsentview");
        urlPatterns.add("/aisp/consent");
        urlPatterns.add("/aisp/cancelConsent");
        urlPatterns.add("/aisp/checkSession");
        urlPatterns.add("/pisp/postAuthorizingParty");
        urlPatterns.add("/aisp/consent/*");
        urlPatterns.add("/aisp/createConsent");
        urlPatterns.add("/pisp/customerconsentview");
        urlPatterns.add("/pisp/consent");
        urlPatterns.add("/pisp/cancelConsent");
        urlPatterns.add("/pisp/checkSession");
        urlPatterns.add("/pisp/preAuthorisation");
        urlPatterns.add("/pisp/consent/*");
        urlPatterns.add("/pisp/createConsent");
        
        urlPatterns.add("/cisp/consent/*");
        urlPatterns.add("/cisp/createConsent");
        filterRegBean.setUrlPatterns(urlPatterns);
		assertNotNull(app.consentFilter());
	}
	
	@Test
	public void testCustomeAccountListAdapter(){
		assertNotNull(app.customeAccountListAdapter());
	}
	
	@Test
	public void testRequestContextListener(){
		assertNotNull(app.requestContextListener());
	}
	
	
}
