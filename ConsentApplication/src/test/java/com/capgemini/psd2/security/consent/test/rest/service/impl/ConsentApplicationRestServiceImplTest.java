package com.capgemini.psd2.security.consent.test.rest.service.impl;


import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.aisp.adapter.AispConsentAdapter;
import com.capgemini.psd2.aisp.domain.OBAccount6Account;
import com.capgemini.psd2.cisp.adapter.CispConsentAdapter;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmationConsent1DataDebtorAccount;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmationConsentResponse1;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmationConsentResponse1Data;
import com.capgemini.psd2.consent.domain.AispConsent;
import com.capgemini.psd2.consent.domain.CispConsent;
import com.capgemini.psd2.consent.domain.PSD2Account;
import com.capgemini.psd2.consent.domain.PSD2AccountsAdditionalInfo;
import com.capgemini.psd2.consent.domain.PispConsent;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.integration.adapter.TPPInformationAdaptor;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.adapter.PispConsentAdapter;
import com.capgemini.psd2.pisp.payment.setup.platform.adapter.PaymentSetupPlatformAdapter;
import com.capgemini.psd2.scaconsenthelper.config.helpers.ConsentAuthorizationHelper;
import com.capgemini.psd2.scaconsenthelper.config.helpers.PispConsentCreationDataHelper;
import com.capgemini.psd2.scaconsenthelper.models.IntentTypeEnum;
import com.capgemini.psd2.scaconsenthelper.models.PickupDataModel;
import com.capgemini.psd2.security.consent.aisp.helpers.AispConsentCreationDataHelper;
import com.capgemini.psd2.security.consent.cisp.helpers.CispConsentCreationDataHelper;
import com.capgemini.psd2.security.consent.rest.service.impl.ConsentApplicationRestServiceImpl;
import com.capgemini.psd2.security.consent.test.mock.data.ConsentApplicationMockdata;

public class ConsentApplicationRestServiceImplTest {
	@InjectMocks
	private ConsentApplicationRestServiceImpl service;

	@Mock
	HttpServletRequest request;

	@Mock
	private AispConsentAdapter aispConsentAdapter;

	@Mock
	private PSD2AccountsAdditionalInfo additionalInfo;

	@Mock
	private AispConsentCreationDataHelper aispConsentCreationDataHelper;

	@Mock
	private RequestHeaderAttributes requestHeaderAttributes;

	@Mock
	private TPPInformationAdaptor tppInformationAdaptor;

	@Mock
	private PispConsentAdapter pispConsentAdapter;

	@Mock
	private CispConsentAdapter cispConsentAdapter;

	@Mock
	private PispConsentCreationDataHelper consentCreationDataHelper;

	@Mock
	private ConsentAuthorizationHelper consentAuthorizationHelper;

	@Mock
	private PaymentSetupPlatformAdapter paymentSetupPlatformAdapter;

	@Mock
	CispConsentCreationDataHelper cispConsentCreationDataHelper;

	

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);

	}

	@Test
	public void retrieveConsentAispTest(){
		when(request.getHeader(PSD2Constants.CONSENT_FLAG)).thenReturn("true");
		service.retrieveConsent("123454-tyyrh-56677", "aisp");
	}

	@Test
	public void retrieveConsentPispTest(){
		when(request.getHeader(PSD2Constants.CONSENT_FLAG)).thenReturn("true");
		service.retrieveConsent("123454-tyyrh-56677", "pisp");
	}

	@Test
	public void retrieveConsentCispTest(){
		when(request.getHeader(PSD2Constants.CONSENT_FLAG)).thenReturn("true");
		service.retrieveConsent("123454-tyyrh-56677", "cisp");
	}

	@Test
	public void retrieveConsentAispFlagTest(){
		when(request.getHeader(PSD2Constants.CONSENT_FLAG)).thenReturn("false");
		AispConsent consent = new AispConsent();
		when(aispConsentAdapter.retrieveConsentByAccountRequestId(anyString())).thenReturn(consent);
		service.retrieveConsent("123454-tyyrh-56677", "aisp");
	}

	@Test
	public void retrieveConsentPispFlagTest(){
		when(request.getHeader(PSD2Constants.CONSENT_FLAG)).thenReturn("false");
		PispConsent consent = new PispConsent();
		when(pispConsentAdapter.retrieveConsentByPaymentId(anyString())).thenReturn(consent);
		service.retrieveConsent("123454-tyyrh-56677", "pisp");
	}

	@Test
	public void retrieveConsentCispFlagTest(){
		when(request.getHeader(PSD2Constants.CONSENT_FLAG)).thenReturn("false");
		CispConsent consent = new CispConsent();
		when(cispConsentAdapter.retrieveConsentByFundsIntentId(anyString())).thenReturn(consent);
		service.retrieveConsent("123454-tyyrh-56677", "cisp");
	}

	@Test
	public void createRestConsentAispTest() throws NamingException{
		service.createRestConsent(additionalInfo, "aisp");
	}
	@Test(expected=PSD2Exception.class)
	public void createRestConsentPispNullTest() throws NamingException{
		service.createRestConsent(null, "pisp");
	}

	@Test
	public void createRestConsentPispTest() throws NamingException{
		PSD2AccountsAdditionalInfo additionalInfo = new PSD2AccountsAdditionalInfo();
		additionalInfo.setHeaders("x-fap-financial-id");

		additionalInfo.setAccountdetails(ConsentApplicationMockdata.getPispPSD2AcountList());

		//OBAccount6 value = new OBAccount6();

		PSD2Account account = new PSD2Account();
		Map<String,String> additionalInformation = new HashMap<>();
		additionalInformation.put("PAYER-JURISDICTION", "abc");
		additionalInformation.put("ACCOUNT-NUMBER", "12345");
		additionalInformation.put("ACCOUNT-NSC", "1024");
		additionalInformation.put("IBAN", "FR1420041010050500013M02606");
		additionalInformation.put("BICFI", "12345abcd");
		additionalInformation.put("plApplId", "78");

		PickupDataModel pickupDataModel = new PickupDataModel();
		pickupDataModel.setIntentTypeEnum(IntentTypeEnum.AISP_INTENT_TYPE);
		pickupDataModel.setIntentId("1234");
		account.setAdditionalInformation(additionalInformation);
		List<OBAccount6Account> account1 = new ArrayList<>();
		OBAccount6Account account2 = new OBAccount6Account();
		account2.setName("test");
		account1.add(account2 );

		account.setAccount(account1 );
		when(consentCreationDataHelper.retrieveCustomerAccountListInfo(anyString(), anyString(), anyString(),
				anyString(), anyString(), anyString(), anyString(), anyString(), anyString()))
		.thenReturn(ConsentApplicationMockdata.getPispCustomerAccountInfo());
		when(consentAuthorizationHelper.matchAccountByIdentification(anyObject(), anyObject())).thenReturn(account);
		doNothing().when(consentCreationDataHelper).createConsent(anyObject(), anyString(), anyString(), anyObject(),
				anyString(), anyString(), anyString(), anyObject(), anyString());
		service.createRestConsent(additionalInfo, "pisp");

	}

	@Test(expected=PSD2Exception.class)
	public void createRestConsentCispNullTest() throws NamingException{
		service.createRestConsent(null, "cisp");
	}


	@Test
	public void createRestConsentCispTest() throws NamingException{

		PSD2AccountsAdditionalInfo additionalInfo = new PSD2AccountsAdditionalInfo();

		additionalInfo.setAccountdetails(ConsentApplicationMockdata.getPispPSD2AcountList());

		PickupDataModel pickupDataModel=new PickupDataModel();
		pickupDataModel.setIntentTypeEnum(IntentTypeEnum.CISP_INTENT_TYPE);

		OBFundsConfirmationConsentResponse1 setUpData = new OBFundsConfirmationConsentResponse1();
		OBFundsConfirmationConsentResponse1Data data = new OBFundsConfirmationConsentResponse1Data();
		data.setConsentId("consentId");

		setUpData.setData(data);
		OBFundsConfirmationConsent1DataDebtorAccount debtorAccount = new OBFundsConfirmationConsent1DataDebtorAccount();
		setUpData.getData().setDebtorAccount(debtorAccount );
		setUpData.getData().getDebtorAccount().setIdentification("identification");
		setUpData.getData().getDebtorAccount().setSchemeName("abc" );

		when(request.getAttribute("intentData")).thenReturn(pickupDataModel);
		List<PSD2Account> accountdetails = new ArrayList<>();
		PSD2Account psd2account = new PSD2Account();
		psd2account.accountId("1235");
		List<OBAccount6Account> accountList= new ArrayList<>();
		OBAccount6Account mockData2Account = new OBAccount6Account();
		mockData2Account.setName("account");
		accountList.add(mockData2Account);
		psd2account.account(accountList);
		psd2account.setHashedValue("fd3dfa76050e048e229d35a01da6974ab0edd353520838235d499163d97df209");
		accountdetails.add(psd2account);
		when(cispConsentCreationDataHelper.retrieveFundsConfirmationSetupData(anyString())).thenReturn(setUpData);
		when(consentAuthorizationHelper.matchAccountByIdentification(any(), any())).thenReturn(psd2account);


		service.createRestConsent(additionalInfo,"cisp");
	}

	@Test(expected=PSD2Exception.class)
	public void testCreateConsentModelAndViewForSinglePageConsent() throws Exception {

		ReflectionTestUtils.setField(service, "isSinglePageConsent", Boolean.TRUE);
		PSD2AccountsAdditionalInfo additionalInfo = new PSD2AccountsAdditionalInfo();

		additionalInfo.setAccountdetails(ConsentApplicationMockdata.getPispPSD2AcountList());


		PSD2Account account = new PSD2Account();
		Map<String,String> additionalInformation = new HashMap<>();
		additionalInformation.put("PAYER-JURISDICTION", "abc");
		additionalInformation.put("ACCOUNT-NUMBER", "12345");
		additionalInformation.put("ACCOUNT-NSC", "1024");
		additionalInformation.put("IBAN", "FR1420041010050500013M02606");
		additionalInformation.put("BICFI", "12345abcd");
		additionalInformation.put("plApplId", "78");

		PickupDataModel pickupDataModel = new PickupDataModel();
		pickupDataModel.setIntentTypeEnum(IntentTypeEnum.AISP_INTENT_TYPE);
		pickupDataModel.setIntentId("1234");
		account.setAdditionalInformation(additionalInformation);
		List<OBAccount6Account> account1 = new ArrayList<>();
		OBAccount6Account account2 = new OBAccount6Account();
		account2.setName("test");
		account1.add(account2 );

		account.setAccount(account1 );
		when(consentCreationDataHelper.retrieveCustomerAccountListInfo(anyString(), anyString(), anyString(),
				anyString(), anyString(), anyString(), anyString(), anyString(), anyString()))
		.thenReturn(ConsentApplicationMockdata.getPispCustomerAccountInfo());
		when(consentAuthorizationHelper.matchAccountByIdentification(anyObject(), anyObject())).thenReturn(account);
		doNothing().when(consentCreationDataHelper).createConsent(anyObject(), anyString(), anyString(), anyObject(),
				anyString(), anyString(), anyString(), anyObject(), anyString());
		service.createRestConsent(additionalInfo, "pisp");

	}

	@Test(expected=PSD2Exception.class)
	public void createRestConsentNullScopeTest() throws NamingException{
		service.retrieveConsent(null, null);
	}


}



