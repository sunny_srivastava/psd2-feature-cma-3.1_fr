package com.capgemini.psd2.security.consent.test.mock.data;

import java.util.ArrayList;
import java.util.List;

import com.capgemini.psd2.aisp.domain.OBAccount6;
import com.capgemini.psd2.aisp.domain.OBAccount6Account;
import com.capgemini.psd2.aisp.domain.OBBranchAndFinancialInstitutionIdentification50;
import com.capgemini.psd2.aisp.domain.OBReadAccount6;
import com.capgemini.psd2.aisp.domain.OBReadAccount6Data;
import com.capgemini.psd2.aisp.domain.OBReadConsentResponse1;
import com.capgemini.psd2.aisp.domain.OBReadConsentResponse1Data;
import com.capgemini.psd2.aisp.domain.OBReadConsentResponse1Data.PermissionsEnum;
import com.capgemini.psd2.aisp.domain.OBReadConsentResponse1Data.StatusEnum;
import com.capgemini.psd2.consent.domain.CustomerAccountInfo;
import com.capgemini.psd2.consent.domain.PSD2Account;
import com.capgemini.psd2.pisp.enums.PaymentTypeEnum;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;

public class ConsentApplicationMockdata {

	public static OBReadConsentResponse1 mockAccountRequestPOSTResponse;
	public static List<CustomerAccountInfo> mockCustomerAccountInfoList;
	
	public static OBReadConsentResponse1 getAccountRequestPOSTResponse() {
		mockAccountRequestPOSTResponse = new OBReadConsentResponse1();

		OBReadConsentResponse1Data data = new OBReadConsentResponse1Data();
		List<PermissionsEnum> permissions = new ArrayList<>();
		permissions.add(PermissionsEnum.READACCOUNTSBASIC);
		permissions.add(PermissionsEnum.READACCOUNTSDETAIL);
		permissions.add(PermissionsEnum.READBALANCES);
		data.setPermissions(permissions);
		data.setCreationDateTime("2017-12-25T00:00:00-00:00");
		data.setExpirationDateTime("2017-05-02T00:00:00-00:00");
		data.setTransactionFromDateTime("2017-05-03T00:00:00-00:00");
		data.setTransactionToDateTime("2017-12-03T00:00:00-00:00");
		data.setConsentId("af5b90c1-64b5-4a52-ba55-5eed68b2a269");
		data.setStatus(StatusEnum.AWAITINGAUTHORISATION);
		mockAccountRequestPOSTResponse.setData(data);
		mockAccountRequestPOSTResponse.setRisk(null);
		return mockAccountRequestPOSTResponse;
	}
	
	public static OBReadConsentResponse1 getAccountRequestPOSTResponseInvalidStatus() {
		OBReadConsentResponse1 mockAccountRequestPOSTResponse = new OBReadConsentResponse1();

		OBReadConsentResponse1Data data = new OBReadConsentResponse1Data();
		List<PermissionsEnum> permissions = new ArrayList<>();
		permissions.add(PermissionsEnum.READACCOUNTSBASIC);
		permissions.add(PermissionsEnum.READACCOUNTSDETAIL);
		permissions.add(PermissionsEnum.READBALANCES);
		data.setPermissions(permissions);
		data.setCreationDateTime("2017-12-25T00:00:00-00:00");
		data.setExpirationDateTime("2017-05-02T00:00:00-00:00");
		data.setTransactionFromDateTime("2017-05-03T00:00:00-00:00");
		data.setTransactionToDateTime("2017-12-03T00:00:00-00:00");
		data.setConsentId("af5b90c1-64b5-4a52-ba55-5eed68b2a269");
		data.setStatus(StatusEnum.AUTHORISED);
		mockAccountRequestPOSTResponse.setData(data);
		mockAccountRequestPOSTResponse.setRisk(null);
		return mockAccountRequestPOSTResponse;
	}
	
	public static List<CustomerAccountInfo> getCustomerAccountInfoList(){
		mockCustomerAccountInfoList = new ArrayList<>();
		CustomerAccountInfo customerAccountInfo1 = new CustomerAccountInfo();
		customerAccountInfo1.setAccountName("John Doe");
		customerAccountInfo1.setUserId("1234");
		customerAccountInfo1.setAccountNumber("10203345");
		customerAccountInfo1.setAccountNSC("SC802001");
		customerAccountInfo1.setCurrency("EUR");
		customerAccountInfo1.setNickname("John");
		customerAccountInfo1.setAccountType("checking");

		CustomerAccountInfo customerAccountInfo2 = new CustomerAccountInfo();
		customerAccountInfo2.setAccountName("Tiffany Doe");
		customerAccountInfo2.setUserId("1234");
		customerAccountInfo2.setAccountNumber("10203346");
		customerAccountInfo2.setAccountNSC("SC802002");
		customerAccountInfo2.setCurrency("GRP");
		customerAccountInfo2.setNickname("Tiffany");
		customerAccountInfo2.setAccountType("savings");

		mockCustomerAccountInfoList.add(customerAccountInfo1);
		mockCustomerAccountInfoList.add(customerAccountInfo2);

		return mockCustomerAccountInfoList;
	}

	public static List<PSD2Account> getPispPSD2AcountList() {

		List<PSD2Account> accountdetails = new ArrayList<>();
		PSD2Account account = new PSD2Account();
		account.setAccountId("32118465");
		List<OBAccount6Account> account1 = new ArrayList<>();
		OBAccount6Account ObAccount = new OBAccount6Account();
		ObAccount.setSchemeName("BICFI");
		account1.add(ObAccount );
		account.setAccount(account1 );
		accountdetails.add(account );	
		return accountdetails;
	}

	public static OBReadAccount6 getPispCustomerAccountInfo() {
		OBReadAccount6 mockOBReadAccount3 = new OBReadAccount6();
		List<OBAccount6> accountData = new ArrayList<>();
		PSD2Account acct = new PSD2Account();
		acct.setAccountId("14556236");
		acct.setCurrency("EUR");
		acct.setNickname("John");
		List<OBAccount6Account> accountList = new ArrayList<>();
		OBAccount6Account OBAccount3Account = new OBAccount6Account();
		OBAccount3Account.setIdentification("12345");
		OBAccount3Account.setSchemeName("IBAN");
		accountList.add(OBAccount3Account);
		OBBranchAndFinancialInstitutionIdentification50 servicer = new OBBranchAndFinancialInstitutionIdentification50();
		servicer.setIdentification("12345");

		acct.setAccount(accountList);
		acct.setServicer(servicer);

		PSD2Account accnt = new PSD2Account();
		accnt.setAccountId("14556236");
		accnt.setCurrency("EUR");
		accnt.setNickname("John");
		accnt.setServicer(servicer);
		accnt.setAccount(accountList);
		// accnt.setAccountType("savings");
		accountData.add(acct);
		accountData.add(accnt);
		OBReadAccount6Data data2 = new OBReadAccount6Data();
		data2.setAccount(accountData);
		mockOBReadAccount3.setData(data2);

		return mockOBReadAccount3;
	}

	public static List<PSD2Account> getPSD2AcountList() {
		List<PSD2Account> accountData = new ArrayList<>();
		PSD2Account acct = new PSD2Account();
		acct.setAccountId("14556236");
		acct.setCurrency("EUR");
		acct.setNickname("John");
		// acct.setAccountType("savings");

		List<OBAccount6Account> accountList = new ArrayList<>();
		OBAccount6Account mockData2Account = new OBAccount6Account();
		mockData2Account.setIdentification("1245676");
		mockData2Account.setSchemeName("IBAN");
		accountList.add(mockData2Account);
		OBBranchAndFinancialInstitutionIdentification50 servicer = new OBBranchAndFinancialInstitutionIdentification50();
		servicer.setIdentification("12345");

		acct.setAccount(accountList);
		acct.setServicer(servicer);

		PSD2Account accnt = new PSD2Account();
		accnt.setAccountId("14556236");
		accnt.setCurrency("EUR");
		accnt.setNickname("John");
		accnt.setServicer(servicer);
		accnt.setAccount(accountList);
		// accnt.setAccountType("savings");
		accountData.add(acct);
		accountData.add(accnt);

		return accountData;
	}

	public static List<CustomerAccountInfo> getPispCustomerAccountInfoList() {
		mockCustomerAccountInfoList = new ArrayList<>();
		CustomerAccountInfo customerAccountInfo1 = new CustomerAccountInfo();
		customerAccountInfo1.setAccountName("John Doe");
		customerAccountInfo1.setUserId("1234");
		customerAccountInfo1.setAccountNumber("10203345");
		customerAccountInfo1.setAccountNSC("SC802001");
		customerAccountInfo1.setCurrency("EUR");
		customerAccountInfo1.setNickname("John");
		customerAccountInfo1.setAccountType("checking");

		CustomerAccountInfo customerAccountInfo2 = new CustomerAccountInfo();
		customerAccountInfo2.setAccountName("Tiffany Doe");
		customerAccountInfo2.setUserId("1234");
		customerAccountInfo2.setAccountNumber("10203346");
		customerAccountInfo2.setAccountNSC("SC802002");
		customerAccountInfo2.setCurrency("GRP");
		customerAccountInfo2.setNickname("Tiffany");
		customerAccountInfo2.setAccountType("savings");

		mockCustomerAccountInfoList.add(customerAccountInfo1);
		mockCustomerAccountInfoList.add(customerAccountInfo2);

		return mockCustomerAccountInfoList;
	}

	public static CustomPaymentStageIdentifiers getStageIdentifiers() {
		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();
		stageIdentifiers.setPaymentConsentId("12345");
		stageIdentifiers.setPaymentTypeEnum(PaymentTypeEnum.DOMESTIC_PAY);

		return stageIdentifiers;
	}

}
