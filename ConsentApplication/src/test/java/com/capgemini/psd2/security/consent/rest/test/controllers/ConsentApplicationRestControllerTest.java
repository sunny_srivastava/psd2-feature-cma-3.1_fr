package com.capgemini.psd2.security.consent.rest.test.controllers;

import javax.naming.NamingException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.consent.domain.PSD2AccountsAdditionalInfo;
import com.capgemini.psd2.security.consent.rest.controllers.ConsentApplicationRestController;
import com.capgemini.psd2.security.consent.rest.service.ConsentApplicationRestService;


@RunWith(SpringJUnit4ClassRunner.class)
public class ConsentApplicationRestControllerTest {
	
	@InjectMocks
	private ConsentApplicationRestController consentApplicationRestController;


	@Mock
	private ConsentApplicationRestService service;


	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);

	}
	
	@Test
	public void checkSessionTest(){
		consentApplicationRestController.checkSession(null);
	}

	@Test
	public void consentViewTest(){
		consentApplicationRestController.consentView("121331-etryr-67889","aisp"); 
	}

	@Test
	public void createConsentTest() throws NamingException{
		PSD2AccountsAdditionalInfo additionalInfo  = new PSD2AccountsAdditionalInfo();
		consentApplicationRestController.createConsent(additionalInfo,"aisp"); 
	}
}
