package com.capgemini.psd2.security.consent.rest.service;

import javax.naming.NamingException;

import com.capgemini.psd2.consent.domain.Consent;
import com.capgemini.psd2.consent.domain.PSD2AccountsAdditionalInfo;

public interface ConsentApplicationRestService {

	public Consent retrieveConsent(String consentId, String scope);

	public void createRestConsent(PSD2AccountsAdditionalInfo accountListAdditionalInfo, String scope) throws NamingException;
}
