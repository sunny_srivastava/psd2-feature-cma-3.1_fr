package com.capgemini.psd2.security.consent.rest.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.capgemini.psd2.aisp.adapter.AispConsentAdapter;
import com.capgemini.psd2.aisp.domain.OBAccount6;
import com.capgemini.psd2.aisp.domain.OBReadAccount6;
import com.capgemini.psd2.aisp.domain.OBReadConsentResponse1Data;
import com.capgemini.psd2.cisp.adapter.CispConsentAdapter;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmationConsentResponse1;
import com.capgemini.psd2.consent.domain.Consent;
import com.capgemini.psd2.consent.domain.PSD2Account;
import com.capgemini.psd2.consent.domain.PSD2AccountsAdditionalInfo;
import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.integration.adapter.TPPInformationAdaptor;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.adapter.PispConsentAdapter;
import com.capgemini.psd2.pisp.domain.OBCashAccountDebtor3;
import com.capgemini.psd2.pisp.domain.PaymentConsentsValidationResponse;
import com.capgemini.psd2.pisp.payment.setup.platform.adapter.PaymentSetupPlatformAdapter;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.pisp.stage.domain.CustomPreAuthorizeAdditionalInfo;
import com.capgemini.psd2.scaconsenthelper.config.helpers.ConsentAuthorizationHelper;
import com.capgemini.psd2.scaconsenthelper.config.helpers.PispConsentCreationDataHelper;
import com.capgemini.psd2.scaconsenthelper.models.IntentTypeEnum;
import com.capgemini.psd2.security.consent.aisp.helpers.AispConsentCreationDataHelper;
import com.capgemini.psd2.security.consent.cisp.helpers.CispConsentCreationDataHelper;
import com.capgemini.psd2.security.consent.rest.service.ConsentApplicationRestService;

@Service
public class ConsentApplicationRestServiceImpl implements ConsentApplicationRestService{

	@Value("${app.isSinglePageConsent:#{false}}")
	private boolean isSinglePageConsent;

	@Autowired
	private TPPInformationAdaptor tppInformationAdaptor;

	@Autowired
	private PispConsentAdapter pispConsentAdapter;

	@Autowired
	private AispConsentAdapter aispConsentAdapter;

	@Autowired
	private CispConsentAdapter cispConsentAdapter;

	@Autowired
	private RequestHeaderAttributes requestHeaderAttributes;

	@Autowired
	private HttpServletRequest request;

	@Autowired
	private PispConsentCreationDataHelper pispConsentCreationDataHelper;

	@Autowired
	private AispConsentCreationDataHelper aispConsentCreationDataHelper;

	@Autowired
	@Qualifier("aispConsentAuthorizationHelper")
	private ConsentAuthorizationHelper consentAuthorizationHelper;

	@Autowired
	private PaymentSetupPlatformAdapter paymentSetupPlatformAdapter;

	@Autowired
	private CispConsentCreationDataHelper consentCreationDataHelper;

	@Override
	public Consent retrieveConsent(String consentId, String scope) {
		Consent consent = null;
		if(Boolean.valueOf(request.getHeader(PSD2Constants.CONSENT_FLAG))){
			if(PSD2Constants.PISP_SCOPE.equals(scope)){
				consent =  pispConsentAdapter.retrieveConsent(consentId);
			}
			else if(PSD2Constants.AISP_SCOPE.equals(scope)){
				consent =  aispConsentAdapter.retrieveConsent(consentId);
			}
			else if(PSD2Constants.CISP_SCOPE.equals(scope)){
				consent =  cispConsentAdapter.retrieveConsent(consentId);
			}

		}
		else{
			if(PSD2Constants.PISP_SCOPE.equals(scope)){
				consent =  pispConsentAdapter.retrieveConsentByPaymentId(consentId);
			}
			else if(PSD2Constants.AISP_SCOPE.equals(scope)){
				consent =  aispConsentAdapter.retrieveConsentByAccountRequestId(consentId);
			}
			else if(PSD2Constants.CISP_SCOPE.equals(scope)){
				consent =  cispConsentAdapter.retrieveConsentByFundsIntentId(consentId);
			}
			if(consent == null){
				throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.NO_CONSENT_FOUND);
			}}
		return consent;
	}


	@Override
	public void createRestConsent(PSD2AccountsAdditionalInfo accountListAdditionalInfo, String scope) throws NamingException {
		if(PSD2Constants.AISP_SCOPE.equals(scope))
			aispcreateConsent(accountListAdditionalInfo);

		if(PSD2Constants.PISP_SCOPE.equals(scope))
			pispcreateConsent(accountListAdditionalInfo);

		if(PSD2Constants.CISP_SCOPE.equals(scope))
			cispcreateConsent(accountListAdditionalInfo);

	}	

	public void aispcreateConsent(PSD2AccountsAdditionalInfo accountListAdditionalInfo) throws NamingException {
		List<PSD2Account> customerAccountList;
		OBAccount6 matchedAccount;
		List<OBAccount6> consentCustAcctList = new ArrayList<>();

		String headers = accountListAdditionalInfo.getHeaders();
	
		OBReadAccount6 custAccountList = aispConsentCreationDataHelper.retrieveCustomerAccountListInfo(
				request.getHeader(PSD2Constants.USER_ID), request.getHeader(PSD2Constants.CLIENT_ID),
				IntentTypeEnum.AISP_INTENT_TYPE.getIntentType(), requestHeaderAttributes.getCorrelationId(),
				request.getHeader(PSD2Constants.CHANNEL_ID), request.getHeader(PSD2Constants.TENANT_ID), request.getHeader(PSD2Constants.INTENT_ID), aispConsentCreationDataHelper.getCMAVersion(request.getHeader(PSD2Constants.INTENT_ID)));

		customerAccountList = accountListAdditionalInfo.getAccountdetails();

		for (PSD2Account selectedAccount : customerAccountList) { 
			matchedAccount = consentAuthorizationHelper.matchAccountByIdentification(custAccountList, selectedAccount);
			consentCustAcctList.add(matchedAccount);
		}

		String tppApplicationName = tppInformationAdaptor.fetchApplicationName(request.getHeader(PSD2Constants.CLIENT_ID));
		Object tppInformationObj = tppInformationAdaptor.fetchTPPInformation(request.getHeader(PSD2Constants.CLIENT_ID));

		aispConsentCreationDataHelper.createConsent(consentCustAcctList, request.getHeader(PSD2Constants.USER_ID),
				request.getHeader(PSD2Constants.CLIENT_ID), request.getHeader(PSD2Constants.INTENT_ID), request.getHeader(PSD2Constants.CHANNEL_ID) ,
				headers, tppApplicationName, tppInformationObj, request.getHeader(PSD2Constants.TENANT_ID));

	}
	public void pispcreateConsent(PSD2AccountsAdditionalInfo accountListAdditionalInfo) throws NamingException{
		PSD2Account customerAccount = null;
		String headers = null;
		PSD2Account unmaskedAccount;

		if (accountListAdditionalInfo != null && accountListAdditionalInfo.getHeaders() != null) {
			headers = accountListAdditionalInfo.getHeaders();
		}
		if (accountListAdditionalInfo != null && accountListAdditionalInfo.getAccountdetails() != null) {
			customerAccount = accountListAdditionalInfo.getAccountdetails().get(0);
		}
		if (customerAccount == null) {
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.NO_ACCOUNTS_SELECTED_BY_CUSTOMER);
		}

		//#Stage Identifiers called at the top
		CustomPaymentStageIdentifiers stageIdentifiers = paymentSetupPlatformAdapter
				.populateStageIdentifiers(request.getHeader(PSD2Constants.PAYMENT_ID));

		// Fix Scheme name validation failed. In MongoDB producing exception:
		// can't have. In field names [UK.OBIE.IBAN] while creating test data.
		// Hence, in mock data, we used "_" instead of "." character
		// Replacing "_" to "." to convert valid Scheme Name into mongo db
		// adapter only. there is no impact on CMA api flow. Its specifc to
		// Sandbox functionality

		OBReadAccount6 oBReadAccount = pispConsentCreationDataHelper.retrieveCustomerAccountListInfo(
				request.getHeader(PSD2Constants.USER_ID), request.getHeader(PSD2Constants.CLIENT_ID),
				IntentTypeEnum.PISP_INTENT_TYPE.getIntentType(), requestHeaderAttributes.getCorrelationId(),
				request.getHeader(PSD2Constants.CHANNEL_ID), customerAccount.getAccount().get(0).getSchemeName(),
				requestHeaderAttributes.getTenantId(), request.getHeader(PSD2Constants.INTENT_ID), stageIdentifiers!=null ? stageIdentifiers.getPaymentSetupVersion():null);

		unmaskedAccount = (PSD2Account) consentAuthorizationHelper.matchAccountByIdentification(oBReadAccount,
					customerAccount);
		
		Map<String, String> paramsMap = new HashMap<>();
		paramsMap.put(PSD2Constants.CONSENT_FLOW_TYPE, request.getHeader(PSD2Constants.INTENT_TYPE));
		paramsMap.put(PSD2Constants.CHANNEL_IN_REQ_HEADER, request.getHeader(PSD2Constants.CHANNEL_ID));
		paramsMap.put(PSD2Constants.USER_IN_REQ_HEADER, request.getHeader(PSD2Constants.USER_ID));

		if (isSinglePageConsent) {
			CustomPreAuthorizeAdditionalInfo preAuthAdditionalInfo = populatePreAuthAdditionalInfo(unmaskedAccount);
			OBCashAccountDebtor3 selectedDebtorDetails = pispConsentCreationDataHelper
					.populateSelectedDebtorDetails(unmaskedAccount);
			
			PaymentConsentsValidationResponse preAuthorisationResponse;
			if (selectedDebtorDetails != null) {
				/* Removed call for retrieve payment stage details */
				preAuthorisationResponse = pispConsentCreationDataHelper.validatePreAuthorisation(selectedDebtorDetails,
						preAuthAdditionalInfo, request.getHeader(PSD2Constants.INTENT_ID), paramsMap, stageIdentifiers);
			} else {
				OBCashAccountDebtor3 selectedStandingOrderDebtorDetails = pispConsentCreationDataHelper
						.populateSelectedStandingOrderDebtorDetails(unmaskedAccount);

				preAuthorisationResponse = pispConsentCreationDataHelper.validatePreAuthorisationforStandingOrder(
						selectedStandingOrderDebtorDetails, preAuthAdditionalInfo, request.getHeader(PSD2Constants.INTENT_ID),
						paramsMap, stageIdentifiers);
			}
			if (preAuthorisationResponse == null
					|| OBReadConsentResponse1Data.StatusEnum.fromValue("REJECTED").equals(preAuthorisationResponse.getPaymentSetupValidationStatus())) {
				throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.PRE_AUTHORISATION_FAILED);
			}
		}

		Object tppInformationObj = tppInformationAdaptor.fetchTPPInformation(request.getHeader(PSD2Constants.CLIENT_ID));
		String tppApplicationName = tppInformationAdaptor.fetchApplicationName(request.getHeader(PSD2Constants.CLIENT_ID));
		
		pispConsentCreationDataHelper.createConsent(unmaskedAccount, request.getHeader(PSD2Constants.USER_ID),
				request.getHeader(PSD2Constants.CLIENT_ID), stageIdentifiers, request.getHeader(PSD2Constants.CHANNEL_ID), headers,
				tppApplicationName, tppInformationObj, requestHeaderAttributes.getTenantId());

	}

	public void cispcreateConsent(PSD2AccountsAdditionalInfo accountListAdditionalInfo) throws NamingException {
		OBFundsConfirmationConsentResponse1 setupData;
		PSD2Account customerAccount = null;

		if (accountListAdditionalInfo != null) {
			customerAccount = accountListAdditionalInfo.getAccountdetails().get(0);
		}
		if (customerAccount == null) {
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.NO_ACCOUNTS_SELECTED_BY_CUSTOMER);
		}

			setupData = consentCreationDataHelper
					.retrieveFundsConfirmationSetupData(request.getHeader(PSD2Constants.INTENT_ID));
			OBReadAccount6 oBReadAccount = consentCreationDataHelper.retrieveCustomerAccountListInfo(
					request.getHeader(PSD2Constants.USER_ID), IntentTypeEnum.CISP_INTENT_TYPE.getIntentType(),
					requestHeaderAttributes.getCorrelationId(), request.getHeader(PSD2Constants.CHANNEL_ID),
					setupData.getData().getDebtorAccount().getSchemeName(),
					requestHeaderAttributes.getTenantId(), request.getHeader(PSD2Constants.INTENT_ID));

			PSD2Account unmaskedAccount = (PSD2Account) consentAuthorizationHelper.matchAccountByIdentification(oBReadAccount,
					customerAccount);

			Object tppInformationObj = tppInformationAdaptor.fetchTPPInformation(request.getHeader(PSD2Constants.CLIENT_ID));
			String tppApplicationName = tppInformationAdaptor.fetchApplicationName(request.getHeader(PSD2Constants.CLIENT_ID));
			consentCreationDataHelper.createConsent(unmaskedAccount, request.getHeader(PSD2Constants.USER_ID),
					request.getHeader(PSD2Constants.CLIENT_ID), request.getHeader(PSD2Constants.INTENT_ID), request.getHeader(PSD2Constants.CHANNEL_ID),
					tppApplicationName, tppInformationObj, requestHeaderAttributes.getTenantId());
		

	}

	private CustomPreAuthorizeAdditionalInfo populatePreAuthAdditionalInfo(PSD2Account selectedAccountUnmasked) {
		CustomPreAuthorizeAdditionalInfo additionalAuthInfo = new CustomPreAuthorizeAdditionalInfo();
		Map<String, String> additionalInfo = selectedAccountUnmasked.getAdditionalInformation();

		additionalAuthInfo.setPayerCurrency(selectedAccountUnmasked.getCurrency());
		additionalAuthInfo.setPayerJurisdiction(additionalInfo.get(PSD2Constants.PAYER_JURISDICTION));
		additionalAuthInfo.setAccountNumber(additionalInfo.get(PSD2Constants.ACCOUNT_NUMBER));
		additionalAuthInfo.setAccountNSC(additionalInfo.get(PSD2Constants.ACCOUNT_NSC));	
		additionalAuthInfo.setAccountIban(additionalInfo.get(PSD2Constants.IBAN));
		additionalAuthInfo.setAccountBic(additionalInfo.get(PSD2Constants.BIC));
		additionalAuthInfo.setAccountPan(additionalInfo.get(PSD2Constants.PLAPPLID));
		return additionalAuthInfo;
	}




}