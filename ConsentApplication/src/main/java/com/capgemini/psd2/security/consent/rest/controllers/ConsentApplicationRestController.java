package com.capgemini.psd2.security.consent.rest.controllers;


import javax.naming.NamingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.psd2.consent.domain.Consent;
import com.capgemini.psd2.consent.domain.PSD2AccountsAdditionalInfo;
import com.capgemini.psd2.security.consent.rest.service.ConsentApplicationRestService;

@RestController
public class ConsentApplicationRestController {
	
	@Autowired
	private ConsentApplicationRestService service;


	@RequestMapping("/{intentType}/checkSession")
	public void checkSession(@PathVariable("intentType") String intentType) {
		//Dummy Method to check session.		
	}

	@RequestMapping(value = "/{scope}/consent/{consentId}", method = RequestMethod.GET, produces = {
			MediaType.APPLICATION_JSON_UTF8_VALUE })
	public Consent consentView(@PathVariable("consentId") String consentId, @PathVariable("scope") String scope) {
		return  service.retrieveConsent(consentId, scope);
	}

	@RequestMapping(value = "/{scope}/createConsent", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	public void createConsent(
			@RequestBody PSD2AccountsAdditionalInfo accountListAdditionalInfo, @PathVariable("scope") String scope) throws NamingException {
				service.createRestConsent(accountListAdditionalInfo, scope);
	
	}




}
