/*************************************************************************
* 
 * CAPGEMINI CONFIDENTIAL
* __________________
* 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
* 
 * NOTICE:  All information contained herein is, and remains
* the property of CAPGEMINI GROUP.
* The intellectual and technical concepts contained herein
* are proprietary to CAPGEMINI GROUP and may be covered
* by patents, patents in process, and are protected by trade secret
* or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from CAPGEMINI GROUP.
*/
package com.capgemini.psd2.security.consent.application;

import java.util.ArrayList;
import java.util.List;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.web.context.request.RequestContextListener;

import com.capgemini.psd2.aisp.adapter.CustomerAccountListAdapter;
import com.capgemini.psd2.customer.account.list.routing.adapter.impl.CustomerAccountListRoutingAdapter;
import com.capgemini.psd2.scaconsenthelper.filters.PSD2SecurityFilter;

@SpringBootApplication
@ComponentScan(basePackages = { "com.capgemini.psd2" })
@EnableMongoRepositories(basePackages = { "com.capgemini.psd2" })
@EnableAutoConfiguration
@EnableEurekaClient
@EnableConfigurationProperties
public class ConsentApplication extends SpringBootServletInitializer {

	static ConfigurableApplicationContext context = null;

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(ConsentApplication.class);
	}

	@Bean
	public RequestContextListener requestContextListener() {
		return new RequestContextListener();
	}

	public static void main(String[] args) {
		try {
			context = SpringApplication.run(ConsentApplication.class, args);
		} catch (Exception e) {
			if (context != null) {
				SpringApplication.exit(context, () -> 1);
			}
			System.exit(1);
		}
	}

	@Bean
	public FilterRegistrationBean consentFilter() {
		FilterRegistrationBean filterRegBean = new FilterRegistrationBean();
		filterRegBean.setFilter(buildConsentFilter());
		List<String> urlPatterns = new ArrayList<>();
		urlPatterns.add("/index.jsp");
		urlPatterns.add("/aisp/customerconsentview");
		urlPatterns.add("/aisp/consent");
		urlPatterns.add("/aisp/cancelConsent");
		urlPatterns.add("/aisp/checkSession");
		urlPatterns.add("/pisp/postAuthorizingParty");
		urlPatterns.add("/aisp/consent/*");
		urlPatterns.add("/aisp/createConsent");
		urlPatterns.add("/pisp/customerconsentview");
		urlPatterns.add("/pisp/consent");
		urlPatterns.add("/pisp/cancelConsent");
		urlPatterns.add("/pisp/checkSession");
		urlPatterns.add("/pisp/preAuthorisation");
		urlPatterns.add("/pisp/consent/*");
		urlPatterns.add("/pisp/createConsent");
		
		urlPatterns.add("/cisp/consent/*");
		urlPatterns.add("/cisp/createConsent");
		filterRegBean.setUrlPatterns(urlPatterns);
		return filterRegBean;
	}

	@Bean
	public PSD2SecurityFilter buildConsentFilter() {
		return new PSD2SecurityFilter();
	}

	@Bean(name = "CustomerAccountListAdapter")
	public CustomerAccountListAdapter customeAccountListAdapter() {
		return new CustomerAccountListRoutingAdapter();
	}
}
