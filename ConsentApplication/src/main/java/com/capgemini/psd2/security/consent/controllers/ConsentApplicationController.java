package com.capgemini.psd2.security.consent.controllers;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.capgemini.psd2.exceptions.ErrorInfo;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.payment.setup.platform.adapter.PaymentSetupPlatformAdapter;
import com.capgemini.psd2.pisp.stage.domain.CustomConsentAppViewData;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.scaconsenthelper.config.CdnConfig;
import com.capgemini.psd2.scaconsenthelper.config.helpers.PispConsentCreationDataHelper;
import com.capgemini.psd2.scaconsenthelper.constants.PFConstants;
import com.capgemini.psd2.scaconsenthelper.constants.SCAConsentHelperConstants;
import com.capgemini.psd2.scaconsenthelper.models.IntentTypeEnum;
import com.capgemini.psd2.scaconsenthelper.models.PickupDataModel;
import com.capgemini.psd2.security.consent.aisp.helpers.AispConsentCreationDataHelper;
import com.capgemini.psd2.security.constants.PSD2SecurityConstants;
import com.capgemini.psd2.ui.content.utility.controller.UIStaticContentUtilityController;
import com.capgemini.psd2.utilities.JSONUtilities;
import com.capgemini.psd2.utilities.ValidationUtility;

@Controller
public class ConsentApplicationController {

	@Autowired
	private UIStaticContentUtilityController uiController;
	
	@Autowired
	private HttpServletRequest request;
	
	@Autowired
	private RequestHeaderAttributes requestHeaderAttributes;
	
	@Autowired
	private AispConsentCreationDataHelper aispConsentCreationDataHelper;
	
	@Autowired
	private PispConsentCreationDataHelper pispConsentCreationDataHelper;
	
	@Autowired
	private PaymentSetupPlatformAdapter paymentSetupPlatformAdapter;		
	
	@Value("${spring.application.name}")
	private String applicationName;


	@RequestMapping("/healthjsp")
	public String hello(Model model) {
		return "health";
	}
	
	
	@RequestMapping(value = "/errors", method = RequestMethod.PUT)
	@ResponseBody
	public String handleSessionTimeOutOnCancel(Model model) {
		return error(model);
	}
	
	@RequestMapping("/sessionerrors")
	public String viewErrors(Map<String, Object> model){
		model.putAll(CdnConfig.populateCdnAttributes());
		model.put(PSD2SecurityConstants.LOGO_URL,uiController.retrieveUiParamValue(PSD2SecurityConstants.CONSENT_LOGO_URL_PATH));
		model.put(PSD2SecurityConstants.ERROR_GENERIC_MSG,uiController.retrieveUiParamValue(PSD2SecurityConstants.GENERIC_ERROR_PATH));
	    model.put(PSD2SecurityConstants.ERROR_GENERIC_DESCRIPTION,uiController.retrieveUiParamValue(PSD2SecurityConstants.GENERIC_ERROR_DESCRIPTION_PATH));
		return "viewerrors";
	}
	
	@RequestMapping("/errors")
	@ResponseBody
	public String error(Model model) {

		String statusCode = null;
		
		PickupDataModel pickUpData = (PickupDataModel)request.getAttribute(SCAConsentHelperConstants.INTENT_DATA);
		
		ErrorInfo errorInfo = (ErrorInfo) request.getAttribute(SCAConsentHelperConstants.EXCEPTION);

		// Resume path will come from reqeust attribute only if there is a case of session timeout
		String resumePath = (String) request.getAttribute(PFConstants.RESUME_PATH);

		boolean serverError = Boolean.FALSE;

		if (resumePath == null || resumePath.isEmpty()) {
			if (request.getParameter(PFConstants.RESUME_PATH) != null && !request.getParameter(PFConstants.RESUME_PATH).isEmpty()) {
				resumePath = request.getParameter(PFConstants.RESUME_PATH);
			} else if (request.getParameter(SCAConsentHelperConstants.OAUTH_URL_PARAM) != null && !request.getParameter(SCAConsentHelperConstants.OAUTH_URL_PARAM).isEmpty()) {
				resumePath = request.getParameter(SCAConsentHelperConstants.OAUTH_URL_PARAM);
			}
		}
		
		String currentCorrId =  requestHeaderAttributes.getCorrelationId();
		
		if(currentCorrId == null || currentCorrId.isEmpty()){
			currentCorrId = request.getParameter(PSD2Constants.CO_RELATION_ID);
			ValidationUtility.isValidUUID(currentCorrId);
		}
				
		if (errorInfo != null) {
			model.addAttribute(SCAConsentHelperConstants.EXCEPTION, errorInfo);
			errorInfo.setDetailErrorMessage(null);
			statusCode = errorInfo.getStatusCode();
		}

		if (statusCode != null && !statusCode.isEmpty()) {
			serverError = statusCode.equalsIgnoreCase(Integer.toString(HttpStatus.INTERNAL_SERVER_ERROR.value()))
					|| Integer.parseInt(statusCode) > HttpStatus.INTERNAL_SERVER_ERROR.value();
		}
				
		model.addAttribute(PSD2Constants.SERVER_ERROR_FLAG_ATTR, serverError);
		
		model.addAttribute(PSD2SecurityConstants.REDIRECT_URI_MODEL_ATTRIBUTE, resumePath);

		model.addAttribute(PSD2Constants.JS_MSG,
				uiController.retrieveUiParamValue(PSD2Constants.ERROR_MESSAGE, PSD2Constants.JS_MSG));

		if(pickUpData != null) {
		
			model.addAttribute(PSD2Constants.CONSENT_FLOW_TYPE, pickUpData.getIntentTypeEnum().getIntentType());
		}
		else if(request.getAttribute(PSD2Constants.CONSENT_FLOW_TYPE) != null){
			model.addAttribute(PSD2Constants.CONSENT_FLOW_TYPE, request.getAttribute(PSD2Constants.CONSENT_FLOW_TYPE));
		}
		model.addAttribute(PSD2Constants.APPLICATION_NAME, applicationName);
		model.addAttribute(PSD2Constants.CO_RELATION_ID, currentCorrId);
		
		if (pickUpData != null && IntentTypeEnum.AISP_INTENT_TYPE.getIntentType().equals(pickUpData.getIntentTypeEnum().getIntentType())) {
			model.addAttribute(PSD2Constants.CMAVERSION,
					aispConsentCreationDataHelper.getCMAVersion(pickUpData.getIntentId()));

		}

		if (pickUpData != null && IntentTypeEnum.PISP_INTENT_TYPE.getIntentType().equals(pickUpData.getIntentTypeEnum().getIntentType())) {
			CustomPaymentStageIdentifiers stageIdentifiers = paymentSetupPlatformAdapter
					.populateStageIdentifiers(pickUpData.getIntentId());
			model.addAttribute(PSD2Constants.CMAVERSION, stageIdentifiers.getPaymentSetupVersion());

			CustomConsentAppViewData consentAppPaymentSetupData = pispConsentCreationDataHelper
					.retrieveConsentAppStagedViewData(pickUpData.getIntentId(), stageIdentifiers);
			model.addAttribute(PSD2Constants.PAYMENT_CONSENT_DATA,
					JSONUtilities.getJSONOutPutFromObject(consentAppPaymentSetupData));
		}
		
		return JSONUtilities.getJSONOutPutFromObject(model);
	}
}
