package com.capgemini.psd2.tppinformation.adaptor.ldap.constants;

public class TPPInformationConstants {
	public static final String UNIQUE_MEMBER = "uniqueMember";
	public static final String CN = "cn";
	public static final String LEGAL_ENTITY_NAME = "o";
	public static final String X_ROLES = "x-roles";
	public static final String X_BLOCK = "x-block";
	public static final String PUBLIC_KEY = "publicKey";
	public static final String SOFTWARE_JWKS_ENDPOINT = "softwareJwksEndpoint";
	public static final String SSA_TOKEN = "ssaToken";
	public static final String SSA_JWKS_ENDPOINT = "software_jwks_endpoint";
	public static final String SOFTWARE_ON_BEHALF_OF_ORG = "software_on_behalf_of_org";
}
