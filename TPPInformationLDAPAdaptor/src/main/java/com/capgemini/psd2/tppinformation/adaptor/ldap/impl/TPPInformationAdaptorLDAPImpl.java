package com.capgemini.psd2.tppinformation.adaptor.ldap.impl;

import java.util.Arrays;
import java.util.Base64;
import java.util.List;
import java.util.Map;

import javax.naming.NamingException;
import javax.naming.directory.Attributes;
import javax.naming.directory.BasicAttributes;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.ldap.core.AttributesMapper;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.query.LdapQuery;
import org.springframework.ldap.query.LdapQueryBuilder;
import org.springframework.security.jwt.Jwt;
import org.springframework.security.jwt.JwtHelper;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.integration.adapter.TPPInformationAdaptor;
import com.capgemini.psd2.logger.LoggerUtils;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.RestClientSync;
import com.capgemini.psd2.tppinformation.adaptor.ldap.config.LdapConfiguration;
import com.capgemini.psd2.tppinformation.adaptor.ldap.constants.TPPInformationConstants;
import com.capgemini.psd2.tppinformation.adaptor.ldap.model.ClientModel;
import com.capgemini.psd2.tppinformation.adaptor.pf.config.PfConfiguration;
import com.capgemini.psd2.utilities.GenerateUniqueIdUtilities;
import com.capgemini.psd2.utilities.JSONUtilities;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Component
public class TPPInformationAdaptorLDAPImpl implements TPPInformationAdaptor {

	private static final Logger LOG = LoggerFactory.getLogger(TPPInformationAdaptorLDAPImpl.class);

	@Autowired
	private LoggerUtils loggerUtils;

	@Autowired
	private LdapConfiguration ldapConfiguration;

	@Autowired
	private PfConfiguration pfConfiguration;

	@Autowired
	private RequestHeaderAttributes requestHeaderAttributes;

	@Autowired
	private LdapTemplate ldapTemplate;

	@Autowired
	private RestClientSync restClientSync;

	@Value("${pf.clientByIdUrl:null}")
	private String clientByIdUrl;

	// Modified fetching based on tenant
	@Override
	public Object fetchTPPInformation(String clientId) {
		LOG.info("{\"Enter\":\"{}\",\"{}\"}",
				"com.capgemini.psd2.tppinformation.adaptor.ldap.impl.fetchTPPInformation()",
				loggerUtils.populateLoggerData("fetchTppInfo"));

		String tppUniqueMember = null;

		List<String> uniqueMemberString = null;

		// ou=clientAppMapping,ou=groups,dc=boi,dc=co.uk,dc=capgeminibank,dc=com
		LdapQuery ldapQuery = LdapQueryBuilder.query().attributes("*", "+")
				.base(ldapConfiguration.getTenantSpecificClientGroupBaseDn(requestHeaderAttributes.getTenantId()))
				.filter("(cn=" + clientId + ")");

		List<Object> tppAppDetails = ldapTemplate.search(ldapQuery, new AttributesMapper<Object>() {
			@Override
			public Object mapFromAttributes(Attributes attrs) throws NamingException {
				return attrs;
			}
		});
		if (tppAppDetails == null || tppAppDetails.isEmpty()) {
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.NO_CLIENT_APP_DATA_FOUND);
		}

		uniqueMemberString = TPPInformationAdaptorLDAPImpl.getBasicAttributes((BasicAttributes) tppAppDetails.get(0),
				TPPInformationConstants.UNIQUE_MEMBER);

		for (String uniqueMemberDn : uniqueMemberString) {
			if (uniqueMemberDn.contains(
					ldapConfiguration.getTenantSpecificTppGroupBaseDn(requestHeaderAttributes.getTenantId()))) {
				tppUniqueMember = uniqueMemberDn;
				break;
			}
		}

		Object tppDetails = ldapTemplate.lookup(tppUniqueMember, new AttributesMapper<Object>() {
			@Override
			public Object mapFromAttributes(Attributes attrs) throws NamingException {
				return attrs;
			}
		});

		if (tppDetails == null) {
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.NO_TPP_DATA_AVAILABLE);
		}

		LOG.info("{\"Exit\":\"{}\"}", "com.capgemini.psd2.tppinformation.adaptor.ldap.impl.fetchTPPInformation()",
				loggerUtils.populateLoggerData("fetchTppInfo"));
		return tppDetails;

	}

	public static List<String> getBasicAttributes(BasicAttributes object, String ldapAttr) {
		String[] str;
		List<String> retrunValue = null;
		try {
			if (object.get(ldapAttr) != null && object.get(ldapAttr).get() != null) {
				str = object.get(ldapAttr).toString().split(":");
				retrunValue = Arrays.asList(str[1].split(", "));
			}
		} catch (NamingException e) {
			throw PSD2Exception.populatePSD2Exception(e.getMessage(), ErrorCodeEnum.NO_TPP_DATA_AVAILABLE);
		}
		return retrunValue;
	}

	// Modified fetching based on tenant
	@Override
	public String fetchApplicationName(String clientId) {
		LOG.info("{\"Enter\":\"{}\",\"{}\"}",
				"com.capgemini.psd2.tppinformation.adaptor.ldap.impl.fetchApplicationName()",
				loggerUtils.populateLoggerData("fetchPfApplicationName"));

		RequestInfo requestInfo = new RequestInfo();
		String pfOAuthClientByIdUrl = pfConfiguration.getTenantSpecificPrfix(requestHeaderAttributes.getTenantId())
				+ clientByIdUrl;
		pfOAuthClientByIdUrl = pfOAuthClientByIdUrl.replaceAll("\\{clientId\\}", clientId);
		requestInfo.setUrl(pfOAuthClientByIdUrl);
		HttpHeaders httpHeaders = populatePFHttpHeaders();
		ClientModel model = restClientSync.callForGet(requestInfo, ClientModel.class, httpHeaders);

		LOG.info("{\"Exit\":\"{}\"}", "com.capgemini.psd2.tppinformation.adaptor.ldap.impl.fetchApplicationName()",
				loggerUtils.populateLoggerData("fetchPfApplicationName"));
		return model.getName();
	}

	private HttpHeaders populatePFHttpHeaders() {
		HttpHeaders requestHeaders = new HttpHeaders();
		requestHeaders.setContentType(MediaType.APPLICATION_JSON);
		requestHeaders.add("X-XSRF-Header", GenerateUniqueIdUtilities.generateRandomUniqueID());
		requestHeaders.add("Authorization", "Basic " + encodeCredentials());
		return requestHeaders;
	}

	// Modified credentials based on tenant
	public String encodeCredentials() {
		String credentials = pfConfiguration.getTenantSpecificAdminUser(requestHeaderAttributes.getTenantId())
				.concat(":").concat(pfConfiguration.getTenantSpecificAdminPwd(requestHeaderAttributes.getTenantId()));
		return Base64.getEncoder().encodeToString(credentials.getBytes());
	}

	// Modified fetching based on tenant
	public List<Object> fetchTppAppMappingForClient(String clientId) {
		LOG.info("{\"Enter\":\"{}\",\"{}\"}",
				"com.capgemini.psd2.tppinformation.adaptor.ldap.impl.fetchTppAppMappingForClient()",
				loggerUtils.populateLoggerData("fetchTppApp"));

		LdapQuery ldapQuery = LdapQueryBuilder.query().attributes("*", "+")
				.base(ldapConfiguration.getTenantSpecificClientGroupBaseDn(requestHeaderAttributes.getTenantId()))
				.filter("(cn=" + clientId + ")");

		List<Object> tppAppDetails = ldapTemplate.search(ldapQuery, new AttributesMapper<Object>() {
			@Override
			public Object mapFromAttributes(Attributes attrs) throws NamingException {
				return attrs;
			}
		});
		if (tppAppDetails == null || tppAppDetails.isEmpty()) {
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.NO_CLIENT_APP_DATA_FOUND);
		}

		LOG.info("{\"Exit\":\"{}\"}",
				"com.capgemini.psd2.tppinformation.adaptor.ldap.impl.fetchTppAppMappingForClient()",
				loggerUtils.populateLoggerData("fetchTppApp"));
		return tppAppDetails;
	}

	// Fetching software_on_behalf_of_org from SSA
	@Override
	public String fetchSoftwareOnBehalfOfOrg(String clientId) {
		LOG.info("Entering fetchSoftwareOnBehalfOfOrg(). Client ID: "+clientId);
		String softwareOnBehalfOfOrg=null;
		if(clientId!=null && !clientId.isEmpty()) {
			LOG.info("Before call to fetchTppAppMappingForClient");
			List<Object> listObj=fetchTppAppMappingForClient(clientId);
			LOG.info("After call to fetchTppAppMappingForClient");
			BasicAttributes object = (BasicAttributes) listObj.get(0);
			try {
				softwareOnBehalfOfOrg= getAttributeValue(object, TPPInformationConstants.SOFTWARE_ON_BEHALF_OF_ORG);
				if(NullCheckUtils.isNullOrEmpty(softwareOnBehalfOfOrg)) {
					String ssaToken=getAttributeValue(object, TPPInformationConstants.SSA_TOKEN);
					Jwt byRefJwt = JwtHelper.decode(ssaToken);
					softwareOnBehalfOfOrg=(String)JSONUtilities.getObjectFromJSONString(byRefJwt.getClaims(), Map.class).get(TPPInformationConstants.SOFTWARE_ON_BEHALF_OF_ORG);
				}
				
			} catch (NamingException e) {
				throw PSD2Exception.populatePSD2Exception(e.getMessage(), ErrorCodeEnum.NO_CLIENT_APP_DATA_FOUND);
			}
		}
		LOG.info("Exiting fetchSoftwareOnBehalfOfOrg()");
		return softwareOnBehalfOfOrg;
	}
	
	private String getAttributeValue(BasicAttributes tppApplication, String ldapAttr) throws NamingException {
		String attributeValue = null;
		if (tppApplication.get(ldapAttr) != null && tppApplication.get(ldapAttr).get() != null) {
			attributeValue = tppApplication.get(ldapAttr).get().toString();
		}
		return attributeValue;
	}
	
	
	
}
