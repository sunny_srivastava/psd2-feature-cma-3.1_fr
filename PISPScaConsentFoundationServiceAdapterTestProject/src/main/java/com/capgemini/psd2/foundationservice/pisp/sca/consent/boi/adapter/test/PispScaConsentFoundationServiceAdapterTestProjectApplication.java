package com.capgemini.psd2.foundationservice.pisp.sca.consent.boi.adapter.test;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.psd2.adapter.fraudnet.domain.DecisionType;
import com.capgemini.psd2.adapter.fraudnet.domain.FraudServiceResponse;
import com.capgemini.psd2.foundationservice.pisp.sca.consent.boi.adapter.PispScaConsentFoundationServiceAdapter;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.pisp.domain.OBCashAccountDebtor3;
import com.capgemini.psd2.pisp.domain.PaymentConsentsValidationResponse;
import com.capgemini.psd2.pisp.enums.PaymentTypeEnum;
import com.capgemini.psd2.pisp.stage.domain.CustomConsentAppViewData;
import com.capgemini.psd2.pisp.stage.domain.CustomFraudSystemPaymentData;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageUpdateData;
import com.capgemini.psd2.pisp.stage.domain.CustomPreAuthorizeAdditionalInfo;

@SpringBootApplication
@ComponentScan("com.capgemini.psd2")
public class PispScaConsentFoundationServiceAdapterTestProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(PispScaConsentFoundationServiceAdapterTestProjectApplication.class, args);
	}
}




@RestController
@ResponseBody
class TestPispScaConsentController
{
	@Autowired
	private PispScaConsentFoundationServiceAdapter pispScaConsentFoundationServiceAdapter;
	
	
	@RequestMapping("/testPispScaConsentController")
	public CustomConsentAppViewData getData()
	{
		CustomPaymentStageIdentifiers customPaymentStageIdentifiers=new CustomPaymentStageIdentifiers();
		customPaymentStageIdentifiers.setPaymentConsentId("9598e2fb-4a1f-4f0a-98bb-6dea27c23407");
		customPaymentStageIdentifiers.setPaymentSetupVersion("v2.0");
		customPaymentStageIdentifiers.setPaymentTypeEnum(PaymentTypeEnum.DOMESTIC_PAY);
		
		Map<String,String> params=new HashMap<String ,String>();
		 params.put("sourceSystemReqHeader", "PSD2API");
		    params.put("correlationMuleReqHeader", "2345678652002");
		    params.put("X-API-TRANSACTION-ID", "BOI2002");
		    params.put("partysourceReqHeader", "platform");
		    params.put("sourceUserReqHeader", "87654321");
		    params.put("apiChannelCode","BOI");
		    params.put("tenant_id", "BOIUK");
		return pispScaConsentFoundationServiceAdapter.retrieveConsentAppStagedViewData( customPaymentStageIdentifiers, params);
		
	}
	
	@RequestMapping("/testPispScaConsentControllerForScheduled")
	public CustomConsentAppViewData getDataScheduled()
	{
		CustomPaymentStageIdentifiers customPaymentStageIdentifiers=new CustomPaymentStageIdentifiers();
		customPaymentStageIdentifiers.setPaymentConsentId("5df87a0e-2273-469f-8f52-2b32de11f4b6");
		customPaymentStageIdentifiers.setPaymentSetupVersion("v2.0");
		customPaymentStageIdentifiers.setPaymentTypeEnum(PaymentTypeEnum.DOMESTIC_SCH_PAY);
		
		Map<String,String> params=new HashMap<String ,String>();
		params.put("sourceSystemReqHeader", "PSD2API");
	    params.put("correlationMuleReqHeader", "");
	    params.put("transactioReqHeader", "BOI999");
	    params.put("partysourceReqHeader", "platform");
	    params.put("sourceUserReqHeader", "87654321");
	    params.put("X-BOI-CHANNEL","BOL");
	    params.put("tenant_id", "BOIUK");
		return pispScaConsentFoundationServiceAdapter.retrieveConsentAppStagedViewData( customPaymentStageIdentifiers, params);
		
	}
	
	@RequestMapping("/testValidatePispScaConsentController")
	public PaymentConsentsValidationResponse validateData()
	{	
		
		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();
		stageIdentifiers.setPaymentConsentId("9598e2fb-4a1f-4f0a-98bb-6dea27c23407");
		stageIdentifiers.setPaymentSetupVersion("v2.0");
		stageIdentifiers.setPaymentSubmissionId("12345");
		stageIdentifiers.setPaymentTypeEnum(PaymentTypeEnum.DOMESTIC_PAY);
		
		Map<String,String> params=new HashMap<String ,String>();
		 params.put("sourceSystemReqHeader", "PSD2API");
		    params.put("correlationMuleReqHeader", "");
		    params.put("transactioReqHeader", "BOI999");
		    params.put("partysourceReqHeader", "platform");
		    params.put("sourceUserReqHeader", "87654321");
		    params.put("X-BOI-CHANNEL","BOL");
		    params.put("tenant_id", "BOIUK");
		   OBCashAccountDebtor3 selectedDebtorDetails = new OBCashAccountDebtor3();
		   selectedDebtorDetails.setIdentification("validatorIdentification");
		   selectedDebtorDetails.setName("validatorName");
		   selectedDebtorDetails.setSchemeName("validatorSchemeName");
		   selectedDebtorDetails.setSecondaryIdentification("validatorSecondaryIdentification");
		   
		   CustomPreAuthorizeAdditionalInfo preAuthAdditionalInfo = new CustomPreAuthorizeAdditionalInfo();
		   preAuthAdditionalInfo.setAccountBic("02113sds");
		   preAuthAdditionalInfo.setAccountIban("123abc");
		   preAuthAdditionalInfo.setAccountNSC("abc123");
		   preAuthAdditionalInfo.setAccountNumber("abc123");
		   preAuthAdditionalInfo.setAccountPan("abc123");
		   preAuthAdditionalInfo.setPayerCurrency("payerCurrency");
		   preAuthAdditionalInfo.setPayerJurisdiction("payerJurisdiction");
		   
		return pispScaConsentFoundationServiceAdapter.validatePreAuthorisation(selectedDebtorDetails,
				 preAuthAdditionalInfo, stageIdentifiers ,params);
		
	}
	
	
	@RequestMapping("/testPispScaConsentControllerForUpdate")
    public void getDataForUpdate()
    {
          CustomPaymentStageIdentifiers customPaymentStageIdentifiers=new CustomPaymentStageIdentifiers();
          customPaymentStageIdentifiers.setPaymentConsentId("9598e2fb-4a1f-4f0a-98bb-6dea27c23407");
          customPaymentStageIdentifiers.setPaymentSetupVersion("1.0");
          customPaymentStageIdentifiers.setPaymentTypeEnum(PaymentTypeEnum.DOMESTIC_PAY);

          OBCashAccountDebtor3 debtorDetails = new OBCashAccountDebtor3();
          debtorDetails.setIdentification("12345678");
          debtorDetails.setName("Update");
          debtorDetails.setSchemeName("PISP");
          debtorDetails.setSecondaryIdentification("456789");

          CustomPaymentStageUpdateData updateData = new CustomPaymentStageUpdateData();
          FraudServiceResponse fraudServiceResponse= new FraudServiceResponse();
          fraudServiceResponse.setDecisionType((DecisionType.INVESTIGATE_APPROVE));
          //Object fraud = FraudSystemResponse.APPROVE;
          updateData.setDebtorDetails(debtorDetails);
          updateData.setDebtorDetailsUpdated(true);
          updateData.setFraudScoreUpdated(true);
          updateData.setFraudScore(fraudServiceResponse);
          updateData.setSetupStatus("Authorised");
          updateData.setSetupStatusUpdated(true);
          updateData.setSetupStatusUpdateDateTime("2019-01-22T15:15:37+00:00");

          Map<String,String> params=new HashMap<String,String>();
          params.put("x-api-source-system", "PSD2API");
          params.put("x-api-correlation-id", "");
          params.put("x-api-transaction-id", "BOI999");
          params.put("x-api-party-source-id-number", "platform");
          params.put("x-api-source-user", "87654321");
          params.put("x-api-channel-code", "BOL");
          params.put("tenant_id", "BOIUK");
          params.put("X-BOI-CHANNEL","BOL");

          pispScaConsentFoundationServiceAdapter.updatePaymentStageData(customPaymentStageIdentifiers, updateData, params);

    }
	
	@RequestMapping("/testPispScheduledScaConsentControllerForUpdate")
    public void getDataScheduledForUpdate()
    {
          CustomPaymentStageIdentifiers customPaymentStageIdentifiers=new CustomPaymentStageIdentifiers();
          customPaymentStageIdentifiers.setPaymentConsentId("5df87a0e-2273-469f-8f52-2b32de11f4b6");
          customPaymentStageIdentifiers.setPaymentSetupVersion("1.0");
          customPaymentStageIdentifiers.setPaymentTypeEnum(PaymentTypeEnum.DOMESTIC_SCH_PAY);

          OBCashAccountDebtor3 debtorDetails = new OBCashAccountDebtor3();
          debtorDetails.setIdentification("08080021325698");
          debtorDetails.setName("Update");
          debtorDetails.setSchemeName("UK.OBIE.SortCodeAccountNumber");
          debtorDetails.setSecondaryIdentification("456789");

          CustomPaymentStageUpdateData updateData = new CustomPaymentStageUpdateData();
          updateData.setDebtorDetails(debtorDetails);
          updateData.setDebtorDetailsUpdated(true);
          FraudServiceResponse fraudServiceResponse= new FraudServiceResponse();
          fraudServiceResponse.setDecisionType((DecisionType.INVESTIGATE_APPROVE));
          updateData.setFraudScoreUpdated(true);
          updateData.setSetupStatus("Authorised");
          updateData.setSetupStatusUpdated(true);
          updateData.setSetupStatusUpdateDateTime("2019-01-22T15:15:37+00:00");
         // Object fraud = FraudSystemResponse.APPROVE; 
          updateData.setFraudScoreUpdated(true);
          updateData.setFraudScore(fraudServiceResponse); 
         

          Map<String,String> params=new HashMap<String,String>();
          params.put("X-API-SOURCE-SYSTEM", "PSD2API");
          params.put("X-API-CORRELATION-ID", "");
          params.put("X-API-TRANSACTION-ID", "BOI999");
          params.put("X-API-PARTY-SOURCE-ID-NUMBER", "platform");
          params.put("X-API-SOURCE-USER", "87654321");
          params.put("X-API-CHANNEL-CODE", "BOL");

          pispScaConsentFoundationServiceAdapter.updatePaymentStageData(customPaymentStageIdentifiers, updateData, params);

    }

	@RequestMapping("/testValidateStandingOrderPispScaConsentController")
	public void validateStandingOrderData()
	{	
		 
		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();
		stageIdentifiers.setPaymentConsentId("e15cffc5-46de-4694-9852-16ebad3a9568");
		stageIdentifiers.setPaymentSetupVersion("v1.0");
		stageIdentifiers.setPaymentSubmissionId("12345");
		stageIdentifiers.setPaymentTypeEnum(PaymentTypeEnum.DOMESTIC_ST_ORD);
		
		Map<String,String> params=new HashMap<String ,String>();
		 params.put("sourceSystemReqHeader", "PSD2API");
		    params.put("correlationMuleReqHeader", "");
		    params.put("transactioReqHeader", "BOI999");
		    params.put("partysourceReqHeader", "platform");
		    params.put("sourceUserReqHeader", "87654321");
		    params.put("apiChannelCode","BOI");
		    
		   OBCashAccountDebtor3 selectedDebtorDetails = new OBCashAccountDebtor3();
		   selectedDebtorDetails.setIdentification("validatorIdentification");
		   selectedDebtorDetails.setName("validatorName");
		   selectedDebtorDetails.setSchemeName("validatorSchemeName");
		   selectedDebtorDetails.setSecondaryIdentification("validatorSecondaryIdentification");
		   
		   CustomPreAuthorizeAdditionalInfo preAuthAdditionalInfo = new CustomPreAuthorizeAdditionalInfo();
		   preAuthAdditionalInfo.setAccountBic("02113sds");
		   preAuthAdditionalInfo.setAccountIban("123abc");
		   preAuthAdditionalInfo.setAccountNSC("abc123");
		   preAuthAdditionalInfo.setAccountNumber("abc123");
		   preAuthAdditionalInfo.setAccountPan("abc123");
		   preAuthAdditionalInfo.setPayerCurrency("payerCurrency");
		   preAuthAdditionalInfo.setPayerJurisdiction("payerJurisdiction");
		   
		 pispScaConsentFoundationServiceAdapter.validatePreAuthorisation(selectedDebtorDetails,
				 preAuthAdditionalInfo, stageIdentifiers ,params);
		
	}
	
	@RequestMapping("/testPispScaConsentControllerForStandingOrder")
    public CustomConsentAppViewData getDataForStandingOrder()
    {
		CustomPaymentStageIdentifiers customPaymentStageIdentifiers=new CustomPaymentStageIdentifiers();
        customPaymentStageIdentifiers.setPaymentConsentId("26789");
        customPaymentStageIdentifiers.setPaymentSetupVersion("1.0");
        customPaymentStageIdentifiers.setPaymentTypeEnum(PaymentTypeEnum.DOMESTIC_ST_ORD);
        Map<String,String> params=new HashMap<String,String>();
        params.put("x-api-source-system", "PSD2API");
        params.put("x-api-correlation-id", "");
        params.put("x-api-transaction-id", "BOI999");
        params.put("x-api-party-source-id-number", "platform");
        params.put("x-api-source-user", "87654321");
        params.put("x-api-channel-code", "BOL");
		
		return pispScaConsentFoundationServiceAdapter.retrieveConsentAppStagedViewData(customPaymentStageIdentifiers, params);
    }
	
	@RequestMapping("/testValidateScheduledPispScaConsentController")
	public void validateScheduledData()
	{	
		 
		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();
		stageIdentifiers.setPaymentConsentId("5df87a0e-2273-469f-8f52-2b32de11f4b6");
		stageIdentifiers.setPaymentSetupVersion("v1.0");
		stageIdentifiers.setPaymentSubmissionId("12345");
		stageIdentifiers.setPaymentTypeEnum(PaymentTypeEnum.DOMESTIC_SCH_PAY);
		
		Map<String,String> params=new HashMap<String ,String>();
		 params.put("sourceSystemReqHeader", "PSD2API");
		    params.put("correlationMuleReqHeader", "");
		    params.put("transactioReqHeader", "BOI999");
		    params.put("partysourceReqHeader", "platform");
		    params.put("sourceUserReqHeader", "87654321");
		    params.put("apiChannelCode","BOI");
		    
		   OBCashAccountDebtor3 selectedDebtorDetails = new OBCashAccountDebtor3();
		   selectedDebtorDetails.setIdentification("validatorIdentification");
		   selectedDebtorDetails.setName("validatorName");
		   selectedDebtorDetails.setSchemeName("validatorSchemeName");
		   selectedDebtorDetails.setSecondaryIdentification("validatorSecondaryIdentification");
		   
		   CustomPreAuthorizeAdditionalInfo preAuthAdditionalInfo = new CustomPreAuthorizeAdditionalInfo();
		   preAuthAdditionalInfo.setAccountBic("02113sds");
		   preAuthAdditionalInfo.setAccountIban("123abc");
		   preAuthAdditionalInfo.setAccountNSC("abc123");
		   preAuthAdditionalInfo.setAccountNumber("abc123");
		   preAuthAdditionalInfo.setAccountPan("abc123");
		   preAuthAdditionalInfo.setPayerCurrency("payerCurrency");
		   preAuthAdditionalInfo.setPayerJurisdiction("payerJurisdiction");
		   
		 pispScaConsentFoundationServiceAdapter.validatePreAuthorisation(selectedDebtorDetails,
				 preAuthAdditionalInfo, stageIdentifiers ,params);
		
	}

	@RequestMapping("/testPispScaConsentControllerForUpdateStOrd")
    public void getDataStandingOrderForUpdate()
    {
          CustomPaymentStageIdentifiers customPaymentStageIdentifiers=new CustomPaymentStageIdentifiers();
          customPaymentStageIdentifiers.setPaymentConsentId("e15cffc5-46de-4694-9852-16ebad3a9568");
          customPaymentStageIdentifiers.setPaymentSetupVersion("1.0");
          customPaymentStageIdentifiers.setPaymentTypeEnum(PaymentTypeEnum.DOMESTIC_ST_ORD);

          OBCashAccountDebtor3 debtorDetails = new OBCashAccountDebtor3();
          debtorDetails.setIdentification("12345678");
          debtorDetails.setName("Update");
          debtorDetails.setSchemeName("PISP");
          debtorDetails.setSecondaryIdentification("456789");

          CustomPaymentStageUpdateData updateData = new CustomPaymentStageUpdateData();
          updateData.setDebtorDetails(debtorDetails);
          updateData.setDebtorDetailsUpdated(true);
          updateData.setFraudScoreUpdated(true);
          updateData.setSetupStatus("Authorised");
          updateData.setSetupStatusUpdated(true);
          updateData.setSetupStatusUpdateDateTime("09/12/2018");

          Map<String,String> params=new HashMap<String,String>();
          params.put("x-api-source-system", "PSD2API");
          params.put("x-api-correlation-id", "");
          params.put("x-api-transaction-id", "BOI999");
          params.put("x-api-party-source-id-number", "platform");
          params.put("x-api-source-user", "87654321");
          params.put("x-api-channel-code", "BOL");

          pispScaConsentFoundationServiceAdapter.updatePaymentStageData(customPaymentStageIdentifiers, updateData, params);

    }
	
	@RequestMapping("/testFraudSystemDomesticSchedulePaymentStagedData")
    public CustomFraudSystemPaymentData getFraudSystemDomesticSchedulePaymentStagedData()
    {
		
		CustomPaymentStageIdentifiers customPaymentStageIdentifiers=new CustomPaymentStageIdentifiers();
        customPaymentStageIdentifiers.setPaymentConsentId("65e940da-c445-42c9-abd3-d360a5a1b0a2");
        customPaymentStageIdentifiers.setPaymentSetupVersion("1.0");
        customPaymentStageIdentifiers.setPaymentTypeEnum(PaymentTypeEnum.DOMESTIC_SCH_PAY);
        Map<String,String> params=new HashMap<String,String>();
        params.put("x-api-source-system", "PSD2API");
        params.put("x-api-correlation-id", "");
        params.put("x-api-transaction-id", "BOI999");
        params.put("x-api-party-source-id-number", "platform");
        params.put("x-api-source-user", "87654321");
        params.put("x-api-channel-code", "BOL");
		
		return pispScaConsentFoundationServiceAdapter.retrieveFraudSystemPaymentStagedData(customPaymentStageIdentifiers, params);
    }
	
	
	@RequestMapping("/testPispScaConsentControllerForUpdateStOrdReject")
    public void getDataStandingOrderForReject()
    {
          CustomPaymentStageIdentifiers customPaymentStageIdentifiers=new CustomPaymentStageIdentifiers();
          customPaymentStageIdentifiers.setPaymentConsentId("e15cffc5-46de-4694-9852-16ebad3a9568");
          customPaymentStageIdentifiers.setPaymentSetupVersion("3.0");
          customPaymentStageIdentifiers.setPaymentTypeEnum(PaymentTypeEnum.DOMESTIC_ST_ORD);

          OBCashAccountDebtor3 debtorDetails = new OBCashAccountDebtor3();
          debtorDetails.setIdentification("FR1420041010050500013M02606");
          debtorDetails.setName("Tom Druise");
          debtorDetails.setSchemeName("UK.OBIE.IBAN");
          debtorDetails.setSecondaryIdentification("456789");

          CustomPaymentStageUpdateData updateData = new CustomPaymentStageUpdateData();
          updateData.setDebtorDetails(debtorDetails);
          updateData.setDebtorDetailsUpdated(true);
          updateData.setFraudScoreUpdated(true);
          updateData.setSetupStatus("Rejected");
          updateData.setSetupStatusUpdated(true);
          updateData.setSetupStatusUpdateDateTime("09/12/2018");

          Map<String,String> params=new HashMap<String,String>();
          params.put("x-api-source-system", "PSD2API");
          params.put("x-api-correlation-id", "");
          params.put("x-api-transaction-id", "BOI999");
          params.put("x-api-party-source-id-number", "platform");
          params.put("x-api-source-user", "87654321");
          params.put("x-api-channel-code", "BOL");
          
          params.put("tenant_id", "BOIUK");
          

          pispScaConsentFoundationServiceAdapter.updatePaymentStageData(customPaymentStageIdentifiers, updateData, params);

    }
	
	@RequestMapping("/testPispScaConsentControllerForStOrdAuthoriseParty")
    public void getDataStandingOrderForAuthoriseParty()
    {
          CustomPaymentStageIdentifiers customPaymentStageIdentifiers=new CustomPaymentStageIdentifiers();
          customPaymentStageIdentifiers.setPaymentConsentId("e15cffc5-46de-4694-9852-16ebad3a9568");
          customPaymentStageIdentifiers.setPaymentSetupVersion("3.0");
          customPaymentStageIdentifiers.setPaymentTypeEnum(PaymentTypeEnum.DOMESTIC_ST_ORD);

          OBCashAccountDebtor3 debtorDetails = new OBCashAccountDebtor3();
          debtorDetails.setIdentification("FR1420041010050500013M02606");
          debtorDetails.setName("Tom123");
          debtorDetails.setSchemeName("UK.OBIE.IBAN");
          debtorDetails.setSecondaryIdentification("852147");

          CustomPaymentStageUpdateData updateData = new CustomPaymentStageUpdateData();
          updateData.setDebtorDetails(debtorDetails);
          updateData.setDebtorDetailsUpdated(true);
         
          Map<String,String> params=new HashMap<String,String>();
          params.put("X-BOI-USER", "99999999");
          params.put("X-BOI-CHANNEL", "BOL");
          params.put("x-api-channel-code", "BOL");
          
          params.put("tenant_id", "BOIUK");
          
          CustomConsentAppViewData customConsentAppViewData=
          pispScaConsentFoundationServiceAdapter.retrieveConsentAppDebtorDetails(customPaymentStageIdentifiers, updateData, params);
          System.out.println(customConsentAppViewData.toString());
    }

	@RequestMapping("/testPispScaConsentControllerForStOrdAuthorise")
    public void getDataStandingOrderForAuthorise()
    {
          CustomPaymentStageIdentifiers customPaymentStageIdentifiers=new CustomPaymentStageIdentifiers();
          customPaymentStageIdentifiers.setPaymentConsentId("7118ae3f-233c-4722-b649-1db5f9b766da");
          customPaymentStageIdentifiers.setPaymentSetupVersion("3.0");
          customPaymentStageIdentifiers.setPaymentTypeEnum(PaymentTypeEnum.DOMESTIC_ST_ORD);

          OBCashAccountDebtor3 debtorDetails = new OBCashAccountDebtor3();
          debtorDetails.setIdentification("FR1420041010050500013M02606");
          debtorDetails.setName("Tom123");
          debtorDetails.setSchemeName("UK.OBIE.IBAN");
          debtorDetails.setSecondaryIdentification("852147");

          CustomPaymentStageUpdateData updateData = new CustomPaymentStageUpdateData();
          updateData.setStatus("AwaitingAuthorisation");
          updateData.setDebtorDetails(debtorDetails);
          updateData.setDebtorDetailsUpdated(true);
          updateData.setSetupStatus("AwaitingAuthorisation");
          Map<String,String> params=new HashMap<String,String>();
          params.put("X-BOI-USER", "99999999");
          params.put("X-BOI-CHANNEL", "BOL");
          params.put("x-api-channel-code", "BOL");
          params.put("tenant_id", "BOIUK");
          params.put(PSD2Constants.PARTY_IDENTIFIER,"partySourceId");          
          pispScaConsentFoundationServiceAdapter.updatePaymentStageData(customPaymentStageIdentifiers, updateData, params);          
    }
	
}


