package com.capgemini.psd2.foundationservice.authentication.application.service.boi.adapter.transformer;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.capgemini.psd2.adapter.security.custom.domain.CustomAuthenticationServicePostRequest;
import com.capgemini.psd2.adapter.security.custom.domain.CustomAuthenticationServicePostResponse;
import com.capgemini.psd2.adapter.security.domain.AuthenticationMethodCode1;
import com.capgemini.psd2.adapter.security.domain.AuthenticationParameterTextTypeValue;
import com.capgemini.psd2.adapter.security.domain.ChannelType1;
import com.capgemini.psd2.adapter.security.domain.CustomerAuthenticationSession;
import com.capgemini.psd2.adapter.security.domain.DigitalUser13;
import com.capgemini.psd2.adapter.security.domain.EventType1;
import com.capgemini.psd2.adapter.security.domain.Login;
import com.capgemini.psd2.adapter.security.domain.LoginResponse;
import com.capgemini.psd2.adapter.security.domain.SecureAccessKeyPositionValue;
import com.capgemini.psd2.adapter.security.domain.SelectedDeviceReference;
import com.capgemini.psd2.adapter.security.domain.Type2;
import com.capgemini.psd2.pisp.stage.domain.CustomConsentAppViewData;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Component
public class SCAAuthenticationServiceFoundationServiceTransformer {

	public <T> CustomAuthenticationServicePostResponse transformAuthenticationServiceResponse(T inputBalanceObj) {
		CustomAuthenticationServicePostResponse customAuthenticationServicePostResponse = new CustomAuthenticationServicePostResponse();
		LoginResponse loginResponse = (LoginResponse) inputBalanceObj;
		customAuthenticationServicePostResponse.setLoginResponse(loginResponse);
		return customAuthenticationServicePostResponse;
	}

	public <T> Login transformAuthenticationServiceRequest(T inputBalanceObj, Map<String, Object> params) {
		CustomAuthenticationServicePostRequest request = (CustomAuthenticationServicePostRequest) inputBalanceObj;
		Login login = new Login();
		DigitalUser13 digitalUser = new DigitalUser13();
		List<CustomerAuthenticationSession> custAuthSessionList = null;
		CustomerAuthenticationSession custAuthSession = null;
		SelectedDeviceReference selDevRef = null;
		CustomConsentAppViewData cust = null;
		if (!NullCheckUtils.isNullOrEmpty(request)) {

			custAuthSessionList = new ArrayList<CustomerAuthenticationSession>();
			SecureAccessKeyPositionValue secKey = null;
			AuthenticationParameterTextTypeValue authParam = null;
			List<AuthenticationParameterTextTypeValue> authParamList = null;
			List<SecureAccessKeyPositionValue> secKeyList = null;

			if (!request.getDigitalUser().getCustomerAuthenticationSession().isEmpty()) {

				for (CustomerAuthenticationSession custAuth1 : request.getDigitalUser()
						.getCustomerAuthenticationSession()) {
					custAuthSession = new CustomerAuthenticationSession();
					if (!NullCheckUtils.isNullOrEmpty(custAuth1.getAuthenticationMethodCode())) {
						if (custAuth1.getAuthenticationMethodCode().equals(AuthenticationMethodCode1.DATE_OF_BIRTH)) {
							custAuthSession.setAuthenticationMethodCode(AuthenticationMethodCode1
									.fromValue(custAuth1.getAuthenticationMethodCode().toString()));
							if (!custAuth1.getSecureAccessKeyUsed().isEmpty()) {
								secKeyList = new ArrayList<SecureAccessKeyPositionValue>();
								for (SecureAccessKeyPositionValue secKey1 : custAuth1.getSecureAccessKeyUsed()) {
									secKey = new SecureAccessKeyPositionValue();
									if (!NullCheckUtils.isNullOrEmpty(secKey1.getValue()))
										secKey.setValue(secKey1.getValue());
									secKeyList.add(secKey);
								}
								custAuthSession.setSecureAccessKeyUsed(secKeyList);
							}
						}
						if (custAuth1.getAuthenticationMethodCode().equals(AuthenticationMethodCode1.PIN)) {
							custAuthSession.setAuthenticationMethodCode(AuthenticationMethodCode1
									.fromValue(custAuth1.getAuthenticationMethodCode().toString()));
							if (!custAuth1.getSecureAccessKeyUsed().isEmpty()) {
								secKeyList = new ArrayList<SecureAccessKeyPositionValue>();
								for (SecureAccessKeyPositionValue secKey1 : custAuth1.getSecureAccessKeyUsed()) {
									secKey = new SecureAccessKeyPositionValue();
									if (!NullCheckUtils.isNullOrEmpty(secKey1.getPosition())
											|| !NullCheckUtils.isNullOrEmpty(secKey1.getValue())) {
										secKey.setPosition(secKey1.getPosition());
										secKey.setValue(secKey1.getValue());
									}
									secKeyList.add(secKey);
								}
								custAuthSession.setSecureAccessKeyUsed(secKeyList);
							}

							if (!NullCheckUtils.isNullOrEmpty(custAuth1.getEventType())) {

								custAuthSession.setEventType(EventType1.fromValue(custAuth1.getEventType().toString()));
							}

							if (!NullCheckUtils.isNullOrEmpty(request.getChannelType())) {

								custAuthSession
										.setChannelType(ChannelType1.fromValue(request.getChannelType().toString()));
							}
						} else if (custAuth1.getAuthenticationMethodCode()
								.equals(AuthenticationMethodCode1.PUSH_NOTIFICATION)) {
							custAuthSession.setAuthenticationMethodCode(AuthenticationMethodCode1
									.fromValue(custAuth1.getAuthenticationMethodCode().toString()));
							if (!NullCheckUtils.isNullOrEmpty(custAuth1.getAuthenticationParameterText())) {
								if (!custAuth1.getAuthenticationParameterText().isEmpty()) {
									authParamList = new ArrayList<AuthenticationParameterTextTypeValue>();
									String intent = String.valueOf(params.get("intentType"));
									if (intent.equalsIgnoreCase("PISP")) {
										cust = (CustomConsentAppViewData) params.get("custObj");
									}

									for (AuthenticationParameterTextTypeValue authParam1 : custAuth1
											.getAuthenticationParameterText()) {
										authParam = new AuthenticationParameterTextTypeValue();

										if (!NullCheckUtils.isNullOrEmpty(authParam1.getType())) {
											authParam.setType(Type2.fromValue(authParam1.getType().toString()));
											if (intent.equalsIgnoreCase("PISP")) {

												if (authParam.getType().toString()
														.equalsIgnoreCase("PAYER_ACCOUNT_NUMBER")) {
													if (!NullCheckUtils.isNullOrEmpty(cust.getDebtorDetails())) {
														if (!NullCheckUtils.isNullOrEmpty(
																cust.getDebtorDetails().getIdentification())) {
															authParam.setValue(
																	cust.getDebtorDetails().getIdentification());

															
														}
													} 
													authParamList.add(authParam);
												}
												if (authParam.getType().toString()
														.equalsIgnoreCase("PAYEE_ACCOUNT_NUMBER")) {
													if (!NullCheckUtils.isNullOrEmpty(
															cust.getCreditorDetails().getIdentification())) {
														authParam.setValue(
																cust.getCreditorDetails().getIdentification());
														
													}
													authParamList.add(authParam);
												}

												if (authParam.getType().toString().equalsIgnoreCase("PAYER_IBAN")) {
													if (!NullCheckUtils.isNullOrEmpty(cust.getDebtorDetails())) {
														if (!NullCheckUtils.isNullOrEmpty(
																cust.getDebtorDetails().getIdentification())) {
															authParam.setValue(
																	cust.getDebtorDetails().getIdentification());
															
														}
														authParamList.add(authParam);
													}
													
												}
												if (authParam.getType().toString().equalsIgnoreCase("PAYEE_IBAN")) {
													if (!NullCheckUtils.isNullOrEmpty(
															cust.getCreditorDetails().getIdentification())) {
														authParam.setValue(
																cust.getCreditorDetails().getIdentification());
														
													}
													authParamList.add(authParam);
												}

												if (authParam.getType().toString().equalsIgnoreCase("AMOUNT")) {
													if (!NullCheckUtils
															.isNullOrEmpty(cust.getAmountDetails().getAmount())) {
														authParam.setValue(cust.getAmountDetails().getAmount());
														
													}
													authParamList.add(authParam);
												}

												if (authParam.getType().toString().equalsIgnoreCase("CURRENCY")) {
													if (!NullCheckUtils
															.isNullOrEmpty(cust.getAmountDetails().getCurrency())) {
														authParam.setValue(cust.getAmountDetails().getCurrency());
														
													}
													authParamList.add(authParam);
												}
												if (authParam.getType().toString().equalsIgnoreCase("PAYEE_NAME")) {
													if (!NullCheckUtils
															.isNullOrEmpty(cust.getCreditorDetails().getName())) {
														authParam.setValue(cust.getCreditorDetails().getName());
														
													}
													authParamList.add(authParam);

												}
												if (authParam.getType().toString().equalsIgnoreCase("PAYMENT_START_DATE")) {
													if (!NullCheckUtils
															.isNullOrEmpty(cust.getCustomDSOData())){
													if (!NullCheckUtils
															.isNullOrEmpty(cust.getCustomDSOData().getFirstPaymentDateTime())) {
														DateFormat inputformat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
														DateFormat desiredformat = new SimpleDateFormat("dd/MM/yyyy");
														Date input=new Date();
														try {
															input = inputformat.parse(cust.getCustomDSOData().getFirstPaymentDateTime());
														} catch (ParseException e) {
															// TODO Auto-generated catch block
															e.printStackTrace();
														}
														System.out.println("Updated Format: "+desiredformat.format(input));
														authParam.setValue(desiredformat.format(input));
														
													}
													authParamList.add(authParam);

												}
												}
											}

											

											if (authParam.getType().toString().equalsIgnoreCase("TPP_NAME")) {
												if (!NullCheckUtils.isNullOrEmpty(params.get("tppInformationObj"))) {
													authParam.setValue(params.get("tppInformationObj").toString());
													
												} 
												authParamList.add(authParam);
											}

										}

									}
									custAuthSession.setAuthenticationParameterText(authParamList);
								}
							}

							selDevRef = new SelectedDeviceReference();
							if (!NullCheckUtils.isNullOrEmpty(
									custAuth1.getSelectedDeviceReference().getCustomerAuthenticationAppIdentifier())) {

								selDevRef.setCustomerAuthenticationAppIdentifier(custAuth1.getSelectedDeviceReference()
										.getCustomerAuthenticationAppIdentifier());

							}

							if (!NullCheckUtils
									.isNullOrEmpty(custAuth1.getSelectedDeviceReference().getDeviceStatusCode())) {

								selDevRef.setDeviceStatusCode(
										custAuth1.getSelectedDeviceReference().getDeviceStatusCode());

							}
							if (!NullCheckUtils
									.isNullOrEmpty(custAuth1.getSelectedDeviceReference().getDeviceNameText())) {

								selDevRef.setDeviceNameText(custAuth1.getSelectedDeviceReference().getDeviceNameText());

							}
							if (!NullCheckUtils.isNullOrEmpty(custAuth1.getSelectedDeviceReference().getDeviceType())) {

								selDevRef.setDeviceType(custAuth1.getSelectedDeviceReference().getDeviceType());

							}
							custAuthSession.setSelectedDeviceReference(selDevRef);

							if (!NullCheckUtils.isNullOrEmpty(custAuth1.getEventType())) {

								custAuthSession.setEventType(EventType1.fromValue(custAuth1.getEventType().toString()));
							}

							if (!NullCheckUtils.isNullOrEmpty(request.getChannelType())) {

								custAuthSession
										.setChannelType(ChannelType1.fromValue(request.getChannelType().toString()));
							}

						}

					}
					custAuthSessionList.add(custAuthSession);
				}
			}
			digitalUser.setCustomerAuthenticationSession(custAuthSessionList);
		}
		login.setDigitalUser(digitalUser);

		return login;
	}
}