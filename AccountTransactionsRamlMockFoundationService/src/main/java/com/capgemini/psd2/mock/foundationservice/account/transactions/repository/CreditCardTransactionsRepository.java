package com.capgemini.psd2.mock.foundationservice.account.transactions.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.capgemini.psd2.mock.foundationservice.account.transactions.raml.domain.TransactionList;

public interface CreditCardTransactionsRepository extends MongoRepository<TransactionList, String> {

	/**
	 * Find by nsc and account number.
	 *
	 * @param nsc the nsc
	 * @param accountNumber the account number
	 * @return the accnt
	 */
	public TransactionList findByAccountId(String accountId);
	
}

