package com.capgemini.psd2.cisp.utilities;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.Currency;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.cisp.domain.OBFundsConfirmation1;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmationConsent1;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmationConsent1DataDebtorAccount;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmationConsentResponse1;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmationResponse1;
import com.capgemini.psd2.exceptions.ErrorMapKeys;
import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.InternalServerErrorMessage;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.utilities.DateUtilites;
import com.capgemini.psd2.utilities.NullCheckUtils;
import com.capgemini.psd2.utilities.PSD2IBANUtil;
import com.capgemini.psd2.validator.PSD2Validator;

@Component
@ConfigurationProperties("app")
public class CommonCardValidations {

	@Value("${app.validConsentIdChars}")
	private String validConsentIdCharsRegEx;
	
	@Value("${app.regex.consentId:#{null}}")
	private String fundsConsentIdRegexValidator;

	@Autowired
	private CispDateUtility dateUtility;

	@Autowired
	private PSD2Validator cmaValidator;

	@Value("${app.swaggerValidations.request:#{true}}")
	private Boolean reqValidationEnabled;

	@Value("${app.sortCodeAccountNumberPattern:^[0-9]{6}[0-9]{8}$}")
	private String sortCodeAccountNumberPattern;

	@Value("${app.validPanIdentification:.*[0-9]{4}}")
	private String validPanIdentification;

	private final Pattern bbanPattern = Pattern.compile("^[A-Z]{4}\\d{14}$");
	private final Pattern amountPattern = Pattern.compile("^\\d*\\.?\\d+$");

	private List<String> validSchemeNameList = new ArrayList<>();

	public List<String> getValidSchemeNameList() {
		return validSchemeNameList;
	}

	public OBFundsConfirmationConsent1 validateRequestParams(
			OBFundsConfirmationConsent1 fundsConfirmationConsentPOSTRequest) {
		if (null == fundsConfirmationConsentPOSTRequest || null == fundsConfirmationConsentPOSTRequest.getData()) {
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_RESOURCE_INVALIDFORMAT,
					InternalServerErrorMessage.REQUEST_BODY_EMPTY));
		} else {
			if (reqValidationEnabled) {
				this.performSwaggerValidation(fundsConfirmationConsentPOSTRequest);
			}
			OBFundsConfirmationConsent1DataDebtorAccount debtor = fundsConfirmationConsentPOSTRequest.getData().getDebtorAccount();
			if (!NullCheckUtils.isNullOrEmpty(debtor)) {
				validateSchemeNameWithIdentification(debtor.getSchemeName(), debtor.getIdentification(), null);
			}
		}
		return validateFormingConsentInputs(fundsConfirmationConsentPOSTRequest);
	}

	public void validateUniqueUUID(String consentId) {
		if (NullCheckUtils.isNullOrEmpty(consentId))
			throw PSD2Exception.populatePSD2Exception(
					new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_FIELD_EXPECTED, ErrorMapKeys.NO_CONSENT_ID_FOUND));

		if (consentId.length() < 1 || consentId.length() > 128) {
			throw PSD2Exception.populatePSD2Exception(
					new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_RESOURCE_NOTFOUND, ErrorMapKeys.VALIDATION_ERROR));
		}

		if (!consentId.matches(validConsentIdCharsRegEx)) {
			throw PSD2Exception.populatePSD2Exception(
					new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_RESOURCE_NOTFOUND, ErrorMapKeys.VALIDATION_ERROR));
		}
	}

	public void validateResponseParams(OBFundsConfirmationConsentResponse1 fundsConfirmationConsentPOSTResponse) {

		if (reqValidationEnabled) {
			this.performSwaggerValidation(fundsConfirmationConsentPOSTResponse);
		}

		// Mandatory Field - Creation Date Time
		if (!NullCheckUtils.isNullOrEmpty(fundsConfirmationConsentPOSTResponse.getData().getCreationDateTime())) {
			validateAndParseDateTimeFormatForResponse(
					fundsConfirmationConsentPOSTResponse.getData().getCreationDateTime());
		}

		// Mandatory Field - Status Date Time
		if (!NullCheckUtils.isNullOrEmpty(fundsConfirmationConsentPOSTResponse.getData().getStatusUpdateDateTime())) {
			validateAndParseDateTimeFormatForResponse(
					fundsConfirmationConsentPOSTResponse.getData().getStatusUpdateDateTime());
		}

		// Mandatory Field - Expiration Date Time
		if (!NullCheckUtils.isNullOrEmpty(fundsConfirmationConsentPOSTResponse.getData().getExpirationDateTime())) {
			validateAndParseDateTimeFormatForResponse(
					fundsConfirmationConsentPOSTResponse.getData().getExpirationDateTime());
		}
	}

	public void validateSubmissionRequestParams(OBFundsConfirmation1 oBFundsConfirmation1) {
		if (null == oBFundsConfirmation1 || null == oBFundsConfirmation1.getData()) {
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_RESOURCE_INVALIDFORMAT,
					InternalServerErrorMessage.REQUEST_BODY_EMPTY));
				
		}
		if (!oBFundsConfirmation1.getData().getConsentId().matches(fundsConsentIdRegexValidator)) {
			throw PSD2Exception.populatePSD2Exception(
					new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_RESOURCE_NOTFOUND, ErrorMapKeys.INVALID_CONSENT_ID));
			}
		if (reqValidationEnabled) {
			this.performSwaggerValidation(oBFundsConfirmation1);
		}
	}

	public void validateSubmissionResponseParams(OBFundsConfirmationResponse1 oBFundsConfirmationResponse1) {
		if (reqValidationEnabled) {
			this.performSwaggerValidation(oBFundsConfirmationResponse1);
		}
		// Mandatory Field - Creation Date Time
		if (!NullCheckUtils.isNullOrEmpty(oBFundsConfirmationResponse1.getData().getCreationDateTime())) {
			validateAndParseDateTimeFormatForResponse(
					oBFundsConfirmationResponse1.getData().getCreationDateTime());
		}
		//valid currency validation
		if (!NullCheckUtils.isNullOrEmpty(oBFundsConfirmationResponse1.getData().getInstructedAmount().getCurrency())) {
			isValidCurrency(oBFundsConfirmationResponse1.getData().getInstructedAmount().getCurrency(), null);
		}
		//valid amount validation
		if (!NullCheckUtils.isNullOrEmpty(oBFundsConfirmationResponse1.getData().getInstructedAmount().getAmount())) {
			validateAmount(oBFundsConfirmationResponse1.getData().getInstructedAmount().getAmount());
		}
	}

	public void validateAmount(String amount) {
		if (!NullCheckUtils.isNullOrEmpty(amount)) {
			try {
				Matcher matcher = amountPattern.matcher(amount);
				if (!matcher.find()) {
					throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
							InternalServerErrorMessage.AMOUNT_VALIDATION_FAILED));
				}
			} catch (Exception exception) {
				throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
						InternalServerErrorMessage.AMOUNT_VALIDATION_FAILED));
			}
		}
	}

	public void validateSchemeNameWithIdentification(String inputScheme, String identification,
			OBErrorCodeEnum errorCodeEnum) {
		if (!NullCheckUtils.isNullOrEmpty(inputScheme)) {
			if (!NullCheckUtils.isNullOrEmpty(identification)){
				if (!validSchemeNameList.contains(inputScheme)) {
					if (errorCodeEnum == null) {
						throw PSD2Exception.populatePSD2Exception(
								new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNSUPPORTED_SCHEME, ErrorMapKeys.INVALID_SCHEME));
					} else {
						throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
								InternalServerErrorMessage.INVALID_SCHEME_RESPONSE));
					}
				}

				switch (inputScheme) {
				case CardSetupConstants.UK_OBIE_IBAN:
				case CardSetupConstants.IBAN:
					validateIBAN(identification, errorCodeEnum);
					break;
				case CardSetupConstants.UK_OBIE_SORTCODEACCOUNTNUMBER:
				case CardSetupConstants.SORTCODEACCOUNTNUMBER:
					validateSortCodeAccountNumber(identification, errorCodeEnum);
					break;
				case CardSetupConstants.UK_OBIE_PAN:
				case CardSetupConstants.PAN:
					validatePAN(identification, errorCodeEnum);
					break;
				case CardSetupConstants.UK_OBIE_BBAN:
					validateBBAN(identification, errorCodeEnum);
					break;
				case CardSetupConstants.UK_OBIE_PAYM:
					validatePaym();
					break;
				default:
					break;
				}
			}

			else{
				if (errorCodeEnum == null) {
					throw PSD2Exception.populatePSD2Exception(
							new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNSUPPORTED_ACCOUNTIDENTIFIER, ErrorMapKeys.ACCOUNT_IDENTIFIER_INVALID));
				} else {
					throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
							InternalServerErrorMessage.ACCOUNT_IDENTIFIER_INVALID));
				}
			}
		}
		else{
			if (errorCodeEnum == null) {
				throw PSD2Exception.populatePSD2Exception(
						new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNSUPPORTED_SCHEME, ErrorMapKeys.INVALID_SCHEME));
			} else {
				throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
						InternalServerErrorMessage.INVALID_SCHEME_RESPONSE));
			}
		}
	}


	private void validateIBAN(String iban, OBErrorCodeEnum errorCodeEnum) {
		if (!NullCheckUtils.isNullOrEmpty(iban)) {
			try {
				PSD2IBANUtil.validate(iban.trim());
			} catch (Exception exception) {
				throwIBANException(errorCodeEnum);
			}
		}
	}

	private void throwIBANException(OBErrorCodeEnum errorCodeEnum) {
		if (errorCodeEnum == null) {
			throw PSD2Exception.populatePSD2Exception(
					new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNSUPPORTED_ACCOUNTIDENTIFIER,
							ErrorMapKeys.ACCOUNT_IDENTIFIER_INVALID));
		} else {
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
					InternalServerErrorMessage.IBAN_VALIDATION_FAILED));
		}
	}

	private void validateSortCodeAccountNumber(String identification, OBErrorCodeEnum errorCodeEnum) {
		if (!NullCheckUtils.isNullOrEmpty(identification) && !identification.matches(sortCodeAccountNumberPattern)) {
			if (errorCodeEnum == null) {
				throw PSD2Exception
				.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNSUPPORTED_ACCOUNTIDENTIFIER,
						ErrorMapKeys.ACCOUNT_IDENTIFIER_INVALID));
			} else {
				throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
						InternalServerErrorMessage.INVALID_SORTCODE_NUMBER));
			}
		}
	}


	private void validateBBAN(String bban, OBErrorCodeEnum errorCodeEnum) {
		if (!NullCheckUtils.isNullOrEmpty(bban)) {
			try {
				Matcher matcher = bbanPattern.matcher(bban);
				if (!matcher.find()) {
					throwBBANException(errorCodeEnum);
				}
			} catch (Exception exception) {
				throwBBANException(errorCodeEnum);
			}
		}
	}

	private void throwBBANException(OBErrorCodeEnum errorCodeEnum) {
		if (errorCodeEnum == null) {
			throw PSD2Exception.populatePSD2Exception(
					new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNSUPPORTED_ACCOUNTIDENTIFIER,
							ErrorMapKeys.ACCOUNT_IDENTIFIER_INVALID));
		} else {
			throw PSD2Exception.populatePSD2Exception(
					new ExceptionDTO(errorCodeEnum, InternalServerErrorMessage.BBAN_VALIDATION_FAILED));
		}
	}

	private void validatePaym() {
		// Add validations for Paym here
	}

	public void validatePAN(String identification, OBErrorCodeEnum errorCodeEnum) {
		if (!NullCheckUtils.isNullOrEmpty(identification) && !identification.matches(validPanIdentification)) {
			if (errorCodeEnum == null) {
				throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNSUPPORTED_ACCOUNTIDENTIFIER,
						ErrorMapKeys.ACCOUNT_IDENTIFIER_INVALID));
			} else {
				throw PSD2Exception.populatePSD2Exception(
						new ExceptionDTO(errorCodeEnum, InternalServerErrorMessage.INVALID_PAN_NUMBER));
			}
		}
	} 

	/* validate for response dates */
	public void validateAndParseDateTimeFormatForResponse(String dateTimeString) {
		dateUtility.validateDateTimeInResponse(dateTimeString);
	}

	public OBFundsConfirmationConsent1 validateFormingConsentInputs(
			OBFundsConfirmationConsent1 fundsConfirmationConsentPOSTRequest) {

		if (null == fundsConfirmationConsentPOSTRequest || null == fundsConfirmationConsentPOSTRequest.getData()) {
			throw PSD2Exception.populatePSD2Exception(
					new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_FIELD_INVALID, ErrorMapKeys.VALIDATION_ERROR));
		} else {
			String expirationDateTime = fundsConfirmationConsentPOSTRequest.getData().getExpirationDateTime();
			dateUtility.validateDateTimeInRequest(expirationDateTime);
			expirationDateTime = dateUtility.transformDateTimeInRequest(expirationDateTime);

			OffsetDateTime parsedExpirationDate = null;
			if (!NullCheckUtils.isNullOrEmpty(expirationDateTime)) {
				parsedExpirationDate = OffsetDateTime.parse(expirationDateTime);
			}
			boolean expirationDateTimeCheck = false;
			if (!NullCheckUtils.isNullOrEmpty(parsedExpirationDate)) {
				expirationDateTimeCheck = DateUtilites.isOffsetDateComparisonPassed(parsedExpirationDate,
						OffsetDateTime.now());
			}
			if (expirationDateTimeCheck)
				throw PSD2Exception.populatePSD2Exception(
						new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_FIELD_INVALID, ErrorMapKeys.VALIDATION_ERROR));

			fundsConfirmationConsentPOSTRequest.getData().setExpirationDateTime(expirationDateTime);

		}
		return fundsConfirmationConsentPOSTRequest;
	}

	private void performSwaggerValidation(Object input) {
		cmaValidator.validateRequestWithSwagger(input,OBErrorCodeEnum.UK_OBIE_FIELD_INVALID.toString());
	}

	public boolean isValidCurrency(String currencyCode, OBErrorCodeEnum errorCode) {
		if (NullCheckUtils.isNullOrEmpty(currencyCode)) {
			if (errorCode == null)
				throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNSUPPORTED_CURRENCY,
						ErrorMapKeys.CURRENCY_NOT_SUPPORTED));
			else
				throw PSD2Exception.populatePSD2Exception(
						new ExceptionDTO(errorCode, InternalServerErrorMessage.UNSUPPORTED_CURRENCY));
		}
		try {
			Currency.getInstance(currencyCode.trim());
			return true;
		} catch (Exception exception) {
			if (errorCode == null)
				throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNSUPPORTED_CURRENCY,
						ErrorMapKeys.CURRENCY_NOT_SUPPORTED));
			else
				throw PSD2Exception.populatePSD2Exception(
						new ExceptionDTO(errorCode, InternalServerErrorMessage.UNSUPPORTED_CURRENCY));
		}
	}
}
