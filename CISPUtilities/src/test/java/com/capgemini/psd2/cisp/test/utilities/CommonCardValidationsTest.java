package com.capgemini.psd2.cisp.test.utilities;

import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.cisp.domain.OBFundsConfirmation1;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmation1Data;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmation1DataInstructedAmount;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmationConsent1;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmationConsent1Data;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmationConsent1DataDebtorAccount;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmationConsentResponse1;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmationConsentResponse1Data;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmationResponse1;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmationResponse1Data;
import com.capgemini.psd2.cisp.utilities.CardSetupConstants;
import com.capgemini.psd2.cisp.utilities.CispDateUtility;
import com.capgemini.psd2.cisp.utilities.CommonCardValidations;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.OBPSD2ExceptionUtility;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.validator.PSD2Validator;

@RunWith(SpringJUnit4ClassRunner.class)
public class CommonCardValidationsTest {

	@InjectMocks
	private CommonCardValidations commonCardValidations;

	@Mock
	private RequestHeaderAttributes reqHeaderAtrributes;

	@Mock
	private CispDateUtility dateUtility;
	
	@Mock
	private PSD2Validator cmaValidator;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		Map<String, String> map=new HashMap<>();
		map.put("SIGNATURE", "Something wrong with x-jws-signature request header");
		Map<String, String> specificErrorMessageMap=new HashMap<>();
		specificErrorMessageMap.put("signature_invalid_content", "Invalid content prsent in joseheader of x-jws-signature header");
		specificErrorMessageMap.put("signature_missing", "signature header missing in request");
		OBPSD2ExceptionUtility.genericErrorMessages.putAll(map);
		OBPSD2ExceptionUtility.specificErrorMessages.putAll(specificErrorMessageMap);
	}

	@Before
	public void initializeErrorMessages() {
		Map<String, String> genericErrorMessages = new HashMap<>();
		genericErrorMessages.put("FIELD", "freq ma");

		Map<String, String> specificErrorMessageMap = new HashMap<>();
		specificErrorMessageMap.put("FIELD", "freq ma");

		OBPSD2ExceptionUtility.genericErrorMessages.putAll(genericErrorMessages);
		OBPSD2ExceptionUtility.specificErrorMessages.putAll(specificErrorMessageMap);
	}

	@Before
	public void initializeLists() {
		String[] validSchemeNames = { "IBAN", "SortCodeAccountNumber", "PAN", "UK.OBIE.BBAN", "UK.OBIE.PAN",
			"UK.OBIE.IBAN", "UK.OBIE.SortCodeAccountNumber", "UK.OBIE.Paym" };
		List<String> validSchemeNameList = Arrays.asList(validSchemeNames);
		commonCardValidations.getValidSchemeNameList().addAll(validSchemeNameList);
		
	}

		@Test(expected=PSD2Exception.class)
	public void validateSchemeNameWithIdentification_invalidInputScheme() {
		commonCardValidations.validateSchemeNameWithIdentification("Invalid", "", OBErrorCodeEnum.UK_OBIE_UNSUPPORTED_SCHEME);
	}
	
	@Test(expected=PSD2Exception.class)
	public void validateSchemeNameWithIdentification_validInputScheme() {
		commonCardValidations.validateSchemeNameWithIdentification("Invalid", "", OBErrorCodeEnum.UK_OBIE_UNSUPPORTED_SCHEME);
	}

	

	@Test
	public void validateSchemeNameWithIdentification_validIBAN() {
		commonCardValidations.validateSchemeNameWithIdentification(CardSetupConstants.IBAN, "GB29NWBK60161331926819", null);
	}
	
	
	
	@Test
	public void validateSchemeNameWithIdentification_validIBANFull() {
		commonCardValidations.validateSchemeNameWithIdentification(CardSetupConstants.UK_OBIE_IBAN, "GB29NWBK60161331926819", null);
	}
	
	@Test(expected=PSD2Exception.class)
	public void validateSchemeNameWithIdentification_invalidIBANFullErrorCodeNull() {
		commonCardValidations.validateSchemeNameWithIdentification(CardSetupConstants.UK_OBIE_IBAN, "GK60161331926819", null);
	}

	@Test(expected=PSD2Exception.class)
	public void validateSchemeNameWithIdentification_invalidIBAN() {
		commonCardValidations.validateSchemeNameWithIdentification(CardSetupConstants.IBAN, "Invalid", OBErrorCodeEnum.UK_OBIE_UNSUPPORTED_ACCOUNTIDENTIFIER);
	}
	
	@Test(expected = PSD2Exception.class)
	public void validateSchemeNameWithIdentification_blankIBAN() {
		commonCardValidations.validateSchemeNameWithIdentification(CardSetupConstants.IBAN, "", OBErrorCodeEnum.UK_OBIE_UNSUPPORTED_ACCOUNTIDENTIFIER);
	}
	
	@Test(expected = PSD2Exception.class)
	public void validateSchemeNameWithIdentification_IBAN1() {
		commonCardValidations.validateSchemeNameWithIdentification(CardSetupConstants.IBAN, "12", OBErrorCodeEnum.UK_OBIE_UNSUPPORTED_ACCOUNTIDENTIFIER);
	}

	@Test
	public void validateSchemeNameWithIdentification_validSortCodeAccountNumber() {
		ReflectionTestUtils.setField(commonCardValidations, "sortCodeAccountNumberPattern", "^[0-9]{6}[0-9]{8}$");
		commonCardValidations.validateSchemeNameWithIdentification(CardSetupConstants.SORTCODEACCOUNTNUMBER,
				"09012803745521", null);
	}
	
	@Test(expected=PSD2Exception.class)
	public void validateSchemeNameWithIdentification_invalidSortCodeAccountNumberErrorCodeNull() {
		ReflectionTestUtils.setField(commonCardValidations, "sortCodeAccountNumberPattern", "^[0-9]{6}[0-9]{8}$");
		commonCardValidations.validateSchemeNameWithIdentification(CardSetupConstants.SORTCODEACCOUNTNUMBER,
				"0901280374552112323243243", null);
	}
	
	@Test
	public void validateSchemeNameWithIdentification_validSortCodeAccountNumberFull() {
		ReflectionTestUtils.setField(commonCardValidations, "sortCodeAccountNumberPattern", "^[0-9]{6}[0-9]{8}$");
		commonCardValidations.validateSchemeNameWithIdentification(CardSetupConstants.UK_OBIE_SORTCODEACCOUNTNUMBER,
				"09012803745521", null);
	}
	
	@Test(expected=PSD2Exception.class)
	public void validateSchemeNameWithIdentification_invalidSortCodeAccountNumberErrorCodeNotNull() {
		ReflectionTestUtils.setField(commonCardValidations, "sortCodeAccountNumberPattern", "^[0-9]{6}[0-9]{8}$");
		commonCardValidations.validateSchemeNameWithIdentification(CardSetupConstants.SORTCODEACCOUNTNUMBER,
				"0901280374552112323243243", OBErrorCodeEnum.UK_OBIE_UNSUPPORTED_ACCOUNTIDENTIFIER);
	}

	@Test
	public void validateSchemeNameWithIdentification_validBBAN() {
		commonCardValidations.validateSchemeNameWithIdentification(CardSetupConstants.UK_OBIE_BBAN, "ABCH12345678901112", null);
	}
	
	@Test(expected=PSD2Exception.class)
	public void validateSchemeNameWithIdentification_validBBAN1() {
		commonCardValidations.validateSchemeNameWithIdentification(CardSetupConstants.UK_OBIE_BBAN, "12", null);
	}
	
	@Test(expected=PSD2Exception.class)
	public void validateSchemeNameWithIdentification_validBBAN2() {
		commonCardValidations.validateSchemeNameWithIdentification(CardSetupConstants.UK_OBIE_BBAN, "12", OBErrorCodeEnum.UK_OBIE_FIELD_EXPECTED);
	}
	

	@Test
	public void validateSchemeNameWithIdentification_validPAN() {
		ReflectionTestUtils.setField(commonCardValidations, "validPanIdentification", "\\d+");
		commonCardValidations.validateSchemeNameWithIdentification(CardSetupConstants.PAN, "123456789", null);
	}
	
	@Test
	public void validateSchemeNameWithIdentification_validPANFull() {
		ReflectionTestUtils.setField(commonCardValidations, "validPanIdentification", "\\d+");
		commonCardValidations.validateSchemeNameWithIdentification(CardSetupConstants.UK_OBIE_PAN, "123456789", null);
	}
	
	@Test(expected=PSD2Exception.class)
	public void validateSchemeNameWithIdentification_invalidPAN() {
		ReflectionTestUtils.setField(commonCardValidations, "validPanIdentification", "\\d+");
		commonCardValidations.validateSchemeNameWithIdentification(CardSetupConstants.PAN, "ABC123456789", OBErrorCodeEnum.UK_OBIE_UNSUPPORTED_ACCOUNTIDENTIFIER);
	}
	
	
	
	@Test(expected = PSD2Exception.class)
	public void validateSchemeNameWithIdentification_blankPAYM() {
		commonCardValidations.validateSchemeNameWithIdentification(CardSetupConstants.UK_OBIE_PAYM, "", null);
	}
	

	@Test
	public void validateSchemeNameWithIdentification_PAYM() {
		commonCardValidations.validateSchemeNameWithIdentification(CardSetupConstants.UK_OBIE_PAYM, "ABCH12345678901112", null);
	}

	@Test(expected=PSD2Exception.class)
	public void validateSchemeNameWithIdentification_invalidSortCodeAccountNumber() {
		ReflectionTestUtils.setField(commonCardValidations, "sortCodeAccountNumberPattern", "^[0-9]{6}[0-9]{8}$");
		commonCardValidations.validateSchemeNameWithIdentification(CardSetupConstants.SORTCODEACCOUNTNUMBER,
				"Invalid", null);
	}
	
	
	
	
	@Test
	public void validateCompletionDate_validDateTime() {
		commonCardValidations.validateAndParseDateTimeFormatForResponse("1981-03-20T06:06:06+00:00");
	}

	@Test
	public void validateCompletionDateTime_offsetAbsent() {
		commonCardValidations.validateAndParseDateTimeFormatForResponse("1981-03-20T06:06:06");
	}
	
	/*@Test
	public void validateCompletionDateTime_invalidDate() {
		commonAccountValidations.validateAndParseDateTimeFormatForResponse("191-03-20T06:06:06");
	}*/
	
	@Test(expected=PSD2Exception.class)
	public void testValidateAccountRequestIdException() {
		String accountRequestId = "56981c81-a529-45be-9ea8-88addc01858$";
		ReflectionTestUtils.setField(commonCardValidations, "validConsentIdCharsRegEx", "[^A-Za-z0-9-]+");
		commonCardValidations.validateUniqueUUID(accountRequestId);
	}
	
	@Test
	public void testValidateAccountRequestId() {
		String accountRequestId = "56981c81-a529-45be-9ea8-88addc018585";
		ReflectionTestUtils.setField(commonCardValidations, "validConsentIdCharsRegEx", "[a-zA-Z0-9-]{1,40}");
		commonCardValidations.validateUniqueUUID(accountRequestId);
	}
	
	@Test(expected=PSD2Exception.class)
	public void testValidateAccountRequestIdInvalidLengthException() {
		String accountRequestId = "56981c81-a529-45be-9ea8-88addc0185856981c81-a529-45be-9ea8-88addc0185856981c81-a529-45be-9ea8-88addc0185856981c81-a529-45be-9ea8-88addc01858";
		commonCardValidations.validateUniqueUUID(accountRequestId);
	}
	
	@Test(expected=PSD2Exception.class)
	public void testValidateAccountRequestIdInvalidLengthZeroException() {
		String accountRequestId = "";
		commonCardValidations.validateUniqueUUID(accountRequestId);
	}
	
	@After
	public void tearDown() throws Exception {
	}
	
	@Test
	public void validateAmount() {
		commonCardValidations.validateAmount("10.00"); 
	}
	
	@Test
	public void validateAmount_nullValue() {
		commonCardValidations.validateAmount(null); 
	}
	
	@Test
	public void validateAmount_blankValue() {
		commonCardValidations.validateAmount(""); 
	}
	
	@Test(expected=PSD2Exception.class)
	public void validateAmount_invalidValue() {
		commonCardValidations.validateAmount("ABCDEFGH.23454");
	}
	
	@Test
	public void getValidSchemeNameList() {
		assertNotNull(commonCardValidations.getValidSchemeNameList()); 
	}
	
	@Test
	public void validateSubmissionResponseParams_validValues() {
		ReflectionTestUtils.setField(commonCardValidations, "reqValidationEnabled", true);
		OBFundsConfirmationResponse1 oBFundsConfirmationResponse1 = new OBFundsConfirmationResponse1();
		oBFundsConfirmationResponse1.setData(new OBFundsConfirmationResponse1Data());
		oBFundsConfirmationResponse1.getData().setInstructedAmount(new OBFundsConfirmation1DataInstructedAmount());
		oBFundsConfirmationResponse1.getData().setCreationDateTime("1981-03-20T06:06:06+00:00");
		oBFundsConfirmationResponse1.getData().getInstructedAmount().setCurrency("GBP");
		oBFundsConfirmationResponse1.getData().getInstructedAmount().setAmount("10.00");
		commonCardValidations.validateSubmissionResponseParams(oBFundsConfirmationResponse1);
	}
	
	@Test
	public void validateSubmissionResponseParams_blankCreationDateValue() {
		ReflectionTestUtils.setField(commonCardValidations, "reqValidationEnabled", true);
		OBFundsConfirmationResponse1 oBFundsConfirmationResponse1 = new OBFundsConfirmationResponse1();
		oBFundsConfirmationResponse1.setData(new OBFundsConfirmationResponse1Data());
		oBFundsConfirmationResponse1.getData().setInstructedAmount(new OBFundsConfirmation1DataInstructedAmount());
		oBFundsConfirmationResponse1.getData().setCreationDateTime("");
		commonCardValidations.validateSubmissionResponseParams(oBFundsConfirmationResponse1);
	}
	
	@Test
	public void validateSubmissionResponseParams_invalidCreationDateValue() {
		ReflectionTestUtils.setField(commonCardValidations, "reqValidationEnabled", true);
		OBFundsConfirmationResponse1 oBFundsConfirmationResponse1 = new OBFundsConfirmationResponse1();
		oBFundsConfirmationResponse1.setData(new OBFundsConfirmationResponse1Data());
		oBFundsConfirmationResponse1.getData().setInstructedAmount(new OBFundsConfirmation1DataInstructedAmount());
		oBFundsConfirmationResponse1.getData().setCreationDateTime("1-03-20T06:06:06+00:00");
		commonCardValidations.validateSubmissionResponseParams(oBFundsConfirmationResponse1);
	}
	
	@Test
	public void validateSubmissionResponseParams_nullCreationDateValue() {
		ReflectionTestUtils.setField(commonCardValidations, "reqValidationEnabled", true);
		OBFundsConfirmationResponse1 oBFundsConfirmationResponse1 = new OBFundsConfirmationResponse1();
		oBFundsConfirmationResponse1.setData(new OBFundsConfirmationResponse1Data());
		oBFundsConfirmationResponse1.getData().setInstructedAmount(new OBFundsConfirmation1DataInstructedAmount());
		oBFundsConfirmationResponse1.getData().setCreationDateTime(null);
		commonCardValidations.validateSubmissionResponseParams(oBFundsConfirmationResponse1);
	}
	
	@Test(expected=PSD2Exception.class)
	public void validateSubmissionResponseParams_invalidCurrencyValue() {
		OBFundsConfirmationResponse1 oBFundsConfirmationResponse1 = new OBFundsConfirmationResponse1();
		oBFundsConfirmationResponse1.setData(new OBFundsConfirmationResponse1Data());
		oBFundsConfirmationResponse1.getData().setInstructedAmount(new OBFundsConfirmation1DataInstructedAmount());
		oBFundsConfirmationResponse1.getData().setCreationDateTime("1981-03-20T06:06:06+00:00");
		oBFundsConfirmationResponse1.getData().getInstructedAmount().setCurrency("ABC");
		ReflectionTestUtils.setField(commonCardValidations, "reqValidationEnabled", true);
		commonCardValidations.validateSubmissionResponseParams(oBFundsConfirmationResponse1);
	}
	
	@Test
	public void validateSubmissionResponseParams_blankCurrencyValue() {
		ReflectionTestUtils.setField(commonCardValidations, "reqValidationEnabled", true);
		OBFundsConfirmationResponse1 oBFundsConfirmationResponse1 = new OBFundsConfirmationResponse1();
		oBFundsConfirmationResponse1.setData(new OBFundsConfirmationResponse1Data());
		oBFundsConfirmationResponse1.getData().setInstructedAmount(new OBFundsConfirmation1DataInstructedAmount());
		oBFundsConfirmationResponse1.getData().setCreationDateTime("1981-03-20T06:06:06+00:00");
		oBFundsConfirmationResponse1.getData().getInstructedAmount().setCurrency("");
		commonCardValidations.validateSubmissionResponseParams(oBFundsConfirmationResponse1);
	}
	
	@Test
	public void validateSubmissionResponseParams_nullCurrencyValue() {
		ReflectionTestUtils.setField(commonCardValidations, "reqValidationEnabled", true);
		OBFundsConfirmationResponse1 oBFundsConfirmationResponse1 = new OBFundsConfirmationResponse1();
		oBFundsConfirmationResponse1.setData(new OBFundsConfirmationResponse1Data());
		oBFundsConfirmationResponse1.getData().setInstructedAmount(new OBFundsConfirmation1DataInstructedAmount());
		oBFundsConfirmationResponse1.getData().setCreationDateTime("1981-03-20T06:06:06+00:00");
		oBFundsConfirmationResponse1.getData().getInstructedAmount().setCurrency(null);
		commonCardValidations.validateSubmissionResponseParams(oBFundsConfirmationResponse1);
	}
	
	@Test(expected=PSD2Exception.class)
	public void validateSubmissionResponseParams_invalidAmountValue() {
		ReflectionTestUtils.setField(commonCardValidations, "reqValidationEnabled", true);
		OBFundsConfirmationResponse1 oBFundsConfirmationResponse1 = new OBFundsConfirmationResponse1();
		oBFundsConfirmationResponse1.setData(new OBFundsConfirmationResponse1Data());
		oBFundsConfirmationResponse1.getData().setInstructedAmount(new OBFundsConfirmation1DataInstructedAmount());
		oBFundsConfirmationResponse1.getData().setCreationDateTime("1981-03-20T06:06:06+00:00");
		oBFundsConfirmationResponse1.getData().getInstructedAmount().setCurrency("GBP");
		oBFundsConfirmationResponse1.getData().getInstructedAmount().setAmount("ABC");
		commonCardValidations.validateSubmissionResponseParams(oBFundsConfirmationResponse1);
	}
	
	@Test
	public void validateSubmissionResponseParams_blankAmountValue() {
		ReflectionTestUtils.setField(commonCardValidations, "reqValidationEnabled", true);
		OBFundsConfirmationResponse1 oBFundsConfirmationResponse1 = new OBFundsConfirmationResponse1();
		oBFundsConfirmationResponse1.setData(new OBFundsConfirmationResponse1Data());
		oBFundsConfirmationResponse1.getData().setInstructedAmount(new OBFundsConfirmation1DataInstructedAmount());
		oBFundsConfirmationResponse1.getData().setCreationDateTime("1981-03-20T06:06:06+00:00");
		oBFundsConfirmationResponse1.getData().getInstructedAmount().setCurrency("GBP");
		oBFundsConfirmationResponse1.getData().getInstructedAmount().setAmount("");
		commonCardValidations.validateSubmissionResponseParams(oBFundsConfirmationResponse1);
	}
	
	@Test
	public void validateSubmissionResponseParams_nullAmountValue() {
		OBFundsConfirmationResponse1 oBFundsConfirmationResponse1 = new OBFundsConfirmationResponse1();
		oBFundsConfirmationResponse1.setData(new OBFundsConfirmationResponse1Data());
		oBFundsConfirmationResponse1.getData().setCreationDateTime("1981-03-20T06:06:06+00:00");
		oBFundsConfirmationResponse1.getData().setInstructedAmount(new OBFundsConfirmation1DataInstructedAmount());
		oBFundsConfirmationResponse1.getData().getInstructedAmount().setCurrency("GBP");
		oBFundsConfirmationResponse1.getData().getInstructedAmount().setAmount(null);
		ReflectionTestUtils.setField(commonCardValidations, "reqValidationEnabled", true);
		commonCardValidations.validateSubmissionResponseParams(oBFundsConfirmationResponse1);
	}

	@Test
	public void validateResponseParams_validValues() {
		OBFundsConfirmationConsentResponse1 obFundsConfirmationConsentResponse1 = new OBFundsConfirmationConsentResponse1();
		obFundsConfirmationConsentResponse1.setData(new OBFundsConfirmationConsentResponse1Data());
		obFundsConfirmationConsentResponse1.getData().setCreationDateTime("1981-03-20T06:06:06+00:00");
		obFundsConfirmationConsentResponse1.getData().setExpirationDateTime("1981-03-20T06:06:06+00:00");
		obFundsConfirmationConsentResponse1.getData().setStatusUpdateDateTime("1981-03-20T06:06:06+00:00");
		ReflectionTestUtils.setField(commonCardValidations, "reqValidationEnabled", true);
		commonCardValidations.validateResponseParams(obFundsConfirmationConsentResponse1);
	}
	
	@Test
	public void validateResponseParams_nullCreationDateTime() {
		OBFundsConfirmationConsentResponse1 obFundsConfirmationConsentResponse1 = new OBFundsConfirmationConsentResponse1();
		obFundsConfirmationConsentResponse1.setData(new OBFundsConfirmationConsentResponse1Data());
		obFundsConfirmationConsentResponse1.getData().setCreationDateTime(null);
		obFundsConfirmationConsentResponse1.getData().setExpirationDateTime("1981-03-20T06:06:06+00:00");
		obFundsConfirmationConsentResponse1.getData().setStatusUpdateDateTime("1981-03-20T06:06:06+00:00");
		ReflectionTestUtils.setField(commonCardValidations, "reqValidationEnabled", true);
		commonCardValidations.validateResponseParams(obFundsConfirmationConsentResponse1);
	}
	
	@Test
	public void validateResponseParams_blankCreationDateTime() {
		OBFundsConfirmationConsentResponse1 obFundsConfirmationConsentResponse1 = new OBFundsConfirmationConsentResponse1();
		obFundsConfirmationConsentResponse1.setData(new OBFundsConfirmationConsentResponse1Data());
		obFundsConfirmationConsentResponse1.getData().setCreationDateTime("");
		obFundsConfirmationConsentResponse1.getData().setExpirationDateTime("1981-03-20T06:06:06+00:00");
		obFundsConfirmationConsentResponse1.getData().setStatusUpdateDateTime("1981-03-20T06:06:06+00:00");
		ReflectionTestUtils.setField(commonCardValidations, "reqValidationEnabled", true);
		commonCardValidations.validateResponseParams(obFundsConfirmationConsentResponse1);
	}
	
	@Test
	public void validateResponseParams_invalidCreationDateTime() {
		OBFundsConfirmationConsentResponse1 obFundsConfirmationConsentResponse1 = new OBFundsConfirmationConsentResponse1();
		obFundsConfirmationConsentResponse1.setData(new OBFundsConfirmationConsentResponse1Data());
		obFundsConfirmationConsentResponse1.getData().setCreationDateTime("ABC");
		obFundsConfirmationConsentResponse1.getData().setExpirationDateTime("1981-03-20T06:06:06+00:00");
		obFundsConfirmationConsentResponse1.getData().setStatusUpdateDateTime("1981-03-20T06:06:06+00:00");
		ReflectionTestUtils.setField(commonCardValidations, "reqValidationEnabled", true);
		commonCardValidations.validateResponseParams(obFundsConfirmationConsentResponse1);
	}
	
	@Test
	public void validateResponseParams_nullExpirationDateTime() {
		OBFundsConfirmationConsentResponse1 obFundsConfirmationConsentResponse1 = new OBFundsConfirmationConsentResponse1();
		obFundsConfirmationConsentResponse1.setData(new OBFundsConfirmationConsentResponse1Data());
		obFundsConfirmationConsentResponse1.getData().setCreationDateTime("1981-03-20T06:06:06+00:00");
		obFundsConfirmationConsentResponse1.getData().setExpirationDateTime(null);
		obFundsConfirmationConsentResponse1.getData().setStatusUpdateDateTime("1981-03-20T06:06:06+00:00");
		ReflectionTestUtils.setField(commonCardValidations, "reqValidationEnabled", true);
		commonCardValidations.validateResponseParams(obFundsConfirmationConsentResponse1);
	}
	
	@Test
	public void validateResponseParams_blankExpirationDateTime() {
		OBFundsConfirmationConsentResponse1 obFundsConfirmationConsentResponse1 = new OBFundsConfirmationConsentResponse1();
		obFundsConfirmationConsentResponse1.setData(new OBFundsConfirmationConsentResponse1Data());
		obFundsConfirmationConsentResponse1.getData().setCreationDateTime("1981-03-20T06:06:06+00:00");
		obFundsConfirmationConsentResponse1.getData().setExpirationDateTime("");
		obFundsConfirmationConsentResponse1.getData().setStatusUpdateDateTime("1981-03-20T06:06:06+00:00");
		ReflectionTestUtils.setField(commonCardValidations, "reqValidationEnabled", true);
		commonCardValidations.validateResponseParams(obFundsConfirmationConsentResponse1);
	}
	
	@Test
	public void validateResponseParams_invalidExpirationDateTime() {
		OBFundsConfirmationConsentResponse1 obFundsConfirmationConsentResponse1 = new OBFundsConfirmationConsentResponse1();
		obFundsConfirmationConsentResponse1.setData(new OBFundsConfirmationConsentResponse1Data());
		obFundsConfirmationConsentResponse1.getData().setCreationDateTime("1981-03-20T06:06:06+00:00");
		obFundsConfirmationConsentResponse1.getData().setExpirationDateTime("ABC");
		obFundsConfirmationConsentResponse1.getData().setStatusUpdateDateTime("1981-03-20T06:06:06+00:00");
		ReflectionTestUtils.setField(commonCardValidations, "reqValidationEnabled", true);
		commonCardValidations.validateResponseParams(obFundsConfirmationConsentResponse1);
	}
	
	@Test
	public void validateResponseParams_invalidStatusUpdateDateTime() {
		OBFundsConfirmationConsentResponse1 obFundsConfirmationConsentResponse1 = new OBFundsConfirmationConsentResponse1();
		obFundsConfirmationConsentResponse1.setData(new OBFundsConfirmationConsentResponse1Data());
		obFundsConfirmationConsentResponse1.getData().setCreationDateTime("1981-03-20T06:06:06+00:00");
		obFundsConfirmationConsentResponse1.getData().setExpirationDateTime("1981-03-20T06:06:06+00:00");
		obFundsConfirmationConsentResponse1.getData().setStatusUpdateDateTime("ABC");
		ReflectionTestUtils.setField(commonCardValidations, "reqValidationEnabled", true);
		commonCardValidations.validateResponseParams(obFundsConfirmationConsentResponse1);
	}
	
	@Test
	public void validateResponseParams_nullStatusUpdateDateTime() {
		OBFundsConfirmationConsentResponse1 obFundsConfirmationConsentResponse1 = new OBFundsConfirmationConsentResponse1();
		obFundsConfirmationConsentResponse1.setData(new OBFundsConfirmationConsentResponse1Data());
		obFundsConfirmationConsentResponse1.getData().setCreationDateTime("1981-03-20T06:06:06+00:00");
		obFundsConfirmationConsentResponse1.getData().setExpirationDateTime("1981-03-20T06:06:06+00:00");
		obFundsConfirmationConsentResponse1.getData().setStatusUpdateDateTime(null);
		ReflectionTestUtils.setField(commonCardValidations, "reqValidationEnabled", true);
		commonCardValidations.validateResponseParams(obFundsConfirmationConsentResponse1);
	}
	
	@Test
	public void validateResponseParams_blankStatusUpdateDateTime() {
		OBFundsConfirmationConsentResponse1 obFundsConfirmationConsentResponse1 = new OBFundsConfirmationConsentResponse1();
		obFundsConfirmationConsentResponse1.setData(new OBFundsConfirmationConsentResponse1Data());
		obFundsConfirmationConsentResponse1.getData().setCreationDateTime("1981-03-20T06:06:06+00:00");
		obFundsConfirmationConsentResponse1.getData().setExpirationDateTime("1981-03-20T06:06:06+00:00");
		obFundsConfirmationConsentResponse1.getData().setStatusUpdateDateTime("");
		ReflectionTestUtils.setField(commonCardValidations, "reqValidationEnabled", true);
		commonCardValidations.validateResponseParams(obFundsConfirmationConsentResponse1);
	}

	@Test
	public void validateSubmissionRequestParams() {
		OBFundsConfirmation1 oBFundsConfirmation1 = new OBFundsConfirmation1();
		oBFundsConfirmation1.setData(new OBFundsConfirmation1Data());
		oBFundsConfirmation1.getData().setConsentId("c95199c8-e6e1-4eac-afd4-95cfe972bffa");
		ReflectionTestUtils.setField(commonCardValidations, "fundsConsentIdRegexValidator", "^[a-zA-Z0-9-_]{1,128}$"); 		
		ReflectionTestUtils.setField(commonCardValidations, "reqValidationEnabled", true);
		commonCardValidations.validateSubmissionRequestParams(oBFundsConfirmation1);
	}

	@Test(expected=PSD2Exception.class)
	public void validateSubmissionRequestParams_NullData() {
		OBFundsConfirmation1 oBFundsConfirmation1 = new OBFundsConfirmation1();
		ReflectionTestUtils.setField(commonCardValidations, "reqValidationEnabled", true);
		commonCardValidations.validateSubmissionRequestParams(oBFundsConfirmation1);
	}
	
	@Test (expected=PSD2Exception.class)
	public void validateSubmissionRequestParams_NullParam() {
		OBFundsConfirmation1 oBFundsConfirmation1 = null;
		ReflectionTestUtils.setField(commonCardValidations, "reqValidationEnabled", true);
		commonCardValidations.validateSubmissionRequestParams(oBFundsConfirmation1);
	}
	
	@Test
	public void validateRequestParams() {
		OBFundsConfirmationConsent1 fundsConfirmationConsentPOSTRequest = new OBFundsConfirmationConsent1(); 
		fundsConfirmationConsentPOSTRequest.setData(new OBFundsConfirmationConsent1Data());
		fundsConfirmationConsentPOSTRequest.getData().setDebtorAccount(new OBFundsConfirmationConsent1DataDebtorAccount());
		fundsConfirmationConsentPOSTRequest.getData().getDebtorAccount().setIdentification("GB29NWBK60161331926819");
		fundsConfirmationConsentPOSTRequest.getData().getDebtorAccount().setSchemeName("IBAN");
		ReflectionTestUtils.setField(commonCardValidations, "reqValidationEnabled", true);
		commonCardValidations.validateRequestParams(fundsConfirmationConsentPOSTRequest);
	}
	
	@Test(expected=PSD2Exception.class)
	public void validateRequestParams_NullParam() {
		OBFundsConfirmationConsent1 fundsConfirmationConsentPOSTRequest = null; 
		ReflectionTestUtils.setField(commonCardValidations, "reqValidationEnabled", true);
		commonCardValidations.validateRequestParams(fundsConfirmationConsentPOSTRequest);
	}
	
	@Test
	public void validateRequestParams_NullDebtorAccount() {
		OBFundsConfirmationConsent1 fundsConfirmationConsentPOSTRequest = new OBFundsConfirmationConsent1(); 
		fundsConfirmationConsentPOSTRequest.setData(new OBFundsConfirmationConsent1Data());
		ReflectionTestUtils.setField(commonCardValidations, "reqValidationEnabled", true);
		commonCardValidations.validateRequestParams(fundsConfirmationConsentPOSTRequest);
	}
	
	@Test(expected=PSD2Exception.class)
	public void validateRequestParams_NullData() {
		OBFundsConfirmationConsent1 fundsConfirmationConsentPOSTRequest = new OBFundsConfirmationConsent1(); 
		ReflectionTestUtils.setField(commonCardValidations, "reqValidationEnabled", true);
		commonCardValidations.validateRequestParams(fundsConfirmationConsentPOSTRequest);
	}
	
	@Test
	public void validateFormingConsentInputs() {
		OBFundsConfirmationConsent1 fundsConfirmationConsentPOSTRequest = new OBFundsConfirmationConsent1(); 
		fundsConfirmationConsentPOSTRequest.setData(new OBFundsConfirmationConsent1Data());
		fundsConfirmationConsentPOSTRequest.getData().setExpirationDateTime("1981-03-20T06:06:06+00:00");
		ReflectionTestUtils.setField(commonCardValidations, "reqValidationEnabled", true);
		commonCardValidations.validateFormingConsentInputs(fundsConfirmationConsentPOSTRequest);
	}
	
	@Test
	public void validateFormingConsentInputs_BlankExpirationDateTime() {
		OBFundsConfirmationConsent1 fundsConfirmationConsentPOSTRequest = new OBFundsConfirmationConsent1(); 
		fundsConfirmationConsentPOSTRequest.setData(new OBFundsConfirmationConsent1Data());
		fundsConfirmationConsentPOSTRequest.getData().setExpirationDateTime("");
		ReflectionTestUtils.setField(commonCardValidations, "reqValidationEnabled", true);
		commonCardValidations.validateFormingConsentInputs(fundsConfirmationConsentPOSTRequest);
	}
	
	@Test
	public void validateFormingConsentInputs_NullExpirationDateTime() {
		OBFundsConfirmationConsent1 fundsConfirmationConsentPOSTRequest = new OBFundsConfirmationConsent1(); 
		fundsConfirmationConsentPOSTRequest.setData(new OBFundsConfirmationConsent1Data());
		fundsConfirmationConsentPOSTRequest.getData().setExpirationDateTime(null);
		ReflectionTestUtils.setField(commonCardValidations, "reqValidationEnabled", true);
		commonCardValidations.validateFormingConsentInputs(fundsConfirmationConsentPOSTRequest);
	}
	
	@Test(expected=PSD2Exception.class)
	public void validateFormingConsentInputs_NullParam() {
		OBFundsConfirmationConsent1 fundsConfirmationConsentPOSTRequest = null; 
		ReflectionTestUtils.setField(commonCardValidations, "reqValidationEnabled", true);
		commonCardValidations.validateFormingConsentInputs(fundsConfirmationConsentPOSTRequest);
	}
	
	@Test(expected=PSD2Exception.class)
	public void validateFormingConsentInputs_NullData() {
		OBFundsConfirmationConsent1 fundsConfirmationConsentPOSTRequest = new OBFundsConfirmationConsent1(); 
		ReflectionTestUtils.setField(commonCardValidations, "reqValidationEnabled", true);
		commonCardValidations.validateFormingConsentInputs(fundsConfirmationConsentPOSTRequest);
	}	
	
	@Test(expected=PSD2Exception.class)
	public void isValidCurrencyTest(){
		commonCardValidations.isValidCurrency(null, null);
	}
	
	@Test(expected=PSD2Exception.class)
	public void isValidCurrencyExceptionTest(){
		commonCardValidations.isValidCurrency("1234", OBErrorCodeEnum.UK_OBIE_FIELD_EXPECTED);
	}
	
	@Test(expected=PSD2Exception.class)
	public void isValidCurrency1Test(){
		commonCardValidations.isValidCurrency(null, OBErrorCodeEnum.UK_OBIE_FIELD_EXPECTED);
	}
	
	@Test(expected=PSD2Exception.class)
	public void validatePANExceptionTest(){
		ReflectionTestUtils.setField(commonCardValidations, "validPanIdentification", "1234");
		commonCardValidations.validatePAN("12345", null);
	}
	
	@Test(expected=PSD2Exception.class)
	public void validateSchemeNameWithIdentificationExceptionTest(){
		List<String> list = new ArrayList<>();
		list.add("BBAN");
		ReflectionTestUtils.setField(commonCardValidations, "validSchemeNameList", list);
		commonCardValidations.validateSchemeNameWithIdentification("IBAN", "123145", null);
	}
	
	
	@Test
	public void validateSchemeNameWithIdentification_validIBAN_XK() {
		commonCardValidations.validateSchemeNameWithIdentification("IBAN", "XK051212012345678906",
				null);
	}
	
	@Test
	public void validateSchemeNameWithIdentification_validIBAN_VG() {
		commonCardValidations.validateSchemeNameWithIdentification("IBAN", "VG96VPVG0000012345678901",
				null);
	}
	
	@Test
	public void validateSchemeNameWithIdentification_validIBAN_UA() {
		commonCardValidations.validateSchemeNameWithIdentification("IBAN", "UA213223130000026007233566001",
				null);
	}
	
	@Test
	public void validateSchemeNameWithIdentification_validIBAN_TR() {
		commonCardValidations.validateSchemeNameWithIdentification("IBAN", "TR330006100519786457841326",
				null);
	}
	
	@Test
	public void validateSchemeNameWithIdentification_validIBAN_TN() {
		commonCardValidations.validateSchemeNameWithIdentification("IBAN", "TN5910006035183598478831",
				null);
	}
	
	@Test
	public void validateSchemeNameWithIdentification_validIBAN_TL() {
		commonCardValidations.validateSchemeNameWithIdentification("IBAN", "TL380080012345678910157",
				null);
	}
	
	@Test
	public void validateSchemeNameWithIdentification_validIBAN_ST() {
		commonCardValidations.validateSchemeNameWithIdentification("IBAN", "ST68000100010051845310112",
				null);
	}
	
	@Test
	public void validateSchemeNameWithIdentification_validIBAN_SM() {
		commonCardValidations.validateSchemeNameWithIdentification("IBAN", "SM86U0322509800000000270100",
				null);
	}
	
	@Test
	public void validateSchemeNameWithIdentification_validIBAN_SK() {
		commonCardValidations.validateSchemeNameWithIdentification("IBAN", "SK3112000000198742637541",
				null);
	}
	
	@Test
	public void validateSchemeNameWithIdentification_validIBAN_SI() {
		commonCardValidations.validateSchemeNameWithIdentification("IBAN", "SI56263300012039086",
				null);
	}
	
	@Test
	public void validateSchemeNameWithIdentification_validIBAN_SE() {
		commonCardValidations.validateSchemeNameWithIdentification("IBAN", "SE4550000000058398257466",
				null);
	}
	
	@Test
	public void validateSchemeNameWithIdentification_validIBAN_SC() {
		commonCardValidations.validateSchemeNameWithIdentification("IBAN", "SC18SSCB11010000000000001497USD",
				null);
	}
	
	@Test
	public void validateSchemeNameWithIdentification_validIBAN_SA() {
		commonCardValidations.validateSchemeNameWithIdentification("IBAN", "SA0380000000608010167519",
				null);
	}
	
	@Test
	public void validateSchemeNameWithIdentification_validIBAN_RS() {
		commonCardValidations.validateSchemeNameWithIdentification("IBAN", "RS35260005601001611379",
				null);
	}
	
	@Test
	public void validateSchemeNameWithIdentification_validIBAN_RO() {
		commonCardValidations.validateSchemeNameWithIdentification("IBAN", "RO49AAAA1B31007593840000",
				null);
	}
	
	@Test
	public void validateSchemeNameWithIdentification_validIBAN_QA() {
		commonCardValidations.validateSchemeNameWithIdentification("IBAN", "QA58DOHB00001234567890ABCDEFG",
				null);
	}
	
	@Test
	public void validateSchemeNameWithIdentification_validIBAN_PT() {
		commonCardValidations.validateSchemeNameWithIdentification("IBAN", "PT50000201231234567890154",
				null);
	}
	
	@Test
	public void validateSchemeNameWithIdentification_validIBAN_PS() {
		commonCardValidations.validateSchemeNameWithIdentification("IBAN", "PS92PALS000000000400123456702",
				null);
	}
	
	@Test
	public void validateSchemeNameWithIdentification_validIBAN_PL() {
		commonCardValidations.validateSchemeNameWithIdentification("IBAN", "PL61109010140000071219812874",
				null);
	}
	
	@Test
	public void validateSchemeNameWithIdentification_validIBAN_PK() {
		commonCardValidations.validateSchemeNameWithIdentification("IBAN", "PK36SCBL0000001123456702",
				null);
	}

	@Test
	public void validateIBAN_Andorra() {
		commonCardValidations.validateSchemeNameWithIdentification("IBAN", "AD1200012030200359100100",
				null);
	}
	
	@Test
	public void validateIBAN_United_Arab_Emirates() {
		commonCardValidations.validateSchemeNameWithIdentification("IBAN", "AE070331234567890123456",
				null);
	}
	
	@Test
	public void validateIBAN_Albania() {
		commonCardValidations.validateSchemeNameWithIdentification("IBAN", "AL47212110090000000235698741",
				null);
	}
	
	@Test
	public void validateIBAN_Austria() {
		commonCardValidations.validateSchemeNameWithIdentification("IBAN", "AT611904300234573201",
				null);
	}
	
	@Test
	public void validateIBAN_Republic_Of_Azerbaijan() {
		commonCardValidations.validateSchemeNameWithIdentification("IBAN", "AZ21NABZ00000000137010001944",
				null);
	}
	
	@Test
	public void validateIBAN_Bosnia_Herzegovina() {
		commonCardValidations.validateSchemeNameWithIdentification("IBAN", "BA391290079401028494",
				null);
	}
	
	@Test
	public void validateIBAN_Belgium() {
		commonCardValidations.validateSchemeNameWithIdentification("IBAN", "BE68539007547034",
				null);
	}
	
	@Test
	public void validateIBAN_Bulgaria() {
		commonCardValidations.validateSchemeNameWithIdentification("IBAN", "BG80BNBG96611020345678",
				null);
	}
	
	@Test
	public void validateIBAN_Bahrain() {
		commonCardValidations.validateSchemeNameWithIdentification("IBAN", "BH67BMAG00001299123456",
				null);
	}
	
	@Test
	public void validateIBAN_Brazil() {
		commonCardValidations.validateSchemeNameWithIdentification("IBAN", "BR9700360305000010009795493P1",
				null);
	}
	
	@Test
	public void validateIBAN_Belarus() {
		commonCardValidations.validateSchemeNameWithIdentification("IBAN", "BY13NBRB3600900000002Z00AB00",
				null);
	}
	
	@Test
	public void validateIBAN_Switzerland() {
		commonCardValidations.validateSchemeNameWithIdentification("IBAN", "CH9300762011623852957",
				null);
	}
	
	@Test
	public void validateIBAN_Costa_Rica() {
		commonCardValidations.validateSchemeNameWithIdentification("IBAN", "CR05015202001026284066",
				null);
	}
	
	@Test
	public void validateIBAN_Cyprus() {
		commonCardValidations.validateSchemeNameWithIdentification("IBAN", "CY17002001280000001200527600",
				null);
	}
	
	@Test
	public void validateIBAN_CzechRepublic() {
		commonCardValidations.validateSchemeNameWithIdentification("IBAN", "CZ6508000000192000145399",
				null);
	}
	
	@Test
	public void validateIBAN_Germany() {
		commonCardValidations.validateSchemeNameWithIdentification("IBAN", "DE89370400440532013000",
				null);
	}
	
	@Test
	public void validateIBAN_Denmark() {
		commonCardValidations.validateSchemeNameWithIdentification("IBAN", "DK5000400440116243",
				null);
	}
	
	@Test
	public void validateIBAN_Dominican_Republic() {
		commonCardValidations.validateSchemeNameWithIdentification("IBAN", "DO28BAGR00000001212453611324",
				null);
	}
	
	@Test
	public void validateIBAN_Estonia() {
		commonCardValidations.validateSchemeNameWithIdentification("IBAN", "EE382200221020145685",
				null);
	}
	
	@Test
	public void validateIBAN_Spain() {
		commonCardValidations.validateSchemeNameWithIdentification("IBAN", "ES9121000418450200051332",
				null);
	}
	
	@Test
	public void validateIBAN_Finland() {
		commonCardValidations.validateSchemeNameWithIdentification("IBAN", "FI2112345600000785",
				null);
	}
	
	@Test
	public void validateIBAN_France() {
		commonCardValidations.validateSchemeNameWithIdentification("IBAN", "FR1420041010050500013M02606",
				null);
	}
	
	@Test
	public void validateIBAN_UnitedKingdom() {
		commonCardValidations.validateSchemeNameWithIdentification("IBAN", "GB29NWBK60161331926819",
				null);
	}
	
	@Test
	public void validateIBAN_Georgia() {
		commonCardValidations.validateSchemeNameWithIdentification("IBAN", "GE29NB0000000101904917",
				null);
	}
	
	@Test
	public void validateIBAN_Gibraltar() {
		commonCardValidations.validateSchemeNameWithIdentification("IBAN", "GI75NWBK000000007099453",
				null);
	}
	
	@Test
	public void validateIBAN_Greece() {
		commonCardValidations.validateSchemeNameWithIdentification("IBAN", "GR1601101250000000012300695",
				null);
	}
	
	@Test
	public void validateIBAN_Guatemala() {
		commonCardValidations.validateSchemeNameWithIdentification("IBAN", "GT82TRAJ01020000001210029690",
				null);
	}
	
	@Test
	public void validateIBAN_Croatia() {
		commonCardValidations.validateSchemeNameWithIdentification("IBAN", "HR1210010051863000160",
				null);
	}
	
	@Test
	public void validateIBAN_Hungary() {
		commonCardValidations.validateSchemeNameWithIdentification("IBAN", "HU42117730161111101800000000",
				null);
	}
	
	@Test
	public void validateIBAN_Israel() {
		commonCardValidations.validateSchemeNameWithIdentification("IBAN", "IL620108000000099999999",
				null);
	}
	@Test
	public void validateIBAN_Iceland() {
		commonCardValidations.validateSchemeNameWithIdentification("IBAN", "IS140159260076545510730339",
				null);
	}
	@Test
	public void validateIBAN_Italy() {
		commonCardValidations.validateSchemeNameWithIdentification("IBAN", "IT60X0542811101000000123456",
				null);
	}
	
	@Test
	public void validateIBAN_Iraq() {
		commonCardValidations.validateSchemeNameWithIdentification("IBAN", "IQ98NBIQ850123456789012",
				null);
	}
	
	@Test
	public void validateIBAN_Jordan() {
		commonCardValidations.validateSchemeNameWithIdentification("IBAN", "JO94CBJO0010000000000131000302",
				null);
	}
	
	@Test
	public void validateIBAN_Kuwait() {
		commonCardValidations.validateSchemeNameWithIdentification("IBAN", "KW81CBKU0000000000001234560101",
				null);
	}
	
	@Test
	public void validateIBAN_Kazakhstan() {
		commonCardValidations.validateSchemeNameWithIdentification("IBAN", "KZ86125KZT5004100100",
				null);
	}
	
	@Test
	public void validateIBAN_Lebanon() {
		commonCardValidations.validateSchemeNameWithIdentification("IBAN", "LB62099900000001001901229114",
				null);
	}
	
	@Test
	public void validateIBAN_Saint_Lucia() {
		commonCardValidations.validateSchemeNameWithIdentification("IBAN", "LC55HEMM000100010012001200023015",
				null);
	}
	
	@Test
	public void validateIBAN_Liechtenstein() {
		commonCardValidations.validateSchemeNameWithIdentification("IBAN", "LI21088100002324013AA",
				null);
	}
	
	@Test
	public void validateIBAN_Lithuania() {
		commonCardValidations.validateSchemeNameWithIdentification("IBAN", "LT121000011101001000",
				null);
	}
	
	@Test
	public void validateIBAN_Luxembourg() {
		commonCardValidations.validateSchemeNameWithIdentification("IBAN", "LU280019400644750000",
				null);
	}
	
	@Test
	public void validateIBAN_Latvia() {
		commonCardValidations.validateSchemeNameWithIdentification("IBAN", "LV80BANK0000435195001",
				null);
	}
	
	@Test
	public void validateIBAN_Monaco() {
		commonCardValidations.validateSchemeNameWithIdentification("IBAN", "MC5811222000010123456789030",
				null);
	}
	
	@Test
	public void validateIBAN_Moldova() {
		commonCardValidations.validateSchemeNameWithIdentification("IBAN", "MD24AG000225100013104168",
				null);
	}
	
	@Test
	public void validateIBAN_Montenegro() {
		commonCardValidations.validateSchemeNameWithIdentification("IBAN", "ME25505000012345678951",
				null);
	}
	
	@Test
	public void validateIBAN_Macedonia() {
		commonCardValidations.validateSchemeNameWithIdentification("IBAN", "MK07250120000058984",
				null);
	}
	
	@Test
	public void validateIBAN_Mauritania() {
		commonCardValidations.validateSchemeNameWithIdentification("IBAN", "MR1300020001010000123456753",
				null);
	}
	
	@Test
	public void validateIBAN_Malta() {
		commonCardValidations.validateSchemeNameWithIdentification("IBAN", "MT84MALT011000012345MTLCAST001S",
				null);
	}
	
	@Test
	public void validateIBAN_Mauritius() {
		commonCardValidations.validateSchemeNameWithIdentification("IBAN", "MU17BOMM0101101030300200000MUR",
				null);
	}
	
	@Test
	public void validateIBAN_Netherlands() {
		commonCardValidations.validateSchemeNameWithIdentification("IBAN", "NL91ABNA0417164300",
				null);
	}
	
	@Test
	public void validateIBAN_Norway() {
		commonCardValidations.validateSchemeNameWithIdentification("IBAN", "NO9386011117947",
				null);
	}
	
	@Test
	public void validateIBAN_Denmark_Faroe_Islands() {
		commonCardValidations.validateSchemeNameWithIdentification("IBAN", "FO6264600001631634",
				null);
	}
	
	@Test
	public void validateIBAN_ELSalvador() {
		commonCardValidations.validateSchemeNameWithIdentification("IBAN", "SV62CENR00000000000000700025",
				null);
	}

	@Test
	public void validateIBAN_Vatican() {
		commonCardValidations.validateSchemeNameWithIdentification("IBAN", "VA59001123000012345678",
				null);
	}
	
	@Test(expected = PSD2Exception.class)
	public void validateSchemeNameWithIdentification_invalidIBAN_BIC_SV() {
		commonCardValidations.validateSchemeNameWithIdentification("UK.OBIE.IBAN", "SV62CEMN00000000000000700025", null);
	}
	
	@Test(expected = PSD2Exception.class)
	public void validateSchemeNameWithIdentification_invalidIBAN_Length_VA() {
		commonCardValidations.validateSchemeNameWithIdentification("UK.OBIE.IBAN", "VA5900112300001234567812344", null);
	}
	
	@Test(expected = PSD2Exception.class)
	public void validateSchemeNameWithIdentification_invalidIBAN_Length_null() {
		commonCardValidations.validateSchemeNameWithIdentification("UK.OBIE.IBAN", null, null);
	}
	
	@Test(expected = PSD2Exception.class)
	public void validateSchemeNameWithIdentification_invalidIBAN_Length_blank() {
		commonCardValidations.validateSchemeNameWithIdentification("UK.OBIE.IBAN", "", null);
	}
	
	@Test(expected = PSD2Exception.class)
	public void validateSchemeNameWithIdentification_invalidIBAN_Spaces_SV() {
		commonCardValidations.validateSchemeNameWithIdentification("UK.OBIE.IBAN", "SV62CENR0  00000 00000000700025", null);
	}
	
	@Test
	public void validateSchemeNameWithIdentification_invalidIBAN_LeadingAndTrailingSpaces_SV() {
		commonCardValidations.validateSchemeNameWithIdentification("UK.OBIE.IBAN", "  SV62CENR00000000000000700025     ", null);
	}
	
	@Test
	public void validateSchemeNameWithIdentification_validIBAN_GB() {
		commonCardValidations.validateSchemeNameWithIdentification("IBAN", "GB33BUKB20201555555555",
				null);
	}
	
	@Test
	public void validateSchemeNameWithIdentification_validIBAN_IE() {
		commonCardValidations.validateSchemeNameWithIdentification("IBAN", "IE64IRCE92050112345678",
				null);
	}
	
	@Test(expected = PSD2Exception.class)
	public void validateSchemeNameWithIdentification_invalidIBAN_LengthInvalid_IE() {
		commonCardValidations.validateSchemeNameWithIdentification("IBAN", "IE64ARCE920501123456780", null);
	}
	
	@Test(expected = PSD2Exception.class)
	public void validateSchemeNameWithIdentification_invalidIBAN_FormatInvalid_IE() {
		commonCardValidations.validateSchemeNameWithIdentification("IBAN", "IE64ARCE92050112345@#$", null);
	}
	
	@Test(expected = PSD2Exception.class)
	public void validateSchemeNameWithIdentification_invalidIBAN_BICInvalid_GB() {
		commonCardValidations.validateSchemeNameWithIdentification("IBAN", "GB33BAKB20201555555555", null);
	}
	
	@Test(expected = PSD2Exception.class)
	public void validateSchemeNameWithIdentification_invalidIBAN_LengthInvalid_GB() {
		commonCardValidations.validateSchemeNameWithIdentification("IBAN", "GB33BUKB202015555555556", null);
	}
	
	@Test(expected = PSD2Exception.class)
	public void validateSchemeNameWithIdentification_invalidIBAN_CheckDigitInvalid_GB() {
		commonCardValidations.validateSchemeNameWithIdentification("IBAN", "GB30BUKB20201555555555", null);
	}

	@Test(expected = PSD2Exception.class)
	public void validateSchemeNameWithIdentification_invalidIBAN_LengthInvalid_HR() {
		commonCardValidations.validateSchemeNameWithIdentification("IBAN", "HR17236000011012345651", null);
	}
	
	@Test(expected = PSD2Exception.class)
	public void validateSchemeNameWithIdentification_invalidIBAN_BICInvalid_HR() {
		commonCardValidations.validateSchemeNameWithIdentification("IBAN", "HR1723500001101234565", null);
	}
	
	@Test(expected = PSD2Exception.class)
	public void validateSchemeNameWithIdentification_invalidIBAN_CheckDigitInvalid_HR() {
		commonCardValidations.validateSchemeNameWithIdentification("IBAN", "HR1223600001101234565", null);
	}
	
	@Test(expected = PSD2Exception.class)
	public void validateSchemeNameWithIdentification_invalidIBAN_CountryInvalid() {
		commonCardValidations.validateSchemeNameWithIdentification("IBAN", "BB33BUKB20201555555555", null);
	}

	@Test(expected = PSD2Exception.class)
	public void validateSchemeNameWithIdentification_invalidUKOBIEIBAN_errorCodeAbsent() {
		commonCardValidations.validateSchemeNameWithIdentification("UK.OBIE.IBAN", "Invalid", null);
	}
}