<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">

	<modelVersion>4.0.0</modelVersion>
	<groupId>com.capgemini.psd2</groupId>
	<artifactId>RevokeConsent</artifactId>
	<version>2105</version>
	<packaging>jar</packaging>
	<name>RevokeConsent</name>
	<description>RevokeConsent</description>

	<parent>
		<groupId>org.springframework.boot</groupId>
		<artifactId>spring-boot-starter-parent</artifactId>
		<version>1.5.6.RELEASE</version>
	</parent>

	<properties>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
		<project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
		<java.version>1.8</java.version>
		<mongodb.version>3.3.0</mongodb.version>
		<sonar.core.codeCoveragePlugin>jacoco</sonar.core.codeCoveragePlugin>
		<sonar.jacoco.reportPath>${project.basedir}/../coverage-reports/jacoco.exec</sonar.jacoco.reportPath>
		<sonar.exclusions>
			<!-- Excluding the Constants class from sonar coverage. -->
			**/SecurityConfig.java,
			**/RevokeConsentApplication.java
		</sonar.exclusions>
	</properties>

	<dependencyManagement>
		<dependencies>
			<dependency>
				<groupId>org.springframework.cloud</groupId>
				<artifactId>spring-cloud-dependencies</artifactId>
				<version>Edgware.RELEASE</version>
				<type>pom</type>
				<scope>import</scope>
			</dependency>
		</dependencies>
	</dependencyManagement>

	<dependencies>

		<!-- For web integration specifically Restful -->
		<dependency>
			<groupId>org.springframework.retry</groupId>
			<artifactId>spring-retry</artifactId>
		</dependency>

		<!-- for security response header -->
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-security</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-web</artifactId>
		</dependency>

		<!-- For restart endpoint enablement with config server -->
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId> spring-boot-starter-actuator</artifactId>
		</dependency>

		<!-- To register the application to eureka server -->
		<dependency>
			<groupId>org.springframework.cloud</groupId>
			<artifactId>spring-cloud-starter-eureka</artifactId>
		</dependency>

		<!-- To connect the api with config server -->
		<dependency>
			<groupId>org.springframework.cloud</groupId>
			<artifactId>spring-cloud-starter-config</artifactId>
		</dependency>

		<!-- To connect with MongoDB for response retrival -->
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-data-mongodb</artifactId>
		</dependency>
		<dependency>
			<groupId>org.iban4j</groupId>
			<artifactId>iban4j</artifactId>
			<version>3.2.1</version>
		</dependency>

		<dependency>
			<groupId>com.fasterxml.jackson.datatype</groupId>
			<artifactId>jackson-datatype-joda</artifactId>
		</dependency>

		<dependency>
			<groupId>com.capgemini.psd2</groupId>
			<artifactId>PSD2RestClient</artifactId>
			<version>2105</version>
		</dependency>

		<!-- Dependency for AWS KMS -->
		<dependency>
			<groupId>com.capgemini.psd2</groupId>
			<artifactId>PSD2KMSUtilities</artifactId>
			<version>2105</version>
		</dependency>

		<dependency>
			<groupId>com.capgemini.psd2</groupId>
			<artifactId>ApiComponents</artifactId>
			<version>2105</version>
		</dependency>

		<!-- <dependency> <groupId>commons-codec</groupId> <artifactId>commons-codec</artifactId> 
			<version>1.11</version> </dependency> -->
		<dependency>
			<groupId>com.capgemini.psd2</groupId>
			<artifactId>CoreSystemAdapter</artifactId>
			<version>2105</version>
		</dependency>
		<!-- For Splunk Logging of the api -->
		<dependency>
			<groupId>com.capgemini.psd2</groupId>
			<artifactId>PSD2SplunkLogger</artifactId>
			<version>2105</version>
		</dependency>

		<!-- To use the project specific cutomized utilities like Exceptionhandling, 
			logging, while fetching the headers from the request and mapping it etc -->
		<dependency>
			<groupId>com.capgemini.psd2</groupId>
			<artifactId>PSD2Utilities</artifactId>
			<version>2105</version>
		</dependency>

		<dependency>
			<groupId>com.capgemini.psd2</groupId>
			<artifactId>PISPUtilities</artifactId>
			<version>2105</version>
		</dependency>

		<!-- added for junit -->
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-test</artifactId>
			<scope>test</scope>
		</dependency>
		<!-- added for SecurityConfig -->
		<dependency>
			<groupId>org.springframework.security</groupId>
			<artifactId>spring-security-ldap</artifactId>
		</dependency>

		<dependency>
			<groupId>com.fasterxml.jackson.datatype</groupId>
			<artifactId>jackson-datatype-jsr310</artifactId>
		</dependency>

		<dependency>
			<groupId>com.capgemini.psd2</groupId>
			<artifactId>RevokeConsentMongoDbAdapter</artifactId>
			<version>2105</version>
		</dependency>

	</dependencies>

	<build>
		<plugins>
			<plugin>
				<groupId>org.springframework.boot</groupId>
				<artifactId>spring-boot-maven-plugin</artifactId>
				<configuration>
					<layout>ZIP</layout>
					<configuration>
					<destFile>${sonar.jacoco.reportPath}</destFile>
					<dataFile>${sonar.jacoco.reportPath}</dataFile>
					<append>true</append>
				   </configuration>
				</configuration>
			</plugin>
			<plugin>
				<groupId>org.jacoco</groupId>
				<artifactId>jacoco-maven-plugin</artifactId>
				<version>0.7.8</version>
				<configuration>
					<destFile>${sonar.jacoco.reportPath}</destFile>
					<dataFile>${sonar.jacoco.reportPath}</dataFile>
					<append>true</append>
				</configuration>
				<executions>
					<execution>
						<id>jacoco-initialize</id>
						<goals>
							<goal>prepare-agent</goal>
						</goals>
					</execution>
					<execution>
						<id>jacoco-site</id>
						<phase>package</phase>
						<goals>
							<goal>report</goal>
						</goals>
					</execution>
				</executions>
			</plugin>
		</plugins>
	</build>

	<modules></modules>
</project>
