package com.capgemini.psd2.revoke.consent.test.validator;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.revoke.consent.validator.RevokeConsentValidatorImpl;

public class RevokeConsentValidatorImplTest {
	@InjectMocks
		private RevokeConsentValidatorImpl impl;
		
		@Before
		public void setUp() {
			MockitoAnnotations.initMocks(this);
		}
		
		@Test(expected=PSD2Exception.class)
		public void validateUniqueUUIDTest1(){
			impl.validateUniqueUUID("");
		}
		
		@Test(expected=PSD2Exception.class)
		public void validateUniqueUUIDTest2(){
			ReflectionTestUtils.setField(impl, "validAccountRequestIdChars", "1234");
			impl.validateUniqueUUID("12345");
		}
		
		@Test(expected=PSD2Exception.class)
		public void validateConsentIdTest(){
			impl.validateConsentId(null);
		}
		
		@Test
		public void validateConsentId1Test(){
			ReflectionTestUtils.setField(impl, "validAccountRequestIdChars", "[a-zA-Z0-9-]{1,40}");
			impl.validateConsentId("123445");
		}
}
