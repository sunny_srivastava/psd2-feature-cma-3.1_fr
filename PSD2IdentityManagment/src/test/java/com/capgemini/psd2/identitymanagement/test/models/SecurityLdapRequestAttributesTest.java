package com.capgemini.psd2.identitymanagement.test.models;

import static org.junit.Assert.assertEquals;

import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import com.capgemini.psd2.identitymanagement.models.CustomClientDetails;
import com.capgemini.psd2.identitymanagement.models.SecurityLdapRequestAttributes;

public class SecurityLdapRequestAttributesTest {

	private CustomClientDetails customClientDetails;
	private SecurityLdapRequestAttributes securityLdapRequestAttributes;
	@Before
	public void setUp() throws Exception {
		securityLdapRequestAttributes = new SecurityLdapRequestAttributes();
		customClientDetails = new CustomClientDetails();
		customClientDetails.setAccessTokenValiditySeconds(12);
		customClientDetails.setLegalEntityName("Moneywise");
		securityLdapRequestAttributes.setCustomClientDetails(customClientDetails);
	}
	
	@Test
	public void test(){
		assertEquals("Moneywise",securityLdapRequestAttributes.getCustomClientDetails().getLegalEntityName());
	}
	
}
