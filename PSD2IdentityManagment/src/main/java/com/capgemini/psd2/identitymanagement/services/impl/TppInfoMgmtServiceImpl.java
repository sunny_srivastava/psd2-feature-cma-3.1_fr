package com.capgemini.psd2.identitymanagement.services.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;

import javax.naming.NamingException;
import javax.naming.directory.Attributes;
import javax.naming.directory.BasicAttributes;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.ldap.core.AttributesMapper;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.query.LdapQuery;
import org.springframework.ldap.query.LdapQueryBuilder;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.oauth2.common.exceptions.OAuth2Exception;
import org.springframework.security.oauth2.provider.token.store.jwk.JwkException;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.capgemini.psd2.identitymanagement.constants.IdentityManagementConstants;
import com.capgemini.psd2.identitymanagement.models.CustomClientDetails;
import com.capgemini.psd2.identitymanagement.models.TppAuthorities;
import com.capgemini.psd2.identitymanagement.services.TppInfoMgmtService;

@Service("tppInfoMgmtService")
@ConditionalOnProperty(name = "app.isSandBoxProfile", havingValue = "false", matchIfMissing = true)
public class TppInfoMgmtServiceImpl implements TppInfoMgmtService {

	@Autowired
	private LdapTemplate ldapTemplate;

	@Override
	public CustomClientDetails findTppInfo(String clientId){
		return findTPPDetail(clientId);
	}

	private CustomClientDetails findTPPDetail(String clientId){
		LdapQuery ldapQuery = LdapQueryBuilder.query().attributes("*","+").
				where(IdentityManagementConstants.CLIENT_ID_LDAP).is(clientId);
		CustomClientDetails customClientDetails = null;
		try {
			List<Object> applicationDetail =  ldapTemplate.search(ldapQuery,
					new AttributesMapper<Object>() {
				@Override
				public Object mapFromAttributes(Attributes attrs)
						throws NamingException {
					return attrs;
				}
			});

			if(applicationDetail==null || applicationDetail.isEmpty() || (BasicAttributes)applicationDetail.get(0)==null || ((BasicAttributes)applicationDetail.get(0)).get(IdentityManagementConstants.IS_MEMBER_OF_LDAP)==null){
				throw OAuth2Exception.create(OAuth2Exception.INVALID_CLIENT, "Invalid client id provided.");
			}

			String memberOf = ((BasicAttributes)applicationDetail.get(0)).get(IdentityManagementConstants.IS_MEMBER_OF_LDAP).get().toString();

			Object groupDetail =  ldapTemplate.lookup(memberOf,
					new AttributesMapper<Object>() {
				@Override
				public Object mapFromAttributes(Attributes attrs)
						throws NamingException {
					return attrs;
				}
			});

			if(groupDetail==null){
				throw OAuth2Exception.create(OAuth2Exception.INVALID_CLIENT, "Invalid client id provided.");
			}

			customClientDetails = populateCustomClientDetails((BasicAttributes)applicationDetail.get(0),(BasicAttributes)groupDetail);
		}
		catch(OAuth2Exception e){
			throw e;
		} 
		catch (Exception e) {
			throw new JwkException(e.getMessage());
		}
		return customClientDetails;
	}

	private CustomClientDetails populateCustomClientDetails(BasicAttributes tppApplication, BasicAttributes tppGroup){

		CustomClientDetails clientDetails=null;

		try {
			String clientId= tppApplication.get(IdentityManagementConstants.CLIENT_ID_LDAP).get()!=null?tppApplication.get(IdentityManagementConstants.CLIENT_ID_LDAP).get().toString():null; 	   
			String clientSecrate= tppApplication.get(IdentityManagementConstants.SECRET_LDAP).get()!=null?tppApplication.get(IdentityManagementConstants.SECRET_LDAP).get().toString():null;
			String registeredRedirectUri= tppApplication.get(IdentityManagementConstants.REDIRECT_URL_LDAP).get()!=null?tppApplication.get(IdentityManagementConstants.REDIRECT_URL_LDAP).get().toString():null;
			String authorities = null;
			if(tppApplication.get(IdentityManagementConstants.AUTHORITIES_LDAP)!=null)
				authorities = tppApplication.get(IdentityManagementConstants.AUTHORITIES_LDAP).get()!=null?tppApplication.get(IdentityManagementConstants.AUTHORITIES_LDAP).get().toString():null;
			String tppBlocked = tppGroup.get(IdentityManagementConstants.TPP_BLOCK_LDAP).get()!=null?tppGroup.get(IdentityManagementConstants.TPP_BLOCK_LDAP).get().toString():null;		      
			String legalEntityName = tppGroup.get(IdentityManagementConstants.LEGAL_ENTITY_NAME_LDAP).get()!=null?tppGroup.get(IdentityManagementConstants.LEGAL_ENTITY_NAME_LDAP).get().toString():null;
			String registeredId = tppGroup.get(IdentityManagementConstants.REGISTERED_ID_LDAP).get()!=null?tppGroup.get(IdentityManagementConstants.REGISTERED_ID_LDAP).get().toString():null;

			Set<String> registeredRedirectUriSet = retriveSetFromString(registeredRedirectUri);
			Set<String> authoritiesSet = retriveSetFromString(authorities);
			Collection<GrantedAuthority> tppAuthorities = new ArrayList<>();

			if(authoritiesSet!=null){
				for (Iterator<String> iterator = authoritiesSet.iterator(); iterator.hasNext();) {
					tppAuthorities.add(new TppAuthorities(iterator.next()));
				}
			}

			clientDetails = new CustomClientDetails(clientId, clientSecrate
					, registeredRedirectUriSet, tppAuthorities, 
					new Boolean(tppBlocked), legalEntityName, registeredId);

		} catch (NamingException e) {
			throw new JwkException(e.getMessage());
		}

		return clientDetails;
	}


	private Set<String> retriveSetFromString(String inputString){
		if(!StringUtils.hasText(inputString))
			return null;

		HashSet<String> hashSet = new HashSet<>(); 
		StringTokenizer st = new StringTokenizer(inputString, ",");
		while(st.hasMoreTokens())
			hashSet.add(st.nextToken());
		return hashSet;
	}
}