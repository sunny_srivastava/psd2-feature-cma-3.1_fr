/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.rest.client.sync.test;

import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.test.web.client.match.MockRestRequestMatchers;
import org.springframework.test.web.client.response.MockRestResponseCreators;
import org.springframework.web.client.RestTemplate;

import com.capgemini.psd2.exceptions.ErrorInfo;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.impl.RestClientSyncImpl;
import com.capgemini.psd2.rest.client.sync.test.config.RestClientTestConfiguration;
import com.capgemini.psd2.utilities.JSONUtilities;

/**
 * The Class RestClientSyncTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes=RestClientTestConfiguration.class)
public class RestClientSyncTest {
   
	/** The rest client sync. */
	@Autowired
	private RestClientSyncImpl restClientSync;
	

	/** The server. */
	private MockRestServiceServer server;
	
	/** The rest template. */
	@Autowired
	private RestTemplate restTemplate;
	
	/** The info. */
	private ErrorInfo info;
	
	/** The att. */
	private RequestHeaderAttributes att  = new RequestHeaderAttributes();
	
	/** The test URL. */
	private String testURL = "/test";
	
	/** The test input. */
	private String testInput = "{\"test\"}";
	
	/**
	 * Setup.
	 */
	@Before
	public void setup() {
		String file = ClassLoader.getSystemResource("truststore.jks").getFile();
		System.setProperty("javax.net.ssl.trustStore", file);
		System.setProperty("javax.net.ssl.trustStorePassword", "server");
		server = MockRestServiceServer.createServer(restTemplate);
		info = new ErrorInfo("123", "Error message", "Detail error message", "400");
	}
	/**
	 * Test call for get.
	 */
	@Test
	public void testCallForGet(){
		server.expect(MockRestRequestMatchers.requestTo(testURL)).andExpect(MockRestRequestMatchers.method(HttpMethod.GET))
		.andRespond(MockRestResponseCreators.withStatus(HttpStatus.OK)
				.contentType(MediaType.APPLICATION_JSON)
				.body(testInput));
		String responseData;
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setRequestHeaderAttributes(att);
		requestInfo.setUrl(testURL);
		responseData = restClientSync.callForGet(requestInfo, String.class, null);
		Assert.assertNotNull(responseData);
	}
	
	/**
	 * Test call for get url null exception.
	 */
	@Test(expected=PSD2Exception.class)
	public void testCallForGetUrlNullException(){
		RequestInfo requestInfo = new RequestInfo();
		restClientSync.callForGet(requestInfo, String.class, null);
		restClientSync.callForGet(null, String.class, null);
	}
	
	/**
	 * Test call for get client exception.
	 */
	@Test(expected=PSD2Exception.class)
	public void testCallForGetClientException(){
		server.expect(MockRestRequestMatchers.requestTo(testURL)).andExpect(MockRestRequestMatchers.method(HttpMethod.GET))
		.andRespond(MockRestResponseCreators.withBadRequest());
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(testURL);
		restClientSync.callForGetWithParams(requestInfo, String.class, new HashMap<>(), null);
	}
	
	/**
	 * Test call for get client exception.
	 */
	@Test(expected=PSD2Exception.class)
	public void testNullRequestInfo(){
		restClientSync.callForGetWithParams(null, String.class, new HashMap<>(), null);
	}
	
	/**
	 * Test call for delete client exception.
	 */
	@Test(expected=PSD2Exception.class)
	public void callForDeleteException(){
		restClientSync.callForDelete(null, String.class, null);
	}
	
	/**
	 * Test call for get client exception.
	 */
	@Test(expected=PSD2Exception.class)
	public void testCallForGetClientException1(){
		server.expect(MockRestRequestMatchers.requestTo(testURL)).andExpect(MockRestRequestMatchers.method(HttpMethod.GET))
		.andRespond(MockRestResponseCreators.withBadRequest());
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(testURL);
		restClientSync.callForGetWithParams(requestInfo, String.class, new HashMap<>(), null);
	}
	
	/**
	 * Test call for get server exception.
	 */
	@Test(expected=PSD2Exception.class)
	public void testCallForGetServerException(){
		server.expect(MockRestRequestMatchers.requestTo(testURL)).andExpect(MockRestRequestMatchers.method(HttpMethod.GET))
		.andRespond(MockRestResponseCreators.withStatus(HttpStatus.INTERNAL_SERVER_ERROR)
				.contentType(MediaType.APPLICATION_JSON)
				.body(JSONUtilities.getJSONOutPutFromObject(info)));
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(testURL);
		restClientSync.callForGetWithParams(requestInfo, String.class, new HashMap<>(), new HttpHeaders());
	}
	
	/**
	 * Test call for get with param.
	 */
	@Test
	public void testCallForGetWithParam(){
		server.expect(MockRestRequestMatchers.requestTo(testURL)).andExpect(MockRestRequestMatchers.method(HttpMethod.GET))
		.andRespond(MockRestResponseCreators.withStatus(HttpStatus.OK)
				.contentType(MediaType.APPLICATION_JSON)
				.body(testInput));
		String responseData;
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(testURL);
		responseData = restClientSync.callForGetWithParams(requestInfo, String.class, new HashMap<>(), null);
		Assert.assertNotNull(responseData);
	}
	
	/**
	 * Test call for get with param url null exception.
	 */
	@Test(expected=PSD2Exception.class)
	public void testCallForGetWithParamUrlNullException(){
		RequestInfo requestInfo = new RequestInfo();
		restClientSync.callForGetWithParams(requestInfo, String.class, new HashMap<>(), null);
		restClientSync.callForGetWithParams(null, String.class, new HashMap<>(), null);
	}
	
	/**
	 * Test call for post.
	 */
	@Test
	public void testCallForPost(){
		server.expect(MockRestRequestMatchers.requestTo(testURL)).andExpect(MockRestRequestMatchers.method(HttpMethod.POST))
		.andRespond(MockRestResponseCreators.withStatus(HttpStatus.OK)
				.contentType(MediaType.APPLICATION_JSON)
				.body(testInput));
		String responseData;
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(testURL);
		responseData = restClientSync.callForPost(requestInfo, testInput , String.class, null);
		Assert.assertNotNull(responseData);
	}
	
	/**
	 * Test call for post url null exception.
	 */
	@Test(expected=PSD2Exception.class)
	public void testCallForPostUrlNullException(){
		RequestInfo requestInfo = new RequestInfo();
		restClientSync.callForPost(requestInfo, testInput , String.class, null);
		restClientSync.callForPost(null, testInput , String.class, null);
	}
	
	/**
	 * Test call for post client exception.
	 */
	@Test(expected=PSD2Exception.class)
	public void testCallForPostClientException(){
		server.expect(MockRestRequestMatchers.requestTo(testURL)).andExpect(MockRestRequestMatchers.method(HttpMethod.POST))
		.andRespond(MockRestResponseCreators.withBadRequest());
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(testURL);
		restClientSync.callForPost(requestInfo, testInput , String.class, null);
	}
	
	/**
	 * Test call for post server exception.
	 */
	@Test(expected=PSD2Exception.class)
	public void testCallForPostServerException(){
		server.expect(MockRestRequestMatchers.requestTo(testURL)).andExpect(MockRestRequestMatchers.method(HttpMethod.POST))
		.andRespond(MockRestResponseCreators.withStatus(HttpStatus.INTERNAL_SERVER_ERROR)
				.contentType(MediaType.APPLICATION_JSON)
				.body(JSONUtilities.getJSONOutPutFromObject(info)));
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(testURL);
		restClientSync.callForPost(requestInfo, testInput , String.class, null);
	}
	
	/**
	 * Test call for put url null exception.
	 */
	@Test(expected=PSD2Exception.class)
	public void testCallForPutUrlNullException(){
		RequestInfo requestInfo = new RequestInfo();
		restClientSync.callForPut(requestInfo, testInput , String.class, null);
		restClientSync.callForPut(null, testInput , String.class, null);
	}
	
	/**
	 * Test call for put.
	 */
	@Test
	public void testCallForPut(){
		server.expect(MockRestRequestMatchers.requestTo(testURL)).andExpect(MockRestRequestMatchers.method(HttpMethod.PUT))
		.andRespond(MockRestResponseCreators.withStatus(HttpStatus.OK)
				.contentType(MediaType.APPLICATION_JSON)
				.body(testInput));
		String responseData;
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(testURL);
		responseData = restClientSync.callForPut(requestInfo, testInput , String.class, null);
		Assert.assertNotNull(responseData);
	}
	

	/**
	 * Test call for put client exception.
	 */
	@Test(expected=PSD2Exception.class)
	public void testCallForPutClientException(){
		server.expect(MockRestRequestMatchers.requestTo(testURL)).andExpect(MockRestRequestMatchers.method(HttpMethod.PUT))
		.andRespond(MockRestResponseCreators.withBadRequest());
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(testURL);
		restClientSync.callForPut(requestInfo, testInput , String.class, null);
	}
	
	/**
	 * Test call for put resource not found exception.
	 */
	@Test(expected=PSD2Exception.class)
	public void testCallForPutResourceNotFoundException(){
		server.expect(MockRestRequestMatchers.requestTo(testURL)).andExpect(MockRestRequestMatchers.method(HttpMethod.PUT))
		.andRespond(MockRestResponseCreators.withStatus(HttpStatus.REQUEST_TIMEOUT));
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(testURL);
		restClientSync.callForPut(requestInfo, testInput , String.class, null);
	}
	
	/**
	 * Test call for put client exception with error info.
	 */
	@Test(expected=PSD2Exception.class)
	public void testCallForPutClientExceptionWithErrorInfo(){
		server.expect(MockRestRequestMatchers.requestTo(testURL)).andExpect(MockRestRequestMatchers.method(HttpMethod.PUT))
		.andRespond(MockRestResponseCreators.withStatus(HttpStatus.NOT_FOUND)
				.contentType(MediaType.APPLICATION_JSON)
				.body(JSONUtilities.getJSONOutPutFromObject(info)));
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(testURL);
		restClientSync.callForPut(requestInfo, testInput , String.class, null);
	}
	
	/**
	 * Test call for put server exception.
	 */
	@Test(expected=PSD2Exception.class)
	public void testCallForPutServerException(){
		server.expect(MockRestRequestMatchers.requestTo(testURL)).andExpect(MockRestRequestMatchers.method(HttpMethod.PUT))
		.andRespond(MockRestResponseCreators.withStatus(HttpStatus.INTERNAL_SERVER_ERROR)
				.contentType(MediaType.APPLICATION_JSON)
				.body(JSONUtilities.getJSONOutPutFromObject(info)));
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(testURL);
		restClientSync.callForPut(requestInfo, testInput , String.class, null);
	}
	
	/**
	 * Test request info.
	 */
	@Test
	public void testRequestInfo(){
		RequestInfo info = new RequestInfo();
		info.setUrl("test");
		info.setRequestHeaderAttributes(att);
		assertNotNull(info.getUrl());
		assertNotNull(info.getRequestHeaderAttributes());
	}
	
	/**
	 * Test init with JKS.
	 */
	@Ignore
	@Test
	public void testInitWithJKS(){
		String file = ClassLoader.getSystemResource("truststore.jks").getFile();
		System.setProperty("javax.net.ssl.trustStore", file);
		System.setProperty("javax.net.ssl.trustStorePassword", "server");
		restClientSync.init();
		assertNotNull(restTemplate.getRequestFactory());
		
	}
	
	@Test
	public void testCallForDelete(){
		server.expect(MockRestRequestMatchers.requestTo(testURL)).andExpect(MockRestRequestMatchers.method(HttpMethod.DELETE))
		.andRespond(MockRestResponseCreators.withStatus(HttpStatus.OK)
				.contentType(MediaType.APPLICATION_JSON)
				.body(testInput));
		String responseData;
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(testURL);
		responseData = restClientSync.callForDelete(requestInfo, String.class, null);
		Assert.assertNotNull(responseData);
	}
	
	@Test(expected=PSD2Exception.class) 
	public void testCallForDeleteRequestInfoNull(){
		server.expect(MockRestRequestMatchers.requestTo(testURL)).andExpect(MockRestRequestMatchers.method(HttpMethod.DELETE))
		.andRespond(MockRestResponseCreators.withStatus(HttpStatus.OK)
				.contentType(MediaType.APPLICATION_JSON)
				.body(testInput));
		String responseData;
		responseData = restClientSync.callForDelete(null, String.class, null);
		Assert.assertNotNull(responseData);
	}
	
	@Test (expected=PSD2Exception.class) 
	public void testCallForDeleteWithParamsRequestInfoNull(){
		server.expect(MockRestRequestMatchers.requestTo(testURL)).andExpect(MockRestRequestMatchers.method(HttpMethod.DELETE))
		.andRespond(MockRestResponseCreators.withStatus(HttpStatus.OK)
				.contentType(MediaType.APPLICATION_JSON)
				.body(testInput));
		String responseData;
		responseData = restClientSync.callForDeleteWithParams(null, String.class, null, null);
		Assert.assertNotNull(responseData);
	}
	
	@Test (expected=AssertionError.class) 
	public void testCallForDeleteWithParamsAndHeader(){
		server.expect(MockRestRequestMatchers.requestTo(testURL)).andExpect(MockRestRequestMatchers.method(HttpMethod.DELETE))
		.andRespond(MockRestResponseCreators.withStatus(HttpStatus.OK)
				.contentType(MediaType.APPLICATION_JSON)
				.body(testInput));
		String responseData;
		Map<String,String> map= new HashMap<>();
		map.put("key", "value");
		
		Map<String, Map<String,String>> params= new HashMap<>();
		params.put("test", map);
		
		HttpHeaders header = new HttpHeaders();
		header.add("x-fapi-interaction-id", "value");

		RequestInfo info = new RequestInfo();
		info.setUrl("test");
		info.setRequestHeaderAttributes(att);
		
		responseData = restClientSync.callForDeleteWithParams(info, String.class, params, header);
		Assert.assertNotNull(responseData);
	}
	
	@Test
	public void setExcludeProxyUrlsTest() {
		List<String> list = new ArrayList<>();
		list.add("www.test.com");
		restClientSync.setExcludeProxyUrls(list);
	}
}
