/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.rest.client.sync;

import java.util.Map;

import org.springframework.http.HttpHeaders;

import com.capgemini.psd2.rest.client.model.RequestInfo;

/**
 * The Interface RestClientSync.
 */
public interface RestClientSync {
	
	/**
	 * Call for get.
	 *
	 * @param <T> the generic type
	 * @param requestInfo the request info
	 * @param responseType the response type
	 * @param httpHeaders the http headers
	 * @return the t
	 */
	public <T> T callForGet(RequestInfo requestInfo, Class<T> responseType, HttpHeaders httpHeaders);
	
	/**
	 * Call for get with params.
	 *
	 * @param <T> the generic type
	 * @param requestInfo the request info
	 * @param responseType the response type
	 * @param params the params
	 * @param httpHeaders the http headers
	 * @return the t
	 */
	public <T> T callForGetWithParams(RequestInfo requestInfo, Class<T> responseType, Map<String, ?> params, HttpHeaders httpHeaders);
	
	/**
	 * Call for post.
	 *
	 * @param <T> the generic type
	 * @param <R> the generic type
	 * @param requestInfo the request info
	 * @param input the input
	 * @param responseType the response type
	 * @param httpHeaders the http headers
	 * @return the r
	 */
	public <T,R> R callForPost(RequestInfo requestInfo,T input,Class<R> responseType, HttpHeaders httpHeaders);
	
	/**
	 * Call for put.
	 *
	 * @param <T> the generic type
	 * @param <R> the generic type
	 * @param requestInfo the request info
	 * @param input the input
	 * @param responseType the response type
	 * @param httpHeaders the http headers
	 * @return the r
	 */
	public <T, R> R callForPut(RequestInfo requestInfo, T input, Class<R> responseType, HttpHeaders httpHeaders);
	
	/**
	 * Call for delete.
	 *
	 * @param <T> the generic type
	 * @param requestInfo the request info
	 * @param responseType the response type
	 * @param httpHeaders the http headers
	 * @return the t
	 */
	public <T> T callForDelete(RequestInfo requestInfo, Class<T> responseType, HttpHeaders httpHeaders);

	/**
	 * Call for delete with params.
	 *
	 * @param <T> the generic type
	 * @param requestInfo the request info
	 * @param responseType the response type
	 * @param params the params
	 * @param httpHeaders the http headers
	 * @return the t
	 */
	public <T> T callForDeleteWithParams(RequestInfo requestInfo, Class<T> responseType, Map<String, ?> params, HttpHeaders httpHeaders);
	
	/**
	 * For buildSSL request
	 * 
	 * @param keyAlias
	 */
	public void buildSSLRequest(final String keyAlias);
}
