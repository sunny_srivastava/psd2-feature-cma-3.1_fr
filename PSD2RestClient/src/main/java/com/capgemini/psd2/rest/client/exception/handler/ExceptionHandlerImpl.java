/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.rest.client.exception.handler;

import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.ResourceAccessException;

import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.ErrorInfo;
import com.capgemini.psd2.exceptions.OAuthErrorInfo;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.utilities.JSONUtilities;


/**
 * The Class ExceptionHandlerImpl.
 */
public class ExceptionHandlerImpl implements ExceptionHandler{

	/* (non-Javadoc)
	 * @see com.capgemini.psd2.rest.client.exception.handler.ExceptionHandler#handleException(org.springframework.web.client.HttpServerErrorException)
	 */
	@Override
	public void handleException(HttpServerErrorException e) {
		String responseBody  = e.getResponseBodyAsString();

		if(responseBody != null && responseBody.contains("error") && responseBody.contains("error_description")) {
			OAuthErrorInfo errorInfo = JSONUtilities.getObjectFromJSONString(responseBody, OAuthErrorInfo.class);
			ErrorInfo info = new ErrorInfo(ErrorCodeEnum.OAUTH_EXCEPTION.getErrorCode(), errorInfo.getError(), errorInfo.getError_description(), String.valueOf(e.getStatusCode().value()));
			throw new PSD2Exception(info.getDetailErrorMessage(), info);
		}
		else {
			ErrorInfo errorInfoObj = JSONUtilities.getObjectFromJSONString(responseBody, ErrorInfo.class); 
			errorInfoObj.setStatusCode(String.valueOf(e.getStatusCode().value()));
			throw new PSD2Exception(errorInfoObj.getDetailErrorMessage(), errorInfoObj);
		}
	}

	/* (non-Javadoc)
	 * @see com.capgemini.psd2.rest.client.exception.handler.ExceptionHandler#handleException(org.springframework.web.client.HttpClientErrorException)
	 */
	@Override
	public void handleException(HttpClientErrorException e) {
		String responseBody  = e.getResponseBodyAsString();
		if(responseBody!=null && responseBody.contains("errorCode") && responseBody.contains("errorMessage")){
			ErrorInfo errorInfoObj =  JSONUtilities.getObjectFromJSONString(responseBody, ErrorInfo.class);
			errorInfoObj.setStatusCode(String.valueOf(e.getStatusCode().value()));
			throw new PSD2Exception(errorInfoObj.getDetailErrorMessage(), errorInfoObj);
		}
		// OAuth2 REST Call errors
		else if(responseBody != null && responseBody.contains("error") && responseBody.contains("error_description")) {
			OAuthErrorInfo errorInfo = JSONUtilities.getObjectFromJSONString(responseBody, OAuthErrorInfo.class);
			ErrorInfo info = new ErrorInfo(ErrorCodeEnum.OAUTH_EXCEPTION.getErrorCode(), errorInfo.getError_description(), errorInfo.getError_description(), String.valueOf(e.getStatusCode().value()));
			throw new PSD2Exception(info.getDetailErrorMessage(), info);
		}
		throw PSD2Exception.populatePSD2Exception(e.getMessage(), ErrorCodeEnum.REST_TRANSPORT_ADAPTER_TECHNICAL_ERROR);
	}

	/* (non-Javadoc)
	 * @see com.capgemini.psd2.rest.client.exception.handler.ExceptionHandler#handleException(org.springframework.web.client.ResourceAccessException)
	 */
	@Override
	public void handleException(ResourceAccessException e) {
		throw PSD2Exception.populatePSD2Exception(e.getMessage(), ErrorCodeEnum.REST_TRANSPORT_ADAPTER_TECHNICAL_ERROR);
	}
}