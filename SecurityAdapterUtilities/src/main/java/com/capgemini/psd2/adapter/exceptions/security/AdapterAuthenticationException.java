package com.capgemini.psd2.adapter.exceptions.security;

import com.capgemini.psd2.exceptions.ErrorInfo;
import com.capgemini.psd2.security.exceptions.PSD2AuthenticationException;


public class AdapterAuthenticationException extends PSD2AuthenticationException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private AdapterAuthenticationException(String message,ErrorInfo errorInfo){
		super(message,errorInfo);
	}
	
	

	public static AdapterAuthenticationException populateAuthenticationFailedException(String detailedErrorMessage,SecurityAdapterErrorCodeEnum errorCodeEnum){
		ErrorInfo errorInfo = new ErrorInfo(errorCodeEnum.getErrorCode(), errorCodeEnum.getErrorMessage(), detailedErrorMessage, errorCodeEnum.getStatusCode());
		/*errorInfo.setErrorCode(errorCodeEnum.getErrorCode());
		errorInfo.setErrorMessage(errorCodeEnum.getErrorMessage());
		errorInfo.setDetailErrorMessage(detailedErrorMessage);*/
		AdapterAuthenticationException authenticationException = new AdapterAuthenticationException(detailedErrorMessage,errorInfo);
		return authenticationException;
	}
	
	public static AdapterAuthenticationException populateAuthenticationFailedException(ErrorInfo errorInfo){
		AdapterAuthenticationException authenticationException = new AdapterAuthenticationException(errorInfo.getDetailErrorMessage(),errorInfo);
		return authenticationException;
	}
}