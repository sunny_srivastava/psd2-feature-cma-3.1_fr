package com.capgemini.psd2.foundationservice.pisp.sca.consent.boi.adapter.test;



import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;

import java.lang.reflect.Method;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.RestTemplate;

import com.capgemini.psd2.foundationservice.pisp.sca.consent.boi.adapter.client.PispScaConsentFoundationServiceClient;
import com.capgemini.psd2.foundationservice.pisp.sca.consent.boi.adapter.delegate.PispScaConsentFoundationServiceDelegate;
import com.capgemini.psd2.foundationservice.pisp.sca.consent.boi.adapter.raml.domain.Brand;
import com.capgemini.psd2.foundationservice.pisp.sca.consent.boi.adapter.raml.domain.PaymentInstructionProposal;
import com.capgemini.psd2.foundationservice.pisp.sca.consent.boi.adapter.raml.domain.ScheduledPaymentInstructionProposal;
import com.capgemini.psd2.foundationservice.pisp.sca.consent.boi.adapter.raml.domain.StandingOrderInstructionProposal;
import com.capgemini.psd2.foundationservice.pisp.sca.consent.boi.adapter.transformer.PispScaConsentFoundationServiceTransformer;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.RestClientSync;

@RunWith(SpringJUnit4ClassRunner.class)
public class PispScaConsentFoundationServiceDelegateRestCallTest {
	
	@Mock
	private PispScaConsentFoundationServiceDelegate delegate;
	
	@InjectMocks
	private PispScaConsentFoundationServiceClient PispScaConsentFoundationServiceClient;
	
	@Mock
	private PispScaConsentFoundationServiceTransformer pispScaConsentFoundationServiceTransformer;

	/** The rest client. */

	@Mock
	private RestClientSync restClient;
	
	@Mock
	private RestTemplate restTemplate;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		Method method1 = PispScaConsentFoundationServiceClient.class.getDeclaredMethod("init", null);
		method1.setAccessible(true);
		method1.invoke(PispScaConsentFoundationServiceClient);
		assertNotNull(method1);

	}
	
	@Test
	public void testPispScaConsentFoundationServiceClient(){
		RequestInfo reqInfo= new RequestInfo();
		HttpHeaders headers= new HttpHeaders();
		Mockito.when(restClient.callForGet(any(),any(),any())).thenReturn(new PaymentInstructionProposal());
		
		PispScaConsentFoundationServiceClient.restTransportForPispScaConsentFoundationService(reqInfo, PaymentInstructionProposal.class, headers);
	}
	
	@Test
	public void testPispScaConsentFoundationServiceClientForValidate(){
		RequestInfo reqInfo= new RequestInfo();
		reqInfo.setUrl("http://google.com");
		HttpHeaders httpHeaders= new HttpHeaders();
		httpHeaders.add("X-BOI-USER", "header user");
		httpHeaders.add("X-BOI-CHANNEL", "header channel");
		httpHeaders.add("X-BOI-PLATFORM", "header platform");
		httpHeaders.add("X-CORRELATION-ID", "header correlation Id");
		PaymentInstructionProposal paymentInstructionProposal= new PaymentInstructionProposal();
		paymentInstructionProposal.setAuthorisationDatetime("18/09/1996");
		
		
		PispScaConsentFoundationServiceClient.restValidateTransportForPispScaConsentFoundationService(reqInfo, paymentInstructionProposal,PaymentInstructionProposal.class, httpHeaders);
	
		Mockito.when(restClient.callForPost(any(),any(),any(),any())).thenReturn(paymentInstructionProposal);
		}
	
	@Test
	public void testPispScaConsentFoundationServiceClientForPut(){
		RequestInfo requestInfo= new RequestInfo();
		HttpHeaders httpHeaders= new HttpHeaders();
		PaymentInstructionProposal paymentInstructionProposal= new PaymentInstructionProposal();
	    Mockito.when(restClient.callForPut(any(), any(), any(), any())).thenReturn(new PaymentInstructionProposal());
		
		PispScaConsentFoundationServiceClient.restTransportForDomesticPaymentFoundationServicePut(requestInfo, paymentInstructionProposal, PaymentInstructionProposal.class, httpHeaders);
	}
	
	@Test
	public void testPispScaConsentFoundationServiceClientForPutScdeuled(){
		RequestInfo requestInfo= new RequestInfo();
		HttpHeaders httpHeaders= new HttpHeaders();
		ScheduledPaymentInstructionProposal scheduledPaymentInstructionProposalResponseAfterTransform = new ScheduledPaymentInstructionProposal();
		Mockito.when(restClient.callForPut(any(), any(), any(), any())).thenReturn(new ScheduledPaymentInstructionProposal());
		PispScaConsentFoundationServiceClient.restTransportForDomesticScheduledPaymentFoundationServicePut(requestInfo, scheduledPaymentInstructionProposalResponseAfterTransform, ScheduledPaymentInstructionProposal.class, httpHeaders);
	}
	
	@Test
	public void testPispScaConsentFoundationServiceClientForScdeuled(){
		RequestInfo requestInfo= new RequestInfo();
		HttpHeaders httpHeaders= new HttpHeaders();
		Mockito.when(restClient.callForPut(any(), any(), any(), any())).thenReturn(new ScheduledPaymentInstructionProposal());
		PispScaConsentFoundationServiceClient.restTransportForPispScaConsentScheduledFoundationService(requestInfo, ScheduledPaymentInstructionProposal.class, httpHeaders);
	}
	
	@Test
	public void testPispScaConsentFoundationServiceClientForScdeuledvalidate(){
		RequestInfo requestInfo= new RequestInfo();
		HttpHeaders httpHeaders= new HttpHeaders();
		ScheduledPaymentInstructionProposal scheduledPaymentInstructionProposalResponseAfterTransform = new ScheduledPaymentInstructionProposal();
		Mockito.when(restClient.callForPut(any(), any(), any(), any())).thenReturn(new ScheduledPaymentInstructionProposal());
		PispScaConsentFoundationServiceClient.restValidateTransportForPispScaConsentFoundationServiceScheduled(requestInfo, scheduledPaymentInstructionProposalResponseAfterTransform, ScheduledPaymentInstructionProposal.class, httpHeaders);
	}
	
	
	
	@Test
	public void testPispScaConsentFoundationServiceClientForstandingvalidate(){
		RequestInfo requestInfo= new RequestInfo();
		HttpHeaders httpHeaders= new HttpHeaders();
		StandingOrderInstructionProposal scheduledPaymentInstructionProposalResponseAfterTransform = new StandingOrderInstructionProposal();
		Mockito.when(restClient.callForPut(any(), any(), any(), any())).thenReturn(new PaymentInstructionProposal());
		PispScaConsentFoundationServiceClient.restValidateTransportForPispScaConsentFoundationServiceStandingOrder(requestInfo, scheduledPaymentInstructionProposalResponseAfterTransform, StandingOrderInstructionProposal.class, httpHeaders);
	}
	
	
	@Test
	public void testPispScaConsentFoundationServiceClientForstandingput(){
		RequestInfo requestInfo= new RequestInfo();
		HttpHeaders httpHeaders= new HttpHeaders();
		StandingOrderInstructionProposal scheduledPaymentInstructionProposalResponseAfterTransform = new StandingOrderInstructionProposal();
		Mockito.when(restClient.callForPut(any(), any(), any(), any())).thenReturn(new StandingOrderInstructionProposal());
		PispScaConsentFoundationServiceClient.restTransportForDomesticStandingOrderFoundationServicePut(requestInfo, scheduledPaymentInstructionProposalResponseAfterTransform, StandingOrderInstructionProposal.class, httpHeaders);
	}
	
	@Test
	public void testPispScaConsentFoundationServiceClientForScdeuledvalidate2(){
		RequestInfo requestInfo= new RequestInfo();
		HttpHeaders httpHeaders= new HttpHeaders();
		Mockito.when(restClient.callForPut(any(), any(), any(), any())).thenReturn(new ScheduledPaymentInstructionProposal());
		PispScaConsentFoundationServiceClient.restTransportForPispScaConsentFoundationService2(requestInfo, ScheduledPaymentInstructionProposal.class, httpHeaders);
	}
	
	
}
