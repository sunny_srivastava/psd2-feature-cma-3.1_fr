package com.capgemini.psd2.foundationservice.pisp.sca.consent.boi.adapter.test;

import static org.junit.Assert.assertNotNull;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.foundationservice.pisp.sca.consent.boi.adapter.delegate.PispScaConsentFoundationServiceDelegate;
import com.capgemini.psd2.foundationservice.pisp.sca.consent.boi.adapter.transformer.PispScaConsentFoundationServiceTransformer;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;

@RunWith(SpringJUnit4ClassRunner.class)
public class PispScaConsentFoundationServiceDelegateTransformationTest {

	@InjectMocks
	private PispScaConsentFoundationServiceDelegate delegate;

	@Mock
	private PispScaConsentFoundationServiceTransformer pispScaConsentFoundationServiceTransformer;

	/** The rest client. */
	/*
	 * @Mock private RestClientSyncImpl restClient;
	 * 
	 *//** The psd 2 validator. */
	/*
	 * @Mock private PSD2Validator psd2Validator;
	 * 
	 * @Mock private TimeZoneDateTimeAdapter timeZoneDateTimeAdapter;
	 * 
	 *//**
		 * Sets the up.
		 */

	/*
	 * @Before public void setUp() { MockitoAnnotations.initMocks(this); }
	 * 
	 *//**
		 * Context loads.
		 */
	@Test
	public void contextLoads() {
	}

	@Test
	public void testPispScaConsentFoundationServiceDelegate() {

		ReflectionTestUtils.setField(delegate, "transactioReqHeader", "transactioReqHeader");
		ReflectionTestUtils.setField(delegate, "correlationMuleReqHeader", "correlation");
		ReflectionTestUtils.setField(delegate, "sourceUserReqHeader", "sorceuser");
		ReflectionTestUtils.setField(delegate, "sourceSystemReqHeader", "source");
		ReflectionTestUtils.setField(delegate, "apiChannelCode", "channel");
		ReflectionTestUtils.setField(delegate, "partysourceReqHeader", "partysource");
		ReflectionTestUtils.setField(delegate, "domesticPaymentConsentBaseURL", "jf");
		ReflectionTestUtils.setField(delegate, "systemApiVersion", "jf");
		ReflectionTestUtils.setField(delegate, "channelBrand", "tenant_id");
		Map<String, String> params = new HashMap<>();
		CustomPaymentStageIdentifiers stageIden=new CustomPaymentStageIdentifiers();
		stageIden.setPaymentSetupVersion("1.1");
		params.put("X-API-SOURCE-SYSTEM", "fgrd");
		params.put("X-API-SOURCE-USER", "USER_ID");
		params.put("X-API-PARTY-SOURCE-ID-NUMBER", "PARTY_SOURCE_ID");
		params.put("X-API-TRANSACTION-ID", "TRANSACTION_ID");
		params.put("BOL", "BOL");
		params.put("correlationMuleReqHeader", "correlationMuleReqHeader");
		params.put("x-api-party-source-id-number", "x-api-party-source-id-number");
		params.put("X-CORRELATION-ID", "X-CORRELATION-ID");
		params.put("X-BOI-USER", "X-BOI-USER");
		params.put("X-BOI-CHANNEL", "BOL");
		params.put("tenant_id", "BOIUK");
		delegate.createPispScaConsentRequestHeaders(stageIden, params);
	}
	
	@Test
	public void testPispScaConsentFoundationServiceDelegate12() {

		ReflectionTestUtils.setField(delegate, "transactioReqHeader", "transactioReqHeader");
		ReflectionTestUtils.setField(delegate, "correlationMuleReqHeader", "correlation");
		ReflectionTestUtils.setField(delegate, "sourceUserReqHeader", "sorceuser");
		ReflectionTestUtils.setField(delegate, "sourceSystemReqHeader", "source");
		ReflectionTestUtils.setField(delegate, "apiChannelCode", "channel");
		ReflectionTestUtils.setField(delegate, "partysourceReqHeader", "partysource");
		ReflectionTestUtils.setField(delegate, "domesticPaymentConsentBaseURL", "jf");
		ReflectionTestUtils.setField(delegate, "systemApiVersion", "jf");
		ReflectionTestUtils.setField(delegate, "channelBrand", "BOIROI");
		Map<String, String> params = new HashMap<>();
		CustomPaymentStageIdentifiers stageIden=new CustomPaymentStageIdentifiers();
		stageIden.setPaymentSetupVersion("1.0");
		params.put("X-API-SOURCE-SYSTEM", "fgrd");
		params.put("X-API-SOURCE-USER", "USER_ID");
		params.put("X-API-PARTY-SOURCE-ID-NUMBER", "PARTY_SOURCE_ID");
		params.put("X-API-TRANSACTION-ID", "TRANSACTION_ID");
		params.put("BOL", "BOL");
		params.put("tenant_id", "BOIROI");
		params.put("X-BOI-CHANNEL", "B365");
		delegate.createPispScaConsentRequestHeaders(stageIden, params);
	}
	@Test
	public void testPispScaConsentFoundationServiceDelegate1() {

		ReflectionTestUtils.setField(delegate, "transactioReqHeader", "transactioReqHeader");
		ReflectionTestUtils.setField(delegate, "correlationMuleReqHeader", "correlation");
		ReflectionTestUtils.setField(delegate, "sourceUserReqHeader", "sorceuser");
		ReflectionTestUtils.setField(delegate, "sourceSystemReqHeader", "source");
		ReflectionTestUtils.setField(delegate, "apiChannelCode", "channel");
		ReflectionTestUtils.setField(delegate, "partysourceReqHeader", "partysource");
		ReflectionTestUtils.setField(delegate, "domesticPaymentConsentBaseURL", "jf");
		ReflectionTestUtils.setField(delegate, "channelBrand", "tenant_id");
		Map<String, String> params = new HashMap<>();
		params.put("BOL", "BOL");
		params.put("tenant_id", "BOIUK");
		params.put("X-API-SOURCE-SYSTEM", "fgrd");
		params.put("X-API-SOURCE-USER", "USER_ID");
		params.put("X-API-PARTY-SOURCE-ID-NUMBER", "PARTY_SOURCE_ID");
		params.put("X-API-TRANSACTION-ID", "TRANSACTION_ID");
		params.put("X-BOI-CHANNEL", "B365");
		params.put("tenant_id", "BOIUK");
		delegate.createPispScaConsentRequestHeadersForStandingOrders( params);
		params.put("X-BOI-CHANNEL", "B365");
		params.put("tenant_id", "BOIUK");
		delegate.createPispScaConsentRequestHeadersForStandingOrders( params);
		params.put("tenant_id", null);
		delegate.createPispScaConsentRequestHeadersForStandingOrders( params);
		params.put("tenant_id", "abcd");
		delegate.createPispScaConsentRequestHeadersForStandingOrders( params);
	}

	@Test
	public void testPispScaConsentFoundationServiceDelegate2() {

		ReflectionTestUtils.setField(delegate, "transactioReqHeader", "transactioReqHeader");
		ReflectionTestUtils.setField(delegate, "correlationMuleReqHeader", "correlation");
		ReflectionTestUtils.setField(delegate, "sourceUserReqHeader", "sorceuser");
		ReflectionTestUtils.setField(delegate, "sourceSystemReqHeader", "source");
		ReflectionTestUtils.setField(delegate, "apiChannelCode", "channel");
		ReflectionTestUtils.setField(delegate, "partysourceReqHeader", "partysource");
		ReflectionTestUtils.setField(delegate, "domesticPaymentConsentBaseURL", "jf");
		ReflectionTestUtils.setField(delegate, "channelBrand", "tenant_id");
		ReflectionTestUtils.setField(delegate, "systemApiVersion", "apiVersion");
		Map<String, String> params = new HashMap<>();
		params.put("tenant_id", "tfhfg");
		params.put("X-API-SOURCE-SYSTEM", "fgrd");
		params.put("X-API-SOURCE-USER", "USER_ID");
		params.put("X-API-PARTY-SOURCE-ID-NUMBER", "PARTY_SOURCE_ID");
		params.put("X-API-TRANSACTION-ID", "TRANSACTION_ID");
		params.put("BOL", "BOL");
		params.put("X-CORRELATION-ID", "TRANSACTION_ID");
		params.put("x-api-party-source-id-number", "TRANSACTION_ID");
		params.put("X-BOI-USER", "TRANSACTION_ID");
		params.put("X-BOI-CHANNEL", "TRANSACTION_ID");
		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();
		delegate.createPispScaConsentRequestHeadersScheduled(stageIdentifiers,params);
		params.put("tenant_id", "BOIUK");
		params.put("X-BOI-CHANNEL", null);
		stageIdentifiers.setPaymentSetupVersion("2.0");
		delegate.createPispScaConsentRequestHeadersScheduled(stageIdentifiers,params);
		params.put("tenant_id", "BOIROI");
		params.put("X-BOI-CHANNEL", "B365");
		params.put("X-CORRELATION-ID", null);
		delegate.createPispScaConsentRequestHeadersScheduled(stageIdentifiers,params);
		params.put("tenant_id", null);
		params.put("X-BOI-USER", null);
		params.put("X-BOI-CHANNEL", "BOL");
		params.put("X-API-PARTY-SOURCE-ID-NUMBER", null);
		delegate.createPispScaConsentRequestHeadersScheduled(stageIdentifiers,params);
	}

	@Test
	public void testPispScaConsentFoundationServiceDelegate3() {

		ReflectionTestUtils.setField(delegate, "transactioReqHeader", "transactioReqHeader");
		ReflectionTestUtils.setField(delegate, "correlationMuleReqHeader", "correlation");
		ReflectionTestUtils.setField(delegate, "sourceUserReqHeader", "sorceuser");
		ReflectionTestUtils.setField(delegate, "sourceSystemReqHeader", "source");
		ReflectionTestUtils.setField(delegate, "apiChannelCode", "channel");
		ReflectionTestUtils.setField(delegate, "partysourceReqHeader", "partysource");
		ReflectionTestUtils.setField(delegate, "domesticPaymentConsentBaseURL", "jf");
		ReflectionTestUtils.setField(delegate, "channelBrand", "tenant_id");
		Map<String, String> params = new HashMap<>();

		params.put("X-API-SOURCE-SYSTEM", "fgrd");
		params.put("X-API-SOURCE-USER", "USER_ID");
		params.put("X-API-PARTY-SOURCE-ID-NUMBER", "PARTY_SOURCE_ID");
		params.put("X-API-TRANSACTION-ID", "TRANSACTION_ID");
		params.put("BOL", "BOL");
		params.put("tenant_id", "BOIUK");
		delegate.createPispScaConsentRequestHeadersStandingOrder(params);
	}

	@Test
	public void testPispScaConsentFoundationServiceDelegate5() {

		ReflectionTestUtils.setField(delegate, "transactioReqHeader", "transactioReqHeader");
		ReflectionTestUtils.setField(delegate, "correlationMuleReqHeader", "correlation");
		ReflectionTestUtils.setField(delegate, "sourceUserReqHeader", "sorceuser");
		ReflectionTestUtils.setField(delegate, "sourceSystemReqHeader", "source");
		ReflectionTestUtils.setField(delegate, "apiChannelCode", "channel");
		ReflectionTestUtils.setField(delegate, "partysourceReqHeader", "partysource");
		ReflectionTestUtils.setField(delegate, "domesticPaymentConsentBaseURL", "jf");
		ReflectionTestUtils.setField(delegate, "channelBrand", "tenant_id");
		Map<String, String> params = new HashMap<>();

		params.put("X-API-SOURCE-SYSTEM", "fgrd");
		params.put("X-API-SOURCE-USER", "USER_ID");
		params.put("X-API-PARTY-SOURCE-ID-NUMBER", "PARTY_SOURCE_ID");
		params.put("X-API-TRANSACTION-ID", "TRANSACTION_ID");
		params.put("BOL", "BOL");
		params.put("tenant_id", "BOIUK");
		delegate.createDomesticStandingOrderConsentRequestHeaders(params);
	}

	@Test
	public void testPispScaConsentFoundationServiceDelegateForUrl() {

		String version = "v2.0";
		String PaymentInstuctionProposalId = "1234";
		delegate.getPispScaConsentFoundationServiceURL(version, PaymentInstuctionProposalId);
		assertNotNull(delegate);
	}

	@Test(expected = AdapterException.class)
	public void testPispScaConsentFoundationServiceDelegateForUrl1() {

		String version = null;
		String PaymentInstuctionProposalId = "1234";
		delegate.getPispScaConsentFoundationServiceURL(version, PaymentInstuctionProposalId);

	}

	@Test(expected = AdapterException.class)
	public void testPispScaConsentFoundationServiceDelegateForUrl5() {

		String version = null;
		String PaymentInstuctionProposalId = "1234";
		delegate.putPaymentScheduledFoundationServiceURL(version, PaymentInstuctionProposalId);

	}

	@Test
	public void testPispScaConsentFoundationServiceDelegateForUrl7() {

		String version = "v1.0";
		String PaymentInstuctionProposalId = "1234";
		delegate.putPaymentScheduledFoundationServiceURL(version, PaymentInstuctionProposalId);

	}

	@Test(expected = AdapterException.class)
	public void testPispScaConsentFoundationServiceDelegateForUrlId() {

		String version = "v1.0";
		String PaymentInstuctionProposalId = null;
		delegate.putPaymentScheduledFoundationServiceURL(version, PaymentInstuctionProposalId);

	}

	@Test(expected = AdapterException.class)
	public void testPispScaConsentFoundationServiceDelegateForUrl4() {

		String version = null;
		String PaymentInstuctionProposalId = "1234";
		delegate.getPispScaConsentScheduledFoundationServiceURL(version, PaymentInstuctionProposalId);

	}

	@Test
	public void testPispScaConsentFoundationServiceDelegateForUrl65() {

		String version = "v1.0";
		String PaymentInstuctionProposalId = "1234";
		delegate.getPispScaConsentScheduledFoundationServiceURL(version, PaymentInstuctionProposalId);

	}

	@Test(expected = AdapterException.class)
	public void testPispScaConsentFoundationServiceDelegateForUrlId1() {

		String version = "v1.0";
		String PaymentInstuctionProposalId = null;
		delegate.getPispScaConsentScheduledFoundationServiceURL(version, PaymentInstuctionProposalId);

	}

	@Test(expected = AdapterException.class)
	public void testPispScaConsentFoundationServiceDelegateForUrlversion() {

		String version = null;
		String PaymentInstuctionProposalId = "1234";
		delegate.putDomesticStandingOrderFoundationServiceURL(version, PaymentInstuctionProposalId);

	}

	@Test(expected = AdapterException.class)
	public void testPispScaConsentFoundationServiceDelegateForUrlversion2() {

		String version = null;
		String PaymentInstuctionProposalId = "1234";
		delegate.getPispScaScheduleConsentFoundationServiceURL(version, PaymentInstuctionProposalId);

	}

	@Test(expected = AdapterException.class)
	public void testPispScaConsentFoundationServiceDelegateForUrlversion3() {

		String version = "v1.0";
		String PaymentInstuctionProposalId = null;
		delegate.getPispScaScheduleConsentFoundationServiceURL(version, PaymentInstuctionProposalId);

	}

	@Test
	public void testPispScaConsentFoundationServiceDelegateForUrlversion4() {

		String version = "v1.0";
		String PaymentInstuctionProposalId = "5786";
		delegate.getPispScaScheduleConsentFoundationServiceURL(version, PaymentInstuctionProposalId);

	}

	@Test(expected = AdapterException.class)
	public void testPispScaConsentFoundationServiceDelegateForUrl2() {

		String version = "v2.0";
		String PaymentInstuctionProposalId = null;
		delegate.getPispScaConsentFoundationServiceURL(version, PaymentInstuctionProposalId);

	}

	@Test
	public void testPispScaConsentFoundationServiceDelegateForUrlst() {

		String version = "v2.0";
		String PaymentInstuctionProposalId = "1234";
		delegate.validatePispScaConsentFoundationServiceURLStandingOrder(version, PaymentInstuctionProposalId);
		assertNotNull(delegate);
	}

	@Test(expected = AdapterException.class)
	public void testPispScaConsentFoundationServiceDelegateForUrlst1() {

		String version = null;
		String PaymentInstuctionProposalId = "1234";
		delegate.validatePispScaConsentFoundationServiceURLStandingOrder(version, PaymentInstuctionProposalId);

	}

	@Test(expected = AdapterException.class)
	public void testPispScaConsentFoundationServiceDelegateForUrlst2() {

		String version = "v2.0";
		String PaymentInstuctionProposalId = null;
		delegate.validatePispScaConsentFoundationServiceURLStandingOrder(version, PaymentInstuctionProposalId);

	}

	@Test
	public void testPispScaConsentFoundationServiceDelegateForUrgglst() {

		String version = "v2.0";
		String PaymentInstuctionProposalId = "1234";
		delegate.getDomesticStandingOrderConsentScheduledFoundationServiceURL(version, PaymentInstuctionProposalId);
		assertNotNull(delegate);
	}

	@Test(expected = AdapterException.class)
	public void testPispScaConsentFoundationServiceDelegateForUrlggst1() {

		String version = null;
		String PaymentInstuctionProposalId = "1234";
		delegate.getDomesticStandingOrderConsentScheduledFoundationServiceURL(version, PaymentInstuctionProposalId);

	}

	@Test(expected = AdapterException.class)
	public void testPispScaConsentFoundationServiceDelegateForUrgglst2() {

		String version = "v2.0";
		String PaymentInstuctionProposalId = null;
		delegate.getDomesticStandingOrderConsentScheduledFoundationServiceURL(version, PaymentInstuctionProposalId);

	}

	@Test
	public void testPispScaConsentFoundationServiceDelegateForUrggst() {

		String version = "v2.0";
		String PaymentInstuctionProposalId = "1234";
		delegate.putDomesticStandingOrderFoundationServiceURL(version, PaymentInstuctionProposalId);
		assertNotNull(delegate);
	}

	@Test(expected = AdapterException.class)
	public void testPispScaConsentFoundationServiceDelegateForUrlggsghht1() {

		String version = null;
		String PaymentInstuctionProposalId = "1234";
		delegate.putDomesticStandingOrderFoundationServiceURL(version, PaymentInstuctionProposalId);

	}

	@Test(expected = AdapterException.class)
	public void testPispScaConsentFoundationServiceDelegateForUrlggsghht3() {

		String version = null;
		String PaymentInstuctionProposalId = "1234";
		delegate.validatePispScaConsentFoundationServiceURLScheduled(version, PaymentInstuctionProposalId);

	}

	@Test
	public void testPispScaConsentFoundationServiceDelegateForUrlggsghht4() {

		String version = "v1.0";
		String PaymentInstuctionProposalId = "1234";
		delegate.validatePispScaConsentFoundationServiceURLScheduled(version, PaymentInstuctionProposalId);

	}

	@Test(expected = AdapterException.class)
	public void testPispScaConsentFoundationServiceDelegateForUrlggsghhtId2() {

		String version = "v1.0";
		String PaymentInstuctionProposalId = null;
		delegate.validatePispScaConsentFoundationServiceURLScheduled(version, PaymentInstuctionProposalId);

	}

	@Test(expected = AdapterException.class)
	public void testPispScaConsentFoundationServiceDelegateForUrgglhst2() {

		String version = "v2.0";
		String PaymentInstuctionProposalId = null;
		delegate.putDomesticStandingOrderFoundationServiceURL(version, PaymentInstuctionProposalId);

	}

	@Test(expected = AdapterException.class)
	public void testPispScaConsentFoundationServiceDelegateForUrgglhst2version() {

		String version = null;
		String PaymentInstuctionProposalId = null;
		delegate.putDomesticStandingOrderFoundationServiceURL(version, PaymentInstuctionProposalId);

	}

	@Test(expected = AdapterException.class)
	public void testPispScaConsentFoundationServiceDelegateForValidateNullcheck() {

		String version = "v2.0";
		String PaymentInstuctionProposalId = null;
		delegate.validatePispScaConsentFoundationServiceURL(version, PaymentInstuctionProposalId);

	}

	@Test
	public void testPispScaConsentFoundationServiceDelegateForUrlForValidate() {

		String version = "v2.0";
		String PaymentInstuctionProposalId = "1234";
		delegate.validatePispScaConsentFoundationServiceURL(version, PaymentInstuctionProposalId);
		assertNotNull(delegate);
	}

	@Test(expected = AdapterException.class)
	public void testPispScaConsentFoundationServiceDelegateForUrlForValidateversion() {

		String version = null;
		String PaymentInstuctionProposalId = "1234";
		delegate.validatePispScaConsentFoundationServiceURL(version, PaymentInstuctionProposalId);
		assertNotNull(delegate);
	}

	@Test(expected = AdapterException.class)
	public void testPispScaConsentFoundationServiceDelegateForUrlForValidateversion5() {

		String version = null;
		String PaymentInstuctionProposalId = "1234";
		delegate.putPaymentFoundationServiceURL(version, PaymentInstuctionProposalId);
		assertNotNull(delegate);
	}

	@Test(expected = AdapterException.class)
	public void testPispScaConsentFoundationServiceDelegateForUrlForValidateversionId3() {

		String version = "v1.0";
		String PaymentInstuctionProposalId = null;
		delegate.putPaymentFoundationServiceURL(version, PaymentInstuctionProposalId);
		assertNotNull(delegate);
	}

	@Test
	public void testPispScaConsentFoundationServiceDelegateForUrlForPut() {

		String version = "v2.0";
		String PaymentInstuctionProposalId = "1234";
		delegate.putPaymentFoundationServiceURL(version, PaymentInstuctionProposalId);
		assertNotNull(delegate);
	}

	@Test
	public void testgetPispScaConsentFoundationServiceURLForStandingOrders() {

		String version = "v2.0";
		String PaymentInstuctionProposalId = "1234";
		delegate.getPispScaConsentFoundationServiceURLForStandingOrders(PaymentInstuctionProposalId);
	}

}
