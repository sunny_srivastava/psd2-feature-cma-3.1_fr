package com.capgemini.psd2.foundationservice.pisp.sca.consent.boi.adapter.client;

import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.capgemini.psd2.foundationservice.pisp.sca.consent.boi.adapter.raml.domain.PaymentInstructionProposal;
import com.capgemini.psd2.foundationservice.pisp.sca.consent.boi.adapter.raml.domain.ScheduledPaymentInstructionProposal;
import com.capgemini.psd2.foundationservice.pisp.sca.consent.boi.adapter.raml.domain.StandingOrderInstructionProposal;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.RestClientSync;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.ObjectMapper;

@Component
public class PispScaConsentFoundationServiceClient {

	@Autowired
	@Qualifier("restClientFoundation")
	private RestClientSync restClient;

	@Autowired
	private RestTemplate restTemplate;

	@PostConstruct
	private void init() {
		List<HttpMessageConverter<?>> list = restTemplate.getMessageConverters();

		for (HttpMessageConverter<?> conv : list) {
			if (conv instanceof MappingJackson2HttpMessageConverter) {
				MappingJackson2HttpMessageConverter m = (MappingJackson2HttpMessageConverter) conv;
				m.getObjectMapper().setSerializationInclusion(Include.NON_NULL);
			}
		}

	}

	public PaymentInstructionProposal restTransportForPispScaConsentFoundationService(RequestInfo reqInfo,
			Class<PaymentInstructionProposal> responseType, HttpHeaders headers) {

		return restClient.callForGet(reqInfo, responseType, headers);
	}

	public ScheduledPaymentInstructionProposal restTransportForPispScaConsentFoundationService2(RequestInfo reqInfo,
			Class<ScheduledPaymentInstructionProposal> responseType, HttpHeaders headers) {

		return restClient.callForGet(reqInfo, responseType, headers);
	}

	public PaymentInstructionProposal restValidateTransportForPispScaConsentFoundationService(RequestInfo requestInfo,
			PaymentInstructionProposal paymentInstructionProposalRequest,
			Class<PaymentInstructionProposal> responseType, HttpHeaders httpHeaders) {

		return restClient.callForPost(requestInfo, paymentInstructionProposalRequest, responseType, httpHeaders);
	}

	public PaymentInstructionProposal restTransportForDomesticPaymentFoundationServicePut(RequestInfo requestInfo,
			PaymentInstructionProposal paymentInstructionProposal2, Class<PaymentInstructionProposal> responseType,
			HttpHeaders httpHeaders) {

		return restClient.callForPut(requestInfo, paymentInstructionProposal2, responseType, httpHeaders);
	}

	public ScheduledPaymentInstructionProposal restValidateTransportForPispScaConsentFoundationServiceScheduled(
			RequestInfo requestInfo, ScheduledPaymentInstructionProposal scheduledPaymentInstructionProposalRequest,
			Class<ScheduledPaymentInstructionProposal> responseType, HttpHeaders httpHeaders) {

		return restClient.callForPost(requestInfo, scheduledPaymentInstructionProposalRequest, responseType,
				httpHeaders);
	}

	public ScheduledPaymentInstructionProposal restTransportForPispScaConsentScheduledFoundationService(
			RequestInfo reqInfo, Class<ScheduledPaymentInstructionProposal> responseType, HttpHeaders headers) {

		return restClient.callForGet(reqInfo, responseType, headers);
	}

	public ScheduledPaymentInstructionProposal restTransportForDomesticScheduledPaymentFoundationServicePut(
			RequestInfo requestinfo,
			ScheduledPaymentInstructionProposal scheduledPaymentInstructionProposalResponseAfterTransform,
			Class<ScheduledPaymentInstructionProposal> responseType, HttpHeaders httpHeaders) {

		return restClient.callForPut(requestinfo, scheduledPaymentInstructionProposalResponseAfterTransform,
				responseType, httpHeaders);

	}

	/* Updated Domain Class StandingOrderInstructionProposal according to latest RAML of Domestic Standing Order */
	public com.capgemini.psd2.foundationservice.dso.sca.consent.boi.adapter.raml.domain.StandingOrderInstructionProposal restTransportForPispScaConsentFoundationServiceForStandingOrders(RequestInfo reqInfo,
			Class<com.capgemini.psd2.foundationservice.dso.sca.consent.boi.adapter.raml.domain.StandingOrderInstructionProposal> responseType, HttpHeaders headers) {

		return restClient.callForGet(reqInfo, responseType, headers);
	}

	public StandingOrderInstructionProposal restValidateTransportForPispScaConsentFoundationServiceStandingOrder(
			RequestInfo requestInfo, StandingOrderInstructionProposal standingOrderInstructionProposalRequest,
			Class<StandingOrderInstructionProposal> class1, HttpHeaders httpHeaders) {
		return restClient.callForPost(requestInfo, standingOrderInstructionProposalRequest, class1, httpHeaders);
	}

	public StandingOrderInstructionProposal restTransportForDomesticStandingOrderFoundationServicePut(
			RequestInfo requestinfo,
			StandingOrderInstructionProposal standingorderinstructionproposalResponseAfterTransform,
			Class<StandingOrderInstructionProposal> responseType, HttpHeaders httpHeaders) {

		return restClient.callForPut(requestinfo, standingorderinstructionproposalResponseAfterTransform, responseType,
				httpHeaders);

	}

}
