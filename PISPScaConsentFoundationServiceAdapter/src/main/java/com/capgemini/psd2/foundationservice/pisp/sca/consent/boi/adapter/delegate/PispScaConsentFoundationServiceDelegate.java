package com.capgemini.psd2.foundationservice.pisp.sca.consent.boi.adapter.delegate;

import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.adapter.exceptions.AdapterErrorCodeEnum;
import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.foundationservice.pisp.sca.consent.boi.adapter.constants.PispScaConsentFoundationServiceConstants;
import com.capgemini.psd2.foundationservice.pisp.sca.consent.boi.adapter.raml.domain.BrandCode3;
import com.capgemini.psd2.foundationservice.pisp.sca.consent.boi.adapter.raml.domain.ChannelCode;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Component
public class PispScaConsentFoundationServiceDelegate {

	@Value("${foundationService.sourceSystemReqHeader:#{X-API-SOURCE-SYSTEM}}")
	private String sourceSystemReqHeader;

	@Value("${foundationService.apiChannelCode:#{X-API-CHANNEL-CODE}}")
	private String apiChannelCode;

	@Value("${foundationService.sourceUserReqHeader:#{X-API-SOURCE-USER}}")
	private String sourceUserReqHeader;

	@Value("${foundationService.partysourceReqHeader:#{X-API-PARTY-SOURCE-ID-NUMBER}}")
	private String partysourceReqHeader;

	@Value("${foundationService.transactioReqHeader:#{X-API-TRANSACTION-ID}}")
	private String transactioReqHeader;

	@Value("${foundationService.correlationMuleReqHeader:#{X-API-CORRELATION-ID}}")
	private String correlationMuleReqHeader;

	@Value("${foundationService.systemApiVersion:#{X-SYSTEM-API-VERSION}}")
	private String systemApiVersion;

	@Value("${foundationService.channelBrand:#{X-API-CHANNEL-BRAND}}")
	private String channelBrand;

	@Value("${foundationService.domesticPaymentConsentBaseURL}")
	private String domesticPaymentConsentBaseURL;

	@Value("${foundationService.domesticPaymentConsentPutBaseURL}")
	private String domesticPaymentConsentPutBaseURL;

	@Value("${foundationService.domesticPaymentConsentPostBaseURL}")
	private String domesticPaymentConsentPostBaseURL;

	@Value("${foundationService.domesticPaymentScheduleConsentBaseURL}")
	private String domesticPaymentScheduleConsentBaseURL;

	@Value("${foundationService.domesticStandingOrderConsentBaseURL}")
	private String domesticStandingOrderConsentBaseURL;

	private String psd2API = "PSD2API";

	public HttpHeaders createPispScaConsentRequestHeaders(CustomPaymentStageIdentifiers stageIdentifiers,
			Map<String, String> params) {

		// payments
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setContentType(new MediaType(PispScaConsentFoundationServiceConstants.APPLICATION, "json"));
		httpHeaders.add(sourceSystemReqHeader, psd2API);

		if (!NullCheckUtils.isNullOrEmpty(params.get(PispScaConsentFoundationServiceConstants.CORRELATION_ID))) {
			httpHeaders.add(correlationMuleReqHeader,
					params.get(PispScaConsentFoundationServiceConstants.CORRELATION_ID));
		}
		if (!NullCheckUtils
				.isNullOrEmpty(params.get(PispScaConsentFoundationServiceConstants.CORRELATION_REQ_HEADER))) {
			httpHeaders.add(transactioReqHeader,
					params.get(PispScaConsentFoundationServiceConstants.CORRELATION_REQ_HEADER));
		}
		if (!NullCheckUtils.isNullOrEmpty(params.get(PSD2Constants.PARTY_IDENTIFIER))) {
			httpHeaders.add(partysourceReqHeader, params.get(PSD2Constants.PARTY_IDENTIFIER));
		}
		if (!NullCheckUtils.isNullOrEmpty(params.get(PSD2Constants.USER_IN_REQ_HEADER))) {
			httpHeaders.add(sourceUserReqHeader, params.get(PSD2Constants.USER_IN_REQ_HEADER));
		}

		if (!NullCheckUtils.isNullOrEmpty(params.get(PSD2Constants.CHANNEL_IN_REQ_HEADER))) {
			if (params.get(PSD2Constants.CHANNEL_IN_REQ_HEADER).equalsIgnoreCase("BOL")) {
				httpHeaders.add(apiChannelCode, ChannelCode.BOL.toString());
			}
			if (params.get(PSD2Constants.CHANNEL_IN_REQ_HEADER).equalsIgnoreCase("B365")) {
				httpHeaders.add(apiChannelCode, ChannelCode.B365.toString());
			}
		}
		// we are not getting tenant_id from Api we are configuring in Config
		// file.the code should be replace once api providing tenant_id
		if (!NullCheckUtils.isNullOrEmpty(params.get(PSD2Constants.TENANT_ID))) {
			if (params.get(PSD2Constants.TENANT_ID).equalsIgnoreCase("BOIUK")) {
				httpHeaders.add(channelBrand, BrandCode3.NIGB.toString());
			}
			if (params.get(PSD2Constants.TENANT_ID).equalsIgnoreCase("BOIROI")) {
				httpHeaders.add(channelBrand, BrandCode3.ROI.toString());
			}
		}
		if (stageIdentifiers.getPaymentSetupVersion().equalsIgnoreCase("1.0")) {
			httpHeaders.add(systemApiVersion, "1.1");
		} else {
			httpHeaders.add(systemApiVersion, "3.0");
		}
		return httpHeaders;

	}

	public HttpHeaders createDomesticStandingOrderConsentRequestHeaders(Map<String, String> params) {
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setContentType(new MediaType(PispScaConsentFoundationServiceConstants.APPLICATION, "json"));
		httpHeaders.add(sourceSystemReqHeader, psd2API);
		httpHeaders.add(correlationMuleReqHeader, "");
		httpHeaders.add(transactioReqHeader,
				params.get(PispScaConsentFoundationServiceConstants.CORRELATION_REQ_HEADER));
		httpHeaders.add(partysourceReqHeader, params.get(PispScaConsentFoundationServiceConstants.PARTY_SOURCE_ID));
		httpHeaders.add(sourceUserReqHeader, params.get(PSD2Constants.USER_IN_REQ_HEADER));
		httpHeaders.add(apiChannelCode, ChannelCode.B365.toString());
		return httpHeaders;

	}

	public HttpHeaders createPispScaConsentRequestHeadersScheduled(CustomPaymentStageIdentifiers stageIdentifiers,Map<String, String> params) {
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setContentType(new MediaType(PispScaConsentFoundationServiceConstants.APPLICATION, "json"));
		httpHeaders.add(sourceSystemReqHeader, psd2API);

		if (!NullCheckUtils
				.isNullOrEmpty(params.get(PispScaConsentFoundationServiceConstants.CORRELATION_REQ_HEADER))) {
			httpHeaders.add(transactioReqHeader,
					params.get(PispScaConsentFoundationServiceConstants.CORRELATION_REQ_HEADER));
		}
		if (!NullCheckUtils.isNullOrEmpty(params.get(PispScaConsentFoundationServiceConstants.PARTY_SOURCE_ID))) {
			httpHeaders.add(partysourceReqHeader, params.get(PispScaConsentFoundationServiceConstants.PARTY_SOURCE_ID));
		}
		if (!NullCheckUtils.isNullOrEmpty(params.get(PSD2Constants.TENANT_ID))) {
			if (params.get(PSD2Constants.TENANT_ID).equalsIgnoreCase("BOIUK")) {
				httpHeaders.add(channelBrand, BrandCode3.NIGB.toString());
			}
			if (params.get(PSD2Constants.TENANT_ID).equalsIgnoreCase("BOIROI")) {
				httpHeaders.add(channelBrand, BrandCode3.ROI.toString());
			}
		}
		if (!NullCheckUtils.isNullOrEmpty(params.get(PSD2Constants.USER_IN_REQ_HEADER))) {
			httpHeaders.add(sourceUserReqHeader, params.get(PSD2Constants.USER_IN_REQ_HEADER));
		}
		if (!NullCheckUtils.isNullOrEmpty(params.get(PSD2Constants.CHANNEL_IN_REQ_HEADER))) {
			if (params.get(PSD2Constants.CHANNEL_IN_REQ_HEADER).equalsIgnoreCase("BOL")) {
				httpHeaders.add(apiChannelCode, ChannelCode.BOL.toString());
			}
			if (params.get(PSD2Constants.CHANNEL_IN_REQ_HEADER).equalsIgnoreCase("B365")) {
				httpHeaders.add(apiChannelCode, ChannelCode.B365.toString());
			}
		}
		if (!NullCheckUtils.isNullOrEmpty(stageIdentifiers.getPaymentSetupVersion()))
		{
		httpHeaders.add(systemApiVersion, "3.0");
		}
		return httpHeaders;

	}

	public HttpHeaders createPispScaConsentRequestHeadersForStandingOrders(Map<String, String> params) {
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setContentType(new MediaType(PispScaConsentFoundationServiceConstants.APPLICATION, "json"));
		httpHeaders.add(sourceSystemReqHeader, psd2API);
		httpHeaders.add(correlationMuleReqHeader, "");
		httpHeaders.add(transactioReqHeader,
				params.get(PispScaConsentFoundationServiceConstants.CORRELATION_REQ_HEADER));
		httpHeaders.add(partysourceReqHeader, params.get(PispScaConsentFoundationServiceConstants.PARTY_SOURCE_ID));
		httpHeaders.add(sourceUserReqHeader, params.get(PSD2Constants.USER_IN_REQ_HEADER));
		httpHeaders.add(apiChannelCode, ChannelCode.B365.toString());
		if (!NullCheckUtils.isNullOrEmpty(params.get(PSD2Constants.TENANT_ID))) {
			if (params.get(PSD2Constants.TENANT_ID).equalsIgnoreCase("BOIUK")) {
				httpHeaders.add(channelBrand, BrandCode3.NIGB.toString());
			}
			if (params.get(PSD2Constants.TENANT_ID).equalsIgnoreCase("BOIROI")) {
				httpHeaders.add(channelBrand, BrandCode3.ROI.toString());
			}
		}
		return httpHeaders;

	}

	public HttpHeaders createPispScaConsentRequestHeadersStandingOrder(Map<String, String> params) {
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setContentType(new MediaType(PispScaConsentFoundationServiceConstants.APPLICATION, "json"));
		httpHeaders.add(sourceSystemReqHeader, psd2API);
		httpHeaders.add(correlationMuleReqHeader, "");
		httpHeaders.add(transactioReqHeader,
				params.get(PispScaConsentFoundationServiceConstants.CORRELATION_REQ_HEADER));
		httpHeaders.add(partysourceReqHeader, params.get(PispScaConsentFoundationServiceConstants.PARTY_SOURCE_ID));
		httpHeaders.add(sourceUserReqHeader, params.get(PSD2Constants.USER_IN_REQ_HEADER));
		httpHeaders.add(apiChannelCode, ChannelCode.B365.toString());
		return httpHeaders;
	}

	public String getPispScaConsentFoundationServiceURL(String version, String paymentInstuctionProposalId) {

		if (NullCheckUtils.isNullOrEmpty(version)) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.NO_ACCOUNT_DATA_FOUND);
		}

		if (NullCheckUtils.isNullOrEmpty(paymentInstuctionProposalId)) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.NO_ACCOUNT_DATA_FOUND);
		}

		return domesticPaymentConsentBaseURL + "/" + PispScaConsentFoundationServiceConstants.VERSION_V1 + "/"
				+ "domestic/payment-instruction-proposals" + "/" + paymentInstuctionProposalId;
	}

	public String getPispScaScheduleConsentFoundationServiceURL(String version, String paymentInstuctionProposalId) {

		if (NullCheckUtils.isNullOrEmpty(version)) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.NO_ACCOUNT_DATA_FOUND);
		}

		if (NullCheckUtils.isNullOrEmpty(paymentInstuctionProposalId)) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.NO_ACCOUNT_DATA_FOUND);
		}

		return domesticPaymentScheduleConsentBaseURL + "/" + PispScaConsentFoundationServiceConstants.VERSION_V1
				+ "/domestic/scheduled/payment-instruction-proposals" + "/" + paymentInstuctionProposalId;
	}

	public String validatePispScaConsentFoundationServiceURL(String version, String paymentInstuctionProposalId) {

		if (NullCheckUtils.isNullOrEmpty(version)) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.NO_ACCOUNT_DATA_FOUND);
		}

		if (NullCheckUtils.isNullOrEmpty(paymentInstuctionProposalId)) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.NO_ACCOUNT_DATA_FOUND);
		}

		return domesticPaymentConsentPostBaseURL + "/" + PispScaConsentFoundationServiceConstants.VERSION_V1 + "/"
				+ "domestic/payment-instruction-proposals/validate";

	}

	public String putPaymentFoundationServiceURL(String version, String paymentInstuctionProposalId) {

		if (NullCheckUtils.isNullOrEmpty(version)) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.NO_ACCOUNT_DATA_FOUND);
		}

		if (NullCheckUtils.isNullOrEmpty(paymentInstuctionProposalId)) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.NO_ACCOUNT_DATA_FOUND);
		}

		return domesticPaymentConsentPutBaseURL + "/" + PispScaConsentFoundationServiceConstants.VERSION_V1 + "/"
				+ "domestic/payment-instruction-proposals" + "/" + paymentInstuctionProposalId;
	}

	public String validatePispScaConsentFoundationServiceURLScheduled(String version,
			String paymentInstuctionProposalId) {

		if (NullCheckUtils.isNullOrEmpty(version)) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.NO_ACCOUNT_DATA_FOUND);
		}

		if (NullCheckUtils.isNullOrEmpty(paymentInstuctionProposalId)) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.NO_ACCOUNT_DATA_FOUND);
		}

		return domesticPaymentScheduleConsentBaseURL + "/" + PispScaConsentFoundationServiceConstants.VERSION_V1 + "/"
				+ "domestic/scheduled/payment-instruction-proposals/validate";

	}

	public String getPispScaConsentScheduledFoundationServiceURL(String version, String paymentInstuctionProposalId) {

		if (NullCheckUtils.isNullOrEmpty(version)) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.NO_ACCOUNT_DATA_FOUND);
		}

		if (NullCheckUtils.isNullOrEmpty(paymentInstuctionProposalId)) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.NO_ACCOUNT_DATA_FOUND);
		}

		return domesticPaymentScheduleConsentBaseURL + "/" + PispScaConsentFoundationServiceConstants.VERSION_V1 + "/"
				+ "domestic/scheduled/payment-instruction-proposals" + "/" + paymentInstuctionProposalId;
	}

	public String putPaymentScheduledFoundationServiceURL(String version, String paymentInstuctionProposalId) {

		if (NullCheckUtils.isNullOrEmpty(version)) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.NO_ACCOUNT_DATA_FOUND);
		}

		if (NullCheckUtils.isNullOrEmpty(paymentInstuctionProposalId)) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.NO_ACCOUNT_DATA_FOUND);
		}

		return domesticPaymentScheduleConsentBaseURL + "/" + PispScaConsentFoundationServiceConstants.VERSION_V1 + "/"
				+ "domestic/scheduled/payment-instruction-proposals" + "/" + paymentInstuctionProposalId;
	}

	public String getPispScaConsentFoundationServiceURLForStandingOrders(String paymentInstuctionProposalId) {

		return domesticStandingOrderConsentBaseURL + "/" + PispScaConsentFoundationServiceConstants.VERSION_V1 + "/"
				+ "domestic/standing-orders/payment-instruction-proposals" + "/" + paymentInstuctionProposalId;
	}

	public String validatePispScaConsentFoundationServiceURLStandingOrder(String paymentSetupVersion,
			String paymentConsentId) {
		if (NullCheckUtils.isNullOrEmpty(paymentSetupVersion)) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.NO_ACCOUNT_DATA_FOUND);
		}

		if (NullCheckUtils.isNullOrEmpty(paymentConsentId)) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.NO_ACCOUNT_DATA_FOUND);
		}

		return domesticStandingOrderConsentBaseURL + "/v" + paymentSetupVersion + "/"
				+ "domestic/standing-orders/payment-instruction-proposals" + "/validate";
	}

	public String getDomesticStandingOrderConsentScheduledFoundationServiceURL(String version,
			String paymentInstuctionProposalId) {

		if (NullCheckUtils.isNullOrEmpty(version)) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.NO_ACCOUNT_DATA_FOUND);
		}

		if (NullCheckUtils.isNullOrEmpty(paymentInstuctionProposalId)) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.NO_ACCOUNT_DATA_FOUND);
		}

		return domesticStandingOrderConsentBaseURL + "/v" + version + "/"
				+ "domestic/standing-orders/payment-instruction-proposals" + "/" + paymentInstuctionProposalId;
	}

	public String putDomesticStandingOrderFoundationServiceURL(String version, String paymentInstuctionProposalId) {

		if (NullCheckUtils.isNullOrEmpty(version)) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.NO_ACCOUNT_DATA_FOUND);
		}

		if (NullCheckUtils.isNullOrEmpty(paymentInstuctionProposalId)) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.NO_ACCOUNT_DATA_FOUND);
		}

		return domesticStandingOrderConsentBaseURL + "/v" + version + "/"
				+ "domestic/standing-orders/payment-instruction-proposals" + "/" + paymentInstuctionProposalId;
	}

}
