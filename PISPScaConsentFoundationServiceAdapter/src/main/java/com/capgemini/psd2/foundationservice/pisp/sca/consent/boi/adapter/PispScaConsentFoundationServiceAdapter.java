package com.capgemini.psd2.foundationservice.pisp.sca.consent.boi.adapter;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.adapter.exceptions.AdapterErrorCodeEnum;
import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.adapter.exceptions.domain.CommonResponse;
import com.capgemini.psd2.adapter.exceptions.domain.ValidationWrapper;
import com.capgemini.psd2.adapter.utility.AdapterUtility;
import com.capgemini.psd2.foundationservice.dso.sca.consent.boi.adapter.DomesticStandingOrderScaConsentFoundationServiceAdapter;
import com.capgemini.psd2.foundationservice.pisp.sca.consent.boi.adapter.client.PispScaConsentFoundationServiceClient;
import com.capgemini.psd2.foundationservice.pisp.sca.consent.boi.adapter.constants.PispScaConsentFoundationServiceConstants;
import com.capgemini.psd2.foundationservice.pisp.sca.consent.boi.adapter.delegate.PispScaConsentFoundationServiceDelegate;
import com.capgemini.psd2.foundationservice.pisp.sca.consent.boi.adapter.raml.domain.PaymentInstructionProposal;
import com.capgemini.psd2.foundationservice.pisp.sca.consent.boi.adapter.raml.domain.ScheduledPaymentInstructionProposal;
import com.capgemini.psd2.foundationservice.pisp.sca.consent.boi.adapter.transformer.PispScaConsentFoundationServiceTransformer;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.pisp.domain.OBCashAccountDebtor3;
import com.capgemini.psd2.pisp.domain.PaymentConsentsValidationResponse;
import com.capgemini.psd2.pisp.enums.PaymentTypeEnum;
import com.capgemini.psd2.pisp.sca.consent.operations.adapter.PispScaConsentOperationsAdapter;
import com.capgemini.psd2.pisp.stage.domain.CustomConsentAppViewData;
import com.capgemini.psd2.pisp.stage.domain.CustomFraudSystemPaymentData;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageUpdateData;
import com.capgemini.psd2.pisp.stage.domain.CustomPreAuthorizeAdditionalInfo;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.utilities.NullCheckUtils;
import com.google.gson.Gson;

@Component
public class PispScaConsentFoundationServiceAdapter implements PispScaConsentOperationsAdapter {

	@Autowired
	private PispScaConsentFoundationServiceClient pispScaConsClient;

	@Autowired
	private PispScaConsentFoundationServiceDelegate pispScaConsDelgate;

	@Autowired
	private PispScaConsentFoundationServiceTransformer pispScaConsTransformer;

	@Autowired
	private AdapterUtility adapterUtility;

	@Autowired
	private DomesticStandingOrderScaConsentFoundationServiceAdapter domesticStandingOrderScaConsentFoundationServiceAdapter;

	@Override
	public CustomConsentAppViewData retrieveConsentAppStagedViewData(CustomPaymentStageIdentifiers stageIdentifier,
			Map<String, String> params) {

		CustomConsentAppViewData customConsentAppViewData = null;

		if (stageIdentifier == null || stageIdentifier.getPaymentConsentId() == null
				|| stageIdentifier.getPaymentSetupVersion() == null) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.BAD_REQUEST);
		}

		if (params == null) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.BAD_REQUEST);
		}

		RequestInfo requestinfo = new RequestInfo();

		if (stageIdentifier.getPaymentTypeEnum().equals(PaymentTypeEnum.DOMESTIC_PAY)) {
			HttpHeaders httpHeaders = pispScaConsDelgate.createPispScaConsentRequestHeaders(stageIdentifier, params);
			String pispScaConsUrl = pispScaConsDelgate.getPispScaConsentFoundationServiceURL(
					stageIdentifier.getPaymentSetupVersion(), stageIdentifier.getPaymentConsentId());
			requestinfo.setUrl(pispScaConsUrl);
			PaymentInstructionProposal paymentInstructionProposal = pispScaConsClient
					.restTransportForPispScaConsentFoundationService(requestinfo, PaymentInstructionProposal.class,
							httpHeaders);

			customConsentAppViewData = pispScaConsTransformer
					.transformPispScaConsentResponse(paymentInstructionProposal, params);
			customConsentAppViewData.setPaymentType(PaymentTypeEnum.DOMESTIC_PAY.getPaymentType());
		}

		else if (stageIdentifier.getPaymentTypeEnum().equals(PaymentTypeEnum.DOMESTIC_SCH_PAY)) {
			HttpHeaders httpHeaders = pispScaConsDelgate.createPispScaConsentRequestHeadersScheduled(stageIdentifier,
					params);
			String pispScaConsUrl = pispScaConsDelgate.getPispScaScheduleConsentFoundationServiceURL(
					stageIdentifier.getPaymentSetupVersion(), stageIdentifier.getPaymentConsentId());
			requestinfo.setUrl(pispScaConsUrl);
			ScheduledPaymentInstructionProposal scheduledPaymentInstructionProposal = pispScaConsClient
					.restTransportForPispScaConsentFoundationService2(requestinfo,
							ScheduledPaymentInstructionProposal.class, httpHeaders);

			customConsentAppViewData = pispScaConsTransformer
					.transformPispScaConsentResponseScheduled(scheduledPaymentInstructionProposal, params);
			customConsentAppViewData.setPaymentType(PaymentTypeEnum.DOMESTIC_SCH_PAY.getPaymentType());
		} else if (stageIdentifier.getPaymentTypeEnum().equals(PaymentTypeEnum.DOMESTIC_ST_ORD)) {
			HttpHeaders httpHeaders = pispScaConsDelgate.createPispScaConsentRequestHeadersForStandingOrders(params);
			String pispScaConsUrl = pispScaConsDelgate
					.getPispScaConsentFoundationServiceURLForStandingOrders(stageIdentifier.getPaymentConsentId());
			requestinfo.setUrl(pispScaConsUrl);

			/*
			 * Updated Domain Class StandingOrderInstructionProposal according to latest
			 * RAML of Domestic Standing Order
			 */
			com.capgemini.psd2.foundationservice.dso.sca.consent.boi.adapter.raml.domain.StandingOrderInstructionProposal standingOrderInstructionProposal = pispScaConsClient
					.restTransportForPispScaConsentFoundationServiceForStandingOrders(requestinfo,
							com.capgemini.psd2.foundationservice.dso.sca.consent.boi.adapter.raml.domain.StandingOrderInstructionProposal.class,
							httpHeaders);
			customConsentAppViewData = pispScaConsTransformer
					.transformPispScaConsentResponseForStandingOrders(standingOrderInstructionProposal, params);
			customConsentAppViewData.setPaymentType(PaymentTypeEnum.DOMESTIC_ST_ORD.getPaymentType());

		}
		return customConsentAppViewData;
	}

	@Override
	public CustomFraudSystemPaymentData retrieveFraudSystemPaymentStagedData(
			CustomPaymentStageIdentifiers stageIdentifiers, Map<String, String> params) {
		CustomFraudSystemPaymentData customFraudSystemPaymentData = new CustomFraudSystemPaymentData();

		if (stageIdentifiers == null || stageIdentifiers.getPaymentConsentId() == null
				|| stageIdentifiers.getPaymentSetupVersion() == null) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.BAD_REQUEST);
		}

		if (params == null) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.BAD_REQUEST);
		}

		RequestInfo requestinfo = new RequestInfo();

		if (stageIdentifiers.getPaymentTypeEnum().equals(PaymentTypeEnum.DOMESTIC_PAY)) {
			HttpHeaders httpHeaders = pispScaConsDelgate.createPispScaConsentRequestHeaders(stageIdentifiers, params);
			String pispScaConsUrl = pispScaConsDelgate.getPispScaConsentFoundationServiceURL(
					stageIdentifiers.getPaymentSetupVersion(), stageIdentifiers.getPaymentConsentId());
			requestinfo.setUrl(pispScaConsUrl);
			PaymentInstructionProposal paymentInstructionProposal = pispScaConsClient
					.restTransportForPispScaConsentFoundationService(requestinfo, PaymentInstructionProposal.class,
							httpHeaders);
			customFraudSystemPaymentData = pispScaConsTransformer
					.transformFraudSystemDomesticPaymentStagedData(paymentInstructionProposal);
			customFraudSystemPaymentData.setPaymentType(PaymentTypeEnum.DOMESTIC_PAY.getPaymentType());

		} else if (stageIdentifiers.getPaymentTypeEnum().equals(PaymentTypeEnum.DOMESTIC_SCH_PAY)) {
			HttpHeaders httpHeaders = pispScaConsDelgate.createPispScaConsentRequestHeadersScheduled(stageIdentifiers,
					params);
			String pispScaConsUrl = pispScaConsDelgate.getPispScaScheduleConsentFoundationServiceURL(
					stageIdentifiers.getPaymentSetupVersion(), stageIdentifiers.getPaymentConsentId());
			requestinfo.setUrl(pispScaConsUrl);
			ScheduledPaymentInstructionProposal scheduledPaymentInstructionProposal = pispScaConsClient
					.restTransportForPispScaConsentFoundationService2(requestinfo,
							ScheduledPaymentInstructionProposal.class, httpHeaders);

			customFraudSystemPaymentData = pispScaConsTransformer
					.transformFraudSystemDomesticScheduledPaymentStagedData(scheduledPaymentInstructionProposal);
			customFraudSystemPaymentData.setPaymentType(PaymentTypeEnum.DOMESTIC_SCH_PAY.getPaymentType());

		} else if (stageIdentifiers.getPaymentTypeEnum().equals(PaymentTypeEnum.DOMESTIC_ST_ORD)) {
			HttpHeaders httpHeaders = pispScaConsDelgate.createPispScaConsentRequestHeadersForStandingOrders(params);
			String pispScaConsUrl = pispScaConsDelgate
					.getPispScaConsentFoundationServiceURLForStandingOrders(stageIdentifiers.getPaymentConsentId());
			requestinfo.setUrl(pispScaConsUrl);
			/*
			 * Updated Domain Class StandingOrderInstructionProposal according to latest
			 * RAML of Domestic Standing Order
			 */
			com.capgemini.psd2.foundationservice.dso.sca.consent.boi.adapter.raml.domain.StandingOrderInstructionProposal standingOrderInstructionProposal = pispScaConsClient
					.restTransportForPispScaConsentFoundationServiceForStandingOrders(requestinfo,
							com.capgemini.psd2.foundationservice.dso.sca.consent.boi.adapter.raml.domain.StandingOrderInstructionProposal.class,
							httpHeaders);

			customFraudSystemPaymentData = pispScaConsTransformer
					.transformFraudSystemDomesticStandingOrderPaymentStagedData(standingOrderInstructionProposal);
			customFraudSystemPaymentData.setPaymentType(PaymentTypeEnum.DOMESTIC_ST_ORD.getPaymentType());
		}

		return customFraudSystemPaymentData;
	}

	public PaymentInstructionProposal retrieveConsentAppStagedViewDataForValidate(
			CustomPaymentStageIdentifiers stageIdentifiers, Map<String, String> params) {

		if (stageIdentifiers == null || stageIdentifiers.getPaymentConsentId() == null
				|| stageIdentifiers.getPaymentSetupVersion() == null) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.BAD_REQUEST);
		}

		if (params == null) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.BAD_REQUEST);
		}

		RequestInfo requestinfo = new RequestInfo();
		HttpHeaders httpHeaders = pispScaConsDelgate.createPispScaConsentRequestHeaders(stageIdentifiers, params);
		String pispScaConsUrl = pispScaConsDelgate.getPispScaConsentFoundationServiceURL(
				stageIdentifiers.getPaymentSetupVersion(), stageIdentifiers.getPaymentConsentId());
		requestinfo.setUrl(pispScaConsUrl);
		return pispScaConsClient.restTransportForPispScaConsentFoundationService(requestinfo,
				PaymentInstructionProposal.class, httpHeaders);
	}

	public ScheduledPaymentInstructionProposal retrieveConsentAppStagedViewDataForValidateScheduled(
			CustomPaymentStageIdentifiers stageIdentifiers, Map<String, String> params) {

		if (stageIdentifiers == null || stageIdentifiers.getPaymentConsentId() == null
				|| stageIdentifiers.getPaymentSetupVersion() == null) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.BAD_REQUEST);
		}

		if (params == null) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.BAD_REQUEST);
		}

		RequestInfo requestinfo = new RequestInfo();
		HttpHeaders httpHeaders = pispScaConsDelgate.createPispScaConsentRequestHeadersScheduled(stageIdentifiers,
				params);
		String pispScaConsUrl = pispScaConsDelgate.getPispScaScheduleConsentFoundationServiceURL(
				stageIdentifiers.getPaymentSetupVersion(), stageIdentifiers.getPaymentConsentId());
		requestinfo.setUrl(pispScaConsUrl);
		return pispScaConsClient.restTransportForPispScaConsentFoundationService2(requestinfo,
				ScheduledPaymentInstructionProposal.class, httpHeaders);
	}

	public com.capgemini.psd2.foundationservice.dso.sca.consent.boi.adapter.raml.domain.StandingOrderInstructionProposal retrieveConsentAppStagedViewDataForValidateStandingOrder(
			CustomPaymentStageIdentifiers stageIdentifiers, Map<String, String> params) {
		if (stageIdentifiers == null || stageIdentifiers.getPaymentConsentId() == null
				|| stageIdentifiers.getPaymentSetupVersion() == null) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.BAD_REQUEST);
		}

		if (params == null) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.BAD_REQUEST);
		}

		RequestInfo requestinfo = new RequestInfo();
		HttpHeaders httpHeaders = pispScaConsDelgate.createPispScaConsentRequestHeadersStandingOrder(params);
		String pispScaConsUrl = pispScaConsDelgate
				.getPispScaConsentFoundationServiceURLForStandingOrders(stageIdentifiers.getPaymentConsentId());
		requestinfo.setUrl(pispScaConsUrl);
		return pispScaConsClient.restTransportForPispScaConsentFoundationServiceForStandingOrders(requestinfo,
				com.capgemini.psd2.foundationservice.dso.sca.consent.boi.adapter.raml.domain.StandingOrderInstructionProposal.class,
				httpHeaders);

	}

	@Override
	public PaymentConsentsValidationResponse validatePreAuthorisation(OBCashAccountDebtor3 selectedDebtorDetails,
			CustomPreAuthorizeAdditionalInfo preAuthAdditionalInfo, CustomPaymentStageIdentifiers stageIdentifiers,
			Map<String, String> params) {

		RequestInfo requestInfo = new RequestInfo();
		CommonResponse commonResponse = null;

		PaymentConsentsValidationResponse paymentConsentsValidationResponse = new PaymentConsentsValidationResponse();
		if (stageIdentifiers.getPaymentTypeEnum().equals(PaymentTypeEnum.DOMESTIC_PAY)) {

			HttpHeaders httpHeaders = pispScaConsDelgate.createPispScaConsentRequestHeaders(stageIdentifiers, params);

			String domesticPaymentPostURL = pispScaConsDelgate.validatePispScaConsentFoundationServiceURL(
					stageIdentifiers.getPaymentSetupVersion(), stageIdentifiers.getPaymentConsentId());

			requestInfo.setUrl(domesticPaymentPostURL);

			PaymentInstructionProposal paymentInstructionProposal = retrieveConsentAppStagedViewDataForValidate(
					stageIdentifiers, params);
			params.put("accountIban", preAuthAdditionalInfo.getAccountIban());
			PaymentInstructionProposal paymentInstructionProposalRequest = pispScaConsTransformer
					.transformDomesticConsentResponseFromAPIToFDForInsert(selectedDebtorDetails,
							paymentInstructionProposal, params);

			PaymentInstructionProposal paymentInstructionProposalResponse = null;
			try {
				paymentInstructionProposalResponse = pispScaConsClient
						.restValidateTransportForPispScaConsentFoundationService(requestInfo,
								paymentInstructionProposalRequest, PaymentInstructionProposal.class, httpHeaders);
			} catch (AdapterException e) {
				if ((String.valueOf(e.getFoundationError()).trim()
						.equals(PispScaConsentFoundationServiceConstants.MULE_OUT_OF_BOX_POLICY_ERROR1))
						|| (String.valueOf(e.getFoundationError()).trim()
								.equals(PispScaConsentFoundationServiceConstants.MULE_OUT_OF_BOX_POLICY_ERROR2))) {
					throw AdapterException.populatePSD2Exception(e.getMessage(),
							AdapterErrorCodeEnum.TECHNICAL_ERROR_PPS_PIPV);
				}
				commonResponse = (CommonResponse) e.getFoundationError();
				if (commonResponse.getCode().equals(PispScaConsentFoundationServiceConstants.DOMESTIC_PROCESS_CODE)
						&& commonResponse.getAdditionalInformation().getDetails().toString()
								.contains(PispScaConsentFoundationServiceConstants.VALIDATION_VIOLATIONS)) {
					Gson gson = new Gson();
					ValidationWrapper validationViolations = gson.fromJson(
							gson.toJson(commonResponse.getAdditionalInformation().getDetails()),
							ValidationWrapper.class);
					String fsErrorCode = validationViolations.getValidationViolations().getValidationViolation()
							.getErrorCode();
					String errorMapping = adapterUtility.getUiErrorMap().get(fsErrorCode);
					if (!NullCheckUtils.isNullOrEmpty(errorMapping)) {
						AdapterErrorCodeEnum errorCodeEnum = AdapterErrorCodeEnum.valueOf(errorMapping);
						throw AdapterException.populatePSD2Exception(e.getMessage(), errorCodeEnum);
					}
				}
				throw e;
			}
			paymentConsentsValidationResponse = pispScaConsTransformer
					.transformDomesticConsentResponseFromFDToAPIForInsert(paymentInstructionProposalResponse,
							stageIdentifiers);
		}

		else if (stageIdentifiers.getPaymentTypeEnum().equals(PaymentTypeEnum.DOMESTIC_SCH_PAY)) {

			HttpHeaders httpHeaders = pispScaConsDelgate.createPispScaConsentRequestHeadersScheduled(stageIdentifiers,
					params);

			String domesticPaymentPostURL = pispScaConsDelgate.validatePispScaConsentFoundationServiceURLScheduled(
					stageIdentifiers.getPaymentSetupVersion(), stageIdentifiers.getPaymentConsentId());

			requestInfo.setUrl(domesticPaymentPostURL);

			ScheduledPaymentInstructionProposal scheduledPaymentInstructionProposal = retrieveConsentAppStagedViewDataForValidateScheduled(
					stageIdentifiers, params);

			params.put("accountIban", preAuthAdditionalInfo.getAccountIban());
			System.out.println(
					"Validate Adapter 'validatePreAuthorisatio()'---------->> " + selectedDebtorDetails.toString());

			ScheduledPaymentInstructionProposal scheduledPaymentInstructionProposalRequest = pispScaConsTransformer
					.transformDomesticScheduledConsentResponseFromAPIToFDForInsert(selectedDebtorDetails,
							scheduledPaymentInstructionProposal, params);

			try {
				pispScaConsClient.restValidateTransportForPispScaConsentFoundationServiceScheduled(requestInfo,
						scheduledPaymentInstructionProposalRequest, ScheduledPaymentInstructionProposal.class,
						httpHeaders);
			} catch (AdapterException e) {
				if ((String.valueOf(e.getFoundationError()).trim()
						.equals(PispScaConsentFoundationServiceConstants.MULE_OUT_OF_BOX_POLICY_ERROR1))
						|| (String.valueOf(e.getFoundationError()).trim()
								.equals(PispScaConsentFoundationServiceConstants.MULE_OUT_OF_BOX_POLICY_ERROR2))) {
					throw AdapterException.populatePSD2Exception(e.getMessage(),
							AdapterErrorCodeEnum.TECHNICAL_ERROR_PPS_PIPV);
				}

				commonResponse = (CommonResponse) e.getFoundationError();
				if (commonResponse.getCode().equals(PispScaConsentFoundationServiceConstants.SCHEDULED_PROCESS_CODE)
						&& commonResponse.getAdditionalInformation().getDetails().toString()
								.contains(PispScaConsentFoundationServiceConstants.VALIDATION_VIOLATIONS)) {
					Gson gson = new Gson();
					ValidationWrapper validationViolations = gson.fromJson(
							gson.toJson(commonResponse.getAdditionalInformation().getDetails()),
							ValidationWrapper.class);
					String fsErrorCode = validationViolations.getValidationViolations().getValidationViolation()
							.getErrorCode();
					String errorMapping = adapterUtility.getUiErrorMap().get(fsErrorCode);
					if (!NullCheckUtils.isNullOrEmpty(errorMapping)) {
						AdapterErrorCodeEnum errorCodeEnum = AdapterErrorCodeEnum.valueOf(errorMapping);
						throw AdapterException.populatePSD2Exception(e.getMessage(), errorCodeEnum);
					}
				}
				throw e;
			}
			paymentConsentsValidationResponse = pispScaConsTransformer
					.transformDomesticScheduledConsentResponseFromFDToAPIForInsert(stageIdentifiers);
		}

		else if (stageIdentifiers.getPaymentTypeEnum().equals(PaymentTypeEnum.DOMESTIC_ST_ORD)) {

			// Suppressing Validate Call For Domeestic Standing Order Consent

			paymentConsentsValidationResponse = pispScaConsTransformer
					.transformDomesticStandingOrderConsentResponseFromFDToAPIForInsert(stageIdentifiers);
		}

		return paymentConsentsValidationResponse;
	}

	@Override
	public void updatePaymentStageData(CustomPaymentStageIdentifiers stageIdentifiers,
			CustomPaymentStageUpdateData updateData, Map<String, String> params) {

		if (stageIdentifiers == null || stageIdentifiers.getPaymentConsentId() == null
				|| stageIdentifiers.getPaymentSetupVersion() == null) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.BAD_REQUEST);
		}
	
		if (params == null || (params.get(PSD2Constants.TENANT_ID)==null)) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.BAD_REQUEST);
		}
		
		RequestInfo requestinfo = new RequestInfo();
		if (stageIdentifiers.getPaymentTypeEnum().equals(PaymentTypeEnum.DOMESTIC_PAY)) {
			HttpHeaders httpHeaders = pispScaConsDelgate.createPispScaConsentRequestHeaders(stageIdentifiers, params);
			String pispScaConUrlForGet = pispScaConsDelgate.getPispScaConsentFoundationServiceURL(
					stageIdentifiers.getPaymentSetupVersion(), stageIdentifiers.getPaymentConsentId());
			requestinfo.setUrl(pispScaConUrlForGet);
			PaymentInstructionProposal paymentInstructionProposalResponseAfterGet = pispScaConsClient
					.restTransportForPispScaConsentFoundationService(requestinfo, PaymentInstructionProposal.class,
							httpHeaders);
			PaymentInstructionProposal paymentInstructionProposalResponseAfterTransform = pispScaConsTransformer
					.transformPispScaConsentResponseForUpdate(updateData, paymentInstructionProposalResponseAfterGet,
							params);
			String pispScaConsUrl = pispScaConsDelgate.putPaymentFoundationServiceURL(
					stageIdentifiers.getPaymentSetupVersion(), stageIdentifiers.getPaymentConsentId());
			requestinfo.setUrl(pispScaConsUrl);
			pispScaConsClient.restTransportForDomesticPaymentFoundationServicePut(requestinfo,
					paymentInstructionProposalResponseAfterTransform, PaymentInstructionProposal.class, httpHeaders);
		} else if (stageIdentifiers.getPaymentTypeEnum().equals(PaymentTypeEnum.DOMESTIC_SCH_PAY)) {
			HttpHeaders httpHeaders = pispScaConsDelgate.createPispScaConsentRequestHeadersScheduled(stageIdentifiers,
					params);
			String pispScaConUrlForGet = pispScaConsDelgate.getPispScaConsentScheduledFoundationServiceURL(
					stageIdentifiers.getPaymentSetupVersion(), stageIdentifiers.getPaymentConsentId());
			requestinfo.setUrl(pispScaConUrlForGet);
			System.out.println("RestCallForRetrieveBeforeUpdate....");
			ScheduledPaymentInstructionProposal scheduledPaymentInstructionProposalResponseAfterGet = pispScaConsClient
					.restTransportForPispScaConsentScheduledFoundationService(requestinfo,
							ScheduledPaymentInstructionProposal.class, httpHeaders);
			ScheduledPaymentInstructionProposal scheduledPaymentInstructionProposalResponseAfterTransform = pispScaConsTransformer
					.transformPispScaConsentScheduledResponseForUpdate(updateData,
							scheduledPaymentInstructionProposalResponseAfterGet, params);
			String pispScaConsUrl = pispScaConsDelgate.putPaymentScheduledFoundationServiceURL(
					stageIdentifiers.getPaymentSetupVersion(), stageIdentifiers.getPaymentConsentId());
			requestinfo.setUrl(pispScaConsUrl);
			pispScaConsClient.restTransportForDomesticScheduledPaymentFoundationServicePut(requestinfo,
					scheduledPaymentInstructionProposalResponseAfterTransform,
					ScheduledPaymentInstructionProposal.class, httpHeaders);
		} else if (stageIdentifiers.getPaymentTypeEnum().equals(PaymentTypeEnum.DOMESTIC_ST_ORD)) {														
			
			if (!NullCheckUtils.isNullOrEmpty((updateData.getSetupStatus()))									
					&& (updateData.getSetupStatus().equalsIgnoreCase("Rejected"))) {
				/* Reject Call */
				domesticStandingOrderScaConsentFoundationServiceAdapter.rejectstatusforDSO(stageIdentifiers, updateData, params);
			} else 	if (!NullCheckUtils.isNullOrEmpty((updateData.getSetupStatus()))									
					&& (updateData.getSetupStatus().equalsIgnoreCase("Authorised"))) {
				System.out.println("Suppressed Update Call during ACG");
			
			}
			else
			{
				
				/* Authorise Call */
				domesticStandingOrderScaConsentFoundationServiceAdapter.authoriseforDSO(stageIdentifiers, updateData,params);
			}
		}
	}

	/* Authorising Party Call */
	@Override
	public CustomConsentAppViewData retrieveConsentAppDebtorDetails(CustomPaymentStageIdentifiers stageIdentifiers,
			CustomPaymentStageUpdateData updateData, Map<String, String> paramsMap) {
		if (paramsMap == null || (paramsMap.get(PSD2Constants.TENANT_ID)==null)) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.BAD_REQUEST);
		}
		CustomConsentAppViewData customConsentAppViewData = new CustomConsentAppViewData();
		if (stageIdentifiers.getPaymentTypeEnum().equals(PaymentTypeEnum.DOMESTIC_ST_ORD)) {
			customConsentAppViewData = domesticStandingOrderScaConsentFoundationServiceAdapter
					.retrieveConsentAppDebtorDetails(stageIdentifiers, updateData, paramsMap);
		}
		return customConsentAppViewData;
	}

}
