package com.capgemini.psd2.funds.confirmation.test.controller;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.capgemini.psd2.cisp.domain.OBFundsConfirmation1;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmation1Data;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmation1DataInstructedAmount;
import com.capgemini.psd2.funds.confirmation.controller.FundsConfirmationController;
import com.capgemini.psd2.funds.confirmation.service.FundsConfirmationService;

public class FundsConfirmationControllerTest {

	@Mock
	private FundsConfirmationService fundsConfirmationService;

	@InjectMocks
	private FundsConfirmationController controller;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testCreateFundsConfirmationPositiveCase() {
		OBFundsConfirmation1 fundsConfirmationPOSTRequest = new OBFundsConfirmation1();
		OBFundsConfirmation1Data data = new OBFundsConfirmation1Data();
		OBFundsConfirmation1DataInstructedAmount instructedAmount = new OBFundsConfirmation1DataInstructedAmount();
		instructedAmount.setCurrency("EUR");
		data.setConsentId("12345");
		data.setInstructedAmount(instructedAmount);
		fundsConfirmationPOSTRequest.setData(data);
		controller.createFundsConfirmation(fundsConfirmationPOSTRequest);
	}

}
