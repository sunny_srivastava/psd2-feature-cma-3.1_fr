package com.capgemini.psd2.funds.confirmation.service.test.impl;

import static org.mockito.Matchers.anyMap;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.capgemini.psd2.cisp.adapter.CispConsentAdapter;
import com.capgemini.psd2.cisp.adapter.FundsConfirmationAdapter;
import com.capgemini.psd2.cisp.adapter.FundsConfirmationConsentAdapter;
import com.capgemini.psd2.cisp.domain.Links;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmation1;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmation1Data;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmation1DataInstructedAmount;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmationResponse1;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmationResponse1Data;
import com.capgemini.psd2.cisp.mock.domain.MockFundData;
import com.capgemini.psd2.cisp.utilities.CommonCardValidations;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.CispConsent;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.funds.confirmation.service.impl.FundsConfirmationServiceImpl;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.token.ConsentTokenData;
import com.capgemini.psd2.token.Token;

public class FundsConfirmationServiceImplTest {

	@Mock
	private FundsConfirmationConsentAdapter fundsConfirmationConsentAdapter;

	@Mock
	private RequestHeaderAttributes reqHeaderAtrributes;

	@Mock
	private CispConsentAdapter cispConsentAdapter;

	@Mock
	private FundsConfirmationAdapter fundsConfirmationAdapter;

	@Mock
	private CommonCardValidations commonCardValidation;

	@InjectMocks
	private FundsConfirmationServiceImpl service;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	
	@SuppressWarnings("unchecked")
	@Test
	public void testcreateFundsConfirmationConsent() {
		Token token = new Token();
		token.setRequestId("12345");
		CispConsent cispConsent = new CispConsent();
		cispConsent.setConsentId("12345");
		ConsentTokenData consentTokenData = new ConsentTokenData();
		consentTokenData.setConsentId("12345");
		token.setConsentTokenData(consentTokenData);
		Map<String, String> params = new HashMap<>();
		params.put("tenantId", "idone");

		token.setSeviceParams(params);
		reqHeaderAtrributes.setToken(token);
		when(reqHeaderAtrributes.getToken()).thenReturn(token);
		when(reqHeaderAtrributes.getIntentId()).thenReturn("12345");
		when(reqHeaderAtrributes.getSelfUrl()).thenReturn("localhost");
		OBFundsConfirmation1 fundsConfirmationPOSTRequest = new OBFundsConfirmation1();
		OBFundsConfirmation1Data data2 = new OBFundsConfirmation1Data();
		data2.setConsentId("12345");
		OBFundsConfirmation1DataInstructedAmount instructedAmount = new OBFundsConfirmation1DataInstructedAmount();
		instructedAmount.setCurrency("EUR");
		instructedAmount.setAmount("20");

		data2.setInstructedAmount(instructedAmount);
		data2.setReference("reference");

		fundsConfirmationPOSTRequest.setData(data2);

		AccountDetails acctDetails = new AccountDetails();
		acctDetails.setAccountNumber("12345678");
		acctDetails.setAccountNSC("123456");
		cispConsent.setAccountDetails(acctDetails);

		MockFundData mockFundData = new MockFundData();
		mockFundData.setAccountNumber("12345678");
		mockFundData.setAccountNsc("123456");
		mockFundData.setInstructedAmount(instructedAmount);

		when(cispConsentAdapter.retrieveConsentByFundsIntentId(anyString())).thenReturn(cispConsent);
		OBFundsConfirmationResponse1 fundsConfirmationPOSTResponse = new OBFundsConfirmationResponse1();
		fundsConfirmationPOSTResponse.setData(new OBFundsConfirmationResponse1Data());
		fundsConfirmationPOSTResponse.getData().setFundsConfirmationId("786");
		Links links = new Links();
		fundsConfirmationPOSTResponse.setLinks(links);
		links.setSelf("URL for Links");
		fundsConfirmationPOSTResponse.getLinks().self(PSD2Constants.SLASH);
		fundsConfirmationPOSTResponse.getLinks().self(fundsConfirmationPOSTResponse.getData().getFundsConfirmationId());
		when(fundsConfirmationAdapter.getFundsData(anyObject(), eq(fundsConfirmationPOSTRequest), anyMap()))
				.thenReturn(fundsConfirmationPOSTResponse);

		commonCardValidation.validateSubmissionResponseParams(fundsConfirmationPOSTResponse);
		service.createFundsConfirmation(fundsConfirmationPOSTRequest);
	}
	
	@SuppressWarnings("unchecked")
	@Test(expected = PSD2Exception.class)
	public void testcreateFundsConfirmationConsentWithNoLinksandMeta() {
		Token token = new Token();
		token.setRequestId("12345");
		ConsentTokenData consentTokenData = new ConsentTokenData();
		consentTokenData.setConsentId("12345");
		token.setConsentTokenData(consentTokenData);
		Map<String, String> params = new HashMap<>();
		params.put("tenantId", "idone");

		token.setSeviceParams(params);
		reqHeaderAtrributes.setToken(token);

		when(reqHeaderAtrributes.getToken()).thenReturn(token);
		when(reqHeaderAtrributes.getIntentId()).thenReturn("12345");
		when(reqHeaderAtrributes.getSelfUrl()).thenReturn("localhost");
		OBFundsConfirmation1 fundsConfirmationPOSTRequest = new OBFundsConfirmation1();
		OBFundsConfirmation1Data data2 = new OBFundsConfirmation1Data();
		data2.setConsentId("12345");
		OBFundsConfirmation1DataInstructedAmount instructedAmount = new OBFundsConfirmation1DataInstructedAmount();
		instructedAmount.setCurrency("EUR");
		instructedAmount.setAmount("20");

		data2.setInstructedAmount(instructedAmount);
		data2.setReference("reference");

		fundsConfirmationPOSTRequest.setData(data2);
		CispConsent cispConsent = new CispConsent();
		cispConsent.setConsentId("12345");
		AccountDetails acctDetails = new AccountDetails();
		acctDetails.setAccountNumber("12345678");
		acctDetails.setAccountNSC("123456");
		cispConsent.setAccountDetails(acctDetails);

		MockFundData mockFundData = new MockFundData();
		mockFundData.setAccountNumber("12345678");
		mockFundData.setAccountNsc("123456");
		mockFundData.setInstructedAmount(instructedAmount);
		OBFundsConfirmationResponse1 obj = new OBFundsConfirmationResponse1();
		OBFundsConfirmationResponse1Data data = new OBFundsConfirmationResponse1Data();
		data.setFundsConfirmationId("12345");
		obj.setData(data);
		obj.setLinks(null);
		obj.setMeta(null);
		when(cispConsentAdapter.retrieveConsentByFundsIntentId(anyString())).thenReturn(cispConsent);
		OBFundsConfirmationResponse1 fundsConfirmationPOSTResponse = null;
		when(fundsConfirmationAdapter.getFundsData(anyObject(), eq(fundsConfirmationPOSTRequest), anyMap()))
				.thenReturn(fundsConfirmationPOSTResponse);
		commonCardValidation.validateSubmissionResponseParams(obj);
		service.createFundsConfirmation(fundsConfirmationPOSTRequest);
	}

	@SuppressWarnings("unchecked")
	@Test(expected = PSD2Exception.class)
	public void testcreateFundsConfirmationConsent1() {
		Token token = new Token();
		token.setRequestId("12345");
		ConsentTokenData consentTokenData = new ConsentTokenData();
		consentTokenData.setConsentId("12345");
		token.setConsentTokenData(consentTokenData);
		Map<String, String> params = new HashMap<>();
		params.put("tenantId", "idone");

		token.setSeviceParams(params);
		reqHeaderAtrributes.setToken(token);
		when(reqHeaderAtrributes.getToken()).thenReturn(token);
		when(reqHeaderAtrributes.getIntentId()).thenReturn("12345");
		when(reqHeaderAtrributes.getSelfUrl()).thenReturn("localhost");
		OBFundsConfirmation1 fundsConfirmationPOSTRequest = new OBFundsConfirmation1();
		OBFundsConfirmation1Data data2 = new OBFundsConfirmation1Data();
		data2.setConsentId("12345");
		OBFundsConfirmation1DataInstructedAmount instructedAmount = new OBFundsConfirmation1DataInstructedAmount();
		instructedAmount.setCurrency("EUR");
		instructedAmount.setAmount("20");

		data2.setInstructedAmount(instructedAmount);
		data2.setReference("reference");

		fundsConfirmationPOSTRequest.setData(data2);
		CispConsent cispConsent = new CispConsent();
		cispConsent.setConsentId("12345");
		AccountDetails acctDetails = new AccountDetails();
		acctDetails.setAccountNumber("12345678");
		acctDetails.setAccountNSC("123456");
		cispConsent.setAccountDetails(acctDetails);

		MockFundData mockFundData = new MockFundData();
		mockFundData.setAccountNumber("12345678");
		mockFundData.setAccountNsc("123456");
		mockFundData.setInstructedAmount(instructedAmount);
		when(cispConsentAdapter.retrieveConsentByFundsIntentId(anyString())).thenReturn(null);
		OBFundsConfirmationResponse1 fundsConfirmationPOSTResponse = null;
		when(fundsConfirmationAdapter.getFundsData(anyObject(), eq(fundsConfirmationPOSTRequest), anyMap()))
				.thenReturn(fundsConfirmationPOSTResponse);

		commonCardValidation.validateSubmissionResponseParams(fundsConfirmationPOSTResponse);
		service.createFundsConfirmation(fundsConfirmationPOSTRequest);
	}

	@SuppressWarnings("unchecked")
	@Test(expected = PSD2Exception.class)
	public void testcreateFundsConfirmationConsent2() {
		Token token = new Token();
		token.setRequestId("12345");
		ConsentTokenData consentTokenData = new ConsentTokenData();
		consentTokenData.setConsentId("12345");
		token.setConsentTokenData(consentTokenData);
		Map<String, String> params = new HashMap<>();
		params.put("tenantId", "idone");

		token.setSeviceParams(params);
		reqHeaderAtrributes.setToken(token);
		when(reqHeaderAtrributes.getToken()).thenReturn(token);
		when(reqHeaderAtrributes.getIntentId()).thenReturn("12345");
		when(reqHeaderAtrributes.getSelfUrl()).thenReturn("localhost");
		OBFundsConfirmation1 fundsConfirmationPOSTRequest = new OBFundsConfirmation1();
		OBFundsConfirmation1Data data2 = new OBFundsConfirmation1Data();
		data2.setConsentId("12345");
		OBFundsConfirmation1DataInstructedAmount instructedAmount = new OBFundsConfirmation1DataInstructedAmount();
		instructedAmount.setCurrency("EUR");
		instructedAmount.setAmount("20");

		data2.setInstructedAmount(instructedAmount);
		data2.setReference("reference");

		fundsConfirmationPOSTRequest.setData(data2);
		CispConsent cispConsent = new CispConsent();
		cispConsent.setConsentId("1245");
		AccountDetails acctDetails = new AccountDetails();
		acctDetails.setAccountNumber("12345678");
		acctDetails.setAccountNSC("123456");
		cispConsent.setAccountDetails(acctDetails);

		MockFundData mockFundData = new MockFundData();
		mockFundData.setAccountNumber("12345678");
		mockFundData.setAccountNsc("123456");
		mockFundData.setInstructedAmount(instructedAmount);

		when(cispConsentAdapter.retrieveConsentByFundsIntentId(anyString())).thenReturn(cispConsent);
		OBFundsConfirmationResponse1 fundsConfirmationPOSTResponse = new OBFundsConfirmationResponse1();
		fundsConfirmationPOSTResponse.setData(new OBFundsConfirmationResponse1Data());
		fundsConfirmationPOSTResponse.getData().setFundsConfirmationId("786");
		Links links = new Links();
		fundsConfirmationPOSTResponse.setLinks(links);
		links.setSelf("URL for Links");
		fundsConfirmationPOSTResponse.getLinks().self(PSD2Constants.SLASH);
		fundsConfirmationPOSTResponse.getLinks().self(fundsConfirmationPOSTResponse.getData().getFundsConfirmationId());
		when(fundsConfirmationAdapter.getFundsData(anyObject(), eq(fundsConfirmationPOSTRequest), anyMap()))
				.thenReturn(fundsConfirmationPOSTResponse);

		commonCardValidation.validateSubmissionResponseParams(fundsConfirmationPOSTResponse);
		service.createFundsConfirmation(fundsConfirmationPOSTRequest);
	}

	@SuppressWarnings("unchecked")
	@Test(expected = PSD2Exception.class)
	public void testcreateFundsConfirmationConsent3() {
		Token token = new Token();
		token.setRequestId("12345");
		ConsentTokenData consentTokenData = new ConsentTokenData();
		consentTokenData.setConsentId("12345");
		token.setConsentTokenData(consentTokenData);
		Map<String, String> params = new HashMap<>();
		params.put("tenantId", "idone");

		token.setSeviceParams(params);
		reqHeaderAtrributes.setToken(token);
		when(reqHeaderAtrributes.getToken()).thenReturn(token);
		when(reqHeaderAtrributes.getIntentId()).thenReturn("12345");
		when(reqHeaderAtrributes.getSelfUrl()).thenReturn("localhost");
		OBFundsConfirmation1 fundsConfirmationPOSTRequest = new OBFundsConfirmation1();
		OBFundsConfirmation1Data data2 = new OBFundsConfirmation1Data();
		data2.setConsentId("12345");
		OBFundsConfirmation1DataInstructedAmount instructedAmount = new OBFundsConfirmation1DataInstructedAmount();
		instructedAmount.setCurrency("EUR");
		instructedAmount.setAmount("2a");

		data2.setInstructedAmount(instructedAmount);
		data2.setReference("reference");

		fundsConfirmationPOSTRequest.setData(data2);
		CispConsent cispConsent = new CispConsent();
		cispConsent.setConsentId("12345");
		AccountDetails acctDetails = new AccountDetails();
		acctDetails.setAccountNumber("12345678");
		acctDetails.setAccountNSC("123456");
		cispConsent.setAccountDetails(acctDetails);

		MockFundData mockFundData = new MockFundData();
		mockFundData.setAccountNumber("12345678");
		mockFundData.setAccountNsc("123456");
		mockFundData.setInstructedAmount(instructedAmount);
		when(cispConsentAdapter.retrieveConsentByFundsIntentId(anyString())).thenReturn(cispConsent);
		OBFundsConfirmationResponse1 fundsConfirmationPOSTResponse = null;
		when(fundsConfirmationAdapter.getFundsData(anyObject(), eq(fundsConfirmationPOSTRequest), anyMap()))
				.thenReturn(fundsConfirmationPOSTResponse);
		commonCardValidation.validateSubmissionResponseParams(fundsConfirmationPOSTResponse);
		service.createFundsConfirmation(fundsConfirmationPOSTRequest);
	}

	@SuppressWarnings("unchecked")
	@Test(expected = PSD2Exception.class)
	public void testcreateFundsConfirmationConsent4() {
		Token token = new Token();
		token.setRequestId("1245");
		ConsentTokenData consentTokenData = new ConsentTokenData();
		consentTokenData.setConsentId("12345");
		token.setConsentTokenData(consentTokenData);
		Map<String, String> params = new HashMap<>();
		params.put("tenantId", "idone");

		token.setSeviceParams(params);
		reqHeaderAtrributes.setToken(token);
		when(reqHeaderAtrributes.getToken()).thenReturn(token);
		when(reqHeaderAtrributes.getIntentId()).thenReturn("1245");
		when(reqHeaderAtrributes.getSelfUrl()).thenReturn("localhost");
		OBFundsConfirmation1 fundsConfirmationPOSTRequest = new OBFundsConfirmation1();
		OBFundsConfirmation1Data data2 = new OBFundsConfirmation1Data();
		data2.setConsentId("12345");
		OBFundsConfirmation1DataInstructedAmount instructedAmount = new OBFundsConfirmation1DataInstructedAmount();
		instructedAmount.setCurrency("EUR");
		instructedAmount.setAmount("2a");

		data2.setInstructedAmount(instructedAmount);
		data2.setReference("reference");

		fundsConfirmationPOSTRequest.setData(data2);
		CispConsent cispConsent = new CispConsent();
		cispConsent.setConsentId("12345");
		AccountDetails acctDetails = new AccountDetails();
		acctDetails.setAccountNumber("12345678");
		acctDetails.setAccountNSC("123456");
		cispConsent.setAccountDetails(acctDetails);

		MockFundData mockFundData = new MockFundData();
		mockFundData.setAccountNumber("12345678");
		mockFundData.setAccountNsc("123456");
		mockFundData.setInstructedAmount(instructedAmount);
		when(cispConsentAdapter.retrieveConsentByFundsIntentId(anyString())).thenReturn(cispConsent);
		OBFundsConfirmationResponse1 fundsConfirmationPOSTResponse = null;
		when(fundsConfirmationAdapter.getFundsData(anyObject(), eq(fundsConfirmationPOSTRequest), anyMap()))
				.thenReturn(fundsConfirmationPOSTResponse);
		commonCardValidation.validateSubmissionResponseParams(fundsConfirmationPOSTResponse);
		commonCardValidation.validateSubmissionRequestParams(fundsConfirmationPOSTRequest);
		service.createFundsConfirmation(fundsConfirmationPOSTRequest);
	}
}
