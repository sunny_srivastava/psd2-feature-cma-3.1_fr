package com.capgemini.psd2.funds.confirmation.mongo.db.adapter.test.impl;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.util.HashMap;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.capgemini.psd2.cisp.domain.OBFundsConfirmation1;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmation1Data;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmation1DataInstructedAmount;
import com.capgemini.psd2.cisp.mock.domain.MockFundData;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.funds.confirmation.mongo.db.adapter.impl.FundsConfirmationMongoDbAdapterImpl;
import com.capgemini.psd2.funds.confirmation.mongo.db.adapter.repository.FundsConfirmationRepository;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.utilities.SandboxValidationUtility;

public class FundsConfirmationMongoDbAdapterImplTest {

	@Mock
	private FundsConfirmationRepository fundsConfirmationRepository;

	@Mock
	private SandboxValidationUtility utility;

	@InjectMocks
	private FundsConfirmationMongoDbAdapterImpl impl;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testCreateFundsConfirmationPOSTResponsePositiveUtility() {
		AccountDetails accountDetails = new AccountDetails();

		MockFundData mockFundData = new MockFundData();
		OBFundsConfirmation1DataInstructedAmount instructedAmount = new OBFundsConfirmation1DataInstructedAmount();
		instructedAmount.setAmount("146.56");
		instructedAmount.setCurrency("EUR");
		mockFundData.setInstructedAmount(instructedAmount);
		when(fundsConfirmationRepository.findByAccountNumberAndAccountNsc(anyString(), anyString()))
				.thenReturn(mockFundData);
		OBFundsConfirmation1 fundsConfirmationPOSTRequest = new OBFundsConfirmation1();
		OBFundsConfirmation1Data data = new OBFundsConfirmation1Data();
		data.setConsentId("12345");
		data.setInstructedAmount(instructedAmount);
		fundsConfirmationPOSTRequest.setData(data);

		Mockito.when(utility.isValidCurrency(mockFundData.getInstructedAmount().getCurrency())).thenReturn(true);
		Mockito.when(
				utility.isValidAmount(mockFundData.getInstructedAmount().getAmount(), PSD2Constants.REJECTED_CONDITION))
				.thenReturn(true);

		impl.getFundsData(accountDetails, fundsConfirmationPOSTRequest, new HashMap<>());

		assertNotNull(fundsConfirmationPOSTRequest);
	}

	@Test
	public void testCreateFundsConfirmationPOSTResponseNegativeUtility() {
		AccountDetails accountDetails = new AccountDetails();

		MockFundData mockFundData = new MockFundData();
		OBFundsConfirmation1DataInstructedAmount instructedAmount = new OBFundsConfirmation1DataInstructedAmount();
		instructedAmount.setAmount("146.56");
		instructedAmount.setCurrency("EUR");
		mockFundData.setInstructedAmount(instructedAmount);
		when(fundsConfirmationRepository.findByAccountNumberAndAccountNsc(anyString(), anyString()))
				.thenReturn(mockFundData);
		OBFundsConfirmation1 fundsConfirmationPOSTRequest = new OBFundsConfirmation1();
		OBFundsConfirmation1Data data = new OBFundsConfirmation1Data();
		data.setConsentId("12345");
		data.setInstructedAmount(instructedAmount);
		fundsConfirmationPOSTRequest.setData(data);

		Mockito.when(utility.isValidCurrency(mockFundData.getInstructedAmount().getCurrency())).thenReturn(false);
		Mockito.when(
				utility.isValidAmount(mockFundData.getInstructedAmount().getAmount(), PSD2Constants.REJECTED_CONDITION))
				.thenReturn(false);

		impl.getFundsData(accountDetails, fundsConfirmationPOSTRequest, new HashMap<>());

		assertNotNull(fundsConfirmationPOSTRequest);
	}
  
	}
