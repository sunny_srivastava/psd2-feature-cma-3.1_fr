package com.capgemini.psd2.foundationservice.international.payment.consent.boi.adapter.transformer;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.capgemini.psd2.foundationservice.international.payment.consent.boi.adapter.domain.AccountInformation;
import com.capgemini.psd2.foundationservice.international.payment.consent.boi.adapter.domain.Address;
import com.capgemini.psd2.foundationservice.international.payment.consent.boi.adapter.domain.Agent;
import com.capgemini.psd2.foundationservice.international.payment.consent.boi.adapter.domain.AuthorisationType;
import com.capgemini.psd2.foundationservice.international.payment.consent.boi.adapter.domain.BrandCode3;
import com.capgemini.psd2.foundationservice.international.payment.consent.boi.adapter.domain.Channel;
import com.capgemini.psd2.foundationservice.international.payment.consent.boi.adapter.domain.ChannelCode1;
import com.capgemini.psd2.foundationservice.international.payment.consent.boi.adapter.domain.ChargeBearer;
import com.capgemini.psd2.foundationservice.international.payment.consent.boi.adapter.domain.Country;
import com.capgemini.psd2.foundationservice.international.payment.consent.boi.adapter.domain.Currency;
import com.capgemini.psd2.foundationservice.international.payment.consent.boi.adapter.domain.ExchangeRateQuote;
import com.capgemini.psd2.foundationservice.international.payment.consent.boi.adapter.domain.FinancialEventAmount;
import com.capgemini.psd2.foundationservice.international.payment.consent.boi.adapter.domain.PartyBasicInformation;
import com.capgemini.psd2.foundationservice.international.payment.consent.boi.adapter.domain.PaymentInstructionCharge;
import com.capgemini.psd2.foundationservice.international.payment.consent.boi.adapter.domain.PaymentInstructionPostalAddress;
import com.capgemini.psd2.foundationservice.international.payment.consent.boi.adapter.domain.PaymentInstructionProposalInternational;
import com.capgemini.psd2.foundationservice.international.payment.consent.boi.adapter.domain.PaymentInstrumentRiskFactor;
import com.capgemini.psd2.foundationservice.international.payment.consent.boi.adapter.domain.PriorityCode;
import com.capgemini.psd2.foundationservice.international.payment.consent.boi.adapter.domain.ProposalStatus;
import com.capgemini.psd2.foundationservice.international.payment.consent.boi.adapter.domain.ProposingPartyAccount;
import com.capgemini.psd2.foundationservice.international.payment.consent.boi.adapter.domain.Purpose;
import com.capgemini.psd2.foundationservice.international.payment.consent.boi.adapter.domain.RateQuoteType;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.pisp.domain.CustomIPaymentConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomIPaymentConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.OBAddressTypeCode;
import com.capgemini.psd2.pisp.domain.OBAuthorisation1;
import com.capgemini.psd2.pisp.domain.OBBranchAndFinancialInstitutionIdentification3;
import com.capgemini.psd2.pisp.domain.OBCashAccountCreditor2;
import com.capgemini.psd2.pisp.domain.OBCashAccountDebtor3;
import com.capgemini.psd2.pisp.domain.OBCharge1;
import com.capgemini.psd2.pisp.domain.OBCharge1Amount;
import com.capgemini.psd2.pisp.domain.OBChargeBearerType1Code;
import com.capgemini.psd2.pisp.domain.OBDomestic1InstructedAmount;
import com.capgemini.psd2.pisp.domain.OBExchangeRate1;
import com.capgemini.psd2.pisp.domain.OBExchangeRateType2Code;
import com.capgemini.psd2.pisp.domain.OBExternalAuthorisation1Code;
import com.capgemini.psd2.pisp.domain.OBExternalConsentStatus1Code;
import com.capgemini.psd2.pisp.domain.OBExternalPaymentContext1Code;
import com.capgemini.psd2.pisp.domain.OBInternational1;
import com.capgemini.psd2.pisp.domain.OBPartyIdentification43;
import com.capgemini.psd2.pisp.domain.OBPostalAddress6;
import com.capgemini.psd2.pisp.domain.OBPriority2Code;
import com.capgemini.psd2.pisp.domain.OBRemittanceInformation1;
import com.capgemini.psd2.pisp.domain.OBRisk1;
import com.capgemini.psd2.pisp.domain.OBRisk1DeliveryAddress;
import com.capgemini.psd2.pisp.domain.OBWriteDataInternationalConsentResponse1;
import com.capgemini.psd2.pisp.enums.ProcessConsentStatusEnum;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Component
public class InternationalPaymentConsentsFoundationServiceTransformer {

	public <T> CustomIPaymentConsentsPOSTResponse transformInternationalPaymentResponse(T inputBalanceObj) {

		CustomIPaymentConsentsPOSTResponse customIPaymentConsentsPOSTResponse = new CustomIPaymentConsentsPOSTResponse();

		OBWriteDataInternationalConsentResponse1 oBWriteDataInternationalConsentResponse1 = new OBWriteDataInternationalConsentResponse1();

		PaymentInstructionProposalInternational paymentInstructionProposalInternational = (PaymentInstructionProposalInternational) inputBalanceObj;

		OBInternational1 oBInternational1 = new OBInternational1();

		OBRisk1 oBRisk1 = new OBRisk1();

		if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational.getPaymentInstructionProposalId())) {
			oBWriteDataInternationalConsentResponse1
					.setConsentId(paymentInstructionProposalInternational.getPaymentInstructionProposalId());
		}

		if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational.getProposalCreationDatetime())) {
			oBWriteDataInternationalConsentResponse1
					.setCreationDateTime(paymentInstructionProposalInternational.getProposalCreationDatetime());
		}

		if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational.getProposalStatus())) {
			OBExternalConsentStatus1Code status = OBExternalConsentStatus1Code
					.fromValue(paymentInstructionProposalInternational.getProposalStatus().toString());
			oBWriteDataInternationalConsentResponse1.setStatus(status);
		}

		if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational.getProposalStatusUpdateDatetime())) {
			oBWriteDataInternationalConsentResponse1
					.setStatusUpdateDateTime(paymentInstructionProposalInternational.getProposalStatusUpdateDatetime());
		}

		// CutOffDateTime,ExpectedExecutionDateTime,ExpectedSettlementDateTime were
		// absent--May be we need to set in mock

		List<OBCharge1> charges = new ArrayList<>();
		OBCharge1 oBCharge1 = new OBCharge1();
		if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational.getCharges())) {

			if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational.getCharges().get(0))) {
				if (!NullCheckUtils
						.isNullOrEmpty(paymentInstructionProposalInternational.getCharges().get(0).getChargeBearer())) {
					String chargeBearer = paymentInstructionProposalInternational.getCharges().get(0).getChargeBearer()
							.toString();
					OBChargeBearerType1Code oBChargeBearerType1Code = OBChargeBearerType1Code.fromValue(chargeBearer);
					oBCharge1.setChargeBearer(oBChargeBearerType1Code);
				}
			}

			if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational.getCharges().get(0).getType())) {
				oBCharge1.setType(paymentInstructionProposalInternational.getCharges().get(0).getType());
			}

			if ((!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational.getCharges()))) {

				if ((!NullCheckUtils
						.isNullOrEmpty(paymentInstructionProposalInternational.getCharges().get(0).getAmount()))
						&& (!NullCheckUtils.isNullOrEmpty(
								paymentInstructionProposalInternational.getCharges().get(0).getCurrency()))) {
					OBCharge1Amount chargeAmount = new OBCharge1Amount();
					Double chargeAmountMule = paymentInstructionProposalInternational.getCharges().get(0).getAmount()
							.getTransactionCurrency();
					chargeAmount.setAmount(String.valueOf(chargeAmountMule));
					chargeAmount.setCurrency(paymentInstructionProposalInternational.getCharges().get(0).getCurrency()
							.getIsoAlphaCode());
					oBCharge1.setAmount(chargeAmount);
				}

				charges.add(oBCharge1);
				// oBWriteDataInternationalConsentResponse1.setCharges(charges);
			}
		}

		if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational.getInstructionReference())) {
			oBInternational1
					.setInstructionIdentification(paymentInstructionProposalInternational.getInstructionReference());
		}

		if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational.getInstructionEndToEndReference())) {
			oBInternational1.setEndToEndIdentification(
					paymentInstructionProposalInternational.getInstructionEndToEndReference());
		}

		if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational.getInstructionLocalInstrument())) {
			oBInternational1
					.setLocalInstrument(paymentInstructionProposalInternational.getInstructionLocalInstrument());
		}
		if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational.getPriorityCode())) {
			OBPriority2Code oBPriority2Code = OBPriority2Code
					.fromValue(paymentInstructionProposalInternational.getPriorityCode().toString());
			oBInternational1.setInstructionPriority(oBPriority2Code);
		}
		if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational.getPurpose())) {
			//Updated Mapping based on RAML Upgrade v1.0.64
		if (!NullCheckUtils
				.isNullOrEmpty(paymentInstructionProposalInternational.getPurpose().getPurposeCode())) {
			oBInternational1.setPurpose(paymentInstructionProposalInternational.getPurpose().getPurposeCode());
			}
		}
		if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational.getCharges())) {
			if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational.getCharges().get(0))) {
				if (!NullCheckUtils
						.isNullOrEmpty(paymentInstructionProposalInternational.getCharges().get(0).getChargeBearer())) {
					OBChargeBearerType1Code oBChargeBearerType1Code = OBChargeBearerType1Code.fromValue(
							paymentInstructionProposalInternational.getCharges().get(0).getChargeBearer().toString());
					oBInternational1.setChargeBearer(oBChargeBearerType1Code);
				}
			}
		}
		if (!NullCheckUtils
				.isNullOrEmpty(paymentInstructionProposalInternational.getCurrencyOfTransfer().getIsoAlphaCode())) {

			oBInternational1.setCurrencyOfTransfer(
					paymentInstructionProposalInternational.getCurrencyOfTransfer().getIsoAlphaCode());
		}

		if (!NullCheckUtils.isNullOrEmpty(
				paymentInstructionProposalInternational.getFinancialEventAmount().getTransactionCurrency())
				&& !NullCheckUtils.isNullOrEmpty(
						paymentInstructionProposalInternational.getTransactionCurrency().getIsoAlphaCode())) {
			OBDomestic1InstructedAmount amountInstructed = new OBDomestic1InstructedAmount();

			Double amountMule = paymentInstructionProposalInternational.getFinancialEventAmount()
					.getTransactionCurrency();
		      amountInstructed.setAmount(BigDecimal.valueOf(amountMule).toPlainString());
			//amountInstructed.setAmount(doubleToString(amountMule));
			amountInstructed
					.setCurrency(paymentInstructionProposalInternational.getTransactionCurrency().getIsoAlphaCode());
			oBInternational1.setInstructedAmount(amountInstructed);
		}

		// ExchangeRateInformation
		OBExchangeRate1 oBExchangeRate1 = new OBExchangeRate1();
		if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational.getExchangeRateQuote())) {

			if (!NullCheckUtils
					.isNullOrEmpty(paymentInstructionProposalInternational.getExchangeRateQuote().getUnitCurrency())) {
				oBExchangeRate1.setUnitCurrency(paymentInstructionProposalInternational.getExchangeRateQuote()
						.getUnitCurrency().getIsoAlphaCode().toString());
			}
			if (!NullCheckUtils
					.isNullOrEmpty(paymentInstructionProposalInternational.getExchangeRateQuote().getExchangeRate())) {
				oBExchangeRate1.setExchangeRate(BigDecimal
						.valueOf(paymentInstructionProposalInternational.getExchangeRateQuote().getExchangeRate()));
			}
			if (!NullCheckUtils
					.isNullOrEmpty(paymentInstructionProposalInternational.getExchangeRateQuote().getRateQuoteType())) {
				oBExchangeRate1.setRateType(OBExchangeRateType2Code.fromValue(
						paymentInstructionProposalInternational.getExchangeRateQuote().getRateQuoteType().toString()));
			}

			oBInternational1.setExchangeRateInformation(oBExchangeRate1);
		}

		// Debtor Account

		if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational.getAuthorisingPartyAccount())) {
			OBCashAccountDebtor3 oBCashAccountDebtor3 = new OBCashAccountDebtor3();
			if (!NullCheckUtils.isNullOrEmpty(
					paymentInstructionProposalInternational.getAuthorisingPartyAccount().getSchemeName())) {
				oBCashAccountDebtor3.setSchemeName(
						paymentInstructionProposalInternational.getAuthorisingPartyAccount().getSchemeName());
			}

			if (!NullCheckUtils.isNullOrEmpty(
					paymentInstructionProposalInternational.getAuthorisingPartyAccount().getAccountIdentification())) {
				oBCashAccountDebtor3.setIdentification(paymentInstructionProposalInternational
						.getAuthorisingPartyAccount().getAccountIdentification());
			}

			if (!NullCheckUtils.isNullOrEmpty(
					paymentInstructionProposalInternational.getAuthorisingPartyAccount().getAccountName())) {
				oBCashAccountDebtor3
						.setName(paymentInstructionProposalInternational.getAuthorisingPartyAccount().getAccountName());
			}

			if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational.getAuthorisingPartyAccount()
					.getSecondaryIdentification())) {
				oBCashAccountDebtor3.setSecondaryIdentification(paymentInstructionProposalInternational
						.getAuthorisingPartyAccount().getSecondaryIdentification());
			}

			oBInternational1.setDebtorAccount(oBCashAccountDebtor3);
		}

		// Creditor Account

		if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational.getProposingPartyAccount())) {
			OBCashAccountCreditor2 oBCashAccountCreditor2 = new OBCashAccountCreditor2();
			if (!NullCheckUtils.isNullOrEmpty(
					paymentInstructionProposalInternational.getProposingPartyAccount().getSchemeName())) {
				oBCashAccountCreditor2.setSchemeName(
						paymentInstructionProposalInternational.getProposingPartyAccount().getSchemeName());
			}

			if (!NullCheckUtils.isNullOrEmpty(
					paymentInstructionProposalInternational.getProposingPartyAccount().getAccountIdentification())) {
				oBCashAccountCreditor2.setIdentification(
						paymentInstructionProposalInternational.getProposingPartyAccount().getAccountIdentification());
			}

			if (!NullCheckUtils.isNullOrEmpty(
					paymentInstructionProposalInternational.getProposingPartyAccount().getAccountName())) {
				oBCashAccountCreditor2
						.setName(paymentInstructionProposalInternational.getProposingPartyAccount().getAccountName());
			}

			if (!NullCheckUtils.isNullOrEmpty(
					paymentInstructionProposalInternational.getProposingPartyAccount().getSecondaryIdentification())) {
				oBCashAccountCreditor2.setSecondaryIdentification(paymentInstructionProposalInternational
						.getProposingPartyAccount().getSecondaryIdentification());
			}

			oBInternational1.setCreditorAccount(oBCashAccountCreditor2);

		}

		// Creditor Logic need to add
		OBPartyIdentification43 oBPartyIdentification43 = new OBPartyIdentification43();
		if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational.getProposingParty())) {
		if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational.getProposingParty().getPartyName())) {
			oBPartyIdentification43.setName(paymentInstructionProposalInternational.getProposingParty().getPartyName());
			oBInternational1.setCreditor(oBPartyIdentification43);
			}
		}

		OBPostalAddress6 oBPostalAddress6 = new OBPostalAddress6();

		// Creditor Postal Address

		if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational.getProposingPartyPostalAddress())) {

			if (!NullCheckUtils.isNullOrEmpty(
					paymentInstructionProposalInternational.getProposingPartyPostalAddress().getAddressType())) {
				OBAddressTypeCode addressType = OBAddressTypeCode.fromValue(
						paymentInstructionProposalInternational.getProposingPartyPostalAddress().getAddressType());
				oBPostalAddress6.setAddressType(addressType);
			}

			if (!NullCheckUtils.isNullOrEmpty(
					paymentInstructionProposalInternational.getProposingPartyPostalAddress().getDepartment())) {
				oBPostalAddress6.setDepartment(
						paymentInstructionProposalInternational.getProposingPartyPostalAddress().getDepartment());
			}

			if (!NullCheckUtils.isNullOrEmpty(
					paymentInstructionProposalInternational.getProposingPartyPostalAddress().getSubDepartment())) {
				oBPostalAddress6.setSubDepartment(
						paymentInstructionProposalInternational.getProposingPartyPostalAddress().getSubDepartment());
			}

			if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational.getProposingPartyPostalAddress()
					.getGeoCodeBuildingName())) {
				oBPostalAddress6.setStreetName(paymentInstructionProposalInternational.getProposingPartyPostalAddress()
						.getGeoCodeBuildingName());
			}

			if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational.getProposingPartyPostalAddress()
					.getGeoCodeBuildingNumber())) {
				oBPostalAddress6.setBuildingNumber(paymentInstructionProposalInternational
						.getProposingPartyPostalAddress().getGeoCodeBuildingNumber());
			}

			if (!NullCheckUtils.isNullOrEmpty(
					paymentInstructionProposalInternational.getProposingPartyPostalAddress().getPostCodeNumber())) {
				oBPostalAddress6.setPostCode(
						paymentInstructionProposalInternational.getProposingPartyPostalAddress().getPostCodeNumber());
			}

			if (!NullCheckUtils.isNullOrEmpty(
					paymentInstructionProposalInternational.getProposingPartyPostalAddress().getTownName())) {
				oBPostalAddress6.setTownName(
						paymentInstructionProposalInternational.getProposingPartyPostalAddress().getTownName());
			}

			if (!NullCheckUtils.isNullOrEmpty(
					paymentInstructionProposalInternational.getProposingPartyPostalAddress().getCountrySubDivision())) {
				oBPostalAddress6.setCountrySubDivision(paymentInstructionProposalInternational
						.getProposingPartyPostalAddress().getCountrySubDivision());
			}

			if (!NullCheckUtils.isNullOrEmpty(
					paymentInstructionProposalInternational.getProposingPartyPostalAddress().getAddressCountry())
					&& !NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational
							.getProposingPartyPostalAddress().getAddressCountry().getIsoCountryAlphaTwoCode())) {

				oBPostalAddress6.setCountry(paymentInstructionProposalInternational.getProposingPartyPostalAddress()
						.getAddressCountry().getIsoCountryAlphaTwoCode());

			}

			List<String> addLine = paymentInstructionProposalInternational.getProposingPartyPostalAddress()
					.getAddressLine();
			if (!NullCheckUtils.isNullOrEmpty(addLine) && addLine.size() > 0) {
				oBPostalAddress6.setAddressLine(
						paymentInstructionProposalInternational.getProposingPartyPostalAddress().getAddressLine());
			}
			oBPartyIdentification43.setPostalAddress(oBPostalAddress6);
			oBInternational1.setCreditor(oBPartyIdentification43);

		}

		// CreditorAgent
		if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational.getProposingPartyAgent())) {
			OBBranchAndFinancialInstitutionIdentification3 oBBranchAndFinancialInstitutionIdentification3 = new OBBranchAndFinancialInstitutionIdentification3();
			// SchemeName to be decided
			if (!NullCheckUtils
					.isNullOrEmpty(paymentInstructionProposalInternational.getProposingPartyAgent().getSchemeName())) {
				oBBranchAndFinancialInstitutionIdentification3.setSchemeName(
						paymentInstructionProposalInternational.getProposingPartyAgent().getSchemeName());
			}
			if (!NullCheckUtils.isNullOrEmpty(
					paymentInstructionProposalInternational.getProposingPartyAgent().getAgentIdentification())) {
				oBBranchAndFinancialInstitutionIdentification3.setIdentification(
						paymentInstructionProposalInternational.getProposingPartyAgent().getAgentIdentification());
			}
			if (!NullCheckUtils
					.isNullOrEmpty(paymentInstructionProposalInternational.getProposingPartyAgent().getAgentName())) {
				oBBranchAndFinancialInstitutionIdentification3
						.setName(paymentInstructionProposalInternational.getProposingPartyAgent().getAgentName());
			}

			// PostalAddress of Creditor agent

			if (!NullCheckUtils.isNullOrEmpty(
					paymentInstructionProposalInternational.getProposingPartyAgent().getPartyAddress())) {

				if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational.getProposingPartyAgent()
						.getPartyAddress().getAddressType())) {
					OBAddressTypeCode oBAddressTypeCode = OBAddressTypeCode
							.fromValue(paymentInstructionProposalInternational.getProposingPartyAgent()
									.getPartyAddress().getAddressType().toString());
					oBPostalAddress6.setAddressType(oBAddressTypeCode);
				}

				if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational.getProposingPartyAgent()
						.getPartyAddress().getDepartment())) {
					oBPostalAddress6.setDepartment(paymentInstructionProposalInternational.getProposingPartyAgent()
							.getPartyAddress().getDepartment().toString());
				}

				if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational.getProposingPartyAgent()
						.getPartyAddress().getSubDepartment())) {
					oBPostalAddress6.setSubDepartment(paymentInstructionProposalInternational.getProposingPartyAgent()
							.getPartyAddress().getSubDepartment().toString());
				}

				if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational.getProposingPartyAgent()
						.getPartyAddress().getGeoCodeBuildingName())) {
					oBPostalAddress6.setStreetName(paymentInstructionProposalInternational.getProposingPartyAgent()
							.getPartyAddress().getGeoCodeBuildingName().toString());
				}

				if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational.getProposingPartyAgent()
						.getPartyAddress().getGeoCodeBuildingNumber())) {
					oBPostalAddress6.setBuildingNumber(paymentInstructionProposalInternational.getProposingPartyAgent()
							.getPartyAddress().getGeoCodeBuildingNumber().toString());
				}

				if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational.getProposingPartyAgent()
						.getPartyAddress().getTownName())) {
					oBPostalAddress6.setTownName(paymentInstructionProposalInternational.getProposingPartyAgent()
							.getPartyAddress().getTownName().toString());
				}

				if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational.getProposingPartyAgent()
						.getPartyAddress().getCountrySubDivision())) {
					oBPostalAddress6.setCountrySubDivision(paymentInstructionProposalInternational
							.getProposingPartyAgent().getPartyAddress().getCountrySubDivision().toString());
				}

				if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational.getProposingPartyAgent()
						.getPartyAddress().getAddressCountry())
						&& !NullCheckUtils
								.isNullOrEmpty(paymentInstructionProposalInternational.getProposingPartyAgent()
										.getPartyAddress().getAddressCountry().getIsoCountryAlphaTwoCode())) {
					oBPostalAddress6.setCountry(paymentInstructionProposalInternational.getProposingPartyAgent()
							.getPartyAddress().getAddressCountry().getIsoCountryAlphaTwoCode().toString());
				}

				if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational.getProposingPartyAgent()
						.getPartyAddress().getAddressLine())) {
					List<String> addLine = paymentInstructionProposalInternational.getProposingPartyPostalAddress()
							.getAddressLine();
					if (!NullCheckUtils.isNullOrEmpty(addLine) && addLine.size() > 0) {
						oBPostalAddress6.setAddressLine(addLine);
					}

				}
			}

			oBBranchAndFinancialInstitutionIdentification3.setPostalAddress(oBPostalAddress6);
			oBInternational1.setCreditorAgent(oBBranchAndFinancialInstitutionIdentification3);
		}

		// RemittanceInformation

		if ((!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational.getAuthorisingPartyReference()))
				|| (!NullCheckUtils.isNullOrEmpty(
						paymentInstructionProposalInternational.getAuthorisingPartyUnstructuredReference()))) {
			OBRemittanceInformation1 oBRemittanceInformation1 = new OBRemittanceInformation1();
			boolean isInit = false;
			if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational.getAuthorisingPartyReference())) {
				oBRemittanceInformation1
						.setReference(paymentInstructionProposalInternational.getAuthorisingPartyReference());
				isInit = true;
			}

			if (!NullCheckUtils.isNullOrEmpty(
					paymentInstructionProposalInternational.getAuthorisingPartyUnstructuredReference())) {
				oBRemittanceInformation1.setUnstructured(
						paymentInstructionProposalInternational.getAuthorisingPartyUnstructuredReference());
				isInit = true;
			}

			if (isInit)
				oBInternational1.setRemittanceInformation(oBRemittanceInformation1);
		}

		// Authorisation

		if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational.getAuthorisationType())) {
			OBAuthorisation1 oBAuthorisation1 = new OBAuthorisation1();
			String authType = paymentInstructionProposalInternational.getAuthorisationType().toString();
			OBExternalAuthorisation1Code authCode = OBExternalAuthorisation1Code.fromValue(authType);
			oBAuthorisation1.setAuthorisationType(authCode);

			if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational.getAuthorisationDatetime())) {
				oBAuthorisation1
						.setCompletionDateTime(paymentInstructionProposalInternational.getAuthorisationDatetime());
			}

			oBWriteDataInternationalConsentResponse1.setAuthorisation(oBAuthorisation1);
		}

		// Risk

		if (!NullCheckUtils
				.isNullOrEmpty(paymentInstructionProposalInternational.getPaymentInstructionRiskFactorReference())) {
			OBRisk1DeliveryAddress oBRisk1DeliveryAddress = null;

			if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational
					.getPaymentInstructionRiskFactorReference().getPaymentContextCode())) {
				OBExternalPaymentContext1Code contextCode = OBExternalPaymentContext1Code
						.fromValue(paymentInstructionProposalInternational.getPaymentInstructionRiskFactorReference()
								.getPaymentContextCode());
				oBRisk1.setPaymentContextCode(contextCode);
			}

			if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational
					.getPaymentInstructionRiskFactorReference().getMerchantCategoryCode())) {
				oBRisk1.setMerchantCategoryCode(paymentInstructionProposalInternational
						.getPaymentInstructionRiskFactorReference().getMerchantCategoryCode());
			}

			if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational
					.getPaymentInstructionRiskFactorReference().getMerchantCustomerIdentification())) {
				oBRisk1.setMerchantCustomerIdentification(paymentInstructionProposalInternational
						.getPaymentInstructionRiskFactorReference().getMerchantCustomerIdentification());
			}

			if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational
					.getPaymentInstructionRiskFactorReference().getCounterPartyAddress())) {
				oBRisk1DeliveryAddress = new OBRisk1DeliveryAddress();
				if (!NullCheckUtils.isNullOrEmpty((paymentInstructionProposalInternational
						.getPaymentInstructionRiskFactorReference().getCounterPartyAddress().getFirstAddressLine()))) {
					List<String> addressLine = new ArrayList<>();
					if (!NullCheckUtils.isNullOrEmpty(
							(paymentInstructionProposalInternational.getPaymentInstructionRiskFactorReference()
									.getCounterPartyAddress().getSecondAddressLine()))) {
						addressLine
								.add(paymentInstructionProposalInternational.getPaymentInstructionRiskFactorReference()
										.getCounterPartyAddress().getFirstAddressLine());
						addressLine
								.add(paymentInstructionProposalInternational.getPaymentInstructionRiskFactorReference()
										.getCounterPartyAddress().getSecondAddressLine());
					} else {
						addressLine
								.add(paymentInstructionProposalInternational.getPaymentInstructionRiskFactorReference()
										.getCounterPartyAddress().getFirstAddressLine());
					}

					oBRisk1DeliveryAddress.setAddressLine(addressLine);
				}

				if (!NullCheckUtils.isNullOrEmpty(
						paymentInstructionProposalInternational.getPaymentInstructionRiskFactorReference()
								.getCounterPartyAddress().getGeoCodeBuildingName())) {
					oBRisk1DeliveryAddress.setStreetName(
							paymentInstructionProposalInternational.getPaymentInstructionRiskFactorReference()
									.getCounterPartyAddress().getGeoCodeBuildingName());
				}

				if (!NullCheckUtils.isNullOrEmpty(
						paymentInstructionProposalInternational.getPaymentInstructionRiskFactorReference()
								.getCounterPartyAddress().getGeoCodeBuildingNumber())) {
					oBRisk1DeliveryAddress.setBuildingNumber(
							paymentInstructionProposalInternational.getPaymentInstructionRiskFactorReference()
									.getCounterPartyAddress().getGeoCodeBuildingNumber());
				}

				if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational
						.getPaymentInstructionRiskFactorReference().getCounterPartyAddress().getPostCodeNumber())) {
					oBRisk1DeliveryAddress.setPostCode(paymentInstructionProposalInternational
							.getPaymentInstructionRiskFactorReference().getCounterPartyAddress().getPostCodeNumber());
				}

				if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational
						.getPaymentInstructionRiskFactorReference().getCounterPartyAddress().getThirdAddressLine())) {
					oBRisk1DeliveryAddress.setTownName(paymentInstructionProposalInternational
							.getPaymentInstructionRiskFactorReference().getCounterPartyAddress().getThirdAddressLine());
				}

				if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational
						.getPaymentInstructionRiskFactorReference().getCounterPartyAddress().getFourthAddressLine())) {
					String addressLine = null;
					if (!NullCheckUtils.isNullOrEmpty(
							paymentInstructionProposalInternational.getPaymentInstructionRiskFactorReference()
									.getCounterPartyAddress().getFifthAddressLine())) {
						addressLine = paymentInstructionProposalInternational.getPaymentInstructionRiskFactorReference()
								.getCounterPartyAddress().getFourthAddressLine()
								+ paymentInstructionProposalInternational.getPaymentInstructionRiskFactorReference()
										.getCounterPartyAddress().getFifthAddressLine();
					} else {
						addressLine = paymentInstructionProposalInternational.getPaymentInstructionRiskFactorReference()
								.getCounterPartyAddress().getFourthAddressLine();
					}

					oBRisk1DeliveryAddress.setCountrySubDivision(addressLine);
				}

				if (!NullCheckUtils.isNullOrEmpty(
						paymentInstructionProposalInternational.getPaymentInstructionRiskFactorReference()
								.getCounterPartyAddress().getAddressCountry().getIsoCountryAlphaTwoCode())) {
					oBRisk1DeliveryAddress.setCountry(
							paymentInstructionProposalInternational.getPaymentInstructionRiskFactorReference()
									.getCounterPartyAddress().getAddressCountry().getIsoCountryAlphaTwoCode());
				}

				oBRisk1.setDeliveryAddress(oBRisk1DeliveryAddress);
			}

		}

		oBWriteDataInternationalConsentResponse1.setInitiation(oBInternational1);
		customIPaymentConsentsPOSTResponse.setData(oBWriteDataInternationalConsentResponse1);
		customIPaymentConsentsPOSTResponse.setRisk(oBRisk1);

		return customIPaymentConsentsPOSTResponse;
	}

	public <T> CustomIPaymentConsentsPOSTResponse transformInternationalConsentResponseFromFDToAPIForInsert(
			T paymentInstructionProposalInternationalResponse) {
		CustomIPaymentConsentsPOSTResponse customIPaymentConsentsPOSTResponse = new CustomIPaymentConsentsPOSTResponse();
		PaymentInstructionProposalInternational paymentInstructionProposalInternational = (PaymentInstructionProposalInternational) paymentInstructionProposalInternationalResponse;
		customIPaymentConsentsPOSTResponse = transformInternationalPaymentResponse(
				paymentInstructionProposalInternationalResponse);

		ProposalStatus status = ProposalStatus
				.fromValue(paymentInstructionProposalInternational.getProposalStatus().toString());

		if (((status.toString()).equals("Rejected")) && (!NullCheckUtils
				.isNullOrEmpty(paymentInstructionProposalInternational.getPaymentInstructionProposalId()))) {
			ProcessConsentStatusEnum consentStatus = ProcessConsentStatusEnum.FAIL;
			customIPaymentConsentsPOSTResponse.setConsentPorcessStatus(consentStatus);
		} else if (!NullCheckUtils
				.isNullOrEmpty(paymentInstructionProposalInternational.getPaymentInstructionProposalId())) {

			customIPaymentConsentsPOSTResponse.setConsentPorcessStatus(ProcessConsentStatusEnum.PASS);
		}
		return customIPaymentConsentsPOSTResponse;
	}

	public PaymentInstructionProposalInternational transformInternationalConsentResponseFromAPIToFDForInsert(
			CustomIPaymentConsentsPOSTRequest paymentConsentsRequest, Map<String, String> params) {

		PaymentInstructionProposalInternational paymentInstructionProposalInternational = new PaymentInstructionProposalInternational();

		paymentInstructionProposalInternational.setPaymentInstructionProposalId("");

		Channel channel = new Channel();
		if (!NullCheckUtils.isNullOrEmpty(params.get(PSD2Constants.CHANNEL_IN_REQ_HEADER))) {

			channel.setChannelCode(
					ChannelCode1.fromValue((params.get(PSD2Constants.CHANNEL_IN_REQ_HEADER)).toUpperCase()));
		} else {
			channel.setChannelCode(ChannelCode1.API);
		}

		if (!NullCheckUtils.isNullOrEmpty(params.get(PSD2Constants.TENANT_ID))) {

			if (params.get(PSD2Constants.TENANT_ID).equalsIgnoreCase("BOIROI")) {
				channel.setBrandCode(BrandCode3.ROI);
			}

		}

		paymentInstructionProposalInternational.setChannel(channel);

		if (!NullCheckUtils
				.isNullOrEmpty(paymentConsentsRequest.getData().getInitiation().getInstructionIdentification())) {

			paymentInstructionProposalInternational.setInstructionReference(
					paymentConsentsRequest.getData().getInitiation().getInstructionIdentification());
		}

		if (!NullCheckUtils
				.isNullOrEmpty(paymentConsentsRequest.getData().getInitiation().getEndToEndIdentification())) {

			paymentInstructionProposalInternational.setInstructionEndToEndReference(
					paymentConsentsRequest.getData().getInitiation().getEndToEndIdentification());
		}

		if (!NullCheckUtils.isNullOrEmpty(paymentConsentsRequest.getData().getInitiation().getLocalInstrument())) {
			paymentInstructionProposalInternational.setInstructionLocalInstrument(
					paymentConsentsRequest.getData().getInitiation().getLocalInstrument());
		}

		if (!NullCheckUtils.isNullOrEmpty(paymentConsentsRequest.getData().getInitiation().getInstructionPriority())) {
			PriorityCode priorityCode = PriorityCode
					.fromValue(paymentConsentsRequest.getData().getInitiation().getInstructionPriority().toString());
			paymentInstructionProposalInternational.setPriorityCode(priorityCode);
		}
		Purpose purpose = new Purpose();
		if (!NullCheckUtils.isNullOrEmpty(paymentConsentsRequest.getData().getInitiation().getPurpose())) {
			//Updated Mapping based on RAML Upgrade v1.0.64
			purpose.setPurposeCode(paymentConsentsRequest.getData().getInitiation().getPurpose());
			paymentInstructionProposalInternational.setPurpose(purpose);
		}
		System.out.println(paymentConsentsRequest.getData().getInitiation().getChargeBearer());

		List<PaymentInstructionCharge> charges = new ArrayList<>();
		PaymentInstructionCharge paymentInstructionCharge = new PaymentInstructionCharge();
		if (!NullCheckUtils.isNullOrEmpty(paymentConsentsRequest.getData().getInitiation().getChargeBearer())) {

			ChargeBearer chargeBearer = ChargeBearer
					.fromValue(paymentConsentsRequest.getData().getInitiation().getChargeBearer().toString());

			paymentInstructionCharge.setChargeBearer(chargeBearer);
			charges.add(paymentInstructionCharge);
			paymentInstructionProposalInternational.setCharges(charges);

		}

		Currency currency1 = new Currency();
		if (!NullCheckUtils.isNullOrEmpty(paymentConsentsRequest.getData().getInitiation().getCurrencyOfTransfer())) {

			currency1.setIsoAlphaCode(paymentConsentsRequest.getData().getInitiation().getCurrencyOfTransfer());

			paymentInstructionProposalInternational.setCurrencyOfTransfer(currency1);

		}

		// Instructed Amount
		Currency currency2 = new Currency();
		if (!NullCheckUtils.isNullOrEmpty(paymentConsentsRequest.getData().getInitiation().getInstructedAmount())) {

			if (!NullCheckUtils.isNullOrEmpty(
					paymentConsentsRequest.getData().getInitiation().getInstructedAmount().getAmount())) {
				FinancialEventAmount financialEventAmount = new FinancialEventAmount();
				String amount = paymentConsentsRequest.getData().getInitiation().getInstructedAmount().getAmount();
				Double amountDouble = Double.parseDouble(amount);
				financialEventAmount.setTransactionCurrency(amountDouble);
				paymentInstructionProposalInternational.setFinancialEventAmount(financialEventAmount);
			}

			if (!NullCheckUtils.isNullOrEmpty(
					paymentConsentsRequest.getData().getInitiation().getInstructedAmount().getCurrency())) {
				String currency = paymentConsentsRequest.getData().getInitiation().getInstructedAmount().getCurrency();
				currency2.setIsoAlphaCode(currency);
				paymentInstructionProposalInternational.setTransactionCurrency(currency2);
			}
		}
		ExchangeRateQuote exchangeRateQuote = new ExchangeRateQuote();
		// ExchangeRateInformation
		Currency currency3 = new Currency();
		if (!NullCheckUtils
				.isNullOrEmpty(paymentConsentsRequest.getData().getInitiation().getExchangeRateInformation())) {

			if (!NullCheckUtils.isNullOrEmpty(
					paymentConsentsRequest.getData().getInitiation().getExchangeRateInformation().getUnitCurrency())) {
				String currency = paymentConsentsRequest.getData().getInitiation().getExchangeRateInformation()
						.getUnitCurrency();
				currency3.setIsoAlphaCode(currency);
				exchangeRateQuote.setUnitCurrency(currency3);
			}

			if (!NullCheckUtils.isNullOrEmpty(
					paymentConsentsRequest.getData().getInitiation().getExchangeRateInformation().getExchangeRate())) {

				Double exchangeRate = paymentConsentsRequest.getData().getInitiation().getExchangeRateInformation()
						.getExchangeRate().doubleValue();
				exchangeRateQuote.setExchangeRate(exchangeRate);
			}

			if (!NullCheckUtils.isNullOrEmpty(
					paymentConsentsRequest.getData().getInitiation().getExchangeRateInformation().getRateType())) {

				RateQuoteType rateQuoteType = RateQuoteType.fromValue(paymentConsentsRequest.getData().getInitiation()
						.getExchangeRateInformation().getRateType().toString());
				exchangeRateQuote.setRateQuoteType(rateQuoteType);
			}

			/*
			 * We can reomve this as mapping is not present if
			 * (!NullCheckUtils.isNullOrEmpty(paymentConsentsRequest.getData().getInitiation
			 * () .getExchangeRateInformation().getContractIdentification())) {
			 * 
			 * // ICD Mapping is not present }
			 */
			paymentInstructionProposalInternational.setExchangeRateQuote(exchangeRateQuote);
			/*
			 * if (!NullCheckUtils.isNullOrEmpty(
			 * paymentConsentsRequest.getData().getInitiation().getExchangeRateInformation()
			 * ) {
			 * 
			 * //Field ExpirationDateTime is not present in Domain Class OBExchangeRate1 }
			 */
		}

		// Debtor Account

		if (!NullCheckUtils.isNullOrEmpty(paymentConsentsRequest.getData().getInitiation().getDebtorAccount())) {
			AccountInformation authorisingPartyAccount = new AccountInformation();

			// AuthorisingPartyAccount authorisingPartyAccount = new
			// AuthorisingPartyAccount();
			if (!NullCheckUtils.isNullOrEmpty(
					paymentConsentsRequest.getData().getInitiation().getDebtorAccount().getSchemeName())) {
				authorisingPartyAccount.setSchemeName(
						paymentConsentsRequest.getData().getInitiation().getDebtorAccount().getSchemeName());
			}

			if (!NullCheckUtils.isNullOrEmpty(
					paymentConsentsRequest.getData().getInitiation().getDebtorAccount().getIdentification())) {
				authorisingPartyAccount.setAccountIdentification(
						paymentConsentsRequest.getData().getInitiation().getDebtorAccount().getIdentification());
			}

			if (!NullCheckUtils
					.isNullOrEmpty(paymentConsentsRequest.getData().getInitiation().getDebtorAccount().getName())) {
				authorisingPartyAccount
						.setAccountName(paymentConsentsRequest.getData().getInitiation().getDebtorAccount().getName());
			}

			if (!NullCheckUtils.isNullOrEmpty(
					paymentConsentsRequest.getData().getInitiation().getDebtorAccount().getSecondaryIdentification())) {
				authorisingPartyAccount.setSecondaryIdentification(paymentConsentsRequest.getData().getInitiation()
						.getDebtorAccount().getSecondaryIdentification());
			}

			paymentInstructionProposalInternational.setAuthorisingPartyAccount(authorisingPartyAccount);
		}
		// Creditor Account

		if (!NullCheckUtils.isNullOrEmpty(paymentConsentsRequest.getData().getInitiation().getCreditorAccount())) {
			ProposingPartyAccount proposingPartyAccount = new ProposingPartyAccount();
			if (!NullCheckUtils.isNullOrEmpty(
					paymentConsentsRequest.getData().getInitiation().getCreditorAccount().getSchemeName())) {

				proposingPartyAccount.setSchemeName(
						paymentConsentsRequest.getData().getInitiation().getCreditorAccount().getSchemeName());
			}

			if (!NullCheckUtils.isNullOrEmpty(
					paymentConsentsRequest.getData().getInitiation().getCreditorAccount().getIdentification())) {

				proposingPartyAccount.setAccountIdentification(
						paymentConsentsRequest.getData().getInitiation().getCreditorAccount().getIdentification());
			}

			if (!NullCheckUtils
					.isNullOrEmpty(paymentConsentsRequest.getData().getInitiation().getCreditorAccount().getName())) {

				proposingPartyAccount.setAccountName(
						paymentConsentsRequest.getData().getInitiation().getCreditorAccount().getName());
			}

			if (!NullCheckUtils.isNullOrEmpty(paymentConsentsRequest.getData().getInitiation().getCreditorAccount()
					.getSecondaryIdentification())) {
				proposingPartyAccount.setSecondaryIdentification(paymentConsentsRequest.getData().getInitiation()
						.getCreditorAccount().getSecondaryIdentification());
			}

			paymentInstructionProposalInternational.setProposingPartyAccount(proposingPartyAccount);
		}

		// CreditorAgent
		Agent agent = new Agent();
		if (!NullCheckUtils.isNullOrEmpty(paymentConsentsRequest.getData().getInitiation().getCreditorAgent())) {

			if (!NullCheckUtils.isNullOrEmpty(
					paymentConsentsRequest.getData().getInitiation().getCreditorAgent().getSchemeName())) {
				agent.setSchemeName(
						paymentConsentsRequest.getData().getInitiation().getCreditorAgent().getSchemeName());
			}

			if (!NullCheckUtils.isNullOrEmpty(
					paymentConsentsRequest.getData().getInitiation().getCreditorAgent().getIdentification())) {
				agent.setAgentIdentification(
						paymentConsentsRequest.getData().getInitiation().getCreditorAgent().getIdentification());
			}

			if (!NullCheckUtils
					.isNullOrEmpty(paymentConsentsRequest.getData().getInitiation().getCreditorAgent().getName())) {
				agent.setAgentName(paymentConsentsRequest.getData().getInitiation().getCreditorAgent().getName());
			}

			// Postal Address

			PaymentInstructionPostalAddress paymentInstructionPostalAddress = new PaymentInstructionPostalAddress();
			if (!NullCheckUtils.isNullOrEmpty(
					paymentConsentsRequest.getData().getInitiation().getCreditorAgent().getPostalAddress())) {

				if (!NullCheckUtils.isNullOrEmpty(paymentConsentsRequest.getData().getInitiation().getCreditorAgent()
						.getPostalAddress().getAddressType())) {

					paymentInstructionPostalAddress.setAddressType(paymentConsentsRequest.getData().getInitiation()
							.getCreditorAgent().getPostalAddress().getAddressType().toString());

				}

				if (!NullCheckUtils.isNullOrEmpty(paymentConsentsRequest.getData().getInitiation().getCreditorAgent()
						.getPostalAddress().getDepartment())) {

					paymentInstructionPostalAddress.setDepartment(paymentConsentsRequest.getData().getInitiation()
							.getCreditorAgent().getPostalAddress().getDepartment().toString());

				}

				if (!NullCheckUtils.isNullOrEmpty(paymentConsentsRequest.getData().getInitiation().getCreditorAgent()
						.getPostalAddress().getSubDepartment())) {

					paymentInstructionPostalAddress.setSubDepartment(paymentConsentsRequest.getData().getInitiation()
							.getCreditorAgent().getPostalAddress().getSubDepartment().toString());

				}

				if (!NullCheckUtils.isNullOrEmpty(paymentConsentsRequest.getData().getInitiation().getCreditorAgent()
						.getPostalAddress().getStreetName())) {

					paymentInstructionPostalAddress.setGeoCodeBuildingName(paymentConsentsRequest.getData()
							.getInitiation().getCreditorAgent().getPostalAddress().getStreetName().toString());
				}

				if (!NullCheckUtils.isNullOrEmpty(paymentConsentsRequest.getData().getInitiation().getCreditorAgent()
						.getPostalAddress().getBuildingNumber())) {

					paymentInstructionPostalAddress.setGeoCodeBuildingNumber(paymentConsentsRequest.getData()
							.getInitiation().getCreditorAgent().getPostalAddress().getBuildingNumber().toString());
				}

				if (!NullCheckUtils.isNullOrEmpty(paymentConsentsRequest.getData().getInitiation().getCreditorAgent()
						.getPostalAddress().getPostCode())) {

					paymentInstructionPostalAddress.setPostCodeNumber(paymentConsentsRequest.getData().getInitiation()
							.getCreditorAgent().getPostalAddress().getPostCode().toString());
				}
				if (!NullCheckUtils.isNullOrEmpty(paymentConsentsRequest.getData().getInitiation().getCreditorAgent()
						.getPostalAddress().getTownName())) {

					paymentInstructionPostalAddress.setTownName(paymentConsentsRequest.getData().getInitiation()
							.getCreditorAgent().getPostalAddress().getTownName().toString());
				}

				if (!NullCheckUtils.isNullOrEmpty(paymentConsentsRequest.getData().getInitiation().getCreditorAgent()
						.getPostalAddress().getCountrySubDivision())) {

					paymentInstructionPostalAddress.setCountrySubDivision(paymentConsentsRequest.getData()
							.getInitiation().getCreditorAgent().getPostalAddress().getCountrySubDivision().toString());
				}
				Country country = new Country();
				if (!NullCheckUtils.isNullOrEmpty(paymentConsentsRequest.getData().getInitiation().getCreditorAgent()
						.getPostalAddress().getCountry())) {
					country.setIsoCountryAlphaTwoCode(paymentConsentsRequest.getData().getInitiation()
							.getCreditorAgent().getPostalAddress().getCountry().toString());
					paymentInstructionPostalAddress.setAddressCountry(country);
				}
				if (!NullCheckUtils.isNullOrEmpty(paymentConsentsRequest.getData().getInitiation().getCreditorAgent()
						.getPostalAddress().getAddressLine())) {
					List<String> addLine = paymentConsentsRequest.getData().getInitiation().getCreditorAgent()
							.getPostalAddress().getAddressLine();
					if (!NullCheckUtils.isNullOrEmpty(addLine) && addLine.size() > 0) {
						paymentInstructionPostalAddress.setAddressLine(paymentConsentsRequest.getData().getInitiation()
								.getCreditorAgent().getPostalAddress().getAddressLine());
					}
				}
				agent.setPartyAddress(paymentInstructionPostalAddress);
				paymentInstructionProposalInternational.setProposingPartyAgent(agent);
			}
		}

		// Creditor
		PartyBasicInformation partyBasicInformation = new PartyBasicInformation();
		if (!NullCheckUtils.isNullOrEmpty(paymentConsentsRequest.getData().getInitiation().getCreditor())) {

			if (!NullCheckUtils
					.isNullOrEmpty(paymentConsentsRequest.getData().getInitiation().getCreditor().getName())) {
				partyBasicInformation
						.setPartyName(paymentConsentsRequest.getData().getInitiation().getCreditor().getName());
				paymentInstructionProposalInternational.setProposingParty(partyBasicInformation);
			}

			// Postal Address of Creditor

			PaymentInstructionPostalAddress paymentInstructionPostalAddress = new PaymentInstructionPostalAddress();
			if (!NullCheckUtils
					.isNullOrEmpty(paymentConsentsRequest.getData().getInitiation().getCreditor().getPostalAddress())) {

				if (!NullCheckUtils.isNullOrEmpty(paymentConsentsRequest.getData().getInitiation().getCreditor()
						.getPostalAddress().getAddressType())) {

					paymentInstructionPostalAddress.setAddressType(paymentConsentsRequest.getData().getInitiation()
							.getCreditor().getPostalAddress().getAddressType().toString());

				}

				if (!NullCheckUtils.isNullOrEmpty(paymentConsentsRequest.getData().getInitiation().getCreditor()
						.getPostalAddress().getDepartment())) {

					paymentInstructionPostalAddress.setDepartment(paymentConsentsRequest.getData().getInitiation()
							.getCreditor().getPostalAddress().getDepartment().toString());

				}

				if (!NullCheckUtils.isNullOrEmpty(paymentConsentsRequest.getData().getInitiation().getCreditor()
						.getPostalAddress().getSubDepartment())) {

					paymentInstructionPostalAddress.setSubDepartment(paymentConsentsRequest.getData().getInitiation()
							.getCreditor().getPostalAddress().getSubDepartment().toString());

				}

				if (!NullCheckUtils.isNullOrEmpty(paymentConsentsRequest.getData().getInitiation().getCreditor()
						.getPostalAddress().getStreetName())) {

					paymentInstructionPostalAddress.setGeoCodeBuildingName(paymentConsentsRequest.getData()
							.getInitiation().getCreditor().getPostalAddress().getStreetName().toString());
				}

				if (!NullCheckUtils.isNullOrEmpty(paymentConsentsRequest.getData().getInitiation().getCreditor()
						.getPostalAddress().getBuildingNumber())) {

					paymentInstructionPostalAddress.setGeoCodeBuildingNumber(paymentConsentsRequest.getData()
							.getInitiation().getCreditor().getPostalAddress().getBuildingNumber().toString());
				}

				if (!NullCheckUtils.isNullOrEmpty(paymentConsentsRequest.getData().getInitiation().getCreditor()
						.getPostalAddress().getPostCode())) {

					paymentInstructionPostalAddress.setPostCodeNumber(paymentConsentsRequest.getData().getInitiation()
							.getCreditor().getPostalAddress().getPostCode().toString());
				}
				if (!NullCheckUtils.isNullOrEmpty(paymentConsentsRequest.getData().getInitiation().getCreditor()
						.getPostalAddress().getTownName())) {

					paymentInstructionPostalAddress.setTownName(paymentConsentsRequest.getData().getInitiation()
							.getCreditor().getPostalAddress().getTownName().toString());
				}

				if (!NullCheckUtils.isNullOrEmpty(paymentConsentsRequest.getData().getInitiation().getCreditor()
						.getPostalAddress().getCountrySubDivision())) {

					paymentInstructionPostalAddress.setCountrySubDivision(paymentConsentsRequest.getData()
							.getInitiation().getCreditor().getPostalAddress().getCountrySubDivision().toString());
				}
				Country country = new Country();
				if (!NullCheckUtils.isNullOrEmpty(paymentConsentsRequest.getData().getInitiation().getCreditor()
						.getPostalAddress().getCountry())) {
					country.setIsoCountryAlphaTwoCode(paymentConsentsRequest.getData().getInitiation().getCreditor()
							.getPostalAddress().getCountry().toString());
					paymentInstructionPostalAddress.setAddressCountry(country);
				}
				if (!NullCheckUtils.isNullOrEmpty(paymentConsentsRequest.getData().getInitiation().getCreditor()
						.getPostalAddress().getAddressLine())) {
					List<String> addLine = paymentConsentsRequest.getData().getInitiation().getCreditor()
							.getPostalAddress().getAddressLine();
					if (!NullCheckUtils.isNullOrEmpty(addLine) && addLine.size() > 0) {
						paymentInstructionPostalAddress.setAddressLine(paymentConsentsRequest.getData().getInitiation()
								.getCreditor().getPostalAddress().getAddressLine());
					}
				}

				paymentInstructionProposalInternational.setProposingPartyPostalAddress(paymentInstructionPostalAddress);

			}
		}

		// Remittance Information

		if (!NullCheckUtils
				.isNullOrEmpty(paymentConsentsRequest.getData().getInitiation().getRemittanceInformation())) {

			if (!NullCheckUtils.isNullOrEmpty(
					paymentConsentsRequest.getData().getInitiation().getRemittanceInformation().getUnstructured())) {
				paymentInstructionProposalInternational.setAuthorisingPartyUnstructuredReference(
						paymentConsentsRequest.getData().getInitiation().getRemittanceInformation().getUnstructured());
			}

			if (!NullCheckUtils.isNullOrEmpty(
					paymentConsentsRequest.getData().getInitiation().getRemittanceInformation().getReference())) {
				paymentInstructionProposalInternational.setAuthorisingPartyReference(
						paymentConsentsRequest.getData().getInitiation().getRemittanceInformation().getReference());
			}
		}

		// Authorization

		if (!NullCheckUtils.isNullOrEmpty(paymentConsentsRequest.getData().getAuthorisation())) {

			if (!NullCheckUtils
					.isNullOrEmpty(paymentConsentsRequest.getData().getAuthorisation().getAuthorisationType())) {
				String authorisationType = paymentConsentsRequest.getData().getAuthorisation().getAuthorisationType()
						.toString();
				paymentInstructionProposalInternational
						.setAuthorisationType(AuthorisationType.fromValue(authorisationType));
			}

			if (!NullCheckUtils
					.isNullOrEmpty(paymentConsentsRequest.getData().getAuthorisation().getCompletionDateTime())) {
				paymentInstructionProposalInternational.setAuthorisationDatetime(
						paymentConsentsRequest.getData().getAuthorisation().getCompletionDateTime());
				// Converted Date to String in Adapter Domain Class
			}
		}
		// Risk

		if (!NullCheckUtils.isNullOrEmpty(paymentConsentsRequest.getRisk())) {

			PaymentInstrumentRiskFactor paymentInstructionRiskFactorReference = new PaymentInstrumentRiskFactor();

			if (!NullCheckUtils.isNullOrEmpty(paymentConsentsRequest.getRisk().getPaymentContextCode())) {
				paymentInstructionRiskFactorReference.setPaymentContextCode(
						String.valueOf(paymentConsentsRequest.getRisk().getPaymentContextCode()));
			}

			if (!NullCheckUtils.isNullOrEmpty(paymentConsentsRequest.getRisk().getMerchantCategoryCode())) {
				paymentInstructionRiskFactorReference
						.setMerchantCategoryCode(paymentConsentsRequest.getRisk().getMerchantCategoryCode());
			}

			if (!NullCheckUtils.isNullOrEmpty(paymentConsentsRequest.getRisk().getMerchantCustomerIdentification())) {
				paymentInstructionRiskFactorReference.setMerchantCustomerIdentification(
						paymentConsentsRequest.getRisk().getMerchantCustomerIdentification());
			}

			// Delivery Address

			if (!NullCheckUtils.isNullOrEmpty(paymentConsentsRequest.getRisk().getDeliveryAddress())) {
				Address counterPartyAddress = new Address();
				if (!NullCheckUtils
						.isNullOrEmpty(paymentConsentsRequest.getRisk().getDeliveryAddress().getAddressLine())
						&& paymentConsentsRequest.getRisk().getDeliveryAddress().getAddressLine().size() > 0) {
					counterPartyAddress.setFirstAddressLine(
							paymentConsentsRequest.getRisk().getDeliveryAddress().getAddressLine().get(0));

					if (!NullCheckUtils
							.isNullOrEmpty(paymentConsentsRequest.getRisk().getDeliveryAddress().getAddressLine())
							&& paymentConsentsRequest.getRisk().getDeliveryAddress().getAddressLine().size() > 1) {
						counterPartyAddress.setSecondAddressLine(
								paymentConsentsRequest.getRisk().getDeliveryAddress().getAddressLine().get(1));
					}
				}

				if (!NullCheckUtils
						.isNullOrEmpty(paymentConsentsRequest.getRisk().getDeliveryAddress().getStreetName())) {
					counterPartyAddress.setGeoCodeBuildingName(
							paymentConsentsRequest.getRisk().getDeliveryAddress().getStreetName());
				}

				if (!NullCheckUtils
						.isNullOrEmpty(paymentConsentsRequest.getRisk().getDeliveryAddress().getBuildingNumber())) {
					counterPartyAddress.setGeoCodeBuildingNumber(
							paymentConsentsRequest.getRisk().getDeliveryAddress().getBuildingNumber());
				}

				if (!NullCheckUtils
						.isNullOrEmpty(paymentConsentsRequest.getRisk().getDeliveryAddress().getPostCode())) {
					counterPartyAddress
							.setPostCodeNumber(paymentConsentsRequest.getRisk().getDeliveryAddress().getPostCode());
				}

				if (!NullCheckUtils
						.isNullOrEmpty(paymentConsentsRequest.getRisk().getDeliveryAddress().getTownName())) {
					counterPartyAddress
							.setThirdAddressLine(paymentConsentsRequest.getRisk().getDeliveryAddress().getTownName());
				}

				if (!NullCheckUtils
						.isNullOrEmpty(paymentConsentsRequest.getRisk().getDeliveryAddress().getCountrySubDivision())) {
					counterPartyAddress.setFourthAddressLine(
							paymentConsentsRequest.getRisk().getDeliveryAddress().getCountrySubDivision());

				}

				if (!NullCheckUtils.isNullOrEmpty(paymentConsentsRequest.getRisk().getDeliveryAddress().getCountry())) {
					Country addressCountryRisk = new Country();
					addressCountryRisk.setIsoCountryAlphaTwoCode(
							paymentConsentsRequest.getRisk().getDeliveryAddress().getCountry());
					counterPartyAddress.setAddressCountry(addressCountryRisk);
				}

				paymentInstructionRiskFactorReference.setCounterPartyAddress(counterPartyAddress);
			}

			paymentInstructionProposalInternational
					.setPaymentInstructionRiskFactorReference(paymentInstructionRiskFactorReference);
		}

		return paymentInstructionProposalInternational;
	}

	public static String doubleToString(double amount)

	{

		String fdToApiAmount = BigDecimal.valueOf(amount).toPlainString();

		if (fdToApiAmount.contains("."))

		{

			String[] amountArray = fdToApiAmount.split("\\.");

			if (amountArray[1].length() == 2)

			{

				return fdToApiAmount;

			} else if (amountArray[1].length() == 1)

			{

				return fdToApiAmount + "0";

			}

		}

		else

		{

			fdToApiAmount += ".00";

		}

		return fdToApiAmount;

	}

}
