package com.capgemini.psd2.foundationservice.authentication.application.boi.adapter.test;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.adapter.security.domain.Login;
import com.capgemini.psd2.adapter.security.domain.LoginResponse;
import com.capgemini.psd2.foundationservice.authentication.application.boi.adapter.client.AuthenticationApplicationFoundationServiceClient;
import com.capgemini.psd2.foundationservice.authentication.application.boi.adapter.raml.domain.AuthenticationRequest;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.impl.RestClientSyncImpl;

@RunWith(SpringJUnit4ClassRunner.class)
public class AuthenticationApplicationFoundationServiceClientTest {
	
	@InjectMocks
	private AuthenticationApplicationFoundationServiceClient authenticationApplicationFoundationServiceClient;
	
	@Mock
	private RestClientSyncImpl restClient;
	
	@Before
	public void setUp(){
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void contextLoads() {
	}
	
	@Test
	public void restTransportForAuthenticationApplication(){
	
	PrincipalImpl principal = new PrincipalImpl("99999999");
	CredentialsImpl credentials = new CredentialsImpl("999999");
	AuthenticationImpl authentication = new AuthenticationImpl(principal, credentials);
	
	AuthenticationRequest authenticationRequest = new AuthenticationRequest();
	authenticationRequest.setUserName(authentication.getPrincipal().toString());
	authenticationRequest.setPassword(authentication.getCredentials().toString());
	
	HttpHeaders httpHeaders = new HttpHeaders();
	httpHeaders.add("X-BOI-USER", "b365_User");
	httpHeaders.add("X-BOI-CHANNEL", "b365");
	httpHeaders.add("X-BOI-PLATFORM", "BOI_Platform");
	httpHeaders.add("X-CORRELATION-ID", "12135-78524-85214");
	
	RequestInfo requestInfo = new RequestInfo();
	requestInfo.setUrl("http://localhost:8081/fs-login-business-service/services/loginServiceBusiness/channel/business/login");
	
	Mockito.when(restClient.callForPost(any(),any(),any(),any())).thenReturn("Successfully authenticated");
	
	String response = authenticationApplicationFoundationServiceClient.restTransportForAuthenticationApplication(requestInfo, authenticationRequest, String.class, httpHeaders);
	
	assertNotNull(response);
	
	}
	
	@Test
	public void restTransportForAuthenticationApplication_bol(){
	
	PrincipalImpl principal = new PrincipalImpl("88888888");
	CredentialsImpl credentials = new CredentialsImpl("888888");
	AuthenticationImpl authentication = new AuthenticationImpl(principal, credentials);

	Login digitalUserRequest=null;
	Class<LoginResponse> response=null;
	AuthenticationRequest authenticationRequest = new AuthenticationRequest();
	authenticationRequest.setUserName(authentication.getPrincipal().toString());
	authenticationRequest.setPassword(authentication.getCredentials().toString());
	
	HttpHeaders httpHeaders = new HttpHeaders();
	httpHeaders.add("X-BOI-USER", "88888888");
	httpHeaders.add("X-BOI-CHANNEL", "BOL");
	httpHeaders.add("X-BOI-PLATFORM", "BOI_Platform");
	httpHeaders.add("X-CORRELATION-ID", "12365-14785-96325");
	
	RequestInfo requestInfo = new RequestInfo();
	requestInfo.setUrl("http://localhost:8081/fs-login-business-service/services/loginServiceBusiness/channel/business/login");
	
	//Mockito.when(restClient.callForPost(any(),any(),any(),any())).thenReturn("Successfully authenticated");
	
	authenticationApplicationFoundationServiceClient.restTransportForAuthenticationServicenewBOL(requestInfo, digitalUserRequest, response, httpHeaders);
	
	//assertNotNull(response1);
	
	}
}
