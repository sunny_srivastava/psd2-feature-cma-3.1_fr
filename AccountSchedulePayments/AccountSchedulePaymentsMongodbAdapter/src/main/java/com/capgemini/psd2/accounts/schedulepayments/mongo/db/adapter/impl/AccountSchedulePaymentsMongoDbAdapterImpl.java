package com.capgemini.psd2.accounts.schedulepayments.mongo.db.adapter.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.accounts.schedulepayments.mongo.db.adapter.repository.AccountSchedulePaymentRepository;
import com.capgemini.psd2.aisp.adapter.AccountSchedulePaymentsAdapter;
import com.capgemini.psd2.aisp.domain.OBReadScheduledPayment3;
import com.capgemini.psd2.aisp.domain.OBReadScheduledPayment3Data;
import com.capgemini.psd2.aisp.domain.OBScheduledPayment3;
import com.capgemini.psd2.aisp.mongo.db.adapter.domain.AccountSchedulePaymentsCMA2;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountSchedulePaymentsResponse;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.exceptions.ErrorMapKeys;
import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.InternalServerErrorMessage;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.utilities.SandboxValidationUtility;

@Component
public class AccountSchedulePaymentsMongoDbAdapterImpl implements AccountSchedulePaymentsAdapter{

	@Autowired
	private AccountSchedulePaymentRepository repository;
	
	@Autowired
	private SandboxValidationUtility utility;

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(AccountSchedulePaymentsMongoDbAdapterImpl.class);
	
	@Override
	public PlatformAccountSchedulePaymentsResponse retrieveAccountSchedulePayments(AccountMapping accountMapping,
			Map<String, String> params) {
		PlatformAccountSchedulePaymentsResponse platformAccountSchedulePaymentsResponse = new PlatformAccountSchedulePaymentsResponse();
		OBReadScheduledPayment3 oBReadScheduledPayment1 = new OBReadScheduledPayment3();
		List<AccountSchedulePaymentsCMA2> mockSchedulePayments = new ArrayList<>();
		
		try {
			if(utility.isValidPsuId(accountMapping.getPsuId())){
				throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,InternalServerErrorMessage.SANDBOX_MOCKED_ERROR));
			}
			mockSchedulePayments = repository.findByAccountNumberAndAccountNSC(
					accountMapping.getAccountDetails().get(0).getAccountNumber(),
					accountMapping.getAccountDetails().get(0).getAccountNSC());
		} catch (DataAccessResourceFailureException e) {
			LOG.error(e.getMessage(),e);
			 
			 throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
					 InternalServerErrorMessage.CONNECTION_ERROR));
		}

		List<OBScheduledPayment3> schedulePaymentList = new ArrayList<>();
		
		if (null != mockSchedulePayments && !mockSchedulePayments.isEmpty()) {
			for (AccountSchedulePaymentsCMA2 scheduledPayment : mockSchedulePayments) {
				scheduledPayment.setScheduledPaymentDateTime(utility.addDaysToCurrentDtTm(5));
				scheduledPayment.setAccountId(accountMapping.getAccountDetails().get(0).getAccountId());
				schedulePaymentList.add(scheduledPayment);
			}
		}
		
		OBReadScheduledPayment3Data oBReadScheduledPayment1Data = new OBReadScheduledPayment3Data();
		oBReadScheduledPayment1Data.setScheduledPayment(schedulePaymentList);
		oBReadScheduledPayment1.setData(oBReadScheduledPayment1Data);
		platformAccountSchedulePaymentsResponse.setObReadScheduledPayment3(oBReadScheduledPayment1);
		return platformAccountSchedulePaymentsResponse;
	}

}
