package com.capgemini.psd2.account.schedulepayment.mongodb.adapter.test.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyString;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.account.schedulepayment.mongodb.adapter.test.mock.data.AccountSchedulePaymentMongoMockData;
import com.capgemini.psd2.accounts.schedulepayments.mongo.db.adapter.impl.AccountSchedulePaymentsMongoDbAdapterImpl;
import com.capgemini.psd2.accounts.schedulepayments.mongo.db.adapter.repository.AccountSchedulePaymentRepository;
import com.capgemini.psd2.aisp.mongo.db.adapter.domain.AccountSchedulePaymentsCMA2;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountSchedulePaymentsResponse;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.exceptions.OBPSD2ExceptionUtility;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.LoggerUtils;
import com.capgemini.psd2.utilities.SandboxValidationUtility;

@RunWith(SpringJUnit4ClassRunner.class)
public class AccountSchedulePaymentMongoDbAdapterImplTest{

	@Mock
	private AccountSchedulePaymentRepository accountSchedulePaymentRepository;

	@Mock
	private LoggerUtils loggerUtils;
	
	@Mock
	private SandboxValidationUtility utility;

	@InjectMocks
	private AccountSchedulePaymentsMongoDbAdapterImpl accountSchedulePaymentMongoDbAdapterImpl;
	

	/**
	 * Sets the up.
	 *
	 * @throws Exception
	 *             the exception
	 */
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		Map<String, String> map=new HashMap<>();
		map.put("SIGNATURE", "Something wrong with x-jws-signature request header");
		Map<String, String> specificErrorMessageMap=new HashMap<>();
		specificErrorMessageMap.put("signature_invalid_content", "Invalid content prsent in joseheader of x-jws-signature header");
		specificErrorMessageMap.put("signature_missing", "signature header missing in request");
		OBPSD2ExceptionUtility.genericErrorMessages.putAll(map);
		OBPSD2ExceptionUtility.specificErrorMessages.putAll(specificErrorMessageMap);
	}

	/** The account schedule payment mongo DB adapter impl. */

	/**
	 * Test retrieve blank response account schedule payment success flow.
	 */

	@Test
	public void testRetrieveAccountSchedulePaymentSuccessFlow() {
		AccountMapping accountMapping = new AccountMapping();
		ArrayList<AccountDetails> accountDetailsList = new ArrayList<>();
		AccountDetails accountDetails = new AccountDetails();
		Map<String, String> params = new HashMap<>();

		accountDetails.setAccountId("f4483fda-81be-4873-b4a6-20375b7f55c1");
		accountDetailsList.add(accountDetails);
		accountMapping.setAccountDetails(accountDetailsList);

		List<AccountSchedulePaymentsCMA2> scheduledPaymentList = new ArrayList<>();
		AccountSchedulePaymentsCMA2 scheduledPayment = new AccountSchedulePaymentsCMA2();
		scheduledPaymentList.add(scheduledPayment);
		Mockito.when(accountSchedulePaymentRepository.findByAccountNumberAndAccountNSC(anyString(), anyString()))
				.thenReturn(scheduledPaymentList);

		Mockito.when(loggerUtils.populateLoggerData(anyString()))
				.thenReturn(AccountSchedulePaymentMongoMockData.getMockLoggerData());

		PlatformAccountSchedulePaymentsResponse actualResponse = accountSchedulePaymentMongoDbAdapterImpl.retrieveAccountSchedulePayments(accountMapping, params);

		assertEquals(AccountSchedulePaymentMongoMockData.getMockExpectedAccountSchedulePaymentResponse(),
				actualResponse.getOBReadScheduledPayment3());

	}

	@SuppressWarnings("unchecked")
	@Test(expected = PSD2Exception.class)
	public void testRetrieveAccountSchedulepaymentDbExceptionFlow() {
		AccountMapping accountMapping = new AccountMapping();
		ArrayList<AccountDetails> accountDetailsList = new ArrayList<>();
		AccountDetails accountDetails = new AccountDetails();
		Map<String, String> params = new HashMap<>();

		accountDetails.setAccountId("f4483fda-81be-4873-b4a6-20375b7f55c1");
		accountDetailsList.add(accountDetails);
		accountMapping.setAccountDetails(accountDetailsList);
		
		Mockito.when(utility.isValidPsuId(accountMapping.getPsuId())).thenReturn(false);

		Mockito.when(accountSchedulePaymentRepository.findByAccountNumberAndAccountNSC(anyString(), anyString()))
				.thenThrow(DataAccessResourceFailureException.class);

		Mockito.when(loggerUtils.populateLoggerData(anyString()))
				.thenReturn(AccountSchedulePaymentMongoMockData.getMockLoggerData());

		PlatformAccountSchedulePaymentsResponse actualResponse = accountSchedulePaymentMongoDbAdapterImpl
				.retrieveAccountSchedulePayments(accountMapping, params);

		assertEquals(AccountSchedulePaymentMongoMockData.getMockExpectedAccountSchedulePaymentResponse(),
				actualResponse.getOBReadScheduledPayment3());

	}

	@Test
	public void testRetrieveAccountBeneficiariesDbNullFlow() {
		AccountMapping accountMapping = new AccountMapping();
		ArrayList<AccountDetails> accountDetailsList = new ArrayList<>();
		AccountDetails accountDetails = new AccountDetails();
		Map<String, String> params = new HashMap<>();

		accountDetails.setAccountId("f4483fda-81be-4873-b4a6-20375b7f55c1");
		accountDetailsList.add(accountDetails);
		accountMapping.setAccountDetails(accountDetailsList);
		
		Mockito.when(utility.isValidPsuId(accountMapping.getPsuId())).thenReturn(false);

		Mockito.when(accountSchedulePaymentRepository.findByAccountNumberAndAccountNSC(anyString(), anyString()))
				.thenReturn(null);

		Mockito.when(loggerUtils.populateLoggerData(anyString()))
				.thenReturn(AccountSchedulePaymentMongoMockData.getMockLoggerData());

		PlatformAccountSchedulePaymentsResponse actualResponse = accountSchedulePaymentMongoDbAdapterImpl
				.retrieveAccountSchedulePayments(accountMapping, params);

	}
}
