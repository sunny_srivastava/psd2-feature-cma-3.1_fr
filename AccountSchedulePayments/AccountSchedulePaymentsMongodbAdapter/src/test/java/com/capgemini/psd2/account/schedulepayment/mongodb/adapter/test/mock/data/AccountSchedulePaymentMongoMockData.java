package com.capgemini.psd2.account.schedulepayment.mongodb.adapter.test.mock.data;

import java.util.ArrayList;
import java.util.List;

import com.capgemini.psd2.aisp.domain.OBReadScheduledPayment3;
import com.capgemini.psd2.aisp.domain.OBReadScheduledPayment3Data;
import com.capgemini.psd2.aisp.domain.OBScheduledPayment3;
import com.capgemini.psd2.aisp.mongo.db.adapter.domain.AccountSchedulePaymentsCMA2;
import com.capgemini.psd2.logger.LoggerAttribute;

/**
 * Unit test for simple App.
 */
public class AccountSchedulePaymentMongoMockData {
	
	public static List<AccountSchedulePaymentsCMA2> getMockAccountDetails() {
		return new ArrayList<AccountSchedulePaymentsCMA2>();
	}

	public static LoggerAttribute getMockLoggerData() {
		LoggerAttribute x = new LoggerAttribute();
		x.setApiId("testApiID");
		return x;
	}

	public static OBReadScheduledPayment3 getMockExpectedAccountSchedulePaymentResponse() {
		OBReadScheduledPayment3Data obReadScheduledPayment1Data = new OBReadScheduledPayment3Data();
		OBReadScheduledPayment3 obReadScheduledPayment1 = new OBReadScheduledPayment3();
		AccountSchedulePaymentsCMA2 mockAccountSchedulePayment = new AccountSchedulePaymentsCMA2();

		mockAccountSchedulePayment.setAccountId("f4483fda-81be-4873-b4a6-20375b7f55c1");

		List<OBScheduledPayment3> beneficiaryList = new ArrayList<>();
		beneficiaryList.add(mockAccountSchedulePayment);
		obReadScheduledPayment1Data.setScheduledPayment(beneficiaryList);
		obReadScheduledPayment1.setData(obReadScheduledPayment1Data);
		return obReadScheduledPayment1;
	}
}
