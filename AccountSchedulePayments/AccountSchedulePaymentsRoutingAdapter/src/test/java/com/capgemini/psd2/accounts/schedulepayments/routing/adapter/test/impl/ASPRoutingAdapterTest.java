package com.capgemini.psd2.accounts.schedulepayments.routing.adapter.test.impl;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.anyString;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.account.schedulepayments.routing.adapter.impl.AccountSchedulePaymentsRoutingAdapter;
import com.capgemini.psd2.account.schedulepayments.routing.adapter.routing.AccountSchedulePaymentsAdapterFactory;
import com.capgemini.psd2.accounts.schedulepayments.routing.adapter.test.AccountSchedulePaymentTestRoutingAdapter;
import com.capgemini.psd2.aisp.adapter.AccountSchedulePaymentsAdapter;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountSchedulePaymentsResponse;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.token.Token;

@RunWith(SpringJUnit4ClassRunner.class)
public class ASPRoutingAdapterTest {

	/** The account balance adapter factory. */
	@Mock
	private AccountSchedulePaymentsAdapterFactory accountSchedulePaymentAdapterFactory;

	@Mock
	private RequestHeaderAttributes reqHeaderAtrributes;

	/** The account balance routing adapter. */
	@InjectMocks
	private AccountSchedulePaymentsRoutingAdapter accountSchedulePaymentRoutingAdapter;

	/**
	 * Sets the up.
	 *
	 * @throws Exception
	 *             the exception
	 */
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * Retrieve account schedulePayment Test.
	 */
	@Test
	public void retrieveAccountSchedulePaymentTest() {
		
		Map<String, String> params = new HashMap<>();
		Token token = new Token();
		token.setSeviceParams(params);
		
		AccountSchedulePaymentsAdapter scheduledAdapter = new AccountSchedulePaymentTestRoutingAdapter();
		
		Mockito.when(accountSchedulePaymentAdapterFactory.getAdapterInstance(anyString())).thenReturn(scheduledAdapter);
		Mockito.when(reqHeaderAtrributes.getToken()).thenReturn(token);
		PlatformAccountSchedulePaymentsResponse response = accountSchedulePaymentRoutingAdapter.retrieveAccountSchedulePayments(null, null);
		assertNotNull(response.getOBReadScheduledPayment3().getData());
	}
	

}
