package com.capgemini.psd2.accounts.schedulepayments.routing.adapter.test.mock.data;

import java.util.ArrayList;
import java.util.List;

import com.capgemini.psd2.aisp.domain.OBReadScheduledPayment3;
import com.capgemini.psd2.aisp.domain.OBReadScheduledPayment3Data;
import com.capgemini.psd2.aisp.domain.OBScheduledPayment3;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountSchedulePaymentsResponse;

public class AccountSchedulePaymentMockData {

	public static PlatformAccountSchedulePaymentsResponse getMockSchedulePaymentGETResponse() {
		PlatformAccountSchedulePaymentsResponse platformAccountSchedulePaymentResponse = new PlatformAccountSchedulePaymentsResponse();
		OBReadScheduledPayment3 data = new OBReadScheduledPayment3();
		OBReadScheduledPayment3Data data1 = new OBReadScheduledPayment3Data();
		List<OBScheduledPayment3> payment = new ArrayList<>();
		data1.setScheduledPayment(payment);
		data.setData(data1);
		platformAccountSchedulePaymentResponse.setObReadScheduledPayment3(data);
		return platformAccountSchedulePaymentResponse;
	}
}
