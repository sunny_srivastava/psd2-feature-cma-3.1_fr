package com.capgemini.psd2.accounts.schedulepayments.service.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.capgemini.psd2.accounts.schedulepayments.service.SchedulePaymentService;
import com.capgemini.psd2.aisp.account.mapping.adapter.AccountMappingAdapter;
import com.capgemini.psd2.aisp.adapter.AccountSchedulePaymentsAdapter;
import com.capgemini.psd2.aisp.adapter.AispConsentAdapter;
import com.capgemini.psd2.aisp.domain.Links;
import com.capgemini.psd2.aisp.domain.Meta;
import com.capgemini.psd2.aisp.domain.OBReadScheduledPayment3;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountSchedulePaymentsResponse;
import com.capgemini.psd2.aisp.validation.adapter.AISPCustomValidator;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.consent.domain.AispConsent;
import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;

@Service
public class SchedulePaymentServiceImpl implements SchedulePaymentService {

	/** The req header atrributes. */
	@Autowired
	private RequestHeaderAttributes reqHeaderAtrributes;

	/** The aisp consent adapter. */
	@Autowired
	private AispConsentAdapter aispConsentAdapter;

	/** The account beneficiaries adapter. */
	@Autowired
	@Qualifier("accountSchedulePaymentsRoutingAdapter")
	private AccountSchedulePaymentsAdapter accountSchedulePaymentsAdapter;

	/** The account mapping adapter. */
	@Autowired
	private AccountMappingAdapter accountMappingAdapter;

	@SuppressWarnings("rawtypes")
	@Autowired
	@Qualifier("accountsRoutingValidator")
	private AISPCustomValidator accountsCustomValidator;

	@SuppressWarnings("unchecked")
	@Override
	public OBReadScheduledPayment3 retrieveSchedulePayments(String accountId) {
		//validate accountId
		accountsCustomValidator.validateUniqueId(accountId);
		
		// Get consents by accountId and validate.
		AispConsent aispConsent = aispConsentAdapter.validateAndRetrieveConsentByAccountId(accountId,
				reqHeaderAtrributes.getToken().getConsentTokenData().getConsentId());

		// Get account mapping.
		AccountMapping accountMapping = accountMappingAdapter.retrieveAccountMappingDetails(accountId, aispConsent);

		Map<String,String> params=reqHeaderAtrributes.getToken().getSeviceParams();
		params.put(PSD2Constants.CMAVERSION, aispConsent.getCmaVersion());
		params.put(PSD2Constants.CO_RELATION_ID, reqHeaderAtrributes.getCorrelationId());

		// Retrieve account SCHEDULE PAYMENTS.
		PlatformAccountSchedulePaymentsResponse platformSchedulePaymentsResponse = accountSchedulePaymentsAdapter
				.retrieveAccountSchedulePayments(accountMapping,params);
		
		if (platformSchedulePaymentsResponse == null)
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.NO_RECORD_FOUND_FOR_REQUESTED_ACCT);

		// Get CMA2 schedule payments response.
		OBReadScheduledPayment3 tppSchedulePaymentsResponse = platformSchedulePaymentsResponse
				.getOBReadScheduledPayment3();
		
		if (tppSchedulePaymentsResponse == null)
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.NO_RECORD_FOUND_FOR_REQUESTED_ACCT);

		// update platform response.
		updatePlatformResponse(tppSchedulePaymentsResponse);

		accountsCustomValidator.validateResponseParams(tppSchedulePaymentsResponse);

		return tppSchedulePaymentsResponse;
	}

	private void updatePlatformResponse(OBReadScheduledPayment3 tppSchedulePaymentsResponse) {
		tppSchedulePaymentsResponse.setLinks(new Links());
		
		tppSchedulePaymentsResponse.getLinks().setSelf(reqHeaderAtrributes.getSelfUrl());

		tppSchedulePaymentsResponse.setMeta(new Meta());
		
		tppSchedulePaymentsResponse.getMeta().setTotalPages(1);
	}

}
