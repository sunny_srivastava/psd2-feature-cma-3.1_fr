package com.capgemini.psd2.accounts.schedulepayments.service;

import com.capgemini.psd2.aisp.domain.OBReadScheduledPayment3;

@FunctionalInterface
public interface SchedulePaymentService {

	public OBReadScheduledPayment3 retrieveSchedulePayments(String accountId);
	
}
