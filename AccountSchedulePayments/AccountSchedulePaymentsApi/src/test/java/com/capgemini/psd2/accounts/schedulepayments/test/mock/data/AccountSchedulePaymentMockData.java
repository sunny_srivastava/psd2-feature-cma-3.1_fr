package com.capgemini.psd2.accounts.schedulepayments.test.mock.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.capgemini.psd2.aisp.domain.Links;
import com.capgemini.psd2.aisp.domain.Meta;
import com.capgemini.psd2.aisp.domain.OBReadScheduledPayment3;
import com.capgemini.psd2.aisp.domain.OBReadScheduledPayment3Data;
import com.capgemini.psd2.aisp.domain.OBScheduledPayment3;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountSchedulePaymentsResponse;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.consent.domain.AispConsent;
import com.capgemini.psd2.enums.ConsentStatusEnum;
import com.capgemini.psd2.logger.LoggerAttribute;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.token.ConsentTokenData;
import com.capgemini.psd2.token.Token;

public class AccountSchedulePaymentMockData {

	/** The mock token. */
	public static Token mockToken;

	/**
	 * Gets the token.
	 *
	 * @return the token
	 */
	public static Token getToken() {
		mockToken = new Token();
		ConsentTokenData consentTokenData = new ConsentTokenData();
		consentTokenData.setConsentExpiry("1509348259877L");
		consentTokenData.setConsentId("12345");
		Map<String,String> serviceParams = new HashMap<>();
		mockToken.setSeviceParams(serviceParams);
		mockToken.setConsentTokenData(consentTokenData);
		return mockToken;
	}

	/**
	 * Gets the mock account mapping.
	 *
	 * @return the mock account mapping
	 */
	public static AccountMapping getMockAccountMapping() {
		AccountMapping mapping = new AccountMapping();
		mapping.setTppCID("tpp123");
		mapping.setPsuId("user123");
		List<AccountDetails> selectedAccounts = new ArrayList<>();
		AccountDetails accountRequest = new AccountDetails();
		accountRequest.setAccountId("269c3ff5-d7f8-419b-a3b9-7136c5b4611a");
		accountRequest.setAccountNSC("SC802001");
		accountRequest.setAccountNumber("10203345");
		selectedAccounts.add(accountRequest);
		mapping.setAccountDetails(selectedAccounts);
		return mapping;
	}

	public static AispConsent getMockAispConsent() {
		AispConsent aispConsent = new AispConsent();
		aispConsent.setTppCId("6443e15975554bce8099e35b88b40465");
		aispConsent.setPsuId("88888888");
		aispConsent.setConsentId("39032481");
		aispConsent.setAccountRequestId("ae8c4441-f783-4e80-8810-254241bed98c");
		aispConsent.setChannelId("B365");
		aispConsent.setStartDate("2017-08-09T06:44:31.250Z");
		aispConsent.setEndDate("2018-09-02T00:00:00.875");
		aispConsent.setTransactionFromDateTime("2015-05-03T00:00:00.800");
		aispConsent.setTransactionToDateTime("2018-12-03T00:00:00.345");
		aispConsent.setStatus(ConsentStatusEnum.AUTHORISED);
		aispConsent.setCmaVersion(PSD2Constants.CMA2);
		List<AccountDetails> selectedAispConsentAccountDetails = new ArrayList<>();
		AccountDetails accountRequest = new AccountDetails();

		accountRequest.setAccountId("f4483fda-81be-4873-b4a6-20375b7f55c1");
		accountRequest.setAccountNSC("903779");
		accountRequest.setAccountNumber("76528776");

		selectedAispConsentAccountDetails.add(accountRequest);
		aispConsent.setAccountDetails(selectedAispConsentAccountDetails);

		return aispConsent;
	}

	public static PlatformAccountSchedulePaymentsResponse getMockPlatformReponseWithLinksMeta() {
		PlatformAccountSchedulePaymentsResponse platformAccountSchedulePaymentsResponse = new PlatformAccountSchedulePaymentsResponse();
		OBReadScheduledPayment3Data oBReadScheduledPayment1Data = new OBReadScheduledPayment3Data();
		List<OBScheduledPayment3> obSchedulePaymentList = new ArrayList<>();
		OBScheduledPayment3 oBScheduledPayment1 = new OBScheduledPayment3();
		oBScheduledPayment1.setAccountId("f4483fda-81be-4873-b4a6-20375b7f55c1");
		oBScheduledPayment1.setScheduledPaymentId("TestSchedulePaymentId");
		obSchedulePaymentList.add(oBScheduledPayment1);
		oBReadScheduledPayment1Data.setScheduledPayment(obSchedulePaymentList);

		OBReadScheduledPayment3 obReadScheduledPayment1 = new OBReadScheduledPayment3();
		obReadScheduledPayment1.setData(oBReadScheduledPayment1Data);

		Links links = new Links();
		obReadScheduledPayment1.setLinks(links);
		Meta meta = new Meta();
		obReadScheduledPayment1.setMeta(meta);
		platformAccountSchedulePaymentsResponse.setObReadScheduledPayment3(obReadScheduledPayment1);
		return platformAccountSchedulePaymentsResponse;

	}

	public static Object getMockExpectedAccountSchedulePaymentResponseWithLinksMeta() {
		OBReadScheduledPayment3 oBReadScheduledPayment1 = new OBReadScheduledPayment3();
		OBReadScheduledPayment3Data oBReadScheduledPayment1Data = new OBReadScheduledPayment3Data();
		List<OBScheduledPayment3> obSchedulePaymentList = new ArrayList<>();
		OBScheduledPayment3 obScheduledPayment1 = new OBScheduledPayment3();
		obScheduledPayment1.setAccountId("f4483fda-81be-4873-b4a6-20375b7f55c1");
		obScheduledPayment1.setScheduledPaymentId("TestSchedulePaymentId");
		obSchedulePaymentList.add(obScheduledPayment1);
		oBReadScheduledPayment1Data.setScheduledPayment(obSchedulePaymentList);

		oBReadScheduledPayment1.setData(oBReadScheduledPayment1Data);

		Links links = new Links();
		oBReadScheduledPayment1.setLinks(links);
		Meta meta = new Meta();
		meta.setTotalPages(1);
		oBReadScheduledPayment1.setMeta(meta);

		return oBReadScheduledPayment1;

	}
	
	
	public static LoggerAttribute getMockLoggerData() {
		LoggerAttribute x = new LoggerAttribute();
		x.setApiId("testApiID");
		return x;
	}
	
	
	public static PlatformAccountSchedulePaymentsResponse getMockPlatformReponseWithoutLinksMeta() {
		PlatformAccountSchedulePaymentsResponse platformAccountSchedulePaymentsResponse = new PlatformAccountSchedulePaymentsResponse();
		OBReadScheduledPayment3Data oBReadScheduledPayment1Data = new OBReadScheduledPayment3Data();
		List<OBScheduledPayment3> obSchedulePaymentList = new ArrayList<>();
		OBScheduledPayment3 obScheduledPayment1 = new OBScheduledPayment3();
		obScheduledPayment1.setAccountId("123");
		obScheduledPayment1.setScheduledPaymentId("TestSchedulePaymentId");
		obSchedulePaymentList.add(obScheduledPayment1);
		oBReadScheduledPayment1Data.setScheduledPayment(obSchedulePaymentList);

		OBReadScheduledPayment3 oBReadScheduledPayment1 = new OBReadScheduledPayment3();
		oBReadScheduledPayment1.setData(oBReadScheduledPayment1Data);
		
		platformAccountSchedulePaymentsResponse.setObReadScheduledPayment3(oBReadScheduledPayment1);
		return platformAccountSchedulePaymentsResponse;

	}
	
	public static Object getMockExpectedAccountSchedulePaymentsResponseWithoutLinksMeta() {
		OBReadScheduledPayment3 oBReadScheduledPayment1 = new OBReadScheduledPayment3();
		OBReadScheduledPayment3Data oBReadScheduledPayment1Data = new OBReadScheduledPayment3Data();
		List<OBScheduledPayment3> obSchedulePaymentList = new ArrayList<>();
		OBScheduledPayment3 oBScheduledPayment1 = new OBScheduledPayment3();
		oBScheduledPayment1.setAccountId("123");
		oBScheduledPayment1.setScheduledPaymentId("TestSchedulePaymentId");
		obSchedulePaymentList.add(oBScheduledPayment1);
		oBReadScheduledPayment1Data.setScheduledPayment(obSchedulePaymentList);

		oBReadScheduledPayment1.setData(oBReadScheduledPayment1Data);

		Links links = new Links();
		oBReadScheduledPayment1.setLinks(links);
		Meta meta = new Meta();
		meta.setTotalPages(1);
		oBReadScheduledPayment1.setMeta(meta);

		return oBReadScheduledPayment1;
		
	}

}
