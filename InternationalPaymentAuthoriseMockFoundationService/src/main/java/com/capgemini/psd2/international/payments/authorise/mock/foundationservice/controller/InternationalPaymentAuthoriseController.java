package com.capgemini.psd2.international.payments.authorise.mock.foundationservice.controller;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.psd2.foundationservice.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.foundationservice.exceptions.MockFoundationServiceException;
import com.capgemini.psd2.foundationservice.validator.ValidationUtility;
import com.capgemini.psd2.international.payments.authorise.mock.foundationservice.domain.PaymentInstructionProposalAuthorisingParty;
import com.capgemini.psd2.international.payments.authorise.mock.foundationservice.domain.PaymentInstructionProposalInternational;
import com.capgemini.psd2.international.payments.authorise.mock.foundationservice.service.InternationalPaymentAuthoriseService;

@RestController
@RequestMapping("/group-payments/p/payments-service")
public class InternationalPaymentAuthoriseController {
	/** The account information service. */
	@Autowired
	private InternationalPaymentAuthoriseService internationalPaymentAuthoriseService;

	/** The validation utility. */
	@Autowired
	private ValidationUtility validationUtility;

	/**
	 * Channel A InternationalPaymentConsents information.
	 *
	 * @param paymentInstructionProposalId
	 *            the payment instruction proposal id
	 * @param channelcode
	 *            the channelcode
	 * @param sourceuse
	 *            the sourceuse
	 * @param transactionid
	 *            the transactionid
	 * @param sourcesystem
	 *            the sourcesystem
	 * @param correlationID
	 *            the correlation IDL
	 * @return the accounts
	 * @throws Exception
	 *             the exception
	 */
		// By Amol for International Payment ROI - Post Authorise
	// http://localhost:9051/group-payments/p/payments-service/v1.1/international/payment-instruction-proposals/1111/authorise
	@RequestMapping(value = "/v{version}/international/payment-instruction-proposals/{paymentInstructionProposalId}/authorise", method = RequestMethod.POST,produces =
			MediaType.APPLICATION_JSON_UTF8_VALUE,consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)	
	@ResponseStatus(HttpStatus.OK)
	public PaymentInstructionProposalInternational updateInternationalPaymentAuthorise(
			@RequestBody PaymentInstructionProposalAuthorisingParty paymentInstructionProposalAuthorisingParty,			
			@PathVariable("version") String version,
			@PathVariable("paymentInstructionProposalId") String paymentInstructionProposalId,
			@RequestHeader(required = false, value = "X-API-TRANSACTION-ID") String transactionId,
			@RequestHeader(required = false, value = "X-API-CORRELATION-ID") String correlationId,
			@RequestHeader(required = false, value = "X-API-SOURCE-SYSTEM") String sourceSystem,
			@RequestHeader(required = false, value = "X-API-SOURCE-USER") String sourceUserReqHeader,
			@RequestHeader(required = false, value = "X-API-CHANNEL-CODE") String channelcode,
			@RequestHeader(required = false, value = "X-API-CHANNEL-BRAND") String channelBrand,				
			@RequestHeader(required = false, value = "X-SYSTEM-API-VERSION") String apiVesrion, //need to pass this header from adapter and hard cde its value as 3.0
			@RequestHeader(required = false, value = "X-API-PARTY-SOURCE-ID-NUMBER") String partysourceReqHeader)  throws Exception {		
		if (StringUtils.isBlank(sourceSystem) || StringUtils.isBlank(channelBrand) || 
			StringUtils.isBlank(version) ||  StringUtils.isBlank(paymentInstructionProposalId) ) {
			throw MockFoundationServiceException
					.populateMockFoundationServiceException(ErrorCodeEnum.BAD_REQUEST_PPD_PIP);
		} 		
		return internationalPaymentAuthoriseService.retrieveInternationalPaymentAuthoriseResource(paymentInstructionProposalId);		
	}
}
