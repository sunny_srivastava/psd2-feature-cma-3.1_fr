package com.capgemini.psd2.international.payments.authorise.mock.foundationservice.exception.handler;

/**
 * The Class RecordNotFoundException.
 */
public class RecordNotFoundException extends Exception {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Instantiates a new record not found exception.
	 *
	 * @param s the s
	 */
	public RecordNotFoundException(String s){
		super(s);
		
	}
	
}