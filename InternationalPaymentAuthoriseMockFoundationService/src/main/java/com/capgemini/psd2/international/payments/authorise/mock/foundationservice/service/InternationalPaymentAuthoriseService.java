package com.capgemini.psd2.international.payments.authorise.mock.foundationservice.service;

import org.springframework.http.ResponseEntity;

import com.capgemini.psd2.international.payments.authorise.mock.foundationservice.domain.PaymentInstructionProposalAuthorisingParty;
import com.capgemini.psd2.international.payments.authorise.mock.foundationservice.domain.PaymentInstructionProposalInternational;
import com.capgemini.psd2.international.payments.authorise.mock.foundationservice.exception.handler.RecordNotFoundException;

public interface InternationalPaymentAuthoriseService {
	
	public PaymentInstructionProposalInternational retrieveInternationalPaymentAuthoriseResource(
			String paymentInstructionProposalId) throws RecordNotFoundException;

}
