package com.capgemini.psd2.international.payments.authorise.mock.foundationservice.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.capgemini.psd2.international.payments.authorise.mock.foundationservice.constants.InternationalPaymentAuthoriseFoundationConstants;
import com.capgemini.psd2.international.payments.authorise.mock.foundationservice.domain.PaymentInstructionProposalInternational;
import com.capgemini.psd2.international.payments.authorise.mock.foundationservice.domain.ProposalStatus;
import com.capgemini.psd2.international.payments.authorise.mock.foundationservice.exception.handler.RecordNotFoundException;
import com.capgemini.psd2.international.payments.authorise.mock.foundationservice.repository.InternationalPaymentAuthoriseRepository;
import com.capgemini.psd2.international.payments.authorise.mock.foundationservice.service.InternationalPaymentAuthoriseService;

@Service
public class InternationalPaymentAuthoriseServiceImpl implements InternationalPaymentAuthoriseService {

	@Autowired
	InternationalPaymentAuthoriseRepository internationalPaymentAuthoriseRepository;

	@Override
	public PaymentInstructionProposalInternational retrieveInternationalPaymentAuthoriseResource(
			String paymentInstructionProposalId) throws RecordNotFoundException {
  PaymentInstructionProposalInternational paymentInstructionProposalInternational = internationalPaymentAuthoriseRepository.findByPaymentInstructionProposalId(paymentInstructionProposalId);
		if (null == paymentInstructionProposalInternational) 
			throw new RecordNotFoundException(InternationalPaymentAuthoriseFoundationConstants.RECORD_NOT_FOUND);
		if(paymentInstructionProposalInternational.getProposalStatus().toString().equalsIgnoreCase("AwaitingAuthorisation")) {
			paymentInstructionProposalInternational.setProposalStatus(ProposalStatus.AUTHORISED);
			internationalPaymentAuthoriseRepository.save(paymentInstructionProposalInternational);
		}
        return paymentInstructionProposalInternational;  
}

}