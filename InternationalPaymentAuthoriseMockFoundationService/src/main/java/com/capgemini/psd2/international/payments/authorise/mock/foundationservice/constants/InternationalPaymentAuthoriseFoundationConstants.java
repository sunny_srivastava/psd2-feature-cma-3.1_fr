package com.capgemini.psd2.international.payments.authorise.mock.foundationservice.constants;

public class InternationalPaymentAuthoriseFoundationConstants {	
	public static final String RECORD_NOT_FOUND = "Payment Instruction Proposal Record Not Found";	
}
