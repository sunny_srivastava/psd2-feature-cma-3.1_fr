package com.capgemini.psd2.foundationservice.account.statements.boi.adapter.transformer;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.adapter.datetime.utility.TimeZoneDateTimeAdapter;
import com.capgemini.psd2.aisp.domain.Meta;
import com.capgemini.psd2.aisp.domain.OBExternalStatementType1Code;
import com.capgemini.psd2.aisp.domain.OBReadDataStatement2;
import com.capgemini.psd2.aisp.domain.OBReadStatement2;
import com.capgemini.psd2.aisp.domain.OBStatement2;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountStatementsResponse;
import com.capgemini.psd2.foundationservice.account.statements.boi.adapter.constants.AccountStatementsFoundationServiceConstants;
import com.capgemini.psd2.foundationservice.account.statements.boi.adapter.raml.domain.StatementObject;
import com.capgemini.psd2.foundationservice.account.statements.boi.adapter.raml.domain.Statementsresponse;



@Component
public class AccountStatementsFoundationServiceTransformer {

	@Autowired
	private TimeZoneDateTimeAdapter timeZoneDateTimeAdapter;

	public <T> PlatformAccountStatementsResponse transformAccountStatementRes(T inputBalanceObj,
			Map<String, String> params) {
		PlatformAccountStatementsResponse platformAccountStatementsResponse = new PlatformAccountStatementsResponse();
		OBReadStatement2 oBReadStatement2 = new OBReadStatement2();
		if (params.get(AccountStatementsFoundationServiceConstants.ACCOUNT_TYPE)
				.equals(AccountStatementsFoundationServiceConstants.CREDIT_CARD)) {

			Statementsresponse statementsResponse = (Statementsresponse) inputBalanceObj;
			List<OBStatement2> statementList = new ArrayList<>();

			for (StatementObject statement : statementsResponse.getStatementsList()) {
				
				String creationDateStr = timeZoneDateTimeAdapter.parseDateCMA(statement.getDocumentIssuanceDate());
				
				OBStatement2 obStatement2 = new OBStatement2();
				obStatement2.setAccountId(params.get(AccountStatementsFoundationServiceConstants.ACCOUNT_ID));
				obStatement2.setStatementId(statement.getDocumentIdentificationNumber());
				if (statement.getStatementReasonDescription() != null) {

					String type = statement.getStatementReasonDescription().toString();
					switch (type) {
					case AccountStatementsFoundationServiceConstants.ANNUAL_SUMMARY_STMT:
						obStatement2.setType(OBExternalStatementType1Code.ANNUAL);
						Date date = yearMinus(statement.getDocumentIssuanceDate());
						obStatement2.setStartDateTime(timeZoneDateTimeAdapter.parseDateCMA(date));
						break;
					case AccountStatementsFoundationServiceConstants.REGULAR_MON_STMT:
						obStatement2.setType(OBExternalStatementType1Code.REGULARPERIODIC);
						Date date1 = monthMinus(statement.getDocumentIssuanceDate());
						obStatement2.setStartDateTime(timeZoneDateTimeAdapter.parseDateCMA(date1));
						break;
					case AccountStatementsFoundationServiceConstants.COMM_SUMMARY_STMT:
						obStatement2.setType(OBExternalStatementType1Code.REGULARPERIODIC);
						Date date2 = yearMinus(statement.getDocumentIssuanceDate());
						obStatement2.setStartDateTime(timeZoneDateTimeAdapter.parseDateCMA(date2));
						break;
					case AccountStatementsFoundationServiceConstants.COMM_MONTHLY_STMT:
						obStatement2.setType(OBExternalStatementType1Code.REGULARPERIODIC);
						Date date3 = monthMinus(statement.getDocumentIssuanceDate());
						obStatement2.setStartDateTime(timeZoneDateTimeAdapter.parseDateCMA(date3));
						break;
					default:
						obStatement2.setType(OBExternalStatementType1Code.REGULARPERIODIC);
						obStatement2.setStartDateTime(creationDateStr);
					}
				}

				obStatement2.setEndDateTime(creationDateStr);
				obStatement2.setCreationDateTime(creationDateStr);

				statementList.add(obStatement2);
			}
			OBReadDataStatement2 obReadStatement2Data = new OBReadDataStatement2();

			obReadStatement2Data.setStatement(statementList);
			oBReadStatement2 = new OBReadStatement2();
			oBReadStatement2.setData(obReadStatement2Data);

			if (null == oBReadStatement2.getMeta()) {
				oBReadStatement2.setMeta(new Meta().totalPages(1));
			}

			platformAccountStatementsResponse.setoBReadStatement2(oBReadStatement2);
		}

		else {
			Statementsresponse statementsResponse = (Statementsresponse) inputBalanceObj;
			List<OBStatement2> statementList = new ArrayList<>();

			for (StatementObject statement : statementsResponse.getStatementsList()) {
				
				String creationDateStr = timeZoneDateTimeAdapter.parseDateCMA(statement.getDocumentIssuanceDate());

				OBStatement2 obStatement2 = new OBStatement2();
				obStatement2.setAccountId(params.get(AccountStatementsFoundationServiceConstants.ACCOUNT_ID));
				obStatement2.setStatementId(statement.getDocumentIdentificationNumber());

				if (statement.getStatementReasonDescription() != null) {
					String type = statement.getStatementReasonDescription().toString();
					switch (type) {
					case AccountStatementsFoundationServiceConstants.ANNUAL_SUMMARY_STMT:
						obStatement2.setType(OBExternalStatementType1Code.ANNUAL);
						Date date = yearMinus(statement.getDocumentIssuanceDate());
						obStatement2.setStartDateTime(timeZoneDateTimeAdapter.parseDateCMA(date));
						break;
					case AccountStatementsFoundationServiceConstants.REGULAR_MON_STMT:
						obStatement2.setType(OBExternalStatementType1Code.REGULARPERIODIC);
						Date date1 = monthMinus(statement.getDocumentIssuanceDate());
						obStatement2.setStartDateTime(timeZoneDateTimeAdapter.parseDateCMA(date1));
						break;
					case AccountStatementsFoundationServiceConstants.COMM_SUMMARY_STMT:
						obStatement2.setType(OBExternalStatementType1Code.REGULARPERIODIC);
						Date date2 = yearMinus(statement.getDocumentIssuanceDate());
						obStatement2.setStartDateTime(timeZoneDateTimeAdapter.parseDateCMA(date2));
						break;
					case AccountStatementsFoundationServiceConstants.COMM_MONTHLY_STMT:
						obStatement2.setType(OBExternalStatementType1Code.REGULARPERIODIC);
						Date date3 = monthMinus(statement.getDocumentIssuanceDate());
						obStatement2.setStartDateTime(timeZoneDateTimeAdapter.parseDateCMA(date3));
						break;
					default:
						obStatement2.setType(OBExternalStatementType1Code.REGULARPERIODIC);
						obStatement2.setStartDateTime(creationDateStr);
					}
				}
				obStatement2.setEndDateTime(creationDateStr);
				obStatement2.setCreationDateTime(creationDateStr);

				statementList.add(obStatement2);
			}
			OBReadDataStatement2  obReadStatement2Data = new OBReadDataStatement2 ();
			obReadStatement2Data.setStatement(statementList);
			oBReadStatement2 = new OBReadStatement2();
			oBReadStatement2.setData(obReadStatement2Data);

			if (null == oBReadStatement2.getMeta()) {
				oBReadStatement2.setMeta(new Meta().totalPages(1));
			}

			platformAccountStatementsResponse.setoBReadStatement2(oBReadStatement2);

		}
		return platformAccountStatementsResponse;

	}

	private Date monthMinus(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.MONTH, -1);
		return cal.getTime();
	}

	private Date yearMinus(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.YEAR, -1);
		return cal.getTime();
	}
}
