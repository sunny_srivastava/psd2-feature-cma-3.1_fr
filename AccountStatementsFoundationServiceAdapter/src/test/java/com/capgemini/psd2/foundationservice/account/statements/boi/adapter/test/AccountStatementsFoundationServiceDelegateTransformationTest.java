package com.capgemini.psd2.foundationservice.account.statements.boi.adapter.test;

import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import com.capgemini.psd2.adapter.datetime.utility.TimeZoneDateTimeAdapter;
import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.aisp.domain.OBExternalAccountSubType1Code;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.foundationservice.account.statements.boi.adapter.constants.AccountStatementsFoundationServiceConstants;
import com.capgemini.psd2.foundationservice.account.statements.boi.adapter.delegate.AccountStatementsFoundationServiceDelegate;
import com.capgemini.psd2.foundationservice.account.statements.boi.adapter.transformer.AccountStatementsFoundationServiceTransformer;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.impl.RestClientSyncImpl;
import com.capgemini.psd2.validator.PSD2Validator;

@RunWith(SpringJUnit4ClassRunner.class)
public class AccountStatementsFoundationServiceDelegateTransformationTest {
	
	/** The account statements file foundation service adapter. */
	@InjectMocks
	private AccountStatementsFoundationServiceDelegate delegate;
	
	
	/** The account statements file foundation service client. */
	@InjectMocks
	private AccountStatementsFoundationServiceTransformer accountStatementsFileFoundationServiceTransformer = new AccountStatementsFoundationServiceTransformer();
	
	/** The rest client. */
	@Mock
	private RestClientSyncImpl restClient;
	
	/** The psd 2 validator. */
	@Mock
	private PSD2Validator psd2Validator;
	
	@Mock
	private TimeZoneDateTimeAdapter timeZoneDateTimeAdapter;
	
	

	/**
	 * Sets the up.
	 */
	@Before
	public void setUp(){
		MockitoAnnotations.initMocks(this);
	}
	
	/**
	 * Context loads.
	 */
	@Test
	public void contextLoads() {
	}

	/**
	 * Test account statements file FS.
	 */
	
	@Test
	public void testCreateRequestHeadersFile(){
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
		AccountMapping accountMapping = new AccountMapping();
		Map<String , String> params = new HashMap<String ,String>();
		AccountDetails accDet = new AccountDetails();
		RequestInfo requestInfo = new RequestInfo();
		HttpHeaders httpHeaders = new HttpHeaders();
		ReflectionTestUtils.setField(delegate, "sourceUserReqHeader", "X-API-SOURCE-USER");
		ReflectionTestUtils.setField(delegate, "sourceSystemReqHeader", "X-API-SOURCE-SYSTEM");
		ReflectionTestUtils.setField(delegate, "maskedPan", "X-MASKED-PAN");
		ReflectionTestUtils.setField(delegate, "correlationIdInReqHeader", "X-API-TRANSACTION-ID");
		ReflectionTestUtils.setField(delegate, "channelInReqHeader", "X-API-CHANNEL-CODE");
		ReflectionTestUtils.setField(delegate, "singleAccountStatementsFileBaseURL", "X-API-CHANNEL-CODE");
		ReflectionTestUtils.setField(delegate, "singleAccountStatementsCreditCardFileBaseURL", "X-API-CHANNEL-CODE");
		accDet.setAccountSubType(OBExternalAccountSubType1Code.CREDITCARD);
		accDet.setAccountId("12345");
		accDet.setAccountNSC("nsc1234");
		accDet.setAccountNumber("acct1234");
		accDetList.add(accDet);
		accountMapping.setAccountDetails(accDetList);
		accountMapping.setTppCID("test");
		accountMapping.setPsuId("test");
		accountMapping.setCorrelationId("tgkty5785");
		params.put("channelId","channel123");
		params.put(AccountStatementsFoundationServiceConstants.ACCOUNT_TYPE, "CreditCard");
		params.put(AccountStatementsFoundationServiceConstants.ACCOUNT_NUMBER,"accountNumber" );
		httpHeaders=delegate.createRequestHeadersFile(accountMapping, params);
		assertNotNull(httpHeaders);
		
	}
	
	@Test
	public void testcreateRequestHeadersStmt(){
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
		AccountMapping accountMapping = new AccountMapping();
		Map<String , String> params = new HashMap<String ,String>();
		AccountDetails accDet = new AccountDetails();
		RequestInfo requestInfo = new RequestInfo();
		HttpHeaders httpHeaders = new HttpHeaders();
		ReflectionTestUtils.setField(delegate, "sourceUserReqHeader", "X-API-SOURCE-USER");
		ReflectionTestUtils.setField(delegate, "sourceSystemReqHeader", "X-API-SOURCE-SYSTEM");
		ReflectionTestUtils.setField(delegate, "maskedPan", "X-MASKED-PAN");
		ReflectionTestUtils.setField(delegate, "correlationIdInReqHeader", "X-API-TRANSACTION-ID");
		ReflectionTestUtils.setField(delegate, "channelInReqHeader", "X-API-CHANNEL-CODE");
		accDet.setAccountSubType(OBExternalAccountSubType1Code.CREDITCARD);
		accDet.setAccountId("12345");
		accDet.setAccountNSC("nsc1234");
		accDet.setAccountNumber("acct1234");
		accDetList.add(accDet);
		accountMapping.setAccountDetails(accDetList);
		accountMapping.setTppCID("test");
		accountMapping.setPsuId("test");
		accountMapping.setCorrelationId("tgkty5785");
		params.put("channelId","channel123");
		params.put(AccountStatementsFoundationServiceConstants.ACCOUNT_TYPE, "CreditCard");
		params.put(AccountStatementsFoundationServiceConstants.ACCOUNT_NUMBER,"accountNumber" );
		httpHeaders=delegate.createRequestHeadersStmt( accountMapping, params);
		assertNotNull(httpHeaders);
		
	}
	
	@Test(expected=AdapterException.class)
	public void testgetFoundationServiceCardURLFile(){
		String singleAccountStatementsCreditCardFileBaseURL=null;
		Map<String , String> params = new HashMap<String ,String>();
		params.put(AccountStatementsFoundationServiceConstants.ACCOUNT_NUMBER, "accountNumber");
		params.put(AccountStatementsFoundationServiceConstants.STATEMENT_ID, "statementId");
		delegate.getFoundationServiceCardURLFile(params, singleAccountStatementsCreditCardFileBaseURL);
	}
	
	@Test
	public void testgetFoundationServiceCardURLFile1(){
		String singleAccountStatementsCreditCardFileBaseURL="/dydfyrf";
		Map<String , String> params = new HashMap<String ,String>();
		params.put(AccountStatementsFoundationServiceConstants.ACCOUNT_NUMBER, "accountNumber");
		params.put(AccountStatementsFoundationServiceConstants.STATEMENT_ID, "statementId");
		delegate.getFoundationServiceCardURLFile(params, singleAccountStatementsCreditCardFileBaseURL);
	}
	
	@Test(expected=AdapterException.class)
	public void testgetFoundationServiceCardURLFile2(){
		String singleAccountStatementsCreditCardFileBaseURL="/dydfyrf";
		Map<String , String> params = new HashMap<String ,String>();
		params.put(AccountStatementsFoundationServiceConstants.ACCOUNT_NUMBER, null);
		params.put(AccountStatementsFoundationServiceConstants.STATEMENT_ID, "statementId");
		delegate.getFoundationServiceCardURLFile(params, singleAccountStatementsCreditCardFileBaseURL);
	}
	
	@Test(expected=AdapterException.class)
	public void testgetFoundationServiceCardURLFile3(){
		String singleAccountStatementsCreditCardFileBaseURL="/dydfyrf";
		Map<String , String> params = new HashMap<String ,String>();
		params.put(AccountStatementsFoundationServiceConstants.ACCOUNT_NUMBER, "accountNumber");
		params.put(AccountStatementsFoundationServiceConstants.STATEMENT_ID, null);
		delegate.getFoundationServiceCardURLFile(params, singleAccountStatementsCreditCardFileBaseURL);
	}
	
	
	@Test(expected=AdapterException.class)
	public void testgetFoundationServiceURLFile(){
		String singleAccountStatementsFileBaseURL=null;
		Map<String , String> params = new HashMap<String ,String>();
		params.put(AccountStatementsFoundationServiceConstants.ACCOUNT_NUMBER, "accountNumber");
		params.put(AccountStatementsFoundationServiceConstants.ACCOUNT_NSC, "accountNSC");
		params.put(AccountStatementsFoundationServiceConstants.STATEMENT_ID, "statementId");
		delegate.getFoundationServiceURLFile(params, singleAccountStatementsFileBaseURL);
	}
	
	@Test
	public void testgetFoundationServiceURLFile1(){
		String singleAccountStatementsFileBaseURL="/fujgi";
		Map<String , String> params = new HashMap<String ,String>();
		params.put(AccountStatementsFoundationServiceConstants.ACCOUNT_NUMBER, "accountNumber");
		params.put(AccountStatementsFoundationServiceConstants.ACCOUNT_NSC, "accountNSC");
		params.put(AccountStatementsFoundationServiceConstants.STATEMENT_ID, "statementId");
		delegate.getFoundationServiceURLFile(params, singleAccountStatementsFileBaseURL);
	}
	
	
	@Test(expected=AdapterException.class)
	public void testgetFoundationServiceURLFile2(){
		String singleAccountStatementsFileBaseURL="/fujgi";
		Map<String , String> params = new HashMap<String ,String>();
		params.put(AccountStatementsFoundationServiceConstants.ACCOUNT_NUMBER, null);
		params.put(AccountStatementsFoundationServiceConstants.ACCOUNT_NSC, "accountNSC");
		params.put(AccountStatementsFoundationServiceConstants.STATEMENT_ID, "statementId");
		delegate.getFoundationServiceURLFile(params, singleAccountStatementsFileBaseURL);
	}
	
	@Test(expected=AdapterException.class)
	public void testgetFoundationServiceURLFile3(){
		String singleAccountStatementsFileBaseURL="/fujgi";
		Map<String , String> params = new HashMap<String ,String>();
		params.put(AccountStatementsFoundationServiceConstants.ACCOUNT_NUMBER, "accountNumber");
		params.put(AccountStatementsFoundationServiceConstants.ACCOUNT_NSC, null);
		params.put(AccountStatementsFoundationServiceConstants.STATEMENT_ID, "statementId");
		delegate.getFoundationServiceURLFile(params, singleAccountStatementsFileBaseURL);
	}
	
	@Test(expected=AdapterException.class)
	public void testgetFoundationServiceURLFile4(){
		String singleAccountStatementsFileBaseURL="/fujgi";
		Map<String , String> params = new HashMap<String ,String>();
		params.put(AccountStatementsFoundationServiceConstants.ACCOUNT_NUMBER, "accountNumber");
		params.put(AccountStatementsFoundationServiceConstants.ACCOUNT_NSC, "accountNSC");
		params.put(AccountStatementsFoundationServiceConstants.STATEMENT_ID, null);
		delegate.getFoundationServiceURLFile(params, singleAccountStatementsFileBaseURL);
	}
	
	@Test
	public void testgetFoundationServiceURL(){
		String accountNSC="nsc1234";
		String accountNumber="5858";
		String baseURL="/vjmb";
		delegate.getFoundationServiceURL(accountNSC, accountNumber, baseURL);
	}
	
	@Test(expected=AdapterException.class)
	public void testgetFoundationServiceURL1(){
		String accountNSC=null;
		String accountNumber="5858";
		String baseURL="/vjmb";
		delegate.getFoundationServiceURL(accountNSC, accountNumber, baseURL);
	}
	
	@Test(expected=AdapterException.class)
	public void testgetFoundationServiceURL2(){
		String accountNSC="nsc1234";
		String accountNumber=null;
		String baseURL="/vjmb";
		delegate.getFoundationServiceURL(accountNSC, accountNumber, baseURL);
	}
	
	@Test(expected=AdapterException.class)
	public void testgetFoundationServiceURL3(){
		String accountNSC="nsc1234";
		String accountNumber="5858";
		String baseURL=null;
		delegate.getFoundationServiceURL(accountNSC, accountNumber, baseURL);
	}
	
	@Test
	public void testgetFoundationServiceCreditcardURL(){
		String baseURL="/vjmb";	
		Map<String , String> params = new HashMap<String ,String>();
		params.put(AccountStatementsFoundationServiceConstants.ACCOUNT_NUMBER, "accountNumber");
		String accountNumber = "12345";
		delegate.getFoundationServiceCreditcardURL(accountNumber, baseURL);
	}
	
	@Test
	public void testgetFoundationServiceCreditcardURL1(){
		String baseURL="/vjmb";	
		Map<String , String> params = new HashMap<String ,String>();
		params.put(AccountStatementsFoundationServiceConstants.ACCOUNT_NUMBER, null);
		String accountNumber = "12345";
		delegate.getFoundationServiceCreditcardURL(accountNumber, baseURL);
	}
	
	
	@Test(expected=AdapterException.class)
	public void testgetFoundationServiceCreditcardURL2(){
		String baseURL=null;	
		Map<String , String> params = new HashMap<String ,String>();
		params.put(AccountStatementsFoundationServiceConstants.ACCOUNT_NUMBER, "accountNumber");
		String accountNumber = "12345";
		delegate.getFoundationServiceCreditcardURL(accountNumber, baseURL);
	}
	
	@Test
	public void testgetFoundationServiceCreditcardURL3(){
		MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<>();
		Map<String , String> params = new HashMap<String ,String>();
		params.put(AccountStatementsFoundationServiceConstants.REQUESTED_FROM_DATETIME,"22/2/2019");
		params.put(AccountStatementsFoundationServiceConstants.REQUESTED_TO_DATETIME,"22/3/2019");
		delegate.getFromAndToDate(params);
	}
	
	@Test
	public void testgetFoundationServiceCreditcardURL4(){
		MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<>();
		Map<String , String> params = new HashMap<String ,String>();
		params.put(AccountStatementsFoundationServiceConstants.REQUESTED_FROM_CONSENT_DATETIME,"22/2/2019");
		params.put(AccountStatementsFoundationServiceConstants.REQUESTED_TO_CONSENT_DATETIME,"22/3/2019");
		delegate.getFromAndToDate(params);
	}
	
}
