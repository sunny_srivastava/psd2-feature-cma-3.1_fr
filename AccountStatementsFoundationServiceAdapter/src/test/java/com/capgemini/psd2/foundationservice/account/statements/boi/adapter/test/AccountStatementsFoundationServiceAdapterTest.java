//
//package com.capgemini.psd2.foundationservice.account.statements.boi.adapter.test;
//
//import static org.hamcrest.CoreMatchers.any;
//import static org.junit.Assert.assertNotNull;
//import static org.mockito.Matchers.any;
//
//import java.util.ArrayList;
//import java.util.Date;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.mockito.InjectMocks;
//import org.mockito.Mock;
//import org.mockito.Mockito;
//import org.mockito.MockitoAnnotations;
//import org.springframework.http.HttpHeaders;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//import org.springframework.util.LinkedMultiValueMap;
//import org.springframework.util.MultiValueMap;
//
//import com.capgemini.psd2.adapter.datetime.utility.StatementDateRange;
//import com.capgemini.psd2.adapter.datetime.utility.TransactionDateRange;
//import com.capgemini.psd2.adapter.exceptions.AdapterException;
//import com.capgemini.psd2.aisp.domain.OBAccount2;
//import com.capgemini.psd2.aisp.domain.OBCashAccount3;
//import com.capgemini.psd2.aisp.domain.OBExternalAccountSubType1Code;
//import com.capgemini.psd2.aisp.platform.domain.PlatformAccountSatementsFileResponse;
//import com.capgemini.psd2.aisp.platform.domain.PlatformAccountStatementsResponse;
//import com.capgemini.psd2.aisp.platform.domain.PlatformAccountTransactionResponse;
//import com.capgemini.psd2.consent.domain.AccountDetails;
//import com.capgemini.psd2.consent.domain.AccountMapping;
//import com.capgemini.psd2.foundationservice.account.statements.boi.adapter.AccountStatementsFoundationServiceAdapter;
//import com.capgemini.psd2.foundationservice.account.statements.boi.adapter.client.AccountStatementsFoundationServiceClient;
//import com.capgemini.psd2.foundationservice.account.statements.boi.adapter.constants.AccountStatementsFoundationServiceConstants;
//import com.capgemini.psd2.foundationservice.account.statements.boi.adapter.delegate.AccountStatementsFoundationServiceDelegate;
//import com.capgemini.psd2.foundationservice.account.statements.boi.adapter.raml.domain.StatementObject;
//import com.capgemini.psd2.foundationservice.account.statements.boi.adapter.raml.domain.StatementReasonDescription;
//import com.capgemini.psd2.foundationservice.account.statements.boi.adapter.raml.domain.Statementsresponse;
//import com.capgemini.psd2.rest.client.model.RequestInfo;
//import com.capgemini.psd2.rest.client.sync.impl.RestClientSyncImpl;
//
//@RunWith(SpringJUnit4ClassRunner.class)
//public class AccountStatementsFoundationServiceAdapterTest {
//
//
///** The account statements file foundation service adapter. */
//@InjectMocks
//private AccountStatementsFoundationServiceAdapter accountStatementsFileFoundationServiceAdapter = new AccountStatementsFoundationServiceAdapter();
//
//
///** The account statements file foundation service client. *//*
//* 
//*/
//@Mock
//private AccountStatementsFoundationServiceClient accountStatementsFileFoundationServiceClient;
//
///** The rest client. */
//@Mock
//private RestClientSyncImpl restClient;
//
///** The account statements file foundation service delegate. */
//@Mock
//private AccountStatementsFoundationServiceDelegate accountStatementsFileFoundationServiceDelegate;
//
//
///**
//* Sets the up.
//*/
//@Before
//public void setUp(){
//MockitoAnnotations.initMocks(this);
//}
//
///**
//* Context loads.
//*/
//@Test
//public void contextLoads() {
//}
//
///**
//* Test account statements file FS.
//*/
//
//private AccountMapping accountMapping = new AccountMapping();
//private List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
//private AccountDetails accDet = new AccountDetails();
//private Map<String, String> params = new HashMap<String, String>();
//private StatementDateRange statementDateRange = new StatementDateRange();
//
//@Test(expected=AdapterException.class)
//public void testAccountTransactionsFSAccountIdNull(){
//	accDet.setAccountId(null);
//	accDetList.add(accDet);
//	accountMapping.setAccountDetails(accDetList);
//	accountStatementsFileFoundationServiceAdapter.retrieveAccountStatements(accountMapping, params);
//	
////	accDet.setAccountId("1234");
//}
//
////@Test
////public void testDecision(){
////	statementDateRange.setEmptyResponse(true);
////    PlatformAccountStatementsResponse res = accountStatementsFileFoundationServiceAdapter.retrieveAccountStatements(accountMapping, params);
////	assertNotNull(res);
////	
////}
//
//@Test(expected=AdapterException.class)
//public void testAccountStatementFileFSIsNullAccountMapping()
//{
//Map<String , String> params = new HashMap<String ,String>();
//params.put("a", "b");
//accountStatementsFileFoundationServiceAdapter.downloadStatementFileByStatementsId(null, params);
//} 
//
//@Test(expected=AdapterException.class)
//public void testAccountStatementFileFSpsuIdIsNull()
//{
//AccountMapping accountMapping = new AccountMapping();
//accountMapping.setPsuId(null);
//Map<String , String> params = new HashMap<String ,String>();
//params.put("a", "b");
//accountStatementsFileFoundationServiceAdapter.downloadStatementFileByStatementsId(accountMapping, params);
//}
//
//@Test(expected=AdapterException.class)
//public void testAccountStatementFileFSParamsIsNull()
//{
//AccountMapping accountMapping = new AccountMapping();
//List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
//AccountDetails accDet = new AccountDetails();
//accountMapping.setAccountDetails(accDetList);
//accDet.setAccountId("12345");
//accDet.setAccountNSC("nsc1234");
//accDet.setAccountNumber("acct1234");
//accountMapping.setTppCID("test");
//accountMapping.setCorrelationId("test");
//accountMapping.setPsuId("test");
//accountStatementsFileFoundationServiceAdapter.downloadStatementFileByStatementsId(accountMapping, null);
//}
//
//
//@Test(expected=AdapterException.class)
//public void testAccountStatementFileFSforCurrentAccount()
//{
//List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
//AccountMapping accountMapping = new AccountMapping();
//AccountDetails accDet = new AccountDetails();
//accDet.setAccountSubType(OBExternalAccountSubType1Code.CURRENTACCOUNT);
//accDet.setAccountId("12345");
//accDet.setAccountNSC("nsc1234");
//accDet.setAccountNumber("acct1234");
//accDetList.add(accDet);
//accountMapping.setAccountDetails(accDetList);
//accountMapping.setTppCID("test");
//accountMapping.setCorrelationId("test");
//accountMapping.setPsuId("test");
//Map<String , String> params = new HashMap<String ,String>();
//accountStatementsFileFoundationServiceAdapter.downloadStatementFileByStatementsId(accountMapping, params);
//}
//
//
//@Test(expected=AdapterException.class)
//public void testAccountStatementFileFSforCurrentAccountException()
//{
//List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
//AccountMapping accountMapping = new AccountMapping();
//accDetList.add(null);
//accountMapping.setAccountDetails(null);
//accountMapping.setTppCID("test");
//accountMapping.setCorrelationId("test");
//accountMapping.setPsuId("test");
//Map<String , String> params = new HashMap<String ,String>();
//accountStatementsFileFoundationServiceAdapter.downloadStatementFileByStatementsId(accountMapping, params);
//}
//
//
//
//@Test(expected=AdapterException.class)
//public void testAccountStatementFileFSforCreditCard()
//{
//List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
//AccountMapping accountMapping = new AccountMapping();
//Map<String , String> params = new HashMap<String ,String>();
//AccountDetails accDet = new AccountDetails();
//OBAccount2 oba = new OBAccount2();
//OBCashAccount3 oBCashAccount3 = new OBCashAccount3();
//List<OBCashAccount3> account = new ArrayList<OBCashAccount3>();
//RequestInfo requestInfo = new RequestInfo();
//
//accDet.setAccountSubType(OBExternalAccountSubType1Code.CREDITCARD);
//accDet.setAccountId("12345");
//accDet.setAccountNSC("nsc1234");
//accDet.setAccountNumber("acct1234");
//oBCashAccount3.setIdentification("abcudeudfu");
//account.add(oBCashAccount3);
//oba.setAccount(account);
//accDet.setAccount(oBCashAccount3);
//accDetList.add(accDet);
//accountMapping.setAccountDetails(accDetList);
//accountMapping.setTppCID("test");
//accountMapping.setCorrelationId("test");
//accountMapping.setPsuId("test");
//params.put("channelId","channel123");
//params.put(AccountStatementsFoundationServiceConstants.ACCOUNT_TYPE, "CreditCard");
//params.put(AccountStatementsFoundationServiceConstants.ACCOUNT_NUMBER,"accountNumber" );
//HttpHeaders httpHeaders = new HttpHeaders();
//httpHeaders = accountStatementsFileFoundationServiceDelegate.createRequestHeadersFile(requestInfo,
//accountMapping, params);
//String singleAccountStatementsCreditCardFileBaseURL="fuyfg";
//accountStatementsFileFoundationServiceDelegate.getFoundationServiceCardURLFile(params, singleAccountStatementsCreditCardFileBaseURL);
//accountStatementsFileFoundationServiceAdapter.downloadStatementFileByStatementsId(accountMapping, params);
//}
//
//
//@Test(expected=AdapterException.class)
//public void testAccountStatementFileFSIsNullAccountMappingforretrieveAccountStatements()
//{
//Map<String , String> params = new HashMap<String ,String>();
//params.put("a", "b");
//accountStatementsFileFoundationServiceAdapter.retrieveAccountStatements(null, params);
//} 
//
//@Test(expected=AdapterException.class)
//public void testAccountStatementFileFSpsuIdIsNullforretrieveAccountStatements()
//{
//AccountMapping accountMapping = new AccountMapping();
//accountMapping.setPsuId(null);
//Map<String , String> params = new HashMap<String ,String>();
//params.put("a", "b");
//accountStatementsFileFoundationServiceAdapter.retrieveAccountStatements(accountMapping, params);
//}
//public void testAccountStatementFileFSforforretrieveAccountStatementsException()
//{
//List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
//AccountMapping accountMapping = new AccountMapping();
//accDetList.add(null);
//accountMapping.setAccountDetails(null);
//accountMapping.setTppCID("test");
//accountMapping.setCorrelationId("test");
//accountMapping.setPsuId("test");
//Map<String , String> params = new HashMap<String ,String>();
//accountStatementsFileFoundationServiceAdapter.retrieveAccountStatements(accountMapping, params);
//}
//
//@Test(expected=AdapterException.class)
//public void testAccountStatementFileFSParamsIsNullforretrieveAccountStatements()
//{
//AccountMapping accountMapping = new AccountMapping();
//List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
//AccountDetails accDet = new AccountDetails();
//accountMapping.setAccountDetails(accDetList);
//accDet.setAccountId("12345");
//accDet.setAccountNSC("nsc1234");
//accDet.setAccountNumber("acct1234");
//accountMapping.setTppCID("test");
//accountMapping.setCorrelationId("test");
//accountMapping.setPsuId("test");
//accountStatementsFileFoundationServiceAdapter.retrieveAccountStatements(accountMapping, null);
//}
//@Test
//public void testAccountStatementFileFSforCreditCardforretrieveAccountStatements()
//{
//List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
//AccountMapping accountMapping = new AccountMapping();
//MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<String ,String>();
//Map<String , String> params = new HashMap<String ,String>();
////     MultiValueMap<String, String> queryParams = accountStatementsFileFoundationServiceDelegate.getFromAndToDate(params);
//OBCashAccount3 oBCashAccount3 = new OBCashAccount3();
//List<OBCashAccount3> account = new ArrayList<OBCashAccount3>();
//AccountDetails accDet = new AccountDetails();
//OBAccount2 oba = new OBAccount2();
//Statementsresponse statements = new Statementsresponse();
//RequestInfo requestInfo = new RequestInfo();
//HttpHeaders httpHeaders = new HttpHeaders();
//StatementObject statement=new StatementObject();
//statement.setDocumentIdentificationNumber("12345");
//Date documentIssuanceDate= new Date();
//statement.setDocumentIssuanceDate(documentIssuanceDate);
//statement.setStatementEndDate(documentIssuanceDate);
//statement.setStatementReasonDescription(StatementReasonDescription.ACCOUNT_STATEMENT);
//statement.setStatementStartDate(documentIssuanceDate);
//List<StatementObject> list= new ArrayList<StatementObject>();
//list.add(statement);
//statements.setStatementsList(list);
//accDet.setAccountSubType(OBExternalAccountSubType1Code.CREDITCARD);
//accDet.setAccountId("12345");
//accDet.setAccountNSC("nsc1234");
//accDet.setAccountNumber("acct1234");
//oBCashAccount3.setIdentification("abcudeudfu");
//account.add(oBCashAccount3);
//oba.setAccount(account);
//accDet.setAccount(oBCashAccount3);
//accDetList.add(accDet);
//accountMapping.setAccountDetails(accDetList);
//accountMapping.setTppCID("test");
//accountMapping.setCorrelationId("test");
//accountMapping.setPsuId("test");
//queryParams.add("channelId","channel123");
//queryParams.add(AccountStatementsFoundationServiceConstants.ACCOUNT_TYPE, "CreditCard");
//queryParams.add(AccountStatementsFoundationServiceConstants.ACCOUNT_NUMBER,"accountNumber" );
//httpHeaders = accountStatementsFileFoundationServiceDelegate.createRequestHeadersFile(requestInfo,
//accountMapping, params);
//String singleAccountStatementsFileBaseStmtURL="/fuyfg";
//String accountNSC = "nsc1234";
//String accountNumber= "acct1234";
//String baseURL = "/fuyfg";
//StatementDateRange decision=new StatementDateRange();
//decision.setConsentExpiryDateTime(new Date(2019-01-01));
//decision.setFilterFromDate(new Date(2019-01-01));
//decision.setFilterToDate (new Date(2019-01-01));
//decision.setTransactionFromDateTime(new Date(2019-01-01));
//decision.setTransactionToDateTime(new Date(2019-01-01));
//decision.setNewFilterToDate(new Date(2019-01-01));
//decision.setNewFilterFromDate(new Date(2019-01-01));
//Mockito.when(accountStatementsFileFoundationServiceDelegate
//.createTransactionDateRange(params)).thenReturn(decision);
//
//Mockito.when(accountStatementsFileFoundationServiceDelegate.fsCallFilter(any())).thenReturn(decision);
//
//accountStatementsFileFoundationServiceDelegate.getFoundationServiceURL(accountNSC, accountNumber, baseURL);
////Mockito.when(restTransportForAccountStatementsStmt.getFoundationServiceURL(any(), any(), any(), any())).thenReturn("https://mocksvc-proxy.eu1.anypoint.mulesoft.com/exchange/dd9dfe2a-e2c9-4bb7-8357-3902557f1bf9/account-process-api/1.0.17/core-banking/p/abt/v1.0/accounts/nsc1234/acct1234");
//Mockito.when(accountStatementsFileFoundationServiceClient.restTransportForAccountStatementsStmt(any(),any(),any(),any())).thenReturn(new Statementsresponse());
//accountStatementsFileFoundationServiceDelegate.transformAccountStatementResponse(statements, params);
//accountStatementsFileFoundationServiceAdapter.retrieveAccountStatements(accountMapping, params);
//}
//@Test
//public void testAccountStatementFileFSforCreditCardforretrieveAccountStatementsCA()
//{
//List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
//AccountMapping accountMapping = new AccountMapping();
//MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<String ,String>();
//Map<String , String> params = new HashMap<String ,String>();
////     MultiValueMap<String, String> queryParams = accountStatementsFileFoundationServiceDelegate.getFromAndToDate(params);
//OBCashAccount3 oBCashAccount3 = new OBCashAccount3();
//List<OBCashAccount3> account = new ArrayList<OBCashAccount3>();
//AccountDetails accDet = new AccountDetails();
//OBAccount2 oba = new OBAccount2();
//Statementsresponse statements = new Statementsresponse();
//RequestInfo requestInfo = new RequestInfo();
//HttpHeaders httpHeaders = new HttpHeaders();
//StatementObject statement=new StatementObject();
//statement.setDocumentIdentificationNumber("12345");
//Date documentIssuanceDate= new Date();
//statement.setDocumentIssuanceDate(documentIssuanceDate);
//statement.setStatementEndDate(documentIssuanceDate);
//statement.setStatementReasonDescription(StatementReasonDescription.ACCOUNT_STATEMENT);
//statement.setStatementStartDate(documentIssuanceDate);
//List<StatementObject> list= new ArrayList<StatementObject>();
//list.add(statement);
//statements.setStatementsList(list);
//accDet.setAccountSubType(OBExternalAccountSubType1Code.CREDITCARD);
//accDet.setAccountId("12345");
//accDet.setAccountNSC("nsc1234");
//accDet.setAccountNumber("acct1234");
//oBCashAccount3.setIdentification("abcudeudfu");
//account.add(oBCashAccount3);
//oba.setAccount(account);
//accDet.setAccount(oBCashAccount3);
//accDetList.add(accDet);
//accountMapping.setAccountDetails(accDetList);
//accountMapping.setTppCID("test");
//accountMapping.setCorrelationId("test");
//accountMapping.setPsuId("test");
//queryParams.add("channelId","channel123");
//StatementDateRange decision=new StatementDateRange();
//decision.setConsentExpiryDateTime(new Date(2019-01-01));
//decision.setFilterFromDate(new Date(2019-01-01));
//decision.setFilterToDate (new Date(2019-01-01));
//decision.setTransactionFromDateTime(new Date(2019-01-01));
//decision.setTransactionToDateTime(new Date(2019-01-01));
//decision.setNewFilterToDate(new Date(2019-01-01));
//decision.setNewFilterFromDate(new Date(2019-01-01));
//Mockito.when(accountStatementsFileFoundationServiceDelegate
//.createTransactionDateRange(params)).thenReturn(decision);
//Mockito.when(accountStatementsFileFoundationServiceDelegate.fsCallFilter(any())).thenReturn(decision);
//queryParams.add(AccountStatementsFoundationServiceConstants.ACCOUNT_TYPE, "Current Account");
//queryParams.add(AccountStatementsFoundationServiceConstants.ACCOUNT_NUMBER,"accountNumber" );
//httpHeaders = accountStatementsFileFoundationServiceDelegate.createRequestHeadersFile(requestInfo,
//accountMapping, params);
//String singleAccountStatementsFileBaseStmtURL="/fuyfg";
//String accountNSC = "nsc1234";
//String accountNumber= "acct1234";
//String baseURL = "/fuyfg";
//accountStatementsFileFoundationServiceDelegate.getFoundationServiceURL(accountNSC, accountNumber, baseURL);
////Mockito.when(restTransportForAccountStatementsStmt.getFoundationServiceURL(any(), any(), any(), any())).thenReturn("https://mocksvc-proxy.eu1.anypoint.mulesoft.com/exchange/dd9dfe2a-e2c9-4bb7-8357-3902557f1bf9/account-process-api/1.0.17/core-banking/p/abt/v1.0/accounts/nsc1234/acct1234");
//Mockito.when(accountStatementsFileFoundationServiceClient.restTransportForAccountStatementsStmt(any(),any(),any(),any())).thenReturn(new Statementsresponse());
//accountStatementsFileFoundationServiceDelegate.transformAccountStatementResponse(statements, params);
//accountStatementsFileFoundationServiceAdapter.retrieveAccountStatements(accountMapping, params);
//}
//@Test
//public void testAccountStatementFileFSforCreditCardforretrieve()
//{
//List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
//AccountMapping accountMapping = new AccountMapping();
//MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<String ,String>();
//Map<String , String> params = new HashMap<String ,String>();
////     MultiValueMap<String, String> queryParams = accountStatementsFileFoundationServiceDelegate.getFromAndToDate(params);
//OBCashAccount3 oBCashAccount3 = new OBCashAccount3();
//List<OBCashAccount3> account = new ArrayList<OBCashAccount3>();
//AccountDetails accDet = new AccountDetails();
//OBAccount2 oba = new OBAccount2();
//Statementsresponse statements = new Statementsresponse();
//RequestInfo requestInfo = new RequestInfo();
//HttpHeaders httpHeaders = new HttpHeaders();
//StatementObject statement=new StatementObject();
//statement.setDocumentIdentificationNumber("12345");
//Date documentIssuanceDate= new Date();
//statement.setDocumentIssuanceDate(documentIssuanceDate);
//statement.setStatementEndDate(documentIssuanceDate);
//statement.setStatementReasonDescription(StatementReasonDescription.ACCOUNT_STATEMENT);
//statement.setStatementStartDate(documentIssuanceDate);
//List<StatementObject> list= new ArrayList<StatementObject>();
//list.add(statement);
//statements.setStatementsList(list);
//accDet.setAccountSubType(OBExternalAccountSubType1Code.CREDITCARD);
//accDet.setAccountId("12345");
//accDet.setAccountNSC("nsc1234");
//accDet.setAccountNumber("acct1234");
//oBCashAccount3.setIdentification("abcudeudfu");
//account.add(oBCashAccount3);
//oba.setAccount(account);
//accDet.setAccount(oBCashAccount3);
//accDetList.add(accDet);
//accountMapping.setAccountDetails(accDetList);
//accountMapping.setTppCID("test");
//accountMapping.setCorrelationId("test");
//accountMapping.setPsuId("test");
//queryParams.add("channelId","channel123");
//queryParams.add(AccountStatementsFoundationServiceConstants.ACCOUNT_TYPE, "Current Account");
//queryParams.add(AccountStatementsFoundationServiceConstants.ACCOUNT_NUMBER,"accountNumber" );
//httpHeaders = accountStatementsFileFoundationServiceDelegate.createRequestHeadersFile(requestInfo,
//accountMapping, params);
//String singleAccountStatementsFileBaseStmtURL="/fuyfg";
//String accountNSC = "nsc1234";
//String accountNumber= "acct1234";
//String baseURL = "/fuyfg";
//StatementDateRange decision=new StatementDateRange();
//decision.setConsentExpiryDateTime(new Date(2019-01-01));
//decision.setFilterFromDate(new Date(2019-01-01));
//decision.setFilterToDate (new Date(2019-01-01));
//decision.setTransactionFromDateTime(new Date(2019-01-01));
//decision.setTransactionToDateTime(new Date(2019-01-01));
//decision.setNewFilterToDate(new Date(2019-01-01));
//decision.setNewFilterFromDate(new Date(2019-01-01));
//Mockito.when(accountStatementsFileFoundationServiceDelegate
//    .createTransactionDateRange(params)).thenReturn(decision);
//Mockito.when(accountStatementsFileFoundationServiceDelegate.fsCallFilter(any())).thenReturn(decision);
//accountStatementsFileFoundationServiceDelegate.getFoundationServiceURL(accountNSC, accountNumber, baseURL);
////Mockito.when(restTransportForAccountStatementsStmt.getFoundationServiceURL(any(), any(), any(), any())).thenReturn("https://mocksvc-proxy.eu1.anypoint.mulesoft.com/exchange/dd9dfe2a-e2c9-4bb7-8357-3902557f1bf9/account-process-api/1.0.17/core-banking/p/abt/v1.0/accounts/nsc1234/acct1234");
//Mockito.when(accountStatementsFileFoundationServiceClient.restTransportForAccountStatementsStmt(any(),any(),any(),any())).thenReturn(new Statementsresponse());
//accountStatementsFileFoundationServiceDelegate.transformAccountStatementResponse(statements, params);
//accountStatementsFileFoundationServiceAdapter.retrieveAccountStatements(accountMapping, params);
//}
//@Test
//public void retrieveAccountStatementsByStatementIdTest()
//{
//AccountMapping accountMapping = new AccountMapping();
//Map<String , String> params = new HashMap<String ,String>();
//accountStatementsFileFoundationServiceAdapter.retrieveAccountStatementsByStatementId(accountMapping, params);
//}
//@Test
//public void retrieveAccountTransactionsByStatementsId()
//{
//AccountMapping accountMapping = new AccountMapping();
//Map<String , String> params = new HashMap<String ,String>();
//accountStatementsFileFoundationServiceAdapter.retrieveAccountTransactionsByStatementsId(accountMapping, params);
//}
//}
//


package com.capgemini.psd2.foundationservice.account.statements.boi.adapter.test;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import com.capgemini.psd2.adapter.datetime.utility.StatementDateRange;
import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.aisp.domain.OBAccount6;
import com.capgemini.psd2.aisp.domain.OBAccount6Account;
import com.capgemini.psd2.aisp.domain.OBExternalAccountSubType1Code;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountStatementsResponse;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountTransactionResponse;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.exceptions.ErrorInfo;
import com.capgemini.psd2.foundationservice.account.statements.boi.adapter.AccountStatementsFoundationServiceAdapter;
import com.capgemini.psd2.foundationservice.account.statements.boi.adapter.client.AccountStatementsFoundationServiceClient;
import com.capgemini.psd2.foundationservice.account.statements.boi.adapter.constants.AccountStatementsFoundationServiceConstants;
import com.capgemini.psd2.foundationservice.account.statements.boi.adapter.delegate.AccountStatementsFoundationServiceDelegate;
import com.capgemini.psd2.foundationservice.account.statements.boi.adapter.raml.domain.StatementObject;
import com.capgemini.psd2.foundationservice.account.statements.boi.adapter.raml.domain.StatementReasonDescription;
import com.capgemini.psd2.foundationservice.account.statements.boi.adapter.raml.domain.Statementsresponse;
//import com.capgemini.psd2.foundationservice.account.transactions.boi.adapter.raml.domain.TransactionList;
//import com.capgemini.psd2.foundationservice.account.statements.boi.adapter.domain.StatementObject;
//import com.capgemini.psd2.foundationservice.account.statements.boi.adapter.domain.StatementReasonDescription;
//import com.capgemini.psd2.foundationservice.account.statements.boi.adapter.domain.StatementsResponse;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.impl.RestClientSyncImpl;

@RunWith(SpringJUnit4ClassRunner.class)
public class AccountStatementsFoundationServiceAdapterTest {

	/** The account statements file foundation service adapter. */
	@InjectMocks
	private AccountStatementsFoundationServiceAdapter accountStatementsFileFoundationServiceAdapter = new AccountStatementsFoundationServiceAdapter();

	/** The account statements file foundation service client. *//*
																	* 
																	*/
	@Mock
	private AccountStatementsFoundationServiceClient accountStatementsFileFoundationServiceClient;

	/** The rest client. */
	@Mock
	private RestClientSyncImpl restClient;

	/** The account statements file foundation service delegate. */
	@Mock
	private AccountStatementsFoundationServiceDelegate accountStatementsFileFoundationServiceDelegate;

	/**
	 * Sets the up.
	 */
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * Test account statements file FS.
	 */

	private AccountMapping accountMapping = new AccountMapping();
	private List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
	private AccountDetails accDet = new AccountDetails();
	private Map<String, String> params = new HashMap<String, String>();
	private StatementDateRange statementDateRange = new StatementDateRange();

	
	
	@Test(expected= AdapterException.class)
	public void testDecision(){
		statementDateRange.setEmptyResponse(true);
		PlatformAccountTransactionResponse patr=new PlatformAccountTransactionResponse();
//		Mockito.when(accountTransactionsFoundationServiceDelegate.createTransactionDateRange(any())).thenReturn(patr);
		PlatformAccountStatementsResponse res = accountStatementsFileFoundationServiceAdapter.retrieveAccountStatements(accountMapping, params);
		assertNotNull(res);
		
	}

	@Test(expected= AdapterException.class)
	public void testAccountTransactionsFS4() {

		AccountMapping accountMapping = new AccountMapping();

		accountMapping.setAccountDetails(accDetList);
		accountMapping.setTppCID("test");
		accountMapping.setPsuId("BOI123");
		accountMapping.setCorrelationId("test");
		accountMapping.setAccountDetails(null);

		Map<String, String> params = new HashMap<String, String>();
		params.put("RequestedFromDateTime", "2016-12-31T23:59:59");
		params.put("RequestedToDateTime", null);
		params.put("RequestedPageNumber", "1");
		params.put("RequestedTxnFilter", "Cr");
		params.put("RequestedTxnKey", "trnxKey");
		params.put("channelId", "BOL");
		params.put("x-channel-id", "BOL");
		params.put("RequestedMinPageSize", "25");
		params.put("RequestedMaxPageSize", "100");
		params.put("RequestedPageSize", "25");
		params.put("accountId", "25666");
		params.put("account_subtype", "CreditCard");
		params.put("cmaVersion", null);
		RequestInfo requestInfo = new RequestInfo();
		ErrorInfo errorInfo = null;
		HttpHeaders httpHeaders = null;
		String fromBookingDateTimeInString = null;
		String toBookingDateTimeInString = null;

		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

		StatementDateRange statementDateRange = new StatementDateRange();
		statementDateRange.setNewFilterFromDate(new Date());
		statementDateRange.setNewFilterToDate(new Date());
		StatementDateRange decision = new StatementDateRange();
		decision.setConsentExpiryDateTime(new Date(2019 - 01 - 01));
		decision.setFilterFromDate(new Date(2019 - 01 - 01));
		decision.setFilterToDate(new Date(2019 - 01 - 01));
		decision.setTransactionFromDateTime(new Date(2019 - 01 - 01));
		decision.setTransactionToDateTime(new Date(2019 - 01 - 01));
		decision.setNewFilterToDate(new Date(2019 - 01 - 01));
		decision.setNewFilterFromDate(new Date(2019 - 01 - 01));
		Mockito.when(accountStatementsFileFoundationServiceDelegate.createTransactionDateRange(params))
				.thenReturn(decision);

		Mockito.when(accountStatementsFileFoundationServiceDelegate.fsCallFilter(any())).thenReturn(decision);
		Mockito.when(accountStatementsFileFoundationServiceDelegate.transformAccountStatementResponse(any(), any())).thenReturn(new PlatformAccountStatementsResponse());
		
		Mockito.when(accountStatementsFileFoundationServiceDelegate.createRequestHeadersStmt(anyObject(), anyObject())).thenReturn(new HttpHeaders());

		
//	Mockito.when(restClient.callForGet(any(), any(), any())).thenReturn(accounts);
		PlatformAccountStatementsResponse res = accountStatementsFileFoundationServiceAdapter
				.retrieveAccountStatements(accountMapping, params);

	}

	@Test(expected = AdapterException.class)
	public void testAccountTransactionsFSAccountIdNull() {
		accDet.setAccountId(null);
		accDetList.add(accDet);
		accountMapping.setAccountDetails(accDetList);
		accountStatementsFileFoundationServiceAdapter.retrieveAccountStatements(accountMapping, params);

//	accDet.setAccountId("1234");
	}

	@Test(expected = AdapterException.class)
	public void testAccountStatementFileFSIsNullAccountMapping() {
		Map<String, String> params = new HashMap<String, String>();
		params.put("a", "b");
		accountStatementsFileFoundationServiceAdapter.downloadStatementFileByStatementsId(null, params);
	}

	@Test(expected = AdapterException.class)
	public void testAccountStatementFileFSpsuIdIsNull() {
		AccountMapping accountMapping = new AccountMapping();
		accountMapping.setPsuId(null);
		Map<String, String> params = new HashMap<String, String>();
		params.put("a", "b");
		accountStatementsFileFoundationServiceAdapter.downloadStatementFileByStatementsId(accountMapping, params);
	}

	@Test(expected = AdapterException.class)
	public void testAccountStatementFileFSParamsIsNull() {
		AccountMapping accountMapping = new AccountMapping();
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
		AccountDetails accDet = new AccountDetails();
		accountMapping.setAccountDetails(accDetList);
		accDet.setAccountId("12345");
		accDet.setAccountNSC("nsc1234");
		accDet.setAccountNumber("acct1234");
		accountMapping.setTppCID("test");
		accountMapping.setCorrelationId("test");
		accountMapping.setPsuId("test");
		accountStatementsFileFoundationServiceAdapter.downloadStatementFileByStatementsId(accountMapping, null);
	}

	@Test(expected = AdapterException.class)
	public void testAccountStatementFileFSforCurrentAccount() {
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
		AccountMapping accountMapping = new AccountMapping();
		AccountDetails accDet = new AccountDetails();
		accDet.setAccountSubType(OBExternalAccountSubType1Code.CURRENTACCOUNT);
		accDet.setAccountId("12345");
		accDet.setAccountNSC("nsc1234");
		accDet.setAccountNumber("acct1234");
		accDetList.add(accDet);
		accountMapping.setAccountDetails(accDetList);
		accountMapping.setTppCID("test");
		accountMapping.setCorrelationId("test");
		accountMapping.setPsuId("test");
		Map<String, String> params = new HashMap<String, String>();
		accountStatementsFileFoundationServiceAdapter.downloadStatementFileByStatementsId(accountMapping, params);
	}

	@Test(expected = AdapterException.class)
	public void testAccountStatementFileFSforCurrentAccountException() {
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
		AccountMapping accountMapping = new AccountMapping();
		accDetList.add(null);
		accountMapping.setAccountDetails(null);
		accountMapping.setTppCID("test");
		accountMapping.setCorrelationId("test");
		accountMapping.setPsuId("test");
		Map<String, String> params = new HashMap<String, String>();
		accountStatementsFileFoundationServiceAdapter.downloadStatementFileByStatementsId(accountMapping, params);
	}

	@Test(expected = AdapterException.class)
	public void testAccountStatementFileFSforCreditCard() {
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
		AccountMapping accountMapping = new AccountMapping();
		Map<String, String> params = new HashMap<String, String>();
		AccountDetails accDet = new AccountDetails();
		OBAccount6 oba = new OBAccount6();
		OBAccount6Account oBCashAccount3 = new OBAccount6Account();
		List<OBAccount6Account> account = new ArrayList<OBAccount6Account>();
		RequestInfo requestInfo = new RequestInfo();

		accDet.setAccountSubType(OBExternalAccountSubType1Code.CREDITCARD);
		accDet.setAccountId("12345");
		accDet.setAccountNSC("nsc1234");
		accDet.setAccountNumber("acct1234");
		oBCashAccount3.setIdentification("abcudeudfu");
		account.add(oBCashAccount3);
		oba.setAccount(account);
		accDet.setAccount(oBCashAccount3);
		accDetList.add(accDet);
		accountMapping.setAccountDetails(accDetList);
		accountMapping.setTppCID("test");
		accountMapping.setCorrelationId("test");
		accountMapping.setPsuId("test");
		params.put("channelId", "channel123");
		params.put(AccountStatementsFoundationServiceConstants.ACCOUNT_TYPE, "CreditCard");
		params.put(AccountStatementsFoundationServiceConstants.ACCOUNT_NUMBER, "accountNumber");
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders = accountStatementsFileFoundationServiceDelegate.createRequestHeadersFile(accountMapping, params);
		String singleAccountStatementsCreditCardFileBaseURL = "fuyfg";
		accountStatementsFileFoundationServiceDelegate.getFoundationServiceCardURLFile(params,
				singleAccountStatementsCreditCardFileBaseURL);
		accountStatementsFileFoundationServiceAdapter.downloadStatementFileByStatementsId(accountMapping, params);
	}

	@Test(expected = AdapterException.class)
	public void testAccountStatementFileFSIsNullAccountMappingforretrieveAccountStatements() {
		Map<String, String> params = new HashMap<String, String>();
		params.put("a", "b");
		accountStatementsFileFoundationServiceAdapter.retrieveAccountStatements(null, params);
	}

	@Test(expected = AdapterException.class)
	public void testAccountStatementFileFSpsuIdIsNullforretrieveAccountStatements() {
		AccountMapping accountMapping = new AccountMapping();
		accountMapping.setPsuId(null);
		Map<String, String> params = new HashMap<String, String>();
		params.put("a", "b");
		accountStatementsFileFoundationServiceAdapter.retrieveAccountStatements(accountMapping, params);
	}

	public void testAccountStatementFileFSforforretrieveAccountStatementsException() {
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
		AccountMapping accountMapping = new AccountMapping();
		accDetList.add(null);
		accountMapping.setAccountDetails(null);
		accountMapping.setTppCID("test");
		accountMapping.setCorrelationId("test");
		accountMapping.setPsuId("test");
		accountStatementsFileFoundationServiceAdapter.retrieveAccountStatements(accountMapping, null);
	}

	@Test(expected= AdapterException.class)
	public void testAccountStatementFileFSParamsIsNullforretrieveAccountStatements() {
		AccountMapping accountMapping = new AccountMapping();
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
//AccountDetails accDet = new AccountDetails();
		accountMapping.setAccountDetails(accDetList);
//accDet.setAccountId("12345");
//accDet.setAccountNSC("nsc1234");
//accDet.setAccountNumber("acct1234");
		accountMapping.setTppCID("test");
		accountMapping.setCorrelationId("test");
		accountMapping.setPsuId("test");
		StatementDateRange decision = new StatementDateRange();
		decision.setConsentExpiryDateTime(new Date(2019 - 01 - 01));
		decision.setFilterFromDate(new Date(2019 - 01 - 01));
		decision.setFilterToDate(new Date(2019 - 01 - 01));
		decision.setTransactionFromDateTime(new Date(2019 - 01 - 01));
		decision.setTransactionToDateTime(new Date(2019 - 01 - 01));
		decision.setNewFilterToDate(new Date(2019 - 01 - 01));
		decision.setNewFilterFromDate(new Date(2019 - 01 - 01));
		Mockito.when(accountStatementsFileFoundationServiceDelegate.transformAccountStatementResponse(any(), any())).thenReturn(new PlatformAccountStatementsResponse());

		Mockito.when(accountStatementsFileFoundationServiceDelegate.createTransactionDateRange(params))
				.thenReturn(decision);

		Mockito.when(accountStatementsFileFoundationServiceDelegate.fsCallFilter(any())).thenReturn(decision);
		Map<String, String> params = new HashMap<String, String>();
		accountStatementsFileFoundationServiceAdapter.retrieveAccountStatements(accountMapping, params);
	}

	@Test
	public void testAccountStatementFileFSforCreditCardforretrieveAccountStatements() {
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
		AccountMapping accountMapping = new AccountMapping();
		MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<String, String>();
		Map<String, String> params = new HashMap<String, String>();
//     MultiValueMap<String, String> queryParams = accountStatementsFileFoundationServiceDelegate.getFromAndToDate(params);
		OBAccount6Account oBCashAccount3 = new OBAccount6Account();
		List<OBAccount6Account> account = new ArrayList<OBAccount6Account>();
		AccountDetails accDet = new AccountDetails();
		OBAccount6 oba = new OBAccount6();
		Statementsresponse statements = new Statementsresponse();
		RequestInfo requestInfo = new RequestInfo();
		HttpHeaders httpHeaders = new HttpHeaders();
		StatementObject statement = new StatementObject();
		statement.setDocumentIdentificationNumber("12345");
		Date documentIssuanceDate = new Date();
		statement.setDocumentIssuanceDate(documentIssuanceDate);
		statement.setStatementEndDate(documentIssuanceDate);
		statement.setStatementReasonDescription(StatementReasonDescription.ACCOUNT_STATEMENT);
		statement.setStatementStartDate(documentIssuanceDate);
		List<StatementObject> list = new ArrayList<StatementObject>();
		list.add(statement);
		statements.setStatementsList(list);
		accDet.setAccountSubType(OBExternalAccountSubType1Code.CREDITCARD);
		accDet.setAccountId("12345");
		accDet.setAccountNSC("nsc1234");
		accDet.setAccountNumber("acct1234");
		assertNotNull(accDet);
		oBCashAccount3.setIdentification("abcudeudfu");
		account.add(oBCashAccount3);
		oba.setAccount(account);
		accDet.setAccount(oBCashAccount3);
		accDetList.add(accDet);
		accountMapping.setAccountDetails(accDetList);
		accountMapping.setTppCID("test");
		accountMapping.setCorrelationId("test");
		accountMapping.setPsuId("test");
		queryParams.add("channelId", "channel123");
		queryParams.add(AccountStatementsFoundationServiceConstants.ACCOUNT_TYPE, "CreditCard");
		queryParams.add(AccountStatementsFoundationServiceConstants.ACCOUNT_NUMBER, "accountNumber");
		httpHeaders = accountStatementsFileFoundationServiceDelegate.createRequestHeadersFile(accountMapping, params);
		String singleAccountStatementsFileBaseStmtURL = "/fuyfg";
		String accountNSC = "nsc1234";
		String accountNumber = "acct1234";
		String baseURL = "/fuyfg";
		StatementDateRange decision = new StatementDateRange();
		decision.setConsentExpiryDateTime(new Date(2019 - 01 - 01));
		decision.setFilterFromDate(new Date(2019 - 01 - 01));
		decision.setFilterToDate(new Date(2019 - 01 - 01));
		decision.setTransactionFromDateTime(new Date(2019 - 01 - 01));
		decision.setTransactionToDateTime(new Date(2019 - 01 - 01));
		decision.setNewFilterToDate(new Date(2019 - 01 - 01));
		decision.setNewFilterFromDate(new Date(2019 - 01 - 01));
		Mockito.when(accountStatementsFileFoundationServiceDelegate.createTransactionDateRange(params))
				.thenReturn(decision);

		Mockito.when(accountStatementsFileFoundationServiceDelegate.fsCallFilter(any())).thenReturn(decision);

		accountStatementsFileFoundationServiceDelegate.getFoundationServiceURL(accountNSC, accountNumber, baseURL);
//Mockito.when(restTransportForAccountStatementsStmt.getFoundationServiceURL(any(), any(), any(), any())).thenReturn("https://mocksvc-proxy.eu1.anypoint.mulesoft.com/exchange/dd9dfe2a-e2c9-4bb7-8357-3902557f1bf9/account-process-api/1.0.17/core-banking/p/abt/v1.0/accounts/nsc1234/acct1234");
		Mockito.when(accountStatementsFileFoundationServiceClient.restTransportForAccountStatementsStmt(any(), any(),
				any(), any())).thenReturn(new Statementsresponse());
		accountStatementsFileFoundationServiceDelegate.transformAccountStatementResponse(statements, params);
		accountStatementsFileFoundationServiceAdapter.retrieveAccountStatements(accountMapping, params);
	}

	@Test
	public void testAccountStatementFileFSforCreditCardforretrieveAccountStatementsCA() {
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
		AccountMapping accountMapping = new AccountMapping();
		MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<String, String>();
		Map<String, String> params = new HashMap<String, String>();
//     MultiValueMap<String, String> queryParams = accountStatementsFileFoundationServiceDelegate.getFromAndToDate(params);
		OBAccount6Account oBCashAccount3 = new OBAccount6Account();
		List<OBAccount6Account> account = new ArrayList<OBAccount6Account>();
		AccountDetails accDet = new AccountDetails();
		OBAccount6 oba = new OBAccount6();
		Statementsresponse statements = new Statementsresponse();
		RequestInfo requestInfo = new RequestInfo();
		HttpHeaders httpHeaders = new HttpHeaders();
		StatementObject statement = new StatementObject();
		statement.setDocumentIdentificationNumber("12345");
		Date documentIssuanceDate = new Date();
		statement.setDocumentIssuanceDate(documentIssuanceDate);
		statement.setStatementEndDate(documentIssuanceDate);
		statement.setStatementReasonDescription(StatementReasonDescription.ACCOUNT_STATEMENT);
		statement.setStatementStartDate(documentIssuanceDate);
		List<StatementObject> list = new ArrayList<StatementObject>();
		list.add(statement);
		statements.setStatementsList(list);
		accDet.setAccountSubType(OBExternalAccountSubType1Code.CREDITCARD);
		accDet.setAccountId("12345");
		accDet.setAccountNSC("nsc1234");
		accDet.setAccountNumber("acct1234");
		assertNotNull(accDet);
		oBCashAccount3.setIdentification("abcudeudfu");
		account.add(oBCashAccount3);
		oba.setAccount(account);
		accDet.setAccount(oBCashAccount3);
		accDetList.add(accDet);
		accountMapping.setAccountDetails(accDetList);
		accountMapping.setTppCID("test");
		accountMapping.setCorrelationId("test");
		accountMapping.setPsuId("test");
		queryParams.add("channelId", "channel123");
		StatementDateRange decision = new StatementDateRange();
		decision.setConsentExpiryDateTime(new Date(2019 - 01 - 01));
		decision.setFilterFromDate(new Date(2019 - 01 - 01));
		decision.setFilterToDate(new Date(2019 - 01 - 01));
		decision.setTransactionFromDateTime(new Date(2019 - 01 - 01));
		decision.setTransactionToDateTime(new Date(2019 - 01 - 01));
		decision.setNewFilterToDate(new Date(2019 - 01 - 01));
		decision.setNewFilterFromDate(new Date(2019 - 01 - 01));
		Mockito.when(accountStatementsFileFoundationServiceDelegate.createTransactionDateRange(params))
				.thenReturn(decision);
		Mockito.when(accountStatementsFileFoundationServiceDelegate.fsCallFilter(any())).thenReturn(decision);
		queryParams.add(AccountStatementsFoundationServiceConstants.ACCOUNT_TYPE, "Current Account");
		queryParams.add(AccountStatementsFoundationServiceConstants.ACCOUNT_NUMBER, "accountNumber");
		httpHeaders = accountStatementsFileFoundationServiceDelegate.createRequestHeadersFile(accountMapping, params);
		String singleAccountStatementsFileBaseStmtURL = "/fuyfg";
		String accountNSC = "nsc1234";
		String accountNumber = "acct1234";
		String baseURL = "/fuyfg";
		accountStatementsFileFoundationServiceDelegate.getFoundationServiceURL(accountNSC, accountNumber, baseURL);
//Mockito.when(restTransportForAccountStatementsStmt.getFoundationServiceURL(any(), any(), any(), any())).thenReturn("https://mocksvc-proxy.eu1.anypoint.mulesoft.com/exchange/dd9dfe2a-e2c9-4bb7-8357-3902557f1bf9/account-process-api/1.0.17/core-banking/p/abt/v1.0/accounts/nsc1234/acct1234");
		Mockito.when(accountStatementsFileFoundationServiceClient.restTransportForAccountStatementsStmt(any(), any(),
				any(), any())).thenReturn(new Statementsresponse());
		accountStatementsFileFoundationServiceDelegate.transformAccountStatementResponse(statements, params);
		accountStatementsFileFoundationServiceAdapter.retrieveAccountStatements(accountMapping, params);
	}

	@Test
	public void testAccountStatementFileFSforCreditCardforretrieve() {
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
		AccountMapping accountMapping = new AccountMapping();
		MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<String, String>();
		Map<String, String> params = new HashMap<String, String>();
//     MultiValueMap<String, String> queryParams = accountStatementsFileFoundationServiceDelegate.getFromAndToDate(params);
		OBAccount6Account oBCashAccount3 = new OBAccount6Account();
		List<OBAccount6Account> account = new ArrayList<OBAccount6Account>();
		AccountDetails accDet = new AccountDetails();
		OBAccount6 oba = new OBAccount6();
		Statementsresponse statements = new Statementsresponse();
		RequestInfo requestInfo = new RequestInfo();
		HttpHeaders httpHeaders = new HttpHeaders();
		StatementObject statement = new StatementObject();
		statement.setDocumentIdentificationNumber("12345");
		Date documentIssuanceDate = new Date();
		statement.setDocumentIssuanceDate(documentIssuanceDate);
		statement.setStatementEndDate(documentIssuanceDate);
		statement.setStatementReasonDescription(StatementReasonDescription.ACCOUNT_STATEMENT);
		statement.setStatementStartDate(documentIssuanceDate);
		List<StatementObject> list = new ArrayList<StatementObject>();
		list.add(statement);
		statements.setStatementsList(list);
		accDet.setAccountSubType(OBExternalAccountSubType1Code.CREDITCARD);
		accDet.setAccountId("12345");
		accDet.setAccountNSC("nsc1234");
		accDet.setAccountNumber("acct1234");
		assertNotNull(accDet);
		oBCashAccount3.setIdentification("abcudeudfu");
		account.add(oBCashAccount3);
		oba.setAccount(account);
		accDet.setAccount(oBCashAccount3);
		accDetList.add(accDet);
		accountMapping.setAccountDetails(accDetList);
		accountMapping.setTppCID("test");
		accountMapping.setCorrelationId("test");
		accountMapping.setPsuId("test");
		queryParams.add("channelId", "channel123");
		queryParams.add(AccountStatementsFoundationServiceConstants.ACCOUNT_TYPE, "Current Account");
		queryParams.add(AccountStatementsFoundationServiceConstants.ACCOUNT_NUMBER, "accountNumber");
		httpHeaders = accountStatementsFileFoundationServiceDelegate.createRequestHeadersFile(accountMapping, params);
		String singleAccountStatementsFileBaseStmtURL = "/fuyfg";
		String accountNSC = "nsc1234";
		String accountNumber = "acct1234";
		String baseURL = "/fuyfg";
		StatementDateRange decision = new StatementDateRange();
		decision.setConsentExpiryDateTime(new Date(2019 - 01 - 01));
		decision.setFilterFromDate(new Date(2019 - 01 - 01));
		decision.setFilterToDate(new Date(2019 - 01 - 01));
		decision.setTransactionFromDateTime(new Date(2019 - 01 - 01));
		decision.setTransactionToDateTime(new Date(2019 - 01 - 01));
		decision.setNewFilterToDate(new Date(2019 - 01 - 01));
		decision.setNewFilterFromDate(new Date(2019 - 01 - 01));
		Mockito.when(accountStatementsFileFoundationServiceDelegate.createTransactionDateRange(params))
				.thenReturn(decision);
		Mockito.when(accountStatementsFileFoundationServiceDelegate.fsCallFilter(any())).thenReturn(decision);
		accountStatementsFileFoundationServiceDelegate.getFoundationServiceURL(accountNSC, accountNumber, baseURL);
//Mockito.when(restTransportForAccountStatementsStmt.getFoundationServiceURL(any(), any(), any(), any())).thenReturn("https://mocksvc-proxy.eu1.anypoint.mulesoft.com/exchange/dd9dfe2a-e2c9-4bb7-8357-3902557f1bf9/account-process-api/1.0.17/core-banking/p/abt/v1.0/accounts/nsc1234/acct1234");
		Mockito.when(accountStatementsFileFoundationServiceClient.restTransportForAccountStatementsStmt(any(), any(),
				any(), any())).thenReturn(new Statementsresponse());
		accountStatementsFileFoundationServiceDelegate.transformAccountStatementResponse(statements, params);
		accountStatementsFileFoundationServiceAdapter.retrieveAccountStatements(accountMapping, params);
	}

	@Test
	public void retrieveAccountStatementsByStatementIdTest() {
		AccountMapping accountMapping = new AccountMapping();
		assertNotNull(accountMapping);
		Map<String, String> params = new HashMap<String, String>();
		accountStatementsFileFoundationServiceAdapter.retrieveAccountStatementsByStatementId(accountMapping, params);
	}

	@Test
	public void retrieveAccountTransactionsByStatementsId() {
		AccountMapping accountMapping = new AccountMapping();
		assertNotNull(accountMapping);
		Map<String, String> params = new HashMap<String, String>();
		accountStatementsFileFoundationServiceAdapter.retrieveAccountTransactionsByStatementsId(accountMapping, params);
	}
}

