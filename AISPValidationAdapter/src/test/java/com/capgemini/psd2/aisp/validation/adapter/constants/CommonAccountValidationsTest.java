package com.capgemini.psd2.aisp.validation.adapter.constants;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.aisp.validation.adapter.constants.CommonAccountValidations;
import com.capgemini.psd2.aisp.validation.adapter.constants.PaymentSetupConstants;
import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.OBPSD2Exception;
import com.capgemini.psd2.exceptions.OBPSD2ExceptionUtility;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.domain.OBExchangeRate1;
import com.capgemini.psd2.pisp.domain.OBExchangeRateType2Code;
import com.capgemini.psd2.pisp.domain.OBExternalPaymentContext1Code;
import com.capgemini.psd2.pisp.domain.OBInternational1;
import com.capgemini.psd2.pisp.domain.OBPostalAddress6;
import com.capgemini.psd2.pisp.domain.OBPriority2Code;
import com.capgemini.psd2.pisp.domain.OBRemittanceInformation1;
import com.capgemini.psd2.pisp.domain.OBRisk1;
import com.capgemini.psd2.pisp.domain.OBRisk1DeliveryAddress;
import com.capgemini.psd2.pisp.domain.OBWriteDataInternationalConsent1;
import com.capgemini.psd2.pisp.domain.OBWriteInternationalConsent1;

@RunWith(SpringJUnit4ClassRunner.class)
public class CommonAccountValidationsTest {

	@InjectMocks
	private CommonAccountValidations commonPaymentValidations;

	@Mock
	private RequestHeaderAttributes reqHeaderAtrributes;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Before
	public void initializeErrorMessages() {
		Map<String, String> genericErrorMessages = new HashMap<>();
		genericErrorMessages.put("FIELD", "freq ma");

		Map<String, String> specificErrorMessageMap = new HashMap<>();
		specificErrorMessageMap.put("FIELD", "freq ma");

		OBPSD2ExceptionUtility.genericErrorMessages.putAll(genericErrorMessages);
		OBPSD2ExceptionUtility.specificErrorMessages.putAll(specificErrorMessageMap);
	}

	@Before
	public void initializeLists() {
		String[] validSchemeNames = { "IBAN", "SortCodeAccountNumber", "PAN", "UK.OBIE.BBAN", "UK.OBIE.PAN",
				"UK.OBIE.IBAN", "UK.OBIE.SortCodeAccountNumber", "UK.OBIE.Paym" };
		List<String> validSchemeNameList = Arrays.asList(validSchemeNames);
		commonPaymentValidations.getValidSchemeNameList().addAll(validSchemeNameList);

		String[] validLocalInstruments = { "UK.OBIE.FPS", "UK.OBIE.BACS", "UK.OBIE.CHAPS", "UK.OBIE.Paym",
				"UK.OBIE.BalanceTransfer", "UK.OBIE.MoneyTransfer", "UK.OBIE.Link", "UK.OBIE.SEPACreditTransfer",
				"UK.OBIE.SEPAInstantCreditTransfer", "UK.OBIE.SWIFT", "UK.OBIE.Target2", "UK.OBIE.Euro1" };
		List<String> validLocalInstrumentList = Arrays.asList(validLocalInstruments);
		commonPaymentValidations.getValidLocalInstrumentList().addAll(validLocalInstrumentList);

		String[] validFileTypes = { "UK.OBIE.pain.001.001.08", "UK.OBIE.PaymentInitiation.3.0" };
		List<String> validFileTypeList = Arrays.asList(validFileTypes);
		commonPaymentValidations.getValidFileTypeList().addAll(validFileTypeList);
	}

	@Test(expected = PSD2Exception.class)
	public void validateFileType_invalidFileType_errorCodePresent() {
		commonPaymentValidations.validateFileType("UK.OBIE.Invalid", ErrorCodeEnum.PISP_FILETYPE_NOT_VALID);
	}

	@Test(expected = PSD2Exception.class)
	public void validateFileType_invalidFileType_errorCodeAbsent() {
		commonPaymentValidations.validateFileType("UK.OBIE.Invalid", null);
	}

	@Test(expected = OBPSD2Exception.class)
	public void validateExchangeRate_OBExchangeRateAgreed_contractIdentificationAndExchangeRateAbsent() {
		OBExchangeRate1 obExchangeRate1 = new OBExchangeRate1();
		obExchangeRate1.setRateType(OBExchangeRateType2Code.AGREED);
		commonPaymentValidations.validateExchangeRate(obExchangeRate1);
	}

	@Test(expected = OBPSD2Exception.class)
	public void validateExchangeRate_OBExchangeRateActual() {
		OBExchangeRate1 obExchangeRate1 = new OBExchangeRate1();
		obExchangeRate1.setRateType(OBExchangeRateType2Code.ACTUAL);
		obExchangeRate1.setContractIdentification("contractIdentification");
		obExchangeRate1.setExchangeRate(new BigDecimal(1));
		commonPaymentValidations.validateExchangeRate(obExchangeRate1);
	}

	@Test(expected = OBPSD2Exception.class)
	public void validateExchangeRate_OBExchangeRateActual_contractIdentificationPresent() {
		OBExchangeRate1 obExchangeRate1 = new OBExchangeRate1();
		obExchangeRate1.setRateType(OBExchangeRateType2Code.ACTUAL);
		obExchangeRate1.setContractIdentification("contractIdentification");
		commonPaymentValidations.validateExchangeRate(obExchangeRate1);
	}

	@Test(expected = OBPSD2Exception.class)
	public void validateExchangeRate_OBExchangeRateActual_exchangeRatePresent() {
		OBExchangeRate1 obExchangeRate1 = new OBExchangeRate1();
		obExchangeRate1.setRateType(OBExchangeRateType2Code.ACTUAL);
		obExchangeRate1.setExchangeRate(new BigDecimal(1));
		commonPaymentValidations.validateExchangeRate(obExchangeRate1);
	}

	@Test(expected = OBPSD2Exception.class)
	public void validateExchangeRate_OBExchangeRateIndicative() {
		OBExchangeRate1 obExchangeRate1 = new OBExchangeRate1();
		obExchangeRate1.setRateType(OBExchangeRateType2Code.INDICATIVE);
		obExchangeRate1.setContractIdentification("contractIdentification");
		obExchangeRate1.setExchangeRate(new BigDecimal(1));
		commonPaymentValidations.validateExchangeRate(obExchangeRate1);
	}

	@Test(expected = OBPSD2Exception.class)
	public void isValidCurrency_emptyCurrencyCode_errorCodePresent() {
		commonPaymentValidations.isValidCurrency("", OBErrorCodeEnum.UK_OBIE_UNSUPPORTED_CURRENCY);
	}

	@Test(expected = OBPSD2Exception.class)
	public void isValidCurrency_emptyCurrencyCode_errorCodeAbsent() {
		commonPaymentValidations.isValidCurrency("", null);
	}

	@Test
	public void isValidCurrency_validCurrencyCode() {
		Assert.assertTrue(commonPaymentValidations.isValidCurrency("INR", null));
	}

	@Test(expected = OBPSD2Exception.class)
	public void isValidCurrency_invalidCurrencyCode_errorCodePresent() {
		commonPaymentValidations.isValidCurrency("Invalid", OBErrorCodeEnum.UK_OBIE_UNSUPPORTED_CURRENCY);
	}

	@Test(expected = OBPSD2Exception.class)
	public void isValidCurrency_invalidCurrencyCode_errorCodeAbsent() {
		commonPaymentValidations.isValidCurrency("Invalid", null);
	}

	@Test(expected = OBPSD2Exception.class)
	public void validateIdentification_invalidLocalInstrument_emptyIdentification() {
		commonPaymentValidations.validateIdentification("");
	}

	@Test(expected = OBPSD2Exception.class)
	public void validateLocalInstrument_invalidLocalInstrument_errorCodePresent() {
		commonPaymentValidations.validateLocalInstrument("UK.OBIE.Invalid",
				OBErrorCodeEnum.UK_OBIE_UNSUPPORTED_LOCALINSTRUMENT);
	}

	@Test(expected = OBPSD2Exception.class)
	public void validateLocalInstrument_invalidLocalInstrument_errorCodeAbsent() {
		commonPaymentValidations.validateLocalInstrument("UK.OBIE.Invalid", null);
	}

	@Test(expected = OBPSD2Exception.class)
	public void validateHeaders_emptyIdempotencyKey() {
		Mockito.when(reqHeaderAtrributes.getIdempotencyKey()).thenReturn("");
		commonPaymentValidations.validateHeaders();
	}

	@Test(expected = OBPSD2Exception.class)
	public void validateHeaders_invalidIdempotencyKey() {
		Mockito.when(reqHeaderAtrributes.getIdempotencyKey()).thenReturn("");
		commonPaymentValidations.validateHeaders();
	}

	@Test(expected = OBPSD2Exception.class)
	public void validateHeaders_invalidIdempotencyKeyLength() {
		Mockito.when(reqHeaderAtrributes.getIdempotencyKey()).thenReturn("zsddxdfhdhSDGHre000zsddxdfhdhSDGHre000zsd");
		commonPaymentValidations.validateHeaders();
	}

	@Test(expected = OBPSD2Exception.class)
	public void validateRemittanceInformation_emptyReference() {
		OBRemittanceInformation1 obRemittanceInformation1 = new OBRemittanceInformation1();
		obRemittanceInformation1.setReference("");
		commonPaymentValidations.validateRemittanceInformation("UK.OBIE.FPS", obRemittanceInformation1);
	}

	@Test(expected = OBPSD2Exception.class)
	public void validateRemittanceInformation_invalidReferenceLength() {
		OBRemittanceInformation1 obRemittanceInformation1 = new OBRemittanceInformation1();
		obRemittanceInformation1.setReference("zsddxdfhdhSDGHre000");
		commonPaymentValidations.validateRemittanceInformation("UK.OBIE.FPS", obRemittanceInformation1);
	}

	@Test(expected = OBPSD2Exception.class)
	public void validateSchemeNameWithIdentification_invalidInputScheme_errorCodePresent() {
		commonPaymentValidations.validateSchemeNameWithIdentification("Invalid", "",
				OBErrorCodeEnum.UK_OBIE_UNSUPPORTED_SCHEME);
	}

	@Test(expected = OBPSD2Exception.class)
	public void validateSchemeNameWithIdentification_invalidInputScheme_errorCodeAbsent() {
		commonPaymentValidations.validateSchemeNameWithIdentification("Invalid", "", null);
	}

	@Test(expected = OBPSD2Exception.class)
	public void validateSchemeNameWithIdentification_invalidIBAN_errorCodePresent() {
		commonPaymentValidations.validateSchemeNameWithIdentification(PaymentSetupConstants.IBAN, "Invalid",
				OBErrorCodeEnum.UK_OBIE_UNSUPPORTED_ACCOUNTIDENTIFIER);
	}

	@Test(expected = OBPSD2Exception.class)
	public void validateSchemeNameWithIdentification_invalidIBAN_errorCodeAbsent() {
		commonPaymentValidations.validateSchemeNameWithIdentification(PaymentSetupConstants.IBAN, "Invalid", null);
	}

	@Test(expected = OBPSD2Exception.class)
	public void validateSchemeNameWithIdentification_invalidSortCodeAccountNumber_errorCodeAbsent() {
		commonPaymentValidations.validateSchemeNameWithIdentification(PaymentSetupConstants.SORTCODEACCOUNTNUMBER,
				"Invalid", null);
	}

	@Test(expected = OBPSD2Exception.class)
	public void validateSchemeNameWithIdentification_invalidSortCodeAccountNumber_errorCodePresent() {
		commonPaymentValidations.validateSchemeNameWithIdentification(PaymentSetupConstants.SORTCODEACCOUNTNUMBER,
				"Invalid", OBErrorCodeEnum.UK_OBIE_UNSUPPORTED_ACCOUNTIDENTIFIER);
	}

	@Test(expected = OBPSD2Exception.class)
	public void validateSchemeNameWithIdentification_invalidBBAN_errorCodePresent() {
		commonPaymentValidations.validateSchemeNameWithIdentification(PaymentSetupConstants.UK_OBIE_BBAN, "Invalid",
				OBErrorCodeEnum.UK_OBIE_UNSUPPORTED_ACCOUNTIDENTIFIER);
	}

	@Test(expected = OBPSD2Exception.class)
	public void validateSchemeNameWithIdentification_invalidBBAN_errorCodeAbsent() {
		commonPaymentValidations.validateSchemeNameWithIdentification(PaymentSetupConstants.UK_OBIE_BBAN, "Invalid",
				null);
	}

	@Test
	public void validateSchemeNameWithIdentification_invalidPAN() {
		commonPaymentValidations.validateSchemeNameWithIdentification(PaymentSetupConstants.PAN, "Invalid",
				OBErrorCodeEnum.UK_OBIE_UNSUPPORTED_ACCOUNTIDENTIFIER);
	}

	@Test
	public void validateSchemeNameWithIdentification_invalidPAYM() {
		commonPaymentValidations.validateSchemeNameWithIdentification(PaymentSetupConstants.UK_OBIE_PAYM, "Invalid",
				OBErrorCodeEnum.UK_OBIE_UNSUPPORTED_ACCOUNTIDENTIFIER);
	}

	@Test(expected = OBPSD2Exception.class)
	public void validateSchemeNameWithSecondaryIdentification_invalidInputScheme() {
		commonPaymentValidations.validateSchemeNameWithSecondaryIdentification("Invalid", "");
	}

	@Test(expected = OBPSD2Exception.class)
	public void validateSchemeNameWithSecondaryIdentification_validInputScheme_emptySecondaryIdentification() {
		commonPaymentValidations
				.validateSchemeNameWithSecondaryIdentification(PaymentSetupConstants.SORTCODEACCOUNTNUMBER, "");
	}

	@Test(expected = OBPSD2Exception.class)
	public void validateSchemeNameWithSecondaryIdentification_validInputScheme_invalidSecondaryIdentificationLength() {
		commonPaymentValidations.validateSchemeNameWithSecondaryIdentification(
				PaymentSetupConstants.SORTCODEACCOUNTNUMBER, "zsddxdfhdhSDGHre000zsddxdfhdhSDGHre");
	}

	@Test(expected = OBPSD2Exception.class)
	public void validateDomesticRiskDeliveryAddress_invalidAddressLine() {
		OBRisk1DeliveryAddress obRisk1DeliveryAddress = new OBRisk1DeliveryAddress();
		List<String> addressLine = new ArrayList<>();
		addressLine.add("");
		obRisk1DeliveryAddress.setAddressLine(addressLine);
		commonPaymentValidations.validateDomesticRiskDeliveryAddress(obRisk1DeliveryAddress,
				OBErrorCodeEnum.UK_OBIE_FIELD_INVALID);
	}

	@Test(expected = OBPSD2Exception.class)
	public void validateDomesticRiskDeliveryAddress_invalidAddressLineLength() {
		OBRisk1DeliveryAddress obRisk1DeliveryAddress = new OBRisk1DeliveryAddress();
		List<String> addressLine = new ArrayList<>();
		addressLine.add("zsddxdfhdhSDGHre000zsddxdfhdhSDGHre000zsddxdfhdhSDGHre000zsddxdfhdhSDGH");
		obRisk1DeliveryAddress.setAddressLine(addressLine);
		commonPaymentValidations.validateDomesticRiskDeliveryAddress(obRisk1DeliveryAddress,
				OBErrorCodeEnum.UK_OBIE_FIELD_INVALID);
	}

	@Ignore // As validateISOCountry() method is not being invoked
	@Test(expected = OBPSD2Exception.class)
	public void validateDomesticRiskDeliveryAddress_invalidSubDivisionLength() {
		OBRisk1DeliveryAddress obRisk1DeliveryAddress = new OBRisk1DeliveryAddress();
		obRisk1DeliveryAddress.setAddressLine(null);
		String countrySubDivision = "zsddxdfhdhSDGHre000";
		obRisk1DeliveryAddress.setCountrySubDivision(countrySubDivision);
		commonPaymentValidations.validateDomesticRiskDeliveryAddress(obRisk1DeliveryAddress,
				OBErrorCodeEnum.UK_OBIE_FIELD_INVALID);
	}

	@Ignore // As validateDomesticCountrySubDivision() method is not being invoked
	@Test(expected = OBPSD2Exception.class)
	public void validateDomesticRiskDeliveryAddress_invalidCountryCode() {
		OBRisk1DeliveryAddress obRisk1DeliveryAddress = new OBRisk1DeliveryAddress();
		obRisk1DeliveryAddress.setAddressLine(null);
		obRisk1DeliveryAddress.setCountrySubDivision(null);
		obRisk1DeliveryAddress.setCountry("ZZ");
		commonPaymentValidations.validateDomesticRiskDeliveryAddress(obRisk1DeliveryAddress,
				OBErrorCodeEnum.UK_OBIE_FIELD_INVALID);
	}

	@Test(expected = OBPSD2Exception.class)
	public void validateDomesticCreditorPostalAddress_invalidAddressLine() {
		OBPostalAddress6 obPostalAddress6 = new OBPostalAddress6();
		List<String> addressLine = new ArrayList<>();
		addressLine.add("");
		obPostalAddress6.setAddressLine(addressLine);
		commonPaymentValidations.validateDomesticCreditorPostalAddress(obPostalAddress6, null);
	}

	@Test(expected = OBPSD2Exception.class)
	public void validateDomesticCreditorPostalAddress_invalidAddressLineLength() {
		OBPostalAddress6 obPostalAddress6 = new OBPostalAddress6();
		List<String> addressLine = new ArrayList<>();
		addressLine.add("zsddxdfhdhSDGHre000zsddxdfhdhSDGHre000zsddxdfhdhSDGHre000zsddxdfhdhSDGH");
		obPostalAddress6.setAddressLine(addressLine);
		commonPaymentValidations.validateDomesticCreditorPostalAddress(obPostalAddress6, null);
	}

	@Test(expected = OBPSD2Exception.class)
	public void validateDomesticCreditorPostalAddress_invalidSubDivisionLength() {
		OBPostalAddress6 obPostalAddress6 = new OBPostalAddress6();
		obPostalAddress6.setAddressLine(null);
		obPostalAddress6.setCountrySubDivision("zsddxdfhdhSDGHre000zsddxdfhdhSDGHre000");
		commonPaymentValidations.validateDomesticCreditorPostalAddress(obPostalAddress6, null);
	}

	@Ignore // As validateISOCountry() method is not being invoked
	@Test(expected = OBPSD2Exception.class)
	public void validateDomesticCreditorPostalAddress_invalidCountryCode() {
		OBPostalAddress6 obPostalAddress6 = new OBPostalAddress6();
		obPostalAddress6.setAddressLine(null);
		obPostalAddress6.setCountrySubDivision(null);
		obPostalAddress6.setCountry("ZZ");
		commonPaymentValidations.validateDomesticCreditorPostalAddress(obPostalAddress6, null);
	}

	@Test(expected = OBPSD2Exception.class)
	public void validateInternationalRiskDeliveryAddress_invalidAddressLine() {
		OBRisk1DeliveryAddress obRisk1DeliveryAddress = new OBRisk1DeliveryAddress();
		List<String> addressLine = new ArrayList<>();
		addressLine.add("");
		obRisk1DeliveryAddress.setAddressLine(addressLine);
		commonPaymentValidations.validateInternationalRiskDeliveryAddress(obRisk1DeliveryAddress,
				OBErrorCodeEnum.UK_OBIE_FIELD_INVALID);
	}

	@Test(expected = OBPSD2Exception.class)
	public void validateInternationalRiskDeliveryAddress_invalidAddressLineLength() {
		OBRisk1DeliveryAddress obRisk1DeliveryAddress = new OBRisk1DeliveryAddress();
		List<String> addressLine = new ArrayList<>();
		addressLine.add("zsddxdfhdhSDGHre000zsddxdfhdhSDGHre000zsddxdfhdhSDGHre000zsddxdfhdhSDGH");
		obRisk1DeliveryAddress.setAddressLine(addressLine);
		commonPaymentValidations.validateInternationalRiskDeliveryAddress(obRisk1DeliveryAddress,
				OBErrorCodeEnum.UK_OBIE_FIELD_INVALID);
	}

	@Ignore // As validateDomesticCountrySubDivision() method is not being invoked
	@Test(expected = OBPSD2Exception.class)
	public void validateInternationalRiskDeliveryAddress_invalidSubDivisionLength() {
		OBRisk1DeliveryAddress obRisk1DeliveryAddress = new OBRisk1DeliveryAddress();
		obRisk1DeliveryAddress.setAddressLine(null);
		String countrySubDivision = "zsddxdfhdhSDGHre000";
		obRisk1DeliveryAddress.setCountrySubDivision(countrySubDivision);
		commonPaymentValidations.validateInternationalRiskDeliveryAddress(obRisk1DeliveryAddress, null);
	}

	@Test(expected = OBPSD2Exception.class)
	public void validateInternationalRiskDeliveryAddress_invalidCountryCode() {
		OBRisk1DeliveryAddress obRisk1DeliveryAddress = new OBRisk1DeliveryAddress();
		obRisk1DeliveryAddress.setAddressLine(null);
		obRisk1DeliveryAddress.setCountrySubDivision(null);
		obRisk1DeliveryAddress.setCountry("ZZ");
		commonPaymentValidations.validateInternationalRiskDeliveryAddress(obRisk1DeliveryAddress,
				OBErrorCodeEnum.UK_OBIE_FIELD_INVALID);
	}

	@Test(expected = OBPSD2Exception.class)
	public void validateDomesticPaymentContext_merchantCategoryCodeAbsent() {
		OBRisk1 obRisk1 = new OBRisk1();
		obRisk1.setPaymentContextCode(OBExternalPaymentContext1Code.ECOMMERCEGOODS);
		commonPaymentValidations.validateDomesticPaymentContext(obRisk1);
	}

	@Test(expected = OBPSD2Exception.class)
	public void validateDomesticPaymentContext_MerchantCustomerIdentificationAbsent() {
		OBRisk1 obRisk1 = new OBRisk1();
		obRisk1.setMerchantCategoryCode("merchantCategoryCode");
		obRisk1.setPaymentContextCode(OBExternalPaymentContext1Code.ECOMMERCESERVICES);
		commonPaymentValidations.validateDomesticPaymentContext(obRisk1);
	}

	@Test(expected = OBPSD2Exception.class)
	public void validateDomesticPaymentContext_deliveryAddressAbsent() {
		OBRisk1 obRisk1 = new OBRisk1();
		obRisk1.setPaymentContextCode(OBExternalPaymentContext1Code.ECOMMERCEGOODS);
		obRisk1.setMerchantCategoryCode("merchantCategoryCode");
		obRisk1.setMerchantCustomerIdentification("merchantCustomerIdentification");
		commonPaymentValidations.validateDomesticPaymentContext(obRisk1);
	}

	@Test(expected = OBPSD2Exception.class)
	public void validateInternationalPaymentContext_merchantCategoryCodeAbsent() {
		OBRisk1 obRisk1 = new OBRisk1();
		obRisk1.setPaymentContextCode(OBExternalPaymentContext1Code.ECOMMERCEGOODS);
		commonPaymentValidations.validateInternationalPaymentContext(obRisk1);
	}

	@Test(expected = OBPSD2Exception.class)
	public void validateInternationalPaymentContext_MerchantCustomerIdentificationAbsent() {
		OBRisk1 obRisk1 = new OBRisk1();
		obRisk1.setMerchantCategoryCode("merchantCategoryCode");
		obRisk1.setPaymentContextCode(OBExternalPaymentContext1Code.ECOMMERCESERVICES);
		commonPaymentValidations.validateInternationalPaymentContext(obRisk1);
	}

	@Test(expected = OBPSD2Exception.class)
	public void validateInternationalPaymentContext_deliveryAddressAbsent() {
		OBRisk1 obRisk1 = new OBRisk1();
		obRisk1.setPaymentContextCode(OBExternalPaymentContext1Code.ECOMMERCEGOODS);
		obRisk1.setMerchantCategoryCode("merchantCategoryCode");
		obRisk1.setMerchantCustomerIdentification("merchantCustomerIdentification");
		commonPaymentValidations.validateInternationalPaymentContext(obRisk1);
	}

	@Test(expected = OBPSD2Exception.class)
	public void validateCompletionDateTime_expiredDateTime() {
		commonPaymentValidations.validateDateTime("1981-03-20T06:06:06+00:00");
	}

	@Test(expected = OBPSD2Exception.class)
	public void validateCompletionDateTime_offsetAbsent() {
		commonPaymentValidations.validateDateTime("1981-03-20T06:06:06");
	}

	@Test(expected = OBPSD2Exception.class)
	public void validateInstructionPriority_localInstrumentAbsent() {
		OBInternational1 initiation = new OBInternational1();
		initiation.setInstructionPriority(OBPriority2Code.NORMAL);
		initiation.setLocalInstrument("localInstrument");
		OBWriteDataInternationalConsent1 data = new OBWriteDataInternationalConsent1();
		data.setInitiation(initiation);
		OBWriteInternationalConsent1 consent = new OBWriteInternationalConsent1();
		consent.setData(data);
		commonPaymentValidations.validateInstructionPriority(consent);
	}

	@Test(expected = OBPSD2Exception.class)
	public void validateInstructionPriority_agreedExchangeRateType() {
		OBExchangeRate1 exchangeRate = new OBExchangeRate1();
		exchangeRate.setRateType(OBExchangeRateType2Code.AGREED);
		OBInternational1 initiation = new OBInternational1();
		initiation.setExchangeRateInformation(exchangeRate);
		OBWriteDataInternationalConsent1 data = new OBWriteDataInternationalConsent1();
		data.setInitiation(initiation);
		OBWriteInternationalConsent1 consent = new OBWriteInternationalConsent1();
		consent.setData(data);
		commonPaymentValidations.validateInstructionPriority(consent);
	}
}