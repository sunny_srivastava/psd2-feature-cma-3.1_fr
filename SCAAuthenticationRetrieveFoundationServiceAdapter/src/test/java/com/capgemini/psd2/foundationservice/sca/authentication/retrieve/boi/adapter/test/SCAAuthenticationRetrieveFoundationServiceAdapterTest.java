package com.capgemini.psd2.foundationservice.sca.authentication.retrieve.boi.adapter.test;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyObject;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpHeaders;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.adapter.security.constants.AdapterSecurityConstants;
import com.capgemini.psd2.adapter.security.custom.domain.CustomAuthenticationServiceGetResponse;
import com.capgemini.psd2.adapter.security.domain.AuthenticationParameters;
import com.capgemini.psd2.adapter.security.domain.ChannelCode;
import com.capgemini.psd2.adapter.security.domain.DigitalUser;
import com.capgemini.psd2.foundationservice.authentication.application.retrieve.boi.adapter.SCAAuthenticationRetrieveFoundationServiceAdapter;
import com.capgemini.psd2.foundationservice.authentication.application.retrieve.boi.adapter.client.SCAAuthenticationRetrieveFoundationServiceClient;
import com.capgemini.psd2.foundationservice.authentication.application.retrieve.boi.adapter.delegate.SCAAuthenticationRetrieveFoundationServiceDelegate;
import com.capgemini.psd2.foundationservice.authentication.application.retrieve.boi.adapter.transformer.SCAAuthenticationRetrieveFoundationServiceTransformer;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.rest.client.model.RequestInfo;

public class SCAAuthenticationRetrieveFoundationServiceAdapterTest {

	@InjectMocks
	private SCAAuthenticationRetrieveFoundationServiceAdapter scaAuthenticationRetrieveFoundationServiceAdapter;

	@Mock
	private SCAAuthenticationRetrieveFoundationServiceClient scaAuthenticationRetrieveFoundationServiceClient;

	@Mock
	private SCAAuthenticationRetrieveFoundationServiceDelegate scaAuthenticationRetrieveFoundationServiceDelegate;

	@Mock
	private SCAAuthenticationRetrieveFoundationServiceTransformer scaAuthenticationRetrieveFoundationServiceTransformer;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testgetAuthenticate() {

		Map<String, Object> params = new HashMap<String, Object>();

		params.put(PSD2Constants.USER_ID, "gjhgjh8687");
		params.put(AdapterSecurityConstants.EVENTTYPE, "utgutgu");
		params.put(PSD2Constants.CHANNEL_ID, "b365");

		DigitalUser digitalUser = new DigitalUser();
		digitalUser.setDigitalUserIdentifier("hjgj878");
		digitalUser.channelCode(ChannelCode.B365);
		assertNotNull(digitalUser);
		RequestInfo requestInfo = new RequestInfo();

		HttpHeaders httpHeaders = new HttpHeaders();
		ReflectionTestUtils.setField(scaAuthenticationRetrieveFoundationServiceDelegate, "sourceUserInReqHeader",
				"X-API-SOURCE-USER");
		ReflectionTestUtils.setField(scaAuthenticationRetrieveFoundationServiceDelegate, "sourceSystemInReqHeader",
				"X-API-SOURCE-SYSTEM");
		ReflectionTestUtils.setField(scaAuthenticationRetrieveFoundationServiceDelegate, "trasanctionIdInReqHeader",
				"X-API-TRANSANCTION-ID");
		ReflectionTestUtils.setField(scaAuthenticationRetrieveFoundationServiceDelegate, "apiCorrelationIdInReqHeader",
				"X-API-CORRELATION-ID");

		Mockito.when(scaAuthenticationRetrieveFoundationServiceDelegate.createRequestHeaders(anyObject(), anyObject()))
				.thenReturn(httpHeaders);
		Mockito.when(
				scaAuthenticationRetrieveFoundationServiceDelegate.getAuthenticationFoundationServiceURL(any(), any()))
				.thenReturn("http://localhost:8182/hguhguju/uytycgdghd");

		AuthenticationParameters authenticationParameters = new AuthenticationParameters();
		Mockito.when(scaAuthenticationRetrieveFoundationServiceClient
				.restTransportForRetrieveAuthenticationService(any(), any(), any(), any()))
				.thenReturn(authenticationParameters);
		scaAuthenticationRetrieveFoundationServiceAdapter.getAuthenticate(params);
		CustomAuthenticationServiceGetResponse customAuthenticationServiceGetResponse = new CustomAuthenticationServiceGetResponse();
		Mockito.when(scaAuthenticationRetrieveFoundationServiceTransformer.transformAuthenticationResponse(any(), any()))
				.thenReturn(customAuthenticationServiceGetResponse);
	}

	@Test
	public void testgetAuthenticate3() {

		Map<String, Object> params = new HashMap<String, Object>();

		params.put(PSD2Constants.USER_ID, "gjhgjh8687");
		params.put(AdapterSecurityConstants.EVENTTYPE, "utgutgu");
		params.put(PSD2Constants.CHANNEL_ID, "b365");

		DigitalUser digitalUser = new DigitalUser();
		digitalUser.setDigitalUserIdentifier(null);
		digitalUser.channelCode(ChannelCode.B365);
		assertNotNull(digitalUser);
		RequestInfo requestInfo = new RequestInfo();

		HttpHeaders httpHeaders = new HttpHeaders();
		ReflectionTestUtils.setField(scaAuthenticationRetrieveFoundationServiceDelegate, "sourceUserInReqHeader",
				"X-API-SOURCE-USER");
		ReflectionTestUtils.setField(scaAuthenticationRetrieveFoundationServiceDelegate, "sourceSystemInReqHeader",
				"X-API-SOURCE-SYSTEM");
		ReflectionTestUtils.setField(scaAuthenticationRetrieveFoundationServiceDelegate, "trasanctionIdInReqHeader",
				"X-API-TRANSANCTION-ID");
		ReflectionTestUtils.setField(scaAuthenticationRetrieveFoundationServiceDelegate, "apiCorrelationIdInReqHeader",
				"X-API-CORRELATION-ID");

		Mockito.when(scaAuthenticationRetrieveFoundationServiceDelegate.createRequestHeaders(anyObject(), anyObject()))
				.thenReturn(httpHeaders);
		Mockito.when(
				scaAuthenticationRetrieveFoundationServiceDelegate.getAuthenticationFoundationServiceURL(any(), any()))
				.thenReturn("http://localhost:8182/hguhguju/uytycgdghd");
 
		AuthenticationParameters authenticationParameters = new AuthenticationParameters();
		Mockito.when(scaAuthenticationRetrieveFoundationServiceClient
				.restTransportForRetrieveAuthenticationService(any(), any(), any(), any()))
				.thenReturn(authenticationParameters);
		scaAuthenticationRetrieveFoundationServiceAdapter.getAuthenticate(params);
		CustomAuthenticationServiceGetResponse customAuthenticationServiceGetResponse = new CustomAuthenticationServiceGetResponse();
		Mockito.when(scaAuthenticationRetrieveFoundationServiceTransformer.transformAuthenticationResponse(any(), any()))
				.thenReturn(customAuthenticationServiceGetResponse);
	}

	@Test(expected=AdapterException.class)
	public void testgetAuthenticate1() {

		Map<String, Object> params = new HashMap<String, Object>();

		params.put(PSD2Constants.USER_ID, "gjhgjh8687");
		params.put(AdapterSecurityConstants.USER_HEADER,"");
		params.put(AdapterSecurityConstants.EVENTTYPE, "utgutgu");
		scaAuthenticationRetrieveFoundationServiceAdapter.getAuthenticate(params);
	}
}
