package com.capgemini.psd2.foundationservice.authentication.application.retrieve.boi.adapter.transformer;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.capgemini.psd2.adapter.security.constants.AdapterSecurityConstants;
import com.capgemini.psd2.adapter.security.custom.domain.CustomAuthenticationServiceGetResponse;
import com.capgemini.psd2.adapter.security.domain.AuthenticationMethodCode1;
import com.capgemini.psd2.adapter.security.domain.AuthenticationParameters;
import com.capgemini.psd2.adapter.security.domain.CustomerAuthenticationSession;
import com.capgemini.psd2.adapter.security.domain.DeviceStatusCode;
import com.capgemini.psd2.adapter.security.domain.DeviceType;
import com.capgemini.psd2.adapter.security.domain.ElectronicDevices;
import com.capgemini.psd2.adapter.security.domain.EventType1;
import com.capgemini.psd2.utilities.NullCheckUtils;
@Component
public class SCAAuthenticationRetrieveFoundationServiceTransformer {

	public <T> CustomAuthenticationServiceGetResponse transformAuthenticationResponse(T authenticationParams,Map<String, Object> params) {
		CustomAuthenticationServiceGetResponse customAuthenticationServiceGetResponse = new CustomAuthenticationServiceGetResponse();
		AuthenticationParameters authParams = (AuthenticationParameters) authenticationParams;
		
		AuthenticationParameters authenticationParameters = new AuthenticationParameters();
		authenticationParameters.setDigitalUser(authParams.getDigitalUser());
		for(CustomerAuthenticationSession custSession : authParams.getCustomerAuthenticationSessions() ) {
			
				custSession.setEventType(EventType1.fromValue(params.get(AdapterSecurityConstants.EVENTTYPE).toString()));
			
		}
		authenticationParameters.setCustomerAuthenticationSessions(authParams.getCustomerAuthenticationSessions());
		
		/*Device list filter based on condition.*/
		List<ElectronicDevices> elecDevices = new ArrayList<ElectronicDevices>();
		ElectronicDevices elecDevice = null;
		if (!NullCheckUtils.isNullOrEmpty(authParams.getElectronicDevices())) {
			elecDevice = new ElectronicDevices();
			for (ElectronicDevices electronicDevice : authParams.getElectronicDevices()) {
				if (!NullCheckUtils.isNullOrEmpty(electronicDevice.getDeviceType())
						&& !NullCheckUtils.isNullOrEmpty(electronicDevice.getDeviceStatusCode())) {
					if (electronicDevice.getDeviceStatusCode().equals(DeviceStatusCode.ACTIVE)
						&& electronicDevice.getDeviceType().equals(DeviceType.SOFT_TOKEN)) {

						elecDevices.add(electronicDevice);
					}
				}
			}
		}
		authenticationParameters.setElectronicDevices(elecDevices);
		customAuthenticationServiceGetResponse.setAuthenticationParameters(authenticationParameters);
		
		return customAuthenticationServiceGetResponse;
	}
}
