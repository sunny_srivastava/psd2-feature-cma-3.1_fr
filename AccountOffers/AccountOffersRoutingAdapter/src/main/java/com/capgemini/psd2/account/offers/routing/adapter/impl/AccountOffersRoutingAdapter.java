package com.capgemini.psd2.account.offers.routing.adapter.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.account.offers.routing.adapter.routing.AccountOffersAdapterFactory;
import com.capgemini.psd2.aisp.adapter.AccountOffersAdapter;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountOffersResponse;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Component("accountOffersRoutingAdapter")
public class AccountOffersRoutingAdapter implements AccountOffersAdapter {

	@Autowired
	private AccountOffersAdapterFactory factory;
	
	@Autowired
	private RequestHeaderAttributes reqHeaderAtrributes;

	@Value("${app.defaultAccountOffersAdapter}")
	private String defaultAdapter;

	@Override
	public PlatformAccountOffersResponse retrieveAccountOffers(AccountMapping accountMapping,
			Map<String, String> params) {
		

		AccountOffersAdapter offersAdapter = factory.getAdapterInstance(defaultAdapter);

		return offersAdapter.retrieveAccountOffers(accountMapping, addHeaderParams(params));
	}

	private Map<String, String> addHeaderParams(Map<String, String> mapParam) {
		Map<String,String> mapParamResult;
		if (NullCheckUtils.isNullOrEmpty(mapParam))
			mapParamResult = new HashMap<>(); 
		else mapParamResult=mapParam;

		if (null != reqHeaderAtrributes.getToken() && null != reqHeaderAtrributes.getToken().getSeviceParams()) {
			mapParamResult.put(PSD2Constants.CHANNEL_IN_REQ_HEADER,
					reqHeaderAtrributes.getToken().getSeviceParams().get(PSD2Constants.CHANNEL_NAME));
		}
		mapParamResult.put(PSD2Constants.USER_IN_REQ_HEADER, reqHeaderAtrributes.getPsuId());
		mapParamResult.put(PSD2Constants.CORRELATION_REQ_HEADER, reqHeaderAtrributes.getCorrelationId());
		mapParamResult.put(PSD2Constants.TENANT_ID, reqHeaderAtrributes.getTenantId());
		return mapParamResult;
	}
}
