package com.capgemini.psd2.account.offers.routing.adapter.test.mock.data;

import java.util.ArrayList;
import java.util.List;

import com.capgemini.psd2.aisp.domain.OBReadOffer1;
import com.capgemini.psd2.aisp.domain.OBReadOffer1Data;
import com.capgemini.psd2.aisp.domain.OBReadOffer1DataOffer;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountOffersResponse;

public class AccountOffersRoutingAdapterTestMockData {
	
	public static PlatformAccountOffersResponse getMockOffersGETResponse() {
		
		PlatformAccountOffersResponse platformAccountOffersResponse = new PlatformAccountOffersResponse();
		OBReadOffer1Data data = new OBReadOffer1Data();
		List<OBReadOffer1DataOffer> offers = new ArrayList<>();
		data.setOffer(offers);
		OBReadOffer1 obReadOffer1 = new OBReadOffer1();
		obReadOffer1.setData(data);
		platformAccountOffersResponse.setoBReadOffer1(obReadOffer1);
		return platformAccountOffersResponse;
	}

}
