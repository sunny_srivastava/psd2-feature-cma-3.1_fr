package com.capgemini.psd2.account.offers.mongo.db.adapter.test.mock.data;

import java.util.ArrayList;
import java.util.List;

import com.capgemini.psd2.aisp.domain.OBReadOffer1;
import com.capgemini.psd2.aisp.domain.OBReadOffer1Data;
import com.capgemini.psd2.aisp.domain.OBReadOffer1DataOffer;
import com.capgemini.psd2.aisp.mongo.db.adapter.domain.AccountOffersCMA2;
import com.capgemini.psd2.logger.LoggerAttribute;

public class AccountOffersAdapterTestMockData {

	public static LoggerAttribute getMockLoggerData() {
		LoggerAttribute x = new LoggerAttribute();
		x.setApiId("testApiID");
		return x;
	}

	public static OBReadOffer1 getMockExpectedAccountOffersResponse() {

		OBReadOffer1 obReadOffer1 = new OBReadOffer1();
		OBReadOffer1Data obReadOffer1Data = new OBReadOffer1Data();
		List<AccountOffersCMA2> mockAccountOfferList = new ArrayList<>();

		mockAccountOfferList.add(new AccountOffersCMA2());
		mockAccountOfferList.get(0).setAccountId("d9d9c3dc-1427-4f03-92a7-7916fcf4cc0d");

		List<OBReadOffer1DataOffer> obOfferList = new ArrayList<>();
		obOfferList.addAll(mockAccountOfferList);
		obReadOffer1Data.setOffer(obOfferList);
		obReadOffer1.setData(obReadOffer1Data);

		return obReadOffer1;
	}

	public static OBReadOffer1 getMockExpectedAccountOffersNullResponse() {

		OBReadOffer1 obReadOffer1 = new OBReadOffer1();
		OBReadOffer1Data obReadOffer1Data = new OBReadOffer1Data();
		List<AccountOffersCMA2> mockAccountOfferList = new ArrayList<>();

		mockAccountOfferList.add(new AccountOffersCMA2());
		mockAccountOfferList.get(0).setAccountId("123");

		List<OBReadOffer1DataOffer> obOfferList = new ArrayList<>();
		obOfferList.addAll(mockAccountOfferList);
		obReadOffer1Data.setOffer(obOfferList);
		obReadOffer1.setData(obReadOffer1Data);

		return obReadOffer1;
	}

}
