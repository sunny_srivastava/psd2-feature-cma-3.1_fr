package com.capgemini.psd2.account.offer.mongo.db.adapter.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessResourceFailureException;

import com.capgemini.psd2.account.offer.mongo.db.adapter.repository.AccountOffersRepository;
import com.capgemini.psd2.aisp.adapter.AccountOffersAdapter;
import com.capgemini.psd2.aisp.domain.OBReadOffer1;
import com.capgemini.psd2.aisp.domain.OBReadOffer1Data;
import com.capgemini.psd2.aisp.domain.OBReadOffer1DataOffer;
import com.capgemini.psd2.aisp.mongo.db.adapter.domain.AccountOffersCMA2;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountOffersResponse;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.InternalServerErrorMessage;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.LoggerUtils;
import com.capgemini.psd2.logger.PSD2Constants;

public class AccountOffersMongoDbAdapterImpl implements AccountOffersAdapter {

	@Autowired
	private AccountOffersRepository repository;

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(AccountOffersMongoDbAdapterImpl.class);

	/** The logger utils. */
	@Autowired
	private LoggerUtils loggerUtils;

	@Override
	public PlatformAccountOffersResponse retrieveAccountOffers(AccountMapping accountMapping,
			Map<String, String> params) {
		
		LOG.info("{\"Enter\":\"{}\",\"{}\"}",
				"com.capgemini.psd2.account.product.mongo.db.adapter.impl.retrieveAccountOffers()",
				loggerUtils.populateLoggerData("retrieveAccountOffers"));

		PlatformAccountOffersResponse platformAccountOffersResponse = new PlatformAccountOffersResponse();
		OBReadOffer1 obreadOffer1 = new OBReadOffer1();
		List<AccountOffersCMA2>  mockOffers = null;
		LOG.info("TenantId : " + params.get(PSD2Constants.TENANT_ID));
		
		try {
			mockOffers = repository.findByAccountNumberAndAccountNSC(
					accountMapping.getAccountDetails().get(0).getAccountNumber(),
					accountMapping.getAccountDetails().get(0).getAccountNSC());
		} catch (DataAccessResourceFailureException e) {
			LOG.error(e.getMessage(),e);
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,InternalServerErrorMessage.CONNECTION_ERROR));
		}
		List<OBReadOffer1DataOffer> offerList = new ArrayList<>();
		
		if (mockOffers != null) {
			for(AccountOffersCMA2 obj: mockOffers) {
				obj.setAccountId(accountMapping.getAccountDetails().get(0).getAccountId());
				offerList.add(obj);
			}
		}
       
		OBReadOffer1Data obReadOffer1Data = new OBReadOffer1Data();
		obReadOffer1Data.setOffer(offerList);
		obreadOffer1.setData(obReadOffer1Data);
		platformAccountOffersResponse.setoBReadOffer1(obreadOffer1);
		return platformAccountOffersResponse;
	}

}
