package com.capgemini.psd2.account.offers.service;

import com.capgemini.psd2.aisp.domain.OBReadOffer1;

public interface AccountOffersService {
	
	public OBReadOffer1 retrieveAccountOffers(String accountId);

}
