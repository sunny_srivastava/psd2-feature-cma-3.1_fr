package com.capgemini.psd2.account.offers.service.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.capgemini.psd2.account.offers.service.AccountOffersService;
import com.capgemini.psd2.aisp.account.mapping.adapter.AccountMappingAdapter;
import com.capgemini.psd2.aisp.adapter.AccountOffersAdapter;
import com.capgemini.psd2.aisp.adapter.AispConsentAdapter;
import com.capgemini.psd2.aisp.domain.Links;
import com.capgemini.psd2.aisp.domain.Meta;
import com.capgemini.psd2.aisp.domain.OBReadOffer1;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountOffersResponse;
import com.capgemini.psd2.aisp.validation.adapter.AISPCustomValidator;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.consent.domain.AispConsent;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;

@Service
public class AccountOffersServiceImpl implements AccountOffersService {
	
	
	@Autowired
	@Qualifier("accountOffersRoutingAdapter")
	private AccountOffersAdapter adapter;

	@Autowired
	private RequestHeaderAttributes reqHeaderAtrributes;

	@Autowired
	private AccountMappingAdapter accountMappingAdapter;

	@Autowired
	private AispConsentAdapter aispConsentAdapter;	
	
	@SuppressWarnings("rawtypes")
	@Autowired
	@Qualifier("accountsRoutingValidator")
	private AISPCustomValidator accountsCustomValidator;

	@Override
	public OBReadOffer1 retrieveAccountOffers(String accountId) {
		
		accountsCustomValidator.validateUniqueId(accountId);
		
		AispConsent aispConsent = aispConsentAdapter.validateAndRetrieveConsentByAccountId(accountId,
				reqHeaderAtrributes.getToken().getConsentTokenData().getConsentId());//to change it

		AccountMapping accountMapping = accountMappingAdapter.retrieveAccountMappingDetails(accountId, aispConsent);
		
		Map<String,String> params=reqHeaderAtrributes.getToken().getSeviceParams();
		params.put(PSD2Constants.CMAVERSION, aispConsent.getCmaVersion());
		params.put(PSD2Constants.CO_RELATION_ID, reqHeaderAtrributes.getCorrelationId());

		PlatformAccountOffersResponse platformAccountOffersResponse = adapter.retrieveAccountOffers(accountMapping,params);

		OBReadOffer1 tppOfferResponse = platformAccountOffersResponse.getoBReadOffer1();

		updatePlatformResponse(tppOfferResponse);
		
		accountsCustomValidator.validateResponseParams(tppOfferResponse);

		return tppOfferResponse;
	}

	private void updatePlatformResponse(OBReadOffer1 tppOfferResponse) {
		if (tppOfferResponse.getLinks() == null) {
			tppOfferResponse.setLinks(new Links());
			tppOfferResponse.getLinks().setSelf(reqHeaderAtrributes.getSelfUrl());
		}
		if (tppOfferResponse.getMeta() == null) {
			tppOfferResponse.setMeta(new Meta());
			tppOfferResponse.getMeta().setTotalPages(1);
		}
	}

}
