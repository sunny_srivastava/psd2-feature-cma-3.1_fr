package com.capgemini.psd2.pisp.file.payment.setup.test.service.impl;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.multipart.MultipartFile;

import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.adapter.FilePaymentConsentsAdapter;
import com.capgemini.psd2.pisp.domain.CustomFilePaymentConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.CustomFilePaymentSetupPOSTRequest;
import com.capgemini.psd2.pisp.domain.OBCashAccountDebtor3;
import com.capgemini.psd2.pisp.domain.OBExternalConsentStatus2Code;
import com.capgemini.psd2.pisp.domain.OBFile1;
import com.capgemini.psd2.pisp.domain.OBWriteDataFileConsent1;
import com.capgemini.psd2.pisp.domain.OBWriteDataFileConsentResponse1;
import com.capgemini.psd2.pisp.domain.PaymentConsentsPlatformResource;
import com.capgemini.psd2.pisp.domain.PaymentRetrieveGetRequest;
import com.capgemini.psd2.pisp.domain.PlatformFilePaymentsFileResponse;
import com.capgemini.psd2.pisp.enums.ProcessConsentStatusEnum;
import com.capgemini.psd2.pisp.file.payment.setup.comparator.FilePaymentConsentsPayloadComparator;
import com.capgemini.psd2.pisp.file.payment.setup.service.impl.FilePaymentsConsentsServiceImpl;
import com.capgemini.psd2.pisp.file.payment.setup.transformer.FPaymentConsentsResponseTransformer;
import com.capgemini.psd2.pisp.file.payment.setup.transformer.FileTransformer;
import com.capgemini.psd2.pisp.file.payment.setup.utilities.FilePaymentConsentsUtilities;
import com.capgemini.psd2.pisp.file.payment.setup.utilities.JWSVerificationUtility;
import com.capgemini.psd2.pisp.payment.setup.platform.adapter.PaymentSetupPlatformAdapter;
import com.capgemini.psd2.pisp.payment.setup.platform.adapter.repository.PaymentSetupPlatformRepository;

@RunWith(SpringJUnit4ClassRunner.class)
public class FilePaymentsConsentsServiceImplTest {

	@Mock
	private FilePaymentConsentsAdapter filePaymentCoreAdapter;

	@Mock
	private PaymentSetupPlatformAdapter paymentSetupPlatformAdapter;

	@Mock
	private RequestHeaderAttributes requestHeaderAttr;
	
	@Mock
	private FilePaymentConsentsUtilities utility;

	@Mock
	private FilePaymentConsentsPayloadComparator filePaymentConsentsComparator;

	@Mock
	private FPaymentConsentsResponseTransformer fPaymentConsentsResponseTransformer;

	@Mock
	private FileTransformer fileTransformer;

	@InjectMocks
	private FilePaymentsConsentsServiceImpl consentService = new FilePaymentsConsentsServiceImpl("24H");

	@Mock
	private RequestHeaderAttributes reqHeaderAtrributes;

	@Mock
	private PaymentSetupPlatformRepository paymentSetupPlatformRepository;

	@Mock
	private JWSVerificationUtility jwsVerificationUtility;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		reqHeaderAtrributes.setTppCID("TestTPP");
	}

	@Test
	public void contextLoads() {

	}

	@Test
	public void testCreateFilePaymentConsentsResource() throws Exception {

		CustomFilePaymentSetupPOSTRequest paymentRequest = new CustomFilePaymentSetupPOSTRequest();
		boolean isPaymentValidated = true;
		PaymentConsentsPlatformResource paymentConsentsPlatformResponse = new PaymentConsentsPlatformResource();
		CustomFilePaymentConsentsPOSTResponse customeResponse = new CustomFilePaymentConsentsPOSTResponse();
		OBWriteDataFileConsentResponse1 dataResponse = new OBWriteDataFileConsentResponse1();
		OBWriteDataFileConsent1 data = new OBWriteDataFileConsent1();

		OBFile1 initiation = new OBFile1();
		data.setInitiation(initiation);
		initiation.setLocalInstrument("111");
		paymentRequest.setData(data);

		dataResponse.setInitiation(initiation);
		customeResponse.setData(dataResponse);
		customeResponse.getData().setStatus(OBExternalConsentStatus2Code.AWAITINGAUTHORISATION);
		Mockito.when(paymentSetupPlatformAdapter.getIdempotentPaymentSetupPlatformResource(anyString(), anyLong()))
				.thenReturn(paymentConsentsPlatformResponse);

		Mockito.when(filePaymentConsentsComparator.compareFilePaymentDetails(anyObject(), anyObject(), anyObject()))
				.thenReturn(0);

		Mockito.when(fPaymentConsentsResponseTransformer.filePaymentConsentsResponseTransformer(anyObject(),
				anyObject(), anyString())).thenReturn(customeResponse);
		Mockito.when(filePaymentCoreAdapter.createStagingFilePaymentConsents(anyObject(), anyObject(), anyObject()))
				.thenReturn(customeResponse);
		Mockito.when(paymentSetupPlatformAdapter.createPaymentSetupPlatformResource(anyObject(), anyObject()))
				.thenReturn(paymentConsentsPlatformResponse);

		consentService.createFilePaymentConsentsResource(paymentRequest, isPaymentValidated);

	}

	@Test
	public void testCreateFilePaymentConsentsResourceValidFalse() throws Exception {

		CustomFilePaymentSetupPOSTRequest paymentRequest = new CustomFilePaymentSetupPOSTRequest();
		boolean isPaymentValidated = false;
		PaymentConsentsPlatformResource paymentConsentsPlatformResponse = new PaymentConsentsPlatformResource();
		CustomFilePaymentConsentsPOSTResponse customeResponse = new CustomFilePaymentConsentsPOSTResponse();
		OBWriteDataFileConsentResponse1 dataResponse = new OBWriteDataFileConsentResponse1();
		OBWriteDataFileConsent1 data = new OBWriteDataFileConsent1();

		OBFile1 initiation = new OBFile1();
		data.setInitiation(initiation);
		initiation.setLocalInstrument("111");
		paymentRequest.setData(data);

		dataResponse.setInitiation(initiation);
		customeResponse.setData(dataResponse);
		customeResponse.getData().setStatus(OBExternalConsentStatus2Code.AWAITINGAUTHORISATION);
		Mockito.when(paymentSetupPlatformAdapter.getIdempotentPaymentSetupPlatformResource(anyString(), anyLong()))
				.thenReturn(paymentConsentsPlatformResponse);

		Mockito.when(filePaymentConsentsComparator.compareFilePaymentDetails(anyObject(), anyObject(), anyObject()))
				.thenReturn(0);

		Mockito.when(fPaymentConsentsResponseTransformer.filePaymentConsentsResponseTransformer(anyObject(),
				anyObject(), anyString())).thenReturn(customeResponse);
		Mockito.when(filePaymentCoreAdapter.createStagingFilePaymentConsents(anyObject(), anyObject(), anyObject()))
				.thenReturn(customeResponse);
		Mockito.when(paymentSetupPlatformAdapter.createPaymentSetupPlatformResource(anyObject(), anyObject()))
				.thenReturn(paymentConsentsPlatformResponse);
		PaymentConsentsPlatformResource platformResource = null;
		when(paymentSetupPlatformAdapter.createPaymentSetupPlatformResource(anyObject(), anyString()))
				.thenReturn(platformResource);
		consentService.createFilePaymentConsentsResource(paymentRequest, isPaymentValidated);

	}

	@Test(expected = PSD2Exception.class)
	public void testCreateFilePaymentConsentsResourceStageNull() throws Exception {

		CustomFilePaymentSetupPOSTRequest paymentRequest = new CustomFilePaymentSetupPOSTRequest();
		boolean isPaymentValidated = true;
		PaymentConsentsPlatformResource paymentConsentsPlatformResponse = new PaymentConsentsPlatformResource();
		CustomFilePaymentConsentsPOSTResponse customeResponse = null;
		OBWriteDataFileConsentResponse1 dataResponse = new OBWriteDataFileConsentResponse1();
		OBWriteDataFileConsent1 data = new OBWriteDataFileConsent1();

		OBFile1 initiation = new OBFile1();
		data.setInitiation(initiation);
		initiation.setLocalInstrument("111");
		paymentRequest.setData(data);

		dataResponse.setInitiation(initiation);
		Mockito.when(paymentSetupPlatformAdapter.getIdempotentPaymentSetupPlatformResource(anyString(), anyLong()))
				.thenReturn(paymentConsentsPlatformResponse);

		Mockito.when(filePaymentConsentsComparator.compareFilePaymentDetails(anyObject(), anyObject(), anyObject()))
				.thenReturn(0);

		Mockito.when(fPaymentConsentsResponseTransformer.filePaymentConsentsResponseTransformer(anyObject(),
				anyObject(), anyString())).thenReturn(customeResponse);
		Mockito.when(filePaymentCoreAdapter.createStagingFilePaymentConsents(anyObject(), anyObject(), anyObject()))
				.thenReturn(customeResponse);
		Mockito.when(paymentSetupPlatformAdapter.createPaymentSetupPlatformResource(anyObject(), anyObject()))
				.thenReturn(paymentConsentsPlatformResponse);
		PaymentConsentsPlatformResource platformResource = null;
		when(paymentSetupPlatformAdapter.createPaymentSetupPlatformResource(anyObject(), anyString()))
				.thenReturn(platformResource);
		when(paymentSetupPlatformAdapter.createInvalidPaymentSetupPlatformResource(anyObject(), anyObject(),
				anyObject())).thenReturn(paymentConsentsPlatformResponse);
		/* stage data is null */
		consentService.createFilePaymentConsentsResource(paymentRequest, isPaymentValidated);

	}

	@Test
	public void testCreateFilePaymentConsentsResourceFulllFlow() throws Exception {

		CustomFilePaymentSetupPOSTRequest paymentRequest = new CustomFilePaymentSetupPOSTRequest();
		boolean isPaymentValidated = true;
		PaymentConsentsPlatformResource paymentConsentsPlatformResponse = new PaymentConsentsPlatformResource();
		CustomFilePaymentConsentsPOSTResponse customeResponse = null;
		CustomFilePaymentConsentsPOSTResponse consentResponse = new CustomFilePaymentConsentsPOSTResponse();
		OBWriteDataFileConsentResponse1 dataResponse = new OBWriteDataFileConsentResponse1();
		OBWriteDataFileConsent1 data = new OBWriteDataFileConsent1();

		OBFile1 initiation = new OBFile1();
		OBCashAccountDebtor3 debtorAccount = new OBCashAccountDebtor3();
		debtorAccount.setName("UK.OBIE.BBAN");
		initiation.setDebtorAccount(debtorAccount);
		data.setInitiation(initiation);
		initiation.setLocalInstrument("111");
		paymentRequest.setData(data);

		dataResponse.setConsentId("2267ca1b-77ff-4fe8-af54-9eb969d03ab8");
		dataResponse.setInitiation(initiation);
		dataResponse.setStatus(OBExternalConsentStatus2Code.AWAITINGUPLOAD);
		consentResponse.setData(dataResponse);
		paymentConsentsPlatformResponse.setPaymentConsentId("2267ca1b-77ff-4fe8-af54-9eb969d03ab8");

		Mockito.when(paymentSetupPlatformAdapter.getIdempotentPaymentSetupPlatformResource(anyString(), anyLong()))
				.thenReturn(paymentConsentsPlatformResponse);

		Mockito.when(filePaymentConsentsComparator.compareFilePaymentDetails(anyObject(), anyObject(), anyObject()))
				.thenReturn(0);

		Mockito.when(fPaymentConsentsResponseTransformer.filePaymentConsentsResponseTransformer(anyObject(),
				anyObject(), anyString())).thenReturn(customeResponse);
		Mockito.when(filePaymentCoreAdapter.createStagingFilePaymentConsents(anyObject(), anyObject(), anyObject()))
				.thenReturn(consentResponse);
		Mockito.when(paymentSetupPlatformAdapter.createPaymentSetupPlatformResource(anyObject(), anyString()))
				.thenReturn(paymentConsentsPlatformResponse);

		when(paymentSetupPlatformAdapter.createInvalidPaymentSetupPlatformResource(anyObject(), anyObject(),
				anyObject())).thenReturn(paymentConsentsPlatformResponse);
		when(paymentSetupPlatformRepository.save(any(PaymentConsentsPlatformResource.class)))
				.thenReturn(paymentConsentsPlatformResponse);
		consentService.createFilePaymentConsentsResource(paymentRequest, isPaymentValidated);

	}

	@Test
	public void testPopulateFilePlatformDetails() throws Exception {
		OBWriteDataFileConsent1 data = new OBWriteDataFileConsent1();
		CustomFilePaymentSetupPOSTRequest paymentConsentsRequest = new CustomFilePaymentSetupPOSTRequest();
		OBFile1 initiation = new OBFile1();
		OBCashAccountDebtor3 debtorAccount = new OBCashAccountDebtor3();
		debtorAccount.setName("ACME Inc");
		initiation.setDebtorAccount(debtorAccount);
		initiation.setFileHash("CCiFOEK4V/cU/z0wNKs4MC1ElwBf0qQf23qVzNC/tHU=");
		data.setInitiation(initiation);
		paymentConsentsRequest.setData(data);

		consentService.populateFilePlatformDetails(paymentConsentsRequest);
	}

	@Test
	public void testPopulateStageDetailsNull() throws Exception {

		CustomFilePaymentConsentsPOSTResponse fileStagingResource = null;
		consentService.populateStageDetails(fileStagingResource);
	}

	@Test
	public void testPopulateStageDetails() throws Exception {

		CustomFilePaymentConsentsPOSTResponse fileStagingResource = new CustomFilePaymentConsentsPOSTResponse();
		OBWriteDataFileConsentResponse1 data = new OBWriteDataFileConsentResponse1();
		fileStagingResource.setData(data);
		fileStagingResource.getData().setStatus(OBExternalConsentStatus2Code.AUTHORISED);
		consentService.populateStageDetails(fileStagingResource);
	}

	@Test
	public void testPopulateStageDetailsRejected() throws Exception {

		CustomFilePaymentConsentsPOSTResponse fileStagingResource = new CustomFilePaymentConsentsPOSTResponse();
		OBWriteDataFileConsentResponse1 data = new OBWriteDataFileConsentResponse1();
		fileStagingResource.setData(data);
		fileStagingResource.getData().setStatus(OBExternalConsentStatus2Code.REJECTED);
		fileStagingResource.setConsentProcessStatus(ProcessConsentStatusEnum.FAIL);
		consentService.populateStageDetails(fileStagingResource);
	}

	@Test
	public void testUploadPaymentConsentFile() throws Exception {

		MultipartFile file = new MultipartFile() {

			@Override
			public void transferTo(File dest) throws IOException, IllegalStateException {

			}

			@Override
			public boolean isEmpty() {
				return false;
			}

			@Override
			public long getSize() {
				return 0;
			}

			@Override
			public String getOriginalFilename() {
				return null;
			}

			@Override
			public String getName() {
				return null;
			}

			@Override
			public InputStream getInputStream() throws IOException {
				return null;
			}

			@Override
			public String getContentType() {
				return null;
			}

			@Override
			public byte[] getBytes() throws IOException {
				return null;
			}
		};

		String consentId = "2588f1ec-68ca-4e72-a345-eac4804372bb";

		CustomFilePaymentConsentsPOSTResponse consentResponse = new CustomFilePaymentConsentsPOSTResponse();
		OBWriteDataFileConsentResponse1 data = new OBWriteDataFileConsentResponse1();
		OBFile1 initiation = new OBFile1();
		initiation.setFileType("UK.OBIE.pain.001.001.08");
		data.setInitiation(initiation);
		consentResponse.setData(data);
		PaymentConsentsPlatformResource platformResource = new PaymentConsentsPlatformResource();
		platformResource.setStatus(OBExternalConsentStatus2Code.AWAITINGUPLOAD.toString());
		Mockito.when(paymentSetupPlatformAdapter.retrievePaymentSetupPlatformResource(anyString()))
				.thenReturn(platformResource);
		Mockito.when(paymentSetupPlatformAdapter.getIdempotentFileUploadSetupPlatformResource(anyString(), anyLong(),
				anyString())).thenReturn(platformResource);

		Mockito.when(filePaymentCoreAdapter.fetchFilePaymentMetadata(anyObject(), anyObject()))
				.thenReturn(consentResponse);
		byte[] fileHash = new byte[] {};
		Mockito.when(utility.generateFileHash(anyObject())).thenReturn(fileHash);
		Mockito.when(utility.base64DecodeHash(anyObject())).thenReturn(fileHash);
		when(fileTransformer.fileTypeChecker(anyObject(), anyString())).thenReturn(true);
		ReflectionTestUtils.setField(consentService, "enableSignature", Boolean.TRUE);
		doNothing().when(jwsVerificationUtility).validateSignature(anyString(), anyString());
		
		consentService.uploadPaymentConsentFile(file, consentId);
	}

	@Test(expected = PSD2Exception.class)
	public void testUploadPaymentConsentFileExc() throws Exception {

		MultipartFile file = new MultipartFile() {

			@Override
			public void transferTo(File dest) throws IOException, IllegalStateException {

			}

			@Override
			public boolean isEmpty() {
				return false;
			}

			@Override
			public long getSize() {
				return 0;
			}

			@Override
			public String getOriginalFilename() {
				return null;
			}

			@Override
			public String getName() {
				return null;
			}

			@Override
			public InputStream getInputStream() throws IOException {
				return null;
			}

			@Override
			public String getContentType() {
				return null;
			}

			@Override
			public byte[] getBytes() throws IOException {
				return null;
			}
		};

		String consentId = "2588f1ec-68ca-4e72-a345-eac4804372bb";

		CustomFilePaymentConsentsPOSTResponse consentResponse = new CustomFilePaymentConsentsPOSTResponse();
		OBWriteDataFileConsentResponse1 data = new OBWriteDataFileConsentResponse1();
		OBFile1 initiation = new OBFile1();
		initiation.setFileType("UK.OBIE.pain.001.001.08");
		data.setInitiation(initiation);
		consentResponse.setData(data);
		PaymentConsentsPlatformResource platformResource = new PaymentConsentsPlatformResource();
		platformResource.setStatus(OBExternalConsentStatus2Code.AWAITINGUPLOAD.toString());
		Mockito.when(paymentSetupPlatformAdapter.retrievePaymentSetupPlatformResource(anyString()))
				.thenReturn(platformResource);
		Mockito.when(paymentSetupPlatformAdapter.getIdempotentFileUploadSetupPlatformResource(anyString(), anyLong(),
				anyString())).thenReturn(platformResource);

		Mockito.when(filePaymentCoreAdapter.fetchFilePaymentMetadata(anyObject(), anyObject()))
				.thenReturn(consentResponse);
		byte[] fileHash = new byte[] {};
		Mockito.when(utility.generateFileHash(anyObject())).thenReturn(fileHash);
		Mockito.when(utility.base64DecodeHash(anyObject())).thenReturn(fileHash);
		ReflectionTestUtils.setField(consentService, "enableSignature", Boolean.TRUE);
		doNothing().when(jwsVerificationUtility).validateSignature(anyString(), anyString());
		
		when(fileTransformer.fileTypeChecker(anyObject(), anyString())).thenReturn(false);
		consentService.uploadPaymentConsentFile(file, consentId);
	}

	@Test
	public void testRetrieveFilePaymentConsentsResource() throws Exception {

		PaymentRetrieveGetRequest paymentRetrieveRequest = new PaymentRetrieveGetRequest();
		PaymentConsentsPlatformResource platformResource = new PaymentConsentsPlatformResource();
		CustomFilePaymentConsentsPOSTResponse response = new CustomFilePaymentConsentsPOSTResponse();

		platformResource.setTppCID("TestTPP");
		reqHeaderAtrributes.setTppCID("TestTPP");

		when(paymentSetupPlatformAdapter.retrievePaymentSetupPlatformResource(anyString()))
				.thenReturn(platformResource);
		when(filePaymentCoreAdapter.fetchFilePaymentMetadata(anyObject(), anyObject())).thenReturn(response);
		when(fPaymentConsentsResponseTransformer.filePaymentConsentsResponseTransformer(anyObject(), anyObject(),
				anyString())).thenReturn(response);
		when(paymentSetupPlatformAdapter.retrievePaymentSetupPlatformResourceByIdAndType(anyString(), anyString()))
				.thenReturn(platformResource);
		Mockito.when(reqHeaderAtrributes.getTppCID()).thenReturn("TestTPP");
		consentService.retrieveFilePaymentConsentsResource(paymentRetrieveRequest);
	}

	@Test(expected = PSD2Exception.class)
	public void testRetrieveFilePaymentConsentsResourceException() throws Exception {

		PaymentRetrieveGetRequest paymentRetrieveRequest = new PaymentRetrieveGetRequest();
		PaymentConsentsPlatformResource platformResource = null;
		CustomFilePaymentConsentsPOSTResponse response = new CustomFilePaymentConsentsPOSTResponse();

		when(paymentSetupPlatformAdapter.retrievePaymentSetupPlatformResource(anyString()))
				.thenReturn(platformResource);
		when(paymentSetupPlatformAdapter.retrievePaymentSetupPlatformResourceByIdAndType(anyString(), anyString()))
				.thenReturn(platformResource);
		when(filePaymentCoreAdapter.fetchFilePaymentMetadata(anyObject(), anyObject())).thenReturn(response);
		when(fPaymentConsentsResponseTransformer.filePaymentConsentsResponseTransformer(anyObject(), anyObject(),
				anyString())).thenReturn(response);
		consentService.retrieveFilePaymentConsentsResource(paymentRetrieveRequest);
	}

	@Test
	public void testDownloadTransactionFileByConsentId() throws Exception {

		PaymentConsentsPlatformResource platformResource = new PaymentConsentsPlatformResource();
		PlatformFilePaymentsFileResponse fileResponse = new PlatformFilePaymentsFileResponse();
		platformResource.setStatus(OBExternalConsentStatus2Code.AWAITINGAUTHORISATION.toString());

		when(paymentSetupPlatformAdapter.retrievePaymentSetupPlatformResource(anyString()))
				.thenReturn(platformResource);
		when(filePaymentCoreAdapter.downloadFilePaymentConsents(anyObject())).thenReturn(fileResponse);
		when(paymentSetupPlatformAdapter.retrievePaymentSetupPlatformResourceByIdAndType(anyString(), anyString()))
				.thenReturn(platformResource);

		consentService.downloadTransactionFileByConsentId("2267ca1b-77ff-4fe8-af54-9eb969d03abb");
	}

	@Test(expected = PSD2Exception.class)
	public void testDownloadTransactionFileByConsentIdException() throws Exception {

		PaymentConsentsPlatformResource platformResource = null;
		PlatformFilePaymentsFileResponse fileResponse = new PlatformFilePaymentsFileResponse();

		when(paymentSetupPlatformAdapter.retrievePaymentSetupPlatformResource(anyString()))
				.thenReturn(platformResource);
		when(filePaymentCoreAdapter.downloadFilePaymentConsents(anyObject())).thenReturn(fileResponse);

		consentService.downloadTransactionFileByConsentId("2267ca1b-77ff-4fe8-af54-9eb969d03abb");
	}

}
