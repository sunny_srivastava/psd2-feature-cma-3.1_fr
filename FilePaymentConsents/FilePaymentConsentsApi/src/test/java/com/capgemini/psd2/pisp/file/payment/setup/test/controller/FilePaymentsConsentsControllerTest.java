package com.capgemini.psd2.pisp.file.payment.setup.test.controller;

import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.capgemini.psd2.pisp.domain.CustomFilePaymentConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.CustomFilePaymentSetupPOSTRequest;
import com.capgemini.psd2.pisp.domain.OBFile1;
import com.capgemini.psd2.pisp.domain.OBWriteDataFileConsent1;
import com.capgemini.psd2.pisp.domain.PlatformFilePaymentsFileResponse;
import com.capgemini.psd2.pisp.file.payment.setup.controller.FilePaymentsConsentsController;
import com.capgemini.psd2.pisp.file.payment.setup.service.FilePaymentsConsentsService;
import com.capgemini.psd2.pisp.file.payment.setup.transformer.FPaymentConsentsResponseTransformer;
import com.capgemini.psd2.pisp.file.payments.test.mock.data.FilePaymentConsentPOSTRequestResponseMockData;
import com.capgemini.psd2.pisp.validation.adapter.impl.FilePaymentValidatorImpl;

@RunWith(SpringJUnit4ClassRunner.class)
public class FilePaymentsConsentsControllerTest {

	private MockMvc mockMvc;

	@Mock
	private FilePaymentValidatorImpl validator;

	@Mock
	private FilePaymentsConsentsService service;

	@InjectMocks
	private FilePaymentsConsentsController controller;

	@Mock
	private FPaymentConsentsResponseTransformer transformer;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		this.mockMvc = MockMvcBuilders.standaloneSetup(controller).dispatchOptions(true).build();
	}

	@Test
	public void contextLoads() {

	}

	@Test
	public void testCreateFilePaymentConsentResourceStep1() throws Exception {
		CustomFilePaymentConsentsPOSTResponse response = new CustomFilePaymentConsentsPOSTResponse();
		CustomFilePaymentSetupPOSTRequest request = new CustomFilePaymentSetupPOSTRequest();
		OBWriteDataFileConsent1 data = new OBWriteDataFileConsent1();
		OBFile1 initiation = new OBFile1();
		initiation.setRequestedExecutionDateTime("2019-01-31T00:00:00");
		data.setInitiation(initiation);
		request.setData(data);
		boolean r = true;
		when(validator.validateFilePaymentSetupPOSTRequest(anyObject())).thenReturn(true);
		when(service.createFilePaymentConsentsResource(anyObject(), eq(r))).thenReturn(response);
		when(transformer.paymentConsentRequestTransformer(anyObject())).thenReturn(request);
		this.mockMvc
				.perform(post("/file-payment-consents").contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
						.content(FilePaymentConsentPOSTRequestResponseMockData.asJsonString(request)))
				.andExpect(status().isCreated());
	}

	@Test
	public void testCreateFilePaymentConsentResourceStep2() throws Exception {

		Resource resource = new ClassPathResource("/transaction.xml");
		File file = resource.getFile();
		byte[] byteArray = FileUtils.readFileToByteArray(file);
		MockMultipartFile jsonFile = new MockMultipartFile("FileParam", byteArray);

		HttpHeaders headers = new HttpHeaders();
		headers.set("Content-Type", "multipart/form-data");

		when(validator.validateFilePaymentConsentsPOSTRequest(anyObject())).thenReturn(true);

		doNothing().when(service).uploadPaymentConsentFile(jsonFile, "92346923");

		this.mockMvc.perform(MockMvcRequestBuilders.fileUpload("/file-payment-consents/{ConsentId}/file", 92346923)
				.file(jsonFile).headers(headers)).andExpect(status().isOk());

	}

	@Test
	public void testRetrieveFilePaymentConsentsResource() throws Exception {
		CustomFilePaymentConsentsPOSTResponse response = new CustomFilePaymentConsentsPOSTResponse();
		CustomFilePaymentSetupPOSTRequest request = new CustomFilePaymentSetupPOSTRequest();
		OBWriteDataFileConsent1 data = new OBWriteDataFileConsent1();
		request.setData(data);
		request.getData();

		when(validator.validateFilePaymentConsentsGETRequest(anyObject())).thenReturn(true);
		when(service.retrieveFilePaymentConsentsResource(anyObject())).thenReturn(response);
		this.mockMvc
				.perform(get("/file-payment-consents/{ConsentId}", 789456)
						.contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
						.content(FilePaymentConsentPOSTRequestResponseMockData.asJsonString(request)))
				.andExpect(status().isOk());
	}

	@Test
	public void testDownloadFilePaymentConsentResourceStep2() throws Exception {

		PlatformFilePaymentsFileResponse response = new PlatformFilePaymentsFileResponse();
		response.setFileName("transaction.xml");
		response.setFileType(MediaType.APPLICATION_XML_VALUE);
		response.setFileByteArray("hi".getBytes());

		when(validator.validateFilePaymentConsentsGETRequest(anyObject())).thenReturn(true);
		when(service.downloadTransactionFileByConsentId(anyObject())).thenReturn(response);

		this.mockMvc
				.perform(get("/file-payment-consents/{ConsentId}/file", 2934872)
						.contentType(MediaType.APPLICATION_XML_VALUE).content("hi".getBytes()))
				.andExpect(status().isOk());

	}

	@Test
	public void testDownloadFilePaymentConsentResourceJson() throws Exception {

		PlatformFilePaymentsFileResponse response = new PlatformFilePaymentsFileResponse();
		response.setFileName("transaction.json");
		response.setFileType(MediaType.APPLICATION_JSON.toString());
		response.setFileByteArray("hi".getBytes());

		when(validator.validateFilePaymentConsentsGETRequest(anyObject())).thenReturn(true);
		when(service.downloadTransactionFileByConsentId(anyObject())).thenReturn(response);

		this.mockMvc.perform(get("/file-payment-consents/{ConsentId}/file", 145872)
				.contentType(MediaType.APPLICATION_JSON).content("hi".getBytes())).andExpect(status().isOk());

	}
}
