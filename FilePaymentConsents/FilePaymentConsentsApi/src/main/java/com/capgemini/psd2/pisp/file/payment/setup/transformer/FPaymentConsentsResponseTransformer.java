package com.capgemini.psd2.pisp.file.payment.setup.transformer;

import com.capgemini.psd2.pisp.domain.CustomFilePaymentConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.CustomFilePaymentSetupPOSTRequest;
import com.capgemini.psd2.pisp.domain.PaymentConsentsPlatformResource;

public interface FPaymentConsentsResponseTransformer {

	public CustomFilePaymentConsentsPOSTResponse filePaymentConsentsResponseTransformer(
			CustomFilePaymentConsentsPOSTResponse fileConsentsTppResponse,
			PaymentConsentsPlatformResource paymentSetupPlatformResource, String methodType);

	public CustomFilePaymentSetupPOSTRequest paymentConsentRequestTransformer(
			CustomFilePaymentSetupPOSTRequest paymentConsentRequest);

}
