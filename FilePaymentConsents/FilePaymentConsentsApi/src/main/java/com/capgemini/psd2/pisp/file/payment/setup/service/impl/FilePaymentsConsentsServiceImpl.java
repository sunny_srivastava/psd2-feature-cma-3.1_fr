package com.capgemini.psd2.pisp.file.payment.setup.service.impl;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;

import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.ErrorMapKeys;
import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.InternalServerErrorMessage;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.adapter.FilePaymentConsentsAdapter;
import com.capgemini.psd2.pisp.domain.CustomFilePaymentConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.CustomFilePaymentSetupPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomPaymentSetupPlatformDetails;
import com.capgemini.psd2.pisp.domain.OBExternalConsentStatus2Code;
import com.capgemini.psd2.pisp.domain.PaymentConsentsPlatformResource;
import com.capgemini.psd2.pisp.domain.PaymentResponseInfo;
import com.capgemini.psd2.pisp.domain.PaymentRetrieveGetRequest;
import com.capgemini.psd2.pisp.domain.PlatformFilePaymentsFileResponse;
import com.capgemini.psd2.pisp.enums.PaymentTypeEnum;
import com.capgemini.psd2.pisp.enums.ProcessConsentStatusEnum;
import com.capgemini.psd2.pisp.file.payment.setup.comparator.FilePaymentConsentsPayloadComparator;
import com.capgemini.psd2.pisp.file.payment.setup.service.FilePaymentsConsentsService;
import com.capgemini.psd2.pisp.file.payment.setup.transformer.FPaymentConsentsResponseTransformer;
import com.capgemini.psd2.pisp.file.payment.setup.transformer.FileTransformer;
import com.capgemini.psd2.pisp.file.payment.setup.utilities.FilePaymentConsentsUtilities;
import com.capgemini.psd2.pisp.file.payment.setup.utilities.JWSVerificationUtility;
import com.capgemini.psd2.pisp.payment.setup.platform.adapter.PaymentSetupPlatformAdapter;
import com.capgemini.psd2.pisp.payment.setup.platform.adapter.repository.PaymentSetupPlatformRepository;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.pisp.utilities.PispUtilities;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Service
public class FilePaymentsConsentsServiceImpl implements FilePaymentsConsentsService {

	@Autowired
	@Qualifier("filePaymentConsentsStagingRoutingAdapter")
	private FilePaymentConsentsAdapter filePaymentCoreAdapter;

	@Autowired
	private PaymentSetupPlatformAdapter paymentSetupPlatformAdapter;

	@Autowired
	private RequestHeaderAttributes requestHeaderAttr;
	
	@Autowired
	private FilePaymentConsentsUtilities utility;

	@Autowired
	private FilePaymentConsentsPayloadComparator filePaymentConsentsComparator;

	@Autowired
	private FPaymentConsentsResponseTransformer fPaymentConsentsResponseTransformer;

	@Autowired
	private RequestHeaderAttributes reqHeaderAtrributes;

	@Autowired
	private PaymentSetupPlatformRepository paymentSetupPlatformRepository;

	@Autowired
	private FileTransformer fileTransformer;

	@Autowired
	private JWSVerificationUtility jwsVerificationUtility;

	private long idempotencyDuration;
	
	@Value("${app.signature.enableSignature}")
	private Boolean enableSignature;
	
	@Value("${cmaVersion}")
	private String cmaVersion;

	@Autowired
	public FilePaymentsConsentsServiceImpl(
			@Value("${app.idempotency.durationForPaymentConsents}") String idempotencyDuration) {
		if (idempotencyDuration != null)
			this.idempotencyDuration = PispUtilities.getMilliSeconds(idempotencyDuration);
	}

	@Override
	public CustomFilePaymentConsentsPOSTResponse createFilePaymentConsentsResource(
			CustomFilePaymentSetupPOSTRequest paymentRequest, boolean isPaymentValidated) {

		if (!isPaymentValidated) {
			createInvalidFilePaymentSetupPlatformResource(paymentRequest);
			return null;
		}

		/* Perform idempotency check */
		CustomFilePaymentConsentsPOSTResponse paymentConsentsResponse = performIdempotencyCheckAndStageData(
				paymentRequest);

		if (paymentConsentsResponse != null && paymentConsentsResponse.getData() != null)
			return paymentConsentsResponse;

		String setupCreationDate = PispUtilities.getCurrentDateInISOFormat();

		// save data in platform
		CustomPaymentSetupPlatformDetails customPaymentSetupPlatformDetails = populateFilePlatformDetails(
				paymentRequest);
		PaymentConsentsPlatformResource platformResource = createPlatformResource(customPaymentSetupPlatformDetails);

		/* Save the data in staging */
		CustomFilePaymentConsentsPOSTResponse fileStagingResource = createFilePaymentStagingResource(paymentRequest,
				setupCreationDate);

		/* To populate the values from the object of Staging Response */
		PaymentResponseInfo stageDetails = populateStageDetails(fileStagingResource);

		/*
		 * Check if stage data is null or empty then mark status as rejected and save in
		 * platform
		 */
		if (NullCheckUtils.isNullOrEmpty(stageDetails)) {
			createInvalidFilePaymentSetupPlatformResource(paymentRequest);
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
					InternalServerErrorMessage.PISP_PAYMENT_SETUP_CREATION_FAILED));
		}

		/*
		 * If the Staging response object is not empty then populate the response object
		 * to be stored in platform
		 */

		/* Update the data in platform */
		platformResource = updatePlatformDetails(platformResource, stageDetails);

		/*
		 * Call Stage Service to update final status Date: 04-Aug-2017 While updating
		 * final status of staged payment at bank end, if platform receives any
		 * exception from bank end then staged payment status of platform would not be
		 * rolled back and error log for this exception will be maintained at Adapter
		 * end. No exception will be thrown to PISP from here. Later platform will
		 * update the status at bank end. If payment setup validation's status is
		 * 'Rejected' then no need to send update call to FS
		 */

		return fPaymentConsentsResponseTransformer.filePaymentConsentsResponseTransformer(fileStagingResource,
				platformResource, RequestMethod.POST.toString());
	}

	private PaymentConsentsPlatformResource updatePlatformDetails(PaymentConsentsPlatformResource platformResource,
			PaymentResponseInfo stageDetails) {
		platformResource.setPaymentConsentId(stageDetails.getPaymentConsentId());
		platformResource.setStatus((stageDetails.getPayConsentValidationStatus() != null)
				? stageDetails.getPayConsentValidationStatus().toString()
				: null);
		platformResource.setStatusUpdateDateTime(platformResource.getCreatedAt());
		platformResource.setIdempotencyRequest(stageDetails.getIdempotencyRequest());
		platformResource.setUpdatedAt(PispUtilities.getCurrentDateInISOFormat());
		return paymentSetupPlatformRepository.save(platformResource);
	}

	private PaymentConsentsPlatformResource createPlatformResource(
			CustomPaymentSetupPlatformDetails customPaymentSetupPlatformDetails) {
		String setupCreationDate = PispUtilities.getCurrentDateInISOFormat();
		return paymentSetupPlatformAdapter.createPaymentSetupPlatformResource(customPaymentSetupPlatformDetails,
				setupCreationDate);
	}

	private void createInvalidFilePaymentSetupPlatformResource(
			CustomFilePaymentSetupPOSTRequest paymentConsentsRequest) {

		PaymentResponseInfo validationDetails = new PaymentResponseInfo();
		validationDetails.setPayConsentValidationStatus(OBExternalConsentStatus2Code.REJECTED);
		validationDetails.setIdempotencyRequest(String.valueOf(Boolean.FALSE));
		CustomPaymentSetupPlatformDetails paymentConsentsPlatformDetails = populateFilePlatformDetails(
				paymentConsentsRequest);

		paymentSetupPlatformAdapter.createInvalidPaymentSetupPlatformResource(paymentConsentsPlatformDetails,
				validationDetails, PispUtilities.getCurrentDateInISOFormat());
	}

	private CustomFilePaymentConsentsPOSTResponse createFilePaymentStagingResource(
			CustomFilePaymentSetupPOSTRequest paymentRequest, String setupCreationDate) {

		paymentRequest.setFilePaymentSetupstatus(OBExternalConsentStatus2Code.AWAITINGUPLOAD);
		paymentRequest.setCreatedOn(setupCreationDate);
		paymentRequest.setFilePaymentSetupStatusUpdateDateTime(setupCreationDate);

		paymentRequest.getData().getInitiation().setFileHash(paymentRequest.getData().getInitiation().getFileHash());

		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();
		stageIdentifiers.setPaymentSetupVersion(cmaVersion);
		stageIdentifiers.setPaymentTypeEnum(PaymentTypeEnum.FILE_PAY);

		Map<String, String> paramMap = new HashMap<String, String>();
		paramMap.put(PSD2Constants.CO_RELATION_ID, requestHeaderAttr.getCorrelationId());
		return filePaymentCoreAdapter.createStagingFilePaymentConsents(paymentRequest, stageIdentifiers, paramMap);

	}

	@Override
	public CustomPaymentSetupPlatformDetails populateFilePlatformDetails(
			CustomFilePaymentSetupPOSTRequest paymentConsentsRequest) {

		CustomPaymentSetupPlatformDetails fileConsentsPlatformDetails = new CustomPaymentSetupPlatformDetails();
		String tppDebtorDetailsStatus = String.valueOf(Boolean.FALSE);
		String tppDebtorNameDetailsStatus = String.valueOf(Boolean.FALSE);
		if (paymentConsentsRequest.getData() != null && paymentConsentsRequest.getData().getInitiation() != null) {
			if (paymentConsentsRequest.getData().getInitiation().getDebtorAccount() != null) {
				tppDebtorDetailsStatus = String.valueOf(Boolean.TRUE);
				if (!NullCheckUtils
						.isNullOrEmpty(paymentConsentsRequest.getData().getInitiation().getDebtorAccount().getName()))
					tppDebtorNameDetailsStatus = String.valueOf(Boolean.TRUE);
			}

			fileConsentsPlatformDetails.setTppDebtorDetails(tppDebtorDetailsStatus);
			fileConsentsPlatformDetails.setTppDebtorNameDetails(tppDebtorNameDetailsStatus);
		}

		/*
		 * Setting the file hash for idempotency check and compare for file upload
		 */
		fileConsentsPlatformDetails.setFileHash(paymentConsentsRequest.getData().getInitiation().getFileHash());
		fileConsentsPlatformDetails.setPaymentType(PaymentTypeEnum.FILE_PAY);
		fileConsentsPlatformDetails.setSetupCmaVersion(cmaVersion);
		return fileConsentsPlatformDetails;
	}

	public PaymentResponseInfo populateStageDetails(CustomFilePaymentConsentsPOSTResponse fileStagingResource) {

		PaymentResponseInfo stageDetails = null;
		if (NullCheckUtils.isNullOrEmpty(fileStagingResource)
				|| NullCheckUtils.isNullOrEmpty(fileStagingResource.getData())
				|| NullCheckUtils.isNullOrEmpty(fileStagingResource.getData().getConsentId())
				|| NullCheckUtils.isNullOrEmpty(fileStagingResource.getData().getStatus())) {
			return stageDetails;
		}
		String idempotencyRequest = String.valueOf(Boolean.TRUE);

		stageDetails = new PaymentResponseInfo();
		stageDetails.setConsentProcessStatus(fileStagingResource.getConsentProcessStatus());

		if (stageDetails.getConsentProcessStatus() == ProcessConsentStatusEnum.FAIL) {
			stageDetails.setPayConsentValidationStatus(OBExternalConsentStatus2Code.REJECTED);
		} else {
			stageDetails.setPayConsentValidationStatus(OBExternalConsentStatus2Code.AWAITINGUPLOAD);
		}

		stageDetails.setIdempotencyRequest(idempotencyRequest);
		stageDetails.setPaymentConsentId(fileStagingResource.getData().getConsentId());
		return stageDetails;
	}

	private CustomFilePaymentConsentsPOSTResponse performIdempotencyCheckAndStageData(
			CustomFilePaymentSetupPOSTRequest paymentConsentsRequest) {

		PaymentConsentsPlatformResource paymentConsentsPlatformResponse = paymentSetupPlatformAdapter
				.getIdempotentPaymentSetupPlatformResource(PaymentTypeEnum.FILE_PAY.getPaymentType(),
						idempotencyDuration);

		if (paymentConsentsPlatformResponse == null)
			return null;

		CustomFilePaymentConsentsPOSTResponse paymentConsentsFoundationResponse;

		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();
		stageIdentifiers.setPaymentConsentId(paymentConsentsPlatformResponse.getPaymentConsentId());
		stageIdentifiers.setPaymentTypeEnum(PaymentTypeEnum.FILE_PAY);
		stageIdentifiers.setPaymentSetupVersion(paymentConsentsPlatformResponse.getSetupCmaVersion());

		paymentConsentsFoundationResponse = filePaymentCoreAdapter.fetchFilePaymentMetadata(stageIdentifiers, null);

		if (filePaymentConsentsComparator.compareFilePaymentDetails(paymentConsentsFoundationResponse,
				paymentConsentsRequest, paymentConsentsPlatformResponse) > 0)
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_RESOURCE_CONSENTMISMATCH,
					ErrorMapKeys.RESOURCE_CONSENT_MISMATCH));

		return fPaymentConsentsResponseTransformer.filePaymentConsentsResponseTransformer(
				paymentConsentsFoundationResponse, paymentConsentsPlatformResponse, RequestMethod.POST.toString());
	}

	@Override
	public void uploadPaymentConsentFile(MultipartFile file, String consentId) {

		try {
			if(enableSignature)
			jwsVerificationUtility.validateSignature(Arrays.toString(file.getBytes()), reqHeaderAtrributes.getTppCID());

		} catch (IOException e) {
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_RESOURCE_INVALIDFORMAT,
					ErrorMapKeys.INVALID_FILE));
		}
		PaymentConsentsPlatformResource paymentConsentsPlatformResponse = paymentSetupPlatformAdapter
				.retrievePaymentSetupPlatformResource(consentId);

		if (paymentConsentsPlatformResponse == null)
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_RESOURCE_NOTFOUND,
					ErrorMapKeys.NO_CONSENT_ID_FOUND));

		paymentConsentsPlatformResponse = paymentSetupPlatformAdapter.getIdempotentFileUploadSetupPlatformResource(
				PaymentTypeEnum.FILE_PAY.getPaymentType(), idempotencyDuration, consentId);

		if (paymentConsentsPlatformResponse == null) {
			throw PSD2Exception.populatePSD2Exception(
					new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_FIELD_INVALID, ErrorMapKeys.INVALID_FIELDS_COMBINATION));
		} else {
			CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();
			stageIdentifiers.setPaymentConsentId(consentId);
			CustomFilePaymentConsentsPOSTResponse dbResponse = filePaymentCoreAdapter
					.fetchFilePaymentMetadata(stageIdentifiers, null);
			String fileType = dbResponse.getData().getInitiation().getFileType();

			if (!fileMetaDataCompare(paymentConsentsPlatformResponse, file, fileType)) {
				platformAndStageStatusUpdate(paymentConsentsPlatformResponse, dbResponse,
						OBExternalConsentStatus2Code.REJECTED);
				throw PSD2Exception.populatePSD2Exception(
						new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_RESOURCE_CONSENTMISMATCH, ErrorMapKeys.FILE_METADATA_MISMATCH));
			}

			if (paymentConsentsPlatformResponse.getStatus()
					.equals(OBExternalConsentStatus2Code.AWAITINGUPLOAD.toString())) {
				filePaymentCoreAdapter.uploadFilePaymentConsents(file, consentId);
				platformAndStageStatusUpdate(paymentConsentsPlatformResponse, dbResponse,
						OBExternalConsentStatus2Code.AWAITINGAUTHORISATION);
			}
		}
	}

	private void platformAndStageStatusUpdate(PaymentConsentsPlatformResource paymentConsentsPlatformResponse,
			CustomFilePaymentConsentsPOSTResponse dbResponse, OBExternalConsentStatus2Code status) {

		String statusUpdateDate = PispUtilities.getCurrentDateInISOFormat();

		dbResponse.getData().setStatus(status);
		dbResponse.getData().setStatusUpdateDateTime(statusUpdateDate);

		filePaymentCoreAdapter.updateStagingFilePaymentConsents(dbResponse);

		paymentConsentsPlatformResponse.setUpdatedAt(statusUpdateDate);
		paymentConsentsPlatformResponse.setStatusUpdateDateTime(statusUpdateDate);
		paymentConsentsPlatformResponse.setStatus(status.toString());

		paymentSetupPlatformAdapter.updatePaymentSetupPlatformResource(paymentConsentsPlatformResponse);
	}

	private boolean fileMetaDataCompare(PaymentConsentsPlatformResource paymentConsentsPlatformResponse,
			MultipartFile file, String fileType) {

		byte[] currentFileHash = utility.generateFileHash(file);

		byte[] platformFileHash = utility.base64DecodeHash(paymentConsentsPlatformResponse.getFileHash());

		return (Arrays.equals(currentFileHash, platformFileHash) && fileTransformer.fileTypeChecker(file, fileType))
				? true
				: false;

	}

	@Override
	public CustomFilePaymentConsentsPOSTResponse retrieveFilePaymentConsentsResource(
			PaymentRetrieveGetRequest paymentRetrieveRequest) {

		PaymentConsentsPlatformResource paymentConsentsPlatformResponse = paymentSetupPlatformAdapter
				.retrievePaymentSetupPlatformResourceByIdAndType(paymentRetrieveRequest.getConsentId(),
						PaymentTypeEnum.FILE_PAY.getPaymentType());

		if (paymentConsentsPlatformResponse == null)
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_RESOURCE_NOTFOUND,
					ErrorMapKeys.EXPECTED_PAYMENT_SETUP_ID));

		if (!paymentConsentsPlatformResponse.getTppCID().equalsIgnoreCase(reqHeaderAtrributes.getTppCID()))
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.PISP_NO_PAYMENT_SETUP_RESOURCE_FOUND_AGAINST_TPP);

		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();
		stageIdentifiers.setPaymentSetupVersion(cmaVersion);
		stageIdentifiers.setPaymentTypeEnum(PaymentTypeEnum.FILE_PAY);
		stageIdentifiers.setPaymentConsentId(paymentRetrieveRequest.getConsentId());

		CustomFilePaymentConsentsPOSTResponse paymentConsentsFoundationResponse = filePaymentCoreAdapter
				.fetchFilePaymentMetadata(stageIdentifiers, null);

		return fPaymentConsentsResponseTransformer.filePaymentConsentsResponseTransformer(
				paymentConsentsFoundationResponse, paymentConsentsPlatformResponse, RequestMethod.GET.toString());

	}

	@Override
	public PlatformFilePaymentsFileResponse downloadTransactionFileByConsentId(String consentId) {

		PlatformFilePaymentsFileResponse platformFilePaymentsFileResponse = null;

		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();
		stageIdentifiers.setPaymentConsentId(consentId);
		stageIdentifiers.setPaymentTypeEnum(PaymentTypeEnum.FILE_PAY);

		PaymentConsentsPlatformResource paymentSetupPlatformResource = paymentSetupPlatformAdapter
				.retrievePaymentSetupPlatformResourceByIdAndType(consentId, PaymentTypeEnum.FILE_PAY.getPaymentType());

		if (paymentSetupPlatformResource == null)
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_RESOURCE_NOTFOUND,
					ErrorMapKeys.EXPECTED_PAYMENT_SETUP_ID));

		if (!paymentSetupPlatformResource.getStatus().equals(OBExternalConsentStatus2Code.AWAITINGUPLOAD.toString())
				&& !paymentSetupPlatformResource.getStatus().equals(OBExternalConsentStatus2Code.REJECTED.toString())) {

			platformFilePaymentsFileResponse = filePaymentCoreAdapter.downloadFilePaymentConsents(stageIdentifiers);

			if (platformFilePaymentsFileResponse != null) {
				return platformFilePaymentsFileResponse;
			} else {
				return null;
			}

		} else {
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_RESOURCE_INVALIDCONSENTSTATUS,
					ErrorMapKeys.RESOURCE_INVALIDCONSENTSTATUS));
		}
	}

}