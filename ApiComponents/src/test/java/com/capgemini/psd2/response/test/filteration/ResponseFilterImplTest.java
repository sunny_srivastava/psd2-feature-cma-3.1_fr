package com.capgemini.psd2.response.test.filteration;

import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.capgemini.psd2.aisp.domain.OBAccount6;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.response.filteration.ResponseFilterImpl;

public class ResponseFilterImplTest {

	@Mock
	private RequestHeaderAttributes reqHeaderAttribute;
	
	@InjectMocks
	private ResponseFilterImpl responseFilterImpl = new ResponseFilterImpl();

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void testFilterResponse() {
		Map<String, Map<String, String>> filter = new HashMap<>();
		Map<String, String> subMap = new HashMap<>();
		subMap.put("READACCOUNTSBASIC", "Data{Account[AccountId,Currency,Nickname]},Links[**],Meta[**]");
		filter.put("retrieveAccountInformation", subMap);
		responseFilterImpl.setFilter(filter);
		OBAccount6 obj = new OBAccount6();
		responseFilterImpl.filterResponse(obj, "retrieveAccountInformation");	
	}
	
	@Test
	public void testFilterResponseNull() {
		Map<String, Map<String, String>> filter = new HashMap<>();
		responseFilterImpl.setFilter(filter);
		OBAccount6 obj = new OBAccount6();
		responseFilterImpl.filterResponse(obj, "retrieveAccountInformation");	
	}
	
	@Test
	public void testFilterResponseEmpty() {
		Map<String, Map<String, String>> filter = new HashMap<>();
		Map<String, String> subMap = new HashMap<>();
		subMap.put("", "");
		filter.put("", subMap);
		responseFilterImpl.setFilter(filter);
		OBAccount6 obj = new OBAccount6();
		responseFilterImpl.filterResponse(obj, "retrieveAccountInformation");	
	}
	
	@Test
	public void testFilterResponse1() {
		OBAccount6 obj = new OBAccount6();
		Set<Object> claimList = new HashSet<>();
		claimList.add("READACCOUNTSBASIC");
		when(reqHeaderAttribute.getClaims()).thenReturn(claimList);
		responseFilterImpl.filterResponse(obj, "retrieveAccountInformation");	
	}
	
	@Test
	public void testFilterResponse1ClaimNull() {
		OBAccount6 obj = new OBAccount6();
		Set<Object> claimList = new HashSet<>();
		when(reqHeaderAttribute.getClaims()).thenReturn(claimList);
		responseFilterImpl.filterResponse(obj, "retrieveAccountInformation");	
	}
	
	@Test
	public void testFilterResponse1ClaimEmpty() {
		OBAccount6 obj = new OBAccount6();
		Set<Object> claimList = new HashSet<>();
		claimList.add("");
		when(reqHeaderAttribute.getClaims()).thenReturn(claimList);
		responseFilterImpl.filterResponse(obj, "retrieveAccountInformation");	
	}
	
	@Test
	public void testFilterResponse2() {
		OBAccount6 obj = new OBAccount6();
		Set<Object> claimList = new HashSet<>();
		claimList.add("READACCOUNTSBASIC");
		when(reqHeaderAttribute.getClaims()).thenReturn(claimList);
		responseFilterImpl.filterResponse(obj, "retrieveAccountInformation",null);	
	}
	
	@Test
	public void testFilterResponse2ClaimsEmpty() {
		OBAccount6 obj = new OBAccount6();
		Set<Object> claimList = new HashSet<>();
		claimList.add("READACCOUNTSBASIC");
		when(reqHeaderAttribute.getClaims()).thenReturn(claimList);
		List<String> claims = new ArrayList<>();
		claims.add("READACCOUNTSBASIC");
		responseFilterImpl.filterResponse(obj, "retrieveAccountInformation",claims);	
	}
	
	@Test
	public void testFilterResponse2ClaimsNull() {
		OBAccount6 obj = new OBAccount6();
		Set<Object> claimList = new HashSet<>();
		claimList.add("READACCOUNTSBASIC");
		when(reqHeaderAttribute.getClaims()).thenReturn(claimList);
		List<String> claims = new ArrayList<>();
		claims.add(null);
		responseFilterImpl.filterResponse(obj, "retrieveAccountInformation",claims);	
	}
	
	@Test
	public void testFilterResponse3() {
		Map<String, Map<String, String>> filter = new HashMap<>();
		Map<String, String> subMap = new HashMap<>();
		subMap.put("READACCOUNTSBASIC", "Data{Account[AccountId,Currency,Nickname]},Links[**],Meta[**]");
		filter.put("retrieveAccountInformation", subMap);
		responseFilterImpl.setFilter(filter);
		OBAccount6 obj = new OBAccount6();
		Set<Object> claimList = new HashSet<>();
		when(reqHeaderAttribute.getClaims()).thenReturn(claimList);
		responseFilterImpl.filterResponse(obj, "retrieveAccountInformation",null);	
	}
	
	@Test
	public void testFilterResponse4() {
		Map<String, Map<String, String>> filter = new HashMap<>();
		Map<String, String> subMap = new HashMap<>();
		subMap.put("READACCOUNTSBASIC", "Data{Account[AccountId,Currency,Nickname]},Links[**],Meta[**]");
		filter.put("retrieveAccountInformation", subMap);
		responseFilterImpl.setFilter(filter);
		OBAccount6 obj = new OBAccount6();
		Set<Object> claimList = new HashSet<>();
		claimList.add(null);
		when(reqHeaderAttribute.getClaims()).thenReturn(claimList);
		responseFilterImpl.filterResponse(obj, "retrieveAccountInformation",null);	
	}
	
	@Test
	public void testFilterResponseWithClaimList() {
		Map<String, Map<String, String>> filter = new HashMap<>();
		Map<String, String> subMap = new HashMap<>();
		subMap.put("READACCOUNTSBASIC", "Data{Account[AccountId,Currency,Nickname]},Links[**],Meta[**]");
		filter.put("retrieveAccountInformation", subMap);
		responseFilterImpl.setFilter(filter);
		OBAccount6 obj = new OBAccount6();
		List<String> claimList = new ArrayList<>();
		claimList.add("READACCOUNTSBASIC");
	
		responseFilterImpl.filterResponse(obj, "retrieveAccountInformation",claimList);	
	}
	
	@Test
	public void testFilterResponseWithMultipleClaimList() {
		Map<String, Map<String, String>> filter = new HashMap<>();
		Map<String, String> subMap = new HashMap<>();
		subMap.put("READACCOUNTSBASIC", "Data{Account[AccountId,Currency,Nickname]},Links[**],Meta[**]");
		subMap.put("READACCOUNTSDETAIL", "Data{Account[AccountId,Currency,Nickname]},Links[**],Meta[**]");
		filter.put("retrieveAccountInformation", subMap);
		responseFilterImpl.setFilter(filter);
		OBAccount6 obj = new OBAccount6();
		List<String> claimList = new ArrayList<>();
		claimList.add("READACCOUNTSBASIC");
		claimList.add("READACCOUNTSDETAIL");
	
		responseFilterImpl.filterResponse(obj, "retrieveAccountInformation",claimList);	
	}
	
	@Test
	public void testFilterResponseWithNullClaimList() {
		Map<String, Map<String, String>> filter = new HashMap<>();
		Map<String, String> subMap = new HashMap<>();
		subMap.put("READACCOUNTSBASIC", "Data{Account[AccountId,Currency,Nickname]},Links[**],Meta[**]");
		filter.put("retrieveAccountInformation", subMap);
		responseFilterImpl.setFilter(filter);
		OBAccount6 obj = new OBAccount6();
		List<String> claimList = new ArrayList<>();
		claimList.add("READACCOUNTSBASIC");
	
		responseFilterImpl.filterResponse(obj, "randomstring",claimList);	
	}
	
	
	@Test
	public void testFilterMessage() {
		OBAccount6 obj = new OBAccount6();
		responseFilterImpl.filterMessage(obj, "filter");
		responseFilterImpl.getFilter();
	}
	
	@Test
	public void testFilterMessageException() {
		responseFilterImpl.filterMessage(null,null);
		responseFilterImpl.getFilter();
	}
}
