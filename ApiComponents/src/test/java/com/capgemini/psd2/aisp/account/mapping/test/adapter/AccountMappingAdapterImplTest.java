package com.capgemini.psd2.aisp.account.mapping.test.adapter;

import static org.mockito.Matchers.anyListOf;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.aisp.account.identfication.mapping.adapter.domain.AccountIdentificationMapping;
import com.capgemini.psd2.aisp.account.identfication.mapping.adapter.domain.IdntificationMapping;
import com.capgemini.psd2.aisp.account.mapping.adapter.AccountMappingAdapterImpl;
import com.capgemini.psd2.aisp.adapter.AccountIdentificationMappingAdapter;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.LoggerUtils;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.mock.data.ApiComponentsMockData;
import com.capgemini.psd2.validator.PSD2Validator;


public class AccountMappingAdapterImplTest {

	@Mock
	private RequestHeaderAttributes reqHeaderAttributes;

	@Mock
	@Qualifier("AccountMappingRoutingAdapter")
	private AccountIdentificationMappingAdapter accountIdentificationMappingAdapter;

	@Mock
	private PSD2Validator fieldValidator;

	/** The logger utils. */
	@Mock
	private LoggerUtils loggerUtils;
	
	@InjectMocks
	AccountMappingAdapterImpl obj=new AccountMappingAdapterImpl();

	@Before
	public void setUp() {
	   MockitoAnnotations.initMocks(this);	
	}
	
	@Test
	public void testRetrieveAccountMappingDetails() {
		ReflectionTestUtils.setField(obj, "platformStored", false);
		AccountIdentificationMapping accountIdentificationMapping=new AccountIdentificationMapping();
		List<IdntificationMapping> identificationMaplist=new ArrayList<>();
		IdntificationMapping identicationMapping=new IdntificationMapping();
		identicationMapping.setIdentifier("123456");
		identicationMapping.setAccountNSC("901542");
		identicationMapping.setAccountNumber("12345678");
		identificationMaplist.add(identicationMapping);
		accountIdentificationMapping.setIdentificationMappingList(identificationMaplist);
		when(accountIdentificationMappingAdapter.retrieveAccountIdentificationMapping(anyListOf(String.class))).thenReturn(accountIdentificationMapping);
		obj.retrieveAccountMappingDetails("123456", ApiComponentsMockData.getAccountMapping());
	}
	
	@Test
	public void testRetrieveAccountMappingDetailsPlatformStored() {
		ReflectionTestUtils.setField(obj, "platformStored", true);
		obj.retrieveAccountMappingDetails("123456", ApiComponentsMockData.getAccountMapping());
	}
	
	@Test(expected=PSD2Exception.class)
	public void testRetrieveAccountMappingDetailsException2() {
		ReflectionTestUtils.setField(obj, "platformStored", false);
		AccountIdentificationMapping accountIdentificationMapping=new AccountIdentificationMapping();
		List<IdntificationMapping> identificationMaplist=new ArrayList<>();
		accountIdentificationMapping.setIdentificationMappingList(identificationMaplist);
		when(accountIdentificationMappingAdapter.retrieveAccountIdentificationMapping(anyListOf(String.class))).thenReturn(accountIdentificationMapping);
		obj.retrieveAccountMappingDetails("123456", ApiComponentsMockData.getAccountMapping());
	}
	
	@Test(expected=PSD2Exception.class)
	public void testRetrieveAccountMappingDetailsException3() {
		ReflectionTestUtils.setField(obj, "platformStored", false);
		AccountIdentificationMapping accountIdentificationMapping=new AccountIdentificationMapping();
		List<IdntificationMapping> identificationMaplist=new ArrayList<>();
		IdntificationMapping identicationMapping=new IdntificationMapping();
		identicationMapping.setIdentifier("12346");
		identicationMapping.setAccountNSC("901542");
		identicationMapping.setAccountNumber("12345678");
		identificationMaplist.add(identicationMapping);
		accountIdentificationMapping.setIdentificationMappingList(identificationMaplist);
		when(accountIdentificationMappingAdapter.retrieveAccountIdentificationMapping(anyListOf(String.class))).thenReturn(accountIdentificationMapping);
		obj.retrieveAccountMappingDetails("123456", ApiComponentsMockData.getAccountMapping());
	}
	
	@Test
	public void testRetrieveAccountMappingDetails5() {
		ReflectionTestUtils.setField(obj, "platformStored", true);
		AccountIdentificationMapping accountIdentificationMapping=new AccountIdentificationMapping();
		List<IdntificationMapping> identificationMaplist=new ArrayList<>();
		IdntificationMapping identicationMapping=new IdntificationMapping();
		identicationMapping.setIdentifier("123456");
		identicationMapping.setAccountNSC("901542");
		identicationMapping.setAccountNumber("12345678");
		identificationMaplist.add(identicationMapping);
		accountIdentificationMapping.setIdentificationMappingList(identificationMaplist);
		when(accountIdentificationMappingAdapter.retrieveAccountIdentificationMapping(anyListOf(String.class))).thenReturn(accountIdentificationMapping);
		obj.retrieveAccountMappingDetails(ApiComponentsMockData.getAccountMapping());
	}
	
	@Test
	public void testRetrieveAccountMappingDetailsPlatformStored1() {
		ReflectionTestUtils.setField(obj, "platformStored", true);
		obj.retrieveAccountMappingDetails(ApiComponentsMockData.getAccountMapping());
	}
	
	@Test
	public void testRetrieveAccountMappingDetailsFalse() {
		ReflectionTestUtils.setField(obj, "platformStored", false);
		AccountIdentificationMapping accountIdentificationMapping=new AccountIdentificationMapping();
		List<IdntificationMapping> identificationMaplist=new ArrayList<>();
		IdntificationMapping identicationMapping=new IdntificationMapping();
		identicationMapping.setIdentifier("123456");
		identicationMapping.setAccountNSC("901542");
		identicationMapping.setAccountNumber("12345678");
		identificationMaplist.add(identicationMapping);
		accountIdentificationMapping.setIdentificationMappingList(identificationMaplist);
		when(accountIdentificationMappingAdapter.retrieveAccountIdentificationMapping(anyListOf(String.class))).thenReturn(accountIdentificationMapping);
		obj.retrieveAccountMappingDetails(ApiComponentsMockData.getAccountMapping());
	}
	
	@Test(expected=PSD2Exception.class)
	public void testRetrieveAccountMappingDetailsForException() {
		ReflectionTestUtils.setField(obj, "platformStored", false);
		when(accountIdentificationMappingAdapter.retrieveAccountIdentificationMapping(anyListOf(String.class))).thenReturn(null);
		obj.retrieveAccountMappingDetails(ApiComponentsMockData.getAccountMapping());
	}
	
	@Test(expected=PSD2Exception.class)
	public void testRetrieveAccountMappingDetailsForExceptionFalse() {
		ReflectionTestUtils.setField(obj, "platformStored", false);
		AccountIdentificationMapping accountIdentificationMapping=new AccountIdentificationMapping();
		List<IdntificationMapping> identificationMaplist=new ArrayList<>();
		identificationMaplist.add(null);
		accountIdentificationMapping.setIdentificationMappingList(identificationMaplist);
		when(accountIdentificationMappingAdapter.retrieveAccountIdentificationMapping(anyListOf(String.class))).thenReturn(null);
		obj.retrieveAccountMappingDetails(ApiComponentsMockData.getAccountMapping());
	}
	
	@Test(expected=PSD2Exception.class)
	public void testRetrieveAccountMappingDetailsForExceptionFalse1() {
		ReflectionTestUtils.setField(obj, "platformStored", false);
		AccountIdentificationMapping accountIdentificationMapping=new AccountIdentificationMapping();
		IdntificationMapping identicationMapping=new IdntificationMapping();
		List<IdntificationMapping> identificationMaplist=new ArrayList<>();
		identificationMaplist.add(identicationMapping);
		accountIdentificationMapping.setIdentificationMappingList(identificationMaplist);
		when(accountIdentificationMappingAdapter.retrieveAccountIdentificationMapping(anyListOf(String.class))).thenReturn(null);
		obj.retrieveAccountMappingDetails(ApiComponentsMockData.getAccountMapping());
	}
}
