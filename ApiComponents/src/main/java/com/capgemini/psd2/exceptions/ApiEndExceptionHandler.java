package com.capgemini.psd2.exceptions;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;

import com.capgemini.psd2.logger.LoggerUtils;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.domain.OBError1;
import com.capgemini.psd2.pisp.domain.OBErrorResponse1;
import com.capgemini.psd2.utilities.JSONUtilities;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Component
public class ApiEndExceptionHandler {

	@Autowired
	private LoggerUtils loggerUtils;

	@Autowired
	private RequestHeaderAttributes requestHeaderAttributes;

	@Value("${api.internalEndpoint:#{null}}")
	private String internalEndPoint;

	@Value("${app.sendExternalApiErrorPayload:#{null}}")
	private Boolean sendExternalApiErrorPayload;

	@Value("${app.sendExternalApiDetailErrorMessage:#{null}}")
	private Boolean sendExternalApiDetailErrorMessage;

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(ApiEndExceptionHandler.class);

	private static final String DO_FILTER_INTERNAL_STRING = "com.capgemini.psd2.logger.PSD2Filter.doFilterInternal()";
	
	private static final String DO_FILTER_INTERNAL = "doFilterInternal";

	public ExceptionAttributes handleControllerAdviceException(Exception e, WebRequest request) { //NOSONAR
		ExceptionAttributes exceptionAttributes = new ExceptionAttributes();
		OBErrorResponse1 obErrorResponse1 = null;
		ErrorInfo errorInfo = null;
		HttpStatus status = null;
		String errorResponse = null;
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

		if (e != null && e instanceof HttpClientErrorException) {
			errorResponse = ((HttpClientErrorException) e).getResponseBodyAsString();
			status = ((HttpClientErrorException) e).getStatusCode();
			errorInfo = new ErrorInfo(String.valueOf(status), errorResponse, errorResponse, status.toString());
		} else if (e != null && ((PSD2Exception) e).getOBErrorResponse1() != null) {
			obErrorResponse1 = ((PSD2Exception) e).getOBErrorResponse1();
			status = obErrorResponse1.getHttpStatus();
			obErrorResponse1.setId(requestHeaderAttributes.getCorrelationId());
			if (obErrorResponse1.getErrors().get(0).getErrorCode()
					.equals(OBErrorCodeEnum.UK_OBIE_RESOURCE_INVALIDFORMAT.getObErrorCode()))
				obErrorResponse1.getErrors().get(0).setMessage(
						OBPSD2ExceptionUtility.specificErrorMessages.get(ErrorMapKeys.RESOURCE_INVALIDFORMAT));

			if (status.value() >= 500)
				obErrorResponse1.getErrors().get(0)
						.setMessage(OBPSD2ExceptionUtility.specificErrorMessages.get(ErrorMapKeys.UNEXPECTED_ERROR));
		} else if (e != null && ((PSD2Exception) e).getErrorInfo() != null) {
			errorInfo = ((PSD2Exception) e).getErrorInfo();

			if (errorInfo.getObErrorCode() != null) {
				HttpStatus httpStatus = HttpStatus.valueOf(Integer.parseInt(errorInfo.getStatusCode()));
				StringBuilder code = new StringBuilder(String.valueOf(httpStatus.value()));
				code.append(" ").append(httpStatus.getReasonPhrase());
				obErrorResponse1 = new OBErrorResponse1();
				obErrorResponse1.setId(requestHeaderAttributes.getCorrelationId());
				obErrorResponse1.setCode(new String(code));

				try {
					JSONObject jsonObject = new JSONObject(errorInfo.toString());
					obErrorResponse1.setMessage((String) jsonObject.get("errorMessage"));
					obErrorResponse1.addErrorsItem(
							new OBError1(errorInfo.getObErrorCode(), (String) jsonObject.get("detailErrorMessage")));
				} catch (JSONException e1) {
					LOG.info("Error occured during error response transformation " + e1);
				}
			}
			status = HttpStatus.valueOf(Integer.parseInt(errorInfo.getStatusCode()));
		}

		if (errorInfo == null && obErrorResponse1 == null) {
			status = HttpStatus.INTERNAL_SERVER_ERROR;
			errorInfo = new ErrorInfo(String.valueOf(status), errorResponse, errorResponse, status.toString());
		}

		/*
		 * Error payload will always be sent for internal apis. Error payload
		 * will be configured for external apis.
		 */
		if (internalEndPoint == null
				|| !((ServletWebRequest) request).getRequest().getRequestURI().startsWith(internalEndPoint)) {
			if (sendExternalApiErrorPayload != null && !sendExternalApiErrorPayload && errorInfo != null) {
				errorInfo.setErrorCode(null);
				errorInfo.setErrorMessageAsNull();
			}
			if (sendExternalApiDetailErrorMessage != null && !sendExternalApiDetailErrorMessage && errorInfo != null)
				errorInfo.setDetailErrorMessage(null);
		}

		exceptionAttributes.setObErrorResponse1(obErrorResponse1);
		exceptionAttributes.setErrorInfo(errorInfo);
		exceptionAttributes.setHeaders(headers);
		exceptionAttributes.setStatus(status);
		return exceptionAttributes;
	}

	public void handleAndLogPsd2FilterException(Exception e, HttpServletRequest servletRequest,
			HttpServletResponse servletResponse) throws IOException {
		String errorResponseJSONString;
		PSD2Exception psd2Exception;
		OBErrorResponse1 obErrorResponse1;
		ObjectMapper mapper = new ObjectMapper();

		if (e instanceof PSD2Exception)
			psd2Exception = getExceptionData(e);

		else if (e.getCause() instanceof PSD2Exception)
			psd2Exception = getExceptionData((PSD2Exception) e.getCause());
		else
			psd2Exception = PSD2Exception.populatePSD2Exception(e.getMessage(), ErrorCodeEnum.TECHNICAL_ERROR);

		logFilterExceptions(psd2Exception);

		/*
		 * Error payload will always be sent for internal apis. Error payload
		 * will be configured for external apis.
		 */
		if (internalEndPoint == null || !servletRequest.getRequestURI().startsWith(internalEndPoint)) {
			if (sendExternalApiErrorPayload != null && !sendExternalApiErrorPayload
					&& psd2Exception.getErrorInfo() != null) {
				psd2Exception.getErrorInfo().setErrorCode(null);
				psd2Exception.getErrorInfo().setErrorMessageAsNull();
			}
			if (sendExternalApiDetailErrorMessage != null && !sendExternalApiDetailErrorMessage
					&& psd2Exception.getErrorInfo() != null)

				psd2Exception.getErrorInfo().setDetailErrorMessage(null);
		}
		servletResponse.setContentType("application/json");

		obErrorResponse1 = psd2Exception.getOBErrorResponse1();
		if (obErrorResponse1 != null) {
			if (obErrorResponse1.getErrors().get(0).getErrorCode()
					.equals(OBErrorCodeEnum.UK_OBIE_RESOURCE_INVALIDFORMAT.getObErrorCode()))
				obErrorResponse1.getErrors().get(0).setMessage(
						OBPSD2ExceptionUtility.specificErrorMessages.get(ErrorMapKeys.RESOURCE_INVALIDFORMAT));
			if (obErrorResponse1.getHttpStatus().value() >= 500)
				obErrorResponse1.getErrors().get(0)
						.setMessage(OBPSD2ExceptionUtility.specificErrorMessages.get(ErrorMapKeys.UNEXPECTED_ERROR));
			servletResponse.setStatus(psd2Exception.getOBErrorResponse1().getHttpStatus().value());
			mapper.setSerializationInclusion(Include.NON_NULL);
			try {
				errorResponseJSONString = mapper.writeValueAsString(obErrorResponse1);
			} catch (JsonProcessingException e1) {
				PSD2Exception exception = PSD2Exception.generateResponseForInternalErrors(e1);
				exception.getOBErrorResponse1().setId(requestHeaderAttributes.getCorrelationId());
				logFilterExceptions(exception);
				obErrorResponse1.getErrors().get(0)
						.setMessage(OBPSD2ExceptionUtility.specificErrorMessages.get(ErrorMapKeys.UNEXPECTED_ERROR));
				errorResponseJSONString = exception.getOBErrorResponse1().toString();
			}
			servletResponse.getWriter().write(errorResponseJSONString);
		} else {
			servletResponse.setStatus(Integer.parseInt(psd2Exception.getErrorInfo().getStatusCode()));
			servletResponse.getWriter().write(JSONUtilities.getJSONOutPutFromObject(psd2Exception.getErrorInfo()));
		}
	}

	private void logFilterExceptions(Exception e) {
		if (((PSD2Exception) e).getOBErrorResponse1() != null)
			LOG.error("{\"Exception\":\"{}\",\"{}\",\"ErrorDetails\":{}}", DO_FILTER_INTERNAL_STRING,
					loggerUtils.populateLoggerData(DO_FILTER_INTERNAL), ((PSD2Exception) e).getOBErrorResponse1());
		else
			LOG.error("{\"Exception\":\"{}\",\"{}\",\"ErrorDetails\":{}}", DO_FILTER_INTERNAL_STRING,
					loggerUtils.populateLoggerData(DO_FILTER_INTERNAL), ((PSD2Exception) e).getErrorInfo());
		if (LOG.isDebugEnabled()) {
			LOG.error("{\"Exception\":\"{}\",\"{}\",\"ErrorDetails\":\"{}\"}", DO_FILTER_INTERNAL_STRING,
					loggerUtils.populateLoggerData(DO_FILTER_INTERNAL), e.getStackTrace(), e);
		}
	}

	private PSD2Exception getExceptionData(Exception e) {
		PSD2Exception psd2Exception;
		if (((PSD2Exception) e).getErrorInfo() != null)
			return (PSD2Exception) e;
		else {
			psd2Exception = (PSD2Exception) e;
			psd2Exception.getOBErrorResponse1().setId(requestHeaderAttributes.getCorrelationId());
		}
		return psd2Exception;
	}
}