/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.exceptions.PSD2Exception;

/**
 * The Class PSD2Aspect.
 */
@Component
@Aspect
public class PSD2Aspect {

	@Autowired
	private PSD2AspectUtils aspectUtils;

	/**
	 * Arround logger advice service.
	 *
	 * @param proceedingJoinPoint
	 *            the proceeding join point
	 * @return the object
	 */
	@Around("(execution(* com.capgemini.psd2..service..*.* (..)))()")
	public Object arroundLoggerAdviceService(ProceedingJoinPoint proceedingJoinPoint) {
		return aspectUtils.methodAdvice(proceedingJoinPoint);
	}

	/**
	 * Arround logger advice controller.
	 *
	 * @param proceedingJoinPoint
	 *            the proceeding join point
	 * @return the object
	 */
	@Around("execution(* com.capgemini.psd2..controller..* (..)) && !@annotation(com.capgemini.psd2.cma2.aisp.domain.NoPayloadLogging) "
			+ "&& !@target(com.capgemini.psd2.cma2.aisp.domain.NoPayloadLogging)")
	public Object arroundLoggerAdviceController(ProceedingJoinPoint proceedingJoinPoint) {
		return aspectUtils.methodPayloadAdvice(proceedingJoinPoint);
	}
	

	
	/**
	 * Arround logger advice utilities
	 *
	 * @param proceedingJoinPoint
	 *            the proceeding join point
	 * @return the object
	 */
	@Around("execution(* com.capgemini.psd2..utilities.*.*(..)) && !@target(com.capgemini.psd2.cma2.aisp.domain.NoPayloadLogging) && !@annotation(com.capgemini.psd2.cma2.aisp.domain.NoPayloadLogging)")
	public Object arroundLoggerAdviceUtilities(ProceedingJoinPoint proceedingJoinPoint) {
		return aspectUtils.methodAdviceOnDebug(proceedingJoinPoint);
	}
	
	/**
	 * Arround logger advice transformer, adapter, validator
	 *
	 * @param proceedingJoinPoint
	 *            the proceeding join point
	 * @return the object
	 */
	@Around("execution(* com.capgemini.psd2.pisp..transformer..*.*(..)) || execution(* com.capgemini.psd2.validator.*.*(..))")
	public Object arroundLoggerAdviceValidatorAndTransformer(ProceedingJoinPoint proceedingJoinPoint) {
		return aspectUtils.methodAdviceOnDebug(proceedingJoinPoint);
	}


	/**
	 * Arround logger advice controller.
	 *
	 * @param proceedingJoinPoint
	 *            the proceeding join point
	 * @return the object
	 */
	@Around("@annotation(com.capgemini.psd2.cma2.aisp.domain.NoPayloadLogging) "
			+ "&& @target(com.capgemini.psd2.cma2.aisp.domain.NoPayloadLogging)")
	public Object arroundFileDownloadAdviceController(ProceedingJoinPoint proceedingJoinPoint) {
		return aspectUtils.methodAdvice(proceedingJoinPoint);
	}

	
	/**
	 * This method throws {@link PSD2Exception} on posting no body or invalid
	 * body according to swagger in account request POST API and can be used for
	 * any other POST API
	 * 
	 * @param joinPoint
	 * @param error
	 * 
	 * @throws PSD2Exception
	 * 
	 */
	@AfterThrowing(pointcut = "execution(* org.springframework.http.converter.json.AbstractJackson2HttpMessageConverter.read(..))", throwing = "error")
	public void throwExcpetionOnJsonBinding(JoinPoint joinPoint, Throwable error) {
		aspectUtils.throwExceptionOnJsonBinding(joinPoint, error);
	}

}
