package com.capgemini.tpp.ob.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * OBAccountsPostalAddresses
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-12-21T15:23:35.120+05:30")

public class OBAccountsPostalAddresses   {
  @JsonProperty("AddressLine2")
  private String addressLine2 = null;

  @JsonProperty("Country")
  private String country = null;

  @JsonProperty("County")
  private String county = null;

  @JsonProperty("Name")
  private String name = null;

  @JsonProperty("POBox")
  private String poBox = null;

  @JsonProperty("PostCode")
  private String postCode = null;

  @JsonProperty("Primary")
  private Boolean primary = null;

  @JsonProperty("StreetAddress")
  private String streetAddress = null;

  @JsonProperty("Town")
  private String town = null;

  @JsonProperty("Type")
  private String type = null;

  public OBAccountsPostalAddresses addressLine2(String addressLine2) {
    this.addressLine2 = addressLine2;
    return this;
  }

   /**
   * Extra locality information such as the Village or Minor Town associated with the postal address
   * @return addressLine2
  **/
  @ApiModelProperty(value = "Extra locality information such as the Village or Minor Town associated with the postal address")


  public String getAddressLine2() {
    return addressLine2;
  }

  public void setAddressLine2(String addressLine2) {
    this.addressLine2 = addressLine2;
  }

  public OBAccountsPostalAddresses country(String country) {
    this.country = country;
    return this;
  }

   /**
   * 2 digit country code associated with the address
   * @return country
  **/
  @ApiModelProperty(value = "2 digit country code associated with the address")


  public String getCountry() {
    return country;
  }

  public void setCountry(String country) {
    this.country = country;
  }

  public OBAccountsPostalAddresses county(String county) {
    this.county = county;
    return this;
  }

   /**
   * The country name of the address
   * @return county
  **/
  @ApiModelProperty(value = "The country name of the address")


  public String getCounty() {
    return county;
  }

  public void setCounty(String county) {
    this.county = county;
  }

  public OBAccountsPostalAddresses name(String name) {
    this.name = name;
    return this;
  }

   /**
   * Name of contact
   * @return name
  **/
  @ApiModelProperty(value = "Name of contact")


  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public OBAccountsPostalAddresses poBox(String poBox) {
    this.poBox = poBox;
    return this;
  }

   /**
   * POBox
   * @return poBox
  **/
  @ApiModelProperty(value = "POBox")


  public String getPoBox() {
    return poBox;
  }

  public void setPoBox(String poBox) {
    this.poBox = poBox;
  }

  public OBAccountsPostalAddresses postCode(String postCode) {
    this.postCode = postCode;
    return this;
  }

   /**
   * The post code of the address
   * @return postCode
  **/
  @ApiModelProperty(value = "The post code of the address")


  public String getPostCode() {
    return postCode;
  }

  public void setPostCode(String postCode) {
    this.postCode = postCode;
  }

  public OBAccountsPostalAddresses primary(Boolean primary) {
    this.primary = primary;
    return this;
  }

   /**
   * Boolean indicating if this is the primary address
   * @return primary
  **/
  @ApiModelProperty(value = "Boolean indicating if this is the primary address")


  public Boolean getPrimary() {
    return primary;
  }

  public void setPrimary(Boolean primary) {
    this.primary = primary;
  }

  public OBAccountsPostalAddresses streetAddress(String streetAddress) {
    this.streetAddress = streetAddress;
    return this;
  }

   /**
   * The Street address including the house name and number
   * @return streetAddress
  **/
  @ApiModelProperty(value = "The Street address including the house name and number")


  public String getStreetAddress() {
    return streetAddress;
  }

  public void setStreetAddress(String streetAddress) {
    this.streetAddress = streetAddress;
  }

  public OBAccountsPostalAddresses town(String town) {
    this.town = town;
    return this;
  }

   /**
   * The postal address town
   * @return town
  **/
  @ApiModelProperty(value = "The postal address town")


  public String getTown() {
    return town;
  }

  public void setTown(String town) {
    this.town = town;
  }

  public OBAccountsPostalAddresses type(String type) {
    this.type = type;
    return this;
  }

   /**
   * Type of contact
   * @return type
  **/
  @ApiModelProperty(value = "Type of contact")


  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    OBAccountsPostalAddresses obAccountsPostalAddresses = (OBAccountsPostalAddresses) o;
    return Objects.equals(this.addressLine2, obAccountsPostalAddresses.addressLine2) &&
        Objects.equals(this.country, obAccountsPostalAddresses.country) &&
        Objects.equals(this.county, obAccountsPostalAddresses.county) &&
        Objects.equals(this.name, obAccountsPostalAddresses.name) &&
        Objects.equals(this.poBox, obAccountsPostalAddresses.poBox) &&
        Objects.equals(this.postCode, obAccountsPostalAddresses.postCode) &&
        Objects.equals(this.primary, obAccountsPostalAddresses.primary) &&
        Objects.equals(this.streetAddress, obAccountsPostalAddresses.streetAddress) &&
        Objects.equals(this.town, obAccountsPostalAddresses.town) &&
        Objects.equals(this.type, obAccountsPostalAddresses.type);
  }

  @Override
  public int hashCode() {
    return Objects.hash(addressLine2, country, county, name, poBox, postCode, primary, streetAddress, town, type);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class OBAccountsPostalAddresses {\n");
    
    sb.append("    addressLine2: ").append(toIndentedString(addressLine2)).append("\n");
    sb.append("    country: ").append(toIndentedString(country)).append("\n");
    sb.append("    county: ").append(toIndentedString(county)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    poBox: ").append(toIndentedString(poBox)).append("\n");
    sb.append("    postCode: ").append(toIndentedString(postCode)).append("\n");
    sb.append("    primary: ").append(toIndentedString(primary)).append("\n");
    sb.append("    streetAddress: ").append(toIndentedString(streetAddress)).append("\n");
    sb.append("    town: ").append(toIndentedString(town)).append("\n");
    sb.append("    type: ").append(toIndentedString(type)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

