
package com.capgemini.tpp.muleapi.schema;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "assigned",
    "reassigned"
})
public class VCoresDesign implements Serializable
{

    @JsonProperty("assigned")
    private Integer assigned;
    @JsonProperty("reassigned")
    private Double reassigned;
    private static final long serialVersionUID = -3130278394102935940L;

    @JsonProperty("assigned")
    public Integer getAssigned() {
        return assigned;
    }

    @JsonProperty("assigned")
    public void setAssigned(Integer assigned) {
        this.assigned = assigned;
    }

    @JsonProperty("reassigned")
    public Double getReassigned() {
        return reassigned;
    }

    @JsonProperty("reassigned")
    public void setReassigned(Double reassigned) {
        this.reassigned = reassigned;
    }

}
