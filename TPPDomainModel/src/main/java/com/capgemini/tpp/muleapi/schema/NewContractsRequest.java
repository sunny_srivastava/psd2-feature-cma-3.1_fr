package com.capgemini.tpp.muleapi.schema;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({ "apiVersionId", "applicationId", "partyId", "partyName", "requestedTierId" ,"acceptedTerms" })
public class NewContractsRequest implements Serializable {

	@JsonProperty("apiVersionId")
	private int apiVersionId;
	@JsonProperty("applicationId")
	private int applicationId;
	@JsonProperty("partyId")
	private String partyId;
	@JsonProperty("partyName")
	private String partyName;
	@JsonProperty("requestedTierId")
	private Integer requestedTierId;
	@JsonProperty("acceptedTerms")
	private boolean acceptedTerms;
	public Integer getRequestedTierId() {
		return requestedTierId;
	}

	public void setRequestedTierId(Integer requestedTierId) {
		this.requestedTierId = requestedTierId;
	}

	private static final long serialVersionUID = -5222542984577467717L;

	@JsonProperty("apiVersionId")
	public int getApiVersionId() {
		return apiVersionId;
	}

	@JsonProperty("apiVersionId")
	public void setApiVersionId(int apiVersionId) {
		this.apiVersionId = apiVersionId;
	}

	@JsonProperty("applicationId")
	public int getApplicationId() {
		return applicationId;
	}

	@JsonProperty("applicationId")
	public void setApplicationId(int applicationId) {
		this.applicationId = applicationId;
	}

	
	@JsonProperty("partyId")
	public String getPartyId() {
		return partyId;
	}

	@JsonProperty("partyId")
	public void setPartyId(String partyId) {
		this.partyId = partyId;
	}

	
	@JsonProperty("partyName")
	public String getPartyName() {
		return partyName;
	}

	@JsonProperty("partyName")
	public void setPartyName(String partyName) {
		this.partyName = partyName;
	}

	
	@JsonProperty("acceptedTerms")
	public boolean isAcceptedTerms() {
		return acceptedTerms;
	}

	@JsonProperty("acceptedTerms")
	public void setAcceptedTerms(boolean acceptedTerms) {
		this.acceptedTerms = acceptedTerms;
	}

	
}