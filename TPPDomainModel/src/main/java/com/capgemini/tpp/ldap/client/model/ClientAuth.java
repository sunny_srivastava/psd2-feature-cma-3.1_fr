package com.capgemini.tpp.ldap.client.model;

public class ClientAuth {

	private String type;
	private String secret;
	private String encryptedSecret;
	private String clientCertIssuerDn;
	private String clientCertSubjectDn;
	private String enforceReplayPrevention;

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getSecret() {
		return this.secret;
	}

	public void setSecret(String secret) {
		this.secret = secret;
	}

	public String getEncryptedSecret() {
		return this.encryptedSecret;
	}

	public void setEncryptedSecret(String encryptedSecret) {
		this.encryptedSecret = encryptedSecret;
	}

	public String getClientCertIssuerDn() {
		return this.clientCertIssuerDn;
	}

	public void setClientCertIssuerDn(String clientCertIssuerDn) {
		this.clientCertIssuerDn = clientCertIssuerDn;
	}

	public String getClientCertSubjectDn() {
		return this.clientCertSubjectDn;
	}

	public void setClientCertSubjectDn(String clientCertSubjectDn) {
		this.clientCertSubjectDn = clientCertSubjectDn;
	}

	public String getEnforceReplayPrevention() {
		return this.enforceReplayPrevention;
	}

	public void setEnforceReplayPrevention(String enforceReplayPrevention) {
		this.enforceReplayPrevention = enforceReplayPrevention;
	}
}