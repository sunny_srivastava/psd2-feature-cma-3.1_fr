package com.capgemini.tpp.ldap.client.model;

import java.util.Comparator;
import java.util.List;

public class ClientModel {

	private String clientId;
	private List<String> redirectUris;
	private List<String> grantTypes;
	private String name;
	private String description;
	private DefaultAccessTokenManagerRef defaultAccessTokenManagerRef;
	private String logoUrl;
	private String validateUsingAllEligibleAtms;
	private String refreshRolling;
	private String persistentGrantExpirationType;
	private String persistentGrantExpirationTime;
	private String persistentGrantExpirationTimeUnit;
	private String bypassApprovalPage;
	private String restrictScopes;
	private List<String> restrictedScopes;
	private List<String> exclusiveScopes;
	private List<String> restrictedResponseTypes;
	private String requireSignedRequests;
	private OidcPolicy oidcPolicy;
	private ClientAuth clientAuth;
	private JwksSettings jwksSettings;

	private String requestObjectSigningAlgorithm;

	public String getRequestObjectSigningAlgorithm() {
		return requestObjectSigningAlgorithm;
	}

	public void setRequestObjectSigningAlgorithm(String requestObjectSigningAlgorithm) {
		this.requestObjectSigningAlgorithm = requestObjectSigningAlgorithm;
	}

	public String getClientId() {
		return this.clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public List<String> getRedirectUris() {
		return this.redirectUris;
	}

	public void setRedirectUris(List<String> redirectUris) {
		this.redirectUris = redirectUris;
	}


	public List<String> getGrantTypes() {
		return this.grantTypes;
	}

	public void setGrantTypes(List<String> grantTypes) {
		this.grantTypes = grantTypes;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<String> getExclusiveScopes() {
		return exclusiveScopes;
	}

	public void setExclusiveScopes(List<String> exclusiveScopes) {
		this.exclusiveScopes = exclusiveScopes;
	}

	public List<String> getRestrictedResponseTypes() {
		return restrictedResponseTypes;
	}

	public void setRestrictedResponseTypes(List<String> restrictedResponseTypes) {
		this.restrictedResponseTypes = restrictedResponseTypes;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getLogoUrl() {
		return this.logoUrl;
	}

	public void setLogoUrl(String logoUrl) {
		this.logoUrl = logoUrl;
	}

	public DefaultAccessTokenManagerRef getDefaultAccessTokenManagerRef() {
		return this.defaultAccessTokenManagerRef;
	}

	public void setDefaultAccessTokenManagerRef(DefaultAccessTokenManagerRef defaultAccessTokenManagerRef) {
		this.defaultAccessTokenManagerRef = defaultAccessTokenManagerRef;
	}

	public String getValidateUsingAllEligibleAtms() {
		return this.validateUsingAllEligibleAtms;
	}

	public void setValidateUsingAllEligibleAtms(String validateUsingAllEligibleAtms) {
		this.validateUsingAllEligibleAtms = validateUsingAllEligibleAtms;
	}

	public String getRefreshRolling() {
		return this.refreshRolling;
	}

	public void setRefreshRolling(String refreshRolling) {
		this.refreshRolling = refreshRolling;
	}

	public String getPersistentGrantExpirationType() {
		return this.persistentGrantExpirationType;
	}

	public void setPersistentGrantExpirationType(String persistentGrantExpirationType) {
		this.persistentGrantExpirationType = persistentGrantExpirationType;
	}

	public String getPersistentGrantExpirationTime() {
		return this.persistentGrantExpirationTime;
	}

	public void setPersistentGrantExpirationTime(String persistentGrantExpirationTime) {
		this.persistentGrantExpirationTime = persistentGrantExpirationTime;
	}

	public String getPersistentGrantExpirationTimeUnit() {
		return this.persistentGrantExpirationTimeUnit;
	}

	public void setPersistentGrantExpirationTimeUnit(String persistentGrantExpirationTimeUnit) {
		this.persistentGrantExpirationTimeUnit = persistentGrantExpirationTimeUnit;
	}

	public String getBypassApprovalPage() {
		return this.bypassApprovalPage;
	}

	public void setBypassApprovalPage(String bypassApprovalPage) {
		this.bypassApprovalPage = bypassApprovalPage;
	}

	public String getRestrictScopes() {
		return this.restrictScopes;
	}

	public void setRestrictScopes(String restrictScopes) {
		this.restrictScopes = restrictScopes;
	}

	public List<String> getRestrictedScopes() {
		return this.restrictedScopes;
	}

	public void setRestrictedScopes(List<String> restrictedScopes) {
		this.restrictedScopes = restrictedScopes;
	}

	public String getRequireSignedRequests() {
		return this.requireSignedRequests;
	}

	public void setRequireSignedRequests(String requireSignedRequests) {
		this.requireSignedRequests = requireSignedRequests;
	}

	public OidcPolicy getOidcPolicy() {
		return this.oidcPolicy;
	}

	public void setOidcPolicy(OidcPolicy oidcPolicy) {
		this.oidcPolicy = oidcPolicy;
	}

	public ClientAuth getClientAuth() {
		return this.clientAuth;
	}

	public void setClientAuth(ClientAuth clientAuth) {
		this.clientAuth = clientAuth;
	}

	public JwksSettings getJwksSettings() {
		return this.jwksSettings;
	}

	public void setJwksSettings(JwksSettings jwksSettings) {
		this.jwksSettings = jwksSettings;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof ClientModel))
			return false;
		ClientModel other = (ClientModel) obj;
		grantTypes.sort(Comparator.naturalOrder());
		(other.grantTypes).sort(Comparator.naturalOrder());
		restrictedScopes.sort(Comparator.naturalOrder());
		(other.restrictedScopes).sort(Comparator.naturalOrder());
		if (bypassApprovalPage == null) {
			if (other.bypassApprovalPage != null)
				return false;
		} else if (!bypassApprovalPage.equals(other.bypassApprovalPage))
			return false;
		if (clientId == null) {
			if (other.clientId != null)
				return false;
		} else if (!clientId.equals(other.clientId))
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.trim().equals(other.description.trim()))
			return false;
		if (exclusiveScopes == null) {
			if (other.exclusiveScopes != null)
				return false;
		} else if (!exclusiveScopes.equals(other.exclusiveScopes))
			return false;
		if (grantTypes == null) {
			if (other.grantTypes != null)
				return false;
		} else if (!(grantTypes).equals(other.grantTypes)) {
			return false;}
		if (logoUrl == null) {
			if (other.logoUrl != null)
				return false;
		} else if (!logoUrl.trim().equals(other.logoUrl.trim()))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.trim().equals(other.name.trim()))
			return false;
		if (persistentGrantExpirationTime == null) {
			if (other.persistentGrantExpirationTime != null)
				return false;
		} else if (!persistentGrantExpirationTime.equals(other.persistentGrantExpirationTime))
			return false;
		if (persistentGrantExpirationTimeUnit == null) {
			if (other.persistentGrantExpirationTimeUnit != null)
				return false;
		} else if (!persistentGrantExpirationTimeUnit.equals(other.persistentGrantExpirationTimeUnit))
			return false;
		if (persistentGrantExpirationType == null) {
			if (other.persistentGrantExpirationType != null)
				return false;
		} else if (!persistentGrantExpirationType.equals(other.persistentGrantExpirationType))
			return false;
		if (redirectUris == null) {
			if (other.redirectUris != null)
				return false;
		} else if (!redirectUris.containsAll(other.redirectUris))
			return false;
		if (refreshRolling == null) {
			if (other.refreshRolling != null)
				return false;
		} else if (!refreshRolling.equals(other.refreshRolling))
			return false;
		if (requireSignedRequests == null) {
			if (other.requireSignedRequests != null)
				return false;
		} else if (!requireSignedRequests.equals(other.requireSignedRequests))
			return false;
		if (restrictScopes == null) {
			if (other.restrictScopes != null)
				return false;
		} else if (!restrictScopes.equals(other.restrictScopes))
			return false;
		if (restrictedResponseTypes == null) {
			if (other.restrictedResponseTypes != null)
				return false;
		} else if (!restrictedResponseTypes.equals(other.restrictedResponseTypes))
			return false;
		if (restrictedScopes == null) {
			if (other.restrictedScopes != null)
				return false;
		} else if (!restrictedScopes.equals(other.restrictedScopes))
			return false;
		if (validateUsingAllEligibleAtms == null) {
			if (other.validateUsingAllEligibleAtms != null)
				return false;
		} else if (!validateUsingAllEligibleAtms.equals(other.validateUsingAllEligibleAtms))
			return false;
		return true;
	}
}