package com.capgemini.tpp.ldap.client.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class PTCUser implements Serializable {
	

	private static final long serialVersionUID = 1L;

	private String cn;
	private String giveName;
	private String uid;
	private String sn;
	private String userPassword;
	private List<String> isMemberOf = new ArrayList<String>();
	/**
	 * @return the cn
	 */
	public String getCn() {
		return cn;
	}
	/**
	 * @param cn the cn to set
	 */
	public void setCn(String cn) {
		this.cn = cn;
	}
	/**
	 * @return the giveName
	 */
	public String getGiveName() {
		return giveName;
	}
	/**
	 * @param giveName the giveName to set
	 */
	public void setGiveName(String giveName) {
		this.giveName = giveName;
	}
	/**
	 * @return the uid
	 */
	public String getUid() {
		return uid;
	}
	/**
	 * @param uid the uid to set
	 */
	public void setUid(String uid) {
		this.uid = uid;
	}
	/**
	 * @return the sn
	 */
	public String getSn() {
		return sn;
	}
	/**
	 * @param sn the sn to set
	 */
	public void setSn(String sn) {
		this.sn = sn;
	}
	/**
	 * @return the userPassword
	 */
	public String getUserPassword() {
		return userPassword;
	}
	/**
	 * @param userPassword the userPassword to set
	 */
	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}
	/**
	 * @return the isMemberOf
	 */
	public List<String> getIsMemberOf() {
		return isMemberOf;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("PTCUser [getCn()=");
		builder.append(getCn());
		builder.append(", getGiveName()=");
		builder.append(getGiveName());
		builder.append(", getUid()=");
		builder.append(getUid());
		builder.append(", getSn()=");
		builder.append(getSn());
		builder.append(", getUserPassword()=");
		builder.append(getUserPassword());
		builder.append(", getIsMemberOf()=");
		builder.append(getIsMemberOf());
		builder.append("]");
		return builder.toString();
	}
	
	
	
}
