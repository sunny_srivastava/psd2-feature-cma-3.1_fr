/*package com.capgemini.tpp.registration.model;

import java.util.ArrayList;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;


@Component
@JsonInclude(Include.NON_NULL)
public class RegistrationResponse {

	private String client_id;

	private String client_secret;

	public String getClient_secret() {
		return client_secret;
	}

	public void setClient_secret(String client_secret) {
		this.client_secret = client_secret;
	}

	private String org_id;

	public String getOrg_id() {
		return this.org_id;
	}

	public void setOrg_id(String org_id) {
		this.org_id = org_id;
	}

	private String software_logo_uri;

	private String software_client_name;

	private String software_jwks_endpoint;

	private String software_id;

	public String getSoftware_id() {
		return this.software_id;
	}

	public void setSoftware_id(String software_id) {
		this.software_id = software_id;
	}

	private ArrayList<String> redirect_uris;

	private String scope;

	private String aud;

	private String token_endpoint_auth_method;

	private ArrayList<String> grant_types;

	private ArrayList<String> response_types;

	private String application_type;

	private String id_token_signed_response_alg;

	private String request_object_signing_alg;
	
	private ArrayList<String> software_roles;

	public ArrayList<String> getSoftware_roles() {
		return software_roles;
	}

	public void setSoftware_roles(ArrayList<String> software_roles) {
		this.software_roles = software_roles;
	}

	public ArrayList<String> getRedirect_uris() {
		return redirect_uris;
	}

	public void setRedirect_uris(ArrayList<String> redirect_uris) {
		this.redirect_uris = redirect_uris;
	}

	public String getScope() {
		return scope;
	}

	public void setScope(String scope) {
		this.scope = scope;
	}

	public String getAud() {
		return aud;
	}

	public void setAud(String aud) {
		this.aud = aud;
	}

	public String getToken_endpoint_auth_method() {
		return token_endpoint_auth_method;
	}

	public void setToken_endpoint_auth_method(String token_endpoint_auth_method) {
		this.token_endpoint_auth_method = token_endpoint_auth_method;
	}

	public ArrayList<String> getGrant_types() {
		return grant_types;
	}

	public void setGrant_types(ArrayList<String> grant_types) {
		this.grant_types = grant_types;
	}

	public ArrayList<String> getResponse_types() {
		return response_types;
	}

	public void setResponse_types(ArrayList<String> response_types) {
		this.response_types = response_types;
	}

	public String getApplication_type() {
		return application_type;
	}

	public void setApplication_type(String application_type) {
		this.application_type = application_type;
	}

	public String getId_token_signed_response_alg() {
		return id_token_signed_response_alg;
	}

	public void setId_token_signed_response_alg(String id_token_signed_response_alg) {
		this.id_token_signed_response_alg = id_token_signed_response_alg;
	}

	public String getRequest_object_signing_alg() {
		return request_object_signing_alg;
	}

	public void setRequest_object_signing_alg(String request_object_signing_alg) {
		this.request_object_signing_alg = request_object_signing_alg;
	}

	public String getClient_id() {
		return client_id;
	}

	public void setClient_id(String client_id) {
		this.client_id = client_id;
	}

	public String getSoftware_logo_uri() {
		return software_logo_uri;
	}

	public void setSoftware_logo_uri(String software_logo_uri) {
		this.software_logo_uri = software_logo_uri;
	}

	public String getSoftware_client_name() {
		return software_client_name;
	}

	public void setSoftware_client_name(String software_client_name) {
		this.software_client_name = software_client_name;
	}

	public String getSoftware_jwks_endpoint() {
		return software_jwks_endpoint;
	}

	public void setSoftware_jwks_endpoint(String software_jwks_endpoint) {
		this.software_jwks_endpoint = software_jwks_endpoint;
	}

}
*/