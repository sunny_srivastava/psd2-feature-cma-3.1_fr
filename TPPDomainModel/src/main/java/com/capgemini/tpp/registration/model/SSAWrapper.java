package com.capgemini.tpp.registration.model;

import org.springframework.stereotype.Component;

import com.capgemini.tpp.ssa.model.SSAModel;

@Component
public class SSAWrapper {

	private SSAModel ssaModel;
	
	private String ssaToken;
	
	private OBClientRegistration1 obClientRegistration1;

	public OBClientRegistration1 getObClientRegistration1() {
		return obClientRegistration1;
	}

	public void setObClientRegistration1(OBClientRegistration1 obClientRegistration1) {
		this.obClientRegistration1 = obClientRegistration1;
	}
	public SSAModel getSsaModel() {
		return ssaModel;
	}

	public void setSsaModel(SSAModel ssaModel) {
		this.ssaModel = ssaModel;
	}

	public String getSsaToken() {
		return ssaToken;
	}

	public void setSsaToken(String ssaToken) {
		this.ssaToken = ssaToken;
	}
}
