package com.capgemini.tpp.registration.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.annotations.SerializedName;

import java.io.IOException;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;


/**
 * Gets or Sets SupportedAlgorithms
 */
@JsonAdapter(SupportedAlgorithms.Adapter.class)
public enum SupportedAlgorithms {
  
  RS256("RS256"),
  
  PS256("PS256"),
  
  ES256("ES256");

  private String value;

  SupportedAlgorithms(String value) {
    this.value = value;
  }

  public String getValue() {
    return value;
  }
  @Override

  public String toString() {
    return String.valueOf(value);
  }

  
  public static SupportedAlgorithms fromValue(String text) {
    for (SupportedAlgorithms b : SupportedAlgorithms.values()) {
      if (String.valueOf(b.value).equals(text)) {
        return b;
      }
    }
    return null;
  }
  public static class Adapter extends TypeAdapter<SupportedAlgorithms> {
    @Override
    public void write(final JsonWriter jsonWriter, final SupportedAlgorithms enumeration) throws IOException {
      jsonWriter.value(enumeration.getValue());
    }

    @Override
    public SupportedAlgorithms read(final JsonReader jsonReader) throws IOException {
      String value = jsonReader.nextString();
      return SupportedAlgorithms.fromValue(String.valueOf(value));
    }
  }
}

