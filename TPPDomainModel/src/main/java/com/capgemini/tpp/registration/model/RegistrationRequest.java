/*package com.capgemini.tpp.registration.model;

import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

*//**
 * RegistrationRequest model
 * @author nagoswam
 *
 *//*
public class RegistrationRequest {

	private long iat;

	public long getIat() {
		return this.iat;
	}

	public void setIat(long iat) {
		this.iat = iat;
	}

	private String iss;

	public String getIss() {
		return this.iss;
	}

	public void setIss(String iss) {
		this.iss = iss;
	}

	private String sub;

	public String getSub() {
		return sub;
	}

	public void setSub(String sub) {
		this.sub = sub;
	}

	private String jti;

	public String getJti() {
		return this.jti;
	}

	public void setJti(String jti) {
		this.jti = jti;
	}

	private long exp;

	public long getExp() {
		return exp;
	}

	public void setExp(long exp) {
		this.exp = exp;
	}

	private String org_id;

	public String getOrg_id() {
		return this.org_id;
	}

	public void setOrg_id(String org_id) {
		this.org_id = org_id;
	}

	private String software_id;

	public String getSoftware_id() {
		return this.software_id;
	}

	public void setSoftware_id(String software_id) {
		this.software_id = software_id;
	}

	private ArrayList<String> redirect_uris;

	private String scope;

	private String aud;

	private String token_endpoint_auth_method;

	private ArrayList<String> grant_types;

	private ArrayList<String> response_types;

	private String software_statement;

	private String application_type;

	private String id_token_signed_response_alg;

	private String request_object_signing_alg;
	
	private String tls_client_auth_dn;

	public String getTls_client_auth_dn() {
		return tls_client_auth_dn;
	}

	public void setTls_client_auth_dn(String tls_client_auth_dn) {
		this.tls_client_auth_dn = tls_client_auth_dn;
	}

	public ArrayList<String> getRedirect_uris() {
		return redirect_uris;
	}

	public void setRedirect_uris(ArrayList<String> redirect_uris) {
		this.redirect_uris = redirect_uris;
	}

	public String getScope() {
		return scope;
	}

	public void setScope(String scope) {
		this.scope = scope;
	}

	public String getAud() {
		return aud;
	}

	public void setAud(String aud) {
		this.aud = aud;
	}

	public String getToken_endpoint_auth_method() {
		return token_endpoint_auth_method;
	}

	public void setToken_endpoint_auth_method(String token_endpoint_auth_method) {
		this.token_endpoint_auth_method = token_endpoint_auth_method;
	}

	public ArrayList<String> getGrant_types() {
		return grant_types;
	}

	public void setGrant_types(ArrayList<String> grant_types) {
		this.grant_types = grant_types;
	}

	public ArrayList<String> getResponse_types() {
		return response_types;
	}

	public void setResponse_types(ArrayList<String> response_types) {
		this.response_types = response_types;
	}

	public String getSoftware_statement() {
		return software_statement;
	}

	public void setSoftware_statement(String software_statement) {
		this.software_statement = software_statement;
	}

	public String getApplication_type() {
		return application_type;
	}

	public void setApplication_type(String application_type) {
		this.application_type = application_type;
	}

	public String getId_token_signed_response_alg() {
		return id_token_signed_response_alg;
	}

	public void setId_token_signed_response_alg(String id_token_signed_response_alg) {
		this.id_token_signed_response_alg = id_token_signed_response_alg;
	}

	public String getRequest_object_signing_alg() {
		return request_object_signing_alg;
	}

	public void setRequest_object_signing_alg(String request_object_signing_alg) {
		this.request_object_signing_alg = request_object_signing_alg;
	}

	@JsonCreator
	public RegistrationRequest(@JsonProperty(value = "iat", required = true) long iat,
			@JsonProperty(value = "iss", required = true) String iss,
			@JsonProperty(value = "sub", required = false) String sub,
			@JsonProperty(value = "jti", required = true) String jti,
			@JsonProperty(value = "exp", required = true) long exp,
			@JsonProperty(value = "org_id", required = false) String org_id,
			@JsonProperty(value = "software_id", required = false) String software_id,
			@JsonProperty(value = "redirect_uris", required = true) ArrayList<String> redirect_uris,
			@JsonProperty(value = "scope", required = false) String scope,
			@JsonProperty(value = "aud", required = true) String aud,
			@JsonProperty(value = "token_endpoint_auth_method", required = true) String token_endpoint_auth_method,
			@JsonProperty(value = "grant_types", required = true) ArrayList<String> grant_types,
			@JsonProperty(value = "response_types", required = false) ArrayList<String> response_types,
			@JsonProperty(value = "software_statement", required = true) String software_statement,
			@JsonProperty(value = "application_type", required = true) String application_type,
			@JsonProperty(value = "id_token_signed_response_alg", required = true) String id_token_signed_response_alg,
			@JsonProperty(value = "request_object_signing_alg", required = true) String request_object_signing_alg,
			@JsonProperty(value = "tls_client_auth_dn", required = true) String tls_client_auth_dn) {
		super();
		this.iat = iat;
		this.iss = iss;
		this.sub = sub;
		this.jti = jti;
		this.exp = exp;
		this.org_id = org_id;
		this.software_id = software_id;
		this.redirect_uris = redirect_uris;
		this.scope = scope;
		this.aud = aud;
		this.token_endpoint_auth_method = token_endpoint_auth_method;
		this.grant_types = grant_types;
		this.response_types = response_types;
		this.software_statement = software_statement;
		this.application_type = application_type;
		this.id_token_signed_response_alg = id_token_signed_response_alg;
		this.request_object_signing_alg = request_object_signing_alg;
		this.tls_client_auth_dn = tls_client_auth_dn;
	}

}
*/