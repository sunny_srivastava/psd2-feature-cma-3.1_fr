package com.capgemini.psd2.foundationservice.international.payment.authorisingparty.boi.adapter.delegate;

import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.foundationservice.international.payment.authorisingparty.boi.adapter.domain.BrandCode3;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Component
public class InternationalPaymentAuthorisingPartyFoundationServiceDelegate {

	
	@Value("${foundationService.transactionReqHeader:#{X-API-TRANSACTION-ID}}")
	private String transactionHeader;
	
	@Value("${foundationService.correlationMuleReqHeader:#{X-API-CORRELATION-ID}}")
	private String correlationHeader;
	
	@Value("${foundationService.sourceUserReqHeader:#{X-API-SOURCE-USER}}")
	private String sourceUserHeader;

	@Value("${foundationService.sourcesystem:#{X-API-SOURCE-SYSTEM}}")
	private String sourceSystemHeader;

	@Value("${foundationService.apiChannelCode:#{X-API-CHANNEL-CODE}}")
	private String apiChannelCodeHeader;
	
	@Value("${foundationService.apiChannelBrand:#{X-API-CHANNEL-BRAND}}")
	private String apiChannelBrandHeader;
	
	@Value("${foundationService.systemApiVersion:#{X-SYSTEM-API-VERSION}}")
	private String systemApiVersionHeader;
	
	@Value("${foundationService.apiChannelCode:#{X-API-PARTY-SOURCE-ID-NUMBER}}")
	private String partySourceIdHeader;
	
	@Value("${foundationService.internationalPaymentAuthoriseConsentPostBaseURL}")
	private String internationalPaymentAuthoriseConsentPostBaseURL;

	@Value("${foundationService.internationalPaymentAuthoriseConsentSetupVersion}")
	private String internationalPaymentAuthoriseConsentSetupVersion;
	
	
	public HttpHeaders createPaymentRequestHeadersPost(CustomPaymentStageIdentifiers customStageIdentifiers, Map<String, String> params) {
		
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.add(sourceSystemHeader, "PSD2API");
		
    	if (!NullCheckUtils
				.isNullOrEmpty(params.get(PSD2Constants.USER_IN_REQ_HEADER)))
			httpHeaders.add(sourceUserHeader, params.get(PSD2Constants.USER_IN_REQ_HEADER));
		
		if (!NullCheckUtils
				.isNullOrEmpty(params.get(PSD2Constants.CORRELATION_REQ_HEADER)))
			httpHeaders.add(transactionHeader, params.get(PSD2Constants.CORRELATION_REQ_HEADER));			
		
		if (!NullCheckUtils
				.isNullOrEmpty(params.get(PSD2Constants.CHANNEL_IN_REQ_HEADER)))
			httpHeaders.add(apiChannelCodeHeader, params.get(PSD2Constants.CHANNEL_IN_REQ_HEADER).toUpperCase());
		
		httpHeaders.add(systemApiVersionHeader, "3.0");
		
	 if (!NullCheckUtils.isNullOrEmpty(params.get(PSD2Constants.TENANT_ID))) {			
		if(params.get(PSD2Constants.TENANT_ID).equalsIgnoreCase("BOIROI")){
			httpHeaders.add(apiChannelBrandHeader, BrandCode3.ROI.toString());
		  }
		else
			httpHeaders.add(apiChannelBrandHeader, BrandCode3.NIGB.toString());
	 }
	 
	
		return httpHeaders;
		
	}
	
	public String internationalPaymentAuthoriseConsentPostBaseURL(String paymentInstuctionProposalId) {
		return internationalPaymentAuthoriseConsentPostBaseURL + "/" + "v" + internationalPaymentAuthoriseConsentSetupVersion + "/"
				+ "international/payment-instruction-proposals" + "/" + paymentInstuctionProposalId + "/authorising-party";
	}	
	
	
}
