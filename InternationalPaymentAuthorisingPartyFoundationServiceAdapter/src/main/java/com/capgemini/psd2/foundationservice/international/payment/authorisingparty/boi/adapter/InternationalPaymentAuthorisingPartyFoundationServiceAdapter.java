package com.capgemini.psd2.foundationservice.international.payment.authorisingparty.boi.adapter;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.capgemini.psd2.foundationservice.international.payment.authorisingparty.boi.adapter.client.InternationalPaymentAuthorisingPartyFoundationServiceClient;
import com.capgemini.psd2.foundationservice.international.payment.authorisingparty.boi.adapter.delegate.InternationalPaymentAuthorisingPartyFoundationServiceDelegate;
import com.capgemini.psd2.foundationservice.international.payment.authorisingparty.boi.adapter.domain.PaymentInstructionProposalAuthorisingParty;
import com.capgemini.psd2.foundationservice.international.payment.authorisingparty.boi.adapter.domain.PaymentInstructionProposalInternational;
import com.capgemini.psd2.foundationservice.international.payment.authorisingparty.boi.adapter.transformer.InternationalPaymentAuthorisingPartyFoundationServiceTransformer;
import com.capgemini.psd2.pisp.domain.CustomIPaymentConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.OBWriteInternationalConsentResponse1;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.RestClientSync;

@Component
public class InternationalPaymentAuthorisingPartyFoundationServiceAdapter {

	@Autowired
	private InternationalPaymentAuthorisingPartyFoundationServiceDelegate intPayConsDelegate;
	

	@Autowired
	private InternationalPaymentAuthorisingPartyFoundationServiceClient intPayConsClient;
	
	@Autowired
	private InternationalPaymentAuthorisingPartyFoundationServiceTransformer intPayConsTransformer;
	
	public CustomIPaymentConsentsPOSTResponse createStagingInternationalPaymentConsents(
			OBWriteInternationalConsentResponse1 oBWriteInternationalConsentResponse1,
			CustomPaymentStageIdentifiers customStageIdentifiers, Map<String, String> params) {
		RequestInfo requestInfo = new RequestInfo();
		HttpHeaders httpHeaders = intPayConsDelegate.createPaymentRequestHeadersPost(customStageIdentifiers,params);
		String internationalPaymentAuthoriseURL = intPayConsDelegate.internationalPaymentAuthoriseConsentPostBaseURL(customStageIdentifiers.getPaymentConsentId());
		requestInfo.setUrl(internationalPaymentAuthoriseURL);
		PaymentInstructionProposalAuthorisingParty paymentInstructionProposalAuthorisingPartyRequest = intPayConsTransformer.transformInternationalPaymentRequest(oBWriteInternationalConsentResponse1);
		PaymentInstructionProposalInternational paymentInstructionProposalResponse = intPayConsClient.restTransportForInternationalPaymentFoundationServicePost(requestInfo, paymentInstructionProposalAuthorisingPartyRequest, PaymentInstructionProposalInternational.class, httpHeaders);
		return intPayConsTransformer.transformInternationalPaymentResponse(paymentInstructionProposalResponse);
	}
	

}
