package com.capgemini.psd2.foundationservice.international.payment.authorisingparty.boi.adapter.test;


import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.foundationservice.international.payment.authorisingparty.boi.adapter.InternationalPaymentAuthorisingPartyFoundationServiceAdapter;
import com.capgemini.psd2.foundationservice.international.payment.authorisingparty.boi.adapter.client.InternationalPaymentAuthorisingPartyFoundationServiceClient;
import com.capgemini.psd2.foundationservice.international.payment.authorisingparty.boi.adapter.delegate.InternationalPaymentAuthorisingPartyFoundationServiceDelegate;
import com.capgemini.psd2.foundationservice.international.payment.authorisingparty.boi.adapter.transformer.InternationalPaymentAuthorisingPartyFoundationServiceTransformer;
import com.capgemini.psd2.pisp.domain.CustomIPaymentConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.OBExternalConsentStatus1Code;
import com.capgemini.psd2.pisp.domain.OBWriteInternationalConsentResponse1;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;

@RunWith(SpringJUnit4ClassRunner.class)
public class InternationalPaymentAuthorisingPartyFoundationServiceAdapterTest {
	
	@InjectMocks
	InternationalPaymentAuthorisingPartyFoundationServiceAdapter adapter;
	@Mock
	InternationalPaymentAuthorisingPartyFoundationServiceDelegate delegate;
	@Mock
	InternationalPaymentAuthorisingPartyFoundationServiceClient client;
	@Mock
	InternationalPaymentAuthorisingPartyFoundationServiceTransformer transformer;
	
	OBWriteInternationalConsentResponse1 paymentConsentsRequest= new OBWriteInternationalConsentResponse1();
	OBExternalConsentStatus1Code successStatus=OBExternalConsentStatus1Code.AUTHORISED;
	OBExternalConsentStatus1Code failureStatus=OBExternalConsentStatus1Code.REJECTED;
	CustomPaymentStageIdentifiers customStageIdentifiers= new CustomPaymentStageIdentifiers();
	Map<String, String> params= new HashMap<>();
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * Context loads.
	 */
	@Test
	public void contextLoads() {
	}
	
	
	@Test
	public void testcreateStagingInternationalPaymentConsents(){
		adapter.createStagingInternationalPaymentConsents(paymentConsentsRequest, customStageIdentifiers, params);
	}

	
}

