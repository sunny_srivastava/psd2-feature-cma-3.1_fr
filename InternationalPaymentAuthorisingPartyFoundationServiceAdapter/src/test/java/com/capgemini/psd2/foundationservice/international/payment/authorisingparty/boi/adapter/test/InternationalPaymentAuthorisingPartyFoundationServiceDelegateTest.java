package com.capgemini.psd2.foundationservice.international.payment.authorisingparty.boi.adapter.test;


import static org.junit.Assert.assertNotNull;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpHeaders;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.foundationservice.international.payment.authorisingparty.boi.adapter.delegate.InternationalPaymentAuthorisingPartyFoundationServiceDelegate;
import com.capgemini.psd2.foundationservice.international.payment.authorisingparty.boi.adapter.transformer.InternationalPaymentAuthorisingPartyFoundationServiceTransformer;
import com.capgemini.psd2.pisp.domain.CustomIPaymentConsentsPOSTRequest;
import com.capgemini.psd2.pisp.enums.PaymentTypeEnum;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;

public class InternationalPaymentAuthorisingPartyFoundationServiceDelegateTest {
	@InjectMocks
	InternationalPaymentAuthorisingPartyFoundationServiceDelegate delegate;
	
	
	@Mock
	InternationalPaymentAuthorisingPartyFoundationServiceTransformer transformer;
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * Context loads.
	 */
	@Test
	public void contextLoads() {
	}
	
	
	
	@Test
	public void testcreatePaymentRequestHeadersPost(){
		
		Map<String, String> params=new HashMap<>();
		CustomPaymentStageIdentifiers stageIden=new CustomPaymentStageIdentifiers();
		 ReflectionTestUtils.setField(delegate,"sourceUserHeader","sorceuser");
		 ReflectionTestUtils.setField(delegate,"sourceSystemHeader","source");
		 ReflectionTestUtils.setField(delegate,"transactionHeader","transactioReqHeader");
		 ReflectionTestUtils.setField(delegate,"apiChannelCodeHeader","apiChannelCodeHeader");
		 ReflectionTestUtils.setField(delegate,"systemApiVersionHeader","systemApiVersionHeader");
		 ReflectionTestUtils.setField(delegate,"apiChannelBrandHeader","apiChannelBrandHeader");
		 
		params.put("X-CORRELATION-ID","BOL");
		params.put("X-BOI-CHANNEL","BOIUK");
		params.put("tenant_id","BOIUK");
		params.put("X-BOI-USER","BOIUK");
		
		stageIden.setPaymentSetupVersion("3.0");
		stageIden.setPaymentConsentId("58923");
		stageIden.setPaymentTypeEnum(PaymentTypeEnum.INTERNATIONAL_PAY);
		stageIden.setPaymentSubmissionId("123445");
		delegate.createPaymentRequestHeadersPost(stageIden, params);
		//assertNotNull(header);
		
	
	}
	
	
	
	
	@Test
	public void testgetPaymentFoundationServiceURL(){
		String paymentInstuctionProposalId="415641612";
		delegate.internationalPaymentAuthoriseConsentPostBaseURL(paymentInstuctionProposalId);
	}
	
	
	
	
	
	
}

