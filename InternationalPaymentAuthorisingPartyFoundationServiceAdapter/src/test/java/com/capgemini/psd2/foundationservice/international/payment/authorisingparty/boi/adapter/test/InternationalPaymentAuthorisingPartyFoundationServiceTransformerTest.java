package com.capgemini.psd2.foundationservice.international.payment.authorisingparty.boi.adapter.test;


import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.adapter.fraudnet.domain.Address;
import com.capgemini.psd2.foundationservice.international.payment.authorisingparty.boi.adapter.domain.AccountInformation;
import com.capgemini.psd2.foundationservice.international.payment.authorisingparty.boi.adapter.domain.Agent;
import com.capgemini.psd2.foundationservice.international.payment.authorisingparty.boi.adapter.domain.Amount;
import com.capgemini.psd2.foundationservice.international.payment.authorisingparty.boi.adapter.domain.AuthorisationType;
import com.capgemini.psd2.foundationservice.international.payment.authorisingparty.boi.adapter.domain.ChargeBearer;
import com.capgemini.psd2.foundationservice.international.payment.authorisingparty.boi.adapter.domain.Country;
import com.capgemini.psd2.foundationservice.international.payment.authorisingparty.boi.adapter.domain.Currency;
import com.capgemini.psd2.foundationservice.international.payment.authorisingparty.boi.adapter.domain.ExchangeRateQuote;
import com.capgemini.psd2.foundationservice.international.payment.authorisingparty.boi.adapter.domain.FinancialEventAmount;
import com.capgemini.psd2.foundationservice.international.payment.authorisingparty.boi.adapter.domain.PartyBasicInformation;
import com.capgemini.psd2.foundationservice.international.payment.authorisingparty.boi.adapter.domain.PaymentInstructionCharge;
import com.capgemini.psd2.foundationservice.international.payment.authorisingparty.boi.adapter.domain.PaymentInstructionPostalAddress;
import com.capgemini.psd2.foundationservice.international.payment.authorisingparty.boi.adapter.domain.PaymentInstructionProposalInternational;
import com.capgemini.psd2.foundationservice.international.payment.authorisingparty.boi.adapter.domain.PaymentInstrumentRiskFactor;
import com.capgemini.psd2.foundationservice.international.payment.authorisingparty.boi.adapter.domain.PaymentType;
import com.capgemini.psd2.foundationservice.international.payment.authorisingparty.boi.adapter.domain.PriorityCode;
import com.capgemini.psd2.foundationservice.international.payment.authorisingparty.boi.adapter.domain.ProposalStatus;
import com.capgemini.psd2.foundationservice.international.payment.authorisingparty.boi.adapter.domain.ProposingPartyAccount;
import com.capgemini.psd2.foundationservice.international.payment.authorisingparty.boi.adapter.domain.Purpose;
import com.capgemini.psd2.foundationservice.international.payment.authorisingparty.boi.adapter.domain.RateQuoteType;
import com.capgemini.psd2.foundationservice.international.payment.authorisingparty.boi.adapter.transformer.InternationalPaymentAuthorisingPartyFoundationServiceTransformer;
import com.capgemini.psd2.pisp.domain.CustomIPaymentConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.OBAuthorisation1;
import com.capgemini.psd2.pisp.domain.OBBranchAndFinancialInstitutionIdentification3;
import com.capgemini.psd2.pisp.domain.OBCashAccountCreditor2;
import com.capgemini.psd2.pisp.domain.OBCashAccountDebtor3;
import com.capgemini.psd2.pisp.domain.OBChargeBearerType1Code;
import com.capgemini.psd2.pisp.domain.OBDomestic1InstructedAmount;
import com.capgemini.psd2.pisp.domain.OBExchangeRate1;
import com.capgemini.psd2.pisp.domain.OBExchangeRateType2Code;
import com.capgemini.psd2.pisp.domain.OBExternalAuthorisation1Code;
import com.capgemini.psd2.pisp.domain.OBExternalPaymentContext1Code;
import com.capgemini.psd2.pisp.domain.OBInternational1;
import com.capgemini.psd2.pisp.domain.OBPartyIdentification43;
import com.capgemini.psd2.pisp.domain.OBPostalAddress6;
import com.capgemini.psd2.pisp.domain.OBPriority2Code;
import com.capgemini.psd2.pisp.domain.OBRemittanceInformation1;
import com.capgemini.psd2.pisp.domain.OBRisk1;
import com.capgemini.psd2.pisp.domain.OBRisk1DeliveryAddress;
import com.capgemini.psd2.pisp.domain.OBWriteDataInternationalConsent1;
import com.capgemini.psd2.pisp.domain.OBWriteDataInternationalConsentResponse1;
import com.capgemini.psd2.pisp.domain.OBWriteInternationalConsentResponse1;
import com.capgemini.psd2.pisp.stage.domain.Charge;
import com.capgemini.psd2.validator.PSD2Validator;

@RunWith(SpringJUnit4ClassRunner.class)
public class InternationalPaymentAuthorisingPartyFoundationServiceTransformerTest {
	@InjectMocks
	InternationalPaymentAuthorisingPartyFoundationServiceTransformer transformer;
	
	@Mock
	private PSD2Validator psd2Validator;
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}
	 
	@Test
	public void contextLoads() {
	}
	
	@Test
	public void testtransformInternationalPaymentResponse11(){
	PaymentInstructionProposalInternational inputBalanceObj=new PaymentInstructionProposalInternational();
	Purpose p1= new Purpose();
	inputBalanceObj.setPaymentInstructionProposalId("1234");
	inputBalanceObj.setInstructionEndToEndReference("user");
	inputBalanceObj.setInstructionReference("boi");
	inputBalanceObj.setInstructionLocalInstrument("card");
	inputBalanceObj.setProposalCreationDatetime("03/14/2019");
	inputBalanceObj.setProposalStatus(ProposalStatus.REJECTED);
	inputBalanceObj.setProposalStatusUpdateDatetime("02/15/2019");
	List<PaymentInstructionCharge> charges1 = new ArrayList<>();
	PaymentInstructionCharge pa= new PaymentInstructionCharge();
	pa.setChargeBearer(ChargeBearer.BORNEBYDEBTOR);
	pa.setType("vgju");
	charges1.add(pa);
	inputBalanceObj.setCharges(charges1);
	Charge c = new Charge();
	c.setChargeBearer(OBChargeBearerType1Code.BORNEBYCREDITOR);
	c.setType("ABC");
	List<Charge> charges = new ArrayList<Charge>();
	charges.add(c);
	p1.setProprietaryPurpose("ABCD");
	inputBalanceObj.setPurpose(p1);
	Amount amt = new Amount();
	amt.setTransactionCurrency(12.00);
	pa.setAmount(amt);
	Currency cr = new Currency();
	cr.setIsoAlphaCode("a1s2");
	inputBalanceObj.setCurrencyOfTransfer(cr);
	pa.setCurrency(cr);
	FinancialEventAmount amount=new FinancialEventAmount();
	amount.setTransactionCurrency(23.0);
	inputBalanceObj.setFinancialEventAmount(amount);
	Currency currency= new Currency();
	currency.setIsoAlphaCode("iuhio");
	inputBalanceObj.setTransactionCurrency(currency);
	AccountInformation act=new AccountInformation();
	
	act.setAccountName("nam");
	act.setAccountIdentification("345");
	act.setSchemeName("debit");
	act.setSecondaryIdentification("678");
	inputBalanceObj.setAuthorisingPartyAccount(act);
	ProposingPartyAccount ppa=new ProposingPartyAccount();
	ppa.setAccountName("nam");
	ppa.setAccountNumber("987");
	ppa.setSchemeName("credit");
	ppa.setSecondaryIdentification("456");
	ppa.setAccountIdentification("c4d512");
	inputBalanceObj.setProposingPartyAccount(ppa);
	PartyBasicInformation partyBasicInformation = new PartyBasicInformation();
	partyBasicInformation.setPartyName("ABCD");
	inputBalanceObj.setProposingParty(partyBasicInformation);
	PaymentInstructionPostalAddress add=new PaymentInstructionPostalAddress();
	add.setTownName("pune");
	add.setDepartment("A");
	add.setCountrySubDivision("MH");
	add.setSubDepartment("ABC");
	add.setPostCodeNumber("600789");
	add.setGeoCodeBuildingName("capg");
	add.setGeoCodeBuildingNumber("1521");
	Country country=new Country();
	country.setIsoCountryAlphaTwoCode("IND");
	add.setAddressCountry(country);
	List<String> line= new ArrayList<>();
	line.add("Talwade");
	add.setAddressLine(line);
	add.setAddressType("POSTAL");
	inputBalanceObj.setProposingPartyPostalAddress(add);
	
	inputBalanceObj.setPaymentType(PaymentType.INTERNATIONAL);
	inputBalanceObj.setAuthorisationDatetime("12/12/2018");
	PaymentInstrumentRiskFactor ref = new PaymentInstrumentRiskFactor();
	ref .setMerchantCategoryCode("BUI");
	ref.setPaymentContextCode("BILLPAYMENT");
	ref.setMerchantCustomerIdentification("SBU");
	Address cpa=new Address();
	cpa.setFirstAddressLine("134");
	
	cpa.setThirdAddressLine("sector27");
	cpa.setFourthAddressLine("nigdi");
	
	cpa.setPostCodeNumber("600024");;
	Country country1=new Country();
	country1.setIsoCountryAlphaTwoCode("IND");
	inputBalanceObj.setPaymentInstructionRiskFactorReference(ref);
	
	
	ExchangeRateQuote exchangeRateQuote=new ExchangeRateQuote();
	exchangeRateQuote.setRateQuoteType(RateQuoteType.ACTUAL);
	exchangeRateQuote.setExchangeRate(2.0412);
	//exchangeRateQuote.setExpirationDateTime("12/06/2019");
	currency.setIsoAlphaCode("iuhio");
	exchangeRateQuote.setUnitCurrency(currency);
	
	inputBalanceObj.setExchangeRateQuote(exchangeRateQuote);
	inputBalanceObj.setPriorityCode(PriorityCode.NORMAL);
	//inputBalanceObj.setPurpose("dsfs");
	Agent agent=new Agent();
	agent.setAgentName("Msdzfc");
	agent.setPartyAddress(add);
	//agent.setSwiftBankIdentifierCodeBIC("5311241");
	inputBalanceObj.setProposingPartyAgent(agent);
	transformer.transformInternationalPaymentResponse(inputBalanceObj);
	inputBalanceObj.setProposalStatus(ProposalStatus.AWAITINGAUTHORISATION);
	cpa.setFifthAddressLine("pune");
	inputBalanceObj.setAuthorisingPartyReference("ABCDEF");
	
	inputBalanceObj.setAuthorisingPartyUnstructuredReference("ANY TYPE");
	inputBalanceObj.setAuthorisationType(AuthorisationType.ANY);
	cpa.setSecondAddressLine("pradhikaran");
	transformer.transformInternationalPaymentResponse(inputBalanceObj);
	
	}
	@Test
	public void testTransformDomesticConsentResponseFromAPIToFDForInsert(){
		
		
		CustomIPaymentConsentsPOSTRequest paymentConsentsRequest = new CustomIPaymentConsentsPOSTRequest();
		OBInternational1 oBInternational1 = new OBInternational1();
		OBDomestic1InstructedAmount amount=new OBDomestic1InstructedAmount();
		OBWriteDataInternationalConsentResponse1 oBWriteDataInternationalConsent1 = new OBWriteDataInternationalConsentResponse1();
		OBCashAccountDebtor3 debtorAccount = new OBCashAccountDebtor3();
		OBCashAccountCreditor2 creditorAccount = new OBCashAccountCreditor2();
		OBPostalAddress6 creditorPostalAddress = new OBPostalAddress6(); 
		List<String> addressLine = new ArrayList<String>();
		OBRemittanceInformation1 remittanceInformation = new OBRemittanceInformation1();
		OBAuthorisation1 authorisation = new OBAuthorisation1();
		OBRisk1 risk = new OBRisk1();
		OBRisk1DeliveryAddress deliveryAddress = new OBRisk1DeliveryAddress();
		OBBranchAndFinancialInstitutionIdentification3 creditoragent=new OBBranchAndFinancialInstitutionIdentification3();
		OBPartyIdentification43 creditor=new OBPartyIdentification43();
		Country addressCountry = new Country();
		OBExchangeRate1 oBExchangeRate1=new OBExchangeRate1();
		PaymentInstructionPostalAddress proposingPartyPostalAddress = new PaymentInstructionPostalAddress();
		ProposingPartyAccount proposingPartyAccount = new ProposingPartyAccount();

		OBCashAccountDebtor3 authorisingPartyAccount = new OBCashAccountDebtor3();
		Currency transactionCurrency = new Currency();
		FinancialEventAmount financialEventAmount = new FinancialEventAmount();
		
		financialEventAmount.setTransactionCurrency(22.01);
		transactionCurrency.setIsoAlphaCode("NZD");
		authorisingPartyAccount.setSchemeName("abcd");
		authorisingPartyAccount.setName("user");
		authorisingPartyAccount.setIdentification("absd");
		authorisingPartyAccount.setSecondaryIdentification("243546");
		proposingPartyAccount.setSchemeName("743897");
		proposingPartyAccount.setAccountNumber("349829");
		proposingPartyAccount.setAccountName("User");
		proposingPartyAccount.setSecondaryIdentification("12345");
		addressLine.add("FirstAddressLine");
		addressLine.add("SecondAddressLine");
		proposingPartyPostalAddress.setAddressType("permanent");
		proposingPartyPostalAddress.setSubDepartment("SubDepartment");
		proposingPartyPostalAddress.setDepartment("Department");
		proposingPartyPostalAddress.setGeoCodeBuildingName("GeoCodeBuildingName");
		proposingPartyPostalAddress.setGeoCodeBuildingNumber("GeoCodeBuildingNumber");
		proposingPartyPostalAddress.setTownName("TownName");
		proposingPartyPostalAddress.setCountrySubDivision("CountrySubDivision");
		proposingPartyPostalAddress.setAddressCountry(addressCountry);
		proposingPartyPostalAddress.setAddressLine(addressLine);
		
		addressCountry.setIsoCountryAlphaTwoCode("IsoCountryAlphaTwoCode");
		Address counterPartyAdress=new Address();
		PaymentInstrumentRiskFactor paymentInstructionRiskFactorReference=new PaymentInstrumentRiskFactor() ;
		//paymentInstructionRiskFactorReference.setCounterPartyAddress(counterPartyAdress);
		deliveryAddress.setAddressLine(addressLine);
		deliveryAddress.setStreetName("abc");
		deliveryAddress.setBuildingNumber("1233");
		deliveryAddress.setPostCode("3111455");
		deliveryAddress.setTownName("pune");
		deliveryAddress.setCountrySubDivision("ABCD");
		deliveryAddress.setCountry("India");
		
		risk.setPaymentContextCode(OBExternalPaymentContext1Code.BILLPAYMENT);
		risk.setMerchantCategoryCode("MerchantCategoryCode");
		risk.setMerchantCustomerIdentification("MerchantCustomerIdentification");
		risk.setDeliveryAddress(deliveryAddress);
		authorisation.setAuthorisationType(OBExternalAuthorisation1Code.ANY);
		authorisation.completionDateTime(""+new Date());
		remittanceInformation.setUnstructured("Unstructured");
		remittanceInformation.setReference("Reference");
		
		addressLine.add("abc");
		addressLine.add("xyz");
		creditorPostalAddress.setAddressType(com.capgemini.psd2.pisp.domain.OBAddressTypeCode.POSTAL);
		creditorPostalAddress.setDepartment("Department");
		creditorPostalAddress.setSubDepartment("SubDepartment");
		creditorPostalAddress.setStreetName("StreetName");
		creditorPostalAddress.setBuildingNumber("BuildingNumber");
		creditorPostalAddress.setPostCode("PostCode");
		creditorPostalAddress.setTownName("TownName");
		creditorPostalAddress.setCountrySubDivision("CountrySubDivision");
		creditorPostalAddress.setCountry("India");
		creditorPostalAddress.setAddressLine(addressLine);
		debtorAccount.setName("User");
		debtorAccount.setIdentification("Identification");
		debtorAccount.setName("User");
		debtorAccount.setSecondaryIdentification("SecondaryIdentification");
		creditorAccount.setSchemeName("SchemeName");
		creditorAccount.setIdentification("Identification");
		creditorAccount.setName("Name");
		creditorAccount.setSecondaryIdentification("SecondaryIdentification");
		
		amount.setAmount("29.01");
		amount.setCurrency("NZD");
		
		oBExchangeRate1.setContractIdentification("sfd");
		oBExchangeRate1.setExchangeRate(BigDecimal.valueOf(123.0));
		oBExchangeRate1.setRateType(OBExchangeRateType2Code.ACTUAL);
		oBExchangeRate1.setUnitCurrency("GBP");
		oBInternational1.setExchangeRateInformation(oBExchangeRate1);

	
		oBInternational1.setCreditorAccount(creditorAccount);
		creditoragent.setPostalAddress(creditorPostalAddress);
		oBInternational1.setCreditorAgent(creditoragent);
		creditor.setPostalAddress(creditorPostalAddress);
		oBInternational1.setCreditor(creditor);
		oBInternational1.setRemittanceInformation(remittanceInformation);
		oBInternational1.setChargeBearer(OBChargeBearerType1Code.BORNEBYCREDITOR);
		oBWriteDataInternationalConsent1.setInitiation(oBInternational1);
		oBWriteDataInternationalConsent1.setAuthorisation(authorisation);
		
		//paymentConsentsRequest.setData(oBWriteDataInternationalConsent1);
		paymentConsentsRequest.setRisk(risk);
		OBWriteInternationalConsentResponse1 oBWriteInternationalConsentResponse1 = new OBWriteInternationalConsentResponse1();
		oBWriteInternationalConsentResponse1.setData(oBWriteDataInternationalConsent1);
		Map<String, String> params = new HashMap<String,String>();
		params.put("tenant_id", "BOIROI");
		params.put("X-BOI-CHANNEL", "channel");
		transformer.transformInternationalPaymentRequest(oBWriteInternationalConsentResponse1);
	}
	
	
	
}
