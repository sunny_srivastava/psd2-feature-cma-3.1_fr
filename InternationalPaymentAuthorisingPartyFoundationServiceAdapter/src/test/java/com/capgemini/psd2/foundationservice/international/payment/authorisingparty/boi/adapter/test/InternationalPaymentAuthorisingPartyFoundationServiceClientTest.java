package com.capgemini.psd2.foundationservice.international.payment.authorisingparty.boi.adapter.test;

import java.lang.reflect.Method;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.RestTemplate;

import com.capgemini.psd2.foundationservice.international.payment.authorisingparty.boi.adapter.client.InternationalPaymentAuthorisingPartyFoundationServiceClient;
import com.capgemini.psd2.foundationservice.international.payment.authorisingparty.boi.adapter.domain.PaymentInstructionProposalAuthorisingParty;
import com.capgemini.psd2.foundationservice.international.payment.authorisingparty.boi.adapter.domain.PaymentInstructionProposalInternational;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.RestClientSync;
@RunWith(SpringJUnit4ClassRunner.class)
public class InternationalPaymentAuthorisingPartyFoundationServiceClientTest {
	@InjectMocks
	InternationalPaymentAuthorisingPartyFoundationServiceClient client;
	@Mock
	RestClientSync restClient;
	
	@Mock
	private RestTemplate restTemplate;
	
	RequestInfo reqInfo = new RequestInfo();
	Class<PaymentInstructionProposalInternational> responseType = null;
	HttpHeaders httpHeaders = new HttpHeaders();
	PaymentInstructionProposalInternational payment= new PaymentInstructionProposalInternational();
	PaymentInstructionProposalAuthorisingParty paymentInstructionProposalAuthorisingParty = new PaymentInstructionProposalAuthorisingParty();
	
	@Before
	public void setUp() throws Exception{
		MockitoAnnotations.initMocks(this);
		Method postConstruct= InternationalPaymentAuthorisingPartyFoundationServiceClient.class.getDeclaredMethod("init");
		postConstruct.setAccessible(true);
		postConstruct.invoke(client); 
	}
	
	@Test
	public void testrestTransportForInternationalPaymentFoundationServicePost()
	{
		payment=client.restTransportForInternationalPaymentFoundationServicePost(reqInfo, paymentInstructionProposalAuthorisingParty, responseType, httpHeaders);
	}
	
	
	
}
