package com.capgemini.psd2.security.exceptions;

import org.springframework.security.core.AuthenticationException;

import com.capgemini.psd2.exceptions.ErrorInfo;

public class PSD2AuthenticationException extends AuthenticationException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private ErrorInfo errorInfo = null; //NOSONAR
	
	
	public PSD2AuthenticationException(String message,ErrorInfo errorInfo){
		super(message);
		this.errorInfo = errorInfo;
	}
	
	public ErrorInfo getErrorInfo() {
		return errorInfo;
	}
	

	public static PSD2AuthenticationException populateAuthenticationFailedException(SCAConsentErrorCodeEnum errorCodeEnum,String detailedErrorMessage){
		ErrorInfo errorInfo = new ErrorInfo(errorCodeEnum.getErrorCode(), errorCodeEnum.getErrorMessage(), errorCodeEnum.getDetailErrorMessage(), errorCodeEnum.getStatusCode());
		
		return new PSD2AuthenticationException(detailedErrorMessage,errorInfo);
	}
	
	public static PSD2AuthenticationException populateAuthenticationFailedException(SCAConsentErrorCodeEnum errorCodeEnum){
		ErrorInfo errorInfo = new ErrorInfo(errorCodeEnum.getErrorCode(), errorCodeEnum.getErrorMessage(), errorCodeEnum.getDetailErrorMessage(), errorCodeEnum.getStatusCode());
		PSD2AuthenticationException authenticationException = new PSD2AuthenticationException(errorCodeEnum.getDetailErrorMessage(),errorInfo);
		authenticationException.errorInfo = errorInfo;
		return authenticationException;
	}	
	
	public static PSD2AuthenticationException populateAuthenticationFailedException(ErrorInfo errorInfo){
		return new PSD2AuthenticationException(errorInfo.getDetailErrorMessage(),errorInfo);
	}	
	
}