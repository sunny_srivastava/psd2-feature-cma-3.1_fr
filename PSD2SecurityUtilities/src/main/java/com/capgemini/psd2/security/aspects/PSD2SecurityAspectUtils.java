package com.capgemini.psd2.security.aspects;

import org.aspectj.lang.ProceedingJoinPoint;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.LoggerAttribute;
import com.capgemini.psd2.mask.DataMask;
import com.capgemini.psd2.security.exceptions.PSD2AuthenticationException;
import com.capgemini.psd2.security.logger.SecurityLoggerUtils;
import com.capgemini.psd2.utilities.JSONUtilities;

@Component
public class PSD2SecurityAspectUtils {

	@Autowired
	private SecurityLoggerUtils securityLoggerUtils;
	
	@Autowired
	private LoggerAttribute loggerAttribute;
	
	@Value("${app.payloadLog:#{false}}")
	private boolean payloadLog;

	/** The mask payload log. */
	@Value("${app.maskPayloadLog:#{false}}")
	private boolean maskPayloadLog;
	
	/** The mask payload. */
	@Value("${app.maskPayload:#{false}}")
	private boolean maskPayload;

	@Autowired
	private DataMask dataMask;
	
	private String exceptionLog = "{\"Exception\":\"{}.{}()\",\"{}\",\"ErrorDetails\":{}}";
	private String debugLog = "{\"Exception\":\"{}.{}()\",\"{}\",\"ErrorDetails\":\"{}\"}";
	
	
	private static final Logger LOGGER = LoggerFactory.getLogger(PSD2SecurityAspectUtils.class);
	
	public Object methodAdvice(ProceedingJoinPoint proceedingJoinPoint) {
				
		securityLoggerUtils.populateLoggerData(proceedingJoinPoint.getSignature().getName());
		LOGGER.info("{\"Enter\":\"{}.{}()\",\"{}\"}",
				proceedingJoinPoint.getSignature().getDeclaringTypeName(), proceedingJoinPoint.getSignature().getName(),
				loggerAttribute);
		try {
			Object result = proceedingJoinPoint.proceed();
			LOGGER.info("{\"Exit\":\"{}.{}()\",\"{}\"}",
					proceedingJoinPoint.getSignature().getDeclaringTypeName(),
					proceedingJoinPoint.getSignature().getName(), loggerAttribute);
			return result;
		} catch(PSD2Exception e){
			LOGGER.error(exceptionLog ,
					proceedingJoinPoint.getSignature().getDeclaringTypeName(),
					proceedingJoinPoint.getSignature().getName(), loggerAttribute, e.getErrorInfo());
			if(LOGGER.isDebugEnabled()){
				LOGGER.error(debugLog,proceedingJoinPoint.getSignature().getDeclaringTypeName(),
						proceedingJoinPoint.getSignature().getName(), loggerAttribute, e.getStackTrace());
			}
			throw e;
		} 
		catch(PSD2AuthenticationException e){
			LOGGER.error(exceptionLog ,
					proceedingJoinPoint.getSignature().getDeclaringTypeName(),
					proceedingJoinPoint.getSignature().getName(), loggerAttribute, e.getErrorInfo());
			if(LOGGER.isDebugEnabled()){
				LOGGER.error(debugLog,proceedingJoinPoint.getSignature().getDeclaringTypeName(),
						proceedingJoinPoint.getSignature().getName(), loggerAttribute, e.getStackTrace());
			}
			throw e;
		}
		catch (Throwable e) {
			LOGGER.info("Exception occurred in methodAdvice(): "+e);
			PSD2Exception psd2Exception = PSD2Exception.populatePSD2Exception(e.getMessage(), ErrorCodeEnum.TECHNICAL_ERROR);
			LOGGER.error(exceptionLog ,
					proceedingJoinPoint.getSignature().getDeclaringTypeName(),
					proceedingJoinPoint.getSignature().getName(), loggerAttribute, psd2Exception.getErrorInfo());
			if(LOGGER.isDebugEnabled()){
				LOGGER.error(debugLog,proceedingJoinPoint.getSignature().getDeclaringTypeName(),
						proceedingJoinPoint.getSignature().getName(), loggerAttribute, e.getStackTrace());
			}
			throw psd2Exception;
		}
	}
	
	
	public Object methodPayloadAdvice(ProceedingJoinPoint proceedingJoinPoint) {
		securityLoggerUtils.populateLoggerData(proceedingJoinPoint.getSignature().getName());
		if(maskPayloadLog && payloadLog){
			LOGGER.info("{\"Enter\":\"{}.{}()\",\"{}\",\"Arguments\":{}}",
					proceedingJoinPoint.getSignature().getDeclaringTypeName(), proceedingJoinPoint.getSignature().getName(),
					loggerAttribute, JSONUtilities.getJSONOutPutFromObject(dataMask.maskRequestLog(proceedingJoinPoint.getArgs(), proceedingJoinPoint.getSignature().getName())));

		}else if(!maskPayloadLog && payloadLog){
			LOGGER.info("{\"Enter\":\"{}.{}()\",\"{}\",\"Arguments\":{}}",
					proceedingJoinPoint.getSignature().getDeclaringTypeName(), proceedingJoinPoint.getSignature().getName(),
					loggerAttribute, JSONUtilities.getJSONOutPutFromObject(dataMask.maskMRequestLog(proceedingJoinPoint.getArgs(), proceedingJoinPoint.getSignature().getName())));

		}else{
			LOGGER.info("{\"Enter\":\"{}.{}()\",\"{}\"}",
					proceedingJoinPoint.getSignature().getDeclaringTypeName(), proceedingJoinPoint.getSignature().getName(),
					loggerAttribute);

		}
		try {
			
			Object result = proceedingJoinPoint.proceed();

			if(maskPayloadLog && payloadLog){
				LOGGER.info("{\"Exit\":\"{}.{}()\",\"{}\",\"Payload\":{}}",
						proceedingJoinPoint.getSignature().getDeclaringTypeName(),
						proceedingJoinPoint.getSignature().getName(), loggerAttribute,
						JSONUtilities.getJSONOutPutFromObject(dataMask.maskResponseLog(result, proceedingJoinPoint.getSignature().getName())));
			}else if(!maskPayloadLog && payloadLog){
				LOGGER.info("{\"Exit\":\"{}.{}()\",\"{}\",\"Payload\":{}}",
						proceedingJoinPoint.getSignature().getDeclaringTypeName(),
						proceedingJoinPoint.getSignature().getName(), loggerAttribute,
						JSONUtilities.getJSONOutPutFromObject(dataMask.maskMResponseLog(result, proceedingJoinPoint.getSignature().getName())));
						
			}else{
				LOGGER.info("{\"Exit\":\"{}.{}()\",\"{}\"}",
						proceedingJoinPoint.getSignature().getDeclaringTypeName(),
						proceedingJoinPoint.getSignature().getName(), loggerAttribute);
			}
			
			if(maskPayload)
				dataMask.maskResponse(result,  proceedingJoinPoint.getSignature().getName());
			else
				dataMask.maskMResponse(result,  proceedingJoinPoint.getSignature().getName());
			
			return result;
		}  catch(PSD2Exception e){ //NOSONAR
			LOGGER.error(exceptionLog ,
					proceedingJoinPoint.getSignature().getDeclaringTypeName(),
					proceedingJoinPoint.getSignature().getName(), loggerAttribute, e.getErrorInfo());
			if(LOGGER.isDebugEnabled()){
				LOGGER.error(debugLog,proceedingJoinPoint.getSignature().getDeclaringTypeName(),
						proceedingJoinPoint.getSignature().getName(), loggerAttribute, e.getStackTrace());
			}
			throw e;
		} catch(PSD2AuthenticationException e){ //NOSONAR
			LOGGER.error(exceptionLog ,
					proceedingJoinPoint.getSignature().getDeclaringTypeName(),
					proceedingJoinPoint.getSignature().getName(), loggerAttribute, e.getErrorInfo());
			if(LOGGER.isDebugEnabled()){
				LOGGER.error(debugLog,proceedingJoinPoint.getSignature().getDeclaringTypeName(),
						proceedingJoinPoint.getSignature().getName(), loggerAttribute, e.getStackTrace());
			}
			throw e;
		}
		catch (Throwable e) {
			LOGGER.info("Exception occurred in methodPayloadAdvice(): "+e);
			PSD2Exception psd2Exception = PSD2Exception.populatePSD2Exception(e.getMessage(), ErrorCodeEnum.TECHNICAL_ERROR);
			LOGGER.error(exceptionLog ,
					proceedingJoinPoint.getSignature().getDeclaringTypeName(),
					proceedingJoinPoint.getSignature().getName(), loggerAttribute, psd2Exception.getErrorInfo());
			if(LOGGER.isDebugEnabled()){
				LOGGER.error(debugLog,proceedingJoinPoint.getSignature().getDeclaringTypeName(),
						proceedingJoinPoint.getSignature().getName(), loggerAttribute, e.getStackTrace());
			}
			throw psd2Exception;
		}
	}	
}