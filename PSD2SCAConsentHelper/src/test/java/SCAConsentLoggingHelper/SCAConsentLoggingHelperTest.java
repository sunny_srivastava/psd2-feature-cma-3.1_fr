package SCAConsentLoggingHelper;

import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.aisp.domain.OBReadConsentResponse1;
import com.capgemini.psd2.aisp.domain.OBReadConsentResponse1Data;
import com.capgemini.psd2.cisp.adapter.FundsConfirmationConsentAdapter;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmationConsentResponse1;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmationConsentResponse1Data;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.adapter.PispConsentAdapter;
import com.capgemini.psd2.pisp.domain.PaymentConsentsPlatformResource;
import com.capgemini.psd2.pisp.payment.setup.platform.adapter.PaymentSetupPlatformAdapter;
import com.capgemini.psd2.scaconsenthelper.config.helpers.SCAConsentAdapterHelper;
import com.capgemini.psd2.scaconsenthelper.config.helpers.SCAConsentLoggingHelper;
import com.capgemini.psd2.scaconsenthelper.models.IntentTypeEnum;
import com.capgemini.psd2.scaconsenthelper.models.PickupDataModel;


@RunWith(SpringJUnit4ClassRunner.class)	
public class SCAConsentLoggingHelperTest {

	@InjectMocks
	SCAConsentLoggingHelper scaConsentLoggingHelper;
	
	@Mock
	private RequestHeaderAttributes reqHeaderAttributes;
 
	@Mock
	private PispConsentAdapter pispConsentAdapter;
 
	@Mock
	private SCAConsentAdapterHelper scaConsentAdapterHelper;
 
	@Mock
	private PaymentSetupPlatformAdapter paymentSetupPlatformAdapter;
	
	@Test
	public void updateRequestHeaderAttributesTest1() {
		PickupDataModel intentData = new PickupDataModel();
		intentData.setBypassConsentPage(true);
		intentData.setChannelId("WEB");
		intentData.setClientId("clientID");
		intentData.setConsentExpiryInMinutes(20l);
		intentData.setCorrelationId("12365dja-d912-sd12-23sh-12365djad912");
		intentData.setIntentId("12365dja-d912-sd12-23sh-12365djad912");
		intentData.setIntentTypeEnum(IntentTypeEnum.AISP_INTENT_TYPE);
		intentData.setOriginatorRefId("orig-reference-id");
		intentData.setPaymentType("domestic");
		intentData.setScope("AISP");
		intentData.setTenant_id("TenantID");
		intentData.setUserId("userid");
		
		OBReadConsentResponse1 accountSetupResponse = new OBReadConsentResponse1();
		OBReadConsentResponse1Data data = new OBReadConsentResponse1Data();
		data.setTppCID("1234");
		data.setTppLegalEntityName("tpp");
		accountSetupResponse.setData(data );
		
		when(scaConsentAdapterHelper
				.getAccountRequestSetupData("12365dja-d912-sd12-23sh-12365djad912")).thenReturn(accountSetupResponse);
		scaConsentLoggingHelper.updateRequestHeaderAttributes(intentData);
		
	}
	
	@Test
	public void updateRequestHeaderAttributesTest2() {
		PickupDataModel intentData = new PickupDataModel();
		intentData.setBypassConsentPage(true);
		intentData.setChannelId("WEB");
		intentData.setClientId("clientID");
		intentData.setConsentExpiryInMinutes(20l);
		intentData.setCorrelationId("12365dja-d912-sd12-23sh-12365djad912");
		intentData.setIntentId("12365dja-d912-sd12-23sh-12365djad912");
		intentData.setIntentTypeEnum(IntentTypeEnum.PISP_INTENT_TYPE);
		intentData.setOriginatorRefId("orig-reference-id");
		intentData.setPaymentType("domestic");
		intentData.setScope("AISP");
		intentData.setTenant_id("TenantID");
		intentData.setUserId("userid");
		
		PaymentConsentsPlatformResource paymentConsentsPlatformResource  = new PaymentConsentsPlatformResource();
		paymentConsentsPlatformResource.setTppCID("123");
		paymentConsentsPlatformResource.setTppLegalEntityName("test");
		
		when(paymentSetupPlatformAdapter.retrievePaymentSetupPlatformResource("12365dja-d912-sd12-23sh-12365djad912")).thenReturn(paymentConsentsPlatformResource);
		scaConsentLoggingHelper.updateRequestHeaderAttributes(intentData);
		
	}
	
	@Test
	public void updateRequestHeaderAttributesTest3() {
		PickupDataModel intentData = new PickupDataModel();
		intentData.setBypassConsentPage(true);
		intentData.setChannelId("WEB");
		intentData.setClientId("clientID");
		intentData.setConsentExpiryInMinutes(20l);
		intentData.setCorrelationId("12365dja-d912-sd12-23sh-12365djad912");
		intentData.setIntentId("12365dja-d912-sd12-23sh-12365djad912");
		intentData.setIntentTypeEnum(IntentTypeEnum.PISP_INTENT_TYPE);
		intentData.setOriginatorRefId("orig-reference-id");
		intentData.setPaymentType("domestic");
		intentData.setScope("AISP");
		intentData.setTenant_id("TenantID");
		intentData.setUserId("userid");
		
		PaymentConsentsPlatformResource paymentConsentsPlatformResource  = null;
		
		when(paymentSetupPlatformAdapter.retrievePaymentSetupPlatformResource("12365dja-d912-sd12-23sh-12365djad912")).thenReturn(paymentConsentsPlatformResource);
		scaConsentLoggingHelper.updateRequestHeaderAttributes(intentData);
		
	}
	
	@Test
	public void updateRequestHeaderAttributesTest4() {
		PickupDataModel intentData = new PickupDataModel();
		intentData.setBypassConsentPage(true);
		intentData.setChannelId("WEB");
		intentData.setClientId("clientID");
		intentData.setConsentExpiryInMinutes(20l);
		intentData.setCorrelationId("12365dja-d912-sd12-23sh-12365djad912");
		intentData.setIntentId("12365dja-d912-sd12-23sh-12365djad912");
		intentData.setIntentTypeEnum(IntentTypeEnum.CISP_INTENT_TYPE);
		intentData.setOriginatorRefId("orig-reference-id");
		intentData.setPaymentType("domestic");
		intentData.setScope("AISP");
		intentData.setTenant_id("TenantID");
		intentData.setUserId("userid");
		
		OBFundsConfirmationConsentResponse1 fundsConfirmationSetup  = new OBFundsConfirmationConsentResponse1();
		OBFundsConfirmationConsentResponse1Data data = new OBFundsConfirmationConsentResponse1Data();
		data.setTppCID("123");
		data.setTppLegalEntityName("test");
		fundsConfirmationSetup.setData(data );
		
		when(scaConsentAdapterHelper.getFundsConfirmationSetupData("12365dja-d912-sd12-23sh-12365djad912")).thenReturn(fundsConfirmationSetup);
		scaConsentLoggingHelper.updateRequestHeaderAttributes(intentData);
		
	}
}
