package com.capgemini.psd2.scaconsenthelper.helpers.test;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.naming.NamingException;
import javax.naming.directory.BasicAttributes;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.aisp.adapter.CustomerAccountListAdapter;
import com.capgemini.psd2.aisp.domain.OBAccount6;
import com.capgemini.psd2.aisp.domain.OBAccount6Account;
import com.capgemini.psd2.aisp.domain.OBExternalAccountType1Code;
import com.capgemini.psd2.consent.domain.PSD2Account;
import com.capgemini.psd2.consent.domain.PispConsent;
import com.capgemini.psd2.enums.ConsentStatusEnum;
import com.capgemini.psd2.fraudsystem.constants.FraudSystemConstants;
import com.capgemini.psd2.fraudsystem.helper.FraudSystemHelper;
import com.capgemini.psd2.logger.LoggerUtils;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.pisp.adapter.PispConsentAdapter;
import com.capgemini.psd2.pisp.domain.OBCashAccountDebtor3;
import com.capgemini.psd2.pisp.domain.PaymentConsentsValidationResponse;
import com.capgemini.psd2.pisp.enums.PaymentTypeEnum;
import com.capgemini.psd2.pisp.payment.setup.platform.adapter.PaymentSetupPlatformAdapter;
import com.capgemini.psd2.pisp.sca.consent.operations.adapter.PispScaConsentOperationsAdapter;
import com.capgemini.psd2.pisp.stage.domain.CustomConsentAppViewData;
import com.capgemini.psd2.pisp.stage.domain.CustomFraudSystemPaymentData;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.pisp.stage.domain.CustomPreAuthorizeAdditionalInfo;
import com.capgemini.psd2.scaconsenthelper.config.helpers.PispConsentCreationDataHelperImpl;
import com.capgemini.psd2.scaconsenthelper.models.PickupDataModel;

public class PispConsentCreationDataHelperImplTest {
	
	@Mock
	private CustomerAccountListAdapter customerAccountListAdapter;
	
	@Mock
	private LoggerUtils loggerUtils;

	@Mock
	private PispConsentAdapter pispConsentAdapter;

	@Mock
	private FraudSystemHelper fraudSystemHelper;

	@Mock
	private PaymentSetupPlatformAdapter paymentSetupPlatformAdapter;

	@Mock
	private PispScaConsentOperationsAdapter pispStageOperationsAdapter;

	@InjectMocks
	private PispConsentCreationDataHelperImpl pispConsentCreationDataHelperImpl;
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void retrieveCustomerAccountListInfoTest() throws NamingException{
		
		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();
		stageIdentifiers.setPaymentConsentId("11737fe5d8284fe89f400d608db8dc25");
		stageIdentifiers.setPaymentSubmissionId("11737fe5d8284fe89f400d608db8dc52");
		stageIdentifiers.setPaymentSetupVersion("3.1");
		
		Mockito.when(paymentSetupPlatformAdapter.populateStageIdentifiers(anyString())).thenReturn(stageIdentifiers);
		pispConsentCreationDataHelperImpl.retrieveCustomerAccountListInfo("userId", "clientId", "flowType", "correlationId", "channelId", "schemeName", "tenantId", "intentId","3.1");
		
	}
	
	@Test
	@Ignore
	public void createConsentTest() throws NamingException{
		PSD2Account account1 = new PSD2Account();
		
		List<OBAccount6Account> accountList = new ArrayList<>();
		accountList.add(new OBAccount6Account().schemeName("IBAN"));
		
		OBAccount6Account acc1 = new OBAccount6Account();
		acc1.setSchemeName("IBAN");
		acc1.setIdentification("FR1420041010050500013M02606");
		account1.setAccountId("8d6d274a-d095-4ae1-adb4-c020d7cfe027");
		account1.setAccountType(OBExternalAccountType1Code.BUSINESS);
		
		account1.setAccount(accountList);
		Map<String,String> additionalInformation = new HashMap<>();
		additionalInformation.put("Uk.OBIE.IBAN", "FR1420041010050500013M02606");
		additionalInformation.put("ACCOUNT-NSC", "901542");
		additionalInformation.put("ACCOUNT-NUMBER","236828");
		additionalInformation.put("partyIdentifier", "1234");
		account1.setAdditionalInformation(additionalInformation);
		
		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();
		stageIdentifiers.setPaymentConsentId("11737fe5d8284fe89f400d608db8dc25");
		stageIdentifiers.setPaymentSubmissionId("11737fe5d8284fe89f400d608db8dc52");
		stageIdentifiers.setPaymentSetupVersion("3.1");

		
		Mockito.when(paymentSetupPlatformAdapter.populateStageIdentifiers("11737fe5d8284fe89f400d608db8dc25")).thenReturn(stageIdentifiers);
		
		BasicAttributes tppInformationObj = new BasicAttributes();
		
		CustomFraudSystemPaymentData customFraudSystemPaymentData = new CustomFraudSystemPaymentData();
		customFraudSystemPaymentData.setPaymentType("Domestic");
		
		Mockito.when(pispStageOperationsAdapter.retrieveFraudSystemPaymentStagedData(stageIdentifiers, null)).thenReturn(customFraudSystemPaymentData);
		tppInformationObj.put("1234", "ldapValue");
		
		Map<String, String> paramsMap = new HashMap<>();
		paramsMap.put(PSD2Constants.CHANNEL_NAME, "123");
		paramsMap.put(PSD2Constants.TENANT_ID, "tenant1");
		paramsMap.put(FraudSystemConstants.PAYMENT_ID, stageIdentifiers.getPaymentConsentId());

		Object fraudResponse = new Object();
		
		Mockito.when(fraudSystemHelper.captureFraudEvent("1", account1, customFraudSystemPaymentData,
				paramsMap)).thenReturn(fraudResponse);
		
		Mockito.doNothing().when(pispConsentAdapter).createConsent(any());
		
		Mockito.doNothing().when(pispStageOperationsAdapter).updatePaymentStageData(any(), any(), any());
		ReflectionTestUtils.setField(pispConsentCreationDataHelperImpl, "consentExpiryTime", 360);
		pispConsentCreationDataHelperImpl.createConsent(account1,"88888888","W4H36Sejd6Ei4VyywKJD6p",stageIdentifiers,"1234","headers","paytm",tppInformationObj,"tenant1");

	}
	
	@Test
	@Ignore
	public void createConsentTest1() throws NamingException{
		PSD2Account account1 = new PSD2Account();
		
		List<OBAccount6Account> accountList = new ArrayList<>();
		accountList.add(new OBAccount6Account().schemeName("IBAN"));
		
		OBAccount6Account acc1 = new OBAccount6Account();
		acc1.setSchemeName("IBAN");
		acc1.setIdentification("FR1420041010050500013M02606");
		account1.setAccountId("8d6d274a-d095-4ae1-adb4-c020d7cfe027");
		account1.setAccountType(OBExternalAccountType1Code.BUSINESS);
		
		account1.setAccount(accountList);
		Map<String,String> additionalInformation = new HashMap<>();
		additionalInformation.put("Uk.OBIE.IBAN", "FR1420041010050500013M02606");
		additionalInformation.put("ACCOUNT-NSC", "901542");
		additionalInformation.put("ACCOUNT-NUMBER","236828");
		additionalInformation.put("partyIdentifier", "1234");
		account1.setAdditionalInformation(additionalInformation);
		
		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();
		stageIdentifiers.setPaymentConsentId("11737fe5d8284fe89f400d608db8dc25");
		stageIdentifiers.setPaymentSubmissionId("11737fe5d8284fe89f400d608db8dc52");
		stageIdentifiers.setPaymentSetupVersion("3.1");
		stageIdentifiers.setPaymentTypeEnum(PaymentTypeEnum.DOMESTIC_ST_ORD);

		
		Mockito.when(paymentSetupPlatformAdapter.populateStageIdentifiers("11737fe5d8284fe89f400d608db8dc25")).thenReturn(stageIdentifiers);
		
		BasicAttributes tppInformationObj = new BasicAttributes();
		
		CustomFraudSystemPaymentData customFraudSystemPaymentData = new CustomFraudSystemPaymentData();
		customFraudSystemPaymentData.setPaymentType("Domestic");
		
		Mockito.when(pispStageOperationsAdapter.retrieveFraudSystemPaymentStagedData(stageIdentifiers, null)).thenReturn(customFraudSystemPaymentData);
		tppInformationObj.put("1234", "ldapValue");
		
		Map<String, String> paramsMap = new HashMap<>();
		paramsMap.put(PSD2Constants.CHANNEL_NAME, "123");
		paramsMap.put(PSD2Constants.TENANT_ID, "tenant1");
		paramsMap.put(FraudSystemConstants.PAYMENT_ID, stageIdentifiers.getPaymentConsentId());

		Object fraudResponse = new Object();
		
		Mockito.when(fraudSystemHelper.captureFraudEvent("1", account1, customFraudSystemPaymentData,
				paramsMap)).thenReturn(fraudResponse);
		
		Mockito.doNothing().when(pispConsentAdapter).createConsent(any());
		
		Mockito.doNothing().when(pispStageOperationsAdapter).updatePaymentStageData(any(), any(), any());
		ReflectionTestUtils.setField(pispConsentCreationDataHelperImpl, "consentExpiryTime", 360);
		pispConsentCreationDataHelperImpl.createConsent(account1,"88888888","W4H36Sejd6Ei4VyywKJD6p",stageIdentifiers,"1234","headers","paytm",tppInformationObj,"tenant1");

	}
	
	@Test
	public void populateSelectedStandingOrderDebtorDetails() {
		OBAccount6 account = new PSD2Account();
		account.setAccount(new ArrayList<OBAccount6Account>());
		account.getAccount().add(new OBAccount6Account().schemeName("UK_OBIE_BBAN").identification("FR1420041010050500013M02606").name("name"));
		OBCashAccountDebtor3 result = pispConsentCreationDataHelperImpl.populateSelectedStandingOrderDebtorDetails(account);
		
		assertEquals(account.getAccount().get(0).getIdentification(), result.getIdentification());
		assertEquals("UK.OBIE.BBAN", result.getSchemeName());
		
	}
	
	@Test
	public void cancelPaymentSetup() {
		
		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();
		stageIdentifiers.setPaymentConsentId("11737fe5d8284fe89f400d608db8dc25");
		stageIdentifiers.setPaymentSubmissionId("11737fe5d8284fe89f400d608db8dc52");
		stageIdentifiers.setPaymentSetupVersion("3.1");
		stageIdentifiers.setPaymentTypeEnum(PaymentTypeEnum.DOMESTIC_PAY);
		
		Mockito.when(paymentSetupPlatformAdapter.populateStageIdentifiers(anyString())).thenReturn(stageIdentifiers);
		Mockito.when(pispConsentAdapter.retrieveConsentByPaymentId("11737fe5d8284fe89f400d608db8dc25",
				ConsentStatusEnum.AWAITINGAUTHORISATION)).thenReturn(new PispConsent());
		pispConsentCreationDataHelperImpl.cancelPaymentSetup("11737fe5d8284fe89f400d608db8dc25", new HashMap<String, String>());
	}
	
	@Test
	public void cancelPaymentSetupTest1() {
		
		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();
		stageIdentifiers.setPaymentConsentId("11737fe5d8284fe89f400d608db8dc25");
		stageIdentifiers.setPaymentSubmissionId("11737fe5d8284fe89f400d608db8dc52");
		stageIdentifiers.setPaymentSetupVersion("3.1");
		stageIdentifiers.setPaymentTypeEnum(PaymentTypeEnum.DOMESTIC_PAY);
		
		PispConsent consent = new PispConsent();
		consent.setStatus(ConsentStatusEnum.AWAITINGAUTHORISATION);
		
		Mockito.when(paymentSetupPlatformAdapter.populateStageIdentifiers(anyString())).thenReturn(stageIdentifiers);
		Mockito.when(pispConsentAdapter.retrieveConsentByPaymentId("11737fe5d8284fe89f400d608db8dc25",
				ConsentStatusEnum.AWAITINGAUTHORISATION)).thenReturn(consent);
		pispConsentCreationDataHelperImpl.cancelPaymentSetup("11737fe5d8284fe89f400d608db8dc25", new HashMap<String, String>());
	}
	
	@Test
	public void cancelPaymentSetupTest2() {
		
		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();
		stageIdentifiers.setPaymentConsentId("11737fe5d8284fe89f400d608db8dc25");
		stageIdentifiers.setPaymentSubmissionId("11737fe5d8284fe89f400d608db8dc52");
		stageIdentifiers.setPaymentSetupVersion("3.1");
		stageIdentifiers.setPaymentTypeEnum(PaymentTypeEnum.DOMESTIC_PAY);
		
		PispConsent consent = new PispConsent();
		consent.setStatus(ConsentStatusEnum.AWAITINGAUTHORISATION);
		
		Mockito.when(paymentSetupPlatformAdapter.populateStageIdentifiers(anyString())).thenReturn(stageIdentifiers);
		Mockito.when(pispConsentAdapter.retrieveConsentByPaymentId("11737fe5d8284fe89f400d608db8dc25",
				ConsentStatusEnum.AWAITINGAUTHORISATION)).thenReturn(null);
		pispConsentCreationDataHelperImpl.cancelPaymentSetup("11737fe5d8284fe89f400d608db8dc25", new HashMap<String, String>());
	}
	
	@Test
	public void cancelPaymentSetupTest3() {
		
		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();
		stageIdentifiers.setPaymentConsentId("11737fe5d8284fe89f400d608db8dc25");
		stageIdentifiers.setPaymentSubmissionId("11737fe5d8284fe89f400d608db8dc52");
		stageIdentifiers.setPaymentSetupVersion("3.1");
		stageIdentifiers.setPaymentTypeEnum(PaymentTypeEnum.DOMESTIC_PAY);
		
		PispConsent consent = new PispConsent();
		consent.setStatus(ConsentStatusEnum.AUTHORISED);
		
		Mockito.when(paymentSetupPlatformAdapter.populateStageIdentifiers(anyString())).thenReturn(stageIdentifiers);
		Mockito.when(pispConsentAdapter.retrieveConsentByPaymentId("11737fe5d8284fe89f400d608db8dc25",
				ConsentStatusEnum.AUTHORISED)).thenReturn(null);
		pispConsentCreationDataHelperImpl.cancelPaymentSetup("11737fe5d8284fe89f400d608db8dc25", new HashMap<String, String>());
	}
	
	@Test
	public void retrieveConsentAppStagedViewDataTest() {
		CustomPaymentStageIdentifiers stagedIdentifiers = new CustomPaymentStageIdentifiers();
		CustomConsentAppViewData obj = new CustomConsentAppViewData();
		String paymentConsentId = "1231312";
		when(pispStageOperationsAdapter.retrieveConsentAppStagedViewData(stagedIdentifiers, null)).thenReturn(obj);
		assertEquals(obj, pispConsentCreationDataHelperImpl.retrieveConsentAppStagedViewData(paymentConsentId,stagedIdentifiers));
	}
	
	@Test
	public void getConsentAppViewData() {
		PickupDataModel pickupDataModel = new PickupDataModel();
		CustomPaymentStageIdentifiers stagedIdentifiers = new CustomPaymentStageIdentifiers();
		pickupDataModel.setIntentId("123");
		CustomConsentAppViewData obj = new CustomConsentAppViewData();
		String paymentConsentId = "1231312";
		when(paymentSetupPlatformAdapter.populateStageIdentifiers("123")).thenReturn(stagedIdentifiers);
		when(pispStageOperationsAdapter.retrieveConsentAppStagedViewData(stagedIdentifiers, null)).thenReturn(obj);
		assertEquals(obj, pispConsentCreationDataHelperImpl.getConsentAppViewData(pickupDataModel));
	}
	
	@Test
	public void validatePreAuthorisation() {
		OBCashAccountDebtor3 selectedDebtorDetails = new OBCashAccountDebtor3();
		CustomPreAuthorizeAdditionalInfo preAuthAdditionalInfo = new CustomPreAuthorizeAdditionalInfo();
		PaymentConsentsValidationResponse resp = new PaymentConsentsValidationResponse();
		CustomPaymentStageIdentifiers stagedIdentifiers = new CustomPaymentStageIdentifiers();
		when(pispStageOperationsAdapter.validatePreAuthorisation(selectedDebtorDetails, preAuthAdditionalInfo,
				stagedIdentifiers, null)).thenReturn(resp);
		assertEquals(resp, pispConsentCreationDataHelperImpl.validatePreAuthorisation(
				 selectedDebtorDetails,preAuthAdditionalInfo, "123", null, stagedIdentifiers));
	}
	
	@Test
	public void validatePreAuthorisationforStandingOrder() {
		OBCashAccountDebtor3 selectedDebtorDetails = new OBCashAccountDebtor3();
		CustomPreAuthorizeAdditionalInfo preAuthAdditionalInfo = new CustomPreAuthorizeAdditionalInfo();
		PaymentConsentsValidationResponse resp = new PaymentConsentsValidationResponse();
		CustomPaymentStageIdentifiers stagedIdentifiers = new CustomPaymentStageIdentifiers();
		when(pispStageOperationsAdapter.validatePreAuthorisation(selectedDebtorDetails, preAuthAdditionalInfo,
				stagedIdentifiers, null)).thenReturn(resp);
		assertEquals(resp, pispStageOperationsAdapter.validatePreAuthorisation(
				 selectedDebtorDetails,preAuthAdditionalInfo, stagedIdentifiers, null));
	}
	
	@Test
	public void retrieveConsentAppStagedViewData() {
		
		pispConsentCreationDataHelperImpl.retrieveConsentAppStagedViewData("paymentConsentId", false);
	}

}
