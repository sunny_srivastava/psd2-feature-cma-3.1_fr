package com.capgemini.psd2.scaconsenthelper.test.services;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.rest.client.sync.RestClientSync;
import com.capgemini.psd2.scaconsenthelper.config.PFConfig;
import com.capgemini.psd2.scaconsenthelper.models.PFInstanceData;
import com.capgemini.psd2.scaconsenthelper.models.PickupDataModel;
import com.capgemini.psd2.scaconsenthelper.services.SCAConsentHelperService;

@RunWith(SpringJUnit4ClassRunner.class)
public class SCAConsentHelperServiceTest {

	@Mock
	private PFConfig pfConfig;

	@Mock
	private RestClientSync restClientSyncImpl;

	@Mock
	private RequestHeaderAttributes headerAttributes;

	@Mock
	private HttpServletRequest request;

	@InjectMocks
	private SCAConsentHelperService service;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testCancelJourneyNullJsonResponse() {
		PFInstanceData pfInstanceData = new PFInstanceData();
		pfInstanceData.setPfInstanceId("client1");
		pfInstanceData.setPfInstanceUserName("admin");
		pfInstanceData.setPfInstanceUserPwd("password@1");

		Mockito.when(headerAttributes.getTenantId()).thenReturn("tenant1");

		String redirectURI = "https://google.com";
		service.cancelJourney(redirectURI, pfInstanceData);
	}
	
	@Test
	public void testCancelJourneyNullTenantId() {
		PFInstanceData pfInstanceData = new PFInstanceData();
		pfInstanceData.setPfInstanceId("client1");
		pfInstanceData.setPfInstanceUserName("admin");
		pfInstanceData.setPfInstanceUserPwd("password@1");

		Mockito.when(headerAttributes.getTenantId()).thenReturn(null);

		String redirectURI = "https://google.com";
		service.cancelJourney(redirectURI, pfInstanceData);
	}
	
	@Test
	public void testCancelJourneWithJsonResponse() {
		PFInstanceData pfInstanceData = new PFInstanceData();
		pfInstanceData.setPfInstanceId("client1");
		pfInstanceData.setPfInstanceUserName("admin");
		pfInstanceData.setPfInstanceUserPwd("password@1");

		Mockito.when(headerAttributes.getTenantId()).thenReturn("tenant1");
		Mockito.when(pfConfig.getTenantSpecificDropOffUrl(anyString())).thenReturn("tenant1");

		String redirectURI = "https://google.com";

		when(restClientSyncImpl.callForPost(any(), any(), Mockito.eq(String.class), any()))
				.thenReturn("{\"REF\":\"22D770430210FBA5C34F7A14BC63E4827DB6BC16352A11FD6BDE23171F94\"}");

		service.cancelJourney(redirectURI, pfInstanceData);
	}
	
	@Test
	public void testCancelJourneWithJsonResponse1() {
		PFInstanceData pfInstanceData = new PFInstanceData();
		pfInstanceData.setPfInstanceId("client1");
		pfInstanceData.setPfInstanceUserName("admin");
		pfInstanceData.setPfInstanceUserPwd("password@1");

		Mockito.when(headerAttributes.getTenantId()).thenReturn("tenant1");
		Mockito.when(pfConfig.getTenantSpecificDropOffUrl(anyString())).thenReturn("tenant1");

		String redirectURI = "https://google.com";

		when(restClientSyncImpl.callForPost(any(), any(), Mockito.eq(String.class), any()))
				.thenReturn("Authentication Required");

		service.cancelJourney(redirectURI, pfInstanceData);
	}

	@Test
	public void testRevokeAllPreviousGrants() {
		Mockito.when(headerAttributes.getTenantId()).thenReturn("tenant1");
		Mockito.when(pfConfig.getTenantSpecificGrantsRevocationUrl(anyString())).thenReturn("tenant1");
		Mockito.when(restClientSyncImpl.callForDelete(any(), Mockito.eq(String.class), any()))
				.thenReturn("DeletedSuccessfully");

		service.revokeAllPreviousGrants("intentId", "instanceUserId", "instancePwd");
	}
	
	@Test
	public void testRevokeAllPreviousGrantsNull() {
		Mockito.when(headerAttributes.getTenantId()).thenReturn(null);
		Mockito.when(pfConfig.getTenantSpecificGrantsRevocationUrl(anyString())).thenReturn("tenant1");
		Mockito.when(restClientSyncImpl.callForDelete(any(), Mockito.eq(String.class), any()))
				.thenReturn("DeletedSuccessfully");

		service.revokeAllPreviousGrants("intentId", "instanceUserId", "instancePwd");
	}


	@Test
	public void testDropOffOnConsentSubmissionWithNullJsonResponse() {
		Mockito.when(headerAttributes.getTenantId()).thenReturn("tenant1");

		PickupDataModel pickupDataModel = new PickupDataModel();
		pickupDataModel.setPaymentType("Domestic_Payment");
		pickupDataModel.setIntentId("af0ifjsldkj");
		service.dropOffOnConsentSubmission(pickupDataModel, "intentId", "instanceUserId", "instancePwd");
	}
	
	@Test
	public void testDropOffOnConsentSubmissionWithNullTenantId() {
		Mockito.when(headerAttributes.getTenantId()).thenReturn(null);

		PickupDataModel pickupDataModel = new PickupDataModel();
		pickupDataModel.setPaymentType("Domestic_Payment");
		pickupDataModel.setIntentId("af0ifjsldkj");
		service.dropOffOnConsentSubmission(pickupDataModel, "intentId", "instanceUserId", "instancePwd");
	}

	@Test
	public void testDropOffOnConsentSubmissionWithJsonResponse() {
		Mockito.when(headerAttributes.getTenantId()).thenReturn("tenant1");
		Mockito.when(pfConfig.getTenantSpecificDropOffUrl(anyString())).thenReturn("tenant1");

		PickupDataModel pickupDataModel = new PickupDataModel();
		pickupDataModel.setPaymentType("Domestic_Payment");
		pickupDataModel.setIntentId("af0ifjsldkj");

		when(restClientSyncImpl.callForPost(any(), any(), Mockito.eq(String.class), any()))
				.thenReturn("{\"REF\":\"22D770430210FBA5C34F7A14BC63E4827DB6BC16352A11FD6BDE23171F94\"}");

		service.dropOffOnConsentSubmission(pickupDataModel, "intentId", "instanceUserId", "instancePwd");
	}
	
	@Test
	public void testDropOffOnConsentSubmissionWithJsonResponse1() {
		Mockito.when(headerAttributes.getTenantId()).thenReturn("tenant1");
		Mockito.when(pfConfig.getTenantSpecificDropOffUrl(anyString())).thenReturn("tenant1");

		PickupDataModel pickupDataModel = new PickupDataModel();
		pickupDataModel.setPaymentType("Domestic_Payment");
		pickupDataModel.setIntentId("af0ifjsldkj");
		when(restClientSyncImpl.callForPost(any(), any(), Mockito.eq(String.class), any()))
				.thenReturn("Authentication Required");

		service.dropOffOnConsentSubmission(pickupDataModel, "intentId", "instanceUserId", "instancePwd");
	}
	
	@Test
	public void testDropOffOnConsentSubmissionWithJsonResponse2() {
		Mockito.when(headerAttributes.getTenantId()).thenReturn("tenant1");
		Mockito.when(pfConfig.getTenantSpecificDropOffUrl(anyString())).thenReturn("tenant1");

		PickupDataModel pickupDataModel = new PickupDataModel();
		pickupDataModel.setPaymentType("Domestic_Payment");
		pickupDataModel.setIntentId("af0ifjsldkj");
		pickupDataModel.setConsentExpiryInMinutes(null);
		pickupDataModel.setResumePath("demoResumepPatch");
		when(restClientSyncImpl.callForPost(any(), any(), Mockito.eq(String.class), any()))
				.thenReturn("Authentication Required");

		service.dropOffOnConsentSubmission(pickupDataModel, "intentId", "instanceUserId", "instancePwd");
	}

	@Test
	public void testDropOffOnSCAisBypassConsentPage() {

		Mockito.when(headerAttributes.getTenantId()).thenReturn("tenant1");
		Mockito.when(headerAttributes.getCorrelationId()).thenReturn("d6c1e2c0-e3c0-4e85-af99-34b8654bd1e3");
		Mockito.when(headerAttributes.getChannelId()).thenReturn("b365");
		Mockito.when(headerAttributes.getTppCID()).thenReturn("tppCID");
		Mockito.when(headerAttributes.getScopes()).thenReturn("accounts");
		Mockito.when(headerAttributes.isBypassConsentPage()).thenReturn(true);
		Mockito.when(headerAttributes.getIntentId()).thenReturn("intentID");

		Mockito.when(pfConfig.getTenantSpecificDropOffUrl(anyString())).thenReturn("tenant1");
		Mockito.when(pfConfig.getScainstanceusername()).thenReturn("32118465");
		Mockito.when(pfConfig.getScainstancepassword()).thenReturn("331947");
		Mockito.when(pfConfig.getScainstanceId()).thenReturn("1");

		when(restClientSyncImpl.callForPost(any(), any(), Mockito.eq(String.class), any()))
				.thenReturn("{\"REF\":\"22D770430210FBA5C34F7A14BC63E4827DB6BC16352A11FD6BDE23171F94\"}");

		service.dropOffOnSCA("userId", "authenticationTime");
	}

	@Test
	public void testDropOffOnSCADoesnottBypassConsentPage() {

		Mockito.when(headerAttributes.getTenantId()).thenReturn("tenant1");
		Mockito.when(headerAttributes.getCorrelationId()).thenReturn("d6c1e2c0-e3c0-4e85-af99-34b8654bd1e3");
		Mockito.when(headerAttributes.getChannelId()).thenReturn("b365");
		Mockito.when(headerAttributes.getTppCID()).thenReturn("tppCID");
		Mockito.when(headerAttributes.getScopes()).thenReturn("accounts");
		Mockito.when(headerAttributes.isBypassConsentPage()).thenReturn(false);

		Mockito.when(pfConfig.getTenantSpecificDropOffUrl(anyString())).thenReturn("tenant1");
		Mockito.when(pfConfig.getScainstanceusername()).thenReturn("32118465");
		Mockito.when(pfConfig.getScainstancepassword()).thenReturn("331947");
		Mockito.when(pfConfig.getScainstanceId()).thenReturn("1");

		when(restClientSyncImpl.callForPost(any(), any(), Mockito.eq(String.class), any()))
				.thenReturn("{\"REF\":\"22D770430210FBA5C34F7A14BC63E4827DB6BC16352A11FD6BDE23171F94\"}");

		service.dropOffOnSCA("userId", "authenticationTime");
	}
}
