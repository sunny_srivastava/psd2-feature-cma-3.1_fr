package com.capgemini.psd2.scaconsenthelper.helpers.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.naming.NamingException;
import javax.naming.directory.BasicAttributes;

import org.json.JSONException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.aisp.domain.OBAccount6;
import com.capgemini.psd2.aisp.domain.OBAccount6Account;
import com.capgemini.psd2.aisp.domain.OBBranchAndFinancialInstitutionIdentification50;
import com.capgemini.psd2.aisp.domain.OBExternalAccountSubType1Code;
import com.capgemini.psd2.aisp.domain.OBExternalAccountType1Code;
import com.capgemini.psd2.aisp.domain.OBReadAccount6;
import com.capgemini.psd2.aisp.domain.OBReadAccount6Data;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AispConsent;
import com.capgemini.psd2.consent.domain.PSD2Account;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.integration.adapter.TPPInformationAdaptor;
import com.capgemini.psd2.pisp.domain.CustomDPaymentConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.PaymentConsentsPlatformResource;
import com.capgemini.psd2.pisp.payment.setup.platform.adapter.PaymentSetupPlatformAdapter;
import com.capgemini.psd2.pisp.stage.domain.AmountDetails;
import com.capgemini.psd2.pisp.stage.domain.CreditorDetails;
import com.capgemini.psd2.pisp.stage.domain.CustomConsentAppViewData;
import com.capgemini.psd2.pisp.stage.domain.CustomDebtorDetails;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.pisp.stage.domain.RemittanceDetails;
import com.capgemini.psd2.scaconsenthelper.config.helpers.ConsentViewHelper;
import com.capgemini.psd2.scaconsenthelper.config.helpers.PispConsentCreationDataHelper;
import com.capgemini.psd2.scaconsenthelper.models.PickupDataModel;
import com.capgemini.psd2.tppinformation.adaptor.ldap.constants.TPPInformationConstants;
import com.capgemini.psd2.utilities.StringUtils;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringJUnit4ClassRunner.class)
public class ConsentViewHelperTest {

	@InjectMocks
	private ConsentViewHelper consentViewHelper;
	@Mock
	private PispConsentCreationDataHelper consentCreationDataHelper;

	@Mock
	private PaymentSetupPlatformAdapter paymentSetupPlatformAdapter;

	@Mock
	private TPPInformationAdaptor tppInformationAdaptor;
	
	@Mock
	private Map<String, String> consentSupportedSchemeMap = new HashMap<>();
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void retrieveConsentDetailsTest(){
		PickupDataModel pickupDataModel = new PickupDataModel();
		pickupDataModel.setClientId("W4H36Sejd6Ei4VyywKJD6p");
		pickupDataModel.setIntentId("2bbe2bbf-3d34-4e10-a1ac-c8d5d5865017");
		pickupDataModel.setCorrelationId("1234567");
		
		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();
		
		PaymentConsentsPlatformResource paymentConsentPlatformResource = new PaymentConsentsPlatformResource();
		paymentConsentPlatformResource.setPaymentId("2bbe2bbf-3d34-4e10-a1ac-c8d5d5865017");
		paymentConsentPlatformResource.setPaymentType("DOMESTIC");
		paymentConsentPlatformResource.setSetupCmaVersion("3.1");
		paymentConsentPlatformResource.setTenantId("tenant1");
		
		Mockito.when(paymentSetupPlatformAdapter.retrievePaymentSetupPlatformResource("2bbe2bbf-3d34-4e10-a1ac-c8d5d5865017")).thenReturn(paymentConsentPlatformResource);
		
		CustomConsentAppViewData customConsentAppViewData = new CustomConsentAppViewData();
		CustomDebtorDetails debtorDetails = new CustomDebtorDetails();
		debtorDetails.setSchemeName("UK.OBIE.IBAN");
		debtorDetails.setIdentification("FR1420041010050500013M02606");
		
		customConsentAppViewData.setDebtorDetails(debtorDetails);
		//Added Arguement
		Mockito.when(consentCreationDataHelper.retrieveConsentAppStagedViewData("2bbe2bbf-3d34-4e10-a1ac-c8d5d5865017", stageIdentifiers)).thenReturn(customConsentAppViewData);
		
		consentViewHelper.retrieveConsentDetails(pickupDataModel);
		
	}
	
	@Test(expected = PSD2Exception.class)
	public void retrieveConsentDetailsTest1(){
		PickupDataModel pickupDataModel = new PickupDataModel();
		pickupDataModel.setClientId("W4H36Sejd6Ei4VyywKJD6p");
		pickupDataModel.setIntentId("2bbe2bbf-3d34-4e10-a1ac-c8d5d5865017");
		pickupDataModel.setCorrelationId("1234567");
		
		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();
		
		PaymentConsentsPlatformResource paymentConsentPlatformResource = new PaymentConsentsPlatformResource();
		paymentConsentPlatformResource.setPaymentId("2bbe2bbf-3d34-4e10-a1ac-c8d5d5865017");
		paymentConsentPlatformResource.setPaymentType("DOMESTIC");
		paymentConsentPlatformResource.setSetupCmaVersion("3.1");
		paymentConsentPlatformResource.setTenantId("tenant1");
		paymentConsentPlatformResource.setTppDebtorDetails("true");
		
		Mockito.when(paymentSetupPlatformAdapter.retrievePaymentSetupPlatformResource("2bbe2bbf-3d34-4e10-a1ac-c8d5d5865017")).thenReturn(paymentConsentPlatformResource);
		
		CustomConsentAppViewData customConsentAppViewData = new CustomConsentAppViewData();
		CustomDebtorDetails debtorDetails = new CustomDebtorDetails();
		debtorDetails.setSchemeName("IBAN");
		debtorDetails.setIdentification("FR1420041010050500013M02606");
		
		customConsentAppViewData.setDebtorDetails(debtorDetails);
		//Added Arguement
		Mockito.when(paymentSetupPlatformAdapter.populateStageIdentifiers("2bbe2bbf-3d34-4e10-a1ac-c8d5d5865017")).thenReturn(stageIdentifiers);
		Mockito.when(consentCreationDataHelper.retrieveConsentAppStagedViewData("2bbe2bbf-3d34-4e10-a1ac-c8d5d5865017", stageIdentifiers)).thenReturn(customConsentAppViewData);
		
		consentViewHelper.retrieveConsentDetails(pickupDataModel);
		
	}
	
	@Test
	public void retrieveConsentDetails_validateDebtorDetails_Exception(){
		PickupDataModel pickupDataModel = new PickupDataModel();
		pickupDataModel.setClientId("W4H36Sejd6Ei4VyywKJD6p");
		pickupDataModel.setIntentId("2bbe2bbf-3d34-4e10-a1ac-c8d5d5865017");
		pickupDataModel.setCorrelationId("1234567");
		
		PaymentConsentsPlatformResource paymentConsentPlatformResource = new PaymentConsentsPlatformResource();
		paymentConsentPlatformResource.setPaymentId("2bbe2bbf-3d34-4e10-a1ac-c8d5d5865017");
		paymentConsentPlatformResource.setPaymentType("DOMESTIC");
		paymentConsentPlatformResource.setSetupCmaVersion("3.1");
		paymentConsentPlatformResource.setTenantId("tenant1");
		paymentConsentPlatformResource.setTppDebtorDetails("true");
		
		Mockito.when(paymentSetupPlatformAdapter.retrievePaymentSetupPlatformResource("2bbe2bbf-3d34-4e10-a1ac-c8d5d5865017")).thenReturn(paymentConsentPlatformResource);
		
		CustomConsentAppViewData customConsentAppViewData = new CustomConsentAppViewData();
		CustomDebtorDetails debtorDetails = new CustomDebtorDetails();
		debtorDetails.setSchemeName("UK.OBIE.IBAN");
		debtorDetails.setIdentification("FR1420041010050500013M02606");
		
		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();
		
		customConsentAppViewData.setDebtorDetails(debtorDetails);
		Mockito.when(consentCreationDataHelper.retrieveConsentAppStagedViewData("2bbe2bbf-3d34-4e10-a1ac-c8d5d5865017", stageIdentifiers)).thenReturn(customConsentAppViewData);
		
		consentViewHelper.retrieveConsentDetails(pickupDataModel);
		
	}
	
	@Test(expected=PSD2Exception.class)
	public void validateAccountWithAccountListTest1(){
		CustomDebtorDetails customDebtorDetails = new CustomDebtorDetails();
		customDebtorDetails.setSchemeName("UK.OBIE.IBAN");
		customDebtorDetails.setIdentification("FR1420041010050500013M02606");
		customDebtorDetails.setName("IBAN");
		customDebtorDetails.setSecondaryIdentification("123");
		
		OBReadAccount6 obReadAccount2 = new OBReadAccount6();
		OBReadAccount6Data data = new OBReadAccount6Data();
		
		List<OBAccount6> accountList = new ArrayList<>();
		OBAccount6 account1 = new OBAccount6();
		account1.setAccountId("8d6d274a-d095-4ae1-adb4-c020d7cfe027");
		account1.setAccountType(OBExternalAccountType1Code.BUSINESS);
		
		List<OBAccount6Account> accounts = new ArrayList<>();
		OBAccount6Account acc1 = new OBAccount6Account();
		acc1.setSchemeName("IBAN");
		acc1.setIdentification("FR1420041010050500013M02606");
		
		account1.setAccount(accounts);
		accountList.add(account1);
		data.setAccount(accountList);
		obReadAccount2.setData(data);
		
		consentViewHelper.validateAccountWithAccountList(customDebtorDetails,obReadAccount2);
		
	}
	
	@Test(expected=PSD2Exception.class)
	public void validateAccountWithAccountListTest2(){
		CustomDebtorDetails customDebtorDetails = new CustomDebtorDetails();
		customDebtorDetails.setSchemeName("UK.OBIE.IBAN");
		customDebtorDetails.setIdentification("FR1420041010050500013M02606");
		customDebtorDetails.setName("IBAN");
		customDebtorDetails.setSecondaryIdentification("123");
		
		OBReadAccount6 obReadAccount2 = new OBReadAccount6();
		OBReadAccount6Data data = new OBReadAccount6Data();
		
		List<OBAccount6> accountList = new ArrayList<>();
		OBAccount6 account1 = new OBAccount6();
		account1.setAccountId("8d6d274a-d095-4ae1-adb4-c020d7cfe027");
		account1.setAccountType(OBExternalAccountType1Code.BUSINESS);
		
		List<OBAccount6Account> accounts = new ArrayList<>();
		OBAccount6Account acc1 = new OBAccount6Account();
		acc1.setSchemeName("IBAN");
		acc1.setIdentification("FR1420041010050500013M02606");
		
		account1.setAccount(accounts);
		data.setAccount(accountList);
		obReadAccount2.setData(data);
		
		consentViewHelper.validateAccountWithAccountList(customDebtorDetails,obReadAccount2);
		
	}
	
	@Test(expected=PSD2Exception.class)
	public void validateAccountWithAccountListTest3(){
		CustomDebtorDetails customDebtorDetails = new CustomDebtorDetails();
		customDebtorDetails.setSchemeName("UK.OBIE.IBAN");
		customDebtorDetails.setIdentification("FR1420041010050500013M02606");
		customDebtorDetails.setName("IBAN");
		customDebtorDetails.setSecondaryIdentification("123");
		
		OBReadAccount6 obReadAccount2 = new OBReadAccount6();
		OBReadAccount6Data data = new OBReadAccount6Data();
		List<OBAccount6> accountList = new ArrayList<>();
		
		PSD2Account account1 = new PSD2Account();
		account1.setAccountId("8d6d274a-d095-4ae1-adb4-c020d7cfe027");
		account1.setAccountType(OBExternalAccountType1Code.BUSINESS);
		
		Map<String,String> additionalInformation = new HashMap<>();
		additionalInformation.put("Uk.OBIE.IBAN", "FR1420041010050500013M02606");
		account1.setAdditionalInformation(additionalInformation);
		
		accountList.add(account1);
		data.setAccount(accountList);
		obReadAccount2.setData(data);
		
		consentViewHelper.validateAccountWithAccountList(customDebtorDetails,obReadAccount2);
		
	}
	
	@Test
	public void validateAccountWithAccountListTest4(){
		consentSupportedSchemeMap.put("UK.OBIE.IBAN", "FR1420041010050500013M02606");
		//ReflectionTestUtils.setField(consentViewHelper, "consentSupportedSchemeMap", consentSupportedSchemeMap);
		
		CustomDebtorDetails customDebtorDetails = new CustomDebtorDetails();
		customDebtorDetails.setSchemeName("UK.OBIE.IBAN");
		customDebtorDetails.setIdentification("FR1420041010050500013M02606");
		customDebtorDetails.setName("IBAN");
		customDebtorDetails.setSecondaryIdentification("123");
		
		OBReadAccount6 obReadAccount2 = new OBReadAccount6();
		OBReadAccount6Data data = new OBReadAccount6Data();
		List<OBAccount6> accountList = new ArrayList<>();
		
		PSD2Account account1 = new PSD2Account();
		account1.setAccountId("8d6d274a-d095-4ae1-adb4-c020d7cfe027");
		account1.setAccountType(OBExternalAccountType1Code.BUSINESS);
		
		List<OBAccount6Account> accounts = new ArrayList<>();
		OBAccount6Account acc1 = new OBAccount6Account();
		acc1.setSchemeName("UK.OBIE.IBAN");
		acc1.setIdentification("FR1420041010050500013M02606");
		accounts.add(acc1);
		account1.setAccount(accounts);
		Map<String,String> additionalInformation = new HashMap<>();
		additionalInformation.put("UK.OBIE.IBAN", "FR1420041010050500013M02606");
		account1.setAdditionalInformation(additionalInformation);
		
		accountList.add(account1);
		data.setAccount(accountList);
		obReadAccount2.setData(data);
		
		Mockito.when(consentSupportedSchemeMap.containsKey(customDebtorDetails.getSchemeName())).thenReturn(true);
		Mockito.when(consentSupportedSchemeMap.get("IBAN")).thenReturn("UK.OBIE.IBAN");
		
		
		consentViewHelper.validateAccountWithAccountList(customDebtorDetails,obReadAccount2);
		
	}
	
	@Test
	public void populateAccountListFromAccountDetailsTest(){
		
		
		AispConsent input = new AispConsent();
		AccountDetails details = new AccountDetails();
		details.setCurrency("EUR");
		details.setNickname("nickName");
		details.setAccountId("8d6d274a-d095-4ae1-adb4-c020d7cfe027");
		details.setAccountSubType(OBExternalAccountSubType1Code.CURRENTACCOUNT);
		details.setAccount(new OBAccount6Account().identification("FR1420041010050500013M02606"));
		details.setServicer(new OBBranchAndFinancialInstitutionIdentification50());
		
		input.setAccountDetails(new ArrayList<AccountDetails>());
		input.getAccountDetails().add(details);
		
		
		List<OBAccount6> resultList = ConsentViewHelper.populateAccountListFromAccountDetails(input);
		
		assertEquals(StringUtils.generateHashedValue("FR1420041010050500013M02606"), ((PSD2Account) resultList.get(0)).getHashedValue());
	}
	
	@Test
	public void populateLimitedSetUpDataforUITest(){
		
		CustomConsentAppViewData consentAppPaymentSetupData = new CustomConsentAppViewData();
		consentAppPaymentSetupData.setAmountDetails(new AmountDetails().currency("EUR").amount("20.99") );
		consentAppPaymentSetupData.setCreditorDetails(new CreditorDetails().identification("FR1420041010050500013M02606").name("name"));
		consentAppPaymentSetupData.setRemittanceDetails(new RemittanceDetails().reference("reference").unstructured("unstructure"));
		
		CustomDPaymentConsentsPOSTResponse response = consentViewHelper.populateLimitedSetUpDataforUI(consentAppPaymentSetupData);
		
		assertEquals("20.99", response.getData().getInitiation().getInstructedAmount().getAmount());
		assertNotEquals("INR", response.getData().getInitiation().getInstructedAmount().getCurrency());
	}
	
	@Test
	public void getTppInformationByClientIdTest() throws NamingException, JSONException, JsonParseException, JsonMappingException, IOException{
		BasicAttributes ttpInfoAttr = new BasicAttributes();
		ttpInfoAttr.put(TPPInformationConstants.LEGAL_ENTITY_NAME, "UK");
		
		when(tppInformationAdaptor.fetchTPPInformation(anyString())).thenReturn(ttpInfoAttr);
		when(tppInformationAdaptor.fetchApplicationName(anyString())).thenReturn("tppName");
		
		
		String response = consentViewHelper.getTppInformationByClientId("clientId");
		
		HashMap<String, String> map = new ObjectMapper().readValue(response, HashMap.class );
		
		assertEquals("UK", map.get("tppName"));
	}
	
	@Test
	public void getConsentSupportedSchemeMap(){
		assertEquals(new HashMap<>(), consentViewHelper.getConsentSupportedSchemeMap());
	}
	
	@Test
	public void getTppInformationByClientIdNullTest() throws NamingException, JSONException, JsonParseException, JsonMappingException, IOException{
		BasicAttributes ttpInfoAttr = new BasicAttributes();
		ttpInfoAttr.put(TPPInformationConstants.LEGAL_ENTITY_NAME, "UK");
		
		when(tppInformationAdaptor.fetchTPPInformation(anyString())).thenReturn(null);
		when(tppInformationAdaptor.fetchApplicationName(anyString())).thenReturn("tppName");
		
		assertEquals(null, consentViewHelper.getTppInformationByClientId("clientId"));
	}
	
}
