package com.capgemini.psd2.scaconsenthelper.helpers.test;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import java.util.Base64;

import org.assertj.core.condition.AnyOf;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;

import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.impl.RestClientSyncImpl;
import com.capgemini.psd2.scaconsenthelper.config.PFConfig;
import com.capgemini.psd2.scaconsenthelper.config.helpers.DropOffDataService;
import com.capgemini.psd2.scaconsenthelper.constants.OIDCConstants;
import com.capgemini.psd2.scaconsenthelper.models.DropOffRequest;

import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.rest.client.helpers.HeaderHelper;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.impl.RestClientSyncImpl;
import com.capgemini.psd2.scaconsenthelper.config.PFConfig;
import com.capgemini.psd2.scaconsenthelper.constants.OIDCConstants;
import com.capgemini.psd2.scaconsenthelper.constants.PFConstants;
import com.capgemini.psd2.scaconsenthelper.constants.SCAConsentHelperConstants;
import com.capgemini.psd2.scaconsenthelper.models.DropOffRequest;
import com.capgemini.psd2.scaconsenthelper.models.DropOffResponse;
import com.capgemini.psd2.utilities.JSONUtilities;

import junit.framework.Assert;


@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
public class DropOffDataServiceTest {
	
	@InjectMocks
	private DropOffDataService dropOffDataService = new DropOffDataService();
	
	@Mock
	private PFConfig pfConfig;
	
	@Mock
	private RestClientSyncImpl restClient;
 
	@Mock
	private RequestHeaderAttributes requestHeaderAttributes;
	
	@Test
	public void dropOffOnSuccessTest() {
		DropOffRequest dropOffRequest = new DropOffRequest();
		//when(requestHeaderAttributes.getChannelId()).thenReturn("12345");
		dropOffRequest.setAcr(OIDCConstants.SCA_ACR);
		dropOffRequest.setChannel_id("12311");
		
		DropOffRequest expected = new DropOffRequest();
		expected.setAcr(OIDCConstants.SCA_ACR);
		expected.setChannel_id("12311");
		expected.setSubject("123");
		dropOffRequest.setBypassConsentPage(true);
		//requestHeaderAttributes.setIntentId("123");
		when(pfConfig.getScainstanceusername()).thenReturn("demo");
		when(pfConfig.getScainstancepassword()).thenReturn("demo");
		when(requestHeaderAttributes.getIntentId()).thenReturn("123");
		when(restClient.callForPost(any(RequestInfo.class), any(DropOffRequest.class), Mockito.eq(String.class), any(HttpHeaders.class)))
		.thenReturn("{\"REF\":\"22D770430210FBA5C34F7A14BC63E4827DB6BC16352A11FD6BDE23171F94\"}");
		String actual = dropOffDataService.dropOffOnSuccess(dropOffRequest);
		String expectedResult = dropOff(expected);
		Assert.assertEquals(expectedResult, actual);
		
	}
	
	@Test
	public void dropOffOnSuccessTest_2() {
		DropOffRequest dropOffRequest = new DropOffRequest();
		//when(requestHeaderAttributes.getChannelId()).thenReturn("12345");
		dropOffRequest.setAcr(OIDCConstants.SCA_ACR);
		dropOffRequest.setChannel_id("12311");
		
		DropOffRequest expected = new DropOffRequest();
		expected.setAcr(OIDCConstants.SCA_ACR);
		expected.setChannel_id("12311");
		//expected.setSubject("123");
		dropOffRequest.setBypassConsentPage(false);
		//requestHeaderAttributes.setIntentId("123");
		when(pfConfig.getScainstanceusername()).thenReturn("demo");
		when(pfConfig.getScainstancepassword()).thenReturn("demo");
		when(requestHeaderAttributes.getIntentId()).thenReturn("123");
		when(restClient.callForPost(any(RequestInfo.class), any(DropOffRequest.class), Mockito.eq(String.class), any(HttpHeaders.class)))
		.thenReturn("{\"REF\":\"22D770430210FBA5C34F7A14BC63E4827DB6BC16352A11FD6BDE23171F94\"}");
		String actual = dropOffDataService.dropOffOnSuccess(dropOffRequest);
		String expectedResult = dropOff(expected);
		Assert.assertEquals(expectedResult, actual);
		
	}
	
	@Test
	public void dropOffOnCancelTest() {
		DropOffRequest expected = new DropOffRequest();
		
		when(pfConfig.getScainstanceusername()).thenReturn("demo");
		when(pfConfig.getScainstancepassword()).thenReturn("demo");
		when(requestHeaderAttributes.getIntentId()).thenReturn("123");
		when(restClient.callForPost(any(RequestInfo.class), any(DropOffRequest.class), Mockito.eq(String.class), any(HttpHeaders.class)))
		.thenReturn("{\"REF\":\"22D770430210FBA5C34F7A14BC63E4827DB6BC16352A11FD6BDE23171F94\"}");
		
		String actual = dropOffDataService.dropOffOnCancel();
		String expectedResult = dropOff(expected);
		
		Assert.assertEquals(expectedResult, actual);
		
	}
	
	@Test
	public void dropOffTest() {
		DropOffRequest expected = new DropOffRequest();
		
		when(pfConfig.getScainstanceusername()).thenReturn("demo");
		when(pfConfig.getScainstancepassword()).thenReturn("demo");
		when(requestHeaderAttributes.getIntentId()).thenReturn(null);
		when(restClient.callForPost(any(RequestInfo.class), any(DropOffRequest.class), Mockito.eq(String.class), any(HttpHeaders.class)))
		.thenReturn("{\"REF\":\"22D770430210FBA5C34F7A14BC63E4827DB6BC16352A11FD6BDE23171F94\"}");
		
		String actual = dropOffDataService.dropOffOnCancel();
		String expectedResult = dropOff(expected);
		
		Assert.assertEquals(expectedResult, actual);
		
	}
	
	private String dropOff(DropOffRequest dropOffRequest) {
		when(requestHeaderAttributes.getIntentId()).thenReturn("123");
		RequestInfo requestInfo = new RequestInfo();
		String tenantId = requestHeaderAttributes.getTenantId() != null ? requestHeaderAttributes.getTenantId() : PSD2Constants.DEFAULT;
		requestInfo.setUrl(pfConfig.getTenantSpecificDropOffUrl(tenantId));
		String credentials = pfConfig.getScainstanceusername().concat(":").concat(pfConfig.getScainstancepassword());
		String encoderString = Base64.getEncoder().encodeToString(credentials.getBytes());
		HttpHeaders httpHeaders = HeaderHelper.populateJSONContentTypeHeader();
		httpHeaders.add(SCAConsentHelperConstants.AUTHORIZATION_HEADER,SCAConsentHelperConstants.BASIC + " ".concat(encoderString));
		httpHeaders.add(PFConstants.PING_INSTANCE_ID, pfConfig.getScainstanceId());
		String jsonResponse = restClient.callForPost(requestInfo, dropOffRequest, String.class, httpHeaders);
		DropOffResponse dropOffResponse = JSONUtilities.getObjectFromJSONString(jsonResponse, DropOffResponse.class);
		return dropOffResponse.getRef();
	}
}
