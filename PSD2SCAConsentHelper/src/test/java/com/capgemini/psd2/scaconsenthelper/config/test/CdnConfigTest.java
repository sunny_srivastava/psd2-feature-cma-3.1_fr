package com.capgemini.psd2.scaconsenthelper.config.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.scaconsenthelper.config.CdnConfig;

@RunWith(SpringJUnit4ClassRunner.class)
public class CdnConfigTest {
	
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void cdnConfig_Null() {
		Map<String,String> map = CdnConfig.populateCdnAttributes();
		assertNull(map.get("cdnBaseURL")) ;
		assertNull(map.get("buildVersion"));
	}
	
	@Test
	public void cdnConfig_Populated() {
		CdnConfig config = new CdnConfig();
		config.setAppHashCode("dummyAppHashCode");
		config.setBuildVersion("dummyBuildVersion");
		config.setCdnBaseURL("dummyURL");
		config.setLibsHashCode("dummyLibHashCode");
		config.setTemplateHashCode("dummyTemplateHashCode");
		
		Map<String,String> map = CdnConfig.populateCdnAttributes();
		assertNotNull(map.get("cdnBaseURL")) ;
		assertNotNull(map.get("libsHashCode"));
		assertNotNull(map.get("templateHashCode"));
		assertNotNull(map.get("appHashCode"));
		assertNotNull(map.get("buildVersion"));
		assertEquals("dummyURL", map.get("cdnBaseURL"));
		assertEquals("dummyBuildVersion", map.get("buildVersion"));
	}
	
	@After
	public void tearDown() {
		CdnConfig config = new CdnConfig();
		config.setAppHashCode(null);
		config.setBuildVersion(null);
		config.setCdnBaseURL(null);
		config.setLibsHashCode(null);
		config.setTemplateHashCode(null);
	}
}
