package com.capgemini.psd2.scaconsenthelper.filters;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.LoggerAttribute;
import com.capgemini.psd2.logger.LoggerUtils;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.scaconsenthelper.constants.PFConstants;
import com.capgemini.psd2.scaconsenthelper.constants.SCAConsentHelperConstants;
import com.capgemini.psd2.scaconsenthelper.models.PickupDataModel;
import com.capgemini.psd2.scaconsenthelper.models.RawAccessJwtToken;
import com.capgemini.psd2.scaconsenthelper.models.SecurityRequestAttributes;
import com.capgemini.psd2.security.exceptions.PSD2SecurityException;

@RunWith(SpringJUnit4ClassRunner.class)
public class ConsentBlockBackActionFilterTest {

	@InjectMocks
	private ConsentBlockBackActionFilter filter;
	
	@Mock
	private LoggerUtils loggerUtils = new LoggerUtils();
	
	@Mock
	private SecurityRequestAttributes securityRequestAttributes; 
	
	@Mock
	private RequestHeaderAttributes requestHeaderAttributes;
	
	private HttpServletRequest request;
	
	private HttpServletResponse response;

	private LoggerAttribute loggerAttribute = new LoggerAttribute();
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		request = new MockHttpServletRequest();
		response = new MockHttpServletResponse();
		
		loggerAttribute.setApiId("123");
		
	}

	@After
	public void tearDown() throws Exception {
		request = null;
		response = null;
	}


	@Test
	public void testDoFilterInternal_NullRefId_PSD2Exception() throws IOException, ServletException {
		when(securityRequestAttributes.getParamMap()).thenReturn(new HashMap<String, String[]>());
		
		filter.doFilter(request, response, null);
	}
	
	@Test
	public void testDoFilterInternal_EmptyRefId_PSD2Exception() throws IOException, ServletException {
		
		Map<String, String[]> paramMap = new HashMap<>();
		paramMap.put(PFConstants.REF, new String[]{ "" } );
		when(securityRequestAttributes.getParamMap()).thenReturn(new HashMap<String, String[]>());
		
		filter.doFilter (request, response, null);
	}
	
	
	public void testDoFilterInternal_MismatchRefId_PSD2Exception() throws IOException, ServletException {
		
		Map<String, String[]> paramMap = new HashMap<>();
		paramMap.put(PFConstants.REF, new String[]{ "refId" } );
		
		
		HttpServletRequest mockRequest = Mockito.mock(HttpServletRequest.class);
		
		String token = "dummy";

		Cookie[] cookies = new Cookie[] { new Cookie(SCAConsentHelperConstants.CUSTOM_SESSION_CONTROLL_COOKIE, token) };

		when(mockRequest.getParameter(PFConstants.REF)).thenReturn("refId");
		when(mockRequest.getHeader(PSD2Constants.NETSCALER_TENANT_ID_HEADER_NAME)).thenReturn("tenantId");
		when(mockRequest.getAttribute("FILTERED")).thenReturn(null, "filtered");
		when(mockRequest.getCookies()).thenReturn(cookies);
		
		
		String jweDecryptionKey = "dummyKey";
		
		ReflectionTestUtils.setField(filter, "jweDecryptionKey", jweDecryptionKey);
		
		RawAccessJwtToken spyOnFoo = Mockito.spy(new RawAccessJwtToken(token,requestHeaderAttributes));
		doReturn("{\"originatorRefId\":\"originatorRefId\"}").when(spyOnFoo).parseAndReturnClaims(jweDecryptionKey);
		
		//when(spy(RawAccessJwtToken.class.getConstructor(String.class, RequestHeaderAttributes.class))).thenReturn(value, values)
		
		when(securityRequestAttributes.getParamMap()).thenReturn(new HashMap<String, String[]>());
		
		filter.doFilter(mockRequest, response, null);
		
		
	}

	@Test
	public void testDoFilterInternal_Success() throws IOException, ServletException {
		MockHttpServletRequest request = new MockHttpServletRequest();
		RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
		Map<String, String[]> paramMap = new HashMap<>();
		paramMap.put(PFConstants.REF, new String[]{ "refId" } );
		
		class ConsentBlockBackActionFilterCustom extends ConsentBlockBackActionFilter {
			
			@Override
			protected String getAlreadyFilteredAttributeName() {
				return "FILTERED";
			}
			
		}
		
		ConsentBlockBackActionFilterCustom customFilter = new ConsentBlockBackActionFilterCustom();
		
		HttpServletRequest mockRequest = Mockito.mock(HttpServletRequest.class);
		when(mockRequest.getParameter(PFConstants.REF)).thenReturn("refId");
		when(mockRequest.getHeader(PSD2Constants.NETSCALER_TENANT_ID_HEADER_NAME)).thenReturn("tenantId");
		when(mockRequest.getAttribute("FILTERED")).thenReturn(null, "filtered");
		when(securityRequestAttributes.getParamMap()).thenReturn(new HashMap<String, String[]>());
		
		ReflectionTestUtils.setField(customFilter, "securityRequestAttributes", securityRequestAttributes);
		ReflectionTestUtils.setField(customFilter, "requestHeaderAttributes", requestHeaderAttributes);
		
		customFilter.doFilter(mockRequest, response, new FilterChain() {
			
			@Override
			public void doFilter(ServletRequest request, ServletResponse response)
					throws IOException, ServletException {

			}
		});
		
		
	}
	
	@Test(expected = Exception.class)
	public void testDoFilterInternal() throws IOException, ServletException {
		MockHttpServletRequest request = new MockHttpServletRequest();
		RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
		Map<String, String[]> paramMap = new HashMap<>();
		paramMap.put(PFConstants.REF, new String[]{ "refId" } );
		
		class ConsentBlockBackActionFilterCustom extends ConsentBlockBackActionFilter {
			
			@Override
			protected String getAlreadyFilteredAttributeName() {
				return "FILTERED";
			}
			
		}
		
		ConsentBlockBackActionFilterCustom customFilter = new ConsentBlockBackActionFilterCustom();
		
		HttpServletRequest mockRequest = Mockito.mock(HttpServletRequest.class);
		when(mockRequest.getParameter(PFConstants.REF)).thenReturn("refId");
		
		String token = "dummy";

		Cookie[] cookies = new Cookie[] { new Cookie(SCAConsentHelperConstants.CUSTOM_SESSION_CONTROLL_COOKIE, token) };
		
		when(mockRequest.getHeader(PSD2Constants.NETSCALER_TENANT_ID_HEADER_NAME)).thenReturn("tenantId");
		when(mockRequest.getAttribute("FILTERED")).thenReturn(null, "filtered");
		when(mockRequest.getCookies()).thenReturn(cookies);
		when(securityRequestAttributes.getParamMap()).thenReturn(new HashMap<String, String[]>());
		
		ReflectionTestUtils.setField(customFilter, "securityRequestAttributes", securityRequestAttributes);
		ReflectionTestUtils.setField(customFilter, "requestHeaderAttributes", requestHeaderAttributes);
		
		customFilter.doFilter(mockRequest, response, new FilterChain() {
			
			@Override
			public void doFilter(ServletRequest request, ServletResponse response)
					throws IOException, ServletException {

			}
		});
		
		
	}
	
	
	@Test(expected=Exception.class)
	public void testDoFilterInternal_Cookie() throws IOException, ServletException {
		
		Map<String, String[]> paramMap = new HashMap<>();
		paramMap.put(PFConstants.REF, new String[]{ "refId" } );
		
		class ConsentBlockBackActionFilterCustom extends ConsentBlockBackActionFilter {
			
			@Override
			protected String getAlreadyFilteredAttributeName() {
				return "FILTERED";
			}
			
		}
		
		ConsentBlockBackActionFilterCustom customFilter = new ConsentBlockBackActionFilterCustom();
		
		HttpServletRequest mockRequest = Mockito.mock(HttpServletRequest.class);
		when(mockRequest.getParameter(PFConstants.REF)).thenReturn("refId");
		
		Cookie cookie = new Cookie(SCAConsentHelperConstants.CUSTOM_SESSION_CONTROLL_COOKIE, "test");
		Cookie[] cookieArray = { cookie };
		when(mockRequest.getCookies()).thenReturn(cookieArray);
		
		when(mockRequest.getHeader(PSD2Constants.NETSCALER_TENANT_ID_HEADER_NAME)).thenReturn("tenantId");
		when(mockRequest.getAttribute("FILTERED")).thenReturn(null, "filtered");
		when(securityRequestAttributes.getParamMap()).thenReturn(new HashMap<String, String[]>());
		
		ReflectionTestUtils.setField(customFilter, "securityRequestAttributes", securityRequestAttributes);
		ReflectionTestUtils.setField(customFilter, "requestHeaderAttributes", requestHeaderAttributes);
		
		Mockito.when(loggerUtils.populateLoggerData(any())).thenReturn(loggerAttribute);
		
		customFilter.doFilter(mockRequest, response, new FilterChain() {
			
			@Override
			public void doFilter(ServletRequest request, ServletResponse response)
					throws IOException, ServletException {

			}
		});
		
		
	}
	
	@Test(expected=Exception.class)
	public void testDoFilterInternal_TenantIdNull() throws IOException, ServletException {
		
		Map<String, String[]> paramMap = new HashMap<>();
		paramMap.put(PFConstants.REF, new String[]{ "refId" } );
		
		class ConsentBlockBackActionFilterCustom extends ConsentBlockBackActionFilter {
			
			@Override
			protected String getAlreadyFilteredAttributeName() {
				return "FILTERED";
			}
			
		}
		
		ConsentBlockBackActionFilterCustom customFilter = new ConsentBlockBackActionFilterCustom();
		
		HttpServletRequest mockRequest = Mockito.mock(HttpServletRequest.class);
		when(mockRequest.getParameter(PFConstants.REF)).thenReturn("refId");
		
		Cookie cookie = null;
		Cookie[] cookieArray = { cookie };
		when(mockRequest.getCookies()).thenReturn(cookieArray);
		
		when(mockRequest.getAttribute("FILTERED")).thenReturn(null, "filtered");
		when(securityRequestAttributes.getParamMap()).thenReturn(new HashMap<String, String[]>());
		
		ReflectionTestUtils.setField(customFilter, "securityRequestAttributes", securityRequestAttributes);
		ReflectionTestUtils.setField(customFilter, "requestHeaderAttributes", requestHeaderAttributes);
		
		Mockito.when(loggerUtils.populateLoggerData(any())).thenReturn(loggerAttribute);
		
		customFilter.doFilter(mockRequest, response, new FilterChain() {
			
			@Override
			public void doFilter(ServletRequest request, ServletResponse response)
					throws IOException, ServletException {

			}
		});
		
		
	}
	
	@Test(expected=PSD2SecurityException.class)
	public void refreshBack_Exception() {
		PickupDataModel intentData = new PickupDataModel();
		intentData.setOriginatorRefId("24325236435");
		
		filter.refreshBack(intentData, "24325236435");
	}
	
	@Test
	public void refreshBack() {
		PickupDataModel intentData = new PickupDataModel();
		intentData.setOriginatorRefId("24325235");
		
		filter.refreshBack(intentData, "24325236435");
	}
	
}
