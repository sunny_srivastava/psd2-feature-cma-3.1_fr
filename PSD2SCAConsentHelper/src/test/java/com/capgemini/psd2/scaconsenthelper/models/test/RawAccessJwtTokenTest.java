package com.capgemini.psd2.scaconsenthelper.models.test;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;

import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.scaconsenthelper.models.RawAccessJwtToken;
import com.capgemini.psd2.security.exceptions.PSD2SecurityException;

public class RawAccessJwtTokenTest {

	RequestHeaderAttributes requestHeaderAttributes = null;
	String token = null;
	
	@InjectMocks
	RawAccessJwtToken rawAccessJwtToken;

	@Before
	public void setUp() {
		requestHeaderAttributes = new RequestHeaderAttributes();
		requestHeaderAttributes.setCorrelationId("ba4f73f8-9a60-425b-aed8-2a7ef2509fea");
	}

	@Test
	public void getTokentest() {
		token = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJib2kxMjMiLCJzY29wZXMiOlsiQ1VTVE9NRVJfUk9MRSJdLCJpc3MiOiJodHRwOi8vU2FhUy5jb20iLCJqdGkiOiI3NmRiN2JjZS05Y2RhLTQ5NDMtOGM2ZS01NTFmNGUzNWMxYzciLCJpYXQiOjE1MDAyOTcyODMsImV4cCI6MTUxNTg0OTI4M30.eEpXpsLaoUH70-a19xqCcCtvDUsZ3b8sQ3mH6ovmgXDNiBvGFIts819nU9dnLJt3IMbdsscL5a9IaRGHNu980Q";
		rawAccessJwtToken = new RawAccessJwtToken(token, requestHeaderAttributes);
		assertEquals(token, rawAccessJwtToken.getToken());
	}
	
	@Test(expected=PSD2SecurityException.class)
	public void parseClaimsSuccessFlow(){
		token = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJib2kxMjMiLCJzY29wZXMiOlsiQ1VTVE9NRVJfUk9MRSJdLCJpc3MiOiJodHRwOi8vU2FhUy5jb20iLCJqdGkiOiI3NmRiN2JjZS05Y2RhLTQ5NDMtOGM2ZS01NTFmNGUzNWMxYzciLCJpYXQiOjE1MDAyOTcyODMsImV4cCI6MTUxNTg0OTI4M30.eEpXpsLaoUH70-a19xqCcCtvDUsZ3b8sQ3mH6ovmgXDNiBvGFIts819nU9dnLJt3IMbdsscL5a9IaRGHNu980Q";
		rawAccessJwtToken = new RawAccessJwtToken(token, requestHeaderAttributes);
		rawAccessJwtToken.parseClaims("0123456789abcdef", "0123456789abcdef");
	}
}