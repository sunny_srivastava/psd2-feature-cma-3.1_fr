package com.capgemini.psd2.scaconsenthelper.model.test;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.scaconsenthelper.config.AuthenticationConfig;
import com.capgemini.psd2.scaconsenthelper.models.DropOffRequest;

@RunWith(SpringJUnit4ClassRunner.class)
public class DropOffRequestTest {
	
	@Test
	public void dropOffRequest() {
		DropOffRequest obj = new DropOffRequest();
		obj.setIntentId("dummyID");
		obj.setResumePath("setResumePath");
		
		assertEquals("dummyID", obj.getIntentId());
		assertEquals("setResumePath", obj.getResumePath());
		assertEquals(null, obj.getIntent_type());
		assertEquals(null, obj.getUsername());
		assertEquals(null, obj.getAcr());
		assertEquals(null,obj.getChannel_id());
		assertEquals(null,obj.getAuthnInst());
		assertEquals(null , obj.getSubject());
		assertEquals(null , obj.getCorrelationId());
		assertEquals(null , obj.getClient_id());
		assertEquals(null , obj.getScope());
		assertEquals(null , obj.getTenant_id());
		assertEquals(null , obj.getConsent_expiry());
	}
}
