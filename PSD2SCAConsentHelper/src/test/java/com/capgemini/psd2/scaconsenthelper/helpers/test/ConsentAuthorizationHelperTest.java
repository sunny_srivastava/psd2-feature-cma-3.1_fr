package com.capgemini.psd2.scaconsenthelper.helpers.test;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.aisp.domain.OBAccount6;
import com.capgemini.psd2.aisp.domain.OBAccount6Account;
import com.capgemini.psd2.aisp.domain.OBBranchAndFinancialInstitutionIdentification50;
import com.capgemini.psd2.aisp.domain.OBExternalAccountSubType1Code;
import com.capgemini.psd2.aisp.domain.OBExternalAccountType1Code;
import com.capgemini.psd2.aisp.domain.OBReadAccount6;
import com.capgemini.psd2.aisp.domain.OBReadAccount6Data;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AispConsent;
import com.capgemini.psd2.consent.domain.PSD2Account;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.scaconsenthelper.config.helpers.ConsentAuthorizationHelper;

@RunWith(SpringJUnit4ClassRunner.class)
public class ConsentAuthorizationHelperTest {

	@InjectMocks
	private ConsentAuthorizationHelper helper =  new ConsentAuthorizationHelper() {
		
		@Override
		public PSD2Account populateAccountwithUnmaskedValues(PSD2Account accountwithMask, PSD2Account accountwithoutMask) {
			return accountwithMask;
		}
	};
	
	
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}
	
	
	@Test(expected = PSD2Exception.class)
	public void matchAccountFromConsentAccount_Null_AccountResponse_PSD2Exception() {
		OBReadAccount6 accountGETResponse = null;
		AispConsent aispConsent = new AispConsent();
		helper.matchAccountFromConsentAccount(accountGETResponse, aispConsent);
	}
	
	@Test(expected = PSD2Exception.class)
	public void matchAccountFromConsentAccount_Empty_Account_PSD2Exception() {
		OBReadAccount6 accountGETResponse = new OBReadAccount6().data(new OBReadAccount6Data());
		accountGETResponse.getData().setAccount(new ArrayList<>());
		AispConsent aispConsent = new AispConsent();
		helper.matchAccountFromConsentAccount(accountGETResponse, aispConsent);
	}
	
	@Test(expected = PSD2Exception.class)
	public void matchAccountFromConsentAccount_Null_AccountDetails_PSD2Exception() {
		OBReadAccount6 accountGETResponse = new OBReadAccount6().data(new OBReadAccount6Data());
		accountGETResponse.getData().setAccount(new ArrayList<>());
		accountGETResponse.getData().getAccount().add(new OBAccount6());
		AispConsent aispConsent = new AispConsent();
		helper.matchAccountFromConsentAccount(accountGETResponse, aispConsent);
	}
	
	@Test(expected = PSD2Exception.class)
	public void matchAccountFromConsentAccount_Empty_AccountDetails_PSD2Exception() {
		OBReadAccount6 accountGETResponse = new OBReadAccount6().data(new OBReadAccount6Data());
		accountGETResponse.getData().setAccount(new ArrayList<>());
		accountGETResponse.getData().getAccount().add(new OBAccount6());
		AispConsent aispConsent = new AispConsent();
		aispConsent.setAccountDetails(new ArrayList<>());
		helper.matchAccountFromConsentAccount(accountGETResponse, aispConsent);
	}
	
	@Test
	public void matchAccountFromConsentAccount() {
		OBReadAccount6 accountGETResponse = new OBReadAccount6().data(new OBReadAccount6Data());
		accountGETResponse.getData().setAccount(new ArrayList<>());
		
		List<OBAccount6Account> accountList = new ArrayList<OBAccount6Account>();
		accountList.add(new OBAccount6Account().identification("IE8492184309").schemeName("IBAN").name("dummyName").secondaryIdentification("SIE8492184309"));
		PSD2Account account = new PSD2Account();
		account.setAccountId("dummyAccountId");
		account.setAccountSubType(OBExternalAccountSubType1Code.CURRENTACCOUNT);
		account.setAccountType(OBExternalAccountType1Code.PERSONAL);
		account.setCurrency("EUR");
		account.setDescription("Dummy Description");
		account.setNickname("dummyNickName");
		account.setServicer(new OBBranchAndFinancialInstitutionIdentification50().schemeName("IBAN").identification("IE8492184309"));
		account.setAccount(accountList);
		
		accountGETResponse.getData().getAccount().add(account);
		
		AispConsent aispConsent = new AispConsent();
		aispConsent.setAccountDetails(new ArrayList<>());
		AccountDetails accountDetails = new AccountDetails();
		aispConsent.getAccountDetails().add(accountDetails);

		helper.matchAccountFromConsentAccount(accountGETResponse, aispConsent);
	}
	
	@Test(expected = PSD2Exception.class)
	public void matchAccountFromList() {
		OBReadAccount6 accountGETResponse = null;
		PSD2Account selectedAccount = null;
		helper.matchAccountFromList(accountGETResponse, selectedAccount);
	}
	
	@Test(expected = PSD2Exception.class)
	public void matchAccountFromList_Empty_AccountResponse_Exception() {
		OBReadAccount6 accountGETResponse = new OBReadAccount6().data(new OBReadAccount6Data());
		accountGETResponse.getData().setAccount(new ArrayList<>());
		PSD2Account selectedAccount = null;
		helper.matchAccountFromList(accountGETResponse, selectedAccount);
	}
	
	@Test(expected = PSD2Exception.class)
	public void matchAccountFromList_null_SelectedAccount_Exception() {
		OBReadAccount6 accountGETResponse = new OBReadAccount6().data(new OBReadAccount6Data());
		accountGETResponse.getData().setAccount(new ArrayList<>());
		accountGETResponse.getData().getAccount().add(new OBAccount6());
		PSD2Account selectedAccount = null;
		helper.matchAccountFromList(accountGETResponse, selectedAccount);
	}
	
	@Test(expected = PSD2Exception.class)
	public void matchAccountFromList_emptyHash_SelectedAccount_Exception() {
		OBReadAccount6 accountGETResponse = new OBReadAccount6().data(new OBReadAccount6Data());
		accountGETResponse.getData().setAccount(new ArrayList<>());
		PSD2Account accountInResponse = new PSD2Account();
		accountInResponse.setAccountId("dummyAccountID");
		accountInResponse.addAccountItem(new OBAccount6Account());
		accountGETResponse.getData().getAccount().add(accountInResponse);
		PSD2Account selectedAccount = new PSD2Account();
		selectedAccount.setAccount(new ArrayList<OBAccount6Account>());
		selectedAccount.getAccount().add(new OBAccount6Account());
		
		helper.matchAccountFromList(accountGETResponse, selectedAccount);
	}
	
	@Test(expected = PSD2Exception.class)
	public void matchAccountFromList_mismatch_Account_Exception() {
		OBReadAccount6 accountGETResponse = new OBReadAccount6().data(new OBReadAccount6Data());
		accountGETResponse.getData().setAccount(new ArrayList<>());
		PSD2Account selectedAccount = new PSD2Account();
		selectedAccount.setHashedValue("dummyHashValue");
		PSD2Account accountInResponse = new PSD2Account();
		accountInResponse.setHashedValue("anotherHash");
		accountInResponse.addAccountItem(new OBAccount6Account());
		accountGETResponse.getData().getAccount().add(accountInResponse);
		selectedAccount.setAccount(new ArrayList<OBAccount6Account>());
		selectedAccount.getAccount().add(new OBAccount6Account());
		helper.matchAccountFromList(accountGETResponse, selectedAccount);
	}
	
	@Test
	public void matchAccountFromList_Success() {
		OBReadAccount6 accountGETResponse = new OBReadAccount6().data(new OBReadAccount6Data());
		accountGETResponse.getData().setAccount(new ArrayList<>());
		PSD2Account selectedAccount = new PSD2Account();
		selectedAccount.setHashedValue("dummyHashValue");
		selectedAccount.setAccountId("dummyAccountID");
		PSD2Account accountInResponse = new PSD2Account();
		accountInResponse.setHashedValue("dummyHashValue");
		accountInResponse.setAccountId("dummyAccountID");
		accountInResponse.addAccountItem(new OBAccount6Account());
		accountGETResponse.getData().getAccount().add(accountInResponse);
		selectedAccount.setAccount(new ArrayList<OBAccount6Account>());
		selectedAccount.getAccount().add(new OBAccount6Account());
		OBAccount6 result = helper.matchAccountFromList(accountGETResponse, selectedAccount);
		
		assertEquals("dummyAccountID", result.getAccountId());
		
	}
	
	@Test(expected = PSD2Exception.class)
	public void matchAccountByIdentification_null_accountGETResponse_Exception() {
		OBReadAccount6 accountGETResponse = null;
		PSD2Account selectedAccount = new PSD2Account();
		helper.matchAccountByIdentification(accountGETResponse, selectedAccount);
	}
	
	@Test(expected = PSD2Exception.class)
	public void matchAccountByIdentification_empty_accountGETResponse_Exception() {
		OBReadAccount6 accountGETResponse = new OBReadAccount6().data(new OBReadAccount6Data());
		accountGETResponse.getData().setAccount(new ArrayList<OBAccount6>());
		PSD2Account selectedAccount = new PSD2Account();
		helper.matchAccountByIdentification(accountGETResponse, selectedAccount);
	}
	
	@Test(expected = PSD2Exception.class)
	public void matchAccountByIdentification_null_selected_Exception() {
		OBReadAccount6 accountGETResponse = new OBReadAccount6().data(new OBReadAccount6Data());
		accountGETResponse.getData().setAccount(new ArrayList<>());
		accountGETResponse.getData().getAccount().add(new OBAccount6());
		PSD2Account selectedAccount = null;
		helper.matchAccountByIdentification(accountGETResponse, selectedAccount);
	}
	
	@Test(expected = PSD2Exception.class)
	public void matchAccountByIdentification_mismatch_Account_Exception() {
		OBReadAccount6 accountGETResponse = new OBReadAccount6().data(new OBReadAccount6Data());
		accountGETResponse.getData().setAccount(new ArrayList<>());
		PSD2Account accountInResponse = new PSD2Account();
		accountInResponse.setAccount(new ArrayList<OBAccount6Account>());
		
		accountGETResponse.getData().getAccount().add(accountInResponse);
		
		PSD2Account selectedAccount = new PSD2Account();
		List<OBAccount6Account> accountList = new ArrayList<OBAccount6Account>();
		OBAccount6Account account = new OBAccount6Account();
		account.setIdentification("dummyIdentification");
		accountList.add(account);
		selectedAccount.setAccount(accountList);
		helper.matchAccountByIdentification(accountGETResponse, selectedAccount);
	}
}