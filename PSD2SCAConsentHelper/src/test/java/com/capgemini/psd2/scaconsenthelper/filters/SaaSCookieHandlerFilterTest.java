package com.capgemini.psd2.scaconsenthelper.filters;

import static org.assertj.core.api.Assertions.anyOf;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.parser.ParseException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.logger.LoggerAttribute;
import com.capgemini.psd2.logger.LoggerUtils;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.scaconsenthelper.config.helpers.CookieHelper;
import com.capgemini.psd2.scaconsenthelper.config.helpers.PickUpDataService;
import com.capgemini.psd2.scaconsenthelper.constants.PFConstants;
import com.capgemini.psd2.scaconsenthelper.constants.SCAConsentHelperConstants;
import com.capgemini.psd2.scaconsenthelper.models.IntentTypeEnum;
import com.capgemini.psd2.scaconsenthelper.models.PFInstanceData;
import com.capgemini.psd2.scaconsenthelper.models.PickupDataModel;
import com.capgemini.psd2.scaconsenthelper.models.RawAccessJwtToken;
import com.capgemini.psd2.security.exceptions.PSD2SecurityException;
import com.capgemini.psd2.utilities.JSONUtilities;

@RunWith(SpringJUnit4ClassRunner.class)
public class SaaSCookieHandlerFilterTest {

	@InjectMocks
	private SaaSCookieHandlerFilter saasCookieHandlerFilter;
	
	@Mock
	private HttpServletRequest request;
	
	@Mock
	private HttpServletResponse response;
	
	@Mock
	private CookieHelper cookieHelper;
	
	@Mock
	PickUpDataService pickupDataService;
	
	@Mock
	private LoggerUtils loggerUtils;
	
	private String jweDecryptionKey = "jKey-aaaaaaaaaa-bbbbbbbbbb-cccccccccc";
	
	private RequestHeaderAttributes requestHeaderAttributes = new RequestHeaderAttributes();
	
	@Mock
	private static Logger LOG;
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		when(request.getRequestURI()).thenReturn("/home");
		when(request.getQueryString()).thenReturn("key=value");
		when(request.getMethod()).thenReturn("POST");
		when(request.getParameter(PFConstants.REF)).thenReturn("7gzCfK7zsYelDLhIVjWgHB");
		LOG = LoggerFactory.getLogger(SaaSCookieHandlerFilter.class);
		
		ReflectionTestUtils.setField(saasCookieHandlerFilter, "requestHeaderAttributes", requestHeaderAttributes);
		ReflectionTestUtils.setField(saasCookieHandlerFilter, "jweDecryptionKey", jweDecryptionKey);
		
		when(loggerUtils.populateLoggerData("doFilterInternal")).thenReturn(new LoggerAttribute());
		
	}

	@Test
	public void validateRefreshBackScenarios() throws ParseException{
		
		PickupDataModel intentData = new PickupDataModel();
		intentData.setBypassConsentPage(true);
		intentData.setChannelId("WEB");
		intentData.setClientId("clientID");
		intentData.setConsentExpiryInMinutes(20l);
		intentData.setCorrelationId("12365dja-d912-sd12-23sh-12365djad912");
		intentData.setIntentId("12365dja-d912-sd12-23sh-12365djad912");
		intentData.setIntentTypeEnum(IntentTypeEnum.AISP_INTENT_TYPE);
		intentData.setOriginatorRefId("orig-reference-id");
		intentData.setPaymentType("domestic");
		intentData.setScope("AISP");
		intentData.setTenant_id("TenantID");
		intentData.setUserId("userid");
		
		Cookie cookie = new Cookie(SCAConsentHelperConstants.CUSTOM_SESSION_CONTROLL_COOKIE, null);
		cookie.setSecure(Boolean.TRUE);
		cookie.setHttpOnly(Boolean.TRUE);
		cookie.setValue("eyJlbmMiOiJBMTI4R0NNIiwiYWxnIjoiZGlyIn0..H1jhrwqWiqWMgZ5T.ODKHDavqyRzH9-gKehpFgAAWBRwOJ-IlCxxbAyFc5tHCDQUdUldZP26PVNWkb9d3wgKdrCxtXw2dR1gIlNSA-FvFOo13mCYZxi5WBLAipkclXaTsxaMIWLbPjbBwmS0dcH1QZ52qmTQaZ_sYjDuimwoTGJQpOC9WWbfBmwc98YcXCvJnZZpYqx44AP6lOWFiZQO3viOmiLt_x50APLF6vXoXRzs6e0wUsnvxF7lU5Le6bK7A7RM-_5P-UT2o0LyQ87at7vD3k2Fi5p4wDzq3Zm_POAZlarH1St8Kww6b-TlapWDwDNFWj3OkC5BI9VbT_-W83Z04musRuP6eY1rIb82MWDZCFxrRiDkpyuTaWYMxARw8kCGTYghOS4A2i31mYa_QpPTg4RPI4z_0mq_C7W0rM5fSocNuXsMQvBpQVMSrdnBQ5TIRBch6gc6ZuNiroxyLdi-2khKlAOPxisFDJaJ1Hda9h7y9WqIG9HCtxdvN3cNsD5Dk1_dvCQcA32P91VOHsJ6vCkOz6K2k5zEs6gFpfGeQVPNvaX5QRhRQfTDcbd6nm4nkvoZp4LHVoUQFC_8Ib7sbGtYJEbCeFeY5OyVEhrn3YHdp44o--KKNkjpOLDP9wCgzfRDxLZR-C5h-uKkljAxgSrhhLN0LrsaEjgiB_ex99WMxqQmyGXUBNtbWrE5VqGIcZ1NrGg0w0v7KEgGUdor2fBygbzBzTTCSzlo83-MVs9zpWaYSqXmbWpT-zDO6jm1W53wvvO6Qnjgd3w4VVdIjJVi9qvMdn3w59hWfqu-jwNS32MQyz7m7RWyQZxBS2dRshr2bg4Zy9vplrN9FEEJPAvRDQdy-kJRoY9AGMDlm7_J3m2mvcDsr144b8Roi3X9p1EoGJLuvqcX3Vbrmrqz1lezCWklsPU_d5CpC_-toO3j-Fjq_3ZuFgPFdV9X8dPUcs4nLQXw7Ljicdr88gm-7F1_GOMl21-zQ6Vi_chbXtqryHGJBQdnqsUzdN3Aq_mJJ8WCOZ8fcd0Lcvu8I9aRX1cTwAyuA_XOAGxPayAkkhw04jiP3A1e3Cu2UKV6UwPKB87gtyJgP6DNa2D7q6TL5pHAMAOMe3y82RF-RR_DnpkIYrAYLUtfstHcva51wke9VO8hSEA8YGbiAJEaO4ieJ6UhK4vq75bL9Q0yc71M5JuUOcfGAww.kRkUMBiTxK3KumJmkRdWkQ");
		cookie.setMaxAge(60);
		
		PFInstanceData pfInstanceData = new PFInstanceData();
		pfInstanceData.setPfInstanceId("test string");
		pfInstanceData.setPfInstanceUserName("testuser");
		pfInstanceData.setPfInstanceUserPwd("testpwd");
		
		when(pickupDataService.pickupIntent(any(), any())).thenReturn(intentData);
		when(cookieHelper.createNewSessionCookie(intentData)).thenReturn(cookie);

		try {
			saasCookieHandlerFilter.validateRefreshBackScenarios(cookie, pfInstanceData, request, response);
		} catch (Exception e) {
			e.printStackTrace();
		} 
	}
	
	@Test
	public void validateRefreshBackScenarios1() throws ParseException{
		
		PickupDataModel intentData = new PickupDataModel();
		intentData.setBypassConsentPage(true);
		intentData.setChannelId("WEB");
		intentData.setClientId("clientID");
		intentData.setConsentExpiryInMinutes(20l);
		intentData.setCorrelationId("12365dja-d912-sd12-23sh-12365djad912");
		intentData.setIntentId("12365dja-d912-sd12-23sh-12365djad912");
		intentData.setIntentTypeEnum(IntentTypeEnum.AISP_INTENT_TYPE);
		intentData.setOriginatorRefId("7gzCfK7zsYelDLhIVjWgHB");
		intentData.setPaymentType("domestic");
		intentData.setScope("AISP");
		intentData.setTenant_id("TenantID");
		intentData.setUserId("userid");
		
		Cookie cookie = new Cookie("SessionControllCookie", "eyJhbGciOiJSU0EtT0FFUC0yNTYiLCJlbmMiOiJBMjU2R0NNIn0.lZ2nqCeiPzsPmJShsrDD3uA55-06A649CMtwOyuY9nNzMtUGyzV-G8qc4w4ui1uWrtzypBs5Eyq4GfjnTtVHbcDVkS1HVc3tfxNAPY8dfjVrWNz59HyKt4bCjBdqqhBOdZezLtWB9aoWIwZoHLf4D8aUcVUtDsFELVcScmiQNtzHwvpDHZb4oxRfPl-OuOTkKA23C8lnnDMO1KUy8ZXHD4p0jQKAcaV877gYm8NbHDwOBEf-ItWJOGx2jV60apWd0hKqwfFR2QKD9wmGgXpbFZ08ro7X2fj8rTgKWhDgoBT_JVZdVFhVI4T4RLRDrCJqkyeciXhLm7W_xNhWBXAMrA.94SuB596ZLuUtw53wrofwN5jZXfT5f-ZarJhQc9Mj0M.0Ow5DXfilYX3ty49H4lNMNPljlWAFASc49zljhRSIIUSlmUHLZo0SAezn-n_FdxexAIYLk_FtRgnkMHDEyxJ1V1yHhqa1Jvdb36lTYyptqCJhMkOV1XGn58L4Z9QQmdrIZnn5iHxZ9-N1Jfjs0eoKiLBgR9O7ZEcs7QrWZVT6n_HrGrIloYQu_lFgmk5O7k47_15CVXaFqIohpHXETejoHEwjQj-iTToNRaHWNFAKvlpUBz4mUgk9RSIQCxK1GxxS8wxP44w5G4HdOIjFNwTsRDXeSZy0mU9zTNUCmDEUT9MFESfmVU1nPurdT-VoiPvVklbJZW8Sas0hWgqQkdQdP35nFY1sjCgfMB9iYUeEU-TCE219wkm1XXrLJwLEYZclL_4ckl4zExo2wb3Czwd8f5iO9fBQQWZ4mdwThK4VtZaPs1JEkxwGLI0SHA8Jr-e2PsDrkGEnxs74FsJ5MKluU2ZKvKcGXyQPaaTRa0ecJLD5-YYBuTtxOnU3gM_5aZm97pd_wiPk_h81r5aiwjSfRF3Ihxp37KNPfNOMJoA9xe2F51m1AvmjrOUgSM156LwmFyJFebVfarb9NPtJ_q1wU891sCu2Vmv520BR4QfIc-ayIwTVxLgZSN-BP7PhEJb_x8.XhZpINBxRdFFEgwPTcAgJg");
		//Cookie cookie = new Cookie(SCAConsentHelperConstants.CUSTOM_SESSION_CONTROLL_COOKIE, null);
		cookie.setSecure(Boolean.TRUE);
		cookie.setHttpOnly(Boolean.TRUE);
		cookie.setMaxAge(60);
		
		PFInstanceData pfInstanceData = new PFInstanceData();
		pfInstanceData.setPfInstanceId("test string");
		pfInstanceData.setPfInstanceUserName("testuser");
		pfInstanceData.setPfInstanceUserPwd("testpwd");
		
		RawAccessJwtToken token = mock(RawAccessJwtToken.class);
		//JSONUtilities json = mock(JSONUtilities.class);
		when(token.parseAndReturnClaims(any())).thenReturn("123");
	//	when(json.getObjectFromJSONString(any(), PickupDataModel.class)).thenReturn(intentData);	
		when(pickupDataService.pickupIntent(any(), any())).thenReturn(intentData);
		when(cookieHelper.createNewSessionCookie(intentData)).thenReturn(cookie);

		try {
			saasCookieHandlerFilter.validateRefreshBackScenarios(cookie, pfInstanceData, request, response);
		} catch (Exception e) {
			e.printStackTrace();
		} 
	}
	
	@Test(expected=PSD2SecurityException.class)
	public void validateRefreshBackScenariosException() throws ParseException, PSD2SecurityException, IOException, ServletException {
		
		PickupDataModel intentData = new PickupDataModel();
		intentData.setBypassConsentPage(true);
		intentData.setChannelId("WEB");
		intentData.setClientId("clientID");
		intentData.setConsentExpiryInMinutes(20l);
		intentData.setCorrelationId("12365dja-d912-sd12-23sh-12365djad912");
		intentData.setIntentId("12365dja-d912-sd12-23sh-12365djad912");
		intentData.setIntentTypeEnum(IntentTypeEnum.AISP_INTENT_TYPE);
		intentData.setOriginatorRefId("orig-reference-id");
		intentData.setPaymentType("domestic");
		intentData.setScope("AISP");
		intentData.setTenant_id("TenantID");
		intentData.setUserId("userid");
		
		Cookie cookie = new Cookie(SCAConsentHelperConstants.CUSTOM_SESSION_CONTROLL_COOKIE, null);
		cookie.setSecure(Boolean.TRUE);
		cookie.setHttpOnly(Boolean.TRUE);
		cookie.setValue("eyJlbmMiOiJBMTI4R0NNIiwiYWxnIjoiZGlyIn0..H1jhrwqWiqWMgZ5T.ODKHDavqyRzH9-gKehpFgAAWBRwOJ-IlCxxbAyFc5tHCDQUdUldZP26PVNWkb9d3wgKdrCxtXw2dR1gIlNSA-FvFOo13mCYZxi5WBLAipkclXaTsxaMIWLbPjbBwmS0dcH1QZ52qmTQaZ_sYjDuimwoTGJQpOC9WWbfBmwc98YcXCvJnZZpYqx44AP6lOWFiZQO3viOmiLt_x50APLF6vXoXRzs6e0wUsnvxF7lU5Le6bK7A7RM-_5P-UT2o0LyQ87at7vD3k2Fi5p4wDzq3Zm_POAZlarH1St8Kww6b-TlapWDwDNFWj3OkC5BI9VbT_-W83Z04musRuP6eY1rIb82MWDZCFxrRiDkpyuTaWYMxARw8kCGTYghOS4A2i31mYa_QpPTg4RPI4z_0mq_C7W0rM5fSocNuXsMQvBpQVMSrdnBQ5TIRBch6gc6ZuNiroxyLdi-2khKlAOPxisFDJaJ1Hda9h7y9WqIG9HCtxdvN3cNsD5Dk1_dvCQcA32P91VOHsJ6vCkOz6K2k5zEs6gFpfGeQVPNvaX5QRhRQfTDcbd6nm4nkvoZp4LHVoUQFC_8Ib7sbGtYJEbCeFeY5OyVEhrn3YHdp44o--KKNkjpOLDP9wCgzfRDxLZR-C5h-uKkljAxgSrhhLN0LrsaEjgiB_ex99WMxqQmyGXUBNtbWrE5VqGIcZ1NrGg0w0v7KEgGUdor2fBygbzBzTTCSzlo83-MVs9zpWaYSqXmbWpT-zDO6jm1W53wvvO6Qnjgd3w4VVdIjJVi9qvMdn3w59hWfqu-jwNS32MQyz7m7RWyQZxBS2dRshr2bg4Zy9vplrN9FEEJPAvRDQdy-kJRoY9AGMDlm7_J3m2mvcDsr144b8Roi3X9p1EoGJLuvqcX3Vbrmrqz1lezCWklsPU_d5CpC_-toO3j-Fjq_3ZuFgPFdV9X8dPUcs4nLQXw7Ljicdr88gm-7F1_GOMl21-zQ6Vi_chbXtqryHGJBQdnqsUzdN3Aq_mJJ8WCOZ8fcd0Lcvu8I9aRX1cTwAyuA_XOAGxPayAkkhw04jiP3A1e3Cu2UKV6UwPKB87gtyJgP6DNa2D7q6TL5pHAMAOMe3y82RF-RR_DnpkIYrAYLUtfstHcva51wke9VO8hSEA8YGbiAJEaO4ieJ6UhK4vq75bL9Q0yc71M5JuUOcfGAww.kRkUMBiTxK3KumJmkRdWkQ");
		cookie.setMaxAge(60);
		
		PFInstanceData pfInstanceData = new PFInstanceData();
		pfInstanceData.setPfInstanceId("test string");
		pfInstanceData.setPfInstanceUserName("testuser");
		pfInstanceData.setPfInstanceUserPwd("testpwd");
		
		when(pickupDataService.pickupIntent(any(), any())).thenReturn(null);
		when(cookieHelper.createNewSessionCookie(intentData)).thenReturn(cookie);

			saasCookieHandlerFilter.validateRefreshBackScenarios(null, pfInstanceData, request, response);
	}
	
	@Test
	public void validateRefreshBackScenarios_IntentIdNull() throws ParseException{
	
		PFInstanceData pfInstanceData = new PFInstanceData();
		pfInstanceData.setPfInstanceId("test string");
		pfInstanceData.setPfInstanceUserName("testuser");
		pfInstanceData.setPfInstanceUserPwd("testpwd");
		
		when(pickupDataService.pickupIntent(any(), any())).thenReturn(null);
		when(cookieHelper.createNewSessionCookie(any())).thenReturn(null);

		try {
			saasCookieHandlerFilter.validateRefreshBackScenarios(null, pfInstanceData, request, response);
		} catch (Exception e) {
			e.printStackTrace();
		} 
	}
	
	@Test
	public void validateRefreshBackScenariosException_IntentData() throws ParseException, PSD2SecurityException, IOException, ServletException {
		
		PickupDataModel intentData = new PickupDataModel();
		intentData.setBypassConsentPage(true);
		intentData.setChannelId("WEB");
		intentData.setClientId("clientID");
		intentData.setConsentExpiryInMinutes(20l);
		intentData.setCorrelationId("12365dja-d912-sd12-23sh-12365djad912");
		intentData.setIntentId("12365dja-d912-sd12-23sh-12365djad912");
		intentData.setIntentTypeEnum(IntentTypeEnum.AISP_INTENT_TYPE);
		intentData.setOriginatorRefId("orig-reference-id");
		intentData.setPaymentType("domestic");
		intentData.setScope("AISP");
		intentData.setTenant_id("TenantID");
		intentData.setUserId("userid");
		
		Cookie cookie = new Cookie(SCAConsentHelperConstants.CUSTOM_SESSION_CONTROLL_COOKIE, null);
		cookie.setSecure(Boolean.TRUE);
		cookie.setHttpOnly(Boolean.TRUE);
		cookie.setValue("eyJlbmMiOiJBMTI4R0NNIiwiYWxnIjoiZGlyIn0..H1jhrwqWiqWMgZ5T.ODKHDavqyRzH9-gKehpFgAAWBRwOJ-IlCxxbAyFc5tHCDQUdUldZP26PVNWkb9d3wgKdrCxtXw2dR1gIlNSA-FvFOo13mCYZxi5WBLAipkclXaTsxaMIWLbPjbBwmS0dcH1QZ52qmTQaZ_sYjDuimwoTGJQpOC9WWbfBmwc98YcXCvJnZZpYqx44AP6lOWFiZQO3viOmiLt_x50APLF6vXoXRzs6e0wUsnvxF7lU5Le6bK7A7RM-_5P-UT2o0LyQ87at7vD3k2Fi5p4wDzq3Zm_POAZlarH1St8Kww6b-TlapWDwDNFWj3OkC5BI9VbT_-W83Z04musRuP6eY1rIb82MWDZCFxrRiDkpyuTaWYMxARw8kCGTYghOS4A2i31mYa_QpPTg4RPI4z_0mq_C7W0rM5fSocNuXsMQvBpQVMSrdnBQ5TIRBch6gc6ZuNiroxyLdi-2khKlAOPxisFDJaJ1Hda9h7y9WqIG9HCtxdvN3cNsD5Dk1_dvCQcA32P91VOHsJ6vCkOz6K2k5zEs6gFpfGeQVPNvaX5QRhRQfTDcbd6nm4nkvoZp4LHVoUQFC_8Ib7sbGtYJEbCeFeY5OyVEhrn3YHdp44o--KKNkjpOLDP9wCgzfRDxLZR-C5h-uKkljAxgSrhhLN0LrsaEjgiB_ex99WMxqQmyGXUBNtbWrE5VqGIcZ1NrGg0w0v7KEgGUdor2fBygbzBzTTCSzlo83-MVs9zpWaYSqXmbWpT-zDO6jm1W53wvvO6Qnjgd3w4VVdIjJVi9qvMdn3w59hWfqu-jwNS32MQyz7m7RWyQZxBS2dRshr2bg4Zy9vplrN9FEEJPAvRDQdy-kJRoY9AGMDlm7_J3m2mvcDsr144b8Roi3X9p1EoGJLuvqcX3Vbrmrqz1lezCWklsPU_d5CpC_-toO3j-Fjq_3ZuFgPFdV9X8dPUcs4nLQXw7Ljicdr88gm-7F1_GOMl21-zQ6Vi_chbXtqryHGJBQdnqsUzdN3Aq_mJJ8WCOZ8fcd0Lcvu8I9aRX1cTwAyuA_XOAGxPayAkkhw04jiP3A1e3Cu2UKV6UwPKB87gtyJgP6DNa2D7q6TL5pHAMAOMe3y82RF-RR_DnpkIYrAYLUtfstHcva51wke9VO8hSEA8YGbiAJEaO4ieJ6UhK4vq75bL9Q0yc71M5JuUOcfGAww.kRkUMBiTxK3KumJmkRdWkQ");
		cookie.setMaxAge(60);
		
		PFInstanceData pfInstanceData = new PFInstanceData();
		pfInstanceData.setPfInstanceId("test string");
		pfInstanceData.setPfInstanceUserName("testuser");
		pfInstanceData.setPfInstanceUserPwd("testpwd");
		
		when(pickupDataService.pickupIntent(any(), any())).thenReturn(intentData);
		when(cookieHelper.createNewSessionCookie(intentData)).thenReturn(cookie);

		saasCookieHandlerFilter.validateRefreshBackScenarios(null, pfInstanceData, request, response);
	}

	@Test(expected=PSD2SecurityException.class)
	public void validateRefreshBackScenariosException_IntentDataCookie() throws ParseException, PSD2SecurityException, IOException, ServletException {
		
		PickupDataModel intentData = new PickupDataModel();
		intentData.setBypassConsentPage(true);
		intentData.setChannelId("WEB");
		intentData.setClientId("clientID");
		intentData.setConsentExpiryInMinutes(20l);
		intentData.setCorrelationId("12365dja-d912-sd12-23sh-12365djad912");
		intentData.setIntentId("12365dja-d912-sd12-23sh-12365djad912");
		intentData.setIntentTypeEnum(IntentTypeEnum.AISP_INTENT_TYPE);
		intentData.setOriginatorRefId("orig-reference-id");
		intentData.setPaymentType("domestic");
		intentData.setScope("AISP");
		intentData.setTenant_id("TenantID");
		intentData.setUserId("userid");
		
		Cookie cookie = new Cookie(SCAConsentHelperConstants.CUSTOM_SESSION_CONTROLL_COOKIE, null);
		cookie.setSecure(Boolean.TRUE);
		cookie.setHttpOnly(Boolean.TRUE);
		cookie.setValue("eyJlbmMiOiJBMTI4R0NNIiwiYWxnIjoiZGlyIn0..H1jhrwqWiqWMgZ5T.ODKHDavqyRzH9-gKehpFgAAWBRwOJ-IlCxxbAyFc5tHCDQUdUldZP26PVNWkb9d3wgKdrCxtXw2dR1gIlNSA-FvFOo13mCYZxi5WBLAipkclXaTsxaMIWLbPjbBwmS0dcH1QZ52qmTQaZ_sYjDuimwoTGJQpOC9WWbfBmwc98YcXCvJnZZpYqx44AP6lOWFiZQO3viOmiLt_x50APLF6vXoXRzs6e0wUsnvxF7lU5Le6bK7A7RM-_5P-UT2o0LyQ87at7vD3k2Fi5p4wDzq3Zm_POAZlarH1St8Kww6b-TlapWDwDNFWj3OkC5BI9VbT_-W83Z04musRuP6eY1rIb82MWDZCFxrRiDkpyuTaWYMxARw8kCGTYghOS4A2i31mYa_QpPTg4RPI4z_0mq_C7W0rM5fSocNuXsMQvBpQVMSrdnBQ5TIRBch6gc6ZuNiroxyLdi-2khKlAOPxisFDJaJ1Hda9h7y9WqIG9HCtxdvN3cNsD5Dk1_dvCQcA32P91VOHsJ6vCkOz6K2k5zEs6gFpfGeQVPNvaX5QRhRQfTDcbd6nm4nkvoZp4LHVoUQFC_8Ib7sbGtYJEbCeFeY5OyVEhrn3YHdp44o--KKNkjpOLDP9wCgzfRDxLZR-C5h-uKkljAxgSrhhLN0LrsaEjgiB_ex99WMxqQmyGXUBNtbWrE5VqGIcZ1NrGg0w0v7KEgGUdor2fBygbzBzTTCSzlo83-MVs9zpWaYSqXmbWpT-zDO6jm1W53wvvO6Qnjgd3w4VVdIjJVi9qvMdn3w59hWfqu-jwNS32MQyz7m7RWyQZxBS2dRshr2bg4Zy9vplrN9FEEJPAvRDQdy-kJRoY9AGMDlm7_J3m2mvcDsr144b8Roi3X9p1EoGJLuvqcX3Vbrmrqz1lezCWklsPU_d5CpC_-toO3j-Fjq_3ZuFgPFdV9X8dPUcs4nLQXw7Ljicdr88gm-7F1_GOMl21-zQ6Vi_chbXtqryHGJBQdnqsUzdN3Aq_mJJ8WCOZ8fcd0Lcvu8I9aRX1cTwAyuA_XOAGxPayAkkhw04jiP3A1e3Cu2UKV6UwPKB87gtyJgP6DNa2D7q6TL5pHAMAOMe3y82RF-RR_DnpkIYrAYLUtfstHcva51wke9VO8hSEA8YGbiAJEaO4ieJ6UhK4vq75bL9Q0yc71M5JuUOcfGAww.kRkUMBiTxK3KumJmkRdWkQ");
		cookie.setMaxAge(60);
		
		PFInstanceData pfInstanceData = new PFInstanceData();
		pfInstanceData.setPfInstanceId("test string");
		pfInstanceData.setPfInstanceUserName("testuser");
		pfInstanceData.setPfInstanceUserPwd("testpwd");
		
		when(pickupDataService.pickupIntent(any(), any())).thenReturn(intentData);
		when(cookieHelper.createNewSessionCookie(intentData)).thenReturn(cookie);

		saasCookieHandlerFilter.validateRefreshBackScenarios(cookie, pfInstanceData, request, response);
	}

	@Test
	public void populateSecurityHeadersTest() throws ParseException{
		
		saasCookieHandlerFilter.populateSecurityHeaders(response);
	}

}
