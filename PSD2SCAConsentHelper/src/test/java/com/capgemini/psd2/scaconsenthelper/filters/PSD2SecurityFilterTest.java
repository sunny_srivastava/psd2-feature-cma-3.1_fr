package com.capgemini.psd2.scaconsenthelper.filters;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
 
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.logger.LoggerUtils;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.scaconsenthelper.filters.PSD2SecurityFilter;
import com.capgemini.psd2.scaconsenthelper.models.SecurityRequestAttributes;

@RunWith(SpringJUnit4ClassRunner.class)
public class PSD2SecurityFilterTest {

	@InjectMocks
	private PSD2SecurityFilter psd2SecurityFilter;

	@Mock
	private PSD2SecurityFilter filter;

	@Mock
	private HttpServletRequest request;
	@Mock
	private HttpServletResponse response;

	@Mock
	private FilterChain filterChain;

	@Mock
	private SecurityRequestAttributes securityRequestAttributes;

	@Mock
	private RequestHeaderAttributes requestHeaderAttributes;

	@Mock
	private LoggerUtils loggerUtils;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		psd2SecurityFilter = new PSD2SecurityFilter();
		ReflectionTestUtils.setField(psd2SecurityFilter, "securityRequestAttributes", securityRequestAttributes);
		ReflectionTestUtils.setField(psd2SecurityFilter, "requestHeaderAttributes", requestHeaderAttributes);
		ReflectionTestUtils.setField(psd2SecurityFilter, "loggerUtils", loggerUtils);
	}

	@Test
	public void test() throws ServletException, IOException {
		String oauthUrl = "CFW4KXAQPUG2X5AWzi8Uccp2tLMx3dhC4a/S0P2keKhlVjtCrBGqEQU0s2ZCk/JX/f0CQAr1QMdnZHJoOrFR8BlkVLR+EowwWN+mD6/rF5dHcp7MLSeKuAIPyxxLbGDJCSnxcqEQ/0nd/T98s2XgyyQyVXgV/t9XjjKVC9YdSkTDNZs0XGS4+FtYGLGAaEFhfbHk/HEDSw2DORJDkV6opg7sjQF5NTuq/mt9Af/yYk9BdeOsz70Hqyx5/2/tg3sM4wYafrCV84OgJ94T4eTNVvOk0zarp3LJzgJBCb1DwXYSDkiktY8dcMuhna62mqnIZNDBj2vWyqWlTgaPNee7B1kwz6j6KCEy8WSosI4Q+g7Kjn812CUTgJc0fF/1SOofpO8v6g5Cfn44wAc/5vDioSO2gaW3s18+OeeISOK6F3D3YiE7i6LmmCdQG4m0v4I2Nqos6q3czmIk8O44J5ajz7zD94cdDVz+rHr0EpPbtbb8vrTxYkml2QTaGCh8O6cVSmelUYoPSejz0HT/s5HLRFCaVuj4SHrcYeo5oCLLqEM8LJY+UmuiJb7oKpN0Kz5D8dNa+ZDlzudff39g4lSyWV9JDh7hBTNYyUuoblI12HMsNpuGgQrHTmGPfLxx3KnBzBZ6lpuPtB4dPjDZHQamIIY3uozFbRKBQ4VkhAfZpG33AxwO3Ogr3CBbB0b43w5o9pbf54P0tkXzVRiXMnUulO/6w+4UhGT4C6nD2fIzWXKhmTNJsTB0AsS8nxeUurdSPy7m2MDl4P6FBL+9XoGVG3N7RLvHVdFntA6hsOdBGNhx17q5dLHElf7yICO85bB8TQIknAOADJE9WdCsfXinIfiT2vDfymFDx8Api06u9mMAESSaPKpXpp74RlUgFQXLXdhd0eUE/GikpxWdRlyySdrcojLLbRdWPX4nC/snxnmVEizfVrNSzBET/UiBOT+WtOeFoHm0jGikHA9rPTzdHQYMAMi6Idth2pJ2guBoKCe3dEUqjsd6DZYQ1zmdbKTOBTpcdysKS3flvILLHhiTlhPcD6cFIFGCAr7CU18rYMtpg6UGQIJFfA1ceOslhuYE19PJ78ngChJWoAQGmiQP1w==#!/aisp-account";
		when(request.getParameter(anyString())).thenReturn(oauthUrl);
		when(request.getServletPath()).thenReturn("/SaaS");
		RequestDispatcher requestDispatcher = new RequestDispatcher() {

			@Override
			public void include(ServletRequest request, ServletResponse response) throws ServletException, IOException {
			}

			@Override
			public void forward(ServletRequest request, ServletResponse response) throws ServletException, IOException {
			}
		};
		when(request.getRequestDispatcher("/errors")).thenReturn(requestDispatcher); 
		psd2SecurityFilter.doFilter(request, response, filterChain);
	}

	@Test
	public void test_correlation_id_generated() throws ServletException, IOException {
		when(request.getServletPath()).thenReturn("/SaaS");
		RequestDispatcher requestDispatcher = new RequestDispatcher() {

			@Override
			public void include(ServletRequest request, ServletResponse response) throws ServletException, IOException {
			}

			@Override
			public void forward(ServletRequest request, ServletResponse response) throws ServletException, IOException {
			}
		};
		when(request.getRequestDispatcher("/errors")).thenReturn(requestDispatcher); 
		psd2SecurityFilter.doFilter(request, response, filterChain);
	}

	@Test(expected=Exception.class)
	public void test_exception() throws ServletException, IOException {
		when(request.getServletPath()).thenReturn("/SaaS");
		when(request.getHeader("DN")).thenReturn("ba4f73f89a60425baed82a7ef2509fea");
		RequestDispatcher requestDispatcher = new RequestDispatcher() {

			@Override
			public void include(ServletRequest request, ServletResponse response) throws ServletException, IOException {
			}

			@Override
			public void forward(ServletRequest request, ServletResponse response) throws ServletException, IOException {
			}
		};
		when(request.getRequestDispatcher(anyString())).thenReturn(requestDispatcher);
		ReflectionTestUtils.setField(psd2SecurityFilter, "requestHeaderAttributes", null);

		psd2SecurityFilter.doFilter(request, response, filterChain);
	}

	@Test
	public void testServletPathMatchExclusionList() throws ServletException, IOException {
		String oauthUrl = "CFW4KXAQPUG2X5AWzi8Uccp2tLMx3dhC4a/S0P2keKhlVjtCrBGqEQU0s2ZCk/JX/f0CQAr1QMdnZHJoOrFR8BlkVLR+EowwWN+mD6/rF5dHcp7MLSeKuAIPyxxLbGDJCSnxcqEQ/0nd/T98s2XgyyQyVXgV/t9XjjKVC9YdSkTDNZs0XGS4+FtYGLGAaEFhfbHk/HEDSw2DORJDkV6opg7sjQF5NTuq/mt9Af/yYk9BdeOsz70Hqyx5/2/tg3sM4wYafrCV84OgJ94T4eTNVvOk0zarp3LJzgJBCb1DwXYSDkiktY8dcMuhna62mqnIZNDBj2vWyqWlTgaPNee7B1kwz6j6KCEy8WSosI4Q+g7Kjn812CUTgJc0fF/1SOofpO8v6g5Cfn44wAc/5vDioSO2gaW3s18+OeeISOK6F3D3YiE7i6LmmCdQG4m0v4I2Nqos6q3czmIk8O44J5ajz7zD94cdDVz+rHr0EpPbtbb8vrTxYkml2QTaGCh8O6cVSmelUYoPSejz0HT/s5HLRFCaVuj4SHrcYeo5oCLLqEM8LJY+UmuiJb7oKpN0Kz5D8dNa+ZDlzudff39g4lSyWV9JDh7hBTNYyUuoblI12HMsNpuGgQrHTmGPfLxx3KnBzBZ6lpuPtB4dPjDZHQamIIY3uozFbRKBQ4VkhAfZpG33AxwO3Ogr3CBbB0b43w5o9pbf54P0tkXzVRiXMnUulO/6w+4UhGT4C6nD2fIzWXKhmTNJsTB0AsS8nxeUurdSPy7m2MDl4P6FBL+9XoGVG3N7RLvHVdFntA6hsOdBGNhx17q5dLHElf7yICO85bB8TQIknAOADJE9WdCsfXinIfiT2vDfymFDx8Api06u9mMAESSaPKpXpp74RlUgFQXLXdhd0eUE/GikpxWdRlyySdrcojLLbRdWPX4nC/snxnmVEizfVrNSzBET/UiBOT+WtOeFoHm0jGikHA9rPTzdHQYMAMi6Idth2pJ2guBoKCe3dEUqjsd6DZYQ1zmdbKTOBTpcdysKS3flvILLHhiTlhPcD6cFIFGCAr7CU18rYMtpg6UGQIJFfA1ceOslhuYE19PJ78ngChJWoAQGmiQP1w==#!/aisp-account";
		when(request.getParameter(anyString())).thenReturn(oauthUrl);
		when(request.getServletPath()).thenReturn("/**/build/**");

		psd2SecurityFilter.doFilter(request, response, filterChain);

	}

}
