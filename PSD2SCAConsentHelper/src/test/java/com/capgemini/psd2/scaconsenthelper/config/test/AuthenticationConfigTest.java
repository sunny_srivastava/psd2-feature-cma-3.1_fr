package com.capgemini.psd2.scaconsenthelper.config.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.HashMap;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.scaconsenthelper.config.AuthenticationConfig;
import com.capgemini.psd2.scaconsenthelper.config.CdnConfig;

@RunWith(SpringJUnit4ClassRunner.class)
public class AuthenticationConfigTest {
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void authenticationConfig() {
		AuthenticationConfig config = new AuthenticationConfig();
		config.setAspspRedirectUrl("dummySetAspspRedirectUrl");
		config.setAuthenticationUrl(new HashMap<String,String>());
		config.setAuthenticationUrlMap(new HashMap<String,String>());
		config.setJweDecryptionKey("dummyJweDecryptionKey");
		config.setTokenExpirationTime(12);
		config.setTokenIssuer("dummyTokenIssuer");
		config.setTokenSigningKey("dummyTokenSigningKey");
		
		int i =12;
		assertEquals("dummySetAspspRedirectUrl", config.getAspspRedirectUrl());
		assertEquals(new HashMap<String,String>(), config.getAuthenticationUrl());
		assertEquals(new HashMap<String,String>(), config.getAuthenticationUrlMap());
		assertEquals("dummyJweDecryptionKey", config.getJweDecryptionKey());
		assertEquals(Integer.valueOf(i), config.getTokenExpirationTime());
		assertEquals("dummyTokenIssuer",config.getTokenIssuer());
		assertEquals("dummyTokenSigningKey",config.getTokenSigningKey());
		assertEquals(null , config.getTenantSpecificAutheticationUrl("tenantId"));
	}
}
