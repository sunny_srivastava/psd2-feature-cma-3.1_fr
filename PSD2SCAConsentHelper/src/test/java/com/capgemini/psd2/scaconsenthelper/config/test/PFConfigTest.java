package com.capgemini.psd2.scaconsenthelper.config.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.HashMap;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.MockitoAnnotations;

import com.capgemini.psd2.scaconsenthelper.config.PFConfig;

public class PFConfigTest {

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}
	
	
	@Test
	public void PFConfig_Null() {
		PFConfig config = new PFConfig();
		assertNull(config.getAispinstanceId()) ;
	}
	
	@Test
	public void PFConfig_Populated() {
		PFConfig config = new PFConfig();
		config.setAispinstanceId("dummyInstanceId");
		config.setAispinstancepassword("dummyPassword");
		config.setAispinstanceusername("dummyUserName");
		config.setCispinstanceId("cispInstanceId");
		config.setCispinstanceusername("cispInstanceUsername");
		config.setDropOffURLMap(new HashMap<String, String>());
		config.setGrantsrevocationapipwd("apipwd");
		config.setGrantsrevocationapiuser("apiuser");
		config.setGrantsrevocationURL(new HashMap<>());
		config.setGrantsrevocationURLMap(new HashMap<>());
		config.setPickUpURLMap(new HashMap<>());
		config.setPispinstanceId("pispInstanceId");
		config.setResumePathBaseURLMap(new HashMap<>());
		config.setPispinstanceusername("pispInstanceUsername");
		config.setScainstanceId("scaInstanceId");
		config.setScainstancepassword("scapwd");
		config.setScainstanceusername("scaUsername");

		assertEquals("dummyInstanceId", config.getAispinstanceId());
		assertEquals("dummyPassword", config.getAispinstancepassword());
		assertEquals("dummyUserName", config.getAispinstanceusername());
		assertEquals("cispInstanceId", config.getCispinstanceId());
		assertEquals("cispInstanceUsername", config.getCispinstanceusername());
		assertTrue(config.getDropOffURLMap().isEmpty());
		assertEquals("apipwd", config.getGrantsrevocationapipwd());
		assertEquals("apiuser", config.getGrantsrevocationapiuser());
		assertTrue(config.getGrantsrevocationURLMap().isEmpty());
		assertTrue(config.getPickUpURLMap().isEmpty());
		assertTrue(config.getResumePathBaseURLMap().isEmpty());
		assertEquals("pispInstanceId", config.getPispinstanceId() );
		assertEquals("pispInstanceUsername", config.getPispinstanceusername());
		assertEquals("scaInstanceId", config.getScainstanceId() );
		assertEquals("scaUsername", config.getScainstanceusername() );
		assertEquals(null, config.getTenantSpecificDropOffUrl("1"));
		assertEquals(null, config.getTenantSpecificPickUpUrl("12"));
		assertEquals(null, config.getTenantSpecificGrantsRevocationUrl("123"));
		assertEquals(null, config.getTenantSpecificResumePathBaseUrl("121"));
	}
	
	@After
	public void tearDown() {
		
	}
}
