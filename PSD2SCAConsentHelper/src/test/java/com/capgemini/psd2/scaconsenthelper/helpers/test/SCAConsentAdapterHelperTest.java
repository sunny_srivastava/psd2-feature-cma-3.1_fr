package com.capgemini.psd2.scaconsenthelper.helpers.test;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.aisp.adapter.AccountRequestAdapter;
import com.capgemini.psd2.aisp.domain.OBReadConsentResponse1;
import com.capgemini.psd2.aisp.domain.OBReadConsentResponse1Data;
import com.capgemini.psd2.cisp.adapter.FundsConfirmationConsentAdapter;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmationConsentResponse1;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmationConsentResponse1Data;
import com.capgemini.psd2.exceptions.ErrorMapKeys;
import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.scaconsenthelper.config.helpers.SCAConsentAdapterHelper;

@RunWith(SpringJUnit4ClassRunner.class)
public class SCAConsentAdapterHelperTest {

	@Mock
	private AccountRequestAdapter accountRequestAdapter;

	@Mock
	private FundsConfirmationConsentAdapter fundsConfirmationsConsentAdapter;

	@InjectMocks
	private SCAConsentAdapterHelper scaConsentAdapterHelper;

	@Rule
	public ExpectedException exceptionRule = ExpectedException.none();

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void getFundsConfirmationSetupDataTest() {
		OBFundsConfirmationConsentResponse1 fundsConfirmationSetUpResponse = new OBFundsConfirmationConsentResponse1();
		OBFundsConfirmationConsentResponse1Data data = new OBFundsConfirmationConsentResponse1Data();
		data.setStatus(OBFundsConfirmationConsentResponse1Data.StatusEnum.AWAITINGAUTHORISATION);
		fundsConfirmationSetUpResponse.setData(data);
		when(fundsConfirmationsConsentAdapter.getFundsConfirmationConsentPOSTResponse(anyString())).thenReturn(fundsConfirmationSetUpResponse);
		assertEquals(fundsConfirmationSetUpResponse, scaConsentAdapterHelper.getFundsConfirmationSetupData("intentId"));
	}

	@Test
	public void getFundsConfirmationSetupDataTestWhenNoDataFound() {
		PSD2Exception ex = PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_RESOURCE_NOTFOUND, ErrorMapKeys.RESOURCE_NOT_FOUND));
		when(fundsConfirmationsConsentAdapter.getFundsConfirmationConsentPOSTResponse(anyString())).thenThrow(ex);
		exceptionRule.expect(PSD2Exception.class);
		exceptionRule.expectMessage("No Funds Confirmation Consent data found");
		scaConsentAdapterHelper.getFundsConfirmationSetupData("intentId");
	}
	
	@Test
	public void updateFundsConfirmationSetupDataTest() {
		OBFundsConfirmationConsentResponse1 fundsConfirmationSetUpResponse = new OBFundsConfirmationConsentResponse1();
		when(fundsConfirmationsConsentAdapter.updateFundsConfirmationConsentResponse(anyString(), any())).thenReturn(fundsConfirmationSetUpResponse);
		assertEquals(fundsConfirmationSetUpResponse, scaConsentAdapterHelper.updateFundsConfirmationSetupData("intentId",
				OBFundsConfirmationConsentResponse1Data.StatusEnum.REJECTED));
	}

	@Test
	public void updateFundsConfirmationSetupDataTestWhenNoDataFound() {
		PSD2Exception ex = PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_RESOURCE_NOTFOUND, ErrorMapKeys.RESOURCE_NOT_FOUND));
		when(fundsConfirmationsConsentAdapter.updateFundsConfirmationConsentResponse(anyString(), any())).thenThrow(ex);
		exceptionRule.expect(PSD2Exception.class);
		exceptionRule.expectMessage("No Funds Confirmation Consent data found");
		scaConsentAdapterHelper.updateFundsConfirmationSetupData("intentId",OBFundsConfirmationConsentResponse1Data.StatusEnum.REJECTED);
	}
	
	@Test
	public void getAccountRequestSetupDataTest() {
		OBReadConsentResponse1 accountSetupResponse = new OBReadConsentResponse1();
		OBReadConsentResponse1Data data = new OBReadConsentResponse1Data();
		data.setStatus(OBReadConsentResponse1Data.StatusEnum.AWAITINGAUTHORISATION);
		accountSetupResponse.setData(data);
		when(accountRequestAdapter.getAccountRequestGETResponse(anyString())).thenReturn(accountSetupResponse);
		assertEquals(accountSetupResponse, scaConsentAdapterHelper.getAccountRequestSetupData("intentId"));
	}

	@Test
	public void getAccountRequestSetupDataTestWhenNoDataFound() {
		PSD2Exception ex = PSD2Exception.populatePSD2Exception(new ExceptionDTO(
				OBErrorCodeEnum.UK_OBIE_RESOURCE_NOTFOUND, ErrorMapKeys.NO_ACCOUNT_REQUEST_DATA_FOUND));
		when(accountRequestAdapter.getAccountRequestGETResponse(anyString())).thenThrow(ex);
		exceptionRule.expect(PSD2Exception.class);
		exceptionRule.expectMessage("No account request data found");
		scaConsentAdapterHelper.getAccountRequestSetupData("intentId");
	}

	@Test
	public void updateAccountRequestSetupDataTest() {
		OBReadConsentResponse1 accountSetupResponse = new OBReadConsentResponse1();
		when(accountRequestAdapter.updateAccountRequestResponse(anyString(), any())).thenReturn(accountSetupResponse);
		assertEquals(accountSetupResponse, scaConsentAdapterHelper.updateAccountRequestSetupData("intentId",
				OBReadConsentResponse1Data.StatusEnum.REJECTED));
	}

	@Test
	public void updateAccountRequestSetupDataTestWhenNoDataFound() {
		PSD2Exception ex = PSD2Exception.populatePSD2Exception(new ExceptionDTO(
				OBErrorCodeEnum.UK_OBIE_RESOURCE_NOTFOUND, ErrorMapKeys.NO_ACCOUNT_REQUEST_DATA_FOUND));
		when(accountRequestAdapter.updateAccountRequestResponse(anyString(), any())).thenThrow(ex);
		exceptionRule.expect(PSD2Exception.class);
		exceptionRule.expectMessage("No account request data found");
		scaConsentAdapterHelper.updateAccountRequestSetupData("intentId",OBReadConsentResponse1Data.StatusEnum.REJECTED);
	}
}
