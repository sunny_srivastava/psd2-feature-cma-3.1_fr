package com.capgemini.psd2.scaconsenthelper.helpers.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.when;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpHeaders;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.scaconsenthelper.config.helpers.SCAConsentHelper;
import com.capgemini.psd2.scaconsenthelper.constants.SCAConsentHelperConstants;
import com.capgemini.psd2.scaconsenthelper.models.PickupDataModel;
import com.capgemini.psd2.security.constants.PSD2SecurityConstants;

@RunWith(SpringJUnit4ClassRunner.class)
public class SCAConsentHelperTest {

	
	@Mock
	private HttpServletRequest request;
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void privateConstructorTest() throws NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException{
		Constructor<SCAConsentHelper> c = SCAConsentHelper.class.getDeclaredConstructor();
		c.setAccessible(true);
		SCAConsentHelper u = c.newInstance();
	}
	
	@Test
	public void populatePickupDataModel_Null() {
		request.setAttribute(SCAConsentHelperConstants.INTENT_DATA, new PickupDataModel());
		PickupDataModel pickupDataModel = SCAConsentHelper.populatePickupDataModel(request);
		assertNull(pickupDataModel);
	}
	
	@Test
	public void populatePickupDataModel() {
		
		PickupDataModel model = new PickupDataModel();
		model.setIntentId("dummyIntentId");
		when(request.getAttribute(SCAConsentHelperConstants.INTENT_DATA)).thenReturn(model);
		
		PickupDataModel pickupDataModel = SCAConsentHelper.populatePickupDataModel(request);
		assertEquals(model.getIntentId(), pickupDataModel.getIntentId());
	}
	
	@Test
	public void invalidateCookie() {
		HttpServletResponse response = new MockHttpServletResponse();
		SCAConsentHelper.invalidateCookie(response);
	}
	

	@Test
	public void populateSecurityHeaders() {
		HttpServletResponse response = new MockHttpServletResponse();
		SCAConsentHelper.populateSecurityHeaders(response);
		assertEquals(PSD2SecurityConstants.X_CONTENT_TYPE_OPTIONS_VALUE, response.getHeader(PSD2SecurityConstants.X_CONTENT_TYPE_OPTIONS_HEADER));
	}
	
	@Test
	public void populateCookieHeaders() {
		String cookie = "name:value";
		HttpHeaders requestHeaders = SCAConsentHelper.populateCookieHeaders(cookie);
		assertEquals(SCAConsentHelperConstants.CUSTOM_SESSION_CONTROLL_COOKIE + PSD2Constants.EQUAS
				+ cookie, requestHeaders.get(SCAConsentHelperConstants.COOKIE).get(0));
	}
}
