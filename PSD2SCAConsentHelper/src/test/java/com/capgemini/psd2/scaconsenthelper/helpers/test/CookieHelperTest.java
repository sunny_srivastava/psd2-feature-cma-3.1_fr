package com.capgemini.psd2.scaconsenthelper.helpers.test;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.scaconsenthelper.config.helpers.CookieHelper;
import com.capgemini.psd2.scaconsenthelper.config.helpers.JwtTokenUtility;
import com.capgemini.psd2.scaconsenthelper.constants.SCAConsentHelperConstants;
import com.capgemini.psd2.scaconsenthelper.models.AccessJwtToken;
import com.capgemini.psd2.scaconsenthelper.models.IntentTypeEnum;
import com.capgemini.psd2.scaconsenthelper.models.JwtToken;
import com.capgemini.psd2.scaconsenthelper.models.PickupDataModel;
import com.capgemini.psd2.utilities.JSONUtilities;

@RunWith(SpringJUnit4ClassRunner.class)
public class CookieHelperTest {

	@InjectMocks
	private CookieHelper cookieHelper;
	
	@Mock
	private RequestHeaderAttributes requestHeaderAttributes;
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}

	@After
	public void tearDown() throws Exception {
	}
	
	
	@Test
	public void createNewSessionCookieTest(){
		PickupDataModel intentData = new PickupDataModel();
		intentData.setBypassConsentPage(true);
		intentData.setChannelId("WEB");
		intentData.setClientId("clientID");
		intentData.setConsentExpiryInMinutes(20l);
		intentData.setCorrelationId("correlationId");
		intentData.setIntentId("fsadf-fdasfsd");
		intentData.setIntentTypeEnum(IntentTypeEnum.AISP_INTENT_TYPE);
		intentData.setOriginatorRefId("orig-reference-id");
		intentData.setPaymentType("domestic");
		intentData.setScope("AISP");
		intentData.setTenant_id("TenantID");
		intentData.setUserId("userid");

		//when(JwtTokenUtility.createSessionJwtToken("http://SaaS.com", 60, "abcdefgh12345678abcdefgh12345678abcdefgh12345678abcdefgh12345678", intentDataJson, "87654321abcdefgh")).thenReturn((AccessJwtToken) jwtToken);
		ReflectionTestUtils.setField(cookieHelper, "tokenIssuer", "http://SaaS.com");
		ReflectionTestUtils.setField(cookieHelper, "sessiontimeout", 60);
		ReflectionTestUtils.setField(cookieHelper, "jweDecryptionKey", "87654321abcdefgh");
		ReflectionTestUtils.setField(cookieHelper, "tokenSigningKey", "abcdefgh12345678abcdefgh12345678abcdefgh12345678abcdefgh12345678");
		ReflectionTestUtils.setField(cookieHelper, "expiry", 60);
			
		cookieHelper.createNewSessionCookie(intentData);
	}
	
	@Test(expected=PSD2Exception.class)
	public void setBypassConsentCookieTest(){
		PickupDataModel intentData = new PickupDataModel();
		intentData.setBypassConsentPage(true);
		intentData.setChannelId("WEB");
		intentData.setClientId("clientID");
		intentData.setConsentExpiryInMinutes(20l);
		intentData.setCorrelationId("correlationId");
		intentData.setIntentId("fsadf-fdasfsd");
		intentData.setIntentTypeEnum(IntentTypeEnum.AISP_INTENT_TYPE);
		intentData.setOriginatorRefId("orig-reference-id");
		intentData.setPaymentType("domestic");
		intentData.setScope("AISP");
		intentData.setTenant_id("TenantID");
		intentData.setUserId("userid");
		
		RequestHeaderAttributes req = new RequestHeaderAttributes();
		req.setChannelId("abc");
		req.setIdempotencyKey("FRESH12");
		req.setCorrelationId("12345");
		HttpServletRequest request = mock(HttpServletRequest.class);
		
		Cookie cookie1 = new Cookie("SessionControllCookie", "eyJhbGciOiJSU0EtT0FFUC0yNTYiLCJlbmMiOiJBMjU2R0NNIn0.lZ2nqCeiPzsPmJShsrDD3uA55-06A649CMtwOyuY9nNzMtUGyzV-G8qc4w4ui1uWrtzypBs5Eyq4GfjnTtVHbcDVkS1HVc3tfxNAPY8dfjVrWNz59HyKt4bCjBdqqhBOdZezLtWB9aoWIwZoHLf4D8aUcVUtDsFELVcScmiQNtzHwvpDHZb4oxRfPl-OuOTkKA23C8lnnDMO1KUy8ZXHD4p0jQKAcaV877gYm8NbHDwOBEf-ItWJOGx2jV60apWd0hKqwfFR2QKD9wmGgXpbFZ08ro7X2fj8rTgKWhDgoBT_JVZdVFhVI4T4RLRDrCJqkyeciXhLm7W_xNhWBXAMrA.94SuB596ZLuUtw53wrofwN5jZXfT5f-ZarJhQc9Mj0M.0Ow5DXfilYX3ty49H4lNMNPljlWAFASc49zljhRSIIUSlmUHLZo0SAezn-n_FdxexAIYLk_FtRgnkMHDEyxJ1V1yHhqa1Jvdb36lTYyptqCJhMkOV1XGn58L4Z9QQmdrIZnn5iHxZ9-N1Jfjs0eoKiLBgR9O7ZEcs7QrWZVT6n_HrGrIloYQu_lFgmk5O7k47_15CVXaFqIohpHXETejoHEwjQj-iTToNRaHWNFAKvlpUBz4mUgk9RSIQCxK1GxxS8wxP44w5G4HdOIjFNwTsRDXeSZy0mU9zTNUCmDEUT9MFESfmVU1nPurdT-VoiPvVklbJZW8Sas0hWgqQkdQdP35nFY1sjCgfMB9iYUeEU-TCE219wkm1XXrLJwLEYZclL_4ckl4zExo2wb3Czwd8f5iO9fBQQWZ4mdwThK4VtZaPs1JEkxwGLI0SHA8Jr-e2PsDrkGEnxs74FsJ5MKluU2ZKvKcGXyQPaaTRa0ecJLD5-YYBuTtxOnU3gM_5aZm97pd_wiPk_h81r5aiwjSfRF3Ihxp37KNPfNOMJoA9xe2F51m1AvmjrOUgSM156LwmFyJFebVfarb9NPtJ_q1wU891sCu2Vmv520BR4QfIc-ayIwTVxLgZSN-BP7PhEJb_x8.XhZpINBxRdFFEgwPTcAgJg");
	Cookie cookie2 = new Cookie("SessionControllCookie","88888888");
		Cookie[] cookies = new Cookie[5];
		cookies[0] = cookie1;
		cookies[1] = cookie2;
		
		Mockito.when(request.getCookies()).thenReturn(cookies);

		ReflectionTestUtils.setField(cookieHelper, "tokenIssuer", "http://SaaS.com");
		ReflectionTestUtils.setField(cookieHelper, "sessiontimeout", 60);
		ReflectionTestUtils.setField(cookieHelper, "jweDecryptionKey", "87654321abcdefgh");
		ReflectionTestUtils.setField(cookieHelper, "tokenSigningKey", "abcdefgh12345678abcdefgh12345678abcdefgh12345678abcdefgh12345678");
		ReflectionTestUtils.setField(cookieHelper, "expiry", 60);
		
		
		cookieHelper.setBypassConsentCookie(request);
	}
	
	@Test(expected=PSD2Exception.class)
	public void setBypassConsentCookieTest2(){
		PickupDataModel intentData = new PickupDataModel();
		intentData.setBypassConsentPage(true);
		intentData.setChannelId("WEB");
		intentData.setClientId("clientID");
		intentData.setConsentExpiryInMinutes(20l);
		intentData.setCorrelationId("correlationId");
		intentData.setIntentId("fsadf-fdasfsd");
		intentData.setIntentTypeEnum(IntentTypeEnum.AISP_INTENT_TYPE);
		intentData.setOriginatorRefId("orig-reference-id");
		intentData.setPaymentType("domestic");
		intentData.setScope("AISP");
		intentData.setTenant_id("TenantID");
		intentData.setUserId("userid");
		
		RequestHeaderAttributes req = new RequestHeaderAttributes();
		req.setChannelId("abc");
		req.setIdempotencyKey("FRESH12");
		req.setCorrelationId("12345");
		HttpServletRequest request = mock(HttpServletRequest.class);
		
		Cookie cookie1 = new Cookie("SessionControllCookie", "eyJhbGciOiJSU0EtT0FFUC0yNTYiLCJlbmMiOiJBMjU2R0NNIn0.lZ2nqCeiPzsPmJShsrDD3uA55-06A649CMtwOyuY9nNzMtUGyzV-G8qc4w4ui1uWrtzypBs5Eyq4GfjnTtVHbcDVkS1HVc3tfxNAPY8dfjVrWNz59HyKt4bCjBdqqhBOdZezLtWB9aoWIwZoHLf4D8aUcVUtDsFELVcScmiQNtzHwvpDHZb4oxRfPl-OuOTkKA23C8lnnDMO1KUy8ZXHD4p0jQKAcaV877gYm8NbHDwOBEf-ItWJOGx2jV60apWd0hKqwfFR2QKD9wmGgXpbFZ08ro7X2fj8rTgKWhDgoBT_JVZdVFhVI4T4RLRDrCJqkyeciXhLm7W_xNhWBXAMrA.94SuB596ZLuUtw53wrofwN5jZXfT5f-ZarJhQc9Mj0M.0Ow5DXfilYX3ty49H4lNMNPljlWAFASc49zljhRSIIUSlmUHLZo0SAezn-n_FdxexAIYLk_FtRgnkMHDEyxJ1V1yHhqa1Jvdb36lTYyptqCJhMkOV1XGn58L4Z9QQmdrIZnn5iHxZ9-N1Jfjs0eoKiLBgR9O7ZEcs7QrWZVT6n_HrGrIloYQu_lFgmk5O7k47_15CVXaFqIohpHXETejoHEwjQj-iTToNRaHWNFAKvlpUBz4mUgk9RSIQCxK1GxxS8wxP44w5G4HdOIjFNwTsRDXeSZy0mU9zTNUCmDEUT9MFESfmVU1nPurdT-VoiPvVklbJZW8Sas0hWgqQkdQdP35nFY1sjCgfMB9iYUeEU-TCE219wkm1XXrLJwLEYZclL_4ckl4zExo2wb3Czwd8f5iO9fBQQWZ4mdwThK4VtZaPs1JEkxwGLI0SHA8Jr-e2PsDrkGEnxs74FsJ5MKluU2ZKvKcGXyQPaaTRa0ecJLD5-YYBuTtxOnU3gM_5aZm97pd_wiPk_h81r5aiwjSfRF3Ihxp37KNPfNOMJoA9xe2F51m1AvmjrOUgSM156LwmFyJFebVfarb9NPtJ_q1wU891sCu2Vmv520BR4QfIc-ayIwTVxLgZSN-BP7PhEJb_x8.XhZpINBxRdFFEgwPTcAgJg");
	Cookie cookie2 = new Cookie(SCAConsentHelperConstants.INTENT_DATA,"88888888");
		Cookie[] cookies = new Cookie[5];
		cookies[0] = cookie1;
		cookies[1] = cookie2;
		
		Mockito.when(request.getCookies()).thenReturn(cookies);

		ReflectionTestUtils.setField(cookieHelper, "tokenIssuer", "http://SaaS.com");
		ReflectionTestUtils.setField(cookieHelper, "sessiontimeout", 60);
		ReflectionTestUtils.setField(cookieHelper, "jweDecryptionKey", "87654321abcdefgh");
		ReflectionTestUtils.setField(cookieHelper, "tokenSigningKey", "abcdefgh12345678abcdefgh12345678abcdefgh12345678abcdefgh12345678");
		ReflectionTestUtils.setField(cookieHelper, "expiry", 60);
		
		
		cookieHelper.setBypassConsentCookie(request);
	}
	
	@Test
	public void getCustomSessionCookieTest(){
		
		RequestHeaderAttributes req = new RequestHeaderAttributes();
		req.setChannelId("abc");
		req.setIdempotencyKey("FRESH12");
		req.setCorrelationId("12345");
		HttpServletRequest request = mock(HttpServletRequest.class);
		
		Cookie cookie1 = new Cookie("SessionControllCookie", "eyJhbGciOiJSU0EtT0FFUC0yNTYiLCJlbmMiOiJBMjU2R0NNIn0.lZ2nqCeiPzsPmJShsrDD3uA55-06A649CMtwOyuY9nNzMtUGyzV-G8qc4w4ui1uWrtzypBs5Eyq4GfjnTtVHbcDVkS1HVc3tfxNAPY8dfjVrWNz59HyKt4bCjBdqqhBOdZezLtWB9aoWIwZoHLf4D8aUcVUtDsFELVcScmiQNtzHwvpDHZb4oxRfPl-OuOTkKA23C8lnnDMO1KUy8ZXHD4p0jQKAcaV877gYm8NbHDwOBEf-ItWJOGx2jV60apWd0hKqwfFR2QKD9wmGgXpbFZ08ro7X2fj8rTgKWhDgoBT_JVZdVFhVI4T4RLRDrCJqkyeciXhLm7W_xNhWBXAMrA.94SuB596ZLuUtw53wrofwN5jZXfT5f-ZarJhQc9Mj0M.0Ow5DXfilYX3ty49H4lNMNPljlWAFASc49zljhRSIIUSlmUHLZo0SAezn-n_FdxexAIYLk_FtRgnkMHDEyxJ1V1yHhqa1Jvdb36lTYyptqCJhMkOV1XGn58L4Z9QQmdrIZnn5iHxZ9-N1Jfjs0eoKiLBgR9O7ZEcs7QrWZVT6n_HrGrIloYQu_lFgmk5O7k47_15CVXaFqIohpHXETejoHEwjQj-iTToNRaHWNFAKvlpUBz4mUgk9RSIQCxK1GxxS8wxP44w5G4HdOIjFNwTsRDXeSZy0mU9zTNUCmDEUT9MFESfmVU1nPurdT-VoiPvVklbJZW8Sas0hWgqQkdQdP35nFY1sjCgfMB9iYUeEU-TCE219wkm1XXrLJwLEYZclL_4ckl4zExo2wb3Czwd8f5iO9fBQQWZ4mdwThK4VtZaPs1JEkxwGLI0SHA8Jr-e2PsDrkGEnxs74FsJ5MKluU2ZKvKcGXyQPaaTRa0ecJLD5-YYBuTtxOnU3gM_5aZm97pd_wiPk_h81r5aiwjSfRF3Ihxp37KNPfNOMJoA9xe2F51m1AvmjrOUgSM156LwmFyJFebVfarb9NPtJ_q1wU891sCu2Vmv520BR4QfIc-ayIwTVxLgZSN-BP7PhEJb_x8.XhZpINBxRdFFEgwPTcAgJg");
	Cookie cookie2 = new Cookie(SCAConsentHelperConstants.CUSTOM_SESSION_CONTROLL_COOKIE,"88888888");
		Cookie[] cookies = new Cookie[5];
		cookies[0] = cookie1;
		cookies[1] = cookie2;
		
		Mockito.when(request.getCookies()).thenReturn(cookies);
		
		Cookie customSessionControllCookie = null;
		customSessionControllCookie = cookie2;
		customSessionControllCookie.setHttpOnly(true);
		customSessionControllCookie.setSecure(true);
		customSessionControllCookie.setMaxAge(0);
		
		assertEquals(customSessionControllCookie.getComment(), cookieHelper.getCustomSessionCookie(cookies).getComment());
	}
	
	
	@Test
	public void getCustomSessionCookieTest_Null(){
		
		RequestHeaderAttributes req = new RequestHeaderAttributes();
		req.setChannelId("abc");
		req.setIdempotencyKey("FRESH12");
		req.setCorrelationId("12345");
		HttpServletRequest request = mock(HttpServletRequest.class);
		
		Cookie[] cookies = null;
		
		Mockito.when(request.getCookies()).thenReturn(cookies);
		
		Cookie customSessionControllCookie = null;
		
		assertEquals(customSessionControllCookie, cookieHelper.getCustomSessionCookie(cookies));
	}
	

}
