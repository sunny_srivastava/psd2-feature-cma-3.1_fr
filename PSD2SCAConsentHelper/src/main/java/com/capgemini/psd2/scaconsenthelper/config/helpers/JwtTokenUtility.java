package com.capgemini.psd2.scaconsenthelper.config.helpers;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.UUID;

import org.joda.time.DateTime;

import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.scaconsenthelper.constants.OIDCConstants;
import com.capgemini.psd2.scaconsenthelper.constants.PFConstants;
import com.capgemini.psd2.scaconsenthelper.constants.SCAConsentHelperConstants;
import com.capgemini.psd2.scaconsenthelper.models.AccessJwtToken;
import com.capgemini.psd2.scaconsenthelper.models.DropOffRequest;
import com.capgemini.psd2.security.exceptions.PSD2SecurityException;
import com.capgemini.psd2.security.exceptions.SCAConsentErrorCodeEnum;
import com.capgemini.psd2.utilities.JSONUtilities;
import com.nimbusds.jose.EncryptionMethod;
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JWEAlgorithm;
import com.nimbusds.jose.JWEHeader;
import com.nimbusds.jose.JWEObject;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.JWSHeader;
import com.nimbusds.jose.JWSSigner;
import com.nimbusds.jose.KeyLengthException;
import com.nimbusds.jose.Payload;
import com.nimbusds.jose.crypto.DirectDecrypter;
import com.nimbusds.jose.crypto.DirectEncrypter;
import com.nimbusds.jose.crypto.MACSigner;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;


public final class JwtTokenUtility {

	private JwtTokenUtility() {

	}

	public static AccessJwtToken createSessionJwtToken(String tokenIssuer, Integer tokenExpirationTime,
			String tokenSigningKey,String intentData,String keyForEncryption)  {
		String jweString = createJwtToken(tokenIssuer, tokenExpirationTime, tokenSigningKey, intentData,
				keyForEncryption);
		return new AccessJwtToken(jweString);
	}

	// USed for Creating Session Cookie Token
	public static String createJwtToken(String tokenIssuer, Integer tokenExpirationTime, String tokenSigningKey,
			String intentData, String keyForEncryption) {
		String jweString = null;
		try{
			DateTime currentTime = new DateTime();
			JWTClaimsSet jwtClaims = new JWTClaimsSet.Builder().issuer(tokenIssuer).issueTime(currentTime.toDate())
					.notBeforeTime(currentTime.toDate())
					.expirationTime(currentTime.plusMinutes(tokenExpirationTime).toDate())
					.claim(SCAConsentHelperConstants.INTENT_DATA, intentData).jwtID(UUID.randomUUID().toString())
					.build();
			jweString = jweString(tokenSigningKey, keyForEncryption, jwtClaims);
		} catch (JOSEException je) {
			throw PSD2SecurityException.populatePSD2SecurityException("Invalid Cookie ..",
					SCAConsentErrorCodeEnum.INVALID_REQUEST);
		}
		return jweString;
	}

	// Used to create token for Authentication Application
	public static String createJwtToken(String tokenIssuer, Integer tokenExpirationTime, String tokenSigningKey,
			String keyForEncryption, Map<String, String> claimsSet) {
		String jweString = null;
		try {
			DateTime currentTime = new DateTime();
			JWTClaimsSet jwtClaims = new JWTClaimsSet.Builder().issuer(tokenIssuer).issueTime(currentTime.toDate())
					.notBeforeTime(currentTime.toDate())
					.expirationTime(currentTime.plusMinutes(tokenExpirationTime).toDate())
					.claim(SCAConsentHelperConstants.SCA_INTENT_DATA,claimsSet.get(SCAConsentHelperConstants.INTENT_DATA))
					.claim(SCAConsentHelperConstants.USER_ID,
							(claimsSet.get(SCAConsentHelperConstants.USER_ID) != null ? claimsSet.get(SCAConsentHelperConstants.USER_ID): ""))
					.claim(SCAConsentHelperConstants.TENANT_ID,
							(claimsSet.get(SCAConsentHelperConstants.TENANT_ID) != null ? claimsSet.get(SCAConsentHelperConstants.TENANT_ID): ""))
					.claim(SCAConsentHelperConstants.LOCALE, "").jwtID(UUID.randomUUID().toString()).build();
			
			jweString = jweString(tokenSigningKey, keyForEncryption, jwtClaims);
		} catch (JOSEException je) {
			throw PSD2SecurityException.populatePSD2SecurityException("Invalid Cookie ..",
					SCAConsentErrorCodeEnum.INVALID_REQUEST);
		}
		return jweString;
	}

	private static String jweString(String tokenSigningKey, String keyForEncryption, JWTClaimsSet jwtClaims)
			throws KeyLengthException, JOSEException {
		JWEObject jweObject;
		String jweString;
		// Sign Jwt Token
			JWSSigner signer = new MACSigner(tokenSigningKey);
			SignedJWT signedJWT = new SignedJWT(new JWSHeader(JWSAlgorithm.HS512), jwtClaims);
			signedJWT.sign(signer);
			signedJWT.getParsedString();
			
			//Encrpty JWT token
			JWEHeader header = new JWEHeader(JWEAlgorithm.DIR, EncryptionMethod.A128GCM);
			byte[] key = keyForEncryption.getBytes();
			Payload payload = new Payload(signedJWT);

			jweObject = new JWEObject(header, payload);
			jweObject.encrypt(new DirectEncrypter(key));
			jweString = jweObject.serialize();			
		return jweString;
		}

	public static DropOffRequest parseAndReturnClaims(String token, String decryptionKey) {
		try {
			JWEObject jweObject = JWEObject.parse(token);
			// Decrypt Jwt Token
			byte[] key = decryptionKey.getBytes();
			jweObject.decrypt(new DirectDecrypter(key));
			JWTClaimsSet claimsSet = jweObject.getPayload().toSignedJWT().getJWTClaimsSet();
			Map<?, ?> payloadMap = claimsSet.getClaims();
			payloadMap.entrySet().stream().forEach(System.out::println);
			DropOffRequest dropOffClaims = populateDropOffData(payloadMap);
			return dropOffClaims;
		} catch (Exception ex) {
			throw PSD2Exception.populatePSD2Exception("Token is not in correct format", ErrorCodeEnum.TECHNICAL_ERROR);
		}
	}

	private static DropOffRequest populateDropOffData(Map<?, ?> claimsMap) {
		DropOffRequest dropOffRequest = new DropOffRequest();
		String intentData = null;
		if (!claimsMap.isEmpty()) {
			intentData = (String) claimsMap.get(SCAConsentHelperConstants.SCA_INTENT_DATA);
			Map<?, ?> valueMap = JSONUtilities.getObjectFromJSONString(intentData, Map.class);
			valueMap.entrySet().stream().forEach(System.out::println);
			if (!valueMap.isEmpty()) {
				dropOffRequest.setChannel_id((String) valueMap.get(PSD2Constants.CHANNEL_NAME));
				dropOffRequest.setClient_id((String) valueMap.get(PSD2Constants.CLIENT_ID));
				dropOffRequest.setConsent_expiry((((Integer) valueMap.get(SCAConsentHelperConstants.ConsentExpiryInMinutes)) != null? ((Integer) valueMap.get(SCAConsentHelperConstants.ConsentExpiryInMinutes)).toString(): null));
				dropOffRequest.setCorrelationId((String) valueMap.get(PSD2Constants.CO_RELATION_ID));
				dropOffRequest.setIntent_type((String) valueMap.get(SCAConsentHelperConstants.INTENT_TYPE_ENUM));
				dropOffRequest.setIntentId((String) valueMap.get(PSD2Constants.INTENT_ID));
				dropOffRequest.setResumePath((String) valueMap.get(PFConstants.RESUME_PATH));
				dropOffRequest.setScope((String) valueMap.get(OIDCConstants.SCOPE));
				dropOffRequest.setBypassConsentPage((Boolean) valueMap.get(SCAConsentHelperConstants.BYPASSCONSENTPAGE));
			}
			dropOffRequest.setUsername((String) claimsMap.get(SCAConsentHelperConstants.USER_ID));
			dropOffRequest.setTenant_id((String) claimsMap.get(SCAConsentHelperConstants.TENANT_ID));
			// dropOffRequest.setAuthnInst(((Long)
			// claimsMap.get(SCAConsentHelperConstants.AUTHENTICATION_TIME)).toString());
			SimpleDateFormat sdf = new SimpleDateFormat(PFConstants.DATE_FORMAT);
			String currentDate = sdf.format(new Date());
			dropOffRequest.setAuthnInst(currentDate);
		}
		return dropOffRequest;
	}

}
