package com.capgemini.psd2.scaconsenthelper.config.helpers;

import org.json.simple.parser.ParseException;

import com.capgemini.psd2.scaconsenthelper.models.PFInstanceData;
import com.capgemini.psd2.scaconsenthelper.models.PickupDataModel;

@FunctionalInterface
public interface PickUpDataService {
	
	public PickupDataModel pickupIntent(String refId,PFInstanceData pfInstanceData) throws ParseException;

}
