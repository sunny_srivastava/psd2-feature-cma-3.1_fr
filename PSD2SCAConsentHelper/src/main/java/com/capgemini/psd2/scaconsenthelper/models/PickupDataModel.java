package com.capgemini.psd2.scaconsenthelper.models;

public class PickupDataModel {

	private String intentId;
	private IntentTypeEnum intentTypeEnum;
	private String scope;
	private String userId;
	private String channelId;
	private String clientId;
	private String correlationId;
	private String originatorRefId;
	private String paymentType;
	private Long consentExpiryInMinutes;
	private String tenant_id; //NOSONAR
	private boolean bypassConsentPage;
	private String resumePath;
	public String getResumePath() {
		return resumePath;
	}

	public void setResumePath(String resumePath) {
		this.resumePath = resumePath;
	}

	public String getTenant_id() {
		return tenant_id;
	}

	public void setTenant_id(String tenant_id) { //NOSONAR
		this.tenant_id = tenant_id;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public String getIntentId() {
		return intentId;
	}

	public void setIntentId(String intentId) {
		this.intentId = intentId;
	}

	public IntentTypeEnum getIntentTypeEnum() {
		return intentTypeEnum;
	}

	public void setIntentTypeEnum(IntentTypeEnum intentTypeEnum) {
		this.intentTypeEnum = intentTypeEnum;
	}

	public String getScope() {
		return scope;
	}

	public void setScope(String scope) {
		this.scope = scope;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getChannelId() {
		return channelId;
	}

	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getCorrelationId() {
		return correlationId;
	}

	public void setCorrelationId(String correlationId) {
		this.correlationId = correlationId;
	}

	public String getOriginatorRefId() {
		return originatorRefId;
	}

	public void setOriginatorRefId(String originatorRefId) {
		this.originatorRefId = originatorRefId;
	}

	public Long getConsentExpiryInMinutes() {
		return consentExpiryInMinutes;
	}

	public void setConsentExpiryInMinutes(Long consentExpiryInMinutes) {
		this.consentExpiryInMinutes = consentExpiryInMinutes;
	}

	public boolean isBypassConsentPage() {
		return bypassConsentPage;
	}

	public void setBypassConsentPage(boolean bypassConsentPage) {
		this.bypassConsentPage = bypassConsentPage;
	}
	
	
}