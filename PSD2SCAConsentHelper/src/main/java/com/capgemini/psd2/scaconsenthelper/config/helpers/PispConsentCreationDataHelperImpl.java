package com.capgemini.psd2.scaconsenthelper.config.helpers;

import java.time.OffsetDateTime;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.naming.NamingException;
import javax.naming.directory.BasicAttributes;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.aisp.adapter.CustomerAccountListAdapter;
import com.capgemini.psd2.aisp.domain.OBAccount6;
import com.capgemini.psd2.aisp.domain.OBAccount6Account;
import com.capgemini.psd2.aisp.domain.OBReadAccount6;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.DebtorDetailsResponse;
import com.capgemini.psd2.consent.domain.PSD2Account;
import com.capgemini.psd2.consent.domain.PispConsent;
import com.capgemini.psd2.enums.ConsentStatusEnum;
import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.fraudsystem.constants.FraudSystemConstants;
import com.capgemini.psd2.fraudsystem.helper.FraudSystemHelper;
import com.capgemini.psd2.fraudsystem.request.handler.impl.FraudSystemRequestMapping;
import com.capgemini.psd2.logger.LoggerUtils;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.adapter.PispConsentAdapter;
import com.capgemini.psd2.pisp.domain.LogAttributesPlatformResources;
import com.capgemini.psd2.pisp.domain.OBCashAccountDebtor3;
import com.capgemini.psd2.pisp.domain.PaymentConsentsValidationResponse;
import com.capgemini.psd2.pisp.enums.PaymentTypeEnum;
import com.capgemini.psd2.pisp.payment.setup.platform.adapter.PaymentSetupPlatformAdapter;
import com.capgemini.psd2.pisp.sca.consent.operations.adapter.PispScaConsentOperationsAdapter;
import com.capgemini.psd2.pisp.stage.domain.CustomConsentAppViewData;
import com.capgemini.psd2.pisp.stage.domain.CustomFraudSystemPaymentData;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageUpdateData;
import com.capgemini.psd2.pisp.stage.domain.CustomPreAuthorizeAdditionalInfo;
import com.capgemini.psd2.pisp.status.PaymentStatusEnum;
import com.capgemini.psd2.pisp.utilities.PispUtilities;
import com.capgemini.psd2.scaconsenthelper.models.IntentTypeEnum;
import com.capgemini.psd2.scaconsenthelper.models.PickupDataModel;
import com.capgemini.psd2.tppinformation.adaptor.ldap.constants.TPPInformationConstants;
import com.capgemini.psd2.utilities.DateUtilites;
import com.capgemini.psd2.utilities.JSONUtilities;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Component
@ConfigurationProperties(prefix = "app")
public class PispConsentCreationDataHelperImpl implements PispConsentCreationDataHelper {
	private List<String> postAuthorizingAllowedPaymentTypes;

	private static final Logger LOG = LoggerFactory.getLogger(PispConsentCreationDataHelperImpl.class);

	@Autowired
	private LoggerUtils loggerUtils;

	@Autowired
	@Qualifier("CustomerAccountListAdapter")
	private CustomerAccountListAdapter customerAccountListAdapter;

	@Autowired
	private PispConsentAdapter pispConsentAdapter;

	@Autowired
	private FraudSystemHelper fraudSystemHelper;

	@Autowired
	private PaymentSetupPlatformAdapter paymentSetupPlatformAdapter;

	@Autowired
	private RequestHeaderAttributes reqHeaderAttributes;

	@Autowired
	@Qualifier("pispScaConsentOperationsRoutingAdapter")
	private PispScaConsentOperationsAdapter pispStageOperationsAdapter;

	@Value("${app.paymentSetupExpiryTime:#{24}}")
	private Integer paymentSetupExpiryTime;

	@Value("${app.consentExpiryTime:#{24}}")
	private Integer consentExpiryTime;

	private static final long TIMEINSECONDS = 3600;

	@ConfigurationProperties(value = "postAuthorizingAllowedPaymentTypes")
	public List<String> getPostAuthorizingAllowedPaymentTypes() {
		return postAuthorizingAllowedPaymentTypes;
	}

	public void setPostAuthorizingAllowedPaymentTypes(List<String> postAuthorizingAllowedPaymentTypes) {
		this.postAuthorizingAllowedPaymentTypes = postAuthorizingAllowedPaymentTypes;
	}

	@Override
	public OBReadAccount6 retrieveCustomerAccountListInfo(String userId, String clientId, String flowType,
			String correlationId, String channelId, String schemeName, String tenantId, String intentId, String cmaVersion) {

		Map<String, String> paramsMap = new HashMap<>();
		paramsMap.put(PSD2Constants.CMAVERSION, cmaVersion);
		paramsMap.put(PSD2Constants.CHANNEL_ID, channelId);
		paramsMap.put(PSD2Constants.USER_ID, userId);
		paramsMap.put(PSD2Constants.CORRELATION_ID, correlationId);
		paramsMap.put(PSD2Constants.CONSENT_FLOW_TYPE, flowType);
		paramsMap.put(PSD2Constants.SCHEME_NAME, schemeName);
		paramsMap.put(PSD2Constants.TENANT_ID, tenantId);
		return customerAccountListAdapter.retrieveCustomerAccountList(userId, paramsMap);

	}

	@Override
	public void createConsent(OBAccount6 customerAccount, String userId, String cid,
			CustomPaymentStageIdentifiers stageIdentifiers, String channelId, String headers, String tppApplicationName,
			Object tppInformationObj, String tenantId) throws NamingException {
		AccountDetails acctDetail = new AccountDetails();
		PispConsent consent = new PispConsent();
		String legalEntityName = null;

		Map<String, String> additionalInfo = ((PSD2Account) customerAccount).getAdditionalInformation();

		String accountNumber;
		String accountNSC;

		accountNSC = additionalInfo.get(PSD2Constants.ACCOUNT_NSC);
		accountNumber = additionalInfo.get(PSD2Constants.ACCOUNT_NUMBER);

		acctDetail.setAccountNumber(accountNumber);
		acctDetail.setAccountNSC(accountNSC);

		if (tppInformationObj != null && tppInformationObj instanceof BasicAttributes) {
			BasicAttributes tppInfoAttributes = (BasicAttributes) tppInformationObj;
			legalEntityName = returnLegalEntityName(tppInfoAttributes);
		}

		reqHeaderAttributes.setTppLegalEntityName(legalEntityName);
		reqHeaderAttributes.setTppCID(cid);
		consent.setTppApplicationName(tppApplicationName);
		consent.setTppLegalEntityName(legalEntityName);
		consent.setChannelId(channelId);
		consent.setAccountDetails(acctDetail);
		consent.setPsuId(userId);
		consent.setTppCId(cid);
		consent.setPaymentId(stageIdentifiers.getPaymentConsentId());
		
		/* Consent Version should be same as Setup Version */
		consent.setCmaVersion(stageIdentifiers.getPaymentSetupVersion());
		
		consent.setTenantId(tenantId);
		consent.setPartyIdentifier(additionalInfo.get(PSD2Constants.PARTY_IDENTIFIER));

		OffsetDateTime startDate = OffsetDateTime.now();
		consent.setStartDate(DateUtilites.getCurrentDateInISOFormat(startDate));
		OffsetDateTime endDate = startDate.plusSeconds(consentExpiryTime * TIMEINSECONDS);
		consent.setEndDate(DateUtilites.getCurrentDateInISOFormat(endDate));

		Map<String, String> paramsMap = new HashMap<>();
		paramsMap.putAll(captureFraudParam(headers));
		paramsMap.put(PSD2Constants.CHANNEL_NAME, channelId);
		paramsMap.put(PSD2Constants.TENANT_ID, tenantId);
		paramsMap.put(FraudSystemConstants.PAYMENT_ID, stageIdentifiers.getPaymentConsentId());

		/* Removed call for retrieve payment stage details */
		CustomFraudSystemPaymentData fraudSystemPaymentData = retrieveFraudSystemPaymentData(
				stageIdentifiers.getPaymentConsentId());

		Object fraudResponse = fraudSystemHelper.captureFraudEvent(userId, customerAccount, fraudSystemPaymentData,
				paramsMap);
		((PSD2Account) customerAccount).setFraudResponse(fraudResponse);

		/*
		 * Log here for reporting : Identification of pisp type for which
		 * consent is created
		 */
		LOG.info("{\"Enter\":\"{}\",\"{}\"}",
				"com.capgemini.psd2.pisp.consent.adapter.impl.PispConsentAdapterImpl.createConsent()",
				loggerUtils.populateLoggerData("updateConsentResourceWithConsumed"));

		pispConsentAdapter.createConsent(consent);
		
		LOG.info("{\"Exit\":\"{}\",\"{}\",\"consentCreatedStageIdentifiers\":{}}",
				"com.capgemini.psd2.pisp.consent.adapter.impl.PispConsentAdapterImpl.createConsent()",
				loggerUtils.populateLoggerData("createConsent"),
				JSONUtilities.getJSONOutPutFromObject(stageIdentifiers));

		/* Removed call for retrieve payment stage details */
		/* Single update call to update debtor details and fraud response */
		CustomPaymentStageUpdateData updateData = new CustomPaymentStageUpdateData();
		updateData.setFraudScoreUpdated(Boolean.TRUE);
		updateData.setFraudScore(fraudResponse);
		
		if (stageIdentifiers.getPaymentTypeEnum() == PaymentTypeEnum.DOMESTIC_ST_ORD) {
			OBCashAccountDebtor3 stageStandingOrderDebtorDetails = populateSelectedStandingOrderDebtorDetails(
					customerAccount);
			updateData.setDebtorDetails(stageStandingOrderDebtorDetails);
		} else {
			OBCashAccountDebtor3 stageDebtorDetails = populateSelectedDebtorDetails(customerAccount);
		updateData.setDebtorDetailsUpdated(Boolean.TRUE);
			updateData.setDebtorDetails(stageDebtorDetails);
		}
		
		updateData.setDebtorDetailsUpdated(Boolean.TRUE);
		Map<String, String> params = new HashMap<>();
		params.put(PSD2Constants.PARTY_IDENTIFIER, additionalInfo.get(PSD2Constants.PARTY_IDENTIFIER));
		params.put(PSD2Constants.IBAN, additionalInfo.get(PSD2Constants.IBAN));
		params.put(PSD2Constants.GCIS_CUSTOMER_TYPE,additionalInfo.get(PSD2Constants.GCIS_CUSTOMER_TYPE));
		params.put(PSD2Constants.PARTY_ACTIVE_INDICATOR,additionalInfo.get(PSD2Constants.PARTY_ACTIVE_INDICATOR));
		params.put(PSD2Constants.PARTY_NAME,additionalInfo.get(PSD2Constants.PARTY_NAME));
		pispStageOperationsAdapter.updatePaymentStageData(stageIdentifiers, updateData, params);

	}

	@Override
	public OBCashAccountDebtor3 populateSelectedStandingOrderDebtorDetails(
			OBAccount6 selectedAccount) {

		PSD2Account psd2Account = (PSD2Account) selectedAccount;
		OBCashAccountDebtor3 debtorAccount;
		OBAccount6Account obAccount2Account = psd2Account.getAccount().get(0);
		debtorAccount = new OBCashAccountDebtor3();
		debtorAccount.setIdentification(obAccount2Account.getIdentification());

		// Fix Scheme name validation failed. In MongoDB producing exception:
		// can't have. In field names [UK.OBIE.IBAN] while creating test data.
		// Hence, in mock data, we used "_" instead of "." character
		// Replacing "_" to "." to convert valid Scheme Name into mongo db
		// adapter only. there is no impact on CMA api flow. Its specifc to
		// Sandbox functionality
		debtorAccount.setSchemeName(obAccount2Account.getSchemeName().replace('_', '.'));

		
		 /** If DebtorAccount.Name is not sent by TPP then product will set the
		 * name from PSD2Account.NickName else will set from PSD2.Account.Name
		 * to adapter and it would have same value as sent by TPP during payment
		 * setup. Name is mandatory parameter for FS call hence set it.*/
		 

		String debtorAccountName = psd2Account.getNickname();
		if (!NullCheckUtils.isNullOrEmpty(obAccount2Account.getName()))
			debtorAccountName = obAccount2Account.getName();
		debtorAccount.setName(debtorAccountName);
		debtorAccount.setSecondaryIdentification(obAccount2Account.getSecondaryIdentification());
		return debtorAccount;
	}

	@Override
	public OBCashAccountDebtor3 populateSelectedDebtorDetails(OBAccount6 selectedAccount) {

		PSD2Account psd2Account = (PSD2Account) selectedAccount;
		
		OBCashAccountDebtor3 debtorAccount;
		OBAccount6Account obAccount2Account = psd2Account.getAccount().get(0);
		debtorAccount = new OBCashAccountDebtor3();
		debtorAccount.setIdentification(obAccount2Account.getIdentification());
		
		// Fix Scheme name validation failed. In MongoDB producing exception:
		// can't have. In field names [UK.OBIE.IBAN] while creating test data.
		// Hence, in mock data, we used "_" instead of "." character
		// Replacing "_" to "." to convert valid Scheme Name into mongo db
		// adapter only. there is no impact on CMA api flow. Its specifc to
		// Sandbox functionality
		debtorAccount.setSchemeName(obAccount2Account.getSchemeName().replace('_', '.'));

		/*
		 * If DebtorAccount.Name is not sent by TPP then product will set the
		 * name from PSD2Account.NickName else will set from PSD2.Account.Name
		 * to adapter and it would have same value as sent by TPP during payment
		 * setup. Name is mandatory parameter for FS call hence set it.
		 */

		String debtorAccountName = psd2Account.getNickname();
		if (!NullCheckUtils.isNullOrEmpty(obAccount2Account.getName()))
			debtorAccountName = obAccount2Account.getName();
		debtorAccount.setName(debtorAccountName);
		debtorAccount.setSecondaryIdentification(obAccount2Account.getSecondaryIdentification());
		
		return debtorAccount;
	}

	private Map<String, String> captureFraudParam(String headers) {
		Map<String, String> param = new HashMap<>();
		param.put(PSD2Constants.FLOWTYPE, IntentTypeEnum.PISP_INTENT_TYPE.getIntentType());
		param.put(FraudSystemRequestMapping.FS_HEADERS, new String(Base64.getDecoder().decode(headers)));
		return param;
	}

	@Override
	public void cancelPaymentSetup(String intentId, Map<String, String> paramsMap) {

		LOG.info("{\"Enter\":\"{}\",\"{}\"}", "com.capgemini.psd2.security.consent.pisp.helpers.cancelPaymentSetup()",
				loggerUtils.populateLoggerData("cancelPaymentSetup"));

       //This logger is added for the MI Report, do not remove this.
		LOG.info("{\"Enter\":\"{}\",\"{}\"}",
				"com.capgemini.psd2.pisp.payment.setup.platform.adapter.impl.updatePaymentSetupPlatformResource()",
				loggerUtils.populateLoggerData("updatePaymentSetupPlatformResource"));
				
		PispConsent consent = pispConsentAdapter.retrieveConsentByPaymentId(intentId);
		
		//Defect Fix For P000428-791 : Once access token is generated, we can not cancel consent.
		if (consent != null){
			if(ConsentStatusEnum.AWAITINGAUTHORISATION.equals(consent.getStatus()))
				pispConsentAdapter.updateConsentStatus(consent.getConsentId(), ConsentStatusEnum.REJECTED);
			else 
				throw PSD2Exception.populatePSD2Exception("Consent is not in correct status", ErrorCodeEnum.INTENT_ID_VALIDATION_ERROR);	
		}

		CustomPaymentStageUpdateData updateData = new CustomPaymentStageUpdateData();
		updateData.setSetupStatusUpdated(Boolean.TRUE);
		updateData.setSetupStatus(PaymentStatusEnum.REJECTED.getStatusCode());
		updateData.setSetupStatusUpdateDateTime(PispUtilities.getCurrentDateInISOFormat());
		updateData.setStatus(PaymentStatusEnum.REJECTED.getStatusCode());
		/* Update platform status to Rejected */
		paymentSetupPlatformAdapter.updatePaymentSetupPlatformResource(intentId, updateData);

		/* Removed call for retrieve payment stage details */
		/* Update payment setup status to Rejected */
		CustomPaymentStageIdentifiers stageIdentifiers = getStageIdentifiers(intentId);
		pispStageOperationsAdapter.updatePaymentStageData(stageIdentifiers, updateData, paramsMap);
		
		LogAttributesPlatformResources updatedConsentPlatformResource = new LogAttributesPlatformResources();
		updatedConsentPlatformResource.setStatus(PaymentStatusEnum.REJECTED.getStatusCode());
		updatedConsentPlatformResource.setPaymentConsentId(stageIdentifiers.getPaymentConsentId());
		updatedConsentPlatformResource.setPaymentType(stageIdentifiers.getPaymentTypeEnum());
		
		//This logger is added for the MI Report, do not remove this.
		LOG.info("{\"Exit\":\"{}\",\"{}\",\"updatedConsentPlatformResource\":{}}",
				"com.capgemini.psd2.pisp.payment.setup.platform.adapter.impl.updatePaymentSetupPlatformResource()",
				loggerUtils.populateLoggerData("updatePaymentSetupPlatformResource"),
				JSONUtilities.getJSONOutPutFromObject(updatedConsentPlatformResource));

		LOG.info("{\"Exit\":\"{}\",\"{}\",\"updatePispData\":{},\"stageIdentifiers\":{}}",
				"com.capgemini.psd2.security.consent.pisp.helpers.cancelPaymentSetup()",
				loggerUtils.populateLoggerData("cancelPaymentSetup"), JSONUtilities.getJSONOutPutFromObject(updateData),
				JSONUtilities.getJSONOutPutFromObject(stageIdentifiers));
	}

	private String returnLegalEntityName(BasicAttributes basicAttributes) throws NamingException {
		return getAttributeValue(basicAttributes, TPPInformationConstants.LEGAL_ENTITY_NAME);

	}

	private String getAttributeValue(BasicAttributes tppApplication, String ldapAttr) throws NamingException {
		String attributeValue = null;
		if (tppApplication.get(ldapAttr) != null && tppApplication.get(ldapAttr).get() != null) {
			attributeValue = tppApplication.get(ldapAttr).get().toString();
		}
		return attributeValue;
	}

	@Override
	public CustomConsentAppViewData retrieveConsentAppStagedViewData(String paymentConsentId, CustomPaymentStageIdentifiers stageIdentifiers) {
		Map<String, String> params = null;
		return pispStageOperationsAdapter.retrieveConsentAppStagedViewData(stageIdentifiers, params);
	}
	
	public CustomConsentAppViewData getConsentAppViewData(PickupDataModel pickupDataModel) {
	    CustomPaymentStageIdentifiers stageIdentifiers = paymentSetupPlatformAdapter.populateStageIdentifiers(pickupDataModel.getIntentId());
		return retrieveConsentAppStagedViewData(pickupDataModel.getIntentId(), stageIdentifiers);
	}

	@Override
	public PaymentConsentsValidationResponse validatePreAuthorisation(
			OBCashAccountDebtor3 selectedDebtorDetails,
			CustomPreAuthorizeAdditionalInfo preAuthAdditionalInfo, String intentId, Map<String, String> paramsMap, CustomPaymentStageIdentifiers stageIdentifiers) {
		return pispStageOperationsAdapter.validatePreAuthorisation(selectedDebtorDetails, preAuthAdditionalInfo,
				stageIdentifiers, paramsMap);
	}

	@Override
	public CustomFraudSystemPaymentData retrieveFraudSystemPaymentData(String paymentConsentId) {
		CustomPaymentStageIdentifiers stageIdentifiers = getStageIdentifiers(paymentConsentId);
		Map<String, String> params = null;
		return pispStageOperationsAdapter.retrieveFraudSystemPaymentStagedData(stageIdentifiers, params);
	}

	private CustomPaymentStageIdentifiers getStageIdentifiers(String intentId) {
		return paymentSetupPlatformAdapter.populateStageIdentifiers(intentId);
	}
	
	@Override
	public DebtorDetailsResponse postAuthorisingPartyDetails(PickupDataModel pickupDataModel,PSD2Account customerAccount, CustomPaymentStageIdentifiers stageIdentifiers) {
		Map<String, String> paramsMap = new HashMap<String, String>();
		paramsMap.put(PSD2Constants.USER_IN_REQ_HEADER, pickupDataModel.getUserId());
		paramsMap.put(PSD2Constants.CHANNEL_IN_REQ_HEADER, pickupDataModel.getChannelId());
		paramsMap.put(PSD2Constants.TENANT_ID,pickupDataModel.getTenant_id());
		
		CustomPaymentStageUpdateData updateData = new CustomPaymentStageUpdateData();
		OBCashAccountDebtor3 stageDebtorDetails = populateSelectedDebtorDetails(customerAccount);
		updateData.setDebtorDetailsUpdated(Boolean.TRUE);
		updateData.setDebtorDetails(stageDebtorDetails);
		
		CustomConsentAppViewData consentAppViewData =  pispStageOperationsAdapter.retrieveConsentAppDebtorDetails(stageIdentifiers,updateData,paramsMap);
		
		if(consentAppViewData == null){
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.TECHNICAL_ERROR);
		}
		
		return new DebtorDetailsResponse().baseAmountDetails(consentAppViewData.getBaseAmountDetails())
				.chargeDetails(consentAppViewData.getChargeDetails()).exchangeRateDetails(consentAppViewData.getExchangeRateDetails())
				.totalAmountDetails(consentAppViewData.getTotalAmountDetails());
		
	}
	
	@Override
	public boolean isPostAuthorizingSupported(String paymentType) {
		if (!postAuthorizingAllowedPaymentTypes.isEmpty() && !NullCheckUtils.isNullOrEmpty(paymentType)) {
			return postAuthorizingAllowedPaymentTypes.contains(paymentType);
		}
		return false;
	}
	
	@Override
	public PaymentConsentsValidationResponse validatePreAuthorisationforStandingOrder(
			OBCashAccountDebtor3 standingOrderDebtorDetails,
			CustomPreAuthorizeAdditionalInfo preAuthAdditionalInfo, String intentId, Map<String, String> paramsMap, CustomPaymentStageIdentifiers stageIdentifiers) {
		Map<String, String> params = null;
		return pispStageOperationsAdapter.validatePreAuthorisation(standingOrderDebtorDetails,
				preAuthAdditionalInfo, stageIdentifiers, params);
	}

	@Override
	public CustomConsentAppViewData retrieveConsentAppStagedViewData(String paymentConsentId, boolean isSinnglePageConsent) {
		
		CustomPaymentStageIdentifiers stageIdentifiers = getStageIdentifiers(paymentConsentId);
		Map<String, String> params = null;
		return pispStageOperationsAdapter.retrieveConsentAppStagedViewData(stageIdentifiers, params);
	}
	
	}
