package com.capgemini.psd2.scaconsenthelper.config.helpers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.capgemini.psd2.aisp.domain.OBAccount6;
import com.capgemini.psd2.aisp.domain.OBAccount6Account;
import com.capgemini.psd2.aisp.domain.OBReadAccount6;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AispConsent;
import com.capgemini.psd2.consent.domain.PSD2Account;
import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.utilities.NullCheckUtils;

public abstract class ConsentAuthorizationHelper {

	public List<OBAccount6> matchAccountFromConsentAccount(OBReadAccount6 accountGETResponse,
			AispConsent aispConsent) {
		List<OBAccount6> consentAccounts;
		Map<String, OBAccount6> mapOfAccounts;
		OBAccount6 account;
		if (accountGETResponse == null || accountGETResponse.getData().getAccount().isEmpty()) {
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.SELECTED_ACCOUNTS_NOT_OWNED_BY_CUSTOMER);
		}
		if (aispConsent.getAccountDetails() == null || aispConsent.getAccountDetails().isEmpty()) {
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.NO_ACCOUNTS_SELECTED_BY_CUSTOMER);
		}
		mapOfAccounts = new HashMap<>();
		consentAccounts = new ArrayList<>();
		for (OBAccount6 currAccount : accountGETResponse.getData().getAccount()) {
			mapOfAccounts.put(((PSD2Account) currAccount).getHashedValue(), currAccount);
		}
		for (AccountDetails accountDetail : aispConsent.getAccountDetails()) {
			account = mapOfAccounts.get(accountDetail.getHashValue());
			consentAccounts.add(account);
		}
		return consentAccounts;
	}

	public OBAccount6 matchAccountFromList(OBReadAccount6 accountGETResponse, PSD2Account selectedAccount) {
		PSD2Account customerAccount = null;
		if (accountGETResponse == null || accountGETResponse.getData().getAccount().isEmpty()) {
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.SELECTED_ACCOUNTS_NOT_OWNED_BY_CUSTOMER);
		}
		if (selectedAccount != null) {
			for (OBAccount6 account : accountGETResponse.getData().getAccount()) {
				customerAccount = matchHash(selectedAccount, (PSD2Account) account);
				
				if (customerAccount != null) {
					break;
				}
			}
			if (customerAccount == null) {
				throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.SELECTED_ACCOUNTS_NOT_OWNED_BY_CUSTOMER);
			}
		} else {
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.NO_ACCOUNTS_SELECTED_BY_CUSTOMER);
		}
		customerAccount = populateAccountwithUnmaskedValues(selectedAccount, (PSD2Account) customerAccount);
		return customerAccount;
	}

	public OBAccount6 matchAccountByIdentification(OBReadAccount6 accountGETResponse, PSD2Account selectedAccount) {
		PSD2Account customerAccount = null;
		if (accountGETResponse == null || accountGETResponse.getData().getAccount().isEmpty()) {
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.SELECTED_ACCOUNTS_NOT_OWNED_BY_CUSTOMER);
		}
		if (selectedAccount != null) {
			for (OBAccount6 account : accountGETResponse.getData().getAccount()) {
				customerAccount = matchIdentification(selectedAccount, (PSD2Account) account);
				if (customerAccount != null) {
					break;
				}
			}
			if (customerAccount == null) {
				throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.SELECTED_ACCOUNTS_NOT_OWNED_BY_CUSTOMER);
			}
		} else {
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.NO_ACCOUNTS_SELECTED_BY_CUSTOMER);
		}
		customerAccount = populateAccountwithUnmaskedValues(selectedAccount, (PSD2Account) customerAccount);
		return customerAccount;
	}


	private PSD2Account matchHash(PSD2Account submittedAccount, PSD2Account customerAccount) {

		String acctHashedValue = submittedAccount.getHashedValue();
		if (NullCheckUtils.isNullOrEmpty(acctHashedValue)) {
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.SELECTED_ACCOUNTS_NOT_OWNED_BY_CUSTOMER);
		}
		boolean accountMatch = acctHashedValue.equals(customerAccount.getHashedValue());
		if (accountMatch) {
			return customerAccount;
		}
		return null;
	}
	
	private PSD2Account matchIdentification(PSD2Account submittedAccount, PSD2Account customerAccount) {


		Iterator<OBAccount6Account> itr = submittedAccount.getAccount().iterator();
		Iterator<OBAccount6Account> itr1 = customerAccount.getAccount().iterator();
		String identification = null;
		String identification1 = null;
		while (itr.hasNext()) {
			OBAccount6Account oBAccount3Account = itr.next();
			identification = 	 oBAccount3Account.getIdentification();
		}

		while (itr1.hasNext()) {
			OBAccount6Account oBAccount3Account1 = itr1.next();
			identification1 =   oBAccount3Account1.getIdentification();
		}
		boolean	accountMatch = false;
		if(identification!=null) {
			accountMatch = identification.equals(identification1);
		}
		if (accountMatch) {
			return customerAccount;
		}
		return null;
	}

	public abstract PSD2Account populateAccountwithUnmaskedValues(PSD2Account accountwithMask,
			PSD2Account accountwithoutMask);

	public <T> List<OBAccount6> populateAccountListFromAccountDetails(T input) {
		return null;
	}

}
