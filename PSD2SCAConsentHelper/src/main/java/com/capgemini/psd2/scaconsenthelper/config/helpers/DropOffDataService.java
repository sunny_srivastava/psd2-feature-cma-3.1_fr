package com.capgemini.psd2.scaconsenthelper.config.helpers;



import java.util.Base64;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.rest.client.helpers.HeaderHelper;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.impl.RestClientSyncImpl;
import com.capgemini.psd2.scaconsenthelper.config.PFConfig;
import com.capgemini.psd2.scaconsenthelper.constants.OIDCConstants;
import com.capgemini.psd2.scaconsenthelper.constants.PFConstants;
import com.capgemini.psd2.scaconsenthelper.constants.SCAConsentHelperConstants;
import com.capgemini.psd2.scaconsenthelper.models.DropOffRequest;
import com.capgemini.psd2.scaconsenthelper.models.DropOffResponse;
import com.capgemini.psd2.utilities.JSONUtilities;
@Component
public class DropOffDataService {

	@Autowired
	private PFConfig pfConfig;

	@Autowired
	private RestClientSyncImpl restClient;

	@Autowired
	private RequestHeaderAttributes requestHeaderAttributes;

	public String dropOffOnSuccess(DropOffRequest dropOffRequest) {
		// Fields which are not received from Bank
		dropOffRequest.setAcr(OIDCConstants.SCA_ACR);
		dropOffRequest.setChannel_id(requestHeaderAttributes.getChannelId());
		if ((dropOffRequest.isBypassConsentPage())) {
			dropOffRequest.setSubject(requestHeaderAttributes.getIntentId());
		}
		return dropOff(dropOffRequest);
	}
	
	// Case of PF access_denied error
	public String dropOffOnCancel() {
		DropOffRequest dropOffRequest = new DropOffRequest();
		return dropOff(dropOffRequest); 
	}
	
	private String dropOff(DropOffRequest dropOffRequest) {
		RequestInfo requestInfo = new RequestInfo();
		String tenantId = requestHeaderAttributes.getTenantId() != null ? requestHeaderAttributes.getTenantId() : PSD2Constants.DEFAULT;
		requestInfo.setUrl(pfConfig.getTenantSpecificDropOffUrl(tenantId));
		String credentials = pfConfig.getScainstanceusername().concat(":").concat(pfConfig.getScainstancepassword());
		String encoderString = Base64.getEncoder().encodeToString(credentials.getBytes());
		HttpHeaders httpHeaders = HeaderHelper.populateJSONContentTypeHeader();
		httpHeaders.add(SCAConsentHelperConstants.AUTHORIZATION_HEADER,SCAConsentHelperConstants.BASIC + " ".concat(encoderString));
		httpHeaders.add(PFConstants.PING_INSTANCE_ID, pfConfig.getScainstanceId());
		String jsonResponse = restClient.callForPost(requestInfo, dropOffRequest, String.class, httpHeaders);
		DropOffResponse dropOffResponse = JSONUtilities.getObjectFromJSONString(jsonResponse, DropOffResponse.class);
		return dropOffResponse.getRef();
	}

}
