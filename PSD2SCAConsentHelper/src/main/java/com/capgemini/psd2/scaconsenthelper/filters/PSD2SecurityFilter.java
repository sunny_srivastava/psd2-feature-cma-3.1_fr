package com.capgemini.psd2.scaconsenthelper.filters;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.LoggerUtils;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.scaconsenthelper.constants.SCAConsentHelperConstants;
import com.capgemini.psd2.scaconsenthelper.models.SecurityRequestAttributes;
import com.capgemini.psd2.security.exceptions.PSD2SecurityException;
import com.capgemini.psd2.security.exceptions.SCAConsentErrorCodeEnum;
import com.capgemini.psd2.utilities.DateUtilites;
import com.capgemini.psd2.utilities.GenerateUniqueIdUtilities;
import com.capgemini.psd2.utilities.ValidationUtility;

public class PSD2SecurityFilter extends OncePerRequestFilter {

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(PSD2SecurityFilter.class);

	/** The logger utils. */
	@Autowired
	private LoggerUtils loggerUtils;

	@Autowired
	private RequestHeaderAttributes requestHeaderAttributes;

	@Autowired
	private SecurityRequestAttributes securityRequestAttributes;

	String inInternalFilter = "com.capgemini.psd2.security.filter.PSD2SecurityFilter.doFilterInternal()";

	String filter = "doFilterInternal";

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		request.getServletPath();
		String currentCorrId = requestHeaderAttributes.getCorrelationId();
		LOG.info(
				"{\"Enter\":\"{}\",\"preCorrelationId\":\" \",\"timestamp\":\"{}\",\"remoteAddress\":\"{}\",\"hostName\":\"{}\",\"correlationId\":\"{}\"}",
				"com.capgemini.psd2.security.filter.PSD2SecurityFilter.doFilterInternal()",
				DateUtilites.generateCurrentTimeStamp(), request.getRemoteAddr(), request.getRemoteHost(), currentCorrId);
		LOG.info(
				"{\"Enter\":\"{}\",\"preCorrelationId\":\" \",\"timestamp\":\"{}\",\"remoteAddress\":\"{}\",\"hostName\":\"{}\",\"correlationId\":\"{}\"}",
				"com.capgemini.psd2.security.filter.PSD2SecurityFilter.doFilterInternal()",
				DateUtilites.generateCurrentTimeStamp(), request.getRemoteAddr(), request.getRemoteHost(), request.getParameter(PSD2Constants.CO_RELATION_ID));
		
		try {

			if (currentCorrId == null || currentCorrId.isEmpty()) {
				currentCorrId = request.getParameter(PSD2Constants.CO_RELATION_ID);
				ValidationUtility.isValidUUID(currentCorrId);
				requestHeaderAttributes.setCorrelationId(currentCorrId);
			}
			if ((request.getAttribute(SCAConsentHelperConstants.EXCEPTION) == null)
					&& (currentCorrId == null || currentCorrId.isEmpty())) {
				throw PSD2SecurityException.populatePSD2SecurityException("CorrelationId not found.",
						SCAConsentErrorCodeEnum.VALIDATION_ERROR);
			} 
			
			// CorrelationId is generated for GET /Consent/consentId API
			if (currentCorrId == null || currentCorrId.isEmpty()) {
				currentCorrId = GenerateUniqueIdUtilities.getUniqueId().toString();
				request.setAttribute(PSD2Constants.CORRELATION_ID, currentCorrId);
				requestHeaderAttributes.setCorrelationId(currentCorrId);

			
			LOG.info(
						"{\"Enter\":\"{}\",\"preCorrelationId\":\"Found\",\"timestamp\":\"{}\",\"remoteAddress\":\"{}\",\"hostName\":\"{}\",\"correlationId\":\"{}\"}",
						"com.capgemini.psd2.security.filter.PSD2SecurityFilter.doFilterInternal()",
						DateUtilites.generateCurrentTimeStamp(), request.getRemoteAddr(), request.getRemoteHost(),
						currentCorrId);
			}
			Map<String, String[]> paramsMap = request.getParameterMap();
			securityRequestAttributes.setParamMap(paramsMap);

			String tenantId = request.getHeader(PSD2Constants.NETSCALER_TENANT_ID_HEADER_NAME);

			//In BypassConsentPage scenario, tenantId will be populated through intentData in requestHeaderAttributes
			if (tenantId == null) {
				/*
				 * Case for JWKS end point triggered from PF, which sets
				 * tenantid in req param.
				 */
				tenantId = request.getParameter("tenantId");
			}
			requestHeaderAttributes.setTenantId(tenantId);
	
			/* Load test issue fix, to make thread safe tenantId object holder */
						RequestAttributes springContextAttr = RequestContextHolder.getRequestAttributes();
						springContextAttr.setAttribute("requestTenantId",
								request.getHeader(PSD2Constants.NETSCALER_TENANT_ID_HEADER_NAME),
								RequestAttributes.SCOPE_REQUEST);
			 
			requestHeaderAttributes.setTenantId(request.getHeader("tenantid"));
			LOG.info("{\"Exit\":\"{}\",\"timestamp\":\"{}\",\"correlationId\": \"{}\"}",
					"com.capgemini.psd2.security.filter.PSD2SecurityFilter.doFilterInternal()",
					DateUtilites.generateCurrentTimeStamp(), currentCorrId);

			filterChain.doFilter(request, response);
		} catch (PSD2Exception e) {
			LOG.error("{\"Exception\":\"{}\",\"{}\",\"ErrorDetails\":{}}", inInternalFilter,
					loggerUtils.populateLoggerData(filter), e.getErrorInfo());
			if (LOG.isDebugEnabled()) {
				LOG.error("{\"Exception\":\"{}\",\"{}\",\"ErrorDetails\":\"{}\"}", inInternalFilter,
						loggerUtils.populateLoggerData(filter), e.getStackTrace(), e);
			}
			response.setContentType("application/json");
			response.setStatus(Integer.parseInt(e.getErrorInfo().getStatusCode()));
			request.setAttribute(SCAConsentHelperConstants.EXCEPTION, e.getErrorInfo());
			request.getRequestDispatcher("/errors").forward(request, response);
		} catch (Exception e) {
			PSD2Exception psd2Exception = PSD2Exception.populatePSD2Exception(e.getMessage(),
					ErrorCodeEnum.TECHNICAL_ERROR);
			LOG.error("{\"Exception\":\"{}\",\"{}\",\"ErrorDetails\":{}}", inInternalFilter,
					loggerUtils.populateLoggerData(filter), psd2Exception.getErrorInfo());
			if (LOG.isDebugEnabled()) {
				LOG.error("{\"Exception\":\"{}\",\"{}\",\"ErrorDetails\":\"{}\"}", inInternalFilter,
						loggerUtils.populateLoggerData(filter), e.getStackTrace(), e);
			}
			response.setContentType("application/json");
			response.setStatus(Integer.parseInt(psd2Exception.getErrorInfo().getStatusCode()));
			request.setAttribute(SCAConsentHelperConstants.EXCEPTION, psd2Exception.getErrorInfo());
			request.getRequestDispatcher("/errors").forward(request, response);
		}

	}

	@Override
	protected boolean shouldNotFilter(HttpServletRequest request) throws ServletException {
		List<String> excludeUrlPatterns = new ArrayList<>();
		excludeUrlPatterns.add("/**/sca-ui/**");
		excludeUrlPatterns.add("/**/consent-ui/**");
		excludeUrlPatterns.add("/**/build/**");
		excludeUrlPatterns.add("/ui/staticContentGet");
		excludeUrlPatterns.add("/ui/staticContentGet");
		excludeUrlPatterns.add("/**/fonts/**");
		excludeUrlPatterns.add("/**/img/**");
		excludeUrlPatterns.add("/AISP.**");
		excludeUrlPatterns.add("/ext-libs/fraudnet/**");

		AntPathMatcher matcher = new AntPathMatcher();
		return excludeUrlPatterns.stream().anyMatch(p -> matcher.match(p, request.getServletPath()));
	}
}
