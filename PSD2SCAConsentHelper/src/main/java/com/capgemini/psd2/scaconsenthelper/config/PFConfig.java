package com.capgemini.psd2.scaconsenthelper.config;

import java.util.HashMap;
import java.util.Map;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "pf")
public class PFConfig {

	private Map<String, String> resumePathBaseURLMap;

	private Map<String, String> dropOffURLMap = new HashMap<>();
	
	private Map<String, String> pickUpURLMap = new HashMap<>();
	
	private Map<String, String> grantsrevocationURLMap = new HashMap<>();
	
	private String scainstanceusername;

	private String scainstancepassword;

	private String aispinstanceusername;

	private String aispinstancepassword;

	private String pispinstanceusername;

	private String cispinstanceusername;

	private String scainstanceId;

	private String aispinstanceId;

	private String pispinstanceId;
	
	private String cispinstanceId;

	private String grantsrevocationapiuser;
	
	private String grantsrevocationapipwd;
	
	public String getTenantSpecificDropOffUrl(String id) {
		return dropOffURLMap.get(id);
	}
	
	public String getTenantSpecificPickUpUrl(String id) {
		return pickUpURLMap.get(id);
	}
	
	public String getTenantSpecificGrantsRevocationUrl(String id) {
		return grantsrevocationURLMap.get(id);
	}

	public String getTenantSpecificResumePathBaseUrl(String id) {
		return resumePathBaseURLMap.get(id);
	}

	public String getScainstanceId() {
		return scainstanceId;
	}

	public void setScainstanceId(String scainstanceId) {
		this.scainstanceId = scainstanceId;
	}

	public String getAispinstanceId() {
		return aispinstanceId;
	}

	public void setAispinstanceId(String aispinstanceId) {
		this.aispinstanceId = aispinstanceId;
	}

	public String getPispinstanceId() {
		return pispinstanceId;
	}

	public void setPispinstanceId(String pispinstanceId) {
		this.pispinstanceId = pispinstanceId;
	}

	public String getScainstanceusername() {
		return scainstanceusername;
	}

	public void setScainstanceusername(String scainstanceusername) {
		this.scainstanceusername = scainstanceusername;
	}

	public String getScainstancepassword() {
		return scainstancepassword;
	}

	public void setScainstancepassword(String scainstancepassword) {
		this.scainstancepassword = scainstancepassword;
	}

	public String getAispinstanceusername() {
		return aispinstanceusername;
	}

	public void setAispinstanceusername(String aispinstanceusername) {
		this.aispinstanceusername = aispinstanceusername;
	}

	public String getAispinstancepassword() {
		return aispinstancepassword;
	}

	public void setAispinstancepassword(String aispinstancepassword) {
		this.aispinstancepassword = aispinstancepassword;
	}

	public String getPispinstanceusername() {
		return pispinstanceusername;
	}

	public void setPispinstanceusername(String pispinstanceusername) {
		this.pispinstanceusername = pispinstanceusername;
	}

	public String getGrantsrevocationapiuser() {
		return grantsrevocationapiuser;
	}

	public void setGrantsrevocationapiuser(String grantsrevocationapiuser) {
		this.grantsrevocationapiuser = grantsrevocationapiuser;
	}

	public String getGrantsrevocationapipwd() {
		return grantsrevocationapipwd;
	}

	public void setGrantsrevocationapipwd(String grantsrevocationapipwd) {
		this.grantsrevocationapipwd = grantsrevocationapipwd;
	}

	public String getCispinstanceusername() {
		return cispinstanceusername;
	}

	public void setCispinstanceusername(String cispinstanceusername) {
		this.cispinstanceusername = cispinstanceusername;
	}

	public String getCispinstanceId() {
		return cispinstanceId;
	}

	public void setCispinstanceId(String cispinstanceId) {
		this.cispinstanceId = cispinstanceId;
	}

	public Map<String, String> getDropOffURLMap() {
		return dropOffURLMap;
	}

	public void setDropOffURLMap(Map<String, String> dropOffURLMap) {
		this.dropOffURLMap = dropOffURLMap;
	}

	public Map<String, String> getPickUpURLMap() {
		return pickUpURLMap;
	}

	public void setPickUpURLMap(Map<String, String> pickUpURLMap) {
		this.pickUpURLMap = pickUpURLMap;
	}

	public void setGrantsrevocationURL(Map<String, String> grantsrevocationURLMap) {
		this.grantsrevocationURLMap = grantsrevocationURLMap;
	}	
	
	
	public Map<String, String> getGrantsrevocationURLMap() {
		return grantsrevocationURLMap;
	}

	public void setGrantsrevocationURLMap(Map<String, String> grantsrevocationURLMap) {
		this.grantsrevocationURLMap = grantsrevocationURLMap;
	}

	public Map<String, String> getResumePathBaseURLMap() {
		return resumePathBaseURLMap;
	}

	public void setResumePathBaseURLMap(Map<String, String> resumePathBaseURLMap) {
		this.resumePathBaseURLMap = resumePathBaseURLMap;
	}
}
