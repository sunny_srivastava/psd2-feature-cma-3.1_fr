package com.capgemini.psd2.scaconsenthelper.config.helpers;

import java.util.Map;

import javax.naming.NamingException;

import com.capgemini.psd2.aisp.domain.OBAccount6;
import com.capgemini.psd2.aisp.domain.OBReadAccount6;
import com.capgemini.psd2.consent.domain.DebtorDetailsResponse;
import com.capgemini.psd2.consent.domain.PSD2Account;
import com.capgemini.psd2.pisp.domain.OBCashAccountDebtor3;
import com.capgemini.psd2.pisp.domain.PaymentConsentsValidationResponse;
import com.capgemini.psd2.pisp.stage.domain.CustomConsentAppViewData;
import com.capgemini.psd2.pisp.stage.domain.CustomFraudSystemPaymentData;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.pisp.stage.domain.CustomPreAuthorizeAdditionalInfo;
import com.capgemini.psd2.scaconsenthelper.models.PickupDataModel;

public interface PispConsentCreationDataHelper {

	public void cancelPaymentSetup(String paymentId, Map<String, String> paramsMap);

	public OBReadAccount6 retrieveCustomerAccountListInfo(String userId, String clientId, String flowType, //NOSONAR
			String correlationId, String channelId, String schemeName, String tenantId, String intentId, String cmaVersion);

	public void createConsent(OBAccount6 account, String userId, String cid, //NOSONAR
			CustomPaymentStageIdentifiers stageIdentifiers, String channelId, String header, String tppAppName,
			Object tppInformation, String tenantId) throws NamingException;

	public CustomConsentAppViewData retrieveConsentAppStagedViewData(String paymentConsentId, CustomPaymentStageIdentifiers stageIdentifiers);

	/* Will be called via consent controller pre-authorize request */
	public PaymentConsentsValidationResponse validatePreAuthorisation(
			OBCashAccountDebtor3 selectedDebtorDetails,
			CustomPreAuthorizeAdditionalInfo preAuthAdditionalInfo, String intentId, Map<String, String> paramsMap, CustomPaymentStageIdentifiers stageIdentifiers);

	public PaymentConsentsValidationResponse validatePreAuthorisationforStandingOrder(
			OBCashAccountDebtor3 standingOrderDebtorDetails,
			CustomPreAuthorizeAdditionalInfo preAuthAdditionalInfo, String intentId, Map<String, String> paramsMap, CustomPaymentStageIdentifiers stageIdentifiers);

	/* Will be called via consent controller create request */
	public CustomFraudSystemPaymentData retrieveFraudSystemPaymentData(String paymentConsentId);

	/* Will be called via consentAuthorization an createConsent */
	public OBCashAccountDebtor3 populateSelectedDebtorDetails(OBAccount6 selectedAccount);

	public OBCashAccountDebtor3 populateSelectedStandingOrderDebtorDetails(
			OBAccount6 selectedAccount);

	CustomConsentAppViewData retrieveConsentAppStagedViewData(String paymentConsentId, boolean isSinnglePageConsent);

	boolean isPostAuthorizingSupported(String paymentType);

	DebtorDetailsResponse postAuthorisingPartyDetails(PickupDataModel pickupDataModel, PSD2Account customerAccount,
			CustomPaymentStageIdentifiers stageIdentifiers);

}
