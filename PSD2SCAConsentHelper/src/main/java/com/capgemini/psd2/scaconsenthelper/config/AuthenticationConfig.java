package com.capgemini.psd2.scaconsenthelper.config;

import java.util.Map;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "app")
public class AuthenticationConfig{
	
	private String tokenSigningKey;

	private String jweDecryptionKey;

	private String tokenIssuer;

	private Integer tokenExpirationTime;

	private Map<String, String> authenticationUrlMap;

	private String aspspRedirectUrl;
	
	public String getTokenSigningKey() {
		return tokenSigningKey;
	}

	public void setTokenSigningKey(String tokenSigningKey) {
		this.tokenSigningKey = tokenSigningKey;
	}

	public String getJweDecryptionKey() {
		return jweDecryptionKey;
	}

	public void setJweDecryptionKey(String jweDecryptionKey) {
		this.jweDecryptionKey = jweDecryptionKey;
	}

	public String getTokenIssuer() {
		return tokenIssuer;
	}

	public void setTokenIssuer(String tokenIssuer) {
		this.tokenIssuer = tokenIssuer;
	}

	public Integer getTokenExpirationTime() {
		return tokenExpirationTime;
	}

	public void setTokenExpirationTime(Integer tokenExpirationTime) {
		this.tokenExpirationTime = tokenExpirationTime;
	}

	public Map<String, String> getAuthenticationUrlMap() {
		return authenticationUrlMap;
	}

	public void setAuthenticationUrlMap(Map<String, String> authenticationUrlMap) {
		this.authenticationUrlMap = authenticationUrlMap;
	}

	
	public String getAspspRedirectUrl() {
		return aspspRedirectUrl;
	}

	public void setAspspRedirectUrl(String aspspRedirectUrl) {
		this.aspspRedirectUrl = aspspRedirectUrl;
	}

	public String getTenantSpecificAutheticationUrl(String tenantId) {
		return authenticationUrlMap.get(tenantId);
	}

	public Map<String, String> getAuthenticationUrl() {
		return authenticationUrlMap;
	}

	public void setAuthenticationUrl(Map<String, String> authenticationUrlMap) {
		this.authenticationUrlMap = authenticationUrlMap;
	}

}
