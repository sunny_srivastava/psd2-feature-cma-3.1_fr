package com.capgemini.psd2.scaconsenthelper.models;

import java.util.Map;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

@Component
@Scope(value = "request", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class SecurityRequestAttributes {

	private Map<String,String[]> paramMap = null;

	public Map<String, String[]> getParamMap() {
		return paramMap;
	}
	public void setParamMap(Map<String, String[]> paramMap) {
		this.paramMap = paramMap;
	}
}