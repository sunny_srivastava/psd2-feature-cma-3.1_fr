package com.capgemini.psd2.scaconsenthelper.config.helpers;

import java.util.Base64;
import java.util.Map;

import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.util.UriComponentsBuilder;

import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.rest.client.helpers.HeaderHelper;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.impl.RestClientSyncImpl;
import com.capgemini.psd2.scaconsenthelper.config.PFConfig;
import com.capgemini.psd2.scaconsenthelper.constants.PFConstants;
import com.capgemini.psd2.scaconsenthelper.constants.SCAConsentHelperConstants;
import com.capgemini.psd2.scaconsenthelper.models.IntentTypeEnum;
import com.capgemini.psd2.scaconsenthelper.models.PFInstanceData;
import com.capgemini.psd2.scaconsenthelper.models.PickupDataModel;
import com.capgemini.psd2.utilities.JSONUtilities;

public abstract class MainPickupDataService implements PickUpDataService{

	@Autowired
	private PFConfig pfConfig;
	
	@Autowired
	private RestClientSyncImpl restClientSync;
	
	@Autowired 
	private RequestHeaderAttributes requestHeaderAttributes;
	
	@Override
	public PickupDataModel pickupIntent(String refId,PFInstanceData pfInstanceData) throws ParseException {
		PickupDataModel intentData = null;
		String tenantId = requestHeaderAttributes.getTenantId() != null ? requestHeaderAttributes.getTenantId() : "default";
		String finalPickupURL = UriComponentsBuilder.fromHttpUrl(pfConfig.getTenantSpecificPickUpUrl(tenantId)).queryParam(PFConstants.REF, refId)
				.toUriString();

		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(finalPickupURL);

		HttpHeaders httpHeaders = HeaderHelper.populateJSONContentTypeHeader();

		String credentials = pfInstanceData.getPfInstanceUserName().concat(":").concat(pfInstanceData.getPfInstanceUserPwd());

		String encoderString = Base64.getEncoder().encodeToString(credentials.getBytes());

		httpHeaders.add(SCAConsentHelperConstants.AUTHORIZATION_HEADER, SCAConsentHelperConstants.BASIC+" ".concat(encoderString));

		httpHeaders.add(PFConstants.PING_INSTANCE_ID, pfInstanceData.getPfInstanceId());

		String jsonResponse = restClientSync.callForGetWithParams(requestInfo, String.class, null, httpHeaders);
		Map map = JSONUtilities.getObjectFromJSONString(jsonResponse, Map.class);
		if(!map.isEmpty()){
			intentData = populateIntentData(jsonResponse);
			IntentTypeEnum intentTypeEnum = IntentTypeEnum.findCurrentIntentType(intentData.getScope());
			intentData.setIntentTypeEnum(intentTypeEnum);
			intentData.setOriginatorRefId(refId);
		}		
		return intentData;
	}
	
	public abstract PickupDataModel populateIntentData(String jsonResponse) throws ParseException;
}