package com.capgemini.psd2.scaconsenthelper.services;

import java.util.Base64;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.rest.client.helpers.HeaderHelper;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.RestClientSync;
import com.capgemini.psd2.scaconsenthelper.config.PFConfig;
import com.capgemini.psd2.scaconsenthelper.constants.OIDCConstants;
import com.capgemini.psd2.scaconsenthelper.constants.PFConstants;
import com.capgemini.psd2.scaconsenthelper.constants.SCAConsentHelperConstants;
import com.capgemini.psd2.scaconsenthelper.models.DropOffRequest;
import com.capgemini.psd2.scaconsenthelper.models.DropOffResponse;
import com.capgemini.psd2.scaconsenthelper.models.IntentTypeEnum;
import com.capgemini.psd2.scaconsenthelper.models.PFInstanceData;
import com.capgemini.psd2.scaconsenthelper.models.PickupDataModel;
import com.capgemini.psd2.utilities.GenerateUniqueIdUtilities;
import com.capgemini.psd2.utilities.JSONUtilities;


@Service
public class SCAConsentHelperService {

	@Autowired
	private PFConfig pfConfig;
	
	@Autowired
	private RestClientSync restClientSyncImpl;
	
	@Autowired
	private RequestHeaderAttributes headerAttributes;
	
	@Autowired
	private HttpServletRequest request;
	
	public String cancelJourney(String oAuthUrl,PFInstanceData pfInstanceData) {
		DropOffResponse dropOffResponse = dropOffOnCancel(pfInstanceData.getPfInstanceId(),pfInstanceData.getPfInstanceUserName(),pfInstanceData.getPfInstanceUserPwd());

		return UriComponentsBuilder.fromHttpUrl(oAuthUrl)
				.queryParam(PFConstants.REF, dropOffResponse != null ? dropOffResponse.getRef() : null).toUriString();

	}
	
	public void revokeAllPreviousGrants(String intentId,String instanceUserId,String instancePwd){

		String tenantId = headerAttributes.getTenantId() != null ? headerAttributes.getTenantId() : "default";
		String url = String.format(pfConfig.getTenantSpecificGrantsRevocationUrl(tenantId), intentId);

		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(url);

		HttpHeaders httpHeaders = HeaderHelper.populateJSONContentTypeHeader();

		String credentials = instanceUserId.concat(":").concat(instancePwd);

		String encoderString = Base64.getEncoder().encodeToString(credentials.getBytes());

		httpHeaders.add(SCAConsentHelperConstants.AUTHORIZATION_HEADER, SCAConsentHelperConstants.BASIC+" ".concat(encoderString));

		httpHeaders.add("X-XSRF-Header", GenerateUniqueIdUtilities.generateRandomUniqueID());

		restClientSyncImpl.callForDelete(requestInfo, String.class, httpHeaders);
	}
	
	// Case of PF access_denied error
	private DropOffResponse dropOffOnCancel(String instanceId,String instanceUserId,String instancePwd) {

		RequestInfo requestInfo = new RequestInfo();
		String tenantId = headerAttributes.getTenantId() != null ? headerAttributes.getTenantId() : "default";
		requestInfo.setUrl(pfConfig.getTenantSpecificDropOffUrl(tenantId));

		DropOffRequest dropOffDataModel = new DropOffRequest();
		
		//dropOffDataModel.setCorrelationId(headerAttributes.getCorrelationId());

		HttpHeaders httpHeaders = HeaderHelper.populateJSONContentTypeHeader();

		String credentials = instanceUserId.concat(":").concat(instancePwd);

		String encoderString = Base64.getEncoder().encodeToString(credentials.getBytes());

		httpHeaders.add(SCAConsentHelperConstants.AUTHORIZATION_HEADER, SCAConsentHelperConstants.BASIC+" ".concat(encoderString));

		httpHeaders.add(PFConstants.PING_INSTANCE_ID, instanceId);

		String jsonResponse = restClientSyncImpl.callForPost(requestInfo, dropOffDataModel, String.class, httpHeaders);
		
		if(jsonResponse == null || jsonResponse.trim().length() == 0 || jsonResponse.contains("Authentication Required")){
			return null;
		}

		return JSONUtilities.getObjectFromJSONString(jsonResponse, DropOffResponse.class);
	}	
	
	public DropOffResponse dropOffOnConsentSubmission(PickupDataModel pickupDataModel,String instanceId,String instanceUserName,String instancePwd){
		RequestInfo requestInfo = new RequestInfo();

		String tenantId = headerAttributes.getTenantId() != null ? headerAttributes.getTenantId() : "default";
		requestInfo.setUrl(pfConfig.getTenantSpecificDropOffUrl(tenantId));
		
		DropOffRequest dropOffRequest = new DropOffRequest();
		
		dropOffRequest.setSubject(pickupDataModel.getIntentId());
		
		dropOffRequest.setCorrelationId(pickupDataModel.getCorrelationId());
		
		dropOffRequest.setIntent_type(pickupDataModel.getPaymentType());
		
		dropOffRequest.setTenant_id(headerAttributes.getTenantId());
		
		dropOffRequest.setConsent_expiry(pickupDataModel.getConsentExpiryInMinutes() != null ? pickupDataModel.getConsentExpiryInMinutes().toString() : "-1");
		
		HttpHeaders httpHeaders = HeaderHelper.populateJSONContentTypeHeader();
		
		String credentials = instanceUserName.concat(":").concat(instancePwd);

		String encoderString = Base64.getEncoder().encodeToString(credentials.getBytes());

		httpHeaders.add(SCAConsentHelperConstants.AUTHORIZATION_HEADER, SCAConsentHelperConstants.BASIC+" ".concat(encoderString));

		httpHeaders.add(PFConstants.PING_INSTANCE_ID, instanceId);
		
		String jsonResponse = restClientSyncImpl.callForPost(requestInfo, dropOffRequest, String.class, httpHeaders);
		
		if(jsonResponse == null || jsonResponse.trim().length() == 0 || jsonResponse.contains("Authentication Required")){
			return null;
		}		
		
		return JSONUtilities.getObjectFromJSONString(jsonResponse, DropOffResponse.class);
	}

	public void dropOffOnSCA(String userId, String authenticationTime) {
		
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(pfConfig.getTenantSpecificDropOffUrl(headerAttributes.getTenantId()));

		DropOffRequest dropOffDataModel = new DropOffRequest();
		dropOffDataModel.setUsername(userId);
		dropOffDataModel.setAcr(OIDCConstants.SCA_ACR);
		dropOffDataModel.setChannel_id(headerAttributes.getChannelId());
		dropOffDataModel.setCorrelationId(headerAttributes.getCorrelationId());
		dropOffDataModel.setClient_id(headerAttributes.getTppCID());
		dropOffDataModel.setScope(headerAttributes.getScopes());

		dropOffDataModel.setAuthnInst(authenticationTime);

		// consent submission dropoff fields
		if (headerAttributes.isBypassConsentPage()) {
			dropOffDataModel.setSubject(headerAttributes.getIntentId());
			dropOffDataModel.setIntent_type(IntentTypeEnum.AISP_INTENT_TYPE.getIntentType());
			dropOffDataModel.setTenant_id(headerAttributes.getTenantId());
			dropOffDataModel.setBypassConsentPage(headerAttributes.isBypassConsentPage());
		}
		HttpHeaders httpHeaders = HeaderHelper.populateJSONContentTypeHeader();

		String credentials = pfConfig.getScainstanceusername().concat(":").concat(pfConfig.getScainstancepassword());

		String encoderString = Base64.getEncoder().encodeToString(credentials.getBytes());

		httpHeaders.add(SCAConsentHelperConstants.AUTHORIZATION_HEADER,
				SCAConsentHelperConstants.BASIC + " ".concat(encoderString));

		httpHeaders.add(PFConstants.PING_INSTANCE_ID, pfConfig.getScainstanceId());

		String jsonResponse = restClientSyncImpl.callForPost(requestInfo, dropOffDataModel, String.class, httpHeaders);

		DropOffResponse dropOffResponse = JSONUtilities.getObjectFromJSONString(jsonResponse,DropOffResponse.class);

		request.setAttribute(PFConstants.REF, dropOffResponse.getRef());
	}	

	

}
