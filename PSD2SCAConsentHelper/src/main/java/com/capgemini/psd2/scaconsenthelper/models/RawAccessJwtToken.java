/*************************************************************************
* 
 * CAPGEMINI CONFIDENTIAL
* __________________
* 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
* 
 * NOTICE:  All information contained herein is, and remains
* the property of CAPGEMINI GROUP.
* The intellectual and technical concepts contained herein
* are proprietary to CAPGEMINI GROUP and may be covered
* by patents, patents in process, and are protected by trade secret
* or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from CAPGEMINI GROUP.
*/

package com.capgemini.psd2.scaconsenthelper.models;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.scaconsenthelper.constants.SCAConsentHelperConstants;
import com.capgemini.psd2.security.exceptions.PSD2SecurityException;
import com.capgemini.psd2.security.exceptions.SCAConsentErrorCodeEnum;
import com.capgemini.psd2.utilities.DateUtilites;
import com.nimbusds.jose.JWEObject;
import com.nimbusds.jose.crypto.DirectDecrypter;
import com.nimbusds.jose.crypto.MACVerifier;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;

public class RawAccessJwtToken implements JwtToken, Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static Logger LOGGER = LoggerFactory.getLogger(RawAccessJwtToken.class); //NOSONAR

	private transient RequestHeaderAttributes requestHeaderAttributes;

	private String token;

	public RawAccessJwtToken(String token, RequestHeaderAttributes requestHeaderAttributes) {
		this.token = token;
		this.requestHeaderAttributes = requestHeaderAttributes;
	}

	public String parseAndReturnClaims(String decryptionKey) {

		String intentData = null;

		try {
			JWEObject jweObject = JWEObject.parse(this.token);

			// Decrypt JWEOject with JWE Decryption key and get JWT Token
			byte[] key = decryptionKey.getBytes();
			jweObject.decrypt(new DirectDecrypter(key));

			intentData = (String) jweObject.getPayload().toSignedJWT().getJWTClaimsSet()
					.getClaim(SCAConsentHelperConstants.INTENT_DATA);

		} catch (Exception ex) {
			LOGGER.error("Exception Occurred in parseAndReturnClaims: "+ex);
			PSD2SecurityException authenticationException = PSD2SecurityException
					.populatePSD2SecurityException(ex.getMessage(), SCAConsentErrorCodeEnum.INVALID_TOKEN);
			LOGGER.error("{\"Exception\":\"{}\",\"timestamp\":\"{}\",\"correlationId\": \"{}\",\"ErrorDetails\":{}}",
					"com.capgemini.psd2.scaconsenthelper.models.parseClaims()", DateUtilites.generateCurrentTimeStamp(),
					requestHeaderAttributes.getCorrelationId(), authenticationException.getErrorInfo());
			if (LOGGER.isDebugEnabled()) {
				LOGGER.error(
						"{\"Exception\":\"{}\",\"timestamp\":\"{}\",\"correlationId\": \"{}\",\"ErrorDetails\":\"{}\"}",
						"com.capgemini.psd2.scaconsenthelper.models.parseClaims()",
						DateUtilites.generateCurrentTimeStamp(), requestHeaderAttributes.getCorrelationId(),
						ex.getStackTrace());
			}
			throw authenticationException;
		}
		return intentData;
	}

	/**
	 * Parses and validates JWT Token signature.
	 * 
	 * @throws JwtExpiredTokenException
	 * 
	 */
	public void parseClaims(String signingKey, String decryptionKey) {
		try {

			// Parse JWE Token Payload to get JWE Object
			JWEObject jweObject = JWEObject.parse(this.token);

			// Decrypt JWEOject with JWE Decryption key and get JWT Token
			byte[] key = decryptionKey.getBytes();
			jweObject.decrypt(new DirectDecrypter(key));

			SignedJWT signedJWT = jweObject.getPayload().toSignedJWT();

			// verify JWT with siging key
			signedJWT.verify(new MACVerifier(signingKey));

			checkExpiry(signedJWT.getJWTClaimsSet());

		} catch (Exception ex) {
			LOGGER.error("Exception Occurred in parseClaims: "+ex);
			PSD2SecurityException authenticationException = PSD2SecurityException
					.populatePSD2SecurityException(ex.getMessage(), SCAConsentErrorCodeEnum.INVALID_TOKEN);
			LOGGER.error("{\"Exception\":\"{}\",\"timestamp\":\"{}\",\"correlationId\": \"{}\",\"ErrorDetails\":{}}",
					"com.capgemini.psd2.scaconsenthelper.models.parseClaims()", DateUtilites.generateCurrentTimeStamp(),
					requestHeaderAttributes.getCorrelationId(), authenticationException.getErrorInfo());
			if (LOGGER.isDebugEnabled()) {
				LOGGER.error(
						"{\"Exception\":\"{}\",\"timestamp\":\"{}\",\"correlationId\": \"{}\",\"ErrorDetails\":\"{}\"}",
						"com.capgemini.psd2.scaconsenthelper.models.parseClaims()",
						DateUtilites.generateCurrentTimeStamp(), requestHeaderAttributes.getCorrelationId(),
						ex.getStackTrace());
			}
			throw authenticationException;
		}
	}

	private void checkExpiry(JWTClaimsSet claims) {
		if (claims != null) {
			Date now = new Date();
			SimpleDateFormat sdf;
			// token MUST NOT be accepted on or after any specified exp time:
			Date exp = claims.getExpirationTime();
			if (exp != null && (now.equals(exp) || now.after(exp))) {
					sdf = new SimpleDateFormat();
					String expVal = sdf.format(exp);
					String nowVal = sdf.format(now);

					String msg = "JWT expired at " + expVal + ". Current time: " + nowVal;

					throw PSD2SecurityException.populatePSD2SecurityException(msg,
							SCAConsentErrorCodeEnum.TOKEN_EXPIRED);
			}

			// token MUST NOT be accepted before any specified nbf time:
			Date nbf = claims.getNotBeforeTime();
			if (nbf != null && now.before(nbf)) {
					sdf = new SimpleDateFormat();
					String nbfVal = sdf.format(nbf);
					String nowVal = sdf.format(now);
					String msg = "JWT must not be accepted before " + nbfVal + ". Current time: " + nowVal;
					throw PSD2SecurityException.populatePSD2SecurityException(msg,
							SCAConsentErrorCodeEnum.INVALID_TOKEN);
			}
		}
	}

	@Override
	public String getToken() {
		return token;
	}
}