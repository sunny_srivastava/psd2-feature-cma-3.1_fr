package com.capgemini.psd2.scaconsenthelper.filters;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.List;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.LoggerUtils;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.scaconsenthelper.config.PFConfig;
import com.capgemini.psd2.scaconsenthelper.config.helpers.CookieHelper;
import com.capgemini.psd2.scaconsenthelper.config.helpers.JwtTokenUtility;
import com.capgemini.psd2.scaconsenthelper.constants.PFConstants;
import com.capgemini.psd2.scaconsenthelper.constants.SCAConsentHelperConstants;
import com.capgemini.psd2.scaconsenthelper.models.IntentTypeEnum;
import com.capgemini.psd2.scaconsenthelper.models.JwtToken;
import com.capgemini.psd2.scaconsenthelper.models.PFInstanceData;
import com.capgemini.psd2.scaconsenthelper.models.PickupDataModel;
import com.capgemini.psd2.scaconsenthelper.models.RawAccessJwtToken;
import com.capgemini.psd2.scaconsenthelper.services.SCAConsentHelperService;
import com.capgemini.psd2.security.exceptions.PSD2SecurityException;
import com.capgemini.psd2.security.exceptions.SCAConsentErrorCodeEnum;
import com.capgemini.psd2.utilities.DateUtilites;
import com.capgemini.psd2.utilities.JSONUtilities;

public abstract class CookieHandlerFilter extends OncePerRequestFilter {

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(CookieHandlerFilter.class);

	@Value("${app.protectedEndPoints:#{null}}")
	private String protectedEndPoints;

	@Value("${app.tokenSigningKey:#{null}}")
	private String tokenSigningKey;

	@Value("${app.jweDecryptionKey:#{null}}")
	private String jweDecryptionKey;

	@Value("${app.cookietoken.tokenIssuer:#{null}}")
	private String tokenIssuer;

	@Value("${app.cookievalidity:#{null}}")
	private Integer expiry;

	@Value("${app.sessiontimeout:#{null}}")
	private Integer sessiontimeout;

	/** The logger utils. */
	@Autowired
	private LoggerUtils loggerUtils;

	@Autowired
	private RequestHeaderAttributes requestHeaderAttributes;

	@Autowired
	private SCAConsentHelperService helperService;

	@Autowired
	private PFConfig pfConfig;

	@Autowired
	private CookieHelper cookieHelper;
	
	private String flowType;

	public CookieHandlerFilter(String flowType) {
		this.flowType = flowType;
	}

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		LOG.info(
				"{\"Enter\":\"{}\",\"preCorrelationId\":\" \",\"timestamp\":\"{}\",\"remoteAddress\":\"{}\",\"hostName\":\"{}\",\"correlationId\":\"{}\"}",
				"com.capgemini.psd2.scaconsenthelper.filters.CookieHandlerFilter.doFilterInternal()",
				DateUtilites.generateCurrentTimeStamp(), request.getRemoteAddr(), request.getRemoteHost(), null);
		populateSecurityHeaders(response);
		
		String tenantId = request.getHeader(PSD2Constants.NETSCALER_TENANT_ID_HEADER_NAME);
		Enumeration<String> x = request.getHeaderNames();
		while (x.hasMoreElements()) {
			String headerName = x.nextElement();
			String headerValue = request.getHeader(headerName);
		}
		Enumeration<String> xx = request.getParameterNames();
		while (xx.hasMoreElements()) {
			String paramName = xx.nextElement();
			String paramValue = request.getParameter(paramName);
		}

		if (tenantId == null) {
			/*
			 * Case for JWKS end point triggered from PF, which sets
			 * tenantid in req param.
			 */
			tenantId = request.getParameter("tenantId");

			if (tenantId == null) {
				tenantId = request.getParameter("tenantid");
				tenantId = "BOIROI";
			}
		}
		requestHeaderAttributes.setTenantId(tenantId);
		
		/* Load test issue fix, to make thread safe tenantId object holder */
				RequestAttributes springContextAttr = RequestContextHolder.getRequestAttributes();
				springContextAttr.setAttribute("requestTenantId",
						request.getHeader(PSD2Constants.NETSCALER_TENANT_ID_HEADER_NAME),
						RequestAttributes.SCOPE_REQUEST);
		 
		
		// boolean refreshCase = Boolean.FALSE;
		PickupDataModel intentData = null;
		PFInstanceData pfInstanceData = populatePFInstanceData();
		
		try {
			Cookie[] cookies = request.getCookies();
			Cookie customSessionControllCookie = cookieHelper.getCustomSessionCookie(cookies);
			String[] endPoints = protectedEndPoints.split(",");

			if (cookies != null) {
				for (Cookie cookie : cookies) {
					if (cookie.getName().equalsIgnoreCase(SCAConsentHelperConstants.CUSTOM_SESSION_CONTROLL_COOKIE)) {
						customSessionControllCookie = cookie;
						customSessionControllCookie.setHttpOnly(true);
						customSessionControllCookie.setSecure(true);
						break;
					}
				}
			}

			if (!Arrays.asList(endPoints)
					.contains(request.getServletPath().substring(request.getServletPath().lastIndexOf("/")))) {
				if (customSessionControllCookie != null && !customSessionControllCookie.getValue().trim().isEmpty()) {
					// validate
					String tokenPayload = customSessionControllCookie.getValue();
					try {
						
						RawAccessJwtToken token = new RawAccessJwtToken(tokenPayload, requestHeaderAttributes);

						String intentDataStr = token.parseAndReturnClaims(jweDecryptionKey);
						String requestUri = request.getRequestURI();
						intentData = JSONUtilities.getObjectFromJSONString(intentDataStr, PickupDataModel.class);
						request.setAttribute(SCAConsentHelperConstants.INTENT_DATA, intentData);

						requestHeaderAttributes.setIntentId(intentData.getIntentId());
						requestHeaderAttributes.setTppCID(intentData.getClientId());
						requestHeaderAttributes
								.setPsuId(intentData.getUserId() != null && !intentData.getUserId().trim().isEmpty()
										? intentData.getUserId() : request.getParameter("username"));
						requestHeaderAttributes.setChannelId(intentData.getChannelId());
						requestHeaderAttributes.setCorrelationId(intentData.getCorrelationId());
						requestHeaderAttributes.setScopes(intentData.getScope());
						requestHeaderAttributes.setBypassConsentPage(intentData.isBypassConsentPage());
						//added for renewal flow
						if(requestHeaderAttributes.getTenantId()==null){
						requestHeaderAttributes.setTenantId(intentData.getTenant_id());
						}
						requestHeaderAttributes.setSelfUrl(requestUri.endsWith("/")
								? requestUri.substring(0, requestUri.length() - 1) : requestUri);
						requestHeaderAttributes.setMethodType(request.getMethod());
						token.parseClaims(tokenSigningKey, jweDecryptionKey);
					} catch (Exception e) {
						LOG.error("Exception Occurred: " + e);
						throw PSD2SecurityException.populatePSD2SecurityException(e.getMessage(),
								SCAConsentErrorCodeEnum.SESSION_EXPIRED);
					}
				} else {
					
					throw PSD2SecurityException.populatePSD2SecurityException(SCAConsentErrorCodeEnum.SESSION_EXPIRED);
				}
			} else {
				validateRefreshBackScenarios(customSessionControllCookie, pfInstanceData, request, response);
			}
			
			LOG.info(
					"{\"CookieLogs\":\"{}\",\"timestamp\":\"{}\",\"intentData\":\"{}\"}",
					"com.capgemini.psd2.scaconsenthelper.filters.CookieHandlerFilter.doFilterInternal()",
					DateUtilites.generateCurrentTimeStamp(), JSONUtilities.getJSONOutPutFromObject(intentData));
			
			doFilter(request, response, filterChain);
			
		} catch (PSD2Exception e) {
			
			LOG.error("{\"Exception\":\"{}\",\"{}\",\"ErrorDetails\":{}}",
					"com.capgemini.psd2.scaconsenthelper.filters.CookieHandlerFilter.doFilterInternal()",
					loggerUtils.populateLoggerData("doFilterInternal"), e.getErrorInfo());
			
			if (LOG.isDebugEnabled()) {
				LOG.error("{\"Exception\":\"{}\",\"{}\",\"ErrorDetails\":\"{}\"}",
						"com.capgemini.psd2.scaconsenthelper.filters.CookieHandlerFilter.doFilterInternal()",
						loggerUtils.populateLoggerData("doFilterInternal"), e.getStackTrace(), e);
			}
			request.setAttribute(PSD2Constants.CONSENT_FLOW_TYPE, flowType);
			request.setAttribute(SCAConsentHelperConstants.EXCEPTION, e.getErrorInfo());
			response.setContentType("application/json");
			response.setStatus(Integer.parseInt(e.getErrorInfo().getStatusCode()));
			
			if (request.getAttribute(SCAConsentHelperConstants.UNAUTHORIZED_ERROR) != null
					&& ((Boolean) request.getAttribute(SCAConsentHelperConstants.UNAUTHORIZED_ERROR))) {
				
				request.getRequestDispatcher("/sessionerrors").forward(request, response);
			
			} else {
				
				dropOff(request, response, pfInstanceData);
				request.getRequestDispatcher("/errors").forward(request, response);
			
			}
		
		} catch (Exception e) {
			PSD2Exception psd2Exception = PSD2Exception.populatePSD2Exception(e.getMessage(),
					ErrorCodeEnum.TECHNICAL_ERROR);
			LOG.error("{\"Exception\":\"{}\",\"{}\",\"ErrorDetails\":{}}",
					"com.capgemini.psd2.scaconsenthelper.filters.CookieHandlerFilter.doFilterInternal()",
					loggerUtils.populateLoggerData("doFilterInternal"), psd2Exception.getErrorInfo());
			if (LOG.isDebugEnabled()) {
				LOG.error("{\"Exception\":\"{}\",\"{}\",\"ErrorDetails\":\"{}\"}",
						"com.capgemini.psd2.scaconsenthelper.filters.CookieHandlerFilter.doFilterInternal()",
						loggerUtils.populateLoggerData("doFilterInternal"), e.getStackTrace(), e);
			}
			response.setContentType("application/json");
			response.setStatus(Integer.parseInt(psd2Exception.getErrorInfo().getStatusCode()));
			request.setAttribute(SCAConsentHelperConstants.EXCEPTION, psd2Exception.getErrorInfo());
			dropOff(request, response, pfInstanceData);
			request.setAttribute(PSD2Constants.CONSENT_FLOW_TYPE, flowType);
			request.getRequestDispatcher("/errors").forward(request, response);
		}
	}

	public abstract void validateRefreshBackScenarios(Cookie customSessionControllCookie, PFInstanceData pfInstanceData,
			HttpServletRequest request, HttpServletResponse response)
			throws ParseException, ServletException, IOException;

	private void dropOff(HttpServletRequest request, HttpServletResponse response, PFInstanceData pfInstanceData)
			throws ServletException, IOException {
		String tenantId = requestHeaderAttributes.getTenantId() != null ? requestHeaderAttributes.getTenantId() : "default"; 
		String resumePath = pfConfig.getTenantSpecificResumePathBaseUrl(tenantId);
		String resumePathParam = request.getParameter(PFConstants.RESUME_PATH);
		String OAuthUrl = request.getParameter(SCAConsentHelperConstants.OAUTH_URL_PARAM);

		if (!(resumePathParam == null || resumePathParam.isEmpty())) {
			resumePath = resumePath.concat(resumePathParam);
		} else if (!(OAuthUrl == null || OAuthUrl.isEmpty())) {
			resumePath = resumePath.concat(OAuthUrl);
		}
		resumePath = helperService.cancelJourney(resumePath, pfInstanceData);
		request.setAttribute(PFConstants.RESUME_PATH, resumePath);
	}

	private PFInstanceData populatePFInstanceData() {
		String pfInstanceId = null;
		String pfInstanceUserName = null;
		String pfInstanceUserPwd = null;
		PFInstanceData pfInstanceData = new PFInstanceData();
		if ("SCA".equalsIgnoreCase(flowType)) {
			pfInstanceId = pfConfig.getScainstanceId();
			pfInstanceUserName = pfConfig.getScainstanceusername();
			pfInstanceUserPwd = pfConfig.getScainstancepassword();
		} else if (flowType.equalsIgnoreCase(IntentTypeEnum.AISP_INTENT_TYPE.getIntentType())) {
			pfInstanceId = pfConfig.getAispinstanceId();
			pfInstanceUserName = pfConfig.getAispinstanceusername();
			pfInstanceUserPwd = pfConfig.getAispinstancepassword();
		} else if (flowType.equalsIgnoreCase(IntentTypeEnum.PISP_INTENT_TYPE.getIntentType())) {
			pfInstanceId = pfConfig.getPispinstanceId();
			pfInstanceUserName = pfConfig.getPispinstanceusername();
			pfInstanceUserPwd = pfConfig.getAispinstancepassword();
		} else if (flowType.equalsIgnoreCase(IntentTypeEnum.CISP_INTENT_TYPE.getIntentType())) {
			pfInstanceId = pfConfig.getCispinstanceId();
			pfInstanceUserName = pfConfig.getCispinstanceusername();
			pfInstanceUserPwd = pfConfig.getAispinstancepassword();
		}

		pfInstanceData.setPfInstanceId(pfInstanceId);
		pfInstanceData.setPfInstanceUserName(pfInstanceUserName);
		pfInstanceData.setPfInstanceUserPwd(pfInstanceUserPwd);
		return pfInstanceData;
	}

	public String getFlowType() {
		return this.flowType;
	}

	@Override
	protected boolean shouldNotFilter(HttpServletRequest request) throws ServletException {
		List<String> excludeUrlPatterns = new ArrayList<>();
		excludeUrlPatterns.add("/**/build/**");
		excludeUrlPatterns.add("/**/sca-ui/**");
		excludeUrlPatterns.add("/js/**");
		excludeUrlPatterns.add("/css/**");
		excludeUrlPatterns.add("/ui/staticContentGet");
		excludeUrlPatterns.add("/ui/staticContentGet");
		excludeUrlPatterns.add("/**/fonts/**");
		excludeUrlPatterns.add("/**/img/**");
		excludeUrlPatterns.add("/AISP.**");
		excludeUrlPatterns.add("/ext-libs/fraudnet/**");
		AntPathMatcher matcher = new AntPathMatcher();
		return excludeUrlPatterns.stream().anyMatch(p -> matcher.match(p, request.getServletPath()));
	}

	public Cookie createNewSessionCookie(PickupDataModel intentData) {
		String intentDataJSON = JSONUtilities.getJSONOutPutFromObject(intentData);
		JwtToken sessionAccessToken = JwtTokenUtility.createSessionJwtToken(tokenIssuer, sessiontimeout,
				tokenSigningKey, intentDataJSON, jweDecryptionKey);
		String sessionTokenStr = sessionAccessToken.getToken();
		Cookie cookie = new Cookie(SCAConsentHelperConstants.CUSTOM_SESSION_CONTROLL_COOKIE, null);
		cookie.setSecure(Boolean.TRUE);
		cookie.setHttpOnly(Boolean.TRUE);
		cookie.setValue(sessionTokenStr);
		cookie.setMaxAge(expiry);
		return cookie;
	}

	public abstract void populateSecurityHeaders(HttpServletResponse response);
}
