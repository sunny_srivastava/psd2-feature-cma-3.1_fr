package com.capgemini.psd2.scaconsenthelper.filters;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.LoggerUtils;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.scaconsenthelper.config.helpers.CookieHelper;
import com.capgemini.psd2.scaconsenthelper.config.helpers.PickUpDataService;
import com.capgemini.psd2.scaconsenthelper.constants.PFConstants;
import com.capgemini.psd2.scaconsenthelper.constants.SCAConsentHelperConstants;
import com.capgemini.psd2.scaconsenthelper.models.PFInstanceData;
import com.capgemini.psd2.scaconsenthelper.models.PickupDataModel;
import com.capgemini.psd2.scaconsenthelper.models.RawAccessJwtToken;
import com.capgemini.psd2.security.exceptions.PSD2SecurityException;
import com.capgemini.psd2.security.exceptions.SCAConsentErrorCodeEnum;
import com.capgemini.psd2.utilities.JSONUtilities;
import com.capgemini.psd2.utilities.ValidationUtility;

public class SaaSCookieHandlerFilter extends CookieHandlerFilter{
	
	@Autowired
	private RequestHeaderAttributes requestHeaderAttributes;
	
	@Autowired
	private PickUpDataService pickupDataService;
	
	@Autowired
	private LoggerUtils loggerUtils;

	@Autowired
	private CookieHelper cookieHelper;

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(SaaSCookieHandlerFilter.class);
	
	@Value("${app.tokenSigningKey:#{null}}")
	private String tokenSigningKey;
	
	@Value("${app.jweDecryptionKey:#{null}}")
	private String jweDecryptionKey;
	
	public SaaSCookieHandlerFilter(String flowType){
		super(flowType);
	}

	@Override
	public void validateRefreshBackScenarios(Cookie customSessionControllCookie, PFInstanceData pfInstanceData,
			HttpServletRequest request, HttpServletResponse response)
			throws ParseException, ServletException, IOException {
		PickupDataModel intentData = null;
		RawAccessJwtToken token = null;		
		String requestUri = request.getRequestURI();
		String refId = request.getParameter(PFConstants.REF);
		try {
		
			if(customSessionControllCookie != null){
				String tokenPayload = customSessionControllCookie.getValue();
				token = new RawAccessJwtToken(tokenPayload, requestHeaderAttributes);
				String intentDataStr = token.parseAndReturnClaims(jweDecryptionKey);
				intentData = JSONUtilities.getObjectFromJSONString(intentDataStr, PickupDataModel.class);
			}
			
			// scenario of running the flow again with same or different
			// reference id
			if ((intentData == null)
					|| (intentData != null && !intentData.getOriginatorRefId().equalsIgnoreCase(refId))) { //NOSONAR
				intentData = pickupDataService.pickupIntent(refId,pfInstanceData);
				// scenario of running the flow again with same reference id
				// which has already been consumed after submission.
				invalidRequestCheck(intentData);
				Cookie cookie = cookieHelper.createNewSessionCookie(intentData);
				response.addCookie(cookie);
			}
			
			// scenario of running the flow again with same reference id which
			// has not been consumed after submission.
			else if (intentData != null && intentData.getOriginatorRefId().equalsIgnoreCase(refId)) { //NOSONAR
				try{
					requestHeaderAttributes.setCorrelationId(intentData.getCorrelationId());
					requestHeaderAttributes.setIntentId(intentData.getIntentId());
					requestHeaderAttributes.setTppCID(intentData.getClientId());
					requestHeaderAttributes.setPsuId(request.getParameter("username"));
					requestHeaderAttributes.setChannelId(intentData.getChannelId());
					requestHeaderAttributes.setSelfUrl(
							requestUri.endsWith("/") ? requestUri.substring(0, requestUri.length() - 1) : requestUri);
					requestHeaderAttributes.setMethodType(request.getMethod());
					token.parseClaims(tokenSigningKey,jweDecryptionKey);
				} catch (PSD2Exception pe) {
					LOG.info("Exception Occurred while parsing claims in validateRefreshBackScenarios(): "+pe);
					throw PSD2SecurityException.populatePSD2SecurityException(SCAConsentErrorCodeEnum.SESSION_EXPIRED);
				}
			}
			if (intentData != null) { //NOSONAR
				ValidationUtility.isValidUUID(intentData.getCorrelationId());
				request.setAttribute(SCAConsentHelperConstants.INTENT_DATA, intentData);
				requestHeaderAttributes.setCorrelationId(intentData.getCorrelationId());
				requestHeaderAttributes.setIntentId(intentData.getIntentId());
				requestHeaderAttributes.setTppCID(intentData.getClientId());
				requestHeaderAttributes.setPsuId(request.getParameter("username"));
				requestHeaderAttributes.setChannelId(intentData.getChannelId());
				requestHeaderAttributes.setSelfUrl(
						requestUri.endsWith("/") ? requestUri.substring(0, requestUri.length() - 1) : requestUri);
				requestHeaderAttributes.setMethodType(request.getMethod());
				requestHeaderAttributes.setTenantId(intentData.getTenant_id());
				requestHeaderAttributes.setScopes(intentData.getScope());
			}
		} catch (PSD2Exception pe) {
			LOG.error("Exception Occurred in validateRefreshBackScenarios(): "+pe);
			LOG.error("{\"Exception\":\"{}\",\"{}\",\"ErrorDetails\":{}}",
					"com.capgemini.psd2.scaconsenthelper.filters.SaaSCookieHandlerFilter.doFilterInternal()",
					loggerUtils.populateLoggerData("doFilterInternal"), pe.getErrorInfo());

			debugEnabled(pe);
			
			sessionExpiredCheck(pe); 
			
			request.setAttribute(SCAConsentHelperConstants.UNAUTHORIZED_ERROR, Boolean.TRUE);
			throw pe;
		}
	}

	private void sessionExpiredCheck(PSD2Exception pe) {
		if (pe instanceof PSD2SecurityException && pe.getErrorInfo().getErrorCode().equalsIgnoreCase(SCAConsentErrorCodeEnum.SESSION_EXPIRED.getErrorCode())) { //NOSONAR
			throw pe;
			}			
		}
			
	private void debugEnabled(PSD2Exception pe) {
			if (LOG.isDebugEnabled()) {
				LOG.error("{\"Exception\":\"{}\",\"{}\",\"ErrorDetails\":\"{}\"}",
					"com.capgemini.psd2.scaconsenthelper.filters.SaaSCookieHandlerFilter.doFilterInternal()",
					loggerUtils.populateLoggerData("doFilterInternal"), pe.getStackTrace(), pe);
			}
			}			

	private void invalidRequestCheck(PickupDataModel intentData) {
		if (intentData == null) {
			throw PSD2SecurityException.populatePSD2SecurityException("Invalid Request..",
					SCAConsentErrorCodeEnum.INVALID_REQUEST);
		}
	}	
	
	@Override
	public void populateSecurityHeaders(HttpServletResponse response){
		// No security Headers required for SaaS for now.
	}
}