package com.capgemini.psd2.scaconsenthelper.constants;

public interface OIDCConstants { //NOSONAR

	public static final String REQUEST= "request";
	public static final String SCOPE ="scope";
	public static final String OPENID = "openid";
	public static final String CLAIMS = "claims";
	public static final String USER_INFO = "userinfo";
	public static final String OPENBANKING_INTENT_ID = "openbanking_intent_id";
	public static final String VALUE="value";
	public static final String ID_TOKEN = "id_token";
	public static final String SCA_ACR = "urn:openbanking:psd2:sca";
	public static final String USERNAME= "username";
	public static final String CHANNEL_ID = "channel_id";
	public static final String CLIENT_ID = "client_id";
}
