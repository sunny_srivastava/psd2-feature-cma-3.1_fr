package com.capgemini.psd2.scaconsenthelper.models;

public class DropOffRequest {

	private String username;
	private String acr;
	private String channel_id; //NOSONAR
	private String authnInst; 
	private String subject;
	private String correlationId;
	private String client_id; //NOSONAR
	private String scope;
	private String intent_type; //NOSONAR
	private String tenant_id; //NOSONAR
	private String consent_expiry; //NOSONAR
	private String resumePath;
	private String intentId;
	private Boolean bypassConsentPage;
	
	public String getIntentId() {
		return intentId;
	}
	public void setIntentId(String intentId) {
		this.intentId = intentId;
	}
	public String getResumePath() {
		return resumePath;
	}
	public void setResumePath(String resumePath) {
		this.resumePath = resumePath;
	}
	public String getIntent_type() {
		return intent_type;
	}
	public void setIntent_type(String intent_type) { //NOSONAR
		this.intent_type = intent_type;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getAcr() {
		return acr;
	}
	public void setAcr(String acr) {
		this.acr = acr;
	}
	public String getChannel_id() { //NOSONAR
		return channel_id;
	}
	public void setChannel_id(String channel_id) { //NOSONAR
		this.channel_id = channel_id;
	}
	public String getAuthnInst() {
		return authnInst;
	}
	public void setAuthnInst(String authnInst) {
		this.authnInst = authnInst;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getCorrelationId() {
		return correlationId;
	}
	public void setCorrelationId(String correlationId) {
		this.correlationId = correlationId;
	}
	public String getClient_id() { //NOSONAR
		return client_id;
	}
	public void setClient_id(String client_id) { //NOSONAR
		this.client_id = client_id;
	}
	public String getScope() {
		return scope;
	}
	public void setScope(String scope) {
		this.scope = scope;
	}
	public String getTenant_id() { //NOSONAR
		return tenant_id;
	}
	public void setTenant_id(String tenant_id) { //NOSONAR
		this.tenant_id = tenant_id;
	}
	public String getConsent_expiry() { //NOSONAR
		return consent_expiry;
	}
	public void setConsent_expiry(String consent_expiry) { //NOSONAR
		this.consent_expiry = consent_expiry;
	}		
	public Boolean isBypassConsentPage() {
		return bypassConsentPage;
	}
	public void setBypassConsentPage(Boolean bypassConsentPage) {
		this.bypassConsentPage = bypassConsentPage;
	}
		
}