package com.capgemini.psd2.scaconsenthelper.config.helpers;

import org.springframework.security.core.Authentication;

public interface CustomAuthenticationService {

	public boolean validUserAndPassword(Authentication authentication);
	
	public Authentication authenticateUser(Authentication authentication);
	
	public void createConsentOnSCA();
}
