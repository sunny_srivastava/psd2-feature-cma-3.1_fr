package com.capgemini.psd2.scaconsenthelper.config.helpers;

import org.springframework.ui.ModelMap;

public interface SaasService {

	public ModelMap getPaymentsDataForConsentView();

	public ModelMap getAccountsDataForConsentView();
}
