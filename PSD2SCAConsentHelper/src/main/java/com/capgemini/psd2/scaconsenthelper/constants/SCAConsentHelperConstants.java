package com.capgemini.psd2.scaconsenthelper.constants;

public interface SCAConsentHelperConstants { //NOSONAR
	
	public static final String AUTHORIZATION_HEADER= "Authorization";	
	public static final String BASIC = "Basic";
	public static final String INTENT_DATA = "intentData";
	public static final String CUSTOM_SESSION_CONTROLL_COOKIE="SessionControllCookie";
	public static final String EXCEPTION = "exception";
	public static final String OAUTH_URL_PARAM = "oAuthUrl";
	public static final String UNAUTHORIZED_ERROR ="unauthorizedFlag";
	public static final String COOKIE ="Cookie";
	public static final String ConsentExpiryInMinutes ="consentExpiryInMinutes";
	public static final String INTENT_TYPE_ENUM ="intentTypeEnum";
	public static final String SCA_INTENT_DATA = "intent_data";
	public static final String TENANT_ID = "tenant_id";
	public static final String USER_ID = "user_id";
	public static final String AUTHENTICATION_TIME = "auth_time";
	public static final String TOKEN ="token";
	public static final String REDIRECT_URL ="redirectUrl";
	public static final String LOCALE ="locale";
	public static final String BYPASSCONSENTPAGE ="bypassConsentPage";
}
