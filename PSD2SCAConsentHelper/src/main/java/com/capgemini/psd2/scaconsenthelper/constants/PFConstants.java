package com.capgemini.psd2.scaconsenthelper.constants;

public interface PFConstants {

	public static final String RESUME_PATH	=	"resumePath";
	public static final String PING_INSTANCE_ID = "ping.instanceId";
	public static final String REF = "REF";
	public static final String DATE_FORMAT ="yyyy-MM-dd HH:mm:ssZ";
}
