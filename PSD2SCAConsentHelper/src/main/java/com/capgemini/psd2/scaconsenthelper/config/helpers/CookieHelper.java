package com.capgemini.psd2.scaconsenthelper.config.helpers;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.scaconsenthelper.constants.SCAConsentHelperConstants;
import com.capgemini.psd2.scaconsenthelper.models.JwtToken;
import com.capgemini.psd2.scaconsenthelper.models.PickupDataModel;
import com.capgemini.psd2.scaconsenthelper.models.RawAccessJwtToken;
import com.capgemini.psd2.security.exceptions.PSD2SecurityException;
import com.capgemini.psd2.security.exceptions.SCAConsentErrorCodeEnum;
import com.capgemini.psd2.utilities.JSONUtilities;


@Component
@ConfigurationProperties("app")
public class CookieHelper {

	private static final Logger LOG = LoggerFactory.getLogger(CookieHelper.class);
	
	@Value("${app.jweDecryptionKey:#{null}}")
	private String jweDecryptionKey;

	@Value("${app.tokenSigningKey:#{null}}")
	private String tokenSigningKey;

	@Value("${app.sessiontimeout:#{null}}")
	private Integer sessiontimeout;

	@Value("${app.cookietoken.tokenIssuer:#{null}}")
	private String tokenIssuer;

	@Value("${app.cookievalidity:#{null}}")
	private Integer expiry;

	@Autowired
	private RequestHeaderAttributes requestHeaderAttributes;

	public Cookie createNewSessionCookie(PickupDataModel intentData) {

		String intentDataJSON = JSONUtilities.getJSONOutPutFromObject(intentData);
		JwtToken sessionAccessToken = JwtTokenUtility.createSessionJwtToken(tokenIssuer, sessiontimeout,
				tokenSigningKey, intentDataJSON, jweDecryptionKey);
		String sessionTokenStr = sessionAccessToken.getToken();
		Cookie cookie = new Cookie(SCAConsentHelperConstants.CUSTOM_SESSION_CONTROLL_COOKIE, null);
		cookie.setSecure(Boolean.TRUE);
		cookie.setHttpOnly(Boolean.TRUE);
		cookie.setValue(sessionTokenStr);
		cookie.setMaxAge(expiry);
		return cookie;
	}

	public Cookie setBypassConsentCookie(HttpServletRequest request) {
		PickupDataModel intentData;
		Cookie bypassConsentCookie = null;
		Cookie[] cookies = request.getCookies();
		Cookie customSessionControllCookie = getCustomSessionCookie(cookies);

		if (customSessionControllCookie != null && !customSessionControllCookie.getValue().trim().isEmpty()) {
			String tokenPayload = customSessionControllCookie.getValue();
			try {
				RawAccessJwtToken token = new RawAccessJwtToken(tokenPayload, requestHeaderAttributes);

				String intentDataStr = token.parseAndReturnClaims(jweDecryptionKey);
				intentData = JSONUtilities.getObjectFromJSONString(intentDataStr, PickupDataModel.class);
				
				intentData.setUserId(requestHeaderAttributes.getPsuId());
				intentData.setBypassConsentPage(true);
				bypassConsentCookie = createNewSessionCookie(intentData);

			} catch (Exception e) {
				LOG.error("Exception Occurred: "+e);
				throw PSD2SecurityException.populatePSD2SecurityException(e.getMessage(),
						SCAConsentErrorCodeEnum.SESSION_EXPIRED);
			}
		}

		return bypassConsentCookie;

	}

	public Cookie getCustomSessionCookie(Cookie[] cookies) {
		Cookie customSessionControllCookie = null;
		if (cookies != null) {
			for (Cookie cookie : cookies) {
				if (cookie.getName().equalsIgnoreCase(SCAConsentHelperConstants.CUSTOM_SESSION_CONTROLL_COOKIE)) {
					customSessionControllCookie = cookie;
					customSessionControllCookie.setHttpOnly(true);
					customSessionControllCookie.setSecure(true);
					customSessionControllCookie.setMaxAge(0);
					break;
				}
			}
		}
		return customSessionControllCookie;
	}

	
}
