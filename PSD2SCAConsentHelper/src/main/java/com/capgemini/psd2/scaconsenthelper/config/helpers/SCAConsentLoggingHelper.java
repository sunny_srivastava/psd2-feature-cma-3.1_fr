package com.capgemini.psd2.scaconsenthelper.config.helpers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.aisp.domain.OBReadConsentResponse1;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmationConsentResponse1;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.domain.PaymentConsentsPlatformResource;
import com.capgemini.psd2.pisp.payment.setup.platform.adapter.PaymentSetupPlatformAdapter;
import com.capgemini.psd2.scaconsenthelper.models.IntentTypeEnum;
import com.capgemini.psd2.scaconsenthelper.models.PickupDataModel;

@Component
public class SCAConsentLoggingHelper {

	@Autowired
	private RequestHeaderAttributes reqHeaderAttributes;

	@Autowired
	private SCAConsentAdapterHelper scaConsentAdapterHelper;
	
	@Autowired
	private PaymentSetupPlatformAdapter paymentSetupPlatformAdapter;
	/*
	 * This code snippet set TPPLegalEntityName in RequestHeaderAttribute for
	 * logging
	 */

	public void updateRequestHeaderAttributes(PickupDataModel intentData) {

		if (intentData.getIntentTypeEnum().getIntentType()
				.equalsIgnoreCase(IntentTypeEnum.AISP_INTENT_TYPE.getIntentType())) {

			OBReadConsentResponse1 accountSetupResponse = scaConsentAdapterHelper.getAccountRequestSetupData(intentData.getIntentId());
			reqHeaderAttributes.setTppCID(accountSetupResponse.getData().getTppCID());
			reqHeaderAttributes.setTppLegalEntityName(accountSetupResponse.getData().getTppLegalEntityName());

		} else if (intentData.getIntentTypeEnum().getIntentType()
				.equalsIgnoreCase(IntentTypeEnum.PISP_INTENT_TYPE.getIntentType())) {
			PaymentConsentsPlatformResource paymentConsentsPlatformResource = paymentSetupPlatformAdapter.retrievePaymentSetupPlatformResource(intentData.getIntentId());
			if (paymentConsentsPlatformResource != null) {
				reqHeaderAttributes.setTppCID(paymentConsentsPlatformResource.getTppCID());
				reqHeaderAttributes.setTppLegalEntityName(paymentConsentsPlatformResource.getTppLegalEntityName());
			}		

		} else if (intentData.getIntentTypeEnum().getIntentType()
				.equalsIgnoreCase(IntentTypeEnum.CISP_INTENT_TYPE.getIntentType())) {

			OBFundsConfirmationConsentResponse1 fundsConfirmationSetup = scaConsentAdapterHelper.getFundsConfirmationSetupData(intentData.getIntentId());
			reqHeaderAttributes.setTppCID(fundsConfirmationSetup.getData().getTppCID());
			reqHeaderAttributes.setTppLegalEntityName(fundsConfirmationSetup.getData().getTppLegalEntityName());

		}		
	}
}
