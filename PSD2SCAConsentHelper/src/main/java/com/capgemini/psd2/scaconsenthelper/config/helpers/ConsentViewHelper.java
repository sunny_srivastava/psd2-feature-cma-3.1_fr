package com.capgemini.psd2.scaconsenthelper.config.helpers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.naming.NamingException;
import javax.naming.directory.BasicAttributes;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.aisp.domain.OBAccount6;
import com.capgemini.psd2.aisp.domain.OBAccount6Account;
import com.capgemini.psd2.aisp.domain.OBReadAccount6;
import com.capgemini.psd2.aisp.domain.OBReadAccount6Data;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AispConsent;
import com.capgemini.psd2.consent.domain.PSD2Account;
import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.integration.adapter.TPPInformationAdaptor;
import com.capgemini.psd2.pisp.domain.CustomDPaymentConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.OBCashAccountCreditor2;
import com.capgemini.psd2.pisp.domain.OBDomestic1;
import com.capgemini.psd2.pisp.domain.OBDomestic1InstructedAmount;
import com.capgemini.psd2.pisp.domain.OBRemittanceInformation1;
import com.capgemini.psd2.pisp.domain.OBWriteDataDomesticConsentResponse1;
import com.capgemini.psd2.pisp.domain.PaymentConsentsPlatformResource;
import com.capgemini.psd2.pisp.payment.setup.platform.adapter.PaymentSetupPlatformAdapter;
import com.capgemini.psd2.pisp.stage.domain.CustomConsentAppViewData;
import com.capgemini.psd2.pisp.stage.domain.CustomDebtorDetails;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.scaconsenthelper.models.PickupDataModel;
import com.capgemini.psd2.security.constants.PSD2SecurityConstants;
import com.capgemini.psd2.tppinformation.adaptor.ldap.constants.TPPInformationConstants;
import com.capgemini.psd2.utilities.NullCheckUtils;
import com.capgemini.psd2.utilities.StringUtils;

@Component
@ConfigurationProperties("app")
public class ConsentViewHelper {

	@Autowired
	private PispConsentCreationDataHelper consentCreationDataHelper;

	@Autowired
	private PaymentSetupPlatformAdapter paymentSetupPlatformAdapter;

	@Autowired
	private TPPInformationAdaptor tppInformationAdaptor;
	private Map<String, String> consentSupportedSchemeMap = new HashMap<>();

	public Map<String, String> getConsentSupportedSchemeMap() {
		return consentSupportedSchemeMap;
	}

	public CustomDebtorDetails retrieveConsentDetails(PickupDataModel pickupDataModel) {
		CustomDebtorDetails stagedSetupDebtorDetails = null;

		// Call setupdata, if debtordetails are present then proceed
		PaymentConsentsPlatformResource paymentConsentsPlatformResource = paymentSetupPlatformAdapter
				.retrievePaymentSetupPlatformResource(pickupDataModel.getIntentId());

		if (!NullCheckUtils.isNullOrEmpty(paymentConsentsPlatformResource)
				&& Boolean.TRUE.toString().equals(paymentConsentsPlatformResource.getTppDebtorDetails())) {
			CustomConsentAppViewData consentAppPaymentSetupData = getConsentAppViewData(pickupDataModel);
			stagedSetupDebtorDetails = getCustomDebtorDetails(stagedSetupDebtorDetails, consentAppPaymentSetupData);
		}

		// if debtordetails is not null set field in cookie
		stagedSetupDebtorDetails = validateDebtorDetails(stagedSetupDebtorDetails);

		return stagedSetupDebtorDetails;
	}

	public CustomDebtorDetails validateDebtorDetails(CustomDebtorDetails stagedSetupDebtorDetails) {
		String stagedDebtorSchemeName;
		if (!NullCheckUtils.isNullOrEmpty(stagedSetupDebtorDetails)) {
			stagedDebtorSchemeName = stagedSetupDebtorDetails.getSchemeName();
			if (!consentSupportedSchemeMap.containsKey(stagedDebtorSchemeName)) {
				throw PSD2Exception
						.populatePSD2Exception(ErrorCodeEnum.PROVIDED_DEBTOR_SCHEME_NOT_SUPPORTED_FOR_CONSENT_APP);
			}
		}
		return stagedSetupDebtorDetails;
	}

	public CustomDebtorDetails getCustomDebtorDetails(CustomDebtorDetails stagedSetupDebtorDetails,
			CustomConsentAppViewData consentAppPaymentSetupData) {
		if (!NullCheckUtils.isNullOrEmpty(consentAppPaymentSetupData))
			stagedSetupDebtorDetails = consentAppPaymentSetupData.getDebtorDetails(); //NOSONAR
		return stagedSetupDebtorDetails;
	}

	public CustomConsentAppViewData getConsentAppViewData(PickupDataModel pickupDataModel) {
		
		CustomPaymentStageIdentifiers stageIdentifiers = paymentSetupPlatformAdapter.populateStageIdentifiers(pickupDataModel.getIntentId());
		return consentCreationDataHelper
				.retrieveConsentAppStagedViewData(pickupDataModel.getIntentId(), stageIdentifiers);
	}

	public String returnLegalEntityName(BasicAttributes basicAttributes) throws NamingException {
		return getAttributeValue(basicAttributes, TPPInformationConstants.LEGAL_ENTITY_NAME);
	}

	private String getAttributeValue(BasicAttributes tppApplication, String ldapAttr) throws NamingException {
		String attributeValue = null;
		if (tppApplication.get(ldapAttr) != null && tppApplication.get(ldapAttr).get() != null) {
			attributeValue = tppApplication.get(ldapAttr).get().toString();
		}
		return attributeValue;
	}

	public OBReadAccount6 validateAccountWithAccountList(CustomDebtorDetails debtorDetails,
			OBReadAccount6 obReadAccount2) {
		boolean isAccountMatch = false;

		String debtorSchemeName = debtorDetails.getSchemeName();
		if (obReadAccount2 != null && !obReadAccount2.getData().getAccount().isEmpty()) {
			for (OBAccount6 account : obReadAccount2.getData().getAccount()) {

				if (account instanceof PSD2Account) {
					Map<String, String> additionalInfo = ((PSD2Account) account).getAdditionalInformation();

					if (!consentSupportedSchemeMap.containsKey(debtorSchemeName)) {
						throw PSD2Exception.populatePSD2Exception(
								ErrorCodeEnum.PROVIDED_DEBTOR_SCHEME_NOT_SUPPORTED_FOR_CONSENT_APP);
					}

					String ymlScheme = account.getAccount().get(0).getSchemeName();
					if (null == additionalInfo.get(consentSupportedSchemeMap.get(ymlScheme))) {
						int len = account.getAccount().get(0).getSchemeName().split("\\.").length;
						ymlScheme = account.getAccount().get(0).getSchemeName().split("\\.")[len - 1];
					}

					if ((debtorDetails.getSchemeName().contains(account.getAccount().get(0).getSchemeName())
							|| account.getAccount().get(0).getSchemeName().contains(debtorDetails.getSchemeName()))
							&& additionalInfo.get(consentSupportedSchemeMap.get(ymlScheme))
									.equals(debtorDetails.getIdentification())) {

						isAccountMatch = true;
						obReadAccount2 = populateAccountData((PSD2Account) account, debtorDetails); //NOSONAR
						break;
					}
				}
			}
			if (!isAccountMatch)
				throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.SELECTED_ACCOUNTS_NOT_OWNED_BY_CUSTOMER);
		} else
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.SELECTED_ACCOUNTS_NOT_OWNED_BY_CUSTOMER);
		return obReadAccount2;
	}

	private OBReadAccount6 populateAccountData(PSD2Account account, CustomDebtorDetails debtorDetails) {
		List<OBAccount6> accountData = new ArrayList<>();
		List<OBAccount6Account> obAccount2AccountList = new ArrayList<>();
		OBAccount6Account accountDetails = new OBAccount6Account();

		accountDetails.setName(debtorDetails.getName());
		accountDetails.setSecondaryIdentification(debtorDetails.getSecondaryIdentification());
		accountDetails.setIdentification(debtorDetails.getIdentification());
		accountDetails.setSchemeName(debtorDetails.getSchemeName());
		obAccount2AccountList.add(accountDetails);
		account.setHashedValue(StringUtils.generateHashedValue(debtorDetails.getIdentification()));
		account.setAccount(obAccount2AccountList);
		accountData.add(account);
		OBReadAccount6Data data2 = new OBReadAccount6Data();
		data2.setAccount(accountData);
		OBReadAccount6 oBReadAccount2 = new OBReadAccount6();
		oBReadAccount2.setData(data2);
		return oBReadAccount2;
	}
	
	public static <T> List<OBAccount6> populateAccountListFromAccountDetails(T input) {
		List<AccountDetails> accountDetails = ((AispConsent) input).getAccountDetails();
		List<OBAccount6> accountList = new ArrayList<>();
		for (AccountDetails accountDetail : accountDetails) {
			PSD2Account account = new PSD2Account();
			account.setCurrency(accountDetail.getCurrency());
			account.setNickname(accountDetail.getNickname());
			account.setAccountId(accountDetail.getAccountId());
			account.setAccountSubType(accountDetail.getAccountSubType());
			OBAccount6Account obAccount2Account = accountDetail.getAccount();
			List<OBAccount6Account> obAccount2AccountList = new ArrayList<>();
			obAccount2AccountList.add(obAccount2Account);
			account.setAccount(obAccount2AccountList);
			account.setServicer(accountDetail.getServicer());
			account.setHashedValue(StringUtils.generateHashedValue(accountDetail.getAccount().getIdentification()));
			accountList.add(account);
		}
		return accountList;

	} 

	public CustomDPaymentConsentsPOSTResponse populateLimitedSetUpDataforUI(
			CustomConsentAppViewData consentAppPaymentSetupData) {

		CustomDPaymentConsentsPOSTResponse pmtPostResp = new CustomDPaymentConsentsPOSTResponse();
		OBWriteDataDomesticConsentResponse1 pmtSetupResp = new OBWriteDataDomesticConsentResponse1();
		OBDomestic1 pmtSetupRepsInit = new OBDomestic1();
		OBDomestic1InstructedAmount resultInstAmount = new OBDomestic1InstructedAmount();
		OBCashAccountCreditor2  resultCreditorAccount = new OBCashAccountCreditor2();
		OBRemittanceInformation1 resultRemittanceInfo = new OBRemittanceInformation1();

		if (consentAppPaymentSetupData != null) {
			if (consentAppPaymentSetupData.getAmountDetails() != null) {
				resultInstAmount.setAmount(consentAppPaymentSetupData.getAmountDetails().getAmount());
				resultInstAmount.setCurrency(consentAppPaymentSetupData.getAmountDetails().getCurrency());
			}
			if (consentAppPaymentSetupData.getCreditorDetails() != null) {
				resultCreditorAccount.setName(consentAppPaymentSetupData.getCreditorDetails().getName());
				resultCreditorAccount.setSecondaryIdentification(
						consentAppPaymentSetupData.getCreditorDetails().getSecondaryIdentification());
			}
			if (consentAppPaymentSetupData.getRemittanceDetails() != null) {
				resultRemittanceInfo.setReference(consentAppPaymentSetupData.getRemittanceDetails().getReference());
				resultRemittanceInfo
						.setUnstructured(consentAppPaymentSetupData.getRemittanceDetails().getUnstructured());
			}
		}

		pmtSetupRepsInit.setInstructedAmount(resultInstAmount);
		pmtSetupRepsInit.setCreditorAccount(resultCreditorAccount);
		pmtSetupRepsInit.setRemittanceInformation(resultRemittanceInfo);
		pmtSetupResp.setInitiation(pmtSetupRepsInit);
		pmtPostResp.setData(pmtSetupResp);
		return pmtPostResp;
	}

	 	
	public String getTppInformationByClientId(String clientId) throws NamingException, JSONException { //NOSONAR
		Object tppInformationObj = tppInformationAdaptor.fetchTPPInformation(clientId);
		String tppApplicationName = tppInformationAdaptor.fetchApplicationName(clientId);

		if (tppInformationObj != null && tppInformationObj instanceof BasicAttributes) {
			JSONObject tppInfoJson = new JSONObject();
			tppInfoJson.put(PSD2SecurityConstants.APP_NAME, tppApplicationName);
			BasicAttributes tppInfoAttributes = (BasicAttributes) tppInformationObj;
			String legalEntityName = returnLegalEntityName(tppInfoAttributes);
			tppInfoJson.put(PSD2SecurityConstants.TPP_NAME, legalEntityName);
			return tppInfoJson.toString();
		}
		return null;
	}
	
}
