package com.capgemini.psd2.scaconsenthelper.config.helpers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.aisp.adapter.AccountRequestAdapter;
import com.capgemini.psd2.aisp.domain.OBReadConsentResponse1;
import com.capgemini.psd2.aisp.domain.OBReadConsentResponse1Data;
import com.capgemini.psd2.cisp.adapter.FundsConfirmationConsentAdapter;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmationConsentResponse1;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmationConsentResponse1Data;
import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;

/*
 * A Helper class to fetch/update setup data (Account request/Funds confirmation consent) for SaaS and Consent applications
 */
@Component
public class SCAConsentAdapterHelper {

	@Autowired
	@Qualifier(value = "accountRequestMongoDbAdaptor")
	private AccountRequestAdapter accountRequestAdapter;

	@Autowired
	@Qualifier(value = "fundsConfirmationConsentMongoDbAdapter")
	private FundsConfirmationConsentAdapter fundsConfirmationsConsentAdapter;

	public OBFundsConfirmationConsentResponse1 getFundsConfirmationSetupData(String intentId) {
		OBFundsConfirmationConsentResponse1 fundsConfirmationSetUpResponse = null;
		try {
			fundsConfirmationSetUpResponse = fundsConfirmationsConsentAdapter
					.getFundsConfirmationConsentPOSTResponse(intentId);
		} catch (PSD2Exception ex) {
			if (ex.getOBErrorResponse1() != null && ex.getOBErrorResponse1().getErrors().stream().anyMatch(
					err -> err.getErrorCode().equals(OBErrorCodeEnum.UK_OBIE_RESOURCE_NOTFOUND.getObErrorCode()))) {
				throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.CISP_NO_FUNDS_CONFIRMATION_CONSENT_DATA_FOUND);
			} else
				throw ex;
		}
		return fundsConfirmationSetUpResponse;
	}

	public OBFundsConfirmationConsentResponse1 updateFundsConfirmationSetupData(String intentId,
			OBFundsConfirmationConsentResponse1Data.StatusEnum statusEnum) {
		OBFundsConfirmationConsentResponse1 fundsConfirmationSetUpResponse = null;
		try {
			fundsConfirmationSetUpResponse = fundsConfirmationsConsentAdapter
					.updateFundsConfirmationConsentResponse(intentId, statusEnum);
		} catch (PSD2Exception ex) {
			if (ex.getOBErrorResponse1() != null && ex.getOBErrorResponse1().getErrors().stream().anyMatch(
					err -> err.getErrorCode().equals(OBErrorCodeEnum.UK_OBIE_RESOURCE_NOTFOUND.getObErrorCode()))) {
				throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.CISP_NO_FUNDS_CONFIRMATION_CONSENT_DATA_FOUND);
			} else
				throw ex;
		}
		return fundsConfirmationSetUpResponse;
	}

	public OBReadConsentResponse1 getAccountRequestSetupData(String intentId) {
		OBReadConsentResponse1 accountSetupResponse = null;
		try {
			accountSetupResponse = accountRequestAdapter.getAccountRequestGETResponse(intentId);
		} catch (PSD2Exception ex) {
			if (ex.getOBErrorResponse1() != null && ex.getOBErrorResponse1().getErrors().stream().anyMatch(
					err -> err.getErrorCode().equals(OBErrorCodeEnum.UK_OBIE_RESOURCE_NOTFOUND.getObErrorCode()))) {
				throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.NO_ACCOUNT_REQUEST_DATA_FOUND);
			} else
				throw ex;
		}
		return accountSetupResponse;
	}

	public OBReadConsentResponse1 updateAccountRequestSetupData(String intentId,
			OBReadConsentResponse1Data.StatusEnum statusEnum) {
		OBReadConsentResponse1 accountSetupResponse = null;
		try {
			accountSetupResponse = accountRequestAdapter.updateAccountRequestResponse(intentId, statusEnum);
		} catch (PSD2Exception ex) {
			if (ex.getOBErrorResponse1() != null && ex.getOBErrorResponse1().getErrors().stream().anyMatch(
					err -> err.getErrorCode().equals(OBErrorCodeEnum.UK_OBIE_RESOURCE_NOTFOUND.getObErrorCode()))) {
				throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.NO_ACCOUNT_REQUEST_DATA_FOUND);
			} else
				throw ex;
		}
		return accountSetupResponse;
	}

}
