package com.capgemini.psd2.pisp.payment.submission.platform.test.adapter.impl;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.exceptions.OBPSD2ExceptionUtility;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.domain.OBError1;
import com.capgemini.psd2.pisp.domain.OBErrorResponse1;
import com.capgemini.psd2.pisp.domain.OBExternalConsentStatus1Code;
import com.capgemini.psd2.pisp.domain.PaymentResponseInfo;
import com.capgemini.psd2.pisp.domain.PaymentsPlatformResource;
import com.capgemini.psd2.pisp.enums.PaymentTypeEnum;
import com.capgemini.psd2.pisp.payment.submission.platform.adapter.impl.PaymentsPlatformAdapterImpl;
import com.capgemini.psd2.pisp.payment.submission.platform.repository.PaymentsPlatformRepository;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.product.common.CompatibleVersionList;
import com.capgemini.psd2.token.Token;
import com.capgemini.psd2.token.TppInformationTokenData;

@RunWith(SpringJUnit4ClassRunner.class)
public class PaymentsPlatformAdapterImplTest {

	PSD2Exception PSD2Exception;

	@InjectMocks
	PaymentsPlatformAdapterImpl paymentsPlatformAdapterImpl;

	@Mock
	private RequestHeaderAttributes reqHeaderAtrributes;

	@Mock
	private PaymentsPlatformRepository paymentsPlatformRepository;
	
	@Mock
	private CompatibleVersionList compatibleVersionList;
 
	@Before
	public void setUp() {

		Map<String, String> genericErrorMessages = new HashMap<>();
		Map<String, String> specificErrorMessages = new HashMap<>();

		genericErrorMessages.put("INTERNAL", "INTERNAL Error");
		specificErrorMessages.put("Idempotency", "Idempotency duration is not configured in correct format.");
		OBPSD2ExceptionUtility.genericErrorMessages.putAll(genericErrorMessages);
		OBPSD2ExceptionUtility.specificErrorMessages.putAll(specificErrorMessages);

		OBErrorResponse1 obErrorResponse1 = new OBErrorResponse1();
		obErrorResponse1.setCode("400");
		obErrorResponse1.setMessage("ERROR");
		List<OBError1> errors = new ArrayList<>();
		OBError1 e = new OBError1();
		e.setErrorCode("ERROR");
		e.setMessage("ERROR");
		errors.add(e);
		obErrorResponse1.setErrors(errors);
		String message = "OBPSD2";
		PSD2Exception = new PSD2Exception(message,obErrorResponse1);

		MockitoAnnotations.initMocks(this);
	}

	/**
	 * Context loads.
	 */
	@Test
	public void contextLoads() {
	}

	@Test
	public void testCreateInitialPaymentsPlatformResource() {

		CustomPaymentStageIdentifiers paymentStageIdentifiers = new CustomPaymentStageIdentifiers();
		paymentStageIdentifiers.setPaymentConsentId("23432");
		paymentStageIdentifiers.setPaymentSetupVersion("3.0");
		paymentStageIdentifiers.setPaymentTypeEnum(PaymentTypeEnum.DOMESTIC_PAY);

		String setupCreationDateTime = "2018-11-28T16:32:03+05:30";
		paymentsPlatformAdapterImpl.createInitialPaymentsPlatformResource(paymentStageIdentifiers,
				setupCreationDateTime);
	}

	@SuppressWarnings("unchecked")
	@Test(expected = PSD2Exception.class)
	public void testCreateInitialPaymentsPlatformResourceExc() {
		PaymentsPlatformResource resource=anyObject();
		CustomPaymentStageIdentifiers paymentStageIdentifiers = new CustomPaymentStageIdentifiers();
		paymentStageIdentifiers.setPaymentConsentId("23432");
		paymentStageIdentifiers.setPaymentSetupVersion("3.0");
		paymentStageIdentifiers.setPaymentTypeEnum(PaymentTypeEnum.DOMESTIC_PAY);

		String setupCreationDateTime = "2018-11-28T16:32:03+05:30";
		Mockito.when(paymentsPlatformRepository.save(resource)).thenThrow(DataAccessResourceFailureException.class);
		paymentsPlatformAdapterImpl.createInitialPaymentsPlatformResource(paymentStageIdentifiers,
				setupCreationDateTime);
	}

	@Test
	public void testExceptionCreatePaymentSubmissionResource() {

		String paymentSubmissionId = "123";
		paymentsPlatformAdapterImpl.retrievePaymentsPlatformResource(paymentSubmissionId);
	}

	@SuppressWarnings("unchecked")
	@Test(expected = PSD2Exception.class)
	public void testExceptionCreatePaymentSubmissionResourceExcp() {

		
		String paymentSubmissionId = "123";
		String submissionId = "435";
		Mockito.when(paymentsPlatformRepository.findOneByPaymentSubmissionIdOrSubmissionIdAndSubmissionCmaVersionIn(anyString(), anyString(), any()))
				.thenThrow(DataAccessResourceFailureException.class);
		paymentsPlatformAdapterImpl.retrievePaymentsPlatformResource(paymentSubmissionId);
	}

	@Test
	public void testGetIdempotentPaymentsPlatformResource() {
		String paymentType = "domestic";
		long idempotencyDuration = 24;
		paymentsPlatformAdapterImpl.getIdempotentPaymentsPlatformResource(idempotencyDuration, paymentType);
	}

	@SuppressWarnings("unchecked")
	@Test(expected = PSD2Exception.class)
	public void testGetIdempotentPaymentsPlatformResourceExc() {
		String paymentType = "domestic";
		long idempotencyDuration = 24;
		Mockito.when(paymentsPlatformRepository
				.findOneByTppCIDAndPaymentTypeAndIdempotencyKeyAndIdempotencyRequestAndCreatedAtGreaterThanAndSubmissionCmaVersionIn(anyString(), anyString(), anyString(), anyString(), anyString(),any()))
		.thenThrow(DataAccessResourceFailureException.class);
		paymentsPlatformAdapterImpl.getIdempotentPaymentsPlatformResource(idempotencyDuration, paymentType);
	}

	@Test
	public void testRetrievePaymentsResourceByConsentId() {
		String paymentConsentId = "59bb6c31-5831-4a4a-a2b1-d54f5b8f20d7";
		paymentsPlatformAdapterImpl.retrievePaymentsResourceByConsentId(paymentConsentId);

	}

	@SuppressWarnings("unchecked")
	@Test(expected = PSD2Exception.class)
	public void testRetrievePayResourceByConsentIdExcep() {
		String paymentConsentId = "59bb6c31-5831-4a4a-a2b1-d54f5b8f20d7";
		Mockito.when(paymentsPlatformRepository.findOneByPaymentConsentIdAndSubmissionCmaVersionIn(anyString(),any()))
				.thenThrow(DataAccessResourceFailureException.class);
		paymentsPlatformAdapterImpl.retrievePaymentsResourceByConsentId(paymentConsentId);
	}

	@Test
	public void testUpdatePaymentsPlatformResource() {
		PaymentsPlatformResource submissionPlatformResource = new PaymentsPlatformResource();
		paymentsPlatformAdapterImpl.updatePaymentsPlatformResource(submissionPlatformResource);
	}

	@SuppressWarnings("unchecked")
	@Test(expected = PSD2Exception.class)
	public void testUpdatePaymentsPlatformResourceExcep() {
		PaymentsPlatformResource submissionPlatformResource = new PaymentsPlatformResource();
		Mockito.when(paymentsPlatformRepository.save(submissionPlatformResource))
				.thenThrow(DataAccessResourceFailureException.class);
		paymentsPlatformAdapterImpl.updatePaymentsPlatformResource(submissionPlatformResource);
	}

	@Test
	public void testCreateInvalidPaymentSubmissionPlatformResource() {

		PaymentsPlatformResource paymentsPlatformResource = new PaymentsPlatformResource();
		PaymentResponseInfo validationDetails = new PaymentResponseInfo();
		validationDetails.setPaymentConsentId("234324234324");
		validationDetails.setConsentValidationStatus(OBExternalConsentStatus1Code.AUTHORISED);
		validationDetails.setIdempotencyRequest("2324ksdflk0235");
		String currentDateInISOFormat = "2018-11-28T16:32:03+05:30";

		TppInformationTokenData tppInformation = new TppInformationTokenData();
		Token token = new Token();
		token.setTppInformation(tppInformation);
		token.setClient_id("5334");
		Mockito.when(reqHeaderAtrributes.getToken()).thenReturn(token);

		paymentsPlatformAdapterImpl.createInvalidPaymentSubmissionPlatformResource(paymentsPlatformResource,
				validationDetails, currentDateInISOFormat);
	}

	@SuppressWarnings("unchecked")
	@Test(expected = PSD2Exception.class)
	public void testCreateInvalidSubPlatformResourceException() {

		PaymentsPlatformResource paymentsPlatformResource = new PaymentsPlatformResource();
		PaymentResponseInfo validationDetails = new PaymentResponseInfo();
		validationDetails.setPaymentConsentId("234324234324");
		validationDetails.setConsentValidationStatus(OBExternalConsentStatus1Code.AUTHORISED);
		validationDetails.setIdempotencyRequest("2324ksdflk0235");
		String currentDateInISOFormat = "2018-11-28T16:32:03+05:30";

		TppInformationTokenData tppInformation = new TppInformationTokenData();
		Token token = new Token();
		token.setTppInformation(tppInformation);
		token.setClient_id("5334");
		Mockito.when(reqHeaderAtrributes.getToken()).thenReturn(token);
		Mockito.when(paymentsPlatformRepository.save(paymentsPlatformResource))
				.thenThrow(DataAccessResourceFailureException.class);
		paymentsPlatformAdapterImpl.createInvalidPaymentSubmissionPlatformResource(paymentsPlatformResource,
				validationDetails, currentDateInISOFormat);
	}

	@Test
	public void testretrievePaymentsPlatformResource() {
		String paymentSubmissionId = "23423";
		String submissionId = "54654";
		paymentsPlatformAdapterImpl.retrievePaymentsPlatformResource(paymentSubmissionId, submissionId);

	}

	@SuppressWarnings("unchecked")
	@Test(expected = PSD2Exception.class)
	public void testretrievePaymentsPlatformResourceException() {
		String paymentSubmissionId = "23423";
		String submissionId = "54654";
		Mockito.when(paymentsPlatformRepository.findOneByPaymentSubmissionIdOrSubmissionIdAndSubmissionCmaVersionIn(anyString(),
				anyString(),any())).thenThrow(DataAccessResourceFailureException.class);
		paymentsPlatformAdapterImpl.retrievePaymentsPlatformResource(paymentSubmissionId, submissionId);

	}
	
	@Test
	public void retrievePaymentsPlatformResourceByPaySubmissionIdAndTypeTest(){
		paymentsPlatformAdapterImpl.retrievePaymentsPlatformResourceByPaySubmissionIdAndType("123455", "BUSINESS");
	}
	
	@Test
	public void testRetrievePaymentsPlatformResourceByPaySubmissionIdAndType() {
		String submissionId = "54654";
		String paymentType = "domestic";
		paymentsPlatformAdapterImpl.retrievePaymentsPlatformResourceByPaySubmissionIdAndType(submissionId, paymentType);
	}

	@SuppressWarnings("unchecked")
	@Test(expected = Exception.class)
	public void testRetrievePaymentsPlatformResourceByPaySubmissionIdAndTypeException() {
		String submissionId = "54654";
		String paymentType = "domestic";
		Mockito.when(paymentsPlatformRepository
				.findOneBySubmissionIdAndPaymentTypeAndSubmissionCmaVersionIn(submissionId, paymentType, any()))
				.thenThrow(DataAccessResourceFailureException.class);
		paymentsPlatformAdapterImpl.retrievePaymentsPlatformResourceByPaySubmissionIdAndType(submissionId, paymentType);

	}

	@Test
	public void testRetrievePaymentsPlatformResourceByBackwardSubmissionId() {
		String submissionId = "54654";
		paymentsPlatformAdapterImpl.retrievePaymentsPlatformResourceByBackwardSubmissionId(submissionId);
	}

	@SuppressWarnings("unchecked")
	@Test(expected = Exception.class)
	public void testRetrievePaymentsPlatformResourceByBackwardSubmissionIdException() {
		String submissionId = "54654";
		Mockito.when(
				paymentsPlatformRepository.findOneByPaymentSubmissionIdAndSubmissionCmaVersionIn(submissionId, any()))
				.thenThrow(DataAccessResourceFailureException.class);
		paymentsPlatformAdapterImpl.retrievePaymentsPlatformResourceByBackwardSubmissionId(submissionId);

	}
	
	@Test
	public void retrievePaymentsPlatformResourceByBackwardSubmissionIdTest(){
		paymentsPlatformAdapterImpl.retrievePaymentsPlatformResourceByBackwardSubmissionId("123455");
	}
	
	@SuppressWarnings("unchecked")
	@Test(expected = Exception.class)
	public void retrievePaymentsResourceByConsentIdException() {
		String submissionId = "54654";
		Mockito.when(
				paymentsPlatformRepository.findOneByPaymentConsentIdAndSubmissionCmaVersionIn(any(),
						any()))
				.thenThrow(DataAccessResourceFailureException.class);
		paymentsPlatformAdapterImpl.retrievePaymentsResourceByConsentId(submissionId);

	}
	
	@SuppressWarnings("unchecked")
	@Test(expected = Exception.class)
	public void retrievePaymentsPlatformResourceException() {
		String submissionId = "54654";
		Mockito.when(
				paymentsPlatformRepository.findOneByPaymentSubmissionIdOrSubmissionIdAndSubmissionCmaVersionIn(any(), any(),
						any()))
				.thenThrow(DataAccessResourceFailureException.class);
		paymentsPlatformAdapterImpl.retrievePaymentsPlatformResource(submissionId);

	}
	
	@SuppressWarnings("unchecked")
	@Test(expected = Exception.class)
	public void retrievePaymentsPlatformResourceByPaySubmissionIdAndTypeException() {
		String submissionId = "54654";
		Mockito.when(
				paymentsPlatformRepository.findOneBySubmissionIdAndPaymentTypeAndSubmissionCmaVersionIn(any(), any(),
						any()))
				.thenThrow(DataAccessResourceFailureException.class);
		paymentsPlatformAdapterImpl.retrievePaymentsPlatformResourceByPaySubmissionIdAndType(submissionId, submissionId);

	}
	
	@SuppressWarnings("unchecked")
	@Test(expected = Exception.class)
	public void retrievePaymentsPlatformResourceByBackwardSubmissionIdException() {
		String submissionId = "54654";
		Mockito.when(
				paymentsPlatformRepository.findOneByPaymentSubmissionIdAndSubmissionCmaVersionIn(any(),
						any()))
				.thenThrow(DataAccessResourceFailureException.class);
		paymentsPlatformAdapterImpl.retrievePaymentsPlatformResourceByBackwardSubmissionId(submissionId);
	}
}
