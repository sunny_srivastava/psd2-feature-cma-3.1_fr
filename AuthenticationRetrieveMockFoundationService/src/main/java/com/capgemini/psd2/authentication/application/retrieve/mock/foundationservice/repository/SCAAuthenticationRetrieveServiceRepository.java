package com.capgemini.psd2.authentication.application.retrieve.mock.foundationservice.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.capgemini.psd2.authentication.application.retrieve.mock.foundationservice.domain.AuthenticationParameters;


public interface SCAAuthenticationRetrieveServiceRepository extends MongoRepository<AuthenticationParameters , String > {

	public AuthenticationParameters findAuthenticationParametersByDigitalUserDigitalUserIdentifier(String digitalUserId);
	

}

