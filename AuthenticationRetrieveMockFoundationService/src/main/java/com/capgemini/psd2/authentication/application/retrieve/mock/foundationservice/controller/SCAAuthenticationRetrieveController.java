package com.capgemini.psd2.authentication.application.retrieve.mock.foundationservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.psd2.authentication.application.retrieve.mock.foundationservice.domain.AuthenticationParameters;
import com.capgemini.psd2.authentication.application.retrieve.mock.foundationservice.service.SCAAuthenticationRetrieveService;
import com.capgemini.psd2.foundationservice.validator.ValidationUtility;



@RequestMapping(value = "/exchange/assets/124a7470-1731-4822-8a6f-8436e7e5e1e3/authentication-service/2.0.6/m/it-boi/p/authentication")
@RestController
public class SCAAuthenticationRetrieveController {

	@Autowired
	private SCAAuthenticationRetrieveService authenticationApplicationService;
	
	@Autowired
	private ValidationUtility validationUtility;

	/* B365 */
	@RequestMapping(value = "/v2.0/channels/{channel-code}/digital-users/{digital-user-id}/authentication-parameters", method = RequestMethod.GET, consumes = "application/json", produces = "application/json")
	@ResponseBody
	public ResponseEntity<AuthenticationParameters> retrieveSCAAuthentication(
			@PathVariable("channel-code") String channelCode,
			@PathVariable("digital-user-id") String digitalUserId,
			@RequestHeader(required = false, value = "x-api-source-user") String boiUser,
			@RequestHeader(required = false, value = "x-api-source-system") String boiPlatform,
			@RequestHeader(required = false, value = "x-api-correlation-id") String correlationID,
			@RequestHeader(required = false, value = "x-api-transaction-id") String transanctionID,
			@RequestHeader(required = false, value = "x-api-channel-brand") String apichannelBrand,
			@RequestHeader(required = false, value = "x-api-channel-code") String apiChannelCode,
			@RequestParam(required = false, value = "eventType") String eventType) throws Exception {

		/*
		 * if (NullCheckUtils.isNullOrEmpty(boiPlatform)) { throw
		 * MockFoundationServiceException
		 * .populateMockFoundationServiceException(ErrorCodeEnum.BAD_REQUEST_PPR_AUT); }
		 * 
		 * if (NullCheckUtils.isNullOrEmpty(digitalUser.getDigitalUser().
		 * getCustomerAuthenticationSession())) { throw MockFoundationServiceException
		 * .populateMockFoundationServiceException(ErrorCodeEnum.BAD_REQUEST_PPR_AUT); }
		 */
		validationUtility.sca_authentication_errorcode_Validation(digitalUserId);
		AuthenticationParameters authenticationParameters = authenticationApplicationService.retrieveAuthentication(digitalUserId);

		return new ResponseEntity<>(authenticationParameters, HttpStatus.OK);
	}

}
