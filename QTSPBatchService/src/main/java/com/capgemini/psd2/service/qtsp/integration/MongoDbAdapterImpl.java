package com.capgemini.psd2.service.qtsp.integration;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.capgemini.psd2.model.qtsp.QTSPResource;

@Repository
public class MongoDbAdapterImpl implements MongoDbAdapter {

	@Autowired
	private QTSPServiceRepository qtspServiceRepository;

	@Override
	public List<QTSPResource> saveQTSPs(List<QTSPResource> qtsps) {

		return qtspServiceRepository.save(qtsps);
	}

	@Override
	public List<QTSPResource> retrieveQTSPs() {

		return qtspServiceRepository.findAll();
	}
	
	@Override
	public void removeQTSP(String id) {

		qtspServiceRepository.delete(id);
	}

}