package com.capgemini.psd2.service.qtsp.integration;

import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.model.qtsp.QTSPResource;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.RestClientSync;
import com.capgemini.psd2.service.qtsp.config.NSConfig;
import com.capgemini.psd2.service.qtsp.utility.QTSPServiceUtility;
import com.capgemini.psd2.utilities.JSONUtilities;

@Lazy
@Component
public class NetScalerAdapterImpl implements NetScalerAdapter {

	@Autowired
	private NSConfig nsConfig;
	

	@Autowired
	@Qualifier("qtspServiceRestClient")
	private RestClientSync restClientSync;

	@Autowired
	private QTSPServiceUtility utility;

	@Autowired
	private RequestHeaderAttributes reqHeaderAttribute;

	@Value("${api.nsCertDelayInMillis:0}")
	private String nsCertDelay;

	@Value("${api.nsAPIDelayInMillis:0}")
	private String nsAPIDelay;

	private static final Logger LOGGER = LoggerFactory.getLogger(NetScalerAdapterImpl.class);

	private static final String SUCCESS = "201";

	private static final String DUPLICATE_UPLOAD = "1642";

	private static final String DUPLICATE_ADD = "273";

	private static final String DUPLICATE_BIND = "273";

	private static final String UNBIND_NOT_FOUND = "1540";

	private static final String REMOVE_CA_NOT_FOUND = "1540";

	private static final String REMOVE_CERT_NOT_FOUND = "3445";

	// Pending test
	private static final String REMOVE_SUCCESS = "0";

	private static final String FAILURE = "-1";

	private static final String CERT_EXTENSION = ".crt";

	private static final String ERROR_CODE = "errorcode";

	private static final String NITRO_AUTH_TOKEN = "NITRO_AUTH_TOKEN=";

	private static final String COOKIE = "Cookie";

	private static final Long TIMEOUT_BUFFER = 10L;

	private LocalTime loginTime;

	@Override
	public void processCert(List<? extends Map<String, List<QTSPResource>>> updatedQTSPs) {

		List<QTSPResource> newQTSPs = updatedQTSPs.get(0).get(QTSPServiceUtility.NEW_QTSPS);
		List<QTSPResource> modifiedQTSPs = updatedQTSPs.get(0).get(QTSPServiceUtility.MODIFIED_QTSPS);
		for (String key : nsConfig.getPrefix().keySet()) {
			String prefix = nsConfig.getPrefix().get(key);
			String username = nsConfig.getUsername().get(key);
			String password = nsConfig.getPassword().get(key);
			HttpHeaders requestHeaders = populateHttpHeaders();
			String nitroAuthToken = login(prefix, username, password, requestHeaders);
			if (!nitroAuthToken.isEmpty()) {
				requestHeaders.add(COOKIE, NITRO_AUTH_TOKEN + nitroAuthToken);
				if (!newQTSPs.isEmpty()) {
					addCertInNS(newQTSPs, requestHeaders, prefix, key);
				}
				if (!modifiedQTSPs.isEmpty()) {
					removeCertFromNS(modifiedQTSPs, requestHeaders, prefix, key);
				}
				logout(prefix, requestHeaders);
			}
		}
	}

	private void addCertInNS(List<QTSPResource> newQTSPs, HttpHeaders requestHeaders, String prefix, String key) {

		for (QTSPResource qtsp : newQTSPs) {
			QTSPServiceUtility.addDelay(nsCertDelay);
			performCookieCheck(requestHeaders, prefix, key);
			String uploadStatus = uploadCert(prefix, qtsp, requestHeaders);
			if (uploadStatus.equals(SUCCESS) || uploadStatus.equals(DUPLICATE_UPLOAD)) {
				performCookieCheck(requestHeaders, prefix, key);
				QTSPServiceUtility.addDelay(nsAPIDelay);
				String addStatus = addCertAsCACert(prefix, qtsp, requestHeaders);
				if (addStatus.equals(SUCCESS) || addStatus.equals(DUPLICATE_ADD)) {
					performCookieCheck(requestHeaders, prefix, key);
					QTSPServiceUtility.addDelay(nsAPIDelay);
					bindCACert(prefix, qtsp, requestHeaders, key);
				} else {
					LOGGER.error("Failed to \"Add\" Certificate with Fingerprint \"{}\" in NS, instance : \"{}{}\" ",
							qtsp.getCertificateFingerprint(), prefix, nsConfig.getAddCertAsCACert());
					utility.logFailure(qtsp, prefix, nsConfig.getAddCertAsCACert(), key,
							QTSPServiceUtility.NS_FAILURE_STAGE_ADD);
				}
			} else {
				LOGGER.error("Failed to \"Upload\" Certificate with Fingerprint \"{}\" in NS, instance : \"{}{}\" ",
						qtsp.getCertificateFingerprint(), prefix, nsConfig.getUploadCert());
				utility.logFailure(qtsp, prefix, nsConfig.getUploadCert(), key,
						QTSPServiceUtility.NS_FAILURE_STAGE_ADD);
			}
		}
	}

	private void removeCertFromNS(List<QTSPResource> updatedQTSPs, HttpHeaders requestHeaders, String prefix,
			String key) {

		boolean unbindStatus;
		for (QTSPResource qtsp : updatedQTSPs) {
			QTSPServiceUtility.addDelay(nsCertDelay);
			performCookieCheck(requestHeaders, prefix, key);
			unbindStatus = unbindCACert(prefix, new ArrayList<String>(nsConfig.getVserverName().values()), qtsp,
					requestHeaders, key);
			String removeCA = null;
			if (unbindStatus) {
				QTSPServiceUtility.addDelay(nsAPIDelay);
				performCookieCheck(requestHeaders, prefix, key);
				removeCA = removeCertAsCACert(prefix, qtsp.getId().substring(0, 20), requestHeaders);
			}
			if (unbindStatus && (REMOVE_CA_NOT_FOUND.equals(removeCA) || REMOVE_SUCCESS.equals(removeCA))) {
				QTSPServiceUtility.addDelay(nsAPIDelay);
				performCookieCheck(requestHeaders, prefix, key);
				String removeCert = removeCert(prefix, qtsp.getId().substring(0, 20), requestHeaders);
				if (!REMOVE_SUCCESS.equals(removeCert)) {
					LOGGER.error("Failed to \"Remove\" Certificate with Fingerprint \"{}\" in NS, instance : \"{}{}\" ",
							qtsp.getCertificateFingerprint(), prefix, nsConfig.getRemoveCert());
					utility.logFailure(qtsp, prefix, nsConfig.getRemoveCert(), key,
							QTSPServiceUtility.NS_FAILURE_STAGE_REMOVE);
				}
			} else {
				LOGGER.error("Failed to \"Remove CA\" Certificate with Fingerprint \"{}\" in NS, instance : \"{}{}\" ",
						qtsp.getCertificateFingerprint(), prefix, nsConfig.getRemoveCertAsCACert());
				utility.logFailure(qtsp, prefix, nsConfig.getRemoveCertAsCACert(), key,
						QTSPServiceUtility.NS_FAILURE_STAGE_REMOVE);
			}
		}
	}

	@SuppressWarnings("unchecked")
	private String login(String prefix, String username, String password, HttpHeaders requestHeaders) {

		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(prefix + nsConfig.getLogin());
		Map<String, String> data = new LinkedHashMap<>();
		data.put("username", username);
		data.put("password", password);
		Map<String, Map<String, String>> login = new HashMap<>();
		login.put("login", data);
		Map<String, String> loginResponse;
		try {
			restClientSync.buildSSLRequest(null);
			loginResponse = restClientSync.callForPost(requestInfo, login, Map.class, requestHeaders);
		} catch (PSD2Exception e) {
			LOGGER.error("Failed to Login to NS, Exception : {}", e.getMessage());
			return "";
		}
		loginTime = LocalTime.now();
		return loginResponse.get("sessionid");
	}

	@SuppressWarnings("unchecked")
	private String uploadCert(String prefix, QTSPResource qtsp, HttpHeaders requestHeaders) {

		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(prefix + nsConfig.getUploadCert());
		Map<String, String> data = new LinkedHashMap<>();
		data.put("filename", qtsp.getId().substring(0, 20) + CERT_EXTENSION);
		data.put("filecontent", Base64.getEncoder()
				.encodeToString(QTSPServiceUtility.parseForNS(qtsp.getX509Certificate()).getBytes()));
		data.put("filelocation", nsConfig.getFileLocation());
		data.put("fileencoding", nsConfig.getFileEncoding());
		Map<String, Map<String, String>> systemfile = new HashMap<>();
		systemfile.put("systemfile", data);
		try {
			restClientSync.buildSSLRequest(null);
			restClientSync.callForPost(requestInfo, systemfile, Map.class, requestHeaders);
		} catch (PSD2Exception e) {
			LOGGER.info("Failed to Upload Certificate to NS, Exception : {}", e.getMessage());
			try {
				Map<String, Object> error = JSONUtilities.getObjectFromJSONString(e.getMessage(), Map.class);
				return error.get(ERROR_CODE).toString();
			} catch (Exception e1) {
				LOGGER.error("Could not parse NetScaler error, Exception : {}", e1.getMessage());
				return FAILURE;
			}
		}
		return SUCCESS;
	}

	@SuppressWarnings("unchecked")
	private String addCertAsCACert(String prefix, QTSPResource qtsp, HttpHeaders requestHeaders) {

		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(prefix + nsConfig.getAddCertAsCACert());
		Map<String, String> data = new LinkedHashMap<>();
		data.put("certkey", qtsp.getId().substring(0, 20));
		data.put("cert", qtsp.getId().substring(0, 20) + CERT_EXTENSION);
		data.put("expirymonitor", "ENABLED");
		data.put("notificationperiod", "30");
		Map<String, Map<String, String>> sslcertkey = new HashMap<>();
		sslcertkey.put("sslcertkey", data);
		try {
			restClientSync.buildSSLRequest(null);
			restClientSync.callForPost(requestInfo, sslcertkey, Map.class, requestHeaders);
		} catch (PSD2Exception e) {
			LOGGER.info("Failed to Add Certificate as CA Cert, Exception : {}", e.getMessage());
			try {
				Map<String, Object> error = JSONUtilities.getObjectFromJSONString(e.getMessage(), Map.class);
				return error.get(ERROR_CODE).toString();
			} catch (Exception e1) {
				LOGGER.error("Could not parse NetScaler error, Exception: {}", e1.getMessage());
				return FAILURE;
			}
		}
		return SUCCESS;
	}

	@SuppressWarnings("unchecked")
	private void bindCACert(String prefix, QTSPResource qtsp, HttpHeaders requestHeaders, String key) {

		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(prefix + nsConfig.getBindCACert());
		Map<String, String> data = new LinkedHashMap<>();
		data.put("certkeyname", qtsp.getId().substring(0, 20));
		data.put("ocspcheck", "OPTIONAL");
		data.put("skipcaname", "false");
		data.put("ca", "true");
		Map<String, Map<String, String>> sslVserver = new HashMap<>();
		sslVserver.put("sslvserver_sslcertkey_binding", data);
		Set<String> vservers = new HashSet<>();
		if (utility.isUploadCertRequest())
			vservers.add(nsConfig.getVserverName().get(reqHeaderAttribute.getTenantId()));
		else
			vservers.addAll(nsConfig.getVserverName().values());
		for (String vserverName : vservers) {
			data.put("vservername", vserverName);
			try {
				restClientSync.buildSSLRequest(null);
				restClientSync.callForPost(requestInfo, sslVserver, Map.class, requestHeaders);
				LOGGER.error("Successful : \"Bind\" Certificate with Fingerprint \"{}\" in NS, instance : \"{}{}:{}\"",
						qtsp.getCertificateFingerprint(), prefix, nsConfig.getBindCACert(), vserverName);
				QTSPServiceUtility.addDelay(nsAPIDelay);
				saveCert(prefix, vserverName, qtsp, requestHeaders, key);

			} catch (PSD2Exception e) {
				try {
					Map<String, Object> error = JSONUtilities.getObjectFromJSONString(e.getMessage(), Map.class);
					if (!error.get(ERROR_CODE).toString().equals(DUPLICATE_BIND)) {
						LOGGER.error(
								"Failed to \"Bind\" Certificate with Fingerprint \"{}\" in NS, instance : \"{}{}:{}\", Exception : {}",
								qtsp.getCertificateFingerprint(), prefix, nsConfig.getBindCACert(), vserverName,
								e.getMessage());
						utility.logFailure(qtsp, prefix, nsConfig.getBindCACert(), key,
								QTSPServiceUtility.NS_FAILURE_STAGE_ADD, vserverName);
						QTSPServiceUtility.addDelay(nsAPIDelay);
						removeCertAsCACert(prefix, qtsp.getId().substring(0, 20), requestHeaders);
						QTSPServiceUtility.addDelay(nsAPIDelay);
						saveCert(prefix, vserverName, qtsp, requestHeaders, key);
					}
				} catch (Exception e1) {
					LOGGER.error("Could not parse NetScaler error, Exception :{}", e1.getMessage());
					LOGGER.error("Failed to \"Bind\" Certificate with Fingerprint \"{}\" in NS, instance : \"{}\"",
							qtsp.getCertificateFingerprint(), prefix + nsConfig.getBindCACert() + ":" + vserverName);
					utility.logFailure(qtsp, prefix, nsConfig.getBindCACert(), key,
							QTSPServiceUtility.NS_FAILURE_STAGE_ADD, vserverName);
					QTSPServiceUtility.addDelay(nsAPIDelay);
					removeCertAsCACert(prefix, qtsp.getId().substring(0, 20), requestHeaders);
					QTSPServiceUtility.addDelay(nsAPIDelay);
					saveCert(prefix, vserverName, qtsp, requestHeaders, key);
				}
			}
		}
	}

	private void saveCert(String prefix, String vserverName, QTSPResource qtsp, HttpHeaders requestHeaders,
			String key) {
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(prefix + nsConfig.getSaveUrl());
		Map<String, Map<String, String>> nsConfigMap = new HashMap<>();
		nsConfigMap.put("nsconfig", new HashMap<>());
		try {
			restClientSync.buildSSLRequest(null);
			
			restClientSync.callForPost(requestInfo, nsConfigMap, Map.class, requestHeaders);
		} catch (PSD2Exception e) {
			LOGGER.error(
					"Failed to \"Save\" Certificate with Fingerprint \"{}\" in NS, instance : \"{}\", Exception : {}",
					qtsp.getCertificateFingerprint(), prefix + nsConfig.getSaveUrl() + ":" + vserverName,
					e.getMessage());
			utility.logFailure(qtsp, prefix, nsConfig.getSaveUrl(), key, QTSPServiceUtility.NS_FAILURE_STAGE_ADD,
					vserverName);
		}
	}

	@SuppressWarnings("unchecked")
	private boolean unbindCACert(String prefix, List<String> vserverNames, QTSPResource qtsp,
			HttpHeaders requestHeaders, String key) {

		boolean result = true;
		RequestInfo requestInfo = new RequestInfo();
		for (String vserverName : vserverNames) {
			requestInfo.setUrl(prefix + nsConfig.getUnbindCACert() + vserverName + "?args=ca:true,certkeyname:"
					+ qtsp.getId().substring(0, 20));
			try {
				restClientSync.buildSSLRequest(null);
				Map<String, Object> resultMap;
				resultMap = restClientSync.callForDelete(requestInfo, Map.class, requestHeaders);
				if (null != resultMap && !UNBIND_NOT_FOUND.equals(resultMap.get(ERROR_CODE).toString())
						&& !REMOVE_SUCCESS.equals(resultMap.get(ERROR_CODE).toString())) {
					result = false;
					break;
				}
			} catch (PSD2Exception e) {
				result = false;
				logUnbindFailure(prefix, vserverNames, qtsp, key, e);
				break;
			}
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	private String removeCertAsCACert(String prefix, String certkey, HttpHeaders requestHeaders) {
		String result = REMOVE_SUCCESS;
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(prefix + nsConfig.getRemoveCertAsCACert() + certkey);
		try {
			restClientSync.buildSSLRequest(null);
			Map<String, Object> resultMap;
			resultMap = restClientSync.callForDelete(requestInfo, Map.class, requestHeaders);
			if (null != resultMap && REMOVE_CA_NOT_FOUND.equals(resultMap.get(ERROR_CODE).toString()))
				result = REMOVE_CA_NOT_FOUND;
		} catch (PSD2Exception e) {
			LOGGER.error("Exception : {}", e.getMessage());
			result = FAILURE;
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	private String removeCert(String prefix, String certkey, HttpHeaders requestHeaders) {
		String result = REMOVE_SUCCESS;
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(prefix + nsConfig.getRemoveCert() + certkey + CERT_EXTENSION);
		try {
			restClientSync.buildSSLRequest(null);
			Map<String, Object> resultMap;
			resultMap = restClientSync.callForDelete(requestInfo, Map.class, requestHeaders);
			if (null != resultMap && REMOVE_CERT_NOT_FOUND.equals(resultMap.get(ERROR_CODE).toString()))
				result = REMOVE_CERT_NOT_FOUND;
		} catch (PSD2Exception e) {
			LOGGER.error("Exception : {}", e.getMessage());
			result = FAILURE;
		}
		return result;
	}

	private void logout(String prefix, HttpHeaders requestHeaders) {

		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(prefix + nsConfig.getLogout());
		Map<String, String> data = new LinkedHashMap<>();
		Map<String, Map<String, String>> logout = new HashMap<>();
		logout.put("logout", data);
		try {
			restClientSync.buildSSLRequest(null);
			restClientSync.callForPost(requestInfo, logout, Map.class, requestHeaders);
		} catch (PSD2Exception e) {
			LOGGER.error("Failed to Logout from NS, Exception : {}", e);
		}
	}

	private void logUnbindFailure(String prefix, List<String> vserverNames, QTSPResource qtsp, String key,
			Exception e) {
		for (String vserverName : vserverNames) {
			LOGGER.error(
					"Failed to \"Unbind\" Certificate with Fingerprint \"{}\" in NS, instance : \"{}{}:{}\", Exception : {} ",
					qtsp.getCertificateFingerprint(), prefix, nsConfig.getUnbindCACert(), vserverName, e.getMessage());
			utility.logFailure(qtsp, prefix, nsConfig.getUnbindCACert(), key,
					QTSPServiceUtility.NS_FAILURE_STAGE_REMOVE, vserverName);
		}
	}

	private void performCookieCheck(HttpHeaders requestHeaders, String prefix, String key) {
		if (ChronoUnit.SECONDS.between(loginTime,
				LocalTime.now()) >= (Long.parseLong(nsConfig.getTimeout()) - TIMEOUT_BUFFER)) {
			logout(prefix, requestHeaders);
			String username = nsConfig.getUsername().get(key);
			String password = nsConfig.getPassword().get(key);
			requestHeaders.remove(COOKIE);
			String nitroAuthToken = login(prefix, username, password, requestHeaders);
			if (!nitroAuthToken.isEmpty())
				requestHeaders.add(COOKIE, NITRO_AUTH_TOKEN + nitroAuthToken);
		}
	}

	private HttpHeaders populateHttpHeaders() {

		HttpHeaders requestHeaders = new HttpHeaders();
		requestHeaders.setContentType(new MediaType("application", "json"));

		return requestHeaders;
	}
}
