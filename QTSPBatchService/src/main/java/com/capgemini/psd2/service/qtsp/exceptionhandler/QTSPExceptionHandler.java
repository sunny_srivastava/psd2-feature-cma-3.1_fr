package com.capgemini.psd2.service.qtsp.exceptionhandler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.ResourceAccessException;

import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.ErrorInfo;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.LoggerAttribute;
import com.capgemini.psd2.rest.client.exception.handler.ExceptionHandler;
import com.capgemini.psd2.utilities.JSONUtilities;

public class QTSPExceptionHandler implements ExceptionHandler {

	@Autowired
	LoggerAttribute loggerAttribute;

	@Override
	public void handleException(HttpServerErrorException e) {

		String responseBody = e.getResponseBodyAsString();
		if (responseBody == null || responseBody.isEmpty()) {
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.TECHNICAL_ERROR);
		}
		ErrorInfo errorInfoObj = JSONUtilities.getObjectFromJSONString(responseBody, ErrorInfo.class);
		errorInfoObj.setStatusCode(String.valueOf(e.getStatusCode().value()));
		throw new PSD2Exception(errorInfoObj.getDetailErrorMessage(), errorInfoObj);
	}

	@Override
	public void handleException(HttpClientErrorException e) {

		String responseBody = e.getResponseBodyAsString();
		if (responseBody == null || responseBody.isEmpty()) {
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.TECHNICAL_ERROR);
		}
		throw PSD2Exception.populatePSD2Exception(e.getResponseBodyAsString(), ErrorCodeEnum.TECHNICAL_ERROR);
	}

	@Override
	public void handleException(ResourceAccessException e) {

		throw PSD2Exception.populatePSD2Exception(e.getMessage(), ErrorCodeEnum.TECHNICAL_ERROR);
	}

}
