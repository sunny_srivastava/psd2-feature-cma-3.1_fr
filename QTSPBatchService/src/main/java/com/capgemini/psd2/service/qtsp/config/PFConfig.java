package com.capgemini.psd2.service.qtsp.config;

import java.util.HashMap;
import java.util.Map;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties("qtspservice.writer.pingfederate")
public class PFConfig {

	private Map<String, String> adminUser = new HashMap<>();

	private Map<String, String> adminPwd = new HashMap<>();

	private Map<String, String> prefix = new HashMap<>();

	private String caImportUrl;

	private String caRemoveUrl;

	private String replicateUrl;

	public Map<String, String> getAdminUser() {
		return adminUser;
	}

	public void setAdminUser(Map<String, String> adminUser) {
		this.adminUser = adminUser;
	}

	public Map<String, String> getAdminPwd() {
		return adminPwd;
	}

	public void setAdminPwd(Map<String, String> adminPwd) {
		this.adminPwd = adminPwd;
	}

	public Map<String, String> getPrefix() {
		return prefix;
	}

	public void setPrefix(Map<String, String> prefix) {
		this.prefix = prefix;
	}

	public String getCaImportUrl() {
		return caImportUrl;
	}

	public void setCaImportUrl(String caImportUrl) {
		this.caImportUrl = caImportUrl;
	}

	public String getCaRemoveUrl() {
		return caRemoveUrl;
	}

	public void setCaRemoveUrl(String caRemoveUrl) {
		this.caRemoveUrl = caRemoveUrl;
	}

	public String getReplicateUrl() {
		return replicateUrl;
	}

	public void setReplicateUrl(String replicateUrl) {
		this.replicateUrl = replicateUrl;
	}
}