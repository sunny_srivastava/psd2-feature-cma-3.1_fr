package com.capgemini.psd2.application.qtsp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.scheduling.annotation.Scheduled;

import com.capgemini.psd2.exceptions.OBPSD2ExceptionUtility;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.RestClientSync;
import com.netflix.discovery.EurekaClient;
import com.netflix.discovery.shared.Application;

@EnableEurekaClient
@SpringBootApplication
@ComponentScan(basePackages = { "com.capgemini.psd2" }, excludeFilters = {
		@ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, value = { OBPSD2ExceptionUtility.class }) })
@EnableMongoRepositories(basePackages = { "com.capgemini.psd2" })
@EnableBatchProcessing
public class QTSPBatchServiceApplication {

	/** The context. */
	static ConfigurableApplicationContext context = null;

	private static final Logger LOGGER = LoggerFactory.getLogger(QTSPBatchServiceApplication.class);

	@Autowired
	private RestClientSync restClientSync;

	@Value("${api.batchProcessUrl}")
	private String batchProcessUrl;

	@Value("${spring.application.name}")
	private String serviceName;

	@Autowired
	private EurekaClient eurekaClient;

	public static void main(String[] args) {
		try {
			context = SpringApplication.run(QTSPBatchServiceApplication.class, args);
		} catch (Exception e) {
			if (context != null) {
				SpringApplication.exit(context, () -> 1);
			}
			System.exit(1);
		}
	}

	@Scheduled(cron = "${scheduler.cron.expression}")
	public void perform() {
		LOGGER.info("{\"Enter\":\"{}\"}", "com.capgemini.psd2.application.qtsp.QTSPServiceApplication.perform()");
		Application application = eurekaClient.getApplication(serviceName);
		if (null != application) {
			RequestInfo requestInfo = new RequestInfo();
			requestInfo.setUrl(batchProcessUrl);
			HttpHeaders requestHeaders = new HttpHeaders();
			requestHeaders.setContentType(new MediaType("application", "json"));
			LOGGER.info("Generating an HTTP Get Request: {}", batchProcessUrl);
			restClientSync.buildSSLRequest(null);
			restClientSync.callForGet(requestInfo, String.class, requestHeaders);
			LOGGER.info("Request executed");
		} else
			LOGGER.error("\"{}\" instance not found in eureka, skipping the current job", serviceName);
	}

}