<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<groupId>com.capgemini.psd2</groupId>
	<artifactId>InternationalScheduledPaymentConsents</artifactId>
	<packaging>jar</packaging>
	<name>InternationalScheduledPaymentConsents</name>
	<version>2105</version>
	<parent>
		<groupId>org.springframework.boot</groupId>
		<artifactId>spring-boot-starter-parent</artifactId>
		<version>1.5.9.RELEASE</version>
	</parent>

	<dependencyManagement>
		<dependencies>
			<dependency>
				<groupId>org.springframework.cloud</groupId>
				<artifactId>spring-cloud-dependencies</artifactId>
				<version>Edgware.RELEASE</version>
				<type>pom</type>
				<scope>import</scope>
			</dependency>
		</dependencies>
	</dependencyManagement>


	<properties>
		<java.version>1.8</java.version>
		<mongodb.version>3.3.0</mongodb.version>
		<sonar.jacoco.reportPath>${project.basedir}/../coverage-reports/jacoco.exec</sonar.jacoco.reportPath>
		<sonar.exclusions>
			**/config/*,
			**/ISPConsentsApplication.*,
			**/ISPConsentsPayloadComparator.java,
			**/ISPConsentsApi.*
		</sonar.exclusions>
		
		<spring-retry.version>1.2.0.RELEASE</spring-retry.version>
	</properties>

	<dependencies>
	
		<!-- Domain Dependency -->
		<dependency>
			<groupId>com.capgemini.psd2</groupId>
			<artifactId>CoreSystemAdapter</artifactId>
			<version>2105</version>
		</dependency>

		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-web</artifactId>
		</dependency>

		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-test</artifactId>
			<scope>test</scope>
		</dependency>

		<!-- For web integration specifically Restful -->
		<dependency>
			<groupId>org.springframework.retry</groupId>
			<artifactId>spring-retry</artifactId>
		</dependency>

		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId> spring-boot-starter-actuator</artifactId>
		</dependency>

		<dependency>
			<groupId>org.springframework.cloud</groupId>
			<artifactId>spring-cloud-starter-eureka</artifactId>
		</dependency>

		<dependency>
			<groupId>org.springframework.cloud</groupId>
			<artifactId>spring-cloud-starter-config</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-data-mongodb</artifactId>
		</dependency>

		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId> spring-boot-starter-actuator</artifactId>
		</dependency>

		<!-- For Security Response Header -->
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-security</artifactId>
		</dependency>

		<dependency>
			<groupId>com.fasterxml.jackson.datatype</groupId>
			<artifactId>jackson-datatype-jsr310</artifactId>
		</dependency>

		<!-- Bean Validation API support -->
		<dependency>
			<groupId>javax.validation</groupId>
			<artifactId>validation-api</artifactId>
		</dependency>

		<!-- PaymentSetupPlatformAdapter -->
		<dependency>
			<groupId>com.capgemini.psd2</groupId>
			<artifactId>PaymentSetupPlatformAdapter</artifactId>
			<version>2105</version>
		</dependency>

		<!-- Validation Adapter Dependency -->
		<dependency>
			<groupId>com.capgemini.psd2</groupId>
			<artifactId>PISPValidationAdapter</artifactId>
			<version>2105</version>
		</dependency>

		<!-- PSD2RestClient Dependency -->
		<dependency>
			<groupId>com.capgemini.psd2</groupId>
			<artifactId>PSD2RestClient</artifactId>
			<version>2105</version>
		</dependency>

		<!-- Dependency for AWS KMS -->
		<dependency>
			<groupId>com.capgemini.psd2</groupId>
			<artifactId>PSD2KMSUtilities</artifactId>
			<version>2105</version>
		</dependency>

		<!-- Dependency for PSD2 Utilities, for logging, exception -->
		<dependency>
			<groupId>com.capgemini.psd2</groupId>
			<artifactId>PSD2Utilities</artifactId>
			<version>2105</version>
		</dependency>

		<!-- ApiComponents Dependency -->
		<dependency>
			<groupId>com.capgemini.psd2</groupId>
			<artifactId>ApiComponents</artifactId>
			<version>2105</version>
		</dependency>



		<!-- Dependency for Mongo Db Adapter -->
		 <dependency>
			<groupId>com.capgemini.psd2</groupId>
			<artifactId>InternationalScheduledPaymentConsentsMongoDbAdapter</artifactId>
			<version>2105</version>
		</dependency> 
		
		<!-- Dependency for pisp processing project -->
		<dependency>
			<groupId>com.capgemini.psd2</groupId>
			<artifactId>PISPProcessingAdapter</artifactId>
			<version>2105</version>
		</dependency>
	</dependencies>
	<build>
		<plugins>
			<plugin>
				<groupId>org.springframework.boot</groupId>
				<artifactId>spring-boot-maven-plugin</artifactId>
				<executions>
					<execution>
						<goals>
							<goal>repackage</goal>
						</goals>
						<configuration>
							<mainclass>com.capgemini.psd2.pisp.ISPConsentsApplication</mainclass>
							<layout>ZIP</layout>
						</configuration>
					</execution>
				</executions>
			</plugin>
			
		<plugin>
				<groupId>org.jacoco</groupId>
				<artifactId>jacoco-maven-plugin</artifactId>
				<version>0.7.8</version>
				<configuration>
					<destFile>${sonar.jacoco.reportPath}</destFile>
					<dataFile>${sonar.jacoco.reportPath}</dataFile>
					<append>true</append>
				</configuration>
				<executions>
					<execution>
						<id>jacoco-initialize</id>
						<goals>
							<goal>prepare-agent</goal>
						</goals>
					</execution>
					<execution>
						<id>jacoco-site</id>
						<phase>package</phase>
						<goals>
							<goal>report</goal>
						</goals>
					</execution>
				</executions>
			</plugin>
		
		</plugins>
	</build>
</project>
