package com.capgemini.psd2.pisp.ispc.transformer.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.domain.CustomISPConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomISPConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.Links;
import com.capgemini.psd2.pisp.domain.Meta;
import com.capgemini.psd2.pisp.domain.OBExternalConsentStatus1Code;
import com.capgemini.psd2.pisp.domain.PaymentConsentsPlatformResource;
import com.capgemini.psd2.pisp.processing.adapter.PaymentConsentsTransformer;
import com.capgemini.psd2.pisp.status.PaymentStatusCompatibilityMapEnum;
import com.capgemini.psd2.pisp.utilities.PispDateUtility;
import com.capgemini.psd2.pisp.utilities.PispUtilities;
import com.capgemini.psd2.pisp.validation.adapter.constants.PaymentSetupConstants;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Component("internationalSchedulePaymentConsentTransformer")
public class ISPConsentsResponseTransformerImpl
		implements PaymentConsentsTransformer<CustomISPConsentsPOSTResponse, CustomISPConsentsPOSTRequest> {

	@Autowired
	private RequestHeaderAttributes reqHeaderAtrributes;

	@Autowired
	private PispDateUtility pispDateUtility;

	@Override
	public CustomISPConsentsPOSTResponse paymentConsentsResponseTransformer(
			CustomISPConsentsPOSTResponse internationalConsentsTppResponse,
			PaymentConsentsPlatformResource paymentSetupPlatformResource, String methodType) {
		/*
		 * These fields are expected to already be populated in domesticSetupTppResponse
		 * from Domestic Stage Service : Charges, CutOffDateTime,
		 * ExpectedExecutionDateTime, ExpectedSettlementDateTime
		 */

		/* Fields from platform Resource */
		internationalConsentsTppResponse.getData().setCreationDateTime(paymentSetupPlatformResource.getCreatedAt());
		String setupCompatibleStatus = PaymentStatusCompatibilityMapEnum
				.valueOf(paymentSetupPlatformResource.getStatus().toUpperCase()).getCompatibleStatus();

		internationalConsentsTppResponse.getData()
				.setStatus(OBExternalConsentStatus1Code.fromValue(setupCompatibleStatus));

		internationalConsentsTppResponse.getData().setStatusUpdateDateTime(
				NullCheckUtils.isNullOrEmpty(paymentSetupPlatformResource.getStatusUpdateDateTime())
						? paymentSetupPlatformResource.getUpdatedAt()
						: paymentSetupPlatformResource.getStatusUpdateDateTime());

		/*
		 * If debtorFlag is 'False' then DebtorAccount & DebtorAgent details do not have
		 * to send in response. debtorFlag would be set as Null DebtorAgent is not
		 * available in CMA3
		 */
		if (!Boolean.valueOf(paymentSetupPlatformResource.getTppDebtorDetails()))
			internationalConsentsTppResponse.getData().getInitiation().setDebtorAccount(null);

		/* Adding Prefix uk.obie. in creditor scheme in response */
		String creditorAccountScheme = internationalConsentsTppResponse.getData().getInitiation().getCreditorAccount()
				.getSchemeName();
		if (PaymentSetupConstants.prefixToAddSchemeNameList().contains(creditorAccountScheme)) {
			internationalConsentsTppResponse.getData().getInitiation().getCreditorAccount()
					.setSchemeName(PSD2Constants.OB_SCHEME_PREFIX.concat(creditorAccountScheme));
		}

		/* Adding Prefix uk.obie. in debtor scheme in response */
		if (!NullCheckUtils.isNullOrEmpty(internationalConsentsTppResponse.getData().getInitiation().getDebtorAccount())) {
			String debtorAccountScheme = internationalConsentsTppResponse.getData().getInitiation().getDebtorAccount()
					.getSchemeName();
			if (PaymentSetupConstants.prefixToAddSchemeNameList().contains(debtorAccountScheme)) {
				internationalConsentsTppResponse.getData().getInitiation().getDebtorAccount()
						.setSchemeName(PSD2Constants.OB_SCHEME_PREFIX.concat(debtorAccountScheme));
			}
		}

		
		/*
		 * If tppDebtorNameFlag is 'False' then DebtorAccount.name should not be sent to
		 * TPP.
		 */
		if (Boolean.valueOf(paymentSetupPlatformResource.getTppDebtorDetails())
				&& !Boolean.valueOf(paymentSetupPlatformResource.getTppDebtorNameDetails()))
			internationalConsentsTppResponse.getData().getInitiation().getDebtorAccount().setName(null);

		if (internationalConsentsTppResponse.getLinks() == null)
			internationalConsentsTppResponse.setLinks(new Links());

		internationalConsentsTppResponse.getLinks().setSelf(PispUtilities.populateLinks(
				paymentSetupPlatformResource.getPaymentConsentId(), methodType, reqHeaderAtrributes.getSelfUrl()));

		if (internationalConsentsTppResponse.getMeta() == null)
			internationalConsentsTppResponse.setMeta(new Meta());

		return internationalConsentsTppResponse;
	}

	@Override
	public CustomISPConsentsPOSTRequest paymentConsentRequestTransformer(
			CustomISPConsentsPOSTRequest paymentConsentRequest) {
		if (!NullCheckUtils
				.isNullOrEmpty(paymentConsentRequest.getData().getInitiation().getRequestedExecutionDateTime()))
			paymentConsentRequest.getData().getInitiation()
					.setRequestedExecutionDateTime(pispDateUtility.transformDateTimeInRequest(
							paymentConsentRequest.getData().getInitiation().getRequestedExecutionDateTime()));
		if (!NullCheckUtils.isNullOrEmpty(paymentConsentRequest.getData().getAuthorisation()) && !NullCheckUtils
				.isNullOrEmpty(paymentConsentRequest.getData().getAuthorisation().getCompletionDateTime()))
			paymentConsentRequest.getData().getAuthorisation()
					.setCompletionDateTime(pispDateUtility.transformDateTimeInRequest(
							paymentConsentRequest.getData().getAuthorisation().getCompletionDateTime()));
		return paymentConsentRequest;
	}

}