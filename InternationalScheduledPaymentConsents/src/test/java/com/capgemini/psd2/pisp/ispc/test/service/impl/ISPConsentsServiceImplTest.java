package com.capgemini.psd2.pisp.ispc.test.service.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.domain.CustomISPConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomISPConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.CustomPaymentSetupPlatformDetails;
import com.capgemini.psd2.pisp.domain.GenericPaymentConsentResponse;
import com.capgemini.psd2.pisp.domain.OBInternationalScheduled1;
import com.capgemini.psd2.pisp.domain.OBRemittanceInformation1;
import com.capgemini.psd2.pisp.domain.OBRisk1;
import com.capgemini.psd2.pisp.domain.OBRisk1DeliveryAddress;
import com.capgemini.psd2.pisp.domain.OBWriteDataInternationalScheduledConsent1;
import com.capgemini.psd2.pisp.domain.OBWriteDataInternationalScheduledConsentResponse1;
import com.capgemini.psd2.pisp.domain.PaymentConsentsPlatformResource;
import com.capgemini.psd2.pisp.enums.ProcessConsentStatusEnum;
import com.capgemini.psd2.pisp.ispc.comparator.ISPConsentsPayloadComparator;
import com.capgemini.psd2.pisp.ispc.service.impl.ISPConsentsServiceImpl;
import com.capgemini.psd2.pisp.processing.adapter.service.PaymentConsentProcessingAdapter;
import com.capgemini.psd2.pisp.stage.operations.adapter.InternationalScheduledPaymentStagingAdapter;

@RunWith(SpringJUnit4ClassRunner.class)
public class ISPConsentsServiceImplTest {

	@Mock
	private ISPConsentsPayloadComparator iScheduledPaymentConsentsComparator;

	@Mock
	private InternationalScheduledPaymentStagingAdapter ispStagingAdapter;

	@Mock
	private RequestHeaderAttributes reqHeaderAttributes;
	
	@Mock
	private PaymentConsentProcessingAdapter paymemtRequestAdapter;

	@InjectMocks
	private ISPConsentsServiceImpl service;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void contextLoads() {
	}

	@Test
	public void testCreateInternationalScheduledPaymentConsentsResource_WithNullPaymentId() {
		PaymentConsentsPlatformResource resource = new PaymentConsentsPlatformResource();
		resource.setPaymentConsentId(null);
		resource.setCreatedAt("2019-08-06T06:06:06.666+00:00");

		GenericPaymentConsentResponse value = new GenericPaymentConsentResponse();
		value.setConsentId("1423");

		CustomISPConsentsPOSTResponse response = new CustomISPConsentsPOSTResponse();
		response.setData(new OBWriteDataInternationalScheduledConsentResponse1());
		response.getData().setInitiation(new OBInternationalScheduled1());

		CustomISPConsentsPOSTRequest request = new CustomISPConsentsPOSTRequest();
		request.setData(new OBWriteDataInternationalScheduledConsent1());
		request.getData().setInitiation(new OBInternationalScheduled1());
		OBRemittanceInformation1 remittanceInfo = new OBRemittanceInformation1();
		request.getData().getInitiation().setRemittanceInformation(remittanceInfo);
		request.getData().getInitiation().getRemittanceInformation().setReference("1");
		response.getData().getInitiation().setRemittanceInformation(remittanceInfo);
		OBRisk1 requestRisk = new OBRisk1();
		requestRisk.setDeliveryAddress(new OBRisk1DeliveryAddress());
		requestRisk.getDeliveryAddress().buildingNumber("1");
		request.setRisk(requestRisk);
		response.setRisk(requestRisk);
		
		Mockito.when(paymemtRequestAdapter.preConsentProcessFlows(anyObject(), anyObject())).thenReturn(resource);

		Mockito.when(paymemtRequestAdapter.postConsentProcessFlows(anyObject(), anyObject(), anyObject(), anyObject()))
				.thenReturn(response);

		Mockito.when(ispStagingAdapter.processInternationalScheduledPaymentConsents(anyObject(), anyObject(),
				anyObject(), anyObject(), anyObject())).thenReturn(value);

		service.createInternationalScheduledPaymentConsentsResource(request);
		assertNull(resource.getPaymentConsentId());
	}

	@Test
	public void testCreateInternationalScheduledPaymentConsentsResource_WithPaymentId_TrueDebtorDetails() {
		PaymentConsentsPlatformResource resource = new PaymentConsentsPlatformResource();
		resource.setPaymentConsentId("1458");
		resource.setCreatedAt("2019-08-06T06:06:06.666+00:00");

		CustomISPConsentsPOSTResponse response = new CustomISPConsentsPOSTResponse();
		response.setData(new OBWriteDataInternationalScheduledConsentResponse1());
		response.getData().setInitiation(new OBInternationalScheduled1());
		response.setConsentPorcessStatus(ProcessConsentStatusEnum.PASS);

		CustomISPConsentsPOSTRequest request = new CustomISPConsentsPOSTRequest();
		request.setData(new OBWriteDataInternationalScheduledConsent1());
		request.getData().setInitiation(new OBInternationalScheduled1());
		OBRemittanceInformation1 remittanceInfo = new OBRemittanceInformation1();
		request.getData().getInitiation().setRemittanceInformation(remittanceInfo);
		request.getData().getInitiation().getRemittanceInformation().setReference("1");
		response.getData().getInitiation().setRemittanceInformation(remittanceInfo);
		OBRisk1 requestRisk = new OBRisk1();
		requestRisk.setDeliveryAddress(new OBRisk1DeliveryAddress());
		requestRisk.getDeliveryAddress().buildingNumber("1");
		request.setRisk(requestRisk);
		response.setRisk(requestRisk);

		CustomPaymentSetupPlatformDetails platformDetails = new CustomPaymentSetupPlatformDetails();
		platformDetails.setTppDebtorDetails("true");
		platformDetails.setTppDebtorNameDetails("true");

		Mockito.when(paymemtRequestAdapter.preConsentProcessFlows(anyObject(), anyObject())).thenReturn(resource);

		Mockito.when(ispStagingAdapter.retrieveStagedInternationalScheduledPaymentConsents(anyObject(), anyObject()))
				.thenReturn(response);

		Mockito.when(paymemtRequestAdapter.postConsentProcessFlows(anyObject(), anyObject(), anyObject(), anyObject()))
				.thenReturn(response);

		service.createInternationalScheduledPaymentConsentsResource(request);
		assertNotNull(resource.getPaymentConsentId());
	}

	@Test(expected = PSD2Exception.class)
	public void testCreateInternationalScheduledPaymentConsentsResource_WithPaymentId_FalseDebtorDetails_ComparatorException() {
		PaymentConsentsPlatformResource resource = new PaymentConsentsPlatformResource();
		resource.setPaymentConsentId("1458");
		resource.setCreatedAt("2019-08-06T06:06:06.666+00:00");

		CustomISPConsentsPOSTResponse response = new CustomISPConsentsPOSTResponse();
		response.setData(new OBWriteDataInternationalScheduledConsentResponse1());
		response.getData().setConsentId("14563");
		response.setConsentPorcessStatus(ProcessConsentStatusEnum.PASS);

		CustomISPConsentsPOSTRequest request = new CustomISPConsentsPOSTRequest();
		request.setData(new OBWriteDataInternationalScheduledConsent1());
		request.getData().setInitiation(new OBInternationalScheduled1());
		request.getData().getInitiation().setDebtorAccount(null);

		CustomPaymentSetupPlatformDetails platformDetails = new CustomPaymentSetupPlatformDetails();
		platformDetails.setTppDebtorDetails("false");
		platformDetails.setTppDebtorNameDetails("false");

		Mockito.when(paymemtRequestAdapter.preConsentProcessFlows(anyObject(), anyObject())).thenReturn(resource);

		Mockito.when(ispStagingAdapter.retrieveStagedInternationalScheduledPaymentConsents(anyObject(), anyObject()))
				.thenReturn(response);

		Mockito.when(paymemtRequestAdapter.postConsentProcessFlows(anyObject(), anyObject(), anyObject(), anyObject()))
				.thenReturn(response);

		Mockito.when(iScheduledPaymentConsentsComparator.compareScheduledPaymentDetails(response, request, resource))
				.thenReturn(1);
		service.createInternationalScheduledPaymentConsentsResource(request);
		assertNotNull(resource.getPaymentConsentId());
		assertEquals(1,
				iScheduledPaymentConsentsComparator.compareScheduledPaymentDetails(response, request, resource));
	}

	@Test
	public void testRetrieveInternationalScheduledPaymentConsentsResource() {
		PaymentConsentsPlatformResource resource = new PaymentConsentsPlatformResource();
		resource.setSetupCmaVersion(PSD2Constants.CMAVERSION);

		CustomISPConsentsPOSTResponse response = new CustomISPConsentsPOSTResponse();
		response.setData(new OBWriteDataInternationalScheduledConsentResponse1());
		response.getData().setConsentId("14563");

		Mockito.when(paymemtRequestAdapter.preConsentProcessGETFlow(anyObject())).thenReturn(resource);

		Mockito.when(ispStagingAdapter.retrieveStagedInternationalScheduledPaymentConsents(anyObject(), anyObject()))
				.thenReturn(response);

		Mockito.when(paymemtRequestAdapter.postConsentProcessFlows(anyObject(), anyObject(), anyObject(), anyObject()))
				.thenReturn(response);

		service.retrieveInternationalScheduledPaymentConsentsResource(anyString());
		assertNotNull(resource.getSetupCmaVersion());
	}

	@Test
	public void testRetrieveInternationalScheduledPaymentConsentsResource_NullCMAVersion() {
		PaymentConsentsPlatformResource resource = new PaymentConsentsPlatformResource();
		resource.setSetupCmaVersion(null);

		CustomISPConsentsPOSTResponse response = new CustomISPConsentsPOSTResponse();
		response.setData(new OBWriteDataInternationalScheduledConsentResponse1());
		response.getData().setConsentId("14563");

		Mockito.when(paymemtRequestAdapter.preConsentProcessGETFlow(anyObject())).thenReturn(resource);

		Mockito.when(ispStagingAdapter.retrieveStagedInternationalScheduledPaymentConsents(anyObject(), anyObject()))
				.thenReturn(response);

		Mockito.when(paymemtRequestAdapter.postConsentProcessFlows(anyObject(), anyObject(), anyObject(), anyObject()))
				.thenReturn(response);

		service.retrieveInternationalScheduledPaymentConsentsResource(anyString());
		assertNull(resource.getSetupCmaVersion());
	}

	@Test(expected = PSD2Exception.class)
	public void testRetrieveScheduledDomesticPaymentConsentsResource_Exception() {
		PaymentConsentsPlatformResource resource = new PaymentConsentsPlatformResource();
		resource.setSetupCmaVersion(PSD2Constants.CMAVERSION);

		CustomISPConsentsPOSTResponse response = new CustomISPConsentsPOSTResponse();
		response.setData(new OBWriteDataInternationalScheduledConsentResponse1());
		response.getData().setConsentId("14563");

		Mockito.when(paymemtRequestAdapter.preConsentProcessGETFlow(anyObject())).thenReturn(resource);

		Mockito.when(ispStagingAdapter.retrieveStagedInternationalScheduledPaymentConsents(anyObject(), anyObject()))
				.thenReturn(null);

		Mockito.when(paymemtRequestAdapter.postConsentProcessFlows(anyObject(), anyObject(), anyObject(), anyObject()))
				.thenReturn(response);

		service.retrieveInternationalScheduledPaymentConsentsResource(anyString());
	}

	@After
	public void tearDown() {
		iScheduledPaymentConsentsComparator = null;
		ispStagingAdapter = null;
		paymemtRequestAdapter = null;
		service = null;
	}
}
