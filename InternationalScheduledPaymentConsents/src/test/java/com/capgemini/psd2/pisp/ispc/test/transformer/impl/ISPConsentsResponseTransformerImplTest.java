package com.capgemini.psd2.pisp.ispc.test.transformer.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.bind.annotation.RequestMethod;

import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.domain.CustomISPConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomISPConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.OBAuthorisation1;
import com.capgemini.psd2.pisp.domain.OBCashAccountCreditor2;
import com.capgemini.psd2.pisp.domain.OBCashAccountDebtor3;
import com.capgemini.psd2.pisp.domain.OBExternalConsentStatus1Code;
import com.capgemini.psd2.pisp.domain.OBInternationalScheduled1;
import com.capgemini.psd2.pisp.domain.OBWriteDataInternationalScheduledConsent1;
import com.capgemini.psd2.pisp.domain.OBWriteDataInternationalScheduledConsentResponse1;
import com.capgemini.psd2.pisp.domain.PaymentConsentsPlatformResource;
import com.capgemini.psd2.pisp.domain.PaymentSetupPOSTResponseLinks;
import com.capgemini.psd2.pisp.domain.PaymentSetupPOSTResponseMeta;
import com.capgemini.psd2.pisp.ispc.transformer.impl.ISPConsentsResponseTransformerImpl;
import com.capgemini.psd2.pisp.utilities.PispDateUtility;

@RunWith(SpringJUnit4ClassRunner.class)
public class ISPConsentsResponseTransformerImplTest {

	@Mock
	private RequestHeaderAttributes reqHeaderAtrributes;

	@Mock
	private PispDateUtility pispDateUtility;

	@InjectMocks
	private ISPConsentsResponseTransformerImpl transformer;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void contextLoads() {
	}

	@Test
	public void testPaymentConsentsResponseTransformer() {
		CustomISPConsentsPOSTResponse response = new CustomISPConsentsPOSTResponse();
		PaymentConsentsPlatformResource resource = new PaymentConsentsPlatformResource();
		String methodType = RequestMethod.POST.toString();

		response.setData(new OBWriteDataInternationalScheduledConsentResponse1());
		response.getData().setCreationDateTime("2017-06-05T15:15:13+00:00");
		response.getData().setInitiation(new OBInternationalScheduled1());
		response.getData().getInitiation().setCreditorAccount(new OBCashAccountCreditor2());
		response.getData().getInitiation().setDebtorAccount(new OBCashAccountDebtor3());

		resource.setStatus("ACCEPTEDTECHNICALVALIDATION");
		resource.setStatusUpdateDateTime("2017-06-05T15:18:13+00:00");
		resource.setCreatedAt("2017-06-05T15:15:13+00:00");
		resource.setTppDebtorDetails("true");
		resource.setTppDebtorNameDetails("testDebtor");

		Mockito.when(reqHeaderAtrributes.getSelfUrl()).thenReturn("/testSelfUrl");

		CustomISPConsentsPOSTResponse x = transformer.paymentConsentsResponseTransformer(response, resource,
				methodType);
		assertEquals(OBExternalConsentStatus1Code.AWAITINGAUTHORISATION, x.getData().getStatus());
		assertEquals("2017-06-05T15:18:13+00:00", x.getData().getStatusUpdateDateTime());
	}

	@Test
	public void testPaymentConsentsResponseTransformer_NullDebtorDetails() {
		CustomISPConsentsPOSTResponse response = new CustomISPConsentsPOSTResponse();
		PaymentConsentsPlatformResource resource = new PaymentConsentsPlatformResource();
		String methodType = RequestMethod.POST.toString();

		List<String> schemeNameList = new ArrayList<>();
		String schemeName = "PAN";
		schemeNameList.add(schemeName);

		response.setData(new OBWriteDataInternationalScheduledConsentResponse1());
		response.getData().setCreationDateTime("2017-06-05T15:15:13+00:00");
		response.getData().setInitiation(new OBInternationalScheduled1());
		response.getData().getInitiation().setCreditorAccount(new OBCashAccountCreditor2());
		response.getData().getInitiation().getCreditorAccount().setSchemeName(schemeName);
		response.getData().getInitiation().setDebtorAccount(new OBCashAccountDebtor3());
		response.getData().getInitiation().getDebtorAccount().setSchemeName(schemeName);

		resource.setStatus("ACCEPTEDTECHNICALVALIDATION");
		resource.setStatusUpdateDateTime("2017-06-05T15:18:13+00:00");
		resource.setCreatedAt("2017-06-05T15:15:13+00:00");

		response.setLinks(null);
		PaymentSetupPOSTResponseLinks links = new PaymentSetupPOSTResponseLinks();
		links.getSelf();

		PaymentSetupPOSTResponseMeta meta = new PaymentSetupPOSTResponseMeta();
		meta.setTotalPages(100);

		Mockito.when(reqHeaderAtrributes.getSelfUrl()).thenReturn("/testSelfUrl");

		CustomISPConsentsPOSTResponse x = transformer.paymentConsentsResponseTransformer(response, resource,
				methodType);
		assertEquals(OBExternalConsentStatus1Code.AWAITINGAUTHORISATION, x.getData().getStatus());
		assertEquals("2017-06-05T15:18:13+00:00", x.getData().getStatusUpdateDateTime());
	}

	@Test
	public void testpaymentConsentRequestTransformer_RequestedExecutionDateTime() {
		CustomISPConsentsPOSTRequest request = new CustomISPConsentsPOSTRequest();
		request.setData(new OBWriteDataInternationalScheduledConsent1());
		request.getData().setInitiation(new OBInternationalScheduled1());
		request.getData().setAuthorisation(new OBAuthorisation1());
		request.getData().getInitiation().setRequestedExecutionDateTime("2019-01-31T00:00:00+00:00");

		when(pispDateUtility.transformDateTimeInRequest(anyString())).thenReturn("2019-01-31T00:00:00+00:00");
		transformer.paymentConsentRequestTransformer(request);
	}

	@Test
	public void testpaymentConsentRequestTransformer_CompletionDateTime() {
		CustomISPConsentsPOSTRequest request = new CustomISPConsentsPOSTRequest();
		request.setData(new OBWriteDataInternationalScheduledConsent1());
		request.getData().setInitiation(new OBInternationalScheduled1());
		request.getData().setAuthorisation(new OBAuthorisation1());
		request.getData().getAuthorisation().setCompletionDateTime("2019-12-31T00:00:00+00:00");

		when(pispDateUtility.transformDateTimeInRequest(anyString())).thenReturn("2019-12-31T00:00:00+00:00");
		transformer.paymentConsentRequestTransformer(request);
	}

	@After
	public void tearDown() {
		reqHeaderAtrributes = null;
		pispDateUtility = null;
		transformer = null;
	}
}