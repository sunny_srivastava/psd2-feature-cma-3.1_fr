/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.foundationservice.account.information.boi.adapter.transformer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.adapter.exceptions.AdapterErrorCodeEnum;
import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.aisp.domain.OBAccount6;
import com.capgemini.psd2.aisp.domain.OBAccount6Account;
import com.capgemini.psd2.aisp.domain.OBBranchAndFinancialInstitutionIdentification50;
import com.capgemini.psd2.aisp.domain.OBExternalAccountSubType1Code;
import com.capgemini.psd2.aisp.domain.OBExternalAccountType1Code;
import com.capgemini.psd2.aisp.domain.OBReadAccount6;
import com.capgemini.psd2.aisp.domain.OBReadAccount6Data;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountInformationResponse;
import com.capgemini.psd2.aisp.transformer.AccountInformationTransformer;
import com.capgemini.psd2.foundationservice.account.information.boi.adapter.constants.AccountInformationFoundationServiceConstants;
import com.capgemini.psd2.foundationservice.account.information.boi.adapter.raml.domain.Account;
import com.capgemini.psd2.foundationservice.account.information.boi.adapter.raml.domain.RetailNonRetailCode;
import com.capgemini.psd2.foundationservice.account.information.boi.adapter.raml.domain.SourceSystemAccountType;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.DigitalUserProfile;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.utilities.NullCheckUtils;
import com.capgemini.psd2.validator.PSD2Validator;

/**
 * The Class AccountInformationFoundationServiceTransformer.
 */
@Component
@ConfigurationProperties(prefix = "foundationService")
@Configuration
@EnableAutoConfiguration
public class AccountInformationFoundationServiceTransformer implements AccountInformationTransformer {

	@Autowired
	private PSD2Validator validator;

	/** The Account filters */
	private Map<String, Map<String, List<String>>> accountFiltering = new HashMap<>();

	private Map<String, Map<String, String>> jurisdiction = new HashMap<>();

	@Override
	public <T> PlatformAccountInformationResponse transformAccountInformation(T inputAccountObj,
			Map<String, String> params) {
		PlatformAccountInformationResponse finalAIResponseObj = new PlatformAccountInformationResponse();
		OBReadAccount6 obReadAccount6 = new OBReadAccount6();
		OBReadAccount6Data obReadAccount6Data = new OBReadAccount6Data();
		if (accountFiltering.get(AccountInformationFoundationServiceConstants.JURISDICTION) != null) {

			Set<String> regions = accountFiltering.get(AccountInformationFoundationServiceConstants.JURISDICTION)
					.get(params.get(PSD2Constants.CONSENT_FLOW_TYPE)).stream().map(u -> u.substring(0, u.indexOf(".")))
					.collect(Collectors.toSet());
			regions.stream().forEach(x -> {
				List<String> filterLst = accountFiltering.get(AccountInformationFoundationServiceConstants.JURISDICTION)
						.get(params.get(PSD2Constants.CONSENT_FLOW_TYPE)).stream().filter(u -> u.startsWith(x))
						.collect(Collectors.toList());
				Map<String, String> data = filterLst.stream().collect(Collectors.toMap(
						s1 -> s1.substring(x.length() + 1, s1.indexOf("=")), s1 -> s1.substring(s1.indexOf("=") + 1)));
				jurisdiction.put(x, data);
			});

		}

		if (params.get(AccountInformationFoundationServiceConstants.ACCOUNT)
				.equals(AccountInformationFoundationServiceConstants.SINGLE_ACCOUNT)) {
			List<OBAccount6> accountList = new ArrayList<>();
			if (params.get(AccountInformationFoundationServiceConstants.ACCOUNT_SUBTYPE)
					.equals(AccountInformationFoundationServiceConstants.CURRENT_ACCOUNT)
					|| params.get(AccountInformationFoundationServiceConstants.ACCOUNT_SUBTYPE)
							.equals(AccountInformationFoundationServiceConstants.SAVINGS)) {
				String accountId = null;
				Account accountDetails = (Account) inputAccountObj;

				if (params.get(AccountInformationFoundationServiceConstants.ACCOUNT_SUBTYPE)
						.equals(AccountInformationFoundationServiceConstants.CURRENT_ACCOUNT)) {

					accountId = params.get(accountDetails.getAccountNumber() + "_"
							+ accountDetails.getCurrentAccountInformation().getParentNationalSortCodeNSCNumber());
				} else {
					accountId = params.get(accountDetails.getAccountNumber() + "_"
							+ accountDetails.getSavingsAccountInformation().getParentNationalSortCodeNSCNumber());

				}
				if (accountId != null) {
					OBAccount6 responseObj = new OBAccount6();
					OBBranchAndFinancialInstitutionIdentification50 servicer = new OBBranchAndFinancialInstitutionIdentification50();
					responseObj.setAccountId(accountId);
					OBAccount6Account acc = new OBAccount6Account();
					if (accountDetails.getAccountName() != null) {
						acc.setName(accountDetails.getAccountName());
					}
					if (!NullCheckUtils.isNullOrEmpty(accountDetails.getCurrentAccountInformation())) {

						if (!NullCheckUtils.isNullOrEmpty(accountDetails.getSourceSystemAccountType())
								&& (accountDetails.getSourceSystemAccountType()
										.equals(SourceSystemAccountType.CURRENT_ACCOUNT)))
							responseObj.setAccountSubType(OBExternalAccountSubType1Code.CURRENTACCOUNT);
						else
							throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.TECHNICAL_ERROR);

						/*Mapping changed from brandCode to TenantID from Product*/
						if (params.get(PSD2Constants.TENANT_ID)
								.equals(AccountInformationFoundationServiceConstants.BRANDCODE_BOIUK)) {
							acc.setSchemeName(
									AccountInformationFoundationServiceConstants.UK_OBIE_SortCodeAccountNumber);

							acc.setIdentification(accountDetails.getCurrentAccountInformation()
									.getParentNationalSortCodeNSCNumber().concat(accountDetails.getAccountNumber()));

						/*Mapping changed from brandCode to TenantID from Product*/
						} else if (params.get(PSD2Constants.TENANT_ID)
								.equals(AccountInformationFoundationServiceConstants.BRANDCODE_BOIROI)) {
							acc.setSchemeName(AccountInformationFoundationServiceConstants.UK_OBIE_IBAN);
							acc.setIdentification(accountDetails.getCurrentAccountInformation()
									.getInternationalBankAccountNumberIBAN());
							servicer.setSchemeName(AccountInformationFoundationServiceConstants.UK_OBIE_BICFI);
							servicer.setIdentification(
									accountDetails.getCurrentAccountInformation().getSwiftBankIdentifierCodeBIC());
							responseObj.setServicer(servicer);
							validator.validate(servicer);
						}
					}

					if (!NullCheckUtils.isNullOrEmpty(accountDetails.getSavingsAccountInformation())) {
						if (!NullCheckUtils.isNullOrEmpty(accountDetails.getSourceSystemAccountType())
								&& (accountDetails.getSourceSystemAccountType()
										.equals(SourceSystemAccountType.SAVINGS_ACCOUNT)))
							responseObj.setAccountSubType(OBExternalAccountSubType1Code.SAVINGS);
						else
							throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.TECHNICAL_ERROR);
						
						/*Mapping changed from brandCode to TenantID from Product*/
						if (params.get(PSD2Constants.TENANT_ID)
								.equals(AccountInformationFoundationServiceConstants.BRANDCODE_BOIUK)) {
							acc.setSchemeName(
									AccountInformationFoundationServiceConstants.UK_OBIE_SortCodeAccountNumber);

							acc.setIdentification(accountDetails.getSavingsAccountInformation()
									.getParentNationalSortCodeNSCNumber().concat(accountDetails.getAccountNumber()));
							
							/*Mapping changed from brandCode to TenantID from Product*/
						} else if (params.get(PSD2Constants.TENANT_ID)
								.equals(AccountInformationFoundationServiceConstants.BRANDCODE_BOIROI)) {
							acc.setSchemeName(AccountInformationFoundationServiceConstants.UK_OBIE_IBAN);
							acc.setIdentification(accountDetails.getSavingsAccountInformation()
									.getInternationalBankAccountNumberIBAN());
							servicer.setSchemeName(AccountInformationFoundationServiceConstants.UK_OBIE_BICFI);
							servicer.setIdentification(
									accountDetails.getSavingsAccountInformation().getSwiftBankIdentifierCodeBIC());
							responseObj.setServicer(servicer);
							validator.validate(servicer);
						}
					}

					// code Changes for CMA2.0 starts
					List<OBAccount6Account> obAccount2Accountlist = new ArrayList<OBAccount6Account>();
					obAccount2Accountlist.add(acc);
					// code Changes for CMA2.0 ends
					validator.validate(acc);
					responseObj.setAccount(obAccount2Accountlist);
					validator.validate(accountDetails);
					responseObj.setCurrency(accountDetails.getAccountCurrency().getIsoAlphaCode());
					if ((null != accountDetails.getAccountNickName())
							&& (!(accountDetails.getAccountNickName().isEmpty()))) {
						responseObj.setNickname(accountDetails.getAccountNickName());

					}
					// code Changes for CMA2.0 starts
					if ((!NullCheckUtils.isNullOrEmpty(accountDetails.getRetailNonRetailCode())
							&& (accountDetails.getRetailNonRetailCode().equals(RetailNonRetailCode.NON_RETAIL)))) {
						responseObj.setAccountType(OBExternalAccountType1Code.BUSINESS);
					} else if ((!NullCheckUtils.isNullOrEmpty(accountDetails.getRetailNonRetailCode())
							&& (accountDetails.getRetailNonRetailCode().equals(RetailNonRetailCode.RETAIL)))) {
						responseObj.setAccountType(OBExternalAccountType1Code.PERSONAL);
					}

					validator.validate(responseObj);
					obReadAccount6Data.setAccount(accountList);
					obReadAccount6.setData(obReadAccount6Data);
					finalAIResponseObj.setoBReadAccount6(obReadAccount6);
					finalAIResponseObj.getoBReadAccount6().getData().getAccount().add(responseObj);
				}

			}
			if (params.get(AccountInformationFoundationServiceConstants.ACCOUNT_SUBTYPE)
					.equals(AccountInformationFoundationServiceConstants.CREDIT_CARD)) {
				Account creditCardAccnt = (Account) inputAccountObj;

				String accountId = params.get(creditCardAccnt.getAccountNumber() + "_"
						+ creditCardAccnt.getCreditCardAccountInformation().getCard().getMaskedCardPANNumber());
				if (accountId != null) {
					OBAccount6 responseObj = new OBAccount6();
					responseObj.setAccountId(accountId);
					OBAccount6Account acc = new OBAccount6Account();
					if (creditCardAccnt.getAccountName() != null) {
						acc.setName(creditCardAccnt.getAccountName());
					}
					if (!NullCheckUtils.isNullOrEmpty(
							creditCardAccnt.getCreditCardAccountInformation().getCard().getMaskedCardPANNumber())) {
						acc.setSchemeName(AccountInformationFoundationServiceConstants.UK_OBIE_PAN);

						acc.setIdentification(
								creditCardAccnt.getCreditCardAccountInformation().getCard().getMaskedCardPANNumber());

					}
					// code Changes for CMA2.0 starts
					List<OBAccount6Account> obAccount2Accountlist = new ArrayList<OBAccount6Account>();
					obAccount2Accountlist.add(acc);
					// code Changes for CMA2.0 ends
					validator.validate(acc);
					responseObj.setAccount(obAccount2Accountlist);
					validator.validate(creditCardAccnt);
					responseObj.setCurrency(creditCardAccnt.getAccountCurrency().getIsoAlphaCode());
					if ((null != creditCardAccnt.getAccountNickName())
							&& (!(creditCardAccnt.getAccountNickName().isEmpty()))) {
						responseObj.setNickname(creditCardAccnt.getAccountNickName());

					}
					// code Changes for CMA2.0 starts
					if ((!NullCheckUtils.isNullOrEmpty(creditCardAccnt.getRetailNonRetailCode())
							&& (creditCardAccnt.getRetailNonRetailCode().equals(RetailNonRetailCode.NON_RETAIL)))) {
						responseObj.setAccountType(OBExternalAccountType1Code.BUSINESS);
					} else if ((!NullCheckUtils.isNullOrEmpty(creditCardAccnt.getRetailNonRetailCode())
							&& (creditCardAccnt.getRetailNonRetailCode().equals(RetailNonRetailCode.RETAIL)))) {
						responseObj.setAccountType(OBExternalAccountType1Code.PERSONAL);
					}
					if ((!NullCheckUtils.isNullOrEmpty(creditCardAccnt.getSourceSystemAccountType()) && (creditCardAccnt
							.getSourceSystemAccountType().equals(SourceSystemAccountType.CREDIT_CARD))))
						responseObj.setAccountSubType(OBExternalAccountSubType1Code.CREDITCARD);
					else
						throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.TECHNICAL_ERROR);

					validator.validate(responseObj);
					obReadAccount6Data.setAccount(accountList);
					obReadAccount6.setData(obReadAccount6Data);
					finalAIResponseObj.setoBReadAccount6(obReadAccount6);
					finalAIResponseObj.getoBReadAccount6().getData().getAccount().add(responseObj);
				}

			}

		} else if (params.get(AccountInformationFoundationServiceConstants.ACCOUNT)
				.equals(AccountInformationFoundationServiceConstants.MULTI_ACCOUNT)) {
			List<OBAccount6> accountList = new ArrayList<>();

			DigitalUserProfile digitalUserProfile = (DigitalUserProfile) inputAccountObj;
			if (!digitalUserProfile.getDigitalUser().isDigitalUserLockedOutIndicator()
					&& digitalUserProfile.getPartyInformation().isPartyActiveIndicator()) {
				List<com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.AccountEntitlements> accountEntitlements = digitalUserProfile
						.getAccountEntitlements();
				for (com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.AccountEntitlements accntEntitlement : accountEntitlements) {
					OBAccount6 responseObj = new OBAccount6();
					OBAccount6Account acc = new OBAccount6Account();
					OBBranchAndFinancialInstitutionIdentification50 servicer = new OBBranchAndFinancialInstitutionIdentification50();
					if ((accntEntitlement.getAccount().getSourceSystemAccountType().toString().equalsIgnoreCase(
							AccountInformationFoundationServiceConstants.SOURCESYSTEMACCOUNTTYPE_CURRENTACCOUNT))) {
						String accountId = params.get(
								accntEntitlement.getAccount().getAccountNumber() + "_" + accntEntitlement.getAccount()
										.getCurrentAccountInformation().getParentNationalSortCodeNSCNumber());
						if (accountId != null) {
							responseObj.setAccountId(params.get(accntEntitlement.getAccount().getAccountNumber() + "_"
									+ accntEntitlement.getAccount().getCurrentAccountInformation()
											.getParentNationalSortCodeNSCNumber()));
							responseObj
									.setCurrency(accntEntitlement.getAccount().getAccountCurrency().getIsoAlphaCode());
							responseObj.setAccountType(accntEntitlement.getAccount().getRetailNonRetailCode().toString()
									.equals(AccountInformationFoundationServiceConstants.RETAILNONRETAILCODE)
											? OBExternalAccountType1Code.PERSONAL
											: OBExternalAccountType1Code.BUSINESS);
							responseObj.setNickname(accntEntitlement.getAccount().getAccountNickName());
							if (accntEntitlement.getAccount().getAccountName() != null) {
								acc.setName(accntEntitlement.getAccount().getAccountName());
							}
							responseObj.setAccountSubType(OBExternalAccountSubType1Code.CURRENTACCOUNT);
							
							/*Mapping changed from brandCode to TenantID from Product*/
							if (params.get(PSD2Constants.TENANT_ID)
									.equals(AccountInformationFoundationServiceConstants.BRANDCODE_BOIUK)) {
								acc.setSchemeName(
										AccountInformationFoundationServiceConstants.UK_OBIE_SortCodeAccountNumber);
								acc.setIdentification(accntEntitlement.getAccount().getCurrentAccountInformation()
										.getParentNationalSortCodeNSCNumber()
										.concat(accntEntitlement.getAccount().getAccountNumber()));

							/*Mapping changed from brandCode to TenantID from Product*/	
							} else if (params.get(PSD2Constants.TENANT_ID)
									.equals(AccountInformationFoundationServiceConstants.BRANDCODE_BOIROI)) {
								acc.setSchemeName(AccountInformationFoundationServiceConstants.UK_OBIE_IBAN);
								acc.setIdentification(accntEntitlement.getAccount().getCurrentAccountInformation()
										.getInternationalBankAccountNumberIBAN());
								servicer.setSchemeName(AccountInformationFoundationServiceConstants.UK_OBIE_BICFI);
								servicer.setIdentification(accntEntitlement.getAccount().getCurrentAccountInformation()
										.getSwiftBankIdentifierCodeBIC());
								responseObj.setServicer(servicer);
								validator.validate(servicer);
							}

							List<OBAccount6Account> obAccount2Accountlist = new ArrayList<OBAccount6Account>();
							validator.validate(acc);
							obAccount2Accountlist.add(acc);
							validator.validate(obAccount2Accountlist);
							responseObj.setAccount(obAccount2Accountlist);
							validator.validate(responseObj);
							obReadAccount6Data.setAccount(accountList);
							obReadAccount6.setData(obReadAccount6Data);
							finalAIResponseObj.setoBReadAccount6(obReadAccount6);
							finalAIResponseObj.getoBReadAccount6().getData().getAccount().add(responseObj);

						}
					} else if ((accntEntitlement.getAccount().getSourceSystemAccountType().toString().equalsIgnoreCase(
							AccountInformationFoundationServiceConstants.SOURCESYSTEMACCOUNTTYPE_SAVINGSACCOUNT))) {
						String accountId = params.get(
								accntEntitlement.getAccount().getAccountNumber() + "_" + accntEntitlement.getAccount()
										.getSavingsAccountInformation().getParentNationalSortCodeNSCNumber());
						if (accountId != null) {
							responseObj.setAccountId(params.get(accntEntitlement.getAccount().getAccountNumber() + "_"
									+ accntEntitlement.getAccount().getSavingsAccountInformation()
											.getParentNationalSortCodeNSCNumber()));
							responseObj
									.setCurrency(accntEntitlement.getAccount().getAccountCurrency().getIsoAlphaCode());
							responseObj.setAccountType(accntEntitlement.getAccount().getRetailNonRetailCode().toString()
									.equals(AccountInformationFoundationServiceConstants.RETAILNONRETAILCODE)
											? OBExternalAccountType1Code.PERSONAL
											: OBExternalAccountType1Code.BUSINESS);
							responseObj.setNickname(accntEntitlement.getAccount().getAccountNickName());
							if (accntEntitlement.getAccount().getAccountName() != null) {
								acc.setName(accntEntitlement.getAccount().getAccountName());
							}
							responseObj.setAccountSubType(OBExternalAccountSubType1Code.SAVINGS);
							
							/*Mapping changed from brandCode to TenantID from Product*/
							if (params.get(PSD2Constants.TENANT_ID)
									.equals(AccountInformationFoundationServiceConstants.BRANDCODE_BOIUK)) {
								acc.setSchemeName(
										AccountInformationFoundationServiceConstants.UK_OBIE_SortCodeAccountNumber);
								acc.setIdentification(accntEntitlement.getAccount().getSavingsAccountInformation()
										.getParentNationalSortCodeNSCNumber()
										.concat(accntEntitlement.getAccount().getAccountNumber()));

							/*Mapping changed from brandCode to TenantID from Product*/
							} else if (params.get(PSD2Constants.TENANT_ID)
									.equals(AccountInformationFoundationServiceConstants.BRANDCODE_BOIROI)) {
								acc.setSchemeName(AccountInformationFoundationServiceConstants.UK_OBIE_IBAN);
								acc.setIdentification(accntEntitlement.getAccount().getSavingsAccountInformation()
										.getInternationalBankAccountNumberIBAN());
								servicer.setSchemeName(AccountInformationFoundationServiceConstants.UK_OBIE_BICFI);
								servicer.setIdentification(accntEntitlement.getAccount().getSavingsAccountInformation()
										.getSwiftBankIdentifierCodeBIC());
								responseObj.setServicer(servicer);
								validator.validate(servicer);

							}

							List<OBAccount6Account> obAccount2Accountlist = new ArrayList<OBAccount6Account>();
							validator.validate(acc);
							obAccount2Accountlist.add(acc);
							validator.validate(obAccount2Accountlist);
							responseObj.setAccount(obAccount2Accountlist);
							validator.validate(responseObj);
							obReadAccount6Data.setAccount(accountList);
							obReadAccount6.setData(obReadAccount6Data);
							finalAIResponseObj.setoBReadAccount6(obReadAccount6);
							finalAIResponseObj.getoBReadAccount6().getData().getAccount().add(responseObj);
						}
					} else if ((accntEntitlement.getAccount().getSourceSystemAccountType().toString().equalsIgnoreCase(
							AccountInformationFoundationServiceConstants.SOURCESYSTEMACCOUNTTYPE_CREDITCARD))) {
						String accountId = params.get(
								accntEntitlement.getAccount().getAccountNumber() + "_" + accntEntitlement.getAccount()
										.getCreditCardAccountInformation().getCard().getMaskedCardPANNumber());
						if (accountId != null) {
							responseObj.setAccountId(params.get(accntEntitlement.getAccount().getAccountNumber() + "_"
									+ accntEntitlement.getAccount().getCreditCardAccountInformation().getCard()
											.getMaskedCardPANNumber()));
							responseObj
									.setCurrency(accntEntitlement.getAccount().getAccountCurrency().getIsoAlphaCode());
							responseObj.setAccountType(accntEntitlement.getAccount().getRetailNonRetailCode().toString()
									.equals(AccountInformationFoundationServiceConstants.RETAILNONRETAILCODE)
											? OBExternalAccountType1Code.PERSONAL
											: OBExternalAccountType1Code.BUSINESS);
							responseObj.setNickname(accntEntitlement.getAccount().getAccountNickName());
							if (accntEntitlement.getAccount().getAccountName() != null) {
								acc.setName(accntEntitlement.getAccount().getAccountName());
							}
							responseObj.setAccountSubType(OBExternalAccountSubType1Code.CREDITCARD);
							acc.setSchemeName(AccountInformationFoundationServiceConstants.UK_OBIE_PAN);
							acc.setIdentification(accntEntitlement.getAccount().getCreditCardAccountInformation()
									.getCard().getMaskedCardPANNumber());

							List<OBAccount6Account> obAccount2Accountlist = new ArrayList<OBAccount6Account>();
							validator.validate(acc);
							obAccount2Accountlist.add(acc);
							validator.validate(obAccount2Accountlist);
							responseObj.setAccount(obAccount2Accountlist);
							validator.validate(responseObj);
							obReadAccount6Data.setAccount(accountList);
							obReadAccount6.setData(obReadAccount6Data);
							finalAIResponseObj.setoBReadAccount6(obReadAccount6);
							finalAIResponseObj.getoBReadAccount6().getData().getAccount().add(responseObj);
						}
					}

				}

			} else {
				throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.ACCOUNT_DETAILS_NOT_FOUND);
			}
		}

		validator.validate(finalAIResponseObj);
		return finalAIResponseObj;

	}

	public Map<String, Map<String, List<String>>> getAccountFiltering() {
		return accountFiltering;
	}

	public void setAccountFiltering(Map<String, Map<String, List<String>>> accountFiltering) {
		this.accountFiltering = accountFiltering;
	}

	public Map<String, Map<String, String>> getJurisdiction() {
		return jurisdiction;
	}

	public void setJurisdiction(Map<String, Map<String, String>> jurisdiction) {
		this.jurisdiction = jurisdiction;
	}

}