/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.foundationservice.account.information.boi.adapter.test;

import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.aisp.domain.OBAccount6;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountInformationResponse;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.foundationservice.account.information.boi.adapter.constants.AccountInformationFoundationServiceConstants;
import com.capgemini.psd2.foundationservice.account.information.boi.adapter.raml.domain.Account;
import com.capgemini.psd2.foundationservice.account.information.boi.adapter.raml.domain.Brand;
import com.capgemini.psd2.foundationservice.account.information.boi.adapter.raml.domain.BrandCode;
import com.capgemini.psd2.foundationservice.account.information.boi.adapter.raml.domain.Card;
import com.capgemini.psd2.foundationservice.account.information.boi.adapter.raml.domain.CreditCardAccount;
import com.capgemini.psd2.foundationservice.account.information.boi.adapter.raml.domain.Currency;
import com.capgemini.psd2.foundationservice.account.information.boi.adapter.raml.domain.CurrentAccount;
import com.capgemini.psd2.foundationservice.account.information.boi.adapter.raml.domain.RetailNonRetailCode;
import com.capgemini.psd2.foundationservice.account.information.boi.adapter.raml.domain.SourceSystemAccountType;
//import com.capgemini.psd2.foundationservice.account.information.boi.adapter.raml.domain.DigitalUserProfile;
import com.capgemini.psd2.foundationservice.account.information.boi.adapter.transformer.AccountInformationFoundationServiceTransformer;
//import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.CurrentAccountInformation;
/*import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.AccountBrand;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.AccountCurrency;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.CreditCardAccountInformation;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.CurrentAccountInformation;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.SavingsAccountInformation;*/
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.validator.impl.PSD2ValidatorImpl;

/**
 * The Class AccountInformationFoundationServiceTransformerTest.
 */

@RunWith(SpringJUnit4ClassRunner.class)
public class AccountInformationFoundationServiceTransformerTest {

	@Mock
	private PSD2ValidatorImpl validator;

	/** The account information FS transformer. */
	@InjectMocks
	private AccountInformationFoundationServiceTransformer accountInformationFSTransformer;

	/**
	 * Sets the up.
	 */
	@Before
	public void setUp() {

		MockitoAnnotations.initMocks(this);

	}

	@Test
	public void contextLoads() {
	}

	

	@Test
	public void testTransformAccountInformation3() {
		Map<String, String> params = new HashMap<>();
		params.put("3487_1235", "4548");
		params.put(PSD2Constants.CONSENT_FLOW_TYPE, "AISP");
		params.put(AccountInformationFoundationServiceConstants.ACCOUNT_SUBTYPE,
				AccountInformationFoundationServiceConstants.SAVINGS);
		params.put(AccountInformationFoundationServiceConstants.ACCOUNT,
				AccountInformationFoundationServiceConstants.SINGLE_ACCOUNT);
		Account accounts = new Account();
		RetailNonRetailCode retail = RetailNonRetailCode.fromValue("Non-Retail");
		SourceSystemAccountType sourceSystemAccountType = SourceSystemAccountType.fromValue("Savings Account");
		BrandCode brandCode = BrandCode.fromValue("BOI-GB");
		
		Brand brand = new Brand();
		Currency cur = new Currency();
		
		CurrentAccount savings = new CurrentAccount();
		
		brand.setBrandCode(brandCode);
		cur.setIsoAlphaCode("GBP");
		accounts.setAccountBrand(brand);
		accounts.setAccountCurrency(cur);
		savings.setParentNationalSortCodeNSCNumber("t786");
		accounts.setSavingsAccountInformation(savings);
		accounts.setRetailNonRetailCode(retail);
		
		accounts.setSourceSystemAccountType(sourceSystemAccountType);
		accounts.setAccountName("boi");
		accounts.setAccountNumber("3487");
		
    	PlatformAccountInformationResponse res = accountInformationFSTransformer.transformAccountInformation(accounts, params);
		assertNotNull(res);
	

	}
	
	/**
	 * Brand code BOI-ROI check
	 */
	@Test
	public void testTransformAccountInformationCurrentROICheck() {
		Map<String, String> params = new HashMap<>();
		params.put("3487_1235", "4548");
		params.put(PSD2Constants.CONSENT_FLOW_TYPE, "AISP");
		params.put(PSD2Constants.TENANT_ID, "BOIROI");
		params.put(AccountInformationFoundationServiceConstants.ACCOUNT_SUBTYPE,
				AccountInformationFoundationServiceConstants.CURRENT_ACCOUNT);
		params.put(AccountInformationFoundationServiceConstants.ACCOUNT,
				AccountInformationFoundationServiceConstants.SINGLE_ACCOUNT);
		Account accounts = new Account();
		RetailNonRetailCode retail = RetailNonRetailCode.fromValue("Non-Retail");
		SourceSystemAccountType sourceSystemAccountType = SourceSystemAccountType.fromValue("Current Account");
		BrandCode brandCode = BrandCode.fromValue("BOI-ROI");
		Brand brand = new Brand();
		Currency cur = new Currency();
		CurrentAccount current = new CurrentAccount();
		
		brand.setBrandCode(brandCode);
		cur.setIsoAlphaCode("GBP");
		accounts.setAccountBrand(brand);
		accounts.setAccountCurrency(cur);
		current.setParentNationalSortCodeNSCNumber("1235");
		accounts.setCurrentAccountInformation(current);
		accounts.setRetailNonRetailCode(retail);
		accounts.setSourceSystemAccountType(sourceSystemAccountType);
		accounts.setAccountName("boi");
		accounts.setAccountNumber("3487");
		
    	PlatformAccountInformationResponse res = accountInformationFSTransformer.transformAccountInformation(accounts, params);
		assertNotNull(res);
	

	}
	
	@Test
	public void testTransformAccountInformationSavingsROICheck() {
		Map<String, String> params = new HashMap<>();
		params.put("3487_1235", "4548");
		params.put(PSD2Constants.CONSENT_FLOW_TYPE, "AISP");
		params.put(PSD2Constants.TENANT_ID, "BOIROI");
		params.put(AccountInformationFoundationServiceConstants.ACCOUNT_SUBTYPE,
				AccountInformationFoundationServiceConstants.SAVINGS);
		params.put(AccountInformationFoundationServiceConstants.ACCOUNT,
				AccountInformationFoundationServiceConstants.SINGLE_ACCOUNT);
		Account accounts = new Account();
		RetailNonRetailCode retail = RetailNonRetailCode.fromValue("Non-Retail");
		SourceSystemAccountType sourceSystemAccountType = SourceSystemAccountType.fromValue("Savings Account");
		BrandCode brandCode = BrandCode.fromValue("BOI-ROI");
		Brand brand = new Brand();
		Currency cur = new Currency();
		CurrentAccount savings = new CurrentAccount();
		brand.setBrandCode(brandCode);
		cur.setIsoAlphaCode("GBP");
		accounts.setAccountBrand(brand);
		accounts.setAccountCurrency(cur);
		savings.setParentNationalSortCodeNSCNumber("1235");
		accounts.setSavingsAccountInformation(savings);
		accounts.setRetailNonRetailCode(retail);
		accounts.setSourceSystemAccountType(sourceSystemAccountType);
		accounts.setAccountName("boi");
		accounts.setAccountNumber("3487");
		
    	PlatformAccountInformationResponse res = accountInformationFSTransformer.transformAccountInformation(accounts, params);
		assertNotNull(res);
	

	}

	@Test
	public void testTransformAccountInformationCredit() {
		Map<String, String> params = new HashMap<>();
		params.put("3487_1235", "4548");
		params.put(PSD2Constants.CONSENT_FLOW_TYPE, "AISP");
		params.put(AccountInformationFoundationServiceConstants.ACCOUNT_SUBTYPE,
				AccountInformationFoundationServiceConstants.CREDIT_CARD);
		params.put(AccountInformationFoundationServiceConstants.ACCOUNT,
				AccountInformationFoundationServiceConstants.SINGLE_ACCOUNT);
		Account accounts = new Account();
		RetailNonRetailCode retail = RetailNonRetailCode.fromValue("Non-Retail");
		SourceSystemAccountType sourceSystemAccountType = SourceSystemAccountType.fromValue("Credit Card");
		BrandCode brandCode = BrandCode.fromValue("BOI-GB");
		Brand brand = new Brand();
		Currency cur = new Currency();
		CreditCardAccount credit = new CreditCardAccount();
		CurrentAccount current = new CurrentAccount();
		CurrentAccount savings = new CurrentAccount();
		
		Card card=new Card();
		card.setMaskedCardPANNumber("12345");
		credit.setCard(card);
		brand.setBrandCode(brandCode);
		cur.setIsoAlphaCode("GBP");
		
		accounts.setAccountBrand(brand);
		accounts.setAccountCurrency(cur);
		current.setParentNationalSortCodeNSCNumber("1235");
		accounts.setCurrentAccountInformation(current);
		savings.setParentNationalSortCodeNSCNumber("t786");
		accounts.setSavingsAccountInformation(savings);
		accounts.setCreditCardAccountInformation(credit);
		accounts.setRetailNonRetailCode(retail);
		accounts.setSourceSystemAccountType(sourceSystemAccountType);
		accounts.setAccountName("boi");
		accounts.setAccountNickName("ibc");
		accounts.setAccountNumber("3487");
		params.put("3487_12345","5678" );
    	PlatformAccountInformationResponse res = accountInformationFSTransformer.transformAccountInformation(accounts, params);
		assertNotNull(res);
	

	}

	@Test
	public void testTransformAccountInformationCreditRetail() {
		Map<String, String> params = new HashMap<>();
		params.put("3487_1235", "4548");
		params.put(PSD2Constants.CONSENT_FLOW_TYPE, "AISP");
		params.put(AccountInformationFoundationServiceConstants.ACCOUNT_SUBTYPE,
				AccountInformationFoundationServiceConstants.CREDIT_CARD);
		params.put(AccountInformationFoundationServiceConstants.ACCOUNT,
				AccountInformationFoundationServiceConstants.SINGLE_ACCOUNT);
		Account accounts = new Account();
		RetailNonRetailCode retail = RetailNonRetailCode.fromValue("Retail");
		SourceSystemAccountType sourceSystemAccountType = SourceSystemAccountType.fromValue("Credit Card");
		BrandCode brandCode = BrandCode.fromValue("BOI-GB");
		Brand brand = new Brand();
		Currency cur = new Currency();
		CurrentAccount savings = new CurrentAccount();
		CreditCardAccount credit = new CreditCardAccount();
		CurrentAccount current = new CurrentAccount();
		com.capgemini.psd2.foundationservice.account.information.boi.adapter.raml.domain.Card card=new com.capgemini.psd2.foundationservice.account.information.boi.adapter.raml.domain.Card();
		card.setMaskedCardPANNumber("12345");
		credit.setCard(card);
		brand.setBrandCode(brandCode);
		cur.setIsoAlphaCode("GBP");
		
		accounts.setAccountBrand(brand);
		accounts.setAccountCurrency(cur);
		current.setParentNationalSortCodeNSCNumber("1235");
		accounts.setCurrentAccountInformation(current);
		savings.setParentNationalSortCodeNSCNumber("t786");
		accounts.setSavingsAccountInformation(savings);
		accounts.setCreditCardAccountInformation(credit);
		accounts.setRetailNonRetailCode(retail);
		accounts.setSourceSystemAccountType(sourceSystemAccountType);
		accounts.setAccountName("boi");
		accounts.setAccountNickName("ibc");
		accounts.setAccountNumber("3487");
		params.put("3487_12345","5678" );
    	PlatformAccountInformationResponse res = accountInformationFSTransformer.transformAccountInformation(accounts, params);
		assertNotNull(res);
	

	}

	@Test(expected = AdapterException.class)
	public void testTransformAccountInformationCurrent() {
		Map<String, String> params = new HashMap<>();
		params.put("3487_1235", "4548");
		params.put(PSD2Constants.CONSENT_FLOW_TYPE, "AISP");
		params.put(PSD2Constants.TENANT_ID, "BOIROI");
		params.put(AccountInformationFoundationServiceConstants.ACCOUNT_SUBTYPE,
				AccountInformationFoundationServiceConstants.CURRENT_ACCOUNT);
		params.put(AccountInformationFoundationServiceConstants.ACCOUNT,
				AccountInformationFoundationServiceConstants.SINGLE_ACCOUNT);
		Account accounts = new Account();
		RetailNonRetailCode retail = RetailNonRetailCode.fromValue("Non-Retail");
		SourceSystemAccountType sourceSystemAccountType = SourceSystemAccountType.fromValue("Current Account");
		BrandCode brandCode = BrandCode.fromValue("BOI-GB");
		Brand brand = new Brand();
		Currency cur = new Currency();
		CurrentAccount current = new CurrentAccount();
		CurrentAccount savings = new CurrentAccount();
		brand.setBrandCode(brandCode);
		cur.setIsoAlphaCode("GBP");
		accounts.setAccountBrand(brand);
		accounts.setAccountCurrency(cur);
		current.setParentNationalSortCodeNSCNumber("1235");
		accounts.setCurrentAccountInformation(current);
		savings.setParentNationalSortCodeNSCNumber("t786");
		accounts.setSavingsAccountInformation(savings);
		accounts.setRetailNonRetailCode(retail);
		accounts.setSourceSystemAccountType(sourceSystemAccountType);
		accounts.setAccountName("boi");
		accounts.setAccountNumber("3487");
		
    	PlatformAccountInformationResponse res = accountInformationFSTransformer.transformAccountInformation(accounts, params);
		assertNotNull(res);
	

	}
	@Test
	public void testTransformAccountInformationCurrentRetail() {
		Map<String, String> params = new HashMap<>();
		params.put("3487_1235", "4548");
		params.put(PSD2Constants.TENANT_ID, "BOIROI");
		params.put(PSD2Constants.CONSENT_FLOW_TYPE, "AISP");
		params.put(AccountInformationFoundationServiceConstants.ACCOUNT_SUBTYPE,
				AccountInformationFoundationServiceConstants.CURRENT_ACCOUNT);
		params.put(AccountInformationFoundationServiceConstants.ACCOUNT,
				AccountInformationFoundationServiceConstants.SINGLE_ACCOUNT);
		Account accounts = new Account();
		RetailNonRetailCode retail = RetailNonRetailCode.fromValue("Retail");
		SourceSystemAccountType sourceSystemAccountType = SourceSystemAccountType.fromValue("Current Account");
		Brand brand = new Brand();
		BrandCode brandCode = BrandCode.fromValue("BOI-GB");
		Currency cur = new Currency();
		CurrentAccount current = new CurrentAccount();
		CurrentAccount savings = new CurrentAccount();
		CreditCardAccount credit = new CreditCardAccount();
		brand.setBrandCode(brandCode);
		cur.setIsoAlphaCode("GBP");
		accounts.setAccountBrand(brand);
		accounts.setAccountCurrency(cur);
		current.setParentNationalSortCodeNSCNumber("1235");
		accounts.setCurrentAccountInformation(current);
		accounts.setRetailNonRetailCode(retail);
		accounts.setSourceSystemAccountType(sourceSystemAccountType);
		accounts.setAccountName("boi");
		accounts.setAccountNickName("odc");
		accounts.setAccountNumber("3487");
		
    	PlatformAccountInformationResponse res = accountInformationFSTransformer.transformAccountInformation(accounts, params);
		assertNotNull(res);
	

	}
	@Test
	public void testTransformMultiAccountInformation() {
		Map<String, String> params = new HashMap<>();
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.DigitalUserProfile digitalUserProfile = new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.DigitalUserProfile();
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.AccountEntitlements accountEntitlement = new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.AccountEntitlements();
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.AccountEntitlements accountEntitlement1 = new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.AccountEntitlements();
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.AccountEntitlements accountEntitlement2 = new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.AccountEntitlements();
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.PartyBasicInformation partyInformation = new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.PartyBasicInformation();
		List<com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.AccountEntitlements> accountEntitlementsList = new ArrayList<com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.AccountEntitlements>();
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.Account account = new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.Account();
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.Account account1 = new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.Account();
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.Account account2 = new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.Account();
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.Brand accountBrand = new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.Brand();
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.Currency currency = new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.Currency();
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.CurrentAccount currentAccount = new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.CurrentAccount();
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.CurrentAccount savingsAccount = new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.CurrentAccount();
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.CreditCardAccount ccCredit = new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.CreditCardAccount();
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.Card card = new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.Card();
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.DigitalUser digitalUser = new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.DigitalUser();
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.RetailNonRetailCode retail = com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.RetailNonRetailCode.fromValue("Retail");
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.SourceSystemAccountType sourceSystemAccountCurrent = com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.SourceSystemAccountType.fromValue("Current Account");
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.SourceSystemAccountType sourceSystemAccountSavings = com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.SourceSystemAccountType.fromValue("Savings Account");
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.SourceSystemAccountType sourceSystemAccountCredit = com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.SourceSystemAccountType.fromValue("Credit Card");
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.BrandCode brandNI = com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.BrandCode.fromValue("BOI-NI");
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.BrandCode brandROI = com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.BrandCode.fromValue("BOI-ROI");
		
		params.put("123456_123456789012345", "12345");
		params.put(PSD2Constants.CONSENT_FLOW_TYPE, "AISP");
		params.put(PSD2Constants.TENANT_ID, "BOIROI");
		params.put(AccountInformationFoundationServiceConstants.ACCOUNT_SUBTYPE, "CreditCard");
		params.put(AccountInformationFoundationServiceConstants.ACCOUNT,
				AccountInformationFoundationServiceConstants.MULTI_ACCOUNT);
		params.put("1234_345", "123");
		// Test for Current Account
		currency.setIsoAlphaCode("EUR");
		account.setAccountCurrency(currency);
		account.setRetailNonRetailCode(retail);
		account.setSourceSystemAccountType(sourceSystemAccountCurrent);
		account.setAccountNumber("1234");
		currentAccount.setParentNationalSortCodeNSCNumber("345");
		account.setCurrentAccountInformation(currentAccount);
		accountBrand.setBrandCode(brandNI);
		account.setAccountBrand(accountBrand);
		account.setAccountName("CA");
		accountEntitlement.setAccount(account);
		// Test for Savings Account
		account1.setAccountCurrency(currency);
		account1.setRetailNonRetailCode(retail);
		account1.setSourceSystemAccountType(sourceSystemAccountSavings);
		account1.setAccountNumber("1234");
		accountBrand.setBrandCode(brandROI);
		account1.setAccountBrand(accountBrand);
		savingsAccount.setParentNationalSortCodeNSCNumber("345");
		account1.setSavingsAccountInformation(savingsAccount);
		account1.setAccountNickName("sA");
		accountEntitlement1.setAccount(account1);
		account2.setAccountCurrency(currency);
		account2.setRetailNonRetailCode(retail);
		account2.setSourceSystemAccountType(sourceSystemAccountCredit);
		account2.setAccountNumber("1234");
		account2.setAccountBrand(accountBrand);
		card.setMaskedCardPANNumber("456");
		ccCredit.setCard(card);
		account2.setCreditCardAccountInformation(ccCredit);
		account2.setAccountNickName("cc");
		accountEntitlement2.setAccount(account2);
		params.put("1234_456", "12328");
		// ends
		accountEntitlementsList.add(accountEntitlement);
		accountEntitlementsList.add(accountEntitlement1);
		accountEntitlementsList.add(accountEntitlement2);
		digitalUserProfile.setAccountEntitlements(accountEntitlementsList);

		digitalUser.setDigitalUserLockedOutIndicator(false);
		digitalUserProfile.setDigitalUser(digitalUser);
		partyInformation.setPartyActiveIndicator(true);
		digitalUserProfile.setPartyInformation(partyInformation);
		PlatformAccountInformationResponse res = accountInformationFSTransformer
				.transformAccountInformation(digitalUserProfile, params);
		assertNotNull(res);
		

		
		 		 
	}
/*	@Test
	public void testTransformMultiAccountInformation1() {
		Map<String, String> params = new HashMap<>();
		DigitalUserProfile digitalUserProfile = new DigitalUserProfile();
		AccountEntitlements accountEntitlement = new AccountEntitlements();
		AccountEntitlements accountEntitlement1 = new AccountEntitlements();
		PartyBasicInformation partyInformation = new PartyBasicInformation();
		List<AccountEntitlements> accountEntitlementsList = new ArrayList<AccountEntitlements>();
		Account account = new Account();
		Account account1 = new Account();
		Account account2 = new Account();
		Brand accountBrand = new Brand();
		Currency currency = new Currency();
		CurrentAccount currentAccount = new CurrentAccount();
		CurrentAccount savingsAccount = new CurrentAccount();
		CreditCardAccount ccCredit = new CreditCardAccount();
		Card card = new Card();
		DigitalUser digitalUser = new DigitalUser();
		RetailNonRetailCode retail = RetailNonRetailCode.fromValue("Retail");
		SourceSystemAccountType sourceSystemAccountCurrent = SourceSystemAccountType.fromValue("Current Account");
		SourceSystemAccountType sourceSystemAccountSavings = SourceSystemAccountType.fromValue("Savings Account");
		SourceSystemAccountType sourceSystemAccountCredit = SourceSystemAccountType.fromValue("Credit Card");
		BrandCode brandROI = BrandCode.fromValue("BOI-ROI");
		BrandCode brandGB = BrandCode.fromValue("BOI-GB");
		
		params.put("123456_123456789012345", "12345");
		params.put(PSD2Constants.CONSENT_FLOW_TYPE, "AISP");
		params.put(AccountInformationFoundationServiceConstants.ACCOUNT_SUBTYPE, "CreditCard");
		params.put(AccountInformationFoundationServiceConstants.ACCOUNT,
				AccountInformationFoundationServiceConstants.MULTI_ACCOUNT);
		params.put("1234_345", "123");
		// Test for Current Account
		currency.setIsoAlphaCode("EUR");
		account.setAccountCurrency(currency);
		account.setRetailNonRetailCode(retail);
		account.setSourceSystemAccountType(sourceSystemAccountCurrent);
		account.setAccountNumber("1234");
		currentAccount.setParentNationalSortCodeNSCNumber("345");
		account.setCurrentAccountInformation(currentAccount);
		accountBrand.setBrandCode(brandROI);
		account.setAccountBrand(accountBrand);
		account.setAccountName("CA");
		accountEntitlement.setAccount(account);
		// Test for Savings Account
		account1.setAccountCurrency(currency);
		account1.setRetailNonRetailCode(retail);
		account1.setSourceSystemAccountType(sourceSystemAccountSavings);
		account1.setAccountNumber("1234");
		accountBrand.setBrandCode(brandGB);
		account1.setAccountBrand(accountBrand);
		savingsAccount.setParentNationalSortCodeNSCNumber("345");
		account1.setSavingsAccountInformation(savingsAccount);
		account1.setAccountName("sA");
		accountEntitlement1.setAccount(account1);
		
		
		// ends
		accountEntitlementsList.add(accountEntitlement);
		accountEntitlementsList.add(accountEntitlement1);
		
		digitalUserProfile.setAccountEntitlements(accountEntitlementsList);

		digitalUser.setDigitalUserLockedOutIndicator(false);
		digitalUserProfile.setDigitalUser(digitalUser);
		partyInformation.setPartyActiveIndicator(true);
		digitalUserProfile.setPartyInformation(partyInformation);
		PlatformAccountInformationResponse res = accountInformationFSTransformer
				.transformAccountInformation(digitalUserProfile, params);
		assertNotNull(res);
		

		
		 		 
	}*/
	@Test
	public void testTransformMultiAccountInformation2() {
		Map<String, String> params = new HashMap<>();
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.DigitalUserProfile digitalUserProfile = new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.DigitalUserProfile();
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.AccountEntitlements accountEntitlement2 = new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.AccountEntitlements();
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.PartyBasicInformation partyInformation = new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.PartyBasicInformation();
		List<com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.AccountEntitlements> accountEntitlementsList = new ArrayList<com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.AccountEntitlements>();
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.Account account2 = new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.Account();
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.Brand accountBrand = new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.Brand();
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.Currency currency = new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.Currency();
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.CreditCardAccount ccCredit = new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.CreditCardAccount();
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.Card card = new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.Card();
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.DigitalUser digitalUser = new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.DigitalUser();
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.RetailNonRetailCode retail = com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.RetailNonRetailCode.fromValue("Retail");
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.SourceSystemAccountType sourceSystemAccountCredit = com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.SourceSystemAccountType.fromValue("Credit Card");
		params.put("123456_123456789012345", "12345");
		params.put(PSD2Constants.CONSENT_FLOW_TYPE, "AISP");
		params.put(AccountInformationFoundationServiceConstants.ACCOUNT_SUBTYPE, "CreditCard");
		params.put(AccountInformationFoundationServiceConstants.ACCOUNT,
				AccountInformationFoundationServiceConstants.MULTI_ACCOUNT);
		params.put("1234_345", "123");
		account2.setAccountCurrency(currency);
		account2.setRetailNonRetailCode(retail);
		account2.setSourceSystemAccountType(sourceSystemAccountCredit);
		account2.setAccountNumber("1234");
		account2.setAccountBrand(accountBrand);
		card.setMaskedCardPANNumber("456");
		ccCredit.setCard(card);
		account2.setCreditCardAccountInformation(ccCredit);
		account2.setAccountName("cc");
		account2.setAccountNickName("cbc");
		accountEntitlement2.setAccount(account2);
		params.put("1234_4563456", "12328");
		accountEntitlementsList.add(accountEntitlement2);
		digitalUserProfile.setAccountEntitlements(accountEntitlementsList);

		digitalUser.setDigitalUserLockedOutIndicator(false);
		digitalUserProfile.setDigitalUser(digitalUser);
		partyInformation.setPartyActiveIndicator(true);
		digitalUserProfile.setPartyInformation(partyInformation);
	
		PlatformAccountInformationResponse res = accountInformationFSTransformer
				.transformAccountInformation(digitalUserProfile, params);
		
		assertNotNull(res);
	
	}
	
	

	@Test
	public void testTransformMultiAccountInformationAdapterExceptionCheck() {
		Map<String, String> params = new HashMap<>();
		params.put("123456_123456789012345", "12345");
		params.put(PSD2Constants.CONSENT_FLOW_TYPE, "AISP");
		params.put(AccountInformationFoundationServiceConstants.ACCOUNT,
				AccountInformationFoundationServiceConstants.MULTI_ACCOUNT);
		params.put("1234_345", "123");
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.DigitalUserProfile digitalUserProfile = new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.DigitalUserProfile();
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.DigitalUser digitalUser = new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.DigitalUser();
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.PartyBasicInformation partyInformation = new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.PartyBasicInformation();
		digitalUser.setDigitalUserLockedOutIndicator(false);
		digitalUserProfile.setDigitalUser(digitalUser);
		partyInformation.setPartyActiveIndicator(true);
		digitalUserProfile.setPartyInformation(partyInformation);
		
		PlatformAccountInformationResponse res = accountInformationFSTransformer
				.transformAccountInformation(digitalUserProfile, params);
		assertNotNull(res);
		
		
		
}
	
	@Test
	public void testTransformAccountInformationAccNumberAsNull() {
		Map<String, String> params = new HashMap<>();
		params.put("accountId", "12345");
		params.put(PSD2Constants.CONSENT_FLOW_TYPE, "AISP");
		params.put(AccountInformationFoundationServiceConstants.ACCOUNT_SUBTYPE, "CurrentAccount");
		params.put(AccountInformationFoundationServiceConstants.ACCOUNT,
				AccountInformationFoundationServiceConstants.SINGLE_ACCOUNT);
		Account accounts = new Account();
		RetailNonRetailCode retail = RetailNonRetailCode.fromValue("Non-Retail");
		SourceSystemAccountType sourceSystemAccountTypeCurrent = SourceSystemAccountType.fromValue("Current Account");
		SourceSystemAccountType sourceSystemAccountTypeSavings = SourceSystemAccountType.fromValue("Savings Account");
		BrandCode brandCode = BrandCode.fromValue("BOI-NI");
		Brand brand = new Brand();
		Currency cur = new Currency();
		CurrentAccount current = new CurrentAccount();
		CurrentAccount savings = new CurrentAccount();
		
		brand.setBrandCode(brandCode);
		cur.setIsoAlphaCode("GBP");
		accounts.setAccountBrand(brand);
		accounts.setAccountCurrency(cur);
		current.setParentNationalSortCodeNSCNumber("1235");
		accounts.setCurrentAccountInformation(current);
		accounts.setRetailNonRetailCode(retail);
		accounts.setSourceSystemAccountType(sourceSystemAccountTypeCurrent);
		accounts.setAccountNickName("1deifj");
		accounts.setAccountNumber("3487");
		Account accounts1 = new Account();
		accounts1.setAccountBrand(brand);
		accounts1.setAccountCurrency(cur);
		savings.setParentNationalSortCodeNSCNumber("1287");
		accounts1.setSavingsAccountInformation(savings);
		accounts1.setRetailNonRetailCode(retail);
		accounts1.setSourceSystemAccountType(sourceSystemAccountTypeSavings);
		accounts1.setAccountNickName("1deifj");
		accounts1.setAccountNumber("1234");

		PlatformAccountInformationResponse res = accountInformationFSTransformer.transformAccountInformation(accounts,
				params);
		params.put("3487_1287", "4548");
		
		AccountMapping accountMapping = new AccountMapping();
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
		AccountDetails accDet = new AccountDetails();
		accDet.setAccountId("12345");
		accDet.setAccountNSC("nsc1234");
		accDet.setAccountNumber("acct1234");
		accDetList.add(accDet);
		accountMapping.setAccountDetails(accDetList);
		accountMapping.setTppCID("test");
		accountMapping.setPsuId("test");

		Map<String, Map<String, List<String>>> accountFiltering = new HashMap<>();

		Map<String, List<String>> map1 = new HashMap<String, List<String>>();

		List<String> accountSubType = new ArrayList<>();
		accountSubType.add("Current Account");
		map1.put("AISP", accountSubType);
		map1.put("CISP", accountSubType);
		map1.put("PISP", accountSubType);

		accountFiltering.put("accountType", map1);

		Map<String, List<String>> map2 = new HashMap<String, List<String>>();

		List<String> jurisdictionList = new ArrayList<>();
		jurisdictionList.add("NORTHERN_IRELAND.SchemeName=BBAN");
		jurisdictionList.add("NORTHERN_IRELAND.Identification=IBAN");
		jurisdictionList.add("NORTHERN_IRELAND.ServicerSchemeName=UKSORTCODE");
		jurisdictionList.add("NORTHERN_IRELAND.ServicerIdentification=AccountNSCNumber");
		jurisdictionList.add("GREAT_BRITAIN.SchemeName=BBAN");
		jurisdictionList.add("GREAT_BRITAIN.Identification=AccountNumber");
		jurisdictionList.add("GREAT_BRITAIN.ServicerSchemeName=UKSORTCODE");
		jurisdictionList.add("GREAT_BRITAIN.ServicerIdentification=AccountNSCNumber");

		map2.put("AISP", jurisdictionList);
		map2.put("CISP", jurisdictionList);
		map2.put("PISP", jurisdictionList);

		accountFiltering.put("jurisdiction", map2);

		Map<String, List<String>> map3 = new HashMap<String, List<String>>();

		List<String> permissionList1 = new ArrayList<>();
		permissionList1.add("A");
		permissionList1.add("V");
		List<String> permissionList2 = new ArrayList<>();
		permissionList2.add("A");
		permissionList2.add("X");

		map3.put("AISP", permissionList1);
		map3.put("CISP", permissionList1);
		map3.put("PISP", permissionList2);

		accountFiltering.put("permission", map3);

		accountInformationFSTransformer.setAccountFiltering(accountFiltering);

		accountInformationFSTransformer.setAccountFiltering(accountFiltering);
		accountInformationFSTransformer.getAccountFiltering();

		OBAccount6 responseDataObj = new OBAccount6();
		responseDataObj.setAccountId(params.get("accountId"));
				
		res = accountInformationFSTransformer.transformAccountInformation(accounts, params);
		assertNotNull(res);
		

	}
}
