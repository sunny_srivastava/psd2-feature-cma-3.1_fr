/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/

package com.capgemini.psd2.foundationservice.account.information.boi.adapter.test;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyMap;
import static org.mockito.Matchers.anyObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.aisp.domain.OBAccount6Account;
import com.capgemini.psd2.aisp.domain.OBExternalAccountSubType1Code;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountInformationResponse;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.foundationservice.account.information.boi.adapter.AccountInformationFoundationServiceAdapter;
import com.capgemini.psd2.foundationservice.account.information.boi.adapter.client.AccountInformationFoundationServiceClient;
import com.capgemini.psd2.foundationservice.account.information.boi.adapter.constants.AccountInformationFoundationServiceConstants;
import com.capgemini.psd2.foundationservice.account.information.boi.adapter.delegate.AccountInformationFoundationServiceDelegate;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.CustomerAccountsFilterFoundationServiceAdapter;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.delegate.CustomerAccountsFilterFoundationServiceDelegate;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.filterartion.AdapterFilterUtility;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.DigitalUserProfile;
import com.capgemini.psd2.rest.client.sync.impl.RestClientSyncImpl;

/**
 * The Class AccountInformationFoundationServiceAdapterTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class AccountInformationFoundationServiceAdapterTest {

	/** The account information foundation service adapter. */
	@InjectMocks
	private AccountInformationFoundationServiceAdapter accountInformationFoundationServiceAdapter;

	/** The account information foundation service delegate. */
	@Mock
	private AccountInformationFoundationServiceDelegate accountInformationFoundationServiceDelegate;

	/** The account information foundation service client. */
	@Mock
	private AccountInformationFoundationServiceClient accountInformationFoundationServiceClient;

	@Mock
	private CustomerAccountsFilterFoundationServiceDelegate customerAccountProfileFoundationServiceDelegate;

	/** The rest client. */
	@Mock
	private RestClientSyncImpl restClient;

	@Mock
	AdapterFilterUtility adapterFilterUtility;

	@Mock
	CustomerAccountsFilterFoundationServiceAdapter commonFilterUtility;

	/**
	 * Sets the up.
	 */
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * Context loads.
	 */
	@Test
	public void contextLoads() {
	}

	/**
	 * Test account information FS.
	 */
	@Test
	public void testAccountInformationFS() {
		Map<String, String> params = new HashMap<>();
		params.put("channelId", "B365");
		params.put(AccountInformationFoundationServiceConstants.ACCOUNT,
				AccountInformationFoundationServiceConstants.SINGLE_ACCOUNT);
		com.capgemini.psd2.foundationservice.account.information.boi.adapter.raml.domain.Account accounts = new com.capgemini.psd2.foundationservice.account.information.boi.adapter.raml.domain.Account();
		AccountMapping accountMapping = new AccountMapping();
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
		AccountDetails accDet = new AccountDetails();
		accDet.setAccountId("12345");
		accDet.setAccountNSC("nsc1234");
		accDet.setAccountNumber("acct1234");
		accDet.setAccountSubType(OBExternalAccountSubType1Code.CURRENTACCOUNT);
		;
		accDetList.add(accDet);
		accountMapping.setAccountDetails(accDetList);
		accountMapping.setTppCID("test");
		accountMapping.setPsuId("test");
		accountMapping.setCorrelationId("test");
		Mockito.when(accountInformationFoundationServiceDelegate.getFoundationServiceURL(any(), any(), any(), any()))
				.thenReturn(
						"https://mocksvc-proxy.eu1.anypoint.mulesoft.com/exchange/dd9dfe2a-e2c9-4bb7-8357-3902557f1bf9/account-process-api/1.0.17/core-banking/p/abt/v1.0/accounts/nsc1234/acct1234");
		Mockito.when(
				accountInformationFoundationServiceClient.restTransportForCreditCardInformation(any(), any(), any()))
				.thenReturn(accounts);
		Mockito.when(accountInformationFoundationServiceDelegate.transformResponseFromFDToAPI(any(), any()))
				.thenReturn(new PlatformAccountInformationResponse());
		Mockito.when(restClient.callForGet(any(), any(), any())).thenReturn(accounts);
		accountInformationFoundationServiceAdapter.retrieveAccountInformation(accountMapping, params);

	}

	@Test
	public void testAccountInformationFSCreditCard() {
		Map<String, String> params = new HashMap<>();
		params.put("channelId", "B365");
		params.put(AccountInformationFoundationServiceConstants.ACCOUNT,
				AccountInformationFoundationServiceConstants.SINGLE_ACCOUNT);
		com.capgemini.psd2.foundationservice.account.information.boi.adapter.raml.domain.Account accounts = new com.capgemini.psd2.foundationservice.account.information.boi.adapter.raml.domain.Account();
		AccountMapping accountMapping = new AccountMapping();

		OBAccount6Account credit = new OBAccount6Account();
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
		AccountDetails accDet = new AccountDetails();
		credit.setIdentification("XXXXXXXXXXXX1234");
		accDet.setAccountId("12345");
		accDet.setAccountNumber("2753000000000031");
		accDet.setAccount(credit);
		accDet.setAccountSubType(OBExternalAccountSubType1Code.CREDITCARD);
		accDetList.add(accDet);
		accountMapping.setAccountDetails(accDetList);
		accountMapping.setTppCID("test");
		accountMapping.setPsuId("test");
		accountMapping.setCorrelationId("test");
		Mockito.when(accountInformationFoundationServiceDelegate.getFoundationServiceURL(any(), any(), any(), any()))
				.thenReturn(
						"https://mocksvc-proxy.eu1.anypoint.mulesoft.com/exchange/dd9dfe2a-e2c9-4bb7-8357-3902557f1bf9/account-process-api/1.0.17/core-banking/p/abt/v1.0/accounts/creditcards/1234/2753000000000031");
		Mockito.when(
				accountInformationFoundationServiceClient.restTransportForCreditCardInformation(any(), any(), any()))
				.thenReturn(accounts);
		Mockito.when(accountInformationFoundationServiceDelegate.transformResponseFromFDToAPI(any(), any()))
				.thenReturn(new PlatformAccountInformationResponse());
		Mockito.when(restClient.callForGet(any(), any(), any())).thenReturn(accounts);
		accountInformationFoundationServiceAdapter.retrieveAccountInformation(accountMapping, params);

	}

	/**
	 * Test multiple account information FS.
	 */

	@Test
	public void testMultipleAccountInformationFS() {
		Map<String, String> params = new HashMap<>();
		params.put("channelId", "B365");
		params.put(AccountInformationFoundationServiceConstants.ACCOUNT,
				AccountInformationFoundationServiceConstants.MULTI_ACCOUNT);
		OBAccount6Account obAccount = new OBAccount6Account();
		AccountMapping accountMapping = new AccountMapping();
		DigitalUserProfile accounts = new DigitalUserProfile();
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
		AccountDetails accDet = new AccountDetails();
		AccountDetails accDet1 = new AccountDetails();
		accDet.setAccountId("12345");
		accDet.setAccountNSC("nsc1234");
		accDet.setAccountNumber("acct1234");
		accDet.setAccountSubType(OBExternalAccountSubType1Code.CURRENTACCOUNT);
		accDet1.setAccountId("2345");
		accDet1.setAccountNumber("54341238");
		obAccount.setIdentification("XXXXXXXXXXXX2000");
		accDet1.setAccount(obAccount);
		;
		accDet1.setAccountSubType(OBExternalAccountSubType1Code.CREDITCARD);
		accDetList.add(accDet);
		accDetList.add(accDet1);
		accountMapping.setAccountDetails(accDetList);
		accountMapping.setTppCID("test");
		accountMapping.setPsuId("test");
		accountMapping.setCorrelationId("test");

		Mockito.when(customerAccountProfileFoundationServiceDelegate.getFoundationServiceURLAISP(any(), any()))
				.thenReturn(
						"https://mocksvc-proxy.eu1.anypoint.mulesoft.com/exchange/dd9dfe2a-e2c9-4bb7-8357-3902557f1bf9/party/1.0.12/core-banking/party/p/v1.0/channels/B365/digitalusers/USER001/digital-user-profile");
		Mockito.when(accountInformationFoundationServiceDelegate.transformResponseFromFDToAPI(any(), any()))
				.thenReturn(new PlatformAccountInformationResponse());
		Mockito.when(commonFilterUtility.retrieveCustomerAccountList(any(), any())).thenReturn(accounts);
		Mockito.when(restClient.callForGet(any(), any(), any())).thenReturn(accounts);
		accountInformationFoundationServiceAdapter.retrieveMultipleAccountsInformation(accountMapping, params);
	}

	/**
	 * Test account information accnt mapping as null.
	 */
	@Test(expected = AdapterException.class)
	public void testAccountInformationAccntMappingAsNull() {

		Mockito.when(
				accountInformationFoundationServiceDelegate.createRequestHeaders(anyObject(), anyObject(), anyMap()))
				.thenReturn(new HttpHeaders());
		accountInformationFoundationServiceAdapter.retrieveAccountInformation(null, new HashMap<String, String>());
	}

	@Test(expected = AdapterException.class)
	public void testParamsAsNull() {
		accountInformationFoundationServiceAdapter.retrieveAccountInformation(new AccountMapping(), null);
	}

	/**
	 * Test account information account as null.
	 */
	@Test(expected = AdapterException.class)
	public void testAccountInformationAccountAsNull() {

		Mockito.when(accountInformationFoundationServiceClient.restTransportForCreditCardInformation(anyObject(),
				anyObject(), anyObject())).thenReturn(null);
		accountInformationFoundationServiceAdapter.retrieveAccountInformation(null, new HashMap<String, String>());
	}

	/**
	 * Test exception.
	 */
	@Test(expected = AdapterException.class)
	public void testException() {
		Map<String, String> params = new HashMap<>();
		params.put("channelId", "B365");
		params.put(AccountInformationFoundationServiceConstants.ACCOUNT,
				AccountInformationFoundationServiceConstants.SINGLE_ACCOUNT);
		AccountMapping accountMapping = new AccountMapping();
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
		AccountDetails accDet = new AccountDetails();
		accDet.setAccountId("12345");
		accDet.setAccountNSC("nsc1234");
		accDet.setAccountNumber("acct1234");
		accDetList.add(accDet);
		accountMapping.setAccountDetails(accDetList);
		accountMapping.setTppCID("test");
		accountMapping.setPsuId(null);
		Mockito.when(accountInformationFoundationServiceDelegate.getFoundationServiceURL(any(), any(), any(), any()))
				.thenReturn(
						"https://mocksvc-proxy.eu1.anypoint.mulesoft.com/exchange/dd9dfe2a-e2c9-4bb7-8357-3902557f1bf9/account-process-api/1.0.17/core-banking/p/abt/v1.0/accounts/nsc1234/acct1234");
		Mockito.when(
				accountInformationFoundationServiceClient.restTransportForCreditCardInformation(any(), any(), any()))
				.thenReturn(null);
		Mockito.when(accountInformationFoundationServiceDelegate.transformResponseFromFDToAPI(any(), any()))
				.thenReturn(new PlatformAccountInformationResponse());
		accountInformationFoundationServiceAdapter.retrieveAccountInformation(null, new HashMap<String, String>());
	}

	@Test(expected = AdapterException.class)
	public void testExceptionParam() {
		Map<String, String> params = new HashMap<>();
		params.put("channelId", "chanel");
		params.put(AccountInformationFoundationServiceConstants.ACCOUNT,
				AccountInformationFoundationServiceConstants.SINGLE_ACCOUNT);
		AccountMapping accountMapping = new AccountMapping();
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
		AccountDetails accDet = new AccountDetails();
		accDet.setAccountId("12345");
		accDet.setAccountNSC("nsc1234");
		accDet.setAccountNumber("acct1234");
		accDet.setAccountSubType(OBExternalAccountSubType1Code.CREDITCARD);
		OBAccount6Account account = new OBAccount6Account();
		account.setIdentification("1234");
		accDet.setAccount(account);
		accDetList.add(accDet);
		accountMapping.setAccountDetails(accDetList);
		accountMapping.setTppCID("test");
		accountMapping.setPsuId("1234");
		Mockito.when(
				accountInformationFoundationServiceClient.restTransportForCreditCardInformation(any(), any(), any()))
				.thenReturn(null);
		Mockito.when(accountInformationFoundationServiceDelegate.transformResponseFromFDToAPI(any(), any()))
				.thenReturn(new PlatformAccountInformationResponse());
		accountInformationFoundationServiceAdapter.retrieveAccountInformation(accountMapping, params);
	}

	/**
	 * Test exception.
	 */
	@Test(expected = AdapterException.class)
	public void testMultiAccountException() {
		Map<String, String> params = new HashMap<>();
		params.put("channelId", "B365");
		params.put(AccountInformationFoundationServiceConstants.ACCOUNT,
				AccountInformationFoundationServiceConstants.MULTI_ACCOUNT);
		AccountMapping accountMapping = new AccountMapping();
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
		AccountDetails accDet = new AccountDetails();
		accDet.setAccountId("12345");
		accDet.setAccountNSC("nsc1234");
		accDet.setAccountNumber("acct1234");
		accDetList.add(accDet);
		accountMapping.setAccountDetails(accDetList);
		accountMapping.setTppCID("test");
		accountMapping.setPsuId("test");
		Mockito.when(accountInformationFoundationServiceDelegate.getFoundationServiceURL(any(), any(), any(), any()))
				.thenReturn(
						"https://mocksvc-proxy.eu1.anypoint.mulesoft.com/exchange/dd9dfe2a-e2c9-4bb7-8357-3902557f1bf9/account-process-api/1.0.17/core-banking/p/abt/v1.0/accounts/nsc1234/acct1234");
		Mockito.when(
				accountInformationFoundationServiceClient.restTransportForCreditCardInformation(any(), any(), any()))
				.thenReturn(null);
		Mockito.when(accountInformationFoundationServiceDelegate.transformResponseFromFDToAPI(any(), any()))
				.thenReturn(new PlatformAccountInformationResponse());
		accountInformationFoundationServiceAdapter.retrieveMultipleAccountsInformation(null,
				new HashMap<String, String>());
	}

	/**
	 * Test null exception.
	 */
	@Test(expected = AdapterException.class)
	public void testNullException() {
		Map<String, String> params = new HashMap<>();
		params.put("channelId", "B365");
		params.put(AccountInformationFoundationServiceConstants.ACCOUNT,
				AccountInformationFoundationServiceConstants.SINGLE_ACCOUNT);
		// AccountBaseType accounts = new AccountBaseType();
		com.capgemini.psd2.foundationservice.account.information.boi.adapter.raml.domain.Account accounts = new com.capgemini.psd2.foundationservice.account.information.boi.adapter.raml.domain.Account();
		AccountMapping accountMapping = null;
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
		AccountDetails accDet = new AccountDetails();
		accDet.setAccountId("12345");
		accDet.setAccountNSC("nsc1234");
		accDet.setAccountNumber("acct1234");
		accDetList.add(accDet);
		Mockito.when(accountInformationFoundationServiceDelegate.getFoundationServiceURL(any(), any(), any(), any()))
				.thenReturn(
						"https://mocksvc-proxy.eu1.anypoint.mulesoft.com/exchange/dd9dfe2a-e2c9-4bb7-8357-3902557f1bf9/account-process-api/1.0.17/core-banking/p/abt/v1.0/accounts/nsc1234/acct1234");
		Mockito.when(
				accountInformationFoundationServiceClient.restTransportForCreditCardInformation(any(), any(), any()))
				.thenReturn(accounts);
		Mockito.when(accountInformationFoundationServiceDelegate.transformResponseFromFDToAPI(any(), any()))
				.thenReturn(new PlatformAccountInformationResponse());
		accountInformationFoundationServiceAdapter.retrieveAccountInformation(accountMapping,
				new HashMap<String, String>());
	}

	/**
	 * Test multiple account information account as null.
	 */
	@Test(expected = AdapterException.class)
	public void testMultipleAccountInformationAccountAsNull() {
		Map<String, String> params = new HashMap<>();
		params.put("channelId", "B365");
		params.put(AccountInformationFoundationServiceConstants.ACCOUNT,
				AccountInformationFoundationServiceConstants.MULTI_ACCOUNT);
		AccountMapping accountMapping = new AccountMapping();
		AccountDetails accDet = new AccountDetails();
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
		accDet.setAccountId("12345");
		accDet.setAccountNSC("nsc1234");
		accDet.setAccountNumber("acct1234");
		accDetList.add(accDet);
		accountMapping.setAccountDetails(accDetList);
		accountMapping.setTppCID("test");
		accountMapping.setPsuId("test");
		Mockito.when(accountInformationFoundationServiceClient.restTransportForCreditCardInformation(anyObject(),
				anyObject(), anyObject())).thenReturn(null);
		accountInformationFoundationServiceAdapter.retrieveMultipleAccountsInformation(accountMapping, params);
	}

	/**
	 * Test account information account as null.
	 */
	@Test(expected = AdapterException.class)
	public void testFilterAccountInformationAccountAsNull() {
		Map<String, String> params = new HashMap<>();
		params.put("channelId", "B365");
		params.put(AccountInformationFoundationServiceConstants.ACCOUNT,
				AccountInformationFoundationServiceConstants.SINGLE_ACCOUNT);
		AccountMapping accountMapping = new AccountMapping();
		AccountDetails accDet = new AccountDetails();
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
		accDet.setAccountId("12345");
		accDet.setAccountNSC("nsc1234");
		accDet.setAccountNumber("acct1234");
		accDet.setAccountSubType(OBExternalAccountSubType1Code.CURRENTACCOUNT);
		accDetList.add(accDet);
		accountMapping.setAccountDetails(accDetList);
		accountMapping.setTppCID("test");
		accountMapping.setPsuId("test");
		Mockito.when(accountInformationFoundationServiceClient.restTransportForCreditCardInformation(anyObject(),
				anyObject(), anyObject())).thenReturn(null);
		accountInformationFoundationServiceAdapter.retrieveAccountInformation(accountMapping, params);
	}

	/**
	 * Test account information account as null.
	 */
	@Test(expected = AdapterException.class)
	public void testAccountMappingAccountAsNull() {
		Map<String, String> params = new HashMap<>();
		params.put("channelId", "B365");
		params.put(AccountInformationFoundationServiceConstants.ACCOUNT,
				AccountInformationFoundationServiceConstants.SINGLE_ACCOUNT);
		AccountMapping accountMapping = new AccountMapping();
		AccountDetails accDet = new AccountDetails();
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
		accDet.setAccountId("12345");
		accDet.setAccountNSC("nsc1234");
		accDet.setAccountNumber("acct1234");
		accDet.setAccountSubType(OBExternalAccountSubType1Code.CURRENTACCOUNT);
		accDetList.add(accDet);
		accountMapping.setAccountDetails(accDetList);
		accountMapping.setTppCID("test");
		accountMapping.setPsuId("test");
		Mockito.when(accountInformationFoundationServiceClient.restTransportForCreditCardInformation(anyObject(),
				anyObject(), anyObject())).thenReturn(null);
		Mockito.when(accountInformationFoundationServiceClient.restTransportForCreditCardInformation(anyObject(),
				anyObject(), anyObject())).thenReturn(null);
		accountInformationFoundationServiceAdapter.retrieveAccountInformation(accountMapping, params);
	}

	@Test(expected = AdapterException.class)
	public void testExceptionNullParams() {

		AccountMapping accountMapping = new AccountMapping();
		Map<String, String> params = null;
		accountMapping.setPsuId("test");
		accountMapping.setCorrelationId("test");
		Mockito.when(accountInformationFoundationServiceClient.restTransportForCreditCardInformation(anyObject(),
				anyObject(), anyObject())).thenReturn(null);
		accountInformationFoundationServiceAdapter.retrieveAccountInformation(accountMapping, params);

	}

	@Test(expected = AdapterException.class)
	public void testExceptionMultiNullParams() {

		AccountMapping accountMapping = new AccountMapping();
		Map<String, String> params = null;
		accountMapping.setPsuId("test");
		accountMapping.setCorrelationId("test");
		Mockito.when(accountInformationFoundationServiceClient.restTransportForCreditCardInformation(anyObject(),
				anyObject(), anyObject())).thenReturn(null);
		accountInformationFoundationServiceAdapter.retrieveMultipleAccountsInformation(accountMapping, params);

	}

	/**
	 * Test account account as null.
	 */
	@Test(expected = AdapterException.class)
	public void testAccountAccountAsNull() {
		Map<String, String> params = new HashMap<>();
		params.put("channelId", "B365");
		params.put(AccountInformationFoundationServiceConstants.ACCOUNT,
				AccountInformationFoundationServiceConstants.SINGLE_ACCOUNT);
		AccountMapping accountMapping = new AccountMapping();
		AccountDetails accDet = new AccountDetails();
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
		accDet.setAccountId("12345");
		accDet.setAccountNSC("nsc1234");
		accDet.setAccountNumber("acct1234");
		accDet.setAccountSubType(OBExternalAccountSubType1Code.CURRENTACCOUNT);
		accDetList.add(accDet);
		accountMapping.setAccountDetails(accDetList);
		accountMapping.setTppCID("test");
		accountMapping.setPsuId("test");
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnt accntFilter = new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnt();
		accntFilter.setAccountNumber("US2345");
		accntFilter.setAccountNSC("1234");
		Mockito.when(accountInformationFoundationServiceClient.restTransportForCreditCardInformation(anyObject(),
				anyObject(), anyObject())).thenReturn(null);
		Mockito.when(accountInformationFoundationServiceClient.restTransportForCreditCardInformation(anyObject(),
				anyObject(), anyObject())).thenReturn(null);
		accountInformationFoundationServiceAdapter.retrieveAccountInformation(accountMapping, params);
	}

	/**
	 * Test multiple account account as null.
	 */
	@Test(expected = AdapterException.class)
	public void testMultipleAccountAccountAsNull() {
		Map<String, String> params = new HashMap<>();
		params.put("channelId", "B365");
		params.put(AccountInformationFoundationServiceConstants.ACCOUNT,
				AccountInformationFoundationServiceConstants.MULTI_ACCOUNT);
		AccountMapping accountMapping = new AccountMapping();
		AccountDetails accDet = new AccountDetails();
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
		accDet.setAccountId("12345");
		accDet.setAccountNSC("nsc1234");
		accDet.setAccountNumber("acct1234");
		accDetList.add(accDet);
		accountMapping.setAccountDetails(accDetList);
		accountMapping.setTppCID("test");
		accountMapping.setPsuId("test");
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnts filteredAccounts = new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnts();
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnt accntFilter = new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnt();
		accntFilter.setAccountNumber("US2345");
		accntFilter.setAccountNSC("1234");
		filteredAccounts.getAccount().add(accntFilter);
		Mockito.when(accountInformationFoundationServiceClient.restTransportForCreditCardInformation(anyObject(),
				anyObject(), anyObject())).thenReturn(null);
		Mockito.when(accountInformationFoundationServiceClient.restTransportForCreditCardInformation(anyObject(),
				anyObject(), anyObject())).thenReturn(null);
		accountInformationFoundationServiceAdapter.retrieveMultipleAccountsInformation(accountMapping, params);
	}

	/**
	 * Test null exception.
	 */
	@Test
	public void testMultipleAccountInformationfilteredAccountsAsNull() {
		AccountMapping accountMapping = new AccountMapping();
		Map<String, String> params = null;
		commonFilterUtility.retrieveCustomerAccountList(accountMapping.getPsuId(), params);
	}

}
