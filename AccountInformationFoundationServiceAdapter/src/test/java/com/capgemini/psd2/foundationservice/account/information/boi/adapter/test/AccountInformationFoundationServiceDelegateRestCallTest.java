/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.foundationservice.account.information.boi.adapter.test;

import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.aisp.domain.OBAccount6Account;
import com.capgemini.psd2.aisp.domain.OBExternalAccountSubType1Code;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountInformationResponse;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.foundationservice.account.information.boi.adapter.constants.AccountInformationFoundationServiceConstants;
import com.capgemini.psd2.foundationservice.account.information.boi.adapter.delegate.AccountInformationFoundationServiceDelegate;
import com.capgemini.psd2.foundationservice.account.information.boi.adapter.raml.domain.Account;
import com.capgemini.psd2.foundationservice.account.information.boi.adapter.raml.domain.AccountEntitlements;
import com.capgemini.psd2.foundationservice.account.information.boi.adapter.raml.domain.Brand;
import com.capgemini.psd2.foundationservice.account.information.boi.adapter.raml.domain.BrandCode;
import com.capgemini.psd2.foundationservice.account.information.boi.adapter.raml.domain.Card;
import com.capgemini.psd2.foundationservice.account.information.boi.adapter.raml.domain.CreditCardAccount;
import com.capgemini.psd2.foundationservice.account.information.boi.adapter.raml.domain.Currency;
import com.capgemini.psd2.foundationservice.account.information.boi.adapter.raml.domain.CurrentAccount;
import com.capgemini.psd2.foundationservice.account.information.boi.adapter.raml.domain.DigitalUser;
import com.capgemini.psd2.foundationservice.account.information.boi.adapter.raml.domain.RetailNonRetailCode;
import com.capgemini.psd2.foundationservice.account.information.boi.adapter.raml.domain.SourceSystemAccountType;
import com.capgemini.psd2.foundationservice.account.information.boi.adapter.transformer.AccountInformationFoundationServiceTransformer;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnt;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnts;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.DigitalUserProfile;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.PartyBasicInformation;
/*import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.AccountBrand;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.AccountCurrency;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.AccountEntitlement;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.CreditCardAccountInformation;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.CurrentAccountInformation;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.DigitalUserProfile;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.PartyInformation;*/
import com.capgemini.psd2.rest.client.sync.impl.RestClientSyncImpl;
import com.capgemini.psd2.validator.PSD2Validator;

/**
 * The Class AccountInformationFoundationServiceDelegateRestCallTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class AccountInformationFoundationServiceDelegateRestCallTest {

	/** The account information FS transformer. */
	@Mock
	private AccountInformationFoundationServiceTransformer accountInformationFSTransformer = new AccountInformationFoundationServiceTransformer();

	@InjectMocks
	private AccountInformationFoundationServiceDelegate delegate;

	@Mock
	private PSD2Validator psd2Validator;

	/** The rest client. */
	@Mock
	private RestClientSyncImpl restClient;

/**
	 * Sets the up.
	 */
	@Before
	public void setUp(){
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * Context loads.
	 */
	@Test
	public void contextLoads() {
	}

	/**
	 * Test transform response from FD to API.
	 */
	@Test
	public void testTransformResponseFromFDToAPI() {
		OBAccount6Account obAccount= new OBAccount6Account();
		AccountDetails accDet1 = new AccountDetails();
		Map<String, String> params = new HashMap<>();
		params.put("accountId", "12345");
		params.put(AccountInformationFoundationServiceConstants.ACCOUNT_SUBTYPE,"CurrentAccount");
		params.put(AccountInformationFoundationServiceConstants.ACCOUNT,AccountInformationFoundationServiceConstants.SINGLE_ACCOUNT);
		Account accounts = new Account();
		Accnt acc = new Accnt();
		acc.setAccountNumber("acct1234");
		acc.setAccountNSC("nsc1234");
		acc.setCurrency("GBP");
		acc.setAccountName("BOI_PSD2");
		AccountMapping accountMapping = new AccountMapping();
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
		AccountDetails accDet = new AccountDetails();
		accDet.setAccountId("12345");
		accDet.setAccountNSC("nsc1234");
		accDet.setAccountNumber("acct1234");
		accDet.setAccountSubType(OBExternalAccountSubType1Code.CURRENTACCOUNT);
		accDetList.add(accDet);
		accDet1.setAccountId("2345");
		accDet1.setAccountNumber("54341238");
		obAccount.setIdentification("XXXXXXXXXXXX2000");
		accDet1.setAccount(obAccount);;
		accDet1.setAccountSubType(OBExternalAccountSubType1Code.CREDITCARD);
		accDetList.add(accDet1);
		accountMapping.setAccountDetails(accDetList);
		accountMapping.setTppCID("test");
		accountMapping.setPsuId("test");
		accountMapping.setCorrelationId("test");
		Mockito.when(delegate.transformResponseFromFDToAPI(accounts,params )).thenReturn(new PlatformAccountInformationResponse());
		PlatformAccountInformationResponse res = accountInformationFSTransformer.transformAccountInformation(accounts, params);
		assertNotNull(res);

	}
	@Test
	public void testTransformResponseFromFDToAPIForCreditCard() {
		OBAccount6Account obAccount= new OBAccount6Account();
		AccountDetails accDet1 = new AccountDetails();
		Card card= new Card();
		Map<String, String> params = new HashMap<>();
		params.put("123_####354896", "12345");
		params.put(AccountInformationFoundationServiceConstants.ACCOUNT_SUBTYPE,AccountInformationFoundationServiceConstants.CREDIT_CARD);
		params.put(AccountInformationFoundationServiceConstants.ACCOUNT,AccountInformationFoundationServiceConstants.SINGLE_ACCOUNT);
		
		Account accounts = new Account();
		CreditCardAccount credit = new CreditCardAccount();
		Currency currency = new Currency();
		RetailNonRetailCode retail = RetailNonRetailCode.fromValue("Retail");
		SourceSystemAccountType sourceSystemAccountType = SourceSystemAccountType.fromValue("Credit Card");

		accounts.setAccountName("cc");
		accounts.setAccountNumber("123");
		card.setMaskedCardPANNumber("####354896");
		credit.setCard(card);
		accounts.setCreditCardAccountInformation(credit);
		
		currency.setIsoAlphaCode("EUR");
		accounts.setAccountCurrency(currency);
		accounts.setRetailNonRetailCode(retail);
		accounts.setSourceSystemAccountType(sourceSystemAccountType);

		Mockito.when(delegate.transformResponseFromFDToAPI(accounts,params )).thenReturn(new PlatformAccountInformationResponse());
		PlatformAccountInformationResponse res = accountInformationFSTransformer.transformAccountInformation(accounts, params);
		assertNotNull(res);

	}
	@Test
	public void testTransformResponseFromFDToAPIForMultiAccount1() {
		OBAccount6Account obAccount= new OBAccount6Account();
		AccountDetails accDet1 = new AccountDetails();
		Map<String, String> params = new HashMap<>();
		params.put(AccountInformationFoundationServiceConstants.ACCOUNT,AccountInformationFoundationServiceConstants.MULTI_ACCOUNT);
		Accnts accounts = new Accnts();
		DigitalUser digital=new DigitalUser();
		Account account=new Account();
		PartyBasicInformation pi=new PartyBasicInformation();
		DigitalUserProfile dp=new DigitalUserProfile();
		digital.setDigitalUserLockedOutIndicator(false);
		RetailNonRetailCode retail = RetailNonRetailCode.fromValue("Retail");
		SourceSystemAccountType sourceSystemAccountType = SourceSystemAccountType.fromValue("Current Account");
		BrandCode brandCode = BrandCode.fromValue("BOI-NI");
		pi.setPartyActiveIndicator(true);
		AccountEntitlements acen=new AccountEntitlements();
		Currency currency = new Currency();
		CurrentAccount cuacinfo=new CurrentAccount();
		currency.setIsoAlphaCode("EUR");
		account.setAccountCurrency(currency);
		acen.setAccount(account);
		account.setRetailNonRetailCode(retail);
		account.setSourceSystemAccountType(sourceSystemAccountType);
		account.setAccountNumber("123456");
		cuacinfo.setParentNationalSortCodeNSCNumber("parentNationalSortCodeNSCNumber");
		account.setCurrentAccountInformation(cuacinfo);
		Brand brand=new Brand();
		account.setAccountBrand(brand);
		brand.setBrandCode(brandCode);
		Accnts accounts1 = new Accnts();
		Account account1= new Account();
		account1.setAccountNumber("2345");
		acen.setAccount(account);
		AccountEntitlements acen1=new AccountEntitlements();
		acen1.setAccount(account1);
		List<AccountEntitlements> accountEntitlements = new ArrayList<AccountEntitlements>();
		accountEntitlements.add(acen);
		accountEntitlements.add(acen1);
		Mockito.when(delegate.transformResponseFromFDToAPI(dp,params )).thenReturn(new PlatformAccountInformationResponse());
		PlatformAccountInformationResponse res = accountInformationFSTransformer.transformAccountInformation(dp, params);
		assertNotNull(res);

	}
	@Test
	public void testTransformResponseFromFDToAPIForMultiAccount2() {
		OBAccount6Account obAccount= new OBAccount6Account();
		AccountDetails accDet1 = new AccountDetails();
		Map<String, String> params = new HashMap<>();
		params.put(AccountInformationFoundationServiceConstants.ACCOUNT,AccountInformationFoundationServiceConstants.MULTI_ACCOUNT);
		Accnts accounts = new Accnts();
		DigitalUser digital=new DigitalUser();
		Account account=new Account();
		PartyBasicInformation pi=new PartyBasicInformation();
		DigitalUserProfile dp=new DigitalUserProfile();
		RetailNonRetailCode retail = RetailNonRetailCode.fromValue("Retail");
		SourceSystemAccountType sourceSystemAccountType = SourceSystemAccountType.fromValue("Savings Account");
		BrandCode brandCode = BrandCode.fromValue("BOI-NI");
		digital.setDigitalUserLockedOutIndicator(false);
		pi.setPartyActiveIndicator(true);
		AccountEntitlements acen=new AccountEntitlements();
		Currency currency = new Currency();
		CurrentAccount cuacinfo=new CurrentAccount();
		currency.setIsoAlphaCode("EUR");
		account.setAccountCurrency(currency);
		acen.setAccount(account);
		account.setRetailNonRetailCode(retail);
		account.setSourceSystemAccountType(sourceSystemAccountType);
		account.setAccountNumber("123456");
		cuacinfo.setParentNationalSortCodeNSCNumber("parentNationalSortCodeNSCNumber");
		account.setCurrentAccountInformation(cuacinfo);
		Brand brand=new Brand();
		account.setAccountBrand(brand);
		brand.setBrandCode(brandCode);
		Accnts accounts1 = new Accnts();
		Account account1= new Account();
		account1.setAccountNumber("2345");
		acen.setAccount(account);
		AccountEntitlements acen1=new AccountEntitlements();
		acen1.setAccount(account1);
		List<AccountEntitlements> accountEntitlements = new ArrayList<AccountEntitlements>();
		accountEntitlements.add(acen);
		accountEntitlements.add(acen1);
		Mockito.when(delegate.transformResponseFromFDToAPI(dp,params )).thenReturn(new PlatformAccountInformationResponse());
		PlatformAccountInformationResponse res = accountInformationFSTransformer.transformAccountInformation(dp, params);
		assertNotNull(res);

	}
	@Test
	public void testTransformResponseFromFDToAPIForMultiAccount3CreditCard() {
		OBAccount6Account obAccount= new OBAccount6Account();
		AccountDetails accDet1 = new AccountDetails();
		DigitalUserProfile dp=new DigitalUserProfile();
		Card card= new Card();
		CreditCardAccount cc = new CreditCardAccount();
		Currency currency = new Currency();
		Map<String, String> params = new HashMap<>();
		params.put("123_####354896", "12345");
		params.put(AccountInformationFoundationServiceConstants.ACCOUNT,AccountInformationFoundationServiceConstants.MULTI_ACCOUNT);
		Account accounts = new Account();
		accounts.setAccountName("cc");
		accounts.setAccountNumber("123");
		card.setMaskedCardPANNumber("####354896");
		cc.setCard(card);
		accounts.setCreditCardAccountInformation(cc);
		Mockito.when(delegate.transformResponseFromFDToAPI(dp,params )).thenReturn(new PlatformAccountInformationResponse());
		PlatformAccountInformationResponse res = accountInformationFSTransformer.transformAccountInformation(dp, params);
		assertNotNull(res);

	}

}
