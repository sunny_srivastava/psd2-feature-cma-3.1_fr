package com.capgemini.psd2.foundationservice.international.payments.boi.adapter.test;

import static org.junit.Assert.assertNotNull;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpHeaders;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.foundationservice.international.payments.boi.adapter.delegate.InternationalPaymentsFoundationServiceDelegate;
import com.capgemini.psd2.foundationservice.international.payments.boi.adapter.domain.PaymentInstructionProposalInternationalComposite;
import com.capgemini.psd2.foundationservice.international.payments.boi.adapter.transformer.InternationalPaymentsFoundationServiceTransformer;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.pisp.domain.CustomIPaymentConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomIPaymentsPOSTRequest;
import com.capgemini.psd2.pisp.enums.PaymentTypeEnum;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;

public class InternationalPaymentFoundationServiceDelegateTest {

	@InjectMocks
	InternationalPaymentsFoundationServiceDelegate delegate;
	
	@Mock
	InternationalPaymentsFoundationServiceTransformer transformer;
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void createPaymentRequestHeaders() {
		ReflectionTestUtils.setField(delegate,"transactioReqHeader","transactioReqHeader");
		ReflectionTestUtils.setField(delegate,"correlationMuleReqHeader","correlation");
		ReflectionTestUtils.setField(delegate,"sourceUserReqHeader","sorceuser");
		ReflectionTestUtils.setField(delegate,"sourcesystem","source");
		ReflectionTestUtils.setField(delegate,"apiChannelCode","channel");
		ReflectionTestUtils.setField(delegate,"channelBrand","channelBrand");
		ReflectionTestUtils.setField(delegate,"internationalPaymentSubmissionSetupVersion","internationalPaymentSubmissionSetupVersion");
		ReflectionTestUtils.setField(delegate,"systemApiVersion","sysVersion");
		ReflectionTestUtils.setField(delegate,"partySourceId","partySourceId");
		
		
		Map<String, String> params=new HashMap<>();
		CustomPaymentStageIdentifiers stageIden=new CustomPaymentStageIdentifiers();
		params.put("transactioReqHeader", "boi");
		params.put("tenant_id", "BOIUK");
		params.put("sourceUserReqHeader", "sourceuser");
		params.put("apiChannelCode", "channel");
		params.put("X-BOI-CHANNEL", "channel");
		params.put("X-BOI-USER", "BOI");
		params.put("X-CORRELATION-ID", "Id");
		params.put("X-SYSTEM-API-VERSION","sysVersion");
		params.put(PSD2Constants.PARTY_IDENTIFIER,"PARTY IDENTIFIER");
		
		stageIden.setPaymentSetupVersion("1.0");
		stageIden.setPaymentConsentId("fvyfy8989");
		stageIden.setPaymentSubmissionId("submit46757");
		stageIden.setPaymentTypeEnum(PaymentTypeEnum.DOMESTIC_SCH_PAY);
		HttpHeaders header=delegate.createPaymentRequestHeaders(stageIden, params);
		assertNotNull(header);
		params.put("tenant_id", "BOIROI");
		HttpHeaders header1=delegate.createPaymentRequestHeaders(stageIden, params);
		assertNotNull(header1);
	}
	
	@Test
	public void createPaymentRequestHeadersPost() {
		ReflectionTestUtils.setField(delegate,"transactioReqHeader","transactioReqHeader");
		ReflectionTestUtils.setField(delegate,"correlationMuleReqHeader","correlation");
		ReflectionTestUtils.setField(delegate,"sourceUserReqHeader","sorceuser");
		ReflectionTestUtils.setField(delegate,"sourcesystem","source");
		ReflectionTestUtils.setField(delegate,"apiChannelCode","channel");
		ReflectionTestUtils.setField(delegate,"channelBrand","channelBrand");
		ReflectionTestUtils.setField(delegate,"internationalPaymentSubmissionSetupVersion","internationalPaymentSubmissionSetupVersion");
		ReflectionTestUtils.setField(delegate,"systemApiVersion","sysVersion");
		ReflectionTestUtils.setField(delegate,"partySourceId","partySourceId");
		Map<String, String> params=new HashMap<>();
		
		CustomPaymentStageIdentifiers stageIden=new CustomPaymentStageIdentifiers();
		params.put("transactioReqHeader", "boi");
		params.put("tenant_id", "BOIUK");
		params.put("sourceUserReqHeader", "sourceuser");
		params.put("apiChannelCode", "channel");
		params.put("X-BOI-CHANNEL", "channel");
		params.put("X-BOI-USER", "BOI");
		params.put("X-CORRELATION-ID", "Id");
		params.put("X-SYSTEM-API-VERSION","sysVersion");
		params.put(PSD2Constants.PARTY_IDENTIFIER,"PARTY IDENTIFIER");
		
		stageIden.setPaymentSetupVersion("1.0");
		stageIden.setPaymentConsentId("fvyfy8989");
		stageIden.setPaymentSubmissionId("submit46757");
		stageIden.setPaymentTypeEnum(PaymentTypeEnum.DOMESTIC_SCH_PAY);
		HttpHeaders header=delegate.createPaymentRequestHeadersPost(params);
		assertNotNull(header);
		params.put("transactioReqHeader", "");
		params.put("tenant_id", "");
		params.put("sourceUserReqHeader", "");
		params.put("apiChannelCode", "");
		params.put("X-BOI-CHANNEL", "");
		params.put("X-BOI-USER", "");
		params.put("X-CORRELATION-ID", "");
		params.put("X-SYSTEM-API-VERSION","");
		params.put(PSD2Constants.PARTY_IDENTIFIER,"");
		HttpHeaders header1=delegate.createPaymentRequestHeadersPost(params);
		assertNotNull(header1);
		
		
	}
	
	@Test
	public void getPaymentFoundationServiceURL() {
		String paymentInstuctionProposalId="415641612";
		String internationalPaymentBaseURL= "baseUrl";
		String paymentInstructionNumber = "v2.0";
		delegate.getPaymentFoundationServiceURL(internationalPaymentBaseURL, paymentInstructionNumber);
	}
	
	@Test
	public void postPaymentFoundationServiceURL() {
		ReflectionTestUtils.setField(delegate,"internationalPaymentSubmissionSetupVersion","internationalPaymentSubmissionSetupVersion");
		String internationalPaymentBaseURL= "baseUrl";
		delegate.postPaymentFoundationServiceURL(internationalPaymentBaseURL);
	}
	
	@Test
	public void transformInternationalSubmissionResponseFromAPIToFDForInsert() {
		CustomIPaymentsPOSTRequest customIPaymentsPOSTRequest=new CustomIPaymentsPOSTRequest();
		Map<String, String> params=new HashMap<>();
		params.put("transactioReqHeader", "boi");
		params.put("tenant_id", "BOIUK");
		
		delegate.transformInternationalSubmissionResponseFromAPIToFDForInsert(customIPaymentsPOSTRequest, params);
		
//		params.put("tenant_id", "BOIROI");
//		delegate.transformInternationalSubmissionResponseFromAPIToFDForInsert(customIPaymentsPOSTRequest, params);
	}
	
	@Test
	public void transformInternationalSubmissionResponseFromAPIToFDForInsert1() {
		CustomIPaymentsPOSTRequest customIPaymentsPOSTRequest=new CustomIPaymentsPOSTRequest();
		Map<String, String> params=new HashMap<>();
		params.put("transactioReqHeader", "boi");		
		params.put("tenant_id", "BOIROI");
		delegate.transformInternationalSubmissionResponseFromAPIToFDForInsert(customIPaymentsPOSTRequest, params);
	}
	

	
	@Test
	public void transformInternationalConsentResponseFromFDToAPIForInsert() {
		PaymentInstructionProposalInternationalComposite paymentInstructionProposalCompositeResponse = new PaymentInstructionProposalInternationalComposite();

		delegate.transformInternationalConsentResponseFromFDToAPIForInsert(paymentInstructionProposalCompositeResponse);
	}
	
	@Test
	public void createPaymentRequestHeaderswithNull() {
		ReflectionTestUtils.setField(delegate,"transactioReqHeader","transactioReqHeader");
		ReflectionTestUtils.setField(delegate,"correlationMuleReqHeader","correlation");
		ReflectionTestUtils.setField(delegate,"sourceUserReqHeader","sorceuser");
		ReflectionTestUtils.setField(delegate,"sourcesystem","source");
		ReflectionTestUtils.setField(delegate,"apiChannelCode","channel");
		ReflectionTestUtils.setField(delegate,"channelBrand","channelBrand");
		ReflectionTestUtils.setField(delegate,"internationalPaymentSubmissionSetupVersion","internationalPaymentSubmissionSetupVersion");
		ReflectionTestUtils.setField(delegate,"systemApiVersion","sysVersion");
		ReflectionTestUtils.setField(delegate,"partySourceId","partySourceId");
		
		
		Map<String, String> params=new HashMap<>();
		CustomPaymentStageIdentifiers stageIden=new CustomPaymentStageIdentifiers();
		params.put("transactioReqHeader", "");
		params.put("tenant_id", "");
		params.put("sourceUserReqHeader", "");
		params.put("apiChannelCode", "");
		params.put("X-BOI-CHANNEL", "");
		params.put("X-BOI-USER", "");
		params.put("X-CORRELATION-ID", "");
		params.put("X-SYSTEM-API-VERSION","");
		params.put(PSD2Constants.PARTY_IDENTIFIER,"");
		
		stageIden.setPaymentSetupVersion("3.0");
		stageIden.setPaymentConsentId("fvyfy8989");
		stageIden.setPaymentSubmissionId("submit46757");
		stageIden.setPaymentTypeEnum(PaymentTypeEnum.DOMESTIC_SCH_PAY);
		HttpHeaders header=delegate.createPaymentRequestHeaders(stageIden, params);
		assertNotNull(header);
	}
	
}
