package com.capgemini.psd2.foundationservice.international.payments.boi.adapter.test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import com.capgemini.psd2.foundationservice.international.payments.boi.adapter.domain.AccountInformation;
import com.capgemini.psd2.foundationservice.international.payments.boi.adapter.domain.Address;
import com.capgemini.psd2.foundationservice.international.payments.boi.adapter.domain.Agent;
import com.capgemini.psd2.foundationservice.international.payments.boi.adapter.domain.Amount;
import com.capgemini.psd2.foundationservice.international.payments.boi.adapter.domain.ChargeBearer;
import com.capgemini.psd2.foundationservice.international.payments.boi.adapter.domain.Country;
import com.capgemini.psd2.foundationservice.international.payments.boi.adapter.domain.Currency;
import com.capgemini.psd2.foundationservice.international.payments.boi.adapter.domain.ExchangeRateQuote;
import com.capgemini.psd2.foundationservice.international.payments.boi.adapter.domain.FinancialEventAmount;
import com.capgemini.psd2.foundationservice.international.payments.boi.adapter.domain.PartyBasicInformation;
import com.capgemini.psd2.foundationservice.international.payments.boi.adapter.domain.PaymentInstruction2;
import com.capgemini.psd2.foundationservice.international.payments.boi.adapter.domain.PaymentInstructionCharge;
import com.capgemini.psd2.foundationservice.international.payments.boi.adapter.domain.PaymentInstructionPostalAddress;
import com.capgemini.psd2.foundationservice.international.payments.boi.adapter.domain.PaymentInstructionProposalInternational;
import com.capgemini.psd2.foundationservice.international.payments.boi.adapter.domain.PaymentInstructionProposalInternationalComposite;
import com.capgemini.psd2.foundationservice.international.payments.boi.adapter.domain.PaymentInstructionStatusCode;
import com.capgemini.psd2.foundationservice.international.payments.boi.adapter.domain.PaymentInstrumentRiskFactor;
import com.capgemini.psd2.foundationservice.international.payments.boi.adapter.domain.PaymentType;
import com.capgemini.psd2.foundationservice.international.payments.boi.adapter.domain.PriorityCode;
import com.capgemini.psd2.foundationservice.international.payments.boi.adapter.domain.ProposingPartyAccount;
import com.capgemini.psd2.foundationservice.international.payments.boi.adapter.domain.Purpose;
import com.capgemini.psd2.foundationservice.international.payments.boi.adapter.domain.RateQuoteType;
import com.capgemini.psd2.foundationservice.international.payments.boi.adapter.transformer.InternationalPaymentsFoundationServiceTransformer;
import com.capgemini.psd2.pisp.domain.CustomIPaymentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.OBBranchAndFinancialInstitutionIdentification3;
import com.capgemini.psd2.pisp.domain.OBCashAccountCreditor2;
import com.capgemini.psd2.pisp.domain.OBCashAccountDebtor3;
import com.capgemini.psd2.pisp.domain.OBChargeBearerType1Code;
import com.capgemini.psd2.pisp.domain.OBDomestic1InstructedAmount;
import com.capgemini.psd2.pisp.domain.OBExchangeRate1;
import com.capgemini.psd2.pisp.domain.OBExternalPaymentContext1Code;
import com.capgemini.psd2.pisp.domain.OBInternational1;
import com.capgemini.psd2.pisp.domain.OBPartyIdentification43;
import com.capgemini.psd2.pisp.domain.OBPostalAddress6;
import com.capgemini.psd2.pisp.domain.OBRemittanceInformation1;
import com.capgemini.psd2.pisp.domain.OBRisk1;
import com.capgemini.psd2.pisp.domain.OBRisk1DeliveryAddress;
import com.capgemini.psd2.pisp.domain.OBWriteDataInternational1;
import com.capgemini.psd2.pisp.domain.OBWriteInternational1;
import com.capgemini.psd2.validator.PSD2Validator;

public class InternationalPaymentFoundationServiceTransformerTest {
	@InjectMocks
	InternationalPaymentsFoundationServiceTransformer transformer;

	@Mock
	private PSD2Validator psd2Validator;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void contextLoads() {
	}

	@Test
	public void testtransformInternationalPaymentResponse11() {
		PaymentInstructionProposalInternationalComposite input = new PaymentInstructionProposalInternationalComposite();
		PaymentInstructionProposalInternational inputBalanceObj = new PaymentInstructionProposalInternational();
		PaymentInstruction2 paymentInstruction = new PaymentInstruction2();

		paymentInstruction.setInstructionIssueDate("23/12/2019");
		paymentInstruction.setInstructionStatusUpdateDateTime("2019-12-23T13:03:25+05:30");
		paymentInstruction.setPaymentInstructionNumber("6145b3c4-bf37-4cbf-8bfb-cf13a49ed1c0");
		paymentInstruction.setPaymentInstructionStatusCode(PaymentInstructionStatusCode.ACCEPTEDSETTLEMENTINPROCESS);

		inputBalanceObj.setPaymentInstructionProposalId("8545b3c4-cf37-2cbf-5bfb-df13a49ed1c0");
		inputBalanceObj.setInstructionEndToEndReference("user");
		inputBalanceObj.setInstructionReference("boi");
		inputBalanceObj.setInstructionLocalInstrument("card");
		inputBalanceObj.setPriorityCode(PriorityCode.NORMAL);
		Purpose purpose=new Purpose();
		purpose.setPurposeCode("business");
		inputBalanceObj.setPurpose(purpose);

		List<PaymentInstructionCharge> charges1 = new ArrayList<>();
		PaymentInstructionCharge pa = new PaymentInstructionCharge();
		pa.setChargeBearer(ChargeBearer.BORNEBYDEBTOR);
		charges1.add(pa);
		inputBalanceObj.setCharges(charges1);
		Currency cr1 = new Currency();
		cr1.setIsoAlphaCode("EUR");
		inputBalanceObj.setCurrencyOfTransfer(cr1);

		Amount amt = new Amount();
		amt.setTransactionCurrency(12.00);
		pa.setAmount(amt);
		Currency cr = new Currency();
		cr.setIsoAlphaCode("a1s2");
		inputBalanceObj.setCurrencyOfTransfer(cr);
		pa.setCurrency(cr);
		FinancialEventAmount amount = new FinancialEventAmount();
		amount.setTransactionCurrency(23.0);
		inputBalanceObj.setFinancialEventAmount(amount);
		Currency currency = new Currency();
		currency.setIsoAlphaCode("iuhio");
		inputBalanceObj.setTransactionCurrency(currency);
		
		
		AccountInformation act = new AccountInformation();
		act.setAccountName("nam");
		act.setAccountIdentification("345");
		act.setSchemeName("debit");
		act.setSecondaryIdentification("678");
		inputBalanceObj.setAuthorisingPartyAccount(act);
		ProposingPartyAccount ppa = new ProposingPartyAccount();
		ppa.setAccountName("nam");
		ppa.setAccountNumber("987");
		ppa.setSchemeName("credit");
		ppa.setSecondaryIdentification("456");
		ppa.setAccountIdentification("c4d512");
		inputBalanceObj.setProposingPartyAccount(ppa);
		PartyBasicInformation partyBasicInformation = new PartyBasicInformation();
		partyBasicInformation.setPartyName("ABCD");
		inputBalanceObj.setProposingParty(partyBasicInformation);
		PaymentInstructionPostalAddress add = new PaymentInstructionPostalAddress();
		add.setTownName("pune");
		add.setDepartment("A");
		add.setCountrySubDivision("MH");
		add.setSubDepartment("ABC");
		add.setPostCodeNumber("600789");
		add.setGeoCodeBuildingName("capg");
		add.setGeoCodeBuildingNumber("1521");
		Country country = new Country();
		country.setIsoCountryAlphaTwoCode("IND");
		add.setAddressCountry(country);
		List<String> line = new ArrayList<>();
		line.add("Talwade");
		add.setAddressLine(line);
		add.setAddressType("POSTAL");
		inputBalanceObj.setProposingPartyPostalAddress(add);

		inputBalanceObj.setPaymentType(PaymentType.INTERNATIONAL);
		PaymentInstrumentRiskFactor ref = new PaymentInstrumentRiskFactor();
		ref.setMerchantCategoryCode("BUI");
		ref.setPaymentContextCode("BILLPAYMENT");
		ref.setMerchantCustomerIdentification("SBU");
		
		ExchangeRateQuote exchangeRateQuote = new ExchangeRateQuote();
		exchangeRateQuote.setRateQuoteType(RateQuoteType.ACTUAL);
		exchangeRateQuote.setExchangeRate(2.0412);
		currency.setIsoAlphaCode("iuhio");
		exchangeRateQuote.setUnitCurrency(currency);

		inputBalanceObj.setExchangeRateQuote(exchangeRateQuote);
		inputBalanceObj.setPriorityCode(PriorityCode.NORMAL);
		Agent agent = new Agent();
		agent.setAgentName("Msdzfc");
		agent.setPartyAddress(add);
		inputBalanceObj.setProposingPartyAgent(agent);
		input.setPaymentInstructionProposalInternational(inputBalanceObj);
		input.setPaymentInstruction(paymentInstruction);
		transformer.transformInternationalConsentResponseFromFDToAPIForInsert(input);
	}

	@Test
	public void testTransformInternationalPaymentsResponseFromAPIToFDForInsert() {

		CustomIPaymentsPOSTRequest paymentConsentsRequest = new CustomIPaymentsPOSTRequest();
		
		OBWriteDataInternational1 oBWriteDataInternational1=new OBWriteDataInternational1();
		oBWriteDataInternational1.setConsentId("8545b3c4-cf37-2cbf-5bfb-df13a49ed1c0");
		OBInternational1 oBInternational1 = new OBInternational1();
		OBDomestic1InstructedAmount amount = new OBDomestic1InstructedAmount();
		OBCashAccountDebtor3 debtorAccount = new OBCashAccountDebtor3();
		OBCashAccountCreditor2 creditorAccount = new OBCashAccountCreditor2();
		OBPostalAddress6 creditorPostalAddress = new OBPostalAddress6();
		List<String> addressLine = new ArrayList<String>();
		OBRemittanceInformation1 remittanceInformation = new OBRemittanceInformation1();
		
		OBRisk1 risk = new OBRisk1();
		OBRisk1DeliveryAddress deliveryAddress = new OBRisk1DeliveryAddress();
		OBBranchAndFinancialInstitutionIdentification3 creditoragent = new OBBranchAndFinancialInstitutionIdentification3();
		OBPartyIdentification43 creditor = new OBPartyIdentification43();
		Country addressCountry = new Country();
		OBExchangeRate1 oBExchangeRate1 = new OBExchangeRate1();
		PaymentInstructionPostalAddress proposingPartyPostalAddress = new PaymentInstructionPostalAddress();
		ProposingPartyAccount proposingPartyAccount = new ProposingPartyAccount();

		OBCashAccountDebtor3 authorisingPartyAccount = new OBCashAccountDebtor3();
		Currency transactionCurrency = new Currency();
		FinancialEventAmount financialEventAmount = new FinancialEventAmount();

		financialEventAmount.setTransactionCurrency(22.01);
		transactionCurrency.setIsoAlphaCode("NZD");
		authorisingPartyAccount.setSchemeName("IBAN");
		authorisingPartyAccount.setName("Raghav");
		authorisingPartyAccount.setIdentification("85214789");
		authorisingPartyAccount.setSecondaryIdentification("698");
		proposingPartyAccount.setSchemeName("IBAN");
		proposingPartyAccount.setAccountNumber("85214789");
		proposingPartyAccount.setAccountName("Ravi");
		proposingPartyAccount.setSecondaryIdentification("547");
		addressLine.add("FirstAddressLine");
		addressLine.add("SecondAddressLine");
		proposingPartyPostalAddress.setAddressType("permanent");
		proposingPartyPostalAddress.setSubDepartment("Primary cell");
		proposingPartyPostalAddress.setDepartment("Postal");
		proposingPartyPostalAddress.setGeoCodeBuildingName("Phoenix Building");
		proposingPartyPostalAddress.setGeoCodeBuildingNumber("Unit-3");
		proposingPartyPostalAddress.setTownName("Pune");
		proposingPartyPostalAddress.setCountrySubDivision("Mumbai");
		proposingPartyPostalAddress.setAddressCountry(addressCountry);
		proposingPartyPostalAddress.setAddressLine(addressLine);

		addressCountry.setIsoCountryAlphaTwoCode("IND");
		Address counterPartyAdress = new Address();
		counterPartyAdress.setAddressCountry(addressCountry);
		PaymentInstrumentRiskFactor paymentInstructionRiskFactorReference = new PaymentInstrumentRiskFactor();
		paymentInstructionRiskFactorReference.setCounterPartyAddress(counterPartyAdress);
		deliveryAddress.setAddressLine(addressLine);
		deliveryAddress.setStreetName("Ravi Lines");
		deliveryAddress.setBuildingNumber("1233");
		deliveryAddress.setPostCode("3111455");
		deliveryAddress.setTownName("pune");
		deliveryAddress.setCountrySubDivision("ABCD");
		deliveryAddress.setCountry("India");

		risk.setPaymentContextCode(OBExternalPaymentContext1Code.BILLPAYMENT);
		risk.setMerchantCategoryCode("Retails");
		risk.setMerchantCustomerIdentification("852147");
		risk.setDeliveryAddress(deliveryAddress);
		
		remittanceInformation.setUnstructured("Code-1");
		remittanceInformation.setReference("Refer-01");

		addressLine.add("2-116");
		addressLine.add("D.no:6");
		creditorPostalAddress.setAddressType(com.capgemini.psd2.pisp.domain.OBAddressTypeCode.POSTAL);
		
		creditorPostalAddress.setDepartment("Revenue");
		creditorPostalAddress.setSubDepartment("Income");
		creditorPostalAddress.setStreetName("Ram Lines");
		creditorPostalAddress.setBuildingNumber("5th Block");
		creditorPostalAddress.setPostCode("528524");
		creditorPostalAddress.setTownName("Pune");
		creditorPostalAddress.setCountrySubDivision("Mumbai");
		creditorPostalAddress.setCountry("India");
		creditorPostalAddress.setAddressLine(addressLine);
		debtorAccount.setName("Rathod");
		debtorAccount.setSchemeName("UK.OBIE.IBAN");
		debtorAccount.setIdentification("741258963321");
		debtorAccount.setSecondaryIdentification("852");
		creditorAccount.setSchemeName("UK.OBIE.IBAN");
		creditorAccount.setIdentification("5841258963321");
		creditorAccount.setName("Singh");
		creditorAccount.setSecondaryIdentification("814");

		amount.setAmount("29.01");
		amount.setCurrency("GBP");

		oBExchangeRate1.setContractIdentification("sfd");
		oBExchangeRate1.setExchangeRate(BigDecimal.valueOf(123.0));
		
		oBInternational1.setExchangeRateInformation(oBExchangeRate1);

		oBInternational1.setCreditorAccount(creditorAccount);
		creditoragent.setPostalAddress(creditorPostalAddress);
		oBInternational1.setCreditorAgent(creditoragent);
		creditor.setPostalAddress(creditorPostalAddress);
		oBInternational1.setCreditor(creditor);
		oBInternational1.setRemittanceInformation(remittanceInformation);
		oBInternational1.setChargeBearer(OBChargeBearerType1Code.BORNEBYCREDITOR);
		oBWriteDataInternational1.setInitiation(oBInternational1);
		paymentConsentsRequest.setData(oBWriteDataInternational1);
		paymentConsentsRequest.setRisk(risk);
		
		Map<String, String> params = new HashMap<String, String>();
		params.put("tenant_id", "BOIROI");
		params.put("X-BOI-CHANNEL", "channel");
		transformer.transformInternationalSubmissionResponseFromAPIToFDForInsert(paymentConsentsRequest, params);
	}
}

