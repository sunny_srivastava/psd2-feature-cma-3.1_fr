package com.capgemini.psd2.foundationservice.international.payments.boi.adapter.test;

import java.lang.reflect.Method;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpHeaders;
import org.springframework.web.client.RestTemplate;

import com.capgemini.psd2.foundationservice.international.payments.boi.adapter.client.InternationalPaymentsFoundationServiceClient;
import com.capgemini.psd2.foundationservice.international.payments.boi.adapter.domain.PaymentInstructionProposalInternational;
import com.capgemini.psd2.foundationservice.international.payments.boi.adapter.domain.PaymentInstructionProposalInternationalComposite;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.RestClientSync;

public class InternationalPaymentFoundationServiceClientTest {

	@InjectMocks
	InternationalPaymentsFoundationServiceClient client;
	@Mock
	RestClientSync restClient;
	@Mock
	private RestTemplate restTemplate;
	
	RequestInfo reqInfo = new RequestInfo();
	Class<PaymentInstructionProposalInternationalComposite> responseType = null;
	HttpHeaders httpHeaders = new HttpHeaders();
	PaymentInstructionProposalInternationalComposite payment= new PaymentInstructionProposalInternationalComposite();
	PaymentInstructionProposalInternational paymentInstructionProposalInternational= new PaymentInstructionProposalInternational();
	
	@Before
	public void setUp() throws Exception{
		MockitoAnnotations.initMocks(this);
		Method postConstruct= InternationalPaymentsFoundationServiceClient.class.getDeclaredMethod("init");
		postConstruct.setAccessible(true);
		postConstruct.invoke(client); 
	}
	

	@Test
	public void restTransportForInternationalPaymentFoundationService() {
		payment=client.restTransportForInternationalPaymentFoundationService(reqInfo, responseType, httpHeaders);
	}
	
	@Test
	public void restTransportForInternationalPaymentFoundationServicePost() {
		payment=client.restTransportForInternationalPaymentFoundationServicePost(reqInfo, paymentInstructionProposalInternational, responseType, httpHeaders);
	}
}
