package com.capgemini.psd2.foundationservice.international.payments.boi.adapter.test;

import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpHeaders;

import com.capgemini.psd2.foundationservice.international.payments.boi.adapter.InternationalPaymentsFoundationServiceAdapter;
import com.capgemini.psd2.foundationservice.international.payments.boi.adapter.client.InternationalPaymentsFoundationServiceClient;
import com.capgemini.psd2.foundationservice.international.payments.boi.adapter.delegate.InternationalPaymentsFoundationServiceDelegate;
import com.capgemini.psd2.foundationservice.international.payments.boi.adapter.domain.AuthorisationType;
import com.capgemini.psd2.foundationservice.international.payments.boi.adapter.domain.PaymentInstruction2;
import com.capgemini.psd2.foundationservice.international.payments.boi.adapter.domain.PaymentInstructionProposalInternational;
import com.capgemini.psd2.foundationservice.international.payments.boi.adapter.domain.PaymentInstructionProposalInternationalComposite;
import com.capgemini.psd2.foundationservice.international.payments.boi.adapter.transformer.InternationalPaymentsFoundationServiceTransformer;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.domain.CustomIPaymentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.OBExternalConsentStatus1Code;
import com.capgemini.psd2.pisp.domain.OBTransactionIndividualStatus1Code;
import com.capgemini.psd2.pisp.domain.PaymentInternationalSubmitPOST201Response;
import com.capgemini.psd2.pisp.enums.PaymentTypeEnum;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.rest.client.model.RequestInfo;

public class InternationalPaymentFoundationServiceAdapterTest {
	
	@InjectMocks
	InternationalPaymentsFoundationServiceAdapter adapter;
	
	@Mock
	private InternationalPaymentsFoundationServiceDelegate intPayConsDelegate;
	
	@Mock
	private InternationalPaymentsFoundationServiceClient intPayConsClient;

	@Mock
	private InternationalPaymentsFoundationServiceTransformer intPayConsTransformer;
	
	
	CustomIPaymentsPOSTRequest customIPaymentsPOSTRequest = new CustomIPaymentsPOSTRequest();
	CustomPaymentStageIdentifiers customPaymentStageIdentifiers = new CustomPaymentStageIdentifiers();
	 PaymentInternationalSubmitPOST201Response paymentInternationalSubmitPOST201Response = new PaymentInternationalSubmitPOST201Response();
	
	OBTransactionIndividualStatus1Code successStatus=OBTransactionIndividualStatus1Code.ACCEPTEDSETTLEMENTCOMPLETED;
	OBTransactionIndividualStatus1Code failureStatus=OBTransactionIndividualStatus1Code.PENDING;
	OBTransactionIndividualStatus1Code awaitingAuthorisation=OBTransactionIndividualStatus1Code.REJECTED;
	OBTransactionIndividualStatus1Code consumed=OBTransactionIndividualStatus1Code.ACCEPTEDSETTLEMENTINPROCESS;
	
	Map<String, String> params= new HashMap<>();
	Map<String, OBTransactionIndividualStatus1Code> paymentStatusMap = new  HashMap<>();
	
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void testcreateprocessPayments() {
		RequestInfo requestInfo = new RequestInfo();
		RequestHeaderAttributes requestHeaderAttributes = new RequestHeaderAttributes();
		requestInfo.setRequestHeaderAttributes(requestHeaderAttributes);
		requestInfo.setUrl("http://localhost:9051/group-payments/p/");
		
		params.put("X-TRANSACTION-ID","PISP");
		params.put("X-CORRELATION-ID","BOL");
		params.put("X-SOURCE-SYSTEM","BOI999");
		params.put("X-SOURCE-USER","platform");
		params.put("X-CHANNEL-CODE","87654321");
		params.put("X-BOI-CHANNEL","BOIUK");
		params.put("tenant_id","BOIUK");
		params.put("X-PARTY-SOURCE-ID-NUMBER","BOIUK");
		params.put("X-BOI-USER","BOIUK");
		
		adapter.processPayments(customIPaymentsPOSTRequest, paymentStatusMap, params);
		
		HttpHeaders httpHeaders = new HttpHeaders();
	    Mockito.when(intPayConsDelegate.createPaymentRequestHeadersPost(anyObject())).thenReturn(httpHeaders);
	    
	    Mockito.when(intPayConsDelegate.postPaymentFoundationServiceURL(anyString()))
		.thenReturn(null);
	    
	    customPaymentStageIdentifiers.setPaymentSetupVersion("3.0");
	    customPaymentStageIdentifiers.setPaymentConsentId("DSTr5645");
	    customPaymentStageIdentifiers.setPaymentSubmissionId("QER234");
	    customPaymentStageIdentifiers.setPaymentTypeEnum(PaymentTypeEnum.DOMESTIC_PAY);   
	    Mockito.when(intPayConsDelegate.createPaymentRequestHeaders(anyObject(), anyObject())).thenReturn(httpHeaders);
		
	    Mockito.when(intPayConsDelegate.getPaymentFoundationServiceURL(anyString(), anyString())).thenReturn(null);
	    
	    PaymentInstructionProposalInternational paymentInstructionProposalInternational = new PaymentInstructionProposalInternational();
	    paymentInstructionProposalInternational.setAuthorisingPartyReference("yr6yr");
	    paymentInstructionProposalInternational.setAuthorisationType(AuthorisationType.SINGLE);;
	    Mockito.when(intPayConsTransformer.transformInternationalSubmissionResponseFromAPIToFDForInsert(anyObject(),anyObject())).thenReturn(paymentInstructionProposalInternational);
	    
	    Mockito.when(intPayConsTransformer.transformInternationalConsentResponseFromFDToAPIForInsert(anyObject())).thenReturn(paymentInternationalSubmitPOST201Response);
	}
	
	@Test
	public void testretrieveStagedPaymentsResponse() {
		RequestInfo requestInfo = new RequestInfo();
		RequestHeaderAttributes requestHeaderAttributes = new RequestHeaderAttributes();
		requestInfo.setRequestHeaderAttributes(requestHeaderAttributes);
		requestInfo.setUrl("http://localhost:9051/group-payments/p/");
		
		HttpHeaders httpHeaders = new HttpHeaders();
		
		adapter.retrieveStagedPaymentsResponse(customPaymentStageIdentifiers, params);
		
		 customPaymentStageIdentifiers.setPaymentSetupVersion("3.0");
		 customPaymentStageIdentifiers.setPaymentConsentId("DSTr5645");
		 customPaymentStageIdentifiers.setPaymentSubmissionId("QER234");
		 customPaymentStageIdentifiers.setPaymentTypeEnum(PaymentTypeEnum.DOMESTIC_PAY);   
		Mockito.when(intPayConsDelegate.createPaymentRequestHeaders(anyObject(), anyObject())).thenReturn(httpHeaders);
		Mockito.when(intPayConsDelegate.getPaymentFoundationServiceURL(anyString(), anyString())).thenReturn(null);
		
		PaymentInstructionProposalInternationalComposite paymentInstructionProposal = new PaymentInstructionProposalInternationalComposite();
		PaymentInstruction2 paymentInstruction = new PaymentInstruction2();
		paymentInstruction.setPaymentInstructionNumber("TDTYTU907898");
		paymentInstructionProposal.setPaymentInstruction(paymentInstruction);
		PaymentInstructionProposalInternational paymentInstructionProposalInternational = new PaymentInstructionProposalInternational();
		paymentInstructionProposalInternational.setAuthorisingPartyReference("gbuguy");
		paymentInstructionProposal.setPaymentInstructionProposalInternational(paymentInstructionProposalInternational);
		Mockito.when(intPayConsClient.restTransportForInternationalPaymentFoundationService(anyObject(),anyObject(),anyObject())).thenReturn(paymentInstructionProposal);

		Mockito.when(intPayConsTransformer.transformInternationalPaymentResponse(anyObject())).thenReturn(paymentInternationalSubmitPOST201Response);
	
	}
	
}
