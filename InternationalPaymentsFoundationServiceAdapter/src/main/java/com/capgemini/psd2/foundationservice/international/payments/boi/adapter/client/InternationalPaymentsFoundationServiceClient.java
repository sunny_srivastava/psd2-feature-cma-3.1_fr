
package com.capgemini.psd2.foundationservice.international.payments.boi.adapter.client;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.capgemini.psd2.foundationservice.international.payments.boi.adapter.domain.PaymentInstructionProposalInternational;
import com.capgemini.psd2.foundationservice.international.payments.boi.adapter.domain.PaymentInstructionProposalInternationalComposite;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.RestClientSync;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.ObjectMapper;

@Component
public class InternationalPaymentsFoundationServiceClient {

	@Autowired
	@Qualifier("restClientFoundation")
	private RestClientSync restClient;
	
	@Autowired
	private RestTemplate restTemplate;
	
	@PostConstruct
	private void init() {
		MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(Include.NON_NULL);
        converter.setObjectMapper(mapper);
        restTemplate.getMessageConverters().add(0,converter);
	}

	public PaymentInstructionProposalInternationalComposite restTransportForInternationalPaymentFoundationService(RequestInfo reqInfo,
			Class<PaymentInstructionProposalInternationalComposite> responseType, HttpHeaders headers) {
		return restClient.callForGet(reqInfo, responseType, headers);

	}

	public PaymentInstructionProposalInternationalComposite restTransportForInternationalPaymentFoundationServicePost(
			RequestInfo requestInfo, PaymentInstructionProposalInternational paymentInstructionProposalInternational,
			Class<PaymentInstructionProposalInternationalComposite> responseType, HttpHeaders httpHeaders) {
		
		return restClient.callForPost(requestInfo, paymentInstructionProposalInternational, responseType, httpHeaders);
	}

}
