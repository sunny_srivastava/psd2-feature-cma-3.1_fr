package com.capgemini.psd2.foundationservice.international.payments.boi.adapter;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.foundationservice.international.payments.boi.adapter.client.InternationalPaymentsFoundationServiceClient;
import com.capgemini.psd2.foundationservice.international.payments.boi.adapter.delegate.InternationalPaymentsFoundationServiceDelegate;
import com.capgemini.psd2.foundationservice.international.payments.boi.adapter.domain.PaymentInstructionProposalInternational;
import com.capgemini.psd2.foundationservice.international.payments.boi.adapter.domain.PaymentInstructionProposalInternationalComposite;
import com.capgemini.psd2.foundationservice.international.payments.boi.adapter.transformer.InternationalPaymentsFoundationServiceTransformer;
import com.capgemini.psd2.pisp.adapter.InternationalPaymentsAdapter;
import com.capgemini.psd2.pisp.domain.CustomIPaymentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.OBTransactionIndividualStatus1Code;
import com.capgemini.psd2.pisp.domain.PaymentInternationalSubmitPOST201Response;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.rest.client.model.RequestInfo;

@Component
public class InternationalPaymentsFoundationServiceAdapter implements InternationalPaymentsAdapter {

	@Autowired
	private InternationalPaymentsFoundationServiceDelegate intPayConsDelegate;
	
	@Autowired
	private InternationalPaymentsFoundationServiceClient intPayConsClient;

	@Autowired
	private InternationalPaymentsFoundationServiceTransformer intPayConsTransformer;
	
	@Value("${foundationService.internationalPaymentBaseURL}")
	private String internationalPaymentSubmissionBaseURL;
	
	public PaymentInternationalSubmitPOST201Response processPayments(CustomIPaymentsPOSTRequest requestBody,
			Map<String, OBTransactionIndividualStatus1Code> paymentStatusMap, Map<String, String> params) {
		
		RequestInfo requestInfo = new RequestInfo();
		HttpHeaders httpHeaders = intPayConsDelegate.createPaymentRequestHeadersPost(params);
		String internationalPaymentPostURL = intPayConsDelegate
				.postPaymentFoundationServiceURL(internationalPaymentSubmissionBaseURL);
		requestInfo.setUrl(internationalPaymentPostURL);

		PaymentInstructionProposalInternational paymentInstructionProposalInternationalRequest = intPayConsDelegate
				.transformInternationalSubmissionResponseFromAPIToFDForInsert(requestBody, params);
		PaymentInstructionProposalInternationalComposite paymentInstProposalIntCompositeResponse = intPayConsClient
				.restTransportForInternationalPaymentFoundationServicePost(requestInfo,
						paymentInstructionProposalInternationalRequest,
						PaymentInstructionProposalInternationalComposite.class, httpHeaders);

		return intPayConsDelegate
				.transformInternationalConsentResponseFromFDToAPIForInsert(paymentInstProposalIntCompositeResponse);

	}

	public PaymentInternationalSubmitPOST201Response retrieveStagedPaymentsResponse(
			CustomPaymentStageIdentifiers customPaymentStageIdentifiers, Map<String, String> params) {
		
		RequestInfo requestInfo = new RequestInfo();
		HttpHeaders httpHeaders = intPayConsDelegate.createPaymentRequestHeaders(customPaymentStageIdentifiers, params);
		String internationalPaymentURL = intPayConsDelegate.getPaymentFoundationServiceURL(
				internationalPaymentSubmissionBaseURL, customPaymentStageIdentifiers.getPaymentSubmissionId());
		requestInfo.setUrl(internationalPaymentURL);

		PaymentInstructionProposalInternationalComposite paymentInstructionProposal = intPayConsClient
				.restTransportForInternationalPaymentFoundationService(requestInfo,
						PaymentInstructionProposalInternationalComposite.class, httpHeaders);

		return intPayConsTransformer.transformInternationalPaymentResponse(paymentInstructionProposal);

	}
	
	

	
}
