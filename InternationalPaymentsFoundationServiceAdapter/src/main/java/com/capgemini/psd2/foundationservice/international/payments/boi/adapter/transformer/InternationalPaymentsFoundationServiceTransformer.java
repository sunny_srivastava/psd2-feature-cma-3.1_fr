package com.capgemini.psd2.foundationservice.international.payments.boi.adapter.transformer;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.capgemini.psd2.foundationservice.international.payments.boi.adapter.domain.AccountInformation;
import com.capgemini.psd2.foundationservice.international.payments.boi.adapter.domain.Address;
import com.capgemini.psd2.foundationservice.international.payments.boi.adapter.domain.Agent;
import com.capgemini.psd2.foundationservice.international.payments.boi.adapter.domain.BrandCode3;
import com.capgemini.psd2.foundationservice.international.payments.boi.adapter.domain.Channel;
import com.capgemini.psd2.foundationservice.international.payments.boi.adapter.domain.ChannelCode1;
import com.capgemini.psd2.foundationservice.international.payments.boi.adapter.domain.ChargeBearer;
import com.capgemini.psd2.foundationservice.international.payments.boi.adapter.domain.Country;
import com.capgemini.psd2.foundationservice.international.payments.boi.adapter.domain.Currency;
import com.capgemini.psd2.foundationservice.international.payments.boi.adapter.domain.ExchangeRateQuote;
import com.capgemini.psd2.foundationservice.international.payments.boi.adapter.domain.FinancialEventAmount;
import com.capgemini.psd2.foundationservice.international.payments.boi.adapter.domain.Party2;
import com.capgemini.psd2.foundationservice.international.payments.boi.adapter.domain.PartyBasicInformation;
import com.capgemini.psd2.foundationservice.international.payments.boi.adapter.domain.PaymentInstructionCharge;
import com.capgemini.psd2.foundationservice.international.payments.boi.adapter.domain.PaymentInstructionPostalAddress;
import com.capgemini.psd2.foundationservice.international.payments.boi.adapter.domain.PaymentInstructionProposalInternational;
import com.capgemini.psd2.foundationservice.international.payments.boi.adapter.domain.PaymentInstructionProposalInternationalComposite;
import com.capgemini.psd2.foundationservice.international.payments.boi.adapter.domain.PaymentInstructionStatusCode;
import com.capgemini.psd2.foundationservice.international.payments.boi.adapter.domain.PaymentInstrumentRiskFactor;
import com.capgemini.psd2.foundationservice.international.payments.boi.adapter.domain.PriorityCode;
import com.capgemini.psd2.foundationservice.international.payments.boi.adapter.domain.ProposingPartyAccount;
import com.capgemini.psd2.foundationservice.international.payments.boi.adapter.domain.Purpose;
import com.capgemini.psd2.foundationservice.international.payments.boi.adapter.domain.RateQuoteType;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.pisp.domain.CustomIPaymentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.OBAddressTypeCode;
import com.capgemini.psd2.pisp.domain.OBBranchAndFinancialInstitutionIdentification3;
import com.capgemini.psd2.pisp.domain.OBCashAccountCreditor2;
import com.capgemini.psd2.pisp.domain.OBCashAccountDebtor3;
import com.capgemini.psd2.pisp.domain.OBCharge1;
import com.capgemini.psd2.pisp.domain.OBCharge1Amount;
import com.capgemini.psd2.pisp.domain.OBChargeBearerType1Code;
import com.capgemini.psd2.pisp.domain.OBDomestic1InstructedAmount;
import com.capgemini.psd2.pisp.domain.OBExchangeRate1;
import com.capgemini.psd2.pisp.domain.OBExchangeRateType2Code;
import com.capgemini.psd2.pisp.domain.OBInternational1;
import com.capgemini.psd2.pisp.domain.OBPartyIdentification43;
import com.capgemini.psd2.pisp.domain.OBPostalAddress6;
import com.capgemini.psd2.pisp.domain.OBPriority2Code;
import com.capgemini.psd2.pisp.domain.OBRemittanceInformation1;
import com.capgemini.psd2.pisp.domain.OBTransactionIndividualStatus1Code;
import com.capgemini.psd2.pisp.domain.OBWriteDataInternationalResponse1;
import com.capgemini.psd2.pisp.domain.PaymentInternationalSubmitPOST201Response;
import com.capgemini.psd2.pisp.enums.ProcessExecutionStatusEnum;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Component
public class InternationalPaymentsFoundationServiceTransformer {
	
	public <T> PaymentInternationalSubmitPOST201Response transformInternationalPaymentResponse(T inputBalanceObj) {
		
		PaymentInternationalSubmitPOST201Response response = new PaymentInternationalSubmitPOST201Response();
		OBWriteDataInternationalResponse1 obWriteDataInternationalResponse1 = new OBWriteDataInternationalResponse1();
		OBInternational1 obInternational1 = new OBInternational1();

		PaymentInstructionProposalInternationalComposite paymentInstructionProposal = (PaymentInstructionProposalInternationalComposite) inputBalanceObj;
		
		obInternational1=populateAPIToFDResponse(paymentInstructionProposal, obWriteDataInternationalResponse1, obInternational1);
		
		if ((!NullCheckUtils.isNullOrEmpty(
				paymentInstructionProposal.getPaymentInstructionProposalInternational().getAuthorisingPartyReference()))
				|| (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposal.getPaymentInstructionProposalInternational()
						.getAuthorisingPartyUnstructuredReference()))) {
			OBRemittanceInformation1 oBRemittanceInformation1 = new OBRemittanceInformation1();
			boolean isInt=false;
			
			if (!NullCheckUtils.isNullOrEmpty(
					paymentInstructionProposal.getPaymentInstructionProposalInternational().getAuthorisingPartyReference())){
				oBRemittanceInformation1.setReference(
						paymentInstructionProposal.getPaymentInstructionProposalInternational().getAuthorisingPartyReference());
				isInt=true;
			}

			if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposal.getPaymentInstructionProposalInternational()
					.getAuthorisingPartyUnstructuredReference())) {
				oBRemittanceInformation1.setUnstructured(paymentInstructionProposal.getPaymentInstructionProposalInternational()
						.getAuthorisingPartyUnstructuredReference());
				isInt=true;
			}

			if(isInt)
				obInternational1.setRemittanceInformation(oBRemittanceInformation1);
		}

		obWriteDataInternationalResponse1.setInitiation(obInternational1);
		response.setData(obWriteDataInternationalResponse1);
		return response;
	}

	public PaymentInstructionProposalInternational transformInternationalSubmissionResponseFromAPIToFDForInsert(
			CustomIPaymentsPOSTRequest internationalPaymentsRequest, Map<String, String> params) {
		PaymentInstructionProposalInternational paymentInstructionProposalInternational=new PaymentInstructionProposalInternational();
		
		if (!NullCheckUtils
				.isNullOrEmpty(internationalPaymentsRequest.getData().getConsentId())) {

			paymentInstructionProposalInternational.setPaymentInstructionProposalId(internationalPaymentsRequest.getData().getConsentId());
		}

		
		//Need to check based on Headers from API

		Channel channel = new Channel();
		if (!NullCheckUtils.isNullOrEmpty(params.get(PSD2Constants.CHANNEL_IN_REQ_HEADER))) {

			channel.setChannelCode(
					ChannelCode1.fromValue((params.get(PSD2Constants.CHANNEL_IN_REQ_HEADER)).toUpperCase()));
		} 

		if (!NullCheckUtils.isNullOrEmpty(params.get(PSD2Constants.TENANT_ID))) {

			if (params.get(PSD2Constants.TENANT_ID).equalsIgnoreCase("BOIROI")) {
				channel.setBrandCode(BrandCode3.ROI);
			}

		}

		paymentInstructionProposalInternational.setChannel(channel);
		Party2 authorisingParty=new Party2();
		PartyBasicInformation partyBasicInformation=new PartyBasicInformation();
		
		/*
		 * if(channel.getChannelCode().toString().equalsIgnoreCase("BOL")) {
		 * System.out.println(channel.getChannelCode().toString()); //partyInformation
		 * field }
		 */
		if (!NullCheckUtils.isNullOrEmpty(params.get(PSD2Constants.USER_IN_REQ_HEADER))) {
			partyBasicInformation.setPartySourceIdNumber(params.get(PSD2Constants.USER_IN_REQ_HEADER).toString());
			authorisingParty.setPartyInformation(partyBasicInformation);
		}
		paymentInstructionProposalInternational.setAuthorisingParty(authorisingParty);

		
		if (!NullCheckUtils
				.isNullOrEmpty(internationalPaymentsRequest.getData().getInitiation().getInstructionIdentification())) {

			paymentInstructionProposalInternational.setInstructionReference(
					internationalPaymentsRequest.getData().getInitiation().getInstructionIdentification());
		}

		if (!NullCheckUtils
				.isNullOrEmpty(internationalPaymentsRequest.getData().getInitiation().getEndToEndIdentification())) {

			paymentInstructionProposalInternational.setInstructionEndToEndReference(
					internationalPaymentsRequest.getData().getInitiation().getEndToEndIdentification());
		}

		if (!NullCheckUtils.isNullOrEmpty(internationalPaymentsRequest.getData().getInitiation().getLocalInstrument())) {
			paymentInstructionProposalInternational.setInstructionLocalInstrument(
					internationalPaymentsRequest.getData().getInitiation().getLocalInstrument());
		}

		if (!NullCheckUtils.isNullOrEmpty(internationalPaymentsRequest.getData().getInitiation().getInstructionPriority())) {
			PriorityCode priorityCode = PriorityCode
					.fromValue(internationalPaymentsRequest.getData().getInitiation().getInstructionPriority().toString());
			paymentInstructionProposalInternational.setPriorityCode(priorityCode);
		}
		Purpose purpose = new Purpose();
		if (!NullCheckUtils.isNullOrEmpty(internationalPaymentsRequest.getData().getInitiation().getPurpose())) {
			//Updated Mapping based on RAML Upgrade v1.0.64
			purpose.setPurposeCode(internationalPaymentsRequest.getData().getInitiation().getPurpose());
			paymentInstructionProposalInternational.setPurpose(purpose);
		}
		System.out.println(internationalPaymentsRequest.getData().getInitiation().getChargeBearer());

		List<PaymentInstructionCharge> charges = new ArrayList<>();
		PaymentInstructionCharge paymentInstructionCharge = new PaymentInstructionCharge();
		if (!NullCheckUtils.isNullOrEmpty(internationalPaymentsRequest.getData().getInitiation().getChargeBearer())) {

			ChargeBearer chargeBearer = ChargeBearer
					.fromValue(internationalPaymentsRequest.getData().getInitiation().getChargeBearer().toString());

			paymentInstructionCharge.setChargeBearer(chargeBearer);
			charges.add(paymentInstructionCharge);
			paymentInstructionProposalInternational.setCharges(charges);

		}

		Currency currency1 = new Currency();
		if (!NullCheckUtils.isNullOrEmpty(internationalPaymentsRequest.getData().getInitiation().getCurrencyOfTransfer())) {

			currency1.setIsoAlphaCode(internationalPaymentsRequest.getData().getInitiation().getCurrencyOfTransfer());

			paymentInstructionProposalInternational.setCurrencyOfTransfer(currency1);

		}

		// Instructed Amount
		Currency currency2 = new Currency();
		if (!NullCheckUtils.isNullOrEmpty(internationalPaymentsRequest.getData().getInitiation().getInstructedAmount())) {

			if (!NullCheckUtils.isNullOrEmpty(
					internationalPaymentsRequest.getData().getInitiation().getInstructedAmount().getAmount())) {
				FinancialEventAmount financialEventAmount = new FinancialEventAmount();
				String amount = internationalPaymentsRequest.getData().getInitiation().getInstructedAmount().getAmount();
				Double amountDouble = Double.parseDouble(amount);
				financialEventAmount.setTransactionCurrency(amountDouble);
				paymentInstructionProposalInternational.setFinancialEventAmount(financialEventAmount);
			}

			if (!NullCheckUtils.isNullOrEmpty(
					internationalPaymentsRequest.getData().getInitiation().getInstructedAmount().getCurrency())) {
				String currency = internationalPaymentsRequest.getData().getInitiation().getInstructedAmount().getCurrency();
				currency2.setIsoAlphaCode(currency);
				paymentInstructionProposalInternational.setTransactionCurrency(currency2);
			}
		}
		ExchangeRateQuote exchangeRateQuote = new ExchangeRateQuote();
		// ExchangeRateInformation
		Currency currency3 = new Currency();
		if (!NullCheckUtils
				.isNullOrEmpty(internationalPaymentsRequest.getData().getInitiation().getExchangeRateInformation())) {

			if (!NullCheckUtils.isNullOrEmpty(
					internationalPaymentsRequest.getData().getInitiation().getExchangeRateInformation().getUnitCurrency())) {
				String currency = internationalPaymentsRequest.getData().getInitiation().getExchangeRateInformation()
						.getUnitCurrency();
				currency3.setIsoAlphaCode(currency);
				exchangeRateQuote.setUnitCurrency(currency3);
			}

			if (!NullCheckUtils.isNullOrEmpty(
					internationalPaymentsRequest.getData().getInitiation().getExchangeRateInformation().getExchangeRate())) {

				Double exchangeRate = internationalPaymentsRequest.getData().getInitiation().getExchangeRateInformation()
						.getExchangeRate().doubleValue();
				exchangeRateQuote.setExchangeRate(exchangeRate);
			}

			if (!NullCheckUtils.isNullOrEmpty(
					internationalPaymentsRequest.getData().getInitiation().getExchangeRateInformation().getRateType())) {

				RateQuoteType rateQuoteType = RateQuoteType.fromValue(internationalPaymentsRequest.getData().getInitiation()
						.getExchangeRateInformation().getRateType().toString());
				exchangeRateQuote.setRateQuoteType(rateQuoteType);
			}

			paymentInstructionProposalInternational.setExchangeRateQuote(exchangeRateQuote);
			
		}

		// Debtor Account

		if (!NullCheckUtils.isNullOrEmpty(internationalPaymentsRequest.getData().getInitiation().getDebtorAccount())) {
			AccountInformation authorisingPartyAccount = new AccountInformation();

			
			if (!NullCheckUtils.isNullOrEmpty(
					internationalPaymentsRequest.getData().getInitiation().getDebtorAccount().getSchemeName())) {
				authorisingPartyAccount.setSchemeName(
						internationalPaymentsRequest.getData().getInitiation().getDebtorAccount().getSchemeName());
			}

			if (!NullCheckUtils.isNullOrEmpty(
					internationalPaymentsRequest.getData().getInitiation().getDebtorAccount().getIdentification())) {
				authorisingPartyAccount.setAccountIdentification(
						internationalPaymentsRequest.getData().getInitiation().getDebtorAccount().getIdentification());
			}

			if (!NullCheckUtils
					.isNullOrEmpty(internationalPaymentsRequest.getData().getInitiation().getDebtorAccount().getName())) {
				authorisingPartyAccount
						.setAccountName(internationalPaymentsRequest.getData().getInitiation().getDebtorAccount().getName());
			}

			if (!NullCheckUtils.isNullOrEmpty(
					internationalPaymentsRequest.getData().getInitiation().getDebtorAccount().getSecondaryIdentification())) {
				authorisingPartyAccount.setSecondaryIdentification(internationalPaymentsRequest.getData().getInitiation()
						.getDebtorAccount().getSecondaryIdentification());
			}

			paymentInstructionProposalInternational.setAuthorisingPartyAccount(authorisingPartyAccount);
		}
		// Creditor Account

		if (!NullCheckUtils.isNullOrEmpty(internationalPaymentsRequest.getData().getInitiation().getCreditorAccount())) {
			ProposingPartyAccount proposingPartyAccount = new ProposingPartyAccount();
			if (!NullCheckUtils.isNullOrEmpty(
					internationalPaymentsRequest.getData().getInitiation().getCreditorAccount().getSchemeName())) {

				proposingPartyAccount.setSchemeName(
						internationalPaymentsRequest.getData().getInitiation().getCreditorAccount().getSchemeName());
			}

			if (!NullCheckUtils.isNullOrEmpty(
					internationalPaymentsRequest.getData().getInitiation().getCreditorAccount().getIdentification())) {

				proposingPartyAccount.setAccountIdentification(
						internationalPaymentsRequest.getData().getInitiation().getCreditorAccount().getIdentification());
			}

			if (!NullCheckUtils
					.isNullOrEmpty(internationalPaymentsRequest.getData().getInitiation().getCreditorAccount().getName())) {

				proposingPartyAccount.setAccountName(
						internationalPaymentsRequest.getData().getInitiation().getCreditorAccount().getName());
			}

			if (!NullCheckUtils.isNullOrEmpty(internationalPaymentsRequest.getData().getInitiation().getCreditorAccount()
					.getSecondaryIdentification())) {
				proposingPartyAccount.setSecondaryIdentification(internationalPaymentsRequest.getData().getInitiation()
						.getCreditorAccount().getSecondaryIdentification());
			}

			paymentInstructionProposalInternational.setProposingPartyAccount(proposingPartyAccount);
		}

		// CreditorAgent
		Agent agent = new Agent();
		if (!NullCheckUtils.isNullOrEmpty(internationalPaymentsRequest.getData().getInitiation().getCreditorAgent())) {

			if (!NullCheckUtils.isNullOrEmpty(
					internationalPaymentsRequest.getData().getInitiation().getCreditorAgent().getSchemeName())) {
				agent.setSchemeName(
						internationalPaymentsRequest.getData().getInitiation().getCreditorAgent().getSchemeName());
			}

			if (!NullCheckUtils.isNullOrEmpty(
					internationalPaymentsRequest.getData().getInitiation().getCreditorAgent().getIdentification())) {
				agent.setAgentIdentification(
						internationalPaymentsRequest.getData().getInitiation().getCreditorAgent().getIdentification());
			}

			if (!NullCheckUtils
					.isNullOrEmpty(internationalPaymentsRequest.getData().getInitiation().getCreditorAgent().getName())) {
				agent.setAgentName(internationalPaymentsRequest.getData().getInitiation().getCreditorAgent().getName());
			}

			// Postal Address

			PaymentInstructionPostalAddress paymentInstructionPostalAddress = new PaymentInstructionPostalAddress();
			if (!NullCheckUtils.isNullOrEmpty(
					internationalPaymentsRequest.getData().getInitiation().getCreditorAgent().getPostalAddress())) {

				if (!NullCheckUtils.isNullOrEmpty(internationalPaymentsRequest.getData().getInitiation().getCreditorAgent()
						.getPostalAddress().getAddressType())) {

					paymentInstructionPostalAddress.setAddressType(internationalPaymentsRequest.getData().getInitiation()
							.getCreditorAgent().getPostalAddress().getAddressType().toString());

				}

				if (!NullCheckUtils.isNullOrEmpty(internationalPaymentsRequest.getData().getInitiation().getCreditorAgent()
						.getPostalAddress().getDepartment())) {

					paymentInstructionPostalAddress.setDepartment(internationalPaymentsRequest.getData().getInitiation()
							.getCreditorAgent().getPostalAddress().getDepartment().toString());

				}

				if (!NullCheckUtils.isNullOrEmpty(internationalPaymentsRequest.getData().getInitiation().getCreditorAgent()
						.getPostalAddress().getSubDepartment())) {

					paymentInstructionPostalAddress.setSubDepartment(internationalPaymentsRequest.getData().getInitiation()
							.getCreditorAgent().getPostalAddress().getSubDepartment().toString());

				}

				if (!NullCheckUtils.isNullOrEmpty(internationalPaymentsRequest.getData().getInitiation().getCreditorAgent()
						.getPostalAddress().getStreetName())) {

					paymentInstructionPostalAddress.setGeoCodeBuildingName(internationalPaymentsRequest.getData()
							.getInitiation().getCreditorAgent().getPostalAddress().getStreetName().toString());
				}

				if (!NullCheckUtils.isNullOrEmpty(internationalPaymentsRequest.getData().getInitiation().getCreditorAgent()
						.getPostalAddress().getBuildingNumber())) {

					paymentInstructionPostalAddress.setGeoCodeBuildingNumber(internationalPaymentsRequest.getData()
							.getInitiation().getCreditorAgent().getPostalAddress().getBuildingNumber().toString());
				}

				if (!NullCheckUtils.isNullOrEmpty(internationalPaymentsRequest.getData().getInitiation().getCreditorAgent()
						.getPostalAddress().getPostCode())) {

					paymentInstructionPostalAddress.setPostCodeNumber(internationalPaymentsRequest.getData().getInitiation()
							.getCreditorAgent().getPostalAddress().getPostCode().toString());
				}
				if (!NullCheckUtils.isNullOrEmpty(internationalPaymentsRequest.getData().getInitiation().getCreditorAgent()
						.getPostalAddress().getTownName())) {

					paymentInstructionPostalAddress.setTownName(internationalPaymentsRequest.getData().getInitiation()
							.getCreditorAgent().getPostalAddress().getTownName().toString());
				}

				if (!NullCheckUtils.isNullOrEmpty(internationalPaymentsRequest.getData().getInitiation().getCreditorAgent()
						.getPostalAddress().getCountrySubDivision())) {

					paymentInstructionPostalAddress.setCountrySubDivision(internationalPaymentsRequest.getData()
							.getInitiation().getCreditorAgent().getPostalAddress().getCountrySubDivision().toString());
				}
				Country country = new Country();
				if (!NullCheckUtils.isNullOrEmpty(internationalPaymentsRequest.getData().getInitiation().getCreditorAgent()
						.getPostalAddress().getCountry())) {
					country.setIsoCountryAlphaTwoCode(internationalPaymentsRequest.getData().getInitiation()
							.getCreditorAgent().getPostalAddress().getCountry().toString());
					paymentInstructionPostalAddress.setAddressCountry(country);
				}
				if (!NullCheckUtils.isNullOrEmpty(internationalPaymentsRequest.getData().getInitiation().getCreditorAgent()
						.getPostalAddress().getAddressLine())) {
					List<String> addLine = internationalPaymentsRequest.getData().getInitiation().getCreditorAgent()
							.getPostalAddress().getAddressLine();
					if (!NullCheckUtils.isNullOrEmpty(addLine) && addLine.size() > 0) {
						paymentInstructionPostalAddress.setAddressLine(internationalPaymentsRequest.getData().getInitiation()
								.getCreditorAgent().getPostalAddress().getAddressLine());
					}
				}
				agent.setPartyAddress(paymentInstructionPostalAddress);
				paymentInstructionProposalInternational.setProposingPartyAgent(agent);
			}
		}

		// Creditor
		PartyBasicInformation partyBasicInformation1 = new PartyBasicInformation();
		if (!NullCheckUtils.isNullOrEmpty(internationalPaymentsRequest.getData().getInitiation().getCreditor())) {

			if (!NullCheckUtils
					.isNullOrEmpty(internationalPaymentsRequest.getData().getInitiation().getCreditor().getName())) {
				partyBasicInformation1
						.setPartyName(internationalPaymentsRequest.getData().getInitiation().getCreditor().getName());
				paymentInstructionProposalInternational.setProposingParty(partyBasicInformation1);
			}

			// Postal Address of Creditor

			PaymentInstructionPostalAddress paymentInstructionPostalAddress = new PaymentInstructionPostalAddress();
			if (!NullCheckUtils
					.isNullOrEmpty(internationalPaymentsRequest.getData().getInitiation().getCreditor().getPostalAddress())) {

				if (!NullCheckUtils.isNullOrEmpty(internationalPaymentsRequest.getData().getInitiation().getCreditor()
						.getPostalAddress().getAddressType())) {

					paymentInstructionPostalAddress.setAddressType(internationalPaymentsRequest.getData().getInitiation()
							.getCreditor().getPostalAddress().getAddressType().toString());

				}

				if (!NullCheckUtils.isNullOrEmpty(internationalPaymentsRequest.getData().getInitiation().getCreditor()
						.getPostalAddress().getDepartment())) {

					paymentInstructionPostalAddress.setDepartment(internationalPaymentsRequest.getData().getInitiation()
							.getCreditor().getPostalAddress().getDepartment().toString());

				}

				if (!NullCheckUtils.isNullOrEmpty(internationalPaymentsRequest.getData().getInitiation().getCreditor()
						.getPostalAddress().getSubDepartment())) {

					paymentInstructionPostalAddress.setSubDepartment(internationalPaymentsRequest.getData().getInitiation()
							.getCreditor().getPostalAddress().getSubDepartment().toString());

				}

				if (!NullCheckUtils.isNullOrEmpty(internationalPaymentsRequest.getData().getInitiation().getCreditor()
						.getPostalAddress().getStreetName())) {

					paymentInstructionPostalAddress.setGeoCodeBuildingName(internationalPaymentsRequest.getData()
							.getInitiation().getCreditor().getPostalAddress().getStreetName().toString());
				}

				if (!NullCheckUtils.isNullOrEmpty(internationalPaymentsRequest.getData().getInitiation().getCreditor()
						.getPostalAddress().getBuildingNumber())) {

					paymentInstructionPostalAddress.setGeoCodeBuildingNumber(internationalPaymentsRequest.getData()
							.getInitiation().getCreditor().getPostalAddress().getBuildingNumber().toString());
				}

				if (!NullCheckUtils.isNullOrEmpty(internationalPaymentsRequest.getData().getInitiation().getCreditor()
						.getPostalAddress().getPostCode())) {

					paymentInstructionPostalAddress.setPostCodeNumber(internationalPaymentsRequest.getData().getInitiation()
							.getCreditor().getPostalAddress().getPostCode().toString());
				}
				if (!NullCheckUtils.isNullOrEmpty(internationalPaymentsRequest.getData().getInitiation().getCreditor()
						.getPostalAddress().getTownName())) {

					paymentInstructionPostalAddress.setTownName(internationalPaymentsRequest.getData().getInitiation()
							.getCreditor().getPostalAddress().getTownName().toString());
				}

				if (!NullCheckUtils.isNullOrEmpty(internationalPaymentsRequest.getData().getInitiation().getCreditor()
						.getPostalAddress().getCountrySubDivision())) {

					paymentInstructionPostalAddress.setCountrySubDivision(internationalPaymentsRequest.getData()
							.getInitiation().getCreditor().getPostalAddress().getCountrySubDivision().toString());
				}
				Country country = new Country();
				if (!NullCheckUtils.isNullOrEmpty(internationalPaymentsRequest.getData().getInitiation().getCreditor()
						.getPostalAddress().getCountry())) {
					country.setIsoCountryAlphaTwoCode(internationalPaymentsRequest.getData().getInitiation().getCreditor()
							.getPostalAddress().getCountry().toString());
					paymentInstructionPostalAddress.setAddressCountry(country);
				}
				if (!NullCheckUtils.isNullOrEmpty(internationalPaymentsRequest.getData().getInitiation().getCreditor()
						.getPostalAddress().getAddressLine())) {
					List<String> addLine = internationalPaymentsRequest.getData().getInitiation().getCreditor()
							.getPostalAddress().getAddressLine();
					if (!NullCheckUtils.isNullOrEmpty(addLine) && addLine.size() > 0) {
						paymentInstructionPostalAddress.setAddressLine(internationalPaymentsRequest.getData().getInitiation()
								.getCreditor().getPostalAddress().getAddressLine());
					}
				}

				paymentInstructionProposalInternational.setProposingPartyPostalAddress(paymentInstructionPostalAddress);

			}
		}

		// Remittance Information

		if (!NullCheckUtils
				.isNullOrEmpty(internationalPaymentsRequest.getData().getInitiation().getRemittanceInformation())) {

			if (!NullCheckUtils.isNullOrEmpty(
					internationalPaymentsRequest.getData().getInitiation().getRemittanceInformation().getUnstructured())) {
				paymentInstructionProposalInternational.setAuthorisingPartyUnstructuredReference(
						internationalPaymentsRequest.getData().getInitiation().getRemittanceInformation().getUnstructured());
			}

			if (!NullCheckUtils.isNullOrEmpty(
					internationalPaymentsRequest.getData().getInitiation().getRemittanceInformation().getReference())) {
				paymentInstructionProposalInternational.setAuthorisingPartyReference(
						internationalPaymentsRequest.getData().getInitiation().getRemittanceInformation().getReference());
			}
		}

		
		// Risk

		if (!NullCheckUtils.isNullOrEmpty(internationalPaymentsRequest.getRisk())) {

			PaymentInstrumentRiskFactor paymentInstructionRiskFactorReference = new PaymentInstrumentRiskFactor();

			if (!NullCheckUtils.isNullOrEmpty(internationalPaymentsRequest.getRisk().getPaymentContextCode())) {
				paymentInstructionRiskFactorReference.setPaymentContextCode(
						String.valueOf(internationalPaymentsRequest.getRisk().getPaymentContextCode()));
			}

			if (!NullCheckUtils.isNullOrEmpty(internationalPaymentsRequest.getRisk().getMerchantCategoryCode())) {
				paymentInstructionRiskFactorReference
						.setMerchantCategoryCode(internationalPaymentsRequest.getRisk().getMerchantCategoryCode());
			}

			if (!NullCheckUtils.isNullOrEmpty(internationalPaymentsRequest.getRisk().getMerchantCustomerIdentification())) {
				paymentInstructionRiskFactorReference.setMerchantCustomerIdentification(
						internationalPaymentsRequest.getRisk().getMerchantCustomerIdentification());
			}

			// Delivery Address

			if (!NullCheckUtils.isNullOrEmpty(internationalPaymentsRequest.getRisk().getDeliveryAddress())) {
				Address counterPartyAddress = new Address();
				if (!NullCheckUtils
						.isNullOrEmpty(internationalPaymentsRequest.getRisk().getDeliveryAddress().getAddressLine())
						&& internationalPaymentsRequest.getRisk().getDeliveryAddress().getAddressLine().size() > 0) {
					counterPartyAddress.setFirstAddressLine(
							internationalPaymentsRequest.getRisk().getDeliveryAddress().getAddressLine().get(0));

					if (!NullCheckUtils
							.isNullOrEmpty(internationalPaymentsRequest.getRisk().getDeliveryAddress().getAddressLine())
							&& internationalPaymentsRequest.getRisk().getDeliveryAddress().getAddressLine().size() > 1) {
						counterPartyAddress.setSecondAddressLine(
								internationalPaymentsRequest.getRisk().getDeliveryAddress().getAddressLine().get(1));
					}
				}

				if (!NullCheckUtils
						.isNullOrEmpty(internationalPaymentsRequest.getRisk().getDeliveryAddress().getStreetName())) {
					counterPartyAddress.setGeoCodeBuildingName(
							internationalPaymentsRequest.getRisk().getDeliveryAddress().getStreetName());
				}

				if (!NullCheckUtils
						.isNullOrEmpty(internationalPaymentsRequest.getRisk().getDeliveryAddress().getBuildingNumber())) {
					counterPartyAddress.setGeoCodeBuildingNumber(
							internationalPaymentsRequest.getRisk().getDeliveryAddress().getBuildingNumber());
				}

				if (!NullCheckUtils
						.isNullOrEmpty(internationalPaymentsRequest.getRisk().getDeliveryAddress().getPostCode())) {
					counterPartyAddress
							.setPostCodeNumber(internationalPaymentsRequest.getRisk().getDeliveryAddress().getPostCode());
				}

				if (!NullCheckUtils
						.isNullOrEmpty(internationalPaymentsRequest.getRisk().getDeliveryAddress().getTownName())) {
					counterPartyAddress
							.setThirdAddressLine(internationalPaymentsRequest.getRisk().getDeliveryAddress().getTownName());
				}

				if (!NullCheckUtils
						.isNullOrEmpty(internationalPaymentsRequest.getRisk().getDeliveryAddress().getCountrySubDivision())) {
					counterPartyAddress.setFourthAddressLine(
							internationalPaymentsRequest.getRisk().getDeliveryAddress().getCountrySubDivision());

				}

				if (!NullCheckUtils.isNullOrEmpty(internationalPaymentsRequest.getRisk().getDeliveryAddress().getCountry())) {
					Country addressCountryRisk = new Country();
					addressCountryRisk.setIsoCountryAlphaTwoCode(
							internationalPaymentsRequest.getRisk().getDeliveryAddress().getCountry());
					counterPartyAddress.setAddressCountry(addressCountryRisk);
				}

				paymentInstructionRiskFactorReference.setCounterPartyAddress(counterPartyAddress);
			}

			paymentInstructionProposalInternational
					.setPaymentInstructionRiskFactorReference(paymentInstructionRiskFactorReference);
		}

		return paymentInstructionProposalInternational;
	}

	public PaymentInternationalSubmitPOST201Response transformInternationalConsentResponseFromFDToAPIForInsert(
			PaymentInstructionProposalInternationalComposite paymentInstructionProposal) {

		PaymentInternationalSubmitPOST201Response response = new PaymentInternationalSubmitPOST201Response();
		OBWriteDataInternationalResponse1 obWriteDataInternationalResponse1 = new OBWriteDataInternationalResponse1();
		OBInternational1 obInternational1 = new OBInternational1();

		obInternational1=populateAPIToFDResponse(paymentInstructionProposal, obWriteDataInternationalResponse1,
				obInternational1);
		
		if ((!NullCheckUtils.isNullOrEmpty(
				paymentInstructionProposal.getPaymentInstructionProposalInternational().getAuthorisingPartyReference()))
				|| (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposal.getPaymentInstructionProposalInternational()
						.getAuthorisingPartyUnstructuredReference()))) {
			OBRemittanceInformation1 oBRemittanceInformation1 = new OBRemittanceInformation1();
			
			if (!NullCheckUtils.isNullOrEmpty(
					paymentInstructionProposal.getPaymentInstructionProposalInternational().getAuthorisingPartyReference())) {
				oBRemittanceInformation1.setReference(
						paymentInstructionProposal.getPaymentInstructionProposalInternational().getAuthorisingPartyReference());
			}

			if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposal.getPaymentInstructionProposalInternational()
					.getAuthorisingPartyUnstructuredReference())) {
				oBRemittanceInformation1.setUnstructured(paymentInstructionProposal.getPaymentInstructionProposalInternational()
						.getAuthorisingPartyUnstructuredReference());
			}
			obInternational1.setRemittanceInformation(oBRemittanceInformation1);
		}
		List<OBCharge1> charges = new ArrayList<>();
		OBCharge1 oBCharge1 = new OBCharge1();
		if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposal.getPaymentInstructionProposalInternational().getCharges())) {

			if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposal.getPaymentInstructionProposalInternational().getCharges().get(0))) {
				if (!NullCheckUtils
						.isNullOrEmpty(paymentInstructionProposal.getPaymentInstructionProposalInternational().getCharges().get(0).getChargeBearer())) {
					String chargeBearer = paymentInstructionProposal.getPaymentInstructionProposalInternational().getCharges().get(0).getChargeBearer()
							.toString();
					OBChargeBearerType1Code oBChargeBearerType1Code = OBChargeBearerType1Code.fromValue(chargeBearer);
					oBCharge1.setChargeBearer(oBChargeBearerType1Code);
				}
			}

			if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposal.getPaymentInstructionProposalInternational().getCharges().get(0).getType())) {
				oBCharge1.setType(paymentInstructionProposal.getPaymentInstructionProposalInternational().getCharges().get(0).getType());
			}

			if ((!NullCheckUtils.isNullOrEmpty(paymentInstructionProposal.getPaymentInstructionProposalInternational().getCharges()))) {

				if ((!NullCheckUtils
						.isNullOrEmpty(paymentInstructionProposal.getPaymentInstructionProposalInternational().getCharges().get(0).getAmount()))
						&& (!NullCheckUtils.isNullOrEmpty(
								paymentInstructionProposal.getPaymentInstructionProposalInternational().getCharges().get(0).getCurrency()))) {
					OBCharge1Amount chargeAmount = new OBCharge1Amount();
					Double chargeAmountMule = paymentInstructionProposal.getPaymentInstructionProposalInternational().getCharges().get(0).getAmount()
							.getTransactionCurrency();
					chargeAmount.setAmount(String.valueOf(chargeAmountMule));
					chargeAmount.setCurrency(paymentInstructionProposal.getPaymentInstructionProposalInternational().getCharges().get(0).getCurrency()
							.getIsoAlphaCode());
					oBCharge1.setAmount(chargeAmount);
				}

				charges.add(oBCharge1);
			//	obWriteDataInternationalResponse1.setCharges(charges);
			}
		}

		
		obWriteDataInternationalResponse1.setInitiation(obInternational1);
		response.setData(obWriteDataInternationalResponse1);
		
		PaymentInstructionStatusCode status = PaymentInstructionStatusCode.fromValue(
				paymentInstructionProposal.getPaymentInstruction().getPaymentInstructionStatusCode().toString());
		if (((status.toString()).equalsIgnoreCase("AcceptedSettlementInProcess"))
				&& (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposal.getPaymentInstruction().getPaymentInstructionNumber()))) {
			ProcessExecutionStatusEnum processExecutionStatusEnum = ProcessExecutionStatusEnum.PASS;
			response.setProcessExecutionStatus(processExecutionStatusEnum);
		} else if (((status.toString()).equalsIgnoreCase("Rejected"))
				&& (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposal.getPaymentInstruction().getPaymentInstructionNumber()))) {
			ProcessExecutionStatusEnum processExecutionStatusEnum = ProcessExecutionStatusEnum.FAIL;
			response.setProcessExecutionStatus(processExecutionStatusEnum);
		}
		System.out.println(response);
		return response;
	}
	
	public OBInternational1 populateAPIToFDResponse(
			PaymentInstructionProposalInternationalComposite paymentInstructionProposal,
			OBWriteDataInternationalResponse1 obWriteDataInternationalResponse1, OBInternational1 obInternational1) {
		
		//FD TO API

		OBInternational1 oBInternational1 = new OBInternational1();

		if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposal.getPaymentInstruction())) {
		if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposal.getPaymentInstruction().getPaymentInstructionNumber())) {
			obWriteDataInternationalResponse1
					.setInternationalPaymentId(paymentInstructionProposal.getPaymentInstruction().getPaymentInstructionNumber());
		}
		if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposal.getPaymentInstruction().getInstructionIssueDate())) {
			obWriteDataInternationalResponse1
					.setCreationDateTime(paymentInstructionProposal.getPaymentInstruction().getInstructionIssueDate());
			//Updated getInstructionIssueDate type to string
		}
		if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposal.getPaymentInstruction().getPaymentInstructionStatusCode())) {
			OBTransactionIndividualStatus1Code status=OBTransactionIndividualStatus1Code.fromValue(paymentInstructionProposal.getPaymentInstruction().getPaymentInstructionStatusCode().toString());
			obWriteDataInternationalResponse1
					.setStatus(status);
		}
		if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposal.getPaymentInstruction().getInstructionStatusUpdateDateTime())) {
			obWriteDataInternationalResponse1
					.setStatusUpdateDateTime(paymentInstructionProposal.getPaymentInstruction().getInstructionStatusUpdateDateTime());
		}
		}
		if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposal.getPaymentInstructionProposalInternational().getPaymentInstructionProposalId())) {
			obWriteDataInternationalResponse1
					.setConsentId(paymentInstructionProposal.getPaymentInstructionProposalInternational().getPaymentInstructionProposalId());
		}
		
		
		
		//Initiation
		
		if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposal.getPaymentInstructionProposalInternational().getInstructionReference())) {
			oBInternational1
					.setInstructionIdentification(paymentInstructionProposal.getPaymentInstructionProposalInternational().getInstructionReference());
		}

		if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposal.getPaymentInstructionProposalInternational().getInstructionEndToEndReference())) {
			oBInternational1.setEndToEndIdentification(
					paymentInstructionProposal.getPaymentInstructionProposalInternational().getInstructionEndToEndReference());
		}

		if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposal.getPaymentInstructionProposalInternational().getInstructionLocalInstrument())) {
			oBInternational1
					.setLocalInstrument(paymentInstructionProposal.getPaymentInstructionProposalInternational().getInstructionLocalInstrument());
		}
		if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposal.getPaymentInstructionProposalInternational().getPriorityCode())) {
			OBPriority2Code oBPriority2Code = OBPriority2Code
					.fromValue(paymentInstructionProposal.getPaymentInstructionProposalInternational().getPriorityCode().toString());
			oBInternational1.setInstructionPriority(oBPriority2Code);
		}
		if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposal.getPaymentInstructionProposalInternational().getPurpose())) {
		if (!NullCheckUtils
				.isNullOrEmpty(paymentInstructionProposal.getPaymentInstructionProposalInternational().getPurpose().getPurposeCode())) {
			oBInternational1.setPurpose(paymentInstructionProposal.getPaymentInstructionProposalInternational().getPurpose().getPurposeCode());
			}
		}
		if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposal.getPaymentInstructionProposalInternational().getCharges())) {
			if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposal.getPaymentInstructionProposalInternational().getCharges().get(0))) {
				if (!NullCheckUtils
						.isNullOrEmpty(paymentInstructionProposal.getPaymentInstructionProposalInternational().getCharges().get(0).getChargeBearer())) {
					OBChargeBearerType1Code oBChargeBearerType1Code = OBChargeBearerType1Code.fromValue(
							paymentInstructionProposal.getPaymentInstructionProposalInternational().getCharges().get(0).getChargeBearer().toString());
					oBInternational1.setChargeBearer(oBChargeBearerType1Code);
				}
			}
		}
		if (!NullCheckUtils
				.isNullOrEmpty(paymentInstructionProposal.getPaymentInstructionProposalInternational().getCurrencyOfTransfer().getIsoAlphaCode())) {

			oBInternational1.setCurrencyOfTransfer(
					paymentInstructionProposal.getPaymentInstructionProposalInternational().getCurrencyOfTransfer().getIsoAlphaCode());
		}

		//Instructed Amount
		
		if (!NullCheckUtils.isNullOrEmpty(
				paymentInstructionProposal.getPaymentInstructionProposalInternational().getFinancialEventAmount().getTransactionCurrency())
				&& !NullCheckUtils.isNullOrEmpty(
						paymentInstructionProposal.getPaymentInstructionProposalInternational().getTransactionCurrency().getIsoAlphaCode())) {
			OBDomestic1InstructedAmount amountInstructed = new OBDomestic1InstructedAmount();

			Double amountMule = paymentInstructionProposal.getPaymentInstructionProposalInternational().getFinancialEventAmount()
					.getTransactionCurrency();
		      amountInstructed.setAmount(BigDecimal.valueOf(amountMule).toPlainString());
			
			amountInstructed
					.setCurrency(paymentInstructionProposal.getPaymentInstructionProposalInternational().getTransactionCurrency().getIsoAlphaCode());
			oBInternational1.setInstructedAmount(amountInstructed);
		}

		// ExchangeRateInformation
		OBExchangeRate1 oBExchangeRate1 = new OBExchangeRate1();
		if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposal.getPaymentInstructionProposalInternational().getExchangeRateQuote())) {

			if (!NullCheckUtils
					.isNullOrEmpty(paymentInstructionProposal.getPaymentInstructionProposalInternational().getExchangeRateQuote().getUnitCurrency())) {
				oBExchangeRate1.setUnitCurrency(paymentInstructionProposal.getPaymentInstructionProposalInternational().getExchangeRateQuote()
						.getUnitCurrency().getIsoAlphaCode().toString());
			}
			if (!NullCheckUtils
					.isNullOrEmpty(paymentInstructionProposal.getPaymentInstructionProposalInternational().getExchangeRateQuote().getExchangeRate())) {
				oBExchangeRate1.setExchangeRate(BigDecimal
						.valueOf(paymentInstructionProposal.getPaymentInstructionProposalInternational().getExchangeRateQuote().getExchangeRate()));
			}
			if (!NullCheckUtils
					.isNullOrEmpty(paymentInstructionProposal.getPaymentInstructionProposalInternational().getExchangeRateQuote().getRateQuoteType())) {
				oBExchangeRate1.setRateType(OBExchangeRateType2Code.fromValue(
						paymentInstructionProposal.getPaymentInstructionProposalInternational().getExchangeRateQuote().getRateQuoteType().toString()));
			}

			oBInternational1.setExchangeRateInformation(oBExchangeRate1);
		}

		// Debtor Account

		if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposal.getPaymentInstructionProposalInternational().getAuthorisingPartyAccount())) {
			OBCashAccountDebtor3 oBCashAccountDebtor3 = new OBCashAccountDebtor3();
			if (!NullCheckUtils.isNullOrEmpty(
					paymentInstructionProposal.getPaymentInstructionProposalInternational().getAuthorisingPartyAccount().getSchemeName())) {
				oBCashAccountDebtor3.setSchemeName(
						paymentInstructionProposal.getPaymentInstructionProposalInternational().getAuthorisingPartyAccount().getSchemeName());
			}

			if (!NullCheckUtils.isNullOrEmpty(
					paymentInstructionProposal.getPaymentInstructionProposalInternational().getAuthorisingPartyAccount().getAccountIdentification())) {
				oBCashAccountDebtor3.setIdentification(paymentInstructionProposal
						.getPaymentInstructionProposalInternational().getAuthorisingPartyAccount().getAccountIdentification());
			}

			if (!NullCheckUtils.isNullOrEmpty(
					paymentInstructionProposal.getPaymentInstructionProposalInternational().getAuthorisingPartyAccount().getAccountName())) {
				oBCashAccountDebtor3
						.setName(paymentInstructionProposal.getPaymentInstructionProposalInternational().getAuthorisingPartyAccount().getAccountName());
			}

			if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposal.getPaymentInstructionProposalInternational().getAuthorisingPartyAccount()
					.getSecondaryIdentification())) {
				oBCashAccountDebtor3.setSecondaryIdentification(paymentInstructionProposal
						.getPaymentInstructionProposalInternational().getAuthorisingPartyAccount().getSecondaryIdentification());
			}

			oBInternational1.setDebtorAccount(oBCashAccountDebtor3);
		}

		// Creditor Account

		if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposal.getPaymentInstructionProposalInternational().getProposingPartyAccount())) {
			OBCashAccountCreditor2 oBCashAccountCreditor2 = new OBCashAccountCreditor2();
			if (!NullCheckUtils.isNullOrEmpty(
					paymentInstructionProposal.getPaymentInstructionProposalInternational().getProposingPartyAccount().getSchemeName())) {
				oBCashAccountCreditor2.setSchemeName(
						paymentInstructionProposal.getPaymentInstructionProposalInternational().getProposingPartyAccount().getSchemeName());
			}

			if (!NullCheckUtils.isNullOrEmpty(
					paymentInstructionProposal.getPaymentInstructionProposalInternational().getProposingPartyAccount().getAccountIdentification())) {
				oBCashAccountCreditor2.setIdentification(
						paymentInstructionProposal.getPaymentInstructionProposalInternational().getProposingPartyAccount().getAccountIdentification());
			}

			if (!NullCheckUtils.isNullOrEmpty(
					paymentInstructionProposal.getPaymentInstructionProposalInternational().getProposingPartyAccount().getAccountName())) {
				oBCashAccountCreditor2
						.setName(paymentInstructionProposal.getPaymentInstructionProposalInternational().getProposingPartyAccount().getAccountName());
			}

			if (!NullCheckUtils.isNullOrEmpty(
					paymentInstructionProposal.getPaymentInstructionProposalInternational().getProposingPartyAccount().getSecondaryIdentification())) {
				oBCashAccountCreditor2.setSecondaryIdentification(paymentInstructionProposal
						.getPaymentInstructionProposalInternational().getProposingPartyAccount().getSecondaryIdentification());
			}

			oBInternational1.setCreditorAccount(oBCashAccountCreditor2);

		}

		// Creditor Logic need to add
		OBPartyIdentification43 oBPartyIdentification43 = new OBPartyIdentification43();
		if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposal.getPaymentInstructionProposalInternational().getProposingParty())) {
		if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposal.getPaymentInstructionProposalInternational().getProposingParty().getPartyName())) {
			oBPartyIdentification43.setName(paymentInstructionProposal.getPaymentInstructionProposalInternational().getProposingParty().getPartyName());
			oBInternational1.setCreditor(oBPartyIdentification43);
			}
		}

		OBPostalAddress6 oBPostalAddress6 = new OBPostalAddress6();

		// Creditor Postal Address

		if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposal.getPaymentInstructionProposalInternational().getProposingPartyPostalAddress())) {

			if (!NullCheckUtils.isNullOrEmpty(
					paymentInstructionProposal.getPaymentInstructionProposalInternational().getProposingPartyPostalAddress().getAddressType())) {
				OBAddressTypeCode addressType = OBAddressTypeCode.fromValue(
						paymentInstructionProposal.getPaymentInstructionProposalInternational().getProposingPartyPostalAddress().getAddressType());
				oBPostalAddress6.setAddressType(addressType);
			}

			if (!NullCheckUtils.isNullOrEmpty(
					paymentInstructionProposal.getPaymentInstructionProposalInternational().getProposingPartyPostalAddress().getDepartment())) {
				oBPostalAddress6.setDepartment(
						paymentInstructionProposal.getPaymentInstructionProposalInternational().getProposingPartyPostalAddress().getDepartment());
			}

			if (!NullCheckUtils.isNullOrEmpty(
					paymentInstructionProposal.getPaymentInstructionProposalInternational().getProposingPartyPostalAddress().getSubDepartment())) {
				oBPostalAddress6.setSubDepartment(
						paymentInstructionProposal.getPaymentInstructionProposalInternational().getProposingPartyPostalAddress().getSubDepartment());
			}

			if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposal.getPaymentInstructionProposalInternational().getProposingPartyPostalAddress()
					.getGeoCodeBuildingName())) {
				oBPostalAddress6.setStreetName(paymentInstructionProposal.getPaymentInstructionProposalInternational().getProposingPartyPostalAddress()
						.getGeoCodeBuildingName());
			}

			if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposal.getPaymentInstructionProposalInternational().getProposingPartyPostalAddress()
					.getGeoCodeBuildingNumber())) {
				oBPostalAddress6.setBuildingNumber(paymentInstructionProposal
						.getPaymentInstructionProposalInternational().getProposingPartyPostalAddress().getGeoCodeBuildingNumber());
			}

			if (!NullCheckUtils.isNullOrEmpty(
					paymentInstructionProposal.getPaymentInstructionProposalInternational().getProposingPartyPostalAddress().getPostCodeNumber())) {
				oBPostalAddress6.setPostCode(
						paymentInstructionProposal.getPaymentInstructionProposalInternational().getProposingPartyPostalAddress().getPostCodeNumber());
			}

			if (!NullCheckUtils.isNullOrEmpty(
					paymentInstructionProposal.getPaymentInstructionProposalInternational().getProposingPartyPostalAddress().getTownName())) {
				oBPostalAddress6.setTownName(
						paymentInstructionProposal.getPaymentInstructionProposalInternational().getProposingPartyPostalAddress().getTownName());
			}

			if (!NullCheckUtils.isNullOrEmpty(
					paymentInstructionProposal.getPaymentInstructionProposalInternational().getProposingPartyPostalAddress().getCountrySubDivision())) {
				oBPostalAddress6.setCountrySubDivision(paymentInstructionProposal
						.getPaymentInstructionProposalInternational().getProposingPartyPostalAddress().getCountrySubDivision());
			}

			if (!NullCheckUtils.isNullOrEmpty(
					paymentInstructionProposal.getPaymentInstructionProposalInternational().getProposingPartyPostalAddress().getAddressCountry())
					&& !NullCheckUtils.isNullOrEmpty(paymentInstructionProposal
							.getPaymentInstructionProposalInternational().getProposingPartyPostalAddress().getAddressCountry().getIsoCountryAlphaTwoCode())) {

				oBPostalAddress6.setCountry(paymentInstructionProposal.getPaymentInstructionProposalInternational().getProposingPartyPostalAddress()
						.getAddressCountry().getIsoCountryAlphaTwoCode());

			}

			List<String> addLine = paymentInstructionProposal.getPaymentInstructionProposalInternational().getProposingPartyPostalAddress()
					.getAddressLine();
			if (!NullCheckUtils.isNullOrEmpty(addLine) && addLine.size() > 0) {
				oBPostalAddress6.setAddressLine(
						paymentInstructionProposal.getPaymentInstructionProposalInternational().getProposingPartyPostalAddress().getAddressLine());
			}
			oBPartyIdentification43.setPostalAddress(oBPostalAddress6);
			oBInternational1.setCreditor(oBPartyIdentification43);

		}

		// CreditorAgent
		if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposal.getPaymentInstructionProposalInternational().getProposingPartyAgent())) {
			OBBranchAndFinancialInstitutionIdentification3 oBBranchAndFinancialInstitutionIdentification3 = new OBBranchAndFinancialInstitutionIdentification3();
		
			if (!NullCheckUtils
					.isNullOrEmpty(paymentInstructionProposal.getPaymentInstructionProposalInternational().getProposingPartyAgent().getSchemeName())) {
				oBBranchAndFinancialInstitutionIdentification3.setSchemeName(
						paymentInstructionProposal.getPaymentInstructionProposalInternational().getProposingPartyAgent().getSchemeName());
			}
			if (!NullCheckUtils.isNullOrEmpty(
					paymentInstructionProposal.getPaymentInstructionProposalInternational().getProposingPartyAgent().getAgentIdentification())) {
				oBBranchAndFinancialInstitutionIdentification3.setIdentification(
						paymentInstructionProposal.getPaymentInstructionProposalInternational().getProposingPartyAgent().getAgentIdentification());
			}
			if (!NullCheckUtils
					.isNullOrEmpty(paymentInstructionProposal.getPaymentInstructionProposalInternational().getProposingPartyAgent().getAgentName())) {
				oBBranchAndFinancialInstitutionIdentification3
						.setName(paymentInstructionProposal.getPaymentInstructionProposalInternational().getProposingPartyAgent().getAgentName());
			}

			// PostalAddress of Creditor agent

			if (!NullCheckUtils.isNullOrEmpty(
					paymentInstructionProposal.getPaymentInstructionProposalInternational().getProposingPartyAgent().getPartyAddress())) {

				if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposal.getPaymentInstructionProposalInternational().getProposingPartyAgent()
						.getPartyAddress().getAddressType())) {
					OBAddressTypeCode oBAddressTypeCode = OBAddressTypeCode
							.fromValue(paymentInstructionProposal.getPaymentInstructionProposalInternational().getProposingPartyAgent()
									.getPartyAddress().getAddressType().toString());
					oBPostalAddress6.setAddressType(oBAddressTypeCode);
				}

				if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposal.getPaymentInstructionProposalInternational().getProposingPartyAgent()
						.getPartyAddress().getDepartment())) {
					oBPostalAddress6.setDepartment(paymentInstructionProposal.getPaymentInstructionProposalInternational().getProposingPartyAgent()
							.getPartyAddress().getDepartment().toString());
				}

				if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposal.getPaymentInstructionProposalInternational().getProposingPartyAgent()
						.getPartyAddress().getSubDepartment())) {
					oBPostalAddress6.setSubDepartment(paymentInstructionProposal.getPaymentInstructionProposalInternational().getProposingPartyAgent()
							.getPartyAddress().getSubDepartment().toString());
				}

				if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposal.getPaymentInstructionProposalInternational().getProposingPartyAgent()
						.getPartyAddress().getGeoCodeBuildingName())) {
					oBPostalAddress6.setStreetName(paymentInstructionProposal.getPaymentInstructionProposalInternational().getProposingPartyAgent()
							.getPartyAddress().getGeoCodeBuildingName().toString());
				}

				if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposal.getPaymentInstructionProposalInternational().getProposingPartyAgent()
						.getPartyAddress().getGeoCodeBuildingNumber())) {
					oBPostalAddress6.setBuildingNumber(paymentInstructionProposal.getPaymentInstructionProposalInternational().getProposingPartyAgent()
							.getPartyAddress().getGeoCodeBuildingNumber().toString());
				}

				if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposal.getPaymentInstructionProposalInternational().getProposingPartyAgent()
						.getPartyAddress().getTownName())) {
					oBPostalAddress6.setTownName(paymentInstructionProposal.getPaymentInstructionProposalInternational().getProposingPartyAgent()
							.getPartyAddress().getTownName().toString());
				}

				if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposal.getPaymentInstructionProposalInternational().getProposingPartyAgent()
						.getPartyAddress().getCountrySubDivision())) {
					oBPostalAddress6.setCountrySubDivision(paymentInstructionProposal
							.getPaymentInstructionProposalInternational().getProposingPartyAgent().getPartyAddress().getCountrySubDivision().toString());
				}

				if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposal.getPaymentInstructionProposalInternational().getProposingPartyAgent()
						.getPartyAddress().getAddressCountry())
						&& !NullCheckUtils
								.isNullOrEmpty(paymentInstructionProposal.getPaymentInstructionProposalInternational().getProposingPartyAgent()
										.getPartyAddress().getAddressCountry().getIsoCountryAlphaTwoCode())) {
					oBPostalAddress6.setCountry(paymentInstructionProposal.getPaymentInstructionProposalInternational().getProposingPartyAgent()
							.getPartyAddress().getAddressCountry().getIsoCountryAlphaTwoCode().toString());
				}

				if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposal.getPaymentInstructionProposalInternational().getProposingPartyAgent()
						.getPartyAddress().getAddressLine())) {
					List<String> addLine = paymentInstructionProposal.getPaymentInstructionProposalInternational().getProposingPartyPostalAddress()
							.getAddressLine();
					if (!NullCheckUtils.isNullOrEmpty(addLine) && addLine.size() > 0) {
						oBPostalAddress6.setAddressLine(addLine);
					}

				}
			}

			oBBranchAndFinancialInstitutionIdentification3.setPostalAddress(oBPostalAddress6);
			oBInternational1.setCreditorAgent(oBBranchAndFinancialInstitutionIdentification3);
		}

		// RemittanceInformation

		if ((!NullCheckUtils.isNullOrEmpty(paymentInstructionProposal.getPaymentInstructionProposalInternational().getAuthorisingPartyReference()))
				|| (!NullCheckUtils.isNullOrEmpty(
						paymentInstructionProposal.getPaymentInstructionProposalInternational().getAuthorisingPartyUnstructuredReference()))) {
			OBRemittanceInformation1 oBRemittanceInformation1 = new OBRemittanceInformation1();
			boolean isInit = false;
			if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposal.getPaymentInstructionProposalInternational().getAuthorisingPartyReference())) {
				oBRemittanceInformation1
						.setReference(paymentInstructionProposal.getPaymentInstructionProposalInternational().getAuthorisingPartyReference());
				isInit = true;
			}

			if (!NullCheckUtils.isNullOrEmpty(
					paymentInstructionProposal.getPaymentInstructionProposalInternational().getAuthorisingPartyUnstructuredReference())) {
				oBRemittanceInformation1.setUnstructured(
						paymentInstructionProposal.getPaymentInstructionProposalInternational().getAuthorisingPartyUnstructuredReference());
				isInit = true;
			}

			if (isInit)
				oBInternational1.setRemittanceInformation(oBRemittanceInformation1);
		}

		
		return oBInternational1;
	}
}
