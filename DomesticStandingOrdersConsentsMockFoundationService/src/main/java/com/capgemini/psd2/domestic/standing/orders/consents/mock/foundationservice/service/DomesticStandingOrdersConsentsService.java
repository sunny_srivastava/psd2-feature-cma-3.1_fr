package com.capgemini.psd2.domestic.standing.orders.consents.mock.foundationservice.service;

import com.capgemini.psd2.domestic.standing.orders.consents.mock.foundationservice.exception.handler.RecordNotFoundException;
import com.capgemini.psd2.domestic.standing.orders.consents.mock.foundationservice.raml.domain.PaymentInstructionProposalAuthorisingParty;
import com.capgemini.psd2.domestic.standing.orders.consents.mock.foundationservice.raml.domain.StandingOrderInstructionProposal;

public interface DomesticStandingOrdersConsentsService {
 
	public StandingOrderInstructionProposal retrieveAccountInformation(String paymentInstructionProposalId) throws Exception;

	public StandingOrderInstructionProposal createDomesticStandingOrdersConsentsResource(StandingOrderInstructionProposal standingOrderInstructionProposalReq) throws Exception;

	public StandingOrderInstructionProposal validateDomesticScheduledPaymentConsentsResource(
			StandingOrderInstructionProposal standingOrderInstructionProposalReq);

	public StandingOrderInstructionProposal updateAccountInformation(String paymentInstructionProposalId,
			StandingOrderInstructionProposal standingOrderInstructionProposalReq);
	public StandingOrderInstructionProposal rejectstatusofDSO(String paymentInstructionProposalId) throws Exception;
	public StandingOrderInstructionProposal authorisingpartyforDSO(String paymentInstructionProposalId,PaymentInstructionProposalAuthorisingParty paymentInstructionProposalAuthorisingParty) throws Exception;
	
	public StandingOrderInstructionProposal  retrieveDomesticPaymentAuthoriseResource(PaymentInstructionProposalAuthorisingParty paymentInstructionProposalAuthorisingParty,
			String paymentInstructionProposalId) throws RecordNotFoundException;
}
