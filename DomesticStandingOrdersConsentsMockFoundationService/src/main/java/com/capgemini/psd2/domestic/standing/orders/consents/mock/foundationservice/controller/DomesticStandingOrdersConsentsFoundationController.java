package com.capgemini.psd2.domestic.standing.orders.consents.mock.foundationservice.controller;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.psd2.domestic.standing.orders.consents.mock.foundationservice.exception.handler.RecordNotFoundException;
import com.capgemini.psd2.domestic.standing.orders.consents.mock.foundationservice.raml.domain.PaymentInstructionProposalAuthorisingParty;
import com.capgemini.psd2.domestic.standing.orders.consents.mock.foundationservice.raml.domain.StandingOrderInstructionProposal;
import com.capgemini.psd2.domestic.standing.orders.consents.mock.foundationservice.service.DomesticStandingOrdersConsentsService;
import com.capgemini.psd2.foundationservice.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.foundationservice.exceptions.MockFoundationServiceException;
import com.capgemini.psd2.foundationservice.exceptions.MuleOutofBoxPolicyException;
import com.capgemini.psd2.foundationservice.validator.ValidationUtility;
import com.capgemini.psd2.foundationservice.utilities.MockServiceUtility;

@RestController
@RequestMapping("/group-payments/p/payments-service")
public class DomesticStandingOrdersConsentsFoundationController {
	
	@Autowired
	private DomesticStandingOrdersConsentsService domesticStandingOrdersConsentsService;
	
	@Autowired
	private ValidationUtility validationUtility;
	
	@Autowired
	private MockServiceUtility mockServiceUtility;
	
	private String field = null;

	@RequestMapping(value = "/v{version}/domestic/standing-orders/payment-instruction-proposals/{payment-instruction-proposal-id}", method = RequestMethod.GET, produces = {
MediaType.APPLICATION_JSON_UTF8_VALUE })
	@ResponseBody
	public StandingOrderInstructionProposal domesticPaymentConsentGet(
			@PathVariable("payment-instruction-proposal-id") String paymentInstructionProposalId,
			@PathVariable("version") String version,
			@RequestHeader(required = false, value = "x-api-channel-code") String channelcode,
			@RequestHeader(required = false, value = "x-api-transaction-id") String transactionId,
			@RequestHeader(required = false, value = "x-api-source-system") String sourceSystemReqHeader,
			@RequestHeader(required = false, value = "X-BOI-USER") String sourceUserReqHeader,
			@RequestHeader(required = false, value = "x-api-party-source-id-number") String partysourceReqHeader,
			@RequestHeader(required = false, value = "x-system-api-version") String apiVesrion,
			@RequestHeader(required = false, value = "X-API-CHANNEL-BRAND") String channelBrand,
			@RequestHeader(required = false, value = "x-api-correlation-id") String correlationId) throws Exception {

		System.out.println("In DSO get method" + paymentInstructionProposalId);

		if (StringUtils.isBlank(channelBrand) || StringUtils.isBlank(version)|| StringUtils.isBlank(sourceSystemReqHeader)
				 || StringUtils.isBlank(paymentInstructionProposalId)) {
			throw MockFoundationServiceException
					.populateMockFoundationServiceException(ErrorCodeEnum.BAD_REQUEST_PPD_PIP);
		}
		
		
		validationUtility.validateErrorCode(transactionId);
	
		return domesticStandingOrdersConsentsService.retrieveAccountInformation(paymentInstructionProposalId);

	}
	
	@RequestMapping(value = "/v{version}/domestic/standing-orders/payment-instruction-proposals", method = RequestMethod.POST, produces = {MediaType.APPLICATION_JSON_UTF8_VALUE })
	@ResponseBody
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<StandingOrderInstructionProposal> domesticPaymentConsentPost(
			@RequestBody StandingOrderInstructionProposal standingOrderInstructionProposalReq,
			@PathVariable("version") String version,
			@RequestHeader(required = false, value = "x-api-channel-code") String channelcode,
			@RequestHeader(required = false, value = "x-api-transaction-id") String transactionId,
			@RequestHeader(required = false, value = "x-api-source-system") String sourceSystemReqHeader,
			@RequestHeader(required = false, value = "X-BOI-USER") String sourceUserReqHeader,
			@RequestHeader(required = false, value = "x-api-party-source-id-number") String partysourceReqHeader,
			@RequestHeader(required = false, value = "X-API-CHANNEL-BRAND") String channelBrand,
			@RequestHeader(required = false, value = "x-system-api-version") String apiVesrion,
			@RequestHeader(required = false, value = "x-api-correlation-id") String correlationId)throws Exception {
			
			System.out.println("In DSO post method" + standingOrderInstructionProposalReq );
		
			if (  StringUtils.isBlank(channelBrand) || StringUtils.isBlank(version) || StringUtils.isBlank(sourceSystemReqHeader) ) {
				throw MockFoundationServiceException
						.populateMockFoundationServiceException(ErrorCodeEnum.BAD_REQUEST_PPD_PIP);
			}
			
		if(transactionId!=null) {
				String transId=transactionId.substring(transactionId.length()-4);
				field=mockServiceUtility.getErrorField().get(transId);
			}

		if (standingOrderInstructionProposalReq.getReference().contains("PPA_SOIPP")
				|| standingOrderInstructionProposalReq.getReference().contains("FS")
				|| standingOrderInstructionProposalReq.getReference().equals("401")
				|| standingOrderInstructionProposalReq.getReference().equals("429"))
			validationUtility.validateMockBusinessValidationsForSchedulePayment(standingOrderInstructionProposalReq.getReference(),field);
		StandingOrderInstructionProposal standingOrderInstructionProposalResponse = null;
		try {
			standingOrderInstructionProposalResponse = domesticStandingOrdersConsentsService.createDomesticStandingOrdersConsentsResource(standingOrderInstructionProposalReq);

		} catch (RecordNotFoundException e) {
			e.printStackTrace();
			}
		
		if (null == standingOrderInstructionProposalResponse) {

			throw MockFoundationServiceException.populateMockFoundationServiceException(ErrorCodeEnum.RESOURCE_NOT_FOUND_PPD_PIP);
		}
	
		return new ResponseEntity<>(standingOrderInstructionProposalResponse, HttpStatus.CREATED);

	} 
	@RequestMapping(value = "/v{version}/domestic/standing-orders/payment-instruction-proposals/validate", method = RequestMethod.POST, produces = {MediaType.APPLICATION_JSON_UTF8_VALUE })
	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	public ResponseEntity<StandingOrderInstructionProposal> domesticStandingOrdersConsentValidatePost(
			@RequestBody StandingOrderInstructionProposal standingOrderInstructionProposalReq,
			@RequestHeader(required = false, value = "x-api-channel-code") String channelcode,
			@RequestHeader(required = false, value = "x-api-transaction-id") String transactionId,
			@RequestHeader(required = false, value = "x-api-source-system") String sourceSystemReqHeader,
			@RequestHeader(required = false, value = "X-BOI-USER") String sourceUserReqHeader,
			@RequestHeader(required = false, value = "x-api-party-source-id-number") String partysourceReqHeader,			
			@RequestHeader(required = false, value = "x-api-correlation-id") String correlationId)throws Exception {
		

		
	
	    
		if (standingOrderInstructionProposalReq.getReference().contains("601")
				|| standingOrderInstructionProposalReq.getReference().contains("629")) {
			validationUtility.validateExecutionErrorCodeForMuleOutOfBoxPolicy(
					standingOrderInstructionProposalReq.getReference());
		}
		if (standingOrderInstructionProposalReq.getReference().contains("PPA_SOPIPVP")

                || standingOrderInstructionProposalReq.getReference().contains("FS_PMV")){
		validationUtility.validateExecutionErrorCode(standingOrderInstructionProposalReq.getReference());
		}
		 StandingOrderInstructionProposal standingOrderInstructionProposalResponse = null;

			standingOrderInstructionProposalResponse = domesticStandingOrdersConsentsService.validateDomesticScheduledPaymentConsentsResource(standingOrderInstructionProposalReq);

			
		return new ResponseEntity<>(standingOrderInstructionProposalResponse, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/v{version}/domestic/standing-orders/payment-instruction-proposals/{payment-instruction-proposal-id}", method = RequestMethod.PUT, produces = {MediaType.APPLICATION_JSON_UTF8_VALUE })
	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	public StandingOrderInstructionProposal domesticPaymentConsentPut(
			@PathVariable("payment-instruction-proposal-id") String paymentInstructionProposalId,
			@RequestBody StandingOrderInstructionProposal standingOrderInstructionProposalReq,
			@RequestHeader(required = false, value = "x-api-channel-code") String channelcode,
			@RequestHeader(required = false, value = "x-api-transaction-id") String transactionId,
			@RequestHeader(required = false, value = "x-api-source-system") String sourceSystemReqHeader,
			@RequestHeader(required = false, value = "X-BOI-USER") String sourceUserReqHeader,
			@RequestHeader(required = false, value = "x-api-party-source-id-number") String partysourceReqHeader,
			@RequestHeader(required = false, value = "x-api-correlation-id") String correlationId)throws Exception {


		System.out.println("Before domestic standing order put body is "+standingOrderInstructionProposalReq);
		if (standingOrderInstructionProposalReq.getReference().contains("801")
				|| standingOrderInstructionProposalReq.getReference().contains("829")) {
			validationUtility.validateExecutionErrorCodeForMuleOutOfBoxPolicyForUpdate(
					standingOrderInstructionProposalReq.getReference());
		}
		if (standingOrderInstructionProposalReq.getReference().contains("PPA_SOPIPR")) {
		validationUtility.validateExecutionErrorCode(standingOrderInstructionProposalReq.getReference());
		}
		
		return domesticStandingOrdersConsentsService.updateAccountInformation(paymentInstructionProposalId,standingOrderInstructionProposalReq);

	} 
	
	
	@RequestMapping(value = "/v{version}/domestic/standing-orders/payment-instruction-proposals/{payment-instruction-proposal-id}/reject", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_UTF8_VALUE })
				@ResponseBody
				public StandingOrderInstructionProposal domesticStandingOrderReject(
						@PathVariable("payment-instruction-proposal-id") String paymentInstructionProposalId,
						@PathVariable("version") String version,
						@RequestBody String proposalId,
						@RequestHeader(required = false, value = "x-api-channel-code") String channelcode,
						@RequestHeader(required = false, value = "x-api-transaction-id") String transactionId,
						@RequestHeader(required = false, value = "x-api-source-system") String sourceSystemReqHeader,
						@RequestHeader(required = false, value = "X-API-SOURCE-USER") String sourceUserReqHeader,
						@RequestHeader(required = false, value = "x-api-party-source-id-number") String partysourceReqHeader,
						@RequestHeader(required = false, value = "x-system-api-version") String apiVesrion,
						@RequestHeader(required = false, value = "X-API-CHANNEL-BRAND") String channelBrand,
						@RequestHeader(required = false, value = "x-api-correlation-id") String correlationId) throws Exception {

					System.out.println("In DSO reject method of Mock" +paymentInstructionProposalId );
				
					if (  StringUtils.isBlank(channelBrand) || StringUtils.isBlank(version) || StringUtils.isBlank(sourceSystemReqHeader)
							|| StringUtils.isBlank(paymentInstructionProposalId) ) {
						throw MockFoundationServiceException
								.populateMockFoundationServiceException(ErrorCodeEnum.BAD_REQUEST_PPD_PIP);
					}
					//Reject Error Handling is implemented in User(sourceUserReqHeader) based approach
					System.out.println("Error Simulation for Reject"+sourceUserReqHeader);
					if(sourceUserReqHeader.equalsIgnoreCase("77000421"))
						throw new MuleOutofBoxPolicyException("UNAUTHORIZED");
					else if(sourceUserReqHeader.equalsIgnoreCase("77000429"))
						throw new MuleOutofBoxPolicyException("TOO_MANY_REQUESTS");
						
					if(sourceUserReqHeader.startsWith("7700"))
					validationUtility.validateErrorCodeForConsent(sourceUserReqHeader);
				
					return domesticStandingOrdersConsentsService.rejectstatusofDSO(paymentInstructionProposalId);

				}
	@RequestMapping(value = "/v{version}/domestic/standing-orders/payment-instruction-proposals/{payment-instruction-proposal-id}/authorising-party", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_UTF8_VALUE })
				@ResponseBody
				public StandingOrderInstructionProposal domesticStandingOrderAuthoriseParty(
						@PathVariable("payment-instruction-proposal-id") String paymentInstructionProposalId,
						@PathVariable("version") String version,
						@RequestBody PaymentInstructionProposalAuthorisingParty authorisingPartyAccount,
						@RequestHeader(required = false, value = "x-api-channel-code") String channelcode,
						@RequestHeader(required = false, value = "x-api-transaction-id") String transactionId,
						@RequestHeader(required = false, value = "x-api-source-system") String sourceSystemReqHeader,
						@RequestHeader(required = false, value = "X-BOI-USER") String sourceUserReqHeader,
						@RequestHeader(required = false, value = "x-api-party-source-id-number") String partysourceReqHeader,
						@RequestHeader(required = false, value = "x-system-api-version") String apiVesrion,
						@RequestHeader(required = false, value = "X-API-CHANNEL-BRAND") String channelBrand,
						@RequestHeader(required = false, value = "x-api-source-user") String sourceUser,
						@RequestHeader(required = false, value = "x-api-correlation-id") String correlationId) throws Exception {

					System.out.println("In DSO authorising-party method of Mock" +paymentInstructionProposalId );
				
					if (  StringUtils.isBlank(channelBrand) || StringUtils.isBlank(version) || StringUtils.isBlank(sourceSystemReqHeader)
							|| StringUtils.isBlank(paymentInstructionProposalId) ) {
						throw MockFoundationServiceException
								.populateMockFoundationServiceException(ErrorCodeEnum.BAD_REQUEST_PPD_PIP);
					}					
				
				//Authorising-party Error Handling is implemented in User(sourceUser) based approach
		validationUtility.validateMockBusinessValidationsForAuthorisingParty(sourceUser);
		return domesticStandingOrdersConsentsService.authorisingpartyforDSO(paymentInstructionProposalId,
				authorisingPartyAccount);
				}
	
	@RequestMapping(value = "/v{version}/domestic/standing-orders/payment-instruction-proposals/{payment-instruction-proposal-id}/authorise", method = RequestMethod.POST, produces = {MediaType.APPLICATION_JSON_UTF8_VALUE })	
	@ResponseStatus(HttpStatus.OK)
	public StandingOrderInstructionProposal domesticStandingOrdersConsentAuthorisePost(
			@RequestBody PaymentInstructionProposalAuthorisingParty paymentInstructionProposalAuthorisingParty,
			@PathVariable("payment-instruction-proposal-id") String paymentInstructionProposalId,
			@PathVariable("version") String version,
			@RequestHeader(required = false, value = "x-api-transaction-id") String transactionId,
			@RequestHeader(required = false, value = "x-api-correlation-id") String correlationId,
			@RequestHeader(required = false, value = "x-api-source-user") String sourceUser,
			@RequestHeader(required = false, value = "x-api-source-system") String sourceSystemReqHeader,
			@RequestHeader(required = false, value = "x-api-channel-code") String channelcode,
			@RequestHeader(required = false, value = "X-API-CHANNEL-BRAND") String channelBrand,
			@RequestHeader(required = false, value = "x-system-api-version") String systemApiVersion,
			@RequestHeader(required = false, value = "x-api-party-source-id-number") String partysourceReqHeader			
			)throws Exception {		 
		
		if (  StringUtils.isBlank(paymentInstructionProposalId) || StringUtils.isBlank(version) || StringUtils.isBlank(sourceSystemReqHeader)
			 ||	StringUtils.isBlank(channelBrand)) {
			throw MockFoundationServiceException
					.populateMockFoundationServiceException(ErrorCodeEnum.BAD_REQUEST_PPD_PIP);
		}
		//added condition for authorised error handling using User(sourceUser) based approach
		validationUtility.validateExecutionErrorCode(sourceUser);
		return domesticStandingOrdersConsentsService.retrieveDomesticPaymentAuthoriseResource(
				paymentInstructionProposalAuthorisingParty, paymentInstructionProposalId);
	}
}
