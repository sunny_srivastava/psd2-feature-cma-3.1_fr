package com.capgemini.psd2.domestic.standing.orders.consents.mock.foundationservice.service.impl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeParseException;
import java.util.Date;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.capgemini.psd2.domestic.standing.orders.consents.mock.foundationservice.constants.DomesticStandingOrdersConsentsFoundationConstants;
import com.capgemini.psd2.domestic.standing.orders.consents.mock.foundationservice.exception.handler.RecordNotFoundException;
import com.capgemini.psd2.domestic.standing.orders.consents.mock.foundationservice.raml.domain.PaymentInstructionProposalAuthorisingParty;
import com.capgemini.psd2.domestic.standing.orders.consents.mock.foundationservice.raml.domain.ProposalStatus;
import com.capgemini.psd2.domestic.standing.orders.consents.mock.foundationservice.raml.domain.StandingOrderInstructionProposal;
import com.capgemini.psd2.domestic.standing.orders.consents.mock.foundationservice.repository.DomesticStandingOrdersConsentsFoundationRepository;
import com.capgemini.psd2.domestic.standing.orders.consents.mock.foundationservice.service.DomesticStandingOrdersConsentsService;
import com.capgemini.psd2.foundationservice.datetime.utility.TimeZoneDateTimeAdapter;
import com.capgemini.psd2.foundationservice.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.foundationservice.exceptions.MockFoundationServiceException;


@Service
public class DomesticStandingOrdersConsentsFoundationServiceImpl implements DomesticStandingOrdersConsentsService {

	@Autowired
	private DomesticStandingOrdersConsentsFoundationRepository repository;
	
	@Autowired
	private TimeZoneDateTimeAdapter timeZoneDateTimeAdapter;	
	
	@Override
	public StandingOrderInstructionProposal retrieveAccountInformation(String paymentInstructionProposalId)
			throws Exception {

		StandingOrderInstructionProposal standingOrderInstructionProposal = repository
				.findByPaymentInstructionProposalId(paymentInstructionProposalId);

		if (null == standingOrderInstructionProposal) {
			throw new RecordNotFoundException(DomesticStandingOrdersConsentsFoundationConstants.RECORD_NOT_FOUND);
		}

		return standingOrderInstructionProposal;
	}

	@Override
	public StandingOrderInstructionProposal createDomesticStandingOrdersConsentsResource(
			StandingOrderInstructionProposal standingOrderInstructionProposalReq) throws Exception {
		
		String consentID = null;
		if (null == standingOrderInstructionProposalReq) {

			throw new RecordNotFoundException(DomesticStandingOrdersConsentsFoundationConstants.RECORD_NOT_FOUND);
		}
		standingOrderInstructionProposalReq.setProposalStatus(ProposalStatus.AWAITINGAUTHORISATION);
		if(standingOrderInstructionProposalReq.getReference()!=null) {
		if (!(standingOrderInstructionProposalReq.getReference())
				.equalsIgnoreCase(DomesticStandingOrdersConsentsFoundationConstants.REFERENCE)) {
			standingOrderInstructionProposalReq.setProposalStatus(ProposalStatus.AWAITINGAUTHORISATION);
		} else {
			standingOrderInstructionProposalReq.setProposalStatus(ProposalStatus.REJECTED);
		}
		}
		consentID = UUID.randomUUID().toString();
		standingOrderInstructionProposalReq.setPaymentInstructionProposalId(consentID);
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		
		standingOrderInstructionProposalReq.setProposalCreationDatetime(addOffset(dateFormat.format(new Date())));
		standingOrderInstructionProposalReq.setProposalStatusUpdateDatetime(addOffset(dateFormat.format(new Date())));
		repository.save(standingOrderInstructionProposalReq);
		return standingOrderInstructionProposalReq;
	}

	@Override
	public StandingOrderInstructionProposal validateDomesticScheduledPaymentConsentsResource(
			StandingOrderInstructionProposal standingOrderInstructionProposalReq) {
		repository.save(standingOrderInstructionProposalReq);
		return standingOrderInstructionProposalReq;
	}

	@Override
	public StandingOrderInstructionProposal updateAccountInformation(String paymentInstructionProposalId,
			StandingOrderInstructionProposal standingOrderInstructionProposalReq) {
		repository.save(standingOrderInstructionProposalReq);
		System.out.println("After domestic standing order put body is "+standingOrderInstructionProposalReq);
		return standingOrderInstructionProposalReq;
	}
	
	@Override
	public StandingOrderInstructionProposal rejectstatusofDSO(String paymentInstructionProposalId)
			throws Exception {

		StandingOrderInstructionProposal standingOrderInstructionProposal = repository
				.findByPaymentInstructionProposalId(paymentInstructionProposalId);

		if (null == standingOrderInstructionProposal) {
			throw new RecordNotFoundException(DomesticStandingOrdersConsentsFoundationConstants.RECORD_NOT_FOUND);
		}
		if(standingOrderInstructionProposal.getProposalStatus().toString().equalsIgnoreCase("Authorised"))
		{
			//Will be implementing/Updating this according to latest Error Handling for DSO
			System.out.println("Before Throwing: ");
			throw MockFoundationServiceException
			.populateMockFoundationServiceException(ErrorCodeEnum.BAD_REQUEST_PMPSV);
 
		}
		if(standingOrderInstructionProposal.getProposalStatus().toString().equalsIgnoreCase("AwaitingAuthorisation"))
		{
			standingOrderInstructionProposal.setProposalStatus(ProposalStatus.REJECTED);
		}
		repository.save(standingOrderInstructionProposal);
		return standingOrderInstructionProposal;
	}
	
	@Override
	public StandingOrderInstructionProposal authorisingpartyforDSO(String paymentInstructionProposalId,PaymentInstructionProposalAuthorisingParty paymentInstructionProposalAuthorisingParty)
			throws Exception {

		StandingOrderInstructionProposal standingOrderInstructionProposal = repository
				.findByPaymentInstructionProposalId(paymentInstructionProposalId);

		if (null == standingOrderInstructionProposal) {
			throw new RecordNotFoundException(DomesticStandingOrdersConsentsFoundationConstants.RECORD_NOT_FOUND);
		}
		if(standingOrderInstructionProposal.getProposalStatus().toString().equalsIgnoreCase("AwaitingAuthorisation"))
		{
			
			standingOrderInstructionProposal.setAuthorisingPartyAccount(paymentInstructionProposalAuthorisingParty.getAuthorisingPartyAccount());
			//standingOrderInstructionProposal.setFraudSystemResponse(paymentInstructionProposalAuthorisingParty.getFraudSystemResponse());
			standingOrderInstructionProposal.setChannel(paymentInstructionProposalAuthorisingParty.getChannel());
			
		}
		repository.save(standingOrderInstructionProposal);
		return standingOrderInstructionProposal;
	}
	
	@Override
	public StandingOrderInstructionProposal retrieveDomesticPaymentAuthoriseResource(
			PaymentInstructionProposalAuthorisingParty paymentInstructionProposalAuthorisingParty,
			String paymentInstructionProposalId) throws RecordNotFoundException {

		StandingOrderInstructionProposal standingOrderInstructionProposal = repository
				.findByPaymentInstructionProposalId(paymentInstructionProposalId);
		if (null == standingOrderInstructionProposal)
			throw new RecordNotFoundException(DomesticStandingOrdersConsentsFoundationConstants.RECORD_NOT_FOUND);
		if (standingOrderInstructionProposal.getProposalStatus().toString()
				.equalsIgnoreCase(ProposalStatus.AWAITINGAUTHORISATION.toString())) {
			standingOrderInstructionProposal.setProposalStatus(ProposalStatus.AUTHORISED);

			standingOrderInstructionProposal.setAuthorisingPartyAccount(paymentInstructionProposalAuthorisingParty.getAuthorisingPartyAccount());
			standingOrderInstructionProposal.setFraudSystemResponse(paymentInstructionProposalAuthorisingParty.getFraudSystemResponse());
			standingOrderInstructionProposal.setChannel(paymentInstructionProposalAuthorisingParty.getChannel());

			try {
				DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
				standingOrderInstructionProposal.setProposalStatusUpdateDatetime(addOffset(dateFormat.format(new Date())));
			} catch (Exception e) {

			}
			repository.save(standingOrderInstructionProposal);
		}
		return standingOrderInstructionProposal;
	}	
	private String addOffset(String dateTimeWithoutOffset) throws DateTimeParseException { //NOSONAR
		if ((dateTimeWithoutOffset)!=null) {
			StringBuilder builder = new StringBuilder();
			builder.append(dateTimeWithoutOffset);
			builder.append("+00:00");
			return builder.toString();
		} else {
			return null;
		}
	}

}
