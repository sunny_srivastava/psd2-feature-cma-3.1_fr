package com.capgemini.psd2.service;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.capgemini.psd2.config.OBConfig;
import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.model.OBThirdPartyProviders;
import com.capgemini.psd2.restclient.ScimRestClient;
import com.capgemini.psd2.util.JwtCreate;

@Service
public class TppDetailsService implements ITppDetailsService {

	@Autowired
	ScimRestClient restClient;
	@Autowired
	JwtCreate jwtCreate;
	@Autowired
	OBConfig config;
	@Autowired
	PassPortingCheck check;
	@Override
	public OBThirdPartyProviders checkTppStatus(String tppid, String attributes) {
		config.setResponseattributes(attributes);
		String token = restClient.retrieveOBAccessToken(jwtCreate.createJWTToken());
		return restClient.retrieveTPPStatus(tppid, token);
	}
	
	@Override
	public boolean checkPassporting(OBThirdPartyProviders oBThirdPartyProviders,List<String> roles,String tenantid) {
		return check.passportingCheck(oBThirdPartyProviders,config.getPassporting().get(tenantid),roles);
	}


	@Override
	public boolean passporting(String id, String roles, String tenantid, String flag) {
		OBThirdPartyProviders obThirdPartyProviders=getPassportingList(id,flag);
		List<String> list=Arrays.asList(roles.split(","));
		return check.passportingCheck(obThirdPartyProviders,config.getPassporting().get(tenantid),list);
	}
	
	public OBThirdPartyProviders getPassportingList(String id, String flag) {
		String token = restClient.retrieveOBAccessToken(jwtCreate.createJWTToken());
		String filter;
		if ("ncaid".equals(flag)) {
			String[] arr = id.split("-");
			if (arr.length > 2)
				filter = config.getFilter().replaceAll("\\{regid\\}", arr[2]).replaceAll("\\{authid\\}", arr[1]);
			else
				throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.INVALID_NCA_ID);
			return restClient.getPassportingList(token, id, filter);
		}
		if ("orgid".equals(flag)) {
			return restClient.retrieveTPPStatus(id, token);
		}
		throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.INVALID_FLAG);
	}
}
