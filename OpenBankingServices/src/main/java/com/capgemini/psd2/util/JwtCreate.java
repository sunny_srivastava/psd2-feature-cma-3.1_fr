package com.capgemini.psd2.util;

import java.io.IOException;
import java.io.InputStream;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.Date;
import java.util.UUID;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.config.OBConfig;
import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.JWSHeader;
import com.nimbusds.jose.JWSObject;
import com.nimbusds.jose.JWSSigner;
import com.nimbusds.jose.Payload;
import com.nimbusds.jose.crypto.RSASSASigner;
import com.nimbusds.jwt.JWTClaimsSet;

@Component
public class JwtCreate {
	
	private static final Logger LOG = LoggerFactory.getLogger(JwtCreate.class);
	
	@Autowired
	private OBConfig config;

	private PrivateKey privKey;

	@Value("${server.ssl.key-store}")
	private String keystorePath;

	@Value("${server.ssl.key-store-password}")
	private String keystorePassword;

	@Value("${server.ssl.key-password}")
	private String keyPassword;

	/**
	 * This is used to retrieve the private key from the jks . The corresponding
	 * certificate is added in OpenBanking
	 */
	@PostConstruct
	public void init() {
		try {
			Resource resource = new ClassPathResource(keystorePath);
			InputStream in = resource.getInputStream();
			KeyStore keystore = KeyStore.getInstance(KeyStore.getDefaultType());
			keystore.load(in, keystorePassword.toCharArray());
			privKey = (PrivateKey) keystore.getKey(config.getKeyAlias(), keyPassword.toCharArray());
		} catch (KeyStoreException | NoSuchAlgorithmException | CertificateException | IOException
				| UnrecoverableKeyException e) {
			LOG.error("Exception occurred in init()"+e);
			throw PSD2Exception.populatePSD2Exception(e.getMessage(), ErrorCodeEnum.TECHNICAL_ERROR);
		}
	}

	public String createJWTToken() {
		long nowMillis = System.currentTimeMillis();
		Date now = new Date(nowMillis);
		Date exp = new Date(nowMillis + config.getJwtTokenValidity());
		JWTClaimsSet jwtClaims = new JWTClaimsSet.Builder().expirationTime(exp).issueTime(now)
				.subject(config.getClientId()).issuer(config.getClientId()).jwtID(UUID.randomUUID().toString())
				.claim("scope", config.getScope()).audience(config.getTokenUrl()).build();

		JWSSigner signer = new RSASSASigner(privKey);

		JWSObject jwsObject = new JWSObject(
				new JWSHeader.Builder(JWSAlgorithm.RS256).keyID(config.getObKeyId()).build(),
				new Payload(jwtClaims.toJSONObject()));
		try {
			jwsObject.sign(signer);
		} catch (JOSEException e) {
			LOG.error("Exception occurred in createJWTToken()"+e);
			throw PSD2Exception.populatePSD2Exception(e.getMessage(), ErrorCodeEnum.TECHNICAL_ERROR);
		}
		return jwsObject.serialize();
	}
}
