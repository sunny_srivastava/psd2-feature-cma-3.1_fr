/*package com.capgemini.psd2.util;

import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.config.OBConfig;
import com.nimbusds.jose.jwk.RSAKey;

@RunWith(SpringJUnit4ClassRunner.class)
public class JWTCreateTest {
	@InjectMocks
	JwtCreate jwtCreate;

	RSAKey key;
	String text;

	@Before
	public void setupMockData() throws NoSuchAlgorithmException {
		MockitoAnnotations.initMocks(this);
		ReflectionTestUtils.setField(jwtCreate, "keystorePath", "keystore.jks");
		ReflectionTestUtils.setField(jwtCreate, "keystorePassword", "P@ssw0rd");
		ReflectionTestUtils.setField(jwtCreate, "keyPassword", "P@ssw0rd");
		OBConfig config=new OBConfig();
		config.setClientId("12121");
		config.setClientAssertionType("dadadda");
		config.setApiUrl("https://tet.com");
		config.setGrantType("auth");
		config.setJwtTokenValidity(1800000000);
		config.setObKeyId("eqweq");
		config.setResponseattributes("1231231");
		config.setTransportkeyAlias("transportcert");
		config.setScope("openid");
		config.setKeyAlias("signingcert");
		ReflectionTestUtils.setField(jwtCreate, "config", config);
		KeyPairGenerator keygen = null;
		keygen = KeyPairGenerator.getInstance("RSA");
		ReflectionTestUtils.setField(jwtCreate, "privKey", keygen.generateKeyPair().getPrivate());
		jwtCreate.init();
	}

	@Test
	public void createJWTTokenTest() {
		Assert.assertNotNull(jwtCreate.createJWTToken());
	}
}
*/