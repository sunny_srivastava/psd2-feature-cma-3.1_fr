package com.capgemini.psd2.service;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.model.Authorisation;
import com.capgemini.psd2.model.OBThirdPartyProviders;
import com.capgemini.psd2.model.OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10;
import com.capgemini.psd2.model.OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations;

@RunWith(SpringJUnit4ClassRunner.class)
public class PassPortingCheckTest {

	
	@Mock
	RequestHeaderAttributes requestHeader;
	
	@InjectMocks
	PassPortingCheck checkPassporting;
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		
	}
	
	
	@Test
	public void testPassPortingSuccess()
	{
		OBThirdPartyProviders oBThirdPartyProviders=new OBThirdPartyProviders();
		OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10 urnopenbankingcompetentauthorityclaims10=new OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10();
		List<OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations> authorisations1=new ArrayList<>();
		OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations e1=PassportingMockData.getAuthorisationsA1();
		OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations e2=PassportingMockData.getAuthorisationsA2();
		OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations e3=PassportingMockData.getAuthorisationsA3();
		OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations e4=PassportingMockData.getAuthorisationsA4();
		OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations e5=PassportingMockData.getAuthorisationsA5();
		
		authorisations1.add(e1);
		authorisations1.add(e2);
		authorisations1.add(e3);
		authorisations1.add(e4);
		authorisations1.add(e5);
		
		urnopenbankingcompetentauthorityclaims10.setAuthorisations(authorisations1);
		
		oBThirdPartyProviders.setUrnopenbankingcompetentauthorityclaims10(urnopenbankingcompetentauthorityclaims10);
		List<String> roles=new ArrayList<>();
		roles.add("AISP");
	//	roles.add("PISP");
		
		
		
		List<Authorisation> passPortinglist=new ArrayList<>();
		Authorisation E1=PassportingMockData.getAuthorisation1();
		Authorisation E2=PassportingMockData.getAuthorisation2();
		Authorisation E3=PassportingMockData.getAuthorisation3();
		passPortinglist.add(E1);
		passPortinglist.add(E2);
		passPortinglist.add(E3);
		
		boolean b=checkPassporting.passportingCheck(oBThirdPartyProviders, passPortinglist, roles);
		assertTrue(b);
	}
	
	@Test
	public void testPassPortingFail()
	{
		OBThirdPartyProviders oBThirdPartyProviders=new OBThirdPartyProviders();
		OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10 urnopenbankingcompetentauthorityclaims10=new OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10();
		List<OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations> authorisations1=new ArrayList<>();
		OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations e1=PassportingMockData.getAuthorisationsA1();
		OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations e2=PassportingMockData.getAuthorisationsA2();
		OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations e3=PassportingMockData.getAuthorisationsA3();
		OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations e4=PassportingMockData.getAuthorisationsA4();
		OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations e5=PassportingMockData.getAuthorisationsA5();
		authorisations1.add(e1);
		authorisations1.add(e2);
		authorisations1.add(e3);
		authorisations1.add(e4);
		authorisations1.add(e5);
		urnopenbankingcompetentauthorityclaims10.setAuthorisations(authorisations1);
		oBThirdPartyProviders.setUrnopenbankingcompetentauthorityclaims10(urnopenbankingcompetentauthorityclaims10);
		List<String> roles=new ArrayList<>();
		roles.add("AISP");
		roles.add("PISP");
		
		List<Authorisation> passPortinglist=new ArrayList<>();
		Authorisation E1=PassportingMockData.getAuthorisation1();
		Authorisation E2=PassportingMockData.getAuthorisation2();
		Authorisation E3=PassportingMockData.getAuthorisation3();
		passPortinglist.add(E1);
		passPortinglist.add(E2);
		passPortinglist.add(E3);
		
		boolean b=checkPassporting.passportingCheck(oBThirdPartyProviders, passPortinglist, roles);
		assertFalse(b);
	}
}
