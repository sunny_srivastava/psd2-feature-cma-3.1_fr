package com.capgemini.psd2.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.config.OBConfig;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.model.Authorisation;
import com.capgemini.psd2.model.OBThirdPartyProviders;
import com.capgemini.psd2.model.OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10;
import com.capgemini.psd2.model.OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations;
import com.capgemini.psd2.model.Organisation;
import com.capgemini.psd2.rest.client.sync.RestClientSync;
import com.capgemini.psd2.restclient.ScimRestClient;
import com.capgemini.psd2.util.JwtCreate;

@RunWith(SpringJUnit4ClassRunner.class)
public class TppDetailsServiceTest {

	@InjectMocks
	TppDetailsService detailsService;
	@Mock
	ScimRestClient restClient;
	@Mock
	JwtCreate jwtCreate;
	/*@Mock
	OBConfig config;*/
	@Mock
	private RestClientSync clientSync;
	@Mock
	PassPortingCheck check;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		OBConfig config=new OBConfig();
		config.setFilter("filterghk");
		ReflectionTestUtils.setField(detailsService, "config", config);
		
	}

	@Test
	public void test() {
		when(jwtCreate.createJWTToken()).thenReturn(
				"eyJhbGciOiJkaXIiLCJlbmMiOiJBMTI4R0NNIiwiY3R5IjoiSldUIiwia2lkIjoiRW5jcnlwdCJ9..TwE9PxY6pn-Yk-i5.oxiRIDj-eF6EaBaohbaNex-3YkWcqHd_r_yf5AgKli4BhnQoPtio7pAmg5k9RMFv25PP7w4IAA700UaSYl9tuZiAs2aNEv8UZ_4JhD7Q224lKsFKcpPqHCVoqUXBn9WL0pV-nxB9w9ICG0SxHHPC0RyCmEvAqqZiVenGAn-zyLuYgevOE9d2Oe-tNPpe3h5yqxvo0TU39yEzuWbh_bEugW0rC1iyc0jpIAELpS01HXxq5agfQ4T7FwaYi12odfnZ7I7zIjNU_pTeex6nZpje5T6sFbfTiN4VYeq9Of8yTAlj4mDAk5UZddsRq1c9UL1ZkW_HqxjCZxMqxPRWXohapYabem9IZKOqnG7XzzJYnSoK2ChXBg-Bz7nXslzBwMFYCIrtz-TMy56mzBlCwOmzKEN-bcCsajPV-dQ-gKdZiqfrBN_Mvakq0LbXB30I2nua8ogvRC3dE80LJyRAfxu5hnlpqExJjQN1IDtOyEZSJHvpSH5KulCZgfs3kcVcCWzLaTQRDTWbUWz12W2XPeXKGidXss-GB-r4n17WlExlgk5G2i2bFGJado2RWM7SvEr4oZJNE8c8HtyoXlZTT8aWYy4FdwqfHSTkxOJe-K6IF1pe5IbrJ__jMLSZs6byepJeGuQ.iZavpoMzOzWEyLtOhmnYpg");
		when(restClient.retrieveOBAccessToken(any())).thenReturn("1244");
		OBThirdPartyProviders ob = new OBThirdPartyProviders();
		Organisation urnopenbankingorganisation10 = new Organisation();
		ob.setId("1234");
		ob.setUrnopenbankingorganisation10(urnopenbankingorganisation10);
		when(restClient.retrieveTPPStatus("1234", "1244")).thenReturn(ob);
		Assert.assertEquals(ob, detailsService.checkTppStatus("1234", "124"));
	}

	@Test
	public void passportTest()
	{
		
		OBThirdPartyProviders oBThirdPartyProviders=new OBThirdPartyProviders();
		OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10 urnopenbankingcompetentauthorityclaims10=new OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10();
		List<OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations> authorisations1=new ArrayList<>();
		OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations e1=PassportingMockData.getAuthorisationsA1();
		OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations e2=PassportingMockData.getAuthorisationsA2();
		OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations e3=PassportingMockData.getAuthorisationsA3();
		OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations e4=PassportingMockData.getAuthorisationsA4();
		OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations e5=PassportingMockData.getAuthorisationsA5();
		
		authorisations1.add(e1);
		authorisations1.add(e2);
		authorisations1.add(e3);
		authorisations1.add(e4);
		authorisations1.add(e5);

		urnopenbankingcompetentauthorityclaims10.setAuthorisations(authorisations1);

		oBThirdPartyProviders.setUrnopenbankingcompetentauthorityclaims10(urnopenbankingcompetentauthorityclaims10);
		List<String> roles = new ArrayList<>();
		roles.add("AISP");
		// roles.add("PISP");

		List<Authorisation> passPortinglist = new ArrayList<>();
		Authorisation E1 = PassportingMockData.getAuthorisation1();
		Authorisation E2 = PassportingMockData.getAuthorisation2();
		Authorisation E3 = PassportingMockData.getAuthorisation3();
		passPortinglist.add(E1);
		passPortinglist.add(E2);
		passPortinglist.add(E3);

		Mockito.when(check.passportingCheck(oBThirdPartyProviders, passPortinglist, roles)).thenReturn(false);

		String tenantid = "tenant1";

		boolean b = detailsService.checkPassporting(oBThirdPartyProviders, roles, tenantid);
		assertFalse(b);
	}

	@Test
	public void passportTestSuccess() {
		OBThirdPartyProviders oBThirdPartyProviders = new OBThirdPartyProviders();
		OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10 urnopenbankingcompetentauthorityclaims10 = new OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10();
		List<OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations> authorisations1 = new ArrayList<>();
		OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations e1 = PassportingMockData
				.getAuthorisationsA1();
		OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations e2 = PassportingMockData
				.getAuthorisationsA2();
		OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations e3 = PassportingMockData
				.getAuthorisationsA3();
		OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations e4 = PassportingMockData
				.getAuthorisationsA4();
		OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations e5 = PassportingMockData
				.getAuthorisationsA5();
		authorisations1.add(e1);
		authorisations1.add(e2);
		authorisations1.add(e3);
		authorisations1.add(e4);
		authorisations1.add(e5);
		urnopenbankingcompetentauthorityclaims10.setAuthorisations(authorisations1);
		oBThirdPartyProviders.setUrnopenbankingcompetentauthorityclaims10(urnopenbankingcompetentauthorityclaims10);
		List<String> roles = new ArrayList<>();
		roles.add("AISP");
		roles.add("PISP");
		List<Authorisation> passPortinglist = new ArrayList<>();
		Authorisation E1 = PassportingMockData.getAuthorisation1();
		Authorisation E2 = PassportingMockData.getAuthorisation2();
		Authorisation E3 = PassportingMockData.getAuthorisation3();
		passPortinglist.add(E1);
		passPortinglist.add(E2);
		passPortinglist.add(E3);
		String key = "1";
		Map<String, List<Authorisation>> value = new HashMap<>();
		value.put(key, passPortinglist);
		// Mockito.when(config.getPassporting()).thenReturn(value);
		//Mockito.when(config.getPassporting().get(1)).thenReturn(passPortinglist);
		Mockito.when(check.passportingCheck(oBThirdPartyProviders, passPortinglist, roles)).thenReturn(true);
		String tenantid = "tenant1";
		boolean b = detailsService.checkPassporting(oBThirdPartyProviders, roles, tenantid);
		assertFalse(b);
	}
	
	@Test
	public void getpassportingCheckSuccess()
	{
	
		when(jwtCreate.createJWTToken()).thenReturn(
				"eyJhbGciOiJkaXIiLCJlbmMiOiJBMTI4R0NNIiwiY3R5IjoiSldUIiwia2lkIjoiRW5jcnlwdCJ9..TwE9PxY6pn-Yk-i5.oxiRIDj-eF6EaBaohbaNex-3YkWcqHd_r_yf5AgKli4BhnQoPtio7pAmg5k9RMFv25PP7w4IAA700UaSYl9tuZiAs2aNEv8UZ_4JhD7Q224lKsFKcpPqHCVoqUXBn9WL0pV-nxB9w9ICG0SxHHPC0RyCmEvAqqZiVenGAn-zyLuYgevOE9d2Oe-tNPpe3h5yqxvo0TU39yEzuWbh_bEugW0rC1iyc0jpIAELpS01HXxq5agfQ4T7FwaYi12odfnZ7I7zIjNU_pTeex6nZpje5T6sFbfTiN4VYeq9Of8yTAlj4mDAk5UZddsRq1c9UL1ZkW_HqxjCZxMqxPRWXohapYabem9IZKOqnG7XzzJYnSoK2ChXBg-Bz7nXslzBwMFYCIrtz-TMy56mzBlCwOmzKEN-bcCsajPV-dQ-gKdZiqfrBN_Mvakq0LbXB30I2nua8ogvRC3dE80LJyRAfxu5hnlpqExJjQN1IDtOyEZSJHvpSH5KulCZgfs3kcVcCWzLaTQRDTWbUWz12W2XPeXKGidXss-GB-r4n17WlExlgk5G2i2bFGJado2RWM7SvEr4oZJNE8c8HtyoXlZTT8aWYy4FdwqfHSTkxOJe-K6IF1pe5IbrJ__jMLSZs6byepJeGuQ.iZavpoMzOzWEyLtOhmnYpg");
		
		OBThirdPartyProviders value1=new OBThirdPartyProviders();
		value1.addSchemasItem("bfsdk");
		//	ReflectionTestUtils.setField(JwtCreate.class, "keystorePassword", "P@ssw0rd");
	//	ReflectionTestUtils.setField(JwtCreate.class, "keyPassword", "P@ssw0rd");
		Mockito.when(restClient.retrieveTPPStatus(anyString(), anyString())).thenReturn(value1);
		OBThirdPartyProviders obj=detailsService.getPassportingList("0015800000jfQ9aAAE", "orgid");
		assertEquals(OBThirdPartyProviders.class,obj.getClass());
	}
	
	@Test(expected=PSD2Exception.class)
	public void getpassportingCheckInvalidFlag()
	{
		OBThirdPartyProviders obj=detailsService.getPassportingList("0015800000jfQ9aAAE", "rgd");
		assertEquals(OBThirdPartyProviders.class,obj.getClass());
	}
	
	@Test(expected=PSD2Exception.class)
	public void getpassportingCheckInvalidNCAid()
	{
		OBThirdPartyProviders obj=detailsService.getPassportingList("PSDUK-FCAGBR-512954", "rgd");
		assertEquals(OBThirdPartyProviders.class,obj.getClass());
	}
	
	@Test(expected=PSD2Exception.class)
	public void getpassportingCheckvalidNCAid()
	{
		OBThirdPartyProviders obj=detailsService.getPassportingList("PSDUK-512954", "ncaid");
		assertEquals(OBThirdPartyProviders.class,obj.getClass());
	}
	
	@Test
	public void passportingCheck()
	{
		OBThirdPartyProviders oBThirdPartyProviders = new OBThirdPartyProviders();
		OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10 urnopenbankingcompetentauthorityclaims10 = new OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10();
		List<OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations> authorisations1 = new ArrayList<>();
		OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations e1 = PassportingMockData
				.getAuthorisationsA1();
		OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations e2 = PassportingMockData
				.getAuthorisationsA2();
		OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations e3 = PassportingMockData
				.getAuthorisationsA3();
		OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations e4 = PassportingMockData
				.getAuthorisationsA4();
		OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations e5 = PassportingMockData
				.getAuthorisationsA5();
		authorisations1.add(e1);
		authorisations1.add(e2);
		authorisations1.add(e3);
		authorisations1.add(e4);
		authorisations1.add(e5);
		urnopenbankingcompetentauthorityclaims10.setAuthorisations(authorisations1);
		oBThirdPartyProviders.setUrnopenbankingcompetentauthorityclaims10(urnopenbankingcompetentauthorityclaims10);
		List<String> roles = new ArrayList<>();
		roles.add("AISP");
		roles.add("PISP");
		List<Authorisation> passPortinglist = new ArrayList<>();
		Authorisation E1 = PassportingMockData.getAuthorisation1();
		Authorisation E2 = PassportingMockData.getAuthorisation2();
		Authorisation E3 = PassportingMockData.getAuthorisation3();
		passPortinglist.add(E1);
		passPortinglist.add(E2);
		passPortinglist.add(E3);
		Mockito.when(check.passportingCheck(oBThirdPartyProviders, passPortinglist, roles)).thenReturn(true);
		boolean b=detailsService.passporting("0015800000jfQ9aAAE", "AISP,PISP", "tenant1", "orgid");
		assertFalse(b);
	}
	
	
	@Test
	public void getpassportingCheckSucess()
	{
		
		when(jwtCreate.createJWTToken()).thenReturn(
				"eyJhbGciOiJkaXIiLCJlbmMiOiJBMTI4R0NNIiwiY3R5IjoiSldUIiwia2lkIjoiRW5jcnlwdCJ9..TwE9PxY6pn-Yk-i5.oxiRIDj-eF6EaBaohbaNex-3YkWcqHd_r_yf5AgKli4BhnQoPtio7pAmg5k9RMFv25PP7w4IAA700UaSYl9tuZiAs2aNEv8UZ_4JhD7Q224lKsFKcpPqHCVoqUXBn9WL0pV-nxB9w9ICG0SxHHPC0RyCmEvAqqZiVenGAn-zyLuYgevOE9d2Oe-tNPpe3h5yqxvo0TU39yEzuWbh_bEugW0rC1iyc0jpIAELpS01HXxq5agfQ4T7FwaYi12odfnZ7I7zIjNU_pTeex6nZpje5T6sFbfTiN4VYeq9Of8yTAlj4mDAk5UZddsRq1c9UL1ZkW_HqxjCZxMqxPRWXohapYabem9IZKOqnG7XzzJYnSoK2ChXBg-Bz7nXslzBwMFYCIrtz-TMy56mzBlCwOmzKEN-bcCsajPV-dQ-gKdZiqfrBN_Mvakq0LbXB30I2nua8ogvRC3dE80LJyRAfxu5hnlpqExJjQN1IDtOyEZSJHvpSH5KulCZgfs3kcVcCWzLaTQRDTWbUWz12W2XPeXKGidXss-GB-r4n17WlExlgk5G2i2bFGJado2RWM7SvEr4oZJNE8c8HtyoXlZTT8aWYy4FdwqfHSTkxOJe-K6IF1pe5IbrJ__jMLSZs6byepJeGuQ.iZavpoMzOzWEyLtOhmnYpg");
		OBThirdPartyProviders value1=new OBThirdPartyProviders();
		value1.setExternalId("nkvj");
		Mockito.when(restClient.getPassportingList(anyString(), anyString(), anyString())).thenReturn(value1);
		
		OBThirdPartyProviders obj=detailsService.getPassportingList("PSDUK-FCAGBR-512956", "ncaid");
		assertEquals(OBThirdPartyProviders.class,obj.getClass());
	}
}