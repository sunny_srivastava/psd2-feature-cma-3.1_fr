package com.capgemini.psd2.pisp.payment.setup.platform.adapter;

import com.capgemini.psd2.pisp.domain.CustomPaymentSetupPlatformDetails;
import com.capgemini.psd2.pisp.domain.PaymentResponseInfo;
import com.capgemini.psd2.pisp.domain.PaymentConsentsPlatformResource;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageUpdateData;

public interface PaymentSetupPlatformAdapter {

	/* Backward compatible retrieve */
	public PaymentConsentsPlatformResource retrievePaymentSetupPlatformResource(String paymentId, String paymentConsentId);
	
	// Common Interfaces for All Payment Types
	public PaymentConsentsPlatformResource createPaymentSetupPlatformResource(CustomPaymentSetupPlatformDetails paymentSetupDetails, String setupCreationDateTime);
	public PaymentConsentsPlatformResource updatePaymentSetupPlatformResource(PaymentConsentsPlatformResource paymentSetupResource);
	public PaymentConsentsPlatformResource retrievePaymentSetupPlatformResource(String paymentConsentId);
	public PaymentConsentsPlatformResource getIdempotentPaymentSetupPlatformResource(String paymentType, long idempotencyDuration);
	public CustomPaymentStageIdentifiers populateStageIdentifiers(String paymentConsentId);
	public void updatePaymentSetupPlatformResource(String intentId, CustomPaymentStageUpdateData updateData);
	public PaymentConsentsPlatformResource getIdempotentFilePaymentSetupPlatformResource(String paymentType, long idempotencyDuration, String fileHash);
	public PaymentConsentsPlatformResource createInvalidPaymentSetupPlatformResource(CustomPaymentSetupPlatformDetails paymentSetupDetails, PaymentResponseInfo stageResponse,String setupCreationDateTime);
	public PaymentConsentsPlatformResource getIdempotentFileUploadSetupPlatformResource(String paymentType,
			long idempotencyDuration, String consentId);
	public PaymentConsentsPlatformResource retrievePaymentSetupPlatformResourceByIdAndType(String paymentConsentId, String type);
	public PaymentConsentsPlatformResource retrievePaymentSetupPlatformResourceByBackwardPaymentId(String consentId);
}
