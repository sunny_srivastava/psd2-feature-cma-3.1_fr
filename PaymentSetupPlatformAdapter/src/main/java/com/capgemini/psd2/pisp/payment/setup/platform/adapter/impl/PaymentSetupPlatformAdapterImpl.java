package com.capgemini.psd2.pisp.payment.setup.platform.adapter.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.InternalServerErrorMessage;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.LoggerUtils;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.domain.CustomPaymentSetupPlatformDetails;
import com.capgemini.psd2.pisp.domain.PaymentConsentsPlatformResource;
import com.capgemini.psd2.pisp.domain.PaymentResponseInfo;
import com.capgemini.psd2.pisp.enums.PaymentTypeEnum;
import com.capgemini.psd2.pisp.payment.setup.platform.adapter.PaymentSetupPlatformAdapter;
import com.capgemini.psd2.pisp.payment.setup.platform.adapter.repository.PaymentSetupPlatformRepository;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageUpdateData;
import com.capgemini.psd2.pisp.status.PaymentConstants;
import com.capgemini.psd2.pisp.utilities.PispUtilities;
import com.capgemini.psd2.product.common.CompatibleVersionList;
import com.capgemini.psd2.utilities.DateUtilites;
import com.capgemini.psd2.utilities.JSONUtilities;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Component
public class PaymentSetupPlatformAdapterImpl implements PaymentSetupPlatformAdapter {

	private static final Logger LOG = LoggerFactory.getLogger(PaymentSetupPlatformAdapterImpl.class);

	private String exceptionLog = "DataAccessResourceFailureException Occurred: ";
			
	@Autowired
	private LoggerUtils loggerUtils;

	@Autowired
	private PaymentSetupPlatformRepository paymentSetupPlatformRepository;

	@Autowired
	private RequestHeaderAttributes reqHeaderAtrributes;

	@Autowired
	private MongoTemplate mongoTemplate;

	@Autowired
	private CompatibleVersionList compatibleVersionList;

	@Override
	public PaymentConsentsPlatformResource retrievePaymentSetupPlatformResource(String paymentConsentId) {
		try {
			return paymentSetupPlatformRepository.findOneByPaymentConsentIdAndSetupCmaVersionIn(paymentConsentId,
					compatibleVersionList.fetchVersionList());

		} catch (DataAccessResourceFailureException exception) {
			LOG.error(exceptionLog +exception);
			throw PSD2Exception.populatePSD2Exception(
					new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR, exception.getMessage()));
		}
	}

	@Override
	public PaymentConsentsPlatformResource getIdempotentPaymentSetupPlatformResource(String paymentType,
			long idempotencyDuration) {
		try {
			Long l = System.currentTimeMillis() - idempotencyDuration;
			return paymentSetupPlatformRepository
					.findOneByPaymentTypeAndTppCIDAndIdempotencyKeyAndIdempotencyRequestAndCreatedAtGreaterThanAndSetupCmaVersionIn(
							paymentType, reqHeaderAtrributes.getTppCID(), reqHeaderAtrributes.getIdempotencyKey(),
							String.valueOf(Boolean.TRUE), DateUtilites.formatMilisecondsToISODateFormat(l),
							compatibleVersionList.fetchVersionList());
		} catch (DataAccessResourceFailureException exception) {
			LOG.error(exceptionLog +exception);
			throw PSD2Exception.populatePSD2Exception(
					new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR, exception.getMessage()));
		}

	}

	@Override
	public PaymentConsentsPlatformResource updatePaymentSetupPlatformResource(
			PaymentConsentsPlatformResource paymentSetupPlatformResource) {
		try {
			paymentSetupPlatformResource.setUpdatedAt(PispUtilities.getCurrentDateInISOFormat());
			return paymentSetupPlatformRepository.save(paymentSetupPlatformResource);
		} catch (DataAccessResourceFailureException exception) {
			LOG.error(exceptionLog +exception);
			throw PSD2Exception.populatePSD2Exception(
					new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR, exception.getMessage()));
		}
	}

	@Override
	public PaymentConsentsPlatformResource createInvalidPaymentSetupPlatformResource(
			CustomPaymentSetupPlatformDetails paymentSetupDetails, PaymentResponseInfo stageResponse,
			String setupCreationDateTime) {

		if (!NullCheckUtils.isNullOrEmpty(stageResponse.getPaymentConsentId())
				&& retrievePaymentSetupPlatformResource(stageResponse.getPaymentConsentId(),
						stageResponse.getPaymentConsentId()) != null)
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
					InternalServerErrorMessage.PISP_DUPLICATE_PAYMENT_ID));

		PaymentConsentsPlatformResource paymentSetupPlatformResource = new PaymentConsentsPlatformResource();

		paymentSetupPlatformResource.setTppDebtorDetails(paymentSetupDetails.getTppDebtorDetails());
		paymentSetupPlatformResource.setTppDebtorNameDetails(paymentSetupDetails.getTppDebtorNameDetails());
		paymentSetupPlatformResource.setEndToEndIdentification(paymentSetupDetails.getEndToEndIdentification());
		paymentSetupPlatformResource.setInstructionIdentification(paymentSetupDetails.getInstructionIdentification());
		paymentSetupPlatformResource.setPaymentType(paymentSetupDetails.getPaymentType().getPaymentType());
		paymentSetupPlatformResource.setSetupCmaVersion(paymentSetupDetails.getSetupCmaVersion());

		paymentSetupPlatformResource.setCreatedAt(setupCreationDateTime);
		paymentSetupPlatformResource.setUpdatedAt(setupCreationDateTime);
		paymentSetupPlatformResource.setStatusUpdateDateTime(setupCreationDateTime);

		/*
		 * cma3 pisp status changed from PENDING and ACCEPTEDTECHNICALVALIDATION to
		 * AWAITINGAUTHORISATION. ValidationStatus can be either AWAITINGAUTHORISATION
		 * or REJECTED
		 */
		paymentSetupPlatformResource.setPaymentConsentId(stageResponse.getPaymentConsentId());
		paymentSetupPlatformResource.setStatus(stageResponse.getConsentValidationStatus().toString());
		paymentSetupPlatformResource.setIdempotencyRequest(stageResponse.getIdempotencyRequest());

		return savePaymentSetupPlatformResource(paymentSetupPlatformResource);
	}

	private PaymentConsentsPlatformResource savePaymentSetupPlatformResource(
			PaymentConsentsPlatformResource paymentSetupPlatformResource) {
		PaymentConsentsPlatformResource setupPlatformResource = null;
		
		paymentSetupPlatformResource.setTppCID(reqHeaderAtrributes.getTppCID());
		paymentSetupPlatformResource.setIdempotencyKey(reqHeaderAtrributes.getIdempotencyKey());
		paymentSetupPlatformResource
				.setTppLegalEntityName(reqHeaderAtrributes.getToken().getTppInformation().getTppLegalEntityName());

		try {
			setupPlatformResource = paymentSetupPlatformRepository.save(paymentSetupPlatformResource);

		} catch (DataAccessResourceFailureException exception) {
			LOG.error(exceptionLog +exception);
			throw PSD2Exception.populatePSD2Exception(
					new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR, exception.getMessage()));
		}
		return setupPlatformResource;
	}

	@Override
	public PaymentConsentsPlatformResource retrievePaymentSetupPlatformResource(String paymentId,
			String paymentConsentId) {
		try {
			// An ASPSP may choose to make ConsentIds accessible across versions
			// E.g., for a PaymentId created in v1, an ASPSP may or may not make
			// it available via v3 - as this is a short-lived consent

			return paymentSetupPlatformRepository.findOneByPaymentIdOrPaymentConsentIdAndSetupCmaVersionIn(paymentId,
					paymentConsentId, compatibleVersionList.fetchVersionList());

		} catch (DataAccessResourceFailureException exception) {
			LOG.error(exceptionLog +exception);
			throw PSD2Exception.populatePSD2Exception(
					new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR, exception.getMessage()));
		}
	}

	/* saas cancel call, consent cancel call, integration update call */
	/* Log here for reporting : Consent platform status change */
	@Override
	public void updatePaymentSetupPlatformResource(String intentId, CustomPaymentStageUpdateData updateData) {
		LOG.info("{\"Enter\":\"{}\",\"{}\"}",
				"com.capgemini.psd2.pisp.payment.setup.platform.adapter.impl.updatePaymentSetupPlatformResource()",
				loggerUtils.populateLoggerData("updatePaymentSetupPlatformResource"));

		Query query = new Query();
		Criteria criteria = new Criteria().orOperator(Criteria.where("paymentConsentId").is(intentId),Criteria.where("paymentId").is(intentId));
		query.addCriteria(criteria);
		
		Update update = new Update();
		update.set("status", updateData.getSetupStatus());
		update.set("statusUpdateDateTime", updateData.getSetupStatusUpdateDateTime());

		mongoTemplate.updateFirst(query, update, PaymentConsentsPlatformResource.class);

		LOG.info("{\"Exit\":\"{}\",\"{}\",\"updatedConsentPlatformResource\":{}}",
				"com.capgemini.psd2.pisp.payment.setup.platform.adapter.impl.updatePaymentSetupPlatformResource()",
				loggerUtils.populateLoggerData("updatePaymentSetupPlatformResource"),
				JSONUtilities.getJSONOutPutFromObject(updateData));
	}

	@Override
	public CustomPaymentStageIdentifiers populateStageIdentifiers(String paymentConsentId) {
		PaymentConsentsPlatformResource setupPlatformResource = retrievePaymentSetupPlatformResource(paymentConsentId,
				paymentConsentId);
		String paymentType = setupPlatformResource.getPaymentType();
		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();
		/* Backward compatible CMA Version 1 Setup */
		if (NullCheckUtils.isNullOrEmpty(paymentType)) {
			stageIdentifiers.setPaymentConsentId(paymentConsentId);
			stageIdentifiers.setPaymentSetupVersion(PaymentConstants.CMA_FIRST_VERSION);
			stageIdentifiers.setPaymentTypeEnum(PaymentTypeEnum.DOMESTIC_PAY);
		} else {
			stageIdentifiers.setPaymentConsentId(paymentConsentId);
			// Setup CMA Version should be same as Consent Cma Version and Submission also Cma Version
			stageIdentifiers.setPaymentSetupVersion(setupPlatformResource.getSetupCmaVersion());

			PaymentTypeEnum paymentTypeEnum = PaymentTypeEnum.locatePaymentTypeEnum(paymentType);

			stageIdentifiers.setPaymentTypeEnum(paymentTypeEnum);
		}
		return stageIdentifiers;
	}

	@Override
	public PaymentConsentsPlatformResource getIdempotentFilePaymentSetupPlatformResource(String paymentType,
			long idempotencyDuration, String fileHash) {
		try {
			Long duration = System.currentTimeMillis() - idempotencyDuration;
			return paymentSetupPlatformRepository
					.findOneByPaymentTypeAndTppCIDAndIdempotencyKeyAndIdempotencyRequestAndFileHashAndCreatedAtGreaterThanAndSetupCmaVersionIn(
							paymentType, reqHeaderAtrributes.getTppCID(), reqHeaderAtrributes.getIdempotencyKey(),
							String.valueOf(Boolean.TRUE), fileHash,
							DateUtilites.formatMilisecondsToISODateFormat(duration),
							compatibleVersionList.fetchVersionList());
		} catch (DataAccessResourceFailureException exception) {
			LOG.error(exceptionLog +exception);
			throw PSD2Exception.populatePSD2Exception(
					new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR, exception.getMessage()));
		}

	}

	@Override
	public PaymentConsentsPlatformResource getIdempotentFileUploadSetupPlatformResource(String paymentType,
			long idempotencyDuration, String consentId) {
		try {
			Long duration = System.currentTimeMillis() - idempotencyDuration;
			return paymentSetupPlatformRepository
					.findOneByPaymentTypeAndTppCIDAndIdempotencyKeyAndIdempotencyRequestAndPaymentConsentIdAndCreatedAtGreaterThanAndSetupCmaVersionIn(
							paymentType, reqHeaderAtrributes.getTppCID(), reqHeaderAtrributes.getIdempotencyKey(),
							String.valueOf(Boolean.TRUE), consentId,
							DateUtilites.formatMilisecondsToISODateFormat(duration),
							compatibleVersionList.fetchVersionList());
		} catch (DataAccessResourceFailureException exception) {
			LOG.error(exceptionLog +exception);
			throw PSD2Exception.populatePSD2Exception(
					new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR, exception.getMessage()));
		}

	}

	@Override
	public PaymentConsentsPlatformResource createPaymentSetupPlatformResource(
			CustomPaymentSetupPlatformDetails paymentSetupDetails, String setupCreationDateTime) {

		PaymentConsentsPlatformResource paymentSetupPlatformResource = new PaymentConsentsPlatformResource();

		paymentSetupPlatformResource.setTppDebtorDetails(paymentSetupDetails.getTppDebtorDetails());
		paymentSetupPlatformResource.setTppDebtorNameDetails(paymentSetupDetails.getTppDebtorNameDetails());
		paymentSetupPlatformResource.setEndToEndIdentification(paymentSetupDetails.getEndToEndIdentification());
		paymentSetupPlatformResource.setInstructionIdentification(paymentSetupDetails.getInstructionIdentification());
		paymentSetupPlatformResource.setPaymentType(paymentSetupDetails.getPaymentType().getPaymentType());
		paymentSetupPlatformResource.setSetupCmaVersion(paymentSetupDetails.getSetupCmaVersion());

		paymentSetupPlatformResource.setCreatedAt(setupCreationDateTime);
		paymentSetupPlatformResource.setUpdatedAt(setupCreationDateTime);

		paymentSetupPlatformResource.setFileHash(paymentSetupDetails.getFileHash());

		/*
		 * cma3 pisp status changed from PENDING and ACCEPTEDTECHNICALVALIDATION to
		 * AWAITINGAUTHORISATION. ValidationStatus can be either AWAITINGAUTHORISATION
		 * or REJECTED
		 */

		paymentSetupPlatformResource.setIdempotencyRequest(Boolean.TRUE.toString());
		paymentSetupPlatformResource.setStatusUpdateDateTime(setupCreationDateTime);

		return savePaymentSetupPlatformResource(paymentSetupPlatformResource);
	}

	@Override
	public PaymentConsentsPlatformResource retrievePaymentSetupPlatformResourceByIdAndType(String consentId,
			String paymentType) {
		try {
			return paymentSetupPlatformRepository.findOneByPaymentConsentIdAndPaymentTypeAndSetupCmaVersionIn(consentId,
					paymentType, compatibleVersionList.fetchVersionList());

		} catch (DataAccessResourceFailureException exception) {
			LOG.error(exceptionLog +exception);
			throw PSD2Exception.populatePSD2Exception(
					new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR, exception.getMessage()));
		}
	}

	@Override
	public PaymentConsentsPlatformResource retrievePaymentSetupPlatformResourceByBackwardPaymentId(String paymentId) {
		try {
			return paymentSetupPlatformRepository.findOneByPaymentIdAndSetupCmaVersionIn(paymentId,
					compatibleVersionList.fetchVersionList());

		} catch (DataAccessResourceFailureException exception) {
			LOG.error(exceptionLog +exception);
			throw PSD2Exception.populatePSD2Exception(
					new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR, exception.getMessage()));
		}
	}
}