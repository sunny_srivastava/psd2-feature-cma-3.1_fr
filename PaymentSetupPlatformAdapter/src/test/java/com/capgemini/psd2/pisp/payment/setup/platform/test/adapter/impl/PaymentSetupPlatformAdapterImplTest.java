package com.capgemini.psd2.pisp.payment.setup.platform.test.adapter.impl;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyList;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.consent.domain.AispConsent;
import com.capgemini.psd2.exceptions.OBPSD2ExceptionUtility;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.LoggerUtils;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.domain.CustomPaymentSetupPlatformDetails;
import com.capgemini.psd2.pisp.domain.OBExternalConsentStatus1Code;
import com.capgemini.psd2.pisp.domain.OBExternalConsentStatus2Code;
import com.capgemini.psd2.pisp.domain.PaymentConsentsPlatformResource;
import com.capgemini.psd2.pisp.domain.PaymentResponseInfo;
import com.capgemini.psd2.pisp.enums.PaymentTypeEnum;
import com.capgemini.psd2.pisp.payment.setup.platform.adapter.impl.PaymentSetupPlatformAdapterImpl;
import com.capgemini.psd2.pisp.payment.setup.platform.adapter.repository.PaymentSetupPlatformRepository;
import com.capgemini.psd2.pisp.payment.setup.platform.test.mock.data.PaymentSetupPlatformResourceMockData;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageUpdateData;
import com.capgemini.psd2.product.common.CompatibleVersionList;
import com.capgemini.psd2.token.Token;
import com.capgemini.psd2.token.TppInformationTokenData;
import com.capgemini.psd2.utilities.DateUtilites;
import com.mongodb.WriteResult;

@RunWith(SpringJUnit4ClassRunner.class)
public class PaymentSetupPlatformAdapterImplTest {

	PSD2Exception obPSD2Exception;


	@InjectMocks
	private PaymentSetupPlatformAdapterImpl adapterImpl;

	@Mock
	private PaymentSetupPlatformRepository paymentSetupPlatformRepository;
	
	@Mock
	private PaymentConsentsPlatformResource paymentConsentsPlatformResource;

	@Mock
	private CompatibleVersionList compatibleVersionList;
	
	@Mock
	private RequestHeaderAttributes reqHeaderAtrributes;
	
	@Mock
	private LoggerUtils loggerUtils;
	
	@Mock
	private MongoTemplate mongoTemplate;
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);

		Map<String, String> genericErrorMessages = new HashMap<>();
		Map<String, String> specificErrorMessages = new HashMap<>();

		genericErrorMessages.put("INTERNAL", "INTERNAL Error");
		specificErrorMessages.put("Idempotency", "Idempotency duration is not configured in correct format.");
		OBPSD2ExceptionUtility.genericErrorMessages.putAll(genericErrorMessages);
		OBPSD2ExceptionUtility.specificErrorMessages.putAll(specificErrorMessages);

	}

	@Test
	public void contextLoads() {
	}
	
	@Test
	public void testRetrievePaymentSetupPlatformResource() {
		PaymentConsentsPlatformResource resource = new PaymentConsentsPlatformResource();
		String paymentConsentId = "23423";
		List<String> version = new ArrayList<String>();
		when(compatibleVersionList.fetchVersionList()).thenReturn(version);
		Mockito.when(paymentSetupPlatformRepository.findOneByPaymentConsentIdAndSetupCmaVersionIn(paymentConsentId, version)).thenReturn(resource);
		adapterImpl.retrievePaymentSetupPlatformResource(paymentConsentId);
	}

	@SuppressWarnings("unchecked")
	@Test(expected = PSD2Exception.class)
	public void testRetrievePaymentSetupPlatformResourceExc() {
		String paymentConsentId = "65723";
		Mockito.when(paymentSetupPlatformRepository.findOneByPaymentConsentIdAndSetupCmaVersionIn(anyString(),any()))
				.thenThrow(DataAccessResourceFailureException.class);
		adapterImpl.retrievePaymentSetupPlatformResource(paymentConsentId);
	}

	@Test
	public void testUpdatePaymentSetupPlatformResource() {
		PaymentConsentsPlatformResource paymentSetupPlatformResource = new PaymentConsentsPlatformResource();
		Mockito.when(paymentSetupPlatformRepository.save(paymentSetupPlatformResource))
				.thenReturn(paymentSetupPlatformResource);
		adapterImpl.updatePaymentSetupPlatformResource(paymentSetupPlatformResource);

	}

	@SuppressWarnings("unchecked")
	@Test(expected = PSD2Exception.class)
	public void testupdatePaymentSetupPlatformResourceExc() {
		PaymentConsentsPlatformResource paymentSetupPlatformResource = new PaymentConsentsPlatformResource();
		Mockito.when(paymentSetupPlatformRepository.save(paymentSetupPlatformResource))
				.thenThrow(DataAccessResourceFailureException.class);
		adapterImpl.updatePaymentSetupPlatformResource(paymentSetupPlatformResource);

	}

	@Test
	public void testCreateInvalidPaymentSetupPlatformResource() {
		PaymentConsentsPlatformResource platformResource = new PaymentConsentsPlatformResource();
		Mockito.when(paymentSetupPlatformRepository.save(platformResource)).thenReturn(platformResource);
		CustomPaymentSetupPlatformDetails paymentSetupDetails = new CustomPaymentSetupPlatformDetails();
		paymentSetupDetails.setPaymentType(PaymentTypeEnum.FILE_PAY);
		PaymentResponseInfo stageResponse = new PaymentResponseInfo();
		stageResponse.setConsentValidationStatus(OBExternalConsentStatus1Code.AUTHORISED);
		stageResponse.setPaymentConsentId("12335");
		assertNotNull(stageResponse.getPaymentConsentId());
		String setupCreationDateTime = "2018-11-28T16:32:03+05:30";

		TppInformationTokenData tppInformation = new TppInformationTokenData();
		Token token = new Token();
		token.setTppInformation(tppInformation);
		token.setClient_id("5334");
		Mockito.when(reqHeaderAtrributes.getToken()).thenReturn(token);

		adapterImpl.createInvalidPaymentSetupPlatformResource(paymentSetupDetails, stageResponse,
				setupCreationDateTime);

	}

	@Test
	public void testRetrievePaymentSetupPlatformResource2() {
		PaymentConsentsPlatformResource paymentSetupPlatformResource = new PaymentConsentsPlatformResource();
		Mockito.when(paymentSetupPlatformRepository.save(paymentSetupPlatformResource))
				.thenReturn(paymentSetupPlatformResource);
		String paymentConsentId = "34543";
		String paymentId = "58324";
		List<String> version = new ArrayList<String>();
		when(compatibleVersionList.fetchVersionList()).thenReturn(version);
		adapterImpl.retrievePaymentSetupPlatformResource(paymentId, paymentConsentId);
	}

	@SuppressWarnings("unchecked")
	@Test(expected = PSD2Exception.class)
	public void testRetrievePaymentSetupPlatformResourceException() {
		String paymentConsentId = "34543";
		String paymentId = "58324";
		List<String> version = new ArrayList<String>();
		when(compatibleVersionList.fetchVersionList()).thenReturn(version);
		Mockito.when(paymentSetupPlatformRepository.findOneByPaymentIdOrPaymentConsentIdAndSetupCmaVersionIn(paymentId, paymentConsentId, version))
				.thenThrow(DataAccessResourceFailureException.class);
		adapterImpl.retrievePaymentSetupPlatformResource(paymentId, paymentConsentId);

	}

	@Test
	public void testgetIdempotentPaymentSetupPlatformResource() {
		String paymentType = "domestic";
		long idempotencyDuration = 24;
		PaymentConsentsPlatformResource platformResource = PaymentSetupPlatformResourceMockData
				.getPaymentSetupPlatformResourceResponse();
		String tppCID = "";
		Long l = System.currentTimeMillis();
		String idempotencyKey = "348ydsnf394y";
		List<String> version = new ArrayList<String>();
		when(compatibleVersionList.fetchVersionList()).thenReturn(version);
		Mockito.when(paymentSetupPlatformRepository
				.findOneByPaymentTypeAndTppCIDAndIdempotencyKeyAndIdempotencyRequestAndCreatedAtGreaterThanAndSetupCmaVersionIn(paymentType,
						tppCID, idempotencyKey, String.valueOf(Boolean.TRUE),
						DateUtilites.formatMilisecondsToISODateFormat(l),version))
				.thenReturn(platformResource);

		adapterImpl.getIdempotentPaymentSetupPlatformResource(paymentType, idempotencyDuration);
	}
	
	@Test(expected=PSD2Exception.class)
	public void testgetIdempotentPaymentSetupPlatformResourceException() {
		String paymentType = "domestic";
		long idempotencyDuration = 24;
		PaymentConsentsPlatformResource platformResource = PaymentSetupPlatformResourceMockData
				.getPaymentSetupPlatformResourceResponse();
		String tppCID = "";
		Long l = System.currentTimeMillis();
		String idempotencyKey = "348ydsnf394y";
		List<String> version = new ArrayList<String>();
		when(compatibleVersionList.fetchVersionList()).thenReturn(version);
		Mockito.when(paymentSetupPlatformRepository
				.findOneByPaymentTypeAndTppCIDAndIdempotencyKeyAndIdempotencyRequestAndCreatedAtGreaterThanAndSetupCmaVersionIn(any(),
						any(), any(), any(), any(),any())).thenThrow(DataAccessResourceFailureException.class);

		adapterImpl.getIdempotentPaymentSetupPlatformResource(paymentType, idempotencyDuration);
	}

	@Test
	public void testUpdatePaymentSetupPlatformResource2() {
		String intentId = "455";
		PaymentConsentsPlatformResource resource = new PaymentConsentsPlatformResource();
		CustomPaymentStageUpdateData updateData = new CustomPaymentStageUpdateData();
		updateData.setSetupStatusUpdated(Boolean.TRUE);
		updateData.setSetupStatus("accept");
		updateData.setSetupStatusUpdateDateTime("2018-11-28T16:32:03+05:30");
		Query query = new Query();
		Update update = new Update();
		boolean updateOfExisting = true;
		Object upsertedId = "";
		int n = 1;
		WriteResult value = new WriteResult(n, updateOfExisting, upsertedId);
		when(mongoTemplate.updateFirst(query, update, AispConsent.class)).thenReturn(value);
		Mockito.when(adapterImpl.retrievePaymentSetupPlatformResource(intentId, intentId)).thenReturn(resource);

		adapterImpl.updatePaymentSetupPlatformResource(intentId, updateData);

	}
	
	@Test
	public void testpopulateStageIdentifiersCMA3Onwards() {
		PaymentConsentsPlatformResource resource = new PaymentConsentsPlatformResource();
		resource.setPaymentType("file");
		String paymentConsentId = "43545";
		String paymentId = "43545";
		String paymentType = resource.getPaymentType();
		List<String> version = new ArrayList<String>();
		when(compatibleVersionList.fetchVersionList()).thenReturn(version);	
		Mockito.when(adapterImpl.retrievePaymentSetupPlatformResource(paymentId, paymentConsentId)).thenReturn(resource);

		adapterImpl.populateStageIdentifiers(paymentConsentId);
	}

	@Test
	public void testpopulateStageIdentifiersCMA1() {
		PaymentConsentsPlatformResource resource = new PaymentConsentsPlatformResource();
		String paymentConsentId = "43545";
		String paymentId = "43545";
		String paymentType = resource.getPaymentType();
		List<String> version = new ArrayList<String>();
		when(compatibleVersionList.fetchVersionList()).thenReturn(version);	
		Mockito.when(adapterImpl.retrievePaymentSetupPlatformResource(paymentId, paymentConsentId)).thenReturn(resource);

		adapterImpl.populateStageIdentifiers(paymentConsentId);
	}

	
	@Test
	public void testGetIdempotentFilePaymentSetupPlatformResource() {

		String fileHash = "wekrnb29835nsdf04325rf";
		PaymentConsentsPlatformResource value = new PaymentConsentsPlatformResource();
		Mockito.when(paymentSetupPlatformRepository
				.findOneByPaymentTypeAndTppCIDAndIdempotencyKeyAndIdempotencyRequestAndFileHashAndCreatedAtGreaterThanAndSetupCmaVersionIn(
						anyString(), anyString(), anyString(), anyString(), anyString(), anyString(),any()))
				.thenReturn(value);
		adapterImpl.getIdempotentFilePaymentSetupPlatformResource("file", 24, fileHash);
	}

	@SuppressWarnings("unchecked")
	@Test(expected = PSD2Exception.class)
	public void testGetIdempotentFilePaymentSetupPlatformResourceExc() {

		String fileHash = "wekrnb29835nsdf04325rf";
		Mockito.when(paymentSetupPlatformRepository
				.findOneByPaymentTypeAndTppCIDAndIdempotencyKeyAndIdempotencyRequestAndFileHashAndCreatedAtGreaterThanAndSetupCmaVersionIn(
						anyString(), anyString(), anyString(), anyString(), anyString(), anyString(),any()))
				.thenThrow(DataAccessResourceFailureException.class);
		adapterImpl.getIdempotentFilePaymentSetupPlatformResource("file", 24, fileHash);
	}

	@Test
	public void testGetIdempotentFileUploadSetupPlatformResource() {

		String consent = "59bb6c31-5831-4a4a-a2b1-d54f5b8f20d7";
		PaymentConsentsPlatformResource value = new PaymentConsentsPlatformResource();
		Mockito.when(paymentSetupPlatformRepository
				.findOneByPaymentTypeAndTppCIDAndIdempotencyKeyAndIdempotencyRequestAndPaymentConsentIdAndCreatedAtGreaterThanAndSetupCmaVersionIn(
						anyString(), anyString(), anyString(), anyString(), anyString(), anyString(),any()))
				.thenReturn(value);
		adapterImpl.getIdempotentFilePaymentSetupPlatformResource("file", 24, consent);
	}

	@SuppressWarnings("unchecked")
	@Test
	public void testGetIdempotentFileUploadSetupPlatformResourceExc() {

		String consent = "59bb6c31-5831-4a4a-a2b1-d54f5b8f20d7";
		PaymentConsentsPlatformResource value = new PaymentConsentsPlatformResource();
		Mockito.when(paymentSetupPlatformRepository
				.findOneByPaymentTypeAndTppCIDAndIdempotencyKeyAndIdempotencyRequestAndPaymentConsentIdAndCreatedAtGreaterThanAndSetupCmaVersionIn(
						anyString(), anyString(), anyString(), anyString(), anyString(), anyString(),any()))
				.thenThrow(DataAccessResourceFailureException.class);
		adapterImpl.getIdempotentFilePaymentSetupPlatformResource("file", System.currentTimeMillis(), consent);
	}

	@Test
	public void testCreatePaymentSetupPlatformResource() {

		CustomPaymentSetupPlatformDetails paymentSetupDetails = new CustomPaymentSetupPlatformDetails();
		paymentSetupDetails.setPaymentType(PaymentTypeEnum.DOMESTIC_PAY);
		String setupCreationDateTime = "2018-11-28T16:32:03+05:30";

		TppInformationTokenData tppInformation = new TppInformationTokenData();
		Token token = new Token();
		token.setTppInformation(tppInformation);
		token.setClient_id("5334");
		Mockito.when(reqHeaderAtrributes.getToken()).thenReturn(token);

		adapterImpl.createPaymentSetupPlatformResource(paymentSetupDetails, setupCreationDateTime);
	}

	@SuppressWarnings("unchecked")
	@Test
	public void testCreatePaymentSetupPlatformResourceExcep() {

		PaymentConsentsPlatformResource paymentSetupDetails = null;
		when(paymentSetupPlatformRepository.save(paymentSetupDetails))
				.thenThrow(DataAccessResourceFailureException.class);
		String setupCreationDateTime = null;
		PaymentResponseInfo stageResponse;
	}

	@Test
	public void testCreatePaymentSetupPlatformResourceConsent() {

		CustomPaymentSetupPlatformDetails paymentSetupDetails = new CustomPaymentSetupPlatformDetails();
		paymentSetupDetails.setPaymentType(PaymentTypeEnum.DOMESTIC_PAY);
		String setupCreationDateTime = "2018-11-28T16:32:03+05:30";
		Map<String, PaymentResponseInfo> params = new HashMap<>();
		PaymentResponseInfo stageResponse = new PaymentResponseInfo();
		stageResponse.setPaymentConsentId("1235");
		assertNotNull(stageResponse.getPaymentConsentId());
		params.put("1", stageResponse);
		TppInformationTokenData tppInformation = new TppInformationTokenData();
		Token token = new Token();
		token.setTppInformation(tppInformation);
		token.setClient_id("5334");
		Mockito.when(reqHeaderAtrributes.getToken()).thenReturn(token);

		stageResponse.setPayConsentValidationStatus(OBExternalConsentStatus2Code.AUTHORISED);
		adapterImpl.createPaymentSetupPlatformResource(paymentSetupDetails, setupCreationDateTime);
	}
	
	@Test
	public void PaymentSetupPlatformAdapterImplTest(){
		adapterImpl.getIdempotentFileUploadSetupPlatformResource("1234452636", 0, "85575858");
	}
	
	@Test
	public void retrievePaymentSetupPlatformResourceByIdAndTypeTest(){
		adapterImpl.retrievePaymentSetupPlatformResourceByIdAndType("243553636", "1213141516");
	}
	
	@Test(expected=PSD2Exception.class)
	public void retrievePaymentSetupPlatformResourceByIdAndTypeTestException(){
		when(paymentSetupPlatformRepository.findOneByPaymentConsentIdAndPaymentTypeAndSetupCmaVersionIn(anyString(), anyString(), any())).thenThrow(DataAccessResourceFailureException.class);
		adapterImpl.retrievePaymentSetupPlatformResourceByIdAndType("243553636", "1213141516");
	}
	
	@Test
	public void retrievePaymentSetupPlatformResourceByBackwardPaymentIdTest(){
		adapterImpl.retrievePaymentSetupPlatformResourceByBackwardPaymentId("123425");
	}
	
	@Test(expected=PSD2Exception.class)
	public void retrievePaymentSetupPlatformResourceByBackwardPaymentIdTestException(){
		when(paymentSetupPlatformRepository.findOneByPaymentIdAndSetupCmaVersionIn(anyString(),any())).thenThrow(DataAccessResourceFailureException.class);
		adapterImpl.retrievePaymentSetupPlatformResourceByBackwardPaymentId("123425");
	}

}
