package com.capgemini.psd2.account.direct.debits.mongo.db.adapter.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.account.direct.debits.mongo.db.adapter.repository.AccountDirectDebitRepository;
import com.capgemini.psd2.aisp.adapter.AccountDirectDebitsAdapter;
import com.capgemini.psd2.aisp.domain.OBReadDirectDebit2;
import com.capgemini.psd2.aisp.domain.OBReadDirectDebit2Data;
import com.capgemini.psd2.aisp.domain.OBReadDirectDebit2DataDirectDebit;
import com.capgemini.psd2.aisp.mongo.db.adapter.domain.AccountDirectDebitCMA2;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountDirectDebitsResponse;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.InternalServerErrorMessage;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.LoggerUtils;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.utilities.SandboxValidationUtility;

@Component
public class AccountDirectDebitsMongoDbAdapterImpl implements AccountDirectDebitsAdapter {

	@Autowired
	private AccountDirectDebitRepository repository;
	
	
	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(AccountDirectDebitsMongoDbAdapterImpl.class);
	
	/** The logger utils. */
	@Autowired
	private LoggerUtils loggerUtils;
	
	@Autowired
	SandboxValidationUtility utility;
   
	@Override
	public PlatformAccountDirectDebitsResponse retrieveAccountDirectDebits(AccountMapping accountMapping,
			Map<String, String> params) {
		
		LOG.info("{\"Enter\":\"{}\",\"{}\"}",
				"com.capgemini.psd2.account.product.mongo.db.adapter.impl.retrieveAccountProducts()",
				loggerUtils.populateLoggerData("retrieveAccountProducts"));

		PlatformAccountDirectDebitsResponse platformAccountDirectDebitsResponse = new PlatformAccountDirectDebitsResponse();
		OBReadDirectDebit2 oBReadDirectDebit = new OBReadDirectDebit2();
		AccountDirectDebitCMA2 mockDirectDebit = null;
		LOG.info("TenantId : " + params.get(PSD2Constants.TENANT_ID));

		try {
			if(utility.isValidPsuId(accountMapping.getPsuId())){
				throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,InternalServerErrorMessage.SANDBOX_MOCKED_ERROR));
			}
			mockDirectDebit = repository.findByAccountNumberAndAccountNSC(
					accountMapping.getAccountDetails().get(0).getAccountNumber(),
					accountMapping.getAccountDetails().get(0).getAccountNSC());
		} catch (DataAccessResourceFailureException e) {
			LOG.error(e.getMessage(),e);
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,InternalServerErrorMessage.CONNECTION_ERROR));
		}
		List<OBReadDirectDebit2DataDirectDebit> directDebitList = new ArrayList<>();
		if(mockDirectDebit!=null) {
			mockDirectDebit.setAccountId(accountMapping.getAccountDetails().get(0).getAccountId());
			directDebitList.add(mockDirectDebit);
		}

		OBReadDirectDebit2Data data1 = new OBReadDirectDebit2Data();
		data1.directDebit(directDebitList);

		oBReadDirectDebit.setData(data1);
		platformAccountDirectDebitsResponse.setoBReadDirectDebit2(oBReadDirectDebit);
		return platformAccountDirectDebitsResponse;
	}
}