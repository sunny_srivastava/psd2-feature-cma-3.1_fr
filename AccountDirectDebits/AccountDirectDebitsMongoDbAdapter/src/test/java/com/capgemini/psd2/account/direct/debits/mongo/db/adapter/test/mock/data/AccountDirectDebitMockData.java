package com.capgemini.psd2.account.direct.debits.mongo.db.adapter.test.mock.data;



import java.util.ArrayList;
import java.util.List;

import com.capgemini.psd2.aisp.domain.OBReadDirectDebit2;
import com.capgemini.psd2.aisp.domain.OBReadDirectDebit2Data;
import com.capgemini.psd2.aisp.domain.OBReadDirectDebit2DataDirectDebit;
import com.capgemini.psd2.aisp.mongo.db.adapter.domain.AccountDirectDebitCMA2;
import com.capgemini.psd2.logger.LoggerAttribute;

public class AccountDirectDebitMockData {
	
	public static AccountDirectDebitCMA2 getMockAccountDetails()
	{
		return new AccountDirectDebitCMA2();
	}
	
	public static LoggerAttribute getMockLoggerData() {
		LoggerAttribute x = new LoggerAttribute();
		x.setApiId("testApiID");
		return x;
	}
	
	public static OBReadDirectDebit2 getMockExpectedAccountDirectDebitresponse()
	{
		
		OBReadDirectDebit2Data obReadDirectDebit1Data = new OBReadDirectDebit2Data();
		OBReadDirectDebit2 obreadDirect1 = new OBReadDirectDebit2();
		AccountDirectDebitCMA2 mockDirectDebit = new AccountDirectDebitCMA2();
		
		mockDirectDebit.setAccountId("f4483fda-81be-4873-b4a6-20375b7f55c1");
		
		List<OBReadDirectDebit2DataDirectDebit> directDebitList = new ArrayList<>();
		directDebitList.add(mockDirectDebit);
		obReadDirectDebit1Data.setDirectDebit(directDebitList);
		obreadDirect1.setData(obReadDirectDebit1Data);
	
		
		return obreadDirect1;
	}
	
	

}
