package com.capgemini.psd2.account.direct.debits.routing.adapter.test.mock.data;

import java.util.ArrayList;
import java.util.List;

import com.capgemini.psd2.aisp.domain.OBReadDirectDebit2;
import com.capgemini.psd2.aisp.domain.OBReadDirectDebit2Data;
import com.capgemini.psd2.aisp.domain.OBReadDirectDebit2DataDirectDebit;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountDirectDebitsResponse;

public class AccountDirectDebitsRoutingAdapterTestMockData {

	public static PlatformAccountDirectDebitsResponse getMockDirectDebitResponse() {
		PlatformAccountDirectDebitsResponse platformAccountDirectDebitsResponse = new PlatformAccountDirectDebitsResponse();

		OBReadDirectDebit2Data directDebitData = new OBReadDirectDebit2Data();
		List<OBReadDirectDebit2DataDirectDebit> directDebit = new ArrayList();

		directDebitData.setDirectDebit(directDebit);

		OBReadDirectDebit2 obReadDirectDebit1 = new OBReadDirectDebit2();
		obReadDirectDebit1.setData(directDebitData);
		platformAccountDirectDebitsResponse.setoBReadDirectDebit2(obReadDirectDebit1);

		return platformAccountDirectDebitsResponse;
	}

}
