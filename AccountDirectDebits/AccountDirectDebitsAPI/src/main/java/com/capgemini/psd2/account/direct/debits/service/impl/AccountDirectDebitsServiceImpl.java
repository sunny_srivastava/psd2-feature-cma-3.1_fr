/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.account.direct.debits.service.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.capgemini.psd2.account.direct.debits.service.AccountDirectDebitsService;
import com.capgemini.psd2.aisp.account.mapping.adapter.AccountMappingAdapter;
import com.capgemini.psd2.aisp.adapter.AccountDirectDebitsAdapter;
import com.capgemini.psd2.aisp.adapter.AispConsentAdapter;
import com.capgemini.psd2.aisp.domain.Links;
import com.capgemini.psd2.aisp.domain.Meta;
import com.capgemini.psd2.aisp.domain.OBReadDirectDebit2;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountDirectDebitsResponse;
import com.capgemini.psd2.aisp.validation.adapter.AISPCustomValidator;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.consent.domain.AispConsent;
import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;

/**
 * The Class AccountDirectDebitsServiceImpl.
 */
@Service
public class AccountDirectDebitsServiceImpl implements AccountDirectDebitsService {

	/** The req header atrributes. */
	@Autowired
	private RequestHeaderAttributes reqHeaderAtrributes;

	/** The account direct debits adapter. */
	@Autowired
	@Qualifier("accountDirectDebitsRoutingAdapter")
	private AccountDirectDebitsAdapter accountDirectDebitsAdapter;
	
	/** The aisp consent adapter. */
	@Autowired
	private AispConsentAdapter aispConsentAdapter;
	
	/** The account mapping adapter. */
	@Autowired
	private AccountMappingAdapter accountMappingAdapter;

	@SuppressWarnings("rawtypes")
	@Autowired
	@Qualifier("accountsRoutingValidator")
	private AISPCustomValidator accountsCustomValidator;
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.capgemini.psd2.account.direct.debits.service.AccountDirectDebitsService#
	 * retrieveAccountDirectDebits(java.lang.String)
	 */
	@Override
	public OBReadDirectDebit2 retrieveAccountDirectDebits(String accountId) {

		accountsCustomValidator.validateUniqueId(accountId);
		// Get consents by accountId and validate.
		AispConsent aispConsent = aispConsentAdapter.validateAndRetrieveConsentByAccountId(accountId,
		reqHeaderAtrributes.getToken().getConsentTokenData().getConsentId());

		// Get account mapping.
		AccountMapping accountMapping = accountMappingAdapter.retrieveAccountMappingDetails(accountId, aispConsent);

		Map<String,String> params=reqHeaderAtrributes.getToken().getSeviceParams();
		params.put(PSD2Constants.CMAVERSION, aispConsent.getCmaVersion());
		params.put(PSD2Constants.CO_RELATION_ID, reqHeaderAtrributes.getCorrelationId());
		
		// Retrieve account directDebit.
		PlatformAccountDirectDebitsResponse platformAccountDirectDebitsResponse = accountDirectDebitsAdapter.retrieveAccountDirectDebits(accountMapping,params);
			
		if (platformAccountDirectDebitsResponse == null)
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.NO_RECORD_FOUND_FOR_REQUESTED_ACCT);

		// Get CMA2 directDebit response.
		OBReadDirectDebit2 tppDirectDebitResponse = platformAccountDirectDebitsResponse.getoBReadDirectDebit2();

		if (tppDirectDebitResponse == null)
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.NO_RECORD_FOUND_FOR_REQUESTED_ACCT);

		// update platform response.
		updatePlatformResponse(tppDirectDebitResponse);

		// Apply platform and CMA2-swagger validations.
		accountsCustomValidator.validateResponseParams(tppDirectDebitResponse);

		return tppDirectDebitResponse;
	}

	private void updatePlatformResponse(OBReadDirectDebit2 tppDirectDebitResponse) {
		if (tppDirectDebitResponse.getLinks() == null)
			tppDirectDebitResponse.setLinks(new Links());
		tppDirectDebitResponse.getLinks().setSelf(reqHeaderAtrributes.getSelfUrl());

		if (tppDirectDebitResponse.getMeta() == null)
			tppDirectDebitResponse.setMeta(new Meta());
		tppDirectDebitResponse.getMeta().setTotalPages(1);
	}
}
