package com.capgemini.psd2.account.direct.debits.test.mock.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.capgemini.psd2.aisp.domain.Links;
import com.capgemini.psd2.aisp.domain.Meta;
import com.capgemini.psd2.aisp.domain.OBReadDirectDebit2;
import com.capgemini.psd2.aisp.domain.OBReadDirectDebit2Data;
import com.capgemini.psd2.aisp.domain.OBReadDirectDebit2DataDirectDebit;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountDirectDebitsResponse;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.consent.domain.AispConsent;
import com.capgemini.psd2.enums.ConsentStatusEnum;
import com.capgemini.psd2.logger.LoggerAttribute;
import com.capgemini.psd2.token.ConsentTokenData;
import com.capgemini.psd2.token.Token;

/**
 * The Class AccountDirectDebitsMockData.
 */
public class AccountDirectDebitsMockData {

	/** The mock token. */
	public static Token mockToken;

	/**
	 * Gets the token.
	 *
	 * @return the token
	 */
	public static Token getToken() {
		mockToken = new Token();
		ConsentTokenData consentTokenData = new ConsentTokenData();
		consentTokenData.setConsentExpiry("1509348259877L");
		consentTokenData.setConsentId("12345");
		mockToken.setConsentTokenData(consentTokenData);
		Map<String,String> map=new HashMap<>();
		mockToken.setSeviceParams(map);
		return mockToken;
	}

	/**
	 * Gets the mock account mapping.
	 *
	 * @return the mock account mapping
	 */
	public static AccountMapping getMockAccountMapping() {
		AccountMapping mapping = new AccountMapping();
		mapping.setTppCID("tpp123");
		mapping.setPsuId("user123");
		List<AccountDetails> selectedAccounts = new ArrayList<>();
		AccountDetails accountRequest = new AccountDetails();
		accountRequest.setAccountId("269c3ff5-d7f8-419b-a3b9-7136c5b4611a");
		accountRequest.setAccountNSC("SC802001");
		accountRequest.setAccountNumber("10203345");
		selectedAccounts.add(accountRequest);
		mapping.setAccountDetails(selectedAccounts);
		return mapping;
	}

	public static OBReadDirectDebit2 getMockBlankResponse() {
		OBReadDirectDebit2 accountGETResponse1 = new OBReadDirectDebit2();
		OBReadDirectDebit2Data data = new OBReadDirectDebit2Data();
		List<OBReadDirectDebit2DataDirectDebit> directDebit = new ArrayList<>();
		data.setDirectDebit(directDebit);
		accountGETResponse1.setData(data);
		return accountGETResponse1;
	}

	public static AispConsent getMockAispConsent() {
		AispConsent aispConsent = new AispConsent();
		aispConsent.setTppCId("6443e15975554bce8099e35b88b40465");
		aispConsent.setPsuId("88888888");
		aispConsent.setConsentId("39032481");
		aispConsent.setAccountRequestId("ae8c4441-f783-4e80-8810-254241bed98c");
		aispConsent.setChannelId("B365");
		aispConsent.setStartDate("2017-08-09T06:44:31.250Z");
		aispConsent.setEndDate("2018-09-02T00:00:00.875");
		aispConsent.setTransactionFromDateTime("2015-05-03T00:00:00.800");
		aispConsent.setTransactionToDateTime("2018-12-03T00:00:00.345");
		aispConsent.setStatus(ConsentStatusEnum.AUTHORISED);

		List<AccountDetails> selectedAispConsentAccountDetails = new ArrayList<>();
		AccountDetails accountRequest = new AccountDetails();

		accountRequest.setAccountId("f4483fda-81be-4873-b4a6-20375b7f55c1");
		accountRequest.setAccountNSC("903779");
		accountRequest.setAccountNumber("76528776");

		selectedAispConsentAccountDetails.add(accountRequest);
		aispConsent.setAccountDetails(selectedAispConsentAccountDetails);

		return aispConsent;
	}

	public static PlatformAccountDirectDebitsResponse getAccountDebitCreditMockPlatformResponsewithLinksMeta() {
		PlatformAccountDirectDebitsResponse platformAccountDirectDebitsResponse = new PlatformAccountDirectDebitsResponse();

		OBReadDirectDebit2Data obReadDirectDebit1Data = new OBReadDirectDebit2Data();
		List<OBReadDirectDebit2DataDirectDebit> directDebitList = new ArrayList<>();

		OBReadDirectDebit2DataDirectDebit directDebit = new OBReadDirectDebit2DataDirectDebit();
		directDebit.setAccountId("f4483fda-81be-4873-b4a6-20375b7f55c1");
		directDebit.setDirectDebitId("TestDirectDebitId");
		directDebitList.add(directDebit);

		obReadDirectDebit1Data.setDirectDebit(directDebitList);
		OBReadDirectDebit2 obReadDirectDebit1 = new OBReadDirectDebit2();
		obReadDirectDebit1.setData(obReadDirectDebit1Data);

		Links links = new Links();
		obReadDirectDebit1.setLinks(links);
		Meta meta = new Meta();
		meta.setTotalPages(1);
		obReadDirectDebit1.setMeta(meta);

		platformAccountDirectDebitsResponse.setoBReadDirectDebit2(obReadDirectDebit1);

		return platformAccountDirectDebitsResponse;
	}
	//
	public static PlatformAccountDirectDebitsResponse returnNullplatformAccountDirectDebitsResponse () {
		return null;
	}
	//

	public static LoggerAttribute getMockLoggerData() {
		LoggerAttribute x = new LoggerAttribute();
		x.setApiId("testApiID");
		return x;
	}

	public static Object getMockExpectedAcoountDirectDebitResponseWithLinksMeta() {

		OBReadDirectDebit2Data obReadDirectDebit1Data = new OBReadDirectDebit2Data();
		List<OBReadDirectDebit2DataDirectDebit> directDebitList = new ArrayList<>();

		OBReadDirectDebit2DataDirectDebit directDebit = new OBReadDirectDebit2DataDirectDebit();
		directDebit.setAccountId("f4483fda-81be-4873-b4a6-20375b7f55c1");
		directDebit.setDirectDebitId("TestDirectDebitId");
		directDebitList.add(directDebit);

		obReadDirectDebit1Data.setDirectDebit(directDebitList);
		OBReadDirectDebit2 obReadDirectDebit1 = new OBReadDirectDebit2();
		obReadDirectDebit1.setData(obReadDirectDebit1Data);

		Links links = new Links();
		obReadDirectDebit1.setLinks(links);
		Meta meta = new Meta();

		meta.setTotalPages(1);
		obReadDirectDebit1.setMeta(meta);

		return obReadDirectDebit1;
	}

	public static PlatformAccountDirectDebitsResponse getAccountDebitCreditMockPlatformResponseWithoutLinksMeta() {

		PlatformAccountDirectDebitsResponse platformAccountDirectDebitsResponse = new PlatformAccountDirectDebitsResponse();

		OBReadDirectDebit2Data obReadDirectDebit1Data = new OBReadDirectDebit2Data();
		List<OBReadDirectDebit2DataDirectDebit> directDebitList = new ArrayList<>();

		OBReadDirectDebit2DataDirectDebit directDebit = new OBReadDirectDebit2DataDirectDebit();
		directDebit.setAccountId("123");
		directDebit.setDirectDebitId("TestDirectDebitId");
		directDebitList.add(directDebit);

		obReadDirectDebit1Data.setDirectDebit(directDebitList);
		OBReadDirectDebit2 obReadDirectDebit1 = new OBReadDirectDebit2();
		obReadDirectDebit1.setData(obReadDirectDebit1Data);

		platformAccountDirectDebitsResponse.setoBReadDirectDebit2(obReadDirectDebit1);

		return platformAccountDirectDebitsResponse;

	}
	
	public static Object getMockExpectedAcoountDirectDebitResponseWithoutLinksMeta() {

		OBReadDirectDebit2Data obReadDirectDebit1Data = new OBReadDirectDebit2Data();
		List<OBReadDirectDebit2DataDirectDebit> directDebitList = new ArrayList<>();

		OBReadDirectDebit2DataDirectDebit directDebit = new OBReadDirectDebit2DataDirectDebit();
		directDebit.setAccountId("123");
		directDebit.setDirectDebitId("TestDirectDebitId");
		directDebitList.add(directDebit);

		obReadDirectDebit1Data.setDirectDebit(directDebitList);
		OBReadDirectDebit2 obReadDirectDebit1 = new OBReadDirectDebit2();
		obReadDirectDebit1.setData(obReadDirectDebit1Data);

		Links links = new Links();
		obReadDirectDebit1.setLinks(links);
		Meta meta = new Meta();

		meta.setTotalPages(1);
		obReadDirectDebit1.setMeta(meta);

		return obReadDirectDebit1;
	}

}
