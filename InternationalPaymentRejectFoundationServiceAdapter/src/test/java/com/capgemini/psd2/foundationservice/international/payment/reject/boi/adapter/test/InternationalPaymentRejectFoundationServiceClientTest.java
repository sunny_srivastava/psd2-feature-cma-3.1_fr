package com.capgemini.psd2.foundationservice.international.payment.reject.boi.adapter.test;

import static org.mockito.Matchers.any;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.RestTemplate;

import com.capgemini.psd2.foundationservice.international.payment.reject.boi.adapter.client.InternationalPaymentRejectFoundationServiceClient;
import com.capgemini.psd2.foundationservice.international.payment.reject.boi.adapter.domain.PaymentInstructionProposalInternational;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.RestClientSync;

@RunWith(SpringJUnit4ClassRunner.class)
public class InternationalPaymentRejectFoundationServiceClientTest {
	@InjectMocks
	InternationalPaymentRejectFoundationServiceClient client;
	@Mock
	RestClientSync restClient;

	@Mock
	private RestTemplate restTemplate;

	@Test
	public void testrestTransportForInternationalPaymentFoundationServicePost() {
		RequestInfo reqInfo = new RequestInfo();
		HttpHeaders httpHeaders = new HttpHeaders();
		Mockito.when(restClient.callForPost(any(), any(), any(), any()))
				.thenReturn(new PaymentInstructionProposalInternational());
		client.restTransportForInternationalPaymentFoundationServicePost(reqInfo,
				PaymentInstructionProposalInternational.class, httpHeaders);
	}

}