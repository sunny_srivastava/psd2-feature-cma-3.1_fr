package com.capgemini.psd2.foundationservice.international.payment.reject.boi.adapter.test;

import static org.junit.Assert.assertNotNull;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpHeaders;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.foundationservice.international.payment.reject.boi.adapter.delegate.InternationalPaymentRejectFoundationServiceDelegate;
import com.capgemini.psd2.foundationservice.international.payment.reject.boi.adapter.transformer.InternationalPaymentRejectFoundationServiceTransformer;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;

public class InternationalPaymentRejectFoundationServiceDelegateTest {
	@InjectMocks
	InternationalPaymentRejectFoundationServiceDelegate delegate;

	@Mock
	InternationalPaymentRejectFoundationServiceTransformer transformer;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testcreatePaymentRequestHeadersPostNIGB() {
		ReflectionTestUtils.setField(delegate, "transactioReqHeader", "transactioReqHeader");
		ReflectionTestUtils.setField(delegate, "correlationMuleReqHeader", "correlation");
		ReflectionTestUtils.setField(delegate, "sourceUserReqHeader", "sorceuser");
		ReflectionTestUtils.setField(delegate, "sourcesystem", "source");
		ReflectionTestUtils.setField(delegate, "apiChannelCode", "channel");
		ReflectionTestUtils.setField(delegate, "internationalPaymentRejectBaseURL", "baseUrl");
		ReflectionTestUtils.setField(delegate, "systemApiVersion", "baseUrl");
		ReflectionTestUtils.setField(delegate, "channelBrand", "channelBrand");
		ReflectionTestUtils.setField(delegate, "internationalPaymentRejectSetupVersion", "3.0");

		Map<String, String> params = new HashMap<>();
		CustomPaymentStageIdentifiers stageIden = new CustomPaymentStageIdentifiers();
		params.put("transactioReqHeader", "boi");
		params.put("tenant_id", "BOIUK");
		params.put("sourceUserReqHeader", "sourceuser");
		params.put("apiChannelCode", "channel");
		params.put("X-BOI-CHANNEL", "channel");
		params.put("X-BOI-USER", "BOI");
		params.put("X-CORRELATION-ID", "Id");

		stageIden.setPaymentSetupVersion("3.0");
		HttpHeaders header = delegate.createPaymentRequestHeadersPost(stageIden, params);
		assertNotNull(header);
	}

	@Test
	public void testcreatePaymentRequestHeadersPostROI() {
		ReflectionTestUtils.setField(delegate, "transactioReqHeader", "transactioReqHeader");
		ReflectionTestUtils.setField(delegate, "correlationMuleReqHeader", "correlation");
		ReflectionTestUtils.setField(delegate, "sourceUserReqHeader", "sorceuser");
		ReflectionTestUtils.setField(delegate, "sourcesystem", "source");
		ReflectionTestUtils.setField(delegate, "apiChannelCode", "channel");
		ReflectionTestUtils.setField(delegate, "internationalPaymentRejectBaseURL", "baseUrl");
		ReflectionTestUtils.setField(delegate, "systemApiVersion", "baseUrl");
		ReflectionTestUtils.setField(delegate, "channelBrand", "channelBrand");
		ReflectionTestUtils.setField(delegate, "internationalPaymentRejectSetupVersion", "3.0");

		Map<String, String> params = new HashMap<>();

		CustomPaymentStageIdentifiers stageIden = new CustomPaymentStageIdentifiers();
		params.put("tenant_id", "BOIROI");
		stageIden.setPaymentSetupVersion("1.0");
		HttpHeaders header = delegate.createPaymentRequestHeadersPost(stageIden, params);
		assertNotNull(header);
	}

	@Test
	public void testgetPaymentFoundationServiceURL() {
		ReflectionTestUtils.setField(delegate, "internationalPaymentRejectBaseURL", "baseUrl");
		ReflectionTestUtils.setField(delegate, "internationalPaymentRejectSetupVersion", "3.0");
		String paymentInstuctionProposalId = "415641612";
		delegate.createPaymentFoundationServiceURL(paymentInstuctionProposalId);
	}
}