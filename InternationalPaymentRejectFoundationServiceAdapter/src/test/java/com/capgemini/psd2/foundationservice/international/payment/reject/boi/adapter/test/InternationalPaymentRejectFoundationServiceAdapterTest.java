package com.capgemini.psd2.foundationservice.international.payment.reject.boi.adapter.test;

import static org.mockito.Matchers.any;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.foundationservice.international.payment.reject.boi.adapter.InternationalPaymentRejectFoundationServiceAdapter;
import com.capgemini.psd2.foundationservice.international.payment.reject.boi.adapter.client.InternationalPaymentRejectFoundationServiceClient;
import com.capgemini.psd2.foundationservice.international.payment.reject.boi.adapter.delegate.InternationalPaymentRejectFoundationServiceDelegate;
import com.capgemini.psd2.foundationservice.international.payment.reject.boi.adapter.domain.PaymentInstructionProposalInternational;
import com.capgemini.psd2.foundationservice.international.payment.reject.boi.adapter.transformer.InternationalPaymentRejectFoundationServiceTransformer;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.pisp.domain.CustomIPaymentConsentsPOSTResponse;
import com.capgemini.psd2.pisp.enums.PaymentTypeEnum;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;

@RunWith(SpringJUnit4ClassRunner.class)
public class InternationalPaymentRejectFoundationServiceAdapterTest {

	@InjectMocks
	InternationalPaymentRejectFoundationServiceAdapter adapter;
	@Mock
	InternationalPaymentRejectFoundationServiceDelegate delegate;
	@Mock
	InternationalPaymentRejectFoundationServiceClient client;
	@Mock
	InternationalPaymentRejectFoundationServiceTransformer transformer;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testcreateStagingInternationalPaymentConsents() {
		CustomPaymentStageIdentifiers customPaymentStageIdentifiers = new CustomPaymentStageIdentifiers();
		customPaymentStageIdentifiers.setPaymentConsentId("0fca37a5-40fa-41fe-91c7-9e48453d9692");
		customPaymentStageIdentifiers.setPaymentSetupVersion("3.0");
		customPaymentStageIdentifiers.setPaymentTypeEnum(PaymentTypeEnum.INTERNATIONAL_PAY);
		Map<String, String> params = new HashMap<String, String>();
		params.put("consentFlowType", "PISP");
		params.put("x-channel-id", "BOL");
		params.put("x-user-id", "BOI999");
		params.put("X-BOI-PLATFORM", "platform");
		params.put("x-fapi-interaction-id", "87654321");
		params.put("X-API-SOURCE-SYSTEM", "sdsa");
		params.put("tenant_id", "BOIUK");
		params.put("X-API-CHANNEL-BRAND", "channelBrand");
		params.put(PSD2Constants.USER_IN_REQ_HEADER, "45");
		params.put(PSD2Constants.CORRELATION_REQ_HEADER, "145");
		params.put(PSD2Constants.CHANNEL_IN_REQ_HEADER, "42345");
		params.put(PSD2Constants.TENANT_ID, "BOIROI");
		HttpHeaders httpHeaders = new HttpHeaders();

		Mockito.when(delegate.createPaymentRequestHeadersPost(any(), any())).thenReturn(httpHeaders);
		Mockito.when(delegate.createPaymentFoundationServiceURL(any())).thenReturn(
				"http://localhost:9051//group-payments/p/payments-service/v3.0//international/payment-instruction-proposals/0fca37a5-40fa-41fe-91c7-9e48453d9692/reject");
		Mockito.when(client.restTransportForInternationalPaymentFoundationServicePost(any(), any(), any()))
				.thenReturn(new PaymentInstructionProposalInternational());
		Mockito.when(transformer.transformInternationalPaymentRejectResponse(any()))
				.thenReturn(new CustomIPaymentConsentsPOSTResponse());
		adapter.createStagingInternationalPaymentRejectConsents(customPaymentStageIdentifiers, params);
	}

}