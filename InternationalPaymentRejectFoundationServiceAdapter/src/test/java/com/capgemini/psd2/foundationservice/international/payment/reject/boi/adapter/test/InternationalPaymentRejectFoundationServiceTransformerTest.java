package com.capgemini.psd2.foundationservice.international.payment.reject.boi.adapter.test;

import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.foundationservice.international.payment.reject.boi.adapter.domain.AccountInformation;
import com.capgemini.psd2.foundationservice.international.payment.reject.boi.adapter.domain.Address;
import com.capgemini.psd2.foundationservice.international.payment.reject.boi.adapter.domain.Agent;
import com.capgemini.psd2.foundationservice.international.payment.reject.boi.adapter.domain.Amount;
import com.capgemini.psd2.foundationservice.international.payment.reject.boi.adapter.domain.AuthorisationType;
import com.capgemini.psd2.foundationservice.international.payment.reject.boi.adapter.domain.ChargeBearer;
import com.capgemini.psd2.foundationservice.international.payment.reject.boi.adapter.domain.Country;
import com.capgemini.psd2.foundationservice.international.payment.reject.boi.adapter.domain.Currency;
import com.capgemini.psd2.foundationservice.international.payment.reject.boi.adapter.domain.ExchangeRateQuote;
import com.capgemini.psd2.foundationservice.international.payment.reject.boi.adapter.domain.FinancialEventAmount;
import com.capgemini.psd2.foundationservice.international.payment.reject.boi.adapter.domain.PartyBasicInformation;
import com.capgemini.psd2.foundationservice.international.payment.reject.boi.adapter.domain.PaymentInstructionCharge;
import com.capgemini.psd2.foundationservice.international.payment.reject.boi.adapter.domain.PaymentInstructionPostalAddress;
import com.capgemini.psd2.foundationservice.international.payment.reject.boi.adapter.domain.PaymentInstructionProposalInternational;
import com.capgemini.psd2.foundationservice.international.payment.reject.boi.adapter.domain.PaymentInstrumentRiskFactor;
import com.capgemini.psd2.foundationservice.international.payment.reject.boi.adapter.domain.PaymentType;
import com.capgemini.psd2.foundationservice.international.payment.reject.boi.adapter.domain.PriorityCode;
import com.capgemini.psd2.foundationservice.international.payment.reject.boi.adapter.domain.ProposalStatus;
import com.capgemini.psd2.foundationservice.international.payment.reject.boi.adapter.domain.ProposingPartyAccount;
import com.capgemini.psd2.foundationservice.international.payment.reject.boi.adapter.domain.Purpose;
import com.capgemini.psd2.foundationservice.international.payment.reject.boi.adapter.domain.RateQuoteType;
import com.capgemini.psd2.foundationservice.international.payment.reject.boi.adapter.transformer.InternationalPaymentRejectFoundationServiceTransformer;
import com.capgemini.psd2.pisp.domain.CustomIPaymentConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.OBChargeBearerType1Code;
import com.capgemini.psd2.pisp.stage.domain.Charge;
import com.capgemini.psd2.validator.PSD2Validator;

@RunWith(SpringJUnit4ClassRunner.class)
public class InternationalPaymentRejectFoundationServiceTransformerTest {
	@InjectMocks
	InternationalPaymentRejectFoundationServiceTransformer transformer;

	@Mock
	private PSD2Validator psd2Validator;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testtransformInternationalPaymentResponse() {
		PaymentInstructionProposalInternational inputBalanceObj = new PaymentInstructionProposalInternational();
		inputBalanceObj.setPaymentInstructionProposalId("0fca37a5-40fa-41fe-91c7-9e48453d9692");
		inputBalanceObj.setProposalCreationDatetime("2019-12-05T15:15:37+00:00");
		inputBalanceObj.setProposalStatus(ProposalStatus.AWAITINGAUTHORISATION);
		inputBalanceObj.setProposalStatusUpdateDatetime("2019-12-10T15:15:37+00:00");
		inputBalanceObj.setInstructionEndToEndReference("FRESCO.21302.GFX.20");
		inputBalanceObj.setInstructionReference("ACME412");
		inputBalanceObj.setInstructionLocalInstrument("UK.OBIE.SEPACreditTransfer");
		
		Purpose purpose = new Purpose();
		purpose.setProprietaryPurpose("ABCD");
		inputBalanceObj.setPurpose(purpose);
			
		List<PaymentInstructionCharge> charges1 = new ArrayList<>();
		PaymentInstructionCharge paymentInstructionCharge = new PaymentInstructionCharge();
		paymentInstructionCharge.setChargeBearer(ChargeBearer.BORNEBYDEBTOR);
		paymentInstructionCharge.setType("Type1");
		charges1.add(paymentInstructionCharge);
		inputBalanceObj.setCharges(charges1);
		
		Charge charge = new Charge();
		charge.setChargeBearer(OBChargeBearerType1Code.BORNEBYCREDITOR);
		charge.setType("ABC");
		List<Charge> charges = new ArrayList<Charge>();
		charges.add(charge);
		
		Amount amt = new Amount();
		amt.setTransactionCurrency(12.00);
		paymentInstructionCharge.setAmount(amt);
		
		Currency currency = new Currency();
		currency.setIsoAlphaCode("USD");
		inputBalanceObj.setCurrencyOfTransfer(currency);
		paymentInstructionCharge.setCurrency(currency);
		
		FinancialEventAmount amount = new FinancialEventAmount();
		amount.setTransactionCurrency(23.0);
		inputBalanceObj.setFinancialEventAmount(amount);
		
		Currency currency_1 = new Currency();
		currency_1.setIsoAlphaCode("USD");
		inputBalanceObj.setTransactionCurrency(currency);
		
		AccountInformation acInfo = new AccountInformation();
		acInfo.setAccountName("DebtorAccount");
		acInfo.setAccountIdentification("65009123456789100000");
		acInfo.setSchemeName("IE.BOI.NCCAndAccountNumber.9");
		acInfo.setSecondaryIdentification("545643541584248");
		inputBalanceObj.setAuthorisingPartyAccount(acInfo);
		
		AccountInformation acInfo_1 = new AccountInformation();
		acInfo_1.setAccountName("CreditorAccount");
		acInfo_1.setAccountIdentification("65009123456789100000");
		acInfo_1.setSchemeName("IE.BOI.NCCAndAccountNumber.9");
		acInfo_1.setSecondaryIdentification("545643541584248");
		inputBalanceObj.setAuthorisingPartyAccount(acInfo_1);
		
		ExchangeRateQuote exchangeRateQuote = new ExchangeRateQuote();
		exchangeRateQuote.setRateQuoteType(RateQuoteType.ACTUAL);
		exchangeRateQuote.setExchangeRate(2.0412);
		currency_1.setIsoAlphaCode("USD");
		exchangeRateQuote.setUnitCurrency(currency_1);
		inputBalanceObj.setExchangeRateQuote(exchangeRateQuote);
		inputBalanceObj.setPriorityCode(PriorityCode.NORMAL);
		
		ProposingPartyAccount ppa = new ProposingPartyAccount();
		ppa.setAccountName("John Brooks");
		ppa.setAccountNumber("98778958795662");
		ppa.setSchemeName("UK.OBIE.BICFI");
		ppa.setSecondaryIdentification("002");
		ppa.setAccountIdentification("08080021325698");
		inputBalanceObj.setProposingPartyAccount(ppa);
				
		PartyBasicInformation partyBasicInformation = new PartyBasicInformation();
		partyBasicInformation.setPartyName("Lauren");
		inputBalanceObj.setProposingParty(partyBasicInformation);
		
		PaymentInstructionPostalAddress add = new PaymentInstructionPostalAddress();
		add.setTownName("Croatia");
		add.setDepartment("DEP");
		add.setCountrySubDivision("HR");
		add.setSubDepartment("ABC");
		add.setPostCodeNumber("600789");
		add.setGeoCodeBuildingName("XYZ");
		add.setGeoCodeBuildingNumber("1521");
		
		Country country = new Country();
		country.setIsoCountryAlphaTwoCode("HR");
		add.setAddressCountry(country);
		List<String> line = new ArrayList<>();
		line.add("Florida");
		add.setAddressLine(line);
		add.setAddressType("Postal");
		inputBalanceObj.setProposingPartyPostalAddress(add);

		inputBalanceObj.setPaymentType(PaymentType.INTERNATIONAL);
		inputBalanceObj.setAuthorisationDatetime("2019-12-30T19:07:24+00:00");
		PaymentInstrumentRiskFactor paymentInstrumentRiskFactor = new PaymentInstrumentRiskFactor();
		paymentInstrumentRiskFactor.setMerchantCategoryCode("BUI");
		paymentInstrumentRiskFactor.setPaymentContextCode("BILLPAYMENT");
		paymentInstrumentRiskFactor.setMerchantCustomerIdentification("053598653254");
		
		Address address = new Address();
		address.setFirstAddressLine("Flat 7");
		address.setSecondAddressLine("MIG");
		address.setThirdAddressLine("Acacia Lodge");
		address.setFourthAddressLine("Florida");
		address.setFifthAddressLine("US");
		address.setPostCodeNumber("600024");
		address.setGeoCodeBuildingNumber("1654");
		address.setGeoCodeBuildingName("DU");
		address.setAddressCountry(country);
		
		Country country1 = new Country();
		country1.setIsoCountryAlphaTwoCode("DEU");
		address.setAddressCountry(country1);
		paymentInstrumentRiskFactor.setCounterPartyAddress(address);
		inputBalanceObj.setPaymentInstructionRiskFactorReference(paymentInstrumentRiskFactor);

		Agent agent = new Agent();
		agent.setAgentName("Mike");
		agent.setPartyAddress(add);
		agent.setAgentIdentification("741258963456753159");
		agent.setSchemeName("UK.OBIE.BICFI");

		inputBalanceObj.setProposingPartyAgent(agent);
		inputBalanceObj.setProposalStatus(ProposalStatus.AWAITINGAUTHORISATION);
		inputBalanceObj.setAuthorisingPartyReference("ABCDEF");
		inputBalanceObj.setAuthorisingPartyUnstructuredReference("ANY TYPE");
		inputBalanceObj.setAuthorisationType(AuthorisationType.ANY);
		transformer.transformInternationalPaymentRejectResponse(inputBalanceObj);
	}
	
	@Test
	public void testRemittanceInformationAuthorisingPartyReference() {
		
		PaymentInstructionProposalInternational inputBalanceObj = new PaymentInstructionProposalInternational();
		
		Currency currency = new Currency();
		currency.setIsoAlphaCode("USD");
		inputBalanceObj.setCurrencyOfTransfer(currency);								
		
		FinancialEventAmount amount = new FinancialEventAmount();
		amount.setTransactionCurrency(23.0);
		inputBalanceObj.setFinancialEventAmount(amount);
		
		Currency currency_1 = new Currency();
		currency_1.setIsoAlphaCode("USD");
		inputBalanceObj.setTransactionCurrency(currency);
		
		inputBalanceObj.setAuthorisingPartyReference("ABCDEF");		
		CustomIPaymentConsentsPOSTResponse transformInternationalPaymentRejectResponse = transformer.transformInternationalPaymentRejectResponse(inputBalanceObj);		
		// if isInit condition is executed for not null AuthorisingPartyReference 
		assertNotNull(transformInternationalPaymentRejectResponse);
	}
	
	@Test
	public void testRemittanceInformationAuthorisingPartyUnstructuredReference() {
		
		PaymentInstructionProposalInternational inputBalanceObj = new PaymentInstructionProposalInternational();
		
		Currency currency = new Currency();
		currency.setIsoAlphaCode("USD");
		inputBalanceObj.setCurrencyOfTransfer(currency);								
		
		FinancialEventAmount amount = new FinancialEventAmount();
		amount.setTransactionCurrency(23.0);
		inputBalanceObj.setFinancialEventAmount(amount);
		
		Currency currency_1 = new Currency();
		currency_1.setIsoAlphaCode("USD");
		inputBalanceObj.setTransactionCurrency(currency);
		
		inputBalanceObj.setAuthorisingPartyUnstructuredReference("ANY TYPE");
		CustomIPaymentConsentsPOSTResponse transformInternationalPaymentRejectResponse = transformer.transformInternationalPaymentRejectResponse(inputBalanceObj);		
		// if isInit condition is executed for not null AuthorisingPartyUnstructuredReference 
		assertNotNull(transformInternationalPaymentRejectResponse);
	}
	
}