package com.capgemini.psd2.foundationservice.international.payment.reject.boi.adapter.transformer;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.capgemini.psd2.foundationservice.international.payment.reject.boi.adapter.domain.PaymentInstructionProposalInternational;
import com.capgemini.psd2.pisp.domain.CustomIPaymentConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.OBAddressTypeCode;
import com.capgemini.psd2.pisp.domain.OBAuthorisation1;
import com.capgemini.psd2.pisp.domain.OBBranchAndFinancialInstitutionIdentification3;
import com.capgemini.psd2.pisp.domain.OBCashAccountCreditor2;
import com.capgemini.psd2.pisp.domain.OBCashAccountDebtor3;
import com.capgemini.psd2.pisp.domain.OBCharge1;
import com.capgemini.psd2.pisp.domain.OBCharge1Amount;
import com.capgemini.psd2.pisp.domain.OBChargeBearerType1Code;
import com.capgemini.psd2.pisp.domain.OBDomestic1InstructedAmount;
import com.capgemini.psd2.pisp.domain.OBExchangeRate1;
import com.capgemini.psd2.pisp.domain.OBExchangeRateType2Code;
import com.capgemini.psd2.pisp.domain.OBExternalAuthorisation1Code;
import com.capgemini.psd2.pisp.domain.OBExternalConsentStatus1Code;
import com.capgemini.psd2.pisp.domain.OBExternalPaymentContext1Code;
import com.capgemini.psd2.pisp.domain.OBInternational1;
import com.capgemini.psd2.pisp.domain.OBPartyIdentification43;
import com.capgemini.psd2.pisp.domain.OBPostalAddress6;
import com.capgemini.psd2.pisp.domain.OBPriority2Code;
import com.capgemini.psd2.pisp.domain.OBRemittanceInformation1;
import com.capgemini.psd2.pisp.domain.OBRisk1;
import com.capgemini.psd2.pisp.domain.OBRisk1DeliveryAddress;
import com.capgemini.psd2.pisp.domain.OBWriteDataInternationalConsentResponse1;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Component
public class InternationalPaymentRejectFoundationServiceTransformer {

	public <T> CustomIPaymentConsentsPOSTResponse transformInternationalPaymentRejectResponse(T inputBalanceObj) {

		CustomIPaymentConsentsPOSTResponse customIPaymentConsentsPOSTResponse = new CustomIPaymentConsentsPOSTResponse();

		OBWriteDataInternationalConsentResponse1 oBWriteDataInternationalConsentResponse1 = new OBWriteDataInternationalConsentResponse1();

		PaymentInstructionProposalInternational paymentInstructionProposalInternational = (PaymentInstructionProposalInternational) inputBalanceObj;

		OBInternational1 oBInternational1 = new OBInternational1();

		OBRisk1 oBRisk1 = new OBRisk1();

		if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational.getPaymentInstructionProposalId())) {
			oBWriteDataInternationalConsentResponse1
					.setConsentId(paymentInstructionProposalInternational.getPaymentInstructionProposalId());
		}

		if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational.getProposalCreationDatetime())) {
			oBWriteDataInternationalConsentResponse1
					.setCreationDateTime(paymentInstructionProposalInternational.getProposalCreationDatetime());
		}

		if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational.getProposalStatus())) {
			OBExternalConsentStatus1Code status = OBExternalConsentStatus1Code
					.fromValue(paymentInstructionProposalInternational.getProposalStatus().toString());
			oBWriteDataInternationalConsentResponse1.setStatus(status);
		}

		if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational.getProposalStatusUpdateDatetime())) {
			oBWriteDataInternationalConsentResponse1
					.setStatusUpdateDateTime(paymentInstructionProposalInternational.getProposalStatusUpdateDatetime());
		}

		// CutOffDateTime,ExpectedExecutionDateTime,ExpectedSettlementDateTime were
		// absent--May be we need to set in mock

		List<OBCharge1> charges = new ArrayList<>();
		OBCharge1 oBCharge1 = new OBCharge1();
		if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational.getCharges())) {

			if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational.getCharges().get(0))) {
				if (!NullCheckUtils
						.isNullOrEmpty(paymentInstructionProposalInternational.getCharges().get(0).getChargeBearer())) {
					String chargeBearer = paymentInstructionProposalInternational.getCharges().get(0).getChargeBearer()
							.toString();
					OBChargeBearerType1Code oBChargeBearerType1Code = OBChargeBearerType1Code.fromValue(chargeBearer);
					oBCharge1.setChargeBearer(oBChargeBearerType1Code);
				}
			}

			if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational.getCharges().get(0).getType())) {
				oBCharge1.setType(paymentInstructionProposalInternational.getCharges().get(0).getType());
			}

			if ((!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational.getCharges()))) {

				if ((!NullCheckUtils
						.isNullOrEmpty(paymentInstructionProposalInternational.getCharges().get(0).getAmount()))
						&& (!NullCheckUtils.isNullOrEmpty(
								paymentInstructionProposalInternational.getCharges().get(0).getCurrency()))) {
					OBCharge1Amount chargeAmount = new OBCharge1Amount();
					Double chargeAmountMule = paymentInstructionProposalInternational.getCharges().get(0).getAmount()
							.getTransactionCurrency();
					chargeAmount.setAmount(String.valueOf(chargeAmountMule));
					chargeAmount.setCurrency(paymentInstructionProposalInternational.getCharges().get(0).getCurrency()
							.getIsoAlphaCode());
					oBCharge1.setAmount(chargeAmount);
				}

				charges.add(oBCharge1);
				// oBWriteDataInternationalConsentResponse1.setCharges(charges);
			}
		}

		if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational.getInstructionReference())) {
			oBInternational1
					.setInstructionIdentification(paymentInstructionProposalInternational.getInstructionReference());
		}

		if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational.getInstructionEndToEndReference())) {
			oBInternational1.setEndToEndIdentification(
					paymentInstructionProposalInternational.getInstructionEndToEndReference());
		}

		if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational.getInstructionLocalInstrument())) {
			oBInternational1
					.setLocalInstrument(paymentInstructionProposalInternational.getInstructionLocalInstrument());
		}
		if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational.getPriorityCode())) {
			OBPriority2Code oBPriority2Code = OBPriority2Code
					.fromValue(paymentInstructionProposalInternational.getPriorityCode().toString());
			oBInternational1.setInstructionPriority(oBPriority2Code);
		}
		if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational.getPurpose())) {
			if (!NullCheckUtils
					.isNullOrEmpty(paymentInstructionProposalInternational.getPurpose().getProprietaryPurpose())) {
				oBInternational1
						.setPurpose(paymentInstructionProposalInternational.getPurpose().getProprietaryPurpose());
			}
		}
		if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational.getCharges())) {
			if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational.getCharges().get(0))) {
				if (!NullCheckUtils
						.isNullOrEmpty(paymentInstructionProposalInternational.getCharges().get(0).getChargeBearer())) {
					OBChargeBearerType1Code oBChargeBearerType1Code = OBChargeBearerType1Code.fromValue(
							paymentInstructionProposalInternational.getCharges().get(0).getChargeBearer().toString());
					oBInternational1.setChargeBearer(oBChargeBearerType1Code);
				}
			}
		}
		if (!NullCheckUtils
				.isNullOrEmpty(paymentInstructionProposalInternational.getCurrencyOfTransfer().getIsoAlphaCode())) {

			oBInternational1.setCurrencyOfTransfer(
					paymentInstructionProposalInternational.getCurrencyOfTransfer().getIsoAlphaCode());
		}

		if (!NullCheckUtils.isNullOrEmpty(
				paymentInstructionProposalInternational.getFinancialEventAmount().getTransactionCurrency())
				&& !NullCheckUtils.isNullOrEmpty(
						paymentInstructionProposalInternational.getTransactionCurrency().getIsoAlphaCode())) {
			OBDomestic1InstructedAmount amountInstructed = new OBDomestic1InstructedAmount();

			Double amountMule = paymentInstructionProposalInternational.getFinancialEventAmount()
					.getTransactionCurrency();
			amountInstructed.setAmount(BigDecimal.valueOf(amountMule).toPlainString());
			// amountInstructed.setAmount(doubleToString(amountMule));
			amountInstructed
					.setCurrency(paymentInstructionProposalInternational.getTransactionCurrency().getIsoAlphaCode());
			oBInternational1.setInstructedAmount(amountInstructed);
		}

		// ExchangeRateInformation
		OBExchangeRate1 oBExchangeRate1 = new OBExchangeRate1();
		if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational.getExchangeRateQuote())) {

			if (!NullCheckUtils
					.isNullOrEmpty(paymentInstructionProposalInternational.getExchangeRateQuote().getUnitCurrency())) {
				oBExchangeRate1.setUnitCurrency(paymentInstructionProposalInternational.getExchangeRateQuote()
						.getUnitCurrency().getIsoAlphaCode().toString());
			}
			if (!NullCheckUtils
					.isNullOrEmpty(paymentInstructionProposalInternational.getExchangeRateQuote().getExchangeRate())) {
				oBExchangeRate1.setExchangeRate(BigDecimal
						.valueOf(paymentInstructionProposalInternational.getExchangeRateQuote().getExchangeRate()));
			}
			if (!NullCheckUtils
					.isNullOrEmpty(paymentInstructionProposalInternational.getExchangeRateQuote().getRateQuoteType())) {
				oBExchangeRate1.setRateType(OBExchangeRateType2Code.fromValue(
						paymentInstructionProposalInternational.getExchangeRateQuote().getRateQuoteType().toString()));
			}

			oBInternational1.setExchangeRateInformation(oBExchangeRate1);
		}

		// Debtor Account

		if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational.getAuthorisingPartyAccount())) {
			OBCashAccountDebtor3 oBCashAccountDebtor3 = new OBCashAccountDebtor3();
			if (!NullCheckUtils.isNullOrEmpty(
					paymentInstructionProposalInternational.getAuthorisingPartyAccount().getSchemeName())) {
				oBCashAccountDebtor3.setSchemeName(
						paymentInstructionProposalInternational.getAuthorisingPartyAccount().getSchemeName());
			}

			if (!NullCheckUtils.isNullOrEmpty(
					paymentInstructionProposalInternational.getAuthorisingPartyAccount().getAccountIdentification())) {
				oBCashAccountDebtor3.setIdentification(paymentInstructionProposalInternational
						.getAuthorisingPartyAccount().getAccountIdentification());
			}

			if (!NullCheckUtils.isNullOrEmpty(
					paymentInstructionProposalInternational.getAuthorisingPartyAccount().getAccountName())) {
				oBCashAccountDebtor3
						.setName(paymentInstructionProposalInternational.getAuthorisingPartyAccount().getAccountName());
			}

			if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational.getAuthorisingPartyAccount()
					.getSecondaryIdentification())) {
				oBCashAccountDebtor3.setSecondaryIdentification(paymentInstructionProposalInternational
						.getAuthorisingPartyAccount().getSecondaryIdentification());
			}

			oBInternational1.setDebtorAccount(oBCashAccountDebtor3);
		}

		// Creditor Account

		if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational.getProposingPartyAccount())) {
			OBCashAccountCreditor2 oBCashAccountCreditor2 = new OBCashAccountCreditor2();
			if (!NullCheckUtils.isNullOrEmpty(
					paymentInstructionProposalInternational.getProposingPartyAccount().getSchemeName())) {
				oBCashAccountCreditor2.setSchemeName(
						paymentInstructionProposalInternational.getProposingPartyAccount().getSchemeName());
			}

			if (!NullCheckUtils.isNullOrEmpty(
					paymentInstructionProposalInternational.getProposingPartyAccount().getAccountIdentification())) {
				oBCashAccountCreditor2.setIdentification(
						paymentInstructionProposalInternational.getProposingPartyAccount().getAccountIdentification());
			}

			if (!NullCheckUtils.isNullOrEmpty(
					paymentInstructionProposalInternational.getProposingPartyAccount().getAccountName())) {
				oBCashAccountCreditor2
						.setName(paymentInstructionProposalInternational.getProposingPartyAccount().getAccountName());
			}

			if (!NullCheckUtils.isNullOrEmpty(
					paymentInstructionProposalInternational.getProposingPartyAccount().getSecondaryIdentification())) {
				oBCashAccountCreditor2.setSecondaryIdentification(paymentInstructionProposalInternational
						.getProposingPartyAccount().getSecondaryIdentification());
			}

			oBInternational1.setCreditorAccount(oBCashAccountCreditor2);

		}

		// Creditor Logic need to add
		OBPartyIdentification43 oBPartyIdentification43 = new OBPartyIdentification43();
		if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational.getProposingParty())) {
			if (!NullCheckUtils
					.isNullOrEmpty(paymentInstructionProposalInternational.getProposingParty().getPartyName())) {
				oBPartyIdentification43
						.setName(paymentInstructionProposalInternational.getProposingParty().getPartyName());
				oBInternational1.setCreditor(oBPartyIdentification43);
			}
		}

		OBPostalAddress6 oBPostalAddress6 = new OBPostalAddress6();

		// Creditor Postal Address

		if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational.getProposingPartyPostalAddress())) {

			if (!NullCheckUtils.isNullOrEmpty(
					paymentInstructionProposalInternational.getProposingPartyPostalAddress().getAddressType())) {
				OBAddressTypeCode addressType = OBAddressTypeCode.fromValue(
						paymentInstructionProposalInternational.getProposingPartyPostalAddress().getAddressType());
				oBPostalAddress6.setAddressType(addressType);
			}

			if (!NullCheckUtils.isNullOrEmpty(
					paymentInstructionProposalInternational.getProposingPartyPostalAddress().getDepartment())) {
				oBPostalAddress6.setDepartment(
						paymentInstructionProposalInternational.getProposingPartyPostalAddress().getDepartment());
			}

			if (!NullCheckUtils.isNullOrEmpty(
					paymentInstructionProposalInternational.getProposingPartyPostalAddress().getSubDepartment())) {
				oBPostalAddress6.setSubDepartment(
						paymentInstructionProposalInternational.getProposingPartyPostalAddress().getSubDepartment());
			}

			if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational.getProposingPartyPostalAddress()
					.getGeoCodeBuildingName())) {
				oBPostalAddress6.setStreetName(paymentInstructionProposalInternational.getProposingPartyPostalAddress()
						.getGeoCodeBuildingName());
			}

			if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational.getProposingPartyPostalAddress()
					.getGeoCodeBuildingNumber())) {
				oBPostalAddress6.setBuildingNumber(paymentInstructionProposalInternational
						.getProposingPartyPostalAddress().getGeoCodeBuildingNumber());
			}

			if (!NullCheckUtils.isNullOrEmpty(
					paymentInstructionProposalInternational.getProposingPartyPostalAddress().getPostCodeNumber())) {
				oBPostalAddress6.setPostCode(
						paymentInstructionProposalInternational.getProposingPartyPostalAddress().getPostCodeNumber());
			}

			if (!NullCheckUtils.isNullOrEmpty(
					paymentInstructionProposalInternational.getProposingPartyPostalAddress().getTownName())) {
				oBPostalAddress6.setTownName(
						paymentInstructionProposalInternational.getProposingPartyPostalAddress().getTownName());
			}

			if (!NullCheckUtils.isNullOrEmpty(
					paymentInstructionProposalInternational.getProposingPartyPostalAddress().getCountrySubDivision())) {
				oBPostalAddress6.setCountrySubDivision(paymentInstructionProposalInternational
						.getProposingPartyPostalAddress().getCountrySubDivision());
			}

			if (!NullCheckUtils.isNullOrEmpty(
					paymentInstructionProposalInternational.getProposingPartyPostalAddress().getAddressCountry())
					&& !NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational
							.getProposingPartyPostalAddress().getAddressCountry().getIsoCountryAlphaTwoCode())) {

				oBPostalAddress6.setCountry(paymentInstructionProposalInternational.getProposingPartyPostalAddress()
						.getAddressCountry().getIsoCountryAlphaTwoCode());

			}

			List<String> addLine = paymentInstructionProposalInternational.getProposingPartyPostalAddress()
					.getAddressLine();
			if (!NullCheckUtils.isNullOrEmpty(addLine) && addLine.size() > 0) {
				oBPostalAddress6.setAddressLine(
						paymentInstructionProposalInternational.getProposingPartyPostalAddress().getAddressLine());
			}
			oBPartyIdentification43.setPostalAddress(oBPostalAddress6);

		}

		// CreditorAgent
		if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational.getProposingPartyAgent())) {
			OBBranchAndFinancialInstitutionIdentification3 oBBranchAndFinancialInstitutionIdentification3 = new OBBranchAndFinancialInstitutionIdentification3();
			// SchemeName to be decided
			if (!NullCheckUtils
					.isNullOrEmpty(paymentInstructionProposalInternational.getProposingPartyAgent().getSchemeName())) {
				oBBranchAndFinancialInstitutionIdentification3.setSchemeName(
						paymentInstructionProposalInternational.getProposingPartyAgent().getSchemeName());
			}
			if (!NullCheckUtils.isNullOrEmpty(
					paymentInstructionProposalInternational.getProposingPartyAgent().getAgentIdentification())) {
				oBBranchAndFinancialInstitutionIdentification3.setIdentification(
						paymentInstructionProposalInternational.getProposingPartyAgent().getAgentIdentification());
			}
			if (!NullCheckUtils
					.isNullOrEmpty(paymentInstructionProposalInternational.getProposingPartyAgent().getAgentName())) {
				oBBranchAndFinancialInstitutionIdentification3
						.setName(paymentInstructionProposalInternational.getProposingPartyAgent().getAgentName());
			}

			// PostalAddress of Creditor agent

			if (!NullCheckUtils.isNullOrEmpty(
					paymentInstructionProposalInternational.getProposingPartyAgent().getPartyAddress())) {

				if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational.getProposingPartyAgent()
						.getPartyAddress().getAddressType())) {
					OBAddressTypeCode oBAddressTypeCode = OBAddressTypeCode
							.fromValue(paymentInstructionProposalInternational.getProposingPartyAgent()
									.getPartyAddress().getAddressType().toString());
					oBPostalAddress6.setAddressType(oBAddressTypeCode);
				}

				if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational.getProposingPartyAgent()
						.getPartyAddress().getDepartment())) {
					oBPostalAddress6.setDepartment(paymentInstructionProposalInternational.getProposingPartyAgent()
							.getPartyAddress().getDepartment().toString());
				}

				if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational.getProposingPartyAgent()
						.getPartyAddress().getSubDepartment())) {
					oBPostalAddress6.setSubDepartment(paymentInstructionProposalInternational.getProposingPartyAgent()
							.getPartyAddress().getSubDepartment().toString());
				}

				if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational.getProposingPartyAgent()
						.getPartyAddress().getGeoCodeBuildingName())) {
					oBPostalAddress6.setStreetName(paymentInstructionProposalInternational.getProposingPartyAgent()
							.getPartyAddress().getGeoCodeBuildingName().toString());
				}

				if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational.getProposingPartyAgent()
						.getPartyAddress().getGeoCodeBuildingNumber())) {
					oBPostalAddress6.setBuildingNumber(paymentInstructionProposalInternational.getProposingPartyAgent()
							.getPartyAddress().getGeoCodeBuildingNumber().toString());
				}

				if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational.getProposingPartyAgent()
						.getPartyAddress().getTownName())) {
					oBPostalAddress6.setTownName(paymentInstructionProposalInternational.getProposingPartyAgent()
							.getPartyAddress().getTownName().toString());
				}

				if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational.getProposingPartyAgent()
						.getPartyAddress().getCountrySubDivision())) {
					oBPostalAddress6.setCountrySubDivision(paymentInstructionProposalInternational
							.getProposingPartyAgent().getPartyAddress().getCountrySubDivision().toString());
				}

				if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational.getProposingPartyAgent()
						.getPartyAddress().getAddressCountry())
						&& !NullCheckUtils
								.isNullOrEmpty(paymentInstructionProposalInternational.getProposingPartyAgent()
										.getPartyAddress().getAddressCountry().getIsoCountryAlphaTwoCode())) {
					oBPostalAddress6.setCountry(paymentInstructionProposalInternational.getProposingPartyAgent()
							.getPartyAddress().getAddressCountry().getIsoCountryAlphaTwoCode().toString());
				}

				if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational.getProposingPartyAgent()
						.getPartyAddress().getAddressLine())) {
					List<String> addLine = paymentInstructionProposalInternational.getProposingPartyPostalAddress()
							.getAddressLine();
					if (!NullCheckUtils.isNullOrEmpty(addLine) && addLine.size() > 0) {
						oBPostalAddress6.setAddressLine(addLine);
					}

				}
			}

			oBBranchAndFinancialInstitutionIdentification3.setPostalAddress(oBPostalAddress6);
			oBInternational1.setCreditorAgent(oBBranchAndFinancialInstitutionIdentification3);
		}

		// RemittanceInformation

		if ((!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational.getAuthorisingPartyReference()))
				|| (!NullCheckUtils.isNullOrEmpty(
						paymentInstructionProposalInternational.getAuthorisingPartyUnstructuredReference()))) {
			OBRemittanceInformation1 oBRemittanceInformation1 = new OBRemittanceInformation1();
			boolean isInit = false;
			if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational.getAuthorisingPartyReference())) {
				oBRemittanceInformation1
						.setReference(paymentInstructionProposalInternational.getAuthorisingPartyReference());
				isInit = true;
			}

			if (!NullCheckUtils.isNullOrEmpty(
					paymentInstructionProposalInternational.getAuthorisingPartyUnstructuredReference())) {
				oBRemittanceInformation1.setUnstructured(
						paymentInstructionProposalInternational.getAuthorisingPartyUnstructuredReference());
				isInit = true;
			}

			if (isInit)
				oBInternational1.setRemittanceInformation(oBRemittanceInformation1);
		}

		// Authorisation

		if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational.getAuthorisationType())) {
			OBAuthorisation1 oBAuthorisation1 = new OBAuthorisation1();
			String authType = paymentInstructionProposalInternational.getAuthorisationType().toString();
			OBExternalAuthorisation1Code authCode = OBExternalAuthorisation1Code.fromValue(authType);
			oBAuthorisation1.setAuthorisationType(authCode);

			if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational.getAuthorisationDatetime())) {
				oBAuthorisation1
						.setCompletionDateTime(paymentInstructionProposalInternational.getAuthorisationDatetime());
			}

			oBWriteDataInternationalConsentResponse1.setAuthorisation(oBAuthorisation1);
		}

		// Risk

		if (!NullCheckUtils
				.isNullOrEmpty(paymentInstructionProposalInternational.getPaymentInstructionRiskFactorReference())) {
			OBRisk1DeliveryAddress oBRisk1DeliveryAddress = null;

			if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational
					.getPaymentInstructionRiskFactorReference().getPaymentContextCode())) {
				OBExternalPaymentContext1Code contextCode = OBExternalPaymentContext1Code
						.fromValue(paymentInstructionProposalInternational.getPaymentInstructionRiskFactorReference()
								.getPaymentContextCode());
				oBRisk1.setPaymentContextCode(contextCode);
			}

			if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational
					.getPaymentInstructionRiskFactorReference().getMerchantCategoryCode())) {
				oBRisk1.setMerchantCategoryCode(paymentInstructionProposalInternational
						.getPaymentInstructionRiskFactorReference().getMerchantCategoryCode());
			}

			if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational
					.getPaymentInstructionRiskFactorReference().getMerchantCustomerIdentification())) {
				oBRisk1.setMerchantCustomerIdentification(paymentInstructionProposalInternational
						.getPaymentInstructionRiskFactorReference().getMerchantCustomerIdentification());
			}

			if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational
					.getPaymentInstructionRiskFactorReference().getCounterPartyAddress())) {
				oBRisk1DeliveryAddress = new OBRisk1DeliveryAddress();
				if (!NullCheckUtils.isNullOrEmpty((paymentInstructionProposalInternational
						.getPaymentInstructionRiskFactorReference().getCounterPartyAddress().getFirstAddressLine()))) {
					List<String> addressLine = new ArrayList<>();
					if (!NullCheckUtils.isNullOrEmpty(
							(paymentInstructionProposalInternational.getPaymentInstructionRiskFactorReference()
									.getCounterPartyAddress().getSecondAddressLine()))) {
						addressLine
								.add(paymentInstructionProposalInternational.getPaymentInstructionRiskFactorReference()
										.getCounterPartyAddress().getFirstAddressLine());
						addressLine
								.add(paymentInstructionProposalInternational.getPaymentInstructionRiskFactorReference()
										.getCounterPartyAddress().getSecondAddressLine());
					} else {
						addressLine
								.add(paymentInstructionProposalInternational.getPaymentInstructionRiskFactorReference()
										.getCounterPartyAddress().getFirstAddressLine());
					}

					oBRisk1DeliveryAddress.setAddressLine(addressLine);
				}

				if (!NullCheckUtils.isNullOrEmpty(
						paymentInstructionProposalInternational.getPaymentInstructionRiskFactorReference()
								.getCounterPartyAddress().getGeoCodeBuildingName())) {
					oBRisk1DeliveryAddress.setStreetName(
							paymentInstructionProposalInternational.getPaymentInstructionRiskFactorReference()
									.getCounterPartyAddress().getGeoCodeBuildingName());
				}

				if (!NullCheckUtils.isNullOrEmpty(
						paymentInstructionProposalInternational.getPaymentInstructionRiskFactorReference()
								.getCounterPartyAddress().getGeoCodeBuildingNumber())) {
					oBRisk1DeliveryAddress.setBuildingNumber(
							paymentInstructionProposalInternational.getPaymentInstructionRiskFactorReference()
									.getCounterPartyAddress().getGeoCodeBuildingNumber());
				}

				if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational
						.getPaymentInstructionRiskFactorReference().getCounterPartyAddress().getPostCodeNumber())) {
					oBRisk1DeliveryAddress.setPostCode(paymentInstructionProposalInternational
							.getPaymentInstructionRiskFactorReference().getCounterPartyAddress().getPostCodeNumber());
				}

				if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational
						.getPaymentInstructionRiskFactorReference().getCounterPartyAddress().getThirdAddressLine())) {
					oBRisk1DeliveryAddress.setTownName(paymentInstructionProposalInternational
							.getPaymentInstructionRiskFactorReference().getCounterPartyAddress().getThirdAddressLine());
				}

				if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational
						.getPaymentInstructionRiskFactorReference().getCounterPartyAddress().getFourthAddressLine())) {
					String addressLine = null;
					if (!NullCheckUtils.isNullOrEmpty(
							paymentInstructionProposalInternational.getPaymentInstructionRiskFactorReference()
									.getCounterPartyAddress().getFifthAddressLine())) {
						addressLine = paymentInstructionProposalInternational.getPaymentInstructionRiskFactorReference()
								.getCounterPartyAddress().getFourthAddressLine()
								+ paymentInstructionProposalInternational.getPaymentInstructionRiskFactorReference()
										.getCounterPartyAddress().getFifthAddressLine();
					} else {
						addressLine = paymentInstructionProposalInternational.getPaymentInstructionRiskFactorReference()
								.getCounterPartyAddress().getFourthAddressLine();
					}

					oBRisk1DeliveryAddress.setCountrySubDivision(addressLine);
				}

				if (!NullCheckUtils.isNullOrEmpty(
						paymentInstructionProposalInternational.getPaymentInstructionRiskFactorReference()
								.getCounterPartyAddress().getAddressCountry().getIsoCountryAlphaTwoCode())) {
					oBRisk1DeliveryAddress.setCountry(
							paymentInstructionProposalInternational.getPaymentInstructionRiskFactorReference()
									.getCounterPartyAddress().getAddressCountry().getIsoCountryAlphaTwoCode());
				}

				oBRisk1.setDeliveryAddress(oBRisk1DeliveryAddress);
			}

		}

		oBWriteDataInternationalConsentResponse1.setInitiation(oBInternational1);
		customIPaymentConsentsPOSTResponse.setData(oBWriteDataInternationalConsentResponse1);
		customIPaymentConsentsPOSTResponse.setRisk(oBRisk1);

		return customIPaymentConsentsPOSTResponse;
	}

}
