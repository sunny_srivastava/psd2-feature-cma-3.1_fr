package com.capgemini.psd2.foundationservice.international.payment.reject.boi.adapter.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.foundationservice.international.payment.reject.boi.adapter.domain.PaymentInstructionProposalInternational;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.RestClientSync;

@Component
public class InternationalPaymentRejectFoundationServiceClient {

	@Autowired
	@Qualifier("restClientFoundation")
	private RestClientSync restClient;
	
	public PaymentInstructionProposalInternational restTransportForInternationalPaymentFoundationServicePost(
			RequestInfo requestInfo, Class<PaymentInstructionProposalInternational> responseType,
			HttpHeaders httpHeaders) {
		return restClient.callForPost(requestInfo, null, responseType, httpHeaders);
	}

}
