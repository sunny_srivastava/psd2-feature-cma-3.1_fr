package com.capgemini.psd2.foundationservice.international.payment.reject.boi.adapter;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.foundationservice.international.payment.reject.boi.adapter.client.InternationalPaymentRejectFoundationServiceClient;
import com.capgemini.psd2.foundationservice.international.payment.reject.boi.adapter.delegate.InternationalPaymentRejectFoundationServiceDelegate;
import com.capgemini.psd2.foundationservice.international.payment.reject.boi.adapter.domain.PaymentInstructionProposalInternational;
import com.capgemini.psd2.foundationservice.international.payment.reject.boi.adapter.transformer.InternationalPaymentRejectFoundationServiceTransformer;
import com.capgemini.psd2.pisp.domain.CustomIPaymentConsentsPOSTResponse;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.rest.client.model.RequestInfo;

@Component
public class InternationalPaymentRejectFoundationServiceAdapter {

	@Autowired
	private InternationalPaymentRejectFoundationServiceDelegate intPayConsDelegate;

	@Autowired
	private InternationalPaymentRejectFoundationServiceClient intPayConsClient;
	
	@Autowired
	private InternationalPaymentRejectFoundationServiceTransformer intPayConsTransformer;
	
	public CustomIPaymentConsentsPOSTResponse createStagingInternationalPaymentRejectConsents(
			CustomPaymentStageIdentifiers customStageIdentifiers, Map<String, String> params) {

		RequestInfo requestInfo = new RequestInfo();
		HttpHeaders httpHeaders = intPayConsDelegate.createPaymentRequestHeadersPost(customStageIdentifiers, params);
		String internationalPaymentPostURL = intPayConsDelegate
				.createPaymentFoundationServiceURL(customStageIdentifiers.getPaymentConsentId());
		requestInfo.setUrl(internationalPaymentPostURL);
		PaymentInstructionProposalInternational paymentInstructionProposalResponse = intPayConsClient
				.restTransportForInternationalPaymentFoundationServicePost(requestInfo,
					PaymentInstructionProposalInternational.class, httpHeaders);
		return intPayConsTransformer
				.transformInternationalPaymentRejectResponse(paymentInstructionProposalResponse);
	}

}
