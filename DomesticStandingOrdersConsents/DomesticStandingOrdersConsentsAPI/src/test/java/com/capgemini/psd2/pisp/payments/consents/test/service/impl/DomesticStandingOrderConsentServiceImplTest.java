package com.capgemini.psd2.pisp.payments.consents.test.service.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyMap;
import static org.mockito.Matchers.anyObject;

import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.domain.CustomDStandingOrderConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomDStandingOrderConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.OBCashAccountDebtor3;
import com.capgemini.psd2.pisp.domain.OBDomesticStandingOrder1;
import com.capgemini.psd2.pisp.domain.OBRisk1;
import com.capgemini.psd2.pisp.domain.OBRisk1DeliveryAddress;
import com.capgemini.psd2.pisp.domain.OBWriteDataDomesticStandingOrderConsent1;
import com.capgemini.psd2.pisp.domain.OBWriteDataDomesticStandingOrderConsentResponse1;
import com.capgemini.psd2.pisp.domain.PaymentConsentsPlatformResource;
import com.capgemini.psd2.pisp.domain.PaymentRetrieveGetRequest;
import com.capgemini.psd2.pisp.enums.ProcessConsentStatusEnum;
import com.capgemini.psd2.pisp.payments.consents.comparator.DStandingOrderConsentPayloadComparator;
import com.capgemini.psd2.pisp.payments.consents.service.impl.DomesticStandingOrderConsentServiceImpl;
import com.capgemini.psd2.pisp.processing.adapter.service.PaymentConsentProcessingAdapter;
import com.capgemini.psd2.pisp.stage.operations.adapter.DomesticStandingOrdersPaymentStagingAdapter;

@RunWith(SpringJUnit4ClassRunner.class)
public class DomesticStandingOrderConsentServiceImplTest {

	@Mock
	private DStandingOrderConsentPayloadComparator dStandingOrderConsentsComparator;

	@Mock
	private DomesticStandingOrdersPaymentStagingAdapter dStandingOrderStagingAdapter;

	@Mock
	private PaymentConsentProcessingAdapter paymemtRequestAdapter;
	
	@Mock
	private RequestHeaderAttributes requestHeaderAttr;

	@InjectMocks
	private DomesticStandingOrderConsentServiceImpl service;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void contextLoads() {
	}

	@Test
	public void testCreateDomesticPaymentConsentResouceWithNullId() {
		PaymentConsentsPlatformResource resource = new PaymentConsentsPlatformResource();
		resource.setPaymentConsentId(null);
		resource.setCreatedAt("30/11/18");

		CustomDStandingOrderConsentsPOSTRequest customDomesticStandingOrderPOSTRequest = new CustomDStandingOrderConsentsPOSTRequest();
		customDomesticStandingOrderPOSTRequest.setCreatedOn("30/11/18");
		customDomesticStandingOrderPOSTRequest.setData(new OBWriteDataDomesticStandingOrderConsent1());
		customDomesticStandingOrderPOSTRequest.getData().setInitiation(new OBDomesticStandingOrder1());
		OBRisk1 requestRisk = new OBRisk1();
		requestRisk.setDeliveryAddress(new OBRisk1DeliveryAddress());
		requestRisk.getDeliveryAddress().buildingNumber("1");
		customDomesticStandingOrderPOSTRequest.setRisk(requestRisk);

		OBWriteDataDomesticStandingOrderConsentResponse1 obdata = new OBWriteDataDomesticStandingOrderConsentResponse1();
		CustomDStandingOrderConsentsPOSTResponse response = new CustomDStandingOrderConsentsPOSTResponse();
		response.setId("1234");
		response.setConsentProcessStatus(ProcessConsentStatusEnum.PASS);
		response.setData(obdata.consentId("45612"));
		response.setRisk(requestRisk);

		Mockito.when(paymemtRequestAdapter.preConsentProcessFlows(anyObject(), anyObject())).thenReturn(resource);

		Mockito.when(paymemtRequestAdapter.postConsentProcessFlows(anyObject(), anyObject(), anyObject(), anyObject()))
				.thenReturn(response);

		Map<String, String> paramMap = null;
		Mockito.when(dStandingOrderStagingAdapter.processStandingOrderConsents(anyObject(), anyObject(), anyObject(),
				anyObject(), anyObject())).thenReturn(response);

		service.createDomesticStandingOrderConsentResource(customDomesticStandingOrderPOSTRequest);
		assertEquals("45612", obdata.getConsentId());
	}

	@Test
	public void testCreateDomesticPaymentConsentResouceWithPaymentId() {
		PaymentConsentsPlatformResource resource = new PaymentConsentsPlatformResource();
		resource.setPaymentConsentId("1234");
		resource.setCreatedAt("30/11/18");

		OBWriteDataDomesticStandingOrderConsent1 data1 = new OBWriteDataDomesticStandingOrderConsent1();

		CustomDStandingOrderConsentsPOSTRequest customDomesticStandingOrderPOSTRequest = new CustomDStandingOrderConsentsPOSTRequest();
		customDomesticStandingOrderPOSTRequest.setCreatedOn("30/11/18");
		customDomesticStandingOrderPOSTRequest.setData(new OBWriteDataDomesticStandingOrderConsent1());
		customDomesticStandingOrderPOSTRequest.getData().setInitiation(new OBDomesticStandingOrder1());
		OBRisk1 requestRisk = new OBRisk1();
		requestRisk.setDeliveryAddress(new OBRisk1DeliveryAddress());
		requestRisk.getDeliveryAddress().buildingNumber("1");
		customDomesticStandingOrderPOSTRequest.setRisk(requestRisk);

		OBWriteDataDomesticStandingOrderConsentResponse1 obdata = new OBWriteDataDomesticStandingOrderConsentResponse1();
		CustomDStandingOrderConsentsPOSTResponse response = new CustomDStandingOrderConsentsPOSTResponse();
		response.setId("1234");
		response.setConsentProcessStatus(ProcessConsentStatusEnum.PASS);
		response.setData(obdata.consentId("45612"));
		response.setRisk(requestRisk);

		Mockito.when(paymemtRequestAdapter.preConsentProcessFlows(anyObject(), anyObject())).thenReturn(resource);

		Mockito.when(paymemtRequestAdapter.postConsentProcessFlows(anyObject(), anyObject(), anyObject(), anyObject()))
				.thenReturn(response);

		Map<String, String> paramMap = null;
		Mockito.when(dStandingOrderStagingAdapter.processStandingOrderConsents(anyObject(), anyObject(), anyObject(),
				anyObject(), anyObject())).thenReturn(response);

		Mockito.when(
				dStandingOrderStagingAdapter.retrieveStagedDomesticStandingOrdersConsents(anyObject(), anyObject()))
				.thenReturn(response);

		service.createDomesticStandingOrderConsentResource(customDomesticStandingOrderPOSTRequest);
		assertEquals("45612", obdata.getConsentId());
	}

	@Test(expected = PSD2Exception.class)
	public void testCreateDomesticPaymentConsentResouceWithPaymentIdExcpetion() {
		PaymentConsentsPlatformResource resource = new PaymentConsentsPlatformResource();
		resource.setPaymentConsentId("1234");
		resource.setCreatedAt("30/11/18");

		OBWriteDataDomesticStandingOrderConsent1 data1 = new OBWriteDataDomesticStandingOrderConsent1();

		CustomDStandingOrderConsentsPOSTRequest customDomesticStandingOrderPOSTRequest = new CustomDStandingOrderConsentsPOSTRequest();
		OBDomesticStandingOrder1 intition = new OBDomesticStandingOrder1();

		OBCashAccountDebtor3 ad = new OBCashAccountDebtor3();
		intition.setFrequency("test");
		ad.setName("debtorAccount");
		intition.setDebtorAccount(ad);
		data1.setInitiation(intition);

		customDomesticStandingOrderPOSTRequest.setCreatedOn("30/11/18");
		customDomesticStandingOrderPOSTRequest.setData(data1);

		OBWriteDataDomesticStandingOrderConsentResponse1 obdata = new OBWriteDataDomesticStandingOrderConsentResponse1();
		CustomDStandingOrderConsentsPOSTResponse response = new CustomDStandingOrderConsentsPOSTResponse();
		response.setId("1234");
		response.setConsentProcessStatus(ProcessConsentStatusEnum.PASS);
		response.setData(obdata.consentId("45612"));

		Mockito.when(paymemtRequestAdapter.preConsentProcessFlows(anyObject(), anyObject())).thenReturn(resource);

		Mockito.when(paymemtRequestAdapter.postConsentProcessFlows(anyObject(), anyObject(), anyObject(), anyObject()))
				.thenReturn(response);

		Map<String, String> paramMap = null;
		Mockito.when(dStandingOrderStagingAdapter.processStandingOrderConsents(anyObject(), anyObject(), anyObject(),
				anyObject(), anyObject())).thenReturn(response);

		Mockito.when(
				dStandingOrderStagingAdapter.retrieveStagedDomesticStandingOrdersConsents(anyObject(), anyObject()))
				.thenReturn(response);

		Mockito.when(
				dStandingOrderConsentsComparator.compareStandingOrderDetails(anyObject(), anyObject(), anyObject()))
				.thenReturn(1);

		service.createDomesticStandingOrderConsentResource(customDomesticStandingOrderPOSTRequest);

	}

	/*
	 * @Test public void
	 * testCreateandRetrieveDomesticPaymentConsentResouceWithPaymentId() {
	 * 
	 * PaymentConsentsPlatformResource resource = new
	 * PaymentConsentsPlatformResource(); resource.setPaymentConsentId("123456");
	 * resource.setCreatedAt("01/11/18");
	 * 
	 * Mockito.when(paymemtRequestAdapter.preConsentProcessFlows(anyObject(),
	 * anyObject())).thenReturn(resource);
	 * 
	 * CustomDPaymentConsentsPOSTResponse response = new
	 * CustomDPaymentConsentsPOSTResponse(); OBWriteDataDomesticConsentResponse1
	 * data = new OBWriteDataDomesticConsentResponse1();
	 * data.setStatus(OBExternalConsentStatus1Code.AWAITINGAUTHORISATION);
	 * data.setConsentId("45612"); response.setData(data); OBDomestic1 initiation =
	 * new OBDomestic1(); initiation.setLocalInstrument("test");
	 * OBCashAccountDebtor3 debtorAccount = new OBCashAccountDebtor3();
	 * debtorAccount.setName("debtorAccount");
	 * initiation.setDebtorAccount(debtorAccount); data.setInitiation(initiation);
	 * 
	 * Mockito.when(service.processDomesticPaymentConsents(anyObject(), anyObject(),
	 * anyMap(), Matchers.any(), Matchers.any())).thenReturn(response);
	 * 
	 * CustomDPaymentConsentsPOSTResponse response1 = new
	 * CustomDPaymentConsentsPOSTResponse(); OBWriteDataDomesticConsentResponse1
	 * data1 = new OBWriteDataDomesticConsentResponse1(); OBDomestic1 initiation1 =
	 * new OBDomestic1(); initiation1.setLocalInstrument("test");
	 * OBCashAccountDebtor3 debtorAccount1 = new OBCashAccountDebtor3();
	 * debtorAccount1.setName("debtorAccount");
	 * initiation.setDebtorAccount(debtorAccount1); data.setInitiation(initiation);
	 * data1.setConsentId("45612"); response1.setData(data1);
	 * 
	 * Mockito.when(dPaymentStagingAdapter.retrieveStagedDomesticPaymentConsents(
	 * anyObject(), anyMap())) .thenReturn(response1);
	 * 
	 * Mockito.when(dPaymentConsentsComparator.comparePaymentDetails(anyObject(),
	 * anyObject(), anyObject())) .thenReturn(0);
	 * 
	 * CustomDPaymentConsentsPOSTRequest request = new
	 * CustomDPaymentConsentsPOSTRequest();
	 * dpaymentconsentsserviceimpl.createDomesticPaymentConsentsResource(request);
	 * assertEquals("45612", data.getConsentId()); }
	 */

	@Test
	public void testRetrieveDomesticPaymentConsentsResource() {

		PaymentConsentsPlatformResource resource = new PaymentConsentsPlatformResource();
		resource.setSetupCmaVersion(PSD2Constants.CMAVERSION);

		Mockito.when(paymemtRequestAdapter.preConsentProcessGETFlow(anyObject())).thenReturn(resource);

		CustomDStandingOrderConsentsPOSTResponse response = new CustomDStandingOrderConsentsPOSTResponse();
		OBWriteDataDomesticStandingOrderConsentResponse1 data = new OBWriteDataDomesticStandingOrderConsentResponse1();
		data.setConsentId("45612");
		response.setData(data);

		Mockito.when(dStandingOrderStagingAdapter.retrieveStagedDomesticStandingOrdersConsents(anyObject(), anyMap()))
				.thenReturn(response);

		Mockito.when(
				paymemtRequestAdapter.postConsentProcessFlows(anyObject(), anyObject(), anyObject(), Matchers.any()))
				.thenReturn((CustomDStandingOrderConsentsPOSTResponse) response);

		PaymentRetrieveGetRequest retrieveRequest = new PaymentRetrieveGetRequest();
		retrieveRequest.setConsentId("45612");
		service.retrieveDomesticStandingOrderConsentsResource(retrieveRequest);
		assertEquals("45612", data.getConsentId());
	}

	@Test(expected = PSD2Exception.class)
	public void testRetrieveDomesticCustomDStandingOrderConsentsPOSTResponseNull() {

		PaymentConsentsPlatformResource resource = new PaymentConsentsPlatformResource();
		resource.setSetupCmaVersion(PSD2Constants.CMAVERSION);

		Mockito.when(paymemtRequestAdapter.preConsentProcessGETFlow(anyObject())).thenReturn(resource);

		CustomDStandingOrderConsentsPOSTResponse response = new CustomDStandingOrderConsentsPOSTResponse();
		OBWriteDataDomesticStandingOrderConsentResponse1 data = new OBWriteDataDomesticStandingOrderConsentResponse1();
		data.setConsentId("45612");
		response.setData(data);

		Mockito.when(dStandingOrderStagingAdapter.retrieveStagedDomesticStandingOrdersConsents(anyObject(), anyMap()))
				.thenReturn(null);

		Mockito.when(
				paymemtRequestAdapter.postConsentProcessFlows(anyObject(), anyObject(), anyObject(), Matchers.any()))
				.thenReturn((CustomDStandingOrderConsentsPOSTResponse) response);

		PaymentRetrieveGetRequest retrieveRequest = new PaymentRetrieveGetRequest();
		retrieveRequest.setConsentId("45612");
		service.retrieveDomesticStandingOrderConsentsResource(retrieveRequest);

	}
}
