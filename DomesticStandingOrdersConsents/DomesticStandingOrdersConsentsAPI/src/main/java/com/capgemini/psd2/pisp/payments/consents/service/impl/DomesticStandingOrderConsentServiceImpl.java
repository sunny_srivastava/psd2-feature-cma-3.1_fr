package com.capgemini.psd2.pisp.payments.consents.service.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMethod;

import com.capgemini.psd2.exceptions.ErrorMapKeys;
import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.domain.CustomDStandingOrderConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomDStandingOrderConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.CustomPaymentSetupPlatformDetails;
import com.capgemini.psd2.pisp.domain.OBExternalConsentStatus1Code;
import com.capgemini.psd2.pisp.domain.OBRisk1DeliveryAddress;
import com.capgemini.psd2.pisp.domain.PaymentConsentsPlatformResource;
import com.capgemini.psd2.pisp.domain.PaymentResponseInfo;
import com.capgemini.psd2.pisp.domain.PaymentRetrieveGetRequest;
import com.capgemini.psd2.pisp.enums.PaymentTypeEnum;
import com.capgemini.psd2.pisp.payments.consents.comparator.DStandingOrderConsentPayloadComparator;
import com.capgemini.psd2.pisp.payments.consents.service.DomesticStandingOrderConsentsService;
import com.capgemini.psd2.pisp.processing.adapter.service.PaymentConsentProcessingAdapter;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.pisp.stage.operations.adapter.DomesticStandingOrdersPaymentStagingAdapter;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Service
public class DomesticStandingOrderConsentServiceImpl implements DomesticStandingOrderConsentsService {

	@Autowired
	private DStandingOrderConsentPayloadComparator dStandingOrderConsentsComparator;

	@Autowired
	@Qualifier("dStandingOrderConsentsStagingRoutingAdapter")
	private DomesticStandingOrdersPaymentStagingAdapter dStandingOrderStagingAdapter;

	@SuppressWarnings("rawtypes")
	@Autowired
	private PaymentConsentProcessingAdapter paymemtRequestAdapter;

	@Autowired
	private RequestHeaderAttributes requestHeaderAttr;
	
	@Value("${cmaVersion}")
	private String cmaVersion;
	
	@SuppressWarnings("unchecked")
	@Override
	public CustomDStandingOrderConsentsPOSTResponse createDomesticStandingOrderConsentResource(
			CustomDStandingOrderConsentsPOSTRequest customDomesticStandingOrderPOSTRequest) {

		CustomDStandingOrderConsentsPOSTResponse paymentConsentsFoundationResponse;
		String requestType = PSD2Constants.PAYMENT_RESOURCE_IDEMPOTENT;

 		PaymentConsentsPlatformResource paymentSetupPlatformResource = paymemtRequestAdapter.preConsentProcessFlows(
				customDomesticStandingOrderPOSTRequest,
				populatePlatformDetails(customDomesticStandingOrderPOSTRequest));

		/* Non Idempotent Request */
		if (paymentSetupPlatformResource.getPaymentConsentId() == null) {
			requestType = PSD2Constants.PAYMENT_RESOURCE_NON_IDEMPOTENT;
			/* Create stage call to Adapter */
			paymentConsentsFoundationResponse = processStandingOrderStagingResource(
					customDomesticStandingOrderPOSTRequest, paymentSetupPlatformResource.getCreatedAt());
		}

		/* Idempotent Request */
		else
			paymentConsentsFoundationResponse = retrieveAndCompareDomesticStageResponse(
					customDomesticStandingOrderPOSTRequest, paymentSetupPlatformResource);

		PaymentResponseInfo stageDetails = populateStageDetails(paymentConsentsFoundationResponse);
		stageDetails.setRequestType(requestType);

		/* Fix for SIT issue #2272 
		 * Tpp providing Blank remittance info, and FS is returning null,
		 * Thus in such case returning same Blank remittance info to TPP.
		 * 27.02.2019 */
		OBRisk1DeliveryAddress reqRiskDeliveryAddress = customDomesticStandingOrderPOSTRequest.getRisk().getDeliveryAddress();
		paymentConsentsFoundationResponse.getRisk().setDeliveryAddress(reqRiskDeliveryAddress);
		
		return (CustomDStandingOrderConsentsPOSTResponse) paymemtRequestAdapter.postConsentProcessFlows(stageDetails,
				paymentConsentsFoundationResponse, paymentSetupPlatformResource, RequestMethod.POST.toString());
	}

	private CustomPaymentSetupPlatformDetails populatePlatformDetails(
			CustomDStandingOrderConsentsPOSTRequest customDomesticStandingOrderPOSTRequest) {
		CustomPaymentSetupPlatformDetails standingConsentsPlatformDetails = new CustomPaymentSetupPlatformDetails();
		standingConsentsPlatformDetails.setPaymentType(PaymentTypeEnum.DOMESTIC_ST_ORD);
		standingConsentsPlatformDetails.setSetupCmaVersion(cmaVersion);
		standingConsentsPlatformDetails = checkDebtorDetails(customDomesticStandingOrderPOSTRequest,
				standingConsentsPlatformDetails);
		return standingConsentsPlatformDetails;
	}

	public CustomPaymentSetupPlatformDetails checkDebtorDetails(
			CustomDStandingOrderConsentsPOSTRequest standingOrderConsentsRequest,
			CustomPaymentSetupPlatformDetails platformDetails) {
		String tppDebtorDetailsStatus = String.valueOf(Boolean.FALSE);
		String tppDebtorNameDetailsStatus = String.valueOf(Boolean.FALSE);
		if (standingOrderConsentsRequest.getData() != null
				&& standingOrderConsentsRequest.getData().getInitiation() != null&&standingOrderConsentsRequest.getData().getInitiation().getDebtorAccount() != null) {
			tppDebtorDetailsStatus = String.valueOf(Boolean.TRUE);
			if (!NullCheckUtils.isNullOrEmpty(
					standingOrderConsentsRequest.getData().getInitiation().getDebtorAccount().getName()))
				tppDebtorNameDetailsStatus = String.valueOf(Boolean.TRUE);
		}

		platformDetails.setTppDebtorDetails(tppDebtorDetailsStatus);
		platformDetails.setTppDebtorNameDetails(tppDebtorNameDetailsStatus);
		return platformDetails;
	}

	private CustomDStandingOrderConsentsPOSTResponse processStandingOrderStagingResource(
			CustomDStandingOrderConsentsPOSTRequest domesticPaymentConsentsRequest, String setupCreationDate) {

		/* new fields added */
		domesticPaymentConsentsRequest.setCreatedOn(setupCreationDate);

		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();
		stageIdentifiers.setPaymentSetupVersion(cmaVersion);
		stageIdentifiers.setPaymentTypeEnum(PaymentTypeEnum.DOMESTIC_ST_ORD);

		Map<String, String> paramMap = new HashMap<String, String>();
		paramMap.put(PSD2Constants.CO_RELATION_ID, requestHeaderAttr.getCorrelationId());
		return dStandingOrderStagingAdapter
				.processStandingOrderConsents(domesticPaymentConsentsRequest, stageIdentifiers, paramMap,
						OBExternalConsentStatus1Code.AWAITINGAUTHORISATION, OBExternalConsentStatus1Code.REJECTED);
	}

	private CustomDStandingOrderConsentsPOSTResponse retrieveAndCompareDomesticStageResponse(
			CustomDStandingOrderConsentsPOSTRequest paymentConsentsRequest,
			PaymentConsentsPlatformResource paymentConsentsPlatformResponse) {
		CustomDStandingOrderConsentsPOSTResponse paymentConsentsFoundationResponse;

		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();
		stageIdentifiers.setPaymentConsentId(paymentConsentsPlatformResponse.getPaymentConsentId());
		stageIdentifiers.setPaymentTypeEnum(PaymentTypeEnum.DOMESTIC_ST_ORD);
		stageIdentifiers.setPaymentSetupVersion(paymentConsentsPlatformResponse.getSetupCmaVersion());

		paymentConsentsFoundationResponse = dStandingOrderStagingAdapter
				.retrieveStagedDomesticStandingOrdersConsents(stageIdentifiers, null);

		if (dStandingOrderConsentsComparator.compareStandingOrderDetails(paymentConsentsFoundationResponse,
				paymentConsentsRequest, paymentConsentsPlatformResponse) > 0)
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_RESOURCE_CONSENTMISMATCH,
					ErrorMapKeys.RESOURCE_CONSENT_MISMATCH));

		return paymentConsentsFoundationResponse;
	}

	public PaymentResponseInfo populateStageDetails(CustomDStandingOrderConsentsPOSTResponse domesticStagingResource) {
		PaymentResponseInfo stageDetails = new PaymentResponseInfo();
		stageDetails.setConsentProcessStatus(domesticStagingResource.getConsentProcessStatus());
		stageDetails.setPaymentConsentId(domesticStagingResource.getData().getConsentId());
		return stageDetails;
	}

	@SuppressWarnings("unchecked")
	@Override
	public CustomDStandingOrderConsentsPOSTResponse retrieveDomesticStandingOrderConsentsResource(
			PaymentRetrieveGetRequest request) {
		request.setPaymentTypeEnum(PaymentTypeEnum.DOMESTIC_ST_ORD);
		PaymentConsentsPlatformResource paymentConsentsPlatformResponse = paymemtRequestAdapter
				.preConsentProcessGETFlow(request);

		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();
		stageIdentifiers.setPaymentConsentId(request.getConsentId());
		stageIdentifiers.setPaymentTypeEnum(PaymentTypeEnum.DOMESTIC_ST_ORD);
		stageIdentifiers.setPaymentSetupVersion(paymentConsentsPlatformResponse.getSetupCmaVersion());

		CustomDStandingOrderConsentsPOSTResponse paymentConsentsFoundationResponse = dStandingOrderStagingAdapter
				.retrieveStagedDomesticStandingOrdersConsents(stageIdentifiers, null);
		
		if(paymentConsentsFoundationResponse==null) {
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_RESOURCE_NOTFOUND,
					ErrorMapKeys.NO_PAYMENT_SETUP_RESOURCES_FOUND));
		}

		PaymentResponseInfo stageDetails = new PaymentResponseInfo();
		stageDetails.setRequestType(PSD2Constants.PAYMENT_RESOURCE_IDEMPOTENT);
		return (CustomDStandingOrderConsentsPOSTResponse) paymemtRequestAdapter.postConsentProcessFlows(stageDetails,
				paymentConsentsFoundationResponse, paymentConsentsPlatformResponse, RequestMethod.GET.toString());
	}

}
