package com.capgemini.psd2.pisp.standing.order.consent.routing.test.adapter.impl;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.anyString;
import java.util.HashMap;
import java.util.Map;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.domain.CustomDStandingOrderConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomDStandingOrderConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.OBExternalConsentStatus1Code;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.pisp.stage.operations.adapter.DomesticStandingOrdersPaymentStagingAdapter;
import com.capgemini.psd2.pisp.standing.order.consent.routing.adapter.impl.DStandingOrderConsentStagingRoutingAdapterImpl;
import com.capgemini.psd2.pisp.standing.order.consent.routing.adapter.routing.DStandingOrderConsentAdapterFactory;
import com.capgemini.psd2.token.Token;

@RunWith(SpringJUnit4ClassRunner.class)
public class DStandingOrderConsentStagingRoutingAdapterImplTest {

	@Mock
	private DStandingOrderConsentAdapterFactory factory;

	@Mock
	private RequestHeaderAttributes reqHeaderAtrributes;

	@InjectMocks
	private DStandingOrderConsentStagingRoutingAdapterImpl adapter;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void contextLoads() {
	}

	@Test
	public void processStandingOrderConsents() {
		CustomDStandingOrderConsentsPOSTRequest request = new CustomDStandingOrderConsentsPOSTRequest();
		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();
		Map<String, String> params = new HashMap<>();

		Token token = new Token();
		token.setSeviceParams(params);

		Mockito.when(reqHeaderAtrributes.getToken()).thenReturn(token);
		params.put(PSD2Constants.USER_IN_REQ_HEADER,
				reqHeaderAtrributes.getToken().getSeviceParams().get(PSD2Constants.CHANNEL_NAME));

		DomesticStandingOrdersPaymentStagingAdapter stagingAdapter = new DomesticStandingOrdersPaymentStagingAdapter() {

			@Override
			public CustomDStandingOrderConsentsPOSTResponse retrieveStagedDomesticStandingOrdersConsents(
					CustomPaymentStageIdentifiers customPaymentStageIdentifiers, Map<String, String> params) {
				return null;
			}

			@Override
			public CustomDStandingOrderConsentsPOSTResponse processStandingOrderConsents(
					CustomDStandingOrderConsentsPOSTRequest standingOrderConsentsRequest,
					CustomPaymentStageIdentifiers customStageIdentifiers, Map<String, String> params,
					OBExternalConsentStatus1Code successStatus, OBExternalConsentStatus1Code failureStatus) {
				return null;
			}
		};

		Mockito.when(factory.getDomesticStandingOrdersSetupStagingInstance(anyString())).thenReturn(stagingAdapter);
		adapter.processStandingOrderConsents(request, stageIdentifiers, params, OBExternalConsentStatus1Code.AUTHORISED,
				OBExternalConsentStatus1Code.REJECTED);
		assertNotNull(stagingAdapter);
	}

	@Test
	public void retrieveStagedDomesticStandingOrdersConsents() {
		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();

		Map<String, String> params = new HashMap<>();

		Token token = new Token();
		token.setSeviceParams(params);

		DomesticStandingOrdersPaymentStagingAdapter stagingAdapter = new DomesticStandingOrdersPaymentStagingAdapter() {

			@Override
			public CustomDStandingOrderConsentsPOSTResponse retrieveStagedDomesticStandingOrdersConsents(
					CustomPaymentStageIdentifiers customPaymentStageIdentifiers, Map<String, String> params) {
				return null;
			}

			@Override
			public CustomDStandingOrderConsentsPOSTResponse processStandingOrderConsents(
					CustomDStandingOrderConsentsPOSTRequest standingOrderConsentsRequest,
					CustomPaymentStageIdentifiers customStageIdentifiers, Map<String, String> params,
					OBExternalConsentStatus1Code successStatus, OBExternalConsentStatus1Code failureStatus) {
				return null;
			}
		};

		Mockito.when(factory.getDomesticStandingOrdersSetupStagingInstance(anyString())).thenReturn(stagingAdapter);
		adapter.retrieveStagedDomesticStandingOrdersConsents(stageIdentifiers, params);
		assertNotNull(params);
	}

	@Test
	public void retrieveStagedDomesticStandingOrdersConsentsNullParams() {
		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();
		Map<String, String> params = null;

		DomesticStandingOrdersPaymentStagingAdapter stagingAdapter = new DomesticStandingOrdersPaymentStagingAdapter() {

			@Override
			public CustomDStandingOrderConsentsPOSTResponse retrieveStagedDomesticStandingOrdersConsents(
					CustomPaymentStageIdentifiers customPaymentStageIdentifiers, Map<String, String> params) {
				return null;
			}

			@Override
			public CustomDStandingOrderConsentsPOSTResponse processStandingOrderConsents(
					CustomDStandingOrderConsentsPOSTRequest standingOrderConsentsRequest,
					CustomPaymentStageIdentifiers customStageIdentifiers, Map<String, String> params,
					OBExternalConsentStatus1Code successStatus, OBExternalConsentStatus1Code failureStatus) {
				return null;
			}
		};

		Mockito.when(factory.getDomesticStandingOrdersSetupStagingInstance(anyString())).thenReturn(stagingAdapter);
		adapter.retrieveStagedDomesticStandingOrdersConsents(stageIdentifiers, params);
		assertNull(params);
	}

	@After
	public void tearDown() throws Exception {
		factory = null;
		reqHeaderAtrributes = null;
		adapter = null;
	}

}
