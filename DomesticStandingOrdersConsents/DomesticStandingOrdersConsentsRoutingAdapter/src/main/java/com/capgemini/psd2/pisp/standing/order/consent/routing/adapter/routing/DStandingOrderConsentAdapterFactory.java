package com.capgemini.psd2.pisp.standing.order.consent.routing.adapter.routing;

import com.capgemini.psd2.pisp.stage.operations.adapter.DomesticStandingOrdersPaymentStagingAdapter;

public interface DStandingOrderConsentAdapterFactory {
	
	public DomesticStandingOrdersPaymentStagingAdapter getDomesticStandingOrdersSetupStagingInstance(String coreSystemName);

}
