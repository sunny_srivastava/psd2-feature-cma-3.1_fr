package com.capgemini.psd2.pisp.standing.order.consent.routing.adapter.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.domain.CustomDStandingOrderConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomDStandingOrderConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.OBExternalConsentStatus1Code;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.pisp.stage.operations.adapter.DomesticStandingOrdersPaymentStagingAdapter;
import com.capgemini.psd2.pisp.standing.order.consent.routing.adapter.routing.DStandingOrderConsentAdapterFactory;
import com.capgemini.psd2.utilities.NullCheckUtils;

/**
 * Hello world!
 *
 */
@Component("dStandingOrderConsentsStagingRoutingAdapter")
public class DStandingOrderConsentStagingRoutingAdapterImpl implements DomesticStandingOrdersPaymentStagingAdapter {

	private DomesticStandingOrdersPaymentStagingAdapter beanInstance;

	/** The factory. */
	@Autowired
	private DStandingOrderConsentAdapterFactory factory;

	@Autowired
	private RequestHeaderAttributes reqHeaderAtrributes;

	/** The default adapter. */
	@Value("${app.paymentConsentStagingAdapter}")
	private String paymentConsentStagingAdapter;

	@Override
	public CustomDStandingOrderConsentsPOSTResponse processStandingOrderConsents(
			CustomDStandingOrderConsentsPOSTRequest standingOrderConsentsRequest,
			CustomPaymentStageIdentifiers customStageIdentifiers, Map<String, String> params,
			OBExternalConsentStatus1Code successStatus, OBExternalConsentStatus1Code failureStatus) {

		return getDomesticRoutingAdapter().processStandingOrderConsents(standingOrderConsentsRequest,
				customStageIdentifiers, addHeaderParams(params), null, null);
	}

	@Override
	public CustomDStandingOrderConsentsPOSTResponse retrieveStagedDomesticStandingOrdersConsents(
			CustomPaymentStageIdentifiers customPaymentStageIdentifiers, Map<String, String> params) {

		return getDomesticRoutingAdapter().retrieveStagedDomesticStandingOrdersConsents(customPaymentStageIdentifiers,
				addHeaderParams(params));
	}

	private DomesticStandingOrdersPaymentStagingAdapter getDomesticRoutingAdapter() {
		if (beanInstance == null)
			beanInstance = factory.getDomesticStandingOrdersSetupStagingInstance(paymentConsentStagingAdapter);
		return beanInstance;
	}

	private Map<String, String> addHeaderParams(Map<String, String> params) {
		if (NullCheckUtils.isNullOrEmpty(params))
			params = new HashMap<>(); //NOSONAR

		if (null != reqHeaderAtrributes.getToken() && null != reqHeaderAtrributes.getToken().getSeviceParams()) {
			params.put(PSD2Constants.CHANNEL_IN_REQ_HEADER,
					reqHeaderAtrributes.getToken().getSeviceParams().get(PSD2Constants.CHANNEL_NAME));
		}
		params.put(PSD2Constants.USER_IN_REQ_HEADER, reqHeaderAtrributes.getPsuId());
		params.put(PSD2Constants.CORRELATION_REQ_HEADER, reqHeaderAtrributes.getCorrelationId());
		params.put(PSD2Constants.TENANT_ID, reqHeaderAtrributes.getTenantId());
		return params;
	}
}
