package com.capgemini.psd2.pisp.standing.order.mongodb.adapter.impl;

import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Conditional;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.conditions.MongoDbMockCondition;
import com.capgemini.psd2.exceptions.ErrorMapKeys;
import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.InternalServerErrorMessage;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.domain.CustomDStandingOrderConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomDStandingOrderConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.OBCharge1;
import com.capgemini.psd2.pisp.domain.OBExternalConsentStatus1Code;
import com.capgemini.psd2.pisp.enums.ProcessConsentStatusEnum;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.pisp.stage.operations.adapter.DomesticStandingOrdersPaymentStagingAdapter;
import com.capgemini.psd2.pisp.standing.order.mongodb.adapter.repository.DStandingOrderConsentFoundationRepository;
import com.capgemini.psd2.pisp.utilities.PispUtilities;
import com.capgemini.psd2.utilities.JSONUtilities;
import com.capgemini.psd2.utilities.SandboxValidationUtility;

@Conditional(MongoDbMockCondition.class)
@Component("dStandingOrderConsentsStagingMongoDbAdapter")
public class DStandingOrderConsentStagingMongoDbAdapterImpl implements DomesticStandingOrdersPaymentStagingAdapter {

	private static final Logger LOG = LoggerFactory.getLogger(DStandingOrderConsentStagingMongoDbAdapterImpl.class);
	
	@Autowired
	private DStandingOrderConsentFoundationRepository dStandingOrderConsentsBankRepository;

	@Autowired
	private SandboxValidationUtility utility;

	@Autowired
	private RequestHeaderAttributes reqAttributes;

	@Value("${app.isSandboxEnabled:false}")
	private boolean isSandboxEnabled;
	
	@Override
	public CustomDStandingOrderConsentsPOSTResponse processStandingOrderConsents(
			CustomDStandingOrderConsentsPOSTRequest customRequest, CustomPaymentStageIdentifiers customStageIdentifiers,
			Map<String, String> params, OBExternalConsentStatus1Code successStatus,
			OBExternalConsentStatus1Code failureStatus) {

		CustomDStandingOrderConsentsPOSTResponse paymentConsentsBankResource = transformDomesticStandingOrderConsentsToBankResource(
				customRequest);

		String currency = customRequest.getData().getInitiation().getFirstPaymentAmount().getCurrency();
		String initiationAmount = customRequest.getData().getInitiation().getFirstPaymentAmount().getAmount();

		if (utility.isValidAmount(initiationAmount, PSD2Constants.SANDBOX_PISP_POST_MOCKING)) {
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
					InternalServerErrorMessage.SANDBOX_MOCKED_ERROR));
		}

		// Sandbox Secondary Identification Validation
		String secondaryIdentification = customRequest.getData().getInitiation().getCreditorAccount()
				.getSecondaryIdentification();
		if (utility.isValidSecIdentification(secondaryIdentification)) {
			throw PSD2Exception.populatePSD2Exception(
					new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNSUPPORTED_ACCOUNTSECONDARYIDENTIFIER,
							ErrorMapKeys.ACCOUNT_SECONDARYIDENTIFIER_INVALID));
		}

		String consentId = null;
		String cutOffDateTime = utility.getMockedCutOffDtTm();
		
		ProcessConsentStatusEnum processConsentStatusEnum = utility.getMockedConsentProcessExecStatus(currency,
				initiationAmount);

		if (processConsentStatusEnum == ProcessConsentStatusEnum.PASS
				|| processConsentStatusEnum == ProcessConsentStatusEnum.FAIL)
			consentId = UUID.randomUUID().toString();

		paymentConsentsBankResource.getData().setCreationDateTime(customRequest.getCreatedOn());
		paymentConsentsBankResource.getData().setConsentId(consentId);
		
		// removing charges block as this is not returned for BOI on domestic payment APIs
		if(!isSandboxEnabled){
			List<OBCharge1> charges = utility.getMockedOBChargeList(currency, initiationAmount, null);
			paymentConsentsBankResource.getData().setCharges(charges);
		}
		
		paymentConsentsBankResource.getData().setCutOffDateTime(cutOffDateTime);
		paymentConsentsBankResource.setConsentProcessStatus(processConsentStatusEnum);

		if (consentId != null) {
			OBExternalConsentStatus1Code status = calculateCMAStatus(processConsentStatusEnum, successStatus,
					failureStatus);
			paymentConsentsBankResource.getData().setStatus(status);
			paymentConsentsBankResource.getData().setStatusUpdateDateTime(customRequest.getCreatedOn());
		}

		try {
			dStandingOrderConsentsBankRepository.save(paymentConsentsBankResource);
		} catch (DataAccessResourceFailureException exception) {
			LOG.info("DataAccessResourceFailureException in processStandingOrderConsents:"+exception);
			throw PSD2Exception.populatePSD2Exception(
					new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR, exception.getMessage()));
		}
		return paymentConsentsBankResource;
	}

	@Override
	public CustomDStandingOrderConsentsPOSTResponse retrieveStagedDomesticStandingOrdersConsents(
			CustomPaymentStageIdentifiers customPaymentStageIdentifiers, Map<String, String> params) {
		try {
			String paymentConsentId = customPaymentStageIdentifiers.getPaymentConsentId();

			CustomDStandingOrderConsentsPOSTResponse paymentBankResource;

			paymentBankResource = dStandingOrderConsentsBankRepository.findOneByDataConsentId(paymentConsentId);

			String initiationAmount = null;
			if (paymentBankResource != null && paymentBankResource.getData() != null
					&& paymentBankResource.getData().getInitiation() != null
					&& paymentBankResource.getData().getInitiation().getFirstPaymentAmount() != null) {
				initiationAmount = paymentBankResource.getData().getInitiation().getFirstPaymentAmount().getAmount();
			}
			
			if ("GET".equals(reqAttributes.getMethodType())
					&& (utility.isValidAmount(initiationAmount, PSD2Constants.SANDBOX_PISP_GET_MOCKING))) {
				throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
						InternalServerErrorMessage.SANDBOX_MOCKED_ERROR));
			}
			
			return paymentBankResource;

		} catch (DataAccessResourceFailureException exception) {
			LOG.info("DataAccessResourceFailureException in retrieveStagedDomesticStandingOrdersConsents:"+exception);
			throw PSD2Exception.populatePSD2Exception(
					new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR, exception.getMessage()));
		}
	}

	private CustomDStandingOrderConsentsPOSTResponse transformDomesticStandingOrderConsentsToBankResource(
			CustomDStandingOrderConsentsPOSTRequest paymentConsentsRequest) {
		String paymentFoundationRequest = JSONUtilities.getJSONOutPutFromObject(paymentConsentsRequest);
		return JSONUtilities.getObjectFromJSONString(PispUtilities.getObjectMapper(), paymentFoundationRequest,
				CustomDStandingOrderConsentsPOSTResponse.class);
	}

	private OBExternalConsentStatus1Code calculateCMAStatus(ProcessConsentStatusEnum processConsentStatusEnum,
			OBExternalConsentStatus1Code successStatus, OBExternalConsentStatus1Code failureStatus) {
		return (processConsentStatusEnum == ProcessConsentStatusEnum.PASS) ? successStatus : failureStatus;
	}
}
