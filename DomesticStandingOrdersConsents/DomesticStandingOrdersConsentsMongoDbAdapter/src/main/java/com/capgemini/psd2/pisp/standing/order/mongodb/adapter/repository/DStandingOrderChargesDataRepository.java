package com.capgemini.psd2.pisp.standing.order.mongodb.adapter.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.capgemini.psd2.pisp.domain.OBCharge1;

public interface DStandingOrderChargesDataRepository extends MongoRepository<OBCharge1, String>{

}
