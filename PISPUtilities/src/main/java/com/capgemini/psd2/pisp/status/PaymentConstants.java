package com.capgemini.psd2.pisp.status;

public class PaymentConstants {
	
	public static final String PAYMENT_TYPE = "paymentType";
	public static final String SETUP_CMA_VERSION = "setupCMAVersion";
	public static final String CMA_FIRST_VERSION = "1.1";
	public static final String CMA_THIRD_VERSION = "3.0";
	public static final String INCOMPLETE = "Incomplete";
	public static final String COMPLETED = "Completed";
	public static final String ABORT = "Abort";
	public static final String FAIL = "Fail";
	public static final String PASS = "Pass";
	public static final String INITIAL = "initial";
	public static final String PASS_M_AUTH = "Pass-MAuthrised";
	public static final String PASS_M_AWAIT = "Pass-MAwaitingAuthrise";
	public static final String PASS_M_REJECT = "Pass-MRejected";
	public static final String SUBMISSION = "submission";
	public static final String CONSENT = "consent";
}
