package com.capgemini.psd2.pisp.status;

public enum PaymentStatusCompatibilityMapEnum {

	PENDING("AwaitingAuthorisation"),
	ACCEPTEDTECHNICALVALIDATION("AwaitingAuthorisation"),
	ACCEPTEDCUSTOMERPROFILE("Authorised"),
	ACCEPTEDSETTLEMENTINPROCESS("AcceptedSettlementInProcess"),
	REJECTED("Rejected"),

	AWAITINGAUTHORISATION("AwaitingAuthorisation"),
	AUTHORISED("Authorised"),
	CONSUMED("Consumed"),
	ACCEPTEDSETTLEMENTCOMPLETED("AcceptedSettlementCompleted"),
	AWAITINGUPLOAD("AwaitingUpload");
	
	private String compatibleStatus;

	PaymentStatusCompatibilityMapEnum(String compatibleStatus){
		this.compatibleStatus=compatibleStatus;
	}
	
	public String getCompatibleStatus() {
		return compatibleStatus;
	}
}
