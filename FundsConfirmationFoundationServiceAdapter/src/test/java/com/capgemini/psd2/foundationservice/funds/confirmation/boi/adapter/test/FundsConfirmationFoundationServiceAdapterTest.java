package com.capgemini.psd2.foundationservice.funds.confirmation.boi.adapter.test;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyMap;
import static org.mockito.Matchers.anyObject;

import java.time.OffsetDateTime;
import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.aisp.domain.OBAccount6Account;
import com.capgemini.psd2.aisp.domain.OBExternalAccountSubType1Code;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmation1;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmation1Data;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmation1DataInstructedAmount;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmationResponse1;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.foundationservice.funds.confirmation.boi.adapter.FundsConfirmationFoundationServiceAdapter;
import com.capgemini.psd2.foundationservice.funds.confirmation.boi.adapter.client.FundsConfirmationFoundationServiceClient;
import com.capgemini.psd2.foundationservice.funds.confirmation.boi.adapter.constants.FundsConfirmationFoundationServiceConstants;
import com.capgemini.psd2.foundationservice.funds.confirmation.boi.adapter.delegate.FundsConfirmationFoundationServiceDelegate;
import com.capgemini.psd2.foundationservice.funds.confirmation.boi.adapter.raml.domain.AvailableFunds;
import com.capgemini.psd2.foundationservice.funds.confirmation.boi.adapter.raml.domain.AvailableFundsForWithdrawalAmount;
import com.capgemini.psd2.foundationservice.funds.confirmation.boi.adapter.raml.domain.Currency;
import com.capgemini.psd2.foundationservice.funds.confirmation.boi.adapter.raml.domain.FundsCheckStatus;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.impl.RestClientSyncImpl;

@RunWith(SpringJUnit4ClassRunner.class)
public class FundsConfirmationFoundationServiceAdapterTest {

	@InjectMocks
	private FundsConfirmationFoundationServiceAdapter fundsconfirmationFoundationServiceAdapter;

	@Mock
	private FundsConfirmationFoundationServiceClient fundsconfirmationFoundationServiceClient;

	@Mock
	private RestClientSyncImpl restClient;

	// ** The Funds Confirmation Foundation Service Delegate. *//*
	@Mock
	private FundsConfirmationFoundationServiceDelegate fundsconfirmationFoundationServiceDelegate;

	OBFundsConfirmation1 obFundsConfirmation1 = new OBFundsConfirmation1();

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void contextLoads() {
	}

//	@Test
	public void testCurrentAccountFundsConfirmation() {

		AvailableFunds availableFunds = new AvailableFunds();

		Currency accountCurrency = new Currency();
		accountCurrency.setCurrencyName("Euro");
		accountCurrency.setIsoAlphaCode("EUR");
		availableFunds.setAccountCurrency(accountCurrency);

		availableFunds.setAccountNumber("11111111");
		OffsetDateTime date = OffsetDateTime.now();
		availableFunds.setAvailableFundsCheckDatetime(date);
		availableFunds.setParentNationalSortCodeNSCNumber("111111");
		availableFunds.setFundsCheckStatus(FundsCheckStatus.COMPLETED);
		availableFunds.setFundsCheckStatusReasonDescription(null);

		AvailableFundsForWithdrawalAmount availableFundsForWithdrawalAmount = new AvailableFundsForWithdrawalAmount();
		availableFundsForWithdrawalAmount.setTransactionCurrency(1000.0d);
		availableFunds.setAvailableFundsForWithdrawalAmount(availableFundsForWithdrawalAmount);

		AccountDetails accDet = new AccountDetails();
		OBAccount6Account account = new OBAccount6Account();
		accDet.setAccountSubType(OBExternalAccountSubType1Code.CURRENTACCOUNT);
		accDet.setAccountId("11111111");
		accDet.setAccountNSC("111111");
		accDet.setAccountNumber("111111");
		account.setName("credit");
		account.setSecondaryIdentification("12345678");
		account.setIdentification("XXXXXXXXXXXX2000");
		accDet.setAccount(account);

		OBFundsConfirmation1DataInstructedAmount instructedAmount = new OBFundsConfirmation1DataInstructedAmount();
		instructedAmount.setAmount("500");
		instructedAmount.setCurrency("EUR");

		OBFundsConfirmation1Data data = new OBFundsConfirmation1Data();
		data.setConsentId("6bf54aa2-d1d0-4f49-badf-40694f78743e");
		data.setInstructedAmount(instructedAmount);
		data.setReference("sdfgdfg");
		assertNotNull(data);

		obFundsConfirmation1.setData(data);

		Mockito.when(fundsconfirmationFoundationServiceDelegate.getFoundationServiceURL(any(), any(), any(), any()))
				.thenReturn("http://localhost:9011/core-banking/p/abt/v1.0/accounts/111111/11111111/available-funds");
		Mockito.when(fundsconfirmationFoundationServiceClient.restTransportForFundsConfirmation(any(), any(),any(),any()))
				.thenReturn(availableFunds);
		Mockito.when(fundsconfirmationFoundationServiceDelegate.transformResponseFromFDToAPI(any(), any(), any()))
				.thenReturn(new OBFundsConfirmationResponse1());

		Map<String, String> params = new HashMap<String, String>();
		params.put(FundsConfirmationFoundationServiceConstants.ACCOUNT_SUBTYPE, "CurrentAccount");
		fundsconfirmationFoundationServiceAdapter.getFundsData(accDet, obFundsConfirmation1, params);
	}

	//@Test
	public void testSavingsFundsConfirmationFS3() {

		AvailableFunds availableFunds = new AvailableFunds();

		Currency accountCurrency = new Currency();
		accountCurrency.setCurrencyName("Euro");
		accountCurrency.setIsoAlphaCode("EUR");
		availableFunds.setAccountCurrency(accountCurrency);

		availableFunds.setAccountNumber("11111111");
		OffsetDateTime date = OffsetDateTime.now();
		availableFunds.setAvailableFundsCheckDatetime(date);
		availableFunds.setParentNationalSortCodeNSCNumber("111111");
		availableFunds.setFundsCheckStatus(FundsCheckStatus.COMPLETED);
		availableFunds.setFundsCheckStatusReasonDescription(null);

		AvailableFundsForWithdrawalAmount availableFundsForWithdrawalAmount = new AvailableFundsForWithdrawalAmount();
		availableFundsForWithdrawalAmount.setTransactionCurrency(1000.0d);
		availableFunds.setAvailableFundsForWithdrawalAmount(availableFundsForWithdrawalAmount);

		AccountDetails accDet = new AccountDetails();
		OBAccount6Account  account = new OBAccount6Account();
		accDet.setAccountSubType(OBExternalAccountSubType1Code.SAVINGS);
		accDet.setAccountId("11111111");
		accDet.setAccountNSC("111111");
		accDet.setAccountNumber("111111");
		account.setName("credit");
		account.setSecondaryIdentification("12345678");
		account.setIdentification("XXXXXXXXXXXX2000");
		accDet.setAccount(account);

		OBFundsConfirmation1DataInstructedAmount instructedAmount = new OBFundsConfirmation1DataInstructedAmount();
		instructedAmount.setAmount("500");
		instructedAmount.setCurrency("EUR");

		OBFundsConfirmation1Data data = new OBFundsConfirmation1Data();
		data.setConsentId("6bf54aa2-d1d0-4f49-badf-40694f78743e");
		data.setInstructedAmount(instructedAmount);
		data.setReference("sdfgdfg");
		assertNotNull(data);

		obFundsConfirmation1.setData(data);

		Mockito.when(fundsconfirmationFoundationServiceDelegate.getFoundationServiceURL(any(), any(), any(), any()))
				.thenReturn("http://localhost:9011/core-banking/p/abt/v1.0/accounts/111111/11111111/available-funds");
		Mockito.when(fundsconfirmationFoundationServiceClient.restTransportForFundsConfirmation(any(),any(), any(), any()))
				.thenReturn(availableFunds);
		Mockito.when(fundsconfirmationFoundationServiceDelegate.transformResponseFromFDToAPI(any(), any(), any()))
				.thenReturn(new OBFundsConfirmationResponse1());

		Map<String, String> params = new HashMap<String, String>();
		params.put(FundsConfirmationFoundationServiceConstants.ACCOUNT_SUBTYPE, "Savings");
		fundsconfirmationFoundationServiceAdapter.getFundsData(accDet, obFundsConfirmation1, params);
	}

	@SuppressWarnings("unchecked")
	@Test
	public void testcreateFundsConfirmationRequestHeaders() {
		AccountDetails accDet = new AccountDetails();
		accDet.setAccountSubType(OBExternalAccountSubType1Code.SAVINGS);
		accDet.setAccountId("11111111");
		accDet.setAccountNSC("11111111");
		accDet.setAccountNumber("11111111");

		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.add("x-api-transaction-id", "42c010af-8179-4132-b1db-30030e0b0389");
		httpHeaders.add("x-api-correlation-id", "6842080c-5322-4e80-9c99-a607c6b5abc0");
		httpHeaders.add("x-api-source-system", "PSD2API");
		httpHeaders.add("x-api-source-user", "88888888");
		httpHeaders.add("x-api-channel-Code", "BOIID");
		httpHeaders.add("x-api-party-source-id-number", "BOI");
		assertNotNull(httpHeaders);

		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl("http://localhost:9011/core-banking/p/abt/v1.0/accounts/111111/11111111/available-funds");
		Mockito.when(fundsconfirmationFoundationServiceDelegate.createFundsConfirmationRequestHeaders(anyObject(),
				anyObject(), anyMap())).thenReturn(httpHeaders);
		Map<String, String> params = new HashMap<>();
		params.put("channelId", "channel123");
		httpHeaders = fundsconfirmationFoundationServiceDelegate.createFundsConfirmationRequestHeaders(requestInfo,
				accDet, params);
	}

//	@Test(expected = AdapterException.class)
	public void testAccountNullCheckException() {
		/*
		 * AccountMapping accountMapping = new AccountMapping(); Map<String, String>
		 * params = new HashMap<>(); accountMapping.setPsuId("test");
		 * accountMapping.setCorrelationId("test");
		 */
		AccountDetails acc = new AccountDetails();
		Map<String, String> params = new HashMap<>();
		acc.setAccountId("111111");
		acc.setAccountNSC("111111");
		acc.setAccountNumber("111111");
		fundsconfirmationFoundationServiceAdapter.getFundsData(acc, obFundsConfirmation1, params);
	}

	@Test(expected = AdapterException.class)
	public void testFundsConfirmationNullCheckException() {
		AccountDetails acc = new AccountDetails();
		Map<String, String> params = new HashMap<>();
		acc.setAccountId("12345");
		acc.setAccountNSC("nsc1234");
		acc.setAccountNumber("acct1234");
		OBFundsConfirmation1 oBFundsConfirmation1 = null;
		Mockito.when(fundsconfirmationFoundationServiceClient.restTransportForFundsConfirmation(anyObject(),
				anyObject(),anyObject(), anyObject())).thenReturn(null);
		fundsconfirmationFoundationServiceAdapter.getFundsData(acc, oBFundsConfirmation1, params);
	}

	@Test(expected = AdapterException.class)
	public void testParamsNullCheckException() {
		AccountDetails acc = new AccountDetails();
		Map<String, String> params = null;
		acc.setAccountId("12345");
		acc.setAccountNSC("nsc1234");
		acc.setAccountNumber("acct1234");
		Mockito.when(fundsconfirmationFoundationServiceClient.restTransportForFundsConfirmation(anyObject(),
				anyObject(),anyObject(), anyObject())).thenReturn(null);
		fundsconfirmationFoundationServiceAdapter.getFundsData(acc, obFundsConfirmation1, params);
	}

//	@Test(expected = AdapterException.class)
	public void testExceptionNullCheckUtil() {
		AccountDetails acc = new AccountDetails();
		Map<String, String> params = new HashMap<>();
		acc.setAccountId(null);
		Mockito.when(fundsconfirmationFoundationServiceClient.restTransportForFundsConfirmation(anyObject(),
				anyObject(),anyObject(), anyObject())).thenReturn(null);
		fundsconfirmationFoundationServiceAdapter.getFundsData(acc, obFundsConfirmation1, params);
	}

	@Test(expected = AdapterException.class)
	public void testExceptionNullAccountDetails() {
		AccountDetails accountDet = null;
		Map<String, String> params = new HashMap<>();
		Mockito.when(fundsconfirmationFoundationServiceClient.restTransportForFundsConfirmation(anyObject(),
				anyObject(),anyObject(), anyObject())).thenReturn(null);
		fundsconfirmationFoundationServiceAdapter.getFundsData(accountDet, obFundsConfirmation1, params);
	}

//	@Test(expected=AdapterException.class)
		public void testAvailableFundsCheckForNull() {
	 		
	 		AvailableFunds availableFunds = new AvailableFunds();
	 		
	 		Currency accountCurrency = new Currency();
	 		accountCurrency.setCurrencyName("Euro");
	 		accountCurrency.setIsoAlphaCode("EUR");
	 		availableFunds.setAccountCurrency(accountCurrency);
	 		
	 		availableFunds.setAccountNumber("11111111");
	 		OffsetDateTime date = OffsetDateTime.now();
	 		availableFunds.setAvailableFundsCheckDatetime(date);
	 		availableFunds.setParentNationalSortCodeNSCNumber("111111");
	 		availableFunds.setFundsCheckStatus(FundsCheckStatus.COMPLETED);
	 		availableFunds.setFundsCheckStatusReasonDescription(null);
	 		
	 		AvailableFundsForWithdrawalAmount availableFundsForWithdrawalAmount = new AvailableFundsForWithdrawalAmount();
	 		availableFundsForWithdrawalAmount.setTransactionCurrency(1000.0d);
	 		availableFunds.setAvailableFundsForWithdrawalAmount(availableFundsForWithdrawalAmount);

	 		
	 		AccountDetails accDet = new AccountDetails();
	 		OBAccount6Account  account= new OBAccount6Account();
	 		accDet.setAccountSubType(OBExternalAccountSubType1Code.SAVINGS);
	 		accDet.setAccountId("11111111");
	 		accDet.setAccountNSC("111111");
	 		accDet.setAccountNumber("111111");
	 		account.setName("credit");
	 		account.setSecondaryIdentification("12345678");
	 		account.setIdentification("XXXXXXXXXXXX2000");
	 		accDet.setAccount(account);
	 		
	 		OBFundsConfirmation1DataInstructedAmount instructedAmount = new OBFundsConfirmation1DataInstructedAmount();
	 		instructedAmount.setAmount("500");
	 		instructedAmount.setCurrency("EUR");
	 		
	 		OBFundsConfirmation1Data data = new OBFundsConfirmation1Data();
	 		data.setConsentId("6bf54aa2-d1d0-4f49-badf-40694f78743e");
	 		data.setInstructedAmount(instructedAmount);
	 		data.setReference("sdfgdfg");
	 		
	 		obFundsConfirmation1.setData(data);

	 		Mockito.when(fundsconfirmationFoundationServiceDelegate.getFoundationServiceURL(any(), any(), any(), any()))
	 				.thenReturn("http://localhost:9011/core-banking/p/abt/v1.0/accounts/111111/11111111/available-funds");
	 		Mockito.when(fundsconfirmationFoundationServiceClient.restTransportForFundsConfirmation(any(),any(), any(), any()))
	 				.thenReturn(null);
	 		Map<String, String> params = new HashMap<String, String>();
			params.put(FundsConfirmationFoundationServiceConstants.ACCOUNT_SUBTYPE, "CurrentAccount");
	 		fundsconfirmationFoundationServiceAdapter.getFundsData(accDet, obFundsConfirmation1, params);
	}
	
	@Test(expected = AdapterException.class)
	public void testSavingsFundsConfirmationCreditCardException() {

		AvailableFunds availableFunds = new AvailableFunds();

		Currency accountCurrency = new Currency();
		accountCurrency.setCurrencyName("Euro");
		accountCurrency.setIsoAlphaCode("EUR");
		availableFunds.setAccountCurrency(accountCurrency);

		availableFunds.setAccountNumber("11111111");
		OffsetDateTime date = OffsetDateTime.now();
		availableFunds.setAvailableFundsCheckDatetime(date);
		availableFunds.setParentNationalSortCodeNSCNumber("111111");
		availableFunds.setFundsCheckStatus(FundsCheckStatus.COMPLETED);
		availableFunds.setFundsCheckStatusReasonDescription(null);

		AvailableFundsForWithdrawalAmount availableFundsForWithdrawalAmount = new AvailableFundsForWithdrawalAmount();
		availableFundsForWithdrawalAmount.setTransactionCurrency(1000.0d);
		availableFunds.setAvailableFundsForWithdrawalAmount(availableFundsForWithdrawalAmount);

		AccountDetails accDet = new AccountDetails();
		OBAccount6Account  account = new OBAccount6Account();
		accDet.setAccountSubType(OBExternalAccountSubType1Code.CREDITCARD);
		accDet.setAccountId("11111111");
		accDet.setAccountNSC("111111");
		accDet.setAccountNumber("111111");
		account.setName("credit");
		account.setSecondaryIdentification("12345678");
		account.setIdentification("XXXXXXXXXXXX2000");
		accDet.setAccount(account);

		OBFundsConfirmation1DataInstructedAmount instructedAmount = new OBFundsConfirmation1DataInstructedAmount();
		instructedAmount.setAmount("500");
		instructedAmount.setCurrency("EUR");

		OBFundsConfirmation1Data data = new OBFundsConfirmation1Data();
		data.setConsentId("6bf54aa2-d1d0-4f49-badf-40694f78743e");
		data.setInstructedAmount(instructedAmount);
		data.setReference("sdfgdfg");

		obFundsConfirmation1.setData(data);

		Mockito.when(fundsconfirmationFoundationServiceDelegate.getFoundationServiceURL(any(), any(), any(), any()))
				.thenReturn("http://localhost:9011/core-banking/p/abt/v1.0/accounts/111111/11111111/available-funds");
		Mockito.when(fundsconfirmationFoundationServiceClient.restTransportForFundsConfirmation(any(),any(), any(), any()))
				.thenReturn(availableFunds);
		Mockito.when(fundsconfirmationFoundationServiceDelegate.transformResponseFromFDToAPI(any(), any(), any()))
				.thenReturn(new OBFundsConfirmationResponse1());

		Map<String, String> params = new HashMap<String, String>();
		params.put(FundsConfirmationFoundationServiceConstants.ACCOUNT_SUBTYPE, "Savings");
		fundsconfirmationFoundationServiceAdapter.getFundsData(accDet, obFundsConfirmation1, params);
	}	
}