package com.capgemini.psd2.foundationservice.funds.confirmation.boi.adapter.test;

import static org.junit.Assert.assertNotNull;

import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.aisp.domain.OBExternalAccountSubType1Code;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmation1;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmation1Data;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmation1DataInstructedAmount;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmationResponse1;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.foundationservice.funds.confirmation.boi.adapter.constants.FundsConfirmationFoundationServiceConstants;
import com.capgemini.psd2.foundationservice.funds.confirmation.boi.adapter.raml.domain.AvailableFunds;
import com.capgemini.psd2.foundationservice.funds.confirmation.boi.adapter.raml.domain.AvailableFundsForWithdrawalAmount;
import com.capgemini.psd2.foundationservice.funds.confirmation.boi.adapter.raml.domain.Currency;
import com.capgemini.psd2.foundationservice.funds.confirmation.boi.adapter.raml.domain.FundsCheckStatus;
import com.capgemini.psd2.foundationservice.funds.confirmation.boi.adapter.transformer.FundsConfirmationFoundationServiceTransformer;
import com.capgemini.psd2.validator.impl.PSD2ValidatorImpl;


@RunWith(SpringJUnit4ClassRunner.class)
public class FundsConfirmationFoundationServiceTransformerTest {

	@Mock
	private PSD2ValidatorImpl psd2Validator;

	@InjectMocks
	private FundsConfirmationFoundationServiceTransformer fundsConfirmationFoundationServiceTransformer;
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void contextLoads() {
	}
	
	@Test(expected = AdapterException.class)
	public void testCurrencyMisMatch() {
		OBFundsConfirmation1 oBFundsConfirmation1 = new OBFundsConfirmation1();
		AvailableFunds availableFunds = new AvailableFunds();
		Currency accountCurrency = new Currency();
		accountCurrency.setCurrencyName("GBP");
		accountCurrency.setIsoAlphaCode("GBP");
		availableFunds.setAccountCurrency(accountCurrency);
		availableFunds.setAccountNumber("11111111");
		OffsetDateTime date = OffsetDateTime.now();
		availableFunds.setAvailableFundsCheckDatetime(date);
		availableFunds.setParentNationalSortCodeNSCNumber("90155576756555");
		AvailableFundsForWithdrawalAmount availableFundsForWithdrawalAmount = new AvailableFundsForWithdrawalAmount();
		availableFundsForWithdrawalAmount.setTransactionCurrency(801.77d);
		availableFunds.setAvailableFundsForWithdrawalAmount(availableFundsForWithdrawalAmount);
		AccountDetails accDet = new AccountDetails();
		accDet.setAccountSubType(OBExternalAccountSubType1Code.CURRENTACCOUNT);
		accDet.setAccountId("11111111");
		accDet.setAccountNSC("11111111");
		accDet.setAccountNumber("11111111");
		OBFundsConfirmation1DataInstructedAmount instructedAmount = new OBFundsConfirmation1DataInstructedAmount();
		instructedAmount.setAmount("500");
		instructedAmount.setCurrency("EUR");
		OBFundsConfirmation1Data data = new OBFundsConfirmation1Data();
		data.setConsentId("ab52f025-6185-4a62-a04e-d93e6ab6ec0b");
		data.setReference("anc");
		data.setInstructedAmount(instructedAmount);
		Map<String, String> params = new HashMap<>();
		params.put(FundsConfirmationFoundationServiceConstants.ACCOUNT_SUBTYPE, "CurrentAccount");
		params.put("accountId", "11111111");
		OBFundsConfirmationResponse1 response = new OBFundsConfirmationResponse1();
		OBFundsConfirmation1DataInstructedAmount amount = new OBFundsConfirmation1DataInstructedAmount();
		amount.setAmount(new BigDecimal(5000.00d).toString());
		amount.setCurrency("GBP");
		oBFundsConfirmation1.setData(data);
		assertNotNull(amount);
		fundsConfirmationFoundationServiceTransformer.transformAccountBalance(availableFunds, oBFundsConfirmation1, params);
	}
	
	@Test
	public void testAccountBalanceFSTransformer1a() {
		OBFundsConfirmation1 oBFundsConfirmation1 = new OBFundsConfirmation1();
		AvailableFunds availableFunds = new AvailableFunds();
		Currency accountCurrency = new Currency();
		accountCurrency.setCurrencyName("GBP");
		accountCurrency.setIsoAlphaCode("GBP");
		availableFunds.setAccountCurrency(accountCurrency);
		availableFunds.setAccountNumber("11111111");
		OffsetDateTime date = OffsetDateTime.now();
		availableFunds.setAvailableFundsCheckDatetime(date);
		availableFunds.setParentNationalSortCodeNSCNumber("90155576756555");
		AvailableFundsForWithdrawalAmount availableFundsForWithdrawalAmount = new AvailableFundsForWithdrawalAmount();
		availableFundsForWithdrawalAmount.setTransactionCurrency(801.77d);
		availableFunds.setAvailableFundsForWithdrawalAmount(availableFundsForWithdrawalAmount);
		availableFunds.setFundsCheckStatus(FundsCheckStatus.COMPLETED);
		AccountDetails accDet = new AccountDetails();
		accDet.setAccountSubType(OBExternalAccountSubType1Code.CURRENTACCOUNT);
		accDet.setAccountId("11111111");
		accDet.setAccountNSC("11111111");
		accDet.setAccountNumber("11111111");
		OBFundsConfirmation1DataInstructedAmount instructedAmount = new OBFundsConfirmation1DataInstructedAmount();
		instructedAmount.setAmount("500");
		instructedAmount.setCurrency("GBP");
		OBFundsConfirmation1Data data = new OBFundsConfirmation1Data();
		data.setConsentId("ab52f025-6185-4a62-a04e-d93e6ab6ec0b");
		data.setReference("anc");
		data.setInstructedAmount(instructedAmount);
		Map<String, String> params = new HashMap<>();
		params.put(FundsConfirmationFoundationServiceConstants.ACCOUNT_SUBTYPE, "CurrentAccount");
		params.put("accountId", "11111111");
		OBFundsConfirmationResponse1 response = new OBFundsConfirmationResponse1();
		OBFundsConfirmation1DataInstructedAmount amount = new OBFundsConfirmation1DataInstructedAmount();
		amount.setAmount(new BigDecimal(5000.00d).toString());
		amount.setCurrency("GBP");
		oBFundsConfirmation1.setData(data);
		assertNotNull(amount);
		fundsConfirmationFoundationServiceTransformer.transformAccountBalance(availableFunds, oBFundsConfirmation1, params);
	}
	
	
	@Test
	public void testAccountBalanceFSTransformer1n() {
		OBFundsConfirmation1 oBFundsConfirmation1 = new OBFundsConfirmation1();
		AvailableFunds availableFunds = new AvailableFunds();
		Currency accountCurrency = new Currency();
		accountCurrency.setCurrencyName("GBP");
		accountCurrency.setIsoAlphaCode("GBP");
		availableFunds.setAccountCurrency(accountCurrency);
		availableFunds.setAccountNumber("11111111");
		OffsetDateTime date = OffsetDateTime.now();
		availableFunds.setAvailableFundsCheckDatetime(date);
		availableFunds.setParentNationalSortCodeNSCNumber("90155576756555");
		AvailableFundsForWithdrawalAmount availableFundsForWithdrawalAmount = new AvailableFundsForWithdrawalAmount();
		availableFundsForWithdrawalAmount.setTransactionCurrency(801.77d);
		availableFunds.setAvailableFundsForWithdrawalAmount(availableFundsForWithdrawalAmount);
		availableFunds.setFundsCheckStatus(FundsCheckStatus.COMPLETED);
		AccountDetails accDet = new AccountDetails();
		accDet.setAccountSubType(OBExternalAccountSubType1Code.CURRENTACCOUNT);
		accDet.setAccountId("11111111");
		accDet.setAccountNSC("11111111");
		accDet.setAccountNumber("11111111");
		OBFundsConfirmation1DataInstructedAmount instructedAmount = new OBFundsConfirmation1DataInstructedAmount();
		instructedAmount.setAmount("500");
		instructedAmount.setCurrency("GBP");
		OBFundsConfirmation1Data data = new OBFundsConfirmation1Data();
		data.setConsentId("ab52f025-6185-4a62-a04e-d93e6ab6ec0b");
		data.setReference("anc");
		data.setInstructedAmount(instructedAmount);
		Map<String, String> params = new HashMap<>();
		params.put(FundsConfirmationFoundationServiceConstants.ACCOUNT_SUBTYPE, "CurrentAccount");
		params.put("accountId", null);
		OBFundsConfirmationResponse1 response = new OBFundsConfirmationResponse1();
		OBFundsConfirmation1DataInstructedAmount amount = new OBFundsConfirmation1DataInstructedAmount();
		amount.setAmount(new BigDecimal(5000.00d).toString());
		amount.setCurrency("GBP");
		assertNotNull(amount);
		oBFundsConfirmation1.setData(data);
		fundsConfirmationFoundationServiceTransformer.transformAccountBalance(availableFunds, oBFundsConfirmation1, params);
	}
	@Test
	public void testAccountBalanceFSTransformer1b() {
		OBFundsConfirmation1 oBFundsConfirmation1 = new OBFundsConfirmation1();
		AvailableFunds availableFunds = new AvailableFunds();
		Currency accountCurrency = new Currency();
		accountCurrency.setCurrencyName("GBP");
		accountCurrency.setIsoAlphaCode("GBP");
		availableFunds.setAccountCurrency(accountCurrency);
		availableFunds.setAccountNumber("11111111");
		OffsetDateTime date = OffsetDateTime.now();
		availableFunds.setAvailableFundsCheckDatetime(date);
		availableFunds.setParentNationalSortCodeNSCNumber("90155576756555");
		AvailableFundsForWithdrawalAmount availableFundsForWithdrawalAmount = new AvailableFundsForWithdrawalAmount();
		availableFundsForWithdrawalAmount.setTransactionCurrency(801.77d);
		availableFunds.setAvailableFundsForWithdrawalAmount(availableFundsForWithdrawalAmount);
		availableFunds.setFundsCheckStatus(FundsCheckStatus.COMPLETED);
		AccountDetails accDet = new AccountDetails();
		accDet.setAccountSubType(OBExternalAccountSubType1Code.SAVINGS);
		accDet.setAccountId("11111111");
		accDet.setAccountNSC("11111111");
		accDet.setAccountNumber("11111111");
		OBFundsConfirmation1DataInstructedAmount instructedAmount = new OBFundsConfirmation1DataInstructedAmount();
		instructedAmount.setAmount("500");
		instructedAmount.setCurrency("GBP");
		OBFundsConfirmation1Data data = new OBFundsConfirmation1Data();
		data.setConsentId("ab52f025-6185-4a62-a04e-d93e6ab6ec0b");
		data.setReference("anc");
		data.setInstructedAmount(instructedAmount);
		Map<String, String> params = new HashMap<>();
		params.put(FundsConfirmationFoundationServiceConstants.ACCOUNT_SUBTYPE, "Savings");
		params.put("accountId", "11111111");
		OBFundsConfirmationResponse1 response = new OBFundsConfirmationResponse1();
		OBFundsConfirmation1DataInstructedAmount amount = new OBFundsConfirmation1DataInstructedAmount();
		amount.setAmount(new BigDecimal(5000.00d).toString());
		amount.setCurrency("GBP");
		assertNotNull(amount);
		oBFundsConfirmation1.setData(data);
		fundsConfirmationFoundationServiceTransformer.transformAccountBalance(availableFunds, oBFundsConfirmation1, params);
	}
	
	@Test
	public void testAccountBalanceFSTransformer1c() {
		OBFundsConfirmation1 oBFundsConfirmation1 = new OBFundsConfirmation1();
		AvailableFunds availableFunds = new AvailableFunds();
		Currency accountCurrency = new Currency();
		accountCurrency.setCurrencyName("GBP");
		accountCurrency.setIsoAlphaCode("GBP");
		availableFunds.setAccountCurrency(accountCurrency);
		availableFunds.setAccountNumber("11111111");
		OffsetDateTime date = OffsetDateTime.now();
		availableFunds.setAvailableFundsCheckDatetime(date);
		availableFunds.setParentNationalSortCodeNSCNumber("90155576756555");
		AvailableFundsForWithdrawalAmount availableFundsForWithdrawalAmount = new AvailableFundsForWithdrawalAmount();
		availableFundsForWithdrawalAmount.setTransactionCurrency(801.77d);
		availableFunds.setAvailableFundsForWithdrawalAmount(availableFundsForWithdrawalAmount);
		availableFunds.setFundsCheckStatus(FundsCheckStatus.NOT_COMPLETED);
		AccountDetails accDet = new AccountDetails();
		accDet.setAccountSubType(OBExternalAccountSubType1Code.CURRENTACCOUNT);
		accDet.setAccountId("11111111");
		accDet.setAccountNSC("11111111");
		accDet.setAccountNumber("11111111");
		OBFundsConfirmation1DataInstructedAmount instructedAmount = new OBFundsConfirmation1DataInstructedAmount();
		instructedAmount.setAmount("500");
		instructedAmount.setCurrency("GBP");
		OBFundsConfirmation1Data data = new OBFundsConfirmation1Data();
		data.setConsentId("ab52f025-6185-4a62-a04e-d93e6ab6ec0b");
		data.setReference("anc");
		data.setInstructedAmount(instructedAmount);
		Map<String, String> params = new HashMap<>();
		params.put(FundsConfirmationFoundationServiceConstants.ACCOUNT_SUBTYPE, "CurrentAccount");
		params.put("accountId", "11111111");
		OBFundsConfirmationResponse1 response = new OBFundsConfirmationResponse1();
		OBFundsConfirmation1DataInstructedAmount amount = new OBFundsConfirmation1DataInstructedAmount();
		amount.setAmount(new BigDecimal(5000.00d).toString());
		amount.setCurrency("GBP");
		assertNotNull(amount);
		oBFundsConfirmation1.setData(data);
		fundsConfirmationFoundationServiceTransformer.transformAccountBalance(availableFunds, oBFundsConfirmation1, params);
	}
	
}
