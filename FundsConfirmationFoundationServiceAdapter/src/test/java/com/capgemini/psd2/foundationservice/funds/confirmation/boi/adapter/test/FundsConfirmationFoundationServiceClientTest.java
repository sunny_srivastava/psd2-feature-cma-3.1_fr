//package com.capgemini.psd2.foundationservice.funds.confirmation.boi.adapter.test;
//
//import static org.junit.Assert.assertEquals;
//
//import static org.mockito.Matchers.any;
//
//import java.time.OffsetDateTime;
//
//import org.junit.Before;
//import org.junit.runner.RunWith;
//import org.mockito.InjectMocks;
//import org.mockito.Mock;
//import org.mockito.Mockito;
//import org.mockito.MockitoAnnotations;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//
//import com.capgemini.psd2.foundationservice.funds.confirmation.boi.adapter.client.FundsConfirmationFoundationServiceClient;
//import com.capgemini.psd2.foundationservice.funds.confirmation.boi.adapter.raml.domain.AvailableFunds;
//import com.capgemini.psd2.foundationservice.funds.confirmation.boi.adapter.raml.domain.AvailableFundsForWithdrawalAmount;
//import com.capgemini.psd2.foundationservice.funds.confirmation.boi.adapter.raml.domain.Currency;
//import com.capgemini.psd2.foundationservice.funds.confirmation.boi.adapter.raml.domain.FundsCheckStatus;
//import com.capgemini.psd2.rest.client.sync.RestClientSync;
//
//@RunWith(SpringJUnit4ClassRunner.class)
//public class FundsConfirmationFoundationServiceClientTest {
//
//	@InjectMocks
//	private FundsConfirmationFoundationServiceClient fundsconfirmationFoundationServiceClient;
//
//	@Mock
//	RestClientSync RestClientSync;
//	
////	@Before
////	public void setUp() {
////		MockitoAnnotations.initMocks(this);
////	}
//
////	@Test
//	public void restTransportForFundsConfirmation() {
//		
//		AvailableFunds availableFunds = new AvailableFunds();
//		Currency accountCurrency = new Currency();
//		accountCurrency.setCurrencyName("Euro");
//		accountCurrency.setIsoAlphaCode("EUR");
//		availableFunds.setAccountCurrency(accountCurrency);
//		availableFunds.setAccountNumber("11111111");
//		OffsetDateTime date = OffsetDateTime.now();
//		availableFunds.setAvailableFundsCheckDatetime(date);
//		availableFunds.setParentNationalSortCodeNSCNumber("111111");
//		availableFunds.setFundsCheckStatus(FundsCheckStatus.COMPLETED);
//		availableFunds.setFundsCheckStatusReasonDescription(null);
//		AvailableFundsForWithdrawalAmount availableFundsForWithdrawalAmount = new AvailableFundsForWithdrawalAmount();
//		availableFundsForWithdrawalAmount.setTransactionCurrency(1000.0d);
//		availableFunds.setAvailableFundsForWithdrawalAmount(availableFundsForWithdrawalAmount);
//	    Mockito.doReturn(availableFunds).when(RestClientSync).callForGet(any(), any(), any());
//		AvailableFunds obj =fundsconfirmationFoundationServiceClient.restTransportForFundsConfirmation(any(),any(),any(),any());
//		System.out.println(obj);
//		assertEquals("Euro",obj.getAccountCurrency().getCurrencyName());
//		
//	}
//
//}