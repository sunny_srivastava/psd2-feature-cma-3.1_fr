package com.capgemini.psd2.foundationservice.funds.confirmation.boi.adapter.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.anyObject;

import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.aisp.domain.OBExternalAccountSubType1Code;

import com.capgemini.psd2.cisp.domain.OBFundsConfirmation1;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmation1DataInstructedAmount;

import com.capgemini.psd2.cisp.domain.OBFundsConfirmationResponse1;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmationResponse1Data;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.foundationservice.funds.confirmation.boi.adapter.constants.FundsConfirmationFoundationServiceConstants;
import com.capgemini.psd2.foundationservice.funds.confirmation.boi.adapter.delegate.FundsConfirmationFoundationServiceDelegate;
import com.capgemini.psd2.foundationservice.funds.confirmation.boi.adapter.raml.domain.AvailableFunds;
import com.capgemini.psd2.foundationservice.funds.confirmation.boi.adapter.raml.domain.AvailableFundsForWithdrawalAmount;
import com.capgemini.psd2.foundationservice.funds.confirmation.boi.adapter.raml.domain.BrandCode3;
import com.capgemini.psd2.foundationservice.funds.confirmation.boi.adapter.raml.domain.Currency;
import com.capgemini.psd2.foundationservice.funds.confirmation.boi.adapter.transformer.FundsConfirmationFoundationServiceTransformer;
import com.capgemini.psd2.logger.PSD2Constants;

@RunWith(SpringJUnit4ClassRunner.class)
public class FundsConfirmationFoundationServiceDelegateTest {

	@InjectMocks
	private FundsConfirmationFoundationServiceDelegate delegate;
	
	@Mock
	private FundsConfirmationFoundationServiceTransformer fundsConfirmationFSTransformer;
	
	OBFundsConfirmation1 oBFundsConfirmation1 = new OBFundsConfirmation1();

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}


	@Test
	public void testGetFoundationServiceWithInvalidURL() {
		String accountNSC = "11111111";
		String accountNumber = "11111111";
		String baseURL = "http://localhost:9011/core-banking/p/abt";
		String finalURL = "http://localhost:9011/core-banking/p/abt/accounts/nsc/number";
		String testURL = delegate.getFoundationServiceURL(accountNSC, accountNumber, baseURL, finalURL);
		assertNotEquals("Test for building URL failed.", finalURL, testURL);
		assertNotNull(testURL);
	}

	@Test
	public void testGetFoundationServiceUrl() {
		String accountNSC = "11111111";
		String accountNumber = "11111111";
		String baseURL = "http://localhost:9011/core-banking/p/abt";
		String version = "v3.0";
		String url = delegate.getFoundationServiceURL(version, accountNSC, accountNumber, baseURL);
		assertEquals("http://localhost:9011/core-banking/p/abt/v3.0/accounts/11111111/11111111/available-funds",url);
	}

	@Test(expected = AdapterException.class)
	public void testGetFoundationServiceWithAccountNscNull() {
		String accountNSC = null;
		String accountNumber = "number";
		String baseURL = null;
		String version = "167236";
		delegate.getFoundationServiceURL(version, accountNSC, accountNumber, baseURL);

	}

	@Test(expected = AdapterException.class)
	public void testGetFoundationServiceWithAccountNumberAsNull() {
		String accountNSC = "nsc";
		String accountNumber = null;
		String baseURL = "http://localhost:8081//psd2-abt-service/services/abt";
		String version = "167236";
		delegate.getFoundationServiceURL(version, accountNSC, accountNumber, baseURL);

	}

	@Test(expected = AdapterException.class)
	public void testGetFoundationServiceWithBaseURLAsNull() {
		String accountNSC = "nsc";
		String accountNumber = "number";
		String baseURL = null;
		String version = "167236";
		delegate.getFoundationServiceURL(version, accountNSC, accountNumber, baseURL);

	}

	@Test
	public void testcreateFundsConfirmationRequestHeaders() {
		Map<String, String> params = new HashMap<String, String>();
		params.put(FundsConfirmationFoundationServiceConstants.CHANNEL_ID, "BOI");
		params.put(PSD2Constants.CORRELATION_ID, "6842080c-5322-4e80-9c99-a607c6b5abc0");
		params.put(PSD2Constants.USER_IN_REQ_HEADER, "88888888");
		params.put(PSD2Constants.TENANT_ID, "BOIUK");
		params.put(PSD2Constants.CORRELATION_ID, "6842080c-5322-4e80-9c99-a607c6b5abc0");
		ReflectionTestUtils.setField(delegate, "transactioReqHeader", "X-BOI-USER");
		ReflectionTestUtils.setField(delegate, "correlationMuleReqHeader", "X-BOI-CHANNEL");
		ReflectionTestUtils.setField(delegate, "sourcesystem", "X-BOI-PLATFORM");
		ReflectionTestUtils.setField(delegate, "sourceUserReqHeader", "X-CORRELATION-ID");
		ReflectionTestUtils.setField(delegate, "apiChannelCode", "PSD2API");
		ReflectionTestUtils.setField(delegate, "partySourceNumber", "X-API-PARTY-SOURCE-ID-NUMBER");
		ReflectionTestUtils.setField(delegate, "apiChannelBrand", "x-api-channel-brand");
		HttpHeaders httpHeaders = delegate.createFundsConfirmationRequestHeaders(null, null, params);
		assertEquals(BrandCode3.NIGB.toString(), httpHeaders.get("x-api-channel-brand").get(0));
		params.put(PSD2Constants.TENANT_ID, "BOIROI");
		httpHeaders = delegate.createFundsConfirmationRequestHeaders(null, null, params);
		assertEquals(BrandCode3.ROI.toString(), httpHeaders.get("x-api-channel-brand").get(0));
	}

	@Test
	public void testTransformResponseFromFDToAPI1() {
		AvailableFunds availableFunds = new AvailableFunds();
		Currency accountCurrency = new Currency();
		accountCurrency.setCurrencyName("Euro");
		accountCurrency.setIsoAlphaCode("EUR");
		availableFunds.setAccountCurrency(accountCurrency);
		availableFunds.setAccountNumber("11111111");
		OffsetDateTime date = OffsetDateTime.now();
		availableFunds.setAvailableFundsCheckDatetime(date);
		availableFunds.setParentNationalSortCodeNSCNumber("90155576756555");
		AvailableFundsForWithdrawalAmount availableFundsForWithdrawalAmount = new AvailableFundsForWithdrawalAmount();
		availableFundsForWithdrawalAmount.setTransactionCurrency(801.77d);
		availableFunds.setAvailableFundsForWithdrawalAmount(availableFundsForWithdrawalAmount);
		AccountDetails accDet = new AccountDetails();
		accDet.setAccountSubType(OBExternalAccountSubType1Code.CURRENTACCOUNT);
		accDet.setAccountId("11111111");
		accDet.setAccountNSC("11111111");
		accDet.setAccountNumber("11111111");
		OBFundsConfirmation1DataInstructedAmount instructedAmount = new OBFundsConfirmation1DataInstructedAmount();
		instructedAmount.setAmount("500");
		instructedAmount.setCurrency("EUR");
		OBFundsConfirmationResponse1Data data = new OBFundsConfirmationResponse1Data();
		data.setConsentId("ab52f025-6185-4a62-a04e-d93e6ab6ec0b");
		data.setReference("anc");
		data.setInstructedAmount(instructedAmount);
		data.setFundsConfirmationId("11111111");
		Map<String, String> params = new HashMap<>();
		params.put(FundsConfirmationFoundationServiceConstants.ACCOUNT_SUBTYPE, "CurrentAccount");
		params.put("accountId", "11111111");
		OBFundsConfirmationResponse1 response = new OBFundsConfirmationResponse1();
		response.setData(data);
		OBFundsConfirmation1DataInstructedAmount amount = new OBFundsConfirmation1DataInstructedAmount();
		amount.setAmount(new BigDecimal(5000.00d).toString());
		amount.setCurrency("GBP");
		Mockito.when(fundsConfirmationFSTransformer.transformAccountBalance(anyObject(), anyObject(), anyObject()))
				.thenReturn(response);
		delegate.transformResponseFromFDToAPI(availableFunds, oBFundsConfirmation1, params);
		
	}
}