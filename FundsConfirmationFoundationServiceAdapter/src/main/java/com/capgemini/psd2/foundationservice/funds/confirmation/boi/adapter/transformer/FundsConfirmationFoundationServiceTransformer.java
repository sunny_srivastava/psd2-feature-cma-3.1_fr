/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.foundationservice.funds.confirmation.boi.adapter.transformer;

import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.adapter.exceptions.AdapterErrorCodeEnum;
import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmation1;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmation1DataInstructedAmount;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmationResponse1;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmationResponse1Data;
import com.capgemini.psd2.foundationservice.funds.confirmation.boi.adapter.raml.domain.AvailableFunds;
import com.capgemini.psd2.validator.PSD2Validator;

/**
 * The Class FundsConfirmationFoundationServiceTransformer.
 */
@Component
public class FundsConfirmationFoundationServiceTransformer {

	/** The psd2 validator. */
	@Autowired
	private PSD2Validator psd2Validator;

	public <T> OBFundsConfirmationResponse1 transformAccountBalance(T inputBalanceObj,
			OBFundsConfirmation1 oBFundsConfirmation1, Map<String, String> params) {
		OBFundsConfirmationResponse1 oBFundsConfirmationResponse1 = new OBFundsConfirmationResponse1();
		OBFundsConfirmationResponse1Data data = new OBFundsConfirmationResponse1Data();
		UUID uuid = UUID.randomUUID();
		AvailableFunds availableFunds = (AvailableFunds) inputBalanceObj;
		if (!availableFunds.getAccountCurrency().getIsoAlphaCode()
				.equalsIgnoreCase(oBFundsConfirmation1.getData().getInstructedAmount().getCurrency())) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.CURRENCY_MISMATCH_CBPII);
		}
		boolean fundsAvailbale = false;
		if (availableFunds.getFundsCheckStatus() != null) {
			String status = availableFunds.getFundsCheckStatus().getValue();
			if (!StringUtils.isBlank(status) && status.equalsIgnoreCase("completed")) {
				Double difference = availableFunds.getAvailableFundsForWithdrawalAmount().getTransactionCurrency()
						- Double.valueOf(oBFundsConfirmation1.getData().getInstructedAmount().getAmount());
				if (difference >= 0)
					fundsAvailbale = true;
			}
		}
		data.setFundsAvailable(fundsAvailbale);
		data.setFundsConfirmationId(uuid.toString());
		data.setConsentId(oBFundsConfirmation1.getData().getConsentId());
		data.setReference(oBFundsConfirmation1.getData().getReference());
		OBFundsConfirmation1DataInstructedAmount instructedAmount = oBFundsConfirmation1.getData().getInstructedAmount();
		instructedAmount.setAmount(oBFundsConfirmation1.getData().getInstructedAmount().getAmount());
		instructedAmount.setCurrency(oBFundsConfirmation1.getData().getInstructedAmount().getCurrency());
		data.setInstructedAmount(instructedAmount);
		String isoDateTime = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ssZ").format(OffsetDateTime.now());
		isoDateTime = isoDateTime.substring(0, 22).concat(":").concat(isoDateTime.substring(22, 24));
		data.setCreationDateTime(isoDateTime);
		psd2Validator.validate(data);
		oBFundsConfirmationResponse1.setData(data);
		return oBFundsConfirmationResponse1;
	}
}
