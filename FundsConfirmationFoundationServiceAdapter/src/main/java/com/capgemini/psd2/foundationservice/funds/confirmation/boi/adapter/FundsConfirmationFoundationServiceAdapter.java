/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.foundationservice.funds.confirmation.boi.adapter;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import com.capgemini.psd2.adapter.exceptions.AdapterErrorCodeEnum;
import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.cisp.adapter.FundsConfirmationAdapter;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmation1;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmationResponse1;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.foundationservice.funds.confirmation.boi.adapter.client.FundsConfirmationFoundationServiceClient;
import com.capgemini.psd2.foundationservice.funds.confirmation.boi.adapter.constants.FundsConfirmationFoundationServiceConstants;
import com.capgemini.psd2.foundationservice.funds.confirmation.boi.adapter.delegate.FundsConfirmationFoundationServiceDelegate;
import com.capgemini.psd2.foundationservice.funds.confirmation.boi.adapter.raml.domain.AvailableFunds;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.utilities.NullCheckUtils;


/**
 * The Class FundsConfirmationFoundationServiceAdapter.
 */
@Component
public class FundsConfirmationFoundationServiceAdapter implements FundsConfirmationAdapter  {
	
	/** The funds confirmation balance base URL. */
	@Value("${foundationService.fundsConfirmationBaseURL}")
	private String fundsConfirmationBaseURL;

	@Value("${foundationService.consentFlowType}")
	private String consentFlowType;

	@Value("${foundationService.version}")
	private String version;

	/** The funds confirmation foundation service delegate. */
	@Autowired
	private FundsConfirmationFoundationServiceDelegate fundsConfirmationFoundationServiceDelegate;

	/** The funds confirmation foundation service client. */
	@Autowired
	private FundsConfirmationFoundationServiceClient fundsConfirmationFoundationServiceClient;

	private OBFundsConfirmationResponse1 oBFundsConfirmationResponse1;
	@Override
	public OBFundsConfirmationResponse1 getFundsData(AccountDetails accountDetails,
			OBFundsConfirmation1 fundsConfirmationPOSTRequest, Map<String, String> params) {
		
		if (accountDetails == null ) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.BAD_REQUEST);
		}
		
		if (fundsConfirmationPOSTRequest == null ) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.BAD_REQUEST);
		}

		if (params == null) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.BAD_REQUEST);
		}
		params.put(PSD2Constants.CONSENT_FLOW_TYPE, consentFlowType); 
		
		if (!NullCheckUtils.isNullOrEmpty(accountDetails)) {
		
			params.put(FundsConfirmationFoundationServiceConstants.ACCOUNT_ID, accountDetails.getAccountId());
		   params.put(FundsConfirmationFoundationServiceConstants.ACCOUNT_SUBTYPE,
				String.valueOf(accountDetails.getAccountSubType()));
		} else
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.BAD_REQUEST);
		
		RequestInfo requestInfo = new RequestInfo();
		HttpHeaders httpHeaders = null;
		
	
		AvailableFunds availableFunds =null;
				
		if ((NullCheckUtils.isNullOrEmpty(accountDetails.getAccountSubType())
				&& params.get(PSD2Constants.CMAVERSION) == null)
				|| accountDetails.getAccountSubType().toString().contentEquals(FundsConfirmationFoundationServiceConstants.CURRENT_ACCOUNT)
				|| accountDetails.getAccountSubType().toString().contentEquals(FundsConfirmationFoundationServiceConstants.SAVINGS)) {
			
			httpHeaders = fundsConfirmationFoundationServiceDelegate.createFundsConfirmationRequestHeaders(requestInfo,
					accountDetails, params);
			String finalURL = fundsConfirmationFoundationServiceDelegate.getFoundationServiceURL(version,
					accountDetails.getAccountNSC(), accountDetails.getAccountNumber(), fundsConfirmationBaseURL);
			
			requestInfo.setUrl(finalURL);
			
			MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<String, String>(); 
			queryParams.add("channelCode", params.get(FundsConfirmationFoundationServiceConstants.CHANNEL_ID).toUpperCase());
			availableFunds = fundsConfirmationFoundationServiceClient.restTransportForFundsConfirmation(requestInfo, AvailableFunds.class, queryParams, httpHeaders);
			
			if (NullCheckUtils.isNullOrEmpty(availableFunds) || NullCheckUtils.isNullOrEmpty(availableFunds.getAccountNumber())) {
				throw AdapterException
						.populatePSD2Exception(AdapterErrorCodeEnum.NO_ACCOUNT_DATA_FOUND_FOUNDATION_SERVICE);
			}
			oBFundsConfirmationResponse1 = fundsConfirmationFoundationServiceDelegate
					.transformResponseFromFDToAPI(availableFunds,fundsConfirmationPOSTRequest, params);

		} 
		else if (accountDetails.getAccountSubType().toString().contentEquals(FundsConfirmationFoundationServiceConstants.CREDIT_CARD)) 
		{
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.BAD_REQUEST);
		}
		return oBFundsConfirmationResponse1;
	}
}