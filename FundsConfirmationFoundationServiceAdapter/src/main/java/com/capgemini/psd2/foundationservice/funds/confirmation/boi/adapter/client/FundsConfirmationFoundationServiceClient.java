/******************************************************************************* 
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.foundationservice.funds.confirmation.boi.adapter.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;

import com.capgemini.psd2.foundationservice.funds.confirmation.boi.adapter.raml.domain.AvailableFunds;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.RestClientSync;

/**
 * The Class FundsConfirmationFoundationServiceClient.
 */
@Component
public class FundsConfirmationFoundationServiceClient {
/** The rest client. */
@Autowired
@Qualifier("restClientFoundation")
private RestClientSync restClient;
public AvailableFunds restTransportForFundsConfirmation(RequestInfo reqInfo, Class <AvailableFunds> responseType, MultiValueMap<String, String> queryParams, HttpHeaders headers) {
reqInfo.setUrl(UriComponentsBuilder.fromHttpUrl(reqInfo.getUrl()).queryParams(queryParams).build().toString());
return restClient.callForGet(reqInfo, responseType, headers);
}
}
