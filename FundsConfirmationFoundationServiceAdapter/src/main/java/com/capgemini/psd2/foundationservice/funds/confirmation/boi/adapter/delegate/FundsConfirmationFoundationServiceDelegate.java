/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.foundationservice.funds.confirmation.boi.adapter.delegate;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.adapter.exceptions.AdapterErrorCodeEnum;
import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmation1;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmationResponse1;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.foundationservice.funds.confirmation.boi.adapter.constants.FundsConfirmationFoundationServiceConstants;
import com.capgemini.psd2.foundationservice.funds.confirmation.boi.adapter.raml.domain.AvailableFunds;
import com.capgemini.psd2.foundationservice.funds.confirmation.boi.adapter.raml.domain.BrandCode3;
import com.capgemini.psd2.foundationservice.funds.confirmation.boi.adapter.transformer.FundsConfirmationFoundationServiceTransformer;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.utilities.NullCheckUtils;

/**
 * The Class FundsConfirmationFoundationServiceDelegate.
 */
@Component
public class FundsConfirmationFoundationServiceDelegate {
	
	/** The Funds Confirmation FS transformer. */
	@Autowired
	private FundsConfirmationFoundationServiceTransformer fundsConfirmationFSTransformer;
	
	/** The user in req header. */
	@Value("${foundationService.userInReqHeader:#{X-BOI-USER}}")
	private String userInReqHeader;

	/** The channel in req header. */
	@Value("${foundationService.channelInReqHeader:#{X-BOI-CHANNEL}}")
	private String channelInReqHeader;
	
	/** The platform. */
	@Value("$app.platform")
	private String platform;
	
	@Value("${foundationService.sourcesystem:#{x-api-source-system}}")
	private String sourcesystem;
	
	@Value("${foundationService.transactioReqHeader:#{x-api-transaction-id}}")
	private String transactioReqHeader;
	
	@Value("${foundationService.correlationMuleReqHeader:#{x-api-correlation-id}}")
	private String correlationMuleReqHeader;
	
	@Value("${foundationService.sourceUserReqHeader:#{x-api-source-user}}")
	private String sourceUserReqHeader;
	
	@Value("${foundationService.apiChannelCode:#{x-api-channel-Code}}")
	private String apiChannelCode;
	
	@Value("${foundationService.maskedPan:#{X-MASKED-PAN}}")
	private String maskedPan;
	
	@Value("${foundationService.maskedPan:#{x-api-party-source-id-number}}")
	private String partySourceNumber;
	
	@Value("${foundationService.apiChannelBrand:#{x-api-channel-brand}}")
	private String apiChannelBrand;

	
	/**
	 * Gets the foundation service URL.
	 *
	 * @param accountNSC the account NSC
	 * @param accountNumber the account number
	 * @param baseURL the base URL
	 * @return the foundation service URL
	 */
	public String getFoundationServiceURL(String version, String accountNSC, String accountNumber, String baseURL){
		if(NullCheckUtils.isNullOrEmpty(accountNSC)) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.NO_ACCOUNT_DATA_FOUND);
		}
		if(NullCheckUtils.isNullOrEmpty(accountNumber)) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.NO_ACCOUNT_DATA_FOUND);
		}
		if(NullCheckUtils.isNullOrEmpty(baseURL)) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.NO_ACCOUNT_DATA_FOUND);
		}
		return baseURL + "/" + version + "/" + "accounts" + "/" + accountNSC + "/" + accountNumber + "/" + "available-funds";
	}
	
	/**
	 * Transform response from FD to API.
	 *
	 * @param accounts the accounts
	 * @param params the params
	 * @return the balances GET response
	 */
	
	public <T> OBFundsConfirmationResponse1 transformResponseFromFDToAPI(T inputTransactionObj,OBFundsConfirmation1 oBFundsConfirmation1, Map<String, String> params){
		OBFundsConfirmationResponse1 oBFundsConfirmationResponse = new OBFundsConfirmationResponse1();
			AvailableFunds availableFunds= (AvailableFunds) inputTransactionObj;
			oBFundsConfirmationResponse = fundsConfirmationFSTransformer.transformAccountBalance(availableFunds, oBFundsConfirmation1,params);
		return oBFundsConfirmationResponse;
	}
	
	/**
	 * Creates the request headers.
	 *
	 * @param requestInfo the request info
	 * @param accountMapping the account mapping
	 * @return the http headers
	 */
	public HttpHeaders createFundsConfirmationRequestHeaders(RequestInfo requestInfo,AccountDetails accountDetails,Map<String, String> params) {
		HttpHeaders httpHeaders = new HttpHeaders();
		String channelCode = params.get(FundsConfirmationFoundationServiceConstants.CHANNEL_ID);
		httpHeaders.add(transactioReqHeader,params.get(PSD2Constants.CORRELATION_ID));
		httpHeaders.add(correlationMuleReqHeader,"");
		httpHeaders.add(sourcesystem, FundsConfirmationFoundationServiceConstants.PSD2API);
		httpHeaders.add(sourceUserReqHeader, params.get(PSD2Constants.USER_IN_REQ_HEADER));
		httpHeaders.add(apiChannelCode, channelCode.toUpperCase());
		httpHeaders.add(partySourceNumber, "");
		if(params.get(PSD2Constants.TENANT_ID).equalsIgnoreCase("BOIUK")) {
			httpHeaders.add(apiChannelBrand, BrandCode3.NIGB.toString());
		}else if(params.get(PSD2Constants.TENANT_ID).equalsIgnoreCase("BOIROI")){
			httpHeaders.add(apiChannelBrand, BrandCode3.ROI.toString());
		}
		return httpHeaders;
	}
	
}
