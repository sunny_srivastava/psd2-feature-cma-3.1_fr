package com.capgemini.psd2.foundationservice.domestic.payments.boi.adapter.test;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringRunner;

import com.capgemini.psd2.foundationservice.domestic.payments.boi.adapter.DomesticPaymentsFoundationServiceAdapter;
import com.capgemini.psd2.foundationservice.domestic.payments.boi.adapter.client.DomesticPaymentsFoundationServiceClient;
import com.capgemini.psd2.foundationservice.domestic.payments.boi.adapter.delegate.DomesticPaymentsFoundationServiceDelegate;
import com.capgemini.psd2.foundationservice.domestic.payments.boi.adapter.transformer.DomesticPaymentsFoundationServiceTransformer;
import com.capgemini.psd2.pisp.domain.CustomDPaymentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.OBTransactionIndividualStatus1Code;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;


@RunWith(SpringRunner.class)
public class DomesticPaymentsFoundationServiceAdapterTest {

	@InjectMocks
	DomesticPaymentsFoundationServiceAdapter adapter;
	@Mock
	DomesticPaymentsFoundationServiceDelegate  delegate;
	@Mock
	DomesticPaymentsFoundationServiceClient client;
	@Mock
	DomesticPaymentsFoundationServiceTransformer transformer;
	
	
	
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);	
	}
	
	
	@Test
	public void contextLoads() {
	}
	
	@Test
	public void testretrieveStagedDomesticPayments()
	{
		CustomPaymentStageIdentifiers customPaymentStageIdentifiers= new CustomPaymentStageIdentifiers();
		Map<String, String> params= new HashMap<String,String>();
		customPaymentStageIdentifiers.setPaymentSubmissionId("122");
		customPaymentStageIdentifiers.setPaymentSetupVersion("2.0");
		adapter.retrieveStagedDomesticPayments(customPaymentStageIdentifiers, params);
	}
	
	@Test
	public void testprocessDomesticPayments()
	{
		
		CustomDPaymentsPOSTRequest DomesticPaymentsRequest =new CustomDPaymentsPOSTRequest();
		
		Map<String, String> params= new HashMap<String,String>();
		Map<String, OBTransactionIndividualStatus1Code> paymentStatusMap= new HashMap <String, OBTransactionIndividualStatus1Code>();
		
		
		adapter.processDomesticPayments(DomesticPaymentsRequest, paymentStatusMap, params);
		
	}
	
}
