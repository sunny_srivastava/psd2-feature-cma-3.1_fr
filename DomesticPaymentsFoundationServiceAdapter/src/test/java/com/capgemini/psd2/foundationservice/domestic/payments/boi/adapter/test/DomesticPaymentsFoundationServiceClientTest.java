package com.capgemini.psd2.foundationservice.domestic.payments.boi.adapter.test;

import java.lang.reflect.Method;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import com.capgemini.psd2.foundationservice.domestic.payments.boi.adapter.client.DomesticPaymentsFoundationServiceClient;
import com.capgemini.psd2.foundationservice.domestic.payments.boi.adapter.domain.PaymentInstructionProposalComposite;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.RestClientSync;

@RunWith(SpringRunner.class)
public class DomesticPaymentsFoundationServiceClientTest {
	
	@InjectMocks
	 DomesticPaymentsFoundationServiceClient  client;
	 
	@Mock
	RestClientSync restClient;
	@Mock
	private RestTemplate restTemplate;
	@Before
	public void setUp() throws Exception{
			MockitoAnnotations.initMocks(this);
			Method postConstruct= DomesticPaymentsFoundationServiceClient.class.getDeclaredMethod("init", null);
			postConstruct.setAccessible(true);
			postConstruct.invoke(client); 
	 

	}

	/**
	 * Context loads.
	 */
	@Test
	public void contextLoads() {
	}
	
	
	@Test
	public void testrestTransportForDomesticPaymentFoundationService()
	{
		RequestInfo reqInfo = new RequestInfo();
		HttpHeaders headers=new HttpHeaders();
		PaymentInstructionProposalComposite payment= new PaymentInstructionProposalComposite();
		payment=client.restTransportForDomesticPaymentFoundationService(reqInfo, PaymentInstructionProposalComposite.class, headers);
	}
	
	
	@Test
	public void testrestTransportForDomesticPaymentFoundationServicePost()
	{
		
		
		RequestInfo reqInfo = new RequestInfo();
		HttpHeaders headers=new HttpHeaders();
		PaymentInstructionProposalComposite payment= new PaymentInstructionProposalComposite();
		
		payment=client.restTransportForDomesticPaymentFoundationServicePost(reqInfo, payment, PaymentInstructionProposalComposite.class, headers);
	}

	
	
}
