package com.capgemini.psd2.pisp.isp.test.service.impl;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.consent.domain.PispConsent;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.adapter.InternationalScheduledPaymentsAdapter;
import com.capgemini.psd2.pisp.adapter.PispConsentAdapter;
import com.capgemini.psd2.pisp.domain.CustomISPConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.CustomISPaymentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomISPaymentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.GenericPaymentsResponse;
import com.capgemini.psd2.pisp.domain.OBWriteDataInternationalScheduled1;
import com.capgemini.psd2.pisp.domain.PaymentConsentsPlatformResource;
import com.capgemini.psd2.pisp.domain.PaymentsPlatformResource;
import com.capgemini.psd2.pisp.isp.comparator.ISPaymentsPayloadComparator;
import com.capgemini.psd2.pisp.isp.service.impl.ISPaymentsServiceImpl;
import com.capgemini.psd2.pisp.processing.adapter.service.PaymentSubmissionProcessingAdapter;
import com.capgemini.psd2.pisp.stage.operations.adapter.InternationalScheduledPaymentStagingAdapter;
import com.capgemini.psd2.pisp.status.PaymentConstants;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringJUnit4ClassRunner.class)
public class ISPaymentsServiceImplTest {

	@Mock
	private PaymentSubmissionProcessingAdapter<CustomISPaymentsPOSTRequest, CustomISPaymentsPOSTResponse> paymentsProcessingAdapter;

	@Mock
	private InternationalScheduledPaymentsAdapter isPaymentsAdapter;

	@Mock
	private InternationalScheduledPaymentStagingAdapter isPaymentConsentsAdapter;

	@InjectMocks
	private ISPaymentsServiceImpl service;
	
	@Mock
	private ISPaymentsPayloadComparator isPaymentsComparator;
	
	@Mock
	private RequestHeaderAttributes reqHeaderAtrributes;
	
	@Mock
	private PispConsentAdapter pispConsentAdapter;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}
	private CustomISPaymentsPOSTResponse getCustomISPaymentsPOSTResponse() throws JsonParseException, JsonMappingException, IOException
	{
		CustomISPaymentsPOSTResponse customISPaymentsPOSTResponse= new CustomISPaymentsPOSTResponse();
		String str= "{\"Data\":{\"InternationalScheduledPaymentId\":\"08a71246-4b8e-4fd5-83c1-bea2bf6bc75d\",\"ConsentId\":\"0c1c1db9-96b2-44cf-ad14-1a606834f1e6\",\"CreationDateTime\":\"2019-01-10T08:09:46+00:00\",\"Status\":\"INITIATIONCOMPLETED\",\"StatusUpdateDateTime\":\"2019-01-10T08:09:46+00:00\",\"ExpectedExecutionDateTime\":\"2019-01-11T08:09:46+00:00\",\"ExpectedSettlementDateTime\":\"2019-01-12T08:09:46+00:00\",\"Charges\":[{\"ChargeBearer\":\"SHARED\",\"Type\":\"Type1\",\"Amount\":{\"Amount\":\"69267655.70084\",\"Currency\":\"EUR\"}}],\"Initiation\":{\"InstructionIdentification\":\"ACME412\",\"EndToEndIdentification\":\"FRESCO.21302.GFX.20\",\"RequestedExecutionDateTime\":\"2019-05-02T00:00:00+00:00\",\"CurrencyOfTransfer\":\"GBP\",\"InstructedAmount\":{\"Amount\":\"656565456.88\",\"Currency\":\"GBP\"},\"ExchangeRateInformation\":{\"UnitCurrency\":\"GBP\",\"RateType\":\"ACTUAL\"},\"CreditorAccount\":{\"SchemeName\":\"UK.OBIE.SortCodeAccountNumber\",\"Identification\":\"08080021325698\",\"Name\":\"ACMEInc\",\"SecondaryIdentification\":\"0002\"},\"RemittanceInformation\":{\"Unstructured\":\"Internalopscode5120101\",\"Reference\":\"FRESCO-101\"}}}}";
		ObjectMapper mapper = new ObjectMapper();
		customISPaymentsPOSTResponse= mapper.readValue(str.getBytes(), CustomISPaymentsPOSTResponse.class);
		return customISPaymentsPOSTResponse;
	}
	private CustomISPaymentsPOSTRequest getCustomISPaymentsPOSTRequest() throws JsonParseException, JsonMappingException, IOException
	{
		CustomISPaymentsPOSTRequest customISPaymentsPOSTRequest= new CustomISPaymentsPOSTRequest();
		customISPaymentsPOSTRequest.setCreatedOn("sdfafd");
		OBWriteDataInternationalScheduled1 data= new OBWriteDataInternationalScheduled1();
		data.setConsentId("sadf");
		data.consentId("afsdf");
		data.setInitiation(getCustomISPaymentsPOSTResponse().getData().getInitiation());
		customISPaymentsPOSTRequest.setData(data);
	
		return customISPaymentsPOSTRequest;
	}
	private Map<String,Object>  getpaymentsPlatformResourceMapProcessStatusIncomplete()
	{
		Map<String,Object> paymentsPlatformResourceMap= new HashMap();
		PaymentConsentsPlatformResource paymentConsentsPlatformResponse = new PaymentConsentsPlatformResource();
		
		PaymentsPlatformResource paymentsPlatformResource= new PaymentsPlatformResource();
		paymentConsentsPlatformResponse.setTppDebtorDetails("false"); 
       paymentsPlatformResource.setCreatedAt("");
		paymentsPlatformResource.setStatus("InitiationPending");
		paymentsPlatformResource.setProccessState("Incomplete");
		paymentsPlatformResource.setStatusUpdateDateTime(""); 
        paymentsPlatformResourceMap.put(PaymentConstants.SUBMISSION, paymentsPlatformResource);
		paymentsPlatformResourceMap.put(PaymentConstants.CONSENT, paymentConsentsPlatformResponse); 
		return paymentsPlatformResourceMap;
	}
	private Map<String,Object>  getpaymentsPlatformResourceMapProcessStatuscompleted()
	{
		Map<String,Object> paymentsPlatformResourceMap= new HashMap();
		PaymentConsentsPlatformResource paymentConsentsPlatformResponse = new PaymentConsentsPlatformResource();
		
		PaymentsPlatformResource paymentsPlatformResource= new PaymentsPlatformResource();
		paymentConsentsPlatformResponse.setTppDebtorDetails("false"); 
       paymentsPlatformResource.setCreatedAt("");
		paymentsPlatformResource.setStatus("InitiationPending");
		paymentsPlatformResource.setProccessState("Completed");
		paymentsPlatformResource.setStatusUpdateDateTime(""); 
        paymentsPlatformResourceMap.put(PaymentConstants.SUBMISSION, paymentsPlatformResource);
		paymentsPlatformResourceMap.put(PaymentConstants.CONSENT, paymentConsentsPlatformResponse); 
		return paymentsPlatformResourceMap;
	}
	private Map<String,Object>  getpaymentsPlatformResourceMapProcessStatus()
	{
		Map<String,Object> paymentsPlatformResourceMap= new HashMap();
		PaymentConsentsPlatformResource paymentConsentsPlatformResponse = new PaymentConsentsPlatformResource();
		
		PaymentsPlatformResource paymentsPlatformResource= new PaymentsPlatformResource();
		paymentConsentsPlatformResponse.setTppDebtorDetails("false"); 
       paymentsPlatformResource.setCreatedAt("");
		paymentsPlatformResource.setStatus("InitiationPending");
		paymentsPlatformResource.setProccessState("Completed");
		paymentsPlatformResource.setStatusUpdateDateTime(""); 
        paymentsPlatformResourceMap.put(PaymentConstants.SUBMISSION, paymentsPlatformResource);
		paymentsPlatformResourceMap.put(PaymentConstants.CONSENT, paymentConsentsPlatformResponse); 
		return paymentsPlatformResourceMap;
	}
	private CustomISPConsentsPOSTResponse getCustomISPConsentsPOSTResponse() throws JsonParseException, JsonMappingException, IOException
	{
		CustomISPConsentsPOSTResponse customISPConsentsPOSTResponse= new CustomISPConsentsPOSTResponse();
		String str="{\"consentPorcessStatus\": \"PASS\",\"Data\": {\"ConsentId\": \"b7cc8d88-70d1-4722-9ff4-6b9b114cda92\",\"CreationDateTime\": \"2018-12-26T10:56:29+00:00\",\"Status\": \"AWAITINGAUTHORISATION\",\"StatusUpdateDateTime\": \"2018-12-26T10:56:29+00:00\",\"Permission\": \"CREATE\",\"CutOffDateTime\": \"2018-12-29T10:56:29+00:00\",\"ExpectedExecutionDateTime\": \"2018-12-27T10:56:29+00:00\",\"ExpectedSettlementDateTime\": \"2018-12-28T10:56:29+00:00\",\"Charges\": [{\"ChargeBearer\": \"SHARED\",\"Type\": \"Type1\",\"Amount\": {\"Amount\": \"355.10034\",\"Currency\": \"GBP\"}}],\"Initiation\": {\"InstructionIdentification\": \"ACME412\",\"EndToEndIdentification\": \"FRESCO.21302.GFX.20\",\"RequestedExecutionDateTime\": \"2019-05-02T00:00:00+00:00\",\"CurrencyOfTransfer\": \"GBP\",\"InstructedAmount\": {\"Amount\": \"3365.88\",\"Currency\": \"GBP\"},\"ExchangeRateInformation\": {\"UnitCurrency\": \"GBP\",\"RateType\": \"ACTUAL\"},\"CreditorAccount\": {\"SchemeName\": \"UK.OBIE.SortCodeAccountNumber\",\"Identification\": \"08080021325698\",\"Name\": \"ACME Inc\",\"SecondaryIdentification\": \"0002\"},\"RemittanceInformation\": {\"Unstructured\": \"Internal ops code 5120101\",\"Reference\": \"FRESCO-101\"}}},\"Risk\": {\"PaymentContextCode\": \"PARTYTOPARTY\"}}";
		ObjectMapper mapper = new ObjectMapper();
		customISPConsentsPOSTResponse= mapper.readValue(str.getBytes(), CustomISPConsentsPOSTResponse.class);
		return customISPConsentsPOSTResponse;
	}
	
	@Test
	public void createInternationalScheduledPaymentsResourceTest() throws JsonParseException, JsonMappingException, IOException {
		Mockito.when(isPaymentConsentsAdapter.retrieveStagedInternationalScheduledPaymentConsents(anyObject(), anyObject())).thenReturn(getCustomISPConsentsPOSTResponse());
		Mockito.when(isPaymentsComparator.comparePaymentDetails(anyObject(), anyObject(),anyObject())).thenReturn(0);
		Mockito.when(paymentsProcessingAdapter.preSubmissionProcessing(anyObject(), anyObject(), anyObject())).thenReturn(getpaymentsPlatformResourceMapProcessStatusIncomplete());
		GenericPaymentsResponse response = new GenericPaymentsResponse();
		response.setSubmissionId("4125");
		Mockito.when(isPaymentsAdapter.processISPayments(any(),any(), any())).thenReturn(response);
		service.createInternationalScheduledPaymentsResource(getCustomISPaymentsPOSTRequest());
	}
	
	@Test(expected=PSD2Exception.class)
	public void createInternationalScheduledPaymentsResourceTestProcessStatusCompletedWithException() throws JsonParseException, JsonMappingException, IOException {
		Mockito.when(isPaymentConsentsAdapter.retrieveStagedInternationalScheduledPaymentConsents(anyObject(), anyObject())).thenReturn(getCustomISPConsentsPOSTResponse());
		Mockito.when(isPaymentsComparator.comparePaymentDetails(anyObject(), anyObject(),anyObject())).thenReturn(0);
		Mockito.when(paymentsProcessingAdapter.preSubmissionProcessing(anyObject(), anyObject(), anyObject())).thenReturn(getpaymentsPlatformResourceMapProcessStatuscompleted());
		service.createInternationalScheduledPaymentsResource(getCustomISPaymentsPOSTRequest());
	}
	@Test
	public void createInternationalScheduledPaymentsResourceTestProcessStatusCompleted() throws JsonParseException, JsonMappingException, IOException {
		Mockito.when(isPaymentConsentsAdapter.retrieveStagedInternationalScheduledPaymentConsents(anyObject(), anyObject())).thenReturn(getCustomISPConsentsPOSTResponse());
		Mockito.when(isPaymentsComparator.comparePaymentDetails(anyObject(), anyObject(),anyObject())).thenReturn(0);
		Mockito.when(paymentsProcessingAdapter.preSubmissionProcessing(anyObject(), anyObject(), anyObject())).thenReturn(getpaymentsPlatformResourceMapProcessStatuscompleted());
		Mockito.when(isPaymentsAdapter.retrieveStagedISPaymentsResponse(anyObject(), anyObject())).thenReturn(getCustomISPaymentsPOSTResponse());
		service.createInternationalScheduledPaymentsResource(getCustomISPaymentsPOSTRequest());
	}
	
	@Test(expected=PSD2Exception.class)
	public void retrieveInternationalScheduledPaymentsResourceTestWithException() {
		Mockito.when(paymentsProcessingAdapter.prePaymentProcessGETFlows(anyObject(), anyObject())).thenReturn(getpaymentsPlatformResourceMapProcessStatuscompleted());
		PispConsent consent = new PispConsent();
		consent.setPsuId("12345");
		when(pispConsentAdapter.retrieveConsentByPaymentId(anyObject())).thenReturn(consent);
		service.retrieveInternationalScheduledPaymentsResource("asdfas");
	}
	@Test
	public void retrieveInternationalScheduledPaymentsResourceTest() throws JsonParseException, JsonMappingException, IOException {
		Mockito.when(paymentsProcessingAdapter.prePaymentProcessGETFlows(anyObject(), anyObject())).thenReturn(getpaymentsPlatformResourceMapProcessStatuscompleted());
		Mockito.when(isPaymentsAdapter.retrieveStagedISPaymentsResponse(anyObject(), anyObject())).thenReturn(getCustomISPaymentsPOSTResponse());
		PispConsent consent = new PispConsent();
		consent.setPsuId("12345");
		when(pispConsentAdapter.retrieveConsentByPaymentId(anyObject())).thenReturn(consent);
		service.retrieveInternationalScheduledPaymentsResource("asdfas");
	}
	
	@Test(expected=PSD2Exception.class)
	public void retrieveInternationalScheduledPaymentsResourceTestWithNullPispConsent() throws JsonParseException, JsonMappingException, IOException {
		Mockito.when(paymentsProcessingAdapter.prePaymentProcessGETFlows(anyObject(), anyObject())).thenReturn(getpaymentsPlatformResourceMapProcessStatuscompleted());
		when(pispConsentAdapter.retrieveConsentByPaymentId(any())).thenReturn(null);
		service.retrieveInternationalScheduledPaymentsResource("asdfas");
	}
	
	@After
	public void tearDown() {
		paymentsProcessingAdapter = null;
		isPaymentsAdapter = null;
		isPaymentConsentsAdapter = null;
		isPaymentsComparator = null;
		service = null;
	}
	
	

}