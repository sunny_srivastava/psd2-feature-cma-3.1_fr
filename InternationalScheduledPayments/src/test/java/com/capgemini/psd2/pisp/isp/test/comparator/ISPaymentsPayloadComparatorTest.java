package com.capgemini.psd2.pisp.isp.test.comparator;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.pisp.domain.CustomISPConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomISPConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.CustomISPaymentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.OBCashAccountDebtor3;
import com.capgemini.psd2.pisp.domain.OBDomestic1InstructedAmount;
import com.capgemini.psd2.pisp.domain.OBInternationalScheduled1;
import com.capgemini.psd2.pisp.domain.OBRemittanceInformation1;
import com.capgemini.psd2.pisp.domain.OBRisk1;
import com.capgemini.psd2.pisp.domain.OBRisk1DeliveryAddress;
import com.capgemini.psd2.pisp.domain.OBWriteDataInternationalScheduled1;
import com.capgemini.psd2.pisp.domain.OBWriteDataInternationalScheduledConsent1;
import com.capgemini.psd2.pisp.domain.OBWriteDataInternationalScheduledConsentResponse1;
import com.capgemini.psd2.pisp.domain.PaymentConsentsPlatformResource;
import com.capgemini.psd2.pisp.isp.comparator.ISPaymentsPayloadComparator;

@RunWith(SpringJUnit4ClassRunner.class)
public class ISPaymentsPayloadComparatorTest {

	@InjectMocks
	private ISPaymentsPayloadComparator comparator;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void contextLoads() {
	}

	@Test
	public void compareTestValue0() {

		CustomISPConsentsPOSTResponse paymentResponse = new CustomISPConsentsPOSTResponse();
		CustomISPConsentsPOSTResponse adaptedPaymentResponse = new CustomISPConsentsPOSTResponse();

		OBWriteDataInternationalScheduledConsentResponse1 data = new OBWriteDataInternationalScheduledConsentResponse1();
		paymentResponse.setData(data);
		OBInternationalScheduled1 initiation = new OBInternationalScheduled1();
		data.setInitiation(initiation);
		OBDomestic1InstructedAmount instructedAmount = new OBDomestic1InstructedAmount();
		initiation.setInstructedAmount(instructedAmount);
		instructedAmount.setAmount("823");
		paymentResponse.getData().setInitiation(new OBInternationalScheduled1());
		paymentResponse.setRisk(new OBRisk1());

		adaptedPaymentResponse.setData(data);
		data.setInitiation(initiation);
		adaptedPaymentResponse.setRisk(new OBRisk1());

		int i = comparator.compare(paymentResponse, adaptedPaymentResponse);
		assertEquals(0, i);

	}

	@Test
	public void compareTestValue1() {
		CustomISPConsentsPOSTResponse paymentResponse = new CustomISPConsentsPOSTResponse();
		CustomISPConsentsPOSTResponse adaptedPaymentResponse = new CustomISPConsentsPOSTResponse();

		OBWriteDataInternationalScheduledConsentResponse1 data = new OBWriteDataInternationalScheduledConsentResponse1();
		paymentResponse.setData(data);
		OBInternationalScheduled1 initiation = new  OBInternationalScheduled1();
		data.setInitiation(initiation);
		OBDomestic1InstructedAmount instructedAmount = new OBDomestic1InstructedAmount();
		initiation.setInstructedAmount(instructedAmount);
		instructedAmount.setAmount("823");
		paymentResponse.getData().getInitiation().setCurrencyOfTransfer("USD");
		paymentResponse.setRisk(new OBRisk1());

		adaptedPaymentResponse.setData(data);
		data.setInitiation(initiation);
		initiation.setInstructedAmount(instructedAmount);
		instructedAmount.setAmount("823");
		adaptedPaymentResponse.getData().getInitiation().setCurrencyOfTransfer("GBP");
		adaptedPaymentResponse.setRisk(new OBRisk1());

		int i = comparator.compare(paymentResponse, adaptedPaymentResponse);
		assertEquals(0, i);
	}

	@Test
	public void comparePaymentDetails_TrueDebtorDetails() {

		CustomISPConsentsPOSTResponse response = new CustomISPConsentsPOSTResponse();
		CustomISPConsentsPOSTRequest request = new CustomISPConsentsPOSTRequest();

		response.setData(new OBWriteDataInternationalScheduledConsentResponse1());
		response.getData().setInitiation(new OBInternationalScheduled1());
		response.getData().getInitiation().setInstructedAmount(new OBDomestic1InstructedAmount());
		response.getData().getInitiation().getInstructedAmount().setAmount("142");
		response.getData().getInitiation().setDebtorAccount(new OBCashAccountDebtor3());
		response.getData().getInitiation().getDebtorAccount().setName("test");

		request.setData(new OBWriteDataInternationalScheduledConsent1());
		request.getData().setInitiation(new OBInternationalScheduled1());
		request.getData().getInitiation().setInstructedAmount(new OBDomestic1InstructedAmount());
		request.getData().getInitiation().getInstructedAmount().setAmount("142");
		request.getData().getInitiation().setDebtorAccount(new OBCashAccountDebtor3());
		request.getData().getInitiation().getDebtorAccount().setName("test1");

		OBRisk1 risk = new OBRisk1();
		OBRisk1DeliveryAddress deliveryAddress = new OBRisk1DeliveryAddress();
		risk.setDeliveryAddress(deliveryAddress);
		List<String> addressLine = new ArrayList<>();
		deliveryAddress.setAddressLine(addressLine);
		addressLine.add("dublin east");
		List<String> countrySubDivision = new ArrayList<>();
		countrySubDivision.add("james street");
		deliveryAddress.setCountrySubDivision("warner bros2");
		request.setRisk(risk);

		PaymentConsentsPlatformResource paymentSetupPlatformResource = new PaymentConsentsPlatformResource();
		paymentSetupPlatformResource.setTppDebtorDetails("true");
		paymentSetupPlatformResource.setTppDebtorNameDetails("testdebtor");

		int i = comparator.comparePaymentDetails(response, request, paymentSetupPlatformResource);
		assertEquals(1, i);
	}

	@Test
	public void comparePaymentDetails_FalseDebtorDetails() {

		CustomISPConsentsPOSTResponse response = new CustomISPConsentsPOSTResponse();
		CustomISPaymentsPOSTRequest request = new CustomISPaymentsPOSTRequest();

		response.setData(new OBWriteDataInternationalScheduledConsentResponse1());
		response.getData().setInitiation(new OBInternationalScheduled1());
		response.getData().getInitiation().setInstructedAmount(new OBDomestic1InstructedAmount());
		response.getData().getInitiation().getInstructedAmount().setAmount("142");
		response.getData().getInitiation().setDebtorAccount(new OBCashAccountDebtor3());
		response.getData().getInitiation().getDebtorAccount().setName("test");

		request.setData(new OBWriteDataInternationalScheduled1());
		request.getData().setInitiation(new OBInternationalScheduled1());
		request.getData().getInitiation().setDebtorAccount(null);
		request.getData().getInitiation().setInstructedAmount(new OBDomestic1InstructedAmount());
		request.getData().getInitiation().getInstructedAmount().setAmount("142");
		request.getData().getInitiation().setRemittanceInformation(new OBRemittanceInformation1());
		request.getData().getInitiation().getRemittanceInformation().setReference("testReference");
		request.getData().getInitiation().getRemittanceInformation().setUnstructured("testunstructured");

		OBRisk1 risk = new OBRisk1();
		OBRisk1DeliveryAddress deliveryAddress = new OBRisk1DeliveryAddress();
		risk.setDeliveryAddress(deliveryAddress);
		deliveryAddress.setBuildingNumber("572");
		deliveryAddress.setCountry("Ireland");
		List<String> addressLine = new ArrayList<>();
		deliveryAddress.setAddressLine(addressLine);
		addressLine.add("warner bros");
		addressLine.add("warner bros2");
		List<String> countrySubDivision = new ArrayList<>();
		deliveryAddress.setCountrySubDivision("warner bros2");
		countrySubDivision.add("Dublin west");
		request.setRisk(risk);

		PaymentConsentsPlatformResource paymentSetupPlatformResource = new PaymentConsentsPlatformResource();
		paymentSetupPlatformResource.setTppDebtorDetails("false");
		paymentSetupPlatformResource.setTppDebtorNameDetails("testdebtor");

		int i = comparator.comparePaymentDetails(response, request, paymentSetupPlatformResource);
		assertEquals(1, i);
	}

	@Test
	public void comparePaymentDetails_NullRemittance() {

		CustomISPConsentsPOSTResponse response = new CustomISPConsentsPOSTResponse();
		CustomISPConsentsPOSTRequest request = new CustomISPConsentsPOSTRequest();

		response.setData(new OBWriteDataInternationalScheduledConsentResponse1());
		response.getData().setInitiation(new OBInternationalScheduled1());
		response.getData().getInitiation().setInstructedAmount(new OBDomestic1InstructedAmount());
		response.getData().getInitiation().getInstructedAmount().setAmount("142");
		response.getData().getInitiation().setDebtorAccount(new OBCashAccountDebtor3());
		response.getData().getInitiation().getDebtorAccount().setName("test");
		OBRisk1 risk = new OBRisk1();
		OBRisk1DeliveryAddress addressLine = new OBRisk1DeliveryAddress();
		List<String> addressLineResponse = new ArrayList<>();
		String e1 = "Ground Floor";
		addressLineResponse.add(e1);
		risk.setDeliveryAddress(addressLine);
		response.setRisk(risk);
		
		request.setData(new OBWriteDataInternationalScheduledConsent1());
		request.getData().setInitiation(new OBInternationalScheduled1());
		request.getData().getInitiation().setDebtorAccount(null);
		request.getData().getInitiation().setInstructedAmount(new OBDomestic1InstructedAmount());
		request.getData().getInitiation().getInstructedAmount().setAmount("142");
		request.getData().getInitiation().setRemittanceInformation(new OBRemittanceInformation1());
		request.getData().getInitiation().getRemittanceInformation().setReference("testReference");
		request.getData().getInitiation().getRemittanceInformation().setUnstructured("testunstructured");

		OBRisk1 risk1 = new OBRisk1();
		OBRisk1DeliveryAddress deliveryAddress = new OBRisk1DeliveryAddress();
		risk.setDeliveryAddress(deliveryAddress);
		deliveryAddress.setBuildingNumber("572");
		deliveryAddress.setCountry("Ireland");
		List<String> addressLine1 = new ArrayList<>();
		deliveryAddress.setAddressLine(addressLine1);
		List<String> countrySubDivision = new ArrayList<>();
		deliveryAddress.setCountrySubDivision("warner bros2");
		countrySubDivision.add("Dublin west");
		request.setRisk(risk1);

		PaymentConsentsPlatformResource paymentSetupPlatformResource = new PaymentConsentsPlatformResource();
		paymentSetupPlatformResource.setId("144");
		paymentSetupPlatformResource.setPaymentId("58923-001");
		paymentSetupPlatformResource.setPaymentConsentId("58923");
		paymentSetupPlatformResource.setSetupCmaVersion("3");
		String tppDebtorDetails = "false";
		paymentSetupPlatformResource.setTppDebtorDetails(tppDebtorDetails);
		paymentSetupPlatformResource.setTppDebtorNameDetails("Debtor");

		int i = comparator.comparePaymentDetails(response, request, paymentSetupPlatformResource);
		assertEquals(1, i);
	}

	@Test
	public void validateDebtorDetailsTest() {
		OBCashAccountDebtor3 debtorAccount = new OBCashAccountDebtor3();
		OBInternationalScheduled1 responseInitiation = new OBInternationalScheduled1();
		OBInternationalScheduled1 requestInitiation = new OBInternationalScheduled1();
		PaymentConsentsPlatformResource PaymentConsentsPlatformResource = new PaymentConsentsPlatformResource();
		PaymentConsentsPlatformResource.setTppDebtorDetails("false");
		requestInitiation.setDebtorAccount(debtorAccount);
		boolean result = comparator.validateDebtorDetails(responseInitiation, requestInitiation,
				PaymentConsentsPlatformResource);
		assertThat(result).isEqualTo(false);
		requestInitiation.setDebtorAccount(null);
		responseInitiation.setDebtorAccount(null);
		result = comparator.validateDebtorDetails(responseInitiation, requestInitiation,
				PaymentConsentsPlatformResource);
		assertThat(result).isEqualTo(true);
	}

	@After
	public void tearDown() {
		comparator = null;
	}

}