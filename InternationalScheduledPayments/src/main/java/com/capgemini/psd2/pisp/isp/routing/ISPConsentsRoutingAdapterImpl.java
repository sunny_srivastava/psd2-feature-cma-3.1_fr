package com.capgemini.psd2.pisp.isp.routing;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.domain.CustomISPConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomISPConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.GenericPaymentConsentResponse;
import com.capgemini.psd2.pisp.domain.OBExternalConsentStatus1Code;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.pisp.stage.operations.adapter.InternationalScheduledPaymentStagingAdapter;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Component("isPaymentConsentsStagingRoutingAdapter")
public class ISPConsentsRoutingAdapterImpl
		implements InternationalScheduledPaymentStagingAdapter, ApplicationContextAware {

	private InternationalScheduledPaymentStagingAdapter beanInstance;

	@Autowired
	private RequestHeaderAttributes reqHeaderAtrributes;

	@Value("${app.paymentSetupStagingAdapter}")
	private String beanName;

	/** The application context. */
	private ApplicationContext applicationContext;

	@Override
	public GenericPaymentConsentResponse processInternationalScheduledPaymentConsents(
			CustomISPConsentsPOSTRequest internationalScheduledPaymentRequest,
			CustomPaymentStageIdentifiers customStageIdentifiers, Map<String, String> params,
			OBExternalConsentStatus1Code successStatus, OBExternalConsentStatus1Code failureStatus) {
		return getRoutingInstance(beanName).processInternationalScheduledPaymentConsents(
				internationalScheduledPaymentRequest, customStageIdentifiers, getHeaderParams(params), successStatus,
				failureStatus);
	}

	@Override
	public CustomISPConsentsPOSTResponse retrieveStagedInternationalScheduledPaymentConsents(
			CustomPaymentStageIdentifiers customPaymentStageIdentifiers, Map<String, String> params) {
		return getRoutingInstance(beanName).retrieveStagedInternationalScheduledPaymentConsents(
				customPaymentStageIdentifiers, getHeaderParams(params));

	}

	public InternationalScheduledPaymentStagingAdapter getRoutingInstance(String adapterName) {
		if (beanInstance == null)
			beanInstance = (InternationalScheduledPaymentStagingAdapter) applicationContext.getBean(adapterName);
		return beanInstance;
	}

	@Override
	public void setApplicationContext(ApplicationContext context) {
		this.applicationContext = context;
	}

	private Map<String, String> getHeaderParams(Map<String, String> params) {
		if (NullCheckUtils.isNullOrEmpty(params))
			params = new HashMap<>(); //NOSONAR

		if (null != reqHeaderAtrributes.getToken() && null != reqHeaderAtrributes.getToken().getSeviceParams()) {
			params.put(PSD2Constants.CHANNEL_IN_REQ_HEADER,
					reqHeaderAtrributes.getToken().getSeviceParams().get(PSD2Constants.CHANNEL_NAME));
		}
		params.put(PSD2Constants.USER_IN_REQ_HEADER, reqHeaderAtrributes.getPsuId());
		params.put(PSD2Constants.CORRELATION_REQ_HEADER, reqHeaderAtrributes.getCorrelationId());
		params.put(PSD2Constants.TENANT_ID, reqHeaderAtrributes.getTenantId());
		return params;
	}

}
