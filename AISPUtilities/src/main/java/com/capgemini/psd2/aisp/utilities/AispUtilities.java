package com.capgemini.psd2.aisp.utilities;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAccessor;

import org.springframework.web.bind.annotation.RequestMethod;

import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.InternalServerErrorMessage;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

public class AispUtilities {

	private static ObjectMapper objectMapper = null;

	
	static {
		objectMapper = new ObjectMapper();
	}

	private AispUtilities() {

	}

	public static boolean isEmpty(String data) {
		boolean status = false;
		if (data == null || data.trim().length() == 0)
			status = true;

		return status;
	}
	

	/*
	 * This function is used to convert Hours,Minutes,Seconds to Milliseconds
	 */
	public static long getMilliSeconds(String idempotencyDuration) {
		long duration = 0L;
		if (isEmpty(idempotencyDuration))
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,InternalServerErrorMessage.PISP_IDEMPOTENCY_DURATION_INCORRECT_FORMAT));

		try {
			String inputData = idempotencyDuration.toUpperCase();
			if (inputData.contains("H")) {
				inputData = inputData.replaceAll("H", "");
				duration = (long) 60 * 60 * 1000;
			} else if (inputData.contains("M")) {
				inputData = inputData.replaceAll("M", "");
				duration = (long) 60 * 1000;
			} else if (inputData.contains("S")) {
				inputData = inputData.replaceAll("S", "");
				duration = 1000;
			}

			return Long.parseLong(inputData) * duration;

		} catch (NumberFormatException exception) {
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,exception.getMessage()));
		}
	}

	public static ObjectMapper getObjectMapper() {
		objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

		return objectMapper;
	}

	public static String getCurrentDateInISOFormat() {
		String isoDateTime = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ssZ").format(ZonedDateTime.now());
		isoDateTime = isoDateTime.substring(0, 22).concat(":").concat(isoDateTime.substring(22, 24));
		return isoDateTime;
	}
	
	public static String getTemporalAccessorInISOFormat(TemporalAccessor input){
		String isoDateTime = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ssZ").format(input);
		isoDateTime = isoDateTime.substring(0, 22).concat(":").concat(isoDateTime.substring(22, 24));
		return isoDateTime;
	}
	
	 public static String populateLinks(String id, String methodType, String selfUrl) {
		StringBuilder urlBuilder = new StringBuilder();
		urlBuilder.append(selfUrl);

		if (RequestMethod.POST.toString().equalsIgnoreCase(methodType)) {
			if (!urlBuilder.toString().endsWith("/"))
				urlBuilder.append("/");
			urlBuilder.append(id);
		}
		return urlBuilder.toString();
	}
}
