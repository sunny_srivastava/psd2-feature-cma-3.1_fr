
package com.capgemini.psd2.tpp.block;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.web.client.RestTemplate;

import com.capgemini.psd2.aisp.adapter.TppBlockAdapter;
import com.capgemini.psd2.exceptions.OBPSD2ExceptionUtility;
import com.capgemini.psd2.tpp.block.routing.adapter.impl.TppBlockRoutingAdapter;

@EnableEurekaClient
@SpringBootApplication
@ComponentScan(basePackages = { "com.capgemini.psd2" }, excludeFilters = {
		@ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, value = { OBPSD2ExceptionUtility.class }) })
public class TppBlockApplication {

	/** The context. */
	static ConfigurableApplicationContext context = null;

	public static void main(String[] args) {
		try {
			context = SpringApplication.run(TppBlockApplication.class, args);
		} catch (Exception e) {
			if (context != null) {
				SpringApplication.exit(context, () -> 1);
			}
			System.exit(1);
		}
	}

	@Bean(name = "tppBlockRoutingAdapter")
	public TppBlockAdapter getTppBlockAdapter() {
		return new TppBlockRoutingAdapter();
	}

	@Bean
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}
}