package com.capgemini.psd2.mongo.db.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.Protocol;
import com.capgemini.psd2.account.statements.mongo.db.adapter.impl.AccountStatementsMongoDbAdapterImpl;
import com.capgemini.psd2.aisp.adapter.AccountStatementsAdapter;


@Configuration
public class AccountStatementsMongoDbAdapterConfig {

	@Value("${aws.proxyHost:null}")
	private String proxyHost;

	@Value("${aws.proxyPort:null}")
	private String proxyPort;

	@Value("#{T(com.amazonaws.Protocol).valueOf('${aws.protocol:HTTPS}')}")
	private Protocol protocol;

	@Value("${aws.proxy:#{false}}")
	private boolean proxy;

	
	@Bean
	public ClientConfiguration getClientConfig() {
		ClientConfiguration clientConfiguration = null;
		if(proxy){
			clientConfiguration = new ClientConfiguration();
			clientConfiguration.setProxyHost(proxyHost);
			clientConfiguration.setProxyPort(Integer.valueOf(proxyPort));
			clientConfiguration.setProtocol(protocol);
		}
		return clientConfiguration;
	}
	
	@Bean(name = "accountStatementsMongoDbAdapter")
	public AccountStatementsAdapter mongoDBAdapter() {
		return new AccountStatementsMongoDbAdapterImpl();
	}
	
		
}
