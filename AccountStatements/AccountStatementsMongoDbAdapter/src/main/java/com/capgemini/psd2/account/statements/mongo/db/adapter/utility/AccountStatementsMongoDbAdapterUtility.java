package com.capgemini.psd2.account.statements.mongo.db.adapter.utility;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.account.statements.mongo.db.adapter.constants.AccountStatementMongoDbAdapterConstants;
import com.capgemini.psd2.account.statements.mongo.db.adapter.domain.DateRange;
import com.capgemini.psd2.aisp.domain.Links;
import com.capgemini.psd2.aisp.domain.Meta;
import com.capgemini.psd2.aisp.domain.OBReadDataStatement2;
import com.capgemini.psd2.aisp.domain.OBReadStatement2;
import com.capgemini.psd2.aisp.domain.OBStatement2;
import com.capgemini.psd2.aisp.mongo.db.adapter.domain.AccountStatementsCMA2;
import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Component
public class AccountStatementsMongoDbAdapterUtility {
	
	@Value("${app.isSandboxEnabled:false}")
	private boolean isSandboxEnabled;

	private static final Integer FIRST = Integer.valueOf(1);

	private int defaultTransactionDateRange = -12;

	
	public OBReadStatement2 populateStamentIdAndLinks(Page<AccountStatementsCMA2> pageRecords,
			String txnKey) {
		OBReadStatement2 statementResponse = new OBReadStatement2();

		statementResponse.setLinks(new Links());
		Meta meta = new Meta();
		
		statementResponse.setMeta(meta);

		List<AccountStatementsCMA2> mockStatement = pageRecords.getContent();

		List<OBStatement2> responseDataStatement = new ArrayList<>();
		responseDataStatement.addAll(mockStatement);
		OBReadDataStatement2 responseData = new OBReadDataStatement2();
		responseData.setStatement(responseDataStatement);
		statementResponse.setData(responseData);

		// Setting Links
		if(!isSandboxEnabled) {
			statementResponse.getLinks().setFirst(String.valueOf(FIRST));
			if (txnKey != null)
				statementResponse.getLinks().setSelf(String.valueOf(pageRecords.getNumber() + 1) + ":" + txnKey);
			else
				statementResponse.getLinks().setSelf(String.valueOf(pageRecords.getNumber() + 1));
			statementResponse.getLinks().setLast(String.valueOf(pageRecords.getTotalPages()));
			if (pageRecords.hasNext())
				statementResponse.getLinks().setNext(String.valueOf(pageRecords.getNumber() + 2));
			if (pageRecords.hasPrevious())
				statementResponse.getLinks().setPrev(String.valueOf(pageRecords.getNumber()));
			
			statementResponse.getMeta().setTotalPages(pageRecords.getTotalPages());
		} else {
			statementResponse.getMeta().setTotalPages(1);
			
		} 
		return statementResponse;
	}

	public DateRange createTransactionDateRange(Map<String, String> params) 
	{
		String fromConsentDtString = params.get(AccountStatementMongoDbAdapterConstants.REQUESTED_FROM_CONSENT_DATETIME);
		String toConsentDtString = params.get(AccountStatementMongoDbAdapterConstants.REQUESTED_TO_CONSENT_DATETIME);
		String fromFilterDtString = params.get(AccountStatementMongoDbAdapterConstants.REQUESTED_FROM_DATETIME);
				
		String toFilterDtString = params.get(AccountStatementMongoDbAdapterConstants.REQUESTED_TO_DATETIME);
			
		String expirationDtString = params.get(AccountStatementMongoDbAdapterConstants.CONSENT_EXPIRATION_DATETIME);

		DateRange transactionDateRange = new DateRange();	

		//Consent Expiry 
		if (NullCheckUtils.isNullOrEmpty(expirationDtString)) { 
			Calendar calendar = Calendar.getInstance();
			calendar.add(Calendar.DATE, 1);
			transactionDateRange.setConsentExpiryDateTime(toLocalDateTime(calendar));
		} else {
			transactionDateRange.setConsentExpiryDateTime(LocalDateTime.parse(expirationDtString));
		}

		//TransactionFromDateTime
		if (NullCheckUtils.isNullOrEmpty(fromConsentDtString)) {
			Calendar calendar = Calendar.getInstance();
			calendar.add(Calendar.MONTH, defaultTransactionDateRange);
			calendar.set(Calendar.HOUR_OF_DAY, 0);
			calendar.set(Calendar.SECOND, 0);
			calendar.set(Calendar.MINUTE, 0);
			transactionDateRange.setTransactionFromDateTime(toLocalDateTime(calendar));
		} else {
			transactionDateRange.setTransactionFromDateTime(LocalDateTime.parse(fromConsentDtString)); 
		}

		//TransactionToDateTime
		if (NullCheckUtils.isNullOrEmpty(toConsentDtString)) {
			Calendar calendar = Calendar.getInstance();
			calendar.set(Calendar.HOUR_OF_DAY, 0);
			calendar.set(Calendar.SECOND, 0);
			calendar.set(Calendar.MINUTE, 0);
			transactionDateRange.setTransactionToDateTime(toLocalDateTime(calendar));
		} else {
			transactionDateRange.setTransactionToDateTime(LocalDateTime.parse(toConsentDtString));
		}
		//Request DateTime
		transactionDateRange.setRequestDateTime(LocalDateTime.now()); 
		//FilterFromDate
		if (NullCheckUtils.isNullOrEmpty(params.get("fromStatementDateTime"))) {		
			transactionDateRange.setFilterFromDate(transactionDateRange.getTransactionFromDateTime());
		} else {
			transactionDateRange.setFilterFromDate(LocalDateTime.parse(fromFilterDtString));
		}
		//FilterToDate
		if (NullCheckUtils.isNullOrEmpty(params.get("toStatementDateTime"))) {
			transactionDateRange.setFilterToDate(transactionDateRange.getTransactionToDateTime());
		} else {
			transactionDateRange.setFilterToDate(LocalDateTime.parse(toFilterDtString));
		}
		//To > ConExp
		if (transactionDateRange.getTransactionToDateTime().compareTo(transactionDateRange.getConsentExpiryDateTime())==0) {
			transactionDateRange.setTransactionToDateTime(transactionDateRange.getConsentExpiryDateTime());
		}
		return transactionDateRange;
	}

	private static LocalDateTime toLocalDateTime(Calendar calendar) {
		if (calendar == null) {
			return null;
		}
		TimeZone tz = calendar.getTimeZone();
		ZoneId zid = tz == null ? ZoneId.systemDefault() : tz.toZoneId();
		return LocalDateTime.ofInstant(calendar.toInstant(), zid);
	}
	//TransactionDateRange Validation
	public DateRange fsCallFilter(DateRange transactionDateRange) {

		if (transactionDateRange.getRequestDateTime().isAfter(transactionDateRange.getConsentExpiryDateTime())
				|| transactionDateRange.getFilterFromDate().isAfter(transactionDateRange.getFilterToDate())) {
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.TRANSACTION_CONSENT_EXPIRED);
		}
		else if (transactionDateRange.getTransactionFromDateTime().isAfter(transactionDateRange.getConsentExpiryDateTime())
				|| transactionDateRange.getTransactionFromDateTime().isAfter(transactionDateRange.getFilterToDate())
				|| transactionDateRange.getFilterFromDate().isAfter(transactionDateRange.getTransactionToDateTime())) {
			transactionDateRange.setEmptyResponse(true);
		}
		else if (transactionDateRange.getTransactionFromDateTime().isBefore(transactionDateRange.getFilterFromDate())
				||transactionDateRange.getTransactionFromDateTime().isEqual(transactionDateRange.getFilterFromDate())) {
			if (transactionDateRange.getFilterToDate().isBefore(transactionDateRange.getTransactionToDateTime())
					||transactionDateRange.getFilterToDate().isEqual(transactionDateRange.getTransactionToDateTime())) {
				transactionDateRange.setNewFilterFromDate(transactionDateRange.getFilterFromDate());
				transactionDateRange.setNewFilterToDate(transactionDateRange.getFilterToDate());

			} else {
				transactionDateRange.setNewFilterFromDate(transactionDateRange.getFilterFromDate());
				transactionDateRange.setNewFilterToDate(transactionDateRange.getTransactionToDateTime());
			}
		}
		else if (transactionDateRange.getFilterToDate().isBefore(transactionDateRange.getTransactionToDateTime())
				||transactionDateRange.getFilterToDate().isEqual(transactionDateRange.getTransactionToDateTime())) {
			transactionDateRange.setNewFilterFromDate(transactionDateRange.getTransactionFromDateTime());
			transactionDateRange.setNewFilterToDate(transactionDateRange.getFilterToDate());
		}
		else {
			transactionDateRange.setNewFilterFromDate(transactionDateRange.getTransactionFromDateTime());
			transactionDateRange.setNewFilterToDate(transactionDateRange.getTransactionToDateTime());
		}		
		return transactionDateRange;		
	}
}
