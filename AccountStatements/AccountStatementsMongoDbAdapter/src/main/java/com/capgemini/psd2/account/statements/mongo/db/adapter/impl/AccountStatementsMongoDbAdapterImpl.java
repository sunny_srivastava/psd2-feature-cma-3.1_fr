package com.capgemini.psd2.account.statements.mongo.db.adapter.impl;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Component;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.SdkClientException;
import com.amazonaws.auth.DefaultAWSCredentialsProviderChain;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.S3Object;
import com.capgemini.psd2.account.statements.mongo.db.adapter.constants.AccountStatementMongoDbAdapterConstants;
import com.capgemini.psd2.account.statements.mongo.db.adapter.domain.DateRange;
import com.capgemini.psd2.account.statements.mongo.db.adapter.repository.AccountStatementFileRepository;
import com.capgemini.psd2.account.statements.mongo.db.adapter.repository.AccountStatementTransactionRepository;
import com.capgemini.psd2.account.statements.mongo.db.adapter.repository.AccountStatementsRepository;
import com.capgemini.psd2.account.statements.mongo.db.adapter.utility.AccountStatementsMongoDbAdapterUtility;
import com.capgemini.psd2.aisp.adapter.AccountStatementsAdapter;
import com.capgemini.psd2.aisp.domain.Links;
import com.capgemini.psd2.aisp.domain.Meta;
import com.capgemini.psd2.aisp.domain.OBReadDataTransaction6;
import com.capgemini.psd2.aisp.domain.OBReadStatement2;
import com.capgemini.psd2.aisp.domain.OBReadTransaction6;
import com.capgemini.psd2.aisp.domain.OBTransaction6;
import com.capgemini.psd2.aisp.mongo.db.adapter.domain.AccountStatementsCMA2;
import com.capgemini.psd2.aisp.mongo.db.adapter.domain.AccountTransactionCMA2;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountSatementsFileResponse;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountStatementsResponse;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountTransactionResponse;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.InternalServerErrorMessage;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.mongo.db.config.AccountAwsObject;
import com.capgemini.psd2.utilities.SandboxValidationConfig;
import com.capgemini.psd2.utilities.SandboxValidationUtility;
import com.capgemini.psd2.validator.PSD2Validator;

@Component
public class AccountStatementsMongoDbAdapterImpl implements AccountStatementsAdapter {

	private static final Integer FIRST = Integer.valueOf(1);

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(AccountStatementsMongoDbAdapterImpl.class);

	@Autowired
	private AccountStatementsRepository repository;

	@Autowired
	private AccountStatementFileRepository fileRepository;

	@Autowired
	private AccountStatementTransactionRepository accountStatementTransactionRepository;

	@Autowired
	private AccountStatementsMongoDbAdapterUtility accountStatementUtility;

	/** The psd2 validator. */
	@Autowired
	private PSD2Validator psd2Validator;

	@Autowired
	private SandboxValidationConfig sandboxValidationConfig;

	@Autowired
	private ClientConfiguration clientConfig;

	@Value("${app.awsRegion}")
	private String awsRegion;

	@Value("${app.awsS3BucketName}")
	private String awsS3BucketName;

	@Value("${app.awsS3BucketFilePath}")
	private String awsS3BucketFilePath;

	@Value("${app.awsReportfileName}")
	private String awsReportfileName;

	@Autowired
	public AccountAwsObject s3fullobject;

	@Autowired
	public SandboxValidationUtility utility;

	@Value("${app.isSandboxEnabled:false}")
	private boolean isSandboxEnabled;
	
	
	
	@Override
	public PlatformAccountStatementsResponse retrieveAccountStatements(AccountMapping accountMapping,
			Map<String, String> params) {


		String accountId = accountMapping.getAccountDetails().get(0).getAccountId();
		String accountNumber = accountMapping.getAccountDetails().get(0).getAccountNumber();
		String accountNsc = accountMapping.getAccountDetails().get(0).getAccountNSC();
		LOG.info("TenantId : " + params.get(PSD2Constants.TENANT_ID));

		LocalDateTime fromLocalDateTime;
		LocalDateTime toLocalDateTime;

		DateRange transactionDateRange = accountStatementUtility.createTransactionDateRange(params);
		DateRange decision = accountStatementUtility.fsCallFilter(transactionDateRange);

		if (decision.isEmptyResponse()) {
			return new PlatformAccountStatementsResponse();
		} else {

			fromLocalDateTime = decision.getNewFilterFromDate();
			toLocalDateTime = decision.getNewFilterToDate();
		}

		Integer pageNo = Integer.parseInt(params.get(AccountStatementMongoDbAdapterConstants.REQUESTED_PAGE_NUMBER));
		Integer pageSize = Integer.parseInt(params.get(AccountStatementMongoDbAdapterConstants.REQUESTED_PAGE_SIZE));

		final PageRequest pageReq = new PageRequest(pageNo - 1, pageSize, Direction.ASC, "startDateTime");

		LocalDateTime mongoFromDt = fromLocalDateTime.minusNanos(1);
		LocalDateTime mongoToDt = toLocalDateTime.plusNanos(1000000);

		Instant mongoFromDateTime = Instant.parse(mongoFromDt.toString() + ZoneId.of("UTC").normalized());
		Instant mongoToDateTime = Instant.parse(mongoToDt.toString() + ZoneId.of("UTC").normalized());

		Page<AccountStatementsCMA2> pageRecords;

		try {
			if (utility.isValidPsuId(accountMapping.getPsuId())) {
				throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
						InternalServerErrorMessage.SANDBOX_MOCKED_ERROR));
			}
			pageRecords = repository.findByAccountNumberAndAccountNSCAndStartDateTimeCopyBetween(accountNumber,
					accountNsc, mongoFromDateTime, mongoToDateTime, pageReq);
		} catch (DataAccessResourceFailureException e) {
			LOG.error(e.getMessage(),e);
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
					InternalServerErrorMessage.CONNECTION_ERROR));
		}
		return platformAccountStatementsResponse(pageRecords,accountId);

	}

	@Override
	public PlatformAccountStatementsResponse retrieveAccountStatementsByStatementId(AccountMapping accountMapping,
			Map<String, String> params) {

		Page<AccountStatementsCMA2> pageRecords = null;

		String accountId = accountMapping.getAccountDetails().get(0).getAccountId();
		String accountNumber = accountMapping.getAccountDetails().get(0).getAccountNumber();
		String accountNSC = accountMapping.getAccountDetails().get(0).getAccountNSC();
		String statementId = params.get(AccountStatementMongoDbAdapterConstants.REQUESTED_STATEMENT);

		LOG.info("TenantId : " + params.get(PSD2Constants.TENANT_ID));

		Integer pageNo = Integer.parseInt(params.get(AccountStatementMongoDbAdapterConstants.REQUESTED_PAGE_NUMBER));
		Integer pageSize = Integer.parseInt(params.get(AccountStatementMongoDbAdapterConstants.REQUESTED_PAGE_SIZE));

		final PageRequest pageReq = new PageRequest(pageNo - 1, pageSize);

		try {

			pageRecords = repository.findByAccountNumberAndAccountNSCAndStatementId(accountNumber, accountNSC,
					statementId, pageReq);
		} catch (DataAccessResourceFailureException e) {
			LOG.error(e.getMessage(),e);
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
					InternalServerErrorMessage.CONNECTION_ERROR));
		}

		return platformAccountStatementsResponse(pageRecords,accountId);

	}
	/**
	 *Helper method to get the response of account statements from the platform. 
	 **/
	private PlatformAccountStatementsResponse platformAccountStatementsResponse(Page<AccountStatementsCMA2> pageRecords,String accountId){
		PlatformAccountStatementsResponse platformAccountStatementsResponse = new PlatformAccountStatementsResponse();
		
		String txnKey = null;
		if (!pageRecords.getContent().isEmpty()) {
			pageRecords.getContent().forEach(mockResponseData1 -> mockResponseData1.setAccountId(accountId));

			AccountStatementsCMA2 mockResponseData1 = pageRecords.getContent().get(0);
			txnKey = mockResponseData1.getTxnRetrievalKey();
		}

		OBReadStatement2 data = accountStatementUtility.populateStamentIdAndLinks(pageRecords, txnKey);
		platformAccountStatementsResponse.setoBReadStatement2(data);
		return platformAccountStatementsResponse;
	}

	@Override
	public PlatformAccountTransactionResponse retrieveAccountTransactionsByStatementsId(AccountMapping accountMapping,
			Map<String, String> params) {

		PlatformAccountTransactionResponse platformAccountTransactionResponse = new PlatformAccountTransactionResponse();

		Page<AccountTransactionCMA2> pageRecords;

		String accountId = accountMapping.getAccountDetails().get(0).getAccountId();
		String accountNumber = accountMapping.getAccountDetails().get(0).getAccountNumber();
		String accountNsc = accountMapping.getAccountDetails().get(0).getAccountNSC();
		String statementId = params.get(AccountStatementMongoDbAdapterConstants.REQUESTED_STATEMENT);

		LOG.info("TenantId : " + params.get(PSD2Constants.TENANT_ID));

		String filter = params.get(AccountStatementMongoDbAdapterConstants.REQUESTED_TXN_FILTER);
		Integer pageNo = Integer.parseInt(params.get(AccountStatementMongoDbAdapterConstants.REQUESTED_PAGE_NUMBER));
		Integer pageSize = Integer.parseInt(params.get(AccountStatementMongoDbAdapterConstants.REQUESTED_PAGE_SIZE));

		final PageRequest pageReq = new PageRequest(pageNo - 1, pageSize, Direction.ASC, "bookingDateTime");

		if ("ALL".equals(filter)) {
			try {

				pageRecords = accountStatementTransactionRepository.findByAccountNumberAndAccountNSCAndStatementId(
						accountNumber, accountNsc, statementId, pageReq);

			} catch (DataAccessResourceFailureException e) {
				LOG.error(e.getMessage(),e);
				throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
						InternalServerErrorMessage.CONNECTION_ERROR));
			}
		} else {
			try {
				pageRecords = accountStatementTransactionRepository
						.findByAccountNumberAndAccountNSCAndStatementIdAndCreditDebitIndicator(accountNumber,
								accountNsc, statementId, filter, pageReq);
			} catch (DataAccessResourceFailureException e) {
				LOG.error(e.getMessage(),e);
				throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
						InternalServerErrorMessage.CONNECTION_ERROR));
			}
		}

		String txnKey = null;
		if (!pageRecords.getContent().isEmpty()) {
			pageRecords.getContent().forEach(mockResponseData1 -> {
				mockResponseData1.setAccountId(accountId);
				validateFields(mockResponseData1);
			});
			AccountTransactionCMA2 mockResponseData1 = pageRecords.getContent().get(0);
			txnKey = mockResponseData1.getTxnRetrievalKey();
		}

		OBReadTransaction6 transactionResponsePreValidation = populateIdAndLinks(pageRecords, accountId, txnKey);
		platformAccountTransactionResponse.setoBReadTransaction6(transactionResponsePreValidation);
		return platformAccountTransactionResponse;
	}

	private OBReadTransaction6 populateIdAndLinks(Page<AccountTransactionCMA2> pageRecords, String accountId,
			String txnKey) {

		OBReadTransaction6 transactionResponse = new OBReadTransaction6();
		transactionResponse.setLinks(new Links());
		transactionResponse.setMeta(new Meta());

		if (!pageRecords.getContent().isEmpty()) {
			pageRecords.getContent().get(0).setAccountId(accountId);
			transactionResponse.getMeta()
					.setFirstAvailableDateTime(pageRecords.getContent().get(0).getFirstAvailableDateTime());
			transactionResponse.getMeta()
					.setLastAvailableDateTime(pageRecords.getContent().get(0).getLastAvailableDateTime());

		}

		List<AccountTransactionCMA2> mockTransaction = pageRecords.getContent();

		List<OBTransaction6> responseDataTransaction = new ArrayList<>();
		responseDataTransaction.addAll(mockTransaction);
		OBReadDataTransaction6 responseData = new OBReadDataTransaction6();
		responseData.setTransaction(responseDataTransaction);
		transactionResponse.setData(responseData);

		// Setting Links

		if (txnKey != null)
			transactionResponse.getLinks().setSelf(String.valueOf(pageRecords.getNumber() + 1) + ":" + txnKey);
		else
			transactionResponse.getLinks().setSelf(String.valueOf(pageRecords.getNumber() + 1));

		if (!isSandboxEnabled) {

			transactionResponse.getLinks().setFirst(String.valueOf(FIRST));
			transactionResponse.getLinks().setLast(String.valueOf(pageRecords.getTotalPages()));
			if (pageRecords.hasNext())
				transactionResponse.getLinks().setNext(String.valueOf(pageRecords.getNumber() + 2));
			if (pageRecords.hasPrevious())
				transactionResponse.getLinks().setPrev(String.valueOf(pageRecords.getNumber()));

		}

		return transactionResponse;
	}

	private AccountTransactionCMA2 validateFields(AccountTransactionCMA2 mockTransaction) {
		psd2Validator.validate(mockTransaction.getBankTransactionCode());
		psd2Validator.validateEnum(CurrencyEnum.class, mockTransaction.getAmount().getCurrency());
		psd2Validator.validate(mockTransaction.getAmount());
		psd2Validator.validateEnum(CurrencyEnum.class, mockTransaction.getBalance().getAmount().getCurrency());
		psd2Validator.validate(mockTransaction.getBalance().getAmount());
		psd2Validator.validate(mockTransaction);
		return mockTransaction;
	}

	enum CurrencyEnum {
		GBP, EUR;
	}

	@Override
	public PlatformAccountSatementsFileResponse downloadStatementFileByStatementsId(AccountMapping accountMapping,
			Map<String, String> params) {

		PlatformAccountSatementsFileResponse accountSatementsFileResponse = null;

		String statementId = params.get(AccountStatementMongoDbAdapterConstants.REQUESTED_STATEMENT);
		Page<AccountStatementsCMA2> pageRecords = null;

		LOG.info("TenantId : " + params.get(PSD2Constants.TENANT_ID));

		String accountNumber = accountMapping.getAccountDetails().get(0).getAccountNumber();
		String accountNSC = accountMapping.getAccountDetails().get(0).getAccountNSC();

		Integer pageNo = 1;
		Integer pageSize = 1;

		final PageRequest pageReq = new PageRequest(pageNo - 1, pageSize);
		try {
			if (utility.isValidPsuIdForDownload(accountMapping.getPsuId())) {
				throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
						InternalServerErrorMessage.SANDBOX_MOCKED_ERROR));
			}
			if (utility.isValidPsuId(accountMapping.getPsuId())) {
				throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
						InternalServerErrorMessage.SANDBOX_MOCKED_ERROR));
			}
			pageRecords = repository.findByAccountNumberAndAccountNSCAndStatementId(accountNumber, accountNSC,
					statementId, pageReq);
			if (pageRecords != null && !pageRecords.getContent().isEmpty()) {

				accountSatementsFileResponse = new PlatformAccountSatementsFileResponse();

				// code to download statement pdf file amazon from s3 bucket
				S3Object fullObject;
				AmazonS3 s3client;

				if (clientConfig != null) {

					s3client = AmazonS3ClientBuilder.standard()
							.withCredentials(DefaultAWSCredentialsProviderChain.getInstance())
							.withClientConfiguration(clientConfig).withRegion(awsRegion).build();

				} else {

					s3client = AmazonS3ClientBuilder.standard()
							.withCredentials(DefaultAWSCredentialsProviderChain.getInstance()).withRegion(awsRegion)
							.build();

				}

				fullObject = s3fullobject.getAccountAwsObjectS3Object(s3client, awsS3BucketName, awsS3BucketFilePath);

				if (fullObject != null && fullObject.getObjectContent() != null
						&& fullObject.getObjectMetadata() != null) {

					byte[] targetArray = getPDFByteArray(fullObject.getObjectContent());
					accountSatementsFileResponse.setFileByteArray(targetArray);
					accountSatementsFileResponse.setFileName(awsReportfileName);
				}
			}

		} catch (DataAccessResourceFailureException | SdkClientException e) {
			LOG.info("exception  : " + e.getMessage());
			LOG.error(e.getMessage(),e);
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
					InternalServerErrorMessage.CONNECTION_ERROR));
		}

		return accountSatementsFileResponse;

	}

	private byte[] getPDFByteArray(InputStream stream) {
		byte[] buffer = new byte[8192];
		ByteArrayOutputStream baos = new ByteArrayOutputStream();

		int bytesRead;
		try {
			while ((bytesRead = stream.read(buffer)) != -1) {
				baos.write(buffer, 0, bytesRead);
			}
		} catch (IOException e) {
			LOG.error(e.getMessage(),e);
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
					InternalServerErrorMessage.CONNECTION_ERROR));
		}
		return baos.toByteArray();
	}

}