package com.capgemini.psd2.account.statements.mongo.db.adapter.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;

import com.capgemini.psd2.aisp.mongo.db.adapter.domain.SandboxAccountStatements;

public interface SandboxStatementsRepository extends MongoRepository<SandboxAccountStatements, String>{

	public Page<SandboxAccountStatements> findByAccountNumberAndAccountNSCAndStatementId(String accountNumber, String accountNSC, String statementId, Pageable pageable);

}
