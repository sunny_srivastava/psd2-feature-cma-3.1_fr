package com.capgemini.psd2.account.statements.mongo.db.adapter.test.mock.data;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import com.capgemini.psd2.aisp.domain.OBActiveOrHistoricCurrencyAndAmount5;
import com.capgemini.psd2.aisp.domain.OBActiveOrHistoricCurrencyAndAmount6;
import com.capgemini.psd2.aisp.domain.OBActiveOrHistoricCurrencyAndAmount7;
import com.capgemini.psd2.aisp.domain.OBActiveOrHistoricCurrencyAndAmount8;
import com.capgemini.psd2.aisp.domain.OBCreditDebitCode0;
import com.capgemini.psd2.aisp.domain.OBExternalStatementType1Code;
import com.capgemini.psd2.aisp.domain.OBStatement2StatementAmount;
import com.capgemini.psd2.aisp.domain.OBStatement2StatementBenefit;
import com.capgemini.psd2.aisp.domain.OBStatement2StatementDateTime;
import com.capgemini.psd2.aisp.domain.OBStatement2StatementFee;
import com.capgemini.psd2.aisp.domain.OBStatement2StatementInterest;
import com.capgemini.psd2.aisp.domain.OBStatement2StatementRate;
import com.capgemini.psd2.aisp.domain.OBStatement2StatementValue;
import com.capgemini.psd2.aisp.mongo.db.adapter.domain.AccountStatementsCMA2;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AccountMapping;


public class AccountStatementTestData {

	static AccountStatementsCMA2 data = new AccountStatementsCMA2();
	static OBStatement2StatementAmount statementAmount = new OBStatement2StatementAmount();
	static OBActiveOrHistoricCurrencyAndAmount5 benefitAmount = new OBActiveOrHistoricCurrencyAndAmount5();
	static OBStatement2StatementBenefit benefit = new OBStatement2StatementBenefit();
	static OBStatement2StatementFee statementFee = new OBStatement2StatementFee();
	static OBActiveOrHistoricCurrencyAndAmount6 statementAmount1 = new OBActiveOrHistoricCurrencyAndAmount6();
	static OBStatement2StatementInterest statementInterest = new OBStatement2StatementInterest();
	static OBActiveOrHistoricCurrencyAndAmount7 interestAmount = new OBActiveOrHistoricCurrencyAndAmount7();
	static OBStatement2StatementDateTime statementDateTime = new OBStatement2StatementDateTime();
	static OBStatement2StatementRate statementRate = new OBStatement2StatementRate();
	static OBStatement2StatementValue statementValue = new OBStatement2StatementValue();
	static OBActiveOrHistoricCurrencyAndAmount8 statementAmount3 = new OBActiveOrHistoricCurrencyAndAmount8();
	
	public static String getTestAccountId() {
		return "269c3ff5-d7f8-419b-a3b9-7136c5b4611a";
	}

	public static String getTestBankID() {
		return "TestBankID";
	}
	
	public static AccountMapping getMockAccountMapping() {
		AccountMapping mapping = new AccountMapping();
		mapping.setTppCID("tpp123");
		mapping.setPsuId("user123");
		List<AccountDetails> selectedAccounts = new ArrayList<>();
		AccountDetails accountRequest = new AccountDetails();
		accountRequest.setAccountId("269c3ff5-d7f8-419b-a3b9-7136c5b4611a");
		accountRequest.setAccountNSC("SC802001");
		accountRequest.setAccountNumber("10203345");
		selectedAccounts.add(accountRequest);
		mapping.setAccountDetails(selectedAccounts);
		return mapping;
	}
	
	public static Page<AccountStatementsCMA2> getAccountStatementsDataPage(){
		List<AccountStatementsCMA2> dataList = new ArrayList<AccountStatementsCMA2>();
		
		data.setAccountId("269c3ff5-d7f8-419b-a3b9-7136c5b4611a");
		data.setPsuId("88888888");
		data.setAccountNumber("10203345");
		data.setAccountNSC("SC802001");
		data.setStatementId("8sfhke-sifhkeuf-97819");
		data.setStatementReference("002");
		data.setType(OBExternalStatementType1Code.REGULARPERIODIC);
		data.setStartDateTime("2020-07-31T00:00:00+00:00");
		data.setEndDateTime("2021-12-31T23:59:59+00:00");
		data.setCreationDateTime("2017-09-01T00:00:00+00:00");
		List<String> statementDescription = new ArrayList<String>();
		statementDescription.add("August 2017 Statement");
		data.setStatementDescription(statementDescription);
		List<OBStatement2StatementBenefit> benefitList = new ArrayList<OBStatement2StatementBenefit>();
		benefitAmount.setAmount("10.00");
		benefitAmount.setCurrency("GBP");
		benefit.setAmount(benefitAmount);
		benefit.setType("CASHBACK");
		benefitList.add(benefit);
		data.setStatementBenefit(benefitList);
		List<OBStatement2StatementFee> statementFeeList = new ArrayList<OBStatement2StatementFee>();
		statementAmount1.setAmount("65.98");
		statementAmount1.setCurrency("GBP");
		statementFee.setAmount(statementAmount1);
		statementFee.setCreditDebitIndicator(OBCreditDebitCode0.CREDIT);
		statementFee.setType("ANNUAL");
		statementFeeList.add(statementFee);
		data.setStatementFee(statementFeeList);
		List<OBStatement2StatementInterest> statementInterestList = new ArrayList<OBStatement2StatementInterest>();
		interestAmount.setAmount("45.09");
		interestAmount.setCurrency("GBP");
		statementInterest.setAmount(interestAmount);
		statementInterest.setCreditDebitIndicator(OBCreditDebitCode0.CREDIT);
		statementInterest.setType("BALANCETRANSFER");
		statementInterestList.add(statementInterest);
		data.setStatementInterest(statementInterestList);
		List<OBStatement2StatementDateTime> statementdateTimeList = new ArrayList<OBStatement2StatementDateTime>();
		statementDateTime.setDateTime("2018-07-23T10:29:22.765Z");
		statementDateTime.setType("BALANCETRANSFERPROMOEND");
		statementdateTimeList.add(statementDateTime);
		data.setStatementDateTime(statementdateTimeList);
		List<OBStatement2StatementRate> statementRateList = new ArrayList<OBStatement2StatementRate>();
		statementRate.setRate("1.5");
		statementRate.setType("ANNUALBALANCETRANSFER");
		statementRateList.add(statementRate);
		data.setStatementRate(statementRateList);
		List<OBStatement2StatementValue> statementValueList = new ArrayList<OBStatement2StatementValue>();
		statementValue.setValue("0");
		statementValue.setType("AIRMILESPOINTS");
		statementValueList.add(statementValue);
		data.setStatementValue(statementValueList);
		List<OBStatement2StatementAmount> statementAmountList = new ArrayList<OBStatement2StatementAmount>();
		statementAmount3.setAmount("400.00");
		statementAmount3.setCurrency("GBP");
		statementAmount.setAmount(statementAmount3);
		statementAmount.setCreditDebitIndicator(OBCreditDebitCode0.CREDIT);
		statementAmount.setType("CLOSINGBALANCE");
		statementAmountList.add(statementAmount);
		data.setStatementAmount(statementAmountList);
		dataList.add(data);
		Page<AccountStatementsCMA2> page = new PageImpl<>(dataList);
		return page;
	}
}
