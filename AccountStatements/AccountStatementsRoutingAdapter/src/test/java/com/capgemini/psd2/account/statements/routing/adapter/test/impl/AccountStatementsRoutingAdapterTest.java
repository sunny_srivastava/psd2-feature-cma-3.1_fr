package com.capgemini.psd2.account.statements.routing.adapter.test.impl;

import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyString;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.capgemini.psd2.account.statements.routing.adapter.impl.AccountStatementsRoutingAdapter;
import com.capgemini.psd2.account.statements.routing.adapter.routing.AccountStatementsAdapterFactory;
import com.capgemini.psd2.account.statements.routing.adapter.test.adapter.AccountStatementsTestRoutingAdapter;
import com.capgemini.psd2.aisp.adapter.AccountStatementsAdapter;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountSatementsFileResponse;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountStatementsResponse;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountTransactionResponse;
import com.capgemini.psd2.exceptions.OBPSD2ExceptionUtility;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.token.Token;

public class AccountStatementsRoutingAdapterTest {
	

	/** The account statements adapter factory. */
	@Mock
	private AccountStatementsAdapterFactory accountStatementsAdapterFactory;
	
	@Mock
	private RequestHeaderAttributes reqHeaderAtrributes;

	/** The account statements routing adapter. */
	@InjectMocks
	private AccountStatementsAdapter accountStatementsRoutingAdapter = new AccountStatementsRoutingAdapter();

	/**
	 * Sets the up.
	 *
	 * @throws Exception
	 *             the exception
	 */
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		Map<String, String> map=new HashMap<>();
		map.put("SIGNATURE", "Something wrong with x-jws-signature request header");
		Map<String, String> specificErrorMessageMap=new HashMap<>();
		specificErrorMessageMap.put("signature_invalid_content", "Invalid content prsent in joseheader of x-jws-signature header");
		specificErrorMessageMap.put("signature_missing", "signature header missing in request");
		OBPSD2ExceptionUtility.genericErrorMessages.putAll(map);
		OBPSD2ExceptionUtility.specificErrorMessages.putAll(specificErrorMessageMap);
	}

	/**
	 * Retrieve account beneficiary.
	 */
	@Test
	public void retrieveAccountStatementstest() {
		AccountStatementsAdapter accountStatementsAdapter = new AccountStatementsTestRoutingAdapter();

		Mockito.when(accountStatementsAdapterFactory.getAdapterInstance(anyString()))
				.thenReturn(accountStatementsAdapter);
		Token token = new Token();
		Map<String, String> map  = new HashMap<>();
		token.setSeviceParams(map);
		Mockito.when(reqHeaderAtrributes.getToken()).thenReturn(token);
		PlatformAccountStatementsResponse platformAccountStatementsResponse = accountStatementsRoutingAdapter
				.retrieveAccountStatements(null, null);

		assertTrue(platformAccountStatementsResponse.getoBReadStatement2().getData().getStatement().isEmpty());
		
	}
	
	@Test
	public void retrieveAccountStatementsByStatementIdtest() {
		AccountStatementsAdapter accountStatementsAdapter = new AccountStatementsTestRoutingAdapter();

		Mockito.when(accountStatementsAdapterFactory.getAdapterInstance(anyString()))
				.thenReturn(accountStatementsAdapter);

		PlatformAccountStatementsResponse platformAccountStatementsResponse = accountStatementsRoutingAdapter
				.retrieveAccountStatementsByStatementId(null, null);

		assertTrue(platformAccountStatementsResponse.getoBReadStatement2().getData().getStatement().isEmpty());
		
	}
	
	@Test
	public void retrieveAccountTransactionsByStatementsIdtest() {
		AccountStatementsAdapter accountStatementsAdapter = new AccountStatementsTestRoutingAdapter();

		Mockito.when(accountStatementsAdapterFactory.getAdapterInstance(anyString()))
				.thenReturn(accountStatementsAdapter);
		Token token = new Token();
		Map<String, String> map  = new HashMap<>();
		token.setSeviceParams(map);
		Mockito.when(reqHeaderAtrributes.getToken()).thenReturn(token);
		PlatformAccountTransactionResponse platformAccountTransactionResponse = accountStatementsRoutingAdapter
				.retrieveAccountTransactionsByStatementsId(null, null);

		assertTrue(platformAccountTransactionResponse.getoBReadTransaction6().getData().getTransaction().isEmpty());
		
	}
	
	@Test
	public void downloadStatementFileByStatementsIdtest() {
		AccountStatementsAdapter accountStatementsAdapter = new AccountStatementsTestRoutingAdapter();

		Mockito.when(accountStatementsAdapterFactory.getAdapterInstance(anyString()))
				.thenReturn(accountStatementsAdapter);
		Token token = new Token();
		Map<String, String> map  = new HashMap<>();
		token.setSeviceParams(map);
		Mockito.when(reqHeaderAtrributes.getToken()).thenReturn(token);
		PlatformAccountSatementsFileResponse platformAccountSatementsFileResponse = accountStatementsRoutingAdapter
				.downloadStatementFileByStatementsId(null, null);

		assertTrue(platformAccountSatementsFileResponse.getFileName().equals("local.pdf"));	
	}
}