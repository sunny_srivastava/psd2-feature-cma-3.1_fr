package com.capgemini.psd2.account.statements.routing.adapter.test.mock.data;

import java.util.ArrayList;
import java.util.List;

import com.capgemini.psd2.aisp.domain.OBReadDataStatement2;
import com.capgemini.psd2.aisp.domain.OBReadDataTransaction6;
import com.capgemini.psd2.aisp.domain.OBReadStatement2;
import com.capgemini.psd2.aisp.domain.OBReadTransaction6;
import com.capgemini.psd2.aisp.domain.OBStatement2;
import com.capgemini.psd2.aisp.domain.OBTransaction6;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountSatementsFileResponse;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountStatementsResponse;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountTransactionResponse;

public class AccountStatementsRoutingAdapterTestMockData {


	public static PlatformAccountStatementsResponse getMockStatementGETResponse() {
		PlatformAccountStatementsResponse platformAccountStatementsResponse = new PlatformAccountStatementsResponse();
		OBReadDataStatement2 data = new OBReadDataStatement2();
		List<OBStatement2> statement = new ArrayList<>();
		data.setStatement(statement);
		OBReadStatement2 oBReadStatement = new OBReadStatement2();
		oBReadStatement.setData(data);
		platformAccountStatementsResponse.setoBReadStatement2(oBReadStatement);
		return platformAccountStatementsResponse;
	
}
	public static PlatformAccountTransactionResponse getMockTransactionGETResponse() {
	PlatformAccountTransactionResponse platformAccountTransactionResponse = new PlatformAccountTransactionResponse();
	OBReadDataTransaction6 data = new OBReadDataTransaction6();
	List<OBTransaction6> transaction = new ArrayList<>();
	data.setTransaction(transaction);
	OBReadTransaction6 oBReadTransaction = new OBReadTransaction6();
	oBReadTransaction.setData(data);
	platformAccountTransactionResponse.setoBReadTransaction6(oBReadTransaction);
	return platformAccountTransactionResponse;

}
	
	public static PlatformAccountSatementsFileResponse getMockFileGETResponse() {
		PlatformAccountSatementsFileResponse platformAccountSatementsFileResponse = new PlatformAccountSatementsFileResponse();
		
		platformAccountSatementsFileResponse.setFileName("local.pdf");
		platformAccountSatementsFileResponse.setFileByteArray(null);

		return platformAccountSatementsFileResponse;

	}
	
	
	
	
}	