package com.capgemini.psd2.account.statements.routing.adapter.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.account.statements.routing.adapter.routing.AccountStatementsAdapterFactory;
import com.capgemini.psd2.aisp.adapter.AccountStatementsAdapter;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountSatementsFileResponse;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountStatementsResponse;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountTransactionResponse;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.utilities.NullCheckUtils;
@Component("accountStatementsRoutingAdapter")
public class AccountStatementsRoutingAdapter implements AccountStatementsAdapter
{

	
	/** The account standing order adapter factory. */
	@Autowired
	private AccountStatementsAdapterFactory accountStatementsAdapterFactory;
	
	@Autowired
	private RequestHeaderAttributes reqHeaderAtrributes;

	/** The default adapter. */
	@Value("${app.defaultAccountStatementsAdapter}")
	private String defaultAdapter;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.capgemini.psd2.aisp.adapter.AccountStatementsAdapter#
	 * retrieveAccountInformation(com.capgemini.psd2.aisp.domain.AccountMapping,
	 * java.util.Map)
	 */
	@Override
	public PlatformAccountStatementsResponse retrieveAccountStatements(AccountMapping accountMapping,
			Map<String, String> params) {
		AccountStatementsAdapter accountStatementsAdapter = accountStatementsAdapterFactory
				.getAdapterInstance(defaultAdapter);
		
		return accountStatementsAdapter.retrieveAccountStatements(accountMapping, addHeaderParams(params));
	}

	@Override
	public PlatformAccountStatementsResponse retrieveAccountStatementsByStatementId(AccountMapping accountMapping,
			Map<String, String> params) {
		AccountStatementsAdapter accountStatementsAdapter = accountStatementsAdapterFactory
				.getAdapterInstance(defaultAdapter);
		
		return accountStatementsAdapter.retrieveAccountStatementsByStatementId(accountMapping, addHeaderParams(params));
	}

	@Override
	public PlatformAccountTransactionResponse retrieveAccountTransactionsByStatementsId(AccountMapping accountMapping,
			Map<String, String> params) {
		AccountStatementsAdapter accountStatementsAdapter = accountStatementsAdapterFactory
				.getAdapterInstance(defaultAdapter);
		return accountStatementsAdapter.retrieveAccountTransactionsByStatementsId(accountMapping, addHeaderParams(params));
	}

	@Override
	public PlatformAccountSatementsFileResponse downloadStatementFileByStatementsId(AccountMapping accountMapping, Map<String, String> params) {
		AccountStatementsAdapter accountStatementsAdapter = accountStatementsAdapterFactory
				.getAdapterInstance(defaultAdapter);
		return accountStatementsAdapter.downloadStatementFileByStatementsId(accountMapping, addHeaderParams(params));
	}
	
	private Map<String, String> addHeaderParams(Map<String, String> mapParam) {
		Map<String,String> mapParamResult;
		if (NullCheckUtils.isNullOrEmpty(mapParam))
			mapParamResult = new HashMap<>();
		else mapParamResult=mapParam;

		if (null != reqHeaderAtrributes.getToken() && null != reqHeaderAtrributes.getToken().getSeviceParams()) {
			mapParamResult.put(PSD2Constants.CHANNEL_IN_REQ_HEADER,
					reqHeaderAtrributes.getToken().getSeviceParams().get(PSD2Constants.CHANNEL_NAME));
		}
		mapParamResult.put(PSD2Constants.USER_IN_REQ_HEADER, reqHeaderAtrributes.getPsuId());
		mapParamResult.put(PSD2Constants.CORRELATION_REQ_HEADER, reqHeaderAtrributes.getCorrelationId());
		mapParamResult.put(PSD2Constants.TENANT_ID, reqHeaderAtrributes.getTenantId());
		return mapParamResult;
	}
	
	
	}