package com.capgemini.psd2.account.statements.utilities;


import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.consent.domain.AispConsent;
import com.capgemini.psd2.exceptions.ErrorMapKeys;
import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.netflix.discovery.shared.Pair;

@Component
public class AccountStatementDateUtilities {
	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(AccountStatementDateUtilities.class);
	public Pair<String, String> populateSetupDates(AispConsent aispConsent) {

		String consentFromDateTimeString = aispConsent.getTransactionFromDateTime();
		String consentToDateTimeString = aispConsent.getTransactionToDateTime();

		String requestedFromString = null;
		String requestedToString = null;

		if (consentFromDateTimeString != null) {

			LocalDateTime consentFrmDtTm = convertZonedStringToLocalDateTime(consentFromDateTimeString);
			requestedFromString = convertZonedDateTimeToString(consentFrmDtTm);
		}

		if (consentToDateTimeString != null) {
			LocalDateTime consentToDtTm = convertZonedStringToLocalDateTime(consentToDateTimeString);
			requestedToString = convertZonedDateTimeToString(consentToDtTm);
		}

		return new Pair<>(requestedFromString, requestedToString);
	}
	
	public String getExpirationDate(AispConsent aispConsent) {
		String expirationDateTimeString = aispConsent.getEndDate();

		String expirationDateTimeToString = null;
		if (expirationDateTimeString != null) {
			LocalDateTime expirationDateTime = convertZonedStringToLocalDateTime(expirationDateTimeString);

			expirationDateTimeToString = convertZonedDateTimeToString(expirationDateTime);
		}
		return expirationDateTimeToString;
	}
	
	public LocalDateTime convertDateStringToLocalDateTime(String inputDateString) {
		if (inputDateString != null) {
			try {
				return LocalDateTime.parse(inputDateString, DateTimeFormatter.ISO_LOCAL_DATE_TIME);
			} catch (DateTimeParseException e) {
				throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_FIELD_INVALID,
						ErrorMapKeys.VALIDATION_ERROR));
			}
		}
		return null;
	}
	
	public LocalDateTime convertZonedStringToLocalDateTime(String inputDateString) {
		if (inputDateString != null) {
			try {
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
				Date date = sdf.parse(inputDateString);
				return convertDateStringToLocalDateTime(sdf.format(date));
			} catch (Exception e) {
				LOG.error(e.getMessage(),e);
				throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_FIELD_INVALID,
						ErrorMapKeys.VALIDATION_ERROR));
			}
			 
		}
		return null;
	}

	public String convertZonedDateTimeToString(LocalDateTime inputDateTime) {
		return inputDateTime.plusNanos(1).toString().substring(0, 23);
	}
	
}
