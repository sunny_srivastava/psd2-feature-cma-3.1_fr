package com.capgemini.psd2.account.statements.utilities;

import java.util.List;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Component
@ConfigurationProperties("app")
public class AccountStatementTransactionValidation {

	private List<String> transactionMandatoryClaims;
	
	public List<String> getTransactionMandatoryClaims() {
		return this.transactionMandatoryClaims;
	}

	public void setTransactionMandatoryClaims(List<String> transactionMandatoryClaims) {
		this.transactionMandatoryClaims = transactionMandatoryClaims;
	}

	public void validateStatementTransactionClaims(RequestHeaderAttributes reqHeaderAtrributes ) {
		boolean flag = false;
	
		if (!NullCheckUtils.isNullOrEmpty(transactionMandatoryClaims)) {
			for (String transactionMandatoryClaim : transactionMandatoryClaims) {
				if (reqHeaderAtrributes.getClaims().toString().toUpperCase()
					.contains(transactionMandatoryClaim.toUpperCase())) {
					flag = true;
					break;
				}
			}
			if (!flag) {
				throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.NO_REQUIRED_PERMISSION_PRESENT);
			}
		}
	}
}
