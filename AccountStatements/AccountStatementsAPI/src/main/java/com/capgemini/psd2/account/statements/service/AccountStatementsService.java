package com.capgemini.psd2.account.statements.service;

import com.capgemini.psd2.aisp.domain.OBReadStatement2;
import com.capgemini.psd2.aisp.domain.OBReadTransaction6;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountSatementsFileResponse;
public interface AccountStatementsService 
{
	/**
	 * Retrieve account standing order.
	 *
	 * @param accountId the account id
	 * @return the StandingOrdersGETResponse
	 */
	public OBReadStatement2 retrieveAccountStatements(String accountId, String paramFromDate, String paramToDate, 
			Integer paramPageNumber, Integer paramPageSize, String txnKey);

	public OBReadStatement2 retrieveAccountStatementsByStatementsId(String accountId, String statementId, 
			Integer paramPageNumber, Integer paramPageSize, String txnKey);
	
	public OBReadTransaction6 retrieveAccountTransactionsByStatementsId(String accountId, String statementId, Integer paramPageNumber, Integer paramPageSize, String txnKey);
	
	public PlatformAccountSatementsFileResponse downloadStatementFileByStatementsId(String accountId, String statementId);

}
