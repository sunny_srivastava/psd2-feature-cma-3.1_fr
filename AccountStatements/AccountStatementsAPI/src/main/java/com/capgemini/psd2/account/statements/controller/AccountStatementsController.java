package com.capgemini.psd2.account.statements.controller;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.ServletWebRequest;

import com.capgemini.psd2.account.statements.service.AccountStatementsService;
import com.capgemini.psd2.account.statements.utilities.AccountStatementValidationUtilities;
import com.capgemini.psd2.aisp.domain.OBReadStatement2;
import com.capgemini.psd2.aisp.domain.OBReadTransaction6;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountSatementsFileResponse;
import com.capgemini.psd2.cma2.aisp.domain.NoPayloadLogging;
import com.capgemini.psd2.exceptions.ApiEndExceptionHandler;


@RestController
public class AccountStatementsController {

@Autowired	
private AccountStatementsService service;

@Autowired
ApiEndExceptionHandler apiExceptionHandler;

@Autowired
private AccountStatementValidationUtilities accountStatementValidationUtilities;
	
	@RequestMapping(value ="/accounts/{accountId}/statements", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public OBReadStatement2 retrieveAccountStatements(@PathVariable("accountId") String accountId,
			@RequestParam(value = "fromStatementDateTime", required = false) String fromStatementDateTime,
			@RequestParam(value = "toStatementDateTime", required = false) String toStatementDateTime,
			@RequestParam(value = "page", required = false) Integer pageNumber,
			@RequestParam(value = "pageSize", required = false) Integer pageSize,
			@RequestParam(value = "transactionRetrievalKey", required = false) String txnKey) {

		return service.retrieveAccountStatements(accountId, fromStatementDateTime, toStatementDateTime,
				pageNumber, pageSize, txnKey);
	}
		
	@RequestMapping(value ="/accounts/{accountId}/statements/{statementId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public OBReadStatement2 retrieveAccountStatementByStatementsId(@PathVariable("accountId") String accountId, 
			@PathVariable("statementId") String statementId, 
			@RequestParam(value = "page", required = false) Integer pageNumber,
			@RequestParam(value = "pageSize", required = false) Integer pageSize,
			@RequestParam(value = "transactionRetrievalKey", required = false) String txnKey) {

		
		return service.retrieveAccountStatementsByStatementsId(accountId, statementId, pageNumber, pageSize, txnKey);
	}
	
	@RequestMapping(value ="/accounts/{accountId}/statements/{statementId}/transactions", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public OBReadTransaction6 retrieveAccountTransactionsByStatementsId(@PathVariable("accountId") String accountId, 
			@PathVariable("statementId") String statementId,
			@RequestParam(value = "page", required = false) Integer pageNumber,
			@RequestParam(value = "pageSize", required = false) Integer pageSize,
			@RequestParam(value = "transactionRetrievalKey", required = false) String txnKey) {
		
			
		return service.retrieveAccountTransactionsByStatementsId(accountId, statementId,
				pageNumber, pageSize, txnKey);
	}
	
	@NoPayloadLogging
	@RequestMapping(value ="/accounts/{accountId}/statements/{statementId}/file", method = RequestMethod.GET)
	public ResponseEntity<?> downloadStatementFileByStatementsId(@PathVariable("accountId") String accountId, 
			@PathVariable("statementId") String statementId, ServletWebRequest request) {
		
		  PlatformAccountSatementsFileResponse fileResponse;
		
			fileResponse = service.downloadStatementFileByStatementsId(accountId, statementId);

			HttpHeaders headers = new HttpHeaders();
	        headers.add("Content-Disposition", "inline; filename=" + fileResponse.getFileName());
	        headers.setContentType(MediaType.APPLICATION_PDF);

	        return ResponseEntity.ok().headers(headers).body(new ByteArrayResource(fileResponse.getFileByteArray()));
	}
	
	
}
