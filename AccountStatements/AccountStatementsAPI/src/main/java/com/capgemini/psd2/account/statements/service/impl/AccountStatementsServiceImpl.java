package com.capgemini.psd2.account.statements.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.capgemini.psd2.account.statements.constants.AccountStatementConstants;
import com.capgemini.psd2.account.statements.service.AccountStatementsService;
import com.capgemini.psd2.account.statements.utilities.AccountStatementDateUtilities;
import com.capgemini.psd2.account.statements.utilities.AccountStatementTransactionValidation;
import com.capgemini.psd2.account.statements.utilities.AccountStatementValidationUtilities;
import com.capgemini.psd2.aisp.account.mapping.adapter.AccountMappingAdapter;
import com.capgemini.psd2.aisp.adapter.AccountStatementsAdapter;
import com.capgemini.psd2.aisp.adapter.AispConsentAdapter;
import com.capgemini.psd2.aisp.domain.Links;
import com.capgemini.psd2.aisp.domain.Meta;
import com.capgemini.psd2.aisp.domain.OBReadDataStatement2;
import com.capgemini.psd2.aisp.domain.OBReadDataTransaction6;
import com.capgemini.psd2.aisp.domain.OBReadStatement2;
import com.capgemini.psd2.aisp.domain.OBReadTransaction6;
import com.capgemini.psd2.aisp.domain.OBStatement2;
import com.capgemini.psd2.aisp.domain.OBTransaction6;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountSatementsFileResponse;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountStatementsResponse;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountTransactionResponse;
import com.capgemini.psd2.aisp.validation.adapter.AISPCustomValidator;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.consent.domain.AispConsent;
import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.ErrorMapKeys;
import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.response.validator.ResponseValidator;
import com.capgemini.psd2.utilities.NullCheckUtils;
import com.netflix.discovery.shared.Pair;

@Service
public class AccountStatementsServiceImpl implements AccountStatementsService {

	@Value("${app.minPageSize}")
	private String minPageSize;
	@Value("${app.maxPageSize}")
	private String maxPageSize;
	@Value("${app.defaultPageSize}")
	private String defaultPageSize;
	@Value("${app.fileFilter}")
	private String fileFilter;
	@Value("${app.creditFilter}")
	private String creditFilter;
	@Value("${app.debitFilter}")
	private String debitFilter;

	@Autowired
	private RequestHeaderAttributes reqHeaderAtrributes;

	/** The account standing order adapter. */
	@Autowired
	@Qualifier("accountStatementsRoutingAdapter")
	private AccountStatementsAdapter accountStatementsAdapter;

	/** The account mapping adapter. */
	@Autowired
	private AccountMappingAdapter accountMappingAdapter;

	@Autowired
	private ResponseValidator responseValidator;

	/** The aisp consent adapter. */
	@Autowired
	private AispConsentAdapter aispConsentAdapter;

	@Autowired
	AccountStatementDateUtilities dateUtility;

	@Autowired
	AccountStatementValidationUtilities accountStatementvalUtilities;

	@Autowired
	AccountStatementTransactionValidation accountStatementTransactionValidation;

	@SuppressWarnings("rawtypes")
	@Autowired
	@Qualifier("accountsRoutingValidator")
	private AISPCustomValidator accountsCustomValidator;

	@Value("${app.isSandboxEnabled:false}")
	private boolean isSandboxEnabled;

	@Override
	public OBReadStatement2 retrieveAccountStatements(String accountId, String paramFromDate, String paramToDate,
			Integer paramPageNumber, Integer paramPageSize, String txnKey) {
		// Get consents by accountId and validate.

		validateAccountId(accountId);
		accountStatementvalUtilities.validateParams(paramPageNumber, paramPageSize);
		accountStatementvalUtilities.validateDate(paramFromDate, paramToDate);

		AispConsent aispConsent = aispConsentAdapter.validateAndRetrieveConsentByAccountId(accountId,
				reqHeaderAtrributes.getToken().getConsentTokenData().getConsentId());

		// Get account mapping.
		AccountMapping accountMapping = accountMappingAdapter.retrieveAccountMappingDetails(accountId, aispConsent);

		Pair<String, String> consentDates = dateUtility.populateSetupDates(aispConsent);

		String expirationDate = dateUtility.getExpirationDate(aispConsent);

		Map<String, String> params = new HashMap<>();
		params.put(AccountStatementConstants.REQUESTED_FROM_DATETIME, paramFromDate);
		params.put(AccountStatementConstants.REQUESTED_TO_DATETIME, paramToDate);

		params.put(AccountStatementConstants.REQUESTED_FROM_CONSENT_DATETIME, consentDates.first());
		params.put(AccountStatementConstants.REQUESTED_TO_CONSENT_DATETIME, consentDates.second());
		params.put(AccountStatementConstants.CONSENT_EXPIRATION_DATETIME, expirationDate);

		params.put(AccountStatementConstants.REQUESTED_PAGE_NUMBER,
				paramPageNumber == null ? AccountStatementConstants.DEFAULT_PAGE : paramPageNumber.toString());
		params.put(AccountStatementConstants.REQUESTED_TXN_KEY, txnKey);

		params.put(AccountStatementConstants.REQUESTED_PAGE_SIZE,
				paramPageSize == null ? defaultPageSize : paramPageSize.toString());

		params.put(AccountStatementConstants.REQUESTED_MIN_PAGE_SIZE, String.valueOf(minPageSize));
		params.put(AccountStatementConstants.REQUESTED_MAX_PAGE_SIZE, String.valueOf(maxPageSize));
		params.put(PSD2Constants.CMAVERSION, aispConsent.getCmaVersion());
		params.putAll(reqHeaderAtrributes.getToken().getSeviceParams());
		params.put(PSD2Constants.CO_RELATION_ID, reqHeaderAtrributes.getCorrelationId());

		// Retrieve account statements.
		PlatformAccountStatementsResponse platformAccountStatementsResponse = accountStatementsAdapter
				.retrieveAccountStatements(accountMapping, params);

		if (platformAccountStatementsResponse == null)
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.NO_RECORD_FOUND_FOR_REQUESTED_ACCT);

		// Get CMA2 statements response.
		OBReadStatement2 tppStatementResponse = platformAccountStatementsResponse.getoBReadStatement2();

		params.put(AccountStatementConstants.LINK_PAGE_FROM_DATE, paramFromDate);
		params.put(AccountStatementConstants.LINK_PAGE_TO_DATE, paramToDate);
		if (paramPageNumber != null)
			params.put(AccountStatementConstants.PAGE_LINKS_PAGE_NUM, String.valueOf(paramPageNumber));
		if (paramPageSize != null)
			params.put(AccountStatementConstants.PAGE_SIZE, String.valueOf(paramPageSize));
		tppStatementResponse = populateLinksAndMeta(tppStatementResponse, params);

		// Apply platform and CMA2-swagger validations.
		accountsCustomValidator.validateResponseParams(tppStatementResponse);

		return tppStatementResponse;
	}

	private OBReadStatement2 populateLinksAndMeta(OBReadStatement2 tppStatementResponse, Map<String, String> params) {

		String paramTxnKey = params.get(AccountStatementConstants.REQUESTED_TXN_KEY);
		String paramFromDate = params.get(AccountStatementConstants.LINK_PAGE_FROM_DATE);
		String paramToDate = params.get(AccountStatementConstants.LINK_PAGE_TO_DATE);
		String paramPageNumber = params.get(AccountStatementConstants.PAGE_LINKS_PAGE_NUM);
		String paramPageSize = params.get(AccountStatementConstants.PAGE_SIZE);

		StringBuilder baseUrl = new StringBuilder(reqHeaderAtrributes.getSelfUrl());

		createURLforStaements(baseUrl, AccountStatementConstants.REQUESTED_FROM_DATETIME, paramFromDate);
		createURLforStaements(baseUrl, AccountStatementConstants.REQUESTED_TO_DATETIME, paramToDate);
		createURLforStaements(baseUrl, AccountStatementConstants.PAGE_SIZE_PARAM_NAME, paramPageSize);

		StringBuilder selfUrl = new StringBuilder(baseUrl.toString());
		createURLforStaements(selfUrl, AccountStatementConstants.PAGE_NUMBER_PARAM_NAME, paramPageNumber);
		createURLforStaements(selfUrl, AccountStatementConstants.TXN_KEY_PARAM_NAME, paramTxnKey);
		
		if(null !=tppStatementResponse)
		{
		if (tppStatementResponse.getMeta() == null)
			tppStatementResponse.setMeta(new Meta());

		
		if (tppStatementResponse.getData() == null) {
			OBReadDataStatement2 data = new OBReadDataStatement2();
			List<OBStatement2> statement = new ArrayList<>();
			data.setStatement(statement);
			tppStatementResponse.setData(data);
		}
		if (tppStatementResponse.getLinks() == null) {
			tppStatementResponse.setLinks(new Links());
			tppStatementResponse.getLinks().setSelf(selfUrl.toString());
			return tppStatementResponse;
		}
		}
		else{
			OBReadStatement2  tppStatementResp= new OBReadStatement2();
			tppStatementResp.setMeta(new Meta());
			OBReadDataStatement2 data = new OBReadDataStatement2();
			List<OBStatement2> statement = new ArrayList<>();
			data.setStatement(statement);
			tppStatementResp.setData(data);
			tppStatementResp.setLinks(new Links());
			tppStatementResp.getLinks().setSelf(selfUrl.toString());
			return tppStatementResp;
			
		}
		String txnKey = null;
		if (tppStatementResponse.getLinks().getSelf() != null
				&& tppStatementResponse.getLinks().getSelf().contains(":"))
			txnKey = tppStatementResponse.getLinks().getSelf().split(":")[1];

		tppStatementResponse.getLinks().setSelf(selfUrl.toString());
		tppStatementResponse.getLinks().setFirst(null);

		if (!isSandboxEnabled) {

			tppStatementResponse.getLinks().setNext(
					appendUrlforStatements(tppStatementResponse.getLinks().getNext(), baseUrl.toString(), txnKey));
			tppStatementResponse.getLinks().setPrev(
					appendUrlforStatements(tppStatementResponse.getLinks().getPrev(), baseUrl.toString(), txnKey));
			tppStatementResponse.getLinks().setLast(
					appendUrlforStatements(tppStatementResponse.getLinks().getLast(), baseUrl.toString(), txnKey));

		}
		return tppStatementResponse;
	}

	private void updatePlatformResponse(OBReadStatement2 tppStatementResponse) {
		if (tppStatementResponse.getLinks() == null)
			tppStatementResponse.setLinks(new Links());
		tppStatementResponse.getLinks().setSelf(reqHeaderAtrributes.getSelfUrl());

		if (tppStatementResponse.getMeta() == null) {
			tppStatementResponse.setMeta(new Meta());
			tppStatementResponse.getMeta().setTotalPages(1);
		}

	}

	private String appendUrlforStatements(String fsPageNum, String baseUrl, String txnKey) {
		if (fsPageNum == null)
			return null;
		StringBuilder lastUrl = new StringBuilder(baseUrl);
		createURLforStaements(lastUrl, AccountStatementConstants.PAGE_NUMBER_PARAM_NAME, fsPageNum);
		createURLforStaements(lastUrl, AccountStatementConstants.TXN_KEY_PARAM_NAME, txnKey);
		return lastUrl.toString();
	}

	private void createURLforStaements(StringBuilder url, String key, String value) {
		if (value != null) {
			if (url.indexOf("?") == -1)
				url.append("?").append(key).append("=").append(value);
			else
				url.append("&").append(key).append("=").append(value);
		}
	}

	@Override
	public OBReadStatement2 retrieveAccountStatementsByStatementsId(String accountId, String statementId,
			Integer paramPageNumber, Integer paramPageSize, String txnKey) {

		// Get consents by accountId and validate.
		validateAccountId(accountId);
		validateStatementId(statementId);
		accountStatementvalUtilities.validateParams(paramPageNumber, paramPageSize);

		AispConsent aispConsent = aispConsentAdapter.validateAndRetrieveConsentByAccountId(accountId,
				reqHeaderAtrributes.getToken().getConsentTokenData().getConsentId());

		// Get account mapping.
		AccountMapping accountMapping = accountMappingAdapter.retrieveAccountMappingDetails(accountId, aispConsent);
		Map<String, String> params = reqHeaderAtrributes.getToken().getSeviceParams();

		params.put(AccountStatementConstants.REQUESTED_PAGE_NUMBER,
				paramPageNumber == null ? AccountStatementConstants.DEFAULT_PAGE : paramPageNumber.toString());
		params.put(AccountStatementConstants.REQUESTED_TXN_KEY, txnKey);

		params.put(AccountStatementConstants.REQUESTED_PAGE_SIZE,
				paramPageSize == null ? defaultPageSize : paramPageSize.toString());

		params.put(AccountStatementConstants.REQUESTED_MIN_PAGE_SIZE, String.valueOf(minPageSize));
		params.put(AccountStatementConstants.REQUESTED_MAX_PAGE_SIZE, String.valueOf(maxPageSize));
		params.put(PSD2Constants.CMAVERSION, aispConsent.getCmaVersion());
		params.put(AccountStatementConstants.STATEMENT_ID, statementId);

		// Retrieve account statements.
		PlatformAccountStatementsResponse platformAccountStatementsResponse = accountStatementsAdapter
				.retrieveAccountStatementsByStatementId(accountMapping, params);

		if (platformAccountStatementsResponse == null)
			throw PSD2Exception.populatePSD2Exception(
					new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_RESOURCE_NOTFOUND, ErrorMapKeys.NO_STATEMENT_DATA_FOUND));

		// Get CMA2 standing-orders response.
		OBReadStatement2 tppStatementResponse = platformAccountStatementsResponse.getoBReadStatement2();

		if (tppStatementResponse == null || tppStatementResponse.getData() == null
				|| tppStatementResponse.getData().getStatement() == null
				|| tppStatementResponse.getData().getStatement().isEmpty())
			throw PSD2Exception.populatePSD2Exception(
					new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_RESOURCE_NOTFOUND, ErrorMapKeys.NO_STATEMENT_DATA_FOUND));

		// update platform response.
		updatePlatformResponse(tppStatementResponse);

		// Apply platform and CMA2-swagger validations.
		accountsCustomValidator.validateResponseParams(tppStatementResponse);
		return tppStatementResponse;
	}

	@Override
	public OBReadTransaction6 retrieveAccountTransactionsByStatementsId(String accountId, String statementId,
			Integer paramPageNumber, Integer paramPageSize, String txnKey) {

		accountStatementTransactionValidation.validateStatementTransactionClaims(reqHeaderAtrributes);
		validateAccountId(accountId);
		validateStatementId(statementId);
		accountStatementvalUtilities.validateParams(paramPageNumber, paramPageSize);

		// Get consents by accountId and validate.
		AispConsent aispConsent = aispConsentAdapter.validateAndRetrieveConsentByAccountId(accountId,
				reqHeaderAtrributes.getToken().getConsentTokenData().getConsentId());

		// Get account mapping.
		AccountMapping accountMapping = accountMappingAdapter.retrieveAccountMappingDetails(accountId, aispConsent);

		Map<String, String> params = new HashMap<>();

		params.put(AccountStatementConstants.REQUESTED_PAGE_NUMBER,
				paramPageNumber == null ? AccountStatementConstants.DEFAULT_PAGE : paramPageNumber.toString());
		params.put(AccountStatementConstants.REQUESTED_TXN_FILTER, populateFilter());
		params.put(AccountStatementConstants.REQUESTED_TXN_KEY, txnKey);
		params.put(AccountStatementConstants.REQUESTED_PAGE_NUMBER,
				paramPageNumber == null ? AccountStatementConstants.DEFAULT_PAGE : paramPageNumber.toString());

		params.put(AccountStatementConstants.REQUESTED_TXN_KEY, txnKey);

		params.put(AccountStatementConstants.REQUESTED_PAGE_SIZE,
				paramPageSize == null ? defaultPageSize : paramPageSize.toString());

		params.put(AccountStatementConstants.REQUESTED_MIN_PAGE_SIZE, String.valueOf(minPageSize));
		params.put(AccountStatementConstants.REQUESTED_MAX_PAGE_SIZE, String.valueOf(maxPageSize));
		params.put(PSD2Constants.CMAVERSION, aispConsent.getCmaVersion());
		params.putAll(reqHeaderAtrributes.getToken().getSeviceParams());

		params.put(AccountStatementConstants.STATEMENT_ID, statementId);

		// Retrieve account transactions.
		PlatformAccountTransactionResponse platformAccountTransactionResponse = accountStatementsAdapter
				.retrieveAccountTransactionsByStatementsId(accountMapping, params);

		if (platformAccountTransactionResponse == null)
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.NO_RECORD_FOUND_FOR_REQUESTED_ACCT);

		// Get CMA2 transaction response.
		OBReadTransaction6 tppTransactionResponse = platformAccountTransactionResponse.getoBReadTransaction6();

		if (tppTransactionResponse == null || tppTransactionResponse.getData() == null
				|| tppTransactionResponse.getData().getTransaction() == null
				|| tppTransactionResponse.getData().getTransaction().isEmpty())
			throw PSD2Exception.populatePSD2Exception(
					new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_RESOURCE_NOTFOUND, ErrorMapKeys.NO_STATEMENT_DATA_FOUND));

		if (paramPageNumber != null)
			params.put(AccountStatementConstants.PAGE_LINKS_PAGE_NUM, String.valueOf(paramPageNumber));
		if (paramPageSize != null)
			params.put(AccountStatementConstants.PAGE_SIZE, String.valueOf(paramPageSize));
		tppTransactionResponse = populateLinksAndMeta(tppTransactionResponse, params);

		// Apply platform and CMA2-swagger validations.
		responseValidator.validateResponse(tppTransactionResponse);
		return tppTransactionResponse;
	}

	private String populateFilter() {

		String transactionFilter;
		Set<Object> claims = reqHeaderAtrributes.getToken().getClaims()
				.get(AccountStatementConstants.ACCOUNT_SCOPE_NAME);
		if ((claims.contains(creditFilter)) && (claims.contains(debitFilter)))
			transactionFilter = AccountStatementConstants.FilterEnum.ALL.toString();
		else if (claims.contains(creditFilter))
			transactionFilter = AccountStatementConstants.FilterEnum.CREDIT.toString();
		else
			transactionFilter = AccountStatementConstants.FilterEnum.DEBIT.toString();
		return transactionFilter;
	}

	private OBReadTransaction6 populateLinksAndMeta(OBReadTransaction6 tppTransactionResponse,
			Map<String, String> params) {

		String paramTxnKey = params.get(AccountStatementConstants.REQUESTED_TXN_KEY);
		String paramPageNumber = params.get(AccountStatementConstants.PAGE_LINKS_PAGE_NUM);
		String paramPageSize = params.get(AccountStatementConstants.PAGE_SIZE);

		StringBuilder baseUrl = new StringBuilder(reqHeaderAtrributes.getSelfUrl());

		createURL(baseUrl, AccountStatementConstants.PAGE_SIZE_PARAM_NAME, paramPageSize);

		StringBuilder selfUrl = new StringBuilder(baseUrl.toString());
		createURL(selfUrl, AccountStatementConstants.PAGE_NUMBER_PARAM_NAME, paramPageNumber);
		createURL(selfUrl, AccountStatementConstants.TXN_KEY_PARAM_NAME, paramTxnKey);

		if (tppTransactionResponse.getMeta() == null)
			tppTransactionResponse.setMeta(new Meta());

		if (tppTransactionResponse.getData() == null) {
			OBReadDataTransaction6 data = new OBReadDataTransaction6();
			List<OBTransaction6> transaction = new ArrayList<>();
			data.setTransaction(transaction);
			tppTransactionResponse.setData(data);
		}
		if (tppTransactionResponse.getLinks() == null) {
			tppTransactionResponse.setLinks(new Links());
			tppTransactionResponse.getLinks().setSelf(selfUrl.toString());
			return tppTransactionResponse;
		}

		String txnKey = null;
		if (tppTransactionResponse.getLinks().getSelf() != null
				&& tppTransactionResponse.getLinks().getSelf().contains(":"))
			txnKey = tppTransactionResponse.getLinks().getSelf().split(":")[1];

		tppTransactionResponse.getLinks().setSelf(selfUrl.toString());
		tppTransactionResponse.getLinks().setFirst(null);
		tppTransactionResponse.getLinks()
				.setNext(appendUrl(tppTransactionResponse.getLinks().getNext(), baseUrl.toString(), txnKey));
		tppTransactionResponse.getLinks()
				.setPrev(appendUrl(tppTransactionResponse.getLinks().getPrev(), baseUrl.toString(), txnKey));
		tppTransactionResponse.getLinks()
				.setLast(appendUrl(tppTransactionResponse.getLinks().getLast(), baseUrl.toString(), txnKey));
		return tppTransactionResponse;
	}

	private String appendUrl(String fsPageNum, String baseUrl, String txnKey) {
		if (fsPageNum == null)
			return null;
		StringBuilder lastUrl = new StringBuilder(baseUrl);
		createURL(lastUrl, AccountStatementConstants.PAGE_NUMBER_PARAM_NAME, fsPageNum);
		createURL(lastUrl, AccountStatementConstants.TXN_KEY_PARAM_NAME, txnKey);
		return lastUrl.toString();
	}

	private void createURL(StringBuilder url, String key, String value) {
		if (value != null) {
			if (url.indexOf("?") == -1)
				url.append("?").append(key).append("=").append(value);
			else
				url.append("&").append(key).append("=").append(value);
		}

	}

	@Override
	public PlatformAccountSatementsFileResponse downloadStatementFileByStatementsId(String accountId,
			String statementId) {

		PlatformAccountSatementsFileResponse accountSatementsFileResponse;

		validateAccountId(accountId);
		validateStatementId(statementId);

		if (fileFilterPermission()) {
			// Get consents by accountId and validate.
			AispConsent aispConsent = aispConsentAdapter.validateAndRetrieveConsentByAccountId(accountId,
					reqHeaderAtrributes.getToken().getConsentTokenData().getConsentId());

			// Get account mapping.
			AccountMapping accountMapping = accountMappingAdapter.retrieveAccountMappingDetails(accountId, aispConsent);
			Map<String, String> params = reqHeaderAtrributes.getToken().getSeviceParams();
			params.put(AccountStatementConstants.STATEMENT_ID, statementId);
			params.put(PSD2Constants.CMAVERSION, aispConsent.getCmaVersion());
			accountSatementsFileResponse = accountStatementsAdapter.downloadStatementFileByStatementsId(accountMapping,
					params);

			if (accountSatementsFileResponse == null)
				throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_RESOURCE_NOTFOUND,
						ErrorMapKeys.NO_STATEMENT_DATA_FOUND));

			return accountSatementsFileResponse;
		} else {
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.NO_REQUIRED_PERMISSION_PRESENT);
		}
	}

	private boolean fileFilterPermission() {
		Set<Object> claims = reqHeaderAtrributes.getToken().getClaims()
				.get(AccountStatementConstants.ACCOUNT_SCOPE_NAME);
		return claims.contains(fileFilter);
	}

	private void validateAccountId(String accountId) {
		if (!NullCheckUtils.isNullOrEmpty(accountId)) {
			accountsCustomValidator.validateUniqueId(accountId);
		} else {
			throw PSD2Exception.populatePSD2Exception(
					new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_FIELD_MISSING, ErrorMapKeys.NO_ACCOUNT_ID_FOUND));
		}
	}

	private void validateStatementId(String statementId) {

		if (!NullCheckUtils.isNullOrEmpty(statementId)) {
			accountsCustomValidator.validateUniqueId(statementId);
		} else {
			throw PSD2Exception.populatePSD2Exception(
					new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_FIELD_MISSING, ErrorMapKeys.NO_STATEMENT_ID_FOUND));

		}
	}

}
