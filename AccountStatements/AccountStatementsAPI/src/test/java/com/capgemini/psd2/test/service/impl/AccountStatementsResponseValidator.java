package com.capgemini.psd2.test.service.impl;

import org.springframework.stereotype.Component;

import com.capgemini.psd2.aisp.domain.OBReadStatement2;
import com.capgemini.psd2.aisp.domain.OBReadTransaction6;
import com.capgemini.psd2.aisp.domain.OBTransaction6;
import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.response.validator.ResponseValidator;

@Component
public class AccountStatementsResponseValidator extends ResponseValidator {

	@Override
	public void performPlatformValidation(Object tppProductsResponse) {
		
		OBReadTransaction6 input = (OBReadTransaction6)tppProductsResponse;
		OBReadStatement2 input1 = (OBReadStatement2)tppProductsResponse;

		
		for(OBTransaction6 obTransaction2:input.getData().getTransaction()) {
			if(obTransaction2.getStatementReference()!=null) {
				for(String reference:obTransaction2.getStatementReference()) {
					if(reference.length()>35) {
						throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.TECHNICAL_ERROR);
					}
				}
			}
		}

		
		
	}
	
	
}
