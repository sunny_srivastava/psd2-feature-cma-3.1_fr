package com.capgemini.psd2.test.controller;

import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.HashMap;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.capgemini.psd2.account.statements.controller.AccountStatementsController;
import com.capgemini.psd2.account.statements.service.AccountStatementsService;
import com.capgemini.psd2.account.statements.utilities.AccountStatementValidationUtilities;
import com.capgemini.psd2.aisp.domain.OBReadStatement2;
import com.capgemini.psd2.aisp.domain.OBReadTransaction6;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountSatementsFileResponse;
import com.capgemini.psd2.exceptions.OBPSD2ExceptionUtility;
import com.capgemini.psd2.test.mock.data.AccountStatementsApiTestData;


@RunWith(SpringJUnit4ClassRunner.class)
public class AccountStatementsControllerTest 
{

	/** The service. */
	@Mock
	private AccountStatementsService service;
	
	@Mock
	private AccountStatementValidationUtilities accountStatementValidationUtilities;

	/** The mock mvc. */
	private MockMvc mockMvc;

	/** The controller. */
	@InjectMocks
	private AccountStatementsController controller;

	/** The statement response. */
	OBReadStatement2 statementsGETResponse = null;
	
	/** The statement response. */
	OBReadTransaction6 transactionsGETResponse = null;
	
	PlatformAccountSatementsFileResponse fileResponse = null;
	
	/**
	 * Sets the up.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		this.mockMvc = MockMvcBuilders.standaloneSetup(controller).dispatchOptions(true).build();
		Map<String, String> map=new HashMap<>();
		map.put("SIGNATURE", "Something wrong with x-jws-signature request header");
		Map<String, String> specificErrorMessageMap=new HashMap<>();
		specificErrorMessageMap.put("signature_invalid_content", "Invalid content prsent in joseheader of x-jws-signature header");
		specificErrorMessageMap.put("signature_missing", "signature header missing in request");
		OBPSD2ExceptionUtility.genericErrorMessages.putAll(map);
		OBPSD2ExceptionUtility.specificErrorMessages.putAll(specificErrorMessageMap);
	}

	/**
	 * Test.
	 *
	 * @throws Exception the exception
	 */
	@Test
	public void successTest() {
		when(service.retrieveAccountStatements(anyString(), anyString(), anyString(), anyInt(), anyInt(), anyString())).thenReturn(statementsGETResponse);
		try {
			this.mockMvc.perform(get("/accounts/{accountId}/statements", "269c3ff5-d7f8-419b-a3b9-7136c5b4611a", "2012-01-01T00:00:00+00:00", "2016-01-01T00:00:00+00:00", 1, 1, "dsd")
					.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
		/**
		 * Test.
		 *
		 * @throws Exception the exception
		 */
		@Test
		public void successTestForStatementIdUrl() {
			when(service.retrieveAccountStatementsByStatementsId(anyString(), anyString(), anyInt(), anyInt(), anyString())).thenReturn(statementsGETResponse);
			try {
				this.mockMvc.perform(get("/accounts/{accountId}/statements/{statementId}", "269c3ff5-d7f8-419b-a3b9-7136c5b4611a", "2016", 1, 1, "dsd")
						.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		/**
		 * Test.
		 *
		 * @throws Exception the exception
		 */
		@Test
		public void successTestForTransactionsUrl() {
			when(service.retrieveAccountTransactionsByStatementsId(anyString(), anyString(), anyInt(), anyInt(), anyString())).thenReturn(transactionsGETResponse);
			try {
				this.mockMvc.perform(get("/accounts/{accountId}/statements/{statementId}/transactions", "269c3ff5-d7f8-419b-a3b9-7136c5b4611a", "2012dhwj", 1, 1, "dsd")
						.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	
				/**
				 * Test.
				 *
				 * @throws Exception the exception
				 */
				@Test
				public void successTestForFileDownload() {
					when(service.downloadStatementFileByStatementsId(anyString(), anyString())).thenReturn(AccountStatementsApiTestData.getMockFileResponse());
					try {
						this.mockMvc.perform(get("/accounts/{accountId}/statements/{statementId}/file", "269c3ff5-d7f8-419b-a3b9-7136c5b4611a", "2012dhwj", null)
								.contentType(MediaType.APPLICATION_PDF_VALUE)).andExpect(status().isOk());
					} catch (Exception e) {
						
						e.printStackTrace();
					}
				}
	
	@After
	public void tearDown() throws Exception {
		controller = null;
	}
}
