package com.capgemini.psd2.pisp.sequence.adapter.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import com.capgemini.psd2.pisp.sequence.adapter.SequenceGenerator;
import com.capgemini.psd2.pisp.sequence.document.SequenceDocument;

import static org.springframework.data.mongodb.core.FindAndModifyOptions.options;
import static org.springframework.data.mongodb.core.query.Criteria.where;
import static org.springframework.data.mongodb.core.query.Query.query;

@Repository
public class SequenceGeneratorAdapter implements SequenceGenerator {

	@Autowired
	private MongoOperations mongoOperation;

	public long getNextSequenceId(String sequenceName) {

		try {
			SequenceDocument sequenceDocument = mongoOperation.findAndModify(query(where("_id").is(sequenceName)),
					new Update().inc("counter", 1), options().returnNew(true).upsert(true), SequenceDocument.class);

			return sequenceDocument.getCounter();
		} catch (Exception e) {
			return 0;
		}
	}

	public String getCustomNextSequenceId(String sequenceName, String customParameter) {
		return "SUCCESS";
	}
}
