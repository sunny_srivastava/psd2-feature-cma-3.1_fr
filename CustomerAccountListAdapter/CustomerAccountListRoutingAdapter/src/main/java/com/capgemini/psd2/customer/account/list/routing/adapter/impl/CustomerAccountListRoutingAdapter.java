package com.capgemini.psd2.customer.account.list.routing.adapter.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.capgemini.psd2.aisp.adapter.CustomerAccountListAdapter;
import com.capgemini.psd2.aisp.domain.OBReadAccount6;
import com.capgemini.psd2.customer.account.list.routing.adapter.routing.CustomerListAdapterFactory;
import com.capgemini.psd2.fraudsystem.domain.PSD2CustomerInfo;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.utilities.NullCheckUtils;

public class CustomerAccountListRoutingAdapter implements CustomerAccountListAdapter {

	@Autowired
	private CustomerListAdapterFactory customerListAdapterFactory;
	
	@Autowired
	private RequestHeaderAttributes reqHeaderAtrributes;

	@Value("${app.defaultCustomerListAdapter}")
	private String defaultAdapter;

	@Override
	public OBReadAccount6 retrieveCustomerAccountList(String userId, Map<String, String> params) {
		CustomerAccountListAdapter customerAccountListAdapter = customerListAdapterFactory
				.getAdapterInstance(defaultAdapter);
		return customerAccountListAdapter.retrieveCustomerAccountList(userId, getHeaderParams(params));
	}

	@Override
	public PSD2CustomerInfo retrieveCustomerInfo(String userId, Map<String, String> params) {
		CustomerAccountListAdapter customerAccountListAdapter = customerListAdapterFactory
				.getAdapterInstance(defaultAdapter);
		return customerAccountListAdapter.retrieveCustomerInfo(userId, getHeaderParams(params));
	}
	
	private Map<String, String> getHeaderParams(Map<String, String> params) {
		if (NullCheckUtils.isNullOrEmpty(params))
			params = new HashMap<>(); //NOSONAR

		params.put(PSD2Constants.CORRELATION_REQ_HEADER, reqHeaderAtrributes.getCorrelationId());

		if (!params.containsKey(PSD2Constants.USER_IN_REQ_HEADER)
				|| params.get(PSD2Constants.USER_IN_REQ_HEADER) == null)
			params.put(PSD2Constants.USER_IN_REQ_HEADER, reqHeaderAtrributes.getPsuId());

		if (!params.containsKey(PSD2Constants.TENANT_ID) || params.get(PSD2Constants.TENANT_ID) == null)
			params.put(PSD2Constants.TENANT_ID, reqHeaderAtrributes.getTenantId());

		if (!params.containsKey(PSD2Constants.CHANNEL_IN_REQ_HEADER)
				|| params.get(PSD2Constants.CHANNEL_IN_REQ_HEADER) == null) {
			String channelId;
			String tokenChannelId = null;
			if (null != reqHeaderAtrributes.getToken() && null != reqHeaderAtrributes.getToken().getSeviceParams()) {
				tokenChannelId = reqHeaderAtrributes.getToken().getSeviceParams().get(PSD2Constants.CHANNEL_NAME);
			}
			channelId = (null == tokenChannelId) ? reqHeaderAtrributes.getChannelId() : tokenChannelId;
			params.put(PSD2Constants.CHANNEL_IN_REQ_HEADER, channelId);
		}

		return params;
	}
}