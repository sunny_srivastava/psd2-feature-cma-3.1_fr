package com.capgemini.psd2.customer.account.list.routing.adapter.test.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.aisp.adapter.CustomerAccountListAdapter;
import com.capgemini.psd2.aisp.domain.OBReadAccount6;
import com.capgemini.psd2.customer.account.list.routing.adapter.impl.CustomerAccountListRoutingAdapter;
import com.capgemini.psd2.customer.account.list.routing.adapter.routing.CustomerListAdapterFactory;
import com.capgemini.psd2.customer.account.list.routing.adapter.test.adapter.CustomerAccountListRoutingTestAdapter;
import com.capgemini.psd2.customer.account.list.routing.adapter.test.mock.data.CustomerAccountListRoutingAdapterMockData;
import com.capgemini.psd2.fraudsystem.domain.PSD2CustomerInfo;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;

@RunWith(SpringJUnit4ClassRunner.class)
public class CustomerAccountListRoutingAdapterTest {

	@Mock
	private CustomerListAdapterFactory customerListAdapterFactory;

	@InjectMocks
	private CustomerAccountListRoutingAdapter customerAccountListRoutingAdapter;
	
	@Mock
	private RequestHeaderAttributes reqAttributes;

	@Mock
	private RequestHeaderAttributes req;

	@Before
	public void setUp() {
		req.setTenantId("tenant1");
		ReflectionTestUtils.setField(customerAccountListRoutingAdapter, "reqHeaderAtrributes", req);
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void testRetrieveCustomerAccountListSuccessFlow() {
		OBReadAccount6 acctResponse = CustomerAccountListRoutingAdapterMockData.getCustomerAccountInfo();
		Map<String, String> paramsMap = new HashMap<String, String>();
		paramsMap.put(PSD2Constants.CHANNEL_ID, "12345");
		paramsMap.put(PSD2Constants.USER_ID, "12345");
		paramsMap.put(PSD2Constants.CORRELATION_ID, "12345");
		paramsMap.put(PSD2Constants.CONSENT_FLOW_TYPE, PSD2Constants.PISP);
		paramsMap.put(PSD2Constants.SCHEME_NAME, PSD2Constants.PAN);
		CustomerAccountListAdapter customerAccountListAdapter = mock(CustomerAccountListRoutingTestAdapter.class);
		when(customerListAdapterFactory.getAdapterInstance(anyString())).thenReturn(customerAccountListAdapter);
		when(customerAccountListAdapter.retrieveCustomerAccountList("1234", paramsMap)).thenReturn(acctResponse);

		OBReadAccount6 OBReadAccount3 = acctResponse;
		assertEquals(acctResponse.getData().getAccount().get(0).getAccount(),
				OBReadAccount3.getData().getAccount().get(0).getAccount());
	}

	@Test
	public void testRetrieveCustomerAccountList() {
		CustomerAccountListAdapter customerAccountListAdapter = new CustomerAccountListAdapter() {

			@Override
			public PSD2CustomerInfo retrieveCustomerInfo(String userId, Map<String, String> params) {
				PSD2CustomerInfo customerInfo = new PSD2CustomerInfo();
				customerInfo.setId("1");
				return customerInfo;
			}

			@Override
			public OBReadAccount6 retrieveCustomerAccountList(String userId, Map<String, String> params) {
				OBReadAccount6 accountList = new OBReadAccount6();
				return accountList;
			}
		};
		when(customerListAdapterFactory.getAdapterInstance(anyString())).thenReturn(customerAccountListAdapter);
		customerAccountListRoutingAdapter.retrieveCustomerAccountList(anyString(), new HashMap<String, String>());
		assertNotNull(customerAccountListAdapter);
	}

	@Test
	public void testRetrieveCustomerInfo() {
		CustomerAccountListAdapter customerAccountListAdapter = new CustomerAccountListAdapter() {

			@Override
			public PSD2CustomerInfo retrieveCustomerInfo(String userId, Map<String, String> params) {
				PSD2CustomerInfo customerInfo = new PSD2CustomerInfo();
				customerInfo.setId("1");
				return customerInfo;
			}

			@Override
			public OBReadAccount6 retrieveCustomerAccountList(String userId, Map<String, String> params) {
				OBReadAccount6 accountList = new OBReadAccount6();
				return accountList;
			}
		};
		when(customerListAdapterFactory.getAdapterInstance(anyString())).thenReturn(customerAccountListAdapter);
		HashMap<String, String> map = new HashMap<>();
		map.put(PSD2Constants.USER_IN_REQ_HEADER, "abc");
		map.put(PSD2Constants.TENANT_ID, "xyz");
		map.put(PSD2Constants.CHANNEL_IN_REQ_HEADER, "asdds");
		customerAccountListRoutingAdapter.retrieveCustomerInfo(anyString(), map);
		assertNotNull(customerAccountListAdapter);
	}

	@Test
	public void testRetrieveCustomerInfoWithNullParam() {
		CustomerAccountListAdapter customerAccountListAdapter = new CustomerAccountListAdapter() {

			@Override
			public PSD2CustomerInfo retrieveCustomerInfo(String userId, Map<String, String> params) {
				PSD2CustomerInfo customerInfo = new PSD2CustomerInfo();
				customerInfo.setId("1");
				return customerInfo;
			}

			@Override
			public OBReadAccount6 retrieveCustomerAccountList(String userId, Map<String, String> params) {
				OBReadAccount6 accountList = new OBReadAccount6();
				return accountList;
			}
		};
		when(customerListAdapterFactory.getAdapterInstance(anyString())).thenReturn(customerAccountListAdapter);
		customerAccountListRoutingAdapter.retrieveCustomerInfo(anyString(), null);
		assertNotNull(customerAccountListAdapter);
	}

}