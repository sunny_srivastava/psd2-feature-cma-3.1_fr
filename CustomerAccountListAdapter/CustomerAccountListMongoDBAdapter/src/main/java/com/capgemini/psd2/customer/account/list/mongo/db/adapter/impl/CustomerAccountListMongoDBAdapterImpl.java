package com.capgemini.psd2.customer.account.list.mongo.db.adapter.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.dao.DataAccessResourceFailureException;

import com.capgemini.psd2.aisp.adapter.CustomerAccountListAdapter;
import com.capgemini.psd2.aisp.domain.OBAccount6;
import com.capgemini.psd2.aisp.domain.OBAccount6Account;
import com.capgemini.psd2.aisp.domain.OBReadAccount6;
import com.capgemini.psd2.aisp.domain.OBReadAccount6Data;
import com.capgemini.psd2.cisp.mock.domain.MockChannelProfile;
import com.capgemini.psd2.customer.account.list.mongo.db.adapter.repository.CustomerAccountListMongoDBAdapterRepository;
import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.fraudsystem.domain.PSD2CustomerInfo;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.utilities.NullCheckUtils;
import com.capgemini.psd2.utilities.StringUtils;

@ConfigurationProperties("app")
public class CustomerAccountListMongoDBAdapterImpl implements CustomerAccountListAdapter {

	private static final Logger LOG = LoggerFactory.getLogger(CustomerAccountListMongoDBAdapterImpl.class);
	
	@Autowired
	private CustomerAccountListMongoDBAdapterRepository customerAccountListMongoDBAdapterRepository;

	private Map<String, String> consentSupportedSchemeMap = new HashMap<>();

	@Value("${app.allowCreditCardInPISPConsent:false}")
	private boolean allowCreditCardInPISPConsent;
	
	public Map<String, String> getConsentSupportedSchemeMap() {
		return consentSupportedSchemeMap;
	}

	@Override
	public OBReadAccount6 retrieveCustomerAccountList(String userId, Map<String, String> params) {
		OBReadAccount6 accountGETResponse = new OBReadAccount6();
		try {
			List<OBAccount6> accountsNew = new ArrayList<>();
			List<OBAccount6> accounts = customerAccountListMongoDBAdapterRepository.findByPsuId(userId);
			for (OBAccount6 account : accounts) {

				MockChannelProfile mock = (MockChannelProfile) account;
				List<OBAccount6Account> accts = mock.getAccount();
				// assuming we can only have 2 schema representations of any
				// account (IBAN-SORTCODE)
				OBAccount6Account acct = accts.get(0);
				if ((PSD2Constants.AISP).equals(params.get(PSD2Constants.CONSENT_FLOW_TYPE))) {
					mock.setHashedValue(StringUtils.generateHashedValue(acct.getIdentification()));
					accountsNew.add(mock);
				} else {

					Set<String> additionalInfoKeySet = mock.getAdditionalInformation().keySet();
					String schemeKey = additionalInfoKeySet.stream().filter(key -> consentSupportedSchemeMap.containsValue(key)).findFirst().orElse(null); //NOSONAR
					if (NullCheckUtils.isNullOrEmpty(schemeKey)) { //NOSONAR
						// throw error ?
					} else {
						String schemeIdentification = mock.getAdditionalInformation().get(schemeKey);
						if (!allowCreditCardInPISPConsent && (PSD2Constants.PISP).equals(params.get(PSD2Constants.CONSENT_FLOW_TYPE))
								&& schemeKey.contains(PSD2Constants.PAN)) {
							// Add logic if necessary
						} else {
							mock.setHashedValue(StringUtils.generateHashedValue(schemeIdentification));
							accountsNew.add(mock);
						}
					}
				}
				

			}

			OBReadAccount6Data data2 = new OBReadAccount6Data();
			data2.setAccount(accountsNew);
			accountGETResponse.setData(data2);

		} catch (DataAccessResourceFailureException e) {
			LOG.error("DataAccessResourceFailureException Occurred: "+e);
			throw PSD2Exception.populatePSD2Exception(e.getMessage(), ErrorCodeEnum.CONNECTION_ERROR);
		}
		return accountGETResponse;
	}

	@Override
	public PSD2CustomerInfo retrieveCustomerInfo(String userId, Map<String, String> params) {
		return null;
	}

}
