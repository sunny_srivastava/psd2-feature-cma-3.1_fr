package com.capgemini.psd2.customer.account.list.mongo.db.adapter.test.impl;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.aisp.domain.OBAccount6;
import com.capgemini.psd2.customer.account.list.mongo.db.adapter.impl.CustomerAccountListMongoDBAdapterImpl;
import com.capgemini.psd2.customer.account.list.mongo.db.adapter.repository.CustomerAccountListMongoDBAdapterRepository;
import com.capgemini.psd2.customer.account.list.mongo.db.adapter.test.mock.data.CustomerAccountListMockData;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.PSD2Constants;

@RunWith(SpringJUnit4ClassRunner.class)
public class CustomerAccountListMongoDBAdapterImplTest {

	@InjectMocks
	private CustomerAccountListMongoDBAdapterImpl customerAccountListMongoDBAdapterImpl = new CustomerAccountListMongoDBAdapterImpl();

	@Mock
	private CustomerAccountListMongoDBAdapterRepository customerAccountListMongoDBAdapterRepository;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testRetrieveCustomerAccountListAISPSuccessFlow() {
		List<OBAccount6> data2 = CustomerAccountListMockData.getCustomerAccountInfoList();
		when(customerAccountListMongoDBAdapterRepository.findByPsuId(anyString())).thenReturn(data2);
		Map<String, String> paramsMap = new HashMap<String, String>();
		paramsMap.put(PSD2Constants.CHANNEL_ID, "12345");
		paramsMap.put(PSD2Constants.USER_ID, "12345");
		paramsMap.put(PSD2Constants.CORRELATION_ID, "12345");
		paramsMap.put(PSD2Constants.CONSENT_FLOW_TYPE, PSD2Constants.AISP);
		paramsMap.put(PSD2Constants.SCHEME_NAME, "IBAN");
		customerAccountListMongoDBAdapterImpl.retrieveCustomerAccountList("1234", paramsMap);
	}

	@Test
	public void testRetrieveCustomerAccountListPISPSuccessFlow() {
		List<OBAccount6> data2 = CustomerAccountListMockData.getCustomerAccountInfoList();
		when(customerAccountListMongoDBAdapterRepository.findByPsuId(anyString())).thenReturn(data2);
		Map<String, String> paramsMap = new HashMap<String, String>();
		paramsMap.put(PSD2Constants.CHANNEL_ID, "12345");
		paramsMap.put(PSD2Constants.USER_ID, "12345");
		paramsMap.put(PSD2Constants.CORRELATION_ID, "12345");
		paramsMap.put(PSD2Constants.CONSENT_FLOW_TYPE, PSD2Constants.PISP);
		paramsMap.put(PSD2Constants.SCHEME_NAME, PSD2Constants.SORTCODEACCOUNTNUMBER);
		Map<String, String> consentSupportedSchemeMap = new HashMap<>();
		consentSupportedSchemeMap.put("UK.OBIE.IBAN", "UK_OBIE_IBAN");
		consentSupportedSchemeMap.put("UK.OBIE.SortCodeACcountNumber", "UK_OBIE_SortCodeAccountNumber");
		consentSupportedSchemeMap.put("UK.OBIE.PAN", "UK_OBIE_PAN");
		ReflectionTestUtils.setField(customerAccountListMongoDBAdapterImpl, "consentSupportedSchemeMap", consentSupportedSchemeMap);

		customerAccountListMongoDBAdapterImpl.retrieveCustomerAccountList("1234", paramsMap);
	}

	@Test
	public void testRetrieveCustomerAccountListPISPPANSuccessFlow() {
		List<OBAccount6> data2 = CustomerAccountListMockData.getCustomerAccountInfoList();
		when(customerAccountListMongoDBAdapterRepository.findByPsuId(anyString())).thenReturn(data2);
		Map<String, String> paramsMap = new HashMap<String, String>();
		paramsMap.put(PSD2Constants.CHANNEL_ID, "12345");
		paramsMap.put(PSD2Constants.USER_ID, "12345");
		paramsMap.put(PSD2Constants.CORRELATION_ID, "12345");
		paramsMap.put(PSD2Constants.CONSENT_FLOW_TYPE, PSD2Constants.PISP);
		paramsMap.put(PSD2Constants.SCHEME_NAME, PSD2Constants.PAN);
		customerAccountListMongoDBAdapterImpl.retrieveCustomerAccountList("1234", paramsMap);
	}

	@SuppressWarnings("unchecked")
	@Test(expected = PSD2Exception.class)
	public void testRetrieveCustomerAccountListDataAccessException() {
		when(customerAccountListMongoDBAdapterRepository.findByPsuId("1234"))
				.thenThrow(DataAccessResourceFailureException.class);
		customerAccountListMongoDBAdapterImpl.retrieveCustomerAccountList("1234", null);
	}

}
