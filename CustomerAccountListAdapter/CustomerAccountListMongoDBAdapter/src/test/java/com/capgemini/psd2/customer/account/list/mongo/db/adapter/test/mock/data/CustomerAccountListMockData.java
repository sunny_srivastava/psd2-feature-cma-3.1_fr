package com.capgemini.psd2.customer.account.list.mongo.db.adapter.test.mock.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.capgemini.psd2.aisp.domain.OBAccount6;
import com.capgemini.psd2.aisp.domain.OBAccount6Account;
import com.capgemini.psd2.aisp.domain.OBBranchAndFinancialInstitutionIdentification50;
import com.capgemini.psd2.cisp.mock.domain.MockChannelProfile;
import com.capgemini.psd2.logger.PSD2Constants;

public class CustomerAccountListMockData {

	public static List<OBAccount6> customerAccountInfoList;

	public static List<OBAccount6>  getCustomerAccountInfoList(){
		List<OBAccount6> customerAccountInfoList = new ArrayList<>();
		MockChannelProfile accnt = new MockChannelProfile();
		accnt.setAccountId("John Doe");
		accnt.setCurrency("EUR");
		accnt.setNickname("John");
		Map<String,String> additionalInfo = new HashMap<>();
		additionalInfo.put(PSD2Constants.ACCOUNT_NSC,"123456");
		additionalInfo.put(PSD2Constants.ACCOUNT_NUMBER,"78910111");
		additionalInfo.put("UK_OBIE_IBAN","23344455");
		accnt.setAdditionalInformation(additionalInfo);
		List<OBAccount6Account> accountList = new ArrayList<>();
		OBAccount6Account account = new OBAccount6Account();
		account.setIdentification("23344455");
		account.setSchemeName("IBAN");
		accountList.add(account);
		OBBranchAndFinancialInstitutionIdentification50 servicer = new OBBranchAndFinancialInstitutionIdentification50();
		servicer.setIdentification("23344455");
		accnt.setServicer(servicer );
		accnt.setAccount(accountList);

		MockChannelProfile acct = new MockChannelProfile();
		acct.setAccountId("John Does");
		acct.setCurrency("EUR");
		acct.setNickname("Johns");
		List<OBAccount6Account> accountList1 = new ArrayList<>();
		OBAccount6Account account1 = new OBAccount6Account();
		account1.setIdentification("23344455");
		account1.setSchemeName("IBAN");
		accountList1.add(account1);
		additionalInfo.put(PSD2Constants.ACCOUNT_NSC,"123456");
		additionalInfo.put(PSD2Constants.ACCOUNT_NUMBER,"78910111");
		acct.setAdditionalInformation(additionalInfo);
		acct.setServicer(servicer);
		acct.setAccount(accountList1);
		
		MockChannelProfile acct1 = new MockChannelProfile();
		acct1.setAccountId("John Does");
		acct1.setCurrency("EUR");
		acct1.setNickname("Johns");
		List<OBAccount6Account> accountList2 = new ArrayList<>();
		OBAccount6Account account2 = new OBAccount6Account();
		account2.setIdentification("23344455");
		account2.setSchemeName("IBAN");
		acct1.setAdditionalInformation(additionalInfo);
		accountList2.add(account2);
		acct1.setServicer(servicer);
		acct1.setAccount(accountList2);
		
		customerAccountInfoList.add(accnt);
		customerAccountInfoList.add(acct);
		customerAccountInfoList.add(acct1);
		
		return customerAccountInfoList;
	}
}
