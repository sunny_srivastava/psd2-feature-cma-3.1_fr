package com.capgemini.psd2.pisp.payment.setup.service.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMethod;

import com.capgemini.psd2.exceptions.ErrorMapKeys;
import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.domain.CustomIPaymentConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomIPaymentConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.CustomPaymentSetupPlatformDetails;
import com.capgemini.psd2.pisp.domain.OBExternalConsentStatus1Code;
import com.capgemini.psd2.pisp.domain.OBRemittanceInformation1;
import com.capgemini.psd2.pisp.domain.OBRisk1DeliveryAddress;
import com.capgemini.psd2.pisp.domain.PaymentConsentsPlatformResource;
import com.capgemini.psd2.pisp.domain.PaymentResponseInfo;
import com.capgemini.psd2.pisp.domain.PaymentRetrieveGetRequest;
import com.capgemini.psd2.pisp.enums.PaymentTypeEnum;
import com.capgemini.psd2.pisp.payment.setup.comparator.InternationalPaymentSetupPayloadComparator;
import com.capgemini.psd2.pisp.payment.setup.service.InternationalPaymentConsentsService;
import com.capgemini.psd2.pisp.processing.adapter.service.PaymentConsentProcessingAdapter;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.pisp.stage.operations.adapter.InternationalPaymentStagingAdapter;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Service
public class InternationalPaymentConsentsServiceImpl implements InternationalPaymentConsentsService {

	@Autowired
	private InternationalPaymentSetupPayloadComparator iPaymentConsentsComparator;

	@Autowired
	@Qualifier("iPaymentConsentsStagingRoutingAdapter")
	private InternationalPaymentStagingAdapter iPaymentStagingAdapter;

	@Autowired
	private RequestHeaderAttributes reqHeaderAttributes;
	
	@Autowired
	private PaymentConsentProcessingAdapter<CustomIPaymentConsentsPOSTRequest, CustomIPaymentConsentsPOSTResponse> paymemtRequestAdapter;
	
	@Value("${cmaVersion}")
	private String cmaVersion;

	@Override
	public CustomIPaymentConsentsPOSTResponse createInternationalPaymentConsentsResource(
			CustomIPaymentConsentsPOSTRequest internationalPaymentConsentsRequest) {

		CustomIPaymentConsentsPOSTResponse paymentConsentsFoundationResponse = null;
		String requestType = PSD2Constants.PAYMENT_RESOURCE_IDEMPOTENT;

		PaymentConsentsPlatformResource paymentConsentsPlatformResponse = paymemtRequestAdapter.preConsentProcessFlows(
				internationalPaymentConsentsRequest, populatePlatformDetails(internationalPaymentConsentsRequest));

		/* Non Idempotent Request */
		if (paymentConsentsPlatformResponse.getPaymentConsentId() == null) {
			requestType = PSD2Constants.PAYMENT_RESOURCE_NON_IDEMPOTENT;
			/* Create stage call to Adapter */
			paymentConsentsFoundationResponse = processInternationalPaymentStagingResource(
					internationalPaymentConsentsRequest, paymentConsentsPlatformResponse.getCreatedAt());
		}

		/* Idempotent Request */
		else
			paymentConsentsFoundationResponse = retrieveAndCompareInternationalStageResponse(
					internationalPaymentConsentsRequest, paymentConsentsPlatformResponse);

		PaymentResponseInfo stageDetails = populateStageDetails(paymentConsentsFoundationResponse);
		stageDetails.setRequestType(requestType);

		/* Fix for SIT issue #2272 
		 * Tpp providing Blank remittance info, and FS is returning null,
		 * Thus in such case returning same Blank remittance info to TPP.
		 * 27.02.2019 */
		OBRemittanceInformation1 requestRemittanceInfo = internationalPaymentConsentsRequest.getData().getInitiation().getRemittanceInformation();
		paymentConsentsFoundationResponse.getData().getInitiation().setRemittanceInformation(requestRemittanceInfo);
		OBRisk1DeliveryAddress reqRiskDeliveryAddress = internationalPaymentConsentsRequest.getRisk().getDeliveryAddress();
		paymentConsentsFoundationResponse.getRisk().setDeliveryAddress(reqRiskDeliveryAddress);
		
		/*
		 * Post Process Flows : Update Platform resource(for Non Idempotent), Transform
		 * response, Validate response
		 */
		return paymemtRequestAdapter.postConsentProcessFlows(stageDetails, paymentConsentsFoundationResponse,
				paymentConsentsPlatformResponse, RequestMethod.POST.toString());

	}

	private CustomPaymentSetupPlatformDetails populatePlatformDetails(CustomIPaymentConsentsPOSTRequest request) {
		CustomPaymentSetupPlatformDetails standingConsentsPlatformDetails = new CustomPaymentSetupPlatformDetails();
		standingConsentsPlatformDetails.setPaymentType(PaymentTypeEnum.INTERNATIONAL_PAY);
		standingConsentsPlatformDetails.setSetupCmaVersion(cmaVersion);
		standingConsentsPlatformDetails = checkDebtorDetails(request, standingConsentsPlatformDetails);
		return standingConsentsPlatformDetails;
	}

	public CustomPaymentSetupPlatformDetails checkDebtorDetails(CustomIPaymentConsentsPOSTRequest request,
			CustomPaymentSetupPlatformDetails platformDetails) {
		String tppDebtorDetailsStatus = String.valueOf(Boolean.FALSE);
		String tppDebtorNameDetailsStatus = String.valueOf(Boolean.FALSE);
		if (request.getData() != null && request.getData().getInitiation() != null
				&& request.getData().getInitiation().getDebtorAccount() != null) {
			tppDebtorDetailsStatus = String.valueOf(Boolean.TRUE);
			if (!NullCheckUtils.isNullOrEmpty(request.getData().getInitiation().getDebtorAccount().getName()))
				tppDebtorNameDetailsStatus = String.valueOf(Boolean.TRUE);
		}
		platformDetails.setTppDebtorDetails(tppDebtorDetailsStatus);
		platformDetails.setTppDebtorNameDetails(tppDebtorNameDetailsStatus);
		return platformDetails;
	}

	private CustomIPaymentConsentsPOSTResponse processInternationalPaymentStagingResource(
			CustomIPaymentConsentsPOSTRequest internationalPaymentConsentsRequest, String setupCreationDate) {

		/* new fields added */
		internationalPaymentConsentsRequest.setCreatedOn(setupCreationDate);

		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();
		stageIdentifiers.setPaymentSetupVersion(cmaVersion);
		stageIdentifiers.setPaymentTypeEnum(PaymentTypeEnum.INTERNATIONAL_PAY);

		Map<String, String> paramMap = new HashMap<String, String>();
		paramMap.put(PSD2Constants.CO_RELATION_ID, reqHeaderAttributes.getCorrelationId());
		return iPaymentStagingAdapter.processInternationalPaymentConsents(internationalPaymentConsentsRequest,
				stageIdentifiers, paramMap, OBExternalConsentStatus1Code.AWAITINGAUTHORISATION,
				OBExternalConsentStatus1Code.REJECTED);
	}

	@Override
	public CustomIPaymentConsentsPOSTResponse retrieveInternationalPaymentConsentsResource(
			PaymentRetrieveGetRequest request) {
		request.setPaymentTypeEnum(PaymentTypeEnum.INTERNATIONAL_PAY);
		PaymentConsentsPlatformResource paymentConsentsPlatformResponse = paymemtRequestAdapter
				.preConsentProcessGETFlow(request);

		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();
		stageIdentifiers.setPaymentConsentId(request.getConsentId());
		stageIdentifiers.setPaymentTypeEnum(PaymentTypeEnum.INTERNATIONAL_PAY);

		stageIdentifiers.setPaymentSetupVersion(paymentConsentsPlatformResponse.getSetupCmaVersion());

		CustomIPaymentConsentsPOSTResponse paymentConsentsFoundationResponse = iPaymentStagingAdapter
				.retrieveStagedInternationalPaymentConsents(stageIdentifiers, null);

		PaymentResponseInfo stageDetails = new PaymentResponseInfo();
		stageDetails.setRequestType(PSD2Constants.PAYMENT_RESOURCE_IDEMPOTENT);
		return paymemtRequestAdapter.postConsentProcessFlows(stageDetails, paymentConsentsFoundationResponse,
				paymentConsentsPlatformResponse, RequestMethod.GET.toString());
	}

	private CustomIPaymentConsentsPOSTResponse retrieveAndCompareInternationalStageResponse(
			CustomIPaymentConsentsPOSTRequest paymentConsentsRequest,
			PaymentConsentsPlatformResource paymentConsentsPlatformResponse) {
		CustomIPaymentConsentsPOSTResponse paymentConsentsFoundationResponse;

		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();
		stageIdentifiers.setPaymentConsentId(paymentConsentsPlatformResponse.getPaymentConsentId());
		stageIdentifiers.setPaymentTypeEnum(PaymentTypeEnum.INTERNATIONAL_PAY);
		stageIdentifiers.setPaymentSetupVersion(paymentConsentsPlatformResponse.getSetupCmaVersion());

		paymentConsentsFoundationResponse = iPaymentStagingAdapter
				.retrieveStagedInternationalPaymentConsents(stageIdentifiers, null);

		if (iPaymentConsentsComparator.comparePaymentDetails(paymentConsentsFoundationResponse, paymentConsentsRequest,
				paymentConsentsPlatformResponse) > 0)
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_RESOURCE_CONSENTMISMATCH,
					ErrorMapKeys.RESOURCE_CONSENT_MISMATCH));

		return paymentConsentsFoundationResponse;
	}

	public PaymentResponseInfo populateStageDetails(CustomIPaymentConsentsPOSTResponse internationalStagingResource) {
		PaymentResponseInfo stageDetails = new PaymentResponseInfo();
		stageDetails.setConsentProcessStatus(internationalStagingResource.getConsentPorcessStatus());
		stageDetails.setPaymentConsentId(internationalStagingResource.getData().getConsentId());
		return stageDetails;
	}
}
