package com.capgemini.psd2.pisp.payment.setup.test.routing.adapter.routing;
// Test Class for IPaymentConsentsCoreSystemAdapterFactory
import static org.mockito.Matchers.anyString;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.pisp.payment.setup.routing.adapter.impl.IPaymentConsentsStagingRoutingAdapterImpl;
import com.capgemini.psd2.pisp.payment.setup.routing.adapter.routing.IPaymentConsentsCoreSystemAdapterFactory;
import com.capgemini.psd2.pisp.stage.operations.adapter.InternationalPaymentStagingAdapter;

@RunWith(SpringJUnit4ClassRunner.class)
public class IPaymentConsentsCoreSystemAdapterFactoryTest {

	@Mock
	private ApplicationContext applicationContext;

	@InjectMocks
	private IPaymentConsentsCoreSystemAdapterFactory factory;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void contextLoads() {
	}

	@Test
	public void testGetInternationalPaymentSetupStagingInstance() {
		InternationalPaymentStagingAdapter adapter = new IPaymentConsentsStagingRoutingAdapterImpl();
		Mockito.when(factory.getInternationalPaymentSetupStagingInstance(anyString())).thenReturn(adapter);
	}

	@Test
	public void setApplicationContextTest() {
		factory.setApplicationContext(applicationContext);
	}

	@After
	public void tearDown() throws Exception {
		factory = null;
		applicationContext = null;
	}

}
