package com.capgemini.psd2.foundationservice.customer.account.profile.boi.adapter.transformer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.adapter.exceptions.AdapterErrorCodeEnum;
import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.aisp.domain.OBAccount6;
import com.capgemini.psd2.aisp.domain.OBAccount6Account;
import com.capgemini.psd2.aisp.domain.OBBranchAndFinancialInstitutionIdentification50;
import com.capgemini.psd2.aisp.domain.OBExternalAccountSubType1Code;
import com.capgemini.psd2.aisp.domain.OBExternalAccountType1Code;
import com.capgemini.psd2.aisp.domain.OBReadAccount6;
import com.capgemini.psd2.aisp.domain.OBReadAccount6Data;
import com.capgemini.psd2.aisp.transformer.CustomerAccountListTransformer;
import com.capgemini.psd2.consent.domain.PSD2Account;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.DigitalUserProfile;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.PartyEntitlements;
import com.capgemini.psd2.foundationservice.customer.account.profile.boi.adapter.constants.CustomerAccountProfileFoundationServiceConstants;
import com.capgemini.psd2.fraudsystem.domain.PSD2CustomerInfo;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.utilities.NullCheckUtils;
import com.capgemini.psd2.utilities.StringUtils;
import com.capgemini.psd2.validator.PSD2Validator;

/**
 * The Class CustomerAccountProfileFoundationServiceTransformer
 *
 */

@Component
@ConfigurationProperties(prefix = "foundationService")
@Configuration
@EnableAutoConfiguration
public class CustomerAccountProfileFoundationServiceTransformer implements CustomerAccountListTransformer {

	@Autowired
	private PSD2Validator validator;

	@Value("${foundationService.accuntNumLength:8}")
	private String accuntNumLength;
	@Value("${foundationService.accountNSCLength:6}")
	private String accountNSCLength;

	@Override
	public <T> OBReadAccount6 transformCustomerAccountListAdapter(T source, Map<String, String> params) {
		OBReadAccount6Data data2 = new OBReadAccount6Data();
		List<OBAccount6> accountList = new ArrayList<>();
		OBReadAccount6 finalAIResponseObj = new OBReadAccount6();
		DigitalUserProfile digitalUserProfile = (DigitalUserProfile) source;
		String cmaVersion = params.get(PSD2Constants.CMAVERSION);
		System.out.println(cmaVersion);
		if (NullCheckUtils.isNullOrEmpty(cmaVersion))
			cmaVersion = "1.1";
		if (!digitalUserProfile.getDigitalUser().isDigitalUserLockedOutIndicator()
				&& digitalUserProfile.getPartyInformation().isPartyActiveIndicator()) {
			List<com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.AccountEntitlements> accountEntitlements = digitalUserProfile
					.getAccountEntitlements();
			for (com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.AccountEntitlements accntEntitlement : accountEntitlements) {
				PSD2Account responseObj = new PSD2Account();
				OBAccount6Account acc = new OBAccount6Account();
				OBBranchAndFinancialInstitutionIdentification50 servicer = new OBBranchAndFinancialInstitutionIdentification50();
				Map<String, String> additionalInformation = new HashMap<String, String>();
				responseObj.setAccountId("NA");
				responseObj.setCurrency(accntEntitlement.getAccount().getAccountCurrency().getIsoAlphaCode());
				responseObj.setAccountType(accntEntitlement.getAccount().getRetailNonRetailCode().equals("Retail")
						? OBExternalAccountType1Code.PERSONAL : OBExternalAccountType1Code.BUSINESS);
				responseObj.setNickname(accntEntitlement.getAccount().getAccountNickName());
				additionalInformation.put(PSD2Constants.ACCOUNT_NUMBER,
						accntEntitlement.getAccount().getAccountNumber());
				additionalInformation.put(PSD2Constants.PARTY_IDENTIFIER,
						digitalUserProfile.getPartyInformation().getPartySourceIdNumber());
				if (accntEntitlement.getAccount().getSourceSystemAccountType().toString()
						.equalsIgnoreCase(CustomerAccountProfileFoundationServiceConstants.CURRENTACCOUNT)) {
					responseObj.setAccountSubType(OBExternalAccountSubType1Code.CURRENTACCOUNT);
					acc.setSchemeName(
							CustomerAccountProfileFoundationServiceConstants.UK_OBIE_SortCodeAccountNumber_DOT);
					acc.setIdentification(accntEntitlement.getAccount().getCurrentAccountInformation()
							.getParentNationalSortCodeNSCNumber()
							.concat(accntEntitlement.getAccount().getAccountNumber()));
					if (!NullCheckUtils.isNullOrEmpty(accntEntitlement.getAccount().getAccountName()))
						acc.setName(accntEntitlement.getAccount().getAccountName());
					additionalInformation.put(PSD2Constants.ACCOUNT_NSC, accntEntitlement.getAccount()
							.getCurrentAccountInformation().getParentNationalSortCodeNSCNumber());
					additionalInformation.put(PSD2Constants.IBAN, accntEntitlement.getAccount()
							.getCurrentAccountInformation().getInternationalBankAccountNumberIBAN());
					additionalInformation.put(PSD2Constants.BIC, accntEntitlement.getAccount()
							.getCurrentAccountInformation().getSwiftBankIdentifierCodeBIC());
					additionalInformation.put(
							CustomerAccountProfileFoundationServiceConstants.UK_OBIE_SortCodeAccountNumber,
							accntEntitlement.getAccount().getCurrentAccountInformation()
									.getParentNationalSortCodeNSCNumber()
									+ accntEntitlement.getAccount().getAccountNumber());
					additionalInformation.put(CustomerAccountProfileFoundationServiceConstants.SORTCODEACCOUNTNUMBER,
							accntEntitlement.getAccount().getCurrentAccountInformation()
									.getParentNationalSortCodeNSCNumber()
									+ accntEntitlement.getAccount().getAccountNumber());

				} else if (accntEntitlement.getAccount().getSourceSystemAccountType().toString()
						.equalsIgnoreCase(CustomerAccountProfileFoundationServiceConstants.SAVINGS)) {
					responseObj.setAccountSubType(OBExternalAccountSubType1Code.SAVINGS);
					acc.setSchemeName(
							CustomerAccountProfileFoundationServiceConstants.UK_OBIE_SortCodeAccountNumber_DOT);
					acc.setIdentification(accntEntitlement.getAccount().getSavingsAccountInformation()
							.getParentNationalSortCodeNSCNumber()
							.concat(accntEntitlement.getAccount().getAccountNumber()));
					if (!NullCheckUtils.isNullOrEmpty(accntEntitlement.getAccount().getAccountName()))
						acc.setName(accntEntitlement.getAccount().getAccountName());
					additionalInformation.put(PSD2Constants.ACCOUNT_NSC, accntEntitlement.getAccount()
							.getSavingsAccountInformation().getParentNationalSortCodeNSCNumber());
					additionalInformation.put(PSD2Constants.IBAN, accntEntitlement.getAccount()
							.getSavingsAccountInformation().getInternationalBankAccountNumberIBAN());
					additionalInformation.put(PSD2Constants.BIC, accntEntitlement.getAccount()
							.getSavingsAccountInformation().getSwiftBankIdentifierCodeBIC());
					additionalInformation.put(
							CustomerAccountProfileFoundationServiceConstants.UK_OBIE_SortCodeAccountNumber,
							accntEntitlement.getAccount().getSavingsAccountInformation()
									.getParentNationalSortCodeNSCNumber()
									+ accntEntitlement.getAccount().getAccountNumber());
					additionalInformation.put(CustomerAccountProfileFoundationServiceConstants.SORTCODEACCOUNTNUMBER,
							accntEntitlement.getAccount().getSavingsAccountInformation()
									.getParentNationalSortCodeNSCNumber()
									+ accntEntitlement.getAccount().getAccountNumber());

				} else if (accntEntitlement.getAccount().getSourceSystemAccountType().toString()
						.equalsIgnoreCase(CustomerAccountProfileFoundationServiceConstants.CREDITCARD)) {
					responseObj.setAccountSubType(OBExternalAccountSubType1Code.CREDITCARD);
					acc.setSchemeName(CustomerAccountProfileFoundationServiceConstants.UK_OBIE_PAN_DOT);
					acc.setIdentification(accntEntitlement.getAccount().getCreditCardAccountInformation().getCard()
							.getMaskedCardPANNumber());
					if (!NullCheckUtils.isNullOrEmpty(accntEntitlement.getAccount().getAccountName()))
						acc.setName(accntEntitlement.getAccount().getAccountName());
					additionalInformation.put(CustomerAccountProfileFoundationServiceConstants.TSYSCUTOMERID,
							accntEntitlement.getAccount().getCreditCardAccountInformation().getCard()
									.getCustomerReference());
					additionalInformation.put(PSD2Constants.PLAPPLID, accntEntitlement.getAccount().getAccountNumber());
					acc.setSecondaryIdentification(accntEntitlement.getAccount().getCreditCardAccountInformation()
							.getCard().getCustomerReference());

				}

				String permission = "";
				PartyEntitlements partyEntitlements = digitalUserProfile.getPartyEntitlements();

				for (String per1 : partyEntitlements.getEntitlements()) {
					if (permission.equals("")) {
						permission = per1;
					} else {
						permission = (permission.concat(",")).concat(per1.trim());
					}
				}
				
				for (String per : accntEntitlement.getEntitlements()) {
					if (permission.equals("")) {
						permission = per;
					} else {
						permission = (permission.concat(",")).concat(per.trim());
					}
				}
				
				additionalInformation.put(PSD2Constants.ACCOUNT_PERMISSION, permission);

				if ((accntEntitlement.getAccount().getSourceSystemAccountType().toString()
						.equalsIgnoreCase(CustomerAccountProfileFoundationServiceConstants.CURRENTACCOUNT))
						|| (accntEntitlement.getAccount().getSourceSystemAccountType().toString().equalsIgnoreCase(
								CustomerAccountProfileFoundationServiceConstants.SAVINGS) && !cmaVersion.equals("2.0"))
						|| (accntEntitlement.getAccount().getSourceSystemAccountType().toString()
								.equalsIgnoreCase(CustomerAccountProfileFoundationServiceConstants.CREDITCARD)
								&& !cmaVersion.equals("2.0"))) {
					List<OBAccount6Account> obAccount2Accountlist = new ArrayList<OBAccount6Account>();
					validator.validate(acc);
					obAccount2Accountlist.add(acc);
					validator.validate(obAccount2Accountlist);
					responseObj.setAccount(obAccount2Accountlist);
					String hashedValue = StringUtils.generateHashedValue(acc.getIdentification());
					responseObj.setHashedValue(hashedValue);
					responseObj.setAdditionalInformation(additionalInformation);
					validator.validate(responseObj);
					accountList.add(responseObj);
				}

			}
			data2.setAccount(accountList);
			finalAIResponseObj.setData(data2);

		} else {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.ACCOUNT_DETAILS_NOT_FOUND);
		}
		return finalAIResponseObj;
	}

	@Override
	public <T> PSD2CustomerInfo transformCustomerInfo(T source, Map<String, String> params) {
		// TODO Auto-generated method stub
		return null;
	}
}
