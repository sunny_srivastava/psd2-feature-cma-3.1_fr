package com.capgemini.psd2.foundationservice.customer.account.profile.boi.adapter.delegate;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.adapter.exceptions.AdapterErrorCodeEnum;
import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.adapter.utility.AdapterUtility;
import com.capgemini.psd2.aisp.domain.OBReadAccount6;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.DigitalUserProfile;
import com.capgemini.psd2.foundationservice.customer.account.profile.boi.adapter.transformer.CustomerAccountProfileFoundationServiceTransformer;
import com.capgemini.psd2.foundationservice.customer.account.profile.boi.adapter.transformer.CustomerAccountProfileFoundationServiceTransformerPISP;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.utilities.NullCheckUtils;

/**
 * The class CustomerAccountProfileFoundationServiceDelegate
 *
 */
@Component
public class CustomerAccountProfileFoundationServiceDelegate {

	/** The user in req header. */
	@Value("${foundationService.userInReqHeader:#{X-BOI-USER}}")
	private String userInReqHeader;

	/** The channel in req header. */
	@Value("${foundationService.channelInReqHeader:#{X-BOI-CHANNEL}}")
	private String channelInReqHeader;

	/** The workstation in req header. */
	@Value("${foundationService.platformInReqHeader:#{X-BOI-WORKSTATION}}")
	private String platformInReqHeader;

	/** The correlation req header. */
	@Value("${foundationService.correlationReqHeader:#{X-BOI-WORKSTATION}}")
	private String correlationReqHeader;

	@Value("${foundationService.transactioReqHeader:#{X-API-TRANSACTION-ID}}")
	private String transactioReqHeader;

	/** The correlation-ID req header. */
	@Value("${foundationService.correlationMuleReqHeader:#{X-API-CORRELATION-ID}}")
	private String correlationMuleReqHeader;

	/** The correlation-ID req header. */
	@Value("${foundationService.sourceSystemReqHeader:#{X-API-SOURCE-SYSTEM}}")
	private String sourceSystemReqHeader;

	/** The correlation-ID req header. */
	@Value("${foundationService.sourceUserReqHeader:#{X-API-SOURCE-USER}}")
	private String sourceUserReqHeader;

	/** The platform. */
	@Value("${app.platform}")
	private String platform;

	/* Customer Account Profile Foundation Service Transformer */
	@Autowired
	private CustomerAccountProfileFoundationServiceTransformer custAccntProfileSerTrans;
	
	@Autowired
	private CustomerAccountProfileFoundationServiceTransformerPISP custAccntProfileSerTransPISP;

	@Autowired
	private AdapterUtility adapterUtility;

	/*
	 * This method is calling customer account profile transformer to convert
	 * CMA response in CMA object
	 */
	public OBReadAccount6 transformResponseFromFDToAPI(DigitalUserProfile digitalUserProfile,
			Map<String, String> params) {
		OBReadAccount6 oBReadAccount2 = new OBReadAccount6();
		oBReadAccount2 =  custAccntProfileSerTrans.transformCustomerAccountListAdapter(digitalUserProfile, params);
		
		return oBReadAccount2;
	}
		
	public OBReadAccount6 transformResponseFromFDToAPIPISP(com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.PISP.DigitalUserProfile digitalUserProfile,
			Map<String, String> params) {
		OBReadAccount6 oBReadAccount2 = new OBReadAccount6();
		oBReadAccount2 =  custAccntProfileSerTransPISP.transformCustomerAccountListAdapter(digitalUserProfile, params);
		
		return oBReadAccount2;
		}
		
	public String getFoundationServiceMuleURL(Map<String, String> params, String userId) {
		String baseURL = adapterUtility
				.retriveCustProfileFoundationServiceURLAISP((params.get(PSD2Constants.CHANNEL_ID)).toUpperCase());
		return baseURL + params.get(PSD2Constants.CHANNEL_ID).toUpperCase() + "/digitalusers/" + userId
				+ "/digital-user-profile";
	}

	public HttpHeaders createRequestMuleHeaders(Map<String, String> params) {
		if (NullCheckUtils.isNullOrEmpty(params.get(PSD2Constants.USER_ID))
				|| NullCheckUtils.isNullOrEmpty(params.get(PSD2Constants.CORRELATION_ID))) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.BAD_REQUEST);
		}
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.add(sourceUserReqHeader, params.get(PSD2Constants.USER_ID));
		httpHeaders.add(sourceSystemReqHeader, "PSD2API");
		httpHeaders.add(correlationMuleReqHeader, "PSD2API");
		httpHeaders.add(transactioReqHeader, params.get(PSD2Constants.CORRELATION_ID));
		return httpHeaders;
	}

}