package com.capgemini.psd2.foundationservice.customer.account.profile.boi.adapter.test;

import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.aisp.domain.OBReadAccount6;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.Account;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.AccountEntitlements;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.Card;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.CreditCardAccount;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.Currency;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.CurrentAccount;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.DigitalUser;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.DigitalUserProfile;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.PartyBasicInformation;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.PartyEntitlements;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.RetailNonRetailCode;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.SourceSystemAccountType;
import com.capgemini.psd2.foundationservice.customer.account.profile.boi.adapter.constants.CustomerAccountProfileFoundationServiceConstants;
import com.capgemini.psd2.foundationservice.customer.account.profile.boi.adapter.transformer.CustomerAccountProfileFoundationServiceTransformer;
import com.capgemini.psd2.validator.PSD2Validator;

@RunWith(SpringJUnit4ClassRunner.class)
public class CustomerAccountProfileFoundationServiceTransformerTests {

	@InjectMocks
	CustomerAccountProfileFoundationServiceTransformer cusAccProFoundServiceTransformer;

	@InjectMocks
	private CustomerAccountProfileFoundationServiceTransformer delegate;

	@Mock
	private PSD2Validator validator;

	/**
	 * Sets the up.
	 */
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * Context loads.
	 */
	@Test
	public void contextLoads() {
	}

	@Test
	public void testTransformResponseFromFDToAPI1() {
		Map<String, String> params = new HashMap<>();
		params.put(CustomerAccountProfileFoundationServiceConstants.CURRENTACCOUNT, "Current Account");
		DigitalUser du = new DigitalUser();
		du.setDigitalUserLockedOutIndicator(false);
		DigitalUserProfile dup = new DigitalUserProfile();
		dup.setDigitalUser(du);
		PartyBasicInformation pi = new PartyBasicInformation();
		pi.setPartyActiveIndicator(true);
		dup.setPartyInformation(pi);
		AccountEntitlements acen = new AccountEntitlements();
		List<AccountEntitlements> accountEntitlements = new ArrayList<AccountEntitlements>();
		List<String> ent = new ArrayList<>();
		ent.add("Display Only Account");
		Account account = new Account();
		RetailNonRetailCode retail = RetailNonRetailCode.fromValue("Retail");
		SourceSystemAccountType sourceSystemAccountTypeCurrent = SourceSystemAccountType.fromValue("Current Account");
		account.setAccountNumber("345");
		Currency currency = new Currency();
		CurrentAccount cuacinfo = new CurrentAccount();
		currency.setIsoAlphaCode("EUR");
		account.setAccountCurrency(currency);
		account.setRetailNonRetailCode(retail);
		account.setSourceSystemAccountType(sourceSystemAccountTypeCurrent);
		cuacinfo.setParentNationalSortCodeNSCNumber("ACCOUNT_NSC");
		cuacinfo.setInternationalBankAccountNumberIBAN("IBAN");
		cuacinfo.setSwiftBankIdentifierCodeBIC("BIC");
		account.setCurrentAccountInformation(cuacinfo);
		acen.setAccount(account);
		acen.setEntitlements(ent);
		accountEntitlements.add(0, acen);
		dup.setAccountEntitlements(accountEntitlements);
		PartyEntitlements pe = new PartyEntitlements();
		pe.setEntitlements(ent);
		dup.setPartyEntitlements(pe);
		OBReadAccount6 ob = cusAccProFoundServiceTransformer.transformCustomerAccountListAdapter(dup, params);
		assertNotNull(ob);
	}

	@Test
	public void testTransformResponseFromFDToAPI1Business() {
		Map<String, String> params = new HashMap<>();
		params.put(CustomerAccountProfileFoundationServiceConstants.CURRENTACCOUNT, "Current Account");
		DigitalUser du = new DigitalUser();
		du.setDigitalUserLockedOutIndicator(false);
		DigitalUserProfile dup = new DigitalUserProfile();
		dup.setDigitalUser(du);
		PartyBasicInformation pi = new PartyBasicInformation();
		pi.setPartyActiveIndicator(true);
		dup.setPartyInformation(pi);
		AccountEntitlements acen = new AccountEntitlements();
		List<AccountEntitlements> accountEntitlements = new ArrayList<AccountEntitlements>();
		List<String> ent = new ArrayList<>();
		ent.add("Display Only Account");
		Account account = new Account();
		RetailNonRetailCode retail = RetailNonRetailCode.fromValue("Non-Retail");
		SourceSystemAccountType sourceSystemAccountTypeCurrent = SourceSystemAccountType.fromValue("Current Account");
		account.setAccountNumber("345");
		Currency currency = new Currency();
		CurrentAccount cuacinfo = new CurrentAccount();
		currency.setIsoAlphaCode("EUR");
		account.setAccountCurrency(currency);
		account.setRetailNonRetailCode(retail);
		account.setSourceSystemAccountType(sourceSystemAccountTypeCurrent);
		cuacinfo.setParentNationalSortCodeNSCNumber("ACCOUNT_NSC");
		cuacinfo.setInternationalBankAccountNumberIBAN("IBAN");
		cuacinfo.setSwiftBankIdentifierCodeBIC("BIC");
		account.setCurrentAccountInformation(cuacinfo);
		acen.setAccount(account);
		acen.setEntitlements(ent);
		accountEntitlements.add(0, acen);
		dup.setAccountEntitlements(accountEntitlements);
		PartyEntitlements pe = new PartyEntitlements();
		pe.setEntitlements(ent);
		dup.setPartyEntitlements(pe);
		OBReadAccount6 ob = cusAccProFoundServiceTransformer.transformCustomerAccountListAdapter(dup, params);
		assertNotNull(ob);
	}

	@Test
	public void testTransformResponseFromFDToAPICredit() {
		Map<String, String> params = new HashMap<>();
		params.put(CustomerAccountProfileFoundationServiceConstants.SAVINGS, "Savings Account");
		DigitalUser du = new DigitalUser();
		du.setDigitalUserLockedOutIndicator(false);
		DigitalUserProfile dup = new DigitalUserProfile();
		PartyBasicInformation pi = new PartyBasicInformation();
		pi.setPartyActiveIndicator(true);
		dup.setPartyInformation(pi);
		dup.setDigitalUser(du);
		AccountEntitlements acen = new AccountEntitlements();
		List<AccountEntitlements> accountEntitlements = new ArrayList<AccountEntitlements>();
		List<String> ent = new ArrayList<>();
		ent.add("Display Only Account");
		Account account = new Account();
		RetailNonRetailCode retail = RetailNonRetailCode.fromValue("Retail");
		SourceSystemAccountType sourceSystemAccountTypeSavings = SourceSystemAccountType.fromValue("Savings Account");
		account.setAccountNumber("345");
		Currency currency = new Currency();
		CurrentAccount cuacinfo = new CurrentAccount();
		currency.setIsoAlphaCode("EUR");
		account.setAccountCurrency(currency);
		account.setRetailNonRetailCode(retail);
		account.setSourceSystemAccountType(sourceSystemAccountTypeSavings);
		cuacinfo.setParentNationalSortCodeNSCNumber("ACCOUNT_NSC");
		cuacinfo.setInternationalBankAccountNumberIBAN("IBAN");
		cuacinfo.setSwiftBankIdentifierCodeBIC("BIC");
		account.setSavingsAccountInformation(cuacinfo);
		acen.setAccount(account);
		acen.setEntitlements(ent);
		accountEntitlements.add(0, acen);
		dup.setAccountEntitlements(accountEntitlements);
		PartyEntitlements pe = new PartyEntitlements();
		pe.setEntitlements(ent);
		dup.setPartyEntitlements(pe);
		OBReadAccount6 ob = cusAccProFoundServiceTransformer.transformCustomerAccountListAdapter(dup, params);
		assertNotNull(ob);
	}

	@Test
	public void testTransformResponseFromFDToAPISaving() {
		Map<String, String> params = new HashMap<>();
		params.put(CustomerAccountProfileFoundationServiceConstants.CREDITCARD, "Credit Card");
		DigitalUser du = new DigitalUser();
		du.setDigitalUserLockedOutIndicator(false);
		DigitalUserProfile dup = new DigitalUserProfile();
		dup.setDigitalUser(du);
		PartyBasicInformation pi = new PartyBasicInformation();
		pi.setPartyActiveIndicator(true);
		dup.setPartyInformation(pi);
		AccountEntitlements acen = new AccountEntitlements();
		List<AccountEntitlements> accountEntitlements = new ArrayList<AccountEntitlements>();
		List<String> ent = new ArrayList<>();
		ent.add("Display Only Account");
		Account account = new Account();
		RetailNonRetailCode retail = RetailNonRetailCode.fromValue("Retail");
		SourceSystemAccountType sourceSystemAccountTypeCredit = SourceSystemAccountType.fromValue("Credit Card");
		account.setAccountNumber("345");
		Currency currency = new Currency();
		CreditCardAccount cuacinfo = new CreditCardAccount();
		currency.setIsoAlphaCode("EUR");
		account.setAccountCurrency(currency);
		account.setRetailNonRetailCode(retail);
		account.setSourceSystemAccountType(sourceSystemAccountTypeCredit);
		Card card = new Card();
		card.setCustomerReference("customerReference");
		card.setMaskedCardPANNumber("fchg");
		cuacinfo.setCard(card);
		account.setCreditCardAccountInformation(cuacinfo);
		acen.setAccount(account);
		acen.setEntitlements(ent);
		accountEntitlements.add(0, acen);
		dup.setAccountEntitlements(accountEntitlements);
		PartyEntitlements pe = new PartyEntitlements();
		pe.setEntitlements(ent);
		dup.setPartyEntitlements(pe);
		OBReadAccount6 ob = cusAccProFoundServiceTransformer.transformCustomerAccountListAdapter(dup, params);
		assertNotNull(ob);
	}

	@Test(expected = AdapterException.class)
	public void testTransformResponseFromFDToAPIException() {
		Map<String, String> params = new HashMap<>();
		DigitalUser du = new DigitalUser();
		du.setDigitalUserLockedOutIndicator(false);
		DigitalUserProfile dup = new DigitalUserProfile();
		dup.setDigitalUser(du);
		PartyBasicInformation pi = new PartyBasicInformation();
		pi.setPartyActiveIndicator(false);
		dup.setPartyInformation(pi);
		AccountEntitlements acen = new AccountEntitlements();
		List<AccountEntitlements> accountEntitlements = new ArrayList<AccountEntitlements>();
		accountEntitlements.add(acen);
		Currency currency = new Currency();
		CurrentAccount cuacinfo = new CurrentAccount();
		currency.setIsoAlphaCode("EUR");
		Account account = new Account();
		RetailNonRetailCode retail = RetailNonRetailCode.fromValue("Retail");
		SourceSystemAccountType sourceSystemAccountTypeCurrent = SourceSystemAccountType.fromValue("Current Account");
		account.setAccountCurrency(currency);
		acen.setAccount(account);
		account.setRetailNonRetailCode(retail);
		acen.setAccount(account);
		account.setSourceSystemAccountType(sourceSystemAccountTypeCurrent);
		acen.setAccount(account);
		cuacinfo.setParentNationalSortCodeNSCNumber("parentNationalSortCodeNSCNumber");
		account.setCurrentAccountInformation(cuacinfo);
		acen.setAccount(account);
		Account account1 = new Account();
		account1.setAccountNumber("2345");
		AccountEntitlements acen1 = new AccountEntitlements();
		acen1.setAccount(account1);
		Mockito.when(delegate.transformCustomerAccountListAdapter(dup, params)).thenReturn(new OBReadAccount6());
		OBReadAccount6 ob = cusAccProFoundServiceTransformer.transformCustomerAccountListAdapter(dup, params);

	}

}
