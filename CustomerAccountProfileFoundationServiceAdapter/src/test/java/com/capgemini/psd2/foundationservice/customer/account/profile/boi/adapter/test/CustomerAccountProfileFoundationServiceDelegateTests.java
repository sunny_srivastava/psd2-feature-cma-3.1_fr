package com.capgemini.psd2.foundationservice.customer.account.profile.boi.adapter.test;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.adapter.utility.AdapterUtility;
import com.capgemini.psd2.aisp.domain.OBReadAccount6;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accounts;
//import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.DigitalUserProfile;
import com.capgemini.psd2.foundationservice.customer.account.profile.boi.adapter.client.CustomerAccountProfileFoundationServiceClient;
//import com.capgemini.psd2.foundationservice.customer.account.profile.boi.adapter.config.test.CustomerAccountProfileFoundationServiceAdapterTestConfig;
import com.capgemini.psd2.foundationservice.customer.account.profile.boi.adapter.constants.CustomerAccountProfileFoundationServiceConstants;
import com.capgemini.psd2.foundationservice.customer.account.profile.boi.adapter.delegate.CustomerAccountProfileFoundationServiceDelegate;
import com.capgemini.psd2.foundationservice.customer.account.profile.boi.adapter.domain.Accnt;
import com.capgemini.psd2.foundationservice.customer.account.profile.boi.adapter.domain.Accnts;
import com.capgemini.psd2.foundationservice.customer.account.profile.boi.adapter.raml.domain.DigitalUserProfile;
import com.capgemini.psd2.foundationservice.customer.account.profile.boi.adapter.transformer.CustomerAccountProfileFoundationServiceTransformer;
import com.capgemini.psd2.fraudsystem.domain.PSD2CustomerInfo;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.impl.RestClientSyncImpl;


@RunWith(SpringJUnit4ClassRunner.class)
public class CustomerAccountProfileFoundationServiceDelegateTests {

	@InjectMocks
	private CustomerAccountProfileFoundationServiceDelegate cusAccProFoundServiceDelegate;

	@Mock
	private CustomerAccountProfileFoundationServiceTransformer cusAccProFoundServiceTransformer;

	@Mock
	private CustomerAccountProfileFoundationServiceConstants CusAccProFoundServiceConstants;

	@InjectMocks
	private CustomerAccountProfileFoundationServiceClient CusAccProFounSerClient;

	@Mock
	private Environment env;

	@Mock
	private AdapterUtility adapterUtility;

	@Mock
	private RestClientSyncImpl restClient;
	/**
	 * Sets the up.
	 */
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * Context loads.
	 */
	@Test
	public void contextLoads() {
	}



	@Test
	public void testTransformResponseFromFDToAPI() {
		Accnts accounts = new Accnts();
		Accnt acc = new Accnt();
		acc.setAccountNumber("acct1234");
		acc.setAccountNSC("nsc1234");
		accounts.getAccount().add(acc);
		AccountMapping accountMapping = new AccountMapping();
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.DigitalUserProfile digitalUserProfile = new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.DigitalUserProfile();
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
		AccountDetails accDet = new AccountDetails();
		accDet.setAccountId("12345");
		accDet.setAccountNSC("nsc1234");
		accDet.setAccountNumber("acct1234");
		accDetList.add(accDet);
		accountMapping.setAccountDetails(accDetList);
		accountMapping.setTppCID("test");
		accountMapping.setPsuId("test");
		Map<String, String> params = new HashMap<>();
		params.put("consentFlowType", "AISP");
		Mockito.when(cusAccProFoundServiceDelegate.transformResponseFromFDToAPI(any(), any())).thenReturn(new OBReadAccount6());
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnts acc1= new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnts();

		OBReadAccount6 res = cusAccProFoundServiceDelegate.transformResponseFromFDToAPI(digitalUserProfile, params);
		assertNotNull(res);

	}

	@Test
	public void testTransformResponseCustomerInfoFromFDToAPI() {
		Accounts accounts = new Accounts();
		Accnt acc = new Accnt();
		acc.setAccountNumber("acct1234");
		acc.setAccountNSC("nsc1234");
		//  accounts.getAccount().add(acc);
		AccountMapping accountMapping = new AccountMapping();
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
		AccountDetails accDet = new AccountDetails();
		accDet.setAccountId("12345");
		accDet.setAccountNSC("nsc1234");
		accDet.setAccountNumber("acct1234");
		accDetList.add(accDet);
		accountMapping.setAccountDetails(accDetList);
		accountMapping.setTppCID("test");
		accountMapping.setPsuId("test");
		Mockito.when(cusAccProFoundServiceTransformer.transformCustomerInfo(any(), any())).thenReturn(new PSD2CustomerInfo());
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnts acc1= new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnts();

	}



	@Test
	public void testCreateRequestHeadersAISP2(){

		ReflectionTestUtils.setField(cusAccProFoundServiceDelegate, "userInReqHeader", "x-user-id");
		ReflectionTestUtils.setField(cusAccProFoundServiceDelegate, "channelInReqHeader", "x-channel-id");
		ReflectionTestUtils.setField(cusAccProFoundServiceDelegate, "platformInReqHeader", "platform");
		ReflectionTestUtils.setField(cusAccProFoundServiceDelegate, "correlationReqHeader", "x-fapi-interaction-id");
		ReflectionTestUtils.setField(cusAccProFoundServiceDelegate, "platform", "platform");
		ReflectionTestUtils.setField(cusAccProFoundServiceDelegate, "transactioReqHeader", "transactioReqHeader");
		ReflectionTestUtils.setField(cusAccProFoundServiceDelegate, "correlationMuleReqHeader", "correlationMuleReqHeader");
		ReflectionTestUtils.setField(cusAccProFoundServiceDelegate, "sourceSystemReqHeader", "sourceSystemReqHeader");
		ReflectionTestUtils.setField(cusAccProFoundServiceDelegate, "sourceUserReqHeader", "sourceUserReqHeader");
		HttpHeaders httpHeaders = new HttpHeaders();
		Map<String, String> params = new HashMap<>();

		params.put("x-user-id", "abcd");
		params.put("x-fapi-interaction-id", "abcd");

		httpHeaders=cusAccProFoundServiceDelegate.createRequestMuleHeaders(params);
		assertNotNull(httpHeaders);
	}



	@Test(expected = AdapterException.class)
	public void testCreateRequestHeadersUserIdNull(){

		Map<String, String> params = new HashMap<>();
		params.put(PSD2Constants.USER_ID, null);
		params.put(PSD2Constants.CHANNEL_ID, "channel");
		params.put(PSD2Constants.CORRELATION_ID, "correlation");
		cusAccProFoundServiceDelegate.createRequestMuleHeaders(params);
	}

	@Test(expected = AdapterException.class)
	public void testCreateRequestHeadersCorrelationIdNull(){

		Map<String, String> params = new HashMap<>();
		params.put(PSD2Constants.USER_ID, "user");
		params.put(PSD2Constants.CHANNEL_ID, "channel");
		params.put(PSD2Constants.CORRELATION_ID, null);
		cusAccProFoundServiceDelegate.createRequestMuleHeaders(params);
	}


	@Test
	public void testGetFoundationServiceUrlAISP(){
		
		Map<String, String> params = new HashMap<>();
		params.put(PSD2Constants.CHANNEL_ID, "channel");
		Mockito.when(adapterUtility.retriveCustProfileFoundationServiceURLAISP(anyObject())).thenReturn("test");
		String result = cusAccProFoundServiceDelegate.getFoundationServiceMuleURL(params, "test");
		assertNotNull(result);
	}


	@Test
	public void testCustomerAccountProfileClient(){
		DigitalUserProfile digitalUserProfile = new DigitalUserProfile();
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.add("X-BOI-USER", "header user");
		httpHeaders.add("X-BOI-CHANNEL", "header channel");
		httpHeaders.add("X-BOI-WORKSTATION", "header workstation");
		httpHeaders.add("X-BOI-PLATFORM", "header platform");
		DigitalUserProfile channelProfile=new DigitalUserProfile();
		RequestInfo requestInfo = new RequestInfo();
		Mockito.when(restClient.callForGet(any(), any(), any())).thenReturn(channelProfile);
		channelProfile = CusAccProFounSerClient.restTransportForParty(requestInfo, DigitalUserProfile.class, httpHeaders);
		assertNotNull(channelProfile);
	}
}
