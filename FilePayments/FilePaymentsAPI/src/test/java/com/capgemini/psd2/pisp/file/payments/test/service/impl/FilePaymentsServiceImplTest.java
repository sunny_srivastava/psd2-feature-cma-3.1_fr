package com.capgemini.psd2.pisp.file.payments.test.service.impl;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyMap;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.capgemini.psd2.consent.domain.PispConsent;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.adapter.FilePaymentConsentsAdapter;
import com.capgemini.psd2.pisp.adapter.FilePaymentsAdapter;
import com.capgemini.psd2.pisp.adapter.PispConsentAdapter;
import com.capgemini.psd2.pisp.domain.CustomFilePaymentConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.CustomFilePaymentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.OBCashAccountDebtor3;
import com.capgemini.psd2.pisp.domain.OBExternalStatus2Code;
import com.capgemini.psd2.pisp.domain.OBFile1;
import com.capgemini.psd2.pisp.domain.OBMultiAuthorisation1;
import com.capgemini.psd2.pisp.domain.OBWriteDataFile1;
import com.capgemini.psd2.pisp.domain.OBWriteDataFileConsentResponse1;
import com.capgemini.psd2.pisp.domain.OBWriteDataFileResponse1;
import com.capgemini.psd2.pisp.domain.PaymentConsentsPlatformResource;
import com.capgemini.psd2.pisp.domain.PaymentFileSubmitPOST201Response;
import com.capgemini.psd2.pisp.domain.PaymentsPlatformResource;
import com.capgemini.psd2.pisp.domain.PlatformFilePaymentsFileResponse;
import com.capgemini.psd2.pisp.file.payments.comparator.FilePaymentsPayloadComparator;
import com.capgemini.psd2.pisp.file.payments.service.impl.FilePaymentsServiceImpl;
import com.capgemini.psd2.pisp.payment.submission.platform.adapter.PaymentsPlatformAdapter;
import com.capgemini.psd2.pisp.processing.adapter.service.PaymentSubmissionProcessingAdapter;

@RunWith(SpringJUnit4ClassRunner.class)
public class FilePaymentsServiceImplTest {


	private MockMvc mockMvc;

	@Mock
	private FilePaymentsAdapter filePaymentCoreAdapter;

	@Mock
	private FilePaymentConsentsAdapter filePaymentConsentAdapter;

	@Mock
	private PaymentsPlatformAdapter paymentsPlatformAdapter;

	@Mock
	private FilePaymentsPayloadComparator filePayloadComparator;

	@Mock
	private PaymentSubmissionProcessingAdapter<CustomFilePaymentsPOSTRequest, PaymentFileSubmitPOST201Response> paymentsProcessingAdapter;

	@Mock
	private RequestHeaderAttributes reqHeaderAtrributes;
	
	@Mock
	private PispConsentAdapter pispConsentAdapter;

	@InjectMocks
	private FilePaymentsServiceImpl service;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		this.mockMvc = MockMvcBuilders.standaloneSetup(service).dispatchOptions(true).build();
	}

	@Test
	public void contextLoads() {
		
	}
	
	@Test
	public void testDownloadTransactionFileByConsentId() throws Exception {
		PlatformFilePaymentsFileResponse resource=new PlatformFilePaymentsFileResponse();
		PaymentsPlatformResource value=new PaymentsPlatformResource();
		
		when(paymentsPlatformAdapter
				.retrievePaymentsPlatformResource(anyString(), anyString())).thenReturn(value);
		when(filePaymentCoreAdapter.downloadFilePaymentsReport(anyString())).thenReturn(resource);
		
		service.downloadTransactionFileByConsentId("8de525df-ed9c-4031-a8a9-c0f3887b0de2");
	}
	
	@Test(expected=PSD2Exception.class)
	public void testDownloadTransactionFileByConsentIdNull() throws Exception {
		PlatformFilePaymentsFileResponse resource=null;
		PaymentsPlatformResource value=null;
		
		when(paymentsPlatformAdapter
				.retrievePaymentsPlatformResource(anyString(), anyString())).thenReturn(value);
		when(filePaymentCoreAdapter.downloadFilePaymentsReport(anyString())).thenReturn(resource);
		
		service.downloadTransactionFileByConsentId("8de525df-ed9c-4031-a8a9-c0f3887b0de2");
	}
	
	@Test
	public void testCreateFilePaymentsResource() throws Exception {
		
		CustomFilePaymentsPOSTRequest paymentRequest=new CustomFilePaymentsPOSTRequest();
		CustomFilePaymentConsentsPOSTResponse customResponse=new CustomFilePaymentConsentsPOSTResponse();
		PaymentConsentsPlatformResource paymentConsentPlatformResource=new PaymentConsentsPlatformResource();
		PaymentsPlatformResource submissionResource=new PaymentsPlatformResource();
		PaymentFileSubmitPOST201Response submissionPostResponse=new PaymentFileSubmitPOST201Response();
		PaymentsPlatformResource platformResource=new PaymentsPlatformResource();
		OBWriteDataFileResponse1 dataResponse=new OBWriteDataFileResponse1();
		OBMultiAuthorisation1 multiAuthorisation=new OBMultiAuthorisation1();
		OBWriteDataFileConsentResponse1 dataConsent=new OBWriteDataFileConsentResponse1();
		OBWriteDataFile1 data=new OBWriteDataFile1();
		OBFile1 initiation=new OBFile1();
		OBCashAccountDebtor3 debtorAccount=new OBCashAccountDebtor3();
		OBWriteDataFileResponse1 data12 = new OBWriteDataFileResponse1();
		
		multiAuthorisation.setStatus(OBExternalStatus2Code.AUTHORISED);
		dataResponse.setMultiAuthorisation(multiAuthorisation);
		submissionPostResponse.setData(dataResponse);
		platformResource.setProccessState("Incomplete");
		initiation.setDebtorAccount(debtorAccount);
		data.setInitiation(initiation);
		paymentRequest.setData(data);
		dataResponse.setInitiation(initiation);
		dataConsent.setInitiation(initiation);
		customResponse.setData(dataConsent);
		
		PispConsent consent = new PispConsent();
		consent.setPsuId("12345");
		when(pispConsentAdapter.retrieveConsentByPaymentId(anyObject())).thenReturn(consent);
		
		Map<String, Object> resourceMap=new HashMap<>();
		resourceMap.put("consent", paymentConsentPlatformResource);
		resourceMap.put("submission", submissionResource);
		
		Map<String, Object> map = new HashMap<>();
		Object testObject = new Object();
		map.put("For testing", testObject);
		
		Mockito.when(paymentsProcessingAdapter.preSubmissionProcessing(anyObject(), anyObject(), any()))
		.thenReturn(map);
		
		when(filePaymentConsentAdapter
				.fetchFilePaymentMetadata(anyObject(), anyObject())).thenReturn(customResponse);
		when(filePayloadComparator.comparePaymentDetails(anyObject(), anyObject(),
				anyObject())).thenReturn(0);
		when(paymentsProcessingAdapter.createInitialPaymentsPlatformResource(
					anyString(), anyString(),anyObject())).thenReturn(platformResource);
		when(filePaymentCoreAdapter.processPayments(anyObject(), anyObject(), anyObject())).thenReturn(submissionPostResponse);

		data12.setConsentId("121212");
		submissionPostResponse.setData(data12);

		when(paymentsProcessingAdapter.postPaymentProcessFlows(anyMap(), anyObject(), any(), anyString(), any(),
				anyString())).thenReturn(submissionPostResponse);
		
		service.createFilePaymentsResource(paymentRequest, true);
	}
	
	@Test
	public void testCreateFilePaymentsResourceNoDebtor() throws Exception {
		
		CustomFilePaymentsPOSTRequest paymentRequest=new CustomFilePaymentsPOSTRequest();
		CustomFilePaymentConsentsPOSTResponse customResponse=new CustomFilePaymentConsentsPOSTResponse();
		PaymentConsentsPlatformResource paymentConsentPlatformResource=new PaymentConsentsPlatformResource();
		PaymentsPlatformResource submissionResource=new PaymentsPlatformResource();
		PaymentFileSubmitPOST201Response submissionPostResponse=new PaymentFileSubmitPOST201Response();
		PaymentsPlatformResource platformResource=new PaymentsPlatformResource();
		OBWriteDataFileResponse1 dataResponse=new OBWriteDataFileResponse1();
		OBMultiAuthorisation1 multiAuthorisation=new OBMultiAuthorisation1();
		OBWriteDataFileConsentResponse1 dataConsent=new OBWriteDataFileConsentResponse1();
		OBWriteDataFile1 data=new OBWriteDataFile1();
		OBFile1 initiation=new OBFile1();
		
		multiAuthorisation.setStatus(OBExternalStatus2Code.AUTHORISED);
		dataResponse.setMultiAuthorisation(multiAuthorisation);
		submissionPostResponse.setData(dataResponse);
		platformResource.setProccessState("Incomplete");
		data.setInitiation(initiation);
		paymentRequest.setData(data);
		dataResponse.setInitiation(initiation);
		dataConsent.setInitiation(initiation);
		customResponse.setData(dataConsent);
		
		Map<String, Object> resourceMap=new HashMap<>();
		resourceMap.put("consent", paymentConsentPlatformResource);
		resourceMap.put("submission", submissionResource);
		
		Map<String, Object> map = new HashMap<>();
		Object testObject = new Object();
		map.put("For testing", testObject);

		Mockito.when(paymentsProcessingAdapter.preSubmissionProcessing(anyObject(), anyObject(), any()))
		.thenReturn(map);
		
		when(filePaymentConsentAdapter
				.fetchFilePaymentMetadata(anyObject(), anyObject())).thenReturn(customResponse);
		when(filePayloadComparator.comparePaymentDetails(anyObject(), anyObject(),
				anyObject())).thenReturn(0);
		when(paymentsProcessingAdapter.createInitialPaymentsPlatformResource(
					anyString(), anyString(),anyObject())).thenReturn(platformResource);
		when(filePaymentCoreAdapter.processPayments(anyObject(), anyObject(), anyObject())).thenReturn(submissionPostResponse);
		when(paymentsProcessingAdapter.postPaymentProcessFlows(anyMap(), anyObject(), any(), anyString(), any(),
				anyString())).thenReturn(submissionPostResponse);
		
		service.createFilePaymentsResource(paymentRequest, true);
	}
	
	@Test(expected=PSD2Exception.class)
	public void testCreateFilePaymentsResourceCompleted() throws Exception {
		
		CustomFilePaymentsPOSTRequest paymentRequest=new CustomFilePaymentsPOSTRequest();
		CustomFilePaymentConsentsPOSTResponse customResponse=new CustomFilePaymentConsentsPOSTResponse();
		PaymentConsentsPlatformResource paymentConsentPlatformResource=new PaymentConsentsPlatformResource();
		PaymentsPlatformResource submissionResource=new PaymentsPlatformResource();
		PaymentFileSubmitPOST201Response submissionPostResponse=new PaymentFileSubmitPOST201Response();
		PaymentsPlatformResource platformResource=new PaymentsPlatformResource();
		platformResource.setProccessState("Completed");
		OBWriteDataFileResponse1 dataResponse=new OBWriteDataFileResponse1();
		OBMultiAuthorisation1 multiAuthorisation=new OBMultiAuthorisation1();
		multiAuthorisation.setStatus(OBExternalStatus2Code.AUTHORISED);
		dataResponse.setMultiAuthorisation(multiAuthorisation);
		submissionPostResponse.setData(dataResponse);
		
		OBWriteDataFile1 data=new OBWriteDataFile1();
		OBFile1 initiation=new OBFile1();
		OBCashAccountDebtor3 debtorAccount=new OBCashAccountDebtor3();
		debtorAccount.setName("IBAN");
		initiation.setDebtorAccount(debtorAccount);
		data.setInitiation(initiation);
		paymentRequest.setData(data);
		dataResponse.setInitiation(initiation);

		OBWriteDataFileConsentResponse1 dataConsent=new OBWriteDataFileConsentResponse1();
		dataConsent.setInitiation(initiation);
		customResponse.setData(dataConsent);
		
		Map<String, Object> resourceMap=new HashMap<>();
		resourceMap.put("consent", paymentConsentPlatformResource);
		resourceMap.put("submission", submissionResource);
		
		Map<String, Object> map = new HashMap<>();
		Object testObject = new Object();
		map.put("For testing", testObject);
		
		Mockito.when(paymentsProcessingAdapter.preSubmissionProcessing(anyObject(), anyObject(), any()))
		.thenReturn(map);
		
		when(filePaymentConsentAdapter
				.fetchFilePaymentMetadata(anyObject(), anyObject())).thenReturn(customResponse);
		when(filePayloadComparator.comparePaymentDetails(anyObject(), anyObject(),
				anyObject())).thenReturn(0);

		when(paymentsProcessingAdapter.createInitialPaymentsPlatformResource(
					anyString(), anyString(),any())).thenReturn(platformResource);
		
		when(filePaymentCoreAdapter.processPayments(anyObject(), anyObject(), anyObject())).thenReturn(submissionPostResponse);
		
		PaymentFileSubmitPOST201Response submissionResponse=null;
		when(filePaymentCoreAdapter.retrieveStagedFilePayments(anyObject(), anyObject())).thenReturn(submissionResponse);

		OBWriteDataFileResponse1 data12 = new OBWriteDataFileResponse1();
		data12.setConsentId("121212");
		submissionPostResponse.setData(data12);

		Mockito.when(paymentsProcessingAdapter.postPaymentProcessFlows(anyMap(), anyObject(), any(), anyString(), any(),
				anyString())).thenReturn(submissionPostResponse);
		
		service.createFilePaymentsResource(paymentRequest, true);
	}
	
	@Test
	public void testCreateFilePaymentsResourceComplete() throws Exception {
		
		CustomFilePaymentsPOSTRequest paymentRequest=new CustomFilePaymentsPOSTRequest();
		CustomFilePaymentConsentsPOSTResponse customResponse=new CustomFilePaymentConsentsPOSTResponse();
		PaymentConsentsPlatformResource paymentConsentPlatformResource=new PaymentConsentsPlatformResource();
		PaymentsPlatformResource submissionResource=new PaymentsPlatformResource();
		PaymentFileSubmitPOST201Response submissionPostResponse=new PaymentFileSubmitPOST201Response();
		PaymentsPlatformResource platformResource=new PaymentsPlatformResource();
		platformResource.setProccessState("Completed");
		OBWriteDataFileResponse1 dataResponse=new OBWriteDataFileResponse1();
		OBMultiAuthorisation1 multiAuthorisation=new OBMultiAuthorisation1();
		multiAuthorisation.setStatus(OBExternalStatus2Code.AUTHORISED);
		dataResponse.setMultiAuthorisation(multiAuthorisation);
		submissionPostResponse.setData(dataResponse);
		
		OBWriteDataFile1 data=new OBWriteDataFile1();
		OBFile1 initiation=new OBFile1();
		OBCashAccountDebtor3 debtorAccount=new OBCashAccountDebtor3();
		debtorAccount.setName("IBAN");
		initiation.setDebtorAccount(debtorAccount);
		data.setInitiation(initiation);
		paymentRequest.setData(data);
		dataResponse.setInitiation(initiation);

		OBWriteDataFileConsentResponse1 dataConsent=new OBWriteDataFileConsentResponse1();
		dataConsent.setInitiation(initiation);
		customResponse.setData(dataConsent);
		
		Map<String, Object> resourceMap=new HashMap<>();
		resourceMap.put("consent", paymentConsentPlatformResource);
		resourceMap.put("submission", submissionResource);
		
		Map<String, Object> map = new HashMap<>();
		Object testObject = new Object();
		map.put("For testing", testObject);
		
		Mockito.when(paymentsProcessingAdapter.preSubmissionProcessing(anyObject(), anyObject(), any()))
		.thenReturn(map);
		
		when(filePaymentConsentAdapter
				.fetchFilePaymentMetadata(anyObject(), anyObject())).thenReturn(customResponse);
		when(filePayloadComparator.comparePaymentDetails(anyObject(), anyObject(),
				anyObject())).thenReturn(0);

		when(paymentsProcessingAdapter.createInitialPaymentsPlatformResource(
					anyString(), anyString(),any())).thenReturn(platformResource);
		
		when(filePaymentCoreAdapter.processPayments(anyObject(), anyObject(), anyObject())).thenReturn(submissionPostResponse);
		
		when(filePaymentCoreAdapter.retrieveStagedFilePayments(anyObject(), anyObject())).thenReturn(submissionPostResponse);

		OBWriteDataFileResponse1 data12 = new OBWriteDataFileResponse1();
		data12.setConsentId("121212");
		submissionPostResponse.setData(data12);

		Mockito.when(paymentsProcessingAdapter.postPaymentProcessFlows(anyMap(), anyObject(), any(), anyString(), any(),
				anyString())).thenReturn(submissionPostResponse);
		
		service.createFilePaymentsResource(paymentRequest, true);
	}
	
	@Test(expected=PSD2Exception.class)
	public void testCreateFilePaymentsResourceCompleteCompFailed() throws Exception {
		
		CustomFilePaymentsPOSTRequest paymentRequest=new CustomFilePaymentsPOSTRequest();
		CustomFilePaymentConsentsPOSTResponse customResponse=new CustomFilePaymentConsentsPOSTResponse();
		PaymentConsentsPlatformResource paymentConsentPlatformResource=new PaymentConsentsPlatformResource();
		PaymentsPlatformResource submissionResource=new PaymentsPlatformResource();
		PaymentFileSubmitPOST201Response submissionPostResponse=new PaymentFileSubmitPOST201Response();
		PaymentsPlatformResource platformResource=new PaymentsPlatformResource();
		platformResource.setProccessState("Completed");
		OBWriteDataFileResponse1 dataResponse=new OBWriteDataFileResponse1();
		OBMultiAuthorisation1 multiAuthorisation=new OBMultiAuthorisation1();
		multiAuthorisation.setStatus(OBExternalStatus2Code.AUTHORISED);
		dataResponse.setMultiAuthorisation(multiAuthorisation);
		submissionPostResponse.setData(dataResponse);
		
		OBWriteDataFile1 data=new OBWriteDataFile1();
		OBFile1 initiation=new OBFile1();
		OBCashAccountDebtor3 debtorAccount=new OBCashAccountDebtor3();
		debtorAccount.setName("IBAN");
		initiation.setDebtorAccount(debtorAccount);
		data.setInitiation(initiation);
		paymentRequest.setData(data);
		dataResponse.setInitiation(initiation);

		OBWriteDataFileConsentResponse1 dataConsent=new OBWriteDataFileConsentResponse1();
		dataConsent.setInitiation(initiation);
		customResponse.setData(dataConsent);
		
		Map<String, Object> resourceMap=new HashMap<>();
		resourceMap.put("consent", paymentConsentPlatformResource);
		resourceMap.put("submission", submissionResource);
		
		Map<String, Object> map = new HashMap<>();
		Object testObject = new Object();
		map.put("For testing", testObject);
		
		Mockito.when(paymentsProcessingAdapter.preSubmissionProcessing(anyObject(), anyObject(), any()))
		.thenReturn(map);
		
		when(filePaymentConsentAdapter
				.fetchFilePaymentMetadata(anyObject(), anyObject())).thenReturn(customResponse);
		when(filePayloadComparator.comparePaymentDetails(anyObject(), anyObject(),
				anyObject())).thenReturn(1);

		when(paymentsProcessingAdapter.createInitialPaymentsPlatformResource(
					anyString(), anyString(),any())).thenReturn(platformResource);
		
		when(filePaymentCoreAdapter.processPayments(anyObject(), anyObject(), anyObject())).thenReturn(submissionPostResponse);
		
		when(filePaymentCoreAdapter.retrieveStagedFilePayments(anyObject(), anyObject())).thenReturn(submissionPostResponse);

		OBWriteDataFileResponse1 data12 = new OBWriteDataFileResponse1();
		data12.setConsentId("121212");
		submissionPostResponse.setData(data12);

		Mockito.when(paymentsProcessingAdapter.postPaymentProcessFlows(anyMap(), anyObject(), any(), anyString(), any(),
				anyString())).thenReturn(submissionPostResponse);
		
		service.createFilePaymentsResource(paymentRequest, true);
	}

	@Test
	public void testFindStatusFilePaymentsResource() throws Exception {
		Map<String, Object> map = new HashMap<>();
		PaymentsPlatformResource resource = new PaymentsPlatformResource();
		resource.setSubmissionId("131313");
		resource.setPaymentConsentId("3333");
		map.put("submission", resource);
		
		Mockito.when(paymentsProcessingAdapter.prePaymentProcessGETFlows(anyString(),any())).thenReturn(map);
		
		PaymentFileSubmitPOST201Response response = new PaymentFileSubmitPOST201Response();
		OBWriteDataFileResponse1 data = new OBWriteDataFileResponse1();
		OBMultiAuthorisation1 multiAuthorisation = new OBMultiAuthorisation1();
		multiAuthorisation.setStatus(OBExternalStatus2Code.AUTHORISED);
		data.setMultiAuthorisation(multiAuthorisation);
		response.setData(data);

		PispConsent consent = new PispConsent();
		consent.setPsuId("12345");
		when(pispConsentAdapter.retrieveConsentByPaymentId(anyObject())).thenReturn(consent);
		
		Mockito.when(filePaymentCoreAdapter
				.retrieveStagedFilePayments(anyObject(), anyObject())).thenReturn(response);		
		
		PaymentFileSubmitPOST201Response dresponse = new PaymentFileSubmitPOST201Response();
		OBWriteDataFileResponse1 data1 = new OBWriteDataFileResponse1();
		data1.setConsentId("3333");
		dresponse.setData(data1);
		
		Mockito.when(paymentsProcessingAdapter.postPaymentProcessFlows(anyMap(), anyObject(), any(), anyString(), any(),
				anyString())).thenReturn(dresponse);
		
		service.findStatusFilePaymentsResource("8de525df-ed9c-4031-a8a9-c0f3887b0de2");
	}
	
	@Test(expected=PSD2Exception.class)
	public void testFindStatusFilePaymentsResourceExc() throws Exception {
		Map<String, Object> map = new HashMap<>();
		PaymentsPlatformResource resource = new PaymentsPlatformResource();
		resource.setSubmissionId("131313");
		resource.setPaymentConsentId("3333");
		map.put("submission", resource);
		
		Mockito.when(paymentsProcessingAdapter.prePaymentProcessGETFlows(anyString(),any())).thenReturn(map);
		
		PaymentFileSubmitPOST201Response response = null;

		Mockito.when(filePaymentCoreAdapter
				.retrieveStagedFilePayments(anyObject(), anyObject())).thenReturn(response);		
		
		PaymentFileSubmitPOST201Response dresponse = new PaymentFileSubmitPOST201Response();
		OBWriteDataFileResponse1 data1 = new OBWriteDataFileResponse1();
		data1.setConsentId("3333");
		dresponse.setData(data1);
		
		Mockito.when(paymentsProcessingAdapter.postPaymentProcessFlows(anyMap(), anyObject(), any(), anyString(), any(),
				anyString())).thenReturn(dresponse);
		
		service.findStatusFilePaymentsResource("8de525df-ed9c-4031-a8a9-c0f3887b0de2");
	}
}
