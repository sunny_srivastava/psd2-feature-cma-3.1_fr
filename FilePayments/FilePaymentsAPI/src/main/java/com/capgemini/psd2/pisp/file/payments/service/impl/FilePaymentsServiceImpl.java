package com.capgemini.psd2.pisp.file.payments.service.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.amazonaws.HttpMethod;
import com.capgemini.psd2.consent.domain.PispConsent;
import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.ErrorMapKeys;
import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.InternalServerErrorMessage;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.adapter.FilePaymentConsentsAdapter;
import com.capgemini.psd2.pisp.adapter.FilePaymentsAdapter;
import com.capgemini.psd2.pisp.adapter.PispConsentAdapter;
import com.capgemini.psd2.pisp.domain.CustomFilePaymentConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.CustomFilePaymentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomPaymentSetupPlatformDetails;
import com.capgemini.psd2.pisp.domain.OBExternalStatus1Code;
import com.capgemini.psd2.pisp.domain.OBExternalStatus2Code;
import com.capgemini.psd2.pisp.domain.PaymentConsentsPlatformResource;
import com.capgemini.psd2.pisp.domain.PaymentFileSubmitPOST201Response;
import com.capgemini.psd2.pisp.domain.PaymentsPlatformResource;
import com.capgemini.psd2.pisp.domain.PlatformFilePaymentsFileResponse;
import com.capgemini.psd2.pisp.enums.PaymentTypeEnum;
import com.capgemini.psd2.pisp.file.payments.comparator.FilePaymentsPayloadComparator;
import com.capgemini.psd2.pisp.file.payments.service.FilePaymentsService;
import com.capgemini.psd2.pisp.payment.submission.platform.adapter.PaymentsPlatformAdapter;
import com.capgemini.psd2.pisp.processing.adapter.service.PaymentSubmissionProcessingAdapter;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.pisp.status.PaymentConstants;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Service
public class FilePaymentsServiceImpl implements FilePaymentsService {

	@Autowired
	@Qualifier("filePaymentsStagingRoutingAdapter")
	private FilePaymentsAdapter filePaymentCoreAdapter;

	@Autowired
	@Qualifier("filePaymentConsentsStagingRoutingAdapter")
	private FilePaymentConsentsAdapter filePaymentConsentAdapter;

	@Autowired
	private RequestHeaderAttributes reqHeaderAtrributes;
	
	@Autowired
	private PispConsentAdapter pispConsentAdapter;
	
	@Autowired
	private PaymentsPlatformAdapter paymentsPlatformAdapter;

	@Autowired
	private FilePaymentsPayloadComparator filePayloadComparator;

	@Autowired
	private PaymentSubmissionProcessingAdapter<CustomFilePaymentsPOSTRequest, PaymentFileSubmitPOST201Response> paymentsProcessingAdapter;
	
	@Value("${cmaVersion}")
	private String cmaVersion;

	@Override
	public PlatformFilePaymentsFileResponse downloadTransactionFileByConsentId(String paymentId) {

		PlatformFilePaymentsFileResponse platformFilePaymentsFileResponse = null;

		/*
		 * Implement once submission is done
		 */

		PaymentsPlatformResource submissionPlatformResource = paymentsPlatformAdapter
				.retrievePaymentsPlatformResource(paymentId, paymentId);

		if (submissionPlatformResource == null)
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_RESOURCE_NOTFOUND,
					ErrorMapKeys.MISSING_PAYMENT_SUBMISSION_ID));

		platformFilePaymentsFileResponse = filePaymentCoreAdapter.downloadFilePaymentsReport(paymentId);

		return platformFilePaymentsFileResponse;
	}

	@Override
	public PaymentFileSubmitPOST201Response createFilePaymentsResource(CustomFilePaymentsPOSTRequest paymentRequest,
			boolean isPaymentValidated) {
		PaymentConsentsPlatformResource paymentConsentPlatformResource = null;
		PaymentsPlatformResource paymentsPlatformResource = null;
		PaymentFileSubmitPOST201Response paymentsFoundationResponse = null;

		/*
		 * Checking whether paymentId is already used for other payment initiation or
		 * not. If it is already used then return an error to PISP.
		 */

		Map<String, Object> paymentsPlatformResourceMap = paymentsProcessingAdapter.preSubmissionProcessing(
				paymentRequest, populatePlatformDetails(), paymentRequest.getData().getConsentId());

		paymentConsentPlatformResource = (PaymentConsentsPlatformResource) paymentsPlatformResourceMap.get("consent");
		paymentsPlatformResource = (PaymentsPlatformResource) paymentsPlatformResourceMap.get("submission");

		CustomFilePaymentConsentsPOSTResponse filePaymentConsentStageResource = filePayLoadCompare(paymentRequest,
				paymentConsentPlatformResource, paymentRequest.getData().getConsentId());

		/*
		 * For the first time submission resource will be null so save this new resource
		 * in platform
		 */
		if (paymentsPlatformResource == null) {
			paymentsPlatformResource = paymentsProcessingAdapter.createInitialPaymentsPlatformResource(
					paymentRequest.getData().getConsentId(), cmaVersion,
					PaymentTypeEnum.FILE_PAY);
			paymentsPlatformResourceMap.put("submission", paymentsPlatformResource);

		}

		/* Initially submission resource status will be incomplete */
		if (("Incomplete").equals(paymentsPlatformResource.getProccessState())) {
			paymentRequest = populateFileStagedProcessingFields(paymentRequest, filePaymentConsentStageResource);
			Map<String, OBExternalStatus1Code> paymentStatusMap = new HashMap<>();
			paymentStatusMap.put("initial", OBExternalStatus1Code.INITIATIONPENDING);
			paymentStatusMap.put("fail", OBExternalStatus1Code.INITIATIONFAILED);
			paymentStatusMap.put("Pass-MAuthrised", OBExternalStatus1Code.INITIATIONCOMPLETED);
			paymentStatusMap.put("Pass-MAwaitingAuthrise", OBExternalStatus1Code.INITIATIONPENDING);
			paymentStatusMap.put("Pass-MRejected", OBExternalStatus1Code.INITIATIONFAILED);
			
			Map<String, String> paramMap = new HashMap<String, String>();
			paramMap.put(PSD2Constants.CO_RELATION_ID, reqHeaderAtrributes.getCorrelationId());
			paymentsFoundationResponse = filePaymentCoreAdapter.processPayments(paymentRequest, paymentStatusMap, paramMap);
		}

		if (("Completed").equals(paymentsPlatformResource.getProccessState())) {
			CustomPaymentStageIdentifiers identifiers = new CustomPaymentStageIdentifiers();
			identifiers.setPaymentSetupVersion(cmaVersion);
			identifiers.setPaymentTypeEnum(PaymentTypeEnum.FILE_PAY);
			identifiers.setPaymentConsentId(paymentsPlatformResource.getPaymentConsentId());
			paymentsFoundationResponse = filePaymentCoreAdapter.retrieveStagedFilePayments(identifiers, null);
		}

		if (paymentsFoundationResponse == null) {
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
					InternalServerErrorMessage.PISP_NO_PAYMENT_SETUP_RESOURCES_FOUND));
		}

		OBExternalStatus2Code multiAuthStatus = OBExternalStatus2Code.AUTHORISED;
		if (paymentsFoundationResponse.getData() != null
				&& paymentsFoundationResponse.getData().getMultiAuthorisation() != null)
			multiAuthStatus = paymentsFoundationResponse.getData().getMultiAuthorisation().getStatus();

		// check status si authorized or not
		// retrieve from stage by checking consentId is available if not throw
		// error
		// Validating submission request details against the staged payment
		// setup details. If details are not matched then return an error to
		// PISP.
		// Checking whether staged payment setup resource is expired or not.

		// Initiated pre-submission validation.
		// If submissionId is already used for any other resource then platform
		// will throw the exception
		// Checking again whether paymentId is already used for other payment
		// initiation or not.

		return (PaymentFileSubmitPOST201Response) paymentsProcessingAdapter.postPaymentProcessFlows(
				paymentsPlatformResourceMap, paymentsFoundationResponse,
				paymentsFoundationResponse.getProcessExecutionStatus(),
				paymentsFoundationResponse.getData().getFilePaymentId(), multiAuthStatus, HttpMethod.POST.toString());
	}

	private CustomFilePaymentsPOSTRequest populateFileStagedProcessingFields(
			CustomFilePaymentsPOSTRequest submissionRequest,
			CustomFilePaymentConsentsPOSTResponse paymentSetupFoundationResponse) {
		/*
		 * If TPP has not sent DebtorAccount.Name then product will set the name details
		 * from staging record which is sent by FS
		 */
		if (submissionRequest.getData().getInitiation().getDebtorAccount() != null && NullCheckUtils
				.isNullOrEmpty(submissionRequest.getData().getInitiation().getDebtorAccount().getName())) {

			submissionRequest.getData().getInitiation().getDebtorAccount()
					.setName(paymentSetupFoundationResponse.getData().getInitiation().getDebtorAccount().getName());
		}
		/*
		 * If debtor details are not provided by TPP then Platform will set debtor
		 * details received from FS. Same will be sent to FS for
		 * Pre-SubmissionValidation and Account Permission
		 */

		if (submissionRequest.getData().getInitiation().getDebtorAccount() == null) {
			submissionRequest.getData().getInitiation()
					.setDebtorAccount(paymentSetupFoundationResponse.getData().getInitiation().getDebtorAccount());
		}

		submissionRequest.setCreatedOn(paymentSetupFoundationResponse.getData().getCreationDateTime());

		return submissionRequest;
	}

	@Override
	public PaymentFileSubmitPOST201Response findStatusFilePaymentsResource(String submissionId) {

		Map<String, Object> paymentsPlatformResourceMap = paymentsProcessingAdapter
				.prePaymentProcessGETFlows(submissionId, PaymentTypeEnum.FILE_PAY);

		PaymentsPlatformResource paymentsPlatformResource = (PaymentsPlatformResource) paymentsPlatformResourceMap
				.get(PaymentConstants.SUBMISSION);
		String paymentConsentId;
		String paymentSetUpVersion;

		paymentConsentId = paymentsPlatformResource.getPaymentConsentId();
		paymentSetUpVersion = cmaVersion;
		
		/*
		 * Fix for Splunk defect #P001051-37 
		 * Setting psuId to requestHeaderAttr
		 * in order to log it in errors and message.Exit
		 */
		PispConsent consent = pispConsentAdapter.retrieveConsentByPaymentId(paymentsPlatformResource.getPaymentConsentId());
		if(NullCheckUtils.isNullOrEmpty(consent)){
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.NO_CONSENT_FOUND);
		} else {
			reqHeaderAtrributes.setPsuId(consent.getPsuId());
		}

		CustomPaymentStageIdentifiers identifiers = new CustomPaymentStageIdentifiers();
		identifiers.setPaymentSetupVersion(paymentSetUpVersion);
		identifiers.setPaymentTypeEnum(PaymentTypeEnum.FILE_PAY);
		identifiers.setPaymentConsentId(paymentConsentId);

		PaymentFileSubmitPOST201Response paymentsFoundationResponse = filePaymentCoreAdapter
				.retrieveStagedFilePayments(identifiers, null);

		if (paymentsFoundationResponse == null) {
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_RESOURCE_NOTFOUND,
					ErrorMapKeys.NO_PAYMENT_SETUP_RESOURCES_FOUND));
		}

		OBExternalStatus2Code multiAuthStatus = OBExternalStatus2Code.AUTHORISED;
		if (paymentsFoundationResponse.getData().getMultiAuthorisation() != null)
			multiAuthStatus = paymentsFoundationResponse.getData().getMultiAuthorisation().getStatus();

		return (PaymentFileSubmitPOST201Response) paymentsProcessingAdapter.postPaymentProcessFlows(
				paymentsPlatformResourceMap, paymentsFoundationResponse,
				paymentsFoundationResponse.getProcessExecutionStatus(),
				paymentsFoundationResponse.getData().getFilePaymentId(), multiAuthStatus, HttpMethod.GET.toString());

	}

	private CustomPaymentSetupPlatformDetails populatePlatformDetails() {
		CustomPaymentSetupPlatformDetails customPaymentSetupPlatformDetails = new CustomPaymentSetupPlatformDetails();
		customPaymentSetupPlatformDetails.setSetupCmaVersion(cmaVersion);
		customPaymentSetupPlatformDetails.setPaymentType(PaymentTypeEnum.FILE_PAY);
		return customPaymentSetupPlatformDetails;
	}

	private CustomFilePaymentConsentsPOSTResponse filePayLoadCompare(CustomFilePaymentsPOSTRequest submissionRequest,
			PaymentConsentsPlatformResource paymentConsentPlatformResource, String consentId) {

		CustomPaymentStageIdentifiers identifiers = new CustomPaymentStageIdentifiers();
		identifiers.setPaymentConsentId(consentId);
		identifiers.setPaymentSetupVersion(cmaVersion);
		identifiers.setPaymentTypeEnum(PaymentTypeEnum.FILE_PAY);

		CustomFilePaymentConsentsPOSTResponse paymentSetupFoundationResponse = filePaymentConsentAdapter
				.fetchFilePaymentMetadata(identifiers, null);

		if (filePayloadComparator.comparePaymentDetails(paymentSetupFoundationResponse, submissionRequest,
				paymentConsentPlatformResource) > 0)
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_RESOURCE_CONSENTMISMATCH,
					ErrorMapKeys.RESOURCE_CONSENT_MISMATCH));

		return paymentSetupFoundationResponse;
	}

}
