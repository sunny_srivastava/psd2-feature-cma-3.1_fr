package com.capgemini.psd2.pisp.file.payments.transformer.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMethod;

import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.domain.CustomFilePaymentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.OBCashAccountDebtor3;
import com.capgemini.psd2.pisp.domain.OBExternalStatus1Code;
import com.capgemini.psd2.pisp.domain.OBExternalStatus2Code;
import com.capgemini.psd2.pisp.domain.OBFile1;
import com.capgemini.psd2.pisp.domain.PaymentConsentsPlatformResource;
import com.capgemini.psd2.pisp.domain.PaymentFileSubmitPOST201Response;
import com.capgemini.psd2.pisp.domain.PaymentSetupPOSTResponseLinks;
import com.capgemini.psd2.pisp.domain.PaymentSetupPOSTResponseMeta;
import com.capgemini.psd2.pisp.domain.PaymentsPlatformResource;
import com.capgemini.psd2.pisp.payment.submission.platform.adapter.PaymentsPlatformAdapter;
import com.capgemini.psd2.pisp.processing.adapter.PaymentSubmissionTransformer;
import com.capgemini.psd2.pisp.status.PaymentConstants;
import com.capgemini.psd2.pisp.utilities.PispDateUtility;
import com.capgemini.psd2.pisp.utilities.PispUtilities;
import com.capgemini.psd2.pisp.validation.adapter.constants.PaymentSetupConstants;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Component("filePaymentsTransformer")
public class FPaymentsResponseTransformerImpl
		implements PaymentSubmissionTransformer<PaymentFileSubmitPOST201Response, CustomFilePaymentsPOSTRequest> {

	@Autowired
	private RequestHeaderAttributes reqHeaderAtrributes;

	@Autowired
	private PaymentsPlatformAdapter paymentsPlatformAdapter;

	@Autowired
	private PispDateUtility pispDateUtility;

	public PaymentFileSubmitPOST201Response updateMetaAndLinks(PaymentFileSubmitPOST201Response submissionResponse,
			String methodType) {
		if (submissionResponse.getLinks() == null)
			submissionResponse.setLinks(new PaymentSetupPOSTResponseLinks());
		submissionResponse.getLinks().setSelf(PispUtilities.populateLinks(
				submissionResponse.getData().getFilePaymentId(), methodType, reqHeaderAtrributes.getSelfUrl()));

		if (submissionResponse.getMeta() == null)
			submissionResponse.setMeta(new PaymentSetupPOSTResponseMeta());
		return submissionResponse;
	}

	private void updatePaymentsResource(PaymentsPlatformResource paymentsPlatformResource, OBExternalStatus1Code status,
			String statusUpdateDateTime) {
		paymentsPlatformResource.setStatus(status.toString());
		paymentsPlatformResource.setStatusUpdateDateTime(statusUpdateDateTime);
		paymentsPlatformAdapter.updatePaymentsPlatformResource(paymentsPlatformResource);
	}

	@Override
	public PaymentFileSubmitPOST201Response paymentSubmissionResponseTransformer(
			PaymentFileSubmitPOST201Response paymentSubmissionResponse, Map<String, Object> paymentsPlatformResourceMap,
			String methodType) {

		PaymentsPlatformResource paymentsPlatformResource = (PaymentsPlatformResource) paymentsPlatformResourceMap
				.get(PaymentConstants.SUBMISSION);
		PaymentConsentsPlatformResource paymentConsentsPlatformResource = (PaymentConsentsPlatformResource) paymentsPlatformResourceMap
				.get(PaymentConstants.CONSENT);

		/* Adding Prefix uk.obie. in debtor scheme in response */
		if (!NullCheckUtils.isNullOrEmpty(paymentSubmissionResponse.getData().getInitiation().getDebtorAccount())) {
			String debtorAccountScheme = paymentSubmissionResponse.getData().getInitiation().getDebtorAccount()
					.getSchemeName();
			if (PaymentSetupConstants.prefixToAddSchemeNameList().contains(debtorAccountScheme)) {
				paymentSubmissionResponse.getData().getInitiation().getDebtorAccount()
						.setSchemeName(PSD2Constants.OB_SCHEME_PREFIX.concat(debtorAccountScheme));
			}
		}

		if (methodType.equals(RequestMethod.POST.toString())) {
			paymentSubmissionResponse.getData().setCreationDateTime(paymentsPlatformResource.getCreatedAt());

			/*
			 * Defect Fix for P000428-1353: If FS changes status from InitiationPending to
			 * InitiationFailed/InitiationCompleted, the status in response should also
			 * reflect the updated FS status during POST call.
			 */
			updatePaymentsSubmissionResource(paymentsPlatformResource, paymentSubmissionResponse);

			if (paymentConsentsPlatformResource.getTppDebtorDetails().equalsIgnoreCase(Boolean.FALSE.toString())) {
				paymentSubmissionResponse.getData().getInitiation().setDebtorAccount(null);
			}
			if (Boolean.valueOf(paymentConsentsPlatformResource.getTppDebtorDetails())
					&& !Boolean.valueOf(paymentConsentsPlatformResource.getTppDebtorNameDetails()))
				paymentSubmissionResponse.getData().getInitiation().getDebtorAccount().setName(null);
		} else {
			/*
			 * Fix for P000428-1350: If FS sends InitiationFailed, then InitiationPending
			 * status should change to InitiationFailed on GET end-point
			 */
			updatePaymentsSubmissionResource(paymentsPlatformResource, paymentSubmissionResponse);
			if (!Boolean.valueOf(paymentConsentsPlatformResource.getTppDebtorDetails()))
				paymentSubmissionResponse.getData().getInitiation().setDebtorAccount(null);

			/*
			 * If tppDebtorNameFlag is 'False' then DebtorAccount.name should not be sent to
			 * TPP.
			 */
			if (Boolean.valueOf(paymentConsentsPlatformResource.getTppDebtorDetails())
					&& !Boolean.valueOf(paymentConsentsPlatformResource.getTppDebtorNameDetails()))
				paymentSubmissionResponse.getData().getInitiation().getDebtorAccount().setName(null);
		}
		updateMetaAndLinks(paymentSubmissionResponse, methodType);
		return paymentSubmissionResponse;
	}

	private void updatePaymentsSubmissionResource(PaymentsPlatformResource paymentsPlatformResource,
			PaymentFileSubmitPOST201Response paymentSubmissionResponse) {
		OBExternalStatus1Code status = OBExternalStatus1Code
				.valueOf(paymentsPlatformResource.getStatus().toUpperCase());
		String statusUpdateDateTime = paymentsPlatformResource.getStatusUpdateDateTime();

		if (paymentsPlatformResource.getStatus().equals(OBExternalStatus1Code.INITIATIONPENDING.toString())) {
			if (paymentSubmissionResponse.getData().getStatus() == OBExternalStatus1Code.INITIATIONCOMPLETED) {
				status = paymentSubmissionResponse.getData().getStatus();
				statusUpdateDateTime = paymentSubmissionResponse.getData().getStatusUpdateDateTime();
				updatePaymentsResource(paymentsPlatformResource, status, statusUpdateDateTime);
			} else if (paymentSubmissionResponse.getData().getStatus() == OBExternalStatus1Code.INITIATIONFAILED) {
				status = paymentSubmissionResponse.getData().getStatus();
				statusUpdateDateTime = paymentSubmissionResponse.getData().getStatusUpdateDateTime();
				updatePaymentsResource(paymentsPlatformResource, status, statusUpdateDateTime);
			} else if (!NullCheckUtils.isNullOrEmpty(paymentSubmissionResponse.getData().getMultiAuthorisation())) {
				OBExternalStatus2Code responseMultiAuthStatus = paymentSubmissionResponse.getData()
						.getMultiAuthorisation().getStatus();
				if (responseMultiAuthStatus == OBExternalStatus2Code.AUTHORISED) {
					status = OBExternalStatus1Code.INITIATIONCOMPLETED;
					statusUpdateDateTime = PispUtilities.getCurrentDateInISOFormat();
					updatePaymentsResource(paymentsPlatformResource, status, statusUpdateDateTime);
				} else if (responseMultiAuthStatus == OBExternalStatus2Code.REJECTED) {
					status = OBExternalStatus1Code.INITIATIONFAILED;
					statusUpdateDateTime = PispUtilities.getCurrentDateInISOFormat();
					updatePaymentsResource(paymentsPlatformResource, status, statusUpdateDateTime);
				}
			}
		}
		paymentSubmissionResponse.getData().setStatus(status);
		paymentSubmissionResponse.getData().setStatusUpdateDateTime(statusUpdateDateTime);
	}

	@Override
	public CustomFilePaymentsPOSTRequest paymentSubmissionRequestTransformer(
			CustomFilePaymentsPOSTRequest paymentSubmissionRequest) {
		OBFile1 requestInitiation = paymentSubmissionRequest.getData().getInitiation();
		if (!NullCheckUtils.isNullOrEmpty(requestInitiation.getRequestedExecutionDateTime())) {
			requestInitiation.setRequestedExecutionDateTime(
					pispDateUtility.transformDateTimeInRequest(requestInitiation.getRequestedExecutionDateTime()));
		}
		return paymentSubmissionRequest;
	}

}
