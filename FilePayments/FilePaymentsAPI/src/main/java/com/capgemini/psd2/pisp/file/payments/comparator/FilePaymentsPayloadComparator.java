package com.capgemini.psd2.pisp.file.payments.comparator;

import java.math.BigDecimal;
import java.util.Comparator;
import java.util.Objects;

import org.springframework.stereotype.Component;

import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.pisp.domain.CustomFilePaymentConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.CustomFilePaymentSetupPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomFilePaymentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.OBFile1;
import com.capgemini.psd2.pisp.domain.OBRemittanceInformation1;
import com.capgemini.psd2.pisp.domain.PaymentConsentsPlatformResource;
import com.capgemini.psd2.pisp.utilities.PispUtilities;
import com.capgemini.psd2.utilities.JSONUtilities;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Component
public class FilePaymentsPayloadComparator implements Comparator<CustomFilePaymentConsentsPOSTResponse> {

	@Override
	public int compare(CustomFilePaymentConsentsPOSTResponse paymentResponse,
			CustomFilePaymentConsentsPOSTResponse adaptedPaymentResponse) {

		int value = 1;

		if (Double.compare(Double.parseDouble(paymentResponse.getData().getInitiation().getControlSum().toString()),
				Double.parseDouble(adaptedPaymentResponse.getData().getInitiation().getControlSum().toString())) == 0) {
			adaptedPaymentResponse.getData().getInitiation().setControlSum(
					new BigDecimal(paymentResponse.getData().getInitiation().getControlSum().toString()));
		}

		if (paymentResponse.getData().getInitiation().equals(adaptedPaymentResponse.getData().getInitiation()))
			value = 0;

		return value;
	}

	public int comparePaymentDetails(Object consentFoundationResponse, Object submissionRequest,
			PaymentConsentsPlatformResource paymentConsentPlatformResource) {
		CustomFilePaymentConsentsPOSTResponse paymentSetupResponse = null; // consentFoundationResponse
		CustomFilePaymentConsentsPOSTResponse tempPaymentSetupResponse = null; // consentFoundationResponse
		CustomFilePaymentConsentsPOSTResponse adaptedPaymentResponse = null; // submissionRequest

		CustomFilePaymentSetupPOSTRequest paymentSetupRequest = null;
		CustomFilePaymentsPOSTRequest paymentSubmissionRequest = null;
		int returnValue = 1;

		if (consentFoundationResponse instanceof CustomFilePaymentConsentsPOSTResponse) {
			paymentSetupResponse = (CustomFilePaymentConsentsPOSTResponse) consentFoundationResponse;
			String creationDateTime = paymentSetupResponse.getData().getCreationDateTime();
			paymentSetupResponse.getData().setCreationDateTime(null);
			String strPaymentResponse = JSONUtilities.getJSONOutPutFromObject(paymentSetupResponse);
			tempPaymentSetupResponse = JSONUtilities.getObjectFromJSONString(PispUtilities.getObjectMapper(),
					strPaymentResponse, CustomFilePaymentConsentsPOSTResponse.class);
			paymentSetupResponse.getData().setCreationDateTime(creationDateTime);
		}

		if (submissionRequest instanceof CustomFilePaymentSetupPOSTRequest) {
			paymentSetupRequest = (CustomFilePaymentSetupPOSTRequest) submissionRequest;
			String strPaymentRequest = JSONUtilities.getJSONOutPutFromObject(paymentSetupRequest);
			adaptedPaymentResponse = JSONUtilities.getObjectFromJSONString(PispUtilities.getObjectMapper(),
					strPaymentRequest, CustomFilePaymentConsentsPOSTResponse.class);
		} else if (submissionRequest instanceof CustomFilePaymentsPOSTRequest) {
			paymentSubmissionRequest = (CustomFilePaymentsPOSTRequest) submissionRequest;
			String strSubmissionRequest = JSONUtilities.getJSONOutPutFromObject(paymentSubmissionRequest);
			adaptedPaymentResponse = JSONUtilities.getObjectFromJSONString(PispUtilities.getObjectMapper(),
					strSubmissionRequest, CustomFilePaymentConsentsPOSTResponse.class);
		}

		if (tempPaymentSetupResponse != null && adaptedPaymentResponse != null) {

			if (!validateDebtorDetails(tempPaymentSetupResponse.getData().getInitiation(),
					adaptedPaymentResponse.getData().getInitiation(), paymentConsentPlatformResource))
				return 1;

			checkAndModifyEmptyFields(adaptedPaymentResponse,
					(CustomFilePaymentConsentsPOSTResponse) consentFoundationResponse);

			returnValue = compare(tempPaymentSetupResponse, adaptedPaymentResponse);
		}
		return returnValue;

	}

	public boolean validateDebtorDetails(OBFile1 obFile1, OBFile1 obFile12,
			PaymentConsentsPlatformResource paymentSetupPlatformResource) {

		if (Boolean.valueOf(paymentSetupPlatformResource.getTppDebtorDetails())) {

			/*
			 * If DebtorAccount.Name is sent by TPP,tppDebtoNameDetails would be true, then
			 * name will participate in comparison else product will set foundation response
			 * DebtorAccount.Name as null so that it can be passed in comparison. Both
			 * fields would have the value as 'Null'.
			 */
			if (!Boolean.valueOf(paymentSetupPlatformResource.getTppDebtorNameDetails()))
				obFile1.getDebtorAccount().setName(null);
			String schemeNameOfResponseInitiation = obFile1.getDebtorAccount().getSchemeName();
			String schemeNameOfRequestInitiation = obFile12.getDebtorAccount().getSchemeName();
			if(schemeNameOfResponseInitiation!= null && schemeNameOfResponseInitiation.startsWith(PSD2Constants.OB_SCHEME_PREFIX)){
				obFile1.getDebtorAccount().setSchemeName(schemeNameOfResponseInitiation.split(PSD2Constants.OB_SCHEME_PREFIX)[1]);
		
			}
			if (schemeNameOfRequestInitiation!= null && schemeNameOfRequestInitiation.startsWith(PSD2Constants.OB_SCHEME_PREFIX)) {
				obFile12.getDebtorAccount().setSchemeName(schemeNameOfRequestInitiation.split(PSD2Constants.OB_SCHEME_PREFIX)[1]);
			}
			return Objects.equals(obFile1.getDebtorAccount(), obFile12.getDebtorAccount());
		}
		if (!Boolean.valueOf(paymentSetupPlatformResource.getTppDebtorDetails())
				&& obFile12.getDebtorAccount() != null) {
			return Boolean.FALSE;
		}
		if (!Boolean.valueOf(paymentSetupPlatformResource.getTppDebtorDetails())
				&& obFile1.getDebtorAccount() != null) {
			obFile1.setDebtorAccount(null);
			return Boolean.TRUE;
		}
		return Boolean.TRUE;

	}

	private void checkAndModifyEmptyFields(CustomFilePaymentConsentsPOSTResponse adaptedPaymentResponse,
			CustomFilePaymentConsentsPOSTResponse consentFoundationResponse) {

		checkAndModifyRemittanceInformation(adaptedPaymentResponse.getData().getInitiation(),
				consentFoundationResponse);

	}

	private void checkAndModifyRemittanceInformation(OBFile1 obFile1,
			CustomFilePaymentConsentsPOSTResponse consentFoundationResponse) {

		OBRemittanceInformation1 remittanceInformation = obFile1.getRemittanceInformation();
		if (remittanceInformation != null
				&& consentFoundationResponse.getData().getInitiation().getRemittanceInformation() == null
				&& NullCheckUtils.isNullOrEmpty(remittanceInformation.getReference())
				&& NullCheckUtils.isNullOrEmpty(remittanceInformation.getUnstructured())) {

			obFile1.setRemittanceInformation(null);
		}
	}
}
