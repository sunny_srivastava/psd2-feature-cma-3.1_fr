/*package com.capgemini.psd2.security.consent.pisp.test.controllers;

import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.naming.NamingException;
import javax.naming.directory.BasicAttributes;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.integration.adapter.TPPInformationAdaptor;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.mask.DataMask;
import com.capgemini.psd2.pisp.domain.PaymentConsentsPlatformResource;
import com.capgemini.psd2.pisp.enums.PaymentTypeEnum;
import com.capgemini.psd2.pisp.payment.setup.platform.adapter.PaymentSetupPlatformAdapter;
import com.capgemini.psd2.pisp.stage.domain.AmountDetails;
import com.capgemini.psd2.pisp.stage.domain.CreditorDetails;
import com.capgemini.psd2.pisp.stage.domain.CustomConsentAppViewData;
import com.capgemini.psd2.pisp.stage.domain.CustomDebtorDetails;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.pisp.stage.domain.RemittanceDetails;
import com.capgemini.psd2.scaconsenthelper.config.helpers.PispConsentCreationDataHelper;
import com.capgemini.psd2.scaconsenthelper.models.IntentTypeEnum;
import com.capgemini.psd2.scaconsenthelper.models.PickupDataModel;
import com.capgemini.psd2.security.consent.pisp.config.PispHostNameConfig;
import com.capgemini.psd2.security.consent.pisp.test.mock.data.PispConsentApplicationMockdata;
import com.capgemini.psd2.security.consent.pisp.view.controllers.PispConsentViewController;
import com.capgemini.psd2.ui.content.utility.controller.UIStaticContentUtilityController;
import com.capgemini.psd2.utilities.JSONUtilities;
import com.capgemini.psd2.utilities.SandboxConfig;


@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
public class PispConsentViewControllerTest {

	@Mock
	private PispHostNameConfig pispHostNameConfig;

	@Mock
	private SandboxConfig sandboxConfig;

	@Mock
	private TPPInformationAdaptor tppInformationAdaptor;

	@Mock
	private UIStaticContentUtilityController uiController;

	@Mock
	private RequestHeaderAttributes requestHeaders;

	@Mock
	private PispConsentCreationDataHelper consentCreationDataHelper;

	@Mock
	private HttpServletRequest httpServletRequest;

	@Mock
	private DataMask dataMask;

	@Mock
	private HttpServletResponse response;

	@Mock
	private Map<String, String> consentSupportedSchemeMap = new HashMap<>();

	@InjectMocks
	private PispConsentViewController viewController;

	@Mock 
	private PaymentSetupPlatformAdapter paymentSetupPlatformAdapter;

	@Before
	public void setUp() {
		 
		MockitoAnnotations.initMocks(this);
		InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
		viewResolver.setPrefix("/");
		viewResolver.setSuffix(".jsp");
		PaymentConsentsPlatformResource paymentConsentsPlatformResource = new PaymentConsentsPlatformResource();
		paymentConsentsPlatformResource.setTppCID("asctyafyt");
		paymentConsentsPlatformResource.setTppLegalEntityName("ascascassc");
		when(paymentSetupPlatformAdapter.retrievePaymentSetupPlatformResource(anyObject())).thenReturn(paymentConsentsPlatformResource);
		consentSupportedSchemeMap.put("IBAN", "IBAN");
	}

	@Test
	public void testHomePage() {
		Map<String, Object> map = new HashMap<>();
		when(httpServletRequest.getRequestURI()).thenReturn("/home");
		ReflectionTestUtils.setField(viewController, "applicationName", "pispconsentapplicationv1");
		when(requestHeaders.getTenantId()).thenReturn("tenant1");
		when(pispHostNameConfig.getTenantSpecificEdgeserverhost(anyString())).thenReturn("tenant2");
		when(sandboxConfig.isSandboxEnabled()).thenReturn(true);
		when(httpServletRequest.getQueryString()).thenReturn("key=value");
		
		viewController.homePage(map);
	}

	@Test(expected = PSD2Exception.class)
	public void testCustomerConsentViewDebtorDetailsException() throws NamingException {
		String idToken = "vntwKKvsq6nBd1YkYTVVqVOMEZW5xtZ4MF0vnyS2ocnbckNRi5gK/yBXtpA914/x905zxV86dZkCtnU69DWKgwBcyTlruh0hPe8rlGMdeKdJW+lx2h8WUHDI1g54xcI+OfokOqk59KzOcS+ESIxfzUVr7B0dnaeLVUblbtztpoEimPTnYtK3mXbXoqdtQHnSXtW1jlL0RRPRwTiYpGP44RvZkpGPU+7Re53gFf1FTTxGp0X8wTfiVed+CEvD6ySXRMwX8r0qEo3bNLvdzh4Zu/CznR5igxuSd2KDnr54ZaeB6RiXr3870e47PxaT2qlZmyqZLeSq4XbWlZxuU7x1/lM5qXGVNXslvl/lOLhvQv4xjdPJHKrrtzyL5SnJXifDdBC+O9wWx3ete6EcxrlMpA==";
		Map<String, String> paramMap = new HashMap<>();
		paramMap.put("brandId", "ROI");
		paramMap.put("channelId", "123");
		paramMap.put("idToken", idToken);
		List<String> params = new ArrayList<>();
		params.add("cahnnelId");
		Map<String, Object> map = new HashMap<>();
		map.put("NO_JS_MSG", "NO_JS_MSG");
		String jsonMap = JSONUtilities.getJSONOutPutFromObject(map);
		PickupDataModel pickupobj = new PickupDataModel();
		pickupobj.setClientId("aspsp");
		pickupobj.setIntentTypeEnum(IntentTypeEnum.AISP_INTENT_TYPE);
		pickupobj.setIntentId("1234");
		CustomConsentAppViewData data = new CustomConsentAppViewData();
		data.setDebtorDetails(new CustomDebtorDetails());
		data.getDebtorDetails().setSchemeName("aspsp");
		when(httpServletRequest.getAttribute(anyString())).thenReturn(pickupobj);
		when(consentCreationDataHelper.retrieveCustomerAccountListInfo(anyString(), anyString(), anyString(),
				anyString(), anyString(), anyString(), anyString(), anyString(), anyString()))
		.thenReturn(PispConsentApplicationMockdata.getCustomerAccountList());
		when(dataMask.maskResponseGenerateString(anyObject(), anyString())).thenReturn("account");
		when(tppInformationAdaptor.fetchTPPInformation(anyString())).thenReturn(new BasicAttributes());
		when(consentCreationDataHelper.retrieveConsentAppStagedViewData(anyString(), anyObject())).thenReturn(data);
		when(uiController.getStaticContentForUI()).thenReturn(jsonMap);
		when(requestHeaders.getCorrelationId()).thenReturn("12345");
		ReflectionTestUtils.setField(viewController, "applicationName", "pispconsentapplicationv1");
		Map<String, Object> model = new HashMap<>();
		Enumeration<String> fsHeader = new Enumeration<String>() {

			@Override
			public String nextElement() {
				return null;
			}

			@Override
			public boolean hasMoreElements() {
				return false;
			}
		};
		when(httpServletRequest.getHeaderNames()).thenReturn(fsHeader);
		when(paymentSetupPlatformAdapter
				.populateStageIdentifiers(pickupobj.getIntentId())).thenReturn(new CustomPaymentStageIdentifiers());
		viewController.consentView(model);
	}
	
	@Test(expected=PSD2Exception.class)
	public void testCustomerConsentViewAccountListException() throws NamingException {
		String idToken = "vntwKKvsq6nBd1YkYTVVqVOMEZW5xtZ4MF0vnyS2ocnbckNRi5gK/yBXtpA914/x905zxV86dZkCtnU69DWKgwBcyTlruh0hPe8rlGMdeKdJW+lx2h8WUHDI1g54xcI+OfokOqk59KzOcS+ESIxfzUVr7B0dnaeLVUblbtztpoEimPTnYtK3mXbXoqdtQHnSXtW1jlL0RRPRwTiYpGP44RvZkpGPU+7Re53gFf1FTTxGp0X8wTfiVed+CEvD6ySXRMwX8r0qEo3bNLvdzh4Zu/CznR5igxuSd2KDnr54ZaeB6RiXr3870e47PxaT2qlZmyqZLeSq4XbWlZxuU7x1/lM5qXGVNXslvl/lOLhvQv4xjdPJHKrrtzyL5SnJXifDdBC+O9wWx3ete6EcxrlMpA==";
		Map<String, String> paramMap = new HashMap<>();
		paramMap.put("brandId", "ROI");
		paramMap.put("channelId", "123");
		paramMap.put("idToken", idToken);
		List<String> params = new ArrayList<>();
		params.add("cahnnelId");
		Map<String, Object> map = new HashMap<>();
		map.put("NO_JS_MSG", "NO_JS_MSG");
		String jsonMap = JSONUtilities.getJSONOutPutFromObject(map);
		PickupDataModel pickupobj = new PickupDataModel();
		pickupobj.setClientId("aspsp");
		pickupobj.setIntentTypeEnum(IntentTypeEnum.AISP_INTENT_TYPE);
		pickupobj.setIntentId("1234");
		CustomConsentAppViewData data = new CustomConsentAppViewData(); 
		data.setAmountDetails(new AmountDetails());
		data.getAmountDetails().setAmount("76.89");
		data.getAmountDetails().setCurrency("EUR");
		data.setCreditorDetails(new CreditorDetails());
		data.getCreditorDetails().setName("creditor");
		data.getCreditorDetails().setSecondaryIdentification("secondaryIdentification");
		data.setRemittanceDetails(new RemittanceDetails());
		data.getRemittanceDetails().setReference("ref");
		data.getRemittanceDetails().setUnstructured("true");
		data.setDebtorDetails(new CustomDebtorDetails());
		data.getDebtorDetails().setSchemeName("IBAN");
		data.getDebtorDetails().setIdentification("1234566");
		when(httpServletRequest.getAttribute(anyString())).thenReturn(pickupobj);
		when(consentCreationDataHelper.retrieveCustomerAccountListInfo(anyString(), anyString(), anyString(),
				anyString(), anyString(), anyString(), anyString(), anyString(), anyString()))
		.thenReturn(PispConsentApplicationMockdata.getCustomerAccountList());
		when(dataMask.maskResponseGenerateString(anyObject(), anyString())).thenReturn("account");
		when(tppInformationAdaptor.fetchTPPInformation(anyString())).thenReturn(new BasicAttributes());
		when(consentCreationDataHelper.retrieveConsentAppStagedViewData(anyString(), anyObject())).thenReturn(data);
		when(uiController.getStaticContentForUI()).thenReturn(jsonMap);
		when(requestHeaders.getCorrelationId()).thenReturn("12345");
		when(consentSupportedSchemeMap.containsKey(anyString())).thenReturn(true);
		when(consentSupportedSchemeMap.get(anyString())).thenReturn("IBAN");
		ReflectionTestUtils.setField(viewController, "applicationName", "pispconsentapplicationv1");
		Map<String, Object> model = new HashMap<>();
		Enumeration<String> fsHeader = new Enumeration<String>() {

			@Override
			public String nextElement() {
				return null;
			}

			@Override
			public boolean hasMoreElements() {
				return false;
			}
		};
		when(httpServletRequest.getHeaderNames()).thenReturn(fsHeader);
		when(paymentSetupPlatformAdapter
				.populateStageIdentifiers(pickupobj.getIntentId())).thenReturn(new CustomPaymentStageIdentifiers());
		viewController.consentView(model);
	}
	
	@Test
	public void testCustomerConsentViewAccountList() throws NamingException {
		String idToken = "vntwKKvsq6nBd1YkYTVVqVOMEZW5xtZ4MF0vnyS2ocnbckNRi5gK/yBXtpA914/x905zxV86dZkCtnU69DWKgwBcyTlruh0hPe8rlGMdeKdJW+lx2h8WUHDI1g54xcI+OfokOqk59KzOcS+ESIxfzUVr7B0dnaeLVUblbtztpoEimPTnYtK3mXbXoqdtQHnSXtW1jlL0RRPRwTiYpGP44RvZkpGPU+7Re53gFf1FTTxGp0X8wTfiVed+CEvD6ySXRMwX8r0qEo3bNLvdzh4Zu/CznR5igxuSd2KDnr54ZaeB6RiXr3870e47PxaT2qlZmyqZLeSq4XbWlZxuU7x1/lM5qXGVNXslvl/lOLhvQv4xjdPJHKrrtzyL5SnJXifDdBC+O9wWx3ete6EcxrlMpA==";
		Map<String, String> paramMap = new HashMap<>();
		paramMap.put("brandId", "ROI");
		paramMap.put("channelId", "123");
		paramMap.put("idToken", idToken);
		List<String> params = new ArrayList<>();
		params.add("channelId");
		Map<String, Object> map = new HashMap<>();
		map.put("NO_JS_MSG", "NO_JS_MSG");
		String jsonMap = JSONUtilities.getJSONOutPutFromObject(map);
		PickupDataModel pickupobj = new PickupDataModel();
		pickupobj.setClientId("aspsp");
		pickupobj.setIntentTypeEnum(IntentTypeEnum.AISP_INTENT_TYPE);
		pickupobj.setIntentId("1234");
		CustomConsentAppViewData data = new CustomConsentAppViewData(); 
		data.setAmountDetails(new AmountDetails());
		data.getAmountDetails().setAmount("76.89");
		data.getAmountDetails().setCurrency("EUR");
		data.setCreditorDetails(new CreditorDetails());
		data.getCreditorDetails().setName("creditor");
		data.getCreditorDetails().setSecondaryIdentification("secondaryIdentification");
		data.setRemittanceDetails(new RemittanceDetails());
		data.getRemittanceDetails().setReference("ref");
		data.getRemittanceDetails().setUnstructured("true");
		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();
		stageIdentifiers.setPaymentTypeEnum(PaymentTypeEnum.DOMESTIC_PAY);
		when(paymentSetupPlatformAdapter.populateStageIdentifiers(anyString())).thenReturn(stageIdentifiers);
		when(httpServletRequest.getAttribute(anyString())).thenReturn(pickupobj);
		when(consentCreationDataHelper.retrieveCustomerAccountListInfo(anyString(), anyString(), anyString(),
				anyString(), anyString(), anyString(), anyString(), anyString(), anyString()))
		.thenReturn(PispConsentApplicationMockdata.getCustomerAccountList());
		when(dataMask.maskResponseGenerateString(anyObject(), anyString())).thenReturn("account");
		when(tppInformationAdaptor.fetchTPPInformation(anyString())).thenReturn(new BasicAttributes());
		when(consentCreationDataHelper.retrieveConsentAppStagedViewData(anyString(), anyObject())).thenReturn(data);
		when(uiController.getStaticContentForUI()).thenReturn(jsonMap);
		when(requestHeaders.getCorrelationId()).thenReturn("12345");
		when(consentSupportedSchemeMap.containsKey(anyString())).thenReturn(true);
		when(consentSupportedSchemeMap.get(anyString())).thenReturn("IBAN");
		ReflectionTestUtils.setField(viewController, "applicationName", "pispconsentapplicationv1");
		Map<String, Object> model = new HashMap<>();
		Enumeration<String> fsHeader = new Enumeration<String>() {

			@Override
			public String nextElement() {
				return null;
			}

			@Override
			public boolean hasMoreElements() {
				return false;
			}
		};
		when(httpServletRequest.getHeaderNames()).thenReturn(fsHeader);
		viewController.consentView(model);
	}
	
	@Test
	public void testCustomerConsentViewAccountListWithDebtor() throws NamingException {
		String idToken = "vntwKKvsq6nBd1YkYTVVqVOMEZW5xtZ4MF0vnyS2ocnbckNRi5gK/yBXtpA914/x905zxV86dZkCtnU69DWKgwBcyTlruh0hPe8rlGMdeKdJW+lx2h8WUHDI1g54xcI+OfokOqk59KzOcS+ESIxfzUVr7B0dnaeLVUblbtztpoEimPTnYtK3mXbXoqdtQHnSXtW1jlL0RRPRwTiYpGP44RvZkpGPU+7Re53gFf1FTTxGp0X8wTfiVed+CEvD6ySXRMwX8r0qEo3bNLvdzh4Zu/CznR5igxuSd2KDnr54ZaeB6RiXr3870e47PxaT2qlZmyqZLeSq4XbWlZxuU7x1/lM5qXGVNXslvl/lOLhvQv4xjdPJHKrrtzyL5SnJXifDdBC+O9wWx3ete6EcxrlMpA==";
		Map<String, String> paramMap = new HashMap<>();
		paramMap.put("brandId", "ROI");
		paramMap.put("channelId", "123");
		paramMap.put("idToken", idToken);
		List<String> params = new ArrayList<>();
		params.add("cahnnelId");
		Map<String, Object> map = new HashMap<>();
		map.put("NO_JS_MSG", "NO_JS_MSG");
		String jsonMap = JSONUtilities.getJSONOutPutFromObject(map);
		PickupDataModel pickupobj = new PickupDataModel();
		pickupobj.setClientId("aspsp");
		pickupobj.setIntentTypeEnum(IntentTypeEnum.AISP_INTENT_TYPE);
		pickupobj.setIntentId("1234");
		CustomConsentAppViewData data = new CustomConsentAppViewData(); 
		data.setAmountDetails(new AmountDetails());
		data.getAmountDetails().setAmount("76.89");
		data.getAmountDetails().setCurrency("EUR");
		data.setCreditorDetails(new CreditorDetails());
		data.getCreditorDetails().setName("creditor");
		data.getCreditorDetails().setSecondaryIdentification("secondaryIdentification");
		data.setRemittanceDetails(new RemittanceDetails());
		data.getRemittanceDetails().setReference("ref");
		data.getRemittanceDetails().setUnstructured("true");
		data.setDebtorDetails(new CustomDebtorDetails());
		data.getDebtorDetails().setSchemeName("IBAN");
		data.getDebtorDetails().setIdentification("1234566");
		when(httpServletRequest.getAttribute(anyString())).thenReturn(pickupobj);
		when(consentCreationDataHelper.retrieveCustomerAccountListInfo(anyString(), anyString(), anyString(),
				anyString(), anyString(), anyString(), anyString(), anyString(), anyString()))
		.thenReturn(PispConsentApplicationMockdata.getCustomerAccountList());
		when(dataMask.maskResponseGenerateString(anyObject(), anyString())).thenReturn("account");
		when(tppInformationAdaptor.fetchTPPInformation(anyString())).thenReturn(new BasicAttributes());
		when(consentCreationDataHelper.retrieveConsentAppStagedViewData(anyString(), anyObject())).thenReturn(data);
		when(uiController.getStaticContentForUI()).thenReturn(jsonMap);
		when(requestHeaders.getCorrelationId()).thenReturn("12345");
		when(consentSupportedSchemeMap.containsKey(anyString())).thenReturn(true);
		when(consentSupportedSchemeMap.get(anyString())).thenReturn("IBAN");
		ReflectionTestUtils.setField(viewController, "applicationName", "pispconsentapplicationv1");
		Map<String, Object> model = new HashMap<>();
		Enumeration<String> fsHeader = new Enumeration<String>() {

			@Override
			public String nextElement() {
				return null;
			}

			@Override
			public boolean hasMoreElements() {
				return false;
			}
		};
		
		ReflectionTestUtils.setField(viewController, "consentSupportedSchemeMap", consentSupportedSchemeMap);
		
		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();
		stageIdentifiers.setPaymentTypeEnum(PaymentTypeEnum.DOMESTIC_PAY);
		when(paymentSetupPlatformAdapter.populateStageIdentifiers(anyString())).thenReturn(stageIdentifiers);
		
		
		when(httpServletRequest.getHeaderNames()).thenReturn(fsHeader);
		viewController.consentView(model);
	}
}*/