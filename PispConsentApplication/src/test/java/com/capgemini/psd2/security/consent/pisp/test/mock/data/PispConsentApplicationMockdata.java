package com.capgemini.psd2.security.consent.pisp.test.mock.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.capgemini.psd2.aisp.domain.OBAccount6;
import com.capgemini.psd2.aisp.domain.OBAccount6Account;
import com.capgemini.psd2.aisp.domain.OBBranchAndFinancialInstitutionIdentification50;
import com.capgemini.psd2.aisp.domain.OBReadAccount6;
import com.capgemini.psd2.aisp.domain.OBReadAccount6Data;
import com.capgemini.psd2.consent.domain.CustomerAccountInfo;
import com.capgemini.psd2.consent.domain.PSD2Account;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.pisp.enums.PaymentTypeEnum;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;

public class PispConsentApplicationMockdata {

	public static List<CustomerAccountInfo> mockCustomerAccountInfoList;

	public static CustomPaymentStageIdentifiers getStageIdentifiers() {
		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();
		stageIdentifiers.setPaymentConsentId("12345");
		stageIdentifiers.setPaymentTypeEnum(PaymentTypeEnum.DOMESTIC_PAY);
		
		return stageIdentifiers;
	}

	public static OBAccount6 getAccountData() {
		PSD2Account account = new PSD2Account();
		account.setAccountId("2343253464");
		account.setCurrency("EUR");
		account.setNickname("John");
		List<OBAccount6Account> accountList = new ArrayList<>();
		OBAccount6Account account1 = new OBAccount6Account();
		account1.setIdentification("12345");
		account1.setSchemeName("IBAN");
		OBAccount6Account account2 = new OBAccount6Account();
		account1.setIdentification("12345");
		account1.setSchemeName("IBAN");
		accountList.add(account1);
		accountList.add(account2);
		OBBranchAndFinancialInstitutionIdentification50 servicer = new OBBranchAndFinancialInstitutionIdentification50();
		servicer.setIdentification("35475687");
		account.setAccount(accountList);
		account.setServicer(servicer);
		Map<String, String> additionalInfo = new HashMap<>();
		additionalInfo.put(PSD2Constants.ACCOUNT_NUMBER, "12345");
		additionalInfo.put(PSD2Constants.ACCOUNT_NSC, "12345");
		((PSD2Account) account).setAdditionalInformation(additionalInfo);
		return account;
	}

	public static PSD2Account getPSD2AccountData() {
		PSD2Account account = new PSD2Account();
		account.setAccountId("2343253464");
		account.setCurrency("EUR");
		account.setNickname("John");
		List<OBAccount6Account> accountList = new ArrayList<>();
		OBAccount6Account account1 = new OBAccount6Account();
		account1.setIdentification("12345");
		account1.setSchemeName("IBAN");
		OBAccount6Account account2 = new OBAccount6Account();
		account1.setIdentification("12345");
		account1.setSchemeName("IBAN");
		accountList.add(account1);
		accountList.add(account2);
		OBBranchAndFinancialInstitutionIdentification50 servicer = new OBBranchAndFinancialInstitutionIdentification50();
		servicer.setIdentification("35475687");
		account.setAccount(accountList);
		account.setServicer(servicer);
		return account;
	}

	public static List<CustomerAccountInfo> getCustomerAccountInfoList() {
		mockCustomerAccountInfoList = new ArrayList<>();
		CustomerAccountInfo customerAccountInfo1 = new CustomerAccountInfo();
		customerAccountInfo1.setAccountName("John Doe");
		customerAccountInfo1.setUserId("1234");
		customerAccountInfo1.setAccountNumber("10203345");
		customerAccountInfo1.setAccountNSC("SC802001");
		customerAccountInfo1.setCurrency("EUR");
		customerAccountInfo1.setNickname("John");
		customerAccountInfo1.setAccountType("checking");

		CustomerAccountInfo customerAccountInfo2 = new CustomerAccountInfo();
		customerAccountInfo2.setAccountName("Tiffany Doe");
		customerAccountInfo2.setUserId("1234");
		customerAccountInfo2.setAccountNumber("10203346");
		customerAccountInfo2.setAccountNSC("SC802002");
		customerAccountInfo2.setCurrency("GRP");
		customerAccountInfo2.setNickname("Tiffany");
		customerAccountInfo2.setAccountType("savings");

		mockCustomerAccountInfoList.add(customerAccountInfo1);
		mockCustomerAccountInfoList.add(customerAccountInfo2);

		return mockCustomerAccountInfoList;
	}

	public static OBReadAccount6 getCustomerAccountInfo() {
		OBReadAccount6 mockOBReadAccount6 = new OBReadAccount6();
		List<OBAccount6> accountData = new ArrayList<>();
		PSD2Account acct = new PSD2Account();
		acct.setAccountId("14556236");
		acct.setCurrency("EUR");
		acct.setNickname("John");
		List<OBAccount6Account> accountList = new ArrayList<>();
		OBAccount6Account OBAccount6Account = new OBAccount6Account();
		OBAccount6Account.setIdentification("12345");
		OBAccount6Account.setSchemeName("IBAN");
		accountList.add(OBAccount6Account);
		OBBranchAndFinancialInstitutionIdentification50 servicer = new OBBranchAndFinancialInstitutionIdentification50();
		servicer.setIdentification("12345");

		acct.setAccount(accountList);
		acct.setServicer(servicer);

		PSD2Account accnt = new PSD2Account();
		accnt.setAccountId("14556236");
		accnt.setCurrency("EUR");
		accnt.setNickname("John");
		accnt.setServicer(servicer);
		accnt.setAccount(accountList);
		accountData.add(acct);
		accountData.add(accnt);
		OBReadAccount6Data data2 = new OBReadAccount6Data();
		data2.setAccount(accountData);
		mockOBReadAccount6.setData(data2);

		return mockOBReadAccount6;
	}

	public static OBReadAccount6 getCustomerAccountList() {
		OBReadAccount6 mockOBReadAccount6 = new OBReadAccount6();
		List<OBAccount6> accountData = new ArrayList<>();
		PSD2Account acct = new PSD2Account();
		acct.setAccountId("1234566");
		acct.setCurrency("EUR");
		acct.setNickname("John");
		Map<String, String> additionalInformation = new HashMap<>();
		additionalInformation.put("IBAN", "1234566");
		additionalInformation.put("BICFI", "1234566");
		acct.setAdditionalInformation(additionalInformation);

		List<OBAccount6Account> accountList = new ArrayList<>();
		OBAccount6Account mockData2Account = new OBAccount6Account();
		mockData2Account.setIdentification("1234566");
		mockData2Account.setSchemeName("IBAN");
		accountList.add(mockData2Account);
		OBBranchAndFinancialInstitutionIdentification50 servicer = new OBBranchAndFinancialInstitutionIdentification50();
		servicer.setIdentification("1234566");

		acct.setAccount(accountList);
		acct.setServicer(servicer);

		PSD2Account accnt = new PSD2Account();
		accnt.setAccountId("1234566");
		accnt.setCurrency("EUR");
		accnt.setNickname("John");
		accnt.setServicer(servicer);
		accnt.setAccount(accountList);
		accountData.add(acct);
		accountData.add(accnt);
		OBReadAccount6Data data2 = new OBReadAccount6Data();
		data2.setAccount(accountData);
		mockOBReadAccount6.setData(data2);

		return mockOBReadAccount6;
	}

	public static List<PSD2Account> getPSD2AcountList() {
		List<PSD2Account> accountData = new ArrayList<>();
		PSD2Account acct = new PSD2Account();
		acct.setAccountId("14556236");
		acct.setCurrency("EUR");
		acct.setNickname("John");

		List<OBAccount6Account> accountList = new ArrayList<>();
		OBAccount6Account mockData2Account = new OBAccount6Account();
		mockData2Account.setIdentification("1245676");
		mockData2Account.setSchemeName("IBAN");
		accountList.add(mockData2Account);
		OBBranchAndFinancialInstitutionIdentification50 servicer = new OBBranchAndFinancialInstitutionIdentification50();
		servicer.setIdentification("12345");

		acct.setAccount(accountList);
		acct.setServicer(servicer);

		PSD2Account accnt = new PSD2Account();
		accnt.setAccountId("14556236");
		accnt.setCurrency("EUR");
		accnt.setNickname("John");
		accnt.setServicer(servicer);
		accnt.setAccount(accountList);
		accountData.add(acct);
		accountData.add(accnt);

		return accountData;
	}

	public static CustomPaymentStageIdentifiers getStageIdentifiersForPostAuthorising() {
		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();
		stageIdentifiers.setPaymentConsentId("12345");
		stageIdentifiers.setPaymentTypeEnum(PaymentTypeEnum.INTERNATIONAL_PAY);
		
		return stageIdentifiers;
	}
	
	public static CustomPaymentStageIdentifiers getStageIdentifiersInternational() {
		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();
		stageIdentifiers.setPaymentConsentId("12345");
		stageIdentifiers.setPaymentTypeEnum(PaymentTypeEnum.INTERNATIONAL_PAY);
		
		return stageIdentifiers;
	}
}
