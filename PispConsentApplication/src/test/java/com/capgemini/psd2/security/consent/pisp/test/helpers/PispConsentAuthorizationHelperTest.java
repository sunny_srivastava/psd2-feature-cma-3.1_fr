package com.capgemini.psd2.security.consent.pisp.test.helpers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.aisp.domain.OBAccount6Account;
import com.capgemini.psd2.aisp.domain.OBBranchAndFinancialInstitutionIdentification50;
import com.capgemini.psd2.consent.domain.PSD2Account;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.security.consent.pisp.helpers.PispConsentAuthorizationHelper;

public class PispConsentAuthorizationHelperTest {

	@InjectMocks
	private PispConsentAuthorizationHelper pispConsentAutorizationHelper;
	
	@Mock
	private RequestHeaderAttributes requestHeaderAttributes;
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		requestHeaderAttributes = new RequestHeaderAttributes();
		requestHeaderAttributes.setTenantId("tenant1");
		requestHeaderAttributes.setPsuId("12345");
		

	}

	@Test
	public void populateAccountWithUnmaskedValuesTest() {
		Map<String, String> map = new HashMap<>();
		map.put("UK.OBIE.IBAN", "12345");
		map.put("UK.OBIE.SortCodeAccountNumber", "12345");
		Map<String, String> consentSupportedSchemeMap = new HashMap<String, String>();
		consentSupportedSchemeMap.put("UK.OBIE.IBAN", "UK_OBIE_IBAN");
		consentSupportedSchemeMap.put("UK.OBIE.SortCodeAccountNumber", "UK_OBIE_SortCodeAccountNumber");

		requestHeaderAttributes.setBypassConsentPage(true);
		ReflectionTestUtils.setField(pispConsentAutorizationHelper, "requestHeaderAttributes", requestHeaderAttributes);

		ReflectionTestUtils.setField(pispConsentAutorizationHelper, "consentSupportedSchemeMap",
				consentSupportedSchemeMap);

		PSD2Account accountwithoutmask = new PSD2Account();

		List<OBAccount6Account> accountList = new ArrayList<>();
		OBAccount6Account mockData2Account = new OBAccount6Account();
		mockData2Account.setIdentification("12345");
		mockData2Account.setSchemeName("UK.OBIE.IBAN");
		accountList.add(mockData2Account);
		OBBranchAndFinancialInstitutionIdentification50 servicer = new OBBranchAndFinancialInstitutionIdentification50();
		servicer.setIdentification("12345");

		accountwithoutmask.setServicer(servicer);
		accountwithoutmask.setAccount(accountList);
		accountwithoutmask.setCurrency("EUR");
		accountwithoutmask.setNickname("John");
		accountwithoutmask.setAdditionalInformation(map);

		PSD2Account accountwithmask = new PSD2Account();

		List<OBAccount6Account> accountList1 = new ArrayList<OBAccount6Account>();
		OBAccount6Account mockData2Account1 = new OBAccount6Account();
		mockData2Account1.setIdentification("12345");
		mockData2Account1.setSchemeName("UK.OBIE.IBAN");
		accountList1.add(mockData2Account1);
		OBBranchAndFinancialInstitutionIdentification50 servicer1 = new OBBranchAndFinancialInstitutionIdentification50();
		servicer1.setIdentification("12345");
		accountwithmask.setServicer(servicer);
		accountwithmask.setAccount(accountList1);
		accountwithmask.setCurrency("EUR");
		accountwithmask.setNickname("John");
		accountwithmask.setAdditionalInformation(map);

		pispConsentAutorizationHelper.populateAccountwithUnmaskedValues(accountwithmask, accountwithoutmask);

		mockData2Account1.setSchemeName("UK.OBIE.SortCodeAccountNumber");
		accountList1.add(mockData2Account1);
		mockData2Account.setSchemeName("UK.OBIE.SortCodeAccountNumber");
		accountList.add(mockData2Account);
		accountwithoutmask.setServicer(servicer);
		accountwithoutmask.setAccount(accountList);
		accountwithoutmask.setCurrency("EUR");
		accountwithoutmask.setNickname("John");
		accountwithoutmask.setAdditionalInformation(map);
		servicer1.setIdentification("12345");
		accountwithmask.setServicer(servicer1);
		accountwithmask.setAccount(accountList1);
		accountwithmask.setCurrency("EUR");
		accountwithmask.setNickname("John");
		accountwithmask.setAdditionalInformation(map);
		requestHeaderAttributes.setBypassConsentPage(false);
		ReflectionTestUtils.setField(pispConsentAutorizationHelper, "requestHeaderAttributes", requestHeaderAttributes);
		pispConsentAutorizationHelper.populateAccountwithUnmaskedValues(accountwithmask, accountwithoutmask);

	}
	
	@Test
	public void testpopulateAccountWithUnmaskedNullValues() {
		Map<String, String> map = new HashMap<>();
		map.put("UK.OBIE.IBAN", "12345");
		map.put("UK.OBIE.SortCodeAccountNumber", "12345");
		Map<String, String> consentSupportedSchemeMap = new HashMap<String, String>();
		consentSupportedSchemeMap.put("UK.OBIE.IBAN", "UK_OBIE_IBAN");
		consentSupportedSchemeMap.put("UK.OBIE.SortCodeAccountNumber", "UK_OBIE_SortCodeAccountNumber");

		ReflectionTestUtils.setField(pispConsentAutorizationHelper, "consentSupportedSchemeMap",
				consentSupportedSchemeMap);

		PSD2Account accountwithoutmask = new PSD2Account();

		List<OBAccount6Account> accountList = new ArrayList<>();
		OBAccount6Account mockData2Account = new OBAccount6Account();
		mockData2Account.setIdentification("12345");
		mockData2Account.setSchemeName("UK.OBIE.IBAN");
		accountList.add(mockData2Account);
		OBBranchAndFinancialInstitutionIdentification50 servicer = new OBBranchAndFinancialInstitutionIdentification50();
		servicer.setIdentification("12345");

		accountwithoutmask.setServicer(servicer);
		accountwithoutmask.setAccount(accountList);
		accountwithoutmask.setCurrency("EUR");
		accountwithoutmask.setNickname("John");
		accountwithoutmask.setAdditionalInformation(map);

		PSD2Account accountwithmask = new PSD2Account();

		List<OBAccount6Account> accountList1 = new ArrayList<OBAccount6Account>();
		OBAccount6Account mockData2Account1 = new OBAccount6Account();
		mockData2Account1.setIdentification("12345");
		mockData2Account1.setSchemeName("UK.OBIE.IBAN");
		accountList1.add(mockData2Account1);
		OBBranchAndFinancialInstitutionIdentification50 servicer1 = new OBBranchAndFinancialInstitutionIdentification50();
		servicer1.setIdentification("12345");
		accountwithmask.setServicer(servicer);
		accountwithmask.setAccount(accountList1);
		accountwithmask.setCurrency("EUR");
		accountwithmask.setNickname("John");
		accountwithmask.setAdditionalInformation(null);

		pispConsentAutorizationHelper.populateAccountwithUnmaskedValues(accountwithmask, accountwithoutmask);

		mockData2Account1.setSchemeName("UK.OBIE.SortCodeAccountNumber");
		accountList1.add(mockData2Account1);
		mockData2Account.setSchemeName("UK.OBIE.SortCodeAccountNumber");
		accountList.add(mockData2Account);
		accountwithoutmask.setServicer(servicer);
		accountwithoutmask.setAccount(accountList);
		accountwithoutmask.setCurrency("EUR");
		accountwithoutmask.setNickname("John");
		accountwithoutmask.setAdditionalInformation(map);
		servicer1.setIdentification("12345");
		accountwithmask.setServicer(servicer1);
		accountwithmask.setAccount(accountList1);
		accountwithmask.setCurrency("EUR");
		accountwithmask.setNickname("John");
		accountwithmask.setAdditionalInformation(map);
		pispConsentAutorizationHelper.populateAccountwithUnmaskedValues(accountwithmask, accountwithoutmask);

	}

}
