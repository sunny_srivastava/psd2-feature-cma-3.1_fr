/*package com.capgemini.psd2.security.consent.pisp.test.controllers;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.anyMap;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import com.capgemini.psd2.consent.domain.PSD2Account;
import com.capgemini.psd2.consent.domain.PSD2AccountsAdditionalInfo;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.integration.adapter.TPPInformationAdaptor;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.domain.OBCashAccountDebtor3;
import com.capgemini.psd2.pisp.domain.OBTransactionIndividualStatus1Code;
import com.capgemini.psd2.pisp.domain.PaymentConsentsPlatformResource;
import com.capgemini.psd2.pisp.domain.PaymentConsentsValidationResponse;
import com.capgemini.psd2.pisp.payment.setup.platform.adapter.PaymentSetupPlatformAdapter;
import com.capgemini.psd2.scaconsenthelper.config.PFConfig;
import com.capgemini.psd2.scaconsenthelper.config.helpers.ConsentAuthorizationHelper;
import com.capgemini.psd2.scaconsenthelper.config.helpers.PispConsentCreationDataHelper;
import com.capgemini.psd2.scaconsenthelper.config.helpers.SCAConsentLoggingHelper;
import com.capgemini.psd2.scaconsenthelper.constants.PFConstants;
import com.capgemini.psd2.scaconsenthelper.models.DropOffResponse;
import com.capgemini.psd2.scaconsenthelper.models.IntentTypeEnum;
import com.capgemini.psd2.scaconsenthelper.models.PickupDataModel;
import com.capgemini.psd2.scaconsenthelper.services.SCAConsentHelperService;
import com.capgemini.psd2.security.consent.pisp.rest.controllers.PispConsentAuthorizeController;
import com.capgemini.psd2.security.consent.pisp.test.mock.data.PispConsentApplicationMockdata;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
public class PispConsentAuthorizeControllerTest {

	@Mock
	private PispConsentCreationDataHelper consentCreationDataHelper;

	@InjectMocks
	private PispConsentAuthorizeController consentAuthorizeController;

	@Mock
	private RequestHeaderAttributes requestHeaderAttributes;

	@Mock
	private HttpServletRequest request;

	@Mock
	private HttpServletResponse response;

	@Mock
	private SCAConsentHelperService helperService;

	@Mock
	private TPPInformationAdaptor tppInformationAdaptor;

	@Mock
	private PaymentSetupPlatformAdapter paymentSetupPlatformAdapter;
	
	@Mock
	@Qualifier("pispConsentAuthorizationHelper")
	private ConsentAuthorizationHelper consentAuthorizationHelper;
	
	@Mock
	private SCAConsentLoggingHelper scaConsentLoggingHelper;
	
	

	@Mock
	private PFConfig pfConfig;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		requestHeaderAttributes = new RequestHeaderAttributes();
		requestHeaderAttributes.setTenantId("tenant1");
		requestHeaderAttributes.setPsuId("12345");
		requestHeaderAttributes.setCorrelationId("correlationId");
		ReflectionTestUtils.setField(consentAuthorizeController, "requestHeaderAttributes", requestHeaderAttributes);
		PaymentConsentsPlatformResource paymentConsentsPlatformResource = new PaymentConsentsPlatformResource();
		paymentConsentsPlatformResource.setTppCID("asctyafyt");
		paymentConsentsPlatformResource.setTppLegalEntityName("ascascassc");
		when(paymentSetupPlatformAdapter.retrievePaymentSetupPlatformResource(anyObject())).thenReturn(paymentConsentsPlatformResource);
		
		InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
		viewResolver.setPrefix("/");
		viewResolver.setSuffix(".jsp");

	}
    
	
	@Test
	public void testCreateConsentModelAndView() throws Exception {
		ModelAndView model = new ModelAndView();
		PSD2AccountsAdditionalInfo obj = new PSD2AccountsAdditionalInfo();
		obj.setAccountdetails(PispConsentApplicationMockdata.getPSD2AcountList());
		String resumePath = "0";
		
		PSD2Account account = new PSD2Account();
		Map<String,String> additionalInformation = new HashMap();
		additionalInformation.put("PAYER-JURISDICTION", "abc");
		additionalInformation.put("ACCOUNT-NUMBER", "12345");
		additionalInformation.put("ACCOUNT-NSC", "1024");
		additionalInformation.put("IBAN", "FR1420041010050500013M02606");
		additionalInformation.put("BICFI", "12345abcd");
		additionalInformation.put("plApplId", "78");
		
		PickupDataModel pickupDataModel = new PickupDataModel();
		pickupDataModel.setIntentTypeEnum(IntentTypeEnum.AISP_INTENT_TYPE);
		pickupDataModel.setIntentId("1234");
		account.setAdditionalInformation(additionalInformation);
		when(consentCreationDataHelper.retrieveCustomerAccountListInfo(anyString(), anyString(),
				anyString(), anyString(), anyString(), anyString(), anyString(), anyString(), anyString()))
						.thenReturn(PispConsentApplicationMockdata.getCustomerAccountInfo());
		when(consentAuthorizationHelper.matchAccountFromList(anyObject(), anyObject())).thenReturn(account);
		doNothing().when(consentCreationDataHelper).createConsent(anyObject(), anyString(), anyString(), anyObject(),
				anyString(), anyString(), anyString(), anyObject(), anyString());

		DropOffResponse dropresponse = new DropOffResponse();
		dropresponse.setRef("abcd");
		when(helperService.dropOffOnConsentSubmission(anyObject(), anyString(), anyString(), anyString()))
				.thenReturn(dropresponse);
		when(pfConfig.getTenantSpecificResumePathBaseUrl(anyString())).thenReturn("http://localhost:8");
		when(request.getAttribute(anyString())).thenReturn(pickupDataModel);
		when(tppInformationAdaptor.fetchApplicationName(anyString())).thenReturn("Moneywise");
		when(tppInformationAdaptor.fetchTPPInformation(anyString())).thenReturn(new Object());

		when(paymentSetupPlatformAdapter.populateStageIdentifiers(pickupDataModel.getIntentId())).thenReturn(PispConsentApplicationMockdata.getStageIdentifiers());
		Map<String, String> paramMap = new HashMap<>();
		paramMap.put("redirect_uri", "/test");
		
		consentAuthorizeController.createConsent(model, obj, resumePath);

	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void testCancelConsent() throws UnsupportedEncodingException {
		Map<String, String> paramsMap = new HashMap<>();
		paramsMap.put(PSD2Constants.SERVER_ERROR_FLAG_ATTR, "true");
		PickupDataModel pickupDataModel = new PickupDataModel();
		pickupDataModel.setChannelId("CHN");
		when(request.getAttribute(anyString())).thenReturn(pickupDataModel);
		paramsMap.put(PFConstants.RESUME_PATH, "80");
		when(pfConfig.getTenantSpecificResumePathBaseUrl("tenant1")).thenReturn("http://localhost:");
		ModelAndView model = new ModelAndView();
		doNothing().when(consentCreationDataHelper).cancelPaymentSetup(anyString(), anyMap());
		assertNotNull(consentAuthorizeController.cancelConsent(model, paramsMap));
	}

	@SuppressWarnings("unchecked")
	@Test
	public void testCancelConsentElseBranch() {
		Map<String, String> paramsMap = new HashMap<>();
		paramsMap.put(PSD2Constants.SERVER_ERROR_FLAG_ATTR, null);
		PickupDataModel pickupDataModel = new PickupDataModel();
		pickupDataModel.setChannelId("CHN");
		when(request.getAttribute(anyString())).thenReturn(pickupDataModel);
		doNothing().when(consentCreationDataHelper).cancelPaymentSetup(anyString(), anyMap());
		paramsMap.put(PFConstants.RESUME_PATH, "");
		when(pfConfig.getTenantSpecificResumePathBaseUrl("tenant1")).thenReturn("");
		when(helperService.cancelJourney(anyString(), anyObject())).thenReturn("http://localhost:80");
		ModelAndView model = new ModelAndView();
		consentAuthorizeController.cancelConsent(model, paramsMap);
	}
	
	@SuppressWarnings("unchecked")
	@Test
	public void testPreAuthorisationSuccessFlow() {
		Map<String, String> paramsMap = new HashMap<>();
		PickupDataModel pickUpModel = new PickupDataModel();
		PSD2Account account = PispConsentApplicationMockdata.getPSD2AccountData();
		Map<String,String> additionalInformation = new HashMap();
		additionalInformation.put("PAYER-JURISDICTION", "abc");
		additionalInformation.put("ACCOUNT-NUMBER", "12345");
		additionalInformation.put("ACCOUNT-NSC", "1024");
		additionalInformation.put("IBAN", "FR1420041010050500013M02606");
		additionalInformation.put("BICFI", "12345abcd");
		additionalInformation.put("plApplId", "78");
		
		account.setAdditionalInformation(additionalInformation);
		pickUpModel.setIntentId("1234");			
		pickUpModel.setIntentTypeEnum(IntentTypeEnum.PISP_INTENT_TYPE);
		
		OBCashAccountDebtor3 debtorAccount = new OBCashAccountDebtor3();
		debtorAccount.setSchemeName("IBAN");
		debtorAccount.setIdentification("FR1420041010050500013M02606");
		debtorAccount.setName("testing");
		debtorAccount.setSecondaryIdentification("asda");
		
		when(request.getAttribute(anyString())).thenReturn(pickUpModel);
		when(consentCreationDataHelper.retrieveCustomerAccountListInfo(anyString(), anyString(),
				anyString(), anyString(), anyString(), anyString(), anyString(), anyString(), anyObject()))
						.thenReturn(PispConsentApplicationMockdata.getCustomerAccountInfo());
		when(consentAuthorizationHelper.matchAccountFromList(anyObject(), anyObject())).thenReturn(account);
		when(consentCreationDataHelper.populateSelectedDebtorDetails(anyObject())).thenReturn(debtorAccount);
		PaymentConsentsValidationResponse validResponse = new PaymentConsentsValidationResponse();
		validResponse.setPaymentSetupValidationStatus(OBTransactionIndividualStatus1Code.ACCEPTEDSETTLEMENTINPROCESS);
		when(consentCreationDataHelper.validatePreAuthorisation(anyObject(), anyObject(), anyString(), anyMap(), anyObject()))
				.thenReturn(validResponse);
		ModelAndView model = new ModelAndView();
		
		when(paymentSetupPlatformAdapter.populateStageIdentifiers(anyString())).thenReturn(PispConsentApplicationMockdata.getStageIdentifiers());

		consentAuthorizeController.preAuthorisation(model, account, paramsMap);
	}
	
	@Test(expected = PSD2Exception.class)
	public void testPreAuthorisationAccountNUll() {
		Map<String, String> paramsMap = new HashMap<>();
		ModelAndView model = new ModelAndView();
		consentAuthorizeController.preAuthorisation(model, null, paramsMap);
	}

	@SuppressWarnings("unchecked")
	@Test(expected = PSD2Exception.class)
	public void testPreAuthorisationValidAuthorisationNull() {
		Map<String, String> paramsMap = new HashMap<>();
		PSD2Account account = PispConsentApplicationMockdata.getPSD2AccountData();
		Map<String,String> additionalInformation = new HashMap();
		additionalInformation.put("PAYER-JURISDICTION", "abc");
		additionalInformation.put("ACCOUNT-NUMBER", "12345");
		additionalInformation.put("ACCOUNT-NSC", "1024");
		additionalInformation.put("IBAN", "FR1420041010050500013M02606");
		additionalInformation.put("BICFI", "12345abcd");
		additionalInformation.put("plApplId", "78");
		
		account.setAdditionalInformation(additionalInformation);
		PickupDataModel pickUpModel = new PickupDataModel();
		pickUpModel.setIntentId("1234");
		pickUpModel.setIntentTypeEnum(IntentTypeEnum.AISP_INTENT_TYPE);
		when(request.getAttribute(anyString())).thenReturn(pickUpModel);
		when(consentCreationDataHelper.retrieveCustomerAccountListInfo(anyString(), anyString(),
				anyString(), anyString(), anyString(), anyString(), anyString(), anyString(), anyString()))
						.thenReturn(PispConsentApplicationMockdata.getCustomerAccountInfo());
		when(consentAuthorizationHelper.matchAccountFromList(anyObject(), anyObject())).thenReturn(account);
		when(consentCreationDataHelper.validatePreAuthorisation(anyObject(), anyObject(), anyString(), anyMap(), anyObject())).thenReturn(null);
		ModelAndView model = new ModelAndView();
		
		when(paymentSetupPlatformAdapter.populateStageIdentifiers(anyString())).thenReturn(PispConsentApplicationMockdata.getStageIdentifiers());
		
		consentAuthorizeController.preAuthorisation(model, account, paramsMap);
	}
}
*/