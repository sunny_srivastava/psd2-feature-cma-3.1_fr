/*package com.capgemini.psd2.security.consent.pisp.test.helpers;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyMap;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.naming.NamingException;
import javax.naming.directory.BasicAttributes;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.aisp.adapter.CustomerAccountListAdapter;
import com.capgemini.psd2.aisp.domain.OBAccount6;
import com.capgemini.psd2.consent.domain.PispConsent;
import com.capgemini.psd2.enums.ConsentStatusEnum;
import com.capgemini.psd2.fraudsystem.helper.FraudSystemHelper;
import com.capgemini.psd2.logger.LoggerUtils;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.adapter.PispConsentAdapter;
import com.capgemini.psd2.pisp.domain.OBCashAccountDebtor3;
import com.capgemini.psd2.pisp.domain.PaymentConsentsValidationResponse;
import com.capgemini.psd2.pisp.enums.PaymentTypeEnum;
import com.capgemini.psd2.pisp.payment.setup.platform.adapter.PaymentSetupPlatformAdapter;
import com.capgemini.psd2.pisp.sca.consent.operations.adapter.PispScaConsentOperationsAdapter;
import com.capgemini.psd2.pisp.stage.domain.CustomConsentAppViewData;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.pisp.stage.domain.CustomPreAuthorizeAdditionalInfo;
import com.capgemini.psd2.scaconsenthelper.config.helpers.PispConsentCreationDataHelperImpl;
import com.capgemini.psd2.security.consent.pisp.test.mock.data.PispConsentApplicationMockdata;


@RunWith(SpringJUnit4ClassRunner.class)
public class PispConsentCreationDataHelperImplTest {
	@Mock
	private List<String> postAuthorizingAllowedPaymentTypes;

	@Mock
	private LoggerUtils loggerUtils;

	@Mock
	private CustomerAccountListAdapter customerAccountListAdapter;

	@Mock
	private PispConsentAdapter pispConsentAdapter;

	@Mock
	private FraudSystemHelper fraudSystemHelper;

	@Mock
	private PaymentSetupPlatformAdapter paymentSetupPlatformAdapter;

	@Mock
	private PispScaConsentOperationsAdapter pispStageOperationsAdapter;

	@Mock
	private RequestHeaderAttributes reqAttributes;

	@InjectMocks
	private PispConsentCreationDataHelperImpl consentCreationDataHelperImpl;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		Map<String, String> paramMap = new HashMap<>();
		paramMap.put("PISP", "CISP");
		paramMap.put("client_id", "client123");

	}

	@Test
	public void testCreateConsent() throws NamingException {
		String userId = "12345";
		String clientId = "1234";
		String channelId = "12345";
		String tenantID = "tenant1";
		String headers = Base64.getEncoder().encodeToString(userId.getBytes());
		String tppApplicationName = "Moneywise.com";
		OBAccount6 account = PispConsentApplicationMockdata.getAccountData();
		ReflectionTestUtils.setField(consentCreationDataHelperImpl, "consentExpiryTime", 24);
		doNothing().when(pispConsentAdapter).createConsent(any(PispConsent.class));
		consentCreationDataHelperImpl.createConsent(account, userId, clientId,
				PispConsentApplicationMockdata.getStageIdentifiers(), channelId, headers, tppApplicationName,
				anyObject(), tenantID);
	}

	@Test
	public void testCreateConsentStandingOrderDebtorDetails() throws NamingException {
		String userId = "12345";
		String clientId = "1234";
		String channelId = "12345";
		String tenantID = "tenant1";
		String headers = Base64.getEncoder().encodeToString(userId.getBytes());
		String tppApplicationName = "Moneywise.com";
		OBAccount6 account = PispConsentApplicationMockdata.getAccountData();
		ReflectionTestUtils.setField(consentCreationDataHelperImpl, "consentExpiryTime", 24);
		doNothing().when(pispConsentAdapter).createConsent(any(PispConsent.class));
		consentCreationDataHelperImpl.populateSelectedDebtorDetails(account);
		consentCreationDataHelperImpl.createConsent(account, userId, clientId,
				PispConsentApplicationMockdata.getStageIdentifiers(), channelId, headers, tppApplicationName,
				anyObject(), tenantID);
	}

	@Test
	public void testCreateConsenttppInformationObj() throws NamingException {
		String userId = "12345";
		String clientId = "1234";
		String channelId = "12345";
		String tenantID = "tenant1";
		String headers = Base64.getEncoder().encodeToString(userId.getBytes());
		String tppApplicationName = "Moneywise.com";
		OBAccount6 account = PispConsentApplicationMockdata.getAccountData();
		ReflectionTestUtils.setField(consentCreationDataHelperImpl, "consentExpiryTime", 24);
		doNothing().when(pispConsentAdapter).createConsent(any(PispConsent.class));
		consentCreationDataHelperImpl.populateSelectedDebtorDetails(account);
		BasicAttributes tppInformationObj = new BasicAttributes();
		BasicAttributes tppInfoAttributes = new BasicAttributes(false);
		tppInfoAttributes.put("123", "456");
		consentCreationDataHelperImpl.createConsent(account, userId, clientId,
				PispConsentApplicationMockdata.getStageIdentifiers(), channelId, headers, tppApplicationName,
				tppInformationObj, tenantID);
	}

	@SuppressWarnings("unchecked")
	@Test
	public void testRetrieveCustomerAccountListInfoSuccessFlow() {
		String userId = "12345";
		String clientId = "1234";
		String flowType = "12345";
		String correlationId = "12345";
		String channelId = "12345";
		String cmaVersion = "3.1";
		when(reqAttributes.getCorrelationId()).thenReturn("123445");
		when(paymentSetupPlatformAdapter.populateStageIdentifiers(anyString()))
				.thenReturn(PispConsentApplicationMockdata.getStageIdentifiers());
		when(customerAccountListAdapter.retrieveCustomerAccountList(anyString(), anyMap()))
				.thenReturn(PispConsentApplicationMockdata.getCustomerAccountInfo());
		assertEquals(
				PispConsentApplicationMockdata.getCustomerAccountInfo().getData().getAccount().get(1).getAccount()
						.get(0).getIdentification(),
				consentCreationDataHelperImpl
						.retrieveCustomerAccountListInfo(userId, clientId, flowType, correlationId, channelId, null,
								channelId, channelId, cmaVersion)
						.getData().getAccount().get(0).getAccount().get(0).getIdentification());
	}

	@Test
	public void testRetrieveConsentAppStagedViewData() {
		CustomConsentAppViewData data = new CustomConsentAppViewData();
		when(pispStageOperationsAdapter
				.retrieveConsentAppStagedViewData(PispConsentApplicationMockdata.getStageIdentifiers(), null))
						.thenReturn(data);
		consentCreationDataHelperImpl.retrieveConsentAppStagedViewData(anyString(), any());
	}

	@Test
	public void testValidatePreAuthorisation() {
		String paymentId = "56323413c061485aaa622c195aa15e7c";
		Map<String, String> paramsMap = new HashMap<>();
		paramsMap.put("channelId", "BOL");
		paramsMap.put("x-user-id", "RMCUT001");
		CustomPreAuthorizeAdditionalInfo preAuthAdditionalInfo = new CustomPreAuthorizeAdditionalInfo();
		OBCashAccountDebtor3 debtorDetails = new OBCashAccountDebtor3();
		PaymentConsentsValidationResponse response = new PaymentConsentsValidationResponse();
		when(pispStageOperationsAdapter.validatePreAuthorisation(debtorDetails, preAuthAdditionalInfo,
				PispConsentApplicationMockdata.getStageIdentifiers(), paramsMap)).thenReturn(response);
		consentCreationDataHelperImpl.validatePreAuthorisation(debtorDetails, preAuthAdditionalInfo, paymentId,
				paramsMap, PispConsentApplicationMockdata.getStageIdentifiers());
	}

	@Test
	public void testValidatePreAuthorisationforStandingOrder() {
		String paymentId = "56323413c061485aaa622c195aa15e7c";
		Map<String, String> paramsMap = new HashMap<>();
		paramsMap.put("channelId", "BOL");
		paramsMap.put("x-user-id", "RMCUT001");
		CustomPreAuthorizeAdditionalInfo preAuthAdditionalInfo = new CustomPreAuthorizeAdditionalInfo();
		PaymentConsentsValidationResponse response = new PaymentConsentsValidationResponse();
		OBCashAccountDebtor3 debtorDetails = new OBCashAccountDebtor3();
		when(pispStageOperationsAdapter.validatePreAuthorisation(debtorDetails, preAuthAdditionalInfo,
				PispConsentApplicationMockdata.getStageIdentifiers(), paramsMap)).thenReturn(response);
		consentCreationDataHelperImpl.validatePreAuthorisation(debtorDetails, preAuthAdditionalInfo, paymentId,
				paramsMap, PispConsentApplicationMockdata.getStageIdentifiers());
	}
	
	@Test
	public void testInvalidIsPostAuthorizingSupportedPaymentType(){
		String paymentType = "";
		List<String> postAuthorizingAllowedPaymentTypes = new ArrayList<>();
		postAuthorizingAllowedPaymentTypes.add("Internationalpayments");
		postAuthorizingAllowedPaymentTypes.add("InternationalScheduledPayments");
		consentCreationDataHelperImpl.isPostAuthorizingSupported(paymentType);
		consentCreationDataHelperImpl.setPostAuthorizingAllowedPaymentTypes(postAuthorizingAllowedPaymentTypes);
	}
	@Test
	public void testValidPostAuthorizingAllowedPaymentTypes(){
		String paymentType = "Internationalpayments";
		List<String> postAuthorizingAllowedPaymentTypes = new ArrayList<>();
		postAuthorizingAllowedPaymentTypes.add("Internationalpayments");
		postAuthorizingAllowedPaymentTypes.add("InternationalScheduledPayments");
		consentCreationDataHelperImpl.setPostAuthorizingAllowedPaymentTypes(postAuthorizingAllowedPaymentTypes);
		Assert.assertTrue(paymentType.contains("Internationalpayments"));
		consentCreationDataHelperImpl.isPostAuthorizingSupported(paymentType);		
	}
	
	@Test
	public void testInvalidPostAuthorizingAllowedPaymentTypes(){
		String paymentType = "Internationalpayments";
		List<String> postAuthorizingAllowedPaymentTypes = new ArrayList<>();
		consentCreationDataHelperImpl.setPostAuthorizingAllowedPaymentTypes(postAuthorizingAllowedPaymentTypes);
		Assert.assertTrue(paymentType.contains(paymentType));
		consentCreationDataHelperImpl.isPostAuthorizingSupported(paymentType);
		
	}
	
	@Test
	public void cancelPaymentSetup() {
		PispConsent pispConsent = new PispConsent();
		pispConsent.setStatus(ConsentStatusEnum.AWAITINGAUTHORISATION);
		when(pispConsentAdapter.retrieveConsentByPaymentId(anyString())).thenReturn(pispConsent);
		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();
		stageIdentifiers.setPaymentTypeEnum(PaymentTypeEnum.DOMESTIC_PAY);
		when(paymentSetupPlatformAdapter.populateStageIdentifiers(anyString())).thenReturn(stageIdentifiers);
		
		Map paramsMap = new HashMap<>();
		paramsMap.put("IBAN", "IBAN");
		consentCreationDataHelperImpl.cancelPaymentSetup("123456", paramsMap);
	}
	
	@Test
	public void cancelPaymentSetupException() {
		PispConsent pispConsent = new PispConsent();
		pispConsent.setStatus(ConsentStatusEnum.AWAITINGAUTHORISATION);
		when(pispConsentAdapter.retrieveConsentByPaymentId(anyString())).thenReturn(pispConsent);
		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();
		stageIdentifiers.setPaymentTypeEnum(PaymentTypeEnum.DOMESTIC_PAY);
		when(paymentSetupPlatformAdapter.populateStageIdentifiers(anyString())).thenReturn(stageIdentifiers);
		
		Map paramsMap = new HashMap<>();
		paramsMap.put("IBAN", "IBAN");
		consentCreationDataHelperImpl.cancelPaymentSetup("123456", paramsMap);
	}

}
*/