/*************************************************************************
 * 
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 * 
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 */
package com.capgemini.psd2.security.consent.pisp.view.controllers;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.naming.NamingException;
import javax.naming.directory.BasicAttributes;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.UriUtils;

import com.capgemini.psd2.aisp.domain.OBAccount6;
import com.capgemini.psd2.aisp.domain.OBAccount6Account;
import com.capgemini.psd2.aisp.domain.OBReadAccount6;
import com.capgemini.psd2.aisp.domain.OBReadAccount6Data;
import com.capgemini.psd2.consent.domain.DebtorDetailsResponse;
import com.capgemini.psd2.consent.domain.PSD2Account;
import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.fraudsystem.request.handler.impl.FraudSystemRequestMapping;
import com.capgemini.psd2.fraudsystem.utilities.FraudSystemUtilities;
import com.capgemini.psd2.integration.adapter.TPPInformationAdaptor;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.mask.DataMask;
import com.capgemini.psd2.pisp.domain.CustomDPaymentConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.OBCashAccountCreditor2;
import com.capgemini.psd2.pisp.domain.OBDomestic1;
import com.capgemini.psd2.pisp.domain.OBDomestic1InstructedAmount;
import com.capgemini.psd2.pisp.domain.OBRemittanceInformation1;
import com.capgemini.psd2.pisp.domain.OBWriteDataDomesticConsentResponse1;
import com.capgemini.psd2.pisp.payment.setup.platform.adapter.PaymentSetupPlatformAdapter;
import com.capgemini.psd2.pisp.stage.domain.CustomConsentAppViewData;
import com.capgemini.psd2.pisp.stage.domain.CustomDebtorDetails;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.scaconsenthelper.config.CdnConfig;
import com.capgemini.psd2.scaconsenthelper.config.helpers.PispConsentCreationDataHelper;
import com.capgemini.psd2.scaconsenthelper.config.helpers.SCAConsentHelper;
import com.capgemini.psd2.scaconsenthelper.constants.OIDCConstants;
import com.capgemini.psd2.scaconsenthelper.models.PickupDataModel;
import com.capgemini.psd2.security.consent.pisp.config.PispHostNameConfig;
import com.capgemini.psd2.security.constants.PSD2SecurityConstants;
import com.capgemini.psd2.security.exceptions.PSD2SecurityException;
import com.capgemini.psd2.security.exceptions.SCAConsentErrorCodeEnum;
import com.capgemini.psd2.tppinformation.adaptor.ldap.constants.TPPInformationConstants;
import com.capgemini.psd2.ui.content.utility.controller.UIStaticContentUtilityController;
import com.capgemini.psd2.utilities.JSONUtilities;
import com.capgemini.psd2.utilities.NullCheckUtils;
import com.capgemini.psd2.utilities.SandboxConfig;

/**
 * ConsentView Controller
 * 
 * @author Capgemini
 *
 */
@Controller
@ConfigurationProperties(prefix = "app")
public class PispConsentViewController {
	
	private static Logger LOGGER = LoggerFactory.getLogger(PispConsentViewController.class); //NOSONAR

	@Autowired
	private PispHostNameConfig pispHostNameConfig;

	@Value("${spring.application.name}")
	private String applicationName;

	@Value("${app.fraudSystem.resUrl}")
	private String resUrl;

	@Value("${app.fraudSystem.hdmUrl}")
	private String hdmUrl;

	@Value("${app.fraudSystem.hdmInputName}")
	private String hdmInputName;

	@Value("${app.fraudSystem.jsc-file-path}")
	private String jscFilePath;
	
	@Value("${app.postAuthorizingFlowEnabled:true}")
	private boolean isPostAuthorisingPartyFlowEnabled;

	@Autowired
	private TPPInformationAdaptor tppInformationAdaptor;

	@Autowired
	private UIStaticContentUtilityController uiController;

	@Autowired
	private RequestHeaderAttributes requestHeaders;

	@Autowired
	private PispConsentCreationDataHelper consentCreationDataHelper;

	@Autowired
	private HttpServletRequest request;

	@Autowired
	private HttpServletResponse response;

	@Autowired
	private DataMask dataMask;

	@Autowired
	private SandboxConfig sandboxConfig;
	
	@Autowired
	private PaymentSetupPlatformAdapter paymentSetupPlatformAdapter;

	@Value("${cdn.baseURL}")
	private String cdnBaseURL;
	
	private Map<String, String> baseCurrency;

	private Map<String, String> consentSupportedSchemeMap = new HashMap<>();

	public Map<String, String> getConsentSupportedSchemeMap() {
		return consentSupportedSchemeMap;
	}
	
	public Map<String, String> getBaseCurrency() {
		return baseCurrency;
	}

	public void setBaseCurrency(Map<String, String> baseCurrency) {
		this.baseCurrency = baseCurrency;
	}

	@RequestMapping(value = "/pisp/home")
	public ModelAndView homePage(Map<String, Object> model) {
		SCAConsentHelper.populateSecurityHeaders(response);

		String homeScreen = "/customerconsentview";
		String requestUrl = request.getRequestURI();
		requestUrl = requestUrl.replace("/home", homeScreen);
		model.put("redirectUrl", pispHostNameConfig.getTenantSpecificEdgeserverhost(requestHeaders.getTenantId())
				.concat("/").concat(applicationName).concat(requestUrl).concat("?").concat(request.getQueryString()));
		model.put(PSD2Constants.NETSCALER_TENANT_ID_HEADER_NAME, requestHeaders.getTenantId());
		model.put(PSD2Constants.IS_SANDBOX_ENABLED, sandboxConfig.isSandboxEnabled());
		return new ModelAndView("homePage", model);
	}

	/**
	 * customerconsentview end-point of consent application gets invoked by
	 * OAuth2 server with some params like - call back URI of OAuth2 server & ID
	 * token which contains the identity information of the customer. It
	 * collects the data for allowed scopes of the caller AISP & fetches
	 * accounts list of the current customer by using the user-id information
	 * taken from the ID token (SaaS Server issued ID token) which gets relayed
	 * by the OAuth server to consent app.
	 * 
	 * @param model
	 * @param oAuthUrl
	 * @param idToken
	 * @return
	 * @throws NamingException
	 */
	@RequestMapping(value = "/pisp/customerconsentview")
	public ModelAndView consentView(Map<String, Object> model) throws NamingException {
		boolean isAccounSelected = false;

		PickupDataModel pickupDataModel = SCAConsentHelper.populatePickupDataModel(request);

		/*
		 * Setting channelId in reqHeader to be available in all adapter calls
		 */
		requestHeaders.setChannelId(pickupDataModel.getChannelId());

		String stagedDebtorSchemeName = null;
		
		CustomPaymentStageIdentifiers stageIdentifiers = paymentSetupPlatformAdapter
				.populateStageIdentifiers(pickupDataModel.getIntentId());

		/* Removed call for retrieve payment stage details */
		CustomConsentAppViewData consentAppPaymentSetupData = consentCreationDataHelper
				.retrieveConsentAppStagedViewData(pickupDataModel.getIntentId(), stageIdentifiers);
		CustomDebtorDetails stagedSetupDebtorDetails = consentAppPaymentSetupData.getDebtorDetails();
		if (!NullCheckUtils.isNullOrEmpty(stagedSetupDebtorDetails)) {
			stagedDebtorSchemeName = stagedSetupDebtorDetails.getSchemeName();
			if (!consentSupportedSchemeMap.containsKey(stagedSetupDebtorDetails.getSchemeName())) {
				throw PSD2Exception
						.populatePSD2Exception(ErrorCodeEnum.PROVIDED_DEBTOR_SCHEME_NOT_SUPPORTED_FOR_CONSENT_APP);
			}
		}
		
		OBReadAccount6 customerAccountList = consentCreationDataHelper.retrieveCustomerAccountListInfo(
				pickupDataModel.getUserId(), pickupDataModel.getClientId(),
				pickupDataModel.getIntentTypeEnum().getIntentType(), requestHeaders.getCorrelationId(),
				pickupDataModel.getChannelId(), stagedDebtorSchemeName, requestHeaders.getTenantId(),
				pickupDataModel.getIntentId(), stageIdentifiers.getPaymentSetupVersion());
		
		Object tppInformationObj = tppInformationAdaptor.fetchTPPInformation(pickupDataModel.getClientId());
		String tppApplicationName = tppInformationAdaptor.fetchApplicationName(pickupDataModel.getClientId());

		if (!NullCheckUtils.isNullOrEmpty(stagedSetupDebtorDetails)) {
			isAccounSelected = true;
			customerAccountList = validateAccountWithAccountList(stagedSetupDebtorDetails, customerAccountList);
		}
		
		// Remove setUpResponseDataforUI once UI changes done.
		CustomDPaymentConsentsPOSTResponse setUpResponseDataforUI = populateLimitedSetUpDataforUI(
				consentAppPaymentSetupData);

		model.put(PSD2SecurityConstants.CUSTOMER_ACCOUNT_LIST, dataMask.maskResponseGenerateString(customerAccountList, "account"));

		model.put(PSD2Constants.IS_SANDBOX_ENABLED, sandboxConfig.isSandboxEnabled());

		// To be used. Changes needed at UI.
		model.put(PSD2Constants.PAYMENT_CONSENT_DATA,dataMask.maskResponseGenerateString(consentAppPaymentSetupData, "paymentSetupData"));
		
		model.put(PSD2SecurityConstants.CONSENT_SETUP_DATA,
				JSONUtilities.getJSONOutPutFromObject(setUpResponseDataforUI));
		model.put(PSD2Constants.CONSENT_FLOW_TYPE, pickupDataModel.getIntentTypeEnum().getIntentType());
		model.put(PSD2Constants.JS_MSG,
				uiController.retrieveUiParamValue(PSD2Constants.ERROR_MESSAGE, PSD2Constants.JS_MSG));
		model.put(PSD2Constants.CO_RELATION_ID, requestHeaders.getCorrelationId());
		
		//Post Authorizing Party data setup
		String debtorDetailsResponseString = null;
		String paymentType = stageIdentifiers.getPaymentTypeEnum().getPaymentType();
		
		if(isPostAuthorisingPartyFlowEnabled && consentCreationDataHelper.isPostAuthorizingSupported(paymentType)){
			model.put(PSD2Constants.IS_POST_AUTHORISING_PARTY_FLOW_ENABLED,true);
			
			//Adding post authorizing call for single account with respect to psuid
			int lengthOfCustomerAccountList =customerAccountList.getData().getAccount().size();
			if(!NullCheckUtils.isNullOrEmpty(consentAppPaymentSetupData.getDebtorDetails()) || (lengthOfCustomerAccountList == 1)){
				PSD2Account unmaskedAccount = (PSD2Account)customerAccountList.getData().getAccount().get(0);
				DebtorDetailsResponse debtorDetailsResponse = consentCreationDataHelper.postAuthorisingPartyDetails(pickupDataModel, unmaskedAccount, stageIdentifiers);
				debtorDetailsResponseString =  JSONUtilities.getJSONOutPutFromObject(debtorDetailsResponse).trim();
			}
		}
		model.put(PSD2Constants.POST_AUTHORISING_PARTY_DETAILS, debtorDetailsResponseString);
		
		String tenantId = requestHeaders.getTenantId();
		String currency;
		
		if (!baseCurrency.isEmpty() && tenantId != null && baseCurrency.containsKey(tenantId)){
			 currency = baseCurrency.get(tenantId);
			 model.put(PSD2Constants.BASE_CURRENCY,currency);
		}

		JSONObject tppInfoJson = new JSONObject();
		if (tppInformationObj != null && tppInformationObj instanceof BasicAttributes) {
			BasicAttributes tppInfoAttributes = (BasicAttributes) tppInformationObj;
			String legalEntityName = returnLegalEntityName(tppInfoAttributes);
			model.put(PSD2SecurityConstants.TPP_NAME, legalEntityName);
			tppInfoJson.put(PSD2SecurityConstants.TPP_NAME, legalEntityName);
			
			requestHeaders.setTppLegalEntityName(legalEntityName);
		}
		tppInfoJson.put(PSD2SecurityConstants.APP_NAME, tppApplicationName);
		model.put(PSD2SecurityConstants.TPP_INFO, tppInfoJson.toString());

		model.put(PSD2SecurityConstants.ACCOUNT_SELECTED, isAccounSelected);
		model.put(PSD2SecurityConstants.USER_ID, pickupDataModel.getUserId());
		model.put(PSD2Constants.JS_MSG,
				uiController.retrieveUiParamValue(PSD2Constants.ERROR_MESSAGE, PSD2Constants.JS_MSG));
		model.put(PSD2Constants.CO_RELATION_ID, requestHeaders.getCorrelationId());
		model.put(PSD2Constants.APPLICATION_NAME, applicationName);
		model.put(OIDCConstants.CHANNEL_ID, pickupDataModel.getChannelId());
		model.put(PSD2Constants.NETSCALER_TENANT_ID_HEADER_NAME, requestHeaders.getTenantId());

		JSONObject fraudHdmInfo = new JSONObject();
		try {
			fraudHdmInfo.put(PSD2SecurityConstants.RES_URL, UriUtils.encode(resUrl, StandardCharsets.UTF_8.toString()));
			fraudHdmInfo.put(PSD2SecurityConstants.HDM_URL, UriUtils.encode(hdmUrl, StandardCharsets.UTF_8.toString()));
		} catch (UnsupportedEncodingException e) {
			LOGGER.error("UnsupportedEncodingException Occurred in parseClaims: "+e);
			throw PSD2SecurityException.populatePSD2SecurityException(e.getMessage(),
					SCAConsentErrorCodeEnum.TECHNICAL_ERROR);
		}
		fraudHdmInfo.put(PSD2SecurityConstants.HDM_INPUT_NAME, hdmInputName);
		model.put(PSD2SecurityConstants.FRAUD_HDM_INFO, fraudHdmInfo.toString());
		model.put(PSD2SecurityConstants.JSC_FILE_PATH, jscFilePath);
		model.put(FraudSystemRequestMapping.FS_HEADERS, Base64.getEncoder()
				.encodeToString(FraudSystemUtilities.populateFraudSystemHeaders(request).getBytes()));
		model.put(PSD2Constants.UICONTENT, uiController.getConfigVariable());
		model.putAll(CdnConfig.populateCdnAttributes());
		model.put(PSD2Constants.CMAVERSION, stageIdentifiers.getPaymentSetupVersion());
		return new ModelAndView("index", model);
	}

	private String returnLegalEntityName(BasicAttributes basicAttributes) throws NamingException {
		return getAttributeValue(basicAttributes, TPPInformationConstants.LEGAL_ENTITY_NAME);
	}

	private String getAttributeValue(BasicAttributes tppApplication, String ldapAttr) throws NamingException {
		String attributeValue = null;
		if (tppApplication.get(ldapAttr) != null && tppApplication.get(ldapAttr).get() != null) {
			attributeValue = tppApplication.get(ldapAttr).get().toString();
		}
		return attributeValue;
	}

	private OBReadAccount6 validateAccountWithAccountList(CustomDebtorDetails debtorDetails,
			OBReadAccount6 obReadAccount2) {
		boolean isAccountMatch = false;

		String debtorSchemeName = debtorDetails.getSchemeName();

		if (obReadAccount2 != null && !obReadAccount2.getData().getAccount().isEmpty()) {
			for (OBAccount6 account : obReadAccount2.getData().getAccount()) {

				if (account instanceof PSD2Account) {
					Map<String, String> additionalInfo = ((PSD2Account) account).getAdditionalInformation();

					if (!consentSupportedSchemeMap.containsKey(debtorSchemeName)) {
						throw PSD2Exception.populatePSD2Exception(
								ErrorCodeEnum.PROVIDED_DEBTOR_SCHEME_NOT_SUPPORTED_FOR_CONSENT_APP);
					}

					String ymlScheme = debtorDetails.getSchemeName(); // Scheme From Payload
					
					int len = ymlScheme.split("\\.").length;
					ymlScheme = ymlScheme.split("\\.")[len - 1];

					if((additionalInfo.containsKey(ymlScheme) 
							&& additionalInfo.get(ymlScheme).equals(debtorDetails.getIdentification()) )) {
						isAccountMatch = true;
						obReadAccount2 = populateAccountData(account, debtorDetails); //NOSONAR
						break;
					}
				}
			}
			if (!isAccountMatch)
				throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.SELECTED_ACCOUNTS_NOT_OWNED_BY_CUSTOMER);
		} else
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.SELECTED_ACCOUNTS_NOT_OWNED_BY_CUSTOMER);
		return obReadAccount2;
	}

	private OBReadAccount6 populateAccountData(OBAccount6 account, CustomDebtorDetails debtorDetails) {
		List<OBAccount6> accountData = new ArrayList<>();
		List<OBAccount6Account> obAccount2AccountList = new ArrayList<>();
		OBAccount6Account accountDetails = new OBAccount6Account();

		accountDetails.setName(debtorDetails.getName());
		accountDetails.setSecondaryIdentification(debtorDetails.getSecondaryIdentification());
		accountDetails.setIdentification(debtorDetails.getIdentification());
		accountDetails.setSchemeName(debtorDetails.getSchemeName());
		obAccount2AccountList.add(accountDetails);
		account.setAccount(obAccount2AccountList);
		accountData.add(account);
		OBReadAccount6Data data2 = new OBReadAccount6Data();
		data2.setAccount(accountData);
		OBReadAccount6 oBReadAccount2 = new OBReadAccount6();
		oBReadAccount2.setData(data2);
		return oBReadAccount2;
	}

	
	private CustomDPaymentConsentsPOSTResponse populateLimitedSetUpDataforUI(
			CustomConsentAppViewData consentAppPaymentSetupData) {

		CustomDPaymentConsentsPOSTResponse pmtPostResp = new CustomDPaymentConsentsPOSTResponse();
		OBWriteDataDomesticConsentResponse1 pmtSetupResp = new OBWriteDataDomesticConsentResponse1();
		OBDomestic1 pmtSetupRepsInit = new OBDomestic1();
		OBDomestic1InstructedAmount resultInstAmount = new OBDomestic1InstructedAmount();
		OBCashAccountCreditor2 resultCreditorAccount = new OBCashAccountCreditor2();
		OBRemittanceInformation1 resultRemittanceInfo = new OBRemittanceInformation1();

		if (consentAppPaymentSetupData != null) {
			if (consentAppPaymentSetupData.getAmountDetails() != null) {
				resultInstAmount.setAmount(consentAppPaymentSetupData.getAmountDetails().getAmount());
				resultInstAmount.setCurrency(consentAppPaymentSetupData.getAmountDetails().getCurrency());
			}
			if (consentAppPaymentSetupData.getCreditorDetails() != null) {
				resultCreditorAccount.setName(consentAppPaymentSetupData.getCreditorDetails().getName());
				resultCreditorAccount.setSecondaryIdentification(
						consentAppPaymentSetupData.getCreditorDetails().getSecondaryIdentification());
			}
			if (consentAppPaymentSetupData.getRemittanceDetails() != null) {
				resultRemittanceInfo.setReference(consentAppPaymentSetupData.getRemittanceDetails().getReference());
				resultRemittanceInfo
						.setUnstructured(consentAppPaymentSetupData.getRemittanceDetails().getUnstructured());
			}
		}

		pmtSetupRepsInit.setInstructedAmount(resultInstAmount);
		pmtSetupRepsInit.setCreditorAccount(resultCreditorAccount);
		pmtSetupRepsInit.setRemittanceInformation(resultRemittanceInfo);
		pmtSetupResp.setInitiation(pmtSetupRepsInit);
		pmtPostResp.setData(pmtSetupResp);
		return pmtPostResp;
	}
}
