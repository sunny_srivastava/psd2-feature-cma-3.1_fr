package com.capgemini.psd2.security.consent.pisp.config;

import java.util.ArrayList;
import java.util.List;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.capgemini.psd2.scaconsenthelper.filters.ConsentBlockBackActionFilter;
import com.capgemini.psd2.scaconsenthelper.filters.ConsentCookieHandlerFilter;
import com.capgemini.psd2.scaconsenthelper.config.helpers.ConsentAuthorizationHelper;
import com.capgemini.psd2.scaconsenthelper.filters.CookieHandlerFilter;
import com.capgemini.psd2.scaconsenthelper.models.IntentTypeEnum;
import com.capgemini.psd2.security.consent.pisp.helpers.PispConsentAuthorizationHelper;

@Configuration
public class PispConsentConfig {

	@Bean
	public CookieHandlerFilter buildPispCookieHandlerFilter(){
		return new ConsentCookieHandlerFilter(IntentTypeEnum.PISP_INTENT_TYPE.getIntentType());
	}
	
	@Bean
	public ConsentBlockBackActionFilter buildPispBlockActionFilter(){
		return new ConsentBlockBackActionFilter();		
	}
	
	@Bean
	public FilterRegistrationBean blockPISPRefreshEventFilter(){
		FilterRegistrationBean filterRegBean = new FilterRegistrationBean();
		filterRegBean.setFilter(buildPispBlockActionFilter());
		List<String> urlPatterns = new ArrayList<>();
		urlPatterns.add("/pisp/home");
		filterRegBean.setName("pispBlockBackEventFilter");
		filterRegBean.setUrlPatterns(urlPatterns);
		return filterRegBean;
	}	
	
	@Bean
	public FilterRegistrationBean pispFilter(){
		FilterRegistrationBean filterRegBean = new FilterRegistrationBean();
		filterRegBean.setFilter(buildPispCookieHandlerFilter());
		List<String> urlPatterns = new ArrayList<>();
		urlPatterns.add("/pisp/customerconsentview");
		urlPatterns.add("/pisp/consent");
		urlPatterns.add("/pisp/postAuthorizingParty");
		urlPatterns.add("/pisp/cancelConsent");		
		urlPatterns.add("/pisp/checkSession");
		urlPatterns.add("/pisp/preAuthorisation");
		filterRegBean.setName("pispCookieFilter");
		filterRegBean.setUrlPatterns(urlPatterns);
		return filterRegBean;
	}	
	
	@Bean(name = "pispConsentAuthorizationHelper")
	public ConsentAuthorizationHelper pispConsentAuthorizationHelper(){
		return new PispConsentAuthorizationHelper();
	}
	
}
