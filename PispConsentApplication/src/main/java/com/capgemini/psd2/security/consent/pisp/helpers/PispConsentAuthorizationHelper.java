
package com.capgemini.psd2.security.consent.pisp.helpers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;

import com.capgemini.psd2.aisp.domain.OBAccount6;
import com.capgemini.psd2.aisp.domain.OBAccount6Account;
import com.capgemini.psd2.consent.domain.PSD2Account;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.scaconsenthelper.config.helpers.ConsentAuthorizationHelper;

@ConfigurationProperties("app")
public class PispConsentAuthorizationHelper extends ConsentAuthorizationHelper {

	@Autowired
	private RequestHeaderAttributes requestHeaderAttributes;
	
	private Map<String, String> consentSupportedSchemeMap = new HashMap<>();

	public Map<String, String> getConsentSupportedSchemeMap() {
		return consentSupportedSchemeMap;
	}

	@Override
	public PSD2Account populateAccountwithUnmaskedValues(PSD2Account accountwithMask, PSD2Account accountwithoutMask) {

		Map<String, String> additionalInfo = accountwithoutMask.getAdditionalInformation();
		Map<String, String> additionalInfotobeUnmasked = accountwithMask.getAdditionalInformation();

		OBAccount6Account maskAccount = accountwithMask.getAccount().get(0);
		if (accountwithoutMask.getAccount() != null) {
			String dbSchemeName = consentSupportedSchemeMap.get(maskAccount.getSchemeName());
			String schemeIdentification = accountwithoutMask.getAdditionalInformation()
					.get(consentSupportedSchemeMap.get(dbSchemeName));

			maskAccount.setIdentification(schemeIdentification);
		}

		if(additionalInfo != null && requestHeaderAttributes.isBypassConsentPage()){
			additionalInfotobeUnmasked = new HashMap<>();
			additionalInfotobeUnmasked.put(PSD2Constants.ACCOUNT_NUMBER,
					additionalInfo.get(PSD2Constants.ACCOUNT_NUMBER));
			additionalInfotobeUnmasked.put(PSD2Constants.ACCOUNT_NSC, additionalInfo.get(PSD2Constants.ACCOUNT_NSC));
			additionalInfotobeUnmasked.put(PSD2Constants.IBAN, additionalInfo.get(PSD2Constants.UK_OBIE_IBAN));
			additionalInfotobeUnmasked.put(PSD2Constants.BIC, additionalInfo.get(PSD2Constants.BICFI));
			accountwithMask.setAdditionalInformation(additionalInfotobeUnmasked);
		
		}else 
		/*
		 * Setting (unmasked) account-number, account-nsc, iban and bicfi from channel
		 * profile additional info
		 */
		if (additionalInfo != null && additionalInfotobeUnmasked != null) {
			additionalInfotobeUnmasked.put(PSD2Constants.ACCOUNT_NUMBER,
					additionalInfo.get(PSD2Constants.ACCOUNT_NUMBER));
			additionalInfotobeUnmasked.put(PSD2Constants.ACCOUNT_NSC, additionalInfo.get(PSD2Constants.ACCOUNT_NSC));
			additionalInfotobeUnmasked.put(PSD2Constants.IBAN, additionalInfo.get(PSD2Constants.IBAN));
			additionalInfotobeUnmasked.put(PSD2Constants.BIC, additionalInfo.get(PSD2Constants.BICFI));
		}
		accountwithMask.getAccount().get(0).setIdentification(accountwithoutMask.getAccount().get(0).getIdentification());
		return accountwithMask;
	}

	@Override
	public <T> List<OBAccount6> populateAccountListFromAccountDetails(T input) {
		return null;
	}
}
