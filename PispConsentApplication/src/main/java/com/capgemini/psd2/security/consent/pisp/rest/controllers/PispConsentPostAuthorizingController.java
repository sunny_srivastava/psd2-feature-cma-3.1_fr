/*************************************************************************
 * 
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 * 
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 */
package com.capgemini.psd2.security.consent.pisp.rest.controllers;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.psd2.aisp.domain.OBReadAccount6;
import com.capgemini.psd2.consent.domain.DebtorDetailsResponse;
import com.capgemini.psd2.consent.domain.PSD2Account;
import com.capgemini.psd2.consent.domain.PSD2AccountsAdditionalInfo;
import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.payment.setup.platform.adapter.PaymentSetupPlatformAdapter;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.scaconsenthelper.config.helpers.ConsentAuthorizationHelper;
import com.capgemini.psd2.scaconsenthelper.config.helpers.PispConsentCreationDataHelper;
import com.capgemini.psd2.scaconsenthelper.config.helpers.SCAConsentHelper;
import com.capgemini.psd2.scaconsenthelper.models.IntentTypeEnum;
import com.capgemini.psd2.scaconsenthelper.models.PickupDataModel;


/**
 * ConsentView Controller
 * 
 * @author Capgemini
 *
 */
@RestController
@ConfigurationProperties(prefix = "app")
public class PispConsentPostAuthorizingController {

	@Value("${spring.application.name}")
	private String applicationName;

	@Value("${app.fraudSystem.resUrl}")
	private String resUrl;

	@Value("${app.fraudSystem.hdmUrl}")
	private String hdmUrl;

	@Value("${app.fraudSystem.hdmInputName}")
	private String hdmInputName;

	@Value("${app.fraudSystem.jsc-file-path}")
	private String jscFilePath;
	
	@Value("${app.postAuthorizingFlowEnabled:true}")
	private boolean isPostAuthorisingPartyFlowEnabled;

	@Autowired
	private PispConsentCreationDataHelper consentCreationDataHelper;
	
	@Autowired
	@Qualifier("pispConsentAuthorizationHelper")
	private ConsentAuthorizationHelper consentAuthorizationHelper;

	@Autowired
	private HttpServletRequest request;

	@Autowired
	private RequestHeaderAttributes requestHeaderAttributes;
	
	@Autowired
	private PaymentSetupPlatformAdapter paymentSetupPlatformAdapter;

	@Value("${cdn.baseURL}")
	private String cdnBaseURL;

	private Map<String, String> consentSupportedSchemeMap = new HashMap<>();

	public Map<String, String> getConsentSupportedSchemeMap() {
		return consentSupportedSchemeMap;
	}

	@RequestMapping(value = "/pisp/postAuthorizingParty",method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_UTF8_VALUE }, produces = { MediaType.APPLICATION_JSON_UTF8_VALUE })
	public DebtorDetailsResponse postAuthorisingParty( @RequestBody PSD2AccountsAdditionalInfo accountAdditionalInfo){
		
		PSD2Account customerAccount = null;
		
		if (accountAdditionalInfo != null && !(accountAdditionalInfo.getAccountdetails().isEmpty())) {
			customerAccount = accountAdditionalInfo.getAccountdetails().get(0);
		}
		
		if (customerAccount == null) {
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.NO_ACCOUNTS_SELECTED_BY_CUSTOMER);
		}
		
		
		PickupDataModel pickupDataModel = SCAConsentHelper.populatePickupDataModel(request);
		CustomPaymentStageIdentifiers stageIdentifiers = paymentSetupPlatformAdapter.populateStageIdentifiers(pickupDataModel.getIntentId());
		String paymentType = stageIdentifiers.getPaymentTypeEnum().getPaymentType();
		String cmaVersion = stageIdentifiers.getPaymentSetupVersion();
		
		OBReadAccount6 oBReadAccount2 = consentCreationDataHelper.retrieveCustomerAccountListInfo(
				pickupDataModel.getUserId(), pickupDataModel.getClientId(),
				IntentTypeEnum.PISP_INTENT_TYPE.getIntentType(), requestHeaderAttributes.getCorrelationId(),
				pickupDataModel.getChannelId(), customerAccount.getAccount().get(0).getSchemeName(),
				requestHeaderAttributes.getTenantId(), pickupDataModel.getIntentId(), cmaVersion);

		PSD2Account unmaskedAccount = (PSD2Account) consentAuthorizationHelper.matchAccountFromList(oBReadAccount2,
				customerAccount);
						
		if(consentCreationDataHelper.isPostAuthorizingSupported(paymentType)){
			DebtorDetailsResponse debtorDetailsResponse = consentCreationDataHelper.postAuthorisingPartyDetails(pickupDataModel,unmaskedAccount,stageIdentifiers);
			return debtorDetailsResponse;
		}	
		else
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.PAYMENT_TYPE_NOT_ALLOWED);
	}
	
}
