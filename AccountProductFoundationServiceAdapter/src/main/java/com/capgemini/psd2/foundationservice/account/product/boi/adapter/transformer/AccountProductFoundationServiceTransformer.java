/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.foundationservice.account.product.boi.adapter.transformer;

import java.util.ArrayList;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;


import com.capgemini.psd2.aisp.domain.OBReadProduct2;
import com.capgemini.psd2.aisp.domain.OBReadProduct2Data;
import com.capgemini.psd2.aisp.domain.OBReadProduct2DataProduct;
import com.capgemini.psd2.aisp.domain.OBReadProduct2DataProduct.ProductTypeEnum;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountProductsResponse;
import com.capgemini.psd2.aisp.transformer.AccountProductsTransformer;
import com.capgemini.psd2.foundationservice.account.product.boi.adapter.constants.AccountProductFoundationServiceConstants;
import com.capgemini.psd2.utilities.NullCheckUtils;
import com.capgemini.psd2.validator.PSD2Validator;

/**
 * The Class AccountProductFoundationServiceTransformer.
 */
@Component
@EnableConfigurationProperties
@ConfigurationProperties(prefix = "foundationService")
@Configuration
@EnableAutoConfiguration
public class AccountProductFoundationServiceTransformer implements AccountProductsTransformer {

	@Autowired
	private PSD2Validator validater;

	@Override
	public <T> PlatformAccountProductsResponse transformAccountProducts(T source, Map<String, String> params) {

		PlatformAccountProductsResponse platformAccountProductsResponse = new PlatformAccountProductsResponse();
		OBReadProduct2Data oBReadProduct2Data = new OBReadProduct2Data();
		com.capgemini.psd2.foundationservice.account.product.boi.adapter.raml.domain.Account product = (com.capgemini.psd2.foundationservice.account.product.boi.adapter.raml.domain.Account) source;
		OBReadProduct2 productGETResponse = new OBReadProduct2();
		if (product.getProduct() == null) {
			oBReadProduct2Data.setProduct(new ArrayList<>());
			productGETResponse.setData(oBReadProduct2Data);
			platformAccountProductsResponse.setoBReadProduct2(productGETResponse);
			return platformAccountProductsResponse;
		}
		OBReadProduct2DataProduct response = new OBReadProduct2DataProduct();
		response.setAccountId(params.get(AccountProductFoundationServiceConstants.ACCOUNT_ID));
		if (!NullCheckUtils.isNullOrEmpty(product.getProduct().getProductCode())) {
			response.setProductId(product.getProduct().getProductCode());
		}
		if (!NullCheckUtils.isNullOrEmpty(product.getProduct().getProductName())) {
			response.setProductName(product.getProduct().getProductName());
		}
		if (product.getProduct().getProductDescription().contentEquals("BCA")) {
			response.setProductType(OBReadProduct2DataProduct.ProductTypeEnum.BUSINESSCURRENTACCOUNT);
		} else if (product.getProduct().getProductDescription().contentEquals("PCA")) {
			response.setProductType(OBReadProduct2DataProduct.ProductTypeEnum.PERSONALCURRENTACCOUNT);
		}
		validater.validate(response);
		oBReadProduct2Data.addProductItem(response);
		validater.validate(oBReadProduct2Data);
		productGETResponse.setData(oBReadProduct2Data);
		platformAccountProductsResponse.setoBReadProduct2(productGETResponse);
		return platformAccountProductsResponse;

	}

}