package com.capgemini.psd2.domestic.standing.order.mock.foundationservice.service.impl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.capgemini.psd2.domestic.standing.order.mock.foundationservice.constants.DomesticStandingOrderFoundationConstants;
import com.capgemini.psd2.domestic.standing.order.mock.foundationservice.exception.handler.RecordNotFoundException;
import com.capgemini.psd2.domestic.standing.order.mock.foundationservice.raml.domain.PaymentInstructionStatusCode3;
import com.capgemini.psd2.domestic.standing.order.mock.foundationservice.raml.domain.StandingOrderInstruction;
import com.capgemini.psd2.domestic.standing.order.mock.foundationservice.raml.domain.StandingOrderInstructionComposite;
import com.capgemini.psd2.domestic.standing.order.mock.foundationservice.raml.domain.StandingOrderInstructionProposal;
import com.capgemini.psd2.domestic.standing.order.mock.foundationservice.repository.DomesticStandingOrderFoundationRepository;
import com.capgemini.psd2.domestic.standing.order.mock.foundationservice.service.DomesticStandingOrderService;


@Service
public class DomesticStandingOrderFoundationServiceImpl implements DomesticStandingOrderService {

	@Autowired
	private DomesticStandingOrderFoundationRepository repository;
	
	@Override
	public StandingOrderInstructionComposite retrieveAccountInformation(String paymentInstructionNumber)
			throws Exception {

		StandingOrderInstructionComposite standingOrderInstructionComposite = repository
				.findByStandingOrderInstructionPaymentInstructionNumber(paymentInstructionNumber);

		if (null == standingOrderInstructionComposite) {
			throw new RecordNotFoundException(DomesticStandingOrderFoundationConstants.RECORD_NOT_FOUND);
		}

		return standingOrderInstructionComposite;
	}

	@Override
	public StandingOrderInstructionComposite createDomesticStandingOrdersConsentsResource(
			StandingOrderInstructionProposal standingOrderInstructionProposalReq) throws Exception {
		StandingOrderInstructionComposite standingOrderInstructionComposite=new StandingOrderInstructionComposite();
		StandingOrderInstruction standingOrderInstructionReq=new StandingOrderInstruction();
		String submissionID=null;
		if (null == standingOrderInstructionProposalReq) {

			throw new RecordNotFoundException(DomesticStandingOrderFoundationConstants.RECORD_NOT_FOUND);
			
		}
		
		submissionID = UUID.randomUUID().toString();
//		String[] check=standingOrderInstructionProposalCompositeReq.getStandingOrderInstructionProposal().getReference().split(":");
//		if (check.length > 1 && check[1].equalsIgnoreCase("REJECTED")) {
//			standingOrderInstructionProposalReq.setPaymentInstructionStatusCode(PaymentInstructionStatusCode3.INITIATIONFAILED);
//		} else {
		standingOrderInstructionReq.setPaymentInstructionStatusCode(PaymentInstructionStatusCode3.INITIATIONPENDING);
	//	}
		
		standingOrderInstructionReq.setPaymentInstructionNumber(submissionID);
		
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
		standingOrderInstructionReq.setInstructionIssueDate(dateFormat.format(new Date()));
		standingOrderInstructionReq.setInstructionStatusUpdateDateTime(dateFormat.format(new Date()));
		//standingOrderInstructionProposalCompositeReq.setPaymentInstruction(standingOrderInstructionProposalReq);
		standingOrderInstructionComposite.setStandingOrderInstruction(standingOrderInstructionReq);
		standingOrderInstructionComposite.setStandingOrderInstructionProposal(standingOrderInstructionProposalReq);
		repository.save(standingOrderInstructionComposite);
		
		return standingOrderInstructionComposite;
	}

}
