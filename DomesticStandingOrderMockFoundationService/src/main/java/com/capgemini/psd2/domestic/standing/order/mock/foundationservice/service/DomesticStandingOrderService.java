package com.capgemini.psd2.domestic.standing.order.mock.foundationservice.service;

import com.capgemini.psd2.domestic.standing.order.mock.foundationservice.raml.domain.StandingOrderInstructionComposite;
import com.capgemini.psd2.domestic.standing.order.mock.foundationservice.raml.domain.StandingOrderInstructionProposal;

public interface DomesticStandingOrderService {
 
	public StandingOrderInstructionComposite retrieveAccountInformation(String paymentInstructionlId) throws Exception;

	public StandingOrderInstructionComposite createDomesticStandingOrdersConsentsResource(StandingOrderInstructionProposal standingOrderInstructionProposalCompositeReq) throws Exception;
	
}
