package com.capgemini.psd2.domestic.standing.order.mock.foundationservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.psd2.domestic.standing.order.mock.foundationservice.exception.handler.InvalidParameterRequestException;
import com.capgemini.psd2.domestic.standing.order.mock.foundationservice.exception.handler.MissingAuthenticationHeaderException;
import com.capgemini.psd2.domestic.standing.order.mock.foundationservice.exception.handler.RecordNotFoundException;
import com.capgemini.psd2.domestic.standing.order.mock.foundationservice.raml.domain.StandingOrderInstructionComposite;
import com.capgemini.psd2.domestic.standing.order.mock.foundationservice.raml.domain.StandingOrderInstructionProposal;
import com.capgemini.psd2.domestic.standing.order.mock.foundationservice.service.DomesticStandingOrderService;
import com.capgemini.psd2.foundationservice.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.foundationservice.exceptions.MockFoundationServiceException;
import com.capgemini.psd2.foundationservice.utilities.MockServiceUtility;
import com.capgemini.psd2.foundationservice.validator.ValidationUtility;

@RestController
@RequestMapping("/group-payments/p/payments-service")
public class DomesticStandingOrderFoundationController {

	@Autowired
	private DomesticStandingOrderService domesticStandingOrdersConsentsService;

	@Autowired
	private ValidationUtility validationUtility;
	
	@Autowired
	private MockServiceUtility mockServiceUtility;
	
	private String field = null;

	@RequestMapping(value = "/v{version}/domestic/standing-orders/payment-instructions/{payment-instruction-id}", method = RequestMethod.GET, produces = {
			MediaType.APPLICATION_JSON_UTF8_VALUE })
	@ResponseBody
	public StandingOrderInstructionComposite domesticPaymentConsentGet(
			@PathVariable("payment-instruction-id") String paymentInstructionlId,
			@RequestHeader(required = false, value = "x-api-channel-code") String channelcode,
			@RequestHeader(required = false, value = "x-api-transaction-id") String transactionId,
			@RequestHeader(required = false, value = "x-api-source-system") String sourceSystemReqHeader,
			@RequestHeader(required = false, value = "x-api-source-user") String sourceUserReqHeader,
			@RequestHeader(required = false, value = "x-api-party-source-id-number") String partysourceReqHeader,
			@RequestHeader(required = false, value = "x-api-correlation-id") String correlationId) throws Exception {

		if (paymentInstructionlId == null) {

			throw new InvalidParameterRequestException("Bad request");
		}
		validationUtility.validateErrorCode(transactionId);
		return domesticStandingOrdersConsentsService.retrieveAccountInformation(paymentInstructionlId);

	}

	@RequestMapping(value = "/v{version}/domestic/standing-orders/payment-instructions", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_UTF8_VALUE })
	@ResponseBody
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<StandingOrderInstructionComposite> domesticPaymentConsentPost(
			@RequestBody StandingOrderInstructionProposal standingOrderInstructionCompositeReq,
			@RequestHeader(required = false, value = "x-api-channel-code") String channelcode,
			@RequestHeader(required = false, value = "x-api-transaction-id") String transactionId,
			@RequestHeader(required = false, value = "x-api-source-system") String sourceSystemReqHeader,
			@RequestHeader(required = false, value = "x-api-source-user") String sourceUserReqHeader,
			@RequestHeader(required = false, value = "x-api-party-source-id-number") String partysourceReqHeader,
			@RequestHeader(required = false, value = "x-api-correlation-id") String correlationId,
	 		@RequestHeader(required = false, value = "X-API-SOURCE-SYSTEM") String sourcesystem, 
	 		@RequestHeader(required = false, value = "X-API-CHANNEL-BRAND") String channelBrand) throws Exception {
		if(sourcesystem == null  || channelBrand == null) {
			throw new MissingAuthenticationHeaderException("Header Missing");
		}
		
		if (null == standingOrderInstructionCompositeReq) {
			throw MockFoundationServiceException
					.populateMockFoundationServiceException(ErrorCodeEnum.RESOURCE_NOT_FOUND_PPD_PIP);
		}
		if(standingOrderInstructionCompositeReq.getReference()!=null && !standingOrderInstructionCompositeReq.getReference().isEmpty())
		{
			if(standingOrderInstructionCompositeReq.getReference().equalsIgnoreCase("601")||(standingOrderInstructionCompositeReq.getReference().equalsIgnoreCase("629")))
			validationUtility.validateExecutionErrorCodeForMuleOutOfBoxPolicy(standingOrderInstructionCompositeReq.getReference());
		}
		if(transactionId!=null) {
			String transId=transactionId.substring(transactionId.length()-4);
			field=mockServiceUtility.getErrorField().get(transId);
			
		}
		if(standingOrderInstructionCompositeReq.getReference()!=null && !standingOrderInstructionCompositeReq.getReference().isEmpty())
		validationUtility.validateMockBusinessValidationsForDSOSubmission(standingOrderInstructionCompositeReq.getReference(),field);
		StandingOrderInstructionComposite standingOrderInstructionProposalCompositeResponse = null;
		try {
			standingOrderInstructionProposalCompositeResponse = domesticStandingOrdersConsentsService
					.createDomesticStandingOrdersConsentsResource(standingOrderInstructionCompositeReq);
			
            
		} catch (RecordNotFoundException e) {
			e.printStackTrace();
		}

		if (null == standingOrderInstructionProposalCompositeResponse) {

			throw MockFoundationServiceException
					.populateMockFoundationServiceException(ErrorCodeEnum.RESOURCE_NOT_FOUND_PPD_PIP);
		}

		
		return new ResponseEntity<>(standingOrderInstructionProposalCompositeResponse, HttpStatus.CREATED);

	}

}
