/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.foundationservice.account.balance.boi.adapter.test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.anyObject;

import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.aisp.domain.OBBalanceType1Code;
import com.capgemini.psd2.aisp.domain.OBReadBalance1;
import com.capgemini.psd2.aisp.domain.OBReadBalance1Data;
import com.capgemini.psd2.aisp.domain.OBReadBalance1DataAmount;
import com.capgemini.psd2.aisp.domain.OBReadBalance1DataBalance;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountBalanceResponse;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.foundationservice.account.balance.boi.adapter.client.AccountBalanceFoundationServiceClient;
import com.capgemini.psd2.foundationservice.account.balance.boi.adapter.constants.AccountBalanceFoundationServiceConstants;
import com.capgemini.psd2.foundationservice.account.balance.boi.adapter.delegate.AccountBalanceFoundationServiceDelegate;
import com.capgemini.psd2.foundationservice.account.balance.boi.adapter.raml.domain.Balance;
import com.capgemini.psd2.foundationservice.account.balance.boi.adapter.raml.domain.BalanceAmount;
import com.capgemini.psd2.foundationservice.account.balance.boi.adapter.raml.domain.BalanceType;
import com.capgemini.psd2.foundationservice.account.balance.boi.adapter.raml.domain.Balanceresponse;
import com.capgemini.psd2.foundationservice.account.balance.boi.adapter.raml.domain.CreditcardsBalanceresponse;
import com.capgemini.psd2.foundationservice.account.balance.boi.adapter.raml.domain.Currency;
import com.capgemini.psd2.foundationservice.account.balance.boi.adapter.transformer.AccountBalanceFoundationServiceTransformer;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.impl.RestClientSyncImpl;

@RunWith(SpringJUnit4ClassRunner.class)
public class AccountBalanceFoundationServiceDelegateRestCallTest {

	@InjectMocks
	private AccountBalanceFoundationServiceDelegate delegate;

	@InjectMocks
	private AccountBalanceFoundationServiceClient accountBalanceFoundationServiceClient;

	@Mock
	private AccountBalanceFoundationServiceTransformer accountBalanceFSTransformer;
	@Mock
	private RestClientSyncImpl restClient;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void contextLoads() {
	}

	@Test
	public void testGetFoundationServiceWithInvalidURL() {

		String accountNSC = "nsc";
		String accountNumber = "number";
		String baseURL = "http://localhost:8081//psd2-abt-service/services/abt";
		String finalURL = "http://localhost:8081//psd2-abt-service/services/abt/accounts/nsc/number";

		String testURL = delegate.getFoundationServiceURL(accountNSC, accountNumber, baseURL, finalURL);

		assertNotEquals("Test for building URL failed.", finalURL, testURL);
		assertNotNull(testURL);
	}

	@Test(expected = AdapterException.class)
	public void testGetFoundationServiceWithAccountNSCAsNull() {

		String accountNSC = null;
		String accountNumber = "number";
		String baseURL = "http://localhost:8081//psd2-abt-service/services/abt";
		String version = "167236";
		delegate.getFoundationServiceURL(version, accountNSC, accountNumber, baseURL);

		// fail("Invalid account NSC");
	}

	@Test(expected = AdapterException.class)
	public void testGetFoundationServiceWithAccountNumberAsNull() {

		String accountNSC = "nsc";
		String accountNumber = null;
		String baseURL = "http://localhost:8081//psd2-abt-service/services/abt";
		String version = "167236";
		delegate.getFoundationServiceURL(version, accountNSC, accountNumber, baseURL);

		// fail("Invalid Account Number");
	}

	@Test(expected = AdapterException.class)
	public void testGetFoundationServiceWithBaseURLAsNull() {

		String accountNSC = "nsc";
		String accountNumber = "number";
		String baseURL = null;
		String version = "167236";
		delegate.getFoundationServiceURL(version, accountNSC, accountNumber, baseURL);

		// fail("Invalid base URL");
	}

	@Test
	public void testTransformResponseFromFDToAPI1() {
		OBReadBalance1Data obReadBalance1Data = new OBReadBalance1Data();
		OBReadBalance1 obReadBalance1 = new OBReadBalance1();
		List<OBReadBalance1DataBalance> balanceList = new ArrayList<>();
		obReadBalance1Data.setBalance(balanceList);
		Map<String, String> params = new HashMap<>();
		params.put(AccountBalanceFoundationServiceConstants.ACCOUNT_SUBTYPE, "CurrentAccount");
		params.put("accountId", "12345");
		Balanceresponse balanceResponse = new Balanceresponse();
		List<Balance> balancesList = new ArrayList<Balance>();
		Balance balanceBaseType = new Balance();
		BalanceAmount balanceAmount = new BalanceAmount();
		Currency balanceCurrency = new Currency();
		OffsetDateTime balanceDate = OffsetDateTime.now();
		
		balanceAmount.setGroupReportingCurrency(700d);
		balanceAmount.setLocalReportingCurrency(900d);
		balanceAmount.setTransactionCurrency(801.77d);
		
		balanceCurrency.setCurrencyName("Euro");
		balanceCurrency.setIsoAlphaCode("EUR");
		
		balanceBaseType.setBalanceAmount(balanceAmount);
		balanceBaseType.setBalanceCurrency(balanceCurrency);
		balanceBaseType.setBalanceDate(balanceDate);
		balanceBaseType.setBalanceType(BalanceType.SHADOW_BALANCE);
		
		balancesList.add(balanceBaseType);
		
		balanceResponse.setBalancesList(balancesList);

		PlatformAccountBalanceResponse finalGBResponseObj = new PlatformAccountBalanceResponse();
		obReadBalance1.setData(obReadBalance1Data);
		finalGBResponseObj.setoBReadBalance1(obReadBalance1);
		OBReadBalance1DataAmount amount = new OBReadBalance1DataAmount();
		amount.setAmount(new BigDecimal(5000.00d).toString());
		amount.setCurrency("GBP");
		OBReadBalance1DataBalance responseDataObj = new OBReadBalance1DataBalance();
		responseDataObj.setAccountId(params.get("accountId"));
		responseDataObj.type(OBBalanceType1Code.CLOSINGBOOKED);
		responseDataObj.setAmount(amount);
		finalGBResponseObj.getoBReadBalance1().getData().getBalance().add(responseDataObj);
		Mockito.when(accountBalanceFSTransformer.transformAccountBalance(anyObject(), anyObject()))
				.thenReturn(finalGBResponseObj);
		delegate.transformResponseFromFDToAPI(balanceResponse, params);
		// assertNotNull(balancesGETResponse);
	}

	@Test
	public void testTransformResponseFromFDToAPI2() {
		OBReadBalance1Data obReadBalance1Data = new OBReadBalance1Data();
		OBReadBalance1 obReadBalance1 = new OBReadBalance1();
		List<OBReadBalance1DataBalance> balanceList = new ArrayList<>();
		obReadBalance1Data.setBalance(balanceList);
		Map<String, String> params = new HashMap<>();
		params.put(AccountBalanceFoundationServiceConstants.ACCOUNT_SUBTYPE, "Savings");
		params.put("accountId", "12345");
		Balanceresponse balanceResponse = new Balanceresponse();
		List<Balance> balancesList = new ArrayList<Balance>();
		Balance balanceBaseType = new Balance();
		BalanceAmount balanceAmount = new BalanceAmount();
		Currency balanceCurrency = new Currency();
		OffsetDateTime balanceDate = OffsetDateTime.now();
		
		balanceAmount.setGroupReportingCurrency(700d);
		balanceAmount.setLocalReportingCurrency(900d);
		balanceAmount.setTransactionCurrency(801.77d);
		
		balanceCurrency.setCurrencyName("Euro");
		balanceCurrency.setIsoAlphaCode("EUR");
		
		balanceBaseType.setBalanceAmount(balanceAmount);
		balanceBaseType.setBalanceCurrency(balanceCurrency);
		balanceBaseType.setBalanceDate(balanceDate);
		balanceBaseType.setBalanceType(BalanceType.SHADOW_BALANCE);
		
		balancesList.add(balanceBaseType);
		
		balanceResponse.setBalancesList(balancesList);

		PlatformAccountBalanceResponse finalGBResponseObj = new PlatformAccountBalanceResponse();
		obReadBalance1.setData(obReadBalance1Data);
		finalGBResponseObj.setoBReadBalance1(obReadBalance1);
		OBReadBalance1DataAmount amount = new OBReadBalance1DataAmount();
		amount.setAmount(new BigDecimal(5000.00d).toString());
		amount.setCurrency("GBP");
		OBReadBalance1DataBalance responseDataObj = new OBReadBalance1DataBalance();
		responseDataObj.setAccountId(params.get("accountId"));
		responseDataObj.type(OBBalanceType1Code.CLOSINGBOOKED);
		responseDataObj.setAmount(amount);
		finalGBResponseObj.getoBReadBalance1().getData().getBalance().add(responseDataObj);
		Mockito.when(accountBalanceFSTransformer.transformAccountBalance(anyObject(), anyObject()))
				.thenReturn(finalGBResponseObj);
		delegate.transformResponseFromFDToAPI(balanceResponse, params);
		// assertNotNull(balancesGETResponse);
	}

	@Test
	public void testTransformResponseFromFDToAPI3() {
		OBReadBalance1Data obReadBalance1Data = new OBReadBalance1Data();
		OBReadBalance1 obReadBalance1 = new OBReadBalance1();
		List<OBReadBalance1DataBalance> balanceList = new ArrayList<>();
		obReadBalance1Data.setBalance(balanceList);
		Map<String, String> params = new HashMap<>();
		params.put(AccountBalanceFoundationServiceConstants.ACCOUNT_SUBTYPE, "CreditCard");
		params.put("accountId", "12345");
		CreditcardsBalanceresponse balanceResponse = new CreditcardsBalanceresponse();
		List<Balance> balancesList = new ArrayList<Balance>();
		Balance balanceBaseType = new Balance();
		BalanceAmount balanceAmount = new BalanceAmount();
		Currency balanceCurrency = new Currency();
		OffsetDateTime balanceDate = OffsetDateTime.now();
		
		balanceAmount.setGroupReportingCurrency(700d);
		balanceAmount.setLocalReportingCurrency(900d);
		balanceAmount.setTransactionCurrency(801.77d);
		
		balanceCurrency.setCurrencyName("Euro");
		balanceCurrency.setIsoAlphaCode("EUR");
		
		balanceBaseType.setBalanceAmount(balanceAmount);
		balanceBaseType.setBalanceCurrency(balanceCurrency);
		balanceBaseType.setBalanceDate(balanceDate);
		balanceBaseType.setBalanceType(BalanceType.LEDGER_BALANCE);
		
		balancesList.add(balanceBaseType);
		
		balanceResponse.setBalancesList(balancesList);

		PlatformAccountBalanceResponse finalGBResponseObj = new PlatformAccountBalanceResponse();
		obReadBalance1.setData(obReadBalance1Data);
		finalGBResponseObj.setoBReadBalance1(obReadBalance1);
		OBReadBalance1DataAmount amount = new OBReadBalance1DataAmount();
		amount.setAmount(new BigDecimal(5000.00d).toString());
		amount.setCurrency("GBP");
		OBReadBalance1DataBalance responseDataObj = new OBReadBalance1DataBalance();
		responseDataObj.setAccountId(params.get("accountId"));
		responseDataObj.type(OBBalanceType1Code.CLOSINGBOOKED);
		responseDataObj.setAmount(amount);
		finalGBResponseObj.getoBReadBalance1().getData().getBalance().add(responseDataObj);
		Mockito.when(accountBalanceFSTransformer.transformAccountBalance(anyObject(), anyObject()))
				.thenReturn(finalGBResponseObj);
		delegate.transformResponseFromFDToAPI(balanceResponse, params);
		// assertNotNull(balancesGETResponse);
	}

	@Test
	public void testCreateRequestHeadersActual() {

		AccountMapping accountMapping = new AccountMapping();
		accountMapping.setPsuId("userId");
		accountMapping.setCorrelationId("correlationId");
		HttpHeaders httpHeaders = new HttpHeaders();
		ReflectionTestUtils.setField(delegate, "transactioReqHeader", "X-BOI-USER");
		ReflectionTestUtils.setField(delegate, "correlationMuleReqHeader", "X-BOI-CHANNEL");
		ReflectionTestUtils.setField(delegate, "sourcesystem", "X-BOI-PLATFORM");
		ReflectionTestUtils.setField(delegate, "sourceUserReqHeader", "X-CORRELATION-ID");
		ReflectionTestUtils.setField(delegate, "apiChannelCode", "PSD2API");
		ReflectionTestUtils.setField(delegate, "partySourceNumber", "X-API-PARTY-SOURCE-ID-NUMBER");
		Map<String, String> params = new HashMap<>();
		params.put("channelId", "channel123");
		httpHeaders = delegate.createAccountRequestHeaders(new RequestInfo(), accountMapping, params);

		assertNotNull(httpHeaders);
	}

	@Test
	public void testCreateCreditCardRequestHeadersActual() {

		AccountMapping accountMapping = new AccountMapping();
		accountMapping.setPsuId("userId");
		accountMapping.setCorrelationId("correlationId");
		HttpHeaders httpHeaders = new HttpHeaders();
		ReflectionTestUtils.setField(delegate, "maskedPan", "X-BOI-USER");
		ReflectionTestUtils.setField(delegate, "correlationMuleReqHeader", "X-BOI-CHANNEL");
		ReflectionTestUtils.setField(delegate, "sourcesystem", "X-BOI-PLATFORM");
		ReflectionTestUtils.setField(delegate, "sourceUserReqHeader", "X-CORRELATION-ID");
		ReflectionTestUtils.setField(delegate, "apiChannelCode", "PSD2API");
		ReflectionTestUtils.setField(delegate, "transactioReqHeader", "");
		ReflectionTestUtils.setField(delegate, "partySourceNumber", "X-API-PARTY-SOURCE-ID-NUMBER");
		Map<String, String> params = new HashMap<>();
		params.put("channelId", "channel123");
		httpHeaders = delegate.createCreditcardRequestHeaders(new RequestInfo(), accountMapping, params);

		assertNotNull(httpHeaders);
	}

	@Test
	public void testGetCreditCardFoundationServiceURL() {

		String creditCardNumber = "creditCardNumber";
		String plApplId = "plApplId";
		String endUrl = "creditcardfoundationserviceurl";
		String baseURL = "https://mocksvc-proxy.eu1.anypoint.mulesoft.com/exchange/dd9dfe2a-e2c9-4bb7-8357-3902557f1bf9/account-process-api/1.0.8/core-banking/abt/p/v1.0/accounts";
		String finalUrl = baseURL + "/" + creditCardNumber + "/" + plApplId + "/" + endUrl;
		String testUrl = delegate.getCreditCardFoundationServiceURL(creditCardNumber, plApplId, baseURL, endUrl);
		// assertEquals( finalUrl,testUrl);
		assertNotEquals("Test for building URL failed.", finalUrl, testUrl);
		assertNotNull(testUrl);
	}

	@Test(expected = AdapterException.class)
	public void testGetCreditCardFoundationServiceURL21() {

		String creditCardNumber = "creditCardNumber";
		String plApplId = null;
		String endUrl = "creditcardfoundationserviceurl";
		String baseURL = "https://mocksvc-proxy.eu1.anypoint.mulesoft.com/exchange/dd9dfe2a-e2c9-4bb7-8357-3902557f1bf9/account-process-api/1.0.8/core-banking/abt/p/v1.0/accounts";
		delegate.getCreditCardFoundationServiceURL(creditCardNumber, plApplId, baseURL, endUrl);
		// assertEquals( finalUrl,testUrl);
		fail("Invalid App Id");
	}

	@Test(expected = AdapterException.class)
	public void testGetCreditCardFoundationServiceURL3() {

		String creditCardNumber = "creditCardNumber";
		String plApplId = "plApplId";
		String endUrl = null;
		String baseURL = "https://mocksvc-proxy.eu1.anypoint.mulesoft.com/exchange/dd9dfe2a-e2c9-4bb7-8357-3902557f1bf9/account-process-api/1.0.8/core-banking/abt/p/v1.0/accounts";
		delegate.getCreditCardFoundationServiceURL(creditCardNumber, plApplId, baseURL, endUrl);
		// assertEquals( finalUrl,testUrl);
		fail("Invalid End Url");
	}

	@Test(expected = AdapterException.class)
	public void testGetCreditCardFoundationServiceURL4() {
		String creditCardNumber = "creditCardNumber";
		String plApplId = "plApplId";
		String endUrl = "creditcardfoundationserviceurl";
		String baseURL = null;
		delegate.getCreditCardFoundationServiceURL(creditCardNumber, plApplId, baseURL, endUrl);
		// assertEquals( finalUrl,testUrl);
		fail("Invalid Base Url");
	}

	@Test
	public void testGetCreditCardFoundationServiceURL1() {
		RequestInfo requestInfo = new RequestInfo();
		CreditcardsBalanceresponse balanceBaseType = new CreditcardsBalanceresponse();
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.add("X-BOI-CHANNEL", "BOL");
		httpHeaders.add("X-BOI-PLATFORM", "PSD2API");
		httpHeaders.add("X-CORRELATION-ID", "Correlation");
		httpHeaders.add("X-API-SOURCE-SYSTEM", "PSD2API");
		requestInfo.setUrl(
				"https://mocksvc-proxy.eu1.anypoint.mulesoft.com/exchange/dd9dfe2a-e2c9-4bb7-8357-3902557f1bf9/account-process-api/1.0.8/core-banking/abt/p/v1.0/accounts");
		Mockito.when(restClient.callForGet(anyObject(), anyObject(), anyObject())).thenReturn(balanceBaseType);
		CreditcardsBalanceresponse response = accountBalanceFoundationServiceClient
				.restTransportForCreditCardBalance(requestInfo, CreditcardsBalanceresponse.class, httpHeaders);
		assertThat(response).isEqualTo(balanceBaseType);
	}

	@Test
	public void testGetCreditCardFoundationServiceURL2() {
		RequestInfo requestInfo = new RequestInfo();
		Balanceresponse balanceResponse = new Balanceresponse();
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.add("X-BOI-CHANNEL", "BOL");
		httpHeaders.add("X-BOI-PLATFORM", "PSD2API");
		httpHeaders.add("X-CORRELATION-ID", "Correlation");
		httpHeaders.add("X-API-SOURCE-SYSTEM", "PSD2API");
		requestInfo.setUrl(
				"https://mocksvc-proxy.eu1.anypoint.mulesoft.com/exchange/dd9dfe2a-e2c9-4bb7-8357-3902557f1bf9/account-process-api/1.0.8/core-banking/abt/p/v1.0/accounts");
		Mockito.when(restClient.callForGet(anyObject(), anyObject(), anyObject())).thenReturn(balanceResponse);
		Balanceresponse response = accountBalanceFoundationServiceClient
				.restTransportForSingleAccountBalance(requestInfo, Balanceresponse.class, httpHeaders);
		assertThat(response).isEqualTo(balanceResponse);
	}
}