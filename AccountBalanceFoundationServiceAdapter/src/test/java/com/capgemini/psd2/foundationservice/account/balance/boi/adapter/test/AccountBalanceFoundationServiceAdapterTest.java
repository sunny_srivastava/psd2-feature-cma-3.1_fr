package com.capgemini.psd2.foundationservice.account.balance.boi.adapter.test;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyMap;
import static org.mockito.Matchers.anyObject;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.aisp.domain.OBAccount6Account;
import com.capgemini.psd2.aisp.domain.OBExternalAccountSubType1Code;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountBalanceResponse;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.foundationservice.account.balance.boi.adapter.AccountBalanceFoundationServiceAdapter;
import com.capgemini.psd2.foundationservice.account.balance.boi.adapter.client.AccountBalanceFoundationServiceClient;
import com.capgemini.psd2.foundationservice.account.balance.boi.adapter.constants.AccountBalanceFoundationServiceConstants;
import com.capgemini.psd2.foundationservice.account.balance.boi.adapter.delegate.AccountBalanceFoundationServiceDelegate;
import com.capgemini.psd2.foundationservice.account.balance.boi.adapter.raml.domain.Balance;
import com.capgemini.psd2.foundationservice.account.balance.boi.adapter.raml.domain.BalanceAmount;
import com.capgemini.psd2.foundationservice.account.balance.boi.adapter.raml.domain.BalanceType;
import com.capgemini.psd2.foundationservice.account.balance.boi.adapter.raml.domain.Balanceresponse;
import com.capgemini.psd2.foundationservice.account.balance.boi.adapter.raml.domain.CreditcardsBalanceresponse;
import com.capgemini.psd2.foundationservice.account.balance.boi.adapter.raml.domain.Currency;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.impl.RestClientSyncImpl;

@RunWith(SpringJUnit4ClassRunner.class)
public class AccountBalanceFoundationServiceAdapterTest {

	/** The account balance foundation service adapter. */
	@InjectMocks
	private AccountBalanceFoundationServiceAdapter accountBalanceFoundationServiceAdapter = new AccountBalanceFoundationServiceAdapter();

	/** The account balance foundation service client. */
	@Mock
	private AccountBalanceFoundationServiceClient accountBalanceFoundationServiceClient;

	/** The rest client. */
	@Mock
	private RestClientSyncImpl restClient;

	/** The account balance foundation service delegate. */
	@Mock
	private AccountBalanceFoundationServiceDelegate accountBalanceFoundationServiceDelegate;

	/**
	 * Sets the up.
	 */
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * Context loads.
	 */
	@Test
	public void contextLoads() {
	}

	/**
	 * Test account balance FS.
	 */

	@Test
	public void testAccountBalanceFS1() {
		Balanceresponse balanceResponse = new Balanceresponse();
		List<Balance> balancesList = new ArrayList<Balance>();
		Balance balanceBaseType = new Balance();
		BalanceAmount balanceAmount = new BalanceAmount();
		Currency balanceCurrency = new Currency();
		OffsetDateTime balanceDate = OffsetDateTime.now();
		
		balanceAmount.setGroupReportingCurrency(700d);
		balanceAmount.setLocalReportingCurrency(900d);
		balanceAmount.setTransactionCurrency(801.77d);
		
		balanceCurrency.setCurrencyName("Euro");
		balanceCurrency.setIsoAlphaCode("EUR");
		
		balanceBaseType.setBalanceAmount(balanceAmount);
		balanceBaseType.setBalanceCurrency(balanceCurrency);
		balanceBaseType.setBalanceDate(balanceDate);
		balanceBaseType.setBalanceType(BalanceType.SHADOW_BALANCE);
		
		balancesList.add(balanceBaseType);
		
		balanceResponse.setBalancesList(balancesList);
		
		AccountMapping accountMapping = new AccountMapping();
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
		AccountDetails accDet = new AccountDetails();
		accDet.setAccountSubType(OBExternalAccountSubType1Code.CURRENTACCOUNT);
		accDet.setAccountId("12345");
		accDet.setAccountNSC("nsc1234");
		accDet.setAccountNumber("acct1234");
		accDetList.add(accDet);
		accountMapping.setAccountDetails(accDetList);
		accountMapping.setTppCID("test");
		accountMapping.setCorrelationId("test");
		accountMapping.setPsuId("test");

		Mockito.when(accountBalanceFoundationServiceDelegate.getFoundationServiceURL(any(), any(), any(), any()))
				.thenReturn("http://10.102.19.131:8081/psd2-abt-service/services/abt/accounts/nsc1234/acct1234");
		Mockito.when(accountBalanceFoundationServiceClient.restTransportForSingleAccountBalance(any(), any(), any()))
				.thenReturn(balanceResponse);
		Mockito.when(accountBalanceFoundationServiceDelegate.transformResponseFromFDToAPI(any(), any()))
				.thenReturn(new PlatformAccountBalanceResponse());

		Map<String, String> params = new HashMap<String, String>();
		params.put(AccountBalanceFoundationServiceConstants.ACCOUNT_SUBTYPE, "CurrentAccount");
		accountBalanceFoundationServiceAdapter.retrieveAccountBalance(accountMapping, params);
	}
	
	@Test
	public void testAccountBalanceFS2() {
		Balanceresponse balanceResponse = new Balanceresponse();
		List<Balance> balancesList = new ArrayList<Balance>();
		Balance balanceBaseType = new Balance();
		BalanceAmount balanceAmount = new BalanceAmount();
		Currency balanceCurrency = new Currency();
		OffsetDateTime balanceDate = OffsetDateTime.now();
		
		balanceAmount.setGroupReportingCurrency(700d);
		balanceAmount.setLocalReportingCurrency(900d);
		balanceAmount.setTransactionCurrency(801.77d);
		
		balanceCurrency.setCurrencyName("Euro");
		balanceCurrency.setIsoAlphaCode("EUR");
		
		balanceBaseType.setBalanceAmount(balanceAmount);
		balanceBaseType.setBalanceCurrency(balanceCurrency);
		balanceBaseType.setBalanceDate(balanceDate);
		balanceBaseType.setBalanceType(BalanceType.SHADOW_BALANCE);
		
		balancesList.add(balanceBaseType);
		
		balanceResponse.setBalancesList(balancesList);
		
		AccountMapping accountMapping = new AccountMapping();
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
		AccountDetails accDet = new AccountDetails();
		accDet.setAccountSubType(OBExternalAccountSubType1Code.SAVINGS);
		accDet.setAccountId("12345");
		accDet.setAccountNSC("nsc1234");
		accDet.setAccountNumber("acct1234");
		accDetList.add(accDet);
		accountMapping.setAccountDetails(accDetList);
		accountMapping.setTppCID("test");
		accountMapping.setCorrelationId("test");
		accountMapping.setPsuId("test");

		Mockito.when(accountBalanceFoundationServiceDelegate.getFoundationServiceURL(any(), any(), any(), any()))
				.thenReturn("http://10.102.19.131:8081/psd2-abt-service/services/abt/accounts/nsc1234/acct1234");
		Mockito.when(accountBalanceFoundationServiceClient.restTransportForSingleAccountBalance(any(), any(), any()))
				.thenReturn(balanceResponse);
		Mockito.when(accountBalanceFoundationServiceDelegate.transformResponseFromFDToAPI(any(), any()))
				.thenReturn(new PlatformAccountBalanceResponse());

		Map<String, String> params = new HashMap<String, String>();
		params.put(AccountBalanceFoundationServiceConstants.ACCOUNT_SUBTYPE, "Savings");
		accountBalanceFoundationServiceAdapter.retrieveAccountBalance(accountMapping, params);
	}
	
	@Test
	public void testAccountBalanceFS3() {
		Balanceresponse balanceResponse = new Balanceresponse();
		List<Balance> balancesList = new ArrayList<Balance>();
		Balance balanceBaseType = new Balance();
		BalanceAmount balanceAmount = new BalanceAmount();
		Currency balanceCurrency = new Currency();
		OffsetDateTime balanceDate = OffsetDateTime.now();
		
		balanceAmount.setGroupReportingCurrency(700d);
		balanceAmount.setLocalReportingCurrency(900d);
		balanceAmount.setTransactionCurrency(801.77d);
		
		balanceCurrency.setCurrencyName("Euro");
		balanceCurrency.setIsoAlphaCode("EUR");
		
		balanceBaseType.setBalanceAmount(balanceAmount);
		balanceBaseType.setBalanceCurrency(balanceCurrency);
		balanceBaseType.setBalanceDate(balanceDate);
		balanceBaseType.setBalanceType(BalanceType.SHADOW_BALANCE);
		
		balancesList.add(balanceBaseType);
		
		balanceResponse.setBalancesList(balancesList);
		
		AccountMapping accountMapping = new AccountMapping();
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
		AccountDetails accDet = new AccountDetails();
		accDet.setAccountId("12345");
		accDet.setAccountNSC("nsc1234");
		accDet.setAccountNumber("acct1234");
		accDetList.add(accDet);
		accountMapping.setAccountDetails(accDetList);
		accountMapping.setTppCID("test");
		accountMapping.setCorrelationId("test");
		accountMapping.setPsuId("test");

		Mockito.when(accountBalanceFoundationServiceDelegate.getFoundationServiceURL(any(), any(), any(), any()))
				.thenReturn("http://10.102.19.131:8081/psd2-abt-service/services/abt/accounts/nsc1234/acct1234");
		Mockito.when(accountBalanceFoundationServiceClient.restTransportForSingleAccountBalance(any(), any(), any()))
				.thenReturn(balanceResponse);
		Mockito.when(accountBalanceFoundationServiceDelegate.transformResponseFromFDToAPI(any(), any()))
				.thenReturn(new PlatformAccountBalanceResponse());

		Map<String, String> params = new HashMap<String, String>();
		params.put("cmaVersion", null);
		accountBalanceFoundationServiceAdapter.retrieveAccountBalance(accountMapping, params);
	}
	
	@Test
	public void testAccountBalanceCreditCardFS1() {
		CreditcardsBalanceresponse balanceResponse = new CreditcardsBalanceresponse();
		List<Balance> balancesList = new ArrayList<Balance>();
		Balance balanceBaseType = new Balance();
		BalanceAmount balanceAmount = new BalanceAmount();
		Currency balanceCurrency = new Currency();
		OffsetDateTime balanceDate = OffsetDateTime.now();
		
		balanceAmount.setGroupReportingCurrency(700d);
		balanceAmount.setLocalReportingCurrency(900d);
		balanceAmount.setTransactionCurrency(801.77d);
		
		balanceCurrency.setCurrencyName("Euro");
		balanceCurrency.setIsoAlphaCode("EUR");
		
		balanceBaseType.setBalanceAmount(balanceAmount);
		balanceBaseType.setBalanceCurrency(balanceCurrency);
		balanceBaseType.setBalanceDate(balanceDate);
		balanceBaseType.setBalanceType(BalanceType.LEDGER_BALANCE);
		
		balancesList.add(balanceBaseType);
		
		balanceResponse.setBalancesList(balancesList);
		
		AccountMapping accountMapping = new AccountMapping();
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
		AccountDetails accDet = new AccountDetails();
		OBAccount6Account account = new OBAccount6Account();
		account.setIdentification("1234567");
		accDet.setAccountSubType(OBExternalAccountSubType1Code.CREDITCARD);
		accDet.setAccountId("12345");
		accDet.setAccountNSC("nsc1234");
		accDet.setAccountNumber("acct1234");
		accDet.setAccount(account);
		accDetList.add(accDet);
		accountMapping.setAccountDetails(accDetList);
		accountMapping.setTppCID("test");
		accountMapping.setCorrelationId("test");
		accountMapping.setPsuId("test");

		Mockito.when(accountBalanceFoundationServiceDelegate.getFoundationServiceURL(any(), any(), any(), any()))
				.thenReturn("http://10.102.19.131:8081/psd2-abt-service/services/abt/accounts/nsc1234/acct1234");
		Mockito.when(accountBalanceFoundationServiceClient.restTransportForCreditCardBalance(any(), any(), any()))
				.thenReturn(balanceResponse);
		Mockito.when(accountBalanceFoundationServiceDelegate.transformResponseFromFDToAPI(any(), any()))
				.thenReturn(new PlatformAccountBalanceResponse());

		Map<String, String> params = new HashMap<String, String>();
		params.put(AccountBalanceFoundationServiceConstants.ACCOUNT_SUBTYPE, "CreditCard");
		accountBalanceFoundationServiceAdapter.retrieveAccountBalance(accountMapping, params);
	}
	
	@Test
	public void testAccountBalanceCreditCardFS2() {
		CreditcardsBalanceresponse balanceResponse = new CreditcardsBalanceresponse();
		List<Balance> balancesList = new ArrayList<Balance>();
		Balance balanceBaseType = new Balance();
		BalanceAmount balanceAmount = new BalanceAmount();
		Currency balanceCurrency = new Currency();
		OffsetDateTime balanceDate = OffsetDateTime.now();
		
		balanceAmount.setGroupReportingCurrency(700d);
		balanceAmount.setLocalReportingCurrency(900d);
		balanceAmount.setTransactionCurrency(801.77d);
		
		balanceCurrency.setCurrencyName("Euro");
		balanceCurrency.setIsoAlphaCode("EUR");
		
		balanceBaseType.setBalanceAmount(balanceAmount);
		balanceBaseType.setBalanceCurrency(balanceCurrency);
		balanceBaseType.setBalanceDate(balanceDate);
		balanceBaseType.setBalanceType(BalanceType.AVAILABLE_LIMIT);
		
		balancesList.add(balanceBaseType);
		
		balanceResponse.setBalancesList(balancesList);
		
		AccountMapping accountMapping = new AccountMapping();
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
		AccountDetails accDet = new AccountDetails();
		OBAccount6Account account = new OBAccount6Account();
		account.setIdentification("1234");
		accDet.setAccountSubType(OBExternalAccountSubType1Code.CREDITCARD);
		accDet.setAccountId("12345");
		accDet.setAccountNSC("nsc1234");
		accDet.setAccountNumber("acct1234");
		accDet.setAccount(account);
		accDetList.add(accDet);
		accountMapping.setAccountDetails(accDetList);
		accountMapping.setTppCID("test");
		accountMapping.setCorrelationId("test");
		accountMapping.setPsuId("test");

		Mockito.when(accountBalanceFoundationServiceDelegate.getFoundationServiceURL(any(), any(), any(), any()))
				.thenReturn("http://10.102.19.131:8081/psd2-abt-service/services/abt/accounts/nsc1234/acct1234");
		Mockito.when(accountBalanceFoundationServiceClient.restTransportForCreditCardBalance(any(), any(), any()))
				.thenReturn(balanceResponse);
		Mockito.when(accountBalanceFoundationServiceDelegate.transformResponseFromFDToAPI(any(), any()))
				.thenReturn(new PlatformAccountBalanceResponse());

		Map<String, String> params = new HashMap<String, String>();
		params.put(AccountBalanceFoundationServiceConstants.ACCOUNT_SUBTYPE, "CreditCard");
		accountBalanceFoundationServiceAdapter.retrieveAccountBalance(accountMapping, params);
	}

	@Test
	public void testAccountBalanceCreditCardFS3() {
		CreditcardsBalanceresponse balanceResponse = new CreditcardsBalanceresponse();
		List<Balance> balancesList = new ArrayList<Balance>();
		Balance balanceBaseType = new Balance();
		BalanceAmount balanceAmount = new BalanceAmount();
		Currency balanceCurrency = new Currency();
		OffsetDateTime balanceDate = OffsetDateTime.now();
		
		balanceAmount.setGroupReportingCurrency(700d);
		balanceAmount.setLocalReportingCurrency(900d);
		balanceAmount.setTransactionCurrency(801.77d);
		
		balanceCurrency.setCurrencyName("Euro");
		balanceCurrency.setIsoAlphaCode("EUR");
		
		balanceBaseType.setBalanceAmount(balanceAmount);
		balanceBaseType.setBalanceCurrency(balanceCurrency);
		balanceBaseType.setBalanceDate(balanceDate);
		balanceBaseType.setBalanceType(BalanceType.STATEMENT_CLOSING_BALANCE);
		
		balancesList.add(balanceBaseType);
		
		balanceResponse.setBalancesList(balancesList);
		
		AccountMapping accountMapping = new AccountMapping();
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
		AccountDetails accDet = new AccountDetails();
		OBAccount6Account account = new OBAccount6Account();
		account.setIdentification("12345");
		accDet.setAccountSubType(OBExternalAccountSubType1Code.CREDITCARD);
		accDet.setAccountId("12345");
		accDet.setAccountNSC("nsc1234");
		accDet.setAccountNumber("acct1234");
		accDet.setAccount(account);
		accDetList.add(accDet);
		accountMapping.setAccountDetails(accDetList);
		accountMapping.setTppCID("test");
		accountMapping.setCorrelationId("test");
		accountMapping.setPsuId("test");

		Mockito.when(accountBalanceFoundationServiceDelegate.getFoundationServiceURL(any(), any(), any(), any()))
				.thenReturn("http://10.102.19.131:8081/psd2-abt-service/services/abt/accounts/nsc1234/acct1234");
		Mockito.when(accountBalanceFoundationServiceClient.restTransportForCreditCardBalance(any(), any(), any()))
				.thenReturn(balanceResponse);
		Mockito.when(accountBalanceFoundationServiceDelegate.transformResponseFromFDToAPI(any(), any()))
				.thenReturn(new PlatformAccountBalanceResponse());

		Map<String, String> params = new HashMap<String, String>();
		params.put(AccountBalanceFoundationServiceConstants.ACCOUNT_SUBTYPE, "CreditCard");
		accountBalanceFoundationServiceAdapter.retrieveAccountBalance(accountMapping, params);
	}

	
	@SuppressWarnings("unchecked")
	@Test
	public void testCreateRequestHeaders() {
		AccountMapping accountMapping = new AccountMapping();
		accountMapping.setPsuId("userId");
		accountMapping.setCorrelationId("correlationId");
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.add("X-BOI-USER", "header user");
		httpHeaders.add("X-BOI-CHANNEL", "header channel");
		httpHeaders.add("X-BOI-PLATFORM", "header platform");
		httpHeaders.add("X-CORRELATION-ID", "header correlation Id");

		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl("http://localhost:8081//psd2-abt-service/services/abt/accounts/nsc/number");
		Mockito.when(
				accountBalanceFoundationServiceDelegate.createAccountRequestHeaders(anyObject(), anyObject(), anyMap()))
				.thenReturn(httpHeaders);
		Map<String, String> params = new HashMap<>();
		params.put("channelId", "channel123");
		httpHeaders = accountBalanceFoundationServiceDelegate.createAccountRequestHeaders(requestInfo, accountMapping,
				params);
	}

	@Test(expected = AdapterException.class)
	public void testExceptionFilteredAccounts() {
		AccountMapping accountMapping = new AccountMapping();
		Map<String, String> params = new HashMap<>();
		accountMapping.setPsuId("test");
		accountMapping.setCorrelationId("test");
		accountBalanceFoundationServiceAdapter.retrieveAccountBalance(accountMapping, params);
	}

	@Test(expected = AdapterException.class)
	public void testExceptionNullCheckUtils() {
		AccountMapping accountMapping = new AccountMapping();
		Map<String, String> params = new HashMap<>();
		accountMapping.setPsuId("test");
		accountMapping.setCorrelationId("test");
		Mockito.when(accountBalanceFoundationServiceClient.restTransportForCreditCardBalance(anyObject(), anyObject(),
				anyObject())).thenReturn(null);
		accountBalanceFoundationServiceAdapter.retrieveAccountBalance(accountMapping, params);
	}

	@Test(expected = AdapterException.class)
	public void testExceptionNullParams() {
		AccountMapping accountMapping = new AccountMapping();
		Map<String, String> params = null;
		accountMapping.setPsuId("test");
		accountMapping.setCorrelationId("test");
		Mockito.when(accountBalanceFoundationServiceClient.restTransportForCreditCardBalance(anyObject(), anyObject(),
				anyObject())).thenReturn(null);
		accountBalanceFoundationServiceAdapter.retrieveAccountBalance(accountMapping, params);
	}

	@Test(expected = AdapterException.class)
	public void testExceptionNullCheckUtil() {
		AccountMapping accountMapping = new AccountMapping();
		Map<String, String> params = new HashMap<>();
		accountMapping.setPsuId(null);
		Mockito.when(accountBalanceFoundationServiceClient.restTransportForCreditCardBalance(anyObject(), anyObject(),
				anyObject())).thenReturn(null);
		accountBalanceFoundationServiceAdapter.retrieveAccountBalance(accountMapping, params);
	}

	@Test(expected = AdapterException.class)
	public void testExceptionNullAccountMappingPsu() {
		AccountMapping accountMapping = null;
		Map<String, String> params = new HashMap<>();
		Mockito.when(accountBalanceFoundationServiceClient.restTransportForCreditCardBalance(anyObject(), anyObject(),
				anyObject())).thenReturn(null);
		accountBalanceFoundationServiceAdapter.retrieveAccountBalance(accountMapping, params);
	}

	@Test(expected = AdapterException.class)
	public void testAccountMappingAccountAsNull() {
		AccountMapping accountMapping = new AccountMapping();
		AccountDetails accDet = new AccountDetails();
		accDet.setAccountSubType(OBExternalAccountSubType1Code.CURRENTACCOUNT);
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
		accDet.setAccountId("12345");
		accDet.setAccountNSC("nsc1234");
		accDet.setAccountNumber("acct1234");
		accDetList.add(accDet);
		accountMapping.setAccountDetails(accDetList);
		accountMapping.setTppCID("test");
		accountMapping.setPsuId("test");
		Mockito.when(accountBalanceFoundationServiceClient.restTransportForCreditCardBalance(anyObject(), anyObject(),
				anyObject())).thenReturn(null);
		Map<String, String> params = new HashMap<String, String>();
		params.put(AccountBalanceFoundationServiceConstants.ACCOUNT_SUBTYPE, "CurrentAccount");
		accountBalanceFoundationServiceAdapter.retrieveAccountBalance(accountMapping, params);
	}

	@Test(expected = AdapterException.class)
	public void testAccountMappingElse() {
		AccountMapping accountMapping = new AccountMapping();
		AccountDetails accDet = new AccountDetails();
		accDet.setAccountSubType(OBExternalAccountSubType1Code.CURRENTACCOUNT);
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
		accDet.setAccountId("12345");
		accDet.setAccountNSC("nsc1234");
		accDet.setAccountNumber("acct1234");
		accDetList.add(accDet);
		accountMapping.setAccountDetails(null);
		accountMapping.setTppCID("test");
		accountMapping.setPsuId("test");
		Mockito.when(accountBalanceFoundationServiceClient.restTransportForCreditCardBalance(anyObject(), anyObject(),
				anyObject())).thenReturn(null);
		accountBalanceFoundationServiceAdapter.retrieveAccountBalance(accountMapping, new HashMap<String, String>());
	}

}