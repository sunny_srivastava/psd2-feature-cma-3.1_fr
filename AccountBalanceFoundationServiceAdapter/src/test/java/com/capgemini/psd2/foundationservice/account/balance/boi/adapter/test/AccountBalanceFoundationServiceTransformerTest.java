/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.foundationservice.account.balance.boi.adapter.test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.adapter.datetime.utility.TimeZoneDateTimeAdapter;
import com.capgemini.psd2.aisp.domain.OBReadBalance1;
import com.capgemini.psd2.aisp.domain.OBReadBalance1Data;
import com.capgemini.psd2.aisp.domain.OBReadBalance1DataBalance;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountBalanceResponse;
import com.capgemini.psd2.foundationservice.account.balance.boi.adapter.constants.AccountBalanceFoundationServiceConstants;
import com.capgemini.psd2.foundationservice.account.balance.boi.adapter.raml.domain.Balance;
import com.capgemini.psd2.foundationservice.account.balance.boi.adapter.raml.domain.BalanceAmount;
import com.capgemini.psd2.foundationservice.account.balance.boi.adapter.raml.domain.BalanceType;
import com.capgemini.psd2.foundationservice.account.balance.boi.adapter.raml.domain.Balanceresponse;
import com.capgemini.psd2.foundationservice.account.balance.boi.adapter.raml.domain.CreditcardsBalanceresponse;
import com.capgemini.psd2.foundationservice.account.balance.boi.adapter.raml.domain.Currency;
import com.capgemini.psd2.foundationservice.account.balance.boi.adapter.transformer.AccountBalanceFoundationServiceTransformer;
import com.capgemini.psd2.validator.impl.PSD2ValidatorImpl;

/**
 * The Class AccountBalanceFoundationServiceTransformerTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class AccountBalanceFoundationServiceTransformerTest {

	@Mock
	private PSD2ValidatorImpl psd2Validator;

	/** The account balance FS transformer. */
	@InjectMocks
	private AccountBalanceFoundationServiceTransformer accountBalanceFSTransformer;

	@Mock
	private TimeZoneDateTimeAdapter timeZoneDateTimeAdapter;

	/**
	 * Sets the up.
	 */
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testAccountBalanceFSTransformer1a() {
		Map<String, String> params = new HashMap<>();
		params.put(AccountBalanceFoundationServiceConstants.ACCOUNT_SUBTYPE, "CurrentAccount");

		Balance lobi = new Balance();
		List<Balance> balList = new ArrayList<Balance>();
		balList.add(lobi);

		Balanceresponse balanceResponse = new Balanceresponse();
		balanceResponse.setBalancesList(balList);

		lobi.setBalanceType(BalanceType.LEDGER_BALANCE);
		BalanceAmount babt = new BalanceAmount();
		Double amt = 5000d;
		babt.setLocalReportingCurrency(amt);
		lobi.setBalanceAmount(babt);
		Currency balanceCurrency = new Currency();
		lobi.setBalanceCurrency(balanceCurrency);
		balanceCurrency.setIsoAlphaCode("GBP");
		lobi.setBalanceCurrency(balanceCurrency);

		OBReadBalance1 obReadBalance1 = new OBReadBalance1();
		PlatformAccountBalanceResponse platformAccountBalanceResponse = new PlatformAccountBalanceResponse();
		OBReadBalance1Data obReadBalance1Data = new OBReadBalance1Data();
		List<OBReadBalance1DataBalance> oBCashBalance1 = new ArrayList<>();
		oBCashBalance1.addAll(oBCashBalance1);
		obReadBalance1Data.setBalance(oBCashBalance1);
		obReadBalance1.setData(obReadBalance1Data);
		platformAccountBalanceResponse.setoBReadBalance1(obReadBalance1);

		accountBalanceFSTransformer.transformAccountBalance(balanceResponse, params);
	}

	@Test
	public void testAccountBalanceFSTransformer2a() {
		Map<String, String> params = new HashMap<>();
		params.put(AccountBalanceFoundationServiceConstants.ACCOUNT_SUBTYPE, "Savings");

		Balance lobi = new Balance();
		List<Balance> balList = new ArrayList<Balance>();
		balList.add(lobi);

		Balanceresponse balanceResponse = new Balanceresponse();
		balanceResponse.setBalancesList(balList);

		lobi.setBalanceType(BalanceType.SHADOW_BALANCE);
		BalanceAmount babt = new BalanceAmount();
		Double amt = 0d;
		babt.setLocalReportingCurrency(amt);
		lobi.setBalanceAmount(babt);
		Currency balanceCurrency = new Currency();
		lobi.setBalanceCurrency(balanceCurrency);
		balanceCurrency.setIsoAlphaCode("GBP");
		lobi.setBalanceCurrency(balanceCurrency);

		OBReadBalance1 obReadBalance1 = new OBReadBalance1();
		PlatformAccountBalanceResponse platformAccountBalanceResponse = new PlatformAccountBalanceResponse();
		OBReadBalance1Data obReadBalance1Data = new OBReadBalance1Data();
		List<OBReadBalance1DataBalance> oBCashBalance1 = new ArrayList<>();
		oBCashBalance1.addAll(oBCashBalance1);
		obReadBalance1Data.setBalance(oBCashBalance1);
		obReadBalance1.setData(obReadBalance1Data);
		platformAccountBalanceResponse.setoBReadBalance1(obReadBalance1);

		accountBalanceFSTransformer.transformAccountBalance(balanceResponse, params);
	}

	@Test
	public void testAccountBalanceFSTransformer3a() {
		Map<String, String> params = new HashMap<>();
		params.put(AccountBalanceFoundationServiceConstants.ACCOUNT_SUBTYPE, "");

		Balance lobi = new Balance();
		List<Balance> balList = new ArrayList<Balance>();
		balList.add(lobi);

		Balanceresponse balanceResponse = new Balanceresponse();
		balanceResponse.setBalancesList(balList);

		lobi.setBalanceType(BalanceType.LEDGER_BALANCE);
		BalanceAmount babt = new BalanceAmount();
		Double amt = -5000d;
		babt.setLocalReportingCurrency(amt);
		lobi.setBalanceAmount(babt);
		Currency balanceCurrency = new Currency();
		lobi.setBalanceCurrency(balanceCurrency);
		balanceCurrency.setIsoAlphaCode("GBP");
		lobi.setBalanceCurrency(balanceCurrency);

		OBReadBalance1 obReadBalance1 = new OBReadBalance1();
		PlatformAccountBalanceResponse platformAccountBalanceResponse = new PlatformAccountBalanceResponse();
		OBReadBalance1Data obReadBalance1Data = new OBReadBalance1Data();
		List<OBReadBalance1DataBalance> oBCashBalance1 = new ArrayList<>();
		oBCashBalance1.addAll(oBCashBalance1);
		obReadBalance1Data.setBalance(oBCashBalance1);
		obReadBalance1.setData(obReadBalance1Data);
		platformAccountBalanceResponse.setoBReadBalance1(obReadBalance1);

		accountBalanceFSTransformer.transformAccountBalance(balanceResponse, params);
	}

	@Test
	public void testAccountBalanceFSTransformer1b() {
		Map<String, String> params = new HashMap<>();
		params.put(AccountBalanceFoundationServiceConstants.ACCOUNT_SUBTYPE, "CreditCard");

		Balance lobi = new Balance();
		List<Balance> balList = new ArrayList<Balance>();
		balList.add(lobi);

		CreditcardsBalanceresponse balanceResponse = new CreditcardsBalanceresponse();
		balanceResponse.setBalancesList(balList);

		lobi.setBalanceType(BalanceType.LEDGER_BALANCE);
		BalanceAmount babt = new BalanceAmount();
		Double amt = 5000d;
		babt.setTransactionCurrency(amt);
		lobi.setBalanceAmount(babt);
		Currency balanceCurrency = new Currency();
		lobi.setBalanceCurrency(balanceCurrency);
		balanceCurrency.setIsoAlphaCode("GBP");
		lobi.setBalanceCurrency(balanceCurrency);

		OBReadBalance1 obReadBalance1 = new OBReadBalance1();
		PlatformAccountBalanceResponse platformAccountBalanceResponse = new PlatformAccountBalanceResponse();
		OBReadBalance1Data obReadBalance1Data = new OBReadBalance1Data();
		List<OBReadBalance1DataBalance> oBCashBalance1 = new ArrayList<>();
		oBCashBalance1.addAll(oBCashBalance1);
		obReadBalance1Data.setBalance(oBCashBalance1);
		obReadBalance1.setData(obReadBalance1Data);
		platformAccountBalanceResponse.setoBReadBalance1(obReadBalance1);

		accountBalanceFSTransformer.transformAccountBalance(balanceResponse, params);
	}

	@Test
	public void testAccountBalanceFSTransformer1bn() {
		Map<String, String> params = new HashMap<>();
		params.put(AccountBalanceFoundationServiceConstants.ACCOUNT_SUBTYPE, "CreditCard");

		Balance lobi = new Balance();
		List<Balance> balList = new ArrayList<Balance>();
		balList.add(lobi);

		CreditcardsBalanceresponse balanceResponse = new CreditcardsBalanceresponse();
		balanceResponse.setBalancesList(balList);

		lobi.setBalanceType(BalanceType.LEDGER_BALANCE);
		BalanceAmount babt = new BalanceAmount();
		Double amt = -5000d;
		babt.setTransactionCurrency(amt);
		lobi.setBalanceAmount(babt);
		Currency balanceCurrency = new Currency();
		lobi.setBalanceCurrency(balanceCurrency);
		balanceCurrency.setIsoAlphaCode("GBP");
		lobi.setBalanceCurrency(balanceCurrency);

		OBReadBalance1 obReadBalance1 = new OBReadBalance1();
		PlatformAccountBalanceResponse platformAccountBalanceResponse = new PlatformAccountBalanceResponse();
		OBReadBalance1Data obReadBalance1Data = new OBReadBalance1Data();
		List<OBReadBalance1DataBalance> oBCashBalance1 = new ArrayList<>();
		oBCashBalance1.addAll(oBCashBalance1);
		obReadBalance1Data.setBalance(oBCashBalance1);
		obReadBalance1.setData(obReadBalance1Data);
		platformAccountBalanceResponse.setoBReadBalance1(obReadBalance1);

		accountBalanceFSTransformer.transformAccountBalance(balanceResponse, params);
	}

	@Test
	public void testAccountBalanceFSTransformer2b() {
		Map<String, String> params = new HashMap<>();
		params.put(AccountBalanceFoundationServiceConstants.ACCOUNT_SUBTYPE, "CreditCard");

		Balance lobi = new Balance();
		List<Balance> balList = new ArrayList<Balance>();
		balList.add(lobi);

		CreditcardsBalanceresponse balanceResponse = new CreditcardsBalanceresponse();
		balanceResponse.setBalancesList(balList);

		lobi.setBalanceType(BalanceType.STATEMENT_CLOSING_BALANCE);
		BalanceAmount babt = new BalanceAmount();
		Double amt = 0d;
		babt.setLocalReportingCurrency(amt);
		lobi.setBalanceAmount(babt);
		Currency balanceCurrency = new Currency();
		lobi.setBalanceCurrency(balanceCurrency);
		balanceCurrency.setIsoAlphaCode("GBP");
		lobi.setBalanceCurrency(balanceCurrency);

		OBReadBalance1 obReadBalance1 = new OBReadBalance1();
		PlatformAccountBalanceResponse platformAccountBalanceResponse = new PlatformAccountBalanceResponse();
		OBReadBalance1Data obReadBalance1Data = new OBReadBalance1Data();
		List<OBReadBalance1DataBalance> oBCashBalance1 = new ArrayList<>();
		oBCashBalance1.addAll(oBCashBalance1);
		obReadBalance1Data.setBalance(oBCashBalance1);
		obReadBalance1.setData(obReadBalance1Data);
		platformAccountBalanceResponse.setoBReadBalance1(obReadBalance1);

		accountBalanceFSTransformer.transformAccountBalance(balanceResponse, params);
	}

	@Test
	public void testAccountBalanceFSTransformer2bn() {
		Map<String, String> params = new HashMap<>();
		params.put(AccountBalanceFoundationServiceConstants.ACCOUNT_SUBTYPE, "CreditCard");

		Balance lobi = new Balance();
		List<Balance> balList = new ArrayList<Balance>();
		balList.add(lobi);

		CreditcardsBalanceresponse balanceResponse = new CreditcardsBalanceresponse();
		balanceResponse.setBalancesList(balList);

		lobi.setBalanceType(BalanceType.STATEMENT_CLOSING_BALANCE);
		BalanceAmount babt = new BalanceAmount();
		Double amt = -5000d;
		babt.setLocalReportingCurrency(amt);
		lobi.setBalanceAmount(babt);
		Currency balanceCurrency = new Currency();
		lobi.setBalanceCurrency(balanceCurrency);
		balanceCurrency.setIsoAlphaCode("GBP");
		lobi.setBalanceCurrency(balanceCurrency);

		OBReadBalance1 obReadBalance1 = new OBReadBalance1();
		PlatformAccountBalanceResponse platformAccountBalanceResponse = new PlatformAccountBalanceResponse();
		OBReadBalance1Data obReadBalance1Data = new OBReadBalance1Data();
		List<OBReadBalance1DataBalance> oBCashBalance1 = new ArrayList<>();
		oBCashBalance1.addAll(oBCashBalance1);
		obReadBalance1Data.setBalance(oBCashBalance1);
		obReadBalance1.setData(obReadBalance1Data);
		platformAccountBalanceResponse.setoBReadBalance1(obReadBalance1);

		accountBalanceFSTransformer.transformAccountBalance(balanceResponse, params);
	}

	@Test
	public void testAccountBalanceFSTransformer3b() {
		Map<String, String> params = new HashMap<>();
		params.put(AccountBalanceFoundationServiceConstants.ACCOUNT_SUBTYPE, "CreditCard");

		Balance lobi = new Balance();
		List<Balance> balList = new ArrayList<Balance>();
		balList.add(lobi);

		CreditcardsBalanceresponse balanceResponse = new CreditcardsBalanceresponse();
		balanceResponse.setBalancesList(balList);

		lobi.setBalanceType(BalanceType.AVAILABLE_LIMIT);
		BalanceAmount babt = new BalanceAmount();
		Double amt = -5000d;
		babt.setLocalReportingCurrency(amt);
		lobi.setBalanceAmount(babt);
		Currency balanceCurrency = new Currency();
		lobi.setBalanceCurrency(balanceCurrency);
		balanceCurrency.setIsoAlphaCode("GBP");
		lobi.setBalanceCurrency(balanceCurrency);

		OBReadBalance1 obReadBalance1 = new OBReadBalance1();
		PlatformAccountBalanceResponse platformAccountBalanceResponse = new PlatformAccountBalanceResponse();
		OBReadBalance1Data obReadBalance1Data = new OBReadBalance1Data();
		List<OBReadBalance1DataBalance> oBCashBalance1 = new ArrayList<>();
		oBCashBalance1.addAll(oBCashBalance1);
		obReadBalance1Data.setBalance(oBCashBalance1);
		obReadBalance1.setData(obReadBalance1Data);
		platformAccountBalanceResponse.setoBReadBalance1(obReadBalance1);

		accountBalanceFSTransformer.transformAccountBalance(balanceResponse, params);
	}

	@Test
	public void testAccountBalanceFSTransformer3bg() {
		Map<String, String> params = new HashMap<>();
		params.put(AccountBalanceFoundationServiceConstants.ACCOUNT_SUBTYPE, "CreditCard");

		Balance lobi = new Balance();
		List<Balance> balList = new ArrayList<Balance>();
		balList.add(lobi);

		CreditcardsBalanceresponse balanceResponse = new CreditcardsBalanceresponse();
		balanceResponse.setBalancesList(balList);

		lobi.setBalanceType(BalanceType.AVAILABLE_LIMIT);
		BalanceAmount babt = new BalanceAmount();
		Double amt = 5000d;
		babt.setLocalReportingCurrency(amt);
		lobi.setBalanceAmount(babt);
		Currency balanceCurrency = new Currency();
		lobi.setBalanceCurrency(balanceCurrency);
		balanceCurrency.setIsoAlphaCode("GBP");
		lobi.setBalanceCurrency(balanceCurrency);

		OBReadBalance1 obReadBalance1 = new OBReadBalance1();
		PlatformAccountBalanceResponse platformAccountBalanceResponse = new PlatformAccountBalanceResponse();
		OBReadBalance1Data obReadBalance1Data = new OBReadBalance1Data();
		List<OBReadBalance1DataBalance> oBCashBalance1 = new ArrayList<>();
		oBCashBalance1.addAll(oBCashBalance1);
		obReadBalance1Data.setBalance(oBCashBalance1);
		obReadBalance1.setData(obReadBalance1Data);
		platformAccountBalanceResponse.setoBReadBalance1(obReadBalance1);

		accountBalanceFSTransformer.transformAccountBalance(balanceResponse, params);
	}

}
