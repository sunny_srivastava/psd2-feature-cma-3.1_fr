package com.capgemini.psd2.security.saas.mongo.db.test.adapter;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.anyString;

import java.util.HashMap;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.dao.DataAccessResourceFailureException;

import com.capgemini.psd2.aisp.adapter.LoginDetails;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.security.saas.mongo.db.adapter.SaaSMongoDbAdaptorImpl;
import com.capgemini.psd2.security.saas.mongo.db.adapter.repository.SaaSRepository;
import com.capgemini.psd2.utilities.SandboxConfig;

public class SaaSMongoDbAdaptorImplTest {

	@Mock
	private SaaSRepository repository;

	@Mock
	private SandboxConfig sandboxConfig;

	@InjectMocks
	private SaaSMongoDbAdaptorImpl adapter;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testAuthenticate_SandboxConfigEnabled() {
		LoginDetails loginDetails = new LoginDetails();

		Mockito.when(repository.findByLoginIdAndPassword(anyString(), anyString())).thenReturn(loginDetails);
		Mockito.when(sandboxConfig.isSandboxEnabled()).thenReturn(true);
		Mockito.when(sandboxConfig.getSandboxPassword()).thenReturn("password");

		adapter.authenticate(AuthenticationMockData.getAuthenticationMockData(), new HashMap<String, String>());
		assertNotNull(AuthenticationMockData.getAuthenticationMockData().getPrincipal());
	}

	@Test
	public void testAuthenticate_SandboxConfigDisnabled() {
		LoginDetails loginDetails = new LoginDetails();
		
		Mockito.when(repository.findByLoginIdAndPassword(anyString(), anyString())).thenReturn(loginDetails);
		Mockito.when(sandboxConfig.isSandboxEnabled()).thenReturn(false);
		
		adapter.authenticate(AuthenticationMockData.getAuthenticationMockData(), new HashMap<String,String>());
		assertNotNull(AuthenticationMockData.getAuthenticationMockData().getCredentials());
	}
	
	@Test(expected=PSD2Exception.class)
	public void testAuthenticate_loginDetailsNull() {
		LoginDetails loginDetails = null;

		adapter.authenticate(AuthenticationMockData.getAuthenticationMockData(), new HashMap<String, String>());
		assertNull(loginDetails);
	}
	
	@SuppressWarnings("unchecked")
	@Test(expected=PSD2Exception.class)
	public void testAuthenticate_DataResourceFailureException() {
		LoginDetails loginDetails = new LoginDetails();

		Mockito.when(repository.findByLoginIdAndPassword(anyString(), anyString())).thenThrow(DataAccessResourceFailureException.class);
		Mockito.when(sandboxConfig.isSandboxEnabled()).thenReturn(true);
		Mockito.when(sandboxConfig.getSandboxPassword()).thenReturn("password");

		adapter.authenticate(AuthenticationMockData.getAuthenticationMockData(), new HashMap<String, String>());
		assertNotNull(loginDetails);
	}
}
