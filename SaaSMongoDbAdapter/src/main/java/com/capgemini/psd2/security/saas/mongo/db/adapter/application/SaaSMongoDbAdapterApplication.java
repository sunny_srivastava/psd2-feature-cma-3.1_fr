package com.capgemini.psd2.security.saas.mongo.db.adapter.application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class SaaSMongoDbAdapterApplication {

	/** The context. */
	static ConfigurableApplicationContext context = null;

	public static void main(String[] args) {
		try {
			context = SpringApplication.run(SaaSMongoDbAdapterApplication.class, args);
		} catch (Exception e) {
			if (context != null) {
				SpringApplication.exit(context, () -> 1);
			}
			System.exit(1);
		}
	}
}
