package com.capgemini.psd2.security.saas.test.cancel.controller;

import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.parser.ParseException;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.servlet.ModelAndView;

import com.capgemini.psd2.aisp.adapter.AccountRequestAdapter;
import com.capgemini.psd2.aisp.adapter.AispConsentAdapter;
import com.capgemini.psd2.aisp.domain.OBReadConsentResponse1;
import com.capgemini.psd2.aisp.domain.OBReadConsentResponse1Data;
import com.capgemini.psd2.cisp.adapter.CispConsentAdapter;
import com.capgemini.psd2.cisp.adapter.FundsConfirmationConsentAdapter;
//import com.capgemini.psd2.cisp.domain.OBExternalRequestStatus1Code;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmationConsentResponse1;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmationConsentResponse1Data;
import com.capgemini.psd2.consent.domain.AispConsent;
import com.capgemini.psd2.consent.domain.CispConsent;
import com.capgemini.psd2.enums.ConsentStatusEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.LoggerAttribute;
import com.capgemini.psd2.logger.LoggerUtils;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.pisp.adapter.PispConsentAdapter;
import com.capgemini.psd2.scaconsenthelper.config.PFConfig;
import com.capgemini.psd2.scaconsenthelper.config.helpers.SCAConsentLoggingHelper;
import com.capgemini.psd2.scaconsenthelper.constants.SCAConsentHelperConstants;
import com.capgemini.psd2.scaconsenthelper.models.IntentTypeEnum;
import com.capgemini.psd2.scaconsenthelper.models.PickupDataModel;
import com.capgemini.psd2.scaconsenthelper.services.SCAConsentHelperService;
import com.capgemini.psd2.security.saas.cancel.controller.SaaSCancelController;

@RunWith(SpringJUnit4ClassRunner.class)
public class SaaSCancelControllerTest {
	
	@Mock
	private SCAConsentHelperService helperService;

	@Mock
	private AccountRequestAdapter accountRequestAdapter;

	@Mock
	private PFConfig pfConfig;
	
	@Mock
	private HttpServletRequest request;
	
	@Mock
	private HttpServletResponse response;
	
	@Mock
	private AispConsentAdapter aispConsentAdapter;
	
	@Mock
	private PispConsentAdapter pispConsentAdapter;
	
	@Mock
	private CispConsentAdapter cispConsentAdapter;
	
	@Mock
	private LoggerUtils loggerUtils;
	
	@Mock
	private FundsConfirmationConsentAdapter fundsConfirmationsConsentAdapter;
	
	@Mock
	private SCAConsentLoggingHelper scaConsentHelper;
	
	@InjectMocks
	private SaaSCancelController controller = new SaaSCancelController();
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}
	
	@Ignore
	@Test(expected=PSD2Exception.class)
	public void cancelSetUpTestWithAISPIntentTypeServerErrorFlagNotNull() throws ParseException{
		AispConsent aispConsent=new AispConsent();
		aispConsent.setStatus(ConsentStatusEnum.AUTHORISED);
		when(aispConsentAdapter.retrieveConsentByAccountRequestIdAndStatus(anyString(), anyObject())).thenReturn(aispConsent);
		OBReadConsentResponse1 accountRequestPOSTResponse=new OBReadConsentResponse1();
		OBReadConsentResponse1Data obj=new OBReadConsentResponse1Data();
		obj.setStatus(OBReadConsentResponse1Data.StatusEnum.AUTHORISED);
		accountRequestPOSTResponse.setData(obj);
		when(accountRequestAdapter.getAccountRequestGETResponse(anyString())).thenReturn(accountRequestPOSTResponse);
		ModelAndView model = new ModelAndView();
		String oAuthUrl = "12345";
		String channelId = "abcd";
		when(pfConfig.getTenantSpecificResumePathBaseUrl("BOIUK")).thenReturn("http://localhost:8");
		PickupDataModel intentData = mock(PickupDataModel.class);
		request.setAttribute(SCAConsentHelperConstants.INTENT_DATA, intentData);
		when(request.getAttribute(SCAConsentHelperConstants.INTENT_DATA)).thenReturn(intentData);
		when(loggerUtils.populateLoggerData(anyString())).thenReturn(new LoggerAttribute());
		String serverErrorFlag = "true";
		when(intentData.getIntentTypeEnum()).thenReturn(IntentTypeEnum.AISP_INTENT_TYPE);
		controller.cancelSetUp(model, oAuthUrl, serverErrorFlag, channelId,"APP");
	}
	
	@Test(expected=PSD2Exception.class)
	public void cancelSetUpTestWithAISPIntentTypeAuthorised() throws ParseException{
		AispConsent aispConsent=new AispConsent();
		aispConsent.setStatus(ConsentStatusEnum.AWAITINGAUTHORISATION);
		when(aispConsentAdapter.retrieveConsentByAccountRequestIdAndStatus(anyString(), anyObject())).thenReturn(aispConsent);
		OBReadConsentResponse1 accountRequestPOSTResponse=new OBReadConsentResponse1();
		OBReadConsentResponse1Data obj=new OBReadConsentResponse1Data();
		obj.setStatus(OBReadConsentResponse1Data.StatusEnum.AWAITINGAUTHORISATION);
		accountRequestPOSTResponse.setData(obj);
		when(accountRequestAdapter.getAccountRequestGETResponse(anyString())).thenReturn(accountRequestPOSTResponse);
		ModelAndView model = new ModelAndView();
		String oAuthUrl = "12345";
		String channelId = "abcd";
		
		PickupDataModel intentData = mock(PickupDataModel.class);
		request.setAttribute(SCAConsentHelperConstants.INTENT_DATA, intentData);
		when(request.getAttribute(SCAConsentHelperConstants.INTENT_DATA)).thenReturn(intentData);
		when(request.getAttribute(PSD2Constants.NETSCALER_TENANT_ID_HEADER_NAME)).thenReturn("BOIROI");
		when(pfConfig.getTenantSpecificResumePathBaseUrl(anyString())).thenReturn("http://localhost:8");
		when(loggerUtils.populateLoggerData(anyString())).thenReturn(new LoggerAttribute());
		when(request.getAttribute(SCAConsentHelperConstants.INTENT_DATA)).thenReturn(intentData);
		String serverErrorFlag = "true";
		when(intentData.getIntentTypeEnum()).thenReturn(IntentTypeEnum.AISP_INTENT_TYPE);
		when(aispConsentAdapter.retrieveConsentByAccountRequestId(anyString())).thenReturn(aispConsent);
		controller.cancelSetUp(model, oAuthUrl, serverErrorFlag, channelId, "APP");
	}
	
	@Test(expected=PSD2Exception.class)
	public void cancelSetUpTestWithAISPIntentTypeAwaitingAuthorisation() throws ParseException{
		AispConsent aispConsent=new AispConsent();
		aispConsent.setStatus(ConsentStatusEnum.AWAITINGAUTHORISATION);
		when(aispConsentAdapter.retrieveConsentByAccountRequestIdAndStatus(anyString(), anyObject())).thenReturn(aispConsent);
		OBReadConsentResponse1 accountRequestPOSTResponse=new OBReadConsentResponse1();
		OBReadConsentResponse1Data obj=new OBReadConsentResponse1Data();
		obj.setStatus(OBReadConsentResponse1Data.StatusEnum.REJECTED);
		accountRequestPOSTResponse.setData(obj);
		when(accountRequestAdapter.getAccountRequestGETResponse(anyString())).thenReturn(accountRequestPOSTResponse);
		ModelAndView model = new ModelAndView();
		String oAuthUrl = "12345";
		String channelId = "abcd";
		
		PickupDataModel intentData = mock(PickupDataModel.class);
		request.setAttribute(SCAConsentHelperConstants.INTENT_DATA, intentData);
		when(request.getAttribute(PSD2Constants.NETSCALER_TENANT_ID_HEADER_NAME)).thenReturn("BOIROI");
		when(pfConfig.getTenantSpecificResumePathBaseUrl(anyString())).thenReturn("http://localhost:8");
		when(request.getAttribute(SCAConsentHelperConstants.INTENT_DATA)).thenReturn(intentData);
		when(loggerUtils.populateLoggerData(anyString())).thenReturn(new LoggerAttribute());
		when(request.getAttribute(SCAConsentHelperConstants.INTENT_DATA)).thenReturn(intentData);
		String serverErrorFlag = "true";
		when(intentData.getIntentTypeEnum()).thenReturn(IntentTypeEnum.AISP_INTENT_TYPE);
		when(aispConsentAdapter.retrieveConsentByAccountRequestId(anyString())).thenReturn(aispConsent);
		controller.cancelSetUp(model, oAuthUrl, serverErrorFlag, channelId, "APP");
	}
	
	//@Test
	public void cancelSetUpTestWithPISPIntentTypeServerErrorFlagNotNull() throws ParseException{
		ModelAndView model = new ModelAndView();
		String oAuthUrl = "12345";
		String channelId = "abcd";
		when(pfConfig.getTenantSpecificResumePathBaseUrl("BOIUK")).thenReturn("http://localhost:8");
		PickupDataModel intentData = mock(PickupDataModel.class);
		request.setAttribute(SCAConsentHelperConstants.INTENT_DATA, intentData);
		when(loggerUtils.populateLoggerData(anyString())).thenReturn(new LoggerAttribute());
		when(request.getAttribute(PSD2Constants.NETSCALER_TENANT_ID_HEADER_NAME)).thenReturn("BOIROI");
		when(request.getAttribute(SCAConsentHelperConstants.INTENT_DATA)).thenReturn(intentData);
		String serverErrorFlag = "true";
		when(intentData.getIntentTypeEnum()).thenReturn(IntentTypeEnum.PISP_INTENT_TYPE);
		controller.cancelSetUp(model, oAuthUrl, serverErrorFlag, channelId,"APP");
	}
	
	//@Test
	public void cancelSetUpTestWithPISPIntentTypeServerErrorFlagNull() throws ParseException{
		ModelAndView model = new ModelAndView();
		String oAuthUrl = "12345";
		String channelId = "abcd";
		when(pfConfig.getTenantSpecificResumePathBaseUrl("BOIUK")).thenReturn("http://localhost:8");
		PickupDataModel intentData = mock(PickupDataModel.class);
		request.setAttribute(SCAConsentHelperConstants.INTENT_DATA, intentData);
		when(request.getAttribute(PSD2Constants.NETSCALER_TENANT_ID_HEADER_NAME)).thenReturn("BOIROI");
		when(request.getAttribute(SCAConsentHelperConstants.INTENT_DATA)).thenReturn(intentData);
		when(loggerUtils.populateLoggerData(anyString())).thenReturn(new LoggerAttribute());
		String serverErrorFlag = null;
		when(intentData.getIntentTypeEnum()).thenReturn(IntentTypeEnum.PISP_INTENT_TYPE);
		controller.cancelSetUp(model, oAuthUrl, serverErrorFlag, channelId,"APP");
	}
	
	@Test(expected=PSD2Exception.class)
	public void cancelSetUpTestWithCISPIntentTypeConsentNull() throws ParseException{
		ModelAndView model = new ModelAndView();
		OBFundsConfirmationConsentResponse1 confirmationResponse = new OBFundsConfirmationConsentResponse1();
		OBFundsConfirmationConsentResponse1Data data = new OBFundsConfirmationConsentResponse1Data();
		data.setStatus(OBFundsConfirmationConsentResponse1Data.StatusEnum.AUTHORISED);
		confirmationResponse.setData(data);
		String oAuthUrl = "12345";
		String channelId = "abcd";
		
		PickupDataModel intentData = mock(PickupDataModel.class);
		request.setAttribute(SCAConsentHelperConstants.INTENT_DATA, intentData);
		when(request.getAttribute(PSD2Constants.NETSCALER_TENANT_ID_HEADER_NAME)).thenReturn("BOIROI");
		when(pfConfig.getTenantSpecificResumePathBaseUrl(anyString())).thenReturn("http://localhost:8");
		when(request.getAttribute(SCAConsentHelperConstants.INTENT_DATA)).thenReturn(intentData);
		String serverErrorFlag = "true";
		when(loggerUtils.populateLoggerData(anyString())).thenReturn(new LoggerAttribute());
		when(intentData.getIntentTypeEnum()).thenReturn(IntentTypeEnum.CISP_INTENT_TYPE);
		when(fundsConfirmationsConsentAdapter.getFundsConfirmationConsentPOSTResponse(anyString())).thenReturn(confirmationResponse);
		controller.cancelSetUp(model, oAuthUrl, serverErrorFlag, channelId,"APP");
	}
	
	@Test(expected=PSD2Exception.class)
	public void cancelSetUpTestWithCISPIntentTypeConsentNullAuthorised() throws ParseException{
		ModelAndView model = new ModelAndView();
		OBFundsConfirmationConsentResponse1 confirmationResponse = new OBFundsConfirmationConsentResponse1();
		OBFundsConfirmationConsentResponse1Data data = new OBFundsConfirmationConsentResponse1Data();
		data.setStatus(OBFundsConfirmationConsentResponse1Data.StatusEnum.AUTHORISED);
		confirmationResponse.setData(data);
		String oAuthUrl = "12345";
		String channelId = "abcd";
		PickupDataModel intentData = mock(PickupDataModel.class);
		request.setAttribute(SCAConsentHelperConstants.INTENT_DATA, intentData);
		when(request.getAttribute(SCAConsentHelperConstants.INTENT_DATA)).thenReturn(intentData);
		when(request.getAttribute(PSD2Constants.NETSCALER_TENANT_ID_HEADER_NAME)).thenReturn("BOIROI");
		when(pfConfig.getTenantSpecificResumePathBaseUrl(anyString())).thenReturn("http://localhost:8");
		String serverErrorFlag = "true";
		when(loggerUtils.populateLoggerData(anyString())).thenReturn(new LoggerAttribute());
		when(intentData.getIntentTypeEnum()).thenReturn(IntentTypeEnum.CISP_INTENT_TYPE);
		when(fundsConfirmationsConsentAdapter.getFundsConfirmationConsentPOSTResponse(anyString())).thenReturn(confirmationResponse);
		controller.cancelSetUp(model, oAuthUrl, serverErrorFlag, channelId,"APP");
	}
	
	@Test(expected=PSD2Exception.class)
	public void cancelSetUpTestWithCISPIntentTypeConsent() throws ParseException{
		ModelAndView model = new ModelAndView();
		OBFundsConfirmationConsentResponse1 confirmationResponse = new OBFundsConfirmationConsentResponse1();
		OBFundsConfirmationConsentResponse1Data data = new OBFundsConfirmationConsentResponse1Data();
		data.setStatus(OBFundsConfirmationConsentResponse1Data.StatusEnum.AWAITINGAUTHORISATION);
		confirmationResponse.setData(data);
		String oAuthUrl = "12345";
		String channelId = "abcd";
		CispConsent cispConsent = new CispConsent();
		cispConsent.setStatus(ConsentStatusEnum.AUTHORISED);
		
		PickupDataModel intentData = mock(PickupDataModel.class);
		request.setAttribute(SCAConsentHelperConstants.INTENT_DATA, intentData);
		when(request.getAttribute(PSD2Constants.NETSCALER_TENANT_ID_HEADER_NAME)).thenReturn("BOIROI");
		when(request.getAttribute(SCAConsentHelperConstants.INTENT_DATA)).thenReturn(intentData);
		when(pfConfig.getTenantSpecificResumePathBaseUrl(anyString())).thenReturn("http://localhost:8");
		String serverErrorFlag = "true";
		when(loggerUtils.populateLoggerData(anyString())).thenReturn(new LoggerAttribute());
		when(intentData.getIntentTypeEnum()).thenReturn(IntentTypeEnum.CISP_INTENT_TYPE);
		when(fundsConfirmationsConsentAdapter.getFundsConfirmationConsentPOSTResponse(anyString())).thenReturn(confirmationResponse);
		when(cispConsentAdapter.retrieveConsentByFundsIntentId(anyString())).thenReturn(cispConsent);
		controller.cancelSetUp(model, oAuthUrl, serverErrorFlag, channelId,"APP");
	}
	
	@Test(expected=PSD2Exception.class)
	public void cancelSetUpTestWithCISPIntentTypeConsentAwaitingAuthorisation() throws ParseException{
		ModelAndView model = new ModelAndView();
		OBFundsConfirmationConsentResponse1 confirmationResponse = new OBFundsConfirmationConsentResponse1();
		OBFundsConfirmationConsentResponse1Data data = new OBFundsConfirmationConsentResponse1Data();
		data.setStatus(OBFundsConfirmationConsentResponse1Data.StatusEnum.AWAITINGAUTHORISATION);
		confirmationResponse.setData(data);
		String oAuthUrl = "12345";
		String channelId = "abcd";
		CispConsent cispConsent = new CispConsent();
		cispConsent.setStatus(ConsentStatusEnum.AWAITINGAUTHORISATION);
		
		PickupDataModel intentData = mock(PickupDataModel.class);
		request.setAttribute(SCAConsentHelperConstants.INTENT_DATA, intentData);
		when(request.getAttribute(PSD2Constants.NETSCALER_TENANT_ID_HEADER_NAME)).thenReturn("BOIROI");
		when(pfConfig.getTenantSpecificResumePathBaseUrl(anyString())).thenReturn("http://localhost:8");
		when(request.getAttribute(SCAConsentHelperConstants.INTENT_DATA)).thenReturn(intentData);
		String serverErrorFlag = "true";
		when(loggerUtils.populateLoggerData(anyString())).thenReturn(new LoggerAttribute());
		when(request.getAttribute(SCAConsentHelperConstants.INTENT_DATA)).thenReturn(intentData);
		when(intentData.getIntentTypeEnum()).thenReturn(IntentTypeEnum.CISP_INTENT_TYPE);
		when(fundsConfirmationsConsentAdapter.getFundsConfirmationConsentPOSTResponse(anyString())).thenReturn(confirmationResponse);
		when(cispConsentAdapter.retrieveConsentByFundsIntentId(anyString())).thenReturn(cispConsent);
		controller.cancelSetUp(model, oAuthUrl, serverErrorFlag, channelId,"APP");
	}
	
	@Test(expected=PSD2Exception.class)
	public void cancelSetUpTestWithCISPIntentTypeConsentNotAwaitingAuthorisation() throws ParseException{
		ModelAndView model = new ModelAndView();
		OBFundsConfirmationConsentResponse1 confirmationResponse = new OBFundsConfirmationConsentResponse1();
		OBFundsConfirmationConsentResponse1Data data = new OBFundsConfirmationConsentResponse1Data();
		data.setStatus(OBFundsConfirmationConsentResponse1Data.StatusEnum.AWAITINGAUTHORISATION);
		confirmationResponse.setData(data);
		String oAuthUrl = "12345";
		String channelId = "abcd";
		CispConsent cispConsent = new CispConsent();
		cispConsent.setStatus(ConsentStatusEnum.AUTHORISED);
		
		PickupDataModel intentData = mock(PickupDataModel.class);
		request.setAttribute(SCAConsentHelperConstants.INTENT_DATA, intentData);
		when(request.getAttribute(PSD2Constants.NETSCALER_TENANT_ID_HEADER_NAME)).thenReturn("BOIROI");
		when(pfConfig.getTenantSpecificResumePathBaseUrl(anyString())).thenReturn("http://localhost:8");
		when(request.getAttribute(SCAConsentHelperConstants.INTENT_DATA)).thenReturn(intentData);
		String serverErrorFlag = "true";
		when(loggerUtils.populateLoggerData(anyString())).thenReturn(new LoggerAttribute());
		when(intentData.getIntentTypeEnum()).thenReturn(IntentTypeEnum.CISP_INTENT_TYPE);
		when(fundsConfirmationsConsentAdapter.getFundsConfirmationConsentPOSTResponse(anyString())).thenReturn(confirmationResponse);
		when(cispConsentAdapter.retrieveConsentByFundsIntentId(anyString())).thenReturn(cispConsent);
		controller.cancelSetUp(model, oAuthUrl, serverErrorFlag, channelId,"APP");
	}
	
	@Test(expected=PSD2Exception.class)
	public void cancelSetUpTestChannelIdNull() throws ParseException{
		ModelAndView model = new ModelAndView();
		OBFundsConfirmationConsentResponse1 confirmationResponse = new OBFundsConfirmationConsentResponse1();
		OBFundsConfirmationConsentResponse1Data data = new OBFundsConfirmationConsentResponse1Data();
		data.setStatus(OBFundsConfirmationConsentResponse1Data.StatusEnum.AWAITINGAUTHORISATION);
		confirmationResponse.setData(data);
		String oAuthUrl = "12345";
		CispConsent cispConsent = new CispConsent();
		cispConsent.setStatus(ConsentStatusEnum.AWAITINGAUTHORISATION);
		
		PickupDataModel intentData = mock(PickupDataModel.class);
		request.setAttribute(SCAConsentHelperConstants.INTENT_DATA, intentData);
		when(request.getAttribute(SCAConsentHelperConstants.INTENT_DATA)).thenReturn(intentData);
		when(request.getAttribute(PSD2Constants.NETSCALER_TENANT_ID_HEADER_NAME)).thenReturn("BOIROI");
		when(pfConfig.getTenantSpecificResumePathBaseUrl(anyString())).thenReturn("http://localhost:8");
		String serverErrorFlag = "true";
		when(loggerUtils.populateLoggerData(anyString())).thenReturn(new LoggerAttribute());
		when(intentData.getIntentTypeEnum()).thenReturn(IntentTypeEnum.CISP_INTENT_TYPE);
		when(fundsConfirmationsConsentAdapter.getFundsConfirmationConsentPOSTResponse(anyString())).thenReturn(confirmationResponse);
		when(cispConsentAdapter.retrieveConsentByFundsIntentId(anyString())).thenReturn(cispConsent);
		controller.cancelSetUp(model, oAuthUrl, serverErrorFlag, null,"APP");
	}
}
