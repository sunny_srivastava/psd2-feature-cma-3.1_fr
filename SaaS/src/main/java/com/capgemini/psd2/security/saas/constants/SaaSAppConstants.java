package com.capgemini.psd2.security.saas.constants;

public class SaaSAppConstants {

	public static final String DIGITAL_USER = "digitalUser";
	
	public static final String CUSTOMER_AUTHENTICATION_SESSION = "customerAuthenticationSession";
	
	public static final String ELECTRONIC_DEVICES = "electronicDevices";
	
	public static final String DIGITAL_USER_SESSION = "digitalUserSession";
	
	public static final String SESSION_INITIATION_FAILURE_INDICATOR = "sessionInitiationFailureIndicator";
	
	public static final String TPP_OBJECT = "tppInformationObj";
	
	public static final String TPP_NAME = "tppApplicationName";
	
	public static final String CHANNEL_TYPE_WEB = "WEB";
	
	public static final String CHANNEL_TYPE_APP = "APP";
	
	public static final String FLOW_TYPE_CISP = "CISP";
}
