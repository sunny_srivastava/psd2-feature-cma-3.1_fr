package com.capgemini.psd2.foundationservice.account.beneficiaries.boi.adapter.test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.anyObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountBeneficiariesResponse;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.foundationservice.account.beneficiaries.boi.adapter.constants.AccountBeneficiariesFoundationServiceConstants;
import com.capgemini.psd2.foundationservice.account.beneficiaries.boi.adapter.delegate.AccountBeneficiariesFoundationServiceDelegate;
import com.capgemini.psd2.foundationservice.account.beneficiaries.boi.adapter.domain.PartiesPaymentBeneficiariesresponse;
import com.capgemini.psd2.foundationservice.account.beneficiaries.boi.adapter.transformer.AccountBeneficiariesFoundationServiceTransformer;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.rest.client.model.RequestInfo;

@RunWith(SpringJUnit4ClassRunner.class)
public class AccountBeneficiariesFoundationServiceDelegateTest {

	@InjectMocks
	private AccountBeneficiariesFoundationServiceDelegate accountBeneficiariesFoundationServiceDelegate;

	@Mock
	private AccountBeneficiariesFoundationServiceTransformer validatePaymentFSTransformer;

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void contextLoads() {
	}

	@Test
	public void testTransform() {
		PlatformAccountBeneficiariesResponse platformAccountBeneficiariesResponse = new PlatformAccountBeneficiariesResponse();
		Mockito.when(validatePaymentFSTransformer.transformAccountBeneficiaries(anyObject(), anyObject()))
				.thenReturn(platformAccountBeneficiariesResponse);
		PlatformAccountBeneficiariesResponse response = accountBeneficiariesFoundationServiceDelegate
				.transform(new PartiesPaymentBeneficiariesresponse(), new HashMap<String, String>());
		assertThat(platformAccountBeneficiariesResponse).isEqualTo(response);
	}

	@Test
	public void testGetFoundationServiceURL() {
		String finalURL = "http://localhost:9087/fs-abt-service/services/user/v1.0/parties/12345678/payment-beneficiaries";
		String response = accountBeneficiariesFoundationServiceDelegate.getFoundationServiceURL("v1.0", "12345678",
				"http://localhost:9087/fs-abt-service/services/user");
		assertThat(response).isEqualTo(finalURL);
	}

	@Test(expected = AdapterException.class)
	public void testGetFoundationServiceURLUserIdNull() {
		accountBeneficiariesFoundationServiceDelegate.getFoundationServiceURL("v1.0", null,
				"http://localhost:9087/fs-abt-service/services/user");
	}

	@Test(expected = AdapterException.class)
	public void testGetFoundationServiceURLVersionNull() {
		accountBeneficiariesFoundationServiceDelegate.getFoundationServiceURL(null, "12345678",
				"http://localhost:9087/fs-abt-service/services/user");
	}

	@Test
	public void testCreateRequestHeaders() {
		ReflectionTestUtils.setField(accountBeneficiariesFoundationServiceDelegate, "userInReqHeader", "X-BOI-USER");
		ReflectionTestUtils.setField(accountBeneficiariesFoundationServiceDelegate, "channelInReqHeader",
				"X-BOI-CHANNEL");
		ReflectionTestUtils.setField(accountBeneficiariesFoundationServiceDelegate, "platformInReqHeader",
				"X-BOI-PLATFORM");
		ReflectionTestUtils.setField(accountBeneficiariesFoundationServiceDelegate, "transactionReqHeader",
				"X-TRANSACTION-ID");
		ReflectionTestUtils.setField(accountBeneficiariesFoundationServiceDelegate, "platform", "PSD2API");
		AccountMapping accountMapping = new AccountMapping();
		accountMapping.setPsuId("12345678");
		accountMapping.setCorrelationId("123");
		AccountDetails accountDetails = new AccountDetails();
		accountDetails.setAccountNSC("123456");
		accountDetails.setAccountNumber("12345678");
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
		accDetList.add(accountDetails);
		accountMapping.setAccountDetails(accDetList);
		Map<String, String> params = new HashMap<>();
		params.put(PSD2Constants.CHANNEL_NAME, "Channel");
		params.put(PSD2Constants.PLATFORM_IN_REQ_HEADER, "PSD2API");
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.add("X-BOI-USER", "12345678");
		httpHeaders.add("X-BOI-CHANNEL", "CHANNEL");
		httpHeaders.add("X-BOI-PLATFORM", "PSD2API");
		httpHeaders.add("X-TRANSACTION-ID", "123");
		httpHeaders.add(AccountBeneficiariesFoundationServiceConstants.PARENTASORTCODENSCNUMBERHEADER, "123456");
		httpHeaders.add( AccountBeneficiariesFoundationServiceConstants.ACCOUNTNUMBERHEADER, "12345678");
		HttpHeaders response = accountBeneficiariesFoundationServiceDelegate.createRequestHeaders(new RequestInfo(),
				accountMapping, params);
		assertThat(response).isEqualTo(httpHeaders);
		params.put(PSD2Constants.PLATFORM_IN_REQ_HEADER, null);
		response = accountBeneficiariesFoundationServiceDelegate.createRequestHeaders(new RequestInfo(), accountMapping,
				params);
		assertThat(response).isEqualTo(httpHeaders);
	}
}