
package com.capgemini.psd2.foundationservice.account.beneficiaries.boi.adapter;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import com.capgemini.psd2.adapter.exceptions.AdapterErrorCodeEnum;
import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.aisp.adapter.AccountBeneficiariesAdapter;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountBeneficiariesResponse;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.foundationservice.account.beneficiaries.boi.adapter.client.AccountBeneficiariesFoundationServiceClient;
import com.capgemini.psd2.foundationservice.account.beneficiaries.boi.adapter.constants.AccountBeneficiariesFoundationServiceConstants;
import com.capgemini.psd2.foundationservice.account.beneficiaries.boi.adapter.delegate.AccountBeneficiariesFoundationServiceDelegate;
import com.capgemini.psd2.foundationservice.account.beneficiaries.boi.adapter.domain.PartiesPaymentBeneficiariesresponse;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.CustomerAccountsFilterFoundationServiceAdapter;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.filterartion.AdapterFilterUtility;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.DigitalUserProfile;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Component
public class AccountBeneficiariesFoundationServiceAdapter implements AccountBeneficiariesAdapter {

	@Value("${foundationService.accountBeneficiaryBaseURL}")
	private String accountBeneficiaryBaseURL;

	@Value("${foundationService.consentFlowType}")
	private String consentFlowType;

	@Value("${foundationService.version}")
	private String version;

	/** The AdapterFilterUtility object */
	@Autowired
	private AdapterFilterUtility adapterFilterUtility;

	@Autowired
	private CustomerAccountsFilterFoundationServiceAdapter commonFilterUtility;

	@Autowired
	private AccountBeneficiariesFoundationServiceDelegate accountBeneficiariesFoundationServiceDelegate;

	@Autowired
	private AccountBeneficiariesFoundationServiceClient accountBeneficiariesFoundationServiceClient;

	@Override
	public PlatformAccountBeneficiariesResponse retrieveAccountBeneficiaries(AccountMapping accountMapping,
			Map<String, String> params) {
		if (NullCheckUtils.isNullOrEmpty(accountMapping) || NullCheckUtils.isNullOrEmpty(accountMapping.getPsuId())) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.BAD_REQUEST);
		}
		if (NullCheckUtils.isNullOrEmpty(params)) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.BAD_REQUEST);
		}
		params.put(PSD2Constants.CONSENT_FLOW_TYPE, consentFlowType);
		params.put(PSD2Constants.CHANNEL_ID, params.get("channelId"));
		params.put(PSD2Constants.USER_ID, accountMapping.getPsuId());
		params.put(PSD2Constants.CORRELATION_ID, accountMapping.getCorrelationId());
		String channelId = params.get("channelId");
		RequestInfo requestInfo = new RequestInfo();
		HttpHeaders httpHeaders = accountBeneficiariesFoundationServiceDelegate.createRequestHeaders(requestInfo,
				accountMapping, params);
		// Assuming only one AccountDetail comes from API
		AccountDetails accountDetails = null;
		if (accountMapping.getAccountDetails() != null && !accountMapping.getAccountDetails().isEmpty()) {
			accountDetails = accountMapping.getAccountDetails().get(0);
			params.put(AccountBeneficiariesFoundationServiceConstants.ACCOUNT_ID, accountDetails.getAccountId());
		} else {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.BAD_REQUEST);
		}

		PartiesPaymentBeneficiariesresponse beneficiaries = null;
		if (channelId.equalsIgnoreCase("BOL") || channelId.equalsIgnoreCase("B365")) {

			if ((NullCheckUtils.isNullOrEmpty(accountDetails.getAccountSubType())
					&& params.get(PSD2Constants.CMAVERSION) == null)
					|| accountDetails.getAccountSubType().toString().contentEquals("CurrentAccount")
					|| accountDetails.getAccountSubType().toString().contentEquals("Savings")) {

				String finalURL = accountBeneficiariesFoundationServiceDelegate.getFoundationServiceURL(version,
						accountMapping.getPsuId(), accountBeneficiaryBaseURL);
				requestInfo.setUrl(finalURL);

				MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<String, String>();
				queryParams.add("sourceSystemCode", channelId);
				beneficiaries = accountBeneficiariesFoundationServiceClient.restTransportForAccountBeneficiaryFS(
						requestInfo, PartiesPaymentBeneficiariesresponse.class, httpHeaders, queryParams);

				if (NullCheckUtils.isNullOrEmpty(beneficiaries.getPaymentBeneficiaryList()))
					beneficiaries = new PartiesPaymentBeneficiariesresponse();

			} else if (accountDetails.getAccountSubType().toString().contentEquals("CreditCard")) {

				DigitalUserProfile filteredAccounts = commonFilterUtility
						.retrieveCustomerAccountList(accountMapping.getPsuId(), params);
				if (filteredAccounts == null || filteredAccounts.getAccountEntitlements() == null
						|| filteredAccounts.getAccountEntitlements().isEmpty()) {
					throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.ACCOUNT_NOT_FOUND);
				}

				adapterFilterUtility.prevalidateAccounts(filteredAccounts, accountMapping);

				beneficiaries = new PartiesPaymentBeneficiariesresponse();
			}
		}
		return accountBeneficiariesFoundationServiceDelegate.transform(beneficiaries, params);
	}

}