package com.capgemini.psd2.foundationservice.account.beneficiaries.boi.adapter.transformer;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.aisp.domain.OBBeneficiary5;
import com.capgemini.psd2.aisp.domain.OBBeneficiaryType1Code;
import com.capgemini.psd2.aisp.domain.OBBranchAndFinancialInstitutionIdentification60;
import com.capgemini.psd2.aisp.domain.OBCashAccount50;
import com.capgemini.psd2.aisp.domain.OBReadBeneficiary5;
import com.capgemini.psd2.aisp.domain.OBReadBeneficiary5Data;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountBeneficiariesResponse;
import com.capgemini.psd2.aisp.transformer.AccountBeneficiariesTransformer;
import com.capgemini.psd2.foundationservice.account.beneficiaries.boi.adapter.constants.AccountBeneficiariesFoundationServiceConstants;
import com.capgemini.psd2.foundationservice.account.beneficiaries.boi.adapter.domain.Beneficiary;
import com.capgemini.psd2.foundationservice.account.beneficiaries.boi.adapter.domain.PartiesPaymentBeneficiariesresponse;
import com.capgemini.psd2.utilities.NullCheckUtils;
import com.capgemini.psd2.validator.PSD2Validator;

@Component
public class AccountBeneficiariesFoundationServiceTransformer implements AccountBeneficiariesTransformer {

	@Autowired
	private PSD2Validator validator;

	@Value("${foundationService.filteredBeneficiariesCountryCode1}")
	private String filteredBeneficiariesCountryCode1;

	@Value("${foundationService.filteredBeneficiariesCountryCode2}")
	private String filteredBeneficiariesCountryCode2;

	@Value("${foundationService.filteredBeneficiariesCountryCode3}")
	private String filteredBeneficiariesCountryCode3;

	@Value("${foundationService.filteredBeneficiariesCountryCode4}")
	private String filteredBeneficiariesCountryCode4;

	@Value("${foundationService.filteredBeneficiariesCountryCode5}")
	private String filteredBeneficiariesCountryCode5;

	@Value("${foundationService.filterBeneficiariesResponse}")
	private boolean filterBeneficiariesResponse;

	public <T> PlatformAccountBeneficiariesResponse transformAccountBeneficiaries(T source,
			Map<String, String> params) {

		PlatformAccountBeneficiariesResponse platformAccountBeneficiariesResponse = new PlatformAccountBeneficiariesResponse();
		OBReadBeneficiary5 obReadBeneficiary5 = new OBReadBeneficiary5();
		OBReadBeneficiary5Data obReadBeneficiary5Data = new OBReadBeneficiary5Data();

		PartiesPaymentBeneficiariesresponse beneficiaries = (PartiesPaymentBeneficiariesresponse) source;

		List<Beneficiary> beneficiaryList = beneficiaries.getPaymentBeneficiaryList();
		for (Beneficiary beneficiary : beneficiaryList) {
			String code = beneficiary.getBeneficiaryAccount().getOperationalOrganisationUnit().getAddressReference()
					.getAddressCountry().getIsoCountryAlphaTwoCode();
			if (!NullCheckUtils.isNullOrEmpty(code) && code.length() == 2) {
				if (filteredBeneficiariesCountryCode1
						.contains(beneficiary.getBeneficiaryAccount().getOperationalOrganisationUnit()
								.getAddressReference().getAddressCountry().getIsoCountryAlphaTwoCode())) {

					if (!NullCheckUtils
							.isNullOrEmpty(beneficiary.getBeneficiaryAccount().getSwiftBankIdentifierCodeBIC())) {
						OBBranchAndFinancialInstitutionIdentification60 obBeneficiary5CreditorAgent = new OBBranchAndFinancialInstitutionIdentification60();

						obBeneficiary5CreditorAgent
								.setSchemeName(AccountBeneficiariesFoundationServiceConstants.UK_OBIE_BICFI);
						obBeneficiary5CreditorAgent
								.setIdentification(beneficiary.getBeneficiaryAccount().getSwiftBankIdentifierCodeBIC());

						OBBeneficiary5 obBeneficiary5 = new OBBeneficiary5();

						// AccountId, beneficiaryId and Reference
						obBeneficiary5
								.setAccountId(params.get(AccountBeneficiariesFoundationServiceConstants.ACCOUNT_ID));
						if (!NullCheckUtils.isNullOrEmpty(beneficiary.getPaymentBeneficiaryIdentifier())) {
							obBeneficiary5.setBeneficiaryId(beneficiary.getPaymentBeneficiaryIdentifier());
						}
						if (!NullCheckUtils.isNullOrEmpty(beneficiary.getPaymentBeneficiaryNarrativeText())) {
							obBeneficiary5.setReference(beneficiary.getPaymentBeneficiaryNarrativeText());
						}

						OBCashAccount50 obBeneficiary5CreditorAccount = new OBCashAccount50();

						obBeneficiary5CreditorAccount
								.setSchemeName(AccountBeneficiariesFoundationServiceConstants.UK_OBIE_IBAN);
						obBeneficiary5CreditorAccount.setIdentification(
								beneficiary.getBeneficiaryAccount().getInternationalBankAccountNumberIBAN());
						obBeneficiary5CreditorAccount.setName(beneficiary.getPaymentBeneficiaryLabel());

						obBeneficiary5.setCreditorAgent(obBeneficiary5CreditorAgent);
						obBeneficiary5.setCreditorAccount(obBeneficiary5CreditorAccount);
						// BeneficiaryType added for Adaptor version 3.1.8
						obBeneficiary5.setBeneficiaryType(OBBeneficiaryType1Code.ORDINARY);
						validator.validate(obBeneficiary5);
						obReadBeneficiary5Data.addBeneficiaryItem(obBeneficiary5);
					}
				} else if (filteredBeneficiariesCountryCode2
						.contains(beneficiary.getBeneficiaryAccount().getOperationalOrganisationUnit()
								.getAddressReference().getAddressCountry().getIsoCountryAlphaTwoCode())) {

					if (!NullCheckUtils
							.isNullOrEmpty(beneficiary.getBeneficiaryAccount().getSwiftBankIdentifierCodeBIC())) {
						OBBranchAndFinancialInstitutionIdentification60 obBeneficiary5CreditorAgent = new OBBranchAndFinancialInstitutionIdentification60();

						obBeneficiary5CreditorAgent
								.setSchemeName(AccountBeneficiariesFoundationServiceConstants.UK_OBIE_BICFI);
						obBeneficiary5CreditorAgent
								.setIdentification(beneficiary.getBeneficiaryAccount().getSwiftBankIdentifierCodeBIC());

						OBBeneficiary5 obBeneficiary5 = new OBBeneficiary5();

						// AccountId, beneficiaryId and Reference
						obBeneficiary5
								.setAccountId(params.get(AccountBeneficiariesFoundationServiceConstants.ACCOUNT_ID));
						if (!NullCheckUtils.isNullOrEmpty(beneficiary.getPaymentBeneficiaryIdentifier())) {
							obBeneficiary5.setBeneficiaryId(beneficiary.getPaymentBeneficiaryIdentifier());
						}
						if (!NullCheckUtils.isNullOrEmpty(beneficiary.getPaymentBeneficiaryNarrativeText())) {
							obBeneficiary5.setReference(beneficiary.getPaymentBeneficiaryNarrativeText());
						}

						OBCashAccount50 obBeneficiary5CreditorAccount = new OBCashAccount50();

						obBeneficiary5CreditorAccount
								.setSchemeName(AccountBeneficiariesFoundationServiceConstants.UK_BOI_AccountNumber);
						obBeneficiary5CreditorAccount
								.setIdentification(beneficiary.getBeneficiaryAccount().getAccountNumber());
						obBeneficiary5CreditorAccount.setName(beneficiary.getPaymentBeneficiaryLabel());

						obBeneficiary5.setCreditorAgent(obBeneficiary5CreditorAgent);
						obBeneficiary5.setCreditorAccount(obBeneficiary5CreditorAccount);
						// BeneficiaryType added for Adaptor version 3.1.8
						obBeneficiary5.setBeneficiaryType(OBBeneficiaryType1Code.ORDINARY);
						validator.validate(obBeneficiary5);
						obReadBeneficiary5Data.addBeneficiaryItem(obBeneficiary5);
					}
				} else if (filteredBeneficiariesCountryCode3
						.contains(beneficiary.getBeneficiaryAccount().getOperationalOrganisationUnit()
								.getAddressReference().getAddressCountry().getIsoCountryAlphaTwoCode())) {

					if (!NullCheckUtils
							.isNullOrEmpty(beneficiary.getBeneficiaryAccount().getABARTNRoutingTransitNumber())
							&& (beneficiary.getBeneficiaryAccount().getABARTNRoutingTransitNumber().length() == 9)) {

						OBBeneficiary5 obBeneficiary5 = new OBBeneficiary5();

						// AccountId, beneficiaryId and Reference
						obBeneficiary5
								.setAccountId(params.get(AccountBeneficiariesFoundationServiceConstants.ACCOUNT_ID));
						if (!NullCheckUtils.isNullOrEmpty(beneficiary.getPaymentBeneficiaryIdentifier())) {
							obBeneficiary5.setBeneficiaryId(beneficiary.getPaymentBeneficiaryIdentifier());
						}
						if (!NullCheckUtils.isNullOrEmpty(beneficiary.getPaymentBeneficiaryNarrativeText())) {
							obBeneficiary5.setReference(beneficiary.getPaymentBeneficiaryNarrativeText());
						}

						OBCashAccount50 obBeneficiary5CreditorAccount = new OBCashAccount50();

						obBeneficiary5CreditorAccount.setSchemeName(
								AccountBeneficiariesFoundationServiceConstants.UK_OBIE_ABACodeAccountNumber);
						obBeneficiary5CreditorAccount.setIdentification(
								beneficiary.getBeneficiaryAccount().getABARTNRoutingTransitNumber().trim()
										+ beneficiary.getBeneficiaryAccount().getAccountNumber().trim());
						obBeneficiary5CreditorAccount.setName(beneficiary.getPaymentBeneficiaryLabel());

						obBeneficiary5.setCreditorAccount(obBeneficiary5CreditorAccount);
						// BeneficiaryType added for Adaptor version 3.1.8
						obBeneficiary5.setBeneficiaryType(OBBeneficiaryType1Code.ORDINARY);
						validator.validate(obBeneficiary5);
						obReadBeneficiary5Data.addBeneficiaryItem(obBeneficiary5);
					}
				} else if ((filteredBeneficiariesCountryCode4
						.contains(beneficiary.getBeneficiaryAccount().getOperationalOrganisationUnit()
								.getAddressReference().getAddressCountry().getIsoCountryAlphaTwoCode()))
						|| (filteredBeneficiariesCountryCode5
								.contains(beneficiary.getBeneficiaryAccount().getOperationalOrganisationUnit()
										.getAddressReference().getAddressCountry().getIsoCountryAlphaTwoCode()))) {

					if (!NullCheckUtils
							.isNullOrEmpty(beneficiary.getBeneficiaryAccount().getSwiftBankIdentifierCodeBIC())) {
						OBBranchAndFinancialInstitutionIdentification60 obBeneficiary5CreditorAgent = new OBBranchAndFinancialInstitutionIdentification60();

						obBeneficiary5CreditorAgent
								.setSchemeName(AccountBeneficiariesFoundationServiceConstants.UK_OBIE_BICFI);
						obBeneficiary5CreditorAgent
								.setIdentification(beneficiary.getBeneficiaryAccount().getSwiftBankIdentifierCodeBIC());

						OBBeneficiary5 obBeneficiary5 = new OBBeneficiary5();

						// AccountId, beneficiaryId and Reference
						obBeneficiary5
								.setAccountId(params.get(AccountBeneficiariesFoundationServiceConstants.ACCOUNT_ID));
						if (!NullCheckUtils.isNullOrEmpty(beneficiary.getPaymentBeneficiaryIdentifier())) {
							obBeneficiary5.setBeneficiaryId(beneficiary.getPaymentBeneficiaryIdentifier());
						}
						if (!NullCheckUtils.isNullOrEmpty(beneficiary.getPaymentBeneficiaryNarrativeText())) {
							obBeneficiary5.setReference(beneficiary.getPaymentBeneficiaryNarrativeText());
						}

						OBCashAccount50 obBeneficiary5CreditorAccount = new OBCashAccount50();

						obBeneficiary5CreditorAccount
								.setSchemeName(AccountBeneficiariesFoundationServiceConstants.UK_BOI_AccountNumber);
						obBeneficiary5CreditorAccount
								.setIdentification(beneficiary.getBeneficiaryAccount().getAccountNumber());
						obBeneficiary5CreditorAccount.setName(beneficiary.getPaymentBeneficiaryLabel());

						obBeneficiary5.setCreditorAgent(obBeneficiary5CreditorAgent);
						obBeneficiary5.setCreditorAccount(obBeneficiary5CreditorAccount);
						// BeneficiaryType added for Adaptor version 3.1.8
						obBeneficiary5.setBeneficiaryType(OBBeneficiaryType1Code.ORDINARY);
						validator.validate(obBeneficiary5);
						obReadBeneficiary5Data.addBeneficiaryItem(obBeneficiary5);
					}
				}
			}
			if (NullCheckUtils.isNullOrEmpty(code)) {
				OBBeneficiary5 obBeneficiary5 = new OBBeneficiary5();
				// AccountId, beneficiaryId and Reference
				obBeneficiary5.setAccountId(params.get(AccountBeneficiariesFoundationServiceConstants.ACCOUNT_ID));
				if (!NullCheckUtils.isNullOrEmpty(beneficiary.getPaymentBeneficiaryIdentifier())) {
					obBeneficiary5.setBeneficiaryId(beneficiary.getPaymentBeneficiaryIdentifier());
				}
				if (!NullCheckUtils.isNullOrEmpty(beneficiary.getPaymentBeneficiaryNarrativeText())) {
					obBeneficiary5.setReference(beneficiary.getPaymentBeneficiaryNarrativeText());
				}

				OBCashAccount50 obBeneficiary5CreditorAccount = null;

				if (!NullCheckUtils
						.isNullOrEmpty(beneficiary.getBeneficiaryAccount().getInternationalBankAccountNumberIBAN())) {
					if (!NullCheckUtils
							.isNullOrEmpty(beneficiary.getBeneficiaryAccount().getSwiftBankIdentifierCodeBIC())) {
						OBBranchAndFinancialInstitutionIdentification60 obBeneficiary5CreditorAgent = new OBBranchAndFinancialInstitutionIdentification60();
						obBeneficiary5CreditorAgent
								.setSchemeName(AccountBeneficiariesFoundationServiceConstants.UK_OBIE_BICFI);
						obBeneficiary5CreditorAgent
								.setIdentification(beneficiary.getBeneficiaryAccount().getSwiftBankIdentifierCodeBIC());
						obBeneficiary5.setCreditorAgent(obBeneficiary5CreditorAgent);
					}
					obBeneficiary5CreditorAccount = new OBCashAccount50();
					obBeneficiary5CreditorAccount
							.setSchemeName(AccountBeneficiariesFoundationServiceConstants.UK_OBIE_IBAN);
					obBeneficiary5CreditorAccount.setIdentification(
							beneficiary.getBeneficiaryAccount().getInternationalBankAccountNumberIBAN());
					obBeneficiary5CreditorAccount.setName(beneficiary.getPaymentBeneficiaryLabel());

				} else {
					obBeneficiary5CreditorAccount = new OBCashAccount50();
					obBeneficiary5CreditorAccount.setSchemeName(
							AccountBeneficiariesFoundationServiceConstants.UK_OBIE_SortCodeAccountNumber);
					obBeneficiary5CreditorAccount.setIdentification(
							beneficiary.getBeneficiaryAccount().getParentNationalSortCodeNSCNumber().trim()
									+ beneficiary.getBeneficiaryAccount().getAccountNumber().trim());
					obBeneficiary5CreditorAccount.setName(beneficiary.getPaymentBeneficiaryLabel());

				}

				obBeneficiary5.setCreditorAccount(obBeneficiary5CreditorAccount);
				// BeneficiaryType added for Adaptor version 3.1.8
				obBeneficiary5.setBeneficiaryType(OBBeneficiaryType1Code.ORDINARY);
				validator.validate(obBeneficiary5);
				obReadBeneficiary5Data.addBeneficiaryItem(obBeneficiary5);
			}
		}
		if (null == obReadBeneficiary5Data.getBeneficiary() || obReadBeneficiary5Data.getBeneficiary().isEmpty()) {
			obReadBeneficiary5Data.setBeneficiary(new ArrayList<>());
		}
		validator.validate(obReadBeneficiary5Data);
		obReadBeneficiary5.setData(obReadBeneficiary5Data);
		platformAccountBeneficiariesResponse.setoBReadBeneficiary5(obReadBeneficiary5);
		return platformAccountBeneficiariesResponse;
	}
}
