
package com.capgemini.psd2.foundationservice.account.beneficiaries.boi.adapter.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;

import com.capgemini.psd2.foundationservice.account.beneficiaries.boi.adapter.domain.PartiesPaymentBeneficiariesresponse;
import com.capgemini.psd2.mask.DataMask;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.RestClientSync;

@Service
public class AccountBeneficiariesFoundationServiceClientImpl implements AccountBeneficiariesFoundationServiceClient {

	/** The rest client. */
	@Autowired
	@Qualifier("restClientFoundation")
	private RestClientSync restClient;
	
	@Value("${foundationService.maskBeneficiariesResponse:#{false}}")
	private boolean maskBeneficiariesResponse;
	
	@Autowired
	private DataMask dataMask;
	
	@Override
	public PartiesPaymentBeneficiariesresponse restTransportForAccountBeneficiaryFS(RequestInfo requestInfo, Class<PartiesPaymentBeneficiariesresponse> responseType, HttpHeaders headers, MultiValueMap<String, String> queryParams) {
		requestInfo.setUrl(UriComponentsBuilder.fromHttpUrl(requestInfo.getUrl()).queryParams(queryParams).build().toString());
		PartiesPaymentBeneficiariesresponse beneficiaries = restClient.callForGet(requestInfo, responseType, headers);
		if(maskBeneficiariesResponse){
			beneficiaries = dataMask.maskResponse(beneficiaries, "restTransportForAccountBeneficiaryFS");
		}
		
		return beneficiaries;
	}
}
