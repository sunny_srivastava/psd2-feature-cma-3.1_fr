/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.cisp.consent.adapter.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.cisp.adapter.CispConsentAdapter;
import com.capgemini.psd2.cisp.consent.adapter.repository.CispConsentMongoRepository;
import com.capgemini.psd2.consent.domain.CispConsent;
import com.capgemini.psd2.enums.ConsentStatusEnum;
import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.product.common.CompatibleVersionList;
import com.capgemini.psd2.utilities.GenerateUniqueIdUtilities;

/**
 * The Class CispConsentAdapterImpl.
 */
@Component
public class CispConsentAdapterImpl implements CispConsentAdapter {
	
	private static final Logger LOG = LoggerFactory.getLogger(CispConsentAdapterImpl.class);

	/** The cisp consent repository. */
	@Autowired
	private CispConsentMongoRepository cispConsentMongoRepository;

	@Autowired
	private MongoTemplate mongoTemplate;
	
	@Autowired
	private CompatibleVersionList compatibleVersionList;
	
	@Override
	public void createConsent(CispConsent cispConsent) {
		CispConsent consent = null;
		try {
			consent = retrieveConsentByFundsIntentId(cispConsent.getFundsIntentId());
			if (consent != null) {
				if (ConsentStatusEnum.AWAITINGAUTHORISATION.equals(consent.getStatus())) {
					/*
					 * Since only one version of Consent is deployed, to maintain backward compatibility
					 * we need to set the status according to the CMA Version.
					 */
					if (cispConsent.getCmaVersion() == null || cispConsent.getCmaVersion().equals(PSD2Constants.CMA3))
						consent.setStatus(ConsentStatusEnum.REVOKED);
					else 
						consent.setStatus(ConsentStatusEnum.DELETED);
					cispConsentMongoRepository.save(consent);
				} else if (ConsentStatusEnum.AUTHORISED.equals(consent.getStatus())) {
					throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.INTENT_ID_VALIDATION_ERROR);
				}
			}
			if (cispConsent.getAccountDetails() == null) {
				throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.NO_ACCOUNT_DETAILS_FOUND);
			}
			cispConsent.setConsentId(GenerateUniqueIdUtilities.generateRandomUniqueID());
			cispConsent.setStatus(ConsentStatusEnum.AWAITINGAUTHORISATION);
			cispConsentMongoRepository.save(cispConsent);
		} catch (DataAccessResourceFailureException e) {
			throw PSD2Exception.populatePSD2Exception(e.getMessage(), ErrorCodeEnum.CONNECTION_ERROR);
		}
	}

	@Override
	public CispConsent retrieveConsentByFundsIntentIdAndStatus(String fundsIntentId, ConsentStatusEnum status) {
		CispConsent consent = null;
		try {
			consent = cispConsentMongoRepository.findByFundsIntentIdAndStatusAndCmaVersionIn(fundsIntentId, status, compatibleVersionList.fetchVersionList());
		} catch (DataAccessResourceFailureException e) {
			throw PSD2Exception.populatePSD2Exception(e.getMessage(), ErrorCodeEnum.CONNECTION_ERROR);
		}
		return consent;
	}

	@Override
	public void updateConsentStatus(String fundsCheckConsentId, ConsentStatusEnum statusEnum) {
		try {
			Query query = new Query(Criteria.where("consentId").is(fundsCheckConsentId));
			Update update = new Update();
			update.set("status", statusEnum);

			mongoTemplate.updateFirst(query, update, CispConsent.class);
		} catch (DataAccessResourceFailureException e) {
			throw PSD2Exception.populatePSD2Exception(e.getMessage(), ErrorCodeEnum.CONNECTION_ERROR);
		}
	}

	@Override
	public CispConsent retrieveConsent(String fundsCheckConsentId) {
		CispConsent consent = null;
		try {
			consent = cispConsentMongoRepository.findByConsentIdAndCmaVersionIn(fundsCheckConsentId, compatibleVersionList.fetchVersionList());
		} catch (DataAccessResourceFailureException e) {
			throw PSD2Exception.populatePSD2Exception(e.getMessage(), ErrorCodeEnum.CONNECTION_ERROR);
		}
		if (consent == null)
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.NO_CONSENT_FOUND);
		return consent;
	}

	@Override
	public List<CispConsent> retrieveConsentByPsuIdAndConsentStatus(String psuId, ConsentStatusEnum statusEnum) {

		List<CispConsent> consentList = null;
		try {
			if (null == statusEnum) {
				consentList = cispConsentMongoRepository.findByPsuIdAndCmaVersionIn(psuId , compatibleVersionList.fetchVersionList());
			} else {
				consentList = cispConsentMongoRepository.findByPsuIdAndStatusAndCmaVersionIn(psuId, statusEnum, compatibleVersionList.fetchVersionList());
			}

		} catch (DataAccessResourceFailureException e) {
			throw PSD2Exception.populatePSD2Exception(e.getMessage(), ErrorCodeEnum.CONNECTION_ERROR);
		}

		return consentList;

	}
	
	@Override
	public List<CispConsent> retrieveConsentByPsuIdConsentStatusAndTenantId(String psuId, ConsentStatusEnum statusEnum, String tenantId) {

		LOG.info(psuId+"statusenum"+statusEnum+"tenantID"+tenantId+"version"+compatibleVersionList.fetchVersionList());
		List<CispConsent> consentList = null;
		try {
			if (null == statusEnum) {
				consentList = cispConsentMongoRepository.findByPsuIdAndTenantIdAndCmaVersionIn(psuId, tenantId, compatibleVersionList.fetchVersionList());
			} else {
				consentList = cispConsentMongoRepository.findByPsuIdAndStatusAndTenantIdAndCmaVersionIn(psuId, statusEnum, tenantId, compatibleVersionList.fetchVersionList());
			}

		} catch (DataAccessResourceFailureException e) {
			throw PSD2Exception.populatePSD2Exception(e.getMessage(), ErrorCodeEnum.CONNECTION_ERROR);
			
		}

		return consentList;
	}

	@Override
	public void updateConsentStatusWithResponse(String fundsCheckConsentId, ConsentStatusEnum statusEnum) {
		try {
			Query query = new Query(Criteria.where("consentId").is(fundsCheckConsentId));
			Update update = new Update();
			update.set("status", statusEnum);

			mongoTemplate.updateFirst(query, update, CispConsent.class);
		} catch (DataAccessResourceFailureException e) {
			throw PSD2Exception.populatePSD2Exception(e.getMessage(), ErrorCodeEnum.CONNECTION_ERROR);
		}
	}

	@Override
	public CispConsent retrieveConsentByFundsIntentId(String fundsIntentId) {
		CispConsent consent = null;
		try {
			consent = cispConsentMongoRepository.findByFundsIntentIdAndCmaVersionIn(fundsIntentId, compatibleVersionList.fetchVersionList());
		} catch (DataAccessResourceFailureException e) {
			throw PSD2Exception.populatePSD2Exception(e.getMessage(), ErrorCodeEnum.CONNECTION_ERROR);
		}
		return consent;
	}

}
