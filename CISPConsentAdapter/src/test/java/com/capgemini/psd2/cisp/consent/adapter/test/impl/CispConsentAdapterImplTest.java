/******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.cisp.consent.adapter.test.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.cisp.consent.adapter.impl.CispConsentAdapterImpl;
import com.capgemini.psd2.cisp.consent.adapter.repository.CispConsentMongoRepository;
import com.capgemini.psd2.cisp.consent.adapter.test.data.CispConsentAdapterMockData;
import com.capgemini.psd2.consent.domain.CispConsent;
import com.capgemini.psd2.enums.ConsentStatusEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.product.common.CompatibleVersionList;


/**
 * The Class ConsentMappingAdapterImplTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class CispConsentAdapterImplTest {

	/** The consent mapping repository. */
	@Mock
	private CispConsentMongoRepository cispConsentMongoRepository;

	/** The req header attributes. */
	@Mock
	private RequestHeaderAttributes reqHeaderAttributes;

	@Mock
	private CompatibleVersionList compatibleVersionList;

	@Mock
	private MongoTemplate mongoTemplate;

	@Mock
	private CispConsent cispConsent;

	/** The consent mapping adapter impl. */
	@InjectMocks
	private CispConsentAdapterImpl cispConsentAdapterImpl = new CispConsentAdapterImpl();

	/**
	 * Sets the up.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * Test retrieve consent success flow.
	 */
	@Test
	public void testRetrieveConsentSuccessFlow() {
		List<String> versionList = new ArrayList<>();
		versionList.add("2.0");
		when(compatibleVersionList.fetchVersionList()).thenReturn(versionList);
		when(cispConsentMongoRepository.findByConsentIdAndCmaVersionIn(anyString(),any()))
		.thenReturn(CispConsentAdapterMockData.getConsentMockData());
		CispConsent consent = cispConsentAdapterImpl.retrieveConsent("a1e590ad-73dc-453a-841e-2a2ef055e878");
		assertEquals(CispConsentAdapterMockData.getConsentMockData().getPsuId(),
				consent.getPsuId());
		assertEquals(CispConsentAdapterMockData.getConsentMockData().getConsentId(),
				consent.getConsentId());
	}

	/**
	 * Test retrieve consent data access resource failure exception.
	 */
	@Test(expected = PSD2Exception.class)
	public void testRetrieveConsentDataAccessResourceFailureException() {
		List<String> versionList = new ArrayList<>();
		versionList.add("2.0");
		when(compatibleVersionList.fetchVersionList()).thenReturn(versionList);
		when(cispConsentMongoRepository.findByConsentIdAndCmaVersionIn(anyString(),any())).thenThrow(new DataAccessResourceFailureException("Test"));
		cispConsentAdapterImpl.retrieveConsent("a1e590ad-73dc-453a-841e-2a2ef055e878");
	}

	/**
	 * Test retrieve consent null exception.
	 */
	@Test(expected = PSD2Exception.class)
	public void testRetrieveConsentNullException() {
		List<String> versionList = new ArrayList<>();
		versionList.add("2.0");
		when(compatibleVersionList.fetchVersionList()).thenReturn(versionList);
		when(cispConsentMongoRepository.findByConsentIdAndCmaVersionIn(anyString(),any())).thenReturn(null);
		cispConsentAdapterImpl.retrieveConsent("a1e590ad-73dc-453a-841e-2a2ef055e878");
	}

	@Test 
	public void testCreateConsent(){
		when(cispConsentMongoRepository.save(any(CispConsent.class))).thenReturn(CispConsentAdapterMockData.getConsentMockData());
		CispConsent cispConsent=CispConsentAdapterMockData.getConsentMockData();
		cispConsentAdapterImpl.createConsent(cispConsent);
	}
	
	

	@SuppressWarnings("unchecked")
	@Test(expected = PSD2Exception.class)
	public void testCreateConsentWithException(){
		List<String> versionList = new ArrayList<>();
		versionList.add("2.0");
		when(compatibleVersionList.fetchVersionList()).thenReturn(versionList);
		when(cispConsentMongoRepository.findByFundsIntentIdAndStatusAndCmaVersionIn(anyString(),any(),any())).thenReturn(CispConsentAdapterMockData.getConsentMockData());

		when(cispConsentMongoRepository.save(any(CispConsent.class))).thenThrow(DataAccessResourceFailureException.class);
		cispConsentAdapterImpl.createConsent(CispConsentAdapterMockData.getConsentMockData());
	}


	@SuppressWarnings("unchecked")
	@Test(expected = PSD2Exception.class)
	public void testConsentAlreadyExist(){
		List<String> versionList = new ArrayList<>();
		versionList.add("2.0");
		when(compatibleVersionList.fetchVersionList()).thenReturn(versionList);
		when(cispConsentMongoRepository.findByFundsIntentIdAndStatusAndCmaVersionIn(anyString(),any(),any())).thenReturn(CispConsentAdapterMockData.getConsentMockData());

		when(cispConsentMongoRepository.save(any(CispConsent.class))).thenThrow(PSD2Exception.class);
		CispConsent cispConsent=CispConsentAdapterMockData.getConsentMockData();
		cispConsentAdapterImpl.createConsent(cispConsent);
	}

	@Test(expected = PSD2Exception.class)
	public void testNoAccountDetails(){
		List<String> versionList = new ArrayList<>();
		versionList.add("2.0");
		when(compatibleVersionList.fetchVersionList()).thenReturn(versionList);
		when(cispConsentMongoRepository.findByFundsIntentIdAndStatusAndCmaVersionIn(anyString(),any(),any())).thenReturn(null);
		CispConsent cispConsent=CispConsentAdapterMockData.getConsentMockDataWithOutAccountDetails();
		cispConsentAdapterImpl.createConsent(cispConsent);
	}
	
	@Test(expected = PSD2Exception.class)
	public void testNoAccountDetails2WithCmaVersion3_0(){
		List<String> versionList = new ArrayList<>();
		versionList.add("3.0");
		when(compatibleVersionList.fetchVersionList()).thenReturn(versionList);
		CispConsent consent = CispConsentAdapterMockData.getConsentMockData();
		consent.setStatus(ConsentStatusEnum.AWAITINGAUTHORISATION);
		consent.setCmaVersion(PSD2Constants.CMA3);
		when(cispConsentMongoRepository.findByFundsIntentIdAndCmaVersionIn(anyString(),any())).thenReturn(consent);
		cispConsentAdapterImpl.createConsent(cispConsent);
	}
	
	@Test(expected = PSD2Exception.class)
	public void testNoAccountDetails2WithCmaVersion3_1(){
		List<String> versionList = new ArrayList<>();
		versionList.add("3.1");
		when(compatibleVersionList.fetchVersionList()).thenReturn(versionList);
		CispConsent consent = CispConsentAdapterMockData.getConsentMockData();
		consent.setStatus(ConsentStatusEnum.AWAITINGAUTHORISATION);
		consent.setCmaVersion("3.1");
		when(cispConsentMongoRepository.findByFundsIntentIdAndCmaVersionIn(anyString(),any())).thenReturn(consent);
		cispConsentAdapterImpl.createConsent(cispConsent);
	}
	
	@Test(expected = PSD2Exception.class)
	public void testNoAccountDetails3(){
		List<String> versionList = new ArrayList<>();
		versionList.add("2.0");
		when(compatibleVersionList.fetchVersionList()).thenReturn(versionList);
		CispConsent consent = CispConsentAdapterMockData.getConsentMockData();
		consent.setStatus(ConsentStatusEnum.AUTHORISED);
		when(cispConsentMongoRepository.findByFundsIntentIdAndCmaVersionIn(anyString(),any())).thenReturn(consent);
		cispConsentAdapterImpl.createConsent(cispConsent);
	}

	@Test
	public void testRetrieveConsentByAccountRequestIdSuccessFlow(){
		List<String> versionList = new ArrayList<>();
		versionList.add("2.0");
		when(compatibleVersionList.fetchVersionList()).thenReturn(versionList);
		CispConsent consent = CispConsentAdapterMockData.getConsentMockData();
		when(cispConsentMongoRepository.findByFundsIntentIdAndStatusAndCmaVersionIn(anyString(),any(),any())).thenReturn(consent);
		assertEquals(consent.getPsuId(),cispConsentAdapterImpl.retrieveConsentByFundsIntentIdAndStatus("1234",ConsentStatusEnum.AWAITINGAUTHORISATION).getPsuId());
	}

	@SuppressWarnings("unchecked")
	@Test(expected=PSD2Exception.class)
	public void testRetrieveConsentByAccountRequestIdDataAccessResourceFailureException(){
		List<String> versionList = new ArrayList<>();
		versionList.add("2.0");
		when(compatibleVersionList.fetchVersionList()).thenReturn(versionList);
		when(cispConsentMongoRepository.findByFundsIntentIdAndStatusAndCmaVersionIn(anyString(),any(),any())).thenThrow(DataAccessResourceFailureException.class);
		cispConsentAdapterImpl.retrieveConsentByFundsIntentIdAndStatus("1234",ConsentStatusEnum.AWAITINGAUTHORISATION);
	}

		@SuppressWarnings("unchecked")
	@Test
	public void testUpdateConsentStatusPSD2Exception(){
		List<String> versionList = new ArrayList<>();
		versionList.add("2.0");
		when(compatibleVersionList.fetchVersionList()).thenReturn(versionList);
		CispConsent consent = CispConsentAdapterMockData.getConsentMockData();
		when(cispConsentMongoRepository.findByConsentIdAndCmaVersionIn(anyString(),any())).thenReturn(consent);
		when(cispConsentMongoRepository.save(any(CispConsent.class))).thenThrow(PSD2Exception.class);
		cispConsentAdapterImpl.updateConsentStatus("1234",ConsentStatusEnum.AUTHORISED);
	}

		@SuppressWarnings("unchecked")
	@Test
	public void testUpdateConsentStatusException(){
		List<String> versionList = new ArrayList<>();
		versionList.add("2.0");
		when(compatibleVersionList.fetchVersionList()).thenReturn(versionList);
		CispConsent consent = CispConsentAdapterMockData.getConsentMockData();
		when(cispConsentMongoRepository.findByConsentIdAndCmaVersionIn(anyString(),any())).thenReturn(consent);
		when(cispConsentMongoRepository.save(any(CispConsent.class))).thenThrow(DataAccessResourceFailureException.class);
		cispConsentAdapterImpl.updateConsentStatus("1234",ConsentStatusEnum.AUTHORISED);
	}

	@Test
	public void testRetrieveConsentByPsuIdAndConsentStatusNull(){
		CispConsent consent = CispConsentAdapterMockData.getConsentMockData();
		List<CispConsent> consentList = new ArrayList<>();
		consentList.add(consent);
		List<String> versionList = new ArrayList<>();
		versionList.add("2.0");
		when(compatibleVersionList.fetchVersionList()).thenReturn(versionList);
		when(cispConsentMongoRepository.findByPsuIdAndCmaVersionIn(anyString(),any())).thenReturn(consentList);
		cispConsentAdapterImpl.retrieveConsentByPsuIdAndConsentStatus("1234",null);
	}

	@SuppressWarnings("unchecked")
	@Test(expected = PSD2Exception.class)
	public void testRetrieveConsentByPsuIdAndConsentStatusException(){
		CispConsent consent = CispConsentAdapterMockData.getConsentMockData();
		List<CispConsent> consentList = new ArrayList<>();
		consentList.add(consent);
		List<String> versionList = new ArrayList<>();
		versionList.add("2.0");
		when(compatibleVersionList.fetchVersionList()).thenReturn(versionList);
		when(cispConsentMongoRepository.findByPsuIdAndCmaVersionIn(anyString(),any())).thenThrow(DataAccessResourceFailureException.class);
		cispConsentAdapterImpl.retrieveConsentByPsuIdAndConsentStatus("1234", null);
	}

	@Test
	public void testRetrieveConsentByPsuIdAndConsentStatus(){
		CispConsent consent = CispConsentAdapterMockData.getConsentMockData();
		List<CispConsent> consentList = new ArrayList<>();
		consentList.add(consent);
		List<String> versionList = new ArrayList<>();
		versionList.add("2.0");
		when(compatibleVersionList.fetchVersionList()).thenReturn(versionList);
		when(cispConsentMongoRepository.findByPsuIdAndStatusAndCmaVersionIn(anyString(),anyObject(),any())).thenReturn(consentList);
		cispConsentAdapterImpl.retrieveConsentByPsuIdAndConsentStatus("1234",ConsentStatusEnum.AUTHORISED);
	}

	@Test
	public void testUpdateConsentStatusWithResponse(){
		List<String> versionList = new ArrayList<>();
		versionList.add("2.0");
		when(compatibleVersionList.fetchVersionList()).thenReturn(versionList);
		CispConsent consent = CispConsentAdapterMockData.getConsentMockData();
		when(cispConsentMongoRepository.findByConsentIdAndCmaVersionIn(anyString(),any())).thenReturn(consent);
		cispConsentAdapterImpl.updateConsentStatusWithResponse("12345", ConsentStatusEnum.AUTHORISED);
	}

	@Test
	public void retrieveConsentByFundsIntentId(){
		cispConsentAdapterImpl.retrieveConsentByFundsIntentId("fundsIntentId");
	}

	@Test
	public void retrieveConsentByPsuIdConsentStatusAndTenantIdTest(){
		cispConsentAdapterImpl.retrieveConsentByPsuIdConsentStatusAndTenantId("32118465", ConsentStatusEnum.AUTHORISED, "BOIROI");
	}

	@Test
	public void retrieveConsentByPsuIdConsentStatusAndTenantIdNullTest(){
		cispConsentAdapterImpl.retrieveConsentByPsuIdConsentStatusAndTenantId("32118465", null, "BOIROI");
	}

	@Test(expected = Exception.class)
	public void retrieveConsentByPsuIdConsentStatusAndTenantIdNullExceptionTest(){
		doThrow(new DataAccessResourceFailureException("msg")).when(cispConsentMongoRepository).findByPsuIdAndTenantIdAndCmaVersionIn("32118465", "BOIROI", compatibleVersionList.fetchVersionList());
		cispConsentAdapterImpl.retrieveConsentByPsuIdConsentStatusAndTenantId("32118465", null, "BOIROI");
	}

	@Test
	public void updateConsentStatusTest(){
		cispConsentAdapterImpl.updateConsentStatus("123445", ConsentStatusEnum.AUTHORISED);
	}

	@Test(expected = Exception.class)
	public void retrieveConsentByPsuIdConsentStatusAndTenantIdExceptionTest(){
		
		List<String> list = new ArrayList<>();
		doThrow(new DataAccessResourceFailureException("msg")).when(cispConsentMongoRepository).findByPsuIdAndStatusAndTenantIdAndCmaVersionIn("32118465",ConsentStatusEnum.AUTHORISED , "BOIROI",list );
		cispConsentAdapterImpl.retrieveConsentByPsuIdConsentStatusAndTenantId("32118465", ConsentStatusEnum.AUTHORISED, "BOIROI");
	}
	
	@Test(expected = Exception.class)
	public void retrieveConsentByFundsIntentId1ExceptionTest(){
		doThrow(new DataAccessResourceFailureException("msg")).when(cispConsentMongoRepository).findByFundsIntentIdAndCmaVersionIn("32118465", compatibleVersionList.fetchVersionList());
		cispConsentAdapterImpl.retrieveConsentByFundsIntentId("32118465");
	}




@SuppressWarnings("unchecked")
@Test(expected = PSD2Exception.class)
	public void retrieveConsentByFundsIntentId1(){
		when(cispConsentMongoRepository.findByFundsIntentIdAndCmaVersionIn("fundsIntentId", new ArrayList<String>())).thenThrow(DataAccessResourceFailureException.class);
		cispConsentAdapterImpl.retrieveConsentByFundsIntentId("fundsIntentId");
	}

	/*@Test(expected = PSD2Exception.class)
	public void updateConsentStatusWithResponse(){
		Query query = new Query(Criteria.where("consentId").is(true));
		Update update = new Update();
		update.set("status", "sdjfdjs");
		when(mongoTemplate.updateFirst(query, update, CispConsent.class)).thenThrow(DataAccessResourceFailureException.class);
		cispConsentAdapterImpl.updateConsentStatusWithResponse("fundsCheckConsentId", ConsentStatusEnum.AWAITINGAUTHORISATION);
	}*/


}
