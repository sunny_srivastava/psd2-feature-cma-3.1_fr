package com.capgemini.psd2.account.transaction.routing.adapter.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.account.transaction.routing.adapter.routing.AccountTransactionAdapterFactory;
import com.capgemini.psd2.aisp.adapter.AccountTransactionAdapter;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountTransactionResponse;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Component("accountTransactionRoutingAdapter")
public class AccountTransactionRoutingAdapter implements AccountTransactionAdapter {

	@Autowired
	private AccountTransactionAdapterFactory adapters;
	
	@Autowired
	private RequestHeaderAttributes reqHeaderAtrributes;

	@Value("${app.defaultAdapterAccountTransaction}")
	private String defaultAdapter;

	@Override
	public PlatformAccountTransactionResponse retrieveAccountTransaction(AccountMapping accountMapping,
			Map<String, String> params) {
		AccountTransactionAdapter accountTransactionAdapter = adapters.getAdapterInstance(defaultAdapter);
		return accountTransactionAdapter.retrieveAccountTransaction(accountMapping, addHeaderParams(params));
	}

	private Map<String, String> addHeaderParams(Map<String, String> mapParam) {
		Map<String,String> mapParamResult;
		if (NullCheckUtils.isNullOrEmpty(mapParam))
			mapParamResult = new HashMap<>();
		else mapParamResult=mapParam;

		if (null != reqHeaderAtrributes.getToken() && null != reqHeaderAtrributes.getToken().getSeviceParams()) {
			mapParamResult.put(PSD2Constants.CHANNEL_IN_REQ_HEADER,
					reqHeaderAtrributes.getToken().getSeviceParams().get(PSD2Constants.CHANNEL_NAME));
		}
		mapParamResult.put(PSD2Constants.USER_IN_REQ_HEADER, reqHeaderAtrributes.getPsuId());
		mapParamResult.put(PSD2Constants.CORRELATION_REQ_HEADER, reqHeaderAtrributes.getCorrelationId());
		mapParamResult.put(PSD2Constants.TENANT_ID, reqHeaderAtrributes.getTenantId());
		return mapParamResult;
	}

}