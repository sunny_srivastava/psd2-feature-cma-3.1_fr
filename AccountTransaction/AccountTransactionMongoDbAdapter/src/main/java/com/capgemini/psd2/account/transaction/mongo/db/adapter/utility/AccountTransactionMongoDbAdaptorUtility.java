package com.capgemini.psd2.account.transaction.mongo.db.adapter.utility;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Map;
import java.util.TimeZone;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.account.transaction.mongo.db.adapter.constants.AccountTransactionMongoDbAdapterConstants;
import com.capgemini.psd2.account.transaction.mongo.db.adapter.domain.TransactionDateRange;
import com.capgemini.psd2.exceptions.ErrorMapKeys;
import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.utilities.DateUtilites;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Component
public class AccountTransactionMongoDbAdaptorUtility {

	@Value("${app.defaultTransactionDateRange}")
	private int defaultTransactionDateRange;
	
	public TransactionDateRange createTransactionDateRange(Map<String, String> params) 
	{
		String fromConsentDtString = params.get(AccountTransactionMongoDbAdapterConstants.REQUESTED_FROM_CONSENT_DATETIME);
		String toConsentDtString = params.get(AccountTransactionMongoDbAdapterConstants.REQUESTED_TO_CONSENT_DATETIME);
		String fromFilterDtString = params.get(AccountTransactionMongoDbAdapterConstants.REQUESTED_FROM_DATETIME);
		String toFilterDtString = params.get(AccountTransactionMongoDbAdapterConstants.REQUESTED_TO_DATETIME);
		String expirationDtString = params.get(AccountTransactionMongoDbAdapterConstants.CONSENT_EXPIRATION_DATETIME);

		TransactionDateRange transactionDateRange = new TransactionDateRange();	

		//Consent Expiry 
		if (NullCheckUtils.isNullOrEmpty(expirationDtString)) {
			Calendar calendar = Calendar.getInstance();
			calendar.add(Calendar.DATE, 1);
			transactionDateRange.setConsentExpiryDateTime(toLocalDateTime(calendar));
		} else {
			transactionDateRange.setConsentExpiryDateTime(LocalDateTime.parse(expirationDtString));
		}

		//TransactionFromDateTime
		if (NullCheckUtils.isNullOrEmpty(fromConsentDtString)) {
			Calendar calendar = Calendar.getInstance();
			calendar.add(Calendar.MONTH, defaultTransactionDateRange);
			calendar.set(Calendar.HOUR_OF_DAY, 0);
			calendar.set(Calendar.SECOND, 0);
			calendar.set(Calendar.MINUTE, 0);
			transactionDateRange.setTransactionFromDateTime(toLocalDateTime(calendar));
		} else {
			transactionDateRange.setTransactionFromDateTime(LocalDateTime.parse(fromConsentDtString));
		}

		//TransactionToDateTime
		if (NullCheckUtils.isNullOrEmpty(toConsentDtString)) {
			Calendar calendar = Calendar.getInstance();
			calendar.set(Calendar.HOUR_OF_DAY, 0);
			calendar.set(Calendar.SECOND, 0);
			calendar.set(Calendar.MINUTE, 0);
			transactionDateRange.setTransactionToDateTime(toLocalDateTime(calendar));
		} else {
			transactionDateRange.setTransactionToDateTime(LocalDateTime.parse(toConsentDtString));
		}
		//Request DateTime
		transactionDateRange.setRequestDateTime(LocalDateTime.now());
		//FilterFromDate
		if (NullCheckUtils.isNullOrEmpty(params.get(AccountTransactionMongoDbAdapterConstants.REQUESTED_FROM_DATETIME))) {		
			transactionDateRange.setFilterFromDate(transactionDateRange.getTransactionFromDateTime());
		} else {
			transactionDateRange.setFilterFromDate(getISOParamDate(fromFilterDtString));
			
		}
		//FilterToDate
		if (NullCheckUtils.isNullOrEmpty(params.get(AccountTransactionMongoDbAdapterConstants.REQUESTED_TO_DATETIME))) {
			transactionDateRange.setFilterToDate(transactionDateRange.getTransactionToDateTime());
		} else {
			transactionDateRange.setFilterToDate(getISOParamDate(toFilterDtString));
		}
		//To > ConExp
		if (transactionDateRange.getTransactionToDateTime().compareTo(transactionDateRange.getConsentExpiryDateTime())==0) {
			transactionDateRange.setTransactionToDateTime(transactionDateRange.getConsentExpiryDateTime());
		}
		return transactionDateRange;
	}

	private LocalDateTime getISOParamDate(String dateTime) {
		dateTime= dateTime.trim();//NOSONAR
		
		//"+" sign in query param gets converted to " ", so replacing it to avoid parse exception
		if (dateTime.contains(" ")) {
			dateTime = dateTime.replace(' ', '+');//NOSONAR
		}
		return DateUtilites.getISODateFromString(dateTime);
	}

	private static LocalDateTime toLocalDateTime(Calendar calendar) {
		if (calendar == null) {
			return null;
		}
		TimeZone tz = calendar.getTimeZone();
		ZoneId zid = tz == null ? ZoneId.systemDefault() : tz.toZoneId();
		return LocalDateTime.ofInstant(calendar.toInstant(), zid);
	}
	//TransactionDateRange Validation
	public TransactionDateRange fsCallFilter(TransactionDateRange transactionDateRange) {

		if (transactionDateRange.getRequestDateTime().isAfter(transactionDateRange.getConsentExpiryDateTime())
				|| transactionDateRange.getFilterFromDate().isAfter(transactionDateRange.getFilterToDate())) {
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_INVALID_DATES, ErrorMapKeys.INVALID_DATE_RANGE));
		}
		else if (transactionDateRange.getTransactionFromDateTime().isAfter(transactionDateRange.getConsentExpiryDateTime())
				|| transactionDateRange.getTransactionFromDateTime().isAfter(transactionDateRange.getFilterToDate())
				|| transactionDateRange.getFilterFromDate().isAfter(transactionDateRange.getTransactionToDateTime())) {
			transactionDateRange.setEmptyResponse(true);
		}
		else if (transactionDateRange.getTransactionFromDateTime().isBefore(transactionDateRange.getFilterFromDate())
				||transactionDateRange.getTransactionFromDateTime().isEqual(transactionDateRange.getFilterFromDate())) {
			if (transactionDateRange.getFilterToDate().isBefore(transactionDateRange.getTransactionToDateTime())
					||transactionDateRange.getFilterToDate().isEqual(transactionDateRange.getTransactionToDateTime())) {
				transactionDateRange.setNewFilterFromDate(transactionDateRange.getFilterFromDate());
				transactionDateRange.setNewFilterToDate(transactionDateRange.getFilterToDate());

			} else {
				transactionDateRange.setNewFilterFromDate(transactionDateRange.getFilterFromDate());
				transactionDateRange.setNewFilterToDate(transactionDateRange.getTransactionToDateTime());
			}
		}
		else if (transactionDateRange.getFilterToDate().isBefore(transactionDateRange.getTransactionToDateTime())
				||transactionDateRange.getFilterToDate().isEqual(transactionDateRange.getTransactionToDateTime())) {
			transactionDateRange.setNewFilterFromDate(transactionDateRange.getTransactionFromDateTime());
			transactionDateRange.setNewFilterToDate(transactionDateRange.getFilterToDate());
		}
		else {
			transactionDateRange.setNewFilterFromDate(transactionDateRange.getTransactionFromDateTime());
			transactionDateRange.setNewFilterToDate(transactionDateRange.getTransactionToDateTime());
		}		
		return transactionDateRange;		
	}
}
