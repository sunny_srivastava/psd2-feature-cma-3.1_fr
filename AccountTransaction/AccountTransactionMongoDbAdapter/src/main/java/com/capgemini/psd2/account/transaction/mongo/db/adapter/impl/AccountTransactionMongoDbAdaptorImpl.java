package com.capgemini.psd2.account.transaction.mongo.db.adapter.impl;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.account.transaction.mongo.db.adapter.constants.AccountTransactionMongoDbAdapterConstants;
import com.capgemini.psd2.account.transaction.mongo.db.adapter.domain.TransactionDateRange;
import com.capgemini.psd2.account.transaction.mongo.db.adapter.repository.AccountTransactionRepository;
import com.capgemini.psd2.account.transaction.mongo.db.adapter.utility.AccountTransactionMongoDbAdaptorUtility;
import com.capgemini.psd2.aisp.adapter.AccountTransactionAdapter;
import com.capgemini.psd2.aisp.domain.Links;
import com.capgemini.psd2.aisp.domain.Meta;
import com.capgemini.psd2.aisp.domain.OBReadDataTransaction6;
import com.capgemini.psd2.aisp.domain.OBReadTransaction6;
import com.capgemini.psd2.aisp.domain.OBTransaction6;
import com.capgemini.psd2.aisp.mongo.db.adapter.domain.AccountTransactionCMA2;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountTransactionResponse;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.InternalServerErrorMessage;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.utilities.SandboxValidationUtility;

@Component
public class AccountTransactionMongoDbAdaptorImpl implements AccountTransactionAdapter {

	@Value("${app.isSandboxEnabled:false}")
	private boolean isSandboxEnabled;

	private static final Integer FIRST = Integer.valueOf(1);

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(AccountTransactionMongoDbAdaptorImpl.class);

	@Autowired
	private AccountTransactionRepository accountTransRepo;

	@Autowired
	private AccountTransactionMongoDbAdaptorUtility accountTransactionMongoDbAdaptorUtility;

	@Autowired
	private SandboxValidationUtility utility;

	@Override
	public PlatformAccountTransactionResponse retrieveAccountTransaction(AccountMapping accountMapping,
			Map<String, String> params) {

		PlatformAccountTransactionResponse platformAccountTransactionResponse = new PlatformAccountTransactionResponse();

		String accountId = accountMapping.getAccountDetails().get(0).getAccountId();
		String accountNumber = accountMapping.getAccountDetails().get(0).getAccountNumber();
		String accountNsc = accountMapping.getAccountDetails().get(0).getAccountNSC();

		LOG.info("TenantId : " + params.get(PSD2Constants.TENANT_ID));

		if (utility.isValidPsuId(accountMapping.getPsuId())) {
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
					InternalServerErrorMessage.SANDBOX_MOCKED_ERROR));
		}

		LocalDateTime fromLocalDateTime;
		LocalDateTime toLocalDateTime;
		TransactionDateRange transactionDateRange = accountTransactionMongoDbAdaptorUtility
				.createTransactionDateRange(params);
		TransactionDateRange decision = accountTransactionMongoDbAdaptorUtility.fsCallFilter(transactionDateRange);
		if (decision.isEmptyResponse()) {
			return new PlatformAccountTransactionResponse();
		} else {

			fromLocalDateTime = decision.getNewFilterFromDate();
			toLocalDateTime = decision.getNewFilterToDate();
		}

		LocalDateTime mongoFromDt = fromLocalDateTime.minusNanos(1);
		LocalDateTime mongoToDt = toLocalDateTime.plusNanos(1000000);

		String filter = params.get(AccountTransactionMongoDbAdapterConstants.REQUESTED_TXN_FILTER);
		Integer pageNo = Integer.parseInt(params.get("RequestedPageNumber"));
		Integer pageSize = Integer.parseInt(params.get("RequestedPageSize"));

		final PageRequest pageReq = new PageRequest(pageNo - 1, pageSize, Direction.ASC, "bookingDateTime");

		Page<AccountTransactionCMA2> pageRecords;
		if ("ALL".equals(filter)) {
			try {
				pageRecords = accountTransRepo.findByAccountNumberAndAccountNSCAndBookingDateTimeCopyBetween(
						accountNumber, accountNsc, mongoFromDt, mongoToDt, pageReq);

			} catch (DataAccessResourceFailureException e) {
				LOG.error(e.getMessage(),e);
				throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
						InternalServerErrorMessage.CONNECTION_ERROR));
			}
		} else {
			try {
				pageRecords = accountTransRepo
						.findByAccountNumberAndAccountNSCAndCreditDebitIndicatorAndBookingDateTimeCopyBetween(
								accountNumber, accountNsc, filter, mongoFromDt, mongoToDt, pageReq);
			} catch (DataAccessResourceFailureException e) {
				LOG.error(e.getMessage(),e);
				throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
						InternalServerErrorMessage.CONNECTION_ERROR));
			}
		}

		String txnKey = null;
		if (!pageRecords.getContent().isEmpty()) {
			pageRecords.getContent().forEach(mockResponseData1 -> mockResponseData1.setAccountId(accountId));
			AccountTransactionCMA2 mockResponseData1 = pageRecords.getContent().get(0);
			txnKey = mockResponseData1.getTxnRetrievalKey();
		}

		OBReadTransaction6 transactionResponsePreValidation = populateIdAndLinks(pageRecords, accountId, txnKey);

		platformAccountTransactionResponse.setoBReadTransaction6(transactionResponsePreValidation);
		return platformAccountTransactionResponse;
	}

	/**
	 * @param pageRecords
	 * @param accountId
	 * @param txnKey
	 * @return
	 */
	private OBReadTransaction6 populateIdAndLinks(Page<AccountTransactionCMA2> pageRecords, String accountId,
			String txnKey) {
		OBReadTransaction6 transactionResponse = new OBReadTransaction6();

		//
		transactionResponse.setLinks(new Links());
		transactionResponse.setMeta(new Meta());

		if (!pageRecords.getContent().isEmpty()) {
			pageRecords.getContent().get(0).setAccountId(accountId);
			if (!isSandboxEnabled) {
				transactionResponse.getMeta()
						.setFirstAvailableDateTime(pageRecords.getContent().get(0).getFirstAvailableDateTime());
				transactionResponse.getMeta()
						.setLastAvailableDateTime(pageRecords.getContent().get(0).getLastAvailableDateTime());
			}
			pageRecords.getContent().get(0).getLastAvailableDateTime();
		}

		List<AccountTransactionCMA2> mockTransaction = pageRecords.getContent();

		List<OBTransaction6> responseDataTransaction = new ArrayList<>();
		responseDataTransaction.addAll(mockTransaction);
		OBReadDataTransaction6 responseData = new OBReadDataTransaction6();
		responseData.setTransaction(responseDataTransaction);
		transactionResponse.setData(responseData);

		// Setting Links

		transactionResponse.getLinks().setFirst(String.valueOf(FIRST));
		if (txnKey != null)
			transactionResponse.getLinks().setSelf(String.valueOf(pageRecords.getNumber() + 1) + ":" + txnKey);
		else
			transactionResponse.getLinks().setSelf(String.valueOf(pageRecords.getNumber() + 1));
		if (!isSandboxEnabled) {
			transactionResponse.getLinks().setLast(String.valueOf(pageRecords.getTotalPages()));
			transactionResponse.getMeta().setTotalPages(pageRecords.getTotalPages());
		}
		if (pageRecords.hasNext())
			transactionResponse.getLinks().setNext(String.valueOf(pageRecords.getNumber() + 2));
		if (pageRecords.hasPrevious())
			transactionResponse.getLinks().setPrev(String.valueOf(pageRecords.getNumber()));

		return transactionResponse;
	}

}

enum CurrencyEnum {
	GBP, EUR;
}
