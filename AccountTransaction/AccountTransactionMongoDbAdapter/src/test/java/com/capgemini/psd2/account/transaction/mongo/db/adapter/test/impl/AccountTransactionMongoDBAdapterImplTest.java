package com.capgemini.psd2.account.transaction.mongo.db.adapter.test.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.time.LocalDateTime;
import java.time.Month;
import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.account.transaction.mongo.db.adapter.constants.AccountTransactionMongoDbAdapterConstants;
import com.capgemini.psd2.account.transaction.mongo.db.adapter.domain.TransactionDateRange;
import com.capgemini.psd2.account.transaction.mongo.db.adapter.impl.AccountTransactionMongoDbAdaptorImpl;
import com.capgemini.psd2.account.transaction.mongo.db.adapter.repository.AccountTransactionRepository;
import com.capgemini.psd2.account.transaction.mongo.db.adapter.test.mock.data.AccountTransactionTestData;
import com.capgemini.psd2.account.transaction.mongo.db.adapter.utility.AccountTransactionMongoDbAdaptorUtility;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountTransactionResponse;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.utilities.JSONUtilities;
import com.capgemini.psd2.utilities.SandboxValidationUtility;
import com.capgemini.psd2.validator.PSD2Validator;

/**
 * The Class AccountTransactionMongoDBAdapterImplTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class AccountTransactionMongoDBAdapterImplTest {

	/** The account info repo. */
	@Mock
	private AccountTransactionRepository accountTransactionRepository;
	
	@Mock
	private AccountTransactionMongoDbAdaptorUtility accountTransactionMongoDbAdaptorUtility;
	
	@Mock
	PSD2Validator psd2Validator;
	
	@Mock
	SandboxValidationUtility utility;
	
	

	/**
	 * Sets the up.
	 *
	 * @throws Exception
	 *             the exception
	 */
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}

	/** The account transaction mongo DB adapter impl. */
	@InjectMocks
	private AccountTransactionMongoDbAdaptorImpl accountTransactionMongoDBAdapterImpl;

	/**
	 * Test retrieve account transaction success flow.
	 */
	@Test
	public void testRetrieveAccountTransactionSuccessFlowNoFilter() {
		
		Mockito.when(utility.isValidPsuId("psuId")).thenReturn(false);
		when(accountTransactionRepository.findByAccountNumberAndAccountNSCAndBookingDateTimeCopyBetween(
				anyString(), anyString(), any(LocalDateTime.class), any(LocalDateTime.class), any(Pageable.class)))
				.thenReturn(AccountTransactionTestData.getAccountTransactionsDataPage());

		Map<String, String> params = new HashMap<>();
		params.put("RequestedPageNumber", "1");
		params.put(AccountTransactionMongoDbAdapterConstants.REQUESTED_FROM_DATETIME, "2015-01-01T00:00:00");
		params.put(AccountTransactionMongoDbAdapterConstants.REQUESTED_TO_DATETIME, "2017-01-01T00:00:00");
		params.put(AccountTransactionMongoDbAdapterConstants.REQUESTED_FROM_CONSENT_DATETIME,"2015-01-01T00:00:00" );
		params.put(AccountTransactionMongoDbAdapterConstants.REQUESTED_TO_CONSENT_DATETIME,"2018-01-01T00:00:00");
		params.put(AccountTransactionMongoDbAdapterConstants.CONSENT_EXPIRATION_DATETIME, "2020-01-01T00:00:00");
		params.put("RequestedPageSize", "10");
		LocalDateTime localDate = LocalDateTime.now();
		LocalDateTime localDate2 = LocalDateTime.of(2015, Month.MAY, 15, 10, 0);
		params.put(AccountTransactionMongoDbAdapterConstants.REQUESTED_TXN_FILTER, "ALL");
		TransactionDateRange transactionDateRange = new TransactionDateRange();
		transactionDateRange.setNewFilterFromDate(localDate2);
		transactionDateRange.setNewFilterToDate(localDate);
		
		when(accountTransactionMongoDbAdaptorUtility.fsCallFilter(null)).thenReturn(transactionDateRange);
		PlatformAccountTransactionResponse accountTransactionsGETResponse = accountTransactionMongoDBAdapterImpl
				.retrieveAccountTransaction(AccountTransactionTestData.getMockAccountMapping(), params);

		String actualResponseJson = JSONUtilities.getJSONOutPutFromObject(accountTransactionsGETResponse);
		/*String expectedResponseJson = "{\"Data\":{\"Transaction\": [{\"AccountId\": \"269c3ff5-d7f8-419b-a3b9-7136c5b4611a\",\"TransactionId\": \"123\",\"TransactionReference\": \"Ref123\",\"Amount\": {\"Amount\": \"10.00\",\"Currency\": \"GBP\"},\"CreditDebitIndicator\": \"Credit\",\"Status\": \"Booked\",\"BookingDateTime\": \"2017-04-05T00:00:00.000\",\"ValueDateTime\": \"2017-04-05T00:00:00.000\",\"TransactionInformation\": \"Cash from Aubrey\",\"AddressLine\": \"XYZ address Line\",\"BankTransactionCode\": {\"Code\": \"ReceivedCreditTransfer\",\"SubCode\": \"DomesticCreditTransfer\"},\"ProprietaryBankTransactionCode\": {\"Code\": \"Transfer\",\"Issuer\": \"AlphaBank\"},\"Balance\": {\"Amount\": {\"Amount\": \"230.00\",\"Currency\": \"GBP\"},\"CreditDebitIndicator\": \"Credit\",\"Type\": \"InterimBooked\"},\"MerchantDetails\": {\"MerchantName\": \"MerchantXYZ\",\"MerchantCategoryCode\": \"MerchantXYZcode\"}}]}}";
		JSONAssert.assertEquals(expectedResponseJson, actualResponseJson, Boolean.FALSE);
*/	}

	@Test
	public void testRetrieveAccountTransactionSuccessFlowWithFilter() {
		
		Mockito.when(utility.isValidPsuId("psuId")).thenReturn(false);
		Mockito.when(accountTransactionRepository
				.findByAccountNumberAndAccountNSCAndCreditDebitIndicatorAndBookingDateTimeCopyBetween(anyString(),
						anyString(), anyString(), any(LocalDateTime.class), any(LocalDateTime.class),
						any(Pageable.class)))
				.thenReturn(AccountTransactionTestData.getAccountTransactionsDataPage());

		Map<String, String> params = new HashMap<>();
		params.put("RequestedPageNumber", "2");
		params.put(AccountTransactionMongoDbAdapterConstants.REQUESTED_FROM_DATETIME, "2015-01-01T00:00:00");
		params.put(AccountTransactionMongoDbAdapterConstants.REQUESTED_TO_DATETIME, "2017-01-01T00:00:00");
		params.put(AccountTransactionMongoDbAdapterConstants.REQUESTED_FROM_CONSENT_DATETIME,"2015-01-01T00:00:00" );
		params.put(AccountTransactionMongoDbAdapterConstants.REQUESTED_TO_CONSENT_DATETIME,"2018-01-01T00:00:00");
		params.put(AccountTransactionMongoDbAdapterConstants.CONSENT_EXPIRATION_DATETIME, "2020-01-01T00:00:00");
		params.put(AccountTransactionMongoDbAdapterConstants.REQUESTED_TXN_FILTER, "DEBIT");
		params.put("RequestedPageSize", "10");
		LocalDateTime localDate = LocalDateTime.now();
		LocalDateTime localDate2 = LocalDateTime.of(2015, Month.MAY, 15, 10, 0);
		
		TransactionDateRange transactionDateRange = new TransactionDateRange();
		transactionDateRange.setNewFilterFromDate(localDate2);
		transactionDateRange.setNewFilterToDate(localDate);
		when(accountTransactionMongoDbAdaptorUtility.fsCallFilter(null)).thenReturn(transactionDateRange);
		
		PlatformAccountTransactionResponse accountTransactionsGETResponse = accountTransactionMongoDBAdapterImpl
				.retrieveAccountTransaction(AccountTransactionTestData.getMockAccountMapping(), params);
		

		String actualResponseJson = JSONUtilities.getJSONOutPutFromObject(accountTransactionsGETResponse);
//		String expectedResponseJson = "{\"Data\": {\"Transaction\":[{\"AccountId\": \"269c3ff5-d7f8-419b-a3b9-7136c5b4611a\",\"TransactionId\": \"123\",\"TransactionReference\": \"Ref123\",\"Amount\": {\"Amount\": \"10.00\",\"Currency\": \"GBP\"},\"CreditDebitIndicator\": \"Credit\",\"Status\": \"Booked\",\"BookingDateTime\": \"2017-04-05T00:00:00.000\",\"ValueDateTime\": \"2017-04-05T00:00:00.000\",\"TransactionInformation\": \"Cash from Aubrey\",\"AddressLine\": \"XYZ address Line\",\"BankTransactionCode\": {\"Code\": \"ReceivedCreditTransfer\",\"SubCode\": \"DomesticCreditTransfer\"},\"ProprietaryBankTransactionCode\": {\"Code\": \"Transfer\",\"Issuer\": \"AlphaBank\"},\"Balance\": {\"Amount\": {\"Amount\": \"230.00\",\"Currency\": \"GBP\"},\"CreditDebitIndicator\": \"Credit\",\"Type\": \"InterimBooked\"},\"MerchantDetails\": {\"MerchantName\": \"MerchantXYZ\",\"MerchantCategoryCode\": \"MerchantXYZcode\"}}]}}";
//		JSONAssert.assertEquals(expectedResponseJson, actualResponseJson, Boolean.FALSE);
	}

	
	 
	@Test(expected = PSD2Exception.class)
	public void testRetrieveAccountTransactionDataAccessResourceFailureExceptionWithNoFilter() {
		
		Mockito.when(utility.isValidPsuId("psuId")).thenReturn(false);
		Mockito.when(accountTransactionRepository.findByAccountNumberAndAccountNSCAndBookingDateTimeCopyBetween(
				anyString(), anyString(), any(LocalDateTime.class), any(LocalDateTime.class), any(Pageable.class)))
				.thenThrow(new DataAccessResourceFailureException("Test"));

		Map<String, String> params = new HashMap<>();
		params.put("RequestedPageNumber", "2");
		params.put(AccountTransactionMongoDbAdapterConstants.REQUESTED_FROM_DATETIME, "2015-01-01T00:00:00");
		params.put(AccountTransactionMongoDbAdapterConstants.REQUESTED_TO_DATETIME, "2017-01-01T00:00:00");
		params.put(AccountTransactionMongoDbAdapterConstants.REQUESTED_FROM_CONSENT_DATETIME,"2015-01-01T00:00:00" );
		params.put(AccountTransactionMongoDbAdapterConstants.REQUESTED_TO_CONSENT_DATETIME,"2018-01-01T00:00:00");
		params.put(AccountTransactionMongoDbAdapterConstants.CONSENT_EXPIRATION_DATETIME, "2020-01-01T00:00:00");
		params.put(AccountTransactionMongoDbAdapterConstants.REQUESTED_TXN_FILTER, "ALL");
		params.put("RequestedPageSize", "10");
		LocalDateTime localDate = LocalDateTime.now();
		LocalDateTime localDate2 = LocalDateTime.of(2015, Month.MAY, 15, 10, 0);
		
		TransactionDateRange transactionDateRange = new TransactionDateRange();
		transactionDateRange.setNewFilterFromDate(localDate2);
		transactionDateRange.setNewFilterToDate(localDate);
		when(accountTransactionMongoDbAdaptorUtility.fsCallFilter(null)).thenReturn(transactionDateRange);
		try {
			accountTransactionMongoDBAdapterImpl
					.retrieveAccountTransaction(AccountTransactionTestData.getMockAccountMapping(), params);
		/*} catch (PSD2Exception e) {
			assertEquals("Error while establishing connection,please try again..", e.getErrorInfo().getErrorMessage());
			throw e;
		}*/
		} catch (PSD2Exception e) {
			assertEquals("Technical error while establishing data base connection", e.getOBErrorResponse1().getErrors().get(0).getMessage());		
			throw e;
		}
	}

	@Test(expected = PSD2Exception.class)
	public void testRetrieveAccountTransactionDataAccessResourceFailureExceptionWithFilter() {
		
		Mockito.when(utility.isValidPsuId("psuId")).thenReturn(false);
		Mockito.when(accountTransactionRepository
				.findByAccountNumberAndAccountNSCAndCreditDebitIndicatorAndBookingDateTimeCopyBetween(anyString(),
						anyString(), anyString(), any(LocalDateTime.class), any(LocalDateTime.class),
						any(Pageable.class)))
				.thenThrow(new DataAccessResourceFailureException("Test"));

		Map<String, String> params = new HashMap<>();
		params.put("RequestedPageNumber", "2");
		params.put(AccountTransactionMongoDbAdapterConstants.REQUESTED_FROM_DATETIME, "2015-01-01T00:00:00");
		params.put(AccountTransactionMongoDbAdapterConstants.REQUESTED_TO_DATETIME, "2017-01-01T00:00:00");
		params.put(AccountTransactionMongoDbAdapterConstants.REQUESTED_FROM_CONSENT_DATETIME,"2015-01-01T00:00:00" );
		params.put(AccountTransactionMongoDbAdapterConstants.REQUESTED_TO_CONSENT_DATETIME,"2018-01-01T00:00:00");
		params.put(AccountTransactionMongoDbAdapterConstants.CONSENT_EXPIRATION_DATETIME, "2020-01-01T00:00:00");
		params.put(AccountTransactionMongoDbAdapterConstants.REQUESTED_TXN_FILTER, "CREDIT");
		params.put("RequestedPageSize", "10");
		LocalDateTime localDate = LocalDateTime.now();
		LocalDateTime localDate2 = LocalDateTime.of(2015, Month.MAY, 15, 10, 0);
		
		TransactionDateRange transactionDateRange = new TransactionDateRange();
		transactionDateRange.setNewFilterFromDate(localDate2);
		transactionDateRange.setNewFilterToDate(localDate);
		when(accountTransactionMongoDbAdaptorUtility.fsCallFilter(null)).thenReturn(transactionDateRange);
		try {
			
			
			
			accountTransactionMongoDBAdapterImpl
					.retrieveAccountTransaction(AccountTransactionTestData.getMockAccountMapping(), params);
		/*} catch (PSD2Exception e) {
			assertEquals("Error while establishing connection,please try again..", e.getErrorInfo().getErrorMessage());
			throw e;
		}*/
		} catch (PSD2Exception e) {
			assertEquals("Technical error while establishing data base connection", e.getOBErrorResponse1().getErrors().get(0).getMessage());		
			throw e;
		}

	}
    
	@Test(expected = PSD2Exception.class)
	public void testThrowExceptionForUtilityPsuidTrue() {
		
		Mockito.when(utility.isValidPsuId(any(String.class))).thenReturn(true);
		Mockito.when(accountTransactionRepository
				.findByAccountNumberAndAccountNSCAndCreditDebitIndicatorAndBookingDateTimeCopyBetween(anyString(),
						anyString(), anyString(), any(LocalDateTime.class), any(LocalDateTime.class),
						any(Pageable.class)))
				.thenReturn(AccountTransactionTestData.getAccountTransactionsDataPage());

		Map<String, String> params = new HashMap<>();
		params.put("RequestedPageNumber", "2");
		params.put(AccountTransactionMongoDbAdapterConstants.REQUESTED_FROM_DATETIME, "2015-01-01T00:00:00");
		params.put(AccountTransactionMongoDbAdapterConstants.REQUESTED_TO_DATETIME, "2017-01-01T00:00:00");
		params.put(AccountTransactionMongoDbAdapterConstants.REQUESTED_FROM_CONSENT_DATETIME,"2015-01-01T00:00:00" );
		params.put(AccountTransactionMongoDbAdapterConstants.REQUESTED_TO_CONSENT_DATETIME,"2018-01-01T00:00:00");
		params.put(AccountTransactionMongoDbAdapterConstants.CONSENT_EXPIRATION_DATETIME, "2020-01-01T00:00:00");
		params.put(AccountTransactionMongoDbAdapterConstants.REQUESTED_TXN_FILTER, "DEBIT");
		params.put("RequestedPageSize", "10");
		LocalDateTime localDate = LocalDateTime.now();
		LocalDateTime localDate2 = LocalDateTime.of(2015, Month.MAY, 15, 10, 0);
		
		TransactionDateRange transactionDateRange = new TransactionDateRange();
		transactionDateRange.setNewFilterFromDate(localDate2);
		transactionDateRange.setNewFilterToDate(localDate);
		when(accountTransactionMongoDbAdaptorUtility.fsCallFilter(null)).thenReturn(transactionDateRange);
		
		PlatformAccountTransactionResponse accountTransactionsGETResponse = accountTransactionMongoDBAdapterImpl
				.retrieveAccountTransaction(AccountTransactionTestData.getMockAccountMapping(), params);
		

//		String actualResponseJson = JSONUtilities.getJSONOutPutFromObject(accountTransactionsGETResponse);
//		String expectedResponseJson = "{\"Data\": {\"Transaction\":[{\"AccountId\": \"269c3ff5-d7f8-419b-a3b9-7136c5b4611a\",\"TransactionId\": \"123\",\"TransactionReference\": \"Ref123\",\"Amount\": {\"Amount\": \"10.00\",\"Currency\": \"GBP\"},\"CreditDebitIndicator\": \"Credit\",\"Status\": \"Booked\",\"BookingDateTime\": \"2017-04-05T00:00:00.000\",\"ValueDateTime\": \"2017-04-05T00:00:00.000\",\"TransactionInformation\": \"Cash from Aubrey\",\"AddressLine\": \"XYZ address Line\",\"BankTransactionCode\": {\"Code\": \"ReceivedCreditTransfer\",\"SubCode\": \"DomesticCreditTransfer\"},\"ProprietaryBankTransactionCode\": {\"Code\": \"Transfer\",\"Issuer\": \"AlphaBank\"},\"Balance\": {\"Amount\": {\"Amount\": \"230.00\",\"Currency\": \"GBP\"},\"CreditDebitIndicator\": \"Credit\",\"Type\": \"InterimBooked\"},\"MerchantDetails\": {\"MerchantName\": \"MerchantXYZ\",\"MerchantCategoryCode\": \"MerchantXYZcode\"}}]}}";
//		JSONAssert.assertEquals(expectedResponseJson, actualResponseJson, Boolean.FALSE);

	}







}
