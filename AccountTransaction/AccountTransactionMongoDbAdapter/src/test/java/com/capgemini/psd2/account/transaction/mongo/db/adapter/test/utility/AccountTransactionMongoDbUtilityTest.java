package com.capgemini.psd2.account.transaction.mongo.db.adapter.test.utility;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.account.transaction.mongo.db.adapter.constants.AccountTransactionMongoDbAdapterConstants;
import com.capgemini.psd2.account.transaction.mongo.db.adapter.domain.TransactionDateRange;
import com.capgemini.psd2.account.transaction.mongo.db.adapter.utility.AccountTransactionMongoDbAdaptorUtility;
import com.capgemini.psd2.exceptions.PSD2Exception;

@RunWith(SpringJUnit4ClassRunner.class)
public class AccountTransactionMongoDbUtilityTest {
	
	@InjectMocks
	AccountTransactionMongoDbAdaptorUtility accountTransactionMongoDbAdaptorUtility; 
	
	Map<String, String> params = new HashMap<>();
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void createTransactionDateRangeTestWithAllData() {
		params.put(AccountTransactionMongoDbAdapterConstants.REQUESTED_FROM_DATETIME, "2018-09-09T00:00:00-05:30");
		params.put(AccountTransactionMongoDbAdapterConstants.REQUESTED_TO_DATETIME, "2020-01-19T00:00:00.52Z");
		params.put(AccountTransactionMongoDbAdapterConstants.REQUESTED_FROM_CONSENT_DATETIME,"2015-01-01T00:00:00" );
		params.put(AccountTransactionMongoDbAdapterConstants.REQUESTED_TO_CONSENT_DATETIME,"2018-01-01T00:00:00");
		params.put(AccountTransactionMongoDbAdapterConstants.CONSENT_EXPIRATION_DATETIME, "2020-01-01T00:00:00");
		
		accountTransactionMongoDbAdaptorUtility.createTransactionDateRange(params);
	}
	
	@Test
	public void createTransactionDateRangeTestWithNullData() {
		
		accountTransactionMongoDbAdaptorUtility.createTransactionDateRange(params);
	}
	
	@Test
	public void createTransactionDateRangeTestWithTogreaterConsent() {
		params.put(AccountTransactionMongoDbAdapterConstants.REQUESTED_FROM_DATETIME, "2018-09-09T00:00:00-05:30");
		params.put(AccountTransactionMongoDbAdapterConstants.REQUESTED_TO_DATETIME, "2020-01-19T00:00:00.52Z");
		params.put(AccountTransactionMongoDbAdapterConstants.REQUESTED_FROM_CONSENT_DATETIME,"2015-01-01T00:00:00" );
		params.put(AccountTransactionMongoDbAdapterConstants.REQUESTED_TO_CONSENT_DATETIME,"2021-01-01T00:00:00");
		params.put(AccountTransactionMongoDbAdapterConstants.CONSENT_EXPIRATION_DATETIME, "2020-01-01T00:00:00");
		
		accountTransactionMongoDbAdaptorUtility.createTransactionDateRange(params);
	}
	
	@Test(expected = PSD2Exception.class)
	public void fsCallFilterExpiredConsent () {
		
		params.put(AccountTransactionMongoDbAdapterConstants.REQUESTED_FROM_DATETIME, "2018-09-09T00:00:00-05:30");
		params.put(AccountTransactionMongoDbAdapterConstants.REQUESTED_TO_DATETIME, "2021-01-01T00:00:00Z");
		params.put(AccountTransactionMongoDbAdapterConstants.REQUESTED_FROM_CONSENT_DATETIME,"2015-01-01T00:00:00" );
		params.put(AccountTransactionMongoDbAdapterConstants.REQUESTED_TO_CONSENT_DATETIME,"2021-01-01T00:00:00");
		params.put(AccountTransactionMongoDbAdapterConstants.CONSENT_EXPIRATION_DATETIME, "2018-01-01T00:00:00");
		
		TransactionDateRange transactionDateRange = accountTransactionMongoDbAdaptorUtility.createTransactionDateRange(params);
		accountTransactionMongoDbAdaptorUtility.fsCallFilter(transactionDateRange);
	}
	
	@Test(expected = PSD2Exception.class)
	public void fsCallFilterFromAfterConsent() {
		params.put(AccountTransactionMongoDbAdapterConstants.REQUESTED_FROM_DATETIME, "2020-01-01T00:00:00+05:30");
		params.put(AccountTransactionMongoDbAdapterConstants.REQUESTED_TO_DATETIME, "2021-01-01T00:00:00Z");
		params.put(AccountTransactionMongoDbAdapterConstants.REQUESTED_FROM_CONSENT_DATETIME,"2020-01-01T00:00:00" );
		params.put(AccountTransactionMongoDbAdapterConstants.REQUESTED_TO_CONSENT_DATETIME,"2021-01-01T00:00:00");
		params.put(AccountTransactionMongoDbAdapterConstants.CONSENT_EXPIRATION_DATETIME, "2019-01-01T00:00:00");
		
		TransactionDateRange transactionDateRange = accountTransactionMongoDbAdaptorUtility.createTransactionDateRange(params);
		accountTransactionMongoDbAdaptorUtility.fsCallFilter(transactionDateRange);
	}
	
	@Test
	public void fsCallFilterFromBeforeFromFilter() {
		params.put(AccountTransactionMongoDbAdapterConstants.REQUESTED_FROM_DATETIME, "2020-01-01T00:00:00+04:00");
		params.put(AccountTransactionMongoDbAdapterConstants.REQUESTED_TO_DATETIME, "2021-01-01T00:00:00Z");
		params.put(AccountTransactionMongoDbAdapterConstants.REQUESTED_FROM_CONSENT_DATETIME,"2019-01-01T00:00:00" );
		params.put(AccountTransactionMongoDbAdapterConstants.REQUESTED_TO_CONSENT_DATETIME,"2021-01-01T00:00:00");
		params.put(AccountTransactionMongoDbAdapterConstants.CONSENT_EXPIRATION_DATETIME, "2022-01-01T00:00:00");
		
		TransactionDateRange transactionDateRange = accountTransactionMongoDbAdaptorUtility.createTransactionDateRange(params);
		accountTransactionMongoDbAdaptorUtility.fsCallFilter(transactionDateRange);
	}
	
	@Test
	public void fsCallFilterToBeforeToFilter() {
		params.put(AccountTransactionMongoDbAdapterConstants.REQUESTED_FROM_DATETIME, "2019-01-01T00:00:00Z");
		params.put(AccountTransactionMongoDbAdapterConstants.REQUESTED_TO_DATETIME, "2020-01-01T00:00:00+03:00");
		params.put(AccountTransactionMongoDbAdapterConstants.REQUESTED_FROM_CONSENT_DATETIME,"2018-01-01T00:00:00" );
		params.put(AccountTransactionMongoDbAdapterConstants.REQUESTED_TO_CONSENT_DATETIME,"2022-01-01T00:00:00");
		params.put(AccountTransactionMongoDbAdapterConstants.CONSENT_EXPIRATION_DATETIME, "2023-01-01T00:00:00");
		
		TransactionDateRange transactionDateRange = accountTransactionMongoDbAdaptorUtility.createTransactionDateRange(params);
		accountTransactionMongoDbAdaptorUtility.fsCallFilter(transactionDateRange);
	}
}
