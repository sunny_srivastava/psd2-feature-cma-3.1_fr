package com.capgemini.psd2.account.transaction.mongo.db.adapter.test.mock.data;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import com.capgemini.psd2.aisp.domain.OBActiveOrHistoricCurrencyAndAmount9;
import com.capgemini.psd2.aisp.domain.OBBalanceType1Code;
//import com.capgemini.psd2.aisp.domain.OBTransaction2BalanceAmount;
import com.capgemini.psd2.aisp.domain.OBBankTransactionCodeStructure1;
import com.capgemini.psd2.aisp.domain.OBCreditDebitCode1;
import com.capgemini.psd2.aisp.domain.OBCreditDebitCode2;
import com.capgemini.psd2.aisp.domain.OBEntryStatus1Code;
import com.capgemini.psd2.aisp.domain.OBMerchantDetails1;
import com.capgemini.psd2.aisp.domain.OBTransactionCashBalance;
import com.capgemini.psd2.aisp.domain.OBTransactionCashBalanceAmount;
import com.capgemini.psd2.aisp.domain.ProprietaryBankTransactionCodeStructure1;
import com.capgemini.psd2.aisp.mongo.db.adapter.domain.AccountTransactionCMA2;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AccountMapping;

public class AccountTransactionTestData {

	static AccountTransactionCMA2 data = new AccountTransactionCMA2();
	static OBActiveOrHistoricCurrencyAndAmount9 amount = new OBActiveOrHistoricCurrencyAndAmount9();
	static OBTransactionCashBalance balance = new OBTransactionCashBalance();
	static OBTransactionCashBalanceAmount amount2 = new OBTransactionCashBalanceAmount();
	static OBBankTransactionCodeStructure1 bankTransactionCode = new OBBankTransactionCodeStructure1();
	static ProprietaryBankTransactionCodeStructure1 proprietaryBankTransactionCode = new ProprietaryBankTransactionCodeStructure1();
	static OBMerchantDetails1 merchantDetails = new OBMerchantDetails1();
	
	public static String getTestAccountId() {
		return "269c3ff5-d7f8-419b-a3b9-7136c5b4611a";
	}

	public static String getTestBankID() {
		return "TestBankID";
	}

	public static AccountMapping getMockAccountMapping() {
		AccountMapping mapping = new AccountMapping();
		mapping.setTppCID("tpp123");
		mapping.setPsuId("user123");
		List<AccountDetails> selectedAccounts = new ArrayList<>();
		AccountDetails accountRequest = new AccountDetails();
		accountRequest.setAccountId("269c3ff5-d7f8-419b-a3b9-7136c5b4611a");
		accountRequest.setAccountNSC("SC802001");
		accountRequest.setAccountNumber("10203345");
		selectedAccounts.add(accountRequest);
		mapping.setAccountDetails(selectedAccounts);
		return mapping;
	}

	public static Page<AccountTransactionCMA2> getAccountTransactionsDataPage() {
		List<AccountTransactionCMA2> dataList = new ArrayList<AccountTransactionCMA2>();
		data.setAccountId("269c3ff5-d7f8-419b-a3b9-7136c5b4611a");
		data.setTransactionId("123");
		data.setTransactionReference("Ref123");
		amount.setAmount("10.00");
		amount.setCurrency("GBP");
		data.setAmount(amount);
		data.setCreditDebitIndicator(OBCreditDebitCode1.CREDIT);
		data.setStatus(OBEntryStatus1Code.BOOKED);
		data.setBookingDateTime("2017-04-05T00:00:00.000");
		data.setValueDateTime("2017-04-05T00:00:00.000");
		data.setTransactionInformation("Cash from Aubrey");
		data.setAddressLine("XYZ address Line");
		bankTransactionCode.setCode("ReceivedCreditTransfer");
		bankTransactionCode.setSubCode("DomesticCreditTransfer");
		data.setBankTransactionCode(bankTransactionCode);
		proprietaryBankTransactionCode.setCode("Transfer");
		proprietaryBankTransactionCode.setIssuer("AlphaBank");
		data.setProprietaryBankTransactionCode(proprietaryBankTransactionCode);
		balance.setAmount(amount2);
		amount2.setAmount("230.00");
		amount2.setCurrency("GBP");
		balance.setCreditDebitIndicator(OBCreditDebitCode2.CREDIT);
		balance.setType(OBBalanceType1Code.INTERIMBOOKED);
		data.setBalance(balance);
		data.setMerchantDetails(merchantDetails);
		merchantDetails.setMerchantCategoryCode("MerchantXYZcode");
		merchantDetails.setMerchantName("MerchantXYZ");
		dataList.add(data);
		Page<AccountTransactionCMA2> page = new PageImpl<>(dataList);
		return page;
	}	
}
