package com.capgemini.psd2.account.transaction;

import javax.servlet.Filter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import com.capgemini.psd2.account.transaction.routing.adapter.impl.AccountTransactionRoutingAdapter;
/*import com.capgemini.psd2.filter.PSD2Filter;*/
import com.capgemini.psd2.filter.PSD2Filter;

@SpringBootApplication
@EnableEurekaClient
@ComponentScan(basePackages = { "com.capgemini.psd2" })
@EnableMongoRepositories(basePackages = { "com.capgemini.psd2" })
public class AccountTransactionApplication {

	/** The context. */
	static ConfigurableApplicationContext context = null;

	public static void main(String[] args) {
		try {
			context = SpringApplication.run(AccountTransactionApplication.class, args);
		} catch (Exception e) {
			if (context != null) {
				SpringApplication.exit(context, () -> 1);
			}
			System.exit(1);
		}
	}

	@Bean(name = "transactionRoutingAdapter")
	public AccountTransactionRoutingAdapter buildAdapter() {
		return new AccountTransactionRoutingAdapter();
	}

	@Bean(name = "psd2Filter")
	public Filter psd2Filter() {
		return new PSD2Filter();
	}
}
