package com.capgemini.psd2.account.transaction.service.impl;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.capgemini.psd2.account.transaction.constants.AccountTransactionConstants;
import com.capgemini.psd2.account.transaction.service.AccountTransactionService;
import com.capgemini.psd2.account.transaction.utilities.AccountTransactionDateUtilities;
import com.capgemini.psd2.aisp.account.mapping.adapter.AccountMappingAdapter;
import com.capgemini.psd2.aisp.adapter.AccountTransactionAdapter;
import com.capgemini.psd2.aisp.adapter.AispConsentAdapter;
import com.capgemini.psd2.aisp.domain.Links;
import com.capgemini.psd2.aisp.domain.Meta;
import com.capgemini.psd2.aisp.domain.OBReadDataTransaction6;
import com.capgemini.psd2.aisp.domain.OBReadTransaction6;
import com.capgemini.psd2.aisp.domain.OBTransaction6;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountTransactionResponse;
import com.capgemini.psd2.aisp.validation.adapter.AISPCustomValidator;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.consent.domain.AispConsent;
import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.netflix.discovery.shared.Pair;

@Service
public class AccountTransactionServiceImpl implements AccountTransactionService {
	@Value("${app.creditFilter}")
	private String creditFilter;
	@Value("${app.debitFilter}")
	private String debitFilter;
	@Value("${app.minPageSize}")
	private String minPageSize;
	@Value("${app.maxPageSize}")
	private String maxPageSize;
	@Value("${app.defaultPageSize}")
	private String defaultPageSize;

	@Autowired
	@Qualifier("transactionRoutingAdapter")
	private AccountTransactionAdapter accountTransactionAdapter;

	@Value("${app.isSandboxEnabled:false}")
	private boolean isSandboxEnabled;

	@Autowired
	private RequestHeaderAttributes reqHeaderAtrributes;

	/** The account mapping adapter. */
	@Autowired
	private AccountMappingAdapter accountMappingAdapter;

	@Autowired
	private AispConsentAdapter aispConsentAdapter;

	@Autowired
	AccountTransactionDateUtilities apiDateUtil;

	@SuppressWarnings("rawtypes")
	@Autowired
	@Qualifier("accountsRoutingValidator")
	private AISPCustomValidator accountsCustomValidator;

	@Override
	public OBReadTransaction6 retrieveAccountTransaction(String accountId, String paramFromDate, String paramToDate,
			Integer paramPageNumber, Integer paramPageSize, String txnKey) {

		accountsCustomValidator.validateUniqueId(reqHeaderAtrributes.getToken().getConsentTokenData().getConsentId());
		// Get consents by accountId and validate.
		AispConsent aispConsent = aispConsentAdapter.validateAndRetrieveConsentByAccountId(accountId,
				reqHeaderAtrributes.getToken().getConsentTokenData().getConsentId());

		Pair<String, String> consentDates = populateSetupDates(aispConsent);

		String expirationDate = getExpirationDate(aispConsent);

		// Get account mapping.
		AccountMapping accountMapping = accountMappingAdapter.retrieveAccountMappingDetails(accountId, aispConsent);

		Map<String, String> params = new HashMap<>();
		params.put(AccountTransactionConstants.REQUESTED_FROM_DATETIME, paramFromDate);
		params.put(AccountTransactionConstants.REQUESTED_TO_DATETIME, paramToDate);

		// new addition to params
		params.put(AccountTransactionConstants.REQUESTED_FROM_CONSENT_DATETIME, consentDates.first());
		params.put(AccountTransactionConstants.REQUESTED_TO_CONSENT_DATETIME, consentDates.second());
		params.put(AccountTransactionConstants.CONSENT_EXPIRATION_DATETIME, expirationDate);

		params.put(AccountTransactionConstants.REQUESTED_PAGE_NUMBER,
				paramPageNumber == null ? AccountTransactionConstants.DEFAULT_PAGE : paramPageNumber.toString());
		params.put(AccountTransactionConstants.REQUESTED_TXN_FILTER, populateFilter());
		params.put(AccountTransactionConstants.REQUESTED_TXN_KEY, txnKey);
		params.put(AccountTransactionConstants.REQUESTED_PAGE_SIZE,
				paramPageSize == null ? defaultPageSize : paramPageSize.toString());
		params.put(AccountTransactionConstants.REQUESTED_MIN_PAGE_SIZE, String.valueOf(minPageSize));
		params.put(AccountTransactionConstants.REQUESTED_MAX_PAGE_SIZE, String.valueOf(maxPageSize));
		params.put(PSD2Constants.CMAVERSION, aispConsent.getCmaVersion());
		params.putAll(reqHeaderAtrributes.getToken().getSeviceParams());
		params.put(PSD2Constants.CO_RELATION_ID, reqHeaderAtrributes.getCorrelationId());

		// Retrieve account transaction.
		PlatformAccountTransactionResponse platformAccountTransactionResponse = accountTransactionAdapter
				.retrieveAccountTransaction(accountMapping, params);
		if (platformAccountTransactionResponse == null)
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.NO_RECORD_FOUND_FOR_REQUESTED_ACCT);

		// Get CMA2 transaction response.
		OBReadTransaction6 tppTransactionResponse = platformAccountTransactionResponse.getoBReadTransaction6();

		params.put("linksFromDate", paramFromDate);
		params.put("linksToDate", paramToDate);
		if (paramPageNumber != null)
			params.put("linksPageNumber", String.valueOf(paramPageNumber));
		if (paramPageSize != null)
			params.put("linksPageSize", String.valueOf(paramPageSize));
		tppTransactionResponse = populateLinksAndMeta(tppTransactionResponse, params);

		accountsCustomValidator.validateResponseParams(tppTransactionResponse);
		return tppTransactionResponse;

	}

	// CR20
	private Pair<String, String> populateSetupDates(AispConsent aispConsent) {

		String consentFromDateTimeString = aispConsent.getTransactionFromDateTime();
		String consentToDateTimeString = aispConsent.getTransactionToDateTime();

		String requestedFromString = null;
		String requestedToString = null;

		if (consentFromDateTimeString != null) {

			LocalDateTime consentFrmDtTm = apiDateUtil.convertZonedStringToLocalDateTime(consentFromDateTimeString);
			requestedFromString = apiDateUtil.convertZonedDateTimeToString(consentFrmDtTm);
		}

		if (consentToDateTimeString != null) {
			LocalDateTime consentToDtTm = apiDateUtil.convertZonedStringToLocalDateTime(consentToDateTimeString);
			requestedToString = apiDateUtil.convertZonedDateTimeToString(consentToDtTm);
		}

		return new Pair<>(requestedFromString, requestedToString);
	}

	// CR20
	private String getExpirationDate(AispConsent aispConsent) {
		String expirationDateTimeString = aispConsent.getEndDate();

		String expirationDateTimeToString = null;
		if (expirationDateTimeString != null) {
			LocalDateTime expirationDateTime = apiDateUtil.convertZonedStringToLocalDateTime(expirationDateTimeString);

			expirationDateTimeToString = apiDateUtil.convertZonedDateTimeToString(expirationDateTime);
		}
		return expirationDateTimeToString;
	}

	private String populateFilter() {

		String transactionFilter;
		Set<Object> claims = reqHeaderAtrributes.getToken().getClaims()
				.get(AccountTransactionConstants.ACCOUNT_SCOPE_NAME);
		if ((claims.contains(creditFilter)) && (claims.contains(debitFilter)))
			transactionFilter = AccountTransactionConstants.FilterEnum.ALL.toString();
		else if (claims.contains(creditFilter))
			transactionFilter = AccountTransactionConstants.FilterEnum.CREDIT.toString();
		else
			transactionFilter = AccountTransactionConstants.FilterEnum.DEBIT.toString();
		return transactionFilter;
	}

	private OBReadTransaction6 populateLinksAndMeta(OBReadTransaction6 tppTransactionResponse,
			Map<String, String> params) {

		String paramTxnKey = params.get(AccountTransactionConstants.REQUESTED_TXN_KEY);
		String paramFromDate = params.get("linksFromDate");
		String paramToDate = params.get("linksToDate");
		String paramPageNumber = params.get("linksPageNumber");
		String paramPageSize = params.get("linksPageSize");

		StringBuilder baseUrl = new StringBuilder(reqHeaderAtrributes.getSelfUrl());

		createURL(baseUrl, AccountTransactionConstants.FROM_DATE_PARAM_NAME, paramFromDate);
		createURL(baseUrl, AccountTransactionConstants.TO_DATE_PARAM_NAME, paramToDate);
		createURL(baseUrl, AccountTransactionConstants.PAGE_SIZE_PARAM_NAME, paramPageSize);

		StringBuilder selfUrl = new StringBuilder(baseUrl.toString());
		createURL(selfUrl, AccountTransactionConstants.PAGE_NUMBER_PARAM_NAME, paramPageNumber);
		createURL(selfUrl, AccountTransactionConstants.TXN_KEY_PARAM_NAME, paramTxnKey);

		if (tppTransactionResponse != null) {
			if (tppTransactionResponse.getMeta() == null)
				tppTransactionResponse.setMeta(new Meta());

			if (tppTransactionResponse.getData() == null) {
				OBReadDataTransaction6 data = new OBReadDataTransaction6();
				List<OBTransaction6> transaction = new ArrayList<>();
				data.setTransaction(transaction);
				tppTransactionResponse.setData(data);
			}
			if (tppTransactionResponse.getLinks() == null) {
				tppTransactionResponse.setLinks(new Links());
				tppTransactionResponse.getLinks().setSelf(selfUrl.toString());
				return tppTransactionResponse;
			}
		} else {
			OBReadTransaction6 tppTransactionResp = new OBReadTransaction6();
			tppTransactionResp.setMeta(new Meta());
			OBReadDataTransaction6 data = new OBReadDataTransaction6();
			List<OBTransaction6> transaction = new ArrayList<>();
			data.setTransaction(transaction);
			tppTransactionResp.setData(data);
			tppTransactionResp.setLinks(new Links());
			tppTransactionResp.getLinks().setSelf(selfUrl.toString());
			return tppTransactionResp;
		}
		String txnKey = null;
		if (tppTransactionResponse.getLinks().getSelf() != null
				&& tppTransactionResponse.getLinks().getSelf().contains(":"))
			txnKey = tppTransactionResponse.getLinks().getSelf().split(":")[1];

		tppTransactionResponse.getLinks().setSelf(selfUrl.toString());
		tppTransactionResponse.getLinks().setFirst(null);
		tppTransactionResponse.getLinks()
				.setNext(appendUrl(tppTransactionResponse.getLinks().getNext(), baseUrl.toString(), txnKey));
		tppTransactionResponse.getLinks()
				.setPrev(appendUrl(tppTransactionResponse.getLinks().getPrev(), baseUrl.toString(), txnKey));

		if (!isSandboxEnabled) {
			tppTransactionResponse.getLinks()
					.setLast(appendUrl(tppTransactionResponse.getLinks().getLast(), baseUrl.toString(), txnKey));
		}

		return tppTransactionResponse;
	}

	private String appendUrl(String fsPageNum, String baseUrl, String txnKey) {
		if (fsPageNum == null)
			return null;
		StringBuilder lastUrl = new StringBuilder(baseUrl);
		createURL(lastUrl, AccountTransactionConstants.PAGE_NUMBER_PARAM_NAME, fsPageNum);
		createURL(lastUrl, AccountTransactionConstants.TXN_KEY_PARAM_NAME, txnKey);
		return lastUrl.toString();
	}

	private void createURL(StringBuilder url, String key, String value) {
		if (value != null) {
			if (url.indexOf("?") == -1)
				url.append("?").append(key).append("=").append(value);
			else
				url.append("&").append(key).append("=").append(value);
		}
	}

	@Override
	public OBReadTransaction6 retrieveAccountTransactions(String paramString1, String paramString2, String paramString3,
			Integer paramInteger, Integer pageSize, String txnKey) {
		return null;
	}

}