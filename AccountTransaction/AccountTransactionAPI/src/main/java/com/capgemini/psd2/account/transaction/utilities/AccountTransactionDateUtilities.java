package com.capgemini.psd2.account.transaction.utilities;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Date;

import org.springframework.stereotype.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.capgemini.psd2.exceptions.ErrorMapKeys;
import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;

@Component
public class AccountTransactionDateUtilities {
	private static final Logger LOG = LoggerFactory.getLogger(AccountTransactionDateUtilities.class);
	public LocalDateTime convertDateStringToLocalDateTime(String inputDateString) {
		if (inputDateString != null) {
			try {
				return LocalDateTime.parse(inputDateString, DateTimeFormatter.ISO_LOCAL_DATE_TIME);
			} catch (DateTimeParseException e) {
				throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_FIELD_INVALID,ErrorMapKeys.VALIDATION_ERROR));
			}
		}
		return null;
	}
	
	public LocalDateTime convertZonedStringToLocalDateTime(String inputDateString) {
		if (inputDateString != null) {
			try {
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
				Date date = sdf.parse(inputDateString);
				return convertDateStringToLocalDateTime(sdf.format(date));
			} catch (DateTimeParseException | ParseException e) {
				LOG.error(e.getMessage(),e);
				throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_FIELD_INVALID,ErrorMapKeys.VALIDATION_ERROR));
			}
		}
		return null;
	}

	public String convertZonedDateTimeToString(LocalDateTime inputDateTime) {
		return inputDateTime.plusNanos(1).toString().substring(0, 23);
	}
}
