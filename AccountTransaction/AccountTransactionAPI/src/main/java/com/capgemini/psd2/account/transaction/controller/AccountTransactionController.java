package com.capgemini.psd2.account.transaction.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.psd2.account.transaction.service.AccountTransactionService;
import com.capgemini.psd2.account.transaction.utilities.AccountTransactionValidationUtilities;
import com.capgemini.psd2.aisp.domain.OBReadTransaction6;

@RestController
public class AccountTransactionController {

	@Autowired
	AccountTransactionValidationUtilities accountTransactionValidationUtilities;

	@Autowired
	private AccountTransactionService accountTransactionService;

	@RequestMapping(value = "/accounts/{accountId}/transactions", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public OBReadTransaction6 retrieveAccountTransaction(@PathVariable("accountId") String accountId,
			@RequestParam(value = "fromBookingDateTime", required = false) String fromBookingDateTime,
			@RequestParam(value = "toBookingDateTime", required = false) String toBookingDateTime,
			@RequestParam(value = "page", required = false) Integer pageNumber,
			@RequestParam(value = "pageSize", required = false) Integer pageSize,
			@RequestParam(value = "transactionRetrievalKey", required = false) String txnKey) {

		accountTransactionValidationUtilities.validateParams( pageNumber, pageSize);
		return accountTransactionService.retrieveAccountTransaction(accountId, fromBookingDateTime, toBookingDateTime,
				pageNumber, pageSize, txnKey);
	}
}
