package com.capgemini.psd2.test.service.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyMapOf;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.account.transaction.routing.adapter.impl.AccountTransactionRoutingAdapter;
import com.capgemini.psd2.account.transaction.service.impl.AccountTransactionServiceImpl;
import com.capgemini.psd2.account.transaction.utilities.AccountTransactionDateUtilities;
import com.capgemini.psd2.aisp.account.mapping.adapter.AccountMappingAdapter;
import com.capgemini.psd2.aisp.adapter.AispConsentAdapter;
import com.capgemini.psd2.aisp.domain.OBReadTransaction6;
import com.capgemini.psd2.aisp.validation.adapter.AISPCustomValidator;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.response.validator.ResponseValidator;
import com.capgemini.psd2.test.mock.data.AccountTransactionApiTestData;
import com.capgemini.psd2.utilities.JSONUtilities;

@RunWith(SpringJUnit4ClassRunner.class)
public class AccountTransactionServiceImplTest {

	@Mock
	private AccountTransactionRoutingAdapter accountTransactionAdapter;

	@Mock
	private AispConsentAdapter aispConsentAdapter;

	@Mock
	private RequestHeaderAttributes reqHeaderAtrributes;

	@Mock
	AccountTransactionDateUtilities apiDateUtil;
	
	@Mock
	private AccountMappingAdapter accountMappingAdapter;
	
	@Mock
	private ResponseValidator responseValidator;
	

	@InjectMocks
	static AccountTransactionServiceImpl service = new AccountTransactionServiceImpl();

	@Mock
	private AISPCustomValidator<OBReadTransaction6, OBReadTransaction6> accountsCustomValidator;
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);

		Mockito.when(reqHeaderAtrributes.getToken())
				.thenReturn(AccountTransactionApiTestData.getMockTokenWithALLFilter());

		Mockito.when(aispConsentAdapter.validateAndRetrieveConsentByAccountId(anyString(), anyString())).thenReturn(AccountTransactionApiTestData.getAispConsent());
		Mockito.when(aispConsentAdapter.retrieveConsent(anyString()))
				.thenReturn(AccountTransactionApiTestData.getAispConsent());

		
		ReflectionTestUtils.setField(service, "debitFilter", "ReadTransactionsDebits");
		ReflectionTestUtils.setField(service, "creditFilter", "ReadTransactionsCredits");

		Mockito.when(reqHeaderAtrributes.getSelfUrl())
				.thenReturn("/accounts/269c3ff5-d7f8-419b-a3b9-7136c5b4611a/transactions/");
	}

	@Test
	public void retrieveTransactionTestWithNullParams() {
		doNothing().when(responseValidator).validateResponse(anyObject());
		when(accountMappingAdapter.retrieveAccountMappingDetails(anyString(), anyObject())).thenReturn(AccountTransactionApiTestData.getMockAccountMapping());
		LocalDateTime local=LocalDateTime.now();
		when(apiDateUtil.convertDateStringToLocalDateTime(anyString())).thenReturn(local);
		Mockito.when(aispConsentAdapter.validateAndRetrieveConsentByAccountId(anyString(), anyString())).thenReturn(AccountTransactionApiTestData.getAispConsent());
		Mockito.when(
				accountTransactionAdapter.retrieveAccountTransaction(anyObject(), anyMapOf(String.class, String.class)))
				.thenReturn(AccountTransactionApiTestData.getAccountTransactionsGETResponseForService());

		OBReadTransaction6 response = service
				.retrieveAccountTransaction("269c3ff5-d7f8-419b-a3b9-7136c5b4611a", null, null, null, null, null);
		String actualResponseJson = JSONUtilities.getJSONOutPutFromObject(response);
		String expectedResponseJson = "{\"Data\": {\"Transaction\": [{\"AccountId\": \"269c3ff5-d7f8-419b-a3b9-7136c5b4611a\",\"TransactionId\": \"123\",\"TransactionReference\": \"Ref123\",\"Amount\": {\"Amount\": \"10.00\",\"Currency\": \"GBP\"},\"CreditDebitIndicator\": \"Credit\",\"Status\": \"Booked\",\"BookingDateTime\": \"2017-04-05T10:43:07+00:00\",\"ValueDateTime\": \"2017-04-05T10:45:22+00:00\",\"TransactionInformation\": \"Cash from Aubrey\",\"AddressLine\": \"XYZ address Line\",\"BankTransactionCode\": {\"Code\": \"ReceivedCreditTransfer\",\"SubCode\": \"DomesticCreditTransfer\"},\"ProprietaryBankTransactionCode\": {\"Code\": \"Transfer\",\"Issuer\": \"AlphaBank\"},\"Balance\": {\"Amount\": {\"Amount\": \"230.00\",\"Currency\": \"GBP\"},\"CreditDebitIndicator\": \"Credit\",\"Type\": \"InterimBooked\"},\"MerchantDetails\": {\"MerchantName\": \"MerchantXYZ\",\"MerchantCategoryCode\": \"MerchantXYZcode\"}}]},\"Links\":{\"Prev\": \"/accounts/269c3ff5-d7f8-419b-a3b9-7136c5b4611a/transactions/?page=3\"}}";
		JSONAssert.assertEquals(expectedResponseJson, actualResponseJson, Boolean.FALSE);
	}
	
	@Test
	public void retrieveTransactionTestWithNullParamsEmptyData() {
		doNothing().when(responseValidator).validateResponse(anyObject());
		when(accountMappingAdapter.retrieveAccountMappingDetails(anyString(), anyObject())).thenReturn(AccountTransactionApiTestData.getMockAccountMapping());
		LocalDateTime local=LocalDateTime.now();
		when(apiDateUtil.convertDateStringToLocalDateTime(anyString())).thenReturn(local);
		Mockito.when(aispConsentAdapter.validateAndRetrieveConsentByAccountId(anyString(), anyString())).thenReturn(AccountTransactionApiTestData.getAispConsent());
		Mockito.when(
				accountTransactionAdapter.retrieveAccountTransaction(anyObject(), anyMapOf(String.class, String.class)))
				.thenReturn(AccountTransactionApiTestData.getAccountTransactionsGETResponseForServiceTesting());


		service.retrieveAccountTransaction("269c3ff5-d7f8-419b-a3b9-7136c5b4611a", null, null, null, null, null);
	}
	
	@Test
	public void retrieveTransactionTestWithNullResponse() {
		doNothing().when(responseValidator).validateResponse(anyObject());
		when(accountMappingAdapter.retrieveAccountMappingDetails(anyString(), anyObject())).thenReturn(AccountTransactionApiTestData.getMockAccountMapping());
		LocalDateTime local=LocalDateTime.now();
		when(apiDateUtil.convertDateStringToLocalDateTime(anyString())).thenReturn(local);
		Mockito.when(aispConsentAdapter.validateAndRetrieveConsentByAccountId(anyString(), anyString())).thenReturn(AccountTransactionApiTestData.getAispConsent());
		Mockito.when(
				accountTransactionAdapter.retrieveAccountTransaction(anyObject(), anyMapOf(String.class, String.class)))
				.thenReturn(AccountTransactionApiTestData.getAccountTransactionsForNullResponse());


		service.retrieveAccountTransaction("269c3ff5-d7f8-419b-a3b9-7136c5b4611a", null, null, null, null, null);
	}

	@Test
	public void retrieveTransactionTestWithNoRecord() {
		Mockito.when(
				accountTransactionAdapter.retrieveAccountTransaction(anyObject(), anyMapOf(String.class, String.class)))
				.thenReturn(null);
		try {
			service.retrieveAccountTransaction("269c3ff5-d7f8-419b-a3b9-7136c5b4611a", "2015-01-01T00:00:00",
					"2016-12-31T23:59:59", 2, null, null);
		} catch (PSD2Exception e) {
			assertEquals(HttpStatus.NOT_FOUND.toString(), e.getErrorInfo().getStatusCode());
			
		}
	}

	// Test Links population different scenarios
	@Test
	public void retrieveTransactionTestWithNullLinksFromAdapter() {
		Mockito.when(
				accountTransactionAdapter.retrieveAccountTransaction(anyObject(), anyMapOf(String.class, String.class)))
				.thenReturn(AccountTransactionApiTestData.getAccountTransactionsGETResponseWithNullLinks());

		OBReadTransaction6 response = service
				.retrieveAccountTransaction("269c3ff5-d7f8-419b-a3b9-7136c5b4611a", null, null, null, null, null);
		String actualResponseJson = JSONUtilities.getJSONOutPutFromObject(response);
		String expectedResponseJson = "{\"Data\": {\"Transaction\": [{\"AccountId\": \"269c3ff5-d7f8-419b-a3b9-7136c5b4611a\",\"TransactionId\": \"123\",\"TransactionReference\": \"Ref123\",\"Amount\": {\"Amount\": \"10.00\",\"Currency\": \"GBP\"},\"CreditDebitIndicator\": \"Credit\",\"Status\": \"Booked\",\"BookingDateTime\": \"2017-04-05T10:43:07+00:00\",\"ValueDateTime\": \"2017-04-05T10:45:22+00:00\",\"TransactionInformation\": \"Cash from Aubrey\",\"AddressLine\": \"XYZ address Line\",\"BankTransactionCode\": {\"Code\": \"ReceivedCreditTransfer\",\"SubCode\": \"DomesticCreditTransfer\"},\"ProprietaryBankTransactionCode\": {\"Code\": \"Transfer\",\"Issuer\": \"AlphaBank\"},\"Balance\": {\"Amount\": {\"Amount\": \"230.00\",\"Currency\": \"GBP\"},\"CreditDebitIndicator\": \"Credit\",\"Type\": \"InterimBooked\"},\"MerchantDetails\": {\"MerchantName\": \"MerchantXYZ\",\"MerchantCategoryCode\": \"MerchantXYZcode\"}}]},\"Links\":{\"Prev\": null}}";
		JSONAssert.assertEquals(expectedResponseJson, actualResponseJson, Boolean.FALSE);
	}

	
	@Test
	public void retrieveTransactionTestWithNullLinksObjectFromAdapter() {
		Mockito.when(
				accountTransactionAdapter.retrieveAccountTransaction(anyObject(), anyMapOf(String.class, String.class)))
				.thenReturn(AccountTransactionApiTestData.getAccountTransactionsGETResponseWithNullLinksObject());

		OBReadTransaction6 response = service
				.retrieveAccountTransaction("269c3ff5-d7f8-419b-a3b9-7136c5b4611a", null, null, null, null, null);
		String actualResponseJson = JSONUtilities.getJSONOutPutFromObject(response);
		String expectedResponseJson = "{\"Data\": {\"Transaction\": [{\"AccountId\": \"269c3ff5-d7f8-419b-a3b9-7136c5b4611a\",}]},\"Links\":{\"Prev\": null}}";
		JSONAssert.assertEquals(expectedResponseJson, actualResponseJson, Boolean.FALSE);
	}

	@Test
	public void retrieveTransactionTestLinksWhenDatesInParams() {
		Mockito.when(
				accountTransactionAdapter.retrieveAccountTransaction(anyObject(), anyMapOf(String.class, String.class)))
				.thenReturn(AccountTransactionApiTestData.getAccountTransactionsGETResponseForService());

		Mockito.when(reqHeaderAtrributes.getSelfUrl())
				.thenReturn("/accounts/269c3ff5-d7f8-419b-a3b9-7136c5b4611a/transactions/");

		OBReadTransaction6 response = service.retrieveAccountTransaction(
				"269c3ff5-d7f8-419b-a3b9-7136c5b4611a", "2015-01-01T00:00:00", "2017-01-01T00:00:00", null, null, null);
		String actualResponseJson = JSONUtilities.getJSONOutPutFromObject(response);
		String expectedResponseJson = "{\"Data\": {\"Transaction\":[{\"AccountId\": \"269c3ff5-d7f8-419b-a3b9-7136c5b4611a\"}]},\"Links\":{\"Next\":\"/accounts/269c3ff5-d7f8-419b-a3b9-7136c5b4611a/transactions/?fromBookingDateTime=2015-01-01T00:00:00&toBookingDateTime=2017-01-01T00:00:00&page=5\"}}";
		JSONAssert.assertEquals(expectedResponseJson, actualResponseJson, Boolean.FALSE);
	}

		
	
	// Test different filter permissions present in token
	@Test(expected = PSD2Exception.class)
	public void TestCreditFilter() {
		Mockito.when(reqHeaderAtrributes.getToken())
				.thenReturn(AccountTransactionApiTestData.getMockTokenWithCreditFilter());
		service.retrieveAccountTransaction("269c3ff5-d7f8-419b-a3b9-7136c5b4611a", "2015-01-01T00:00:00",
				"2017-01-01T00:00:00", 1, 1, "asdasd");
	}
	
	@Test(expected = PSD2Exception.class)
	public void TestDebitFilter() {
		Mockito.when(reqHeaderAtrributes.getToken())
				.thenReturn(AccountTransactionApiTestData.getMockTokenWithDebitFilter());
		String futureDate = LocalDateTime.now().plusDays(1).toString();
		Mockito.when(apiDateUtil.convertDateStringToLocalDateTime(futureDate))
				.thenReturn(LocalDateTime.parse(futureDate, DateTimeFormatter.ISO_DATE_TIME));
		service.retrieveAccountTransaction("269c3ff5-d7f8-419b-a3b9-7136c5b4611a", "2015-01-01T00:00:00",
				"2017-01-01T00:00:00", 1, 1, "asdasd");
	}
	
	// Test Consent Dates Availability
	@Test(expected = PSD2Exception.class)
	public void TestOnlyParamDatesPresent() {
		Mockito.when(reqHeaderAtrributes.getToken())
				.thenReturn(AccountTransactionApiTestData.getMockTokenWithDebitFilter());
		Mockito.when(apiDateUtil.convertDateStringToLocalDateTime("2017-01-01T00:00:00"))
				.thenReturn(LocalDateTime.parse("2017-01-01T00:00:00", DateTimeFormatter.ISO_DATE_TIME));
		Mockito.when(apiDateUtil.convertDateStringToLocalDateTime("2015-01-01T00:00:00"))
				.thenReturn(LocalDateTime.parse("2015-01-01T00:00:00", DateTimeFormatter.ISO_DATE_TIME));
		service.retrieveAccountTransaction("269c3ff5-d7f8-419b-a3b9-7136c5b4611a", "2015-01-01T00:00:00",
				"2017-01-01T00:00:00", 1, 1, "asdasd");
	}

	
	@Test(expected = PSD2Exception.class)
	public void TestOnlyConsentDatesPresent() {
		Mockito.when(reqHeaderAtrributes.getToken())
				.thenReturn(AccountTransactionApiTestData.getMockTokenWithDebitFilter());
		Mockito.when(apiDateUtil.convertDateStringToLocalDateTime("2017-01-02T00:00:00"))
				.thenReturn(LocalDateTime.parse("2017-01-02T00:00:00", DateTimeFormatter.ISO_DATE_TIME));
		Mockito.when(apiDateUtil.convertDateStringToLocalDateTime("2015-01-02T00:00:00"))
				.thenReturn(LocalDateTime.parse("2015-01-02T00:00:00", DateTimeFormatter.ISO_DATE_TIME));
		Mockito.when(aispConsentAdapter.retrieveConsent(anyString()))
				.thenReturn(AccountTransactionApiTestData.getAispConsent2());

		service.retrieveAccountTransaction("269c3ff5-d7f8-419b-a3b9-7136c5b4611a", null, null, 1, 1, "asdasd");
	}

	
	@Test(expected = PSD2Exception.class)
	public void TestBothConsentAndParamDatesPresentParamsInRange() {
		Mockito.when(reqHeaderAtrributes.getToken())
				.thenReturn(AccountTransactionApiTestData.getMockTokenWithDebitFilter());
		Mockito.when(apiDateUtil.convertDateStringToLocalDateTime("2017-01-02T00:00:00"))
				.thenReturn(LocalDateTime.parse("2017-01-02T00:00:00", DateTimeFormatter.ISO_DATE_TIME));
		Mockito.when(apiDateUtil.convertDateStringToLocalDateTime("2015-01-02T00:00:00"))
				.thenReturn(LocalDateTime.parse("2015-01-02T00:00:00", DateTimeFormatter.ISO_DATE_TIME));
		Mockito.when(apiDateUtil.convertDateStringToLocalDateTime("2017-01-03T00:00:00"))
				.thenReturn(LocalDateTime.parse("2017-01-03T00:00:00", DateTimeFormatter.ISO_DATE_TIME));
		Mockito.when(apiDateUtil.convertDateStringToLocalDateTime("2015-01-03T00:00:00"))
				.thenReturn(LocalDateTime.parse("2015-01-03T00:00:00", DateTimeFormatter.ISO_DATE_TIME));
		Mockito.when(aispConsentAdapter.retrieveConsent(anyString()))
				.thenReturn(AccountTransactionApiTestData.getAispConsent2());

		service.retrieveAccountTransaction("269c3ff5-d7f8-419b-a3b9-7136c5b4611a", "2015-01-03T00:00:00",
				"2017-01-03T00:00:00", 1, 1, "asdasd");
	}


	@Test(expected = PSD2Exception.class)
	public void TestBothConsentAndParamDatesPresentParamsOutOfRange() {
		Mockito.when(reqHeaderAtrributes.getToken())
				.thenReturn(AccountTransactionApiTestData.getMockTokenWithDebitFilter());
		Mockito.when(apiDateUtil.convertDateStringToLocalDateTime("2017-01-02T00:00:00"))
				.thenReturn(LocalDateTime.parse("2017-01-02T00:00:00", DateTimeFormatter.ISO_DATE_TIME));
		Mockito.when(apiDateUtil.convertDateStringToLocalDateTime("2015-01-02T00:00:00"))
				.thenReturn(LocalDateTime.parse("2015-01-02T00:00:00", DateTimeFormatter.ISO_DATE_TIME));
		Mockito.when(apiDateUtil.convertDateStringToLocalDateTime("2017-01-01T00:00:00"))
				.thenReturn(LocalDateTime.parse("2017-01-01T00:00:00", DateTimeFormatter.ISO_DATE_TIME));
		Mockito.when(apiDateUtil.convertDateStringToLocalDateTime("2015-01-01T00:00:00"))
				.thenReturn(LocalDateTime.parse("2015-01-01T00:00:00", DateTimeFormatter.ISO_DATE_TIME));
		Mockito.when(aispConsentAdapter.retrieveConsent(anyString()))
				.thenReturn(AccountTransactionApiTestData.getAispConsent2());

		service.retrieveAccountTransaction("269c3ff5-d7f8-419b-a3b9-7136c5b4611a", "2015-01-01T00:00:00",
				"2017-01-01T00:00:00", 1, 1, "asdasd");
	}

	@After
	public void tearDown() throws Exception {
		service = null;
	}

}
