package com.capgemini.psd2.test.mock.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.capgemini.psd2.aisp.domain.Links;
import com.capgemini.psd2.aisp.domain.Meta;
import com.capgemini.psd2.aisp.domain.OBActiveOrHistoricCurrencyAndAmount9;
import com.capgemini.psd2.aisp.domain.OBBalanceType1Code;
//import com.capgemini.psd2.aisp.domain.OBTransaction2BalanceAmount;
import com.capgemini.psd2.aisp.domain.OBBankTransactionCodeStructure1;
import com.capgemini.psd2.aisp.domain.OBCreditDebitCode1;
import com.capgemini.psd2.aisp.domain.OBCreditDebitCode2;
import com.capgemini.psd2.aisp.domain.OBEntryStatus1Code;
import com.capgemini.psd2.aisp.domain.OBMerchantDetails1;
import com.capgemini.psd2.aisp.domain.OBReadDataTransaction6;
import com.capgemini.psd2.aisp.domain.OBReadTransaction6;
import com.capgemini.psd2.aisp.domain.OBTransaction6;
import com.capgemini.psd2.aisp.domain.OBTransactionCashBalance;
import com.capgemini.psd2.aisp.domain.OBTransactionCashBalanceAmount;
import com.capgemini.psd2.aisp.domain.ProprietaryBankTransactionCodeStructure1;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountTransactionResponse;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.consent.domain.AispConsent;
import com.capgemini.psd2.token.ConsentTokenData;
import com.capgemini.psd2.token.Token;

public class AccountTransactionApiTestData {

	static OBTransaction6 data = new OBTransaction6();
	static OBActiveOrHistoricCurrencyAndAmount9 amount = new OBActiveOrHistoricCurrencyAndAmount9();
	static OBTransactionCashBalance balance = new OBTransactionCashBalance();
	static OBTransactionCashBalanceAmount amount2 = new OBTransactionCashBalanceAmount();
	static OBBankTransactionCodeStructure1 bankTransactionCode = new OBBankTransactionCodeStructure1();
	static ProprietaryBankTransactionCodeStructure1 proprietaryBankTransactionCode = new ProprietaryBankTransactionCodeStructure1();
	static OBMerchantDetails1 merchantDetails = new OBMerchantDetails1();

	public static String getTestAccountId() {
		return "269c3ff5-d7f8-419b-a3b9-7136c5b4611a";
	}

	public static String getTestBankID() {
		return "TestBankID";
	}

	public static List<Object> getMockClaimsList() {
		List<Object> claimList = new ArrayList<Object>();
		claimList.add("ReadTransactionsCredits");
		claimList.add("ReadTransactionsDebits");
		return claimList;
	}

	public static OBReadTransaction6 getAccountTransactionsGETResponse() {
		OBReadTransaction6 resp = new OBReadTransaction6();
		List<OBTransaction6> dataList = new ArrayList<OBTransaction6>();
		data.setAccountId("269c3ff5-d7f8-419b-a3b9-7136c5b4611a");
		data.setTransactionId("123");
		data.setTransactionReference("Ref123");
		amount.setAmount("10.00");
		amount.setCurrency("GBP");
		data.setAmount(amount);
		data.setCreditDebitIndicator(OBCreditDebitCode1 .CREDIT);
		data.setStatus(OBEntryStatus1Code.BOOKED);
		data.setBookingDateTime("2017-04-05T10:43:07+00:00");
		data.setValueDateTime("2017-04-05T10:45:22+00:00");
		data.setTransactionInformation("Cash from Aubrey");
		data.setAddressLine("XYZ address Line");
		bankTransactionCode.setCode("ReceivedCreditTransfer");
		bankTransactionCode.setSubCode("DomesticCreditTransfer");
		data.setBankTransactionCode(bankTransactionCode);
		proprietaryBankTransactionCode.setCode("Transfer");
		proprietaryBankTransactionCode.setIssuer("AlphaBank");
		data.setProprietaryBankTransactionCode(proprietaryBankTransactionCode);
		balance.setAmount(amount2);
		amount2.setAmount("230.00");
		amount2.setCurrency("GBP");
		balance.setCreditDebitIndicator(OBCreditDebitCode2.CREDIT);
		balance.setType(OBBalanceType1Code.INTERIMBOOKED);
		data.setBalance(balance);
		data.setMerchantDetails(merchantDetails);
		merchantDetails.setMerchantCategoryCode("MerchantXYZcode");
		merchantDetails.setMerchantName("MerchantXYZ");
		dataList.add(data);
		OBReadDataTransaction6 data3 = new OBReadDataTransaction6();
		data3.setTransaction(dataList);
		resp.setData(data3);
		Links links = new Links();
		links.setFirst("1");
		links.setLast("10");
		links.setNext("5");
		links.setPrev("3");
		links.setSelf("4");
		resp.setLinks(links);
		Meta meta = new Meta();
		resp.setMeta(meta);
		
		return resp;
	}
	
	public static PlatformAccountTransactionResponse getAccountTransactionsGETResponseForService() {
		OBReadTransaction6 resp = new OBReadTransaction6();
		List<OBTransaction6> dataList = new ArrayList<OBTransaction6>();
		data.setAccountId("269c3ff5-d7f8-419b-a3b9-7136c5b4611a");
		data.setTransactionId("123");
		data.setTransactionReference("Ref123");
		amount.setAmount("10.00");
		amount.setCurrency("GBP");
		data.setAmount(amount);
		data.setCreditDebitIndicator(OBCreditDebitCode1 .CREDIT);
		data.setStatus(OBEntryStatus1Code.BOOKED);
		data.setBookingDateTime("2017-04-05T10:43:07+00:00");
		data.setValueDateTime("2017-04-05T10:45:22+00:00");
		data.setTransactionInformation("Cash from Aubrey");
		data.setAddressLine("XYZ address Line");
		bankTransactionCode.setCode("ReceivedCreditTransfer");
		bankTransactionCode.setSubCode("DomesticCreditTransfer");
		data.setBankTransactionCode(bankTransactionCode);
		proprietaryBankTransactionCode.setCode("Transfer");
		proprietaryBankTransactionCode.setIssuer("AlphaBank");
		data.setProprietaryBankTransactionCode(proprietaryBankTransactionCode);
		balance.setAmount(amount2);
		amount2.setAmount("230.00");
		amount2.setCurrency("GBP");
		balance.setCreditDebitIndicator(OBCreditDebitCode2.CREDIT);
		balance.setType(OBBalanceType1Code.INTERIMBOOKED);
		data.setBalance(balance);
		data.setMerchantDetails(merchantDetails);
		merchantDetails.setMerchantCategoryCode("MerchantXYZcode");
		merchantDetails.setMerchantName("MerchantXYZ");
		dataList.add(data);
		OBReadDataTransaction6 data3 = new OBReadDataTransaction6();
		data3.setTransaction(dataList);
		resp.setData(data3);
		Links links = new Links();
		links.setFirst("1");
		links.setLast("10");
		links.setNext("5");
		links.setPrev("3");
		links.setSelf("4");
		resp.setLinks(links);
		Meta meta = new Meta();
		resp.setMeta(meta);
		PlatformAccountTransactionResponse platResp=new PlatformAccountTransactionResponse();
		platResp.setoBReadTransaction6(resp);
		return platResp;
	}
	
	public static PlatformAccountTransactionResponse getAccountTransactionsGETResponseForServiceTesting() {
		OBReadTransaction6 resp = new OBReadTransaction6();
		List<OBTransaction6> dataList = new ArrayList<OBTransaction6>();
		data.setAccountId("269c3ff5-d7f8-419b-a3b9-7136c5b4611a");
		data.setTransactionId("123");
		data.setTransactionReference("Ref123");
		amount.setAmount("10.00");
		amount.setCurrency("GBP");
		data.setAmount(amount);
		data.setCreditDebitIndicator(OBCreditDebitCode1 .CREDIT);
		data.setStatus(OBEntryStatus1Code.BOOKED);
		data.setBookingDateTime("2017-04-05T10:43:07+00:00");
		data.setValueDateTime("2017-04-05T10:45:22+00:00");
		data.setTransactionInformation("Cash from Aubrey");
		data.setAddressLine("XYZ address Line");
		bankTransactionCode.setCode("ReceivedCreditTransfer");
		bankTransactionCode.setSubCode("DomesticCreditTransfer");
		data.setBankTransactionCode(bankTransactionCode);
		proprietaryBankTransactionCode.setCode("Transfer");
		proprietaryBankTransactionCode.setIssuer("AlphaBank");
		data.setProprietaryBankTransactionCode(proprietaryBankTransactionCode);
		balance.setAmount(amount2);
		amount2.setAmount("230.00");
		amount2.setCurrency("GBP");
		balance.setCreditDebitIndicator(OBCreditDebitCode2.CREDIT);
		balance.setType(OBBalanceType1Code.INTERIMBOOKED);
		data.setBalance(balance);
		data.setMerchantDetails(merchantDetails);
		merchantDetails.setMerchantCategoryCode("MerchantXYZcode");
		merchantDetails.setMerchantName("MerchantXYZ");
		dataList.add(data);
		OBReadDataTransaction6 data3 = new OBReadDataTransaction6();
		data3.setTransaction(dataList);
		//resp.setData(data3);
		Links links = new Links();
		links.setFirst("1");
		links.setLast("10");
		links.setNext("5");
		links.setPrev("3");
		links.setSelf("4");
		resp.setLinks(links);
		Meta meta = new Meta();
		resp.setMeta(meta);
		PlatformAccountTransactionResponse platResp=new PlatformAccountTransactionResponse();
		platResp.setoBReadTransaction6(resp);
		return platResp;
	}
	
	public static PlatformAccountTransactionResponse getAccountTransactionsForNullResponse() {
		OBReadTransaction6 resp = new OBReadTransaction6();
		List<OBTransaction6> dataList = new ArrayList<OBTransaction6>();
		data.setAccountId("269c3ff5-d7f8-419b-a3b9-7136c5b4611a");
		data.setTransactionId("123");
		data.setTransactionReference("Ref123");
		amount.setAmount("10.00");
		amount.setCurrency("GBP");
		data.setAmount(amount);
		data.setCreditDebitIndicator(OBCreditDebitCode1 .CREDIT);
		data.setStatus(OBEntryStatus1Code.BOOKED);
		data.setBookingDateTime("2017-04-05T10:43:07+00:00");
		data.setValueDateTime("2017-04-05T10:45:22+00:00");
		data.setTransactionInformation("Cash from Aubrey");
		data.setAddressLine("XYZ address Line");
		bankTransactionCode.setCode("ReceivedCreditTransfer");
		bankTransactionCode.setSubCode("DomesticCreditTransfer");
		data.setBankTransactionCode(bankTransactionCode);
		proprietaryBankTransactionCode.setCode("Transfer");
		proprietaryBankTransactionCode.setIssuer("AlphaBank");
		data.setProprietaryBankTransactionCode(proprietaryBankTransactionCode);
		balance.setAmount(amount2);
		amount2.setAmount("230.00");
		amount2.setCurrency("GBP");
		balance.setCreditDebitIndicator(OBCreditDebitCode2.CREDIT);
		balance.setType(OBBalanceType1Code.INTERIMBOOKED);
		data.setBalance(balance);
		data.setMerchantDetails(merchantDetails);
		merchantDetails.setMerchantCategoryCode("MerchantXYZcode");
		merchantDetails.setMerchantName("MerchantXYZ");
		dataList.add(data);
		OBReadDataTransaction6 data3 = new OBReadDataTransaction6();
		data3.setTransaction(dataList);
		//resp.setData(data3);
		Links links = new Links();
		links.setFirst("1");
		links.setLast("10");
		links.setNext("5");
		links.setPrev("3");
		links.setSelf("4");
		resp.setLinks(links);
		Meta meta = new Meta();
		resp.setMeta(meta);
		PlatformAccountTransactionResponse platResp=new PlatformAccountTransactionResponse();
		platResp.setoBReadTransaction6(null);
		return platResp;
	}

	public static Token getMockTokenWithALLFilter() {
		Token x = new Token();
		x.setRequestId("123434321");
		Map<String, Set<Object>> claims = new HashMap<String, Set<Object>>();
		Set<Object> value = new HashSet<Object>();
		value.add("ReadTransactionsCredits");
		value.add("ReadTransactionsDebits");
		claims.put("accounts", value);
		x.setClaims(claims);
		ConsentTokenData consentTokenData = new ConsentTokenData();
		x.setConsentTokenData(consentTokenData);
		Map<String, String> seviceParams = new HashMap<>();
		seviceParams.put("channelId", "demoChannelID");
		x.setSeviceParams(seviceParams);
		return x;
	}

	public static Token getMockTokenWithCreditFilter() {
		Token x = new Token();
		x.setRequestId("123434321");
		Map<String, Set<Object>> claims = new HashMap<String, Set<Object>>();
		Set<Object> value = new HashSet<Object>();
		value.add("ReadTransactionsCredits");
		claims.put("accounts", value);
		x.setClaims(claims);
		ConsentTokenData consentTokenData = new ConsentTokenData();
		x.setConsentTokenData(consentTokenData);
		Map<String, String> seviceParams = new HashMap<>();
		seviceParams.put("channelId", "demoChannelID");
		x.setSeviceParams(seviceParams);
		return x;
	}

	public static Token getMockTokenWithDebitFilter() {
		Token x = new Token();
		x.setRequestId("123434321");
		Map<String, Set<Object>> claims = new HashMap<String, Set<Object>>();
		Set<Object> value = new HashSet<Object>();
		value.add("ReadTransactionsDebits");
		claims.put("accounts", value);
		x.setClaims(claims);
		ConsentTokenData consentTokenData = new ConsentTokenData();
		x.setConsentTokenData(consentTokenData);
		Map<String, String> seviceParams = new HashMap<>();
		seviceParams.put("channelId", "demoChannelID");
		x.setSeviceParams(seviceParams);
		return x;
	}

	public static PlatformAccountTransactionResponse getAccountTransactionsGETResponseWithNullLinks() {
		OBReadTransaction6 resp = new OBReadTransaction6();
		List<OBTransaction6> dataList = new ArrayList<OBTransaction6>();
		data.setAccountId("269c3ff5-d7f8-419b-a3b9-7136c5b4611a");
		data.setTransactionId("123");
		data.setTransactionReference("Ref123");
		amount.setAmount("10.00");
		amount.setCurrency("GBP");
		data.setAmount(amount);
		data.setCreditDebitIndicator(OBCreditDebitCode1 .CREDIT);
		data.setStatus(OBEntryStatus1Code.BOOKED);
		data.setBookingDateTime("2017-04-05T10:43:07+00:00");
		data.setValueDateTime("2017-04-05T10:45:22+00:00");
		data.setTransactionInformation("Cash from Aubrey");
		data.setAddressLine("XYZ address Line");
		bankTransactionCode.setCode("ReceivedCreditTransfer");
		bankTransactionCode.setSubCode("DomesticCreditTransfer");
		data.setBankTransactionCode(bankTransactionCode);
		proprietaryBankTransactionCode.setCode("Transfer");
		proprietaryBankTransactionCode.setIssuer("AlphaBank");
		data.setProprietaryBankTransactionCode(proprietaryBankTransactionCode);
		balance.setAmount(amount2);
		amount2.setAmount("230.00");
		amount2.setCurrency("GBP");
		balance.setCreditDebitIndicator(OBCreditDebitCode2.CREDIT);
		balance.setType(OBBalanceType1Code.INTERIMBOOKED);
		data.setBalance(balance);
		data.setMerchantDetails(merchantDetails);
		merchantDetails.setMerchantCategoryCode("MerchantXYZcode");
		merchantDetails.setMerchantName("MerchantXYZ");
		dataList.add(data);
		OBReadDataTransaction6 data3 = new OBReadDataTransaction6();
		data3.setTransaction(dataList);
		resp.setData(data3);
		Links links = new Links();
		resp.setLinks(links);
		PlatformAccountTransactionResponse transResp=new PlatformAccountTransactionResponse();
		transResp.setoBReadTransaction6(resp);
		return transResp;
	}

	public static PlatformAccountTransactionResponse getAccountTransactionsGETResponseWithNullLinksObject() {
		OBReadTransaction6 resp = new OBReadTransaction6();
		List<OBTransaction6> dataList = new ArrayList<OBTransaction6>();
		data.setAccountId("269c3ff5-d7f8-419b-a3b9-7136c5b4611a");
		data.setTransactionId("123");
		data.setTransactionReference("Ref123");
		amount.setAmount("10.00");
		amount.setCurrency("GBP");
		data.setAmount(amount);
		data.setCreditDebitIndicator(OBCreditDebitCode1.CREDIT);
		data.setStatus(OBEntryStatus1Code.BOOKED);
		data.setBookingDateTime("2017-04-05T10:43:07+00:00");
		data.setValueDateTime("2017-04-05T10:45:22+00:00");
		data.setTransactionInformation("Cash from Aubrey");
		data.setAddressLine("XYZ address Line");
		bankTransactionCode.setCode("ReceivedCreditTransfer");
		bankTransactionCode.setSubCode("DomesticCreditTransfer");
		data.setBankTransactionCode(bankTransactionCode);
		proprietaryBankTransactionCode.setCode("Transfer");
		proprietaryBankTransactionCode.setIssuer("AlphaBank");
		data.setProprietaryBankTransactionCode(proprietaryBankTransactionCode);
		balance.setAmount(amount2);
		amount2.setAmount("230.00");
		amount2.setCurrency("GBP");
		balance.setCreditDebitIndicator(OBCreditDebitCode2.CREDIT);
		balance.setType(OBBalanceType1Code.INTERIMBOOKED);
		data.setBalance(balance);
		data.setMerchantDetails(merchantDetails);
		merchantDetails.setMerchantCategoryCode("MerchantXYZcode");
		merchantDetails.setMerchantName("MerchantXYZ");
		dataList.add(data);
		OBReadDataTransaction6 data3 = new OBReadDataTransaction6();
		data3.setTransaction(dataList);
		resp.setData(data3);
		PlatformAccountTransactionResponse transResp=new PlatformAccountTransactionResponse();
		transResp.setoBReadTransaction6(resp);
		return transResp;
	}

	public static AispConsent getAispConsent() {
		AispConsent aispConsent = new AispConsent();
		aispConsent.setTransactionFromDateTime("2017-05-03T00:00:00");
		aispConsent.setTransactionToDateTime("2017-12-03T00:00:00");
		return aispConsent;
	}
	
	public static AispConsent getAispConsent2() {
		AispConsent aispConsent = new AispConsent();
		aispConsent.setTransactionFromDateTime("2015-01-02T00:00:00");
		aispConsent.setTransactionToDateTime("2017-01-02T00:00:00");
		return aispConsent;
	}
	
	public static AccountMapping getMockAccountMapping() {
		AccountMapping mapping = new AccountMapping();
		mapping.setTppCID("tpp123");
		mapping.setPsuId("user123");
		List<AccountDetails> selectedAccounts = new ArrayList<>();
		AccountDetails accountRequest = new AccountDetails();
		accountRequest.setAccountId("269c3ff5-d7f8-419b-a3b9-7136c5b4611a");
		accountRequest.setAccountNSC("SC802001");
		accountRequest.setAccountNumber("10203345");
		selectedAccounts.add(accountRequest);
		mapping.setAccountDetails(selectedAccounts);
		return mapping;
	}
}