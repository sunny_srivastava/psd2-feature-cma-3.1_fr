/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.domestic.scheduled.payments.consents.mock.foundationservice.service.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.apache.tomcat.util.digester.SetPropertiesRule;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.capgemini.psd2.domestic.scheduled.payments.consents.mock.foundationservice.domain.Amount;
import com.capgemini.psd2.domestic.scheduled.payments.consents.mock.foundationservice.domain.AuthorisingPartyAccount;
import com.capgemini.psd2.domestic.scheduled.payments.consents.mock.foundationservice.domain.BrandCode3;
import com.capgemini.psd2.domestic.scheduled.payments.consents.mock.foundationservice.domain.Channel;
import com.capgemini.psd2.domestic.scheduled.payments.consents.mock.foundationservice.domain.ChannelCode;
import com.capgemini.psd2.domestic.scheduled.payments.consents.mock.foundationservice.domain.ChargeBearer;
import com.capgemini.psd2.domestic.scheduled.payments.consents.mock.foundationservice.domain.Currency;
import com.capgemini.psd2.domestic.scheduled.payments.consents.mock.foundationservice.domain.FraudSystemResponse;
import com.capgemini.psd2.domestic.scheduled.payments.consents.mock.foundationservice.domain.PaymentInstructionCharge;
import com.capgemini.psd2.domestic.scheduled.payments.consents.mock.foundationservice.domain.ProposalStatus;
import com.capgemini.psd2.domestic.scheduled.payments.consents.mock.foundationservice.domain.ScheduledPaymentInstructionProposal;
import com.capgemini.psd2.domestic.scheduled.payments.consents.mock.foundationservice.exception.handler.RecordNotFoundException;
import com.capgemini.psd2.domestic.scheduled.payments.consents.mock.foundationservice.repository.DomesticScheduledPaymentConsentsRepository;
import com.capgemini.psd2.domestic.scheduled.payments.consents.mock.foundationservice.service.DomesticaScheduledPaymentConsentsService;
import com.capgemini.psd2.foundationservice.datetime.utility.TimeZoneDateTimeAdapter;
import com.capgemini.psd2.foundationservice.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.foundationservice.exceptions.MockFoundationServiceException;
import com.capgemini.psd2.foundationservice.utilities.NullCheckUtils;





// TODO: Auto-generated Javadoc
/**
 * The Class DomesticPaymentConsentsServiceImpl.
 */
@Service
public class DomesticScheduledPaymentConsentsServiceImpl implements DomesticaScheduledPaymentConsentsService {

	/** The repository. */
	@Autowired
	private DomesticScheduledPaymentConsentsRepository repository;
	@Autowired
	private TimeZoneDateTimeAdapter timeZoneDateTimeAdapter;

	@Override
	public ScheduledPaymentInstructionProposal retrieveAccountInformation(String consentId) throws Exception {

		ScheduledPaymentInstructionProposal response1 = repository.findByPaymentInstructionProposalId(consentId);
		if (null == response1) {
			throw MockFoundationServiceException.populateMockFoundationServiceException(ErrorCodeEnum.SERVER_RESOURCE_NOT_FOUND_PPA_SPIPP);
		}

		return response1;
	}

	@Override
	public ScheduledPaymentInstructionProposal createDomesticScheduledPaymentConsentsResource(
			ScheduledPaymentInstructionProposal paymentInstProposalReq) {
		String consentID = null;
		if (null == paymentInstProposalReq) {

			throw MockFoundationServiceException.populateMockFoundationServiceException(ErrorCodeEnum.SERVER_RESOURCE_NOT_FOUND_PPA_SPIPP);
		}

		consentID = UUID.randomUUID().toString();
		paymentInstProposalReq.setPaymentInstructionProposalId(consentID);

		PaymentInstructionCharge charge= new PaymentInstructionCharge();
		Amount amount = new Amount();
		charge.setChargeBearer(ChargeBearer.BorneByDebtor);
		charge.setType("UK.OBIE.ChapsOut");
		amount.setTransactionCurrency(500.01);
		charge.setAmount(amount);
		Currency currency= new Currency();
		currency.setIsoAlphaCode("GBP");
		charge.setCurrency(currency);
		List<PaymentInstructionCharge> List= new ArrayList();
		List.add(charge);
		//paymentInstProposalReq.setCharges(List);
		try {
			Date date = Calendar.getInstance().getTime();		
			String strDate = timeZoneDateTimeAdapter.parseDateTimeCMA(date);
			paymentInstProposalReq.setProposalCreationDatetime(strDate);
			paymentInstProposalReq.setProposalStatusUpdateDatetime(strDate);
		}catch(Exception e) {}
		//----Reject status for testing...
		if (paymentInstProposalReq.getInstructionReference()!=null && paymentInstProposalReq.getInstructionReference().equalsIgnoreCase("REJECTED"))
		{
			paymentInstProposalReq.setProposalStatus(ProposalStatus.Rejected);
		}
		else
		{
			paymentInstProposalReq.setProposalStatus(ProposalStatus.AwaitingAuthorisation);
			if (null != paymentInstProposalReq.getChannel()
					&& null != paymentInstProposalReq.getChannel().getBrandCode()) {

				if (paymentInstProposalReq.getChannel().getBrandCode().toString().equalsIgnoreCase("BOI-ROI")) {
					if (paymentInstProposalReq.getProposingPartyAccount().getSchemeName()
							.equalsIgnoreCase("UK.OBIE.SortCodeAccountNumber")
							|| paymentInstProposalReq.getProposingPartyAccount().getSchemeName()
									.equalsIgnoreCase("SortCodeAccountNumber")
							) {
						paymentInstProposalReq.setProposalStatus(ProposalStatus.Rejected);
					}
					else if (null != paymentInstProposalReq.getAuthorisingPartyAccount() && null != paymentInstProposalReq.getAuthorisingPartyAccount().getSchemeName()) {
						if(paymentInstProposalReq.getAuthorisingPartyAccount().getSchemeName()
									.equalsIgnoreCase("UK.OBIE.SortCodeAccountNumber")
							|| paymentInstProposalReq.getAuthorisingPartyAccount().getSchemeName()
									.equalsIgnoreCase("SortCodeAccountNumber")) {
							paymentInstProposalReq.setProposalStatus(ProposalStatus.Rejected);
						}
						
					}
				}

			}
		}
		if(paymentInstProposalReq.getInstructionEndToEndReference().equalsIgnoreCase("NOTPROVIDED"))
		{
			paymentInstProposalReq.setInstructionEndToEndReference(null);
		}
		paymentInstProposalReq.setPaymentInstructionProposalId(consentID);
		return repository.save(paymentInstProposalReq);

	}

	@Override
	public ScheduledPaymentInstructionProposal validateDomesticScheduledPaymentConsentsResource(
			ScheduledPaymentInstructionProposal paymentInstProposalReq) throws RecordNotFoundException {
		// TODO Auto-generated method stub
		repository.save(paymentInstProposalReq);
		return paymentInstProposalReq;
	}

	@Override
	public ScheduledPaymentInstructionProposal updateDomesticScheduledPaymentConsentsResource(
			String paymentInstructionProposalId, ScheduledPaymentInstructionProposal paymentInstProposalReq) throws Exception {
				
		ScheduledPaymentInstructionProposal paymentInstructionpropsal = repository.findByPaymentInstructionProposalId(paymentInstructionProposalId);
		//if (!NullCheckUtils.isNullOrEmpty(paymentInstProposalReq.getInstructionEndToEndReference())) {
		//	paymentInstructionpropsal
		//			.setInstructionEndToEndReference(paymentInstProposalReq.getInstructionEndToEndReference());
		//}
		
		
		//paymentInstructionpropsal.setProposalStatusUpdateDatetime(paymentInstProposalReq.getProposalStatusUpdateDatetime());
		paymentInstructionpropsal.setProposalStatus(ProposalStatus.fromValue(paymentInstProposalReq.getProposalStatus().toString()));
		
		
		
		  Channel channel = new Channel(); 
		  if (!NullCheckUtils.isNullOrEmpty(paymentInstProposalReq.getChannel().getChannelCode())){
		if (paymentInstProposalReq.getChannel().getChannelCode().equals(ChannelCode.BOL)) {
			channel.setChannelCode(ChannelCode.BOL);
		} 
		else if (paymentInstProposalReq.getChannel().getChannelCode().equals(ChannelCode.B365)) {
			channel.setChannelCode(ChannelCode.BOL);
		} 
		else if (paymentInstProposalReq.getChannel().getChannelCode().equals(ChannelCode.API)) {
			channel.setChannelCode(ChannelCode.API);
		}
		  } 
		  // Update Channel Brand 
		  if (!NullCheckUtils.isNullOrEmpty(paymentInstProposalReq.getChannel().getBrandCode())) {
			  if (paymentInstProposalReq.getChannel().getBrandCode().equals(BrandCode3.NIGB)) {
					channel.setBrandCode(BrandCode3.NIGB);
				}
				else if (paymentInstProposalReq.getChannel().getBrandCode().equals(BrandCode3.ROI)) {
					channel.setBrandCode(BrandCode3.ROI);
				}
		  }
		
		 
		
		//paymentInstructionpropsal.setChannel(channel);
		
		// Update FraudScore
		  if (!NullCheckUtils.isNullOrEmpty(paymentInstProposalReq.getFraudSystemResponse())) {
			  if (paymentInstProposalReq.getFraudSystemResponse().equals(FraudSystemResponse.Approve)) {
					paymentInstructionpropsal.setFraudSystemResponse(FraudSystemResponse.Approve);
				} else if (paymentInstProposalReq.getFraudSystemResponse().equals(FraudSystemResponse.Investigate)) {
					paymentInstructionpropsal.setFraudSystemResponse(FraudSystemResponse.Investigate);
				} else if (paymentInstProposalReq.getFraudSystemResponse().equals(FraudSystemResponse.Reject)) {
					paymentInstructionpropsal.setFraudSystemResponse(FraudSystemResponse.Reject);
				}
		  }
		
		
		// Update Debtors Details
		AuthorisingPartyAccount authorisingPartyAccount = new AuthorisingPartyAccount();
		if (!NullCheckUtils.isNullOrEmpty(paymentInstProposalReq.getAuthorisingPartyAccount())) {
			authorisingPartyAccount.setSchemeName(paymentInstProposalReq.getAuthorisingPartyAccount().getSchemeName());
			authorisingPartyAccount.setAccountIdentification(
								paymentInstProposalReq.getAuthorisingPartyAccount().getAccountIdentification());
			authorisingPartyAccount
							.setAccountName(paymentInstProposalReq.getAuthorisingPartyAccount().getAccountName());
			authorisingPartyAccount
					.setAccountNumber(paymentInstProposalReq.getAuthorisingPartyAccount().getAccountNumber());
			authorisingPartyAccount.setSecondaryIdentification(
			paymentInstProposalReq.getAuthorisingPartyAccount().getSecondaryIdentification());
			paymentInstructionpropsal.setAuthorisingPartyAccount(authorisingPartyAccount);
		}
		
		paymentInstructionpropsal
				.setProposalStatusUpdateDatetime(paymentInstProposalReq.getProposalStatusUpdateDatetime());
		
		
		repository.save(paymentInstructionpropsal);
		return paymentInstProposalReq;
	}

}
