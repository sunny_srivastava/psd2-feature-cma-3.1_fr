/*************************************************************************
 * 
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 * 
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 */
package com.capgemini.psd2.security.consent.cisp.view.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.naming.NamingException;
import javax.naming.directory.BasicAttributes;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.capgemini.psd2.aisp.domain.OBAccount6;
import com.capgemini.psd2.aisp.domain.OBReadAccount6;
import com.capgemini.psd2.aisp.domain.OBReadAccount6Data;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmationConsent1DataDebtorAccount;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmationConsentResponse1;
import com.capgemini.psd2.consent.domain.PSD2Account;
import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.integration.adapter.TPPInformationAdaptor;
import com.capgemini.psd2.logger.LoggerUtils;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.mask.DataMask;
import com.capgemini.psd2.scaconsenthelper.config.CdnConfig;
import com.capgemini.psd2.scaconsenthelper.config.helpers.SCAConsentHelper;
import com.capgemini.psd2.scaconsenthelper.constants.OIDCConstants;
import com.capgemini.psd2.scaconsenthelper.models.PickupDataModel;
import com.capgemini.psd2.security.consent.cisp.config.CispHostNameConfig;
import com.capgemini.psd2.security.consent.cisp.helpers.CispConsentCreationDataHelper;
import com.capgemini.psd2.security.constants.PSD2SecurityConstants;
import com.capgemini.psd2.tppinformation.adaptor.ldap.constants.TPPInformationConstants;
import com.capgemini.psd2.ui.content.utility.controller.UIStaticContentUtilityController;
import com.capgemini.psd2.utilities.JSONUtilities;
import com.capgemini.psd2.utilities.NullCheckUtils;
import com.capgemini.psd2.utilities.SandboxConfig;

/**
 * ConsentView Controller
 * 
 * @author Capgemini
 * 
 */
@Controller
@ConfigurationProperties("app")
public class CispConsentViewController {

	private static final Logger LOG = LoggerFactory.getLogger(CispConsentViewController.class);
	
	@Autowired
	private LoggerUtils loggerUtils;

	@Autowired
	private CispHostNameConfig edgeserverhost;

	@Value("${spring.application.name}")
	private String applicationName;

	@Autowired
	private TPPInformationAdaptor tppInformationAdaptor;

	@Autowired
	private UIStaticContentUtilityController uiController;

	@Autowired
	private RequestHeaderAttributes requestHeaders;

	@Autowired
	private CispConsentCreationDataHelper consentCreationDataHelper;

	@Autowired
	private HttpServletRequest request;

	@Autowired
	private HttpServletResponse response;

	@Autowired
	private DataMask dataMask;
	
	@Autowired
	private SandboxConfig sandboxConfig;

	@Value("${cdn.baseURL}")
	private String cdnBaseURL;

	private Map<String, String> consentSupportedSchemeMap = new HashMap<>();

	public Map<String, String> getConsentSupportedSchemeMap() {
		return consentSupportedSchemeMap;
	}

	@RequestMapping(value = "/cisp/home")
	public ModelAndView homePage(Map<String, Object> model) {
		SCAConsentHelper.populateSecurityHeaders(response);

		String homeScreen = "/customerconsentview";
		String requestUrl = request.getRequestURI();
		requestUrl = requestUrl.replace("/home", homeScreen);
		model.put("redirectUrl", edgeserverhost.getTenantSpecificEdgeserverhost(requestHeaders.getTenantId()).concat("/").concat(applicationName).concat(requestUrl).concat("?")
				.concat(request.getQueryString()));
		model.put(PSD2Constants.NETSCALER_TENANT_ID_HEADER_NAME, requestHeaders.getTenantId());
		model.put(PSD2Constants.IS_SANDBOX_ENABLED,sandboxConfig.isSandboxEnabled());
		return new ModelAndView("homePage", model);
	}

	/**
	 * customerconsentview end-point of consent application gets invoked by
	 * OAuth2 server with some params like - call back URI of OAuth2 server & ID
	 * token which contains the identity information of the customer. It
	 * collects the data for allowed scopes of the caller AISP & fetches
	 * accounts list of the current customer by using the user-id information
	 * taken from the ID token (SaaS Server issued ID token) which gets relayed
	 * by the OAuth server to consent app.
	 * 
	 * @param model
	 * @param oAuthUrl
	 * @param idToken
	 * @return
	 * @throws NamingException
	 */
	@RequestMapping(value = "/cisp/customerconsentview")
	public ModelAndView consentView(Map<String, Object> model) throws NamingException {
		boolean isAccounSelected = false;
		boolean isConsentAccountsPresent = Boolean.FALSE;
		OBFundsConfirmationConsentResponse1 setUpResponseData;
		String accountInfoList;

		PickupDataModel pickupDataModel = SCAConsentHelper.populatePickupDataModel(request);

		model.put(PSD2Constants.IS_SANDBOX_ENABLED,sandboxConfig.isSandboxEnabled());
		
		OBReadAccount6 consentAccounts = consentCreationDataHelper
				.findExistingConsentAccounts(pickupDataModel.getIntentId());

		setUpResponseData = consentCreationDataHelper.retrieveFundsConfirmationSetupData(pickupDataModel.getIntentId());

		if (consentAccounts != null && consentAccounts.getData() != null
				&& !consentAccounts.getData().getAccount().isEmpty()) {
			// renewal case
			LOG.info("{\"Enter\":\"{}\",\"{}\"}",
					"com.capgemini.psd2.security.consent.cisp.view.controllers.consentView()",
					loggerUtils.populateLoggerData("renewalConsentFlow"));
			isConsentAccountsPresent = Boolean.TRUE;
			accountInfoList = dataMask.maskResponseGenerateString(consentAccounts, "account");
		} else {

			LOG.info("{\"Enter\":\"{}\",\"{}\"}",
					"com.capgemini.psd2.security.consent.cisp.view.controllers.consentView()",
					loggerUtils.populateLoggerData("freshConsentFlow"));
			
			OBReadAccount6 customerAccountList = consentCreationDataHelper.retrieveCustomerAccountListInfo(
					pickupDataModel.getUserId(), pickupDataModel.getIntentTypeEnum().getIntentType(),
					requestHeaders.getCorrelationId(), pickupDataModel.getChannelId(),
					setUpResponseData.getData().getDebtorAccount().getSchemeName(),
					requestHeaders.getTenantId(), pickupDataModel.getIntentId());

			if (setUpResponseData != null && setUpResponseData.getData() != null //NOSONAR
					&& !NullCheckUtils.isNullOrEmpty(setUpResponseData.getData().getDebtorAccount()) && !NullCheckUtils
							.isNullOrEmpty(setUpResponseData.getData().getDebtorAccount().getIdentification())) {

				isAccounSelected = true;
				customerAccountList = validateAccountWithAccountList(setUpResponseData, customerAccountList);

			}
			accountInfoList = dataMask.maskResponseGenerateString(customerAccountList, "account");
		}

		if (isConsentAccountsPresent) {
			model.put(PSD2SecurityConstants.REFRESH_TOKEN_RENEWAL_FLOW, Boolean.TRUE);
		}

		Object tppInformationObj = tppInformationAdaptor.fetchTPPInformation(pickupDataModel.getClientId());
		String tppApplicationName = tppInformationAdaptor.fetchApplicationName(pickupDataModel.getClientId());

		model.put(PSD2SecurityConstants.CUSTOMER_ACCOUNT_LIST, accountInfoList);
		model.put(PSD2SecurityConstants.CONSENT_SETUP_DATA, JSONUtilities.getJSONOutPutFromObject(setUpResponseData));
		model.put(PSD2Constants.CONSENT_FLOW_TYPE, pickupDataModel.getIntentTypeEnum().getIntentType());
		model.put(PSD2Constants.CO_RELATION_ID, requestHeaders.getCorrelationId());
		model.put(PSD2SecurityConstants.USER_ID, pickupDataModel.getUserId());
		model.put(PSD2Constants.APPLICATION_NAME, applicationName);
		
		if (tppInformationObj != null && tppInformationObj instanceof BasicAttributes) {
			JSONObject tppInfoJson = new JSONObject();
			tppInfoJson.put(PSD2SecurityConstants.APP_NAME, tppApplicationName);

			BasicAttributes tppInfoAttributes = (BasicAttributes) tppInformationObj;
			String legalEntityName = returnLegalEntityName(tppInfoAttributes);
			model.put(PSD2SecurityConstants.TPP_NAME, legalEntityName);
			tppInfoJson.put(PSD2SecurityConstants.TPP_NAME, legalEntityName);
			model.put(PSD2SecurityConstants.TPP_INFO, tppInfoJson.toString());
			
			requestHeaders.setTppLegalEntityName(legalEntityName);

		}

		model.put(PSD2SecurityConstants.ACCOUNT_SELECTED, isAccounSelected);
		model.put(PSD2Constants.UICONTENT, uiController.getConfigVariable());
		model.put(OIDCConstants.CHANNEL_ID,pickupDataModel.getChannelId());
		model.put(PSD2Constants.NETSCALER_TENANT_ID_HEADER_NAME, requestHeaders.getTenantId());
		
		model.putAll(CdnConfig.populateCdnAttributes());
		
		LOG.info("{\"Exit\":\"{}\",\"{}\"}",
				"com.capgemini.psd2.security.consent.cisp.view.controllers.consentView()",
				loggerUtils.populateLoggerData("consentView"));
		
		return new ModelAndView("index", model);
	}

	private String returnLegalEntityName(BasicAttributes basicAttributes) throws NamingException {
		String legalEntityName = getAttributeValue(basicAttributes, TPPInformationConstants.LEGAL_ENTITY_NAME);
		return legalEntityName;
	}

	private String getAttributeValue(BasicAttributes tppApplication, String ldapAttr) throws NamingException {
		String attributeValue = null;
		if (tppApplication.get(ldapAttr) != null && tppApplication.get(ldapAttr).get() != null) {
			attributeValue = tppApplication.get(ldapAttr).get().toString();
		}
		return attributeValue;
	}

	private OBReadAccount6 validateAccountWithAccountList(OBFundsConfirmationConsentResponse1 setUpResponseData,
			OBReadAccount6 obReadAccount2) {
		boolean isAccountMatch = false;

		OBFundsConfirmationConsent1DataDebtorAccount debtorDetails = setUpResponseData.getData().getDebtorAccount();
		String debtorSchemeName = debtorDetails.getSchemeName();

		if (obReadAccount2 != null && !obReadAccount2.getData().getAccount().isEmpty()) {
			for (OBAccount6 account : obReadAccount2.getData().getAccount()) {

				if (account instanceof PSD2Account) {
					Map<String, String> additionalInfo = ((PSD2Account) account).getAdditionalInformation();

					if (!consentSupportedSchemeMap.containsKey(debtorSchemeName)) {
						throw PSD2Exception.populatePSD2Exception(
								ErrorCodeEnum.PROVIDED_DEBTOR_SCHEME_NOT_SUPPORTED_FOR_CONSENT_APP);
					}

					String ymlScheme = debtorDetails.getSchemeName(); // Scheme From Payload

					int len = ymlScheme.split("\\.").length;
					ymlScheme = ymlScheme.split("\\.")[len - 1];

					if ((additionalInfo.containsKey(ymlScheme)
							&& additionalInfo.get(ymlScheme).equals(debtorDetails.getIdentification()))) {
						isAccountMatch = true;
							obReadAccount2 = populateAccountData(account, debtorDetails); //NOSONAR
						break;
					}
				}
			}
			if (!isAccountMatch)
				throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.SELECTED_ACCOUNTS_NOT_OWNED_BY_CUSTOMER);
		} else
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.SELECTED_ACCOUNTS_NOT_OWNED_BY_CUSTOMER);
		return obReadAccount2;
	}

	private OBReadAccount6 populateAccountData(OBAccount6 account, OBFundsConfirmationConsent1DataDebtorAccount debtorAcccount) {
		List<OBAccount6> accountData = new ArrayList<>();
		account.getAccount().get(0).setSecondaryIdentification(debtorAcccount.getSecondaryIdentification());
		account.getAccount().get(0).setIdentification(debtorAcccount.getIdentification());
		account.getAccount().get(0).setSchemeName(debtorAcccount.getSchemeName());
		accountData.add(account);
		OBReadAccount6Data data2 = new OBReadAccount6Data();
		data2.setAccount(accountData);
		OBReadAccount6 OBReadAccount2 = new OBReadAccount6();
		OBReadAccount2.setData(data2);
		return OBReadAccount2;

	}
}