package com.capgemini.psd2.security.consent.cisp.helpers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.boot.context.properties.ConfigurationProperties;

import com.capgemini.psd2.aisp.domain.OBAccount6;
import com.capgemini.psd2.aisp.domain.OBAccount6Account;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.CispConsent;
import com.capgemini.psd2.consent.domain.PSD2Account;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.scaconsenthelper.config.helpers.ConsentAuthorizationHelper;

@ConfigurationProperties("app")
public class CispConsentAuthorizationHelper extends ConsentAuthorizationHelper {

	private Map<String, String> consentSupportedSchemeMap = new HashMap<>();

	public Map<String, String> getConsentSupportedSchemeMap() {
		return consentSupportedSchemeMap;
	}
	
	@Override
	public PSD2Account populateAccountwithUnmaskedValues(PSD2Account accountwithMask, PSD2Account accountwithoutMask) {

		Map<String, String> additionalInfo = accountwithoutMask.getAdditionalInformation();
		Map<String, String> additionalInfotobeUnmasked = accountwithMask.getAdditionalInformation();

		OBAccount6Account maskAccount = accountwithMask.getAccount().get(0);
		if (accountwithoutMask.getAccount() != null) {
			String dbSchemeName = consentSupportedSchemeMap.get(maskAccount.getSchemeName());
			String schemeIdentification = accountwithoutMask.getAdditionalInformation()
					.get(consentSupportedSchemeMap.get(dbSchemeName));

			maskAccount.setIdentification(schemeIdentification);
		}

		/*
		 * Setting (unmasked) account-number and account-nsc from channel
		 * profile additional info
		 */
		if (additionalInfo != null && additionalInfotobeUnmasked != null) {
			additionalInfotobeUnmasked.put(PSD2Constants.ACCOUNT_NUMBER,
					additionalInfo.get(PSD2Constants.ACCOUNT_NUMBER));
			additionalInfotobeUnmasked.put(PSD2Constants.ACCOUNT_NSC, additionalInfo.get(PSD2Constants.ACCOUNT_NSC));
		}

		return accountwithMask;
	}

	public <T> List<OBAccount6> populateAccountListFromAccountDetails(T input) {
		AccountDetails accountDetail = ((CispConsent) input).getAccountDetails();
		List<OBAccount6> accountList = new ArrayList<>();
		PSD2Account account = new PSD2Account();
		List<OBAccount6Account> data2AccountList = new ArrayList<>();
		OBAccount6Account data2Account = new OBAccount6Account();
		data2Account.setIdentification(accountDetail.getAccountNumber());
		data2AccountList.add(data2Account);
		account.setAccount(data2AccountList);
		accountList.add(account);
		return accountList;
	}

}
