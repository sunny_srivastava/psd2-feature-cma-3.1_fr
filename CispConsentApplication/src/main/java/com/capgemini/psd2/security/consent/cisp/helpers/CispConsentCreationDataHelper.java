package com.capgemini.psd2.security.consent.cisp.helpers;

import java.util.Map;

import javax.naming.NamingException;

import com.capgemini.psd2.aisp.domain.OBAccount6;
import com.capgemini.psd2.aisp.domain.OBReadAccount6;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmationConsentResponse1;

public interface CispConsentCreationDataHelper {

	public void cancelFundsConfirmationSetup(String fundsIntentId, Map<String, String> paramsMap);

	public OBReadAccount6 retrieveCustomerAccountListInfo(String userId, String flowType, String schemeName,
			String correlationId, String channelId, String tenantId, String intentId);

	public void createConsent(OBAccount6 account, String userId, String cid, String fundsIntentId, String channelId,
			String tppAppName, Object tppInformation,String tenantId) throws NamingException;

	public OBFundsConfirmationConsentResponse1 retrieveFundsConfirmationSetupData(String fundsIntentId);

	public OBReadAccount6 findExistingConsentAccounts(String intentId);
}
