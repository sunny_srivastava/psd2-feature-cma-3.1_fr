package com.capgemini.psd2.security.consent.cisp.helpers;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.naming.NamingException;
import javax.naming.directory.BasicAttributes;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.aisp.adapter.CustomerAccountListAdapter;
import com.capgemini.psd2.aisp.domain.OBAccount6;
import com.capgemini.psd2.aisp.domain.OBReadAccount6;
import com.capgemini.psd2.aisp.domain.OBReadAccount6Data;
import com.capgemini.psd2.cisp.adapter.CispConsentAdapter;
import com.capgemini.psd2.cisp.adapter.FundsConfirmationConsentAdapter;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmationConsentResponse1;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmationConsentResponse1Data;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmationConsentResponse1Data.StatusEnum;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.CispConsent;
import com.capgemini.psd2.consent.domain.PSD2Account;
import com.capgemini.psd2.enums.ConsentStatusEnum;
import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.funds.confirmation.consent.mongo.db.adapter.repository.FundsConfirmationConsentRepository;
import com.capgemini.psd2.logger.LoggerUtils;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.product.common.CompatibleVersionList;
import com.capgemini.psd2.scaconsenthelper.config.helpers.ConsentAuthorizationHelper;
import com.capgemini.psd2.scaconsenthelper.config.helpers.ConsentViewHelper;
import com.capgemini.psd2.scaconsenthelper.config.helpers.SCAConsentAdapterHelper;
import com.capgemini.psd2.tppinformation.adaptor.ldap.constants.TPPInformationConstants;
import com.capgemini.psd2.utilities.DateUtilites;
import com.capgemini.psd2.utilities.JSONUtilities;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Component
@ConfigurationProperties
public class CispConsentCreationDataHelperImpl implements CispConsentCreationDataHelper {

	private static final Logger LOG = LoggerFactory.getLogger(CispConsentCreationDataHelperImpl.class);
	@Autowired
	private LoggerUtils loggerUtils;

	@Autowired
	@Qualifier("CustomerAccountListAdapter")
	private CustomerAccountListAdapter customerAccountListAdapter;

	@Autowired
	private CispConsentAdapter cispConsentAdapter;

	@Autowired
	private SCAConsentAdapterHelper scaConsentAdapterHelper;

	@Autowired
	@Qualifier("cispConsentAuthorizationHelper")
	private ConsentAuthorizationHelper consentAuthorizationHelper;

	@Value("${cmaVersion}")
	private String cmaVersion;

	@Autowired
	private FundsConfirmationConsentRepository fundsConfirmationConsentRepository;

	@Autowired
	private CompatibleVersionList compatibleVersionList;

	@Override
	public OBFundsConfirmationConsentResponse1 retrieveFundsConfirmationSetupData(String fundsIntentId) {
		return scaConsentAdapterHelper.getFundsConfirmationSetupData(fundsIntentId);
	}

	@Override
	public OBReadAccount6 retrieveCustomerAccountListInfo(String userId, String flowType, String correlationId,
			String channelId, String schemeName, String tenantId, String intentId) {
		Map<String, String> paramsMap = new HashMap<>();
		paramsMap.put(PSD2Constants.CMAVERSION, getCMAVersion(intentId));
		paramsMap.put(PSD2Constants.CHANNEL_ID, channelId);
		paramsMap.put(PSD2Constants.USER_ID, userId);
		paramsMap.put(PSD2Constants.CORRELATION_ID, correlationId);
		paramsMap.put(PSD2Constants.CONSENT_FLOW_TYPE, flowType);
		paramsMap.put(PSD2Constants.SCHEME_NAME, schemeName);
		paramsMap.put(PSD2Constants.TENANT_ID, tenantId);
		OBReadAccount6 OBReadAccount2 = customerAccountListAdapter.retrieveCustomerAccountList(userId, paramsMap);

		return OBReadAccount2;
	}

	@Override
	public void createConsent(OBAccount6 customerAccount, String userId, String cid, String intentId, String channelId,
			String tppApplicaitonName, Object tppInformationObj, String tenantId) throws NamingException {

		LOG.info("{\"Enter\":\"{}\",\"{}\"}", "com.capgemini.psd2.security.consent.cisp.helpers.createConsent()",
				loggerUtils.populateLoggerData("createConsent"));

		AccountDetails acctDetail = new AccountDetails();
		String legalEntityName = null;
		CispConsent consent = new CispConsent();
		PSD2Account customerAccountWithAdditionalInfo = (PSD2Account) customerAccount;
		acctDetail.setHashValue(customerAccountWithAdditionalInfo.getHashedValue());

		Map<String, String> additionalInfo = ((PSD2Account) customerAccount).getAdditionalInformation();

		String accountNumber;
		String accountNSC;

		accountNSC = additionalInfo.get(PSD2Constants.ACCOUNT_NSC);
		accountNumber = additionalInfo.get(PSD2Constants.ACCOUNT_NUMBER);

		acctDetail.setAccountNumber(accountNumber);
		acctDetail.setAccountNSC(accountNSC);
		acctDetail.setAccountSubType(customerAccountWithAdditionalInfo.getAccountSubType());

		if (tppInformationObj != null && tppInformationObj instanceof BasicAttributes) {
			BasicAttributes tppInfoAttributes = (BasicAttributes) tppInformationObj;
			legalEntityName = returnLegalEntityName(tppInfoAttributes);
		}
		consent.setTppApplicationName(tppApplicaitonName);
		consent.setTppLegalEntityName(legalEntityName);

		consent.setTenantId(tenantId);
		consent.setPartyIdentifier(additionalInfo.get(PSD2Constants.PARTY_IDENTIFIER));

		consent.setChannelId(channelId);
		consent.setAccountDetails(acctDetail);
		consent.setPsuId(userId);
		consent.setTppCId(cid);
		consent.setFundsIntentId(intentId);
		ZonedDateTime startDate = ZonedDateTime.now();
		consent.setStartDate(DateUtilites.getCurrentDateInISOFormat(startDate));

		OBFundsConfirmationConsentResponse1 fundsConfirmationConsentData = this
				.retrieveFundsConfirmationSetupData(intentId);

		if (!NullCheckUtils.isNullOrEmpty(fundsConfirmationConsentData.getData().getExpirationDateTime())) {
			String consentExpiry = fundsConfirmationConsentData.getData().getExpirationDateTime();
			consent.setEndDate(consentExpiry);
		}

		/* Consent Version should be same as Setup Version */
		consent.setCmaVersion(fundsConfirmationConsentData.getData().getCmaVersion());

		cispConsentAdapter.createConsent(consent);

		LOG.info("{\"Exit\":\"{}\",\"{}\",\"createdCispConsent\":{}}",
				"com.capgemini.psd2.security.consent.cisp.helpers.createConsent()",
				loggerUtils.populateLoggerData("createConsent"), JSONUtilities.getJSONOutPutFromObject(consent));
	}

	@Override
	public void cancelFundsConfirmationSetup(String intentId, Map<String, String> paramsMap) {
		// The case is if the consent present but token has not been issued yet
		// and allowing the TPP go initate auth flow again with the same intent.
		CispConsent consent = cispConsentAdapter.retrieveConsentByFundsIntentId(intentId);
		// Defect Fix For P000428-791 : Consent status will be updated only for
		// AwaitingAuthorization
		if (consent != null) {
			if (ConsentStatusEnum.REVOKED.equals(consent.getStatus()))
				throw PSD2Exception.populatePSD2Exception("Consent is not in correct status",ErrorCodeEnum.INTENT_ID_VALIDATION_ERROR);
			/*
			 * v3.1.8 change, DELETED status implies that the resource doesn't exist
			 */
			else if (ConsentStatusEnum.DELETED.equals(consent.getStatus()))
				throw PSD2Exception.populatePSD2Exception("Consent not found",ErrorCodeEnum.INTENT_ID_VALIDATION_ERROR);
			else if (ConsentStatusEnum.AWAITINGAUTHORISATION.equals(consent.getStatus()))
				cispConsentAdapter.updateConsentStatus(consent.getConsentId(), ConsentStatusEnum.REJECTED);
		}

		scaConsentAdapterHelper.updateFundsConfirmationSetupData(intentId,OBFundsConfirmationConsentResponse1Data.StatusEnum.REJECTED);

	}

	private String returnLegalEntityName(BasicAttributes basicAttributes) throws NamingException {
		String legalEntityName = getAttributeValue(basicAttributes, TPPInformationConstants.LEGAL_ENTITY_NAME);
		return legalEntityName;
	}

	private String getAttributeValue(BasicAttributes tppApplication, String ldapAttr) throws NamingException {
		String attributeValue = null;
		if (tppApplication.get(ldapAttr) != null && tppApplication.get(ldapAttr).get() != null) {
			attributeValue = tppApplication.get(ldapAttr).get().toString();
		}
		return attributeValue;
	}

	@Override
	public OBReadAccount6 findExistingConsentAccounts(String intentId) {
		List<OBAccount6> consentAccounts = null;
		OBReadAccount6 OBReadAccount2 = new OBReadAccount6();
		OBReadAccount6Data data2 = new OBReadAccount6Data();
		List<OBAccount6> accountList = new ArrayList<>();
		data2.setAccount(accountList);
		CispConsent cispConsent = cispConsentAdapter.retrieveConsentByFundsIntentIdAndStatus(intentId,
				ConsentStatusEnum.AUTHORISED);
		if (cispConsent != null) {
			consentAccounts = ConsentViewHelper.populateAccountListFromAccountDetails(cispConsent);
			data2.getAccount().addAll(consentAccounts);
			OBReadAccount2.setData(data2);
		}
		return OBReadAccount2;
	}

	private String getCMAVersion(String intentId) {
		OBFundsConfirmationConsentResponse1Data cispConsentReq = fundsConfirmationConsentRepository
				.findByConsentIdAndCmaVersionIn(intentId, compatibleVersionList.fetchVersionList());
		if (cispConsentReq == null) {
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.FUNDS_DATA_NOT_FOUND);
		}
		return (cispConsentReq.getCmaVersion() == null) ? PSD2Constants.CMA1 : cispConsentReq.getCmaVersion();
	}
}
