package com.capgemini.psd2.security.consent.cisp.view.test.config;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.naming.NamingException;
import javax.naming.directory.BasicAttributes;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.aisp.domain.OBAccount6;
import com.capgemini.psd2.aisp.domain.OBAccount6Account;
import com.capgemini.psd2.aisp.domain.OBReadAccount6;
import com.capgemini.psd2.aisp.domain.OBReadAccount6Data;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmationConsent1DataDebtorAccount;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmationConsentResponse1;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmationConsentResponse1Data;
import com.capgemini.psd2.consent.domain.PSD2Account;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.integration.adapter.TPPInformationAdaptor;
import com.capgemini.psd2.logger.LoggerUtils;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.mask.DataMask;
import com.capgemini.psd2.scaconsenthelper.models.IntentTypeEnum;
import com.capgemini.psd2.scaconsenthelper.models.PickupDataModel;
import com.capgemini.psd2.security.consent.cisp.config.CispHostNameConfig;
import com.capgemini.psd2.security.consent.cisp.helpers.CispConsentCreationDataHelper;
import com.capgemini.psd2.security.consent.cisp.view.controllers.CispConsentViewController;
import com.capgemini.psd2.ui.content.utility.controller.UIStaticContentUtilityController;
import com.capgemini.psd2.utilities.SandboxConfig;

public class CispConsentViewControllerTest {

	@Mock
	private TPPInformationAdaptor tppInformationAdaptor;

	@Mock
	private UIStaticContentUtilityController uiController;

	@Mock
	private RequestHeaderAttributes requestHeaders;

	@Mock
	private CispConsentCreationDataHelper consentCreationDataHelper;

	@Mock
	private HttpServletRequest request;

	@Mock
	private HttpServletResponse response;

	@Mock
	private DataMask dataMask;
	
	@Mock
	private CispHostNameConfig edgeserverhost;
	
	@Mock
	private SandboxConfig sandboxConfig;
	
	@Mock
	private LoggerUtils loggerUtils;
	
	@Mock
	private Map<String, String> consentSupportedSchemeMap;

	@InjectMocks
	private CispConsentViewController cispConsentViewController;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		when(requestHeaders.getTenantId()).thenReturn("tenant1");
	}

	@Test
	public void testConsentView() throws NamingException {
		PickupDataModel pickupDataModel = new PickupDataModel();
		pickupDataModel.setIntentTypeEnum(IntentTypeEnum.CISP_INTENT_TYPE);
		when(request.getAttribute("intentData")).thenReturn(pickupDataModel);
		when(consentCreationDataHelper.findExistingConsentAccounts(anyString())).thenReturn(getCustomerAccountInfo());
		when(dataMask.maskResponseGenerateString(anyObject(), anyString())).thenReturn("123");
		Map<String, Object> model = new HashMap<>();

		Object tppInfo = new Object();
		when(tppInformationAdaptor.fetchTPPInformation(anyString())).thenReturn(tppInfo);
		when(tppInformationAdaptor.fetchApplicationName(anyString())).thenReturn("string");
		when(uiController.getConfigVariable()).thenReturn("random");
		cispConsentViewController.consentView(model);
	}
	
	@Test
	public void testGetConsentSupportedSchemeMap() {
		assertEquals(new HashMap<String, String>(), cispConsentViewController.getConsentSupportedSchemeMap());
	}

	@Test
	public void testConsentViewElseTest() throws NamingException {
		PickupDataModel pickupDataModel = new PickupDataModel();
		pickupDataModel.setIntentTypeEnum(IntentTypeEnum.CISP_INTENT_TYPE);
		pickupDataModel.setIntentId("intentId");
		pickupDataModel.setUserId("userId");
		requestHeaders.setCorrelationId("correlationId");
		pickupDataModel.setChannelId("ChannelId");

		when(request.getAttribute("intentData")).thenReturn(pickupDataModel);
		when(dataMask.maskResponseGenerateString(anyObject(), anyString())).thenReturn("123");
		Map<String, Object> model = new HashMap<>();
		OBFundsConfirmationConsentResponse1 setUpResponseData = new OBFundsConfirmationConsentResponse1();
		OBFundsConfirmationConsentResponse1Data data = new OBFundsConfirmationConsentResponse1Data();
		setUpResponseData.setData(data);
		OBFundsConfirmationConsent1DataDebtorAccount debtorAccount = new OBFundsConfirmationConsent1DataDebtorAccount();
		data.setDebtorAccount(debtorAccount);
		data.getDebtorAccount().setSchemeName("ABC");

		Object tppInfo = new Object();
		when(tppInformationAdaptor.fetchTPPInformation(anyString())).thenReturn(tppInfo);
		when(tppInformationAdaptor.fetchApplicationName(anyString())).thenReturn("string");
		when(uiController.getConfigVariable()).thenReturn("random");
		when(consentCreationDataHelper.retrieveFundsConfirmationSetupData(pickupDataModel.getIntentId()))
				.thenReturn(setUpResponseData);
		cispConsentViewController.consentView(model);
	}
	
	@Test
	public void testConsentViewElseTest1() throws NamingException {
		PickupDataModel pickupDataModel = new PickupDataModel();
		pickupDataModel.setIntentTypeEnum(IntentTypeEnum.CISP_INTENT_TYPE);
		pickupDataModel.setIntentId("intentId");
		pickupDataModel.setUserId("userId");
		requestHeaders.setCorrelationId("correlationId");
		pickupDataModel.setChannelId("ChannelId");

		when(request.getAttribute("intentData")).thenReturn(pickupDataModel);
		when(dataMask.maskResponseGenerateString(anyObject(), anyString())).thenReturn("123");
		Map<String, Object> model = new HashMap<>();
		OBFundsConfirmationConsentResponse1 setUpResponseData = new OBFundsConfirmationConsentResponse1();
		OBFundsConfirmationConsentResponse1Data data = new OBFundsConfirmationConsentResponse1Data();
		setUpResponseData.setData(data);
		OBFundsConfirmationConsent1DataDebtorAccount debtorAccount = new OBFundsConfirmationConsent1DataDebtorAccount();
		data.setDebtorAccount(debtorAccount);
		data.getDebtorAccount().setSchemeName("ABC");

		Object tppInfo = new Object();
		BasicAttributes tppInformationObj = new BasicAttributes("123", 12);
		when(tppInformationAdaptor.fetchTPPInformation(anyString())).thenReturn(tppInformationObj);
		when(tppInformationAdaptor.fetchApplicationName(anyString())).thenReturn("string");
		when(uiController.getConfigVariable()).thenReturn("random");
		when(consentCreationDataHelper.retrieveFundsConfirmationSetupData(pickupDataModel.getIntentId()))
				.thenReturn(setUpResponseData);
		cispConsentViewController.consentView(model);
	}

	@Test(expected = PSD2Exception.class)
	public void testConsentViewElseNotNullTest() throws NamingException {
		PickupDataModel pickupDataModel = new PickupDataModel();
		pickupDataModel.setIntentTypeEnum(IntentTypeEnum.CISP_INTENT_TYPE);
		pickupDataModel.setIntentId("intentId");
		pickupDataModel.setUserId("userId");
		requestHeaders.setCorrelationId("correlationId");
		pickupDataModel.setChannelId("ChannelId");
		when(request.getAttribute("intentData")).thenReturn(pickupDataModel);
		when(dataMask.maskResponseGenerateString(anyObject(), anyString())).thenReturn("123");
		Map<String, Object> model = new HashMap<>();
		OBFundsConfirmationConsentResponse1 setUpResponseData = new OBFundsConfirmationConsentResponse1();
		OBFundsConfirmationConsentResponse1Data data = new OBFundsConfirmationConsentResponse1Data();
		data.setConsentId("consentId");
		setUpResponseData.setData(data);
		OBFundsConfirmationConsent1DataDebtorAccount debtorAccount = new OBFundsConfirmationConsent1DataDebtorAccount();
		debtorAccount.setIdentification("identification");
		debtorAccount.setSchemeName("UK.OBIE.IBAN");
		setUpResponseData.getData().setDebtorAccount(debtorAccount);
		BasicAttributes tppInfo = new BasicAttributes();
		tppInfo.put("o", new Object());
		when(tppInformationAdaptor.fetchTPPInformation(anyString())).thenReturn(tppInfo);
		when(tppInformationAdaptor.fetchApplicationName(anyString())).thenReturn("string");
		when(uiController.getConfigVariable()).thenReturn("random");
		when(consentCreationDataHelper.retrieveFundsConfirmationSetupData(pickupDataModel.getIntentId()))
				.thenReturn(setUpResponseData);
		when(consentCreationDataHelper.retrieveCustomerAccountListInfo(pickupDataModel.getUserId(),
				pickupDataModel.getIntentTypeEnum().getIntentType(), requestHeaders.getCorrelationId(),
				pickupDataModel.getChannelId(), setUpResponseData.getData().getDebtorAccount().getSchemeName().toString(),
				requestHeaders.getTenantId(), pickupDataModel.getIntentId())).thenReturn(getCustomerAccountInfo());
		when(consentSupportedSchemeMap.containsKey(anyString())).thenReturn(true);
		when(consentSupportedSchemeMap.get(anyString())).thenReturn("identification");
		cispConsentViewController.consentView(model);
	}

	@Test(expected = PSD2Exception.class)
	public void testConsentViewElseNotNull1Test() throws NamingException {

		requestHeaders.setCorrelationId("correlationId");
		PickupDataModel pickupDataModel = new PickupDataModel();
		pickupDataModel.setIntentTypeEnum(IntentTypeEnum.CISP_INTENT_TYPE);
		pickupDataModel.setIntentId("intentId");
		when(request.getAttribute("intentData")).thenReturn(pickupDataModel);
		when(dataMask.maskResponseGenerateString(anyObject(), anyString())).thenReturn("123");
		Map<String, Object> model = new HashMap<>();
		OBFundsConfirmationConsentResponse1 setUpResponseData = new OBFundsConfirmationConsentResponse1();
		OBFundsConfirmationConsentResponse1Data data = new OBFundsConfirmationConsentResponse1Data();
		data.setConsentId("consentId");
		setUpResponseData.setData(data);
		OBFundsConfirmationConsent1DataDebtorAccount debtorAccount = new OBFundsConfirmationConsent1DataDebtorAccount();
		debtorAccount.setIdentification("identification");
		debtorAccount.setSchemeName("ABC");
		setUpResponseData.getData().setDebtorAccount(debtorAccount);
		Object tppInfo = new Object();
		when(tppInformationAdaptor.fetchTPPInformation(anyString())).thenReturn(tppInfo);
		when(tppInformationAdaptor.fetchApplicationName(anyString())).thenReturn("string");
		when(uiController.getConfigVariable()).thenReturn("random");
		when(consentCreationDataHelper.retrieveFundsConfirmationSetupData(pickupDataModel.getIntentId()))
				.thenReturn(setUpResponseData);
		when(consentCreationDataHelper.retrieveCustomerAccountListInfo(pickupDataModel.getUserId(),
				pickupDataModel.getIntentTypeEnum().getIntentType(), requestHeaders.getCorrelationId(),
				pickupDataModel.getChannelId(), "schemeName", "", "")).thenReturn(getCustomerAccountInfo());
		BasicAttributes tppInformationObj = new BasicAttributes();
		when(tppInformationAdaptor.fetchTPPInformation(pickupDataModel.getClientId())).thenReturn(tppInformationObj);
		when(consentCreationDataHelper.retrieveCustomerAccountListInfo(pickupDataModel.getUserId(),
				pickupDataModel.getIntentTypeEnum().getIntentType(), requestHeaders.getCorrelationId(),
				pickupDataModel.getChannelId(),
				setUpResponseData.getData().getDebtorAccount().getSchemeName().toString(), "", ""))
						.thenReturn(getCustomerAccountInfo());
		cispConsentViewController.consentView(model);
	}
	
	@Test(expected = PSD2Exception.class)
	public void testConsentViewElseNotNull3() throws NamingException {

		requestHeaders.setCorrelationId("correlationId");
		PickupDataModel pickupDataModel = new PickupDataModel();
		pickupDataModel.setIntentTypeEnum(IntentTypeEnum.CISP_INTENT_TYPE);
		pickupDataModel.setIntentId("intentId");
		OBReadAccount6 obReadAccount = new OBReadAccount6();
		obReadAccount.setData(new OBReadAccount6Data());
		when(request.getAttribute("intentData")).thenReturn(pickupDataModel);
		when(dataMask.maskResponseGenerateString(anyObject(), anyString())).thenReturn("123");
		Map<String, Object> model = new HashMap<>();
		OBFundsConfirmationConsentResponse1 setUpResponseData = new OBFundsConfirmationConsentResponse1();
		OBFundsConfirmationConsentResponse1Data data = new OBFundsConfirmationConsentResponse1Data();
		data.setConsentId("consentId");
		setUpResponseData.setData(data);
		OBFundsConfirmationConsent1DataDebtorAccount debtorAccount = new OBFundsConfirmationConsent1DataDebtorAccount();
		debtorAccount.setIdentification("identification");
		debtorAccount.setSchemeName("ABC");
		setUpResponseData.getData().setDebtorAccount(debtorAccount);
		Object tppInfo = new Object();
		when(tppInformationAdaptor.fetchTPPInformation(anyString())).thenReturn(tppInfo);
		when(tppInformationAdaptor.fetchApplicationName(anyString())).thenReturn("string");
		when(uiController.getConfigVariable()).thenReturn("random");
		when(consentCreationDataHelper.retrieveFundsConfirmationSetupData(pickupDataModel.getIntentId()))
				.thenReturn(setUpResponseData);
		when(consentCreationDataHelper.retrieveCustomerAccountListInfo(pickupDataModel.getUserId(),
				pickupDataModel.getIntentTypeEnum().getIntentType(), requestHeaders.getCorrelationId(),
				pickupDataModel.getChannelId(), "schemeName", "", "")).thenReturn(getCustomerAccountInfo());
		BasicAttributes tppInformationObj = new BasicAttributes("123", 12);
		when(tppInformationAdaptor.fetchTPPInformation(pickupDataModel.getClientId())).thenReturn(tppInformationObj);
		when(consentCreationDataHelper.retrieveCustomerAccountListInfo(anyString(),
				anyString(), anyString(),
				anyString(),
				anyString(), anyString(), anyString()))
						.thenReturn(getCustomerAccountInfo());
		cispConsentViewController.consentView(model);
	}
	
	@Test(expected = PSD2Exception.class)
	public void testConsentViewElseNotNull4() throws NamingException {

		requestHeaders.setCorrelationId("correlationId");
		PickupDataModel pickupDataModel = new PickupDataModel();
		pickupDataModel.setIntentTypeEnum(IntentTypeEnum.CISP_INTENT_TYPE);
		pickupDataModel.setIntentId("intentId");
		when(request.getAttribute("intentData")).thenReturn(pickupDataModel);
		when(dataMask.maskResponseGenerateString(anyObject(), anyString())).thenReturn("123");
		Map<String, Object> model = new HashMap<>();
		OBFundsConfirmationConsentResponse1 setUpResponseData = new OBFundsConfirmationConsentResponse1();
		OBFundsConfirmationConsentResponse1Data data = new OBFundsConfirmationConsentResponse1Data();
		data.setConsentId("consentId");
		setUpResponseData.setData(data);
		OBFundsConfirmationConsent1DataDebtorAccount debtorAccount = new OBFundsConfirmationConsent1DataDebtorAccount();
		debtorAccount.setIdentification("identification");
		debtorAccount.setSchemeName("ABC");
		setUpResponseData.getData().setDebtorAccount(debtorAccount);
		Object tppInfo = new Object();
		
		OBReadAccount6 mockOBReadAccount3 = new OBReadAccount6();
		OBReadAccount6Data data1 = new OBReadAccount6Data();
		List<OBAccount6> accountData = new ArrayList<>();
		accountData.add(new OBAccount6());
		data1.setAccount(accountData);
		mockOBReadAccount3.setData(data1);
		
		when(tppInformationAdaptor.fetchTPPInformation(anyString())).thenReturn(tppInfo);
		when(tppInformationAdaptor.fetchApplicationName(anyString())).thenReturn("string");
		when(uiController.getConfigVariable()).thenReturn("random");
		when(consentCreationDataHelper.retrieveFundsConfirmationSetupData(pickupDataModel.getIntentId()))
				.thenReturn(setUpResponseData);
		when(consentCreationDataHelper.retrieveCustomerAccountListInfo(pickupDataModel.getUserId(),
				pickupDataModel.getIntentTypeEnum().getIntentType(), requestHeaders.getCorrelationId(),
				pickupDataModel.getChannelId(), "schemeName", "", "")).thenReturn(getCustomerAccountInfo());
		BasicAttributes tppInformationObj = new BasicAttributes("123", 12);
		when(tppInformationAdaptor.fetchTPPInformation(pickupDataModel.getClientId())).thenReturn(tppInformationObj);
		when(consentCreationDataHelper.retrieveCustomerAccountListInfo(anyString(),
				anyString(), anyString(),
				anyString(),
				anyString(), anyString(), anyString()))
						.thenReturn(mockOBReadAccount3);
		cispConsentViewController.consentView(model);
	}

	@Test(expected = PSD2Exception.class)
	public void testConsentViewElseNotNull2Test() throws NamingException {

		requestHeaders.setCorrelationId("correlationId");
		PickupDataModel pickupDataModel = new PickupDataModel();
		pickupDataModel.setIntentTypeEnum(IntentTypeEnum.CISP_INTENT_TYPE);
		pickupDataModel.setIntentId("intentId");
		when(request.getAttribute("intentData")).thenReturn(pickupDataModel);
		when(dataMask.maskResponseGenerateString(anyObject(), anyString())).thenReturn("123");
		Map<String, Object> model = new HashMap<>();
		OBFundsConfirmationConsentResponse1 setUpResponseData = new OBFundsConfirmationConsentResponse1();
		OBFundsConfirmationConsentResponse1Data data = new OBFundsConfirmationConsentResponse1Data();
		data.setConsentId("consentId");
		setUpResponseData.setData(data);
		OBFundsConfirmationConsent1DataDebtorAccount debtorAccount = new OBFundsConfirmationConsent1DataDebtorAccount();
		debtorAccount.setIdentification("IBAN");
		debtorAccount.setSecondaryIdentification("1234");
		debtorAccount.setSchemeName("ABC");
		setUpResponseData.getData().setDebtorAccount(debtorAccount);
		Object tppInfo = new Object();
		when(tppInformationAdaptor.fetchTPPInformation(anyString())).thenReturn(tppInfo);
		when(tppInformationAdaptor.fetchApplicationName(anyString())).thenReturn("string");
		when(uiController.getConfigVariable()).thenReturn("random");
		when(consentCreationDataHelper.retrieveFundsConfirmationSetupData(pickupDataModel.getIntentId()))
				.thenReturn(setUpResponseData);
		when(consentCreationDataHelper.retrieveCustomerAccountListInfo(pickupDataModel.getUserId(),
				pickupDataModel.getIntentTypeEnum().getIntentType(), requestHeaders.getCorrelationId(),
				pickupDataModel.getChannelId(), "schemeName", "", "")).thenReturn(getCustomerAccountInfo());
		BasicAttributes tppInformationObj = new BasicAttributes();
		when(tppInformationAdaptor.fetchTPPInformation(pickupDataModel.getClientId())).thenReturn(tppInformationObj);
		when(consentCreationDataHelper.retrieveCustomerAccountListInfo(pickupDataModel.getUserId(),
				pickupDataModel.getIntentTypeEnum().getIntentType(), requestHeaders.getCorrelationId(),
				pickupDataModel.getChannelId(),
				setUpResponseData.getData().getDebtorAccount().getSchemeName().toString(), "", ""))
						.thenReturn(getCustomerAccountInfo());
		cispConsentViewController.consentView(model);
	}

	private static OBReadAccount6 getCustomerAccountInfo() {
		OBReadAccount6 mockOBReadAccount3 = new OBReadAccount6();
		List<OBAccount6> accountData = new ArrayList<>();
		PSD2Account acct = new PSD2Account();
		acct.setAccountId("14556236");
		acct.setCurrency("EUR");
		acct.setNickname("John");
		Map<String, String> additionalInformation = new HashMap<>();
		additionalInformation.put("Scheme", "IBAN");
		additionalInformation.put(PSD2Constants.ACCOUNT_NUMBER, "78910111");
		additionalInformation.put(PSD2Constants.ACCOUNT_NSC, "123456");
		additionalInformation.put("identification", "identification");

		acct.setAdditionalInformation(additionalInformation);
		List<OBAccount6Account> accountList = new ArrayList<>();
		OBAccount6Account account = new OBAccount6Account();
		account.setIdentification("identification");
		account.setSchemeName("UK.OBIE.IBAN");
		accountList.add(account);
		acct.setAccount(accountList);

		PSD2Account accnt = new PSD2Account();
		accnt.setAccountId("14556236");
		accnt.setCurrency("EUR");
		accnt.setNickname("John");
		accnt.setAccount(accountList);
		accountData.add(acct);
		accountData.add(accnt);
		OBReadAccount6Data data2 = new OBReadAccount6Data();
		data2.setAccount(accountData);
		account.setSchemeName("UK.OBIE.IBAN");
		Map<String, String> additionalInformation1 = new HashMap<>();
		additionalInformation1.put("Scheme", "IBAN");
		additionalInformation1.put(PSD2Constants.ACCOUNT_NUMBER, "78910111");
		additionalInformation1.put(PSD2Constants.ACCOUNT_NSC, "123456");

		accnt.setAdditionalInformation(additionalInformation1);
		mockOBReadAccount3.setData(data2);

		return mockOBReadAccount3;
	}

	@Test
	public void homePageTest() {

		Map<String, Object> model = new HashMap<>();
		when(request.getRequestURI())
				.thenReturn("https://www.tutorialspoint.com/java/lang/boolean_valueof_string.htm/home");
		when(request.getQueryString()).thenReturn("Dummy Query");
		when(edgeserverhost.getTenantSpecificEdgeserverhost(anyString())).thenReturn("https://www.tutorialspoint.com");
		//ReflectionTestUtils.setField(cispConsentViewController, "edgeserverhost", "edgeserverhost");
		ReflectionTestUtils.setField(cispConsentViewController, "applicationName", "applicationName");
		cispConsentViewController.homePage(model);
	}

}
