package com.capgemini.psd2.security.consent.cisp.rest.test.controller;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.servlet.ModelAndView;

import com.capgemini.psd2.aisp.domain.OBAccount6Account;
import com.capgemini.psd2.cisp.adapter.CispConsentAdapter;
import com.capgemini.psd2.cisp.adapter.FundsConfirmationConsentAdapter;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmationConsent1DataDebtorAccount;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmationConsentResponse1;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmationConsentResponse1Data;
import com.capgemini.psd2.consent.domain.CispConsent;
import com.capgemini.psd2.consent.domain.PSD2Account;
import com.capgemini.psd2.consent.domain.PSD2AccountsAdditionalInfo;
import com.capgemini.psd2.enums.ConsentStatusEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.integration.adapter.TPPInformationAdaptor;
import com.capgemini.psd2.logger.LoggerUtils;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.scaconsenthelper.config.PFConfig;
import com.capgemini.psd2.scaconsenthelper.config.helpers.ConsentAuthorizationHelper;
import com.capgemini.psd2.scaconsenthelper.config.helpers.SCAConsentAdapterHelper;
import com.capgemini.psd2.scaconsenthelper.config.helpers.SCAConsentLoggingHelper;
import com.capgemini.psd2.scaconsenthelper.constants.PFConstants;
import com.capgemini.psd2.scaconsenthelper.models.DropOffResponse;
import com.capgemini.psd2.scaconsenthelper.models.IntentTypeEnum;
import com.capgemini.psd2.scaconsenthelper.models.PickupDataModel;
import com.capgemini.psd2.scaconsenthelper.services.SCAConsentHelperService;
import com.capgemini.psd2.security.consent.cisp.helpers.CispConsentCreationDataHelper;
import com.capgemini.psd2.security.consent.cisp.helpers.CispConsentCreationDataHelperImpl;
import com.capgemini.psd2.security.consent.cisp.rest.controllers.CispConsentAuthorizeController;
import com.capgemini.psd2.security.constants.PSD2SecurityConstants;

public class CispConsentAuthorizeControllerTest {

	@Mock
	private LoggerUtils loggerUtils;
	
	@Mock
	CispConsentCreationDataHelper consentCreationDataHelper;
	
	@Mock
	private SCAConsentHelperService helperService;
	
	@Mock
	CispConsentAdapter cispConsentAdapter;

	@Mock
	private CispConsentCreationDataHelperImpl consentCreationDataHelperImpl;

	@Mock
	ConsentAuthorizationHelper consentAuthorizationHelper;

	@Mock
	RequestHeaderAttributes requestHeaderAttributes;

	@Mock
	PFConfig pfConfig;
	
	@Mock
	TPPInformationAdaptor tppInformationAdaptor;
	
	@Mock
	CispConsentAdapter cispConsentApdater;

	@Mock
	SCAConsentHelperService helpService;

	@Mock
	HttpServletRequest request;

	@Mock
	HttpServletResponse response;
	
	@Mock
	private SCAConsentLoggingHelper scaConsentLoggingHelper;


	@InjectMocks
	private CispConsentAuthorizeController controller;


	@Before
	public void setup() {		
		MockitoAnnotations.initMocks(this);
		doNothing().when(scaConsentLoggingHelper).updateRequestHeaderAttributes(anyObject());
		ReflectionTestUtils.setField(controller, "years", 19);
	}

	@Test
	public void createConsentTest() throws NamingException, NoSuchMethodException, SecurityException{
		
		PickupDataModel pickupDataModel=new PickupDataModel();
		pickupDataModel.setIntentTypeEnum(IntentTypeEnum.CISP_INTENT_TYPE);

		OBFundsConfirmationConsentResponse1 setUpData = new OBFundsConfirmationConsentResponse1();
		OBFundsConfirmationConsentResponse1Data data = new OBFundsConfirmationConsentResponse1Data();
		data.setConsentId("consentId");
		
		setUpData.setData(data);
		OBFundsConfirmationConsent1DataDebtorAccount debtorAccount = new OBFundsConfirmationConsent1DataDebtorAccount();
		setUpData.getData().setDebtorAccount(debtorAccount );
		setUpData.getData().getDebtorAccount().setIdentification("identification");
		setUpData.getData().getDebtorAccount().setSchemeName("abc" );
		when(pfConfig.getCispinstanceId()).thenReturn("CispinstanceId");
		when(pfConfig.getCispinstanceusername()).thenReturn("username");
		when(pfConfig.getAispinstancepassword()).thenReturn("password");
		DropOffResponse dropOffResponse = new DropOffResponse();
		PSD2AccountsAdditionalInfo accountAdditionalInfo = new PSD2AccountsAdditionalInfo();
		when(request.getAttribute("intentData")).thenReturn(pickupDataModel);
		List<PSD2Account> accountdetails = new ArrayList<>();
		PSD2Account psd2account = new PSD2Account();
		psd2account.accountId("1235");
		List<OBAccount6Account> accountList= new ArrayList<>();
		OBAccount6Account mockData2Account = new OBAccount6Account();
		mockData2Account.setName("account");
		accountList.add(mockData2Account);
		psd2account.account(accountList);
		psd2account.setHashedValue("fd3dfa76050e048e229d35a01da6974ab0edd353520838235d499163d97df209");
		accountdetails.add(psd2account);
		accountAdditionalInfo.setAccountdetails(accountdetails);
		ModelAndView model = new ModelAndView();
		String resumePath = "resumePath";
		when(request.getAttribute(PSD2Constants.NETSCALER_TENANT_ID_HEADER_NAME)).thenReturn("tenant2");
		when(pfConfig.getTenantSpecificResumePathBaseUrl(anyString())).thenReturn("resumePath");
		when(pfConfig.getTenantSpecificResumePathBaseUrl(anyString()).concat(resumePath)).thenReturn("https://www.tutorialspoint.com/java/lang/boolean_valueof_string.htm");
		when(consentCreationDataHelper.retrieveFundsConfirmationSetupData(anyString())).thenReturn(setUpData);
		when(consentAuthorizationHelper.matchAccountFromList(any(), any())).thenReturn(psd2account);
		when(helperService.dropOffOnConsentSubmission(any(),any(),any(),any())).thenReturn(dropOffResponse);
		
		when(requestHeaderAttributes.getCorrelationId()).thenReturn("correlationId");
		
		controller.createConsent(model , accountAdditionalInfo, "resumePath", "refreshTokenRenewalFlow");

	}
	
	@Test
	public void createConsent1Test() throws NamingException, NoSuchMethodException, SecurityException{
		CispConsent cispConsent = new CispConsent();
		cispConsent.setChannelId("channelId");
		PickupDataModel pickupDataModel=new PickupDataModel();
		pickupDataModel.setIntentTypeEnum(IntentTypeEnum.CISP_INTENT_TYPE);
		OBFundsConfirmationConsentResponse1 setUpData = new OBFundsConfirmationConsentResponse1();
		OBFundsConfirmationConsentResponse1Data data = new OBFundsConfirmationConsentResponse1Data();
		data .setConsentId("consentId");
		setUpData.setData(data);
		OBFundsConfirmationConsent1DataDebtorAccount debtorAccount = new OBFundsConfirmationConsent1DataDebtorAccount();
		setUpData.getData().setDebtorAccount(debtorAccount );
		setUpData.getData().getDebtorAccount().setIdentification("identification");
		when(pfConfig.getCispinstanceId()).thenReturn("CispinstanceId");
		when(pfConfig.getCispinstanceusername()).thenReturn("username");
		when(pfConfig.getAispinstancepassword()).thenReturn("password");
		DropOffResponse dropOffResponse = new DropOffResponse();
		PSD2AccountsAdditionalInfo accountAdditionalInfo = new PSD2AccountsAdditionalInfo();
		when(request.getAttribute("intentData")).thenReturn(pickupDataModel);
		List<PSD2Account> accountdetails = new ArrayList<>();
		PSD2Account psd2account = new PSD2Account();
		psd2account.accountId("1235");

		List<OBAccount6Account> accountList= new ArrayList<>();
		OBAccount6Account mockData2Account = new OBAccount6Account();
		mockData2Account.setName("account");
		accountList.add(mockData2Account);
		psd2account.account(accountList);
		
		psd2account.setHashedValue("fd3dfa76050e048e229d35a01da6974ab0edd353520838235d499163d97df209");
		
		accountdetails.add(psd2account);
		accountAdditionalInfo.setAccountdetails(accountdetails);
		ModelAndView model = new ModelAndView();
		String resumePath = "resumePath";
		when(request.getAttribute(PSD2Constants.NETSCALER_TENANT_ID_HEADER_NAME)).thenReturn("tenant2");
		when(pfConfig.getTenantSpecificResumePathBaseUrl(anyString())).thenReturn("resumePath");
		when(pfConfig.getTenantSpecificResumePathBaseUrl(anyString()).concat(resumePath)).thenReturn("https://www.tutorialspoint.com/java/lang/boolean_valueof_string.htm");
		when(consentCreationDataHelper.retrieveFundsConfirmationSetupData(anyString())).thenReturn(setUpData);
		when(consentAuthorizationHelper.matchAccountFromList(any(), any())).thenReturn(psd2account);
		when(helperService.dropOffOnConsentSubmission(any(),any(),any(),any())).thenReturn(dropOffResponse);
		
		when(cispConsentApdater.retrieveConsentByFundsIntentIdAndStatus(pickupDataModel.getIntentId(),
					ConsentStatusEnum.AUTHORISED)).thenReturn(cispConsent);
		controller.createConsent(model , accountAdditionalInfo, "resumePath", "true");

	}
	
	@Test
	public void createConsent4Test() throws NamingException, NoSuchMethodException, SecurityException{
		CispConsent cispConsent = new CispConsent();
		cispConsent.setChannelId("channelId");
		PickupDataModel pickupDataModel=new PickupDataModel();
		pickupDataModel.setIntentTypeEnum(IntentTypeEnum.CISP_INTENT_TYPE);
		OBFundsConfirmationConsentResponse1 setUpData = new OBFundsConfirmationConsentResponse1();
		OBFundsConfirmationConsentResponse1Data data = new OBFundsConfirmationConsentResponse1Data();
		data .setConsentId("consentId");
		setUpData.setData(data);
		OBFundsConfirmationConsent1DataDebtorAccount debtorAccount = new OBFundsConfirmationConsent1DataDebtorAccount();
		setUpData.getData().setDebtorAccount(debtorAccount );
		setUpData.getData().setExpirationDateTime("2020-01-19T00:00:00+05:30");
		setUpData.getData().getDebtorAccount().setIdentification("identification");
		when(pfConfig.getCispinstanceId()).thenReturn("CispinstanceId");
		when(pfConfig.getCispinstanceusername()).thenReturn("username");
		when(pfConfig.getAispinstancepassword()).thenReturn("password");
		DropOffResponse dropOffResponse = new DropOffResponse();
		PSD2AccountsAdditionalInfo accountAdditionalInfo = new PSD2AccountsAdditionalInfo();
		when(request.getAttribute("intentData")).thenReturn(pickupDataModel);
		List<PSD2Account> accountdetails = new ArrayList<>();
		PSD2Account psd2account = new PSD2Account();
		psd2account.accountId("1235");

		List<OBAccount6Account> accountList= new ArrayList<>();
		OBAccount6Account mockData2Account = new OBAccount6Account();
		mockData2Account.setName("account");
		accountList.add(mockData2Account);
		psd2account.account(accountList);
		
		psd2account.setHashedValue("fd3dfa76050e048e229d35a01da6974ab0edd353520838235d499163d97df209");
		
		accountdetails.add(psd2account);
		accountAdditionalInfo.setAccountdetails(accountdetails);
		ModelAndView model = new ModelAndView();
		String resumePath = "resumePath";
		when(request.getAttribute(PSD2Constants.NETSCALER_TENANT_ID_HEADER_NAME)).thenReturn("tenant2");
		when(pfConfig.getTenantSpecificResumePathBaseUrl(anyString())).thenReturn("resumePath");
		when(pfConfig.getTenantSpecificResumePathBaseUrl(anyString()).concat(resumePath)).thenReturn("https://www.tutorialspoint.com/java/lang/boolean_valueof_string.htm");
		when(consentCreationDataHelper.retrieveFundsConfirmationSetupData(anyString())).thenReturn(setUpData);
		when(consentAuthorizationHelper.matchAccountFromList(any(), any())).thenReturn(psd2account);
		when(helperService.dropOffOnConsentSubmission(any(),any(),any(),any())).thenReturn(dropOffResponse);
		
		when(cispConsentApdater.retrieveConsentByFundsIntentIdAndStatus(pickupDataModel.getIntentId(),
					ConsentStatusEnum.AUTHORISED)).thenReturn(null);
		controller.createConsent(model , accountAdditionalInfo, "resumePath", "false");

	}
	
	@Test(expected = PSD2Exception.class)
	public void createConsent2Test() throws NamingException, NoSuchMethodException, SecurityException{
		
		PickupDataModel pickupDataModel=new PickupDataModel();
		pickupDataModel.setIntentTypeEnum(IntentTypeEnum.CISP_INTENT_TYPE);
		OBFundsConfirmationConsentResponse1 setUpData = new OBFundsConfirmationConsentResponse1();
		OBFundsConfirmationConsentResponse1Data data = new OBFundsConfirmationConsentResponse1Data();
		data .setConsentId("consentId");
		setUpData.setData(data);
		OBFundsConfirmationConsent1DataDebtorAccount debtorAccount = new OBFundsConfirmationConsent1DataDebtorAccount();
		setUpData.getData().setDebtorAccount(debtorAccount );
		setUpData.getData().getDebtorAccount().setIdentification("identification");
		when(pfConfig.getCispinstanceId()).thenReturn("CispinstanceId");
		when(pfConfig.getCispinstanceusername()).thenReturn("username");
		when(pfConfig.getAispinstancepassword()).thenReturn("password");
		DropOffResponse dropOffResponse = new DropOffResponse();
		PSD2AccountsAdditionalInfo accountAdditionalInfo = new PSD2AccountsAdditionalInfo();
		when(request.getAttribute("intentData")).thenReturn(pickupDataModel);
		List<PSD2Account> accountdetails = new ArrayList<>();
		PSD2Account psd2account = new PSD2Account();
		psd2account.accountId("1235");
		List<OBAccount6Account> accountList= new ArrayList<>();
		OBAccount6Account mockData2Account = new OBAccount6Account();
		mockData2Account.setName("account");
		accountList.add(mockData2Account);	
		psd2account.setHashedValue("fd3dfa76050e048e229d35a01da6974ab0edd353520838235d499163d97df209");
		psd2account.setAccount(accountList);
		accountdetails.add(psd2account);
		accountAdditionalInfo.setAccountdetails(accountdetails);
		ModelAndView model = new ModelAndView();
		String resumePath = "resumePath";
		when(request.getAttribute(PSD2Constants.NETSCALER_TENANT_ID_HEADER_NAME)).thenReturn("tenant2");
		when(pfConfig.getTenantSpecificResumePathBaseUrl(anyString())).thenReturn("resumePath");
		when(pfConfig.getTenantSpecificResumePathBaseUrl(anyString()).concat(resumePath)).thenReturn("https://www.tutorialspoint.com/java/lang/boolean_valueof_string.htm");
		when(consentCreationDataHelper.retrieveFundsConfirmationSetupData(anyString())).thenReturn(setUpData);
		when(consentAuthorizationHelper.matchAccountFromList(any(), any())).thenReturn(psd2account);
		when(helperService.dropOffOnConsentSubmission(any(),any(),any(),any())).thenReturn(dropOffResponse);
		
		when(cispConsentApdater.retrieveConsentByFundsIntentIdAndStatus(pickupDataModel.getIntentId(),
					ConsentStatusEnum.AUTHORISED)).thenReturn(null);
		controller.createConsent(model , accountAdditionalInfo, "resumePath", "true");

	}
	
	@Test(expected= PSD2Exception.class)
	public void createConsentTest1() throws NamingException, NoSuchMethodException, SecurityException{
		PickupDataModel pickupDataModel=new PickupDataModel();
		pickupDataModel.setIntentTypeEnum(IntentTypeEnum.CISP_INTENT_TYPE);
		OBFundsConfirmationConsentResponse1 setUpData = new OBFundsConfirmationConsentResponse1();
		OBFundsConfirmationConsentResponse1Data data = new OBFundsConfirmationConsentResponse1Data();
		data .setConsentId("consentId");
		setUpData.setData(data);
		OBFundsConfirmationConsent1DataDebtorAccount debtorAccount = new OBFundsConfirmationConsent1DataDebtorAccount();
		setUpData.getData().setDebtorAccount(debtorAccount );
		setUpData.getData().getDebtorAccount().setIdentification("identification");
		when(pfConfig.getCispinstanceId()).thenReturn("CispinstanceId");
		when(pfConfig.getCispinstanceusername()).thenReturn("username");
		when(pfConfig.getAispinstancepassword()).thenReturn("password");
		DropOffResponse dropOffResponse = new DropOffResponse();
		when(request.getAttribute("intentData")).thenReturn(pickupDataModel);
		List<OBAccount6Account> accountList= new ArrayList<>();
		OBAccount6Account mockData2Account = new OBAccount6Account();
		mockData2Account.setName("account");
		accountList.add(mockData2Account);
		ModelAndView model = new ModelAndView();
		String resumePath = "resumePath";
		when(request.getAttribute(PSD2Constants.NETSCALER_TENANT_ID_HEADER_NAME)).thenReturn("tenant2");
		when(pfConfig.getTenantSpecificResumePathBaseUrl(anyString())).thenReturn("resumePath");
		when(pfConfig.getTenantSpecificResumePathBaseUrl(anyString()).concat(resumePath)).thenReturn("https://www.tutorialspoint.com/java/lang/boolean_valueof_string.htm");
		when(consentCreationDataHelper.retrieveFundsConfirmationSetupData(anyString())).thenReturn(setUpData);
		when(helperService.dropOffOnConsentSubmission(any(),any(),any(),any())).thenReturn(dropOffResponse);
		controller.createConsent(model , null, "resumePath", "refreshTokenRenewalFlow");

	}
	
	@Test
	public void createConsentTest2() throws NamingException, NoSuchMethodException, SecurityException{
		PickupDataModel pickupDataModel=new PickupDataModel();
		pickupDataModel.setChannelId("channelId");
		
		pickupDataModel.setIntentTypeEnum(IntentTypeEnum.CISP_INTENT_TYPE);
		OBFundsConfirmationConsentResponse1 setUpData = new OBFundsConfirmationConsentResponse1();
		OBFundsConfirmationConsentResponse1Data data = new OBFundsConfirmationConsentResponse1Data();
		data .setConsentId("consentId");
		setUpData.setData(data);
		OBFundsConfirmationConsent1DataDebtorAccount debtorAccount = new OBFundsConfirmationConsent1DataDebtorAccount();
		setUpData.getData().setDebtorAccount(debtorAccount );
		setUpData.getData().getDebtorAccount().setIdentification("identification");
		setUpData.getData().getDebtorAccount().setSchemeName("abc");
		when(pfConfig.getCispinstanceId()).thenReturn("CispinstanceId");
		when(pfConfig.getCispinstanceusername()).thenReturn("username");
		when(pfConfig.getAispinstancepassword()).thenReturn("password");
		DropOffResponse dropOffResponse = new DropOffResponse();
		PSD2AccountsAdditionalInfo accountAdditionalInfo = new PSD2AccountsAdditionalInfo();
		when(request.getAttribute("intentData")).thenReturn(pickupDataModel);
		List<PSD2Account> accountdetails = new ArrayList<>();
		PSD2Account psd2account = new PSD2Account();
		psd2account.accountId("1235");
		List<OBAccount6Account> accountList= new ArrayList<>();
		OBAccount6Account mockData2Account = new OBAccount6Account();
		mockData2Account.setName("account");
		accountList.add(mockData2Account);
		psd2account.setHashedValue("fd3dfa76050e048e229d35a01da6974ab0edd353520838235d499163d97df209");
		psd2account.setAccount(accountList);
		accountdetails.add(psd2account);
		accountAdditionalInfo.setAccountdetails(accountdetails);
		ModelAndView model = new ModelAndView();
		String resumePath = "resumePath";
		when(request.getAttribute(PSD2Constants.NETSCALER_TENANT_ID_HEADER_NAME)).thenReturn("tenant2");
		when(pfConfig.getTenantSpecificResumePathBaseUrl(anyString())).thenReturn("resumePath");
		when(pfConfig.getTenantSpecificResumePathBaseUrl(anyString()).concat(resumePath)).thenReturn("https://www.tutorialspoint.com/java/lang/boolean_valueof_string.htm");
		when(consentCreationDataHelper.retrieveFundsConfirmationSetupData(anyString())).thenReturn(setUpData);
		when(consentAuthorizationHelper.matchAccountFromList(any(), any())).thenReturn(psd2account);
		when(helperService.dropOffOnConsentSubmission(any(),any(),any(),any())).thenReturn(dropOffResponse);
		controller.createConsent(model , accountAdditionalInfo, "resumePath", null);

	}
	
	@Test(expected= PSD2Exception.class)
	public void createConsentExceptionTest() throws NamingException, NoSuchMethodException, SecurityException{
		PickupDataModel pickupDataModel=new PickupDataModel();
		pickupDataModel.setIntentTypeEnum(IntentTypeEnum.CISP_INTENT_TYPE);
		OBFundsConfirmationConsentResponse1 setUpData = new OBFundsConfirmationConsentResponse1();
		OBFundsConfirmationConsentResponse1Data data = new OBFundsConfirmationConsentResponse1Data();
		data .setConsentId("consentId");
		setUpData.setData(data);
		OBFundsConfirmationConsent1DataDebtorAccount debtorAccount = new OBFundsConfirmationConsent1DataDebtorAccount();
		setUpData.getData().setDebtorAccount(debtorAccount );
		setUpData.getData().getDebtorAccount().setIdentification("identification");
		when(pfConfig.getCispinstanceId()).thenReturn("CispinstanceId");
		when(pfConfig.getCispinstanceusername()).thenReturn("username");
		when(pfConfig.getAispinstancepassword()).thenReturn("password");
		DropOffResponse dropOffResponse = new DropOffResponse();
		PSD2AccountsAdditionalInfo accountAdditionalInfo = new PSD2AccountsAdditionalInfo();
		when(request.getAttribute("intentData")).thenReturn(pickupDataModel);
		List<PSD2Account> accountdetails = new ArrayList<>();
		PSD2Account psd2account = new PSD2Account();
		psd2account.accountId("1235");
		List<OBAccount6Account> accountList= new ArrayList<>();
		OBAccount6Account mockData2Account = new OBAccount6Account();
		mockData2Account.setName("account");
		accountList.add(mockData2Account);
		psd2account.setHashedValue("fd3dfa76050e048e229d35a01da6974ab0edd353520838235d499163d97df209");
		psd2account.setAccount(accountList);
		accountdetails.add(psd2account);
		accountAdditionalInfo.setAccountdetails(accountdetails);
		ModelAndView model = new ModelAndView();
		String resumePath = "resumePath";
		when(request.getAttribute(PSD2Constants.NETSCALER_TENANT_ID_HEADER_NAME)).thenReturn("tenant2");
		when(pfConfig.getTenantSpecificResumePathBaseUrl(anyString())).thenReturn("resumePath");
		when(pfConfig.getTenantSpecificResumePathBaseUrl(anyString()).concat(resumePath)).thenReturn("https://www.tutorialspoint.com/java/lang/boolean_valueof_string.htm");
		when(consentCreationDataHelper.retrieveFundsConfirmationSetupData(anyString())).thenReturn(setUpData);
		when(consentAuthorizationHelper.matchAccountFromList(any(), any())).thenReturn(psd2account);
		when(helperService.dropOffOnConsentSubmission(any(),any(),any(),any())).thenReturn(dropOffResponse);
		controller.createConsent(model , accountAdditionalInfo, "resumePath", "true");

	}

	@Test
	public void cancelConsentTest(){
		PickupDataModel pickupDataModel=new PickupDataModel();
		pickupDataModel.setIntentTypeEnum(IntentTypeEnum.CISP_INTENT_TYPE);
		pickupDataModel.setIntentId("IntendId");
		when(request.getAttribute("intentData")).thenReturn(pickupDataModel);
		Map<String, String> paramsMap = new HashMap<>();
		paramsMap.put(PFConstants.RESUME_PATH,"resumePath");
		String resumePath = "resumePath";
		when(requestHeaderAttributes.getTenantId()).thenReturn("tenant1");
		when(pfConfig.getTenantSpecificResumePathBaseUrl("tenant1")).thenReturn("resumePath");
		when(pfConfig.getTenantSpecificResumePathBaseUrl("tenant1").concat(resumePath)).thenReturn("https://www.tutorialspoint.com/java/lang/boolean_valueof_string.htm");
		ModelAndView model = new ModelAndView();
		controller.cancelConsent(model , paramsMap);
	}
	
	@Test
	public void cancelConsent1Test(){
		PickupDataModel pickupDataModel=new PickupDataModel();
		pickupDataModel.setIntentTypeEnum(IntentTypeEnum.CISP_INTENT_TYPE);
		pickupDataModel.setIntentId("IntendId");
		when(request.getAttribute("intentData")).thenReturn(pickupDataModel);
		Map<String, String> paramsMap = new HashMap<>();
		paramsMap.put(PFConstants.RESUME_PATH,"https://www.tutorialspoint.com/java/");
		paramsMap.put(PSD2Constants.SERVER_ERROR_FLAG_ATTR,"true");
		String resumePath = "resumePath";
		when(request.getAttribute(PSD2Constants.NETSCALER_TENANT_ID_HEADER_NAME)).thenReturn("tenant2");
		when(pfConfig.getTenantSpecificResumePathBaseUrl(anyString())).thenReturn("resumePath");
		when(pfConfig.getTenantSpecificResumePathBaseUrl(anyString()).concat(resumePath)).thenReturn("https://www.tutorialspoint.com/java/lang/boolean_valueof_string.htm");		ModelAndView model = new ModelAndView();
		controller.cancelConsent(model , paramsMap);
	}
	
	@Test(expected= PSD2Exception.class)
	public void cancelConsentExceptionTest(){
		PickupDataModel pickupDataModel=new PickupDataModel();
		pickupDataModel.setIntentTypeEnum(IntentTypeEnum.CISP_INTENT_TYPE);
		pickupDataModel.setIntentId("IntendId");
		when(request.getAttribute("intentData")).thenReturn(pickupDataModel);
		Map<String, String> paramsMap = new HashMap<>();
		paramsMap.put(PFConstants.RESUME_PATH,"resumePath");
		String resumePath = "resumePath";
		when(request.getAttribute(PSD2Constants.NETSCALER_TENANT_ID_HEADER_NAME)).thenReturn("tenant2");
		when(pfConfig.getTenantSpecificResumePathBaseUrl(anyString())).thenReturn("resumePath");
		when(pfConfig.getTenantSpecificResumePathBaseUrl(anyString()).concat(resumePath)).thenReturn("https://www.tutorialspoint.com/java/lang/boolean_valueof_string.htm");
		ModelAndView model = new ModelAndView();
		paramsMap.put(PSD2SecurityConstants.REFRESH_TOKEN_RENEWAL_FLOW,"true");
		controller.cancelConsent(model , paramsMap);
	}
	
	@Test
	public void cancelConsentException1Test(){
		
		PickupDataModel pickupDataModel=new PickupDataModel();
		pickupDataModel.setIntentTypeEnum(IntentTypeEnum.CISP_INTENT_TYPE);
		pickupDataModel.setIntentId("IntendId");
		when(request.getAttribute("intentData")).thenReturn(pickupDataModel);
		Map<String, String> paramsMap = new HashMap<>();
		paramsMap.put(PFConstants.RESUME_PATH,"resumePath");
		String resumePath = "resumePath";
		when(request.getAttribute(PSD2Constants.NETSCALER_TENANT_ID_HEADER_NAME)).thenReturn("tenant2");
		when(pfConfig.getTenantSpecificResumePathBaseUrl(anyString())).thenReturn("resumePath");
		when(pfConfig.getTenantSpecificResumePathBaseUrl(anyString()).concat(resumePath)).thenReturn("https://www.tutorialspoint.com/java/lang/boolean_valueof_string.htm");		ModelAndView model = new ModelAndView();
		paramsMap.put(PSD2SecurityConstants.REFRESH_TOKEN_RENEWAL_FLOW,"false");
		
		controller.cancelConsent(model , paramsMap);
	}
	
	@Test(expected= PSD2Exception.class)
	public void cancelConsentException2Test(){
		
		PickupDataModel pickupDataModel=new PickupDataModel();
		pickupDataModel.setIntentTypeEnum(IntentTypeEnum.CISP_INTENT_TYPE);
		pickupDataModel.setIntentId("IntendId");
		when(request.getAttribute("intentData")).thenReturn(pickupDataModel);
		Map<String, String> paramsMap = new HashMap<>();
		paramsMap.put(PFConstants.RESUME_PATH,"resumePath");
		String resumePath = "resumePath";
		when(request.getAttribute(PSD2Constants.NETSCALER_TENANT_ID_HEADER_NAME)).thenReturn("tenant2");
		when(pfConfig.getTenantSpecificResumePathBaseUrl(anyString())).thenReturn("resumePath");
		when(pfConfig.getTenantSpecificResumePathBaseUrl(anyString()).concat(resumePath)).thenReturn("https://www.tutorialspoint.com/java/lang/boolean_valueof_string.htm");
		ModelAndView model = new ModelAndView();
		paramsMap.put(PSD2SecurityConstants.REFRESH_TOKEN_RENEWAL_FLOW,"true");
		
		when(cispConsentApdater.retrieveConsentByFundsIntentIdAndStatus(pickupDataModel.getIntentId(),
				ConsentStatusEnum.AUTHORISED)).thenReturn(null);
		controller.cancelConsent(model , paramsMap);
	}
	@Test
	public void cancelConsentExceptionTest1(){
		PickupDataModel pickupDataModel=new PickupDataModel();
		pickupDataModel.setIntentTypeEnum(IntentTypeEnum.CISP_INTENT_TYPE);
		pickupDataModel.setIntentId("IntendId");
		when(request.getAttribute("intentData")).thenReturn(pickupDataModel);
		Map<String, String> paramsMap = new HashMap<>();
		paramsMap.put(PFConstants.RESUME_PATH,"resumePath");
		String resumePath = "resumePath";
		when(request.getAttribute(PSD2Constants.NETSCALER_TENANT_ID_HEADER_NAME)).thenReturn("tenant2");
		when(pfConfig.getTenantSpecificResumePathBaseUrl(anyString())).thenReturn("resumePath");
		when(pfConfig.getTenantSpecificResumePathBaseUrl(anyString()).concat(resumePath)).thenReturn("https://www.tutorialspoint.com/java/lang/boolean_valueof_string.htm");		ModelAndView model = new ModelAndView();
		paramsMap.put(PSD2SecurityConstants.REFRESH_TOKEN_RENEWAL_FLOW,null);
		controller.cancelConsent(model , paramsMap);
	}
	

}
