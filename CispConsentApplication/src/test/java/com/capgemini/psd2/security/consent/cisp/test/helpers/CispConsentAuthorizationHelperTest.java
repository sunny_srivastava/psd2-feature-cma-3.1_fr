package com.capgemini.psd2.security.consent.cisp.test.helpers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;

import com.capgemini.psd2.aisp.domain.OBAccount6Account;
import com.capgemini.psd2.aisp.domain.OBBranchAndFinancialInstitutionIdentification50;
import com.capgemini.psd2.aisp.domain.OBExternalAccountSubType1Code;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.CispConsent;
import com.capgemini.psd2.consent.domain.PSD2Account;
import com.capgemini.psd2.security.consent.cisp.helpers.CispConsentAuthorizationHelper;

public class CispConsentAuthorizationHelperTest {

	@InjectMocks
	private CispConsentAuthorizationHelper helper;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void populateAccountWithUnmaskedValuesTest() {
		Map<String, String> map = new HashMap<>();

		PSD2Account accountwithoutmask = new PSD2Account();
		List<OBAccount6Account> acct = new ArrayList<>();
		OBAccount6Account account = new OBAccount6Account();
		account.setIdentification("12345");
		account.setSchemeName("IBAN");
		acct.add(account);
		OBBranchAndFinancialInstitutionIdentification50 servicer = new OBBranchAndFinancialInstitutionIdentification50();
		servicer.setIdentification("12345");
		accountwithoutmask.setServicer(servicer);
		accountwithoutmask.setAccount(acct);
		accountwithoutmask.setCurrency("EUR");
		accountwithoutmask.setNickname("John");
		accountwithoutmask.setAdditionalInformation(map);

		PSD2Account accountwithmask = new PSD2Account();
		OBAccount6Account account1 = new OBAccount6Account();
		account1.setIdentification("12345");
		account1.setSchemeName("IBAN");
		OBBranchAndFinancialInstitutionIdentification50 servicer1 = new OBBranchAndFinancialInstitutionIdentification50();
		servicer1.setIdentification("12345");
		accountwithmask.setServicer(servicer);
		accountwithmask.setAccount(acct);
		accountwithmask.setCurrency("EUR");
		accountwithmask.setNickname("John");
		accountwithmask.setAdditionalInformation(map);

		helper.populateAccountwithUnmaskedValues(accountwithmask, accountwithoutmask);
	}

	@Test
	public void testGetConsentSupportedSchemeMap() {
		helper.getConsentSupportedSchemeMap();
	}

	@Test
	public void testPopulateAccountListFromAccountDetails() {
		CispConsent cispConsent = new CispConsent();
		List<AccountDetails> accountDetails = new ArrayList<>();
		AccountDetails adetails = new AccountDetails();
		adetails.setAccount(new OBAccount6Account());
		adetails.getAccount().setIdentification("BOI");
		adetails.setServicer(new OBBranchAndFinancialInstitutionIdentification50());
		adetails.getServicer().setIdentification("served");
		accountDetails.add(adetails);
		cispConsent.setAccountDetails(adetails);
		PSD2Account account = new PSD2Account();
		account.setCurrency("EUR");
		account.setNickname("acc");
		account.setAccountSubType(OBExternalAccountSubType1Code.CHARGECARD);
		account.setAccountId("456");
		List<OBAccount6Account> cashAccount = new ArrayList<>();;
		account.setAccount(cashAccount);
		account.setServicer(new OBBranchAndFinancialInstitutionIdentification50());

		helper.populateAccountListFromAccountDetails(cispConsent);
	}
}
