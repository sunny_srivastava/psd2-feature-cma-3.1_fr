package com.capgemini.psd2.security.consent.cisp.test.helpers;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyList;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.naming.NamingException;
import javax.naming.directory.BasicAttributes;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.capgemini.psd2.aisp.adapter.CustomerAccountListAdapter;
import com.capgemini.psd2.aisp.domain.OBAccount6;
import com.capgemini.psd2.aisp.domain.OBAccount6Account;
import com.capgemini.psd2.aisp.domain.OBBranchAndFinancialInstitutionIdentification50;
import com.capgemini.psd2.cisp.adapter.CispConsentAdapter;
import com.capgemini.psd2.cisp.consent.adapter.repository.CispConsentMongoRepository;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmationConsentResponse1;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmationConsentResponse1Data;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.CispConsent;
import com.capgemini.psd2.consent.domain.PSD2Account;
import com.capgemini.psd2.enums.ConsentStatusEnum;
import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.funds.confirmation.consent.mongo.db.adapter.repository.FundsConfirmationConsentRepository;
import com.capgemini.psd2.logger.LoggerUtils;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.product.common.CompatibleVersionList;
import com.capgemini.psd2.scaconsenthelper.config.helpers.ConsentAuthorizationHelper;
import com.capgemini.psd2.scaconsenthelper.config.helpers.SCAConsentAdapterHelper;
import com.capgemini.psd2.security.consent.cisp.helpers.CispConsentCreationDataHelperImpl;

public class CispConsentCreationDataHelperImplTest {

	@Mock
	private LoggerUtils loggerUtils;
	
	@Mock
	CustomerAccountListAdapter customerAccountListAdapter;

	@Mock
	CispConsentAdapter cispConsentAdapter;

	@Mock
	CispConsent cispConsent;

	@Mock
	private CompatibleVersionList compatibleVersionList;
	
	@Mock
	ConsentAuthorizationHelper consentAuthorizationHelper;

	@Mock
	OBFundsConfirmationConsentResponse1 fundsConfirmationConsentPOSTResponse;

	@Mock
	SCAConsentAdapterHelper scaConsentAdapterHelper;

	@Mock
	CispConsentMongoRepository cispConsentMongoRepository;

	@Mock
	private FundsConfirmationConsentRepository fundsConfirmationConsentRepository;

	@InjectMocks
	private CispConsentCreationDataHelperImpl helper;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);

	}

	@Test(expected = PSD2Exception.class)
	public void retrieveFundsConfirmationSetupDataExceptionTest() {
		String fundsIntentId = "fundsIntentId";
		when(scaConsentAdapterHelper.getFundsConfirmationSetupData("fundsIntentId"))
		.thenThrow(PSD2Exception.populatePSD2Exception(ErrorCodeEnum.CISP_NO_FUNDS_CONFIRMATION_CONSENT_DATA_FOUND));
		helper.retrieveFundsConfirmationSetupData(fundsIntentId);
	}

	@Test
	public void retrieveFundsConfirmationSetupDataTest() {
		when(scaConsentAdapterHelper.getFundsConfirmationSetupData("fundsIntentId"))
				.thenReturn(fundsConfirmationConsentPOSTResponse);
		helper.retrieveFundsConfirmationSetupData("fundsIntentId");
	}

	@Test
	public void retrieveCustomerAccountListInfoTest() {
		String userId = "rakshasaxena@capgemini.com";
		String flowType = "AISP";
		String correlationId = "123456788";
		String channelId = "1234";
		String tenantId = "12345";
		String intentId = "12345";
		CispConsent cispValue = new CispConsent();
		OBFundsConfirmationConsentResponse1Data responseData = new OBFundsConfirmationConsentResponse1Data();
		responseData.setCmaVersion("1.0");
		List<String> versionList = new ArrayList<>();
		versionList.add("2.0");
		when(compatibleVersionList.fetchVersionList()).thenReturn(versionList);
		Mockito.when(cispConsentAdapter.retrieveConsentByFundsIntentId(intentId)).thenReturn(cispValue);
		Mockito.when(fundsConfirmationConsentRepository.findByConsentIdAndCmaVersionIn(anyString(), anyList()))
				.thenReturn(responseData);
		helper.retrieveCustomerAccountListInfo(userId, flowType, correlationId, channelId, "SchemeNameEnum.IBAN",
				tenantId, intentId );
	}

	@Test(expected = PSD2Exception.class)
	public void retrieveCustomerAccountListInfoTest_Exception() {
		String userId = "rakshasaxena@capgemini.com";
		String flowType = "AISP";
		String correlationId = "123456788";
		String channelId = "1234";
		String tenantId = "12345";
		String intentId = "12345";
		CispConsent cispValue = new CispConsent();
		List<String> versionList = new ArrayList<>();
		versionList.add("2.0");
		when(compatibleVersionList.fetchVersionList()).thenReturn(versionList);
		Mockito.when(cispConsentAdapter.retrieveConsentByFundsIntentId(intentId)).thenReturn(cispValue);
		Mockito.when(fundsConfirmationConsentRepository.findByConsentIdAndCmaVersionIn(anyString(), anyList()))
				.thenReturn(null);
		helper.retrieveCustomerAccountListInfo(userId, flowType, correlationId, channelId, "SchemeNameEnum.IBAN",
				tenantId, intentId);
	}

	@Test
	public void findExistingConsentAccountsNullConsentIdTest() {
		String intentId = "1234";
		List<OBAccount6> consentAccounts = new ArrayList<>();
		when(cispConsentAdapter.retrieveConsentByFundsIntentIdAndStatus(intentId, ConsentStatusEnum.AUTHORISED))
				.thenReturn(null);
		helper.findExistingConsentAccounts(intentId);
	}

	@Test
	public void cancelFundsConfirmationSetupTest() {
		Map<String, String> paramsMap = new HashMap<>();
		when(cispConsentAdapter.retrieveConsentByFundsIntentIdAndStatus("intentId",
				ConsentStatusEnum.AWAITINGAUTHORISATION)).thenReturn(cispConsent);
		List<String> versionList = new ArrayList<>();
		versionList.add("2.0");
		when(compatibleVersionList.fetchVersionList()).thenReturn(versionList);
		when(cispConsentMongoRepository.findByConsentIdAndCmaVersionIn(anyString(),anyList())).thenReturn(cispConsent);
		helper.cancelFundsConfirmationSetup("intendId", paramsMap);
	}

	@Test(expected=PSD2Exception.class)
	public void cancelFundsConfirmationSetupTestWhenConsentStatusRevoked() {
		Map<String, String> paramsMap = new HashMap<>();
		CispConsent cispConsentNew= new CispConsent();
		cispConsentNew.setStatus(ConsentStatusEnum.REVOKED);
		when(cispConsentAdapter.retrieveConsentByFundsIntentId(any())).thenReturn(cispConsentNew);
		helper.cancelFundsConfirmationSetup("intendId", paramsMap);
	}
	
	@Test(expected=PSD2Exception.class)
	public void cancelFundsConfirmationSetupTestWhenConsentStatusDeleted() {
		Map<String, String> paramsMap = new HashMap<>();
		CispConsent cispConsentNew= new CispConsent();
		cispConsentNew.setStatus(ConsentStatusEnum.DELETED);
		when(cispConsentAdapter.retrieveConsentByFundsIntentId(any())).thenReturn(cispConsentNew);
		helper.cancelFundsConfirmationSetup("intendId", paramsMap);
	}

	@Test
	public void createConsentTest() throws NamingException {
		OBFundsConfirmationConsentResponse1 fundsConfirmationConsentPOSTResponse = new OBFundsConfirmationConsentResponse1();
		OBFundsConfirmationConsentResponse1Data data = new OBFundsConfirmationConsentResponse1Data();
		fundsConfirmationConsentPOSTResponse.setData(data);
		fundsConfirmationConsentPOSTResponse.getData().setExpirationDateTime("expirationDateTime");

		PSD2Account customerAccount = new PSD2Account();
		AccountDetails acctDetail = new AccountDetails();
		Map<String, String> additionalInfo = new HashMap<>();
		additionalInfo.put(PSD2Constants.ACCOUNT_NUMBER, "78910111");
		additionalInfo.put(PSD2Constants.ACCOUNT_NSC, "123456");
		customerAccount.setAdditionalInformation(additionalInfo);

		List<OBAccount6Account> accountList = new ArrayList<>();
		OBAccount6Account mockData2Account = new OBAccount6Account();
		accountList.add(mockData2Account);
		customerAccount.setAccountId("accountId");
		customerAccount.setAccount(accountList);
		customerAccount.getAccount().get(0).setSchemeName("UK.OBIE.IBAN");
		OBBranchAndFinancialInstitutionIdentification50 servicer = new OBBranchAndFinancialInstitutionIdentification50();
		customerAccount.setServicer(servicer);
		customerAccount.getServicer().setIdentification("identification");
		acctDetail.setHashValue("hashedValue");

		when(scaConsentAdapterHelper.getFundsConfirmationSetupData(anyString()))
				.thenReturn(fundsConfirmationConsentPOSTResponse);
		BasicAttributes tppInformationObj = new BasicAttributes();

		helper.createConsent(customerAccount, "userId", "cid", "intentId", "channelId", "tppApplicaitonName",
				tppInformationObj, "tenantId");
	}

	@Test
	public void createConsentElseTest() throws NamingException {
		OBFundsConfirmationConsentResponse1 fundsConfirmationConsentPOSTResponse = new OBFundsConfirmationConsentResponse1();
		OBFundsConfirmationConsentResponse1Data data = new OBFundsConfirmationConsentResponse1Data();
		fundsConfirmationConsentPOSTResponse.setData(data);
		fundsConfirmationConsentPOSTResponse.getData().setExpirationDateTime("expirationDateTime");

		PSD2Account customerAccount = new PSD2Account();
		AccountDetails acctDetail = new AccountDetails();

		Map<String, String> additionalInfo = new HashMap<>();
		additionalInfo.put(PSD2Constants.ACCOUNT_NUMBER, "78910111");
		additionalInfo.put(PSD2Constants.ACCOUNT_NSC, "123456");
		customerAccount.setAdditionalInformation(additionalInfo);

		List<OBAccount6Account> accountList = new ArrayList<>();
		OBAccount6Account mockData2Account = new OBAccount6Account();
		mockData2Account.setIdentification("identification");
		accountList.add(mockData2Account);
		customerAccount.setAccountId("accountId");
		customerAccount.setAccount(accountList);
		customerAccount.getAccount().get(0).setSchemeName("UK.OBIE.SortCodeAccountNumber");
		OBBranchAndFinancialInstitutionIdentification50 servicer = new OBBranchAndFinancialInstitutionIdentification50();
		customerAccount.setServicer(servicer);
		customerAccount.getServicer().setIdentification("identification");
		acctDetail.setHashValue("hashedValue");

		when(scaConsentAdapterHelper.getFundsConfirmationSetupData(anyString()))
				.thenReturn(fundsConfirmationConsentPOSTResponse);
		helper.createConsent(customerAccount, "userId", "cid", "intentId", "channelId", "tppApplicaitonName",
				"tppInformationObj","tenantId");
	}
}
