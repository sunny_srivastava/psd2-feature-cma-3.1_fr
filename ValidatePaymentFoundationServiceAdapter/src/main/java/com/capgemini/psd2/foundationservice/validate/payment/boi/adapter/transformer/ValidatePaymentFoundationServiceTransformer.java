
package com.capgemini.psd2.foundationservice.validate.payment.boi.adapter.transformer;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.adapter.datetime.utility.TimeZoneDateTimeAdapter;
import com.capgemini.psd2.adapter.exceptions.AdapterErrorCodeEnum;
import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.adapter.fraudnet.domain.FraudServiceResponse;
import com.capgemini.psd2.foundationservice.validate.payment.boi.adapter.constants.ValidatePaymentFoundationServiceConstants;
import com.capgemini.psd2.foundationservice.validate.payment.boi.adapter.domain.DeliveryAddress;
import com.capgemini.psd2.foundationservice.validate.payment.boi.adapter.domain.FraudInputs;
import com.capgemini.psd2.foundationservice.validate.payment.boi.adapter.domain.PayeeInformation;
import com.capgemini.psd2.foundationservice.validate.payment.boi.adapter.domain.PayerInformation;
import com.capgemini.psd2.foundationservice.validate.payment.boi.adapter.domain.Payment;
import com.capgemini.psd2.foundationservice.validate.payment.boi.adapter.domain.PaymentInformation;
import com.capgemini.psd2.foundationservice.validate.payment.boi.adapter.domain.PaymentInstruction;
import com.capgemini.psd2.foundationservice.validate.payment.boi.adapter.domain.ValidationPassed;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.pisp.domain.CustomPaymentSetupPOSTResponse;
import com.capgemini.psd2.pisp.domain.DebtorAgent.SchemeNameEnum;
import com.capgemini.psd2.pisp.domain.PaymentSetupPOSTResponse;
import com.capgemini.psd2.pisp.domain.PaymentSetupResponseInitiation;
import com.capgemini.psd2.pisp.domain.PaymentSetupValidationResponse;
import com.capgemini.psd2.pisp.domain.RiskDeliveryAddress;
import com.capgemini.psd2.utilities.NullCheckUtils;


@Component
public class ValidatePaymentFoundationServiceTransformer  {

	@Autowired
	private TimeZoneDateTimeAdapter timeZoneDateTimeAdapter;
	
	public PaymentInstruction transformPaymentSetupPOSTResponse(CustomPaymentSetupPOSTResponse paymentSetupPOSTResponse, Map<String, String> params) {
		
		//PaymentInstruction Objects
		PaymentInstruction paymentInstruction = null;
		Payment payment = null;
		PaymentInformation paymentInformation = null;
		PayerInformation payerInformation = null;
		PayeeInformation payeeInformation = null;
		
		PaymentSetupResponseInitiation initiation = paymentSetupPOSTResponse.getData().getInitiation();
		
		//PaymentSetup/Data
		paymentInformation = new PaymentInformation();
		paymentInformation.setPaymentDate(timeZoneDateTimeAdapter.parseNewDateTimeFS(new Date()));
		paymentInformation.setInitiatingPartyName(params.get(PSD2Constants.CHANNEL_IN_REQ_HEADER));
		payment = new Payment();
		payment.setRequestType("ValidatePayment");
		payment.setChannelUserID(params.get(PSD2Constants.USER_IN_REQ_HEADER));
		payerInformation = new PayerInformation();
		payerInformation.setPayerCurrency(paymentSetupPOSTResponse.getPayerCurrency());
		paymentInformation.setJurisdiction(paymentSetupPOSTResponse.getPayerJurisdiction());
		payment.setPaymentId(paymentSetupPOSTResponse.getData().getPaymentId());
		
		//PaymentSetup/Data/Initiation
		paymentInformation.setInstructionIdentification(initiation.getInstructionIdentification());
		paymentInstruction = new PaymentInstruction();
		paymentInstruction.setEndToEndIdentification(initiation.getEndToEndIdentification());
		BigDecimal amount = new BigDecimal(initiation.getInstructedAmount().getAmount());
		paymentInformation.setAmount(amount);
		payeeInformation = new PayeeInformation();
		payeeInformation.setBeneficiaryCurrency(initiation.getInstructedAmount().getCurrency());
		
	
		//PaymentSetup/Data/Initiation/DebtorAgent
		
		if(!NullCheckUtils.isNullOrEmpty(initiation.getDebtorAgent())){
			if(SchemeNameEnum.BICFI.toString().equals(initiation.getDebtorAgent().getSchemeName().toString())){
				payerInformation.setBic(initiation.getDebtorAgent().getIdentification());
			}
		}
		
		//PaymentSetup/Data/Initiation/DebtorAccount
		if(!NullCheckUtils.isNullOrEmpty(initiation.getDebtorAccount())){
			if(com.capgemini.psd2.pisp.domain.DebtorAccount.SchemeNameEnum.SORTCODEACCOUNTNUMBER.toString().equals(initiation.getDebtorAccount().getSchemeName().toString())){	
				payerInformation.setNsc(initiation.getDebtorAccount().getIdentification().substring(0, 6));
				payerInformation.setAccountNumber(initiation.getDebtorAccount().getIdentification().substring(6, 14));
			}
			else if(com.capgemini.psd2.pisp.domain.DebtorAccount.SchemeNameEnum.IBAN.toString().equals(initiation.getDebtorAccount().getSchemeName().toString())){	
				payerInformation.setIban(initiation.getDebtorAccount().getIdentification());
			}
			payerInformation.setAccountName(initiation.getDebtorAccount().getName());
			payerInformation.setAccountSecondaryID(initiation.getDebtorAccount().getSecondaryIdentification());
		}
		
		
		//PaymentSetup/Data/Initiation/CreditorAgent
		if(!NullCheckUtils.isNullOrEmpty(initiation.getCreditorAgent())){
			if(com.capgemini.psd2.pisp.domain.CreditorAgent.SchemeNameEnum.BICFI.toString().equals(initiation.getCreditorAgent().getSchemeName().toString())){
				payeeInformation.setBic(initiation.getCreditorAgent().getIdentification());
			} 
		}
		
		//PaymentSetup/Data/Initiation/CreditorAccount
		if(com.capgemini.psd2.pisp.domain.CreditorAccount.SchemeNameEnum.SORTCODEACCOUNTNUMBER.toString().equals(initiation.getCreditorAccount().getSchemeName().toString())){
			payeeInformation.setBeneficiaryAccountNumber(initiation.getCreditorAccount().getIdentification().substring(6, 14));
			payeeInformation.setBeneficiaryNsc(initiation.getCreditorAccount().getIdentification().substring(0, 6));
			payeeInformation.setBeneficiaryCountry(ValidatePaymentFoundationServiceConstants.BENEFICIARY_COUNTRY);
		} else if(com.capgemini.psd2.pisp.domain.CreditorAccount.SchemeNameEnum.IBAN.toString().equals(initiation.getCreditorAccount().getSchemeName().toString())){	
			payeeInformation.setIban(initiation.getCreditorAccount().getIdentification());
			payeeInformation.setBeneficiaryCountry(initiation.getCreditorAccount().getIdentification().substring(0, 2));
		}
		payeeInformation.setBeneficiaryName(initiation.getCreditorAccount().getName());
		payeeInformation.setAccountSecondaryID(initiation.getCreditorAccount().getSecondaryIdentification());
		
		if(!NullCheckUtils.isNullOrEmpty(initiation.getRemittanceInformation())){
			paymentInformation.setUnstructured(initiation.getRemittanceInformation().getUnstructured());
			paymentInformation.setBeneficiaryReference(initiation.getRemittanceInformation().getReference());
		}
		
		//PaymentSetup/Risk
		if(!NullCheckUtils.isNullOrEmpty(paymentSetupPOSTResponse.getRisk().getPaymentContextCode())){
			paymentInformation.setPaymentContextCode(paymentSetupPOSTResponse.getRisk().getPaymentContextCode().toString());
		}

		payeeInformation.setMerchentCategoryCode(paymentSetupPOSTResponse.getRisk().getMerchantCategoryCode());
		payerInformation.setMerchantCustomerIdentification(paymentSetupPOSTResponse.getRisk().getMerchantCustomerIdentification());
		
		//fraud input data
		Object fnResponse =  paymentSetupPOSTResponse.getFraudnetResponse();
		if(fnResponse != null ){
			if(fnResponse instanceof  FraudServiceResponse){
				FraudServiceResponse fraudnetResp = (FraudServiceResponse) fnResponse;
				FraudInputs fnInputs = new FraudInputs();
				fnInputs.setSystemResponse( fraudnetResp.getDecisionType());
				paymentInformation.setFraudInputs(fnInputs);
			}
			
		}

		//PaymentSetup/Risk/DeliveryAddress
		setDeliveryAddress(paymentSetupPOSTResponse, payeeInformation);
			

        payment.setPaymentInformation(paymentInformation);
        payment.setPayerInformation(payerInformation);
        payment.setPayeeInformation(payeeInformation);
        paymentInstruction.setPayment(payment);
	 
		return paymentInstruction;
		
	}
	
	private void setDeliveryAddress(PaymentSetupPOSTResponse paymentSetupPOSTResponse, PayeeInformation payeeInformation) {
		
		if (!NullCheckUtils.isNullOrEmpty(paymentSetupPOSTResponse.getRisk().getDeliveryAddress())) {
			DeliveryAddress deliveryAddress = new DeliveryAddress();
			RiskDeliveryAddress riskDeliveryAddress = paymentSetupPOSTResponse.getRisk().getDeliveryAddress();
			if (!NullCheckUtils.isNullOrEmpty(riskDeliveryAddress.getAddressLine())) {
				int addressLineLength = riskDeliveryAddress.getAddressLine().size();
				if (addressLineLength > 0)
					deliveryAddress.setAddressLine1(riskDeliveryAddress.getAddressLine().get(0));
				if (addressLineLength > 1)
					deliveryAddress.setAddressLine2(riskDeliveryAddress.getAddressLine().get(1));
			}
			deliveryAddress.setStreetName(riskDeliveryAddress.getStreetName());
			deliveryAddress.setBuildingNumber(riskDeliveryAddress.getBuildingNumber());
			deliveryAddress.setPostCode(riskDeliveryAddress.getPostCode());
			deliveryAddress.setTownName(riskDeliveryAddress.getTownName());

			if (!NullCheckUtils.isNullOrEmpty(riskDeliveryAddress.getCountrySubDivision())) {
				int countrySubDivisionCount = riskDeliveryAddress.getCountrySubDivision().size();
				if (countrySubDivisionCount > 0)
					deliveryAddress.setCountrySubDivision1(riskDeliveryAddress.getCountrySubDivision().get(0));
				if (countrySubDivisionCount > 1)
					deliveryAddress.setCountrySubDivision2(riskDeliveryAddress.getCountrySubDivision().get(1));
			}
			deliveryAddress.setCountry(riskDeliveryAddress.getCountry());
			payeeInformation.setDeliveryAddress(deliveryAddress);
		}	
	}
	
	public PaymentSetupValidationResponse transformValidatePaymentResponse(ValidationPassed validationPassed) {
		
		if (NullCheckUtils.isNullOrEmpty(validationPassed) || NullCheckUtils.isNullOrEmpty(validationPassed.getSuccessMessage())) {
			throw AdapterException.populatePSD2Exception("FS response is null for Stage Validate Payment.", AdapterErrorCodeEnum.TECHNICAL_ERROR);
		} else {
			PaymentSetupValidationResponse paymentSetupValidationResponse = new PaymentSetupValidationResponse();
			paymentSetupValidationResponse.setPaymentSetupValidationStatus(ValidatePaymentFoundationServiceConstants.VALIDATION_STATUS_PASS);
			paymentSetupValidationResponse.setPaymentSubmissionId(validationPassed.getSuccessMessage());
			return paymentSetupValidationResponse;
		}
	}	
}
