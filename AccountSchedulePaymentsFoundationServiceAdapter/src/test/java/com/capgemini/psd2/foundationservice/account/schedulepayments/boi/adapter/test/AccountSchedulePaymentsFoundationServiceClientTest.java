package com.capgemini.psd2.foundationservice.account.schedulepayments.boi.adapter.test;



import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpHeaders;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import com.capgemini.psd2.foundationservice.account.schedulepayments.boi.adapter.client.AccountSchedulePaymentsFoundationServiceClient;
import com.capgemini.psd2.foundationservice.account.schedulepayments.boi.adapter.constants.AccountSchedulePaymentsFoundationServiceConstants;
import com.capgemini.psd2.foundationservice.account.schedulepayments.boi.adapter.raml.domain.PartiesOneOffPaymentInstructionsresponse;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.RestClientSync;

public class AccountSchedulePaymentsFoundationServiceClientTest {
	
	@InjectMocks
	AccountSchedulePaymentsFoundationServiceClient client;
	
	@Mock
	RestClientSync restClient;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void contextLoads() {
	}
	
	@Test
	public void testrestTransportForScheduleItems() {
		HttpHeaders headers=new HttpHeaders();
		headers.add("sourcesystem", "PSD2API");
		RequestInfo reqInfo=new RequestInfo();
		reqInfo.setUrl("http://localhost");
		RequestHeaderAttributes requestHeaderAttributes=new RequestHeaderAttributes();
		requestHeaderAttributes.setChannelId("567");
		requestHeaderAttributes.setCorrelationId("897");
		reqInfo.setRequestHeaderAttributes(requestHeaderAttributes);
		MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<String ,String>();
		queryParams.add(AccountSchedulePaymentsFoundationServiceConstants.PARENTNATIONALSORTCODENSCNUMBER, "902127");
		queryParams.add(AccountSchedulePaymentsFoundationServiceConstants.ACCOUNTNUMBER, "23451786");
		PartiesOneOffPaymentInstructionsresponse response= new PartiesOneOffPaymentInstructionsresponse();
		response=client.restTransportForScheduleItems(reqInfo, PartiesOneOffPaymentInstructionsresponse.class, queryParams, headers);
	
	}
}
