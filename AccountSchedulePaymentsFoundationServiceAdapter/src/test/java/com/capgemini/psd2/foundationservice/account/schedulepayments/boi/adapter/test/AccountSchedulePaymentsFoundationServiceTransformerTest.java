package com.capgemini.psd2.foundationservice.account.schedulepayments.boi.adapter.test;

import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.capgemini.psd2.adapter.datetime.utility.TimeZoneDateTimeAdapter;
import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.aisp.domain.OBExternalScheduleType1Code;
import com.capgemini.psd2.aisp.domain.OBScheduledPayment3;
import com.capgemini.psd2.foundationservice.account.schedulepayments.boi.adapter.constants.AccountSchedulePaymentsFoundationServiceConstants;
import com.capgemini.psd2.foundationservice.account.schedulepayments.boi.adapter.raml.domain.Currency;
import com.capgemini.psd2.foundationservice.account.schedulepayments.boi.adapter.raml.domain.OneOffPaymentInstruction;
import com.capgemini.psd2.foundationservice.account.schedulepayments.boi.adapter.raml.domain.PartiesOneOffPaymentInstructionsresponse;
import com.capgemini.psd2.foundationservice.account.schedulepayments.boi.adapter.raml.domain.PaymentAmount;
import com.capgemini.psd2.foundationservice.account.schedulepayments.boi.adapter.raml.domain.PaymentInstructionBasic;
import com.capgemini.psd2.foundationservice.account.schedulepayments.boi.adapter.raml.domain.PaymentInstructionCounterpartyAccountBasic;
import com.capgemini.psd2.foundationservice.account.schedulepayments.boi.adapter.raml.domain.RequestedOccurenceDateType;
import com.capgemini.psd2.foundationservice.account.schedulepayments.boi.adapter.transformer.AccountSchedulePaymentsFoundationServiceTransformer;
import com.capgemini.psd2.validator.PSD2Validator;

public class AccountSchedulePaymentsFoundationServiceTransformerTest {

	@InjectMocks
	AccountSchedulePaymentsFoundationServiceTransformer transfomer;
	@Mock
PSD2Validator psd2Validator;
	@Mock
 TimeZoneDateTimeAdapter timeZoneDateTimeAdapter;
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void contextLoads() {
	}
	

	@Test
	public void testtransformSchedulePayments() {
		PartiesOneOffPaymentInstructionsresponse inputBalanceObj= new PartiesOneOffPaymentInstructionsresponse();
		inputBalanceObj.setTotal(4.00);
		OneOffPaymentInstruction base=new OneOffPaymentInstruction();
		OBScheduledPayment3 response = new OBScheduledPayment3();
		PaymentInstructionBasic paymentInstruction=new PaymentInstructionBasic();
		paymentInstruction.setPaymentInstructionNumber("1234");
		base.setPaymentInstruction(paymentInstruction);
		PaymentInstructionCounterpartyAccountBasic account=new PaymentInstructionCounterpartyAccountBasic();
		account.setCounterpartyInternationalBankAccountNumberIBAN("4567");
		account.setCounterpartyName("boi");
		account.setCounterpartyAccountNumber("12345678");
		account.setCounterpartyNationalSortCodeNSCNumber("123456");
		account.setCounterpartySWIFTBankIdentifierCodeBIC("2345");
		account.setCounterpartyReferenceText("Welcome");
		
		List<PaymentInstructionCounterpartyAccountBasic> paymentInstructionToCounterPartyBasic = new ArrayList<>();
		paymentInstructionToCounterPartyBasic.add(account);
		base.setPaymentInstructionToCounterPartyBasic(paymentInstructionToCounterPartyBasic);
		Currency currency=new Currency();
		currency.setIsoAlphaCode("EU");
		base.setTransactionCurrency(currency);
		response.setScheduledType(OBExternalScheduleType1Code.EXECUTION);
		
		PaymentAmount paymentAmount = new PaymentAmount();
		paymentAmount.setAccountCurrency(2.0);
		paymentAmount.setTransactionCurrency(2.0);
		base.setPaymentAmount(paymentAmount);
		Date d=new Date();
		base.setRequestedOccurenceDate(d);
		base.setRequestedOccurenceDateType(RequestedOccurenceDateType.EXECUTION);
		List<OneOffPaymentInstruction> scheduleItemsList=new ArrayList<>();
		scheduleItemsList.add(base);
		inputBalanceObj.setOneOffPaymentInstructionsList(scheduleItemsList);
		Map<String, String> params=new HashMap<>();
		params.put(AccountSchedulePaymentsFoundationServiceConstants.ACCOUNT_ID, "accountId");
		transfomer.transformSchedulePayments(inputBalanceObj, params);
		assertNotNull(transfomer);
	}
	
	@Test
	public void testtransformSchedulePayments1() {
		PartiesOneOffPaymentInstructionsresponse inputBalanceObj= new PartiesOneOffPaymentInstructionsresponse();
		inputBalanceObj.setTotal(4.00);
		OneOffPaymentInstruction base=new OneOffPaymentInstruction();
		OBScheduledPayment3 response = new OBScheduledPayment3();
		PaymentInstructionBasic paymentInstruction=new PaymentInstructionBasic();
		paymentInstruction.setPaymentInstructionNumber("1234");
		base.setPaymentInstruction(paymentInstruction);
		PaymentInstructionCounterpartyAccountBasic account=new PaymentInstructionCounterpartyAccountBasic();
		account.setCounterpartyInternationalBankAccountNumberIBAN(null);
		account.setCounterpartyName("boi");
		account.setCounterpartyAccountNumber("12345678");
		account.setCounterpartyNationalSortCodeNSCNumber("123456");
		account.setCounterpartySWIFTBankIdentifierCodeBIC("2345");
		account.setCounterpartyReferenceText("Welcome");
		
		List<PaymentInstructionCounterpartyAccountBasic> paymentInstructionToCounterPartyBasic = new ArrayList<>();
		paymentInstructionToCounterPartyBasic.add(account);
		
		base.setPaymentInstructionToCounterPartyBasic(paymentInstructionToCounterPartyBasic);  
		Currency currency=new Currency();
		currency.setIsoAlphaCode("EU");
		base.setTransactionCurrency(currency);
		response.setScheduledType(OBExternalScheduleType1Code.EXECUTION);
		PaymentAmount paymentAmount = new PaymentAmount();
		paymentAmount.setAccountCurrency(2.0);
		paymentAmount.setTransactionCurrency(2.0);
		base.setPaymentAmount(paymentAmount);
		
		Date d=new Date();
		base.setRequestedOccurenceDate(d);
		base.setRequestedOccurenceDateType(RequestedOccurenceDateType.EXECUTION);
		List<OneOffPaymentInstruction> scheduleItemsList=new ArrayList<>();
		scheduleItemsList.add(base);
		inputBalanceObj.setOneOffPaymentInstructionsList(scheduleItemsList);
		Map<String, String> params=new HashMap<>();
		params.put(AccountSchedulePaymentsFoundationServiceConstants.ACCOUNT_ID, "accountId");
		transfomer.transformSchedulePayments(inputBalanceObj, params);
		assertNotNull(transfomer);
	}
	
	@Test
	public void testtransformSchedulePayments5() {
		PartiesOneOffPaymentInstructionsresponse inputBalanceObj= new PartiesOneOffPaymentInstructionsresponse();
		inputBalanceObj.setTotal(4.00);
		OneOffPaymentInstruction base=new OneOffPaymentInstruction();
		OBScheduledPayment3 response = new OBScheduledPayment3();
		PaymentInstructionBasic paymentInstruction=new PaymentInstructionBasic();
		paymentInstruction.setPaymentInstructionNumber("1234");
		base.setPaymentInstruction(paymentInstruction);
		PaymentInstructionCounterpartyAccountBasic account=new PaymentInstructionCounterpartyAccountBasic();
		account.setCounterpartyInternationalBankAccountNumberIBAN(null);
		account.setCounterpartyName("boi");
		account.setCounterpartyAccountNumber("12345678");
		account.setCounterpartyNationalSortCodeNSCNumber(null);
		account.setCounterpartyInternationalBankAccountNumberIBAN("12345");
		account.setCounterpartySWIFTBankIdentifierCodeBIC("2345");
		account.setCounterpartyReferenceText("Welcome");
		
		List<PaymentInstructionCounterpartyAccountBasic> paymentInstructionToCounterPartyBasic = new ArrayList<>();
		paymentInstructionToCounterPartyBasic.add(account);
		
		base.setPaymentInstructionToCounterPartyBasic(paymentInstructionToCounterPartyBasic);  
		Currency currency=new Currency();
		currency.setIsoAlphaCode("EU");
		base.setTransactionCurrency(currency);
		response.setScheduledType(OBExternalScheduleType1Code.EXECUTION);
		PaymentAmount paymentAmount = new PaymentAmount();
		paymentAmount.setAccountCurrency(2.0);
		paymentAmount.setTransactionCurrency(2.0);
		base.setPaymentAmount(paymentAmount);
		
		Date d=new Date();
		base.setRequestedOccurenceDate(d);
		base.setRequestedOccurenceDateType(RequestedOccurenceDateType.EXECUTION);
		List<OneOffPaymentInstruction> scheduleItemsList=new ArrayList<>();
		scheduleItemsList.add(base);
		inputBalanceObj.setOneOffPaymentInstructionsList(scheduleItemsList);
		Map<String, String> params=new HashMap<>();
		params.put(AccountSchedulePaymentsFoundationServiceConstants.ACCOUNT_ID, "accountId");
		transfomer.transformSchedulePayments(inputBalanceObj, params);
		assertNotNull(transfomer);
	}
	
	@Test(expected = AdapterException.class)
	public void testgetScheduledType() {
		PartiesOneOffPaymentInstructionsresponse inputBalanceObj= new PartiesOneOffPaymentInstructionsresponse();
		inputBalanceObj.setTotal(4.00);
		OneOffPaymentInstruction base=new OneOffPaymentInstruction();
		OBScheduledPayment3 response = new OBScheduledPayment3();
		PaymentInstructionBasic paymentInstruction=new PaymentInstructionBasic();
		paymentInstruction.setPaymentInstructionNumber("1234");
		base.setPaymentInstruction(paymentInstruction);
		PaymentInstructionCounterpartyAccountBasic account=new PaymentInstructionCounterpartyAccountBasic();
		account.setCounterpartyInternationalBankAccountNumberIBAN("4567");
		account.setCounterpartyName("boi");
		account.setCounterpartyAccountNumber("7895");
		account.setCounterpartyNationalSortCodeNSCNumber("4562");
		account.setCounterpartySWIFTBankIdentifierCodeBIC("2345");
		account.setCounterpartyReferenceText("Welcome");
		List<PaymentInstructionCounterpartyAccountBasic> paymentInstructionToCounterPartyBasic = new ArrayList<>();
		paymentInstructionToCounterPartyBasic.add(account);
		
		base.setPaymentInstructionToCounterPartyBasic(paymentInstructionToCounterPartyBasic);  
		Currency currency=new Currency();
		currency.setIsoAlphaCode("EU");
		base.setTransactionCurrency(currency);
		response.setScheduledType(OBExternalScheduleType1Code.ARRIVAL);
		PaymentAmount paymentAmount = new PaymentAmount();
		paymentAmount.setAccountCurrency(2.0);
		paymentAmount.setTransactionCurrency(2.0);
		base.setPaymentAmount(paymentAmount);
		base.setRequestedOccurenceDateType(RequestedOccurenceDateType.ARRIVAL);
		List<OneOffPaymentInstruction> scheduleItemsList=new ArrayList<>();
		scheduleItemsList.add(base);
		inputBalanceObj.setOneOffPaymentInstructionsList(scheduleItemsList);
		Map<String, String> params=new HashMap<>();
		params.put(AccountSchedulePaymentsFoundationServiceConstants.ACCOUNT_ID, "accountId");
		transfomer.transformSchedulePayments(inputBalanceObj, params);
		assertNotNull(transfomer);
	}
}
