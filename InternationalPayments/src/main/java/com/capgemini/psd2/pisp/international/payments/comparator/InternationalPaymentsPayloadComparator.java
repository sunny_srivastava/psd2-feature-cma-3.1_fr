package com.capgemini.psd2.pisp.international.payments.comparator;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

import org.springframework.stereotype.Component;

import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.pisp.domain.CustomIPaymentConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomIPaymentConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.CustomIPaymentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.OBDomestic1InstructedAmount;
import com.capgemini.psd2.pisp.domain.OBInternational1;
import com.capgemini.psd2.pisp.domain.OBPostalAddress6;
import com.capgemini.psd2.pisp.domain.OBRemittanceInformation1;
import com.capgemini.psd2.pisp.domain.OBRisk1DeliveryAddress;
import com.capgemini.psd2.pisp.domain.OBWriteInternationalConsentResponse1;
import com.capgemini.psd2.pisp.domain.PaymentConsentsPlatformResource;
import com.capgemini.psd2.pisp.utilities.PispUtilities;
import com.capgemini.psd2.utilities.JSONUtilities;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Component
public class InternationalPaymentsPayloadComparator implements Comparator<CustomIPaymentConsentsPOSTResponse> {

	@Override
	public int compare(CustomIPaymentConsentsPOSTResponse paymentResponse,
			CustomIPaymentConsentsPOSTResponse adaptedPaymentResponse) {

		int value = 1;

		if (Double.compare(Double.parseDouble(paymentResponse.getData().getInitiation().getInstructedAmount().getAmount()) ,Double
				.parseDouble(adaptedPaymentResponse.getData().getInitiation().getInstructedAmount().getAmount()))==0) {
			adaptedPaymentResponse.getData().getInitiation().getInstructedAmount()
					.setAmount(paymentResponse.getData().getInitiation().getInstructedAmount().getAmount());
		}

		if (paymentResponse.getData().getInitiation().equals(adaptedPaymentResponse.getData().getInitiation())
				&& paymentResponse.getRisk().equals(adaptedPaymentResponse.getRisk()))
			value = 0;

		return value;
	}

	public int comparePaymentDetails(Object response, Object request,
			PaymentConsentsPlatformResource paymentSetupPlatformResource) {
		CustomIPaymentConsentsPOSTResponse paymentSetupResponse = null;
		CustomIPaymentConsentsPOSTResponse tempPaymentSetupResponse = null;
		CustomIPaymentConsentsPOSTResponse adaptedPaymentResponse = null;
		CustomIPaymentConsentsPOSTRequest paymentSetupRequest = null;
		CustomIPaymentsPOSTRequest paymentSubmissionRequest = null;
		int returnValue = 1;

		if (response instanceof CustomIPaymentConsentsPOSTResponse) {
			paymentSetupResponse = (CustomIPaymentConsentsPOSTResponse) response;
			String creationDateTime = paymentSetupResponse.getData().getCreationDateTime();
			paymentSetupResponse.getData().setCreationDateTime(null);
			String strPaymentResponse = JSONUtilities.getJSONOutPutFromObject(paymentSetupResponse);
			tempPaymentSetupResponse = JSONUtilities.getObjectFromJSONString(PispUtilities.getObjectMapper(),
					strPaymentResponse, CustomIPaymentConsentsPOSTResponse.class);
			paymentSetupResponse.getData().setCreationDateTime(creationDateTime);
		}

		if (request instanceof CustomIPaymentConsentsPOSTRequest) {
			paymentSetupRequest = (CustomIPaymentConsentsPOSTRequest) request;
			String strPaymentRequest = JSONUtilities.getJSONOutPutFromObject(paymentSetupRequest);
			adaptedPaymentResponse = JSONUtilities.getObjectFromJSONString(PispUtilities.getObjectMapper(),
					strPaymentRequest, CustomIPaymentConsentsPOSTResponse.class);
		} else if (request instanceof CustomIPaymentsPOSTRequest) {
			paymentSubmissionRequest = (CustomIPaymentsPOSTRequest) request;
			String strSubmissionRequest = JSONUtilities.getJSONOutPutFromObject(paymentSubmissionRequest);
			adaptedPaymentResponse = JSONUtilities.getObjectFromJSONString(PispUtilities.getObjectMapper(),
					strSubmissionRequest, CustomIPaymentConsentsPOSTResponse.class);
		}

		if (tempPaymentSetupResponse != null && adaptedPaymentResponse != null) {

			if (!validateDebtorDetails(tempPaymentSetupResponse.getData().getInitiation(),
					adaptedPaymentResponse.getData().getInitiation(), paymentSetupPlatformResource))
				return 1;
			compareAmount(tempPaymentSetupResponse.getData().getInitiation().getInstructedAmount(),
					adaptedPaymentResponse.getData().getInitiation().getInstructedAmount());

			/*
			 * Date: 13-Feb-2018 Changes are made for CR-10
			 */
			checkAndModifyEmptyFields(adaptedPaymentResponse, (CustomIPaymentConsentsPOSTResponse) response);
			// End of CR-10

			returnValue = compare(tempPaymentSetupResponse, adaptedPaymentResponse);
		}
		return returnValue;

	}

	public boolean validateDebtorDetails(OBInternational1 responseInitiation, OBInternational1 requestInitiation,
			PaymentConsentsPlatformResource paymentSetupPlatformResource) {

		if (Boolean.valueOf(paymentSetupPlatformResource.getTppDebtorDetails())) {

			/*
			 * If DebtorAccount.Name is sent by TPP,tppDebtoNameDetails would be true, then
			 * name will participate in comparison else product will set foundation response
			 * DebtorAccount.Name as null so that it can be passed in comparison. Both
			 * fields would have the value as 'Null'.
			 */
			if (!Boolean.valueOf(paymentSetupPlatformResource.getTppDebtorNameDetails()))
				responseInitiation.getDebtorAccount().setName(null);
			String schemeNameOfResponseInitiation = responseInitiation.getDebtorAccount().getSchemeName();
			String schemeNameOfRequestInitiation = requestInitiation.getDebtorAccount().getSchemeName();
			if(schemeNameOfResponseInitiation!= null && schemeNameOfResponseInitiation.startsWith(PSD2Constants.OB_SCHEME_PREFIX)){
				responseInitiation.getDebtorAccount().setSchemeName(schemeNameOfResponseInitiation.split(PSD2Constants.OB_SCHEME_PREFIX)[1]);
		
			}
			if (schemeNameOfRequestInitiation!= null && schemeNameOfRequestInitiation.startsWith(PSD2Constants.OB_SCHEME_PREFIX)) {
				requestInitiation.getDebtorAccount().setSchemeName(schemeNameOfRequestInitiation.split(PSD2Constants.OB_SCHEME_PREFIX)[1]);
			}
			return Objects.equals(responseInitiation.getDebtorAccount(), requestInitiation.getDebtorAccount());
		}
		if (!Boolean.valueOf(paymentSetupPlatformResource.getTppDebtorDetails())
				&& requestInitiation.getDebtorAccount() != null) {
			return Boolean.FALSE;
		}
		if (!Boolean.valueOf(paymentSetupPlatformResource.getTppDebtorDetails())
				&& responseInitiation.getDebtorAccount() != null) {
			responseInitiation.setDebtorAccount(null);
			return Boolean.TRUE;
		}
		return Boolean.TRUE;

	}

	private void compareAmount(OBDomestic1InstructedAmount responseInstructedAmount,
			OBDomestic1InstructedAmount requestInstructedAmount) {
		if (Double.compare(Double.parseDouble(responseInstructedAmount.getAmount()),
				Double.parseDouble(requestInstructedAmount.getAmount())) == 0)
			responseInstructedAmount.setAmount(requestInstructedAmount.getAmount());
	}

	private void checkAndModifyEmptyFields(OBWriteInternationalConsentResponse1 adaptedPaymentResponse,
			CustomIPaymentConsentsPOSTResponse response) {

		checkAndModifyRemittanceInformation(adaptedPaymentResponse.getData().getInitiation(), response);
		if (adaptedPaymentResponse.getRisk().getDeliveryAddress() != null) {
			checkAndModifyAddressLine(adaptedPaymentResponse.getRisk().getDeliveryAddress(), response);
		}

		if (adaptedPaymentResponse.getData().getInitiation().getCreditor()!=null && adaptedPaymentResponse.getData().getInitiation().getCreditor().getPostalAddress() != null
				&& adaptedPaymentResponse.getData().getInitiation().getCreditor().getPostalAddress()
						.getAddressLine() != null) {
			checkAndModifyCreditorPostalAddressLine(
					adaptedPaymentResponse.getData().getInitiation().getCreditor().getPostalAddress(), response);
		}

	}

	private void checkAndModifyRemittanceInformation(OBInternational1 obInternational1,
			CustomIPaymentConsentsPOSTResponse response) {

		OBRemittanceInformation1 remittanceInformation = obInternational1.getRemittanceInformation();
		if (remittanceInformation != null && response.getData().getInitiation().getRemittanceInformation() == null
				&& NullCheckUtils.isNullOrEmpty(remittanceInformation.getReference())
				&& NullCheckUtils.isNullOrEmpty(remittanceInformation.getUnstructured())) {

			obInternational1.setRemittanceInformation(null);
		}
	}

	private void checkAndModifyAddressLine(OBRisk1DeliveryAddress riskDeliveryAddress,
			CustomIPaymentConsentsPOSTResponse response) {

		if (riskDeliveryAddress.getAddressLine() != null) {
			List<String> addressLineList = new ArrayList<>();
			for (String addressLine : riskDeliveryAddress.getAddressLine()) {
				if (NullCheckUtils.isNullOrEmpty(addressLine))
					continue;
				addressLineList.add(addressLine);
			}
			if (addressLineList.isEmpty() && response.getRisk().getDeliveryAddress().getAddressLine() == null)
				addressLineList = null;

			riskDeliveryAddress.setAddressLine(addressLineList);

		}
	}

	private void checkAndModifyCreditorPostalAddressLine(OBPostalAddress6 obPostalAddress6,
			CustomIPaymentConsentsPOSTResponse response) {
		List<String> addressLineList = new ArrayList<>();
		for (String addressLine : (obPostalAddress6.getAddressLine())) {
			if (NullCheckUtils.isNullOrEmpty(addressLine))
				continue;
			addressLineList.add(addressLine);
		}
		if (addressLineList.isEmpty()
				&& response.getData().getInitiation().getCreditor().getPostalAddress().getAddressLine() == null)
			addressLineList = null;

		obPostalAddress6.setAddressLine(addressLineList);
	}
}
