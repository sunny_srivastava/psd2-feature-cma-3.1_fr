package com.capgemini.psd2.pisp.international.payments.test.mockdata;

import javax.ws.rs.POST;

import com.capgemini.psd2.pisp.domain.CustomIPaymentConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomIPaymentConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.CustomIPaymentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.OBCashAccountDebtor3;
import com.capgemini.psd2.pisp.domain.OBDomestic1InstructedAmount;
import com.capgemini.psd2.pisp.domain.OBInternational1;
import com.capgemini.psd2.pisp.domain.OBRisk1;
import com.capgemini.psd2.pisp.domain.OBWriteDataInternational1;
import com.capgemini.psd2.pisp.domain.OBWriteDataInternationalConsent1;
import com.capgemini.psd2.pisp.domain.OBWriteDataInternationalConsentResponse1;
import com.capgemini.psd2.pisp.domain.PaymentConsentsPlatformResource;

public class IPPayloadComparatorTestMockData {

	
	public static CustomIPaymentConsentsPOSTResponse paymentResponse;
	public static CustomIPaymentConsentsPOSTResponse adaptedPaymentResponse;
	public static CustomIPaymentConsentsPOSTRequest request;
	public static  PaymentConsentsPlatformResource PlatformResource;
	public static 	CustomIPaymentsPOSTRequest POSTrequest;
	
	public static CustomIPaymentConsentsPOSTResponse getpaymentresponse() {
		paymentResponse = new CustomIPaymentConsentsPOSTResponse();
		OBWriteDataInternationalConsentResponse1 data = new OBWriteDataInternationalConsentResponse1();
		OBInternational1 initiation = new OBInternational1();
		OBCashAccountDebtor3 debtorAccount=new OBCashAccountDebtor3();
		initiation.setDebtorAccount(debtorAccount);
		OBDomestic1InstructedAmount instructedAmount = new OBDomestic1InstructedAmount();
		instructedAmount.setAmount("12345");
		initiation.setInstructedAmount(instructedAmount);
		data.setInitiation(initiation);
		paymentResponse.setData(data);
		OBRisk1 risk=new OBRisk1();
		paymentResponse.setRisk(risk);
		return paymentResponse;

	}
	
	
	public static CustomIPaymentConsentsPOSTResponse getpaymentNULLresponse() {

		paymentResponse = new CustomIPaymentConsentsPOSTResponse();
		OBWriteDataInternationalConsentResponse1 data = new OBWriteDataInternationalConsentResponse1();
		OBInternational1 initiation = new OBInternational1();

	
		data.setInitiation(initiation);
		paymentResponse.setData(data);
		OBRisk1 risk=new OBRisk1();
		paymentResponse.setRisk(risk);
		return paymentResponse;

	}
	
	
	public static CustomIPaymentConsentsPOSTResponse getadaptedpaymentresponse() {

		adaptedPaymentResponse = new CustomIPaymentConsentsPOSTResponse();
		OBWriteDataInternationalConsentResponse1 data = new OBWriteDataInternationalConsentResponse1();
		OBInternational1 initiation = new OBInternational1();

		data.setInitiation(initiation);

		adaptedPaymentResponse.setData(data);
		OBRisk1 risk=new OBRisk1();
		adaptedPaymentResponse.setRisk(risk);

		return adaptedPaymentResponse;

	}
	
	public static CustomIPaymentConsentsPOSTRequest getrequest()
	{
		request=new CustomIPaymentConsentsPOSTRequest();
		OBWriteDataInternationalConsent1 data=new OBWriteDataInternationalConsent1();
		OBInternational1 initiation=new OBInternational1();
		OBDomestic1InstructedAmount instructedAmount = new OBDomestic1InstructedAmount();
		instructedAmount.setAmount("12345");
		initiation.setInstructedAmount(instructedAmount);
		data.setInitiation(initiation);
		request.setData(data);
		OBRisk1 risk=new OBRisk1();
		request.setRisk(risk);
		
		return request;
		
	}
	
	public static PaymentConsentsPlatformResource getPlatformResource()
	{
		PlatformResource=new PaymentConsentsPlatformResource();
		PlatformResource.setTppDebtorDetails("true");
		PlatformResource.setTppDebtorNameDetails("BOI");
		return PlatformResource;
		
	}
	
	public static CustomIPaymentsPOSTRequest getPOSTrequest()
	{
		POSTrequest=new CustomIPaymentsPOSTRequest();
		POSTrequest.setCreatedOn("30-10-2018");
		OBWriteDataInternational1 data=new OBWriteDataInternational1();
		data.consentId("1234");
		POSTrequest.setData(data);
		OBInternational1 initiation=new OBInternational1();
		data.setInitiation(initiation);
		OBRisk1 risk=new OBRisk1();
		POSTrequest.setRisk(risk);
		
		return POSTrequest;
		
	}
	
	
	
	
	
	
}
