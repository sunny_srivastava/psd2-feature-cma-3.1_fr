package com.capgemini.psd2.pisp.international.payments.test.service.impl;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyMap;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.consent.domain.PispConsent;
import com.capgemini.psd2.exceptions.OBPSD2ExceptionUtility;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.adapter.InternationalPaymentsAdapter;
import com.capgemini.psd2.pisp.adapter.PispConsentAdapter;
import com.capgemini.psd2.pisp.domain.CustomIPaymentConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.CustomIPaymentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.OBCashAccountDebtor3;
import com.capgemini.psd2.pisp.domain.OBExternalStatus2Code;
import com.capgemini.psd2.pisp.domain.OBInternational1;
import com.capgemini.psd2.pisp.domain.OBMultiAuthorisation1;
import com.capgemini.psd2.pisp.domain.OBRemittanceInformation1;
import com.capgemini.psd2.pisp.domain.OBWriteDataInternational1;
import com.capgemini.psd2.pisp.domain.OBWriteDataInternationalResponse1;
import com.capgemini.psd2.pisp.domain.PaymentInternationalSubmitPOST201Response;
import com.capgemini.psd2.pisp.domain.PaymentsPlatformResource;
import com.capgemini.psd2.pisp.enums.PaymentTypeEnum;
import com.capgemini.psd2.pisp.international.payments.comparator.InternationalPaymentsPayloadComparator;
import com.capgemini.psd2.pisp.international.payments.service.impl.InternationalPaymentsServiceImpl;
import com.capgemini.psd2.pisp.processing.adapter.service.PaymentSubmissionProcessingAdapter;
import com.capgemini.psd2.pisp.stage.operations.adapter.InternationalPaymentStagingAdapter;

@RunWith(SpringJUnit4ClassRunner.class)
public class InternationalPaymentsServiceImplTest {

	@Mock
	private PaymentSubmissionProcessingAdapter paymentsProcessingAdapter;

	@Mock
	private InternationalPaymentsAdapter iPaymentsAdapter;

	@Mock
	private InternationalPaymentStagingAdapter iPaymentConsentsAdapter;

	@Mock
	private InternationalPaymentsPayloadComparator iPaymentsComparator;
	
	@Mock
	private RequestHeaderAttributes reqHeaderAtrributes;
	
	@Mock
	private PispConsentAdapter pispConsentAdapter;

	@InjectMocks
	private InternationalPaymentsServiceImpl service;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);

		Map<String, String> map = new HashMap<>();
		map.put("HEADER", "header error message");
		map.put("FREQUENCY", "freq ma");
		map.put("AFTER_CUTOFF_DATE", "freq ma");
		map.put("FIELD", "freq ma");
		map.put("SIGNATURE", "freq ma");
		map.put("INCOMPATIBLE", "freq ma");
		map.put("RES_NOTFOUND", "freq ma");
		map.put("INTERNAL", "freq ma");
		OBPSD2ExceptionUtility.genericErrorMessages.putAll(map);

		Map<String, String> map1 = new HashMap<>();
		map1.put("signature_missing", "drtyrty");
		OBPSD2ExceptionUtility.specificErrorMessages.putAll(map1);

	}

	@Test
	public void contextLoads() {
	}

	@Test(expected = PSD2Exception.class)
	public void testCreateIPResourceWithNULLFoundationResource() {
		CustomIPaymentsPOSTRequest paymentRequest = new CustomIPaymentsPOSTRequest();
		OBWriteDataInternational1 data = new OBWriteDataInternational1();
		OBInternational1 initiation = new OBInternational1();
		data.setInitiation(initiation);
		OBCashAccountDebtor3 debtorAccount = new OBCashAccountDebtor3();
		initiation.setDebtorAccount(debtorAccount);
		debtorAccount.setName("debitor");

		paymentRequest.setData(data);
		data.setConsentId("12345");
		Map<String, Object> value = new HashMap<>();
		Object key = new Object();
		value.put("1", key);

		Mockito.when(paymentsProcessingAdapter.preSubmissionProcessing(anyObject(), anyObject(), any()))
				.thenReturn(value);
		Mockito.when(iPaymentsComparator.comparePaymentDetails(anyObject(), anyObject(), anyObject())).thenReturn(0);
		CustomIPaymentConsentsPOSTResponse value1 = new CustomIPaymentConsentsPOSTResponse();
		value1.setFraudScore(new Object());
		Mockito.when(iPaymentConsentsAdapter.retrieveStagedInternationalPaymentConsents(anyObject(), anyMap()))
				.thenReturn(value1);

		String paymentVersion;
		PaymentTypeEnum paymentTypeEnum;
		PaymentsPlatformResource paymentsplatformresource = new PaymentsPlatformResource();
		paymentsplatformresource.setCreatedAt("30-10-2018");
		paymentsplatformresource.setProccessState("Completed");
		Mockito.when(paymentsProcessingAdapter.createInitialPaymentsPlatformResource(anyString(), anyString(), any()))
				.thenReturn(paymentsplatformresource);

		Object object = service.createInternationalPaymentsResource(paymentRequest);

	}

	@Test
	public void testCreateIPResource() {
		CustomIPaymentsPOSTRequest paymentRequest = new CustomIPaymentsPOSTRequest();
		OBWriteDataInternational1 data = new OBWriteDataInternational1();
		OBInternational1 initiation = new OBInternational1();
		data.setInitiation(initiation);
		OBRemittanceInformation1 remittanceInfo = new OBRemittanceInformation1();
		initiation.setRemittanceInformation(remittanceInfo);
		remittanceInfo.setReference("1");

		OBCashAccountDebtor3 debtorAccount = new OBCashAccountDebtor3();
		initiation.setDebtorAccount(debtorAccount);
		debtorAccount.setName("debitor");

		paymentRequest.setData(data);
		data.setConsentId("12345");
		Map<String, Object> value = new HashMap<>();
		Object key = new Object();
		value.put("1", key);

		Mockito.when(paymentsProcessingAdapter.preSubmissionProcessing(anyObject(), anyObject(), any()))
				.thenReturn(value);
		Mockito.when(iPaymentsComparator.comparePaymentDetails(anyObject(), anyObject(), anyObject())).thenReturn(0);
		CustomIPaymentConsentsPOSTResponse value1 = new CustomIPaymentConsentsPOSTResponse();
		value1.setFraudScore(new Object());
		Mockito.when(iPaymentConsentsAdapter.retrieveStagedInternationalPaymentConsents(anyObject(), anyMap()))
				.thenReturn(value1);
		PaymentInternationalSubmitPOST201Response response = new PaymentInternationalSubmitPOST201Response();
		OBWriteDataInternationalResponse1 data1 = new OBWriteDataInternationalResponse1();
		OBMultiAuthorisation1 multiAuthorisation = new OBMultiAuthorisation1();
		multiAuthorisation.setStatus(OBExternalStatus2Code.AUTHORISED);
		data1.setMultiAuthorisation(multiAuthorisation);

		OBInternational1 initiation1 = new OBInternational1();
		OBRemittanceInformation1 remittanceInformation1 = new OBRemittanceInformation1();
		remittanceInformation1.setReference("12");
		initiation1.setRemittanceInformation(remittanceInformation1);
		data1.setInitiation(initiation1);

		response.setData(data1);
		Mockito.when(iPaymentsAdapter.processPayments(anyObject(), anyMap(), anyMap()))
				.thenReturn(new PaymentInternationalSubmitPOST201Response());
		Mockito.when(iPaymentsAdapter.retrieveStagedPaymentsResponse(anyObject(), anyMap())).thenReturn(response);
		String paymentVersion;
		PaymentTypeEnum paymentTypeEnum;
		PaymentsPlatformResource paymentsplatformresource = new PaymentsPlatformResource();
		paymentsplatformresource.setCreatedAt("30-10-2018");
		paymentsplatformresource.setProccessState("Completed");
		Mockito.when(paymentsProcessingAdapter.createInitialPaymentsPlatformResource(anyString(), anyString(),any()))
				.thenReturn(paymentsplatformresource);
		PaymentInternationalSubmitPOST201Response obj = new PaymentInternationalSubmitPOST201Response();
		OBWriteDataInternationalResponse1 data2 = new OBWriteDataInternationalResponse1();
		data2.setConsentId("9998");
		obj.setData(data2);

		Mockito.when(paymentsProcessingAdapter.postPaymentProcessFlows(anyMap(), anyObject(), any(), anyString(), any(),
				anyString())).thenReturn(obj);
		Object object = service.createInternationalPaymentsResource(paymentRequest);
		assertNotNull(object);
	}

	@Test
	public void testCreateIPResourceIncomplete() {
		CustomIPaymentsPOSTRequest paymentRequest = new CustomIPaymentsPOSTRequest();
		OBWriteDataInternational1 data = new OBWriteDataInternational1();
		OBInternational1 initiation = new OBInternational1();
		data.setInitiation(initiation);
		OBRemittanceInformation1 remittanceInfo = new OBRemittanceInformation1();
		initiation.setRemittanceInformation(remittanceInfo);
		remittanceInfo.setReference("1");

		OBCashAccountDebtor3 debtorAccount = new OBCashAccountDebtor3();
		initiation.setDebtorAccount(debtorAccount);
		debtorAccount.setName("debitor");

		paymentRequest.setData(data);
		data.setConsentId("12345");
		Map<String, Object> value = new HashMap<>();
		Object key = new Object();
		value.put("1", key);

		Mockito.when(paymentsProcessingAdapter.preSubmissionProcessing(anyObject(), anyObject(), any()))
				.thenReturn(value);
		Mockito.when(iPaymentsComparator.comparePaymentDetails(anyObject(), anyObject(), anyObject())).thenReturn(0);
		CustomIPaymentConsentsPOSTResponse value1 = new CustomIPaymentConsentsPOSTResponse();
		value1.setFraudScore(new Object());
		Mockito.when(iPaymentConsentsAdapter.retrieveStagedInternationalPaymentConsents(anyObject(), anyMap()))
				.thenReturn(value1);
		PaymentInternationalSubmitPOST201Response response = new PaymentInternationalSubmitPOST201Response();
		OBWriteDataInternationalResponse1 data1 = new OBWriteDataInternationalResponse1();
		OBMultiAuthorisation1 multiAuthorisation = new OBMultiAuthorisation1();
		multiAuthorisation.setStatus(OBExternalStatus2Code.AUTHORISED);
		data1.setMultiAuthorisation(multiAuthorisation);

		OBInternational1 initiation1 = new OBInternational1();
		OBRemittanceInformation1 remittanceInformation1 = new OBRemittanceInformation1();
		remittanceInformation1.setReference("12");
		initiation1.setRemittanceInformation(remittanceInformation1);
		data1.setInitiation(initiation1);
		response.setData(data1);

		Mockito.when(iPaymentsAdapter.processPayments(anyObject(), anyMap(), anyMap())).thenReturn(response);
		Mockito.when(iPaymentsAdapter.retrieveStagedPaymentsResponse(anyObject(), anyMap()))
				.thenReturn(new PaymentInternationalSubmitPOST201Response());
		String paymentVersion;
		PaymentTypeEnum paymentTypeEnum;
		PaymentsPlatformResource paymentsplatformresource = new PaymentsPlatformResource();
		paymentsplatformresource.setCreatedAt("30-10-2018");
		paymentsplatformresource.setProccessState("Incomplete");
		Mockito.when(paymentsProcessingAdapter.createInitialPaymentsPlatformResource(anyString(), anyString(), any()))
				.thenReturn(paymentsplatformresource);
		PaymentInternationalSubmitPOST201Response obj = new PaymentInternationalSubmitPOST201Response();
		OBWriteDataInternationalResponse1 data2 = new OBWriteDataInternationalResponse1();
		data2.setConsentId("9998");
		obj.setData(data2);
		Mockito.when(paymentsProcessingAdapter.postPaymentProcessFlows(anyMap(), anyObject(), any(), anyString(), any(),
				anyString())).thenReturn(obj);
		Object object = service.createInternationalPaymentsResource(paymentRequest);
		assertNotNull(object);
	}

	@Test
	public void testRetrieve() {

		Map<String, PaymentsPlatformResource> value = new HashMap<>();
		PaymentsPlatformResource resurce = new PaymentsPlatformResource();
		resurce.setPaymentConsentId("123456");

		value.put("submission", resurce);
		Mockito.when(paymentsProcessingAdapter.prePaymentProcessGETFlows(anyString(), anyObject())).thenReturn(value);
		PaymentInternationalSubmitPOST201Response response = new PaymentInternationalSubmitPOST201Response();
		OBWriteDataInternationalResponse1 data1 = new OBWriteDataInternationalResponse1();
		OBMultiAuthorisation1 multiAuthorisation = new OBMultiAuthorisation1();
		multiAuthorisation.setStatus(OBExternalStatus2Code.AUTHORISED);
		data1.setMultiAuthorisation(multiAuthorisation);
		response.setData(data1);
		Mockito.when(iPaymentsAdapter.retrieveStagedPaymentsResponse(anyObject(), anyMap())).thenReturn(response);
		String submissionId = "123456";
		PaymentInternationalSubmitPOST201Response obj = new PaymentInternationalSubmitPOST201Response();
		OBWriteDataInternationalResponse1 data2 = new OBWriteDataInternationalResponse1();
		data2.setConsentId("9998");
		obj.setData(data2);
		
		PispConsent consent = new PispConsent();
		consent.setPsuId("12345");
		when(pispConsentAdapter.retrieveConsentByPaymentId(anyObject())).thenReturn(consent);
		
		Mockito.when(paymentsProcessingAdapter.postPaymentProcessFlows(anyMap(), anyObject(), any(), anyString(), any(),
				anyString())).thenReturn(obj);

		Object object = service.retrieveInternationalPaymentsResource(submissionId);
		assertNotNull(object);
	}

	@Test(expected = PSD2Exception.class)
	public void testRetrieveNULLFoundationresource() {

		Map<String, PaymentsPlatformResource> value = new HashMap<>();
		PaymentsPlatformResource resurce = new PaymentsPlatformResource();
		resurce.setPaymentConsentId("123456");

		value.put("submission", resurce);
		Mockito.when(paymentsProcessingAdapter.prePaymentProcessGETFlows(anyString(), anyObject())).thenReturn(value);
		
		  PaymentInternationalSubmitPOST201Response response = new
		  PaymentInternationalSubmitPOST201Response();
		  
		  OBWriteDataInternationalResponse1 data1=new
		  OBWriteDataInternationalResponse1(); OBMultiAuthorisation1
		  multiAuthorisation=new OBMultiAuthorisation1();
		  multiAuthorisation.setStatus(OBExternalStatus2Code.AUTHORISED);
		  data1.setMultiAuthorisation(multiAuthorisation); response.setData(data1);
		  
		PispConsent consent = new PispConsent();
		consent.setPsuId("12345");
		when(pispConsentAdapter.retrieveConsentByPaymentId(anyObject())).thenReturn(consent);
		 
		Mockito.when(iPaymentsAdapter.retrieveStagedPaymentsResponse(anyObject(), anyMap())).thenReturn(null);
		String submissionId = "123456";
		service.retrieveInternationalPaymentsResource(submissionId);
	}

	@After
	public void tearDown() throws Exception {
		paymentsProcessingAdapter = null;
		iPaymentsAdapter = null;
		iPaymentConsentsAdapter = null;
		iPaymentsComparator = null;
		service = null;
	}

}