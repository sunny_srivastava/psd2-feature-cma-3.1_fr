package com.capgemini.psd2.pisp.international.payments.test.comparator;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.pisp.domain.CustomIPaymentConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomIPaymentConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.CustomIPaymentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.OBCashAccountDebtor3;
import com.capgemini.psd2.pisp.domain.OBDomestic1InstructedAmount;
import com.capgemini.psd2.pisp.domain.OBPartyIdentification43;
import com.capgemini.psd2.pisp.domain.OBPostalAddress6;
import com.capgemini.psd2.pisp.domain.OBRemittanceInformation1;
import com.capgemini.psd2.pisp.domain.OBRisk1DeliveryAddress;
import com.capgemini.psd2.pisp.domain.PaymentConsentsPlatformResource;
import com.capgemini.psd2.pisp.international.payments.comparator.InternationalPaymentsPayloadComparator;
import com.capgemini.psd2.pisp.international.payments.test.mockdata.IPPayloadComparatorTestMockData;

@RunWith(SpringJUnit4ClassRunner.class)
public class InternationalPaymentsPayloadComparatorTest {

	@InjectMocks
	private InternationalPaymentsPayloadComparator comparator;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void contextLoads() {
	}

	
	@Test
	public void compareNULLResponses()
	{
		CustomIPaymentConsentsPOSTResponse paymentResponse=IPPayloadComparatorTestMockData.getpaymentresponse();
		CustomIPaymentConsentsPOSTResponse adaptedPaymentResponse=IPPayloadComparatorTestMockData.getpaymentresponse();
		int i=comparator.compare(paymentResponse, adaptedPaymentResponse);
		assertEquals(0, i);
	}
	
	
	@Test
	public void compare()
	{
		CustomIPaymentConsentsPOSTResponse response=IPPayloadComparatorTestMockData.getpaymentresponse();
		Object request=IPPayloadComparatorTestMockData.getrequest();
		PaymentConsentsPlatformResource paymentSetupPlatformResource= new PaymentConsentsPlatformResource();
		int i=comparator.comparePaymentDetails(response, request, paymentSetupPlatformResource);
		assertEquals(0, i);
	}
	
	@Test
	public void compare1()
	{
		CustomIPaymentConsentsPOSTResponse response=IPPayloadComparatorTestMockData.getpaymentresponse();
		CustomIPaymentConsentsPOSTRequest request=IPPayloadComparatorTestMockData.getrequest();
		OBCashAccountDebtor3 debtorAccount=new OBCashAccountDebtor3();
		debtorAccount.setName("BOI");
		request.getData().getInitiation().setDebtorAccount(debtorAccount);
		PaymentConsentsPlatformResource paymentSetupPlatformResource=IPPayloadComparatorTestMockData.getPlatformResource();
		paymentSetupPlatformResource.setTppDebtorDetails("false");
		int i=comparator.comparePaymentDetails(response, request, paymentSetupPlatformResource);
		assertEquals(1, i);
	}
	
	@Test
	public void compareReponseDebitorNotNULL(){
		CustomIPaymentConsentsPOSTResponse response=IPPayloadComparatorTestMockData.getpaymentresponse();
		OBDomestic1InstructedAmount instructedAmount1=new OBDomestic1InstructedAmount();
		instructedAmount1.setAmount("222");
		OBRisk1DeliveryAddress deliveryAddress=new OBRisk1DeliveryAddress();
		deliveryAddress.setCountry("500901");
		response.getRisk().setDeliveryAddress(deliveryAddress);
		response.getData().getInitiation().setInstructedAmount(instructedAmount1);
		OBCashAccountDebtor3 debtorAccount=new OBCashAccountDebtor3();
		debtorAccount.setName("BOI");
		response.getData().getInitiation().setDebtorAccount(debtorAccount);
		CustomIPaymentConsentsPOSTRequest request=IPPayloadComparatorTestMockData.getrequest();
		OBDomestic1InstructedAmount instructedAmount=new OBDomestic1InstructedAmount();
		instructedAmount.setAmount("222");
		request.getData().getInitiation().setInstructedAmount(instructedAmount);
		OBRisk1DeliveryAddress deliveryAddress1=new OBRisk1DeliveryAddress();
		deliveryAddress1.setCountry("Ireland");
		request.getRisk().setDeliveryAddress(deliveryAddress1);
		List<String> addressLine=new ArrayList<>();
		addressLine.add("DUBLIN");
		deliveryAddress1.setAddressLine(addressLine);
		//List<String> countrySubDivision=new ArrayList<>();
		String countrySubDivision = "DUBLIN";
		deliveryAddress1.setCountrySubDivision(countrySubDivision);
		OBRemittanceInformation1 remittanceInformation=new OBRemittanceInformation1();
		remittanceInformation.setReference("refrence");
		remittanceInformation.setUnstructured("Unstructured");
		OBPartyIdentification43 creditor = new OBPartyIdentification43();
		OBPostalAddress6 postalAddress = new OBPostalAddress6();
		postalAddress.setAddressLine(addressLine);
		creditor.setPostalAddress(postalAddress);
		response.getData().getInitiation().setCreditor(creditor);
		request.getData().getInitiation().setCreditor(creditor);
		request.getData().getInitiation().setRemittanceInformation(remittanceInformation);
		PaymentConsentsPlatformResource paymentSetupPlatformResource=IPPayloadComparatorTestMockData.getPlatformResource();
		paymentSetupPlatformResource.setTppDebtorDetails("false");
		int i=comparator.comparePaymentDetails(response, request, paymentSetupPlatformResource);
		assertEquals(1, i);
	}
	
	@Test
	public void comparePOST()
	{
		CustomIPaymentConsentsPOSTResponse response=IPPayloadComparatorTestMockData.getpaymentresponse();
		OBCashAccountDebtor3 debtorAccount=new OBCashAccountDebtor3();
		response.getData().getInitiation().setDebtorAccount(debtorAccount);
		debtorAccount.setName("IOB");
		CustomIPaymentsPOSTRequest request=IPPayloadComparatorTestMockData.getPOSTrequest();
		OBCashAccountDebtor3 debtorAccount1=new OBCashAccountDebtor3();
		request.getData().getInitiation().setDebtorAccount(debtorAccount1);
		debtorAccount1.setName("BOI");
		PaymentConsentsPlatformResource paymentSetupPlatformResource=IPPayloadComparatorTestMockData.getPlatformResource();
		int i=comparator.comparePaymentDetails(response, request, paymentSetupPlatformResource);
		assertEquals(1, i);
	}
	
	
	@Test
	public void comparePOST1()
	{
		CustomIPaymentConsentsPOSTResponse response=IPPayloadComparatorTestMockData.getpaymentresponse();
		OBCashAccountDebtor3 debtorAccount=new OBCashAccountDebtor3();
		
		CustomIPaymentsPOSTRequest request=IPPayloadComparatorTestMockData.getPOSTrequest();
		OBCashAccountDebtor3 debtorAccount1=new OBCashAccountDebtor3();
		request.getData().getInitiation().setDebtorAccount(debtorAccount1);
		debtorAccount1.setName("BOI");
		PaymentConsentsPlatformResource paymentSetupPlatformResource=new PaymentConsentsPlatformResource();
		paymentSetupPlatformResource.setTppDebtorDetails("false");
		paymentSetupPlatformResource.setTppDebtorDetails("true");
		int i=comparator.comparePaymentDetails(response, request, paymentSetupPlatformResource);
		assertEquals(1, i);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	@After
	public void tearDown() throws Exception {
		comparator = null;
	}

}
