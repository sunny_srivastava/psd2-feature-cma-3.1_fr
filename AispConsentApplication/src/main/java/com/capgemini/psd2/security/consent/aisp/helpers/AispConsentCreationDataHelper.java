package com.capgemini.psd2.security.consent.aisp.helpers;

import java.util.List;

import javax.naming.NamingException;

import com.capgemini.psd2.aisp.domain.OBAccount6;
import com.capgemini.psd2.aisp.domain.OBReadAccount6;

public interface AispConsentCreationDataHelper {

	public String retrieveAccountRequestSetupData(String intentId);

	public OBReadAccount6 retrieveCustomerAccountListInfo(String userId, String clientId, String flowType,
			String correlationId, String channelId, String tenantId, String intentId, String cmaVersion);

	public void createConsent(List<OBAccount6> accountList, String userId, String cid, String intentId,
			String channelId, String headers, String tppAppName, Object tppInformation, String tennantId) throws NamingException;

	public void cancelAccountRequest(String intentId);

	public OBReadAccount6 findExistingConsentAccounts(String intentId);
	
	public String getCMAVersion(String intentId);

}
