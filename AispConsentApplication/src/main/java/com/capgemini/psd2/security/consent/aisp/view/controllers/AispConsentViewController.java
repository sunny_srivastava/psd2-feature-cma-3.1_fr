/*************************************************************************
 * 
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 * 
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 */
package com.capgemini.psd2.security.consent.aisp.view.controllers;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.Map;

import javax.naming.NamingException;
import javax.naming.directory.BasicAttributes;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.UriUtils;

import com.capgemini.psd2.aisp.domain.OBReadAccount6;
import com.capgemini.psd2.fraudsystem.request.handler.impl.FraudSystemRequestMapping;
import com.capgemini.psd2.fraudsystem.utilities.FraudSystemUtilities;
import com.capgemini.psd2.integration.adapter.TPPInformationAdaptor;
import com.capgemini.psd2.logger.LoggerUtils;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.mask.DataMask;
import com.capgemini.psd2.scaconsenthelper.config.CdnConfig;
import com.capgemini.psd2.scaconsenthelper.config.helpers.SCAConsentHelper;
import com.capgemini.psd2.scaconsenthelper.constants.OIDCConstants;
import com.capgemini.psd2.scaconsenthelper.models.PickupDataModel;
import com.capgemini.psd2.security.consent.aisp.config.AispHostNameConfig;
import com.capgemini.psd2.security.consent.aisp.helpers.AispConsentCreationDataHelper;
import com.capgemini.psd2.security.constants.PSD2SecurityConstants;
import com.capgemini.psd2.security.exceptions.PSD2SecurityException;
import com.capgemini.psd2.security.exceptions.SCAConsentErrorCodeEnum;
import com.capgemini.psd2.tppinformation.adaptor.ldap.constants.TPPInformationConstants;
import com.capgemini.psd2.ui.content.utility.controller.UIStaticContentUtilityController;
import com.capgemini.psd2.utilities.SandboxConfig;

/**
 * ConsentView Controller
 * 
 * @author Capgemini
 *
 */
@Controller
public class AispConsentViewController {

	private static final Logger LOG = LoggerFactory.getLogger(AispConsentViewController.class);
	
	@Autowired
	private LoggerUtils loggerUtils;
	
	@Autowired
	private AispHostNameConfig hostNameConfig;
	
	@Value("${spring.application.name}")
	private String applicationName;

	@Value("${app.fraudSystem.resUrl}")
	private String resUrl;

	@Value("${app.fraudSystem.hdmUrl}")
	private String hdmUrl;

	@Value("${app.fraudSystem.hdmInputName}")
	private String hdmInputName;

	@Value("${app.fraudSystem.jsc-file-path}")
	private String jscFilePath;
	
	@Autowired
	private AispConsentCreationDataHelper consentCreationDataHelper;
	

	@Autowired
	private TPPInformationAdaptor tppInformationAdaptor;
	
	@Autowired
	private UIStaticContentUtilityController uiController;

	@Autowired
	private RequestHeaderAttributes requestHeaders;

	@Autowired
	private HttpServletRequest request;
	
	@Autowired
	private HttpServletResponse response;

	@Autowired
	private DataMask dataMask;
	
	@Autowired
	private SandboxConfig sandboxConfig;
	
	@RequestMapping(value="/aisp/home")
	public ModelAndView homePage(Map<String, Object> model){
		SCAConsentHelper.populateSecurityHeaders(response);
		
		String homeScreen = "/customerconsentview";
		String requestUrl  = request.getRequestURI();
		requestUrl = requestUrl.replace("/home", homeScreen);
		model.put("redirectUrl",hostNameConfig.getTenantSpecificEdgeserverhost(requestHeaders.getTenantId()).concat("/").concat(applicationName).concat(requestUrl).concat("?").concat(request.getQueryString()));
		model.put(PSD2Constants.IS_SANDBOX_ENABLED,sandboxConfig.isSandboxEnabled());
		model.put(PSD2Constants.NETSCALER_TENANT_ID_HEADER_NAME, requestHeaders.getTenantId());
		return new ModelAndView("homePage", model);
    }
	
	
	/**
	 * customerconsentview end-point of consent application gets invoked by
	 * OAuth2 server with some params like - call back URI of OAuth2 server & ID
	 * token which contains the identity information of the customer. It
	 * collects the data for allowed scopes of the caller AISP & fetches
	 * accounts list of the current customer by using the user-id information
	 * taken from the ID token (SaaS Server issued ID token) which gets relayed
	 * by the OAuth server to consent app.
	 * 
	 * @param model
	 * @param oAuthUrl
	 * @param idToken
	 * @return
	 * @throws NamingException
	 */
	@RequestMapping(value = "/aisp/customerconsentview")
	public ModelAndView consentView(Map<String, Object> model) throws NamingException {
		String accountInfoList;
		boolean isConsentAccountsPresent = Boolean.FALSE;
		PickupDataModel pickupDataModel = SCAConsentHelper.populatePickupDataModel(request);

		String cmaVersion = consentCreationDataHelper.getCMAVersion(pickupDataModel.getIntentId());
		
		OBReadAccount6 consentAccounts = consentCreationDataHelper.findExistingConsentAccounts(pickupDataModel.getIntentId());
		model.put(PSD2Constants.IS_SANDBOX_ENABLED,sandboxConfig.isSandboxEnabled());
		
		if(consentAccounts != null && consentAccounts.getData() != null && !consentAccounts.getData().getAccount().isEmpty()){
			// renewal case
			LOG.info("{\"Enter\":\"{}\",\"{}\"}",
					"com.capgemini.psd2.security.consent.aisp.view.controllers.consentView()",
					loggerUtils.populateLoggerData("consentView"));
			isConsentAccountsPresent = Boolean.TRUE;
			accountInfoList = dataMask.maskResponseGenerateString(consentAccounts, "account");
		}
		else{
			LOG.info("{\"Enter\":\"{}\",\"{}\"}",
					"com.capgemini.psd2.security.consent.aisp.view.controllers.consentView()",
					loggerUtils.populateLoggerData("consentView"));
			// fresh case
			//getCMAVersion method
			OBReadAccount6 customerAccountList = consentCreationDataHelper.retrieveCustomerAccountListInfo(
					pickupDataModel.getUserId(), pickupDataModel.getClientId(),
					pickupDataModel.getIntentTypeEnum().getIntentType(), requestHeaders.getCorrelationId(),
					pickupDataModel.getChannelId(), requestHeaders.getTenantId(), pickupDataModel.getIntentId(), cmaVersion);
			
			accountInfoList = dataMask.maskResponseGenerateString(customerAccountList, "account");
		}
		if(isConsentAccountsPresent){
			model.put(PSD2SecurityConstants.REFRESH_TOKEN_RENEWAL_FLOW, Boolean.TRUE);
		}		
		String tppApplicationName = tppInformationAdaptor.fetchApplicationName(pickupDataModel.getClientId());
		Object tppInformationObj = tppInformationAdaptor.fetchTPPInformation(pickupDataModel.getClientId());

		// Added for Onward Provisioning
		String softwareOnBehalfOfOrg = fetchSoftwareOnBehalfOfOrg(pickupDataModel.getClientId());
	//	String softwareOnBehalfOfOrg = "Edge";
		model.put(PSD2SecurityConstants.SOFTWARE_ON_BEHALF_OF_ORG, softwareOnBehalfOfOrg);
		
		String setUpResponseData = consentCreationDataHelper
				.retrieveAccountRequestSetupData(pickupDataModel.getIntentId());
		model.put(PSD2SecurityConstants.CUSTOMER_ACCOUNT_LIST, accountInfoList);
		model.put(PSD2SecurityConstants.CONSENT_SETUP_DATA, setUpResponseData);
		model.put(PSD2Constants.CONSENT_FLOW_TYPE, pickupDataModel.getIntentTypeEnum().getIntentType());
		model.put(PSD2Constants.JS_MSG,
				uiController.retrieveUiParamValue(PSD2Constants.ERROR_MESSAGE, PSD2Constants.JS_MSG));
		model.put(PSD2Constants.CO_RELATION_ID, requestHeaders.getCorrelationId());
		model.put(PSD2SecurityConstants.USER_ID, pickupDataModel.getUserId());
		model.put(PSD2Constants.APPLICATION_NAME, applicationName);
		model.put(OIDCConstants.CHANNEL_ID,pickupDataModel.getChannelId());
		model.put(PSD2Constants.NETSCALER_TENANT_ID_HEADER_NAME, requestHeaders.getTenantId());
		
		if (tppInformationObj != null && tppInformationObj instanceof BasicAttributes) {
			JSONObject tppInfoJson = new JSONObject();
			tppInfoJson.put(PSD2SecurityConstants.APP_NAME, tppApplicationName);
			BasicAttributes tppInfoAttributes = (BasicAttributes) tppInformationObj;
			String legalEntityName = returnLegalEntityName(tppInfoAttributes);
			tppInfoJson.put(PSD2SecurityConstants.TPP_NAME, legalEntityName);
			requestHeaders.setTppLegalEntityName(legalEntityName);
			model.put(PSD2SecurityConstants.TPP_INFO, tppInfoJson.toString());
		}
		JSONObject fraudHdmInfo = new JSONObject();
		try {
			fraudHdmInfo.put(PSD2SecurityConstants.RES_URL, UriUtils.encode(resUrl, StandardCharsets.UTF_8.toString()));
			fraudHdmInfo.put(PSD2SecurityConstants.HDM_URL, UriUtils.encode(hdmUrl, StandardCharsets.UTF_8.toString()));
		} catch (UnsupportedEncodingException e) {
			LOG.info("UnsupportedEncodingException in consentView(): "+e);
			throw PSD2SecurityException.populatePSD2SecurityException(e.getMessage(),
					SCAConsentErrorCodeEnum.TECHNICAL_ERROR);
		}
		fraudHdmInfo.put(PSD2SecurityConstants.HDM_INPUT_NAME, hdmInputName);
		model.put(PSD2SecurityConstants.FRAUD_HDM_INFO, fraudHdmInfo.toString());
		model.put(PSD2SecurityConstants.JSC_FILE_PATH, jscFilePath);
		model.put(FraudSystemRequestMapping.FS_HEADERS, (Base64.getEncoder().encodeToString(FraudSystemUtilities.populateFraudSystemHeaders(request).getBytes())));
		model.put(PSD2Constants.UICONTENT, uiController.getConfigVariable());
		model.putAll(CdnConfig.populateCdnAttributes());
		
		//getCMAVersion
		model.put(PSD2Constants.CMAVERSION, cmaVersion);
		
		LOG.info("{\"Exit\":\"{}\",\"{}\"}",
				"com.capgemini.psd2.security.consent.aisp.view.controllers.consentView()",
				loggerUtils.populateLoggerData("consentView"));
		
		return new ModelAndView("index", model);
	}

	private String returnLegalEntityName(BasicAttributes basicAttributes) throws NamingException {
		return getAttributeValue(basicAttributes, TPPInformationConstants.LEGAL_ENTITY_NAME);
	}

	private String fetchSoftwareOnBehalfOfOrg(String clientId) throws NamingException {
		if(clientId!=null && !clientId.isEmpty()) {
			return tppInformationAdaptor.fetchSoftwareOnBehalfOfOrg(clientId);
		}
		return null;
	}

	private String getAttributeValue(BasicAttributes tppApplication, String ldapAttr) throws NamingException {
		String attributeValue = null;
		if (tppApplication.get(ldapAttr) != null && tppApplication.get(ldapAttr).get() != null) {
			attributeValue = tppApplication.get(ldapAttr).get().toString();
		}
		return attributeValue;
	}
}