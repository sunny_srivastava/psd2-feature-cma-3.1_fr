package com.capgemini.psd2.security.consent.aisp.helpers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.boot.context.properties.ConfigurationProperties;

import com.capgemini.psd2.aisp.domain.OBAccount6;
import com.capgemini.psd2.aisp.domain.OBAccount6Account;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AispConsent;
import com.capgemini.psd2.consent.domain.PSD2Account;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.scaconsenthelper.config.helpers.ConsentAuthorizationHelper;
import com.capgemini.psd2.utilities.StringUtils;

@ConfigurationProperties("app")
public class AispConsentAuthorizationHelper extends ConsentAuthorizationHelper {

	private Map<String, String> consentSupportedSchemeMap = new HashMap<>();

	public Map<String, String> getConsentSupportedSchemeMap() {
		return consentSupportedSchemeMap;
	}

	@Override
	public PSD2Account populateAccountwithUnmaskedValues(PSD2Account accountwithMask, PSD2Account accountwithoutMask) {

		Map<String, String> additionalInfo = accountwithoutMask.getAdditionalInformation();
		Map<String, String> additionalInfotobeUnmasked = accountwithMask.getAdditionalInformation();

		OBAccount6Account maskAccount = accountwithMask.getAccount().get(0);
		if (accountwithoutMask.getAccount() != null) {
			String dbSchemeName = consentSupportedSchemeMap.get(maskAccount.getSchemeName());
			String schemeIdentification = accountwithoutMask.getAdditionalInformation()
					.get(consentSupportedSchemeMap.get(dbSchemeName));

			maskAccount.setIdentification(schemeIdentification);
		}

		/*
		 * Setting (unmasked) account-number and account-nsc from channel
		 * profile additional info
		 */
		if (additionalInfo != null && additionalInfotobeUnmasked != null) {
			additionalInfotobeUnmasked.put(PSD2Constants.ACCOUNT_NUMBER,
					additionalInfo.get(PSD2Constants.ACCOUNT_NUMBER));
			additionalInfotobeUnmasked.put(PSD2Constants.ACCOUNT_NSC, additionalInfo.get(PSD2Constants.ACCOUNT_NSC));
		}
		accountwithMask.getAccount().get(0).setIdentification(accountwithoutMask.getAccount().get(0).getIdentification());
		return accountwithMask;
	}

	public <T> List<OBAccount6> populateAccountListFromAccountDetails(T input) {
		List<AccountDetails> accountDetails = ((AispConsent) input).getAccountDetails();
		List<OBAccount6> accountList = new ArrayList<>();
		for (AccountDetails accountDetail : accountDetails) {
			PSD2Account account = new PSD2Account();
			account.setCurrency(accountDetail.getCurrency());
			account.setNickname(accountDetail.getNickname());
			account.setAccountId(accountDetail.getAccountId());
			account.setAccountSubType(accountDetail.getAccountSubType());
			OBAccount6Account obAccount2Account = accountDetail.getAccount();
			List<OBAccount6Account> obAccount2AccountList = new ArrayList<>();
			obAccount2AccountList.add(obAccount2Account);
			account.setAccount(obAccount2AccountList);
			account.setServicer(accountDetail.getServicer());
			account.setHashedValue(StringUtils.generateHashedValue(accountDetail.getAccount().getIdentification()));
			accountList.add(account);
		}
		return accountList;

	}

}
