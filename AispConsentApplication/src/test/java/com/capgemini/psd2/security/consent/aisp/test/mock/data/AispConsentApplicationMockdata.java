package com.capgemini.psd2.security.consent.aisp.test.mock.data;

import java.util.ArrayList;
import java.util.List;

import com.capgemini.psd2.aisp.domain.OBAccount6;
import com.capgemini.psd2.aisp.domain.OBAccount6Account;
import com.capgemini.psd2.aisp.domain.OBBranchAndFinancialInstitutionIdentification50;
import com.capgemini.psd2.aisp.domain.OBReadAccount6;
import com.capgemini.psd2.aisp.domain.OBReadAccount6Data;
import com.capgemini.psd2.aisp.domain.OBReadConsentResponse1;
import com.capgemini.psd2.aisp.domain.OBReadConsentResponse1Data;
import com.capgemini.psd2.consent.domain.CustomerAccountInfo;
import com.capgemini.psd2.consent.domain.PSD2Account;

public class AispConsentApplicationMockdata {

	public static OBReadConsentResponse1 mockAccountRequestPOSTResponse;
	public static List<CustomerAccountInfo> mockCustomerAccountInfoList;
	public static OBReadConsentResponse1 getAccountRequestPOSTResponse() {
		mockAccountRequestPOSTResponse = new OBReadConsentResponse1();

		OBReadConsentResponse1Data data = new OBReadConsentResponse1Data();
		List<OBReadConsentResponse1Data.PermissionsEnum> permissions = new ArrayList<>();
		permissions.add(OBReadConsentResponse1Data.PermissionsEnum.READACCOUNTSBASIC);
		permissions.add(OBReadConsentResponse1Data.PermissionsEnum.READACCOUNTSDETAIL);
		permissions.add(OBReadConsentResponse1Data.PermissionsEnum.READBALANCES);
		data.setPermissions(permissions);
		data.setCreationDateTime("2018-12-25T00:00:00-00:00");
		data.setExpirationDateTime("2018-05-02T00:00:00-00:00");
		data.setTransactionFromDateTime("2018-05-03T00:00:00-00:00");
		data.setTransactionToDateTime("2018-12-03T00:00:00-00:00");
		data.consentId("af5b90c1-64b5-4a52-ba55-5eed68b2a269");
		data.setStatus(OBReadConsentResponse1Data.StatusEnum.AWAITINGAUTHORISATION);
		mockAccountRequestPOSTResponse.setData(data);
		mockAccountRequestPOSTResponse.setRisk(null);
		return mockAccountRequestPOSTResponse;
	}

	public static OBReadConsentResponse1 getAccountRequestPOSTResponseInvalidStatus() {
		OBReadConsentResponse1 mockAccountRequestPOSTResponse = new OBReadConsentResponse1();

		OBReadConsentResponse1Data data = new OBReadConsentResponse1Data();
		List<OBReadConsentResponse1Data.PermissionsEnum> permissions = new ArrayList<>();
		permissions.add(OBReadConsentResponse1Data.PermissionsEnum.READACCOUNTSBASIC);
		permissions.add(OBReadConsentResponse1Data.PermissionsEnum.READACCOUNTSDETAIL);
		permissions.add(OBReadConsentResponse1Data.PermissionsEnum.READBALANCES);
		data.setPermissions(permissions);
		data.setCreationDateTime("2017-12-25T00:00:00-00:00");
		data.setExpirationDateTime("2017-05-02T00:00:00-00:00");
		data.setTransactionFromDateTime("2017-05-03T00:00:00-00:00");
		data.setTransactionToDateTime("2017-12-03T00:00:00-00:00");
		data.consentId("af5b90c1-64b5-4a52-ba55-5eed68b2a269");
		data.setStatus(OBReadConsentResponse1Data.StatusEnum.AUTHORISED);
		mockAccountRequestPOSTResponse.setData(data);
		mockAccountRequestPOSTResponse.setRisk(null);
		return mockAccountRequestPOSTResponse;
	}

	public static List<CustomerAccountInfo> getCustomerAccountInfoList() {
		mockCustomerAccountInfoList = new ArrayList<>();
		CustomerAccountInfo customerAccountInfo1 = new CustomerAccountInfo();
		customerAccountInfo1.setAccountName("John Doe");
		customerAccountInfo1.setUserId("1234");
		customerAccountInfo1.setAccountNumber("10203345");
		customerAccountInfo1.setAccountNSC("SC802001");
		customerAccountInfo1.setCurrency("EUR");
		customerAccountInfo1.setNickname("John");
		customerAccountInfo1.setAccountType("checking");

		CustomerAccountInfo customerAccountInfo2 = new CustomerAccountInfo();
		customerAccountInfo2.setAccountName("Tiffany Doe");
		customerAccountInfo2.setUserId("1234");
		customerAccountInfo2.setAccountNumber("10203346");
		customerAccountInfo2.setAccountNSC("SC802002");
		customerAccountInfo2.setCurrency("GRP");
		customerAccountInfo2.setNickname("Tiffany");
		customerAccountInfo2.setAccountType("savings");

		mockCustomerAccountInfoList.add(customerAccountInfo1);
		mockCustomerAccountInfoList.add(customerAccountInfo2);

		return mockCustomerAccountInfoList;
	}

	public static List<PSD2Account> getCustomerAccounts() {
		List<PSD2Account> mockCustomerAccountList = new ArrayList<>();
		PSD2Account customerAccountInfo1 = new PSD2Account();
		List<OBAccount6Account> accountList = new ArrayList<>();
		OBAccount6Account account = new OBAccount6Account();
		account.setIdentification("12345");
		account.setSchemeName("IBAN");
		accountList.add(account);
		OBBranchAndFinancialInstitutionIdentification50 servicer = new OBBranchAndFinancialInstitutionIdentification50();
		servicer.setIdentification("12345");
		customerAccountInfo1.setServicer(servicer);
		customerAccountInfo1.setAccount(accountList);
		customerAccountInfo1.setCurrency("EUR");
		customerAccountInfo1.setNickname("John");
		//customerAccountInfo1.setHashedValue();
		PSD2Account customerAccountInfo2 = new PSD2Account();

		customerAccountInfo2.setCurrency("EUR");
		customerAccountInfo2.setNickname("John");
		// customerAccountInfo2.setHashedValue();
		customerAccountInfo2.setAccount(accountList);
		customerAccountInfo2.setServicer(servicer);

		mockCustomerAccountList.add(customerAccountInfo1);
		mockCustomerAccountList.add(customerAccountInfo2);
		return mockCustomerAccountList;
	}

	public static OBReadAccount6 getCustomerAccountInfo() {
		OBReadAccount6 mockOBReadAccount6 = new OBReadAccount6();
		List<OBAccount6> accountData = new ArrayList<>();
		PSD2Account acct = new PSD2Account();
		acct.setAccountId("14556236");
		acct.setCurrency("EUR");
		acct.setNickname("John");
		// acct.setHashedValue();
		//acct.setAccountType("savings");
		List<OBAccount6Account> accountList = new ArrayList<>();
		OBAccount6Account account = new OBAccount6Account();
		account.setIdentification("12345");
		account.setSchemeName("IBAN");
		accountList.add(account);
		OBBranchAndFinancialInstitutionIdentification50 servicer = new OBBranchAndFinancialInstitutionIdentification50();
		acct.setAccount(accountList);
		acct.setServicer(servicer);

		PSD2Account accnt = new PSD2Account();
		accnt.setAccountId("14556236");
		accnt.setCurrency("EUR");
		accnt.setNickname("John");
		// accnt.setHashedValue();
		accnt.setServicer(servicer);
		accnt.setAccount(accountList);
		//accnt.setAccountType("savings");
		accountData.add(acct);
		accountData.add(accnt);
		OBReadAccount6Data data2 = new OBReadAccount6Data();
		data2.setAccount(accountData);
		account.setSchemeName("IBAN");
		mockOBReadAccount6.setData(data2);

		return mockOBReadAccount6;
	}

	public static OBReadAccount6 getCustomerAccountData() {
		OBReadAccount6 mockOBReadAccount6 = new OBReadAccount6();
		List<OBAccount6> accountData = new ArrayList<>();
		PSD2Account acct = new PSD2Account();
		acct.setAccountId("14556236");
		acct.setCurrency("EUR");
		acct.setNickname("John");
		//acct.setAccountType("savings");
		List<OBAccount6Account> accountList = new ArrayList<>();
		OBAccount6Account account = new OBAccount6Account();
		account.setIdentification("12345");
		account.setSchemeName("IBAN");
		accountList.add(account);
		OBBranchAndFinancialInstitutionIdentification50 servicer = new OBBranchAndFinancialInstitutionIdentification50();
		servicer.setIdentification("12345");
		acct.setAccount(accountList);
		acct.setServicer(servicer);

		PSD2Account accnt = new PSD2Account();
		accnt.setAccountId("14556236");
		accnt.setCurrency("EUR");
		accnt.setNickname("John");
		accnt.setServicer(servicer);
		accnt.setAccount(accountList);
		//accnt.setAccountType("savings");
		accountData.add(acct);
		accountData.add(accnt);
		OBReadAccount6Data data2 = new OBReadAccount6Data();
		data2.setAccount(accountData);
		mockOBReadAccount6.setData(data2);

		return mockOBReadAccount6;
	}

	public static List<PSD2Account> getCustomerAccountsNew() {
		List<PSD2Account> mockCustomerAccountList = new ArrayList<>();
		PSD2Account customerAccountInfo1 = new PSD2Account();
		List<OBAccount6Account> accountList = new ArrayList<>();
		OBAccount6Account account = new OBAccount6Account();
		account.setIdentification("12345");
		account.setSchemeName("IBAN");
		accountList.add(account);
		OBBranchAndFinancialInstitutionIdentification50 servicer = new OBBranchAndFinancialInstitutionIdentification50();
		servicer.setIdentification("12345");
		customerAccountInfo1.setServicer(servicer);
		customerAccountInfo1.setAccount(accountList);
		customerAccountInfo1.setCurrency("EUR");
		customerAccountInfo1.setNickname("John");
		customerAccountInfo1.setHashedValue("123");
		PSD2Account customerAccountInfo2 = new PSD2Account();

		customerAccountInfo2.setCurrency("EUR");
		customerAccountInfo2.setNickname("John");
		customerAccountInfo2.setAccount(accountList);
		customerAccountInfo2.setHashedValue("123");
		customerAccountInfo2.setServicer(servicer);

		mockCustomerAccountList.add(customerAccountInfo1);
		mockCustomerAccountList.add(customerAccountInfo2);
		return mockCustomerAccountList;
	}

	public static OBReadAccount6 getCustomerAccountInfoNew() {
		OBReadAccount6 mockOBReadAccount6 = new OBReadAccount6();
		List<OBAccount6> accountData = new ArrayList<>();
		PSD2Account acct = new PSD2Account();
		acct.setAccountId("14556236");
		acct.setCurrency("EUR");
		acct.setNickname("John");
		List<OBAccount6Account> accountList = new ArrayList<>();
		OBAccount6Account account = new OBAccount6Account();
		account.setIdentification("12345");
		account.setSchemeName("IBAN");
		accountList.add(account);
		OBBranchAndFinancialInstitutionIdentification50 servicer = new OBBranchAndFinancialInstitutionIdentification50();
		servicer.setIdentification("12345");
		acct.setAccount(accountList);
		acct.setHashedValue("123");
		acct.setServicer(servicer);

		PSD2Account accnt = new PSD2Account();
		accnt.setAccountId("14556236");
		accnt.setCurrency("EUR");
		accnt.setNickname("John");
		accnt.setServicer(servicer);
		accnt.setAccount(accountList);
		accnt.setHashedValue("123");
		accountData.add(acct);
		accountData.add(accnt);
		OBReadAccount6Data data2 = new OBReadAccount6Data();
		data2.setAccount(accountData);
		account.setSchemeName("IBAN");
		mockOBReadAccount6.setData(data2);

		return mockOBReadAccount6;
	}

}
