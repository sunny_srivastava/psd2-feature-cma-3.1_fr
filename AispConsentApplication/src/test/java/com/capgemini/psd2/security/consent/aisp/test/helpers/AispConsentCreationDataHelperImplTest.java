package com.capgemini.psd2.security.consent.aisp.test.helpers;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyMap;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.naming.NamingException;
import javax.naming.directory.BasicAttributes;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.aisp.adapter.AccountRequestAdapter;
import com.capgemini.psd2.aisp.adapter.AispConsentAdapter;
import com.capgemini.psd2.aisp.adapter.CustomerAccountListAdapter;
import com.capgemini.psd2.aisp.domain.OBAccount6;
import com.capgemini.psd2.aisp.domain.OBAccount6Account;
import com.capgemini.psd2.aisp.domain.OBBranchAndFinancialInstitutionIdentification50;
import com.capgemini.psd2.aisp.domain.OBReadAccount6;
import com.capgemini.psd2.aisp.domain.OBReadConsentResponse1;
import com.capgemini.psd2.aisp.domain.OBReadConsentResponse1Data;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AispConsent;
import com.capgemini.psd2.consent.domain.PSD2Account;
import com.capgemini.psd2.enums.ConsentStatusEnum;
import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.OBPSD2ExceptionUtility;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.fraudsystem.helper.FraudSystemHelper;
import com.capgemini.psd2.logger.LoggerUtils;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.scaconsenthelper.config.helpers.SCAConsentAdapterHelper;
import com.capgemini.psd2.security.consent.aisp.helpers.AispConsentCreationDataHelperImpl;
import com.capgemini.psd2.tppinformation.adaptor.ldap.constants.TPPInformationConstants;

import junit.framework.Assert;

@RunWith(SpringJUnit4ClassRunner.class)
public class AispConsentCreationDataHelperImplTest {

	@Mock
	private LoggerUtils loggerUtils;
	
	@Mock
	private SCAConsentAdapterHelper scaConsentAdapterHelper;

	@Mock
	private AispConsentAdapter aispConsentAdapter;

	@Mock
	private CustomerAccountListAdapter customerAccountListAdapter;

	@Mock
	private RequestHeaderAttributes reqAttributes;

	@Mock
	private FraudSystemHelper fraudSystemHelper;

	@InjectMocks
	private AispConsentCreationDataHelperImpl consentCreationDataHelperImpl = new AispConsentCreationDataHelperImpl();

	@Before
	public void setUp() {
		Map<String, String> map=new HashMap<>();
		map.put("SIGNATURE", "Something wrong with x-jws-signature request header");
		Map<String, String> specificErrorMessageMap=new HashMap<>();
		specificErrorMessageMap.put("signature_invalid_content", "Invalid content prsent in joseheader of x-jws-signature header");
		specificErrorMessageMap.put("signature_missing", "signature header missing in request");
		OBPSD2ExceptionUtility.genericErrorMessages.putAll(map);
		OBPSD2ExceptionUtility.specificErrorMessages.putAll(specificErrorMessageMap);
		MockitoAnnotations.initMocks(this);
		Map<String, String> paramMap = new HashMap<>();
		paramMap.put("AISP", "AISP");
		paramMap.put("client_id", "client123");
		paramMap.put("AccountRequestId", "accReqId123");
	}

	@Test
	public void testRetrieveAccountRequestSetupDataAISPCrationDateBeforeException() {
		OBReadConsentResponse1 accountRequestPOSTResponse = new OBReadConsentResponse1();
		OBReadConsentResponse1Data data = new OBReadConsentResponse1Data();
		data.setCreationDateTime("2017-02-25T00:00:00-00:00");
		accountRequestPOSTResponse.data(data);
		when(scaConsentAdapterHelper.getAccountRequestSetupData(anyString())).thenReturn(accountRequestPOSTResponse);
		consentCreationDataHelperImpl.retrieveAccountRequestSetupData("dummy");
	}

	@Test(expected = PSD2Exception.class)
	public void testRetrieveAccountRequestSetupForException() {
		OBReadConsentResponse1 accountRequestPOSTResponse = new OBReadConsentResponse1();
		OBReadConsentResponse1Data data = new OBReadConsentResponse1Data();
		data.setCreationDateTime("2017-02-25T00:00:00-00:00");
		accountRequestPOSTResponse.data(data);
		when(scaConsentAdapterHelper.getAccountRequestSetupData(anyString())).thenThrow(PSD2Exception.populatePSD2Exception(ErrorCodeEnum.NO_ACCOUNT_DETAILS_FOUND));
		consentCreationDataHelperImpl.retrieveAccountRequestSetupData("dummy");
	}

	@Test
	public void testCreateConsent() throws NamingException {
		doNothing().when(aispConsentAdapter).createConsent(any(AispConsent.class));
		when(scaConsentAdapterHelper.getAccountRequestSetupData(anyString()))
				.thenReturn(getAccountRequestPOSTResponseInvalidStatus());

		doNothing().when(fraudSystemHelper).captureFraudEvent(anyObject(), anyObject(), anyObject());

		List<OBAccount6> acctList = new ArrayList<>();

		PSD2Account customerAccountInfo1 = new PSD2Account();
		List<OBAccount6Account> accountList = new ArrayList<>();
		OBAccount6Account account = new OBAccount6Account();
		account.setIdentification("IE29AIBK93115212345678");
		account.setSchemeName("UK.OBIE.IBAN");
		accountList.add(account);
		OBBranchAndFinancialInstitutionIdentification50 servicer = new OBBranchAndFinancialInstitutionIdentification50();
		servicer.setIdentification("123456");
		customerAccountInfo1.setServicer(servicer);
		customerAccountInfo1.setAccount(accountList);
		customerAccountInfo1.setCurrency("EUR");
		customerAccountInfo1.setNickname("John");
		customerAccountInfo1.setHashedValue("123");
		PSD2Account customerAccountInfo2 = new PSD2Account();

		customerAccountInfo2.setCurrency("EUR");
		customerAccountInfo2.setNickname("John");
		customerAccountInfo2.setAccount(accountList);
		customerAccountInfo2.setHashedValue("123");
		customerAccountInfo2.setServicer(servicer);
		Map<String, String> additionalInformation = new HashMap<>();
		additionalInformation.put(PSD2Constants.ACCOUNT_NSC, "123");
		additionalInformation.put(PSD2Constants.ACCOUNT_NUMBER, "456");
		customerAccountInfo1.setAdditionalInformation(additionalInformation);
		customerAccountInfo2.setAdditionalInformation(additionalInformation);
		acctList.add(customerAccountInfo1);
		acctList.add(customerAccountInfo2);
		
		BasicAttributes tppInformation = new BasicAttributes();
		tppInformation.put(TPPInformationConstants.LEGAL_ENTITY_NAME, new String("abc"));

		consentCreationDataHelperImpl.createConsent(acctList, null, null, null, null, "eC1mYXBpLWZpbmFuY2lhbElk", null,
				tppInformation, null);
	}

	@Test
	public void testCreateConsent4() throws NamingException {
		doNothing().when(aispConsentAdapter).createConsent(any(AispConsent.class));
		when(scaConsentAdapterHelper.getAccountRequestSetupData(anyString()))
				.thenReturn(getAccountRequestPOSTResponseInvalidStatus());

		doNothing().when(fraudSystemHelper).captureFraudEvent(anyObject(), anyObject(), anyObject());

		List<OBAccount6> acctList = new ArrayList<>();

		PSD2Account customerAccountInfo1 = new PSD2Account();

		List<OBAccount6Account> accountList = new ArrayList<>();
		OBAccount6Account account = new OBAccount6Account();
		account.setIdentification("IE29AIBK93115212345678");
		account.setSchemeName("IBAN");
		OBBranchAndFinancialInstitutionIdentification50 servicer = new OBBranchAndFinancialInstitutionIdentification50();
		servicer.setIdentification("123456");
		customerAccountInfo1.setServicer(servicer);
		accountList.add(account);
		customerAccountInfo1.setAccount(accountList);
		customerAccountInfo1.setCurrency("EUR");
		customerAccountInfo1.setNickname("John");
		customerAccountInfo1.setHashedValue("123");
		PSD2Account customerAccountInfo2 = new PSD2Account();

		customerAccountInfo2.setCurrency("EUR");
		customerAccountInfo2.setNickname("John");
		customerAccountInfo2.setAccount(accountList);
		customerAccountInfo2.setHashedValue("123");
		customerAccountInfo2.setServicer(servicer);
		Map<String, String> additionalInformation = new HashMap<>();
		additionalInformation.put(PSD2Constants.ACCOUNT_NSC, "123");
		additionalInformation.put(PSD2Constants.ACCOUNT_NUMBER, "456");
		customerAccountInfo1.setAdditionalInformation(additionalInformation);
		customerAccountInfo2.setAdditionalInformation(additionalInformation);
		acctList.add(customerAccountInfo1);
		acctList.add(customerAccountInfo2);

		consentCreationDataHelperImpl.createConsent(acctList, null, null, null, null, "eC1mYXBpLWZpbmFuY2lhbElk", null,
				null,null);
	}

	@Test(expected = Exception.class)
	public void testCreateConsentBranches() throws NamingException {
		doNothing().when(aispConsentAdapter).createConsent(any(AispConsent.class));
		when(scaConsentAdapterHelper.getAccountRequestSetupData(anyString()))
				.thenReturn(getAccountRequestPOSTResponseInvalidStatus());

		doNothing().when(fraudSystemHelper).captureFraudEvent(anyObject(), anyObject(), anyObject());

		List<OBAccount6> acctList = new ArrayList<>();

		PSD2Account customerAccountInfo1 = new PSD2Account();
		List<OBAccount6Account> accountList = new ArrayList<>();
		OBAccount6Account account = new OBAccount6Account();
		account.setIdentification("12345");
		account.setSchemeName("SortCodeAccountNumber");
		accountList.add(account);
		OBBranchAndFinancialInstitutionIdentification50 servicer = new OBBranchAndFinancialInstitutionIdentification50();
		servicer.setIdentification("12345678901234");
		customerAccountInfo1.setServicer(servicer);
		customerAccountInfo1.setAccount(accountList);
		customerAccountInfo1.setCurrency("EUR");
		customerAccountInfo1.setNickname("John");
		customerAccountInfo1.setHashedValue("123");
		PSD2Account customerAccountInfo2 = new PSD2Account();

		customerAccountInfo2.setCurrency("EUR");
		customerAccountInfo2.setNickname("John");
		customerAccountInfo2.setAccount(accountList);
		customerAccountInfo2.setHashedValue("123");
		customerAccountInfo2.setServicer(servicer);
		Map<String, String> additionalInformation = new HashMap<>();
		additionalInformation.put(PSD2Constants.ACCOUNT_NSC, "123");
		additionalInformation.put(PSD2Constants.ACCOUNT_NUMBER, "456");
		customerAccountInfo1.setAdditionalInformation(additionalInformation);
		customerAccountInfo2.setAdditionalInformation(additionalInformation);
		acctList.add(customerAccountInfo1);
		acctList.add(customerAccountInfo2);

		consentCreationDataHelperImpl.createConsent(acctList, null, null, null, null, null, null, null,null);
	}

	@Test
	public void testCreateConsentBranches3() throws NamingException {
		doNothing().when(aispConsentAdapter).createConsent(any(AispConsent.class));
		when(scaConsentAdapterHelper.getAccountRequestSetupData(anyString()))
				.thenReturn(getAccountRequestPOSTResponseInvalidStatus());

		doNothing().when(fraudSystemHelper).captureFraudEvent(anyObject(), anyObject(), anyObject());

		List<OBAccount6> acctList = new ArrayList<>();

		PSD2Account customerAccountInfo1 = new PSD2Account();

		List<OBAccount6Account> accountList = new ArrayList<>();
		OBAccount6Account account = new OBAccount6Account();
		account.setIdentification("12345678901234");
		account.setSchemeName("SortCodeAccountNumber");
		OBBranchAndFinancialInstitutionIdentification50 servicer = new OBBranchAndFinancialInstitutionIdentification50();
		servicer.setIdentification("12345678901234");
		accountList.add(account);
		customerAccountInfo1.setServicer(servicer);
		customerAccountInfo1.setAccount(accountList);
		customerAccountInfo1.setCurrency("EUR");
		customerAccountInfo1.setNickname("John");
		customerAccountInfo1.setHashedValue("123");
		PSD2Account customerAccountInfo2 = new PSD2Account();

		customerAccountInfo2.setCurrency("EUR");
		customerAccountInfo2.setNickname("John");
		customerAccountInfo2.setAccount(accountList);
		customerAccountInfo2.setHashedValue("123");
		customerAccountInfo2.setServicer(servicer);
		Map<String, String> additionalInformation = new HashMap<>();
		additionalInformation.put(PSD2Constants.ACCOUNT_NSC, "123");
		additionalInformation.put(PSD2Constants.ACCOUNT_NUMBER, "456");
		customerAccountInfo1.setAdditionalInformation(additionalInformation);
		customerAccountInfo2.setAdditionalInformation(additionalInformation);
		acctList.add(customerAccountInfo1);
		acctList.add(customerAccountInfo2);

		consentCreationDataHelperImpl.createConsent(acctList, null, null, null, null, "eC1mYXBpLWZpbmFuY2lhbElk", null,
				null,null);
	}

	@SuppressWarnings("unchecked")
	@Test
	public void testRetrieveCustomerAccountListInfoSuccessFlow() {
		// when(securityRequestAttributes.getFlowType()).thenReturn("AISP");
		when(reqAttributes.getCorrelationId()).thenReturn("123445");
		when(customerAccountListAdapter.retrieveCustomerAccountList(anyString(), anyMap()))
				.thenReturn(new OBReadAccount6());
		when(scaConsentAdapterHelper.getAccountRequestSetupData(anyString()))
		.thenReturn(getAccountRequestPOSTResponseInvalidStatus());
		consentCreationDataHelperImpl.retrieveCustomerAccountListInfo(null, null, null, null, null, null, null, null);
	}

	@Test
	public void testCancelAccountRequest() {
		when(aispConsentAdapter.retrieveConsentByAccountRequestIdAndStatus(anyString(), any(ConsentStatusEnum.class)))
				.thenReturn(new AispConsent());
		doNothing().when(aispConsentAdapter).updateConsentStatus(anyString(), any(ConsentStatusEnum.class));
		when(scaConsentAdapterHelper.updateAccountRequestSetupData(anyString(), any(OBReadConsentResponse1Data.StatusEnum.class)))
				.thenReturn(new OBReadConsentResponse1());

		consentCreationDataHelperImpl.cancelAccountRequest("");
	}
	
	
	public static List<PSD2Account> getCustomerAccountsNew() {
		List<PSD2Account> mockCustomerAccountList = new ArrayList<>();
		PSD2Account customerAccountInfo1 = new PSD2Account();
		List<OBAccount6Account> accountList = new ArrayList<>();
		OBAccount6Account account = new OBAccount6Account();
		account.setIdentification("12345");
		account.setSchemeName("IBAN");
		accountList.add(account);
		OBBranchAndFinancialInstitutionIdentification50 servicer = new OBBranchAndFinancialInstitutionIdentification50();
		servicer.setIdentification("12345");
		customerAccountInfo1.setServicer(servicer);
		customerAccountInfo1.setAccount(accountList);
		customerAccountInfo1.setCurrency("EUR");
		customerAccountInfo1.setNickname("John");
		customerAccountInfo1.setHashedValue("123");
		PSD2Account customerAccountInfo2 = new PSD2Account();

		customerAccountInfo2.setCurrency("EUR");
		customerAccountInfo2.setNickname("John");
		customerAccountInfo2.setAccount(accountList);
		customerAccountInfo2.setHashedValue("123");
		customerAccountInfo2.setServicer(servicer);

		mockCustomerAccountList.add(customerAccountInfo1);
		mockCustomerAccountList.add(customerAccountInfo2);
		return mockCustomerAccountList;
	}
	
	
	public static OBReadConsentResponse1 getAccountRequestPOSTResponseInvalidStatus() {
		OBReadConsentResponse1 mockAccountRequestPOSTResponse = new OBReadConsentResponse1();

		OBReadConsentResponse1Data data = new OBReadConsentResponse1Data();
		List<OBReadConsentResponse1Data.PermissionsEnum> permissions = new ArrayList<>();
		permissions.add(OBReadConsentResponse1Data.PermissionsEnum.READACCOUNTSBASIC);
		permissions.add(OBReadConsentResponse1Data.PermissionsEnum.READACCOUNTSDETAIL);
		permissions.add(OBReadConsentResponse1Data.PermissionsEnum.READBALANCES);
		data.setPermissions(permissions);
		data.setCreationDateTime("2017-12-25T00:00:00-00:00");
		data.setExpirationDateTime("2017-05-02T00:00:00-00:00");
		data.setTransactionFromDateTime("2017-05-03T00:00:00-00:00");
		data.setTransactionToDateTime("2017-12-03T00:00:00-00:00");
		data.consentId("af5b90c1-64b5-4a52-ba55-5eed68b2a269");
		data.setStatus(OBReadConsentResponse1Data.StatusEnum.AUTHORISED);
		mockAccountRequestPOSTResponse.setData(data);
		mockAccountRequestPOSTResponse.setRisk(null);
		return mockAccountRequestPOSTResponse;
	}
	
	@Test
	public void findExistingConsentAccounts() {

		AispConsent aispConsent = new AispConsent();
		aispConsent.setAccountDetails(new ArrayList<AccountDetails>());
		when(aispConsentAdapter.retrieveConsentByAccountRequestIdAndStatus(anyString(), any(ConsentStatusEnum.class)))
				.thenReturn(aispConsent);
		consentCreationDataHelperImpl.findExistingConsentAccounts("");
	}
	
	@Test(expected=PSD2Exception.class)
	public void getAccountRequestPOSTResponseNullIntentId() {
		OBReadConsentResponse1 accountRequestPOSTResponse = new OBReadConsentResponse1();
		OBReadConsentResponse1Data data = new OBReadConsentResponse1Data();
		data.setCreationDateTime("2017-02-25T00:00:00-00:00");
		when(scaConsentAdapterHelper.getAccountRequestSetupData(anyString())).thenThrow(PSD2Exception.populatePSD2Exception(ErrorCodeEnum.NO_ACCOUNT_DETAILS_FOUND));
		consentCreationDataHelperImpl.getCMAVersion("af5b90c1-64b5-4a52-ba55-5eed68b2a269");
	}
	
	@Test(expected=PSD2Exception.class)
	public void cancelAccountRequestWhenConsentStatusRevoked() {
		AispConsent aispConsent = new AispConsent();
		aispConsent.setAccountDetails(new ArrayList<AccountDetails>());
		when(aispConsentAdapter.retrieveConsentByAccountRequestId(anyString())).thenReturn(aispConsent);
		aispConsent.setStatus(ConsentStatusEnum.REVOKED);
		consentCreationDataHelperImpl.cancelAccountRequest("af5b90c1-64b5-4a52-ba55-5eed68b2a269");
	}
	
	@Test(expected=PSD2Exception.class)
	public void cancelAccountRequestWhenConsentStatusDeleted() {
		AispConsent aispConsent = new AispConsent();
		aispConsent.setAccountDetails(new ArrayList<AccountDetails>());
		when(aispConsentAdapter.retrieveConsentByAccountRequestId(anyString())).thenReturn(aispConsent);
		aispConsent.setStatus(ConsentStatusEnum.DELETED);
		consentCreationDataHelperImpl.cancelAccountRequest("af5b90c1-64b5-4a52-ba55-5eed68b2a269");
	}
	
	@Test
	public void getAccountRequestPOSTResponseConsentStatusAwaitingAuthorization() {
		AispConsent aispConsent = new AispConsent();
		aispConsent.setAccountDetails(new ArrayList<AccountDetails>());
		when(aispConsentAdapter.retrieveConsentByAccountRequestId(anyString())).thenReturn(aispConsent);
		aispConsent.setStatus(ConsentStatusEnum.AWAITINGAUTHORISATION);
		consentCreationDataHelperImpl.cancelAccountRequest("af5b90c1-64b5-4a52-ba55-5eed68b2a269");
	}
	/*
	@Test
	public void getAccountRequestPOSTResponseConsentStatusAwaitingAuthorization() {
		AispConsent aispConsent = new AispConsent();
		aispConsent.setAccountDetails(new ArrayList<AccountDetails>());
		when(aispConsentAdapter.retrieveConsentByAccountRequestId(anyString())).thenReturn(aispConsent);
		aispConsent.setStatus(ConsentStatusEnum.AWAITINGAUTHORISATION);
		consentCreationDataHelperImpl.cancelAccountRequest("af5b90c1-64b5-4a52-ba55-5eed68b2a269");
		Assert.assertEquals(ConsentStatusEnum.REJECTED, aispConsent.getStatus());
	}
	*/
	
	
	
}
