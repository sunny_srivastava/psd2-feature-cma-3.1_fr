package com.capgemini.psd2.security.consent.aisp.test.helpers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.aisp.domain.OBAccount6Account;
import com.capgemini.psd2.aisp.domain.OBBranchAndFinancialInstitutionIdentification50;
import com.capgemini.psd2.aisp.domain.OBExternalAccountSubType1Code;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AispConsent;
import com.capgemini.psd2.consent.domain.PSD2Account;
import com.capgemini.psd2.security.consent.aisp.helpers.AispConsentAuthorizationHelper;

@RunWith(SpringJUnit4ClassRunner.class)
public class AispConsentAuthorizationHelperTest {

	@InjectMocks
	AispConsentAuthorizationHelper aispHelper;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void populateAccountWithUnmaskedValuesTest() {
		Map<String, String> map = new HashMap<>();

		PSD2Account accountwithoutmask = new PSD2Account();
		List<OBAccount6Account> acct = new ArrayList<>();
		OBAccount6Account account = new OBAccount6Account();
		account.setIdentification("12345");
		account.setSchemeName("IBAN");
		acct.add(account);
		OBBranchAndFinancialInstitutionIdentification50 servicer = new OBBranchAndFinancialInstitutionIdentification50();
		servicer.setIdentification("12345");
		accountwithoutmask.setServicer(servicer);
		accountwithoutmask.setAccount(acct);
		accountwithoutmask.setCurrency("EUR");
		accountwithoutmask.setNickname("John");
		accountwithoutmask.setAdditionalInformation(map);

		PSD2Account accountwithmask = new PSD2Account();
		OBAccount6Account account1 = new OBAccount6Account();
		account1.setIdentification("12345");
		account1.setSchemeName("IBAN");
		OBBranchAndFinancialInstitutionIdentification50 servicer1 = new OBBranchAndFinancialInstitutionIdentification50();
		servicer1.setIdentification("12345");
		accountwithmask.setServicer(servicer);
		accountwithmask.setAccount(acct);
		accountwithmask.setCurrency("EUR");
		accountwithmask.setNickname("John");
		accountwithmask.setAdditionalInformation(map);

		aispHelper.populateAccountwithUnmaskedValues(accountwithmask, accountwithoutmask);
	}

	@Test
	public void testGetConsentSupportedSchemeMap() {
		aispHelper.getConsentSupportedSchemeMap();
	}

	@Test
	public void testPopulateAccountListFromAccountDetails() {
		AispConsent aispConsent = new AispConsent();
		List<AccountDetails> accountDetails = new ArrayList<>();
		AccountDetails adetails = new AccountDetails();
		adetails.setAccount(new OBAccount6Account());
		adetails.getAccount().setIdentification("aspsp");
		adetails.setServicer(new OBBranchAndFinancialInstitutionIdentification50());
		adetails.getServicer().setIdentification("served");
		accountDetails.add(adetails);
		aispConsent.setAccountDetails(accountDetails);
		PSD2Account account = new PSD2Account();
		account.setCurrency("EUR");
		account.setNickname("acc");
		account.setAccountSubType(OBExternalAccountSubType1Code.CHARGECARD);
		account.setAccountId("456");
		List<OBAccount6Account> cashAccount = new ArrayList<>();;
		account.setAccount(cashAccount);
		account.setServicer(adetails.getServicer());

		aispHelper.populateAccountListFromAccountDetails(aispConsent);
	}
}
