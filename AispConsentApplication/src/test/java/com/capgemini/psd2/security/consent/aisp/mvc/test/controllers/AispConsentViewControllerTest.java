package com.capgemini.psd2.security.consent.aisp.mvc.test.controllers;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.naming.directory.BasicAttributes;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.aisp.domain.OBAccount6;
import com.capgemini.psd2.aisp.domain.OBAccount6Account;
import com.capgemini.psd2.aisp.domain.OBBranchAndFinancialInstitutionIdentification50;
import com.capgemini.psd2.aisp.domain.OBExternalAccountType1Code;
import com.capgemini.psd2.aisp.domain.OBReadAccount6;
import com.capgemini.psd2.aisp.domain.OBReadAccount6Data;
import com.capgemini.psd2.consent.domain.PSD2Account;
import com.capgemini.psd2.integration.adapter.TPPInformationAdaptor;
import com.capgemini.psd2.logger.LoggerUtils;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.mask.DataMask;
import com.capgemini.psd2.scaconsenthelper.models.IntentTypeEnum;
import com.capgemini.psd2.scaconsenthelper.models.PickupDataModel;
import com.capgemini.psd2.security.consent.aisp.config.AispHostNameConfig;
import com.capgemini.psd2.security.consent.aisp.helpers.AispConsentCreationDataHelper;
import com.capgemini.psd2.security.consent.aisp.view.controllers.AispConsentViewController;
import com.capgemini.psd2.ui.content.utility.controller.UIStaticContentUtilityController;
import com.capgemini.psd2.utilities.JSONUtilities;
import com.capgemini.psd2.utilities.SandboxConfig;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
public class AispConsentViewControllerTest {

	@Mock
	private LoggerUtils loggerUtils;
	
	@Mock
	private AispConsentCreationDataHelper consentCreationDataHelper;
	
	@Mock
	private AispConsentCreationDataHelper aispConsentCreationDataHelper;
	
	@Mock
	private HttpServletRequest httpServletRequest;
	
	@Mock
	private HttpServletResponse httpServletResponse;

	@Mock
	private TPPInformationAdaptor tppInformationAdaptor;
	
	@Mock
	private AispHostNameConfig hostNameConfig;

	@Mock
	private UIStaticContentUtilityController uiController;

	@Mock
	private DataMask dataMask;
	
	@Mock
	private SandboxConfig sandboxConfig;
	
	@Mock
	private RequestHeaderAttributes requestHeaders;

	@InjectMocks
	private AispConsentViewController consentViewController;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void testHomePage() {
		
		Map<String,Object> map=new HashMap<>();
		when(httpServletRequest.getRequestURI()).thenReturn("/home");
		when(requestHeaders.getTenantId()).thenReturn("tenant2");
		when(hostNameConfig.getTenantSpecificEdgeserverhost(anyString())).thenReturn("https://google.com");
		ReflectionTestUtils.setField(consentViewController, "applicationName", "aispconsentapplicationv1");
		when(httpServletRequest.getQueryString()).thenReturn("key=value");
		when(sandboxConfig.isSandboxEnabled()).thenReturn(Boolean.TRUE);
		consentViewController.homePage(map);
	}

	@Test
	public void testCustomerConsentViewRequestUrlSuccessfulAuthenticationFlow() throws Exception {
		String idToken = "vntwKKvsq6nBd1YkYTVVqVOMEZW5xtZ4MF0vnyS2ocnbckNRi5gK/yBXtpA914/x905zxV86dZkCtnU69DWKgwBcyTlruh0hPe8rlGMdeKdJW+lx2h8WUHDI1g54xcI+OfokOqk59KzOcS+ESIxfzUVr7B0dnaeLVUblbtztpoEimPTnYtK3mXbXoqdtQHnSXtW1jlL0RRPRwTiYpGP44RvZkpGPU+7Re53gFf1FTTxGp0X8wTfiVed+CEvD6ySXRMwX8r0qEo3bNLvdzh4Zu/CznR5igxuSd2KDnr54ZaeB6RiXr3870e47PxaT2qlZmyqZLeSq4XbWlZxuU7x1/lM5qXGVNXslvl/lOLhvQv4xjdPJHKrrtzyL5SnJXifDdBC+O9wWx3ete6EcxrlMpA==";
		Map<String, String> paramMap = new HashMap<>();
		paramMap.put("brandId", "ROI");
		paramMap.put("channelId", "123");
		paramMap.put("idToken", idToken);
		List<String> params = new ArrayList<>();
		params.add("cahnnelId");
		Map<String, Object> map = new HashMap<>();
		map.put("NO_JS_MSG", "NO_JS_MSG");
		String jsonMap = JSONUtilities.getJSONOutPutFromObject(map);
		PickupDataModel pickupobj=new PickupDataModel();
		IntentTypeEnum intentTypeEnum=IntentTypeEnum.AISP_INTENT_TYPE;
		pickupobj.setIntentTypeEnum(intentTypeEnum);
		
		when(sandboxConfig.isSandboxEnabled()).thenReturn(Boolean.TRUE);
		when(httpServletRequest.getAttribute(anyString())).thenReturn(pickupobj);
		when(consentCreationDataHelper.retrieveCustomerAccountListInfo(anyString(),anyString(),anyString(),anyString(),anyString(),anyString(),anyString(), anyString())).thenReturn(getCustomerAccountInfo());
		when(dataMask.maskResponseGenerateString(anyObject(), anyString())).thenReturn("account");
		when(tppInformationAdaptor.fetchTPPInformation(anyString())).thenReturn(new BasicAttributes());
		when(consentCreationDataHelper.retrieveAccountRequestSetupData(anyString())).thenReturn("accountsetupresponse");
		
		when(uiController.getStaticContentForUI()).thenReturn(jsonMap);
		when(requestHeaders.getCorrelationId()).thenReturn("12345");
		ReflectionTestUtils.setField(consentViewController, "applicationName", "aispconsentapplicationv1");
		Map<String,Object> model=new HashMap<>();
		
		Enumeration<String> fsHeader =  new Enumeration<String>() {
			
			@Override
			public String nextElement() {
				return null;
			}
			
			@Override
			public boolean hasMoreElements() {
				return false;
			}
		};
		when(httpServletRequest.getHeaderNames()).thenReturn(fsHeader);
		consentViewController.consentView(model);
	}
	
	@Test
	public void testCustomerConsentViewException() throws Exception {
		String idToken = "vntwKKvsq6nBd1YkYTVVqVOMEZW5xtZ4MF0vnyS2ocnbckNRi5gK/yBXtpA914/x905zxV86dZkCtnU69DWKgwBcyTlruh0hPe8rlGMdeKdJW+lx2h8WUHDI1g54xcI+OfokOqk59KzOcS+ESIxfzUVr7B0dnaeLVUblbtztpoEimPTnYtK3mXbXoqdtQHnSXtW1jlL0RRPRwTiYpGP44RvZkpGPU+7Re53gFf1FTTxGp0X8wTfiVed+CEvD6ySXRMwX8r0qEo3bNLvdzh4Zu/CznR5igxuSd2KDnr54ZaeB6RiXr3870e47PxaT2qlZmyqZLeSq4XbWlZxuU7x1/lM5qXGVNXslvl/lOLhvQv4xjdPJHKrrtzyL5SnJXifDdBC+O9wWx3ete6EcxrlMpA==";
		Map<String, String> paramMap = new HashMap<>();
		paramMap.put("brandId", "ROI");
		paramMap.put("channelId", "123");
		paramMap.put("idToken", idToken);
		List<String> params = new ArrayList<>();
		params.add("cahnnelId");
		Map<String, Object> map = new HashMap<>();
		map.put("NO_JS_MSG", "NO_JS_MSG");
		String jsonMap = JSONUtilities.getJSONOutPutFromObject(map);
		PickupDataModel pickupobj=new PickupDataModel();
		IntentTypeEnum intentTypeEnum=IntentTypeEnum.AISP_INTENT_TYPE;
		pickupobj.setIntentTypeEnum(intentTypeEnum);
		pickupobj.setIntentId("123445");
		
		when(sandboxConfig.isSandboxEnabled()).thenReturn(Boolean.TRUE);
		when(httpServletRequest.getAttribute(anyString())).thenReturn(pickupobj);
		when(consentCreationDataHelper.retrieveCustomerAccountListInfo(anyString(),anyString(),anyString(),anyString(),anyString(),anyString(),anyString(), anyString())).thenReturn(getCustomerAccountInfo());
		when(dataMask.maskResponseGenerateString(anyObject(), anyString())).thenReturn("account");
		when(tppInformationAdaptor.fetchTPPInformation(anyString())).thenReturn(new BasicAttributes());
		when(consentCreationDataHelper.retrieveAccountRequestSetupData(anyString())).thenReturn("accountsetupresponse");
		Mockito.when(consentCreationDataHelper.findExistingConsentAccounts(any())).thenReturn(new OBReadAccount6());
		when(uiController.getStaticContentForUI()).thenReturn(jsonMap);
		when(requestHeaders.getCorrelationId()).thenReturn("12345");
		ReflectionTestUtils.setField(consentViewController, "applicationName", "aispconsentapplicationv1");
		Map<String,Object> model=new HashMap<>();
		Enumeration<String> fsHeader =  new Enumeration<String>() {
			
			@Override
			public String nextElement() {
				return null;
			}
			
			@Override
			public boolean hasMoreElements() {
				return false;
			}
		};
		when(httpServletRequest.getHeaderNames()).thenReturn(fsHeader);
		consentViewController.consentView(model);
	}


	@After
	public void tearDown() throws Exception {
		consentViewController = null;
	}
	
	
	public static OBReadAccount6 getCustomerAccountInfo() {
		OBReadAccount6 mockOBReadAccount6 = new OBReadAccount6();
		List<OBAccount6> accountData = new ArrayList<>();
		PSD2Account acct = new PSD2Account();
		acct.setAccountId("14556236");
		acct.setCurrency("EUR");
		acct.setNickname("John");
		acct.setAccountType(OBExternalAccountType1Code.BUSINESS);
		OBBranchAndFinancialInstitutionIdentification50 servicer = new OBBranchAndFinancialInstitutionIdentification50();
		servicer.setIdentification("12345");
		List<OBAccount6Account> accountList= new ArrayList<>();
		OBAccount6Account account = new OBAccount6Account();

		account.setIdentification("12345");
		account.setSchemeName("IBAN");
		accountList.add(account);
		acct.setAccount(accountList);
		acct.setServicer(servicer);

		PSD2Account accnt = new PSD2Account();
		accnt.setAccountId("14556236");
		accnt.setCurrency("EUR");
		accnt.setNickname("John");
		// accnt.setHashedValue();
		accnt.setServicer(servicer);
		accnt.setAccount(accountList);
		accnt.setAccountType(OBExternalAccountType1Code.BUSINESS);
		accountData.add(acct);
		accountData.add(accnt);
		OBReadAccount6Data data2 = new OBReadAccount6Data();
		data2.setAccount(accountData);
		account.setSchemeName("IBAN");
		mockOBReadAccount6.setData(data2);

		return mockOBReadAccount6;
	}

}
