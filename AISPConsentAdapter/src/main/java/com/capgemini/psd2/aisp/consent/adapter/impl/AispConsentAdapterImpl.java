/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.aisp.consent.adapter.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.aisp.adapter.AispConsentAdapter;
import com.capgemini.psd2.aisp.consent.adapter.repository.AispConsentMongoRepository;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.consent.domain.AispConsent;
import com.capgemini.psd2.enums.ConsentStatusEnum;
import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.ErrorMapKeys;
import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.InternalServerErrorMessage;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.LoggerUtils;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.product.common.CompatibleVersionList;
import com.capgemini.psd2.utilities.GenerateUniqueIdUtilities;

/**
 * The Class AispConsentAdapterImpl.
 */
@Component
public class AispConsentAdapterImpl implements AispConsentAdapter {

	private static final Logger LOG = LoggerFactory.getLogger(AispConsentAdapterImpl.class);

	/** The aisp consent repository. */
	@Autowired
	private AispConsentMongoRepository aispConsentMongoRepository;

	/** The req header attributes. */
	@Autowired
	private RequestHeaderAttributes reqHeaderAttributes;

	@Autowired
	private LoggerUtils loggerUtils;

	@Autowired
	private MongoTemplate mongoTemplate;
	
	@Autowired
	private CompatibleVersionList compatibleVersionList;
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.capgemini.psd2.consent.adapter.AispConsentAdapter#retrieveConsent(
	 * java.lang.String)
	 */
	@Override
	public AispConsent retrieveConsent(String consentId) {
		AispConsent consent = null;
		try {
			
			consent = aispConsentMongoRepository.findByConsentIdAndCmaVersionIn(consentId, compatibleVersionList.fetchVersionList());
		} catch (DataAccessResourceFailureException e) {
			LOG.info("DataAccessResourceFailureException in retrieveConsent(): "+e);
			throw PSD2Exception.populatePSD2Exception(e.getMessage(), ErrorCodeEnum.CONNECTION_ERROR);
		}
		if (consent == null)
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_RESOURCE_NOTFOUND,
			ErrorMapKeys.INTENT_ID_VALIDATION_ERROR));
		return consent;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.capgemini.psd2.consent.adapter.AispConsentMappingAdapter#
	 * retrieveAccountMapping(java.lang.String)
	 */
	@Override
	public AccountMapping retrieveAccountMapping(String consentId) {
		AispConsent consent = retrieveConsent(consentId);
		AccountMapping accountMapping = new AccountMapping();
		accountMapping.setPsuId(consent.getPsuId());
		accountMapping.setTppCID(consent.getTppCId());
		accountMapping.setCorrelationId(reqHeaderAttributes.getCorrelationId());
		accountMapping.setAccountDetails(consent.getAccountDetails());
		return accountMapping;
	}

	@Override
	public void createConsent(AispConsent aispConsent) {
		AispConsent consent = null;
		try {
			consent = retrieveConsentByAccountRequestId(aispConsent.getAccountRequestId());

			if (consent != null && consent.getStatus().equals(ConsentStatusEnum.AWAITINGAUTHORISATION)) {
				/*
				 * Since only one version of Consent is deployed, to maintain backward compatibility
				 * we need to set the status according to the CMA Version.
				 */
				if (aispConsent.getCmaVersion() == null || aispConsent.getCmaVersion().equals(PSD2Constants.CMA3))
					consent.setStatus(ConsentStatusEnum.REVOKED);
				else 
					consent.setStatus(ConsentStatusEnum.DELETED);
				aispConsentMongoRepository.save(consent);

			} else if (consent != null && consent.getStatus().equals(ConsentStatusEnum.AUTHORISED)) {

				throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.INTENT_ID_VALIDATION_ERROR);

			}

			if (aispConsent.getAccountDetails() == null || aispConsent.getAccountDetails().isEmpty()) {

				throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.NO_ACCOUNT_DETAILS_FOUND);

			}

			for (AccountDetails acctDetail : aispConsent.getAccountDetails()) {

				acctDetail.setAccountId(GenerateUniqueIdUtilities.getUniqueId().toString());
			}

			aispConsent.setConsentId(GenerateUniqueIdUtilities.generateRandomUniqueID());
			aispConsent.setStatus(ConsentStatusEnum.AWAITINGAUTHORISATION);
			aispConsentMongoRepository.save(aispConsent);

		} catch (DataAccessResourceFailureException e) {
			LOG.info("DataAccessResourceFailureException in createConsent(): "+e);
			throw PSD2Exception.populatePSD2Exception(e.getMessage(), ErrorCodeEnum.CONNECTION_ERROR);

		}
	}

	@Override
	public AispConsent retrieveConsentByAccountRequestIdAndStatus(String accountRequestId, ConsentStatusEnum status) {
		AispConsent consent = null;
		try {
			consent = aispConsentMongoRepository.findByAccountRequestIdAndStatusAndCmaVersionIn(accountRequestId, status, compatibleVersionList.fetchVersionList());
			
		} catch (DataAccessResourceFailureException e) {
			LOG.info("DataAccessResourceFailureException in retrieveConsentByAccountRequestIdAndStatus(): "+e);
			throw PSD2Exception.populatePSD2Exception(e.getMessage(), ErrorCodeEnum.CONNECTION_ERROR);

		}
		return consent;
	}

	@Override
	public void updateConsentStatus(String consentId, ConsentStatusEnum statusEnum) {
		try {
			Query query = new Query(Criteria.where("consentId").is(consentId));
			Update update = new Update();
			update.set("status", statusEnum);

			mongoTemplate.updateFirst(query, update, AispConsent.class);
	
		} catch (DataAccessResourceFailureException e) {
			LOG.info("DataAccessResourceFailureException in updateConsentStatus(): "+e); //NOSONAR
			throw PSD2Exception.populatePSD2Exception(e.getMessage(), ErrorCodeEnum.CONNECTION_ERROR);
		}
	}

	@Override
	public List<AispConsent> retrieveConsentByPsuIdAndConsentStatus(String psuId, ConsentStatusEnum statusEnum) {

		List<AispConsent> consentList = null;
		try {
			if (null == statusEnum) {
				consentList = aispConsentMongoRepository.findByPsuIdAndCmaVersionIn(psuId , compatibleVersionList.fetchVersionList());
			} else {
				consentList = aispConsentMongoRepository.findByPsuIdAndStatusAndCmaVersionIn(psuId, statusEnum , compatibleVersionList.fetchVersionList());
			}

		} catch (DataAccessResourceFailureException e) {
			LOG.info("DataAccessResourceFailureException in retrieveConsentByPsuIdAndConsentStatus(): "+e);
			throw PSD2Exception.populatePSD2Exception(e.getMessage(), ErrorCodeEnum.CONNECTION_ERROR);

		}

		return consentList;
	}

	@Override
	public List<AispConsent> retrieveConsentByPsuIdConsentStatusAndTenantId(String psuId, ConsentStatusEnum statusEnum, String tenantId) {
		
		LOG.info(psuId+"statusenum"+statusEnum+"tenantID"+tenantId+"version"+compatibleVersionList.fetchVersionList()); //NOSONAR
		List<AispConsent> consentList = null;
		try {
			if (null == statusEnum) {
				consentList = aispConsentMongoRepository.findByPsuIdAndTenantIdAndCmaVersionIn(psuId, tenantId , compatibleVersionList.fetchVersionList());
			} else {
				consentList = aispConsentMongoRepository.findByPsuIdAndStatusAndTenantIdAndCmaVersionIn(psuId, statusEnum, tenantId, compatibleVersionList.fetchVersionList());
			}

		} catch (DataAccessResourceFailureException e) {
			LOG.info("DataAccessResourceFailureException in retrieveConsentByPsuIdConsentStatusAndTenantId(): "+e);
			throw PSD2Exception.populatePSD2Exception(e.getMessage(), ErrorCodeEnum.CONNECTION_ERROR);
			
		}

		return consentList;
	}
	@Override
	public AispConsent validateAndRetrieveConsentByAccountId(String accountId, String consentId) {
		LOG.info("{\"Enter\":\"{}\",\"{}\"}",
				"com.capgemini.psd2.aisp.consent.adapter.impl.validateAndRetrieveConsentByAccountId()",
				loggerUtils.populateLoggerData("validateAndRetrieveConsentByAccountId"));

		AispConsent aispConsent = null;
		try {
			aispConsent = aispConsentMongoRepository.findByAccountDetailsAccountIdAndCmaVersionIn(accountId,compatibleVersionList.fetchVersionList());
		} catch (DataAccessResourceFailureException e) {
			LOG.info("DataAccessResourceFailureException in validateAndRetrieveConsentByAccountId(): "+e);
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
					InternalServerErrorMessage.CONNECTION_ERROR));
		}

		if (aispConsent == null)
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_RESOURCE_NOTFOUND,
					ErrorMapKeys.INTENT_ID_VALIDATION_ERROR));

		if (!aispConsent.getConsentId().equals(consentId))
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.NO_ACCESS_GIVEN_TO_REQUESTED_ACCOUNT);

		LOG.info("{\"Exit\":\"{}\",\"{}\"}",
				"com.capgemini.psd2.aisp.consent.adapter.impl.validateAndRetrieveConsentByAccountId()",
				loggerUtils.populateLoggerData("validateAndRetrieveConsentByAccountId"));

		return aispConsent;
	}

	@Override
	public void updateConsentStatusWithResponse(String consentId, ConsentStatusEnum statusEnum) {
		try {
			Query query = new Query(Criteria.where("consentId").is(consentId));
			Update update = new Update();
			update.set("status", statusEnum);

			mongoTemplate.updateFirst(query, update, AispConsent.class);
	
		} catch (DataAccessResourceFailureException e) {
			LOG.info("DataAccessResourceFailureException in updateConsentStatusWithResponse(): "+e);
			throw PSD2Exception.populatePSD2Exception(e.getMessage(), ErrorCodeEnum.CONNECTION_ERROR);
		}
	}

	@Override
	public AispConsent retrieveConsentByAccountRequestId(String accountRequestId) {
		AispConsent aispConsent = null;
		try {
			aispConsent = aispConsentMongoRepository.findByAccountRequestIdAndCmaVersionIn(accountRequestId, compatibleVersionList.fetchVersionList());
			return aispConsent;
		} catch (DataAccessResourceFailureException e) {
			LOG.info("DataAccessResourceFailureException in retrieveConsentByAccountRequestId(): "+e);
			throw PSD2Exception.populatePSD2Exception(e.getMessage(), ErrorCodeEnum.CONNECTION_ERROR);
		}
	}

	@Override
	public AispConsent updateConsent(AispConsent aispConsent) {
		try {
			return aispConsentMongoRepository.save(aispConsent);
		} catch (DataAccessResourceFailureException e) {
			LOG.info("DataAccessResourceFailureException in updateConsent(): "+e);
			throw PSD2Exception.populatePSD2Exception(e.getMessage(), ErrorCodeEnum.CONNECTION_ERROR);
		}
	}
}