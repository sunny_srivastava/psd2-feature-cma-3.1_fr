/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.aisp.consent.adapter.test.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyList;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.aisp.consent.adapter.impl.AispConsentAdapterImpl;
import com.capgemini.psd2.aisp.consent.adapter.repository.AispConsentMongoRepository;
import com.capgemini.psd2.aisp.consent.adapter.test.mock.data.AispConsentAdapterMockData;
import com.capgemini.psd2.consent.domain.AispConsent;
import com.capgemini.psd2.enums.ConsentStatusEnum;
import com.capgemini.psd2.exceptions.OBPSD2Exception;
import com.capgemini.psd2.exceptions.OBPSD2ExceptionUtility;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.LoggerUtils;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.product.common.CompatibleVersionList;
import com.mongodb.WriteResult;

/**
 * The Class ConsentMappingAdapterImplTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class AispConsentAdapterImplTest {

	/** The consent mapping repository. */
	@Mock
	private AispConsentMongoRepository aispConsentMongoRepository;

	/** The req header attributes. */
	@Mock
	private RequestHeaderAttributes reqHeaderAttributes;

	@Mock
	private LoggerUtils loggerUtils;

	@Mock
	private MongoTemplate mongoTemplate;

	@Mock
	private CompatibleVersionList compatibleVersionList;

	/** The consent mapping adapter impl. */
	@InjectMocks
	private AispConsentAdapterImpl aispConsentAdapterImpl = new AispConsentAdapterImpl();

	/**
	 * Sets the up.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void setUp() throws Exception {
		Map<String, String> map = new HashMap<>();
		map.put("SIGNATURE", "Something wrong with x-jws-signature request header");
		Map<String, String> specificErrorMessageMap = new HashMap<>();
		specificErrorMessageMap.put("signature_invalid_content",
				"Invalid content prsent in joseheader of x-jws-signature header");
		specificErrorMessageMap.put("signature_missing", "signature header missing in request");
		OBPSD2ExceptionUtility.genericErrorMessages.putAll(map);
		OBPSD2ExceptionUtility.specificErrorMessages.putAll(specificErrorMessageMap);
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * Test retrieve consent success flow.
	 */
	@Test
	public void testRetrieveConsentSuccessFlow() {
		List<String> version = new ArrayList<String>();
		when(compatibleVersionList.fetchVersionList()).thenReturn(version);
		when(aispConsentMongoRepository.findByConsentIdAndCmaVersionIn(anyString(), anyList()))
				.thenReturn(AispConsentAdapterMockData.getConsentMockData());

		AispConsent consent = aispConsentAdapterImpl.retrieveConsent("a1e590ad-73dc-453a-841e-2a2ef055e878");
		assertEquals(AispConsentAdapterMockData.getConsentMockData().getPsuId(), consent.getPsuId());
		assertEquals(AispConsentAdapterMockData.getConsentMockData().getConsentId(), consent.getConsentId());
	}

	/**
	 * Test retrieve consent data access resource failure exception.
	 */
	@Test(expected = PSD2Exception.class)
	public void testRetrieveConsentDataAccessResourceFailureException() {
		when(aispConsentMongoRepository.findByConsentIdAndCmaVersionIn(anyString(), any()))
				.thenThrow(new DataAccessResourceFailureException("Test"));
		aispConsentAdapterImpl.retrieveConsent("a1e590ad-73dc-453a-841e-2a2ef055e878");
	}

	/**
	 * Test retrieve consent null exception.
	 */
	@Test(expected = Exception.class)
	public void testRetrieveConsentNullException() {
		when(aispConsentMongoRepository.findByConsentIdAndCmaVersionIn(anyString(), any())).thenReturn(null);
		aispConsentAdapterImpl.retrieveConsent("a1e590ad-73dc-453a-841e-2a2ef055e878");
	}

	@Test
	public void testRetrieveAccountMappingSuccessFlow() {
		when(aispConsentMongoRepository.findByConsentIdAndCmaVersionIn(anyString(), any()))
				.thenReturn(AispConsentAdapterMockData.getConsentMockData());
		assertEquals(AispConsentAdapterMockData.getAccountMapping().getPsuId(),
				aispConsentAdapterImpl.retrieveAccountMapping("a1e590ad-73dc-453a-841e-2a2ef055e878").getPsuId());
	}

	@Test
	public void testCreateConsent() {
		when(aispConsentMongoRepository.save(any(AispConsent.class)))
				.thenReturn(AispConsentAdapterMockData.getConsentMockData());
		AispConsent aispConsent = AispConsentAdapterMockData.getConsentMockData();
		aispConsentAdapterImpl.createConsent(aispConsent);
	}

	@Test
	public void testCreateConsent_StatusEnum_AWAITINGAUTHORISATION_WithCmaVersionAs3_1() {
		when(aispConsentMongoRepository.save(any(AispConsent.class)))
				.thenReturn(AispConsentAdapterMockData.getConsentMockData());
		AispConsent aispConsent = AispConsentAdapterMockData.getConsentMockData();
		aispConsent.setStatus(ConsentStatusEnum.AWAITINGAUTHORISATION);
		aispConsent.setCmaVersion("3.1");
		when(aispConsentMongoRepository.findByAccountRequestIdAndCmaVersionIn(anyString(), any())).thenReturn(aispConsent);
		aispConsentAdapterImpl.createConsent(aispConsent);
		assertNotNull(aispConsent);
	}
	
	@Test
	public void testCreateConsent_StatusEnum_AWAITINGAUTHORISATION_WithCmaVersionAs3_0() {
		when(aispConsentMongoRepository.save(any(AispConsent.class)))
				.thenReturn(AispConsentAdapterMockData.getConsentMockData());
		AispConsent aispConsent = AispConsentAdapterMockData.getConsentMockData();
		aispConsent.setStatus(ConsentStatusEnum.AWAITINGAUTHORISATION);
		aispConsent.setCmaVersion(PSD2Constants.CMA3);
		when(aispConsentMongoRepository.findByAccountRequestIdAndCmaVersionIn(anyString(), any())).thenReturn(aispConsent);
		aispConsentAdapterImpl.createConsent(aispConsent);
		assertNotNull(aispConsent);
	}
	
	@Test(expected = PSD2Exception.class)
	public void testCreateConsent_StatusEnum_AUTHORISED() {
		when(aispConsentMongoRepository.save(any(AispConsent.class)))
				.thenReturn(AispConsentAdapterMockData.getConsentMockData());
		AispConsent aispConsent = AispConsentAdapterMockData.getConsentMockData();
		aispConsent.setAccountRequestId("123");
		aispConsent.setStatus(ConsentStatusEnum.AUTHORISED);
		when(aispConsentMongoRepository.findByAccountRequestIdAndCmaVersionIn(anyString(), any())).thenReturn(aispConsent);
		aispConsentAdapterImpl.createConsent(aispConsent);
		assertNotNull(aispConsent);
	}
	
	@Test
	public void testUpdateConsent() {
		
		AispConsent aispConsent = AispConsentAdapterMockData.getConsentMockData();
		when(aispConsentMongoRepository.save(aispConsent)).thenReturn(aispConsent);
		aispConsentAdapterImpl.updateConsent(aispConsent);
	}
	
	@Test(expected=PSD2Exception.class)
	public void testUpdateConsentDataAccessResourceFailureException() {
		
		AispConsent aispConsent = AispConsentAdapterMockData.getConsentMockData();
		when(aispConsentMongoRepository.save(aispConsent)).thenThrow(DataAccessResourceFailureException.class);
		aispConsentAdapterImpl.updateConsent(aispConsent);
	}

	@SuppressWarnings("unchecked")
	@Test(expected = PSD2Exception.class)
	public void testCreateConsentWithException() {
		when(aispConsentMongoRepository.findByAccountRequestIdAndStatusAndCmaVersionIn(anyString(), any(), any()))
				.thenReturn(AispConsentAdapterMockData.getConsentMockData());

		when(aispConsentMongoRepository.save(any(AispConsent.class)))
				.thenThrow(DataAccessResourceFailureException.class);
		aispConsentAdapterImpl.createConsent(AispConsentAdapterMockData.getConsentMockData());
	}

	@SuppressWarnings("unchecked")
	@Test(expected = OBPSD2Exception.class)
	public void testConsentAlreadyExist() {
		when(aispConsentMongoRepository.findByAccountRequestIdAndStatusAndCmaVersionIn(anyString(), any(), any()))
				.thenReturn(AispConsentAdapterMockData.getConsentMockData());

		when(aispConsentMongoRepository.save(any(AispConsent.class))).thenThrow(OBPSD2Exception.class);
		AispConsent aispConsent = AispConsentAdapterMockData.getConsentMockData();
		aispConsentAdapterImpl.createConsent(aispConsent);
	}

	@Test(expected = PSD2Exception.class)
	public void testNoAccountDetails() {
		when(aispConsentMongoRepository.findByAccountRequestIdAndStatusAndCmaVersionIn(anyString(), any(), any()))
				.thenReturn(null);
		AispConsent aispConsent = AispConsentAdapterMockData.getConsentMockDataWithOutAccountDetails();
		aispConsentAdapterImpl.createConsent(aispConsent);
	}

	@Test
	public void testRetrieveConsentByAccountRequestIdSuccessFlow() {
		AispConsent consent = AispConsentAdapterMockData.getConsentMockData();
		when(aispConsentMongoRepository.findByAccountRequestIdAndStatusAndCmaVersionIn(anyString(), any(), any()))
				.thenReturn(consent);
		assertEquals(consent.getPsuId(),
				aispConsentAdapterImpl
						.retrieveConsentByAccountRequestIdAndStatus("1234", ConsentStatusEnum.AWAITINGAUTHORISATION)
						.getPsuId());
	}

	@SuppressWarnings("unchecked")
	@Test(expected = PSD2Exception.class)
	public void testRetrieveConsentByAccountRequestIdDataAccessResourceFailureException() {
		when(aispConsentMongoRepository.findByAccountRequestIdAndCmaVersionIn(anyString(), any()))
				.thenThrow(DataAccessResourceFailureException.class);
		aispConsentAdapterImpl.retrieveConsentByAccountRequestId("1234");
	}

	@SuppressWarnings("unchecked")
	@Test
	public void testUpdateConsentStatusPSD2Exception() {
		AispConsent consent = AispConsentAdapterMockData.getConsentMockData();
		Query query = new Query();
		Update update = new Update();
		boolean updateOfExisting = true;
		Object upsertedId = "";
		int n = 1;
		WriteResult value = new WriteResult(n, updateOfExisting, upsertedId);
		when(mongoTemplate.updateFirst(query, update, AispConsent.class))
				.thenThrow(DataAccessResourceFailureException.class);
		when(aispConsentMongoRepository.findByConsentIdAndCmaVersionIn(anyString(), any())).thenReturn(consent);
		when(aispConsentMongoRepository.save(any(AispConsent.class))).thenThrow(PSD2Exception.class);
		aispConsentAdapterImpl.updateConsentStatus("1234", ConsentStatusEnum.AUTHORISED);
	}

	@SuppressWarnings("unchecked")
	@Test
	public void testUpdateConsentStatusException() {
		AispConsent consent = AispConsentAdapterMockData.getConsentMockData();
		when(aispConsentMongoRepository.findByConsentIdAndCmaVersionIn(anyString(), any())).thenReturn(consent);
		when(aispConsentMongoRepository.save(any(AispConsent.class)))
				.thenThrow(DataAccessResourceFailureException.class);
		Query query = mock(Query.class);
		Update update = mock(Update.class);
		when(mongoTemplate.updateFirst(query, update, AispConsent.class))
				.thenThrow(DataAccessResourceFailureException.class);
		aispConsentAdapterImpl.updateConsentStatus("1234", ConsentStatusEnum.AUTHORISED);

	}

	@Test
	public void testRetrieveConsentByPsuIdAndConsentStatusNull() {
		AispConsent consent = AispConsentAdapterMockData.getConsentMockData();
		List<AispConsent> consentList = new ArrayList<>();
		consentList.add(consent);
		when(aispConsentMongoRepository.findByPsuIdAndCmaVersionIn(anyString(), any())).thenReturn(consentList);
		aispConsentAdapterImpl.retrieveConsentByPsuIdAndConsentStatus("1234", null);
	}

	@SuppressWarnings("unchecked")
	@Test(expected = PSD2Exception.class)
	public void testRetrieveConsentByPsuIdAndConsentStatusException() {
		AispConsent consent = AispConsentAdapterMockData.getConsentMockData();
		List<AispConsent> consentList = new ArrayList<>();
		consentList.add(consent);
		when(aispConsentMongoRepository.findByPsuIdAndCmaVersionIn(anyString(), any()))
				.thenThrow(DataAccessResourceFailureException.class);
		aispConsentAdapterImpl.retrieveConsentByPsuIdAndConsentStatus("1234", null);
	}

	@Test
	public void testRetrieveConsentByPsuIdConsentStatusAndTenantId_SuccessFlow() {
		List<AispConsent> consentList = null;
		Mockito.when(aispConsentMongoRepository.findByPsuIdAndTenantIdAndCmaVersionIn(anyString(), anyString(), any()))
				.thenReturn(consentList);
		aispConsentAdapterImpl.retrieveConsentByPsuIdConsentStatusAndTenantId("12345", null, "BOIUK");
	}

	@Test
	public void testRetrieveConsentByPsuIdConsentStatusAndTenantId_StatusEnumNotNull() {
		List<AispConsent> consentList = null;
		Mockito.when(aispConsentMongoRepository.findByPsuIdAndTenantIdAndCmaVersionIn(anyString(), anyString(), any()))
				.thenReturn(consentList);
		aispConsentAdapterImpl.retrieveConsentByPsuIdConsentStatusAndTenantId("12345", ConsentStatusEnum.AUTHORISED,
				"BOIUK");
	}

	@SuppressWarnings("unchecked")
	@Test (expected=PSD2Exception.class)
	public void testRetrieveConsentByPsuIdConsentStatusAndTenantId_DataAccessFailure() {
		Mockito.when(aispConsentMongoRepository.findByPsuIdAndStatusAndTenantIdAndCmaVersionIn(anyString(), any(), anyString(), any()))
				.thenThrow(DataAccessResourceFailureException.class);
		aispConsentAdapterImpl.retrieveConsentByPsuIdConsentStatusAndTenantId("12345", ConsentStatusEnum.AUTHORISED,
				"BOIUK");
	}

	@Test
	public void testValidateAndRetrieveConsentByAccountId_SuccessFlow() {
		AispConsent aispConsent = new AispConsent();
		Mockito.when(aispConsentMongoRepository.findByAccountDetailsAccountIdAndCmaVersionIn(anyString(), any()))
				.thenReturn(aispConsent);
		aispConsent.setConsentId("c123");
		aispConsentAdapterImpl.validateAndRetrieveConsentByAccountId("12345", "c123");
	}

	@Test(expected = PSD2Exception.class)
	public void testValidateAndRetrieveConsentByAccountId_aispConsentNull() {
		AispConsent aispConsent = null;
		Mockito.when(aispConsentMongoRepository.findByAccountDetailsAccountIdAndCmaVersionIn(anyString(), any()))
				.thenReturn(aispConsent);
		aispConsentAdapterImpl.validateAndRetrieveConsentByAccountId("12345", "c123");
	}

	@Test(expected = PSD2Exception.class)
	public void testValidateAndRetrieveConsentByAccountId_aispConsentMismatch() {
		AispConsent aispConsent = new AispConsent();
		Mockito.when(aispConsentMongoRepository.findByAccountDetailsAccountIdAndCmaVersionIn(anyString(), any()))
				.thenReturn(aispConsent);
		aispConsent.setConsentId("456");
		aispConsentAdapterImpl.validateAndRetrieveConsentByAccountId("12345", "c123");
	}

	@SuppressWarnings("unchecked")
	@Test(expected = PSD2Exception.class)
	public void testValidateAndRetrieveConsentByAccountId_DataAccessFailure() {
		Mockito.when(aispConsentMongoRepository.findByAccountDetailsAccountIdAndCmaVersionIn(anyString(), any()))
				.thenThrow(DataAccessResourceFailureException.class);
		aispConsentAdapterImpl.validateAndRetrieveConsentByAccountId("12345", "c123");
	}

	@Test
	public void testRetrieveConsentByPsuIdAndConsentStatus() {
		AispConsent consent = AispConsentAdapterMockData.getConsentMockData();
		List<AispConsent> consentList = new ArrayList<>();
		consentList.add(consent);
		when(aispConsentMongoRepository.findByPsuIdAndStatusAndCmaVersionIn(anyString(), anyObject(), any()))
				.thenReturn(consentList);
		aispConsentAdapterImpl.retrieveConsentByPsuIdAndConsentStatus("1234", ConsentStatusEnum.AUTHORISED);
	}

	@Test
	public void testUpdateConsentStatusWithResponse() {
		AispConsent consent = AispConsentAdapterMockData.getConsentMockData();
		when(aispConsentMongoRepository.findByConsentIdAndCmaVersionIn(anyString(), any())).thenReturn(consent);
		Query query = new Query();
		Update update = new Update();
		boolean updateOfExisting = true;
		Object upsertedId = "";
		int n = 1;
		WriteResult value = new WriteResult(n, updateOfExisting, upsertedId);
		when(mongoTemplate.updateFirst(query, update, AispConsent.class)).thenReturn(value);
		aispConsentAdapterImpl.updateConsentStatusWithResponse("12345", ConsentStatusEnum.AUTHORISED);
	}
}
