(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-header></app-header>\n<section class=\"main-container aisp-container\" role=\"main\" [ngClass]=\"{'consent-renewal' : fetchService.renewalAuthorisation}\">\n  <div class=\"container position-relative\">\n    <div class=\"row\">\n      <div class=\"col-md-10 offset-md-1\">\n        <app-aisp *ngIf=\"fetchService.consentType==='AISP'\"></app-aisp>\n        <app-pisp-account-selection *ngIf=\"fetchService.consentType==='PISP'\"></app-pisp-account-selection>\n        <app-cisp-account-selection *ngIf=\"fetchService.consentType==='CISP'\"></app-cisp-account-selection>\n      </div>\n    </div>\n  </div>\n  <app-footer></app-footer>\n</section>\n<app-loading-spinner *ngIf=\"showLoadingSpinner\"></app-loading-spinner>\n"

/***/ }),

/***/ "./src/app/app.component.scss":
/*!************************************!*\
  !*** ./src/app/app.component.scss ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _helper_account_handler_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./helper/account-handler.service */ "./src/app/helper/account-handler.service.ts");
/* harmony import */ var _helper_error_handler_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./helper/error-handler.service */ "./src/app/helper/error-handler.service.ts");
/* harmony import */ var _helper_static_content_fetch_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./helper/static-content-fetch.service */ "./src/app/helper/static-content-fetch.service.ts");
/* harmony import */ var _helper_shared_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./helper/shared.service */ "./src/app/helper/shared.service.ts");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _components_common_components_modal_popup_modal_popup_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./components/common-components/modal-popup/modal-popup.component */ "./src/app/components/common-components/modal-popup/modal-popup.component.ts");
/* harmony import */ var _helper_modal_definition_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./helper/modal-definition.service */ "./src/app/helper/modal-definition.service.ts");










var AppComponent = /** @class */ (function () {
    function AppComponent(fetchService, accountService, title, modalService, errorService, sharedService, modalDefService) {
        var _this = this;
        this.fetchService = fetchService;
        this.accountService = accountService;
        this.title = title;
        this.modalService = modalService;
        this.errorService = errorService;
        this.sharedService = sharedService;
        this.modalDefService = modalDefService;
        this.error = document.querySelector('#error') && document.querySelector('#error').value ?
            JSON.parse(document.querySelector('#error').value) : '';
        this.showLoadingSpinner = false;
        this.fetchService.consentType = document.querySelector('#consentType') && document.querySelector('#consentType').value ?
            document.querySelector('#consentType').value : 'AISP';
        this.fetchService.tppInfo = document.querySelector('#tppInfo') && document.querySelector('#tppInfo').value ?
            JSON.parse(document.querySelector('#tppInfo').value) : null;
        this.fetchService.resumePath = document.querySelector('#resumePath') && document.querySelector('#resumePath').value ?
            document.querySelector('#resumePath').value : '';
        this.fetchService.correlationId = document.querySelector('#correlationId') && document.querySelector('#correlationId').value ?
            document.querySelector('#correlationId').value : '';
        this.fetchService.renewalAuthorisation = (document.querySelector('#renewalAuthorisation') && (document.querySelector('#renewalAuthorisation').value === 'true')) ?
            true : false;
        this.fetchService.fsHeaders = document.querySelector('#fsHeaders') && document.querySelector('#fsHeaders').value
            ? document.querySelector('#fsHeaders').value : '';
        this.staticContent = document.querySelector('#staticContent');
        this.fetchService.CdnBaseUrl = document.querySelector('#cdnBaseURL') && document.querySelector('#cdnBaseURL').value ? document.querySelector('#cdnBaseURL').value : '';
        this.fetchService.BuildVersion = document.querySelector('#buildversion') && document.querySelector('#buildversion').value ? document.querySelector('#buildversion').value : '';
        this.fetchService.isPostAuthorisingPartyFlowEnabled = document.querySelector('#isPostAuthorisingPartyFlowEnabled') && document.querySelector('#isPostAuthorisingPartyFlowEnabled').value === 'true' ? true : false;
        this.fetchService.baseCurrency = document.querySelector('#baseCurrency') && document.querySelector('#baseCurrency').value ? (document.querySelector('#baseCurrency').value) : 'EUR';
        this.fetchService.postAuthorisingPartyDetails = null;
        this.fetchService.setSoftwareOnBehalfOfOrg((document.querySelector('#softwareOnBehalfOfOrg').value === "null" || document.querySelector('#softwareOnBehalfOfOrg').value == null
            || document.querySelector('#softwareOnBehalfOfOrg').value.length <= 0) ? 'none' : document.querySelector('#softwareOnBehalfOfOrg').value);
        if (document.querySelector('#postAuthorisingPartyDetails') && document.querySelector('#postAuthorisingPartyDetails').value) {
            this.fetchService.postAuthorisingPartyDetails = JSON.parse(document.querySelector('#postAuthorisingPartyDetails').value);
        }
        if (this.staticContent.value) {
            this.staticContent = this.staticContent.value;
            this.staticContent = JSON.parse(this.staticContent);
            this.title.setTitle(this.staticContent.APPLICATION_TITLE);
            this.fetchService.staticContent = this.staticContent;
            this.errorService.setErrors(this.fetchService.getStaticContent('ERROR_MESSAGES'));
        }
        this.sharedService.showLoadingSpinner$.subscribe(function (showLoadingSpinner) { return _this.showLoadingSpinner = showLoadingSpinner; });
        if (document.querySelector('#paymentSetup') && document.querySelector('#paymentSetup').value) {
            this.accountService.paymentSetup = JSON.parse(document.querySelector('#paymentSetup').value);
        }
        try {
            console.log('heya' + navigator.cookieEnabled);
            if (!navigator.cookieEnabled) {
                this.fetchService.serverErrorFlag = true;
                this.errorService.error = 'COOKIE_ENABLE_MSG';
            }
            else {
                if (this.error) {
                    this.pageErrorMap(this.error);
                }
                else {
                    if (document.querySelector('#account-request') && document.querySelector('#account-request').value) {
                        this.accountService.accountRequest = JSON.parse(document.querySelector('#account-request').value);
                    }
                    if (document.querySelector('#psuAccounts') && document.querySelector('#psuAccounts').value) {
                        this.accountService.psuAccounts = JSON.parse(document.querySelector('#psuAccounts').value).Data.Account;
                    }
                    if (!this.accountService.psuAccounts.length) {
                        this.noAccountFound = true;
                        throw null;
                    }
                }
            }
        }
        catch (e) {
            this.fetchService.serverErrorFlag = true;
            if (this.noAccountFound) {
                this.errorService.error = '502';
            }
            else {
                this.errorService.error = '999';
            }
        }
    }
    AppComponent.prototype.pageErrorMap = function (error) {
        this.fetchService.serverErrorFlag = true;
        if (error.errorCode === '731') {
            this.sessionRedirectUri = this.fetchService.resumePath;
            // this.errorService.error=error.errorCode;
            var modalInstance = this.modalService.open(_components_common_components_modal_popup_modal_popup_component__WEBPACK_IMPORTED_MODULE_8__["ModalPopupComponent"], { backdrop: 'static', keyboard: false });
            modalInstance.componentInstance.modalDef = this.modalDefService.defineModalObj(this, 'session');
            modalInstance.componentInstance.classRef = this;
        }
        else {
            this.errorService.error = error.errorCode;
        }
    };
    AppComponent.prototype.onSessionBtnClick = function (classRef) {
        window.location.href = classRef.sessionRedirectUri;
    };
    AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.scss */ "./src/app/app.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_helper_static_content_fetch_service__WEBPACK_IMPORTED_MODULE_5__["StaticContentFetchService"],
            _helper_account_handler_service__WEBPACK_IMPORTED_MODULE_3__["AccountHandlerService"],
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__["Title"],
            _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_7__["NgbModal"],
            _helper_error_handler_service__WEBPACK_IMPORTED_MODULE_4__["ErrorHandlerService"],
            _helper_shared_service__WEBPACK_IMPORTED_MODULE_6__["SharedService"],
            _helper_modal_definition_service__WEBPACK_IMPORTED_MODULE_9__["ModalDefinitionService"]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm5/animations.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _components_common_components_header_header_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./components/common-components/header/header.component */ "./src/app/components/common-components/header/header.component.ts");
/* harmony import */ var _components_common_components_footer_footer_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./components/common-components/footer/footer.component */ "./src/app/components/common-components/footer/footer.component.ts");
/* harmony import */ var _components_common_components_main_header_main_header_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./components/common-components/main-header/main-header.component */ "./src/app/components/common-components/main-header/main-header.component.ts");
/* harmony import */ var _components_common_components_confirm_section_confirm_section_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./components/common-components/confirm-section/confirm-section.component */ "./src/app/components/common-components/confirm-section/confirm-section.component.ts");
/* harmony import */ var _components_aisp_components_static_information_static_information_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./components/aisp-components/static-information/static-information.component */ "./src/app/components/aisp-components/static-information/static-information.component.ts");
/* harmony import */ var _components_aisp_components_permissions_permissions_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./components/aisp-components/permissions/permissions.component */ "./src/app/components/aisp-components/permissions/permissions.component.ts");
/* harmony import */ var _components_aisp_components_accounts_accounts_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./components/aisp-components/accounts/accounts.component */ "./src/app/components/aisp-components/accounts/accounts.component.ts");
/* harmony import */ var _components_aisp_components_aisp_aisp_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./components/aisp-components/aisp/aisp.component */ "./src/app/components/aisp-components/aisp/aisp.component.ts");
/* harmony import */ var _components_aisp_components_transactions_access_transactions_access_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./components/aisp-components/transactions-access/transactions-access.component */ "./src/app/components/aisp-components/transactions-access/transactions-access.component.ts");
/* harmony import */ var _components_common_components_scroll_arrow_scroll_arrow_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./components/common-components/scroll-arrow/scroll-arrow.component */ "./src/app/components/common-components/scroll-arrow/scroll-arrow.component.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _pipes_account_digits_filter_pipe__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./pipes/account-digits-filter.pipe */ "./src/app/pipes/account-digits-filter.pipe.ts");
/* harmony import */ var ngx_pagination__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ngx-pagination */ "./node_modules/ngx-pagination/dist/ngx-pagination.js");
/* harmony import */ var _components_common_components_loading_spinner_loading_spinner_component__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./components/common-components/loading-spinner/loading-spinner.component */ "./src/app/components/common-components/loading-spinner/loading-spinner.component.ts");
/* harmony import */ var _components_common_components_error_error_component__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./components/common-components/error/error.component */ "./src/app/components/common-components/error/error.component.ts");
/* harmony import */ var _components_common_components_modal_popup_modal_popup_component__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ./components/common-components/modal-popup/modal-popup.component */ "./src/app/components/common-components/modal-popup/modal-popup.component.ts");
/* harmony import */ var _components_pisp_components_pisp_account_selection_pisp_account_selection_component__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ./components/pisp-components/pisp-account-selection/pisp-account-selection.component */ "./src/app/components/pisp-components/pisp-account-selection/pisp-account-selection.component.ts");
/* harmony import */ var _components_cisp_components_cisp_account_selection_cisp_account_selection_component__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ./components/cisp-components/cisp-account-selection/cisp-account-selection.component */ "./src/app/components/cisp-components/cisp-account-selection/cisp-account-selection.component.ts");
/* harmony import */ var _components_pisp_components_show_account_details_show_account_details_component__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! ./components/pisp-components/show-account-details/show-account-details.component */ "./src/app/components/pisp-components/show-account-details/show-account-details.component.ts");

// module imports





// component imports












// pipe imports

// pagination import







var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_6__["AppComponent"],
                _components_common_components_header_header_component__WEBPACK_IMPORTED_MODULE_7__["HeaderComponent"],
                _components_common_components_footer_footer_component__WEBPACK_IMPORTED_MODULE_8__["FooterComponent"],
                _components_common_components_main_header_main_header_component__WEBPACK_IMPORTED_MODULE_9__["MainHeaderComponent"],
                _components_common_components_confirm_section_confirm_section_component__WEBPACK_IMPORTED_MODULE_10__["ConfirmSectionComponent"],
                _components_aisp_components_static_information_static_information_component__WEBPACK_IMPORTED_MODULE_11__["StaticInformationComponent"],
                _components_aisp_components_permissions_permissions_component__WEBPACK_IMPORTED_MODULE_12__["PermissionsComponent"],
                _components_aisp_components_accounts_accounts_component__WEBPACK_IMPORTED_MODULE_13__["AccountsComponent"],
                _components_aisp_components_aisp_aisp_component__WEBPACK_IMPORTED_MODULE_14__["AispComponent"],
                _components_aisp_components_transactions_access_transactions_access_component__WEBPACK_IMPORTED_MODULE_15__["TransactionsAccessComponent"],
                _pipes_account_digits_filter_pipe__WEBPACK_IMPORTED_MODULE_18__["AccountDigitsFilterPipe"],
                _components_common_components_loading_spinner_loading_spinner_component__WEBPACK_IMPORTED_MODULE_20__["LoadingSpinnerComponent"],
                _components_common_components_error_error_component__WEBPACK_IMPORTED_MODULE_21__["ErrorComponent"],
                _components_common_components_modal_popup_modal_popup_component__WEBPACK_IMPORTED_MODULE_22__["ModalPopupComponent"],
                _components_common_components_scroll_arrow_scroll_arrow_component__WEBPACK_IMPORTED_MODULE_16__["ScrollArrowComponent"],
                _components_pisp_components_pisp_account_selection_pisp_account_selection_component__WEBPACK_IMPORTED_MODULE_23__["PispAccountSelectionComponent"],
                _components_cisp_components_cisp_account_selection_cisp_account_selection_component__WEBPACK_IMPORTED_MODULE_24__["CispAccountSelectionComponent"],
                _components_pisp_components_show_account_details_show_account_details_component__WEBPACK_IMPORTED_MODULE_25__["ShowAccountDetailsComponent"],
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"], ngx_pagination__WEBPACK_IMPORTED_MODULE_19__["NgxPaginationModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_17__["HttpClientModule"], _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_4__["BrowserAnimationsModule"], _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_5__["NgbModule"]
            ],
            providers: [],
            entryComponents: [_components_common_components_modal_popup_modal_popup_component__WEBPACK_IMPORTED_MODULE_22__["ModalPopupComponent"]],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_6__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/components/aisp-components/accounts/accounts.component.html":
/*!*****************************************************************************!*\
  !*** ./src/app/components/aisp-components/accounts/accounts.component.html ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"table-header-container margin-bottom-20 select-accounts\">\n  <div class=\"header-bar-primary\">\n    <span><span class=\"account-label\" *ngIf=\"!(this.fetchService.renewalAuthorisation);else renewalHeading\" [innerHTML]=\"selectAccountTable.SELECT_ACCOUNT_HEADER\"></span>\n      <ng-template #renewalHeading><span class=\"account-label\" [innerHTML]=\"renewHeading\"></span></ng-template>\n    </span>\n    <button type=\"button\" role=\"button\" *ngIf=\"!this.fetchService.renewalAuthorisation\" class=\"btn btn-default btn-tertiary float-right\" [innerHTML]=\"selectButtonLabel\" (click) = \"selectAllBtn()\"></button>\n  </div>\n  <div class=\"details-container select-accounts\">\n    <div class=\"details-table margin-bottom-20\">\n      <div class=\"details-row font-ikanosans-bold table-border-bottom\">\n        <div class=\"details-colomn\">\n          <div class=\"custom-checkbox-wrap\" *ngIf=\"!(this.fetchService.renewalAuthorisation)\">\n            <input type=\"checkbox\" id='select-all-checkbox' name=\"accountCol\" class=\"account-check-box\" [(ngModel)] = \"selectAllAccounts\" (change)=\"checkAccountSelection(null)\" />\n            <label class=\"custom-checkbox\" aria-label=\"Select all\" for=\"select-all-checkbox\"></label>\n            <!--  -->\n          </div>\n          <label class=\"mobile-only\" for=\"select-all-checkbox\"><span [innerHTML]=\"selectButtonLabel\"></span></label>\n          </div>\n        <div class=\"details-colomn\" [innerHTML]='selectAccountTable.NAME_COLUMN_HEADER'></div>\n        <div class=\"details-colomn mobile-only\" [innerHTML]=\"selectAccountTable.CURRENCY_COLUMN_HEADER\"></div>\n        <div class=\"details-colomn\" [innerHTML]=\"selectAccountTable.ACCOUNT_NUMBER_COLUMN_HEADER\"></div>\n        <div class=\"details-colomn non-mobile\" [innerHTML]=\"selectAccountTable.CURRENCY_COLUMN_HEADER\"></div>\n        <div class=\"details-colomn\" [innerHTML]='selectAccountTable.ACCOUNT_TYPE_COLUMN_HEADER'></div>\n      </div>\n      <!-- loop -->\n      <div class=\"details-row\" *ngFor=\"let account of psuAccountsArr | paginate : {\n        itemsPerPage: accountsPerPage,\n        currentPage : page,\n        totalItems : this.psuAccountsArr.length\n      };index as i\"> \n        <div class=\"details-colomn\">\n          <div class=\"custom-checkbox-wrap\" *ngIf=\"!(this.fetchService.renewalAuthorisation)\">\n            <input type=\"checkbox\" *ngIf=\"!this.fetchService.renewalAuthorisation\" name=\"account\" [id]=\"i\" [(ngModel)] = \"account.isSelected\" (change)=\"checkAccountSelection(account)\">\n            <label class=\"custom-checkbox\" [attr.aria-label]='account.Account[0].Identification | accountDigitsFilter' [attr.for]=\"i\"></label>\n          </div>\n        </div>\n        <div class=\"details-colomn\" [innerHTML]='account.Nickname'></div>\n        <div class=\"details-colomn mobile-only\" [innerHTML]='account.Currency'></div>\n        <div class=\"details-colomn\"><label class=\"mobile-only\"><span [innerHTML]=\"selectAccountTable.ACCOUNT_NUMBER_COLUMN_HEADER\"></span></label><span [innerHTML]='account.Account[0].Identification | accountDigitsFilter'></span></div>\n        <div class=\"details-colomn non-mobile\" [innerHTML]='account.Currency'></div>\n        <div class=\"details-colomn\" [innerHTML]='account.AccountSubType'></div>\n      </div>\n     </div>\n     <pagination-controls (pageChange)=\" page = $event\" class=\"account-pagination\" \n      [maxSize]=\"maxPageNo\"\n      previousLabel = \"\"\n      nextLabel = \"\"\n      screenReaderCurrentLabel = \"You're on page\"\n      ></pagination-controls>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/components/aisp-components/accounts/accounts.component.scss":
/*!*****************************************************************************!*\
  !*** ./src/app/components/aisp-components/accounts/accounts.component.scss ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvYWlzcC1jb21wb25lbnRzL2FjY291bnRzL2FjY291bnRzLmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/components/aisp-components/accounts/accounts.component.ts":
/*!***************************************************************************!*\
  !*** ./src/app/components/aisp-components/accounts/accounts.component.ts ***!
  \***************************************************************************/
/*! exports provided: AccountsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AccountsComponent", function() { return AccountsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_helper_static_content_fetch_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/helper/static-content-fetch.service */ "./src/app/helper/static-content-fetch.service.ts");
/* harmony import */ var src_app_helper_account_handler_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/helper/account-handler.service */ "./src/app/helper/account-handler.service.ts");




var AccountsComponent = /** @class */ (function () {
    function AccountsComponent(fetchService, accountService) {
        this.fetchService = fetchService;
        this.accountService = accountService;
        this.selectAllAccounts = false;
        this.page = 1;
        this.accountsPerPage = 10;
        this.maxPageNo = 5;
    }
    AccountsComponent.prototype.ngOnInit = function () {
        this.renewHeading = this.fetchService.getStaticContent(this.fetchService.consentType, 'REVIEW_CONFIRM_PAGE', 'SELECTED_ACCOUNT_TABLE', 'SELECTED_ACCOUNT_HEADER');
        this.selectAccountTable = this.fetchService.getStaticContent(this.fetchService.consentType, 'ACCOUNT_SELECTION_PAGE', 'SELECT_ACCOUNT_TABLE');
        this.selectButtonLabel = this.fetchService.getStaticContent(this.fetchService.consentType, 'ACCOUNT_SELECTION_PAGE', 'BUTTONS', 'SELECT_ALL_BUTTON_LABEL');
        this.selectButtonLabelAll = this.fetchService.getStaticContent(this.fetchService.consentType, 'ACCOUNT_SELECTION_PAGE', 'BUTTONS', 'SELECT_ALL_BUTTON_LABEL');
        this.selectButtonLabelNone = this.fetchService.getStaticContent(this.fetchService.consentType, 'ACCOUNT_SELECTION_PAGE', 'BUTTONS', 'SELECT_NONE_BUTTON_LABEL');
        // setting accounts according to screen width:
        // this.checkAccountSelection(true);
        if (window.innerWidth >= 320 && window.innerWidth < 768) {
            this.accountsPerPage = 4;
        }
        this.psuAccountsArr = this.accountService.psuAccounts;
        for (var _i = 0, _a = this.psuAccountsArr; _i < _a.length; _i++) {
            var account = _a[_i];
            account.isSelected = false;
        }
        this.updateAccountData();
    };
    AccountsComponent.prototype.ngDoCheck = function () {
        this.checkAccountSelection(true);
    };
    AccountsComponent.prototype.checkAccountSelection = function (state) {
        if (state === void 0) { state = null; }
        var totalPages = Math.ceil(this.psuAccountsArr.length / this.accountsPerPage);
        if (state) {
            var acctPerPage = void 0;
            if (this.page < totalPages) {
                acctPerPage = this.accountsPerPage;
                this.selectAccounts(this.accountsPerPage * (this.page - 1), this.accountsPerPage * this.page, acctPerPage);
            }
            else {
                acctPerPage =
                    this.psuAccountsArr.length - this.accountsPerPage * (this.page - 1);
                this.selectAccounts(this.accountsPerPage * (this.page - 1), this.accountsPerPage * (this.page + 1), acctPerPage);
            }
        }
        else {
            if (this.page < totalPages) {
                this.selectAllAccountsOnBtn(this.accountsPerPage * (this.page - 1), this.accountsPerPage * this.page);
            }
            else {
                this.selectAllAccountsOnBtn(this.accountsPerPage * (this.page - 1), this.accountsPerPage * (this.page + 1));
            }
        }
        this.filterSelectedAccounts();
    };
    AccountsComponent.prototype.selectAccounts = function (fromPage, toPage, acctsPerPage) {
        var i = 0;
        for (var _i = 0, _a = this.psuAccountsArr.slice(fromPage, toPage); _i < _a.length; _i++) {
            var account = _a[_i];
            if (!account.isSelected) {
                this.selectAllAccounts = false;
                break;
            }
            i++;
        }
        if (i === acctsPerPage) {
            this.selectAllAccounts = true;
            this.selectButtonLabel = this.selectButtonLabelNone;
        }
        else {
            this.selectButtonLabel = this.selectButtonLabelAll;
        }
    };
    AccountsComponent.prototype.selectAllAccountsOnBtn = function (fromPage, toPage) {
        for (var _i = 0, _a = this.psuAccountsArr.slice(fromPage, toPage); _i < _a.length; _i++) {
            var account = _a[_i];
            account.isSelected = this.selectAllAccounts;
            if (this.selectAllAccounts) {
                this.selectButtonLabel = this.selectButtonLabelNone;
            }
            else {
                this.selectButtonLabel = this.selectButtonLabelAll;
            }
        }
    };
    AccountsComponent.prototype.selectAllBtn = function () {
        this.selectAllAccounts = !this.selectAllAccounts;
        this.checkAccountSelection(null);
    };
    AccountsComponent.prototype.filterSelectedAccounts = function () {
        var _this = this;
        console.log('ab' + (JSON.stringify(this.accountService)));
        while (this.accountService.selectedAccounts.length > 0) {
            this.accountService.selectedAccounts.pop();
        }
        this.psuAccountsArr.forEach(function (account) {
            if (account.isSelected) {
                _this.accountService.selectedAccounts.push(account);
            }
        });
    };
    AccountsComponent.prototype.updateAccountData = function () {
        for (var _i = 0, _a = this.psuAccountsArr; _i < _a.length; _i++) {
            var account = _a[_i];
            if (Array.isArray(account.Account)) {
                account.Account = account.Account.slice(0, 1);
            }
            else {
                account.Account = [account.Account];
            }
        }
    };
    AccountsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-accounts',
            template: __webpack_require__(/*! ./accounts.component.html */ "./src/app/components/aisp-components/accounts/accounts.component.html"),
            styles: [__webpack_require__(/*! ./accounts.component.scss */ "./src/app/components/aisp-components/accounts/accounts.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_helper_static_content_fetch_service__WEBPACK_IMPORTED_MODULE_2__["StaticContentFetchService"],
            src_app_helper_account_handler_service__WEBPACK_IMPORTED_MODULE_3__["AccountHandlerService"]])
    ], AccountsComponent);
    return AccountsComponent;
}());



/***/ }),

/***/ "./src/app/components/aisp-components/aisp/aisp.component.html":
/*!*********************************************************************!*\
  !*** ./src/app/components/aisp-components/aisp/aisp.component.html ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-main-header *ngIf=!this.fetchService.serverErrorFlag></app-main-header>\n<app-accounts *ngIf=\"!this.fetchService.serverErrorFlag\"></app-accounts>\n<app-permissions *ngIf=\"!this.fetchService.serverErrorFlag\"></app-permissions>\n<app-transactions-access *ngIf=\"!this.fetchService.serverErrorFlag\"></app-transactions-access>\n<app-error aria-atomic=\"true\"></app-error>\n<app-confirm-section></app-confirm-section>\n<app-static-information *ngIf=\"!this.fetchService.serverErrorFlag\"></app-static-information>\n<app-scroll-arrow *ngIf=\"!this.fetchService.serverErrorFlag\"></app-scroll-arrow>\n"

/***/ }),

/***/ "./src/app/components/aisp-components/aisp/aisp.component.scss":
/*!*********************************************************************!*\
  !*** ./src/app/components/aisp-components/aisp/aisp.component.scss ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvYWlzcC1jb21wb25lbnRzL2Fpc3AvYWlzcC5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/components/aisp-components/aisp/aisp.component.ts":
/*!*******************************************************************!*\
  !*** ./src/app/components/aisp-components/aisp/aisp.component.ts ***!
  \*******************************************************************/
/*! exports provided: AispComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AispComponent", function() { return AispComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_helper_static_content_fetch_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/helper/static-content-fetch.service */ "./src/app/helper/static-content-fetch.service.ts");



var AispComponent = /** @class */ (function () {
    function AispComponent(fetchService) {
        this.fetchService = fetchService;
    }
    AispComponent.prototype.ngOnInit = function () {
    };
    AispComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-aisp',
            template: __webpack_require__(/*! ./aisp.component.html */ "./src/app/components/aisp-components/aisp/aisp.component.html"),
            styles: [__webpack_require__(/*! ./aisp.component.scss */ "./src/app/components/aisp-components/aisp/aisp.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_helper_static_content_fetch_service__WEBPACK_IMPORTED_MODULE_2__["StaticContentFetchService"]])
    ], AispComponent);
    return AispComponent;
}());



/***/ }),

/***/ "./src/app/components/aisp-components/permissions/permissions.component.html":
/*!***********************************************************************************!*\
  !*** ./src/app/components/aisp-components/permissions/permissions.component.html ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"table-header-container margin-bottom-20 request-permissions\">\n  <div class=\"header-bar-primary\">Requested permissions</div>\n  <div class=\"details-container padding-top-bottom-15\">\n    <div class=\"accordion\" id=\"permissionsReq\">\n      <ng-container *ngFor=\"let i of permissionGroupsKeys\">\n        <div *ngIf=\"permissionGroups[i].data.length\" class=\"card\">\n          <div class=\"card-header\">\n            <button\n              class=\"btn btn-link\"\n              role=\"button\"\n              (click)=\"showDetail(i)\"\n              [ngClass] = \"{'collapsed' : permissionGroups[i].state }\"\n              [attr.aria-expanded]=\"permissionGroups[i].state\">\n              <span class=\"icon-plus-minus\"></span>\n              <span\n                [innerHTML]=\"accountPermissions[i]\"\n              ></span>\n            </button>\n          </div>\n          <div class=\"collapsed show\" *ngIf=\"permissionGroups[i].state\">\n            <div class=\"card-body\">\n              \n                <ul *ngIf=\"\n                      permissionGroups[i].state &&\n                      permissionGroups[i].data.length !== 0\n                    \"\n                    class=\"permission-description\">\n                    <ng-container *ngFor=\"let temp of permissionGroups[i].data\">\n                    <li\n                      [innerHTML]=\"\n                        accountPermissionDescription[\n                          (temp | uppercase) + '_ELEMENTS'\n                        ]\n                      \"\n                    ></li>\n                  </ng-container>\n                </ul>\n            </div>\n          </div>\n        </div>\n      </ng-container>\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/components/aisp-components/permissions/permissions.component.scss":
/*!***********************************************************************************!*\
  !*** ./src/app/components/aisp-components/permissions/permissions.component.scss ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvYWlzcC1jb21wb25lbnRzL3Blcm1pc3Npb25zL3Blcm1pc3Npb25zLmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/components/aisp-components/permissions/permissions.component.ts":
/*!*********************************************************************************!*\
  !*** ./src/app/components/aisp-components/permissions/permissions.component.ts ***!
  \*********************************************************************************/
/*! exports provided: PermissionsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PermissionsComponent", function() { return PermissionsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_helper_static_content_fetch_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/helper/static-content-fetch.service */ "./src/app/helper/static-content-fetch.service.ts");
/* harmony import */ var src_app_helper_account_handler_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/helper/account-handler.service */ "./src/app/helper/account-handler.service.ts");




var PermissionsComponent = /** @class */ (function () {
    function PermissionsComponent(fetchService, accountService) {
        this.fetchService = fetchService;
        this.accountService = accountService;
        this.permissionListData = [];
        this.accountDetail = false;
        this.regularPayment = false;
        this.accountTransactions = false;
        this.statements = false;
        this.accountFeature = false;
        this.permissionGroups = {};
        this.permissionGroupsKeys = [];
    }
    PermissionsComponent.prototype.showDetail = function (temp) {
        this.permissionGroups[temp].state = !this.permissionGroups[temp].state;
    };
    PermissionsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.accountPermissionHeader = this.fetchService.getStaticContent(this.fetchService.consentType, 'REVIEW_CONFIRM_PAGE', 'PERMISSIONS', 'PERMISSION_HEADER');
        this.accountRequestPermissions = this.accountService.accountRequest.Data.Permissions;
        this.accountPermissions = this.fetchService.getStaticContent(this.fetchService.consentType, 'REVIEW_CONFIRM_PAGE', 'PERMISSIONS', 'PERMISSION_LABEL');
        this.accountPermissionDescription = this.fetchService.getStaticContent(this.fetchService.consentType, 'REVIEW_CONFIRM_PAGE', 'PERMISSIONS', 'PERMISSION_DESCRIPTIONS');
        for (var i = 1; i <= 5; i++) {
            this.permissionGroups['HEADING' + i] = { state: false, data: [] };
        }
        this.permissionGroupsKeys = Object.keys(this.permissionGroups);
        this.accountRequestPermissions.forEach(function (element) {
            if (element !== 'ReadTransactionsCredits' &&
                element !== 'ReadTransactionsDebits') {
                if (element === 'ReadTransactionsBasic' || element === 'ReadTransactionsDetail') {
                    if (_this.accountRequestPermissions.indexOf('ReadTransactionsCredits') !== -1 &&
                        _this.accountRequestPermissions.indexOf('ReadTransactionsDebits') !==
                            -1) {
                        element = 'All' + element;
                    }
                    else if (_this.accountRequestPermissions.indexOf('ReadTransactionsCredits') !== -1) {
                        element = 'ReadTransactionsCreditsBasic';
                    }
                    else {
                        element = 'ReadTransactionsDebitsBasic';
                    }
                }
                _this.permissionListData.push(element);
            }
        });
        this.permissionListData.forEach(function (element1) {
            if (element1 === 'ReadAccountsBasic' ||
                element1 === 'ReadAccountsDetail' ||
                element1 === 'ReadBalances' ||
                element1 === 'ReadPAN') {
                _this.permissionGroups["HEADING1"].data.push(element1);
            }
            else if (element1 === 'ReadBeneficiariesBasic' ||
                element1 === 'ReadBeneficiariesDetail' ||
                element1 === 'ReadStandingOrdersBasic' ||
                element1 === 'ReadStandingOrdersDetail' ||
                element1 === 'ReadDirectDebits' ||
                element1 === 'ReadScheduledPaymentsBasic' ||
                element1 === 'ReadScheduledPaymentsDetail') {
                _this.permissionGroups["HEADING2"].data.push(element1);
            }
            else if (element1 === 'AllReadTransactionsBasic' ||
                element1 === 'ReadTransactionsCreditsBasic' ||
                element1 === 'ReadTransactionsDebitsBasic' ||
                element1 === 'AllReadTransactionsDetail' ||
                element1 === 'ReadTransactionsCreditsDetail' ||
                element1 === 'ReadTransactionsDebitsDetail') {
                _this.permissionGroups["HEADING3"].data.push(element1);
            }
            else if (element1 === 'ReadStatementsBasic' ||
                element1 === 'ReadStatementsDetail') {
                _this.permissionGroups["HEADING4"].data.push(element1);
            }
            else if (element1 === 'ReadProducts') {
                _this.permissionGroups["HEADING5"].data.push(element1);
            }
        });
    };
    PermissionsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-permissions',
            template: __webpack_require__(/*! ./permissions.component.html */ "./src/app/components/aisp-components/permissions/permissions.component.html"),
            styles: [__webpack_require__(/*! ./permissions.component.scss */ "./src/app/components/aisp-components/permissions/permissions.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_helper_static_content_fetch_service__WEBPACK_IMPORTED_MODULE_2__["StaticContentFetchService"],
            src_app_helper_account_handler_service__WEBPACK_IMPORTED_MODULE_3__["AccountHandlerService"]])
    ], PermissionsComponent);
    return PermissionsComponent;
}());



/***/ }),

/***/ "./src/app/components/aisp-components/static-information/static-information.component.html":
/*!*************************************************************************************************!*\
  !*** ./src/app/components/aisp-components/static-information/static-information.component.html ***!
  \*************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<hr />\n<div class=\"table-header-container important-info\">\n  <div class=\"details-container padding-top-bottom-15\">\n    <div class=\"box-container box-container-primary\">\n      <div class=\"help-list help-list-with-title\"> \n        <ng-container *ngFor=\"let index of staticInformationKeys\">\n          <h5 class=\"text-label-semibold text-quinary\" *ngIf=\"staticInformation[index] != '' && index.indexOf('HEADING')!==-1\"\n            [innerHTML]=\"staticInformation[index]\">\n          </h5>\n          <p class=\"text-label-sm-light text-quaternary\" *ngIf=\"staticInformation[index] !='' && index.indexOf('DESCRIPTION')!==-1\"\n            [innerHTML]=\"staticInformation[index]\">\n          </p>\n        </ng-container>\n      </div>\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/components/aisp-components/static-information/static-information.component.scss":
/*!*************************************************************************************************!*\
  !*** ./src/app/components/aisp-components/static-information/static-information.component.scss ***!
  \*************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvYWlzcC1jb21wb25lbnRzL3N0YXRpYy1pbmZvcm1hdGlvbi9zdGF0aWMtaW5mb3JtYXRpb24uY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/components/aisp-components/static-information/static-information.component.ts":
/*!***********************************************************************************************!*\
  !*** ./src/app/components/aisp-components/static-information/static-information.component.ts ***!
  \***********************************************************************************************/
/*! exports provided: StaticInformationComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StaticInformationComponent", function() { return StaticInformationComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_helper_static_content_fetch_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/helper/static-content-fetch.service */ "./src/app/helper/static-content-fetch.service.ts");



var StaticInformationComponent = /** @class */ (function () {
    function StaticInformationComponent(fetchService) {
        this.fetchService = fetchService;
    }
    StaticInformationComponent.prototype.ngOnInit = function () {
        this.consentType = this.fetchService.consentType;
        this.staticInformation = this.fetchService.getStaticContent(this.fetchService.consentType, 'REVIEW_CONFIRM_PAGE', 'IMPORTANT_INFORMATION');
        this.staticInformationKeys = Object.keys(this.staticInformation);
    };
    StaticInformationComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-static-information',
            template: __webpack_require__(/*! ./static-information.component.html */ "./src/app/components/aisp-components/static-information/static-information.component.html"),
            styles: [__webpack_require__(/*! ./static-information.component.scss */ "./src/app/components/aisp-components/static-information/static-information.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_helper_static_content_fetch_service__WEBPACK_IMPORTED_MODULE_2__["StaticContentFetchService"]])
    ], StaticInformationComponent);
    return StaticInformationComponent;
}());



/***/ }),

/***/ "./src/app/components/aisp-components/transactions-access/transactions-access.component.html":
/*!***************************************************************************************************!*\
  !*** ./src/app/components/aisp-components/transactions-access/transactions-access.component.html ***!
  \***************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"table-header-container margin-bottom-20 request-transaction\">\n  <div class=\"header-bar-primary\" [innerHTML]='this.reviewConfirm.TRANSACTION_PERMISSION.TRANSACTION_ACCESS_DATE_HEADER'></div>\n  <div class=\"padding-20\">\n    <div class=\"row\">\n      <div class=\"col-md-6 col-sm-6\">\n        <span [innerHTML]='this.reviewConfirm.TRANSACTION_PERMISSION.FROM_DATE_LABEL'></span> <span class=\"font-ikanosans-bold\" [innerHTML]=\"this.accountRequest.Data.TransactionFromDateTime ? (this.accountRequest.Data.TransactionFromDateTime | date:'dd MMMM yyyy') : invalidFromToLabel \"></span>\n      </div>\n      <div class=\"col-md-6 col-sm-6\">\n        <span [innerHTML]='this.reviewConfirm.TRANSACTION_PERMISSION.TO_DATE_LABEL'></span> <span class=\"font-ikanosans-bold\" [innerHTML]=\"this.accountRequest.Data.TransactionToDateTime ? (this.accountRequest.Data.TransactionToDateTime | date:'dd MMMM yyyy') : invalidFromToLabel\"></span>\n      </div>\n    </div>\n  </div>\n</div>\n<div class=\"table-header-container margin-bottom-20\">\n  <div class=\"details-container padding-top-bottom-15\">\n    <span *ngIf=\"expiryDate\" [innerHTML]='this.reviewConfirm.CONSENT_VALIDITY.PRE_TILL_DATE_LABEL'></span>&nbsp;\n    <span [innerHTML]=\"expiryDate ? (expiryDate | date:'dd MMMM yyyy') : invalidExpiryLabel\"></span>&nbsp;\n    <span *ngIf=\"expiryDate\" [innerHTML]='this.reviewConfirm.CONSENT_VALIDITY.POST_TILL_DATE_LABEL'></span>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/components/aisp-components/transactions-access/transactions-access.component.scss":
/*!***************************************************************************************************!*\
  !*** ./src/app/components/aisp-components/transactions-access/transactions-access.component.scss ***!
  \***************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvYWlzcC1jb21wb25lbnRzL3RyYW5zYWN0aW9ucy1hY2Nlc3MvdHJhbnNhY3Rpb25zLWFjY2Vzcy5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/components/aisp-components/transactions-access/transactions-access.component.ts":
/*!*************************************************************************************************!*\
  !*** ./src/app/components/aisp-components/transactions-access/transactions-access.component.ts ***!
  \*************************************************************************************************/
/*! exports provided: TransactionsAccessComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TransactionsAccessComponent", function() { return TransactionsAccessComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_helper_static_content_fetch_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/helper/static-content-fetch.service */ "./src/app/helper/static-content-fetch.service.ts");
/* harmony import */ var src_app_helper_account_handler_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/helper/account-handler.service */ "./src/app/helper/account-handler.service.ts");




var TransactionsAccessComponent = /** @class */ (function () {
    function TransactionsAccessComponent(fetchService, accountService) {
        this.fetchService = fetchService;
        this.accountService = accountService;
    }
    TransactionsAccessComponent.prototype.ngOnInit = function () {
        this.reviewConfirm = this.fetchService.getStaticContent(this.fetchService.consentType, 'REVIEW_CONFIRM_PAGE');
        this.accountRequest = this.accountService.accountRequest;
        this.accountRequest.Data.TransactionFromDateTime =
            this.accountRequest.Data.TransactionFromDateTime !== ''
                ? this.accountRequest.Data.TransactionFromDateTime.split('T')[0]
                : null;
        this.accountRequest.Data.TransactionToDateTime =
            this.accountRequest.Data.TransactionToDateTime !== ''
                ? this.accountRequest.Data.TransactionToDateTime.split('T')[0]
                : null;
        this.expiryDate = this.accountService.accountRequest.Data.ExpirationDateTime;
        this.expiryDate = this.expiryDate ? this.expiryDate.split('T')[0] : null;
        this.invalidFromToLabel = this.fetchService.getStaticContent(this.fetchService.consentType, 'REVIEW_CONFIRM_PAGE', 'TRANSACTION_PERMISSION', 'NO_CONSENT_DATE_LABEL');
        this.invalidExpiryLabel = this.fetchService.getStaticContent(this.fetchService.consentType, 'REVIEW_CONFIRM_PAGE', 'CONSENT_VALIDITY', 'EXPIRY_INVALID_DATE_LABEL');
    };
    TransactionsAccessComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-transactions-access',
            template: __webpack_require__(/*! ./transactions-access.component.html */ "./src/app/components/aisp-components/transactions-access/transactions-access.component.html"),
            styles: [__webpack_require__(/*! ./transactions-access.component.scss */ "./src/app/components/aisp-components/transactions-access/transactions-access.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_helper_static_content_fetch_service__WEBPACK_IMPORTED_MODULE_2__["StaticContentFetchService"],
            src_app_helper_account_handler_service__WEBPACK_IMPORTED_MODULE_3__["AccountHandlerService"]])
    ], TransactionsAccessComponent);
    return TransactionsAccessComponent;
}());



/***/ }),

/***/ "./src/app/components/cisp-components/cisp-account-selection/cisp-account-selection.component.html":
/*!*********************************************************************************************************!*\
  !*** ./src/app/components/cisp-components/cisp-account-selection/cisp-account-selection.component.html ***!
  \*********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-main-header *ngIf=!this.fetchService.serverErrorFlag></app-main-header> \n<table *ngIf=\"!this.errorOnLoad\" class=\"table table-striped card-detail-viewer\">\n  <thead class=\"header-bar-primary\">\n  <tr><th colspan=\"2\" >Request details</th></tr>\n  </thead>\n \n    <tr>\n      <th>Request Initiated By</th>\n      <td [innerHTML]=\"cispDisplay.RequestInitiater\"></td>\n    </tr>\n\n    <tr>\n      <th>Account Number</th>\n      <td [innerHTML]=\"cispDisplay.ConsentAcctNo | accountDigitsFilter\"></td>\n    </tr>\n\n    <tr>\n      <th>Consent valid until</th>\n      <td [innerHTML]=\"cispDisplay.ConsentValidUntil | date:'dd MMMM yyyy' \"></td>\n    </tr>\n        \n  </tbody>\n</table>\n<app-error aria-atomic=\"true\"></app-error>\n<app-confirm-section></app-confirm-section>\n"

/***/ }),

/***/ "./src/app/components/cisp-components/cisp-account-selection/cisp-account-selection.component.scss":
/*!*********************************************************************************************************!*\
  !*** ./src/app/components/cisp-components/cisp-account-selection/cisp-account-selection.component.scss ***!
  \*********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "table tbody th {\n  text-align: right;\n  width: 25%; }\n\n.btn {\n  width: 100%;\n  white-space: normal;\n  text-align: left;\n  border-color: black; }\n\n.fa-chevron-down:before {\n  content: \"\\f078\"; }\n\n.dropdown-toggle::after {\n  float: right;\n  margin-top: 0.6em; }\n\n.pre-class {\n  white-space: pre; }\n\n.dropdown-menu {\n  width: 100%; }\n\n.dropdown-item {\n  display: block; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi92YXIvbGliL2plbmtpbnMvd29ya3NwYWNlL1BTRDItY21hMi1EZXYvQ29uc2VudC1tY3ItMC43LjEtQ2xvdWRGcm9udC1VSS1DSS9zcmMvYXBwL2NvbXBvbmVudHMvY2lzcC1jb21wb25lbnRzL2Npc3AtYWNjb3VudC1zZWxlY3Rpb24vY2lzcC1hY2NvdW50LXNlbGVjdGlvbi5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGlCQUFnQjtFQUNoQixVQUFTLEVBQUE7O0FBRWI7RUFDSSxXQUFXO0VBQ1gsbUJBQW1CO0VBQ25CLGdCQUFlO0VBQ2YsbUJBQWtCLEVBQUE7O0FBRXRCO0VBQ0ksZ0JBQWdCLEVBQUE7O0FBRXBCO0VBQ0EsWUFBVztFQUNYLGlCQUFnQixFQUFBOztBQUVoQjtFQUNJLGdCQUFlLEVBQUE7O0FBRW5CO0VBQ0ksV0FBVSxFQUFBOztBQUVkO0VBQ0ksY0FBYSxFQUFBIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50cy9jaXNwLWNvbXBvbmVudHMvY2lzcC1hY2NvdW50LXNlbGVjdGlvbi9jaXNwLWFjY291bnQtc2VsZWN0aW9uLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsidGFibGUgdGJvZHkgdGh7XG4gICAgdGV4dC1hbGlnbjpyaWdodDtcbiAgICB3aWR0aDoyNSU7XG59XG4uYnRue1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIHdoaXRlLXNwYWNlOiBub3JtYWw7XG4gICAgdGV4dC1hbGlnbjpsZWZ0O1xuICAgIGJvcmRlci1jb2xvcjpibGFjaztcbn1cbi5mYS1jaGV2cm9uLWRvd246YmVmb3JlIHtcbiAgICBjb250ZW50OiBcIlxcZjA3OFwiO1xufVxuLmRyb3Bkb3duLXRvZ2dsZTo6YWZ0ZXJ7XG5mbG9hdDpyaWdodDtcbm1hcmdpbi10b3A6MC42ZW07XG59XG4ucHJlLWNsYXNze1xuICAgIHdoaXRlLXNwYWNlOnByZTtcbn1cbi5kcm9wZG93bi1tZW51e1xuICAgIHdpZHRoOjEwMCU7XG59XG4uZHJvcGRvd24taXRlbXtcbiAgICBkaXNwbGF5OmJsb2NrO1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/components/cisp-components/cisp-account-selection/cisp-account-selection.component.ts":
/*!*******************************************************************************************************!*\
  !*** ./src/app/components/cisp-components/cisp-account-selection/cisp-account-selection.component.ts ***!
  \*******************************************************************************************************/
/*! exports provided: CispAccountSelectionComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CispAccountSelectionComponent", function() { return CispAccountSelectionComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_helper_static_content_fetch_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/helper/static-content-fetch.service */ "./src/app/helper/static-content-fetch.service.ts");
/* harmony import */ var src_app_helper_account_handler_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/helper/account-handler.service */ "./src/app/helper/account-handler.service.ts");
/* harmony import */ var src_app_helper_error_handler_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/helper/error-handler.service */ "./src/app/helper/error-handler.service.ts");





var CispAccountSelectionComponent = /** @class */ (function () {
    function CispAccountSelectionComponent(accountService, fetchService, errorService) {
        this.accountService = accountService;
        this.fetchService = fetchService;
        this.errorService = errorService;
        this.cispDisplay = {};
        this.consentAccountNumber = "-";
        this.errorOnLoad = false;
    }
    CispAccountSelectionComponent.prototype.ngOnInit = function () {
        if (this.errorService.error) {
            this.errorOnLoad = true;
        }
        if (this.fetchService.tppInfo && this.fetchService.tppInfo.applicationName) {
            this.cispDisplay["RequestInitiater"] = this.fetchService.tppInfo.applicationName;
            this.cispDisplay["RequestInitiater"] += this.fetchService.tppInfo.tppName ? "( " + this.fetchService.tppInfo.tppName + " )" : "";
        }
        if (this.accountService.accountRequest && this.accountService.accountRequest.Data && this.accountService.accountRequest.Data.ExpirationDateTime) {
            this.cispDisplay["ConsentValidUntil"] = this.accountService.accountRequest.Data.ExpirationDateTime;
        }
        if (this.accountService.psuAccounts) {
            if (this.accountService.psuAccounts.length > 0 && this.accountService.psuAccounts[0]) {
                this.consentAccountNumber = this.accountService.psuAccounts[0].Account[0].Identification;
            }
            while (this.accountService.selectedAccounts.length > 0) {
                this.accountService.selectedAccounts.pop();
            }
            this.accountService.selectedAccounts.push(this.accountService.psuAccounts[0]);
        }
        this.cispDisplay["ConsentAcctNo"] = this.consentAccountNumber;
    };
    CispAccountSelectionComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-cisp-account-selection',
            template: __webpack_require__(/*! ./cisp-account-selection.component.html */ "./src/app/components/cisp-components/cisp-account-selection/cisp-account-selection.component.html"),
            styles: [__webpack_require__(/*! ./cisp-account-selection.component.scss */ "./src/app/components/cisp-components/cisp-account-selection/cisp-account-selection.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_helper_account_handler_service__WEBPACK_IMPORTED_MODULE_3__["AccountHandlerService"],
            src_app_helper_static_content_fetch_service__WEBPACK_IMPORTED_MODULE_2__["StaticContentFetchService"],
            src_app_helper_error_handler_service__WEBPACK_IMPORTED_MODULE_4__["ErrorHandlerService"]])
    ], CispAccountSelectionComponent);
    return CispAccountSelectionComponent;
}());



/***/ }),

/***/ "./src/app/components/common-components/confirm-section/confirm-section.component.html":
/*!*********************************************************************************************!*\
  !*** ./src/app/components/common-components/confirm-section/confirm-section.component.html ***!
  \*********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--  -->\n<div class=\"table-header-container margin-bottom-20 confirm-section primary-font-bold position-relative\" [ngClass] = \"{'confirm-section-checked' : isConfirm }\" *ngIf=\"!(this.refreshTokenRenewalFlow || this.fetchService.serverErrorFlag)\">\n  <div class=\"custom-checkbox-wrap margin-right-20\">\n    <input type=\"checkbox\" id=\"confirmCheck\" [(ngModel)]='isConfirm'/>\n    <label class=\"custom-checkbox\" for=\"confirmCheck\"></label>\n  </div>\n  <span [innerHTML]=\"this.confirmSectionData['ACCEPT_CONSENT_REQUEST_LABEL']\"></span>\n</div>\n<!--  -->\n<div class=\"clearfix margin-bottom-20 final-buttons\">\n<div *ngIf=\"!this.refreshTokenRenewalFlow; else renewalBlock\">\n    <button role=\"button\" class=\"btn btn-primary float-right\" *ngIf=\"!this.fetchService.serverErrorFlag\" (click)='consentSubmission()' [disabled]='!(isConfirm && (this.selectedAccounts.length))' \n    [innerHTML]=\"this.confirmSectionData.BUTTONS.CONFIRM_BUTTON_LABEL\"></button>\n    <button role=\"button\" class=\"btn btn-secondary float-left\" (click)=\"cancelConsentConfirmation()\">\n      <span [innerHTML]=\"this.confirmSectionData.BUTTONS.CANCEL_BUTTON_LABEL\"></span>\n      <span class=\"sr-only\" [attr.aria-label]=\"this.confirmSectionData.BUTTONS.PRESS_CANCEL_SCREENREADER_LABEL\"></span>\n    </button>\n  </div>\n  <ng-template #renewalBlock>\n    <button role=\"button\" class=\"btn btn-primary float-right\" *ngIf=\"!this.fetchService.serverErrorFlag\" (click)='consentSubmission()' [innerHTML]=\"this.confirmSectionData.BUTTONS.RENEW_AUTHORISATION_BUTTON_LABEL\"></button>\n    <button role=\"button\" class=\"btn btn-secondary float-left\" (click)=\"cancelConsentConfirmation()\">\n      <span [innerHTML]=\"this.confirmSectionData.BUTTONS.DENY_AUTHORISATION_BUTTON_LABEL\"></span>\n      <span class=\"sr-only\" [attr.aria-label]=\"this.confirmSectionData.BUTTONS.PRESS_DENY_SCREENREADER_LABEL\"></span>\n    </button>\n  </ng-template>\n</div>\n"

/***/ }),

/***/ "./src/app/components/common-components/confirm-section/confirm-section.component.scss":
/*!*********************************************************************************************!*\
  !*** ./src/app/components/common-components/confirm-section/confirm-section.component.scss ***!
  \*********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvY29tbW9uLWNvbXBvbmVudHMvY29uZmlybS1zZWN0aW9uL2NvbmZpcm0tc2VjdGlvbi5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/components/common-components/confirm-section/confirm-section.component.ts":
/*!*******************************************************************************************!*\
  !*** ./src/app/components/common-components/confirm-section/confirm-section.component.ts ***!
  \*******************************************************************************************/
/*! exports provided: ConfirmSectionComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConfirmSectionComponent", function() { return ConfirmSectionComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_http_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/http.service */ "./src/app/services/http.service.ts");
/* harmony import */ var src_app_helper_static_content_fetch_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/helper/static-content-fetch.service */ "./src/app/helper/static-content-fetch.service.ts");
/* harmony import */ var src_app_helper_account_handler_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/helper/account-handler.service */ "./src/app/helper/account-handler.service.ts");
/* harmony import */ var src_app_helper_shared_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/helper/shared.service */ "./src/app/helper/shared.service.ts");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var src_app_components_common_components_modal_popup_modal_popup_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/components/common-components/modal-popup/modal-popup.component */ "./src/app/components/common-components/modal-popup/modal-popup.component.ts");
/* harmony import */ var src_app_helper_error_handler_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/helper/error-handler.service */ "./src/app/helper/error-handler.service.ts");
/* harmony import */ var src_app_helper_modal_definition_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! src/app/helper/modal-definition.service */ "./src/app/helper/modal-definition.service.ts");










var ConfirmSectionComponent = /** @class */ (function () {
    function ConfirmSectionComponent(cdRef, httpService, fetchService, accountService, sharedService, errorService, modalDefService, modalService) {
        this.cdRef = cdRef;
        this.httpService = httpService;
        this.fetchService = fetchService;
        this.accountService = accountService;
        this.sharedService = sharedService;
        this.errorService = errorService;
        this.modalDefService = modalDefService;
        this.modalService = modalService;
        // modalDef object
        this.modalDef = {};
    }
    ConfirmSectionComponent.prototype.ngOnInit = function () {
        this.consentType = this.fetchService.consentType;
        this.selectedAccounts = this.accountService.selectedAccounts;
        this.resumePath = this.fetchService.resumePath;
        this.correlationId = this.fetchService.correlationId;
        this.refreshTokenRenewalFlow = this.fetchService.renewalAuthorisation;
        if (!this.refreshTokenRenewalFlow) {
            this.confirmSectionData = this.fetchService.getStaticContent(this.consentType, 'REVIEW_CONFIRM_PAGE');
        }
        else {
            this.confirmSectionData = this.fetchService.getStaticContent(this.consentType, 'AUTHORISATION_RENEWAL_PAGE');
        }
        this.isConfirm = false;
        this.headers = {};
    };
    ConfirmSectionComponent.prototype.consentSubmission = function () {
        var _this = this;
        this.sharedService.emitShowLoadingSpinner(true);
        // In case of renewal flow we send all the psu accounts in request payload
        if (this.refreshTokenRenewalFlow || (this.accountService.paymentSetup && this.accountService.paymentSetup.debtorDetails)) {
            this.reqData = {
                fsHeaders: this.fetchService.fsHeaders,
                accountDetails: this.accountService.psuAccounts
            };
        }
        else {
            this.reqData = {
                fsHeaders: this.fetchService.fsHeaders,
                accountDetails: this.accountService.selectedAccounts
            };
        }
        this.httpService
            .accountRequest(this.consentType, this.reqData, this.resumePath, this.correlationId, this.refreshTokenRenewalFlow, this.headers)
            .subscribe(function (res) {
            var form = document.createElement('form');
            form.setAttribute('method', 'POST');
            form.setAttribute('action', res.body.model.redirectUri);
            document.body.appendChild(form);
            _this.modalSubmissionResBody = _this.fetchService.getStaticContent('COMMON_SUBMISSION_RESPONSE_POPUP', 'ACCESS_GRANTED_LABEL');
            _this.sharedService.emitShowLoadingSpinner(false);
            var modalInstance = _this.modalService.open(src_app_components_common_components_modal_popup_modal_popup_component__WEBPACK_IMPORTED_MODULE_7__["ModalPopupComponent"], {
                backdrop: 'static',
                keyboard: false
            });
            modalInstance.componentInstance.modalDef = _this.modalDefService.defineModalObj(_this, 'submissionResponse');
            modalInstance.componentInstance.classRef = _this;
            setTimeout(function () { return form.submit(); }, 8000);
        }, function (errorRes) {
            if (errorRes.error.exception &&
                errorRes.error.exception.errorCode === '731') {
                // defining modal definition for session popup
                _this.sharedService.emitShowLoadingSpinner(false);
                var modalInstance = _this.modalService.open(src_app_components_common_components_modal_popup_modal_popup_component__WEBPACK_IMPORTED_MODULE_7__["ModalPopupComponent"], {
                    backdrop: 'static',
                    keyboard: false
                });
                modalInstance.componentInstance.modalDef = _this.modalDefService.defineModalObj(_this, 'session');
                modalInstance.componentInstance.classRef = _this;
                _this.sessionRedirectUri = errorRes.error.redirectUri;
            }
            else {
                var error = errorRes.error.exception
                    ? errorRes.error.exception.errorCode
                    : '800';
                _this.errorService.error = error;
                _this.sharedService.emitShowLoadingSpinner(false);
            }
        });
    };
    ConfirmSectionComponent.prototype.cancelConsentConfirmation = function () {
        // defining modal definition for cancel popup
        var modalInstance = this.modalService.open(src_app_components_common_components_modal_popup_modal_popup_component__WEBPACK_IMPORTED_MODULE_7__["ModalPopupComponent"], {
            backdrop: true,
            keyboard: true
        });
        modalInstance.componentInstance.modalDef = this.modalDefService.defineModalObj(this, 'cancel');
        modalInstance.componentInstance.classRef = this;
    };
    ConfirmSectionComponent.prototype.cancelConsentSubmission = function (classRef) {
        classRef.sharedService.emitShowLoadingSpinner(true);
        classRef.serverErrorFlag = document.querySelector('#serverErrorFlag').value;
        classRef.reqData = null;
        classRef.httpService
            .cancelRequest(classRef.consentType, classRef.reqData, classRef.resumePath, classRef.correlationId, classRef.refreshTokenRenewalFlow, classRef.serverErrorFlag)
            .subscribe(function (res) {
            if (res.status === 200) {
                classRef.modalSubmissionResBody = classRef.fetchService.getStaticContent('COMMON_SUBMISSION_RESPONSE_POPUP', 'ACCESS_DENIED_LABEL');
                classRef.sharedService.emitShowLoadingSpinner(false);
                var modalInstance = classRef.modalService.open(src_app_components_common_components_modal_popup_modal_popup_component__WEBPACK_IMPORTED_MODULE_7__["ModalPopupComponent"], { backdrop: 'static', keyboard: false });
                modalInstance.componentInstance.modalDef = classRef.modalDefService.defineModalObj(classRef, 'submissionResponse');
                modalInstance.componentInstance.classRef = classRef;
                setTimeout(function () { return (window.location.href = res.body.model.redirectUri); }, 8000);
            }
        }, function (errorRes) {
            if (errorRes.error.exception &&
                errorRes.error.exception.errorCode === '731') {
                classRef.sharedService.emitShowLoadingSpinner(false);
                // enter code for session popup handling
                var modalInstance = classRef.modalService.open(src_app_components_common_components_modal_popup_modal_popup_component__WEBPACK_IMPORTED_MODULE_7__["ModalPopupComponent"], { backdrop: 'static', keyboard: false });
                modalInstance.componentInstance.modalDef = classRef.modalDefService.defineModalObj(classRef, 'session');
                modalInstance.componentInstance.classRef = classRef;
                classRef.sessionRedirectUri = errorRes.error.redirectUri;
            }
            else {
                var error = errorRes.error.exception
                    ? errorRes.error.exception.errorCode
                    : '800';
                classRef.errorService.error = error;
                classRef.sharedService.emitShowLoadingSpinner(false);
            }
        });
    };
    ConfirmSectionComponent.prototype.onSessionBtnClick = function (classRef) {
        classRef.sharedService.emitShowLoadingSpinner(false);
        classRef.modalSubmissionResBody = classRef.fetchService.getStaticContent('COMMON_SUBMISSION_RESPONSE_POPUP', 'ACCESS_DENIED_LABEL');
        classRef.sharedService.emitShowLoadingSpinner(false);
        var modalInstance = classRef.modalService.open(src_app_components_common_components_modal_popup_modal_popup_component__WEBPACK_IMPORTED_MODULE_7__["ModalPopupComponent"], {
            backdrop: 'static',
            keyboard: false
        });
        modalInstance.componentInstance.modalDef = classRef.modalDefService.defineModalObj(classRef, 'submissionResponse');
        modalInstance.componentInstance.classRef = classRef;
        setTimeout(function () { return (window.location.href = classRef.sessionRedirectUri); }, 8000);
    };
    ConfirmSectionComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-confirm-section',
            template: __webpack_require__(/*! ./confirm-section.component.html */ "./src/app/components/common-components/confirm-section/confirm-section.component.html"),
            styles: [__webpack_require__(/*! ./confirm-section.component.scss */ "./src/app/components/common-components/confirm-section/confirm-section.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"],
            src_app_services_http_service__WEBPACK_IMPORTED_MODULE_2__["HttpService"],
            src_app_helper_static_content_fetch_service__WEBPACK_IMPORTED_MODULE_3__["StaticContentFetchService"],
            src_app_helper_account_handler_service__WEBPACK_IMPORTED_MODULE_4__["AccountHandlerService"],
            src_app_helper_shared_service__WEBPACK_IMPORTED_MODULE_5__["SharedService"],
            src_app_helper_error_handler_service__WEBPACK_IMPORTED_MODULE_8__["ErrorHandlerService"],
            src_app_helper_modal_definition_service__WEBPACK_IMPORTED_MODULE_9__["ModalDefinitionService"],
            _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_6__["NgbModal"]])
    ], ConfirmSectionComponent);
    return ConfirmSectionComponent;
}());



/***/ }),

/***/ "./src/app/components/common-components/error/error.component.html":
/*!*************************************************************************!*\
  !*** ./src/app/components/common-components/error/error.component.html ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\" errorDataSection\">\n  <div class=\"error alert alert-primary label-secondary\" aria-live=\"assertive\" *ngIf=\"this.serverError.errorCode\">\n    <span class=\"alert-prefix position-relative primary-font-bold color-red\" [innerHTML]=\"this.serverError.type\"></span>: <span [innerHTML]=\"this.serverError.errorMessage\"></span>\n    <span *ngIf=\"this.serverError.errorDetails\" [innerHTML]=\"this.serverError.errorDetails\"></span>\n  </div>\n</div>\n"

/***/ }),

/***/ "./src/app/components/common-components/error/error.component.scss":
/*!*************************************************************************!*\
  !*** ./src/app/components/common-components/error/error.component.scss ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".error {\n  margin: 20px auto;\n  text-align: left; }\n\n.alert-primary {\n  border-color: #ec0000;\n  background-color: #ffffff; }\n\n.label-secondary {\n  color: #34353a; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi92YXIvbGliL2plbmtpbnMvd29ya3NwYWNlL1BTRDItY21hMi1EZXYvQ29uc2VudC1tY3ItMC43LjEtQ2xvdWRGcm9udC1VSS1DSS9zcmMvYXBwL2NvbXBvbmVudHMvY29tbW9uLWNvbXBvbmVudHMvZXJyb3IvZXJyb3IuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxpQkFBaUI7RUFDakIsZ0JBQWdCLEVBQUE7O0FBRXBCO0VBQ0kscUJBQXFCO0VBQ3JCLHlCQUF5QixFQUFBOztBQUU3QjtFQUNJLGNBQWMsRUFBQSIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvY29tbW9uLWNvbXBvbmVudHMvZXJyb3IvZXJyb3IuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuZXJyb3J7XG4gICAgbWFyZ2luOiAyMHB4IGF1dG87XG4gICAgdGV4dC1hbGlnbjogbGVmdDtcbn1cbi5hbGVydC1wcmltYXJ5e1xuICAgIGJvcmRlci1jb2xvcjogI2VjMDAwMDtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmZmZmO1xufVxuLmxhYmVsLXNlY29uZGFyeXtcbiAgICBjb2xvcjogIzM0MzUzYTtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/components/common-components/error/error.component.ts":
/*!***********************************************************************!*\
  !*** ./src/app/components/common-components/error/error.component.ts ***!
  \***********************************************************************/
/*! exports provided: ErrorComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ErrorComponent", function() { return ErrorComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_helper_error_handler_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/helper/error-handler.service */ "./src/app/helper/error-handler.service.ts");
/* harmony import */ var src_app_helper_static_content_fetch_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/helper/static-content-fetch.service */ "./src/app/helper/static-content-fetch.service.ts");




var ErrorComponent = /** @class */ (function () {
    function ErrorComponent(errorService, fetchService) {
        this.errorService = errorService;
        this.fetchService = fetchService;
        this.serverError = {};
        this.noticeCSSClass = 'alert alert-primary label-secondary';
    }
    ErrorComponent.prototype.ngOnInit = function () {
        if (this.errorService.error) {
            this.serverError = this.errorService.handleError();
        }
    };
    ErrorComponent.prototype.ngDoCheck = function () {
        this.serverError = this.errorService.handleError();
    };
    ErrorComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-error',
            template: __webpack_require__(/*! ./error.component.html */ "./src/app/components/common-components/error/error.component.html"),
            styles: [__webpack_require__(/*! ./error.component.scss */ "./src/app/components/common-components/error/error.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_helper_error_handler_service__WEBPACK_IMPORTED_MODULE_2__["ErrorHandlerService"],
            src_app_helper_static_content_fetch_service__WEBPACK_IMPORTED_MODULE_3__["StaticContentFetchService"]])
    ], ErrorComponent);
    return ErrorComponent;
}());



/***/ }),

/***/ "./src/app/components/common-components/footer/footer.component.html":
/*!***************************************************************************!*\
  !*** ./src/app/components/common-components/footer/footer.component.html ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<footer id=\"footer\" class=\"footer-section\">\n  <div class=\"container\">\n    <ul class=\"nav justify-content-end margin-top-20\">\n      <li class=\"nav-item\">\n        <a [href]=\"this.footerContent.ABOUT_US_URL\" class=\"nav-link\" target=\"_blank\">\n          <span [title]=\"this.footerContent.LINK_TOOLTIP_NEW_WINDOW\" [innerHTML]=\"this.footerContent.ABOUT_US_LABEL\"></span>\n          <span class=\"sr-only\" [attr.aria-label]=\"this.footerContent.LINK_TOOLTIP_NEW_WINDOW\"></span>\n        </a>\n      </li>\n      <li class=\"nav-item\">\n        <a [href]=\"this.footerContent.PRIVACY_POLICY_URL\" class=\"nav-link\" target=\"_blank\">\n          <span [title]=\"this.footerContent.LINK_TOOLTIP_NEW_WINDOW\" [innerHTML]=\"this.footerContent.PRIVACY_POLICY_LABEL\"></span>\n          <span class=\"sr-only\" [attr.aria-label]=\"this.footerContent.LINK_TOOLTIP_NEW_WINDOW\"></span>\n        </a>\n      </li>\n      <li class=\"nav-item\">\n        <a [href]=\"this.footerContent.TNC_URL\" class=\"nav-link\" target=\"_blank\">\n          <span [title]=\"this.footerContent.LINK_TOOLTIP_NEW_WINDOW\" [innerHTML]=\"this.footerContent.TNC_LABEL\"></span>\n          <span class=\"sr-only\" [attr.aria-label]=\"this.footerContent.LINK_TOOLTIP_NEW_WINDOW\"></span>\n        </a>\n      </li>\n      <li class=\"nav-item\">\n        <a href=\"javascript:void(0)\" class=\"nav-link\" (click)=\"footerHelp()\">\n          <span [title]=\"this.footerContent.LINK_TOOLTIP_NEW_WINDOW\" [innerHTML]=\"this.footerContent.HELP_LABEL\"></span>\n          <span class=\"sr-only\" [attr.aria-label]=\"this.footerContent.LINK_TOOLTIP_NEW_WINDOW\"></span>\n        </a>\n      </li>\n    </ul>\n    <p class=\"regulatory-statement\" [innerHTML]=\"this.footerContent.REGULATORY_TEXT\"></p>\n  </div>\n</footer>"

/***/ }),

/***/ "./src/app/components/common-components/footer/footer.component.scss":
/*!***************************************************************************!*\
  !*** ./src/app/components/common-components/footer/footer.component.scss ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvY29tbW9uLWNvbXBvbmVudHMvZm9vdGVyL2Zvb3Rlci5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/components/common-components/footer/footer.component.ts":
/*!*************************************************************************!*\
  !*** ./src/app/components/common-components/footer/footer.component.ts ***!
  \*************************************************************************/
/*! exports provided: FooterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FooterComponent", function() { return FooterComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_helper_static_content_fetch_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/helper/static-content-fetch.service */ "./src/app/helper/static-content-fetch.service.ts");



var FooterComponent = /** @class */ (function () {
    function FooterComponent(fetchService) {
        this.fetchService = fetchService;
    }
    FooterComponent.prototype.ngOnInit = function () {
        this.footerContent = this.fetchService.getStaticContent(this.fetchService.consentType, 'FOOTER');
    };
    FooterComponent.prototype.footerHelp = function () {
        window.open(this.footerContent.HELP_URL, "_self");
    };
    FooterComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-footer',
            template: __webpack_require__(/*! ./footer.component.html */ "./src/app/components/common-components/footer/footer.component.html"),
            styles: [__webpack_require__(/*! ./footer.component.scss */ "./src/app/components/common-components/footer/footer.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_helper_static_content_fetch_service__WEBPACK_IMPORTED_MODULE_2__["StaticContentFetchService"]])
    ], FooterComponent);
    return FooterComponent;
}());



/***/ }),

/***/ "./src/app/components/common-components/header/header.component.html":
/*!***************************************************************************!*\
  !*** ./src/app/components/common-components/header/header.component.html ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<header role=\"banner\">\n  <div class=\"container\">\n    <div class=\"logo-container\">\n      <img class=\"logo\" [src]=\"this.fetchService.CdnBaseUrl+'/consent-ui/'+this.fetchService.BuildVersion+'/'+this.headerContent.LOGOURL\" [alt]='this.headerContent.BANK_LOGO_IMAGE_ALTERNATE_LABEL' \n      [title] = 'this.headerContent.BANK_LOGO_IMAGE_ALTERNATE_LABEL'/>\n    </div>\n    <div class=\"portal-details\">\n      <h1 class=\"title-primary text-right\" [innerHTML]='this.headerContent.ACCOUNT_ACCESS_LABEL'></h1>\n      <h2 class=\"title-secondary text-right\" [innerHTML]='this.headerContent.THIRD_PARTY_LABEL'></h2>\n    </div>\n  </div>\n</header>\n"

/***/ }),

/***/ "./src/app/components/common-components/header/header.component.scss":
/*!***************************************************************************!*\
  !*** ./src/app/components/common-components/header/header.component.scss ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvY29tbW9uLWNvbXBvbmVudHMvaGVhZGVyL2hlYWRlci5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/components/common-components/header/header.component.ts":
/*!*************************************************************************!*\
  !*** ./src/app/components/common-components/header/header.component.ts ***!
  \*************************************************************************/
/*! exports provided: HeaderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HeaderComponent", function() { return HeaderComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_helper_static_content_fetch_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/helper/static-content-fetch.service */ "./src/app/helper/static-content-fetch.service.ts");



var HeaderComponent = /** @class */ (function () {
    function HeaderComponent(fetchService) {
        this.fetchService = fetchService;
    }
    HeaderComponent.prototype.ngOnInit = function () {
        this.headerContent = this.fetchService.getStaticContent(this.fetchService.consentType, 'HEADER');
    };
    HeaderComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-header',
            template: __webpack_require__(/*! ./header.component.html */ "./src/app/components/common-components/header/header.component.html"),
            styles: [__webpack_require__(/*! ./header.component.scss */ "./src/app/components/common-components/header/header.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_helper_static_content_fetch_service__WEBPACK_IMPORTED_MODULE_2__["StaticContentFetchService"]])
    ], HeaderComponent);
    return HeaderComponent;
}());



/***/ }),

/***/ "./src/app/components/common-components/loading-spinner/loading-spinner.component.html":
/*!*********************************************************************************************!*\
  !*** ./src/app/components/common-components/loading-spinner/loading-spinner.component.html ***!
  \*********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"loading-spinner-container\">\n  <div class=\"loading-spinner\">\n    <!--<em class=\"fa fa-spinner fa-pulse fa-3x fa-fw\"></em>-->\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/components/common-components/loading-spinner/loading-spinner.component.scss":
/*!*********************************************************************************************!*\
  !*** ./src/app/components/common-components/loading-spinner/loading-spinner.component.scss ***!
  \*********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".loading-spinner-container {\n  display: block;\n  position: fixed;\n  left: 0;\n  top: 0;\n  right: 0;\n  bottom: 0;\n  background-color: rgba(0, 0, 0, 0.7);\n  text-align: center;\n  z-index: 3; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi92YXIvbGliL2plbmtpbnMvd29ya3NwYWNlL1BTRDItY21hMi1EZXYvQ29uc2VudC1tY3ItMC43LjEtQ2xvdWRGcm9udC1VSS1DSS9zcmMvYXBwL2NvbXBvbmVudHMvY29tbW9uLWNvbXBvbmVudHMvbG9hZGluZy1zcGlubmVyL2xvYWRpbmctc3Bpbm5lci5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGNBQWM7RUFDZCxlQUFlO0VBQ2YsT0FBTTtFQUNOLE1BQU07RUFDTixRQUFRO0VBQ1IsU0FBUTtFQUNSLG9DQUFpQztFQUNqQyxrQkFBa0I7RUFDbEIsVUFBVSxFQUFBIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50cy9jb21tb24tY29tcG9uZW50cy9sb2FkaW5nLXNwaW5uZXIvbG9hZGluZy1zcGlubmVyLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmxvYWRpbmctc3Bpbm5lci1jb250YWluZXJ7XG4gICAgZGlzcGxheTogYmxvY2s7XG4gICAgcG9zaXRpb246IGZpeGVkO1xuICAgIGxlZnQ6MDtcbiAgICB0b3A6IDA7XG4gICAgcmlnaHQ6IDA7XG4gICAgYm90dG9tOjA7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogcmdiYSgwLDAsMCwwLjcpO1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICB6LWluZGV4OiAzO1xufVxuIl19 */"

/***/ }),

/***/ "./src/app/components/common-components/loading-spinner/loading-spinner.component.ts":
/*!*******************************************************************************************!*\
  !*** ./src/app/components/common-components/loading-spinner/loading-spinner.component.ts ***!
  \*******************************************************************************************/
/*! exports provided: LoadingSpinnerComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoadingSpinnerComponent", function() { return LoadingSpinnerComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var LoadingSpinnerComponent = /** @class */ (function () {
    function LoadingSpinnerComponent() {
    }
    LoadingSpinnerComponent.prototype.ngOnInit = function () {
    };
    LoadingSpinnerComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-loading-spinner',
            template: __webpack_require__(/*! ./loading-spinner.component.html */ "./src/app/components/common-components/loading-spinner/loading-spinner.component.html"),
            styles: [__webpack_require__(/*! ./loading-spinner.component.scss */ "./src/app/components/common-components/loading-spinner/loading-spinner.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], LoadingSpinnerComponent);
    return LoadingSpinnerComponent;
}());



/***/ }),

/***/ "./src/app/components/common-components/main-header/main-header.component.html":
/*!*************************************************************************************!*\
  !*** ./src/app/components/common-components/main-header/main-header.component.html ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"consentType==='AISP'\">\n<div *ngIf=\"!this.fetchService.renewalAuthorisation; else renewBlock\">\n<h3 class=\"title-tertiary\" [innerHTML]='this.mainHeaderContent.AISP.ACCOUNT_SELECTION_HEADER'></h3>\n<h4 *ngIf=\"consentType==='AISP'\" class=\"header-bar-primary margin-bottom-20\">\n  <span [innerHTML]='this.mainHeaderContent.AISP.PAGE_INSTRUCTION_PRE_LABEL'></span>\n  <span [innerHTML]='this.tppInfo.applicationName'></span>(<span [innerHTML]='this.tppInfo.tppName'></span>)\n  <span [innerHTML]='this.mainHeaderContent.AISP.PAGE_INSTRUCTION_POST_LABEL'></span>\n  <div>\n    <span  class=\"payment-section-label\">Software On Behalf Of: </span>\n    <span class=\"payment-section-value\" [innerHTML]=' this.softwareOnBehalfOfOrg'></span>\n   </div>\n</h4>\n</div>\n<ng-template #renewBlock>\n   <h3 class=\"title-tertiary\" [innerHTML]='this.mainHeaderContent.AUTHORISATION_RENEWAL_HEADER'></h3>\n   <h4 class=\"header-bar-primary margin-bottom-20\">\n  <span [innerHTML]='this.mainHeaderContent.AISP.PAGE_INSTRUCTION_LABEL_PART1'></span>\n  <span [innerHTML]='this.tppInfo.applicationName'></span>(<span [innerHTML]='this.tppInfo.tppName'></span>)\n  <span [innerHTML]='this.mainHeaderContent.AISP.PAGE_INSTRUCTION_LABEL_PART2'></span>\n  <span [innerHTML]='this.mainHeaderContent.AISP.PAGE_INSTRUCTION_LABEL_PART3'></span>\n  <div>\n    <span  class=\"payment-section-label\">Software On Behalf Of: </span>\n    <span class=\"payment-section-value\" [innerHTML]=' this.softwareOnBehalfOfOrg'></span>\n   </div>\n</h4>\n</ng-template>\n</div>\n\n<div *ngIf=\"consentType==='PISP'\">\n    <h3 class=\"title-tertiary\" [innerHTML]='this.mainHeaderContent.PISP.ACCOUNT_SELECTION_HEADER'></h3>\n    <h4 *ngIf=\"consentType==='PISP'\" class=\"header-bar-primary margin-bottom-20\">\n        <span [innerHTML]='this.mainHeaderContent.PISP.PAYMENT_CHECK_LABEL'></span>\n    </h4>\n</div>\n\n<div *ngIf=\"consentType==='CISP'\">\n<div *ngIf=\"!this.fetchService.renewalAuthorisation; else renewBlock\">\n  <h3 class=\"title-tertiary\" [innerHTML]='this.mainHeaderContent.CISP.FUNDS_CHECK_HEADER'></h3>\n  <h4 *ngIf=\"consentType==='CISP'\" class=\"header-bar-primary margin-bottom-20\">\n        <span [innerHTML]='this.mainHeaderContent.CISP.FUNDS_AVAILBILITY_CHECK_LABEL'></span>\n  </h4>\n</div>\n<ng-template #renewBlock>\n    <h3 class=\"title-tertiary\" [innerHTML]='this.mainHeaderContent.AUTHORISATION_RENEWAL_HEADER'></h3>\n    <h4 class=\"header-bar-primary margin-bottom-20\">\n   <span [innerHTML]='this.mainHeaderContent.CISP.PAGE_INSTRUCTION_LABEL_PART1'></span>\n   <span [innerHTML]='this.tppInfo.applicationName'></span>(<span [innerHTML]='this.tppInfo.tppName'></span>)\n   <span [innerHTML]='this.mainHeaderContent.CISP.PAGE_INSTRUCTION_LABEL_PART2'></span>\n   <span [innerHTML]='this.mainHeaderContent.CISP.PAGE_INSTRUCTION_LABEL_PART3'></span>\n   <div>\n    <span  class=\"payment-section-label\">Software On Behalf Of: </span>\n    <span class=\"payment-section-value\" [innerHTML]=' this.softwareOnBehalfOfOrg'></span>\n   </div>\n </h4>\n </ng-template>\n </div>\n \n"

/***/ }),

/***/ "./src/app/components/common-components/main-header/main-header.component.scss":
/*!*************************************************************************************!*\
  !*** ./src/app/components/common-components/main-header/main-header.component.scss ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvY29tbW9uLWNvbXBvbmVudHMvbWFpbi1oZWFkZXIvbWFpbi1oZWFkZXIuY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/components/common-components/main-header/main-header.component.ts":
/*!***********************************************************************************!*\
  !*** ./src/app/components/common-components/main-header/main-header.component.ts ***!
  \***********************************************************************************/
/*! exports provided: MainHeaderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MainHeaderComponent", function() { return MainHeaderComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_helper_static_content_fetch_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/helper/static-content-fetch.service */ "./src/app/helper/static-content-fetch.service.ts");



var MainHeaderComponent = /** @class */ (function () {
    function MainHeaderComponent(fetchService) {
        this.fetchService = fetchService;
        this.mainHeaderContent = {};
    }
    MainHeaderComponent.prototype.ngOnInit = function () {
        this.consentType = this.fetchService.consentType;
        if (!this.fetchService.renewalAuthorisation) {
            this.mainHeaderContent[this.fetchService.consentType] = this.fetchService.getStaticContent(this.fetchService.consentType, 'ACCOUNT_SELECTION_PAGE', 'PAGE_INSTRUCTION');
        }
        else {
            if (this.consentType !== 'PISP') {
                this.mainHeaderContent[this.fetchService.consentType] = this.fetchService.getStaticContent(this.fetchService.consentType, 'AUTHORISATION_RENEWAL_PAGE', 'PAGE_INSTRUCTION');
            }
        }
        this.tppInfo = this.fetchService.tppInfo;
        this.softwareOnBehalfOfOrg = this.fetchService.getSoftwareOnBehalfOfOrg();
        console.log(this.fetchService.getSoftwareOnBehalfOfOrg());
        console.log(this.fetchService.getStaticContent);
    };
    MainHeaderComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-main-header',
            template: __webpack_require__(/*! ./main-header.component.html */ "./src/app/components/common-components/main-header/main-header.component.html"),
            styles: [__webpack_require__(/*! ./main-header.component.scss */ "./src/app/components/common-components/main-header/main-header.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_helper_static_content_fetch_service__WEBPACK_IMPORTED_MODULE_2__["StaticContentFetchService"]])
    ], MainHeaderComponent);
    return MainHeaderComponent;
}());



/***/ }),

/***/ "./src/app/components/common-components/modal-popup/modal-popup.component.html":
/*!*************************************************************************************!*\
  !*** ./src/app/components/common-components/modal-popup/modal-popup.component.html ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "  <div class=\"modal-header\" *ngIf='modalDef.modalType != \"submissionResponse\"'> \n      <span class=\"sr-only\" [innerHTML]=\"screenReaderLabels.titleLabel\"></span>  \n      <span class=\"modal-title\" [innerHTML]=\"modalDef.title\"></span>\n      <a role=\"button\" href=\"javascript:void(0)\" *ngIf = \"modalDef.headerButton\" [title]=\"screenReaderLabels.closeBtnLabel\"\n       class=\"close\" (click)=\"activeModal.dismiss('Cross click')\">\n        <span class=\"sr-only\" [attr.aria-label]=\"screenReaderLabels.closeBtnLabel\"></span>\n        <span aria-hidden=\"true\">&times;</span>\n      </a>\n  </div>\n    <div class=\"modal-body\">\n      <div class=\"intermediate-loader\" *ngIf='modalDef.modalType === \"submissionResponse\"'></div>\n      <p class=\"text-center\" [innerHTML]=\"modalDef.body\"></p>\n    </div>\n    <div class=\"modal-footer\" *ngIf='modalDef.modalType != \"submissionResponse\"'>\n      <div class=\"container\">\n        <div class=\"row\">\n          <div class=\"col-md-6 col-sm-6\" *ngIf='modalDef.noBtn'>\n            <button type=\"button\" class=\"btn btn-secondary\" (click)=\"activeModal.close('Cross click')\">\n              <span [innerHTML]=\"modalDef.noBtnLabel\"></span>\n              <span class=\"sr-only\" [attr.aria-label]=\"this.screenReaderLabels.noBtnLabel\"></span>\n            </button>\n          </div>\n          <div class=\"col-md-6 col-sm-6\" *ngIf='modalDef.yesBtn'>\n            <button type=\"button\" class=\"btn btn-primary\" (click)=\"handleYesBtnClick()\">\n              <span [innerHTML]=\"modalDef.yesBtnLabel\"></span>\n              <span class=\"sr-only\" [attr.aria-label]=\"this.screenReaderLabels.yesBtnLabel\"></span>\n            </button>\n          </div>\n          <div class=\"col-md-6 col-sm-6 offset-sm-3\" *ngIf='modalDef.sessionBtn'>\n              <button type=\"button\" class=\"btn btn-primary\" (click)=\"handleSessionBtnClick()\">\n                <span [innerHTML]=\"modalDef.sessionBtnLabel\"></span>\n              </button>\n          </div>\n        </div>\n      </div>\n    </div>\n  "

/***/ }),

/***/ "./src/app/components/common-components/modal-popup/modal-popup.component.scss":
/*!*************************************************************************************!*\
  !*** ./src/app/components/common-components/modal-popup/modal-popup.component.scss ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvY29tbW9uLWNvbXBvbmVudHMvbW9kYWwtcG9wdXAvbW9kYWwtcG9wdXAuY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/components/common-components/modal-popup/modal-popup.component.ts":
/*!***********************************************************************************!*\
  !*** ./src/app/components/common-components/modal-popup/modal-popup.component.ts ***!
  \***********************************************************************************/
/*! exports provided: ModalPopupComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ModalPopupComponent", function() { return ModalPopupComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var src_app_helper_static_content_fetch_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/helper/static-content-fetch.service */ "./src/app/helper/static-content-fetch.service.ts");




var ModalPopupComponent = /** @class */ (function () {
    function ModalPopupComponent(activeModal, fetchService) {
        this.activeModal = activeModal;
        this.fetchService = fetchService;
        this.screenReaderLabels = {};
    }
    ModalPopupComponent.prototype.ngOnInit = function () {
        if (this.modalDef.modalType === 'cancel') {
            this.screenReaderLabels.titleLabel = this.fetchService.getStaticContent(this.fetchService.consentType, 'CANCEL_POPUP', 'POPUP_DISPLAYED_SCREENREADER_LABEL');
            this.screenReaderLabels.closeBtnLabel = this.fetchService.getStaticContent(this.fetchService.consentType, 'CANCEL_POPUP', 'CLOSE_BUTTON_LABEL');
            this.screenReaderLabels.yesBtnLabel = this.fetchService.getStaticContent(this.fetchService.consentType, 'CANCEL_POPUP', 'POPUP_DISPLAYED_SCREENREADER_LABEL');
            this.screenReaderLabels.noBtnLabel = this.fetchService.getStaticContent(this.fetchService.consentType, 'CANCEL_POPUP', 'PRESS_NO_SCREENREADER_LABEL');
        }
        else {
            this.screenReaderLabels.titleLabel = this.fetchService.getStaticContent(this.fetchService.consentType, 'SESSION_TIMEOUT_POPUP', 'POPUP_DISPLAYED_SCREENREADER_LABEL');
        }
    };
    ModalPopupComponent.prototype.handleYesBtnClick = function () {
        this.activeModal.close('close modal');
        this.modalDef.yesBtnFn(this.classRef);
    };
    ModalPopupComponent.prototype.handleSessionBtnClick = function () {
        this.activeModal.close('close modal');
        this.modalDef.sessionBtnFn(this.classRef);
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], ModalPopupComponent.prototype, "modalDef", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], ModalPopupComponent.prototype, "classRef", void 0);
    ModalPopupComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-modal-popup',
            template: __webpack_require__(/*! ./modal-popup.component.html */ "./src/app/components/common-components/modal-popup/modal-popup.component.html"),
            styles: [__webpack_require__(/*! ./modal-popup.component.scss */ "./src/app/components/common-components/modal-popup/modal-popup.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__["NgbActiveModal"],
            src_app_helper_static_content_fetch_service__WEBPACK_IMPORTED_MODULE_3__["StaticContentFetchService"]])
    ], ModalPopupComponent);
    return ModalPopupComponent;
}());



/***/ }),

/***/ "./src/app/components/common-components/scroll-arrow/scroll-arrow.component.html":
/*!***************************************************************************************!*\
  !*** ./src/app/components/common-components/scroll-arrow/scroll-arrow.component.html ***!
  \***************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div id=\"goToNext\" [ngClass]=\"topDownArrow\" (click)=\"goToNext()\"></div>\n"

/***/ }),

/***/ "./src/app/components/common-components/scroll-arrow/scroll-arrow.component.scss":
/*!***************************************************************************************!*\
  !*** ./src/app/components/common-components/scroll-arrow/scroll-arrow.component.scss ***!
  \***************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvY29tbW9uLWNvbXBvbmVudHMvc2Nyb2xsLWFycm93L3Njcm9sbC1hcnJvdy5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/components/common-components/scroll-arrow/scroll-arrow.component.ts":
/*!*************************************************************************************!*\
  !*** ./src/app/components/common-components/scroll-arrow/scroll-arrow.component.ts ***!
  \*************************************************************************************/
/*! exports provided: ScrollArrowComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ScrollArrowComponent", function() { return ScrollArrowComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var ScrollArrowComponent = /** @class */ (function () {
    function ScrollArrowComponent() {
    }
    ScrollArrowComponent.prototype.ngOnInit = function () { };
    ScrollArrowComponent.prototype.onWindowScroll = function () {
        var elementArray = ["select-accounts", "request-permissions", "request-transaction", "important-info", "footer-section"];
        var scrollY = window.scrollY;
        this.elementArrayPos = elementArray.map(function (item) {
            if (document.querySelector("." + item + "")) {
                return document.querySelector("." + item + "").offsetTop;
            }
        });
        var indexOfResult = this.elementArrayPos.find(function (item, index) {
            return item >= scrollY;
        });
        if (indexOfResult <= document.querySelector("." + elementArray[0] + "").offsetTop) {
            this.topDownArrow = '';
            this.topDownScroll = this.elementArrayPos[1];
        }
        else if (this.elementArrayPos.indexOf(indexOfResult) == (elementArray.length - 1) || this.elementArrayPos.indexOf(indexOfResult) == -1) {
            this.topDownArrow = 'goToNext round-border goToBack';
            this.topDownScroll = this.elementArrayPos[0];
        }
        else {
            this.topDownArrow = 'goToNext round-border';
            this.topDownScroll = this.elementArrayPos[this.elementArrayPos.indexOf(indexOfResult) ? this.elementArrayPos.indexOf(indexOfResult) : this.elementArrayPos.indexOf(this.elementArrayPos.length)];
        }
    };
    ScrollArrowComponent.prototype.goToNext = function () {
        window.scrollTo({
            top: (this.topDownScroll + 50),
            behavior: 'smooth'
        });
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["HostListener"])("window:scroll", []),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Function),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", []),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:returntype", void 0)
    ], ScrollArrowComponent.prototype, "onWindowScroll", null);
    ScrollArrowComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-scroll-arrow',
            template: __webpack_require__(/*! ./scroll-arrow.component.html */ "./src/app/components/common-components/scroll-arrow/scroll-arrow.component.html"),
            styles: [__webpack_require__(/*! ./scroll-arrow.component.scss */ "./src/app/components/common-components/scroll-arrow/scroll-arrow.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], ScrollArrowComponent);
    return ScrollArrowComponent;
}());



/***/ }),

/***/ "./src/app/components/pisp-components/pisp-account-selection/pisp-account-selection.component.html":
/*!*********************************************************************************************************!*\
  !*** ./src/app/components/pisp-components/pisp-account-selection/pisp-account-selection.component.html ***!
  \*********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-main-header *ngIf=!this.fetchService.serverErrorFlag></app-main-header>\n<table class=\"table table-striped table-bordered card-detail-viewer\" *ngIf=\"accountService.paymentSetup\">\n  <thead class=\"header-bar-primary\">\n    <tr><th colspan=\"2\" >Payee details</th></tr>\n  </thead>\n\n    <tr>\n      <th>Payment Initiated By</th>\n      <td [innerHTML]=\"pispDisplay.PaymentInitiater ? pispDisplay.PaymentInitiater : '-'\">{{pispDisplay.PaymentInitiater}}</td>\n    </tr>\n    <tr>\n      <th>Payee name</th>\n      <td [innerHTML]=\"(pispDisplay.PaymentSetup.creditorDetails && pispDisplay.PaymentSetup.creditorDetails.Name) ? pispDisplay.PaymentSetup.creditorDetails.Name : '-'\"></td>\n    </tr>  \n    <tr>\n      <th>Amount</th>\n      <td [innerHTML]=\"pispDisplay.PaymentSetup.amountDetails.Amount ? (pispDisplay.PaymentSetup.amountDetails.Amount | currency: pispDisplay.PaymentSetup.amountDetails.Currency ) : '-'\"></td>\n    </tr>\n    <!-- <tr>\n        <th>Payment Charges</th>\n        <td [innerHTML]=\" (pispDisplay.PaymentSetup.chargeDetails.Charges && pispDisplay.PaymentSetup.chargeDetails.Charges[0].Amount && pispDisplay.PaymentSetup.chargeDetails.Charges[0].Amount.Amount ) ? (pispDisplay.PaymentSetup.chargeDetails.Charges[0].Amount.Amount | currency : pispDisplay.PaymentSetup.chargeDetails.Charges[0].Amount.Currency) : '-'\"></td>\n    </tr>\n    <tr>\n      <th>Exchange Rate</th>\n      <td [innerHTML]=\"pispDisplay.PaymentSetup.exchangeRateDetails ? pispDisplay.PaymentSetup.exchangeRateDetails : '-'\"></td>\n    </tr> -->\n    <tr>\n      <th>Reference</th>\n      <td [innerHTML]=\"pispDisplay.PaymentSetup.remittanceDetails && pispDisplay.PaymentSetup.remittanceDetails.Reference ? pispDisplay.PaymentSetup.remittanceDetails.Reference : '-'\"></td>\n    </tr> \n    <tr>\n    <th [innerHTML]=\"(debtorDetails ? 'Selected' : 'Select') + ' Account'\"></th>\n    <!-- dropdown disabled for single debtor account -->\n    <td>\n        <div *ngIf=\"!this.debtorDetails && accountService.psuAccounts.length>1;else singlePsuAccount\">\n            <select class=\"form-control\" [(ngModel)]=\"selectedSortOrder\" (change)=\"setAccount($event.target.value)\">\n                <option value=\"Select account\" hidden>{{selectedSortOrder}}</option>\n                <option *ngFor=\"let option of accountService.psuAccounts\" [value]=\"option.HashedValue\">\n                     <button class=\"btn\">{{option.Nickname}} {{option.Account[0].Identification | accountDigitsFilter}}</button>\n                </option>\n              </select>\n        </div>\n        <ng-template #singlePsuAccount>{{accountService.psuAccounts[0].Nickname}} {{accountService.psuAccounts[0].Account[0].Identification | accountDigitsFilter}}</ng-template>\n        <div *ngIf=\"this.debtorDetails\" [innerHTML]=\"debtorDetails + (debtorAccount | accountDigitsFilter)\">\n          \n        </div>\n      </td> \n    </tr> \n  </tbody>\n</table>\n<div class=\"row\" *ngIf='isScheduledOrderPayment'>\n    <div class=\"payment-inner-row padding-26\">\n        <span>\n            <i class=\"fa fa-question-circle\"></i>\n        </span>\n        <span class=\"payment-section-title\" [innerHTML]=\"futureDatePaymentText\"></span>\n    </div>\n  <div class=\"col-md-6  col-sm-6\">\n    <div>\n        <div class=\"payment-section-label\" [innerHTML]=\"fetchService.getStaticContent('PISP','ACCOUNT_SELECTION_PAGE','PAYMENT_DATE')\"></div>\n        <div class=\"payment-section-value\" *ngIf=\"customISPData\" [innerHTML]=\"customISPData.requestedExecutionDateTime?(customISPData.requestedExecutionDateTime | date: 'dd-MM-yyyy'):'-'\"></div>\n        <div class=\"payment-section-value\" *ngIf=\"customDSPData\" [innerHTML]=\"customDSPData.requestedExecutionDateTime?(customDSPData.requestedExecutionDateTime | date: 'dd-MM-yyyy'):'-'\"></div>\n    </div>\n  </div>\n</div>\n<div class=\"row\" *ngIf='isStandingOrderPayment'>\n    <div class=\"payment-inner-row padding-26\">\n        <span>\n            <i class=\"fa fa-question-circle\"></i>\n        </span>\n        <span class=\"payment-section-title\" [innerHTML]=\"standingOrderPaymentText\"></span>\n    </div>\n  <div class=\"col-md-6 col-sm-6\">\n    <div class=\"payment-inner-row\">\n        <div class=\"payment-section-label\" [innerHTML]=\"fetchService.getStaticContent('PISP','ACCOUNT_SELECTION_PAGE','FIRST_PAYMENT_DATE')\"></div>\n        <div class=\"payment-section-value\" [innerHTML]=\"customDSOData ? (customDSOData.firstPaymentDateTime | date: 'dd-MM-yyyy') : ' - ' \"></div>\n    </div>\n  </div>\n  <div class=\"col-md-6 col-sm-6\">\n    <div class=\"payment-inner-row\">\n        <div class=\"payment-section-label\" [innerHTML]=\"fetchService.getStaticContent('PISP','ACCOUNT_SELECTION_PAGE','FREQUENCY')\"></div>\n        <div  class=\"payment-section-value\" [innerHTML]=\"customDSOData ? (customDSOData.frequency) : '-'\">\n        </div>\n    </div>\n  </div>\n\n</div>\n<app-show-account-details *ngIf=\"!hidePostAuthorizingData && fetchService.isPostAuthorisingPartyFlowEnabled\" [accountDetails]=\"accountDetails\"  [setupAmount]=\"setupAmount\"></app-show-account-details>\n\n<app-error aria-atomic=\"true\"></app-error>\n<app-confirm-section></app-confirm-section>\n <app-static-information *ngIf=\"!this.fetchService.serverErrorFlag\"></app-static-information>\n\n"

/***/ }),

/***/ "./src/app/components/pisp-components/pisp-account-selection/pisp-account-selection.component.scss":
/*!*********************************************************************************************************!*\
  !*** ./src/app/components/pisp-components/pisp-account-selection/pisp-account-selection.component.scss ***!
  \*********************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "table tr {\n  border: 1px solid black; }\n\n.btn {\n  width: 100%;\n  white-space: normal;\n  text-align: left;\n  border-color: black; }\n\n.fa-chevron-down:before {\n  content: \"\\f078\"; }\n\n.dropdown-toggle::after {\n  float: right;\n  margin-top: 0.6em; }\n\n.pre-class {\n  white-space: pre; }\n\n.dropdown-menu {\n  width: 100%; }\n\n.dropdown-item {\n  display: block; }\n\n.table td, .table th {\n  border-top: 1px solid black !important;\n  text-align: left; }\n\n.payment-section-value {\n  width: 40%;\n  text-align: left;\n  background: rgba(0, 0, 0, 0.05);\n  padding: 14px 14px;\n  border-right: solid 1px #CCCCCC;\n  font-size: 14px;\n  color: #053C59;\n  letter-spacing: 0;\n  vertical-align: middle;\n  display: table-cell;\n  word-break: break-all; }\n\n.payment-section-label {\n  width: 40%;\n  text-align: left;\n  background: #FAFAFA;\n  padding: 14px 14px;\n  border-radius: 4px 0 0 4px;\n  font-size: 14px;\n  color: #053C59;\n  letter-spacing: 0;\n  vertical-align: middle;\n  display: table-cell;\n  font-weight: 700; }\n\n.payment-section-title {\n  width: 100%;\n  text-align: left;\n  background: #FAFAFA;\n  padding: 0 14px;\n  border-radius: 4px 0 0 4px;\n  font-size: 14px;\n  color: #053C59;\n  letter-spacing: 0;\n  vertical-align: auto;\n  display: table-cell;\n  font-weight: 400; }\n\n.padding-26 {\n  padding-left: 26px; }\n\n.payment-inner-row {\n  display: table;\n  width: 100%;\n  margin-bottom: 5px; }\n\n.table-bordered {\n  border: 1px solid black !important; }\n\n.row {\n  margin-bottom: 16px; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi92YXIvbGliL2plbmtpbnMvd29ya3NwYWNlL1BTRDItY21hMi1EZXYvQ29uc2VudC1tY3ItMC43LjEtQ2xvdWRGcm9udC1VSS1DSS9zcmMvYXBwL2NvbXBvbmVudHMvcGlzcC1jb21wb25lbnRzL3Bpc3AtYWNjb3VudC1zZWxlY3Rpb24vcGlzcC1hY2NvdW50LXNlbGVjdGlvbi5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFLQTtFQUNJLHVCQUF1QixFQUFBOztBQUUzQjtFQUNJLFdBQVc7RUFDWCxtQkFBbUI7RUFDbkIsZ0JBQWU7RUFDZixtQkFBa0IsRUFBQTs7QUFFdEI7RUFDSSxnQkFBZ0IsRUFBQTs7QUFFcEI7RUFDQSxZQUFXO0VBQ1gsaUJBQWdCLEVBQUE7O0FBRWhCO0VBQ0ksZ0JBQWUsRUFBQTs7QUFFbkI7RUFDSSxXQUFVLEVBQUE7O0FBRWQ7RUFDSSxjQUFhLEVBQUE7O0FBR2pCO0VBQ0ksc0NBQXFDO0VBQ3JDLGdCQUNKLEVBQUE7O0FBQ0E7RUFDSSxVQUFVO0VBQ1YsZ0JBQWdCO0VBQ2hCLCtCQUEyQjtFQUMzQixrQkFBa0I7RUFDbEIsK0JBQStCO0VBQy9CLGVBQWU7RUFDZixjQUFjO0VBQ2QsaUJBQWlCO0VBQ2pCLHNCQUFzQjtFQUN0QixtQkFBbUI7RUFDbkIscUJBQXFCLEVBQUE7O0FBR3pCO0VBQ0ksVUFBVTtFQUNWLGdCQUFnQjtFQUNoQixtQkFBbUI7RUFDbkIsa0JBQWtCO0VBQ2xCLDBCQUEwQjtFQUMxQixlQUFlO0VBQ2YsY0FBYztFQUNkLGlCQUFpQjtFQUNqQixzQkFBc0I7RUFDdEIsbUJBQW1CO0VBQ25CLGdCQUFlLEVBQUE7O0FBRW5CO0VBQ0ksV0FBVztFQUNYLGdCQUFnQjtFQUNoQixtQkFBbUI7RUFDbkIsZUFBZTtFQUNmLDBCQUEwQjtFQUMxQixlQUFlO0VBQ2YsY0FBYztFQUNkLGlCQUFpQjtFQUNqQixvQkFBb0I7RUFDcEIsbUJBQW1CO0VBQ25CLGdCQUFlLEVBQUE7O0FBRW5CO0VBQ0ksa0JBQ0osRUFBQTs7QUFFQTtFQUNJLGNBQWM7RUFDZCxXQUFXO0VBQ1gsa0JBQWtCLEVBQUE7O0FBSXRCO0VBQ0ksa0NBQWdDLEVBQUE7O0FBR3BDO0VBQ0ksbUJBQWtCLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9jb21wb25lbnRzL3Bpc3AtY29tcG9uZW50cy9waXNwLWFjY291bnQtc2VsZWN0aW9uL3Bpc3AtYWNjb3VudC1zZWxlY3Rpb24uY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJ0YWJsZSB0Ym9keSB0aHtcbiAgICAvL3RleHQtYWxpZ246cmlnaHQ7XG4gICAgLy93aWR0aDoyNSU7XG4gICAgLy9ib3JkZXI6MXB4IHNvbGlkIGJsYWNrXG59XG50YWJsZSB0cntcbiAgICBib3JkZXI6IDFweCBzb2xpZCBibGFjaztcbn1cbi5idG57XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgd2hpdGUtc3BhY2U6IG5vcm1hbDtcbiAgICB0ZXh0LWFsaWduOmxlZnQ7XG4gICAgYm9yZGVyLWNvbG9yOmJsYWNrO1xufVxuLmZhLWNoZXZyb24tZG93bjpiZWZvcmUge1xuICAgIGNvbnRlbnQ6IFwiXFxmMDc4XCI7XG59XG4uZHJvcGRvd24tdG9nZ2xlOjphZnRlcntcbmZsb2F0OnJpZ2h0O1xubWFyZ2luLXRvcDowLjZlbTtcbn1cbi5wcmUtY2xhc3N7XG4gICAgd2hpdGUtc3BhY2U6cHJlO1xufVxuLmRyb3Bkb3duLW1lbnV7XG4gICAgd2lkdGg6MTAwJTtcbn1cbi5kcm9wZG93bi1pdGVte1xuICAgIGRpc3BsYXk6YmxvY2s7XG59XG5cbi50YWJsZSB0ZCwudGFibGUgdGh7XG4gICAgYm9yZGVyLXRvcDogMXB4IHNvbGlkIGJsYWNrIWltcG9ydGFudDtcbiAgICB0ZXh0LWFsaWduOmxlZnRcbn1cbi5wYXltZW50LXNlY3Rpb24tdmFsdWUge1xuICAgIHdpZHRoOiA0MCU7XG4gICAgdGV4dC1hbGlnbjogbGVmdDtcbiAgICBiYWNrZ3JvdW5kOiByZ2JhKDAsMCwwLC4wNSk7XG4gICAgcGFkZGluZzogMTRweCAxNHB4O1xuICAgIGJvcmRlci1yaWdodDogc29saWQgMXB4ICNDQ0NDQ0M7XG4gICAgZm9udC1zaXplOiAxNHB4O1xuICAgIGNvbG9yOiAjMDUzQzU5O1xuICAgIGxldHRlci1zcGFjaW5nOiAwO1xuICAgIHZlcnRpY2FsLWFsaWduOiBtaWRkbGU7XG4gICAgZGlzcGxheTogdGFibGUtY2VsbDtcbiAgICB3b3JkLWJyZWFrOiBicmVhay1hbGw7XG59XG5cbi5wYXltZW50LXNlY3Rpb24tbGFiZWwge1xuICAgIHdpZHRoOiA0MCU7XG4gICAgdGV4dC1hbGlnbjogbGVmdDtcbiAgICBiYWNrZ3JvdW5kOiAjRkFGQUZBO1xuICAgIHBhZGRpbmc6IDE0cHggMTRweDtcbiAgICBib3JkZXItcmFkaXVzOiA0cHggMCAwIDRweDtcbiAgICBmb250LXNpemU6IDE0cHg7XG4gICAgY29sb3I6ICMwNTNDNTk7XG4gICAgbGV0dGVyLXNwYWNpbmc6IDA7XG4gICAgdmVydGljYWwtYWxpZ246IG1pZGRsZTtcbiAgICBkaXNwbGF5OiB0YWJsZS1jZWxsO1xuICAgIGZvbnQtd2VpZ2h0OjcwMDtcbn1cbi5wYXltZW50LXNlY3Rpb24tdGl0bGV7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgdGV4dC1hbGlnbjogbGVmdDtcbiAgICBiYWNrZ3JvdW5kOiAjRkFGQUZBO1xuICAgIHBhZGRpbmc6IDAgMTRweDtcbiAgICBib3JkZXItcmFkaXVzOiA0cHggMCAwIDRweDtcbiAgICBmb250LXNpemU6IDE0cHg7XG4gICAgY29sb3I6ICMwNTNDNTk7XG4gICAgbGV0dGVyLXNwYWNpbmc6IDA7XG4gICAgdmVydGljYWwtYWxpZ246IGF1dG87XG4gICAgZGlzcGxheTogdGFibGUtY2VsbDtcbiAgICBmb250LXdlaWdodDo0MDA7XG59XG4ucGFkZGluZy0yNntcbiAgICBwYWRkaW5nLWxlZnQ6MjZweFxufVxuXG4ucGF5bWVudC1pbm5lci1yb3cge1xuICAgIGRpc3BsYXk6IHRhYmxlO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIG1hcmdpbi1ib3R0b206IDVweDtcbn1cblxuXG4udGFibGUtYm9yZGVyZWR7XG4gICAgYm9yZGVyOjFweCBzb2xpZCBibGFjayFpbXBvcnRhbnQ7XG59XG5cbi5yb3d7XG4gICAgbWFyZ2luLWJvdHRvbToxNnB4O1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/components/pisp-components/pisp-account-selection/pisp-account-selection.component.ts":
/*!*******************************************************************************************************!*\
  !*** ./src/app/components/pisp-components/pisp-account-selection/pisp-account-selection.component.ts ***!
  \*******************************************************************************************************/
/*! exports provided: PispAccountSelectionComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PispAccountSelectionComponent", function() { return PispAccountSelectionComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_helper_static_content_fetch_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/helper/static-content-fetch.service */ "./src/app/helper/static-content-fetch.service.ts");
/* harmony import */ var src_app_helper_account_handler_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/helper/account-handler.service */ "./src/app/helper/account-handler.service.ts");
/* harmony import */ var src_app_services_http_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/http.service */ "./src/app/services/http.service.ts");
/* harmony import */ var src_app_helper_shared_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/helper/shared.service */ "./src/app/helper/shared.service.ts");
/* harmony import */ var src_app_helper_error_handler_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/helper/error-handler.service */ "./src/app/helper/error-handler.service.ts");
/* harmony import */ var src_app_helper_modal_definition_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/helper/modal-definition.service */ "./src/app/helper/modal-definition.service.ts");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm5/ng-bootstrap.js");
/* harmony import */ var _common_components_modal_popup_modal_popup_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../common-components/modal-popup/modal-popup.component */ "./src/app/components/common-components/modal-popup/modal-popup.component.ts");










var PispAccountSelectionComponent = /** @class */ (function () {
    function PispAccountSelectionComponent(accountService, fetchService, httpService, sharedService, errorService, modalDefService, modalService) {
        this.accountService = accountService;
        this.fetchService = fetchService;
        this.httpService = httpService;
        this.sharedService = sharedService;
        this.errorService = errorService;
        this.modalDefService = modalDefService;
        this.modalService = modalService;
        this.silentPostAuthorizing = ['DomesticStandingOrdersPayments'];
        this.hidePostAuthorizingData = (this.accountService.paymentSetup && (this.silentPostAuthorizing.indexOf(this.accountService.paymentSetup.paymentType) != -1));
        this.pispDisplay = {};
        this.Accounts = {};
        this.sortOrders = [];
        this.debtorDetails = null;
        this.debtorAccount = '';
        this.selectedSortOrder = "Select account";
    }
    PispAccountSelectionComponent.prototype.setAccount = function (account) {
        var _this = this;
        if (this.accountService.selectedAccounts.length !== 0)
            this.accountService.selectedAccounts.length = 0;
        this.accountService.selectedAccounts = (this.accountService.psuAccounts).find(function (acct) {
            return acct.HashedValue === account;
        });
        console.log(this.accountService.selectedAccounts);
        //post Authorizing call for selected debtor account details field
        if (this.fetchService.isPostAuthorisingPartyFlowEnabled) {
            if (this.accountService.selectedAccounts) {
                this.reqData = {
                    fsHeaders: this.fetchService.fsHeaders,
                    accountDetails: this.accountService.selectedAccounts
                };
            }
            this.sharedService.emitShowLoadingSpinner(true);
            this.httpService.selectedAccDetailsRequest(this.consentType, this.reqData, this.resumePath, this.correlationId, this.refreshTokenRenewalFlow, this.headers).subscribe(function (res) {
                _this.sharedService.emitShowLoadingSpinner(false);
                if (_this.accountDetails == undefined)
                    _this.accountDetails = res.body;
            }, function (errorRes) {
                if (errorRes.error.exception &&
                    errorRes.error.exception.errorCode === '731') {
                    // defining modal definition for session popup
                    _this.sharedService.emitShowLoadingSpinner(false);
                    var modalInstance = _this.modalService.open(_common_components_modal_popup_modal_popup_component__WEBPACK_IMPORTED_MODULE_9__["ModalPopupComponent"], {
                        backdrop: 'static',
                        keyboard: false
                    });
                    modalInstance.componentInstance.modalDef = _this.modalDefService.defineModalObj(_this, 'session');
                    modalInstance.componentInstance.classRef = _this;
                    _this.sessionRedirectUri = errorRes.error.redirectUri;
                }
                else {
                    var error = errorRes.error.exception
                        ? errorRes.error.exception.errorCode
                        : '800';
                    _this.errorService.error = error;
                    _this.sharedService.emitShowLoadingSpinner(false);
                }
            });
        }
        ;
    };
    PispAccountSelectionComponent.prototype.ngOnInit = function () {
        this.consentType = this.fetchService.consentType;
        if (this.accountService.psuAccounts.length == 1)
            this.accountService.selectedAccounts = this.accountService.psuAccounts[0];
        this.resumePath = this.fetchService.resumePath;
        this.correlationId = this.fetchService.correlationId;
        this.refreshTokenRenewalFlow = this.fetchService.renewalAuthorisation;
        this.headers = {};
        //this.accountService.psuAccounts=this.accountService.psuAccounts[0];
        console.log(this.accountService.psuAccounts + 'cap');
        this.standingOrderPaymentText = this.fetchService.getStaticContent('PISP', 'ACCOUNT_SELECTION_PAGE', 'STANDING_ORDER_PAYMENT_TEXT');
        this.standingOrderPaymentText = (this.standingOrderPaymentText).replace(/&lt;/g, '<').replace(/&gt;/g, '>');
        if (this.fetchService.tppInfo) {
            //pisp payee details table
            this.pispDisplay["PaymentInitiater"] = this.fetchService.tppInfo.applicationName ? this.fetchService.tppInfo.applicationName : '';
            this.pispDisplay["PaymentInitiater"] = this.pispDisplay["PaymentInitiater"] + ' (' + this.fetchService.tppInfo.tppName ? this.fetchService.tppInfo.tppName : '' + ')';
        }
        if (this.accountService.paymentSetup) {
            this.setupAmount = this.accountService.paymentSetup.amountDetails;
            this.pispDisplay["PaymentSetup"] = this.accountService.paymentSetup;
            //debtor details object in case debtor details are provided
            if (this.pispDisplay["PaymentSetup"].debtorDetails) {
                var details = this.pispDisplay["PaymentSetup"].debtorDetails;
                this.debtorDetails = details.Name ? details.Name + ' - ' : '';
                this.debtorDetails = this.debtorDetails + (details.SchemeName ? details.SchemeName + ' ' : '');
                this.debtorAccount = details.Identification ? details.Identification : (details.SecondaryIdentification ? details.SecondaryIdentification : ' ');
                this.accountService.selectedAccounts = details;
            }
            if (this.accountService.paymentSetup.customDSOData && !(Object.entries(this.accountService.paymentSetup.customDSOData).length === 0 && (this.accountService.paymentSetup.customDSOData).constructor === Object)) {
                this.customDSOData = this.accountService.paymentSetup.customDSOData;
                this.isStandingOrderPayment = this.accountService.paymentSetup.paymentType && (this.accountService.paymentSetup.paymentType.indexOf('DomesticStandingOrder') !== -1);
            }
            if (this.accountService.paymentSetup.customDSPData && !((this.accountService.paymentSetup.customDSPData).constructor === Object && Object.entries(this.accountService.paymentSetup.customDSPData).length === 0)) {
                this.customDSPData = this.accountService.paymentSetup.customDSPData;
                this.isScheduledOrderPayment = this.accountService.paymentSetup.paymentType && (this.accountService.paymentSetup.paymentType.indexOf('DomesticScheduledPayment') !== -1);
            }
            if (this.accountService.paymentSetup.customISPData && !((this.accountService.paymentSetup.customISPData).constructor === Object && Object.entries(this.accountService.paymentSetup.customISPData).length === 0)) {
                this.customISPData = this.accountService.paymentSetup.customISPData;
                this.isScheduledOrderPayment = this.accountService.paymentSetup.paymentType && (this.accountService.paymentSetup.paymentType.indexOf('InternationalScheduledPayment') !== -1);
            }
        }
        //rest call data served in postAuthorisingPartyDetails input field
        if (this.fetchService.postAuthorisingPartyDetails && (this.fetchService.postAuthorisingPartyDetails).constructor === Object && !(Object.entries(this.fetchService.postAuthorisingPartyDetails).length === 0)) {
            this.accountDetails = this.fetchService.postAuthorisingPartyDetails;
            // this.accountService.selectedAccounts=this.fetchService.postAuthorisingPartyDetails; 
        }
        this.futureDatePaymentText = this.fetchService.getStaticContent('PISP', 'ACCOUNT_SELECTION_PAGE', 'FUTURE_DATE_PAYMENT_TEXT');
        this.futureDatePaymentText = (this.futureDatePaymentText).replace(/&lt;/g, '<').replace(/&gt;/g, '>');
    };
    PispAccountSelectionComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-pisp-account-selection',
            template: __webpack_require__(/*! ./pisp-account-selection.component.html */ "./src/app/components/pisp-components/pisp-account-selection/pisp-account-selection.component.html"),
            styles: [__webpack_require__(/*! ./pisp-account-selection.component.scss */ "./src/app/components/pisp-components/pisp-account-selection/pisp-account-selection.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_helper_account_handler_service__WEBPACK_IMPORTED_MODULE_3__["AccountHandlerService"],
            src_app_helper_static_content_fetch_service__WEBPACK_IMPORTED_MODULE_2__["StaticContentFetchService"],
            src_app_services_http_service__WEBPACK_IMPORTED_MODULE_4__["HttpService"],
            src_app_helper_shared_service__WEBPACK_IMPORTED_MODULE_5__["SharedService"],
            src_app_helper_error_handler_service__WEBPACK_IMPORTED_MODULE_6__["ErrorHandlerService"],
            src_app_helper_modal_definition_service__WEBPACK_IMPORTED_MODULE_7__["ModalDefinitionService"],
            _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_8__["NgbModal"]])
    ], PispAccountSelectionComponent);
    return PispAccountSelectionComponent;
}());



/***/ }),

/***/ "./src/app/components/pisp-components/show-account-details/show-account-details.component.html":
/*!*****************************************************************************************************!*\
  !*** ./src/app/components/pisp-components/show-account-details/show-account-details.component.html ***!
  \*****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<table class=\"table table-striped card-detail-viewer\" *ngIf=\"accountService.paymentSetup\">\n<thead class=\"header-bar-primary\">\n  <tr><th colspan=\"2\" >Exchange details</th></tr>\n  </thead>\n  <tbody>\n    <tr>\n      <th [innerHTML]=\"'Amount in '+setupAmount.Currency\"></th>\n      <td [innerHTML]=\"setupAmount ? (setupAmount.Amount | currency:setupAmount.Currency) : '-'\">zl 2,103.00</td>\n    </tr>\n    <tr>\n      <th>Exchange Rate</th>\n      <td [innerHTML]=\"(accountDetails && accountDetails.exchangeRateDetails) ? accountDetails.exchangeRateDetails.ExchangeRate : '-'\"></td>\n    </tr>  \n    <tr>\n      <th>Bank Charges</th>\n      <td [innerHTML]=\"accountDetails ?  (accountDetails.chargeDetails.Charges[0].Amount.Amount | currency:accountDetails.chargeDetails.Charges[0].Amount.Currency) : '-'\"></td>\n    </tr>\n    <tr>\n      <th [innerHTML]=\"'Amount in '+fetchService.baseCurrency\"></th>\n      <td [innerHTML]=\"accountDetails ? (accountDetails.baseAmountDetails.Amount | currency:accountDetails.baseAmountDetails.Currency) : '-'\"></td>\n    </tr>\n    <tr>\n        <th>Total Amount</th>\n        <td [innerHTML]=\"accountDetails ? (accountDetails.totalAmountDetails.Amount | currency:accountDetails.totalAmountDetails.Currency) : '-'\"></td>\n    </tr> \n\n  </tbody>\n</table>\n\n\n  "

/***/ }),

/***/ "./src/app/components/pisp-components/show-account-details/show-account-details.component.scss":
/*!*****************************************************************************************************!*\
  !*** ./src/app/components/pisp-components/show-account-details/show-account-details.component.scss ***!
  \*****************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "table tr {\n  border: 1px solid black; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi92YXIvbGliL2plbmtpbnMvd29ya3NwYWNlL1BTRDItY21hMi1EZXYvQ29uc2VudC1tY3ItMC43LjEtQ2xvdWRGcm9udC1VSS1DSS9zcmMvYXBwL2NvbXBvbmVudHMvcGlzcC1jb21wb25lbnRzL3Nob3ctYWNjb3VudC1kZXRhaWxzL3Nob3ctYWNjb3VudC1kZXRhaWxzLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksdUJBQXVCLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9jb21wb25lbnRzL3Bpc3AtY29tcG9uZW50cy9zaG93LWFjY291bnQtZGV0YWlscy9zaG93LWFjY291bnQtZGV0YWlscy5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbInRhYmxlIHRye1xuICAgIGJvcmRlcjogMXB4IHNvbGlkIGJsYWNrO1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/components/pisp-components/show-account-details/show-account-details.component.ts":
/*!***************************************************************************************************!*\
  !*** ./src/app/components/pisp-components/show-account-details/show-account-details.component.ts ***!
  \***************************************************************************************************/
/*! exports provided: ShowAccountDetailsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ShowAccountDetailsComponent", function() { return ShowAccountDetailsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_helper_static_content_fetch_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/helper/static-content-fetch.service */ "./src/app/helper/static-content-fetch.service.ts");
/* harmony import */ var src_app_helper_account_handler_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/helper/account-handler.service */ "./src/app/helper/account-handler.service.ts");




var ShowAccountDetailsComponent = /** @class */ (function () {
    function ShowAccountDetailsComponent(fetchService, accountService) {
        this.fetchService = fetchService;
        this.accountService = accountService;
    }
    ShowAccountDetailsComponent.prototype.ngOnInit = function () {
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], ShowAccountDetailsComponent.prototype, "accountDetails", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], ShowAccountDetailsComponent.prototype, "setupAmount", void 0);
    ShowAccountDetailsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-show-account-details',
            template: __webpack_require__(/*! ./show-account-details.component.html */ "./src/app/components/pisp-components/show-account-details/show-account-details.component.html"),
            styles: [__webpack_require__(/*! ./show-account-details.component.scss */ "./src/app/components/pisp-components/show-account-details/show-account-details.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_helper_static_content_fetch_service__WEBPACK_IMPORTED_MODULE_2__["StaticContentFetchService"], src_app_helper_account_handler_service__WEBPACK_IMPORTED_MODULE_3__["AccountHandlerService"]])
    ], ShowAccountDetailsComponent);
    return ShowAccountDetailsComponent;
}());



/***/ }),

/***/ "./src/app/helper/account-handler.service.ts":
/*!***************************************************!*\
  !*** ./src/app/helper/account-handler.service.ts ***!
  \***************************************************/
/*! exports provided: AccountHandlerService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AccountHandlerService", function() { return AccountHandlerService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var AccountHandlerService = /** @class */ (function () {
    function AccountHandlerService() {
        this._selectedAccounts = [];
    }
    Object.defineProperty(AccountHandlerService.prototype, "paymentSetup", {
        get: function () {
            return this._paymentSetup;
        },
        set: function (paymentSetup) {
            this._paymentSetup = paymentSetup;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AccountHandlerService.prototype, "psuAccounts", {
        get: function () {
            return this._psuAccounts;
        },
        set: function (psuAccounts) {
            this._psuAccounts = psuAccounts;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AccountHandlerService.prototype, "selectedAccounts", {
        get: function () {
            return this._selectedAccounts;
        },
        set: function (selectedAccounts) {
            this._selectedAccounts.push(selectedAccounts);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AccountHandlerService.prototype, "accountRequest", {
        get: function () {
            return this._accountRequest;
        },
        set: function (accountRequest) {
            this._accountRequest = accountRequest;
        },
        enumerable: true,
        configurable: true
    });
    AccountHandlerService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], AccountHandlerService);
    return AccountHandlerService;
}());



/***/ }),

/***/ "./src/app/helper/error-handler.service.ts":
/*!*************************************************!*\
  !*** ./src/app/helper/error-handler.service.ts ***!
  \*************************************************/
/*! exports provided: ErrorHandlerService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ErrorHandlerService", function() { return ErrorHandlerService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var ErrorHandlerService = /** @class */ (function () {
    function ErrorHandlerService() {
        this.serverError = {};
        this.errors = {};
    }
    ErrorHandlerService.prototype.setErrors = function (errors) {
        this.errors = errors;
    };
    ErrorHandlerService.prototype.handleError = function () {
        this.serverError.type = 'Alert';
        this.serverError.errorCode = this.error;
        this.serverError.errorMessage = this.getErrors(this.serverError.errorCode);
        this.serverError.errorDetails = this.getErrors(this.serverError.errorCode + '_DETAILS');
        return this.serverError;
    };
    ErrorHandlerService.prototype.getErrors = function (keys) {
        var i = 0;
        var keyReqd = this.errors;
        keyReqd = keyReqd[keys];
        return keyReqd;
    };
    ErrorHandlerService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], ErrorHandlerService);
    return ErrorHandlerService;
}());



/***/ }),

/***/ "./src/app/helper/modal-definition.service.ts":
/*!****************************************************!*\
  !*** ./src/app/helper/modal-definition.service.ts ***!
  \****************************************************/
/*! exports provided: ModalDefinitionService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ModalDefinitionService", function() { return ModalDefinitionService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var ModalDefinitionService = /** @class */ (function () {
    function ModalDefinitionService() {
        this.modalDef = {
            title: '',
            body: '',
            headerButton: false,
            sessionBtn: false,
            yesBtn: false,
            noBtn: false,
            sessionBtnLabel: '',
            yesBtnLabel: '',
            noBtnLabel: '',
            sessionBtnFn: null,
            yesBtnFn: null,
            noBtnFn: null,
            modalType: ''
        };
    }
    ModalDefinitionService.prototype.defineModalObj = function (classRef, type) {
        if (type === 'session') {
            this.modalDef.title = classRef.fetchService.getStaticContent(classRef.fetchService.consentType, 'SESSION_TIMEOUT_POPUP', 'SESSION_TIMEOUT_POPUP_HEADER');
            this.modalDef.body = classRef.fetchService.getStaticContent(classRef.fetchService.consentType, 'SESSION_TIMEOUT_POPUP', 'SESSION_TIMEOUT_POPUP_MESSAGE');
            this.modalDef.sessionBtn = true;
            this.modalDef.sessionBtnLabel = classRef.fetchService.getStaticContent(classRef.fetchService.consentType, 'SESSION_TIMEOUT_POPUP', 'OK_BUTTON_LABEL');
            this.modalDef.sessionBtnFn = classRef.onSessionBtnClick;
            // resetting other attributes
            this.modalDef.headerButton = false;
            this.modalDef.yesBtn = false;
            this.modalDef.yesBtnLabel = '';
            this.modalDef.yesBtnFn = null;
            this.modalDef.noBtn = false;
            this.modalDef.noBtnLabel = '';
            this.modalDef.noBtnFn = null;
        }
        else if (type === 'cancel') {
            if (classRef.refreshTokenRenewalFlow) {
                this.modalDef.title = classRef.fetchService.getStaticContent(classRef.fetchService.consentType, 'AUTHORISATION_RENEWAL_PAGE', 'DENY_AUTHORISATION_POPUP', 'DENY_AUTHORISATION_HEADER');
                this.modalDef.body =
                    classRef.fetchService.getStaticContent(classRef.fetchService.consentType, 'AUTHORISATION_RENEWAL_PAGE', 'DENY_AUTHORISATION_POPUP', 'DENY_AUTHORISATION_POPUP_MESSAGE_PRE') +
                        classRef.fetchService.tppInfo.tppName +
                        classRef.fetchService.getStaticContent(classRef.fetchService.consentType, 'AUTHORISATION_RENEWAL_PAGE', 'DENY_AUTHORISATION_POPUP', 'DENY_AUTHORISATION_POPUP_MESSAGE_POST');
                this.modalDef.yesBtnLabel = classRef.fetchService.getStaticContent(classRef.fetchService.consentType, 'AUTHORISATION_RENEWAL_PAGE', 'DENY_AUTHORISATION_POPUP', 'YES_BUTTON_LABEL');
                this.modalDef.noBtnLabel = classRef.fetchService.getStaticContent(classRef.fetchService.consentType, 'AUTHORISATION_RENEWAL_PAGE', 'DENY_AUTHORISATION_POPUP', 'CLOSE_BUTTON_LABEL');
            }
            else {
                this.modalDef.title = classRef.fetchService.getStaticContent(classRef.fetchService.consentType, 'CANCEL_POPUP', 'CANCEL_POPUP_HEADER');
                this.modalDef.body = classRef.fetchService.getStaticContent(classRef.fetchService.consentType, 'CANCEL_POPUP', 'CANCEL_POPUP_MESSAGE');
                this.modalDef.yesBtnLabel = classRef.fetchService.getStaticContent(classRef.fetchService.consentType, 'CANCEL_POPUP', 'YES_BUTTON_LABEL');
                this.modalDef.noBtnLabel = classRef.fetchService.getStaticContent(classRef.fetchService.consentType, 'CANCEL_POPUP', 'NO_BUTTON_LABEL');
                console.log(classRef.fetchService.getStaticContent(classRef.fetchService.consentType, 'CANCEL_POPUP', 'NO_BUTTON_LABEL'));
            }
            this.modalDef.headerButton = true;
            this.modalDef.yesBtn = true;
            this.modalDef.noBtn = true;
            this.modalDef.yesBtnFn = classRef.cancelConsentSubmission;
            // resetting the other attributes
            this.modalDef.sessionBtn = false;
            this.modalDef.sessionBtnLabel = '';
            this.modalDef.sessionBtnFn = null;
            this.modalDef.noBtnFn = null;
        }
        else {
            this.modalDef = {
                title: '',
                body: classRef.modalSubmissionResBody,
                headerButton: false,
                sessionBtn: false,
                yesBtn: false,
                noBtn: false,
                sessionBtnLabel: '',
                yesBtnLabel: '',
                noBtnLabel: '',
                sessionBtnFn: null,
                yesBtnFn: null,
                noBtnFn: null,
                modalType: ''
            };
        }
        this.modalDef.modalType = type;
        return this.modalDef;
    };
    ModalDefinitionService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], ModalDefinitionService);
    return ModalDefinitionService;
}());



/***/ }),

/***/ "./src/app/helper/shared.service.ts":
/*!******************************************!*\
  !*** ./src/app/helper/shared.service.ts ***!
  \******************************************/
/*! exports provided: SharedService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SharedService", function() { return SharedService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");



var SharedService = /** @class */ (function () {
    function SharedService() {
        // observable sources
        this.enableModalPopupSource = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        this.showLoadingSpinnerSource = new rxjs__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        // observable streams
        this.enableModalPopup$ = this.enableModalPopupSource.asObservable();
        this.showLoadingSpinner$ = this.showLoadingSpinnerSource.asObservable();
    }
    // emitter functions
    SharedService.prototype.emitEnableModalPopup = function (change) {
        this.enableModalPopupSource.next(change);
    };
    SharedService.prototype.emitShowLoadingSpinner = function (change) {
        this.showLoadingSpinnerSource.next(change);
    };
    SharedService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], SharedService);
    return SharedService;
}());



/***/ }),

/***/ "./src/app/helper/static-content-fetch.service.ts":
/*!********************************************************!*\
  !*** ./src/app/helper/static-content-fetch.service.ts ***!
  \********************************************************/
/*! exports provided: StaticContentFetchService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StaticContentFetchService", function() { return StaticContentFetchService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var StaticContentFetchService = /** @class */ (function () {
    function StaticContentFetchService() {
        this._serverErrorFlag = false;
        this._isPostAuthorisingPartyFlowEnabled = false;
    }
    Object.defineProperty(StaticContentFetchService.prototype, "serverErrorFlag", {
        get: function () {
            return this._serverErrorFlag;
        },
        set: function (serverErrorFlag) {
            this._serverErrorFlag = serverErrorFlag;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(StaticContentFetchService.prototype, "fsHeaders", {
        get: function () {
            return this._fsHeaders;
        },
        set: function (fsHeaders) {
            this._fsHeaders = fsHeaders;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(StaticContentFetchService.prototype, "renewalAuthorisation", {
        get: function () {
            return this._renewalAuthorisation;
        },
        set: function (renewalAuthorisation) {
            this._renewalAuthorisation = renewalAuthorisation;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(StaticContentFetchService.prototype, "correlationId", {
        get: function () {
            return this._correlationId;
        },
        set: function (correlationId) {
            this._correlationId = correlationId;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(StaticContentFetchService.prototype, "resumePath", {
        get: function () {
            return this._resumePath;
        },
        set: function (resumePath) {
            this._resumePath = resumePath;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(StaticContentFetchService.prototype, "tppInfo", {
        get: function () {
            return this._tppInfo;
        },
        set: function (tppInfo) {
            this._tppInfo = tppInfo;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(StaticContentFetchService.prototype, "consentType", {
        get: function () {
            return this._consentType;
        },
        set: function (consentType) {
            this._consentType = consentType;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(StaticContentFetchService.prototype, "staticContent", {
        set: function (staticContent) {
            this._staticContent = staticContent;
        },
        enumerable: true,
        configurable: true
    });
    StaticContentFetchService.prototype.getStaticContent = function () {
        var keys = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            keys[_i] = arguments[_i];
        }
        var i = 0;
        var keyReqd = this._staticContent;
        while (i < keys.length) {
            keyReqd = keyReqd[keys[i]];
            i++;
        }
        return keyReqd;
    };
    StaticContentFetchService.prototype.setSoftwareOnBehalfOfOrg = function (id) {
        this.softwareOnBehalfOfOrg = id;
    };
    StaticContentFetchService.prototype.getSoftwareOnBehalfOfOrg = function () {
        return this.softwareOnBehalfOfOrg;
    };
    Object.defineProperty(StaticContentFetchService.prototype, "CdnBaseUrl", {
        get: function () {
            return this._cdnBaseUrl;
        },
        set: function (cdnBaseUrl) {
            this._cdnBaseUrl = cdnBaseUrl;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(StaticContentFetchService.prototype, "BuildVersion", {
        get: function () {
            return this._buildVersion;
        },
        set: function (buildVersion) {
            this._buildVersion = buildVersion;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(StaticContentFetchService.prototype, "isPostAuthorisingPartyFlowEnabled", {
        get: function () {
            return this._isPostAuthorisingPartyFlowEnabled;
        },
        set: function (isPostAuthorisingPartyFlowEnabled) {
            this._isPostAuthorisingPartyFlowEnabled = isPostAuthorisingPartyFlowEnabled;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(StaticContentFetchService.prototype, "baseCurrency", {
        get: function () {
            return this._baseCurrency;
        },
        set: function (baseCurrency) {
            this._baseCurrency = baseCurrency;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(StaticContentFetchService.prototype, "postAuthorisingPartyDetails", {
        get: function () {
            return this._postAuthorisingPartyDetails;
        },
        set: function (postAuthorisingPartyDetails) {
            this._postAuthorisingPartyDetails = postAuthorisingPartyDetails;
        },
        enumerable: true,
        configurable: true
    });
    StaticContentFetchService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], StaticContentFetchService);
    return StaticContentFetchService;
}());



/***/ }),

/***/ "./src/app/pipes/account-digits-filter.pipe.ts":
/*!*****************************************************!*\
  !*** ./src/app/pipes/account-digits-filter.pipe.ts ***!
  \*****************************************************/
/*! exports provided: AccountDigitsFilterPipe */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AccountDigitsFilterPipe", function() { return AccountDigitsFilterPipe; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var AccountDigitsFilterPipe = /** @class */ (function () {
    function AccountDigitsFilterPipe() {
    }
    AccountDigitsFilterPipe.prototype.transform = function (value) {
        return " ~" + value.slice(-4);
    };
    AccountDigitsFilterPipe = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Pipe"])({
            name: 'accountDigitsFilter'
        })
    ], AccountDigitsFilterPipe);
    return AccountDigitsFilterPipe;
}());



/***/ }),

/***/ "./src/app/services/http.service.ts":
/*!******************************************!*\
  !*** ./src/app/services/http.service.ts ***!
  \******************************************/
/*! exports provided: HttpService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HttpService", function() { return HttpService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");



var HttpService = /** @class */ (function () {
    function HttpService(httpClient) {
        this.httpClient = httpClient;
    }
    HttpService.prototype.accountRequest = function (consentType, reqData, resumePath, correlationId, refreshTokenRenewalFlow, headers) {
        if (reqData) {
            return this.httpClient.post('./' +
                consentType.toLowerCase() +
                '/consent?resumePath=' +
                resumePath +
                '&correlationId=' +
                correlationId +
                '&refreshTokenRenewalFlow=' +
                refreshTokenRenewalFlow, reqData, { headers: headers, observe: 'response' });
        }
    };
    HttpService.prototype.selectedAccDetailsRequest = function (consentType, reqData, resumePath, correlationId, refreshTokenRenewalFlow, headers) {
        return this.httpClient.post(
        // 'https://postauthorizing.free.beeceptor.com/' +
        './' +
            consentType.toLowerCase() +
            '/postAuthorizingParty', reqData, { headers: headers, observe: 'response' });
    };
    HttpService.prototype.cancelRequest = function (consentType, reqData, resumePath, correlationId, refreshTokenRenewalFlow, serverErrorFlag) {
        return this.httpClient.put('./' +
            consentType.toLowerCase() +
            '/cancelConsent?resumePath=' +
            resumePath +
            '&correlationId=' +
            correlationId +
            '&refreshTokenRenewalFlow=' +
            refreshTokenRenewalFlow +
            '&serverErrorFlag=' +
            serverErrorFlag, reqData, { observe: 'response' });
        // return this.httpClient.put(
        //   'https://obuitest.free.beeceptor.com/' +
        //     consentType.toLowerCase() +
        //     '/cancelConsent?resumePath=' +
        //     resumePath +
        //     '&correlationId=' +
        //     correlationId +
        //     '&refreshTokenRenewalFlow=' +
        //     refreshTokenRenewalFlow +
        //     '&serverErrorFlag=' +
        //     serverErrorFlag,
        //   reqData,
        //   { observe: 'response' }
        // );
    };
    HttpService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], HttpService);
    return HttpService;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /var/lib/jenkins/workspace/PSD2-cma2-Dev/Consent-mcr-0.7.1-CloudFront-UI-CI/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map