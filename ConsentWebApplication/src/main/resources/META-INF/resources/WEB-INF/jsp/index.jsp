<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" 
    prefix="fn" %> 
<!DOCTYPE html>
<%@ page session="false" %>
<html lang="en" ng-app="consentApp">
<head>    
	<base href="/${applicationName}<%=request.getContextPath() %>/">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Consent - Bank of Ireland</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="apple-touch-icon" href="${cdnBaseURL}/consent-ui/img/apple-touch-icon.png">
	<link rel="stylesheet" href="${cdnBaseURL}/consent-ui/${buildVersion}/fonts/open-sans/Bold/OpenSans-Bold.css" media="none" onload="this.media='all';">
	<link rel="stylesheet" href="${cdnBaseURL}/consent-ui/${buildVersion}/fonts/open-sans/Italic/OpenSans-Italic.css" media="none" onload="this.media='all';">
	<link rel="stylesheet" href="${cdnBaseURL}/consent-ui/${buildVersion}/fonts/open-sans/Light/OpenSans-Light.css" media="none" onload="this.media='all';">
	<link rel="stylesheet" href="${cdnBaseURL}/consent-ui/${buildVersion}/fonts/open-sans/Regular/OpenSans-Regular.css" media="none" onload="this.media='all';">
	<link rel="stylesheet" href="${cdnBaseURL}/consent-ui/${buildVersion}/fonts/open-sans/Semibold/OpenSans-Semibold.css" media="none" onload="this.media='all';">
	<link rel="stylesheet" href="${cdnBaseURL}/consent-ui/${buildVersion}/fonts/font-awesome/fonts/FontAwesome.css" media="none" onload="this.media='all';">
	<link rel="stylesheet" href="${cdnBaseURL}/consent-ui/${buildVersion}/fonts/font-awesome/fonts/fontawesome-webfont.css" media="none" onload="this.media='all';">
	<link rel="stylesheet" href="${cdnBaseURL}/consent-ui/${buildVersion}/fonts/glyphicons-halflings-regular.css" media="none" onload="this.media='all';">
	<link rel="stylesheet" href="${cdnBaseURL}/consent-ui/${buildVersion}/css/common.min.css">
    <link rel="stylesheet" href="${cdnBaseURL}/consent-ui/${buildVersion}/css/boi-account-access.css" />
    <link rel="shortcut icon" type="image/x-icon" href="${cdnBaseURL}/consent-ui/${buildVersion}/img/favicon.ico?rev=16" />
    <noscript>${JAVASCRIPT_ENABLE_MSG}</noscript>
 </head>
 
<body class="next-gen-home">
		
 	<div class="page-container">
    	<!-- <page-header></page-header> -->
       <div ui-view class="main-container block-ui-main" ui-router-styles autoscroll="false"></div>
		<!-- <page-footer></page-footer> -->
	</div>
	    
<%--  		<input type="hidden" id="csrf" name="${_csrf.parameterName}" value="${_csrf.token}" />
		<input type="hidden" id="oAuthUrl" name="oAuthUrl" value="${oAuthUrl}"/>
		<input type="hidden"  id="correlationId" name="correlationId" value="${correlationId}"/>
		<input type="hidden" id="psuAccounts" name="psuAccounts" value="${fn:escapeXml(customerAccountList)}"/>
		
		<input type="hidden" id="account-request" name="account-request" value="${fn:escapeXml(consentSetupdata)}">
		<input type="hidden" id="paymentSetup" name="paymentSetup" value="${fn:escapeXml(paymentConsentData)}">
		
	     <input id="x-user-id" name="x-user-id" value="${userId}" type="hidden"/>
		<input id="consentType" name="consentType" value="${consentFlowType}" type="hidden"/>
		<input id="accountSelected" name="accountSelected" value="${accountSelected}" type="hidden"/>
		<input type="hidden" id="tppInfo" name="tppInfo" value="${tppName}"/>
		<input type="hidden" id="redirectUri" name="redirectUri" value="${redirectUri}"/>	
		<input id="user_oauth_approval" name="user_oauth_approval" value="true" type="hidden" />
		<input type="hidden" id="channelId" name="channelId" value="${channel_id}"/>
		<c:if test="${not empty exception}">
			<input type="hidden" id="error" name="error" value="${fn:escapeXml(exception)}" />
		</c:if>
 --%>	
		<input type="hidden" id="psuAccounts" name="psuAccounts" value="${fn:escapeXml(customerAccountList)}"/>
		
		<input type="hidden" id="account-request" name="account-request" value="${fn:escapeXml(consentSetupdata)}"/>
		<input type="hidden" id="paymentSetup" name="paymentSetup" value="${fn:escapeXml(paymentConsentData)}"/>
	     <input id="x-user-id" name="x-user-id" value="${userId}" type="hidden"/>
		<input id="consentType" name="consentType" value="${consentFlowType}" type="hidden"/>
		<input type="hidden" id="cmaVersion" name="cmaVersion" value="${cmaVersion}"/>
		<input id="accountSelected" name="accountSelected" value="${accountSelected}" type="hidden"/>
		<input type="hidden" id="tppInfo" name="tppInfo" value="${fn:escapeXml(tppInfo)}"/>
        <input type="hidden" id="resumePath" name="resumePath" value="${not empty redirectUri ? redirectUri : param.resumePath}"/>
        <input type="hidden" id="serverErrorFlag" name="serverErrorFlag" value="${serverErrorFlag}"/>
		<input type="hidden" id="correlationId" name="correlationId" value="${empty correlationId ? param.correlationId : correlationId}"/>
    	<input type="hidden" id="jsc-file-path" name="jsc-file-path" value="${cdnBaseURL}/consent-ui/${buildVersion}/ext-libs/fraudnet/boiukprefs.js"/>
    	<input id="fraudHdmInfo" name="fraudHdmInfo" value='${fraudHdmInfo}' type="hidden"/>
		<input type="hidden" id="staticContent" name="staticContent" value="${fn:escapeXml(UIContent)}"/>
		<input type="hidden" id="fsHeaders" name="fsHeaders" value='${fsHeaders}'/>
		<input type="hidden" id="renewalAuthorisation" name="renewalAuthorisation" value="${refreshTokenRenewalFlow}"/>
		<input type="hidden" id="buildversion" name="buildversion" value="${buildVersion}">
                <input type="hidden" id="isSandboxEnabled" name="isSandboxEnabled" value="${isSandboxEnabled}" />
		<input type="hidden" id="channelId" name="channelId" value="${channel_id}"/>
        <input type="hidden" id="tenantid" name="tenantid" value="${tenantid}"/>
                <input type="hidden" id="cdnBaseURL" name="cdnBaseURL" value="${cdnBaseURL}"/>
                <input type="hidden" id="isPostAuthorisingPartyFlowEnabled" name="isPostAuthorisingPartyFlowEnabled" value="${isPostAuthorisingPartyFlowEnabled}"/>
                <input type="hidden" id="baseCurrency" name="baseCurrency" value="${baseCurrency}"/> 
		<input type="hidden" id="postAuthorisingPartyDetails" name="postAuthorisingPartyDetails" value='${postAuthorisingPartyDetails}'/> 
        <input type="hidden" id="softwareOnBehalfOfOrg" name="softwareOnBehalfOfOrg" value="${software_on_behalf_of_org}"/>
		<c:if test="${not empty exception}">		
		
			<input type="hidden" id="error" name="error" value="${fn:escapeXml(exception)}" />
 		
 		</c:if>
 
  	<script integrity= "${libsHashCode}" crossorigin="anonymous" src="${cdnBaseURL}/consent-ui/${buildVersion}/js/libs.min.js"></script>
	<script integrity="${templateHashCode}" crossorigin="anonymous" src="${cdnBaseURL}/consent-ui/${buildVersion}/js/templates.min.js"></script>
	<script integrity="${appHashCode}" crossorigin="anonymous" src="${cdnBaseURL}/consent-ui/${buildVersion}/js/app.min.js"></script>

 
<%-- 	<script src="${cdnBaseURL}/consent-ui/${buildVersion}/js/libs.min.js"></script>
	<script src="${cdnBaseURL}/consent-ui/${buildVersion}/js/templates.js"></script>
	<script  src="${cdnBaseURL}/consent-ui/${buildVersion}/js/app.js"></script> 
 --%></body>
</html>
