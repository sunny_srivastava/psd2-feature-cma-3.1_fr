/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.token;

import java.util.Map;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * The Class Token.
 */
@JsonIgnoreProperties(ignoreUnknown=true) 
@JsonInclude(Include.NON_NULL)
public class Token {
	
	private String client_id; //NOSONAR
	private TppInformationTokenData tppInformation;
	private Set<String> scope;
	private Map<String,String> seviceParams;
	private String user_name; //NOSONAR
	private ConsentTokenData consentTokenData;
	private String consentType;
	private String requestId;
	private Map<String,Set<Object>> claims;
	private long exp;
	
	public Map<String, String> getSeviceParams() {
		return seviceParams;
	}
	public void setSeviceParams(Map<String, String> seviceParams) {
		this.seviceParams = seviceParams;
	}
	public String getUser_name() {//NOSONAR
		return user_name;
	}
	public void setUser_name(String user_name) {//NOSONAR
		this.user_name = user_name;
	}
	public String getClient_id() {//NOSONAR
		return client_id;
	}
	public void setClient_id(String client_id) {//NOSONAR
		this.client_id = client_id;
	}
	public TppInformationTokenData getTppInformation() {
		return tppInformation;
	}
	public void setTppInformation(TppInformationTokenData tppInformation) {
		this.tppInformation = tppInformation;
	}
	public ConsentTokenData getConsentTokenData() {
		return consentTokenData;
	}
	public void setConsentTokenData(ConsentTokenData consentTokenData) {
		this.consentTokenData = consentTokenData;
	}
	public String getConsentType() {
		return consentType;
	}
	public void setConsentType(String consentType) {
		this.consentType = consentType;
	}
	public String getRequestId() {
		return requestId;
	}
	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}
	public Set<String> getScope() {
		return scope;
	}
	public void setScope(Set<String> scope) {
		this.scope = scope;
	}
	public Map<String, Set<Object>> getClaims() {
		return claims;
	}
	public void setClaims(Map<String, Set<Object>> claims) {
		this.claims = claims;
	}
	public long getExp() {
		return exp;
	}
	public void setExp(long exp) {
		this.exp = exp;
	}
}