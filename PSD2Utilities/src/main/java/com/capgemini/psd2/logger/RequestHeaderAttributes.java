/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.logger;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.token.Token;
import com.capgemini.psd2.utilities.GenerateUniqueIdUtilities;

/**
 * The Class RequestHeaderAttributes.
 */
@Component
@Scope(value = "request", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class RequestHeaderAttributes {

	/** The correlation id. */
	private String correlationId;

	/** The request id. */
	private UUID requestId = GenerateUniqueIdUtilities.getUniqueId();

	/** The tpp CID. */
	private String tppCID;

	/** The tpp legal entity name. */
	private String tppLegalEntityName;

	private Token token;
	
	/** The psu id. */
	private String psuId;

	/** The claims. */
	private Set<Object> claims;

	/** The customer IP address. */
	private String customerIPAddress;

	/** The customer last logged time. */
	private String customerLastLoggedTime;

	/** The financial id. */
	private String financialId;

	/** The self url. */
	private String selfUrl;
	
	private String methodType;
	
	/** The idempotency Key */
	private String idempotencyKey;
	
	private String channelId;
	
	private String intentId;

	private String scopes;
	
	private String x_ssl_client_dn; //NOSONAR
	
	private String x_ssl_client_cn; //NOSONAR
	
	private String x_ssl_client_ou; //NOSONAR
	
	private String x_ssl_client_cert; //NOSONAR
	
	private String tenantId;
	
	private String x_ssl_client_ncaid; //NOSONAR
	
	private String jwksUrlFiles;
	
	private String jwsHeader;
	
	private boolean bypassConsentPage; 
	
	public String getJwksUrlFiles() {
		return jwksUrlFiles;
	}

	public void setJwksUrlFiles(String jwksUrlFiles) {
		this.jwksUrlFiles = jwksUrlFiles;
	}

	public String getX_ssl_client_ncaid() { //NOSONAR
		return x_ssl_client_ncaid;
	}

	public void setX_ssl_client_ncaid(String x_ssl_client_ncaid) { //NOSONAR
		this.x_ssl_client_ncaid = x_ssl_client_ncaid;
	}

	public String getTenantId() {
		return tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}

	public String getX_ssl_client_cert() { //NOSONAR
		return x_ssl_client_cert;
	}

	public void setX_ssl_client_cert(String x_ssl_client_cert) { //NOSONAR
		this.x_ssl_client_cert = x_ssl_client_cert;
	}
	
	public String getJwsHeader() {
		return jwsHeader;
	}

	public void setJwsHeader(String jwsHeader) {
		this.jwsHeader = jwsHeader;
	}
	
	public String getX_ssl_client_dn() { //NOSONAR
		return x_ssl_client_dn;
	}

	public void setX_ssl_client_dn(String x_ssl_client_dn) { //NOSONAR
		this.x_ssl_client_dn = x_ssl_client_dn;
	}

	public String getX_ssl_client_cn() { //NOSONAR
		return x_ssl_client_cn;
	}

	public void setX_ssl_client_cn(String x_ssl_client_cn) {  //NOSONAR
		this.x_ssl_client_cn = x_ssl_client_cn;
	}

	public String getX_ssl_client_ou() { //NOSONAR
		return x_ssl_client_ou;
	}

	public void setX_ssl_client_ou(String x_ssl_client_ou) { //NOSONAR
		this.x_ssl_client_ou = x_ssl_client_ou;
	}
	public String getIdempotencyKey() {
		return idempotencyKey;
	}

	public void setIdempotencyKey(String idempotencyKey) {
		this.idempotencyKey = idempotencyKey;
	}
	
	public String getIntentId() {
		return intentId;
	}

	public void setIntentId(String intentId) {
		this.intentId = intentId;
	}

	public String getChannelId() {
		return channelId;
	}

	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}	
	
	public String getMethodType() {
		return methodType;
	}

	public void setMethodType(String methodType) {
		this.methodType = methodType;
	}

	/**
	 * Gets the self url.
	 *
	 * @return the self url
	 */
	public String getSelfUrl() {
		return selfUrl;
	}

	/**
	 * Sets the self url.
	 *
	 * @param selfUrl
	 *            the new self url
	 */
	public void setSelfUrl(String selfUrl) {
		this.selfUrl = selfUrl;
	}

	/**
	 * Gets the correlation id.
	 *
	 * @return the correlation id
	 */
	public String getCorrelationId() {
		return correlationId;
	}

	/**
	 * Sets the correlation id.
	 *
	 * @param correlationId
	 *            the new correlation id
	 */
	public void setCorrelationId(String correlationId) {
		this.correlationId = correlationId;
	}

	
	
	public void setTppCID(String tppCID) {
		this.tppCID = tppCID;
	}

	/**
	 * Gets the tpp CID.
	 *
	 * @return the tpp CID
	 */
	public String getTppCID() {
		return tppCID;
	}

	/**
	 * Gets the request id.
	 *
	 * @return the request id
	 */
	public UUID getRequestId() {
		return requestId;
	}

	/**
	 * Gets the tpp legal entity name.
	 *
	 * @return the tpp legal entity name
	 */
	public String getTppLegalEntityName() {
		return tppLegalEntityName;
	}


	
	public void setTppLegalEntityName(String tppLegalEntityName) {
		this.tppLegalEntityName = tppLegalEntityName;
	}

	/**
	 * Gets the psu id.
	 *
	 * @return the psu id
	 */
	public String getPsuId() {
		return psuId;
	}

	/**
	 * Gets the claims.
	 *
	 * @return the claims
	 */
	public Set<Object> getClaims() {
		return claims;
	}

	/**
	 * Gets the customer IP address.
	 *
	 * @return the customer IP address
	 */
	public String getCustomerIPAddress() {
		return customerIPAddress;
	}

	/**
	 * Sets the customer IP address.
	 *
	 * @param customerIPAddress
	 *            the new customer IP address
	 */
	public void setCustomerIPAddress(String customerIPAddress) {
		this.customerIPAddress = customerIPAddress;
	}

	/**
	 * Gets the customer last logged time.
	 *
	 * @return the customer last logged time
	 */
	public String getCustomerLastLoggedTime() {
		return customerLastLoggedTime;
	}

	/**
	 * Sets the customer last logged time.
	 *
	 * @param customerLastLoggedTime
	 *            the new customer last logged time
	 */
	public void setCustomerLastLoggedTime(String customerLastLoggedTime) {
		this.customerLastLoggedTime = customerLastLoggedTime;
	}

	/**
	 * Gets the financial id.
	 *
	 * @return the financial id
	 */
	public String getFinancialId() {
		return financialId;
	}

	/**
	 * Sets the financial id.
	 *
	 * @param financialId
	 *            the new financial id
	 */
	public void setFinancialId(String financialId) {
		this.financialId = financialId;
	}

	public void setPsuId(String psuId) {
		this.psuId = psuId;
	}

	/**
	 * Sets the attributes.
	 *
	 * @param token
	 *            the new attributes
	 */
	public void setAttributes(Token token) {
		this.tppCID = token.getClient_id();
		if (token.getTppInformation() == null) {
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.INVALID_ACCESS_TOKEN);
		}
		this.tppLegalEntityName = token.getTppInformation().getTppLegalEntityName();
		this.psuId = token.getUser_name();
		this.token = token;
		populateClaims();
	}
	
	private void populateClaims(){
		Set<Object>claims = new HashSet<Object>(); //NOSONAR
		Set<String> scopes = this.token.getScope(); //NOSONAR
		if(this.token.getClaims() == null || this.token.getClaims().isEmpty()) {
			return;
		}
		if(scopes == null || scopes.isEmpty()) {
			return;
		}
		for(String scope : scopes){
			if(this.token.getClaims().get(scope) != null && !this.token.getClaims().get(scope).isEmpty()){
				claims.addAll(this.token.getClaims().get(scope));
			}
		}
		this.claims = claims;
	}

	/**
	 * Sets the claims.
	 *
	 * @param claims
	 *            the new claims
	 */
	public void setClaims(Set<Object> claims) {
		this.claims = claims;
	}

	public Token getToken() {
		return token;
	}

	public void setToken(Token token) {
		this.token = token;
	}

	public String getScopes() {
		return scopes;
	}

	public void setScopes(String scopes) {
		this.scopes = scopes;
	}
	
	public boolean isBypassConsentPage() {
		return bypassConsentPage;
	}

	public void setBypassConsentPage(boolean bypassConsentPage) {
		this.bypassConsentPage = bypassConsentPage;
	}
	
}
