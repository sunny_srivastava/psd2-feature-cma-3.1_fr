/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.logger;

import java.util.UUID;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

/**
 * The Class LoggerAttribute.
 */
@Component
@Scope(value = "request", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class LoggerAttribute {

	/** The request id. */
	private UUID requestId;

	/** The correlation id. */
	private String correlationId;

	/** The api id. */
	private String apiId;

	/** The tpp CID. */
	private String tppCID;

	/** The tpp legal entity name. */
	private String tppLegalEntityName;

	/** The message. */
	private String message;

	/** The psu id. */
	private String psuId;

	private String methodType;

	private String path;

	/** The customer IP address. */
	private String customerIPAddress;

	/** The customer last logged time. */
	private String customerLastLoggedTime;

	/** The financial id. */
	private String financialId;

	/** The operation name. */
	private String operationName;

	/** The upstream api id. */
	private String upstream_api_id;

	/** The upstream request Payload. */
	private String upstream_request_payload;

	/** The upstream response Payload. */
	private String upstream_response_payload;

	/** The upstream start time. */
	private String upstream_start_time;

	/** The upstream end time. */
	private String upstream_end_time;

	/** The time stamp */
	private String timeStamp;

	/** intent id*/
	private String intentId;
	
	private String channelId;
	
	private String x_ssl_client_dn; //NOSONAR
	
	private String x_ssl_client_cn; //NOSONAR
	
	private String x_ssl_client_ou; //NOSONAR
	
	private String tenantId;
	
	private String x_ssl_client_cert; //NOSONAR
	
	private String x_ssl_client_ncaid; //NOSONAR
	
	public String getX_ssl_client_ncaid() { //NOSONAR
		return x_ssl_client_ncaid;
	}

	public void setX_ssl_client_ncaid(String x_ssl_client_ncaid) { //NOSONAR
		this.x_ssl_client_ncaid = x_ssl_client_ncaid;
	} 
	
	public String getX_ssl_client_cert() { //NOSONAR
		return x_ssl_client_cert;
	}

	public void setX_ssl_client_cert(String x_ssl_client_cert) { //NOSONAR
		this.x_ssl_client_cert = x_ssl_client_cert;
	}

	public String getTenantId() {
		return tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}
	public String getX_ssl_client_dn() { //NOSONAR
		return x_ssl_client_dn;
	}

	public void setX_ssl_client_dn(String x_ssl_client_dn) { //NOSONAR
		this.x_ssl_client_dn = x_ssl_client_dn;
	}

	public String getX_ssl_client_cn() { //NOSONAR
		return x_ssl_client_cn;
	}

	public void setX_ssl_client_cn(String x_ssl_client_cn) { //NOSONAR
		this.x_ssl_client_cn = x_ssl_client_cn;
	}

	public String getX_ssl_client_ou() { //NOSONAR
		return x_ssl_client_ou;
	}

	public void setX_ssl_client_ou(String x_ssl_client_ou) { //NOSONAR
		this.x_ssl_client_ou = x_ssl_client_ou;
	}
	/**
	 * Gets the request id.
	 *
	 * @return the request id
	 */
	public UUID getRequestId() {
		return requestId;
	}

	public String getMethodType() {
		return methodType;
	}

	public void setMethodType(String methodType) {
		this.methodType = methodType;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	/**
	 * Sets the request id.
	 *
	 * @param requestId
	 *            the new request id
	 */
	public void setRequestId(UUID requestId) {
		this.requestId = requestId;
	}

	/**
	 * Gets the correlation id.
	 *
	 * @return the correlation id
	 */
	public String getCorrelationId() {
		return correlationId;
	}

	/**
	 * Sets the correlation id.
	 *
	 * @param correlationId
	 *            the new correlation id
	 */
	public void setCorrelationId(String correlationId) {
		this.correlationId = correlationId;
	}

	/**
	 * Gets the api id.
	 *
	 * @return the api id
	 */
	public String getApiId() {
		return apiId;
	}

	/**
	 * Sets the api id.
	 *
	 * @param apiId
	 *            the new api id
	 */
	public void setApiId(String apiId) {
		this.apiId = apiId;
	}

	/**
	 * Gets the tpp CID.
	 *
	 * @return the tpp CID
	 */
	public String getTppCID() {
		return tppCID;
	}

	/**
	 * Sets the tpp CID.
	 *
	 * @param tppCID
	 *            the new tpp CID
	 */
	public void setTppCID(String tppCID) {
		this.tppCID = tppCID;
	}

	/**
	 * Gets the tpp legal entity name.
	 *
	 * @return the tpp legal entity name
	 */
	public String getTppLegalEntityName() {
		return tppLegalEntityName;
	}

	/**
	 * Sets the tpp legal entity name.
	 *
	 * @param tppLegalEntityName
	 *            the new tpp legal entity name
	 */
	public void setTppLegalEntityName(String tppLegalEntityName) {
		this.tppLegalEntityName = tppLegalEntityName;
	}

	/**
	 * Gets the message.
	 *
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * Sets the message.
	 *
	 * @param message
	 *            the new message
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * Gets the psu id.
	 *
	 * @return the psu id
	 */
	public String getPsuId() {
		return psuId;
	}

	/**
	 * Sets the psu id.
	 *
	 * @param psuId
	 *            the new psu id
	 */
	public void setPsuId(String psuId) {
		this.psuId = psuId;
	}

	/**
	 * Gets the customer IP address.
	 *
	 * @return the customer IP address
	 */
	public String getCustomerIPAddress() {
		return customerIPAddress;
	}

	/**
	 * Sets the customer IP address.
	 *
	 * @param customerIPAddress
	 *            the new customer IP address
	 */
	public void setCustomerIPAddress(String customerIPAddress) {
		this.customerIPAddress = customerIPAddress;
	}

	/**
	 * Gets the customer last logged time.
	 *
	 * @return the customer last logged time
	 */
	public String getCustomerLastLoggedTime() {
		return customerLastLoggedTime;
	}

	/**
	 * Sets the customer last logged time.
	 *
	 * @param customerLastLoggedTime
	 *            the new customer last logged time
	 */
	public void setCustomerLastLoggedTime(String customerLastLoggedTime) {
		this.customerLastLoggedTime = customerLastLoggedTime;
	}

	/**
	 * Gets the financial id.
	 *
	 * @return the financial id
	 */
	public String getFinancialId() {
		return financialId;
	}

	/**
	 * Sets the financial id.
	 *
	 * @param financialId
	 *            the new financial id
	 */
	public void setFinancialId(String financialId) {
		this.financialId = financialId;
	}

	/**
	 * @return the operationName
	 */
	public String getOperationName() {
		return operationName;
	}

	/**
	 * @param operationName
	 *            the operationName to set
	 */
	public void setOperationName(String operationName) {
		this.operationName = operationName;
	}

	/**
	 * @return the upstream_api_id
	 */
	public String getUpstream_api_id() { //NOSONAR
		return upstream_api_id;
	}

	/**
	 * @param upstream_api_id
	 *            the upstream_api_id to set
	 */
	public void setUpstream_api_id(String upstream_api_id) { //NOSONAR
		this.upstream_api_id = upstream_api_id;
	}

	/**
	 * @return the upstream_request_payload
	 */
	public String getUpstream_request_payload() { //NOSONAR
		return upstream_request_payload;
	}

	/**
	 * @param upstream_request_payload
	 *            the upstream_request_payload to set
	 */
	public void setUpstream_request_payload(String upstream_request_payload) { //NOSONAR
		this.upstream_request_payload = upstream_request_payload;
	}

	/**
	 * @return the upstream_response_payload
	 */
	public String getUpstream_response_payload() { //NOSONAR
		return upstream_response_payload;
	}

	/**
	 * @param upstream_response_payload
	 *            the upstream_response_payload to set
	 */
	public void setUpstream_response_payload(String upstream_response_payload) { //NOSONAR
		this.upstream_response_payload = upstream_response_payload;
	}

	/**
	 * @return the upstream_start_time
	 */
	public String getUpstream_start_time() { //NOSONAR
		return upstream_start_time;
	}

	/**
	 * @param upstream_start_time
	 *            the upstream_start_time to set
	 */
	public void setUpstream_start_time(String upstream_start_time) { //NOSONAR
		this.upstream_start_time = upstream_start_time;
	}

	/**
	 * @return the upstream_end_time
	 */
	public String getUpstream_end_time() { //NOSONAR
		return upstream_end_time;
	}

	/**
	 * @param upstream_end_time
	 *            the upstream_end_time to set
	 */
	public void setUpstream_end_time(String upstream_end_time) { //NOSONAR
		this.upstream_end_time = upstream_end_time;
	}

	/**
	 * @return the timeStamp
	 */
	public String getTimeStamp() {
		return timeStamp;
	}

	/**
	 * @param timeStamp
	 *            the timeStamp to set
	 */
	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}
	
	public String getIntentId() {
		return intentId;
	}

	public void setIntentId(String intentId) {
		this.intentId = intentId;
	}
	
	

	public String getChannelId() {
		return channelId;
	}

	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "timestamp\":\"" + timeStamp + "\",\"requestId\":\"" + requestId + "\",\"correlationId\":\""
				+ correlationId + "\",\"methodType\":\"" + methodType + "\",\"path\":\"" + path + "\",\"apiId\":\""
				+ apiId + "\",\"operationName\":\"" + operationName + "\",\"tppCID\":\"" + tppCID
				+ "\",\"tppLegalEntityName\":\"" + tppLegalEntityName + "\",\"psuId\":\"" + psuId
				+ (intentId != null && !intentId.trim().isEmpty() ? ("\",\"intentId\":\"" + intentId) : "")
				+ (channelId != null && !channelId.trim().isEmpty() ? ("\",\"channelId\":\"" + channelId) : "")
				+ "\",\"message\":\"" + message + "\",\"upstream_start_time\":\"" + upstream_start_time
				+ "\",\"upstream_end_time\":\"" + upstream_end_time + "\",\"upstream_api_id\":\"" + upstream_api_id
				+ "\",\"x_ssl_client_dn\":\"" + x_ssl_client_dn + "\",\"x_ssl_client_cn\":\"" + x_ssl_client_cn
				+ "\",\"x_ssl_client_ou\":\""
				+ x_ssl_client_ou+ "\",\"tenantId\":\""
								+ tenantId +"\",\"x_ssl_cert\":\""
										+ x_ssl_client_cert+"\",\"x_ssl_client_ncaid\":\""
												+ x_ssl_client_ncaid;
	}

}
