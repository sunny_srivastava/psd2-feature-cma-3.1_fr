/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.exceptions;

import java.io.Serializable;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import net.minidev.json.JSONValue;

/**
 * The Class ErrorInfo.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(Include.NON_NULL)
public class ErrorInfo implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The error code. */
	private String errorCode;
	
	/** The error message. */
	private String errorMessage;
	
	/** The detail error message. */
	private String detailErrorMessage;
	
	/** The jti. */
	private String jti;
	
	/** The status code. */
	@JsonIgnore
	private String statusCode;
	
	@JsonIgnore
	private String obErrorCode;


	/** The oauth server token refresh URL. */
	private String oauthServerTokenRefreshURL;
	
	protected static Map<String, String> errorMap;
	
	private static Boolean sendDetailErrorMessage;
	
	protected static Boolean sendErrorPayload;
	
	public ErrorInfo() {
		super();
	}
	
	public ErrorInfo(String statusCode) {
		super();
		this.statusCode = statusCode;
	}
	
	public ErrorInfo(String errorCode, String errorMessage, String statusCode) {
		super();
		this.statusCode = statusCode;
		this.errorCode = errorCode;
		this.setErrorMessage(errorMessage);
	}
	
	public ErrorInfo(String errorCode, String errorMessage, String detailErrorMessage, String statusCode) {
		super();
		this.statusCode = statusCode;
		this.errorCode = errorCode;
		this.setErrorMessage(errorMessage);
		this.detailErrorMessage = detailErrorMessage;
	}
	
	public ErrorInfo(String errorCode, String errorMessage, String detailErrorMessage, String statusCode,
			String obErrorCode) {
		super();
		this.errorCode = errorCode;
		this.setErrorMessage(errorMessage);
		this.detailErrorMessage = detailErrorMessage;
		this.statusCode = statusCode;
		this.obErrorCode = obErrorCode;
	}
	
	public String getObErrorCode() {
		return obErrorCode;
	}

	public void setObErrorCode(String obErrorCode) {
		this.obErrorCode = obErrorCode;
	}

	/**
	 * Gets the error code.
	 *
	 * @return the error code
	 */
	public String getErrorCode() {
		if(sendErrorPayload!=null && !sendErrorPayload)
			return null;
		else
			return errorCode;
	}

	/**
	 * Sets the error code.
	 *
	 * @param errorCode the new error code
	 */
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	/**
	 * Gets the error message.
	 *
	 * @return the error message
	 */
	public String getErrorMessage() {
		if(sendErrorPayload!=null && !sendErrorPayload)
			return null;
		else
			return errorMessage;
	}

	/**
	 * Sets the error message.
	 *
	 * @param errorMessage the new error message
	 */
	public void setErrorMessage(String errorMessage) {
		if(errorMap!=null && errorMap.get(statusCode)!=null){
			this.errorMessage =errorMap.get(statusCode);
		} else{
			this.errorMessage = errorMessage;
		}
		
	}
	
	/**
	 * Sets the error message to Null.
	 *
	 * @param errorMessage the new error message
	 */
	public void setErrorMessageAsNull() {
		this.errorMessage = null;
	}

	/**
	 * Gets the detail error message.
	 *
	 * @return the detail error message
	 */
	public String getDetailErrorMessage() {
		if(sendDetailErrorMessage!=null && !sendDetailErrorMessage)
			return null;
		else
			return detailErrorMessage;
	}

	/**
	 * Sets the detail error message.
	 *
	 * @param detailErrorMessage the new detail error message
	 */
	public void setDetailErrorMessage(String detailErrorMessage) {
		this.detailErrorMessage = detailErrorMessage;
	}

	/**
	 * Gets the jti.
	 *
	 * @return the jti
	 */
	public String getJti() {
		return jti;
	}

	/**
	 * Sets the jti.
	 *
	 * @param jti the new jti
	 */
	public void setJti(String jti) {
		this.jti = jti;
	}
	
	/**
	 * Gets the status code.
	 *
	 * @return the status code
	 */
	public String getStatusCode() {
		return statusCode;
	}

	/**
	 * Sets the status code.
	 *
	 * @param statusCode the new status code
	 */
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	/**
	 * Gets the oauth server token refresh URL.
	 *
	 * @return the oauth server token refresh URL
	 */
	public String getOauthServerTokenRefreshURL() {
		return oauthServerTokenRefreshURL;
	}

	/**
	 * Sets the oauth server token refresh URL.
	 *
	 * @param oauthServerTokenRefreshURL the new oauth server token refresh URL
	 */
	public void setOauthServerTokenRefreshURL(String oauthServerTokenRefreshURL) {
		this.oauthServerTokenRefreshURL = oauthServerTokenRefreshURL;
	}
	
	@JsonIgnore
	public String getActualErrorCode(){
		return errorCode;
	}
	
	@JsonIgnore
	public String getActualErrorMessage(){
		return errorMessage;
	}
	
	@JsonIgnore
	public String getActualDetailErrorMessage(){
		return detailErrorMessage;
	}

	/**
	 * To message.
	 *
	 * @return the string
	 */
	public String toMessage() {
		return "ErrorInfo [errorCode=" + errorCode + ", errorMessage=" + errorMessage + ", detailErrorMessage="
				+ detailErrorMessage + ", statusCode="+statusCode+"]";
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "{\"errorCode\":\"" + errorCode + "\",\"errorMessage\":\"" + errorMessage + "\",\"detailErrorMessage\":\""
				+ JSONValue.escape(detailErrorMessage) +"\",\"statusCode\":\""+statusCode+"\"}";
	}
	
	
	public static void loadMap(Map<String, String> map, Boolean sentDetailErrorMessage , Boolean sentErrorPayload){
		errorMap = map;
		sendDetailErrorMessage= sentDetailErrorMessage;
		sendErrorPayload = sentErrorPayload;
	}
	
	
}
