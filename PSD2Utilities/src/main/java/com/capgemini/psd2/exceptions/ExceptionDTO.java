package com.capgemini.psd2.exceptions;

public class ExceptionDTO {

	private OBErrorCodeEnum obErrorCodeEnum;
	
	private String errorMessageKey;
	
	private boolean dynamicMessage;
	
	public ExceptionDTO(OBErrorCodeEnum obErrorCodeEnum, String errorMessageKey) {
		super();
		this.obErrorCodeEnum = obErrorCodeEnum;
		this.errorMessageKey = errorMessageKey;
	}

	public ExceptionDTO(OBErrorCodeEnum obErrorCodeEnum, String errorMessageKey, boolean dynamicMessage) {
		super();
		this.obErrorCodeEnum = obErrorCodeEnum;
		this.errorMessageKey = errorMessageKey;
		this.dynamicMessage = dynamicMessage;
	}

	public boolean isDynamicMessage() {
		return dynamicMessage;
	}

	public void setDynamicMessage(boolean dynamicMessage) {
		this.dynamicMessage = dynamicMessage;
	}

	public OBErrorCodeEnum getObErrorCodeEnum() {
		return obErrorCodeEnum;
	}

	public void setObErrorCodeEnum(OBErrorCodeEnum obErrorCodeEnum) {
		this.obErrorCodeEnum = obErrorCodeEnum;
	}

	public String getErrorMessageKey() {
		return errorMessageKey;
	}

	public void setErrorMessageKey(String errorMessageKey) {
		this.errorMessageKey = errorMessageKey;
	}
}
