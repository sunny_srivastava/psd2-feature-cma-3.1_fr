package com.capgemini.psd2.exceptions;

public interface InternalServerErrorMessage {

	/// custom error messages for logging in case of throwing 500 errors

	String PISP_DUPLICATE_PAYMENT_ID = "Due to duplicate paymentId, payment setup resource is not updated successfully";

	String PISP_NO_PAYMENT_SETUP_RESOURCES_FOUND = "Staged Payment setup details are not found in bank for given paymentId.";

	String PISP_INCOMPATIBLE_SETUP_FOUND_IN_SYSTEM = "Incompatible staged setup resource found in system";

	String TECHNICAL_ERROR_PLATFORM_SUBMISSION_CREATION = "Technical error, while creating submission resource in platform.";

	String PAYMENT_SUBMISSION_CREATION_FAILED = "Payment submission resource is not created successfully at bank end";

	String PISP_PAYMENT_SETUP_CREATION_FAILED = "Payment setup resource is not created successfully at bank end.";

	String TECHNICAL_ERROR = "Techical error.";

	String PISP_IDEMPOTENCY_DURATION_INCORRECT_FORMAT = "Idempotency duration is not configured in correct format.";

	String IBAN_VALIDATION_FAILED = "iban validation failed in response.";

	String BBAN_VALIDATION_FAILED = "bban validation failed in response.";

	String UNSUPPORTED_CURRENCY = "unsupported currency found in response";

	String INVALID_COUNTRY_CODE = "invalid country code found in response";

	String INVALID_ADDRESS_LENGTH = "address line found with invalid length in response";

	String INVALID_SORTCODE_NUMBER = "sortcode account number validation failed in response";
	
	String INVALID_PAN_NUMBER = "sortcode account number validation failed in response";

	String INVALID_COUNTRY_SUBDIVISION = "invalid country subdivison found in response";

	String INVALID_LOCAL_INSTRUMENT = "invalid local instrument found in response";

	String TECHNICAL_ERROR_FS_PAYMENT_RETRIEVE = "technical error, while fetching payment setup resource from bank";

	String PISP_INSTRUCTED_AMT_NOT_MATCHING_UNIT_CURRENCY_OR_CURRENCY_OF_TRANSFER = "Instructed Amount Currency should match Unit Currency or Currency of Transfer";

	String PISP_EXCHANGE_RATE_INFO_NOT_FOUND = "Exchange Rate Details Not Found";

	String PISP_CHARGES_INFO_NOT_FOUND = "Charge Details Not Found";

	String PISP_TECHNICAL_ERROR_FS_PRE_STAGE_INSERTION = "Technical error during pre-stage payment validation from platform to bank";

	String PISP_TECHNICAL_ERROR_FS_PAYMENT_RETRIEVE = "Technical error, while fetching payment setup resource from bank.";

	String PISP_TECHNICAL_ERROR_FS_PRE_STAGE_UPDATION = "Technical error, during pre-stage payment updation from platform to bank.";

	String INVALID_SCHEME_RESPONSE = "Invalid scheme found in response";

	String CONNECTION_ERROR = "Technical error while establishing data base connection";

	String INVALID_PRODUCT_TYPE = "Product details does not match with provided product type";

	String MISSING_OTHER_PRODUCT_TYPE = "As ProductType OTHER is chosen, the object OtherProductType must be populated";

	String INSUFFICIENT_DATA_FOR_DETAIL_PERMISSION = "Insufficient data for SchedulePayment detail permissions";

	String ACCOUNT_REQUEST_ERROR = "Account request error";

	String INVALID_DATE_ERROR = "invalid date format found in response";
	
	String JSON_PROCESSING_ERROR = "Technical Error, JSON Could Not Extracted From Object";
	
	String MULTI_AUTH_STATUS_MISSING = "Missing Multi-Auth Status in Response";
	
	String MULTI_AUTH_ISSUE = "There seems to be a issue with Multi-Auth Block";
	
	String PISP_INVALID_ID="There seems to be a issue with id format";
	
	String PISP_INVALID_COMBINATION ="Invalid Combination of fields in response";
	
	String PISP_INVALID_STATUS = "Invalid Status in Body and Multi-Auth Block";

	String AMOUNT_VALIDATION_FAILED ="Invalid amount response";

	String SANDBOX_MOCKED_ERROR = "Internal Server Error due to Sandbox Mocking";
	
	String REQUEST_BODY_EMPTY = "Request body or Mandatory field cannot be empty";
	
	String CREDITOR_OR_DEBTOR_AGENT_INCOMPLETE = "Creditor or Debtor Agent Details is not valid";
	
	String INVALID_VALUE_INCLUDE = "Include field is null";
	
	String ACCOUNT_IDENTIFIER_INVALID = "Account Identification is Invalid";
	
	String INVALID_BANKTRANSACTION_CODE = "Invalid Bank Transaction Code";
	
	String INVALID_BANKTRANSACTION_SUBCODE = "Invalid Bank Transaction SubCode";

}
