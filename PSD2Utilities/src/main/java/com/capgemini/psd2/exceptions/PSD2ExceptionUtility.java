package com.capgemini.psd2.exceptions;

import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.yaml.snakeyaml.Yaml;

@RestController
public class PSD2ExceptionUtility {

	@Value("${spring.profiles:null}")
	private String profile;
	
	@Value("${spring.cloud.config.uri:null}")
	private String configServerUrl;
	
	/** The connection time out. */
	@Value("${app.sentDetailErrorMessage:#{false}}")
	private Boolean sentDetailErrorMessage;
	
	@Value("${app.sendErrorPayload:#{false}}")
	private Boolean sentErrorPayload;

	@Autowired
	private RestTemplate restTemplate;
	
	private static Map<String,String> errorMap;

	@PostConstruct
	public void init() {
		updateErrorMapping();
	}

	public void updateErrorMapping() {
			try { 
				String url = configServerUrl + "/" + "errorMapping" + "-" + profile + ".yml";
				Object value = restTemplate.getForObject(url, String.class);
				Yaml yaml = new Yaml();
				errorMap = (Map<String, String>) yaml.load(value.toString()); //NOSONAR
				ErrorInfo.loadMap(errorMap, sentDetailErrorMessage,sentErrorPayload);
			} catch(Exception e) {
				// Leaving empty for Dev Portal register issue
			}
	}

	public static String getErrorMessagefromStatusCode(String statusCode){
		if(errorMap!=null){
			return errorMap.get(statusCode);
		}
		return null;
	}
}