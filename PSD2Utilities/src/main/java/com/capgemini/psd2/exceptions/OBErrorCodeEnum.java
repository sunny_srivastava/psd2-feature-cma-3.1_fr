package com.capgemini.psd2.exceptions;

import org.springframework.http.HttpStatus;

public enum OBErrorCodeEnum { 
	
	UK_OBIE_UNEXPECTEDERROR("UK.OBIE.UnexpectedError",HttpStatus.INTERNAL_SERVER_ERROR,OBPSD2ExceptionUtility.genericErrorMessages.get(ErrorMapKeys.INTERNAL),"500 Internal Server Error"),
	
	UK_OBIE_UNSUPPORTED_FREQUENCY("UK.OBIE.Unsupported.Frequency",HttpStatus.BAD_REQUEST,OBPSD2ExceptionUtility.genericErrorMessages.get(ErrorMapKeys.FREQUENCY_GENERIC),"400 BadRequest"),
	
	UK_OBIE_RULES_AFTERCUTOFFDATETIME("UK.OBIE.Rules.AfterCutOffDateTime",HttpStatus.BAD_REQUEST,OBPSD2ExceptionUtility.genericErrorMessages.get(ErrorMapKeys.FIELD),"400 BadRequest"),
	
	UK_OBIE_FIELD_EXPECTED("UK.OBIE.Field.Expected",HttpStatus.BAD_REQUEST,OBPSD2ExceptionUtility.genericErrorMessages.get(ErrorMapKeys.FIELD),"400 BadRequest"),
	
	UK_OBIE_FIELD_INVALID("UK.OBIE.Field.Invalid",HttpStatus.BAD_REQUEST,OBPSD2ExceptionUtility.genericErrorMessages.get(ErrorMapKeys.FIELD),"400 BadRequest"),
	
	UK_OBIE_FIELD_INVALIDDATE("UK.OBIE.Field.InvalidDate",HttpStatus.BAD_REQUEST,OBPSD2ExceptionUtility.genericErrorMessages.get(ErrorMapKeys.FIELD),"400 BadRequest"),
	
	UK_OBIE_FIELD_MISSING("UK.OBIE.Field.Missing",HttpStatus.BAD_REQUEST,OBPSD2ExceptionUtility.genericErrorMessages.get(ErrorMapKeys.FIELD),"400 BadRequest"),
	
	UK_OBIE_FIELD_UNEXPECTED("UK.OBIE.Field.Unexpected",HttpStatus.BAD_REQUEST,OBPSD2ExceptionUtility.genericErrorMessages.get(ErrorMapKeys.FIELD),"400 BadRequest"),
	
	UK_OBIE_HEADER_INVALID("UK.OBIE.Header.Invalid",HttpStatus.BAD_REQUEST,OBPSD2ExceptionUtility.genericErrorMessages.get(ErrorMapKeys.HEADER_GENERIC),"400 BadRequest"),
	
	UK_OBIE_HEADER_MISSING("UK.OBIE.Header.Missing",HttpStatus.BAD_REQUEST,OBPSD2ExceptionUtility.genericErrorMessages.get(ErrorMapKeys.HEADER_GENERIC),"400 BadRequest"),
	
	UK_OBIE_SIGNATURE_INVALID("UK.OBIE.Signature.Invalid",HttpStatus.BAD_REQUEST,OBPSD2ExceptionUtility.genericErrorMessages.get(ErrorMapKeys.SIGNATURE),"400 BadRequest"),
	
	UK_OBIE_SIGNATURE_INVALIDCLAIM("UK.OBIE.Signature.InvalidClaim",HttpStatus.BAD_REQUEST,OBPSD2ExceptionUtility.genericErrorMessages.get(ErrorMapKeys.SIGNATURE),"400 BadRequest"),
	
	UK_OBIE_SIGNATURE_MISSINGCLAIM("UK.OBIE.Signature.MissingClaim",HttpStatus.BAD_REQUEST,OBPSD2ExceptionUtility.genericErrorMessages.get(ErrorMapKeys.SIGNATURE),"400 BadRequest"),
	
	UK_OBIE_SIGNATURE_MALFORMED("UK.OBIE.Signature.Malformed",HttpStatus.BAD_REQUEST,OBPSD2ExceptionUtility.genericErrorMessages.get(ErrorMapKeys.SIGNATURE),"400 BadRequest"),
	
	UK_OBIE_SIGNATURE_MISSING("UK.OBIE.Signature.Missing",HttpStatus.BAD_REQUEST,OBPSD2ExceptionUtility.genericErrorMessages.get(ErrorMapKeys.SIGNATURE),"400 BadRequest"),
	
	UK_OBIE_SIGNATURE_UNEXPECTED("UK.OBIE.Signature.Unexpected",HttpStatus.BAD_REQUEST,OBPSD2ExceptionUtility.genericErrorMessages.get(ErrorMapKeys.SIGNATURE),"400 BadRequest"),
	
	UK_OBIE_RESOURCE_CONSENTMISMATCH("UK.OBIE.Resource.ConsentMismatch",HttpStatus.BAD_REQUEST,OBPSD2ExceptionUtility.genericErrorMessages.get(ErrorMapKeys.FIELD),"400 BadRequest"),
	
	UK_OBIE_RESOURCE_INVALIDCONSENTSTATUS("UK.OBIE.Resource.InvalidConsentStatus",HttpStatus.BAD_REQUEST,OBPSD2ExceptionUtility.genericErrorMessages.get(ErrorMapKeys.INCOMPATIBLE),"400 BadRequest"),
	
	UK_OBIE_RESOURCE_CONSENTSTATUSNOTAUTHORISED("UK.OBIE.Resource.ConsentStatusNotAuthorised",HttpStatus.BAD_REQUEST,OBPSD2ExceptionUtility.genericErrorMessages.get(ErrorMapKeys.INCOMPATIBLE),"400 BadRequest"),	
	
	UK_OBIE_RESOURCE_REVOKEDSTATUS("UK.OBIE.Resource.RevokedStatus",HttpStatus.BAD_REQUEST,OBPSD2ExceptionUtility.genericErrorMessages.get(ErrorMapKeys.INCOMPATIBLE),"400 BadRequest"),
	
	UK_OBIE_RESOURCE_INVALIDFORMAT("UK.OBIE.Resource.InvalidFormat",HttpStatus.BAD_REQUEST,OBPSD2ExceptionUtility.genericErrorMessages.get(ErrorMapKeys.FIELD),"400 BadRequest"),
	
	UK_OBIE_UNSUPPORTED_SCHEME("UK.OBIE.Unsupported.Scheme",HttpStatus.BAD_REQUEST,OBPSD2ExceptionUtility.genericErrorMessages.get(ErrorMapKeys.FIELD),"400 BadRequest"),
	
	UK_OBIE_UNSUPPORTED_LOCALINSTRUMENT("UK.OBIE.Unsupported.LocalInstrument",HttpStatus.BAD_REQUEST,OBPSD2ExceptionUtility.genericErrorMessages.get(ErrorMapKeys.FIELD),"400 BadRequest"),
	
	UK_OBIE_RESOURCE_NOTFOUND("UK.OBIE.Resource.NotFound",HttpStatus.BAD_REQUEST,OBPSD2ExceptionUtility.genericErrorMessages.get(ErrorMapKeys.RES_NOTFOUND),"400 BadRequest"),
	
	UK_OBIE_UNSUPPORTED_ACCOUNTIDENTIFIER("UK.OBIE.Unsupported.AccountIdentifier",HttpStatus.BAD_REQUEST,OBPSD2ExceptionUtility.genericErrorMessages.get(ErrorMapKeys.FIELD),"400 BadRequest"),
	
	UK_OBIE_UNSUPPORTED_ACCOUNTSECONDARYIDENTIFIER("UK.OBIE.Unsupported.AccountSecondaryIdentifier",HttpStatus.BAD_REQUEST,OBPSD2ExceptionUtility.genericErrorMessages.get(ErrorMapKeys.FIELD),"400 BadRequest"),
	
	UK_OBIE_UNSUPPORTED_CURRENCY("UK.OBIE.Unsupported.Currency",HttpStatus.BAD_REQUEST,OBPSD2ExceptionUtility.genericErrorMessages.get(ErrorMapKeys.FIELD),"400 BadRequest"),
	
	UK_OBIE_INVALID_DATES("UK.OBIE.Field.Invalid", HttpStatus.BAD_REQUEST, OBPSD2ExceptionUtility.genericErrorMessages.get(ErrorMapKeys.REQ_PARAM_GENERIC),"400 BadRequest");
	
	private String obErrorCode;
	
	private HttpStatus httpStatus;
	
	private String generalErrorMessage;
	
	private String textualErrorCode;

	public String getTextualErrorCode() {
		return textualErrorCode;
	}
	public HttpStatus getHttpStatus() {
		return httpStatus;
	}

	public String getGeneralErrormessage() {
		return generalErrorMessage;
	}

	public String getObErrorCode() {
		return obErrorCode;
	}



	private OBErrorCodeEnum(String obErrorCode, HttpStatus httpStatus, String generalErrorMessage, String textualErrorCode) {
		this.obErrorCode = obErrorCode;
		this.httpStatus = httpStatus;
		this.generalErrorMessage = generalErrorMessage;
		this.textualErrorCode = textualErrorCode;
	}
}
