package com.capgemini.psd2.exceptions;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections.map.MultiValueMap;
import org.springframework.http.HttpStatus;

import com.capgemini.psd2.pisp.domain.OBError1;
import com.capgemini.psd2.pisp.domain.OBErrorResponse1;

public class OBPSD2Exception extends RuntimeException {

	private static final long serialVersionUID = 1L;
	private OBErrorResponse1 errorResponse;

	public OBPSD2Exception(OBErrorResponse1 obErrorResponse1, String message) {
		super(message);
		errorResponse = obErrorResponse1;
	}
	
	public OBErrorResponse1 getErrorResponse() {
		return errorResponse;
	}
	
	public static OBPSD2Exception populateOBException(OBErrorCodeEnum obErrorCodeEnum, String specificErrorMessageKey) {
		OBErrorResponse1 obErrorResponse1 = new OBErrorResponse1();
		obErrorResponse1.setCode(obErrorCodeEnum.getTextualErrorCode());
		obErrorResponse1.setMessage(obErrorCodeEnum.getGeneralErrormessage());
		List<OBError1> errorList = new ArrayList<>();
		OBError1 obError1 = new OBError1();
		obError1.setErrorCode(obErrorCodeEnum.getObErrorCode());
		if(obErrorCodeEnum.getHttpStatus().toString().equals(HttpStatus.INTERNAL_SERVER_ERROR.toString()))
			obError1.setMessage(specificErrorMessageKey);
		else
			obError1.setMessage(OBPSD2ExceptionUtility.specificErrorMessages.get(specificErrorMessageKey));
		errorList.add(obError1);
		obErrorResponse1.setErrors(errorList);
		return new OBPSD2Exception(obErrorResponse1, OBPSD2ExceptionUtility.specificErrorMessages.get(specificErrorMessageKey));
	}
	
	// used in case of sending 4** errors to client and want to send dynamic generated messages to TPP
	// eg. used in the methods like. swagger validation of request body
	public static OBPSD2Exception populateExceptionWithMessage(OBErrorCodeEnum obErrorCodeEnum, String specificErrorMessage) {
		OBErrorResponse1 obErrorResponse1 = new OBErrorResponse1();
		obErrorResponse1.setCode(obErrorCodeEnum.getTextualErrorCode());
		obErrorResponse1.setMessage(obErrorCodeEnum.getGeneralErrormessage());
		List<OBError1> errorList = new ArrayList<>();
		OBError1 obError1 = new OBError1();
		obError1.setErrorCode(obErrorCodeEnum.getObErrorCode());
		obError1.setMessage(specificErrorMessage);
		errorList.add(obError1);
		obErrorResponse1.setErrors(errorList);
		return new OBPSD2Exception(obErrorResponse1, specificErrorMessage);
	}

	// There's an option to throw multiple errors at a time which can be populated in map and thrown
	public static OBPSD2Exception populateOBException(MultiValueMap errorMap) {
		
		Set entries=errorMap.entrySet();
		Iterator itr=entries.iterator();
		OBErrorResponse1 obErrorResponse1 = new OBErrorResponse1();
		List<OBError1> errorList = new ArrayList<>();
		
		while(itr.hasNext()) {
			Map.Entry entry=(Map.Entry)itr.next();
			OBErrorCodeEnum obErrorCodeEnum=(OBErrorCodeEnum)entry.getKey();
			List<String> errorMessagesKeys=(List<String>)entry.getValue();

			obErrorResponse1.setCode(obErrorCodeEnum.getTextualErrorCode());
			obErrorResponse1.setMessage(obErrorCodeEnum.getGeneralErrormessage());
			
			for(String errorMessageKey: errorMessagesKeys) {
				OBError1 obError1 = new OBError1();
				obError1.setErrorCode(obErrorCodeEnum.getObErrorCode());
				if(obErrorCodeEnum.getHttpStatus().toString().equals(HttpStatus.INTERNAL_SERVER_ERROR.toString()))
					obError1.setMessage(errorMessageKey);
				else
					obError1.setMessage(OBPSD2ExceptionUtility.specificErrorMessages.get(errorMessageKey));
				errorList.add(obError1);
			}
		}
		
		obErrorResponse1.setErrors(errorList);
		return new OBPSD2Exception(obErrorResponse1,obErrorResponse1.getMessage());
	}
}
