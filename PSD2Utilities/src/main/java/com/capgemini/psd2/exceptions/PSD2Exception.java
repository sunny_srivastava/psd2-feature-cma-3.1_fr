/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.exceptions;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import com.capgemini.psd2.dcr.domain.RegistrationError;
import com.capgemini.psd2.pisp.domain.OBError1;
import com.capgemini.psd2.pisp.domain.OBErrorResponse1;

/**
 * The Class PSD2Exception.
 */
public class PSD2Exception extends RuntimeException {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The error info. */
	private final ErrorInfo errorInfo;

	private OBErrorResponse1 obErrorResponse1; 		//NOSONAR

	private transient RegistrationError registrationError = new RegistrationError(); //NOSONAR
	/**
	 * Instantiates a new PSD 2 exception.
	 *
	 * @param message   the message
	 * @param errorInfo the error info
	 */
	public PSD2Exception(String message, ErrorInfo errorInfo) {
		super(message);
		this.errorInfo = errorInfo;
	}

	public PSD2Exception(String message, OBErrorResponse1 obErrorResponse1) {
		super(message);
		this.errorInfo = null;
		this.obErrorResponse1 = obErrorResponse1;
	}

	public PSD2Exception(String message, RegistrationError registrationError) {
		super(message);
		this.errorInfo = null;
		this.registrationError = registrationError;
	}
	/**
	 * Gets the error info.
	 *
	 * @return the error info
	 */
	public ErrorInfo getErrorInfo() {
		return errorInfo;
	}

	public OBErrorResponse1 getOBErrorResponse1() {
		return obErrorResponse1;
	}
	
	public RegistrationError getRegistrationError() {
		return registrationError;
	}

	/**
	 * Populate PSD 2 exception.
	 *
	 * @param detailErrorMessage the detail error message
	 * @param errorCodeEnum      the error code enum
	 * @return the PSD 2 exception
	 */
	public static PSD2Exception populatePSD2Exception(String detailErrorMessage, ErrorCodeEnum errorCodeEnum) {
		ErrorInfo errorInfo = new ErrorInfo(errorCodeEnum.getErrorCode(), errorCodeEnum.getErrorMessage(),
				detailErrorMessage, errorCodeEnum.getStatusCode());
		return new PSD2Exception(detailErrorMessage, errorInfo);
	}

	public static PSD2Exception populateOAuth2Exception(OAuthErrorInfo errorInfo) {
		return new PSD2Exception(errorInfo.getErrorMessage(), errorInfo);
	}

	/**
	 * Populate PSD 2 exception.
	 *
	 * @param errorCodeEnum the error code enum
	 * @return the PSD 2 exception
	 */
	public static PSD2Exception populatePSD2Exception(ErrorCodeEnum errorCodeEnum) {
		ErrorInfo errorInfo = new ErrorInfo(errorCodeEnum.getErrorCode(), errorCodeEnum.getErrorMessage(),
				errorCodeEnum.getDetailErrorMessage(), errorCodeEnum.getStatusCode());
		return new PSD2Exception(errorCodeEnum.getDetailErrorMessage(), errorInfo);
	}

	public static PSD2Exception populatePSD2Exception(ExceptionDTO... exceptionArray) {
		List<OBError1> errorList = new ArrayList<>();
		ExceptionDTO exception = exceptionArray[0];
		OBErrorResponse1 obErrorResponse1 = new OBErrorResponse1();
		if (exceptionArray.length == 1) {
			
			OBError1 obError1 = new OBError1(exception.getObErrorCodeEnum().getObErrorCode(),
					OBPSD2ExceptionUtility.specificErrorMessages.get(exception.getErrorMessageKey()));
			if (exception.getObErrorCodeEnum() == OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR
					|| exception.getObErrorCodeEnum() == OBErrorCodeEnum.UK_OBIE_RESOURCE_INVALIDFORMAT || exception.isDynamicMessage())
				obError1.setMessage(exception.getErrorMessageKey());
			errorList.add(obError1);
		
		} else {
			errorList = Arrays.asList(exceptionArray).stream().map(obj -> {
				OBError1 obError1 = new OBError1(obj.getObErrorCodeEnum().getObErrorCode(),
						OBPSD2ExceptionUtility.specificErrorMessages.get(obj.getErrorMessageKey()));
				if (obj.getObErrorCodeEnum() == OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR
						|| obj.getObErrorCodeEnum() == OBErrorCodeEnum.UK_OBIE_RESOURCE_INVALIDFORMAT || exception.isDynamicMessage())
					obError1.setMessage(obj.getErrorMessageKey());
				return obError1;
			}).collect(Collectors.toList());
		}
		
		obErrorResponse1.setErrors(errorList);
		obErrorResponse1.setCode(exception.getObErrorCodeEnum().getTextualErrorCode());
		obErrorResponse1.setMessage(exception.getObErrorCodeEnum().getGeneralErrormessage());
		obErrorResponse1.setHttpStatus(exception.getObErrorCodeEnum().getHttpStatus());
		
		if (exception.getObErrorCodeEnum() == OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR
				|| exception.getObErrorCodeEnum() == OBErrorCodeEnum.UK_OBIE_RESOURCE_INVALIDFORMAT || exception.isDynamicMessage())
			return new PSD2Exception(exception.getErrorMessageKey(),obErrorResponse1);
		else
			return new PSD2Exception(
				OBPSD2ExceptionUtility.specificErrorMessages.get(exception.getErrorMessageKey()),
				obErrorResponse1);
	}
	
	public static PSD2Exception generateResponseForInternalErrors(Throwable e){
		OBError1 obError1=new OBError1(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR.getObErrorCode(),e.getMessage());
		OBErrorResponse1 obErrorResponse=new OBErrorResponse1();
		obErrorResponse.addErrorsItem(obError1);
		obErrorResponse.setCode(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR.getTextualErrorCode());
		obErrorResponse.setMessage(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR.getGeneralErrormessage());
		obErrorResponse.setHttpStatus(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR.getHttpStatus());
		return new PSD2Exception(e.getMessage(), obErrorResponse);
	}
}
