/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.utilities;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.capgemini.psd2.exceptions.ErrorMapKeys;
import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;


/**
 * The Class DateUtilites.
 */
public final class DateUtilites {

	private static final Logger LOG = LoggerFactory.getLogger(DateUtilites.class);
	
	private static final String ISO_DATE_TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ssZ";
	/**
	 * Instantiates a new date utilites.
	 */
	private DateUtilites() {

	}

	/**
	 * Generate current time stamp.
	 *
	 * @return the string
	 */
	public static String generateCurrentTimeStamp() {
		return Instant.now().toString();
	}

	public static Instant generateCurrentTimeStampInInstant() {
		return Instant.now();
	}

	/**
	 * Generate current time stamp.
	 *
	 * @return the string
	 */
	public static String validateAndUpdateDateTime(String dateTime) {

		String updatedDateTime;
		if (dateTime == null || dateTime.isEmpty()) {
			return null;
		} else {
			try {
				if (':' != dateTime.charAt(22)) {
					LOG.error("{\"Date validation Failed\":\"{}\",\"errorMessageKey\":\"{}\"}",
							"com.capgemini.psd2.utilities.DateUtilites.validateAndUpdateDateTime()",ErrorMapKeys.VALIDATION_ERROR);
					throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_FIELD_INVALID,ErrorMapKeys.VALIDATION_ERROR));
				}

				String dateTimeSubStr = dateTime.substring(0, 22).concat(dateTime.substring(23));
				if ('-' == dateTimeSubStr.charAt(19) && "0000".equals(dateTimeSubStr.substring(20))) {
					LOG.error("{\"Date validation Failed\":\"{}\",\"errorMessageKey\":\"{}\"}",
							"com.capgemini.psd2.utilities.DateUtilites.validateAndUpdateDateTime()",ErrorMapKeys.VALIDATION_ERROR);
					throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_FIELD_INVALID, ErrorMapKeys.VALIDATION_ERROR));
				}

				DateTimeFormatter dtf = DateTimeFormatter.ofPattern(ISO_DATE_TIME_FORMAT);
				updatedDateTime = dtf.format(dtf.parse(dateTimeSubStr));
				updatedDateTime = updatedDateTime.substring(0, 22).concat(":")
						.concat(updatedDateTime.substring(22, 24));
				OffsetDateTime fromDateTime = OffsetDateTime.parse(updatedDateTime,
						DateTimeFormatter.ISO_OFFSET_DATE_TIME);
				ZoneOffset fromOffset = fromDateTime.getOffset();
				if (fromOffset.compareTo(ZoneOffset.of("+14:00")) < 0
						|| fromOffset.compareTo(ZoneOffset.of("-12:00")) > 0) {
					throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_FIELD_INVALID, ErrorMapKeys.VALIDATION_ERROR));
				}
			} catch (StringIndexOutOfBoundsException | DateTimeParseException e) {
				LOG.error("{\"Date validation Failed\":\"{}\",\"errorMessageKey\":\"{}\",\"exception\":\"{}\"}",
						"com.capgemini.psd2.utilities.DateUtilites.validateAndUpdateDateTime()",ErrorMapKeys.VALIDATION_ERROR, e);
				throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_FIELD_INVALID, ErrorMapKeys.VALIDATION_ERROR));
			}
		}
		return updatedDateTime;
	}
	
	public static boolean isOffsetDateComparisonPassed(OffsetDateTime inputFromDateTime,OffsetDateTime inputToDateTime) {
		boolean dateTimeComparison = true;
		if(inputFromDateTime.isAfter(inputToDateTime)) {
			dateTimeComparison = false;
		}
		return dateTimeComparison;
	}

	public static boolean isDateComparisonPassed(String inputfromDateTime, String inputtoDateTime) {

		boolean dateTimeComparison = false;
		OffsetDateTime fromDateTime = null;
		OffsetDateTime toDateTime = null;

		try {
			fromDateTime = OffsetDateTime.parse(inputfromDateTime, DateTimeFormatter.ISO_OFFSET_DATE_TIME);
			toDateTime = OffsetDateTime.parse(inputtoDateTime, DateTimeFormatter.ISO_OFFSET_DATE_TIME);
		} catch (DateTimeParseException e) {
			LOG.error("{\"Date Comparison Failed\":\"{}\",\"errorMessageKey\":\"{}\",\"Exception\":\"{}\"}",
					"com.capgemini.psd2.utilities.DateUtilites.isDateComparisonPassed()",ErrorMapKeys.VALIDATION_ERROR, e);
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_FIELD_INVALID, ErrorMapKeys.VALIDATION_ERROR));
		}

		if (fromDateTime.isBefore(toDateTime) || fromDateTime.isEqual(toDateTime)) {
			dateTimeComparison = true;
		}

		return dateTimeComparison;
	}

	public static void validateStringInRFCDateFormat(String requestHeader) {
		if (requestHeader != null) {
			try {
				SimpleDateFormat sdf = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz");
				sdf.setLenient(false);
				sdf.format(sdf.parse(requestHeader)); 
			} catch (ParseException e) {
				LOG.error("{\"Date validation Failed\":\"{}\",\"errorMessageKey\":\"{}\",\"Exception\":\"{}\"}",
						"com.capgemini.psd2.utilities.DateUtilites.validateStringInRFCDateFormat()",ErrorMapKeys.CUSTOMER_LAST_LOGGEDIN, e);
				throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_HEADER_INVALID, ErrorMapKeys.CUSTOMER_LAST_LOGGEDIN));
			}
		}
	}

	public static void validateStringInISODateTimeWithoutTimeZone(String requestParam) {
		if (requestParam != null) {
			try {
				DateTimeFormatter sdf =DateTimeFormatter.ISO_LOCAL_DATE_TIME;
				sdf.parse(requestParam);
			} catch (DateTimeParseException e) {
				LOG.error("{\"Date validation Failed\":\"{}\",\"errorMessageKey\":\"{}\",\"errorCode\":\"{}\",\"errorMessage\":\"{}\",\"Exception\":\"{}\"}",
						"com.capgemini.psd2.utilities.DateUtilites.validateStringInISODateTimeWithoutTimeZone()",ErrorMapKeys.VALIDATION_ERROR, e);
				throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_FIELD_INVALID,ErrorMapKeys.VALIDATION_ERROR));
			}
		}
	}

	public static String formatMilisecondsToISODateFormat(Long l) {
		SimpleDateFormat sdf = new SimpleDateFormat(ISO_DATE_TIME_FORMAT);
		sdf.setLenient(false);
		String str = sdf.format(l);
		str = str.substring(0, 22).concat(":").concat(str.substring(22, 24));
		return str;
	}

	public static Date getDateFromISOStringFormat(String isoFormatStringWithoutColonInOffset) {
		String isoFormatStringWithoutColonInOffsetSubStr = isoFormatStringWithoutColonInOffset.substring(0, 22)
				.concat(isoFormatStringWithoutColonInOffset.substring(23, 25));
		SimpleDateFormat format = new SimpleDateFormat(ISO_DATE_TIME_FORMAT);
		Date date = null;
		try {
			date = format.parse(isoFormatStringWithoutColonInOffsetSubStr);
		} catch (ParseException e) {
			LOG.error("{\"Date validation Failed\":\"{}\",\"errorMessageKey\":\"{}\",\"Exception\":\"{}\"}",
					"com.capgemini.psd2.utilities.DateUtilites.getDateFromISOStringFormat()", ErrorMapKeys.VALIDATION_ERROR, e);
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_FIELD_INVALID, ErrorMapKeys.VALIDATION_ERROR));
		}
		return date;
	}

	public static String getCurrentDateInISOFormat() {
		String isoDateTime = DateTimeFormatter.ofPattern(ISO_DATE_TIME_FORMAT).format(OffsetDateTime.now());
		isoDateTime = isoDateTime.substring(0, 22).concat(":").concat(isoDateTime.substring(22, 24));
		return isoDateTime;
	}
	
	public static OffsetDateTime getCurrentDateInOffsetDateTime() {
		return OffsetDateTime.now();
	}
	
	public static String getCurrentDateInISOFormat(OffsetDateTime offsetDateTime) {
		String isoDateTime = DateTimeFormatter.ofPattern(ISO_DATE_TIME_FORMAT).format(offsetDateTime);
		isoDateTime = isoDateTime.substring(0, 22).concat(":").concat(isoDateTime.substring(22, 24));
		return isoDateTime;
	}
	
	public static String getCurrentDateInISOFormat(ZonedDateTime zonedDateTime) {
		String isoDateTime = DateTimeFormatter.ofPattern(ISO_DATE_TIME_FORMAT).format(zonedDateTime);
		isoDateTime = isoDateTime.substring(0, 22).concat(":").concat(isoDateTime.substring(22, 24));
		return isoDateTime;
	}
	
	public static LocalDateTime getISODateFromString(String dateInString) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(
				"[yyyy-MM-dd'T'HH:mmXXX][yyyy-MM-dd'T'HH:mm:ssXXX][yyyy-MM-dd'T'HH:mm:ss.SSSXXX][yyyy-MM-dd'T'HH:mm:ss.SXXX]");
		return LocalDateTime.parse(dateInString, formatter);
		
	}
}