package com.capgemini.psd2.utilities;

import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.capgemini.psd2.exceptions.ErrorMapKeys;
import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;

public abstract class DateUtilitiesCMA2 {

	private static final Logger LOG = LoggerFactory.getLogger(DateUtilitiesCMA2.class);
	
	/**
	 * @param dateTime
	 * @return valid dateTime with offset
	 * 
	 *         Use this method to validate the format of the dateTime passed in
	 *         the request and add an offset of +00:00 if offset is not present
	 *         or mentioned as 'Z'
	 */
	public String transformDateTimeInRequest(String dateTime) {
		if (!offsetCheck(dateTime)) {
			try {
				dateTime = addOffset(dateTime); //NOSONAR
			} catch (DateTimeParseException e) {
				OBErrorCodeEnum errorCodeEnum = OBErrorCodeEnum.UK_OBIE_FIELD_INVALID;
				LOG.error("{\"DateTimeParseException\":\"{}\",\"dateTime\":\"{}\",\"errorMessageKey\":\"{}\",\"errorCode\":\"{}\",\"errorMessage\":\"{}\",\"Exception\":\"{}\"}",
						"com.capgemini.psd2.utilities.DateUtilites.DateUtilitiesCMA2.transformDateTimeInRequest()",dateTime, ErrorMapKeys.INVALID_DATE_FORMAT, 
						errorCodeEnum.getObErrorCode(), errorCodeEnum.getGeneralErrormessage(), e);
				throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(errorCodeEnum, ErrorMapKeys.INVALID_DATE_FORMAT));
			}
		}
		return dateTime;
	}

	/**
	 * @param dateTime
	 * @return boolean indicator
	 * 
	 *         This method validates the dateTime against valid patterns
	 */
	public void validateDateTimeInRequest(String dateTime) {
		if (!offsetCheck(dateTime)) {
			try {
				dateTime = addOffset(dateTime); //NOSONAR
			} catch (DateTimeParseException e) {
				LOG.info("DateTimeParseException in validateDateTimeInRequest(): "+e);
				throw PSD2Exception.populatePSD2Exception(
						new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_FIELD_INVALID, ErrorMapKeys.INVALID_DATE_FORMAT));
			}
		}

		if (dateTime != null && !validateDateTime(dateTime)) {
			throw PSD2Exception.populatePSD2Exception(
					new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_FIELD_INVALID, ErrorMapKeys.INVALID_DATE_FORMAT));
		}
	}

	public void validateDateTimeInResponse(String dateTime) {
		if (!validateDateTime(dateTime)) {
			throw PSD2Exception.populatePSD2Exception(
					new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR, ErrorMapKeys.INVALID_DATE_FORMAT));
		}
	}

	private boolean offsetCheck(String dateTime) {
		boolean flag = false;
		try {
			OffsetDateTime.parse(dateTime);
			flag = true;
		} catch (DateTimeParseException e) {
			OBErrorCodeEnum errorCodeEnum = OBErrorCodeEnum.UK_OBIE_FIELD_INVALIDDATE;
			LOG.info("{\"Could not parse for offset\":\"{}\",\"dateTime\":\"{}\",\"errorMessageKey\":\"{}\",\"errorCode\":\"{}\",\"errorMessage\":\"{}\",\"Exception\":\"{}\"}",
					"com.capgemini.psd2.utilities.DateUtilites.DateUtilitiesCMA2.offsetCheck()",dateTime, ErrorMapKeys.INVALID_DATE_FORMAT, 
					errorCodeEnum.getObErrorCode(), errorCodeEnum.getGeneralErrormessage(), e);
			flag = false;
		}
		return flag;

	}

	private String addOffset(String dateTimeWithoutOffset) throws DateTimeParseException { //NOSONAR
		if (!NullCheckUtils.isNullOrEmpty(dateTimeWithoutOffset)) {
			StringBuilder builder = new StringBuilder();
			builder.append(dateTimeWithoutOffset);
			builder.append("+00:00");
			return builder.toString();
		} else {
			return null;
		}
	}

	private boolean validateDateTime(String dateTime) {

		boolean isValid = false;
		String dateTimeCopy = dateTime;

		if (dateTimeCopy.contains("Z"))
			dateTimeCopy = dateTimeCopy.split("Z")[0] + "+00:00";

		DateTimeFormatter[] formatters = { DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mmXXX"),
				DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ssXXX"),
				DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSXXX"),
				DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SXXX") };

		for (DateTimeFormatter formatter : formatters) { //NOSONAR
			try {
				LocalDate.parse(dateTimeCopy, formatter);
				isValid = true;
			} catch (DateTimeParseException | NullPointerException e) {
				LOG.info("Format Mismatch in validateDateTime(), continue looping through formatters array: "+e);
				continue;
			}
			break;
		}
		return isValid;
	}
}