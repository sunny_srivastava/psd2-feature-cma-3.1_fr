package com.capgemini.psd2.utilities;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Set;

public final class StringUtils {

	private StringUtils() {

	}

	public static final String getCommaSeperatedString(Set<String> set) {
		String commaSeperatedStr = null;
		for (String str : set) {
			commaSeperatedStr = !NullCheckUtils.isNullOrEmpty(commaSeperatedStr) ? commaSeperatedStr + "," + str : str;
		}
		return commaSeperatedStr;
	}

	public static final String defaultVal(Object obj) {
		String str;
		if (obj == null) {
			str = org.apache.commons.lang3.StringUtils.EMPTY;
		} else {
			str = (String) obj;
		}
		return str;
	}

	public static final String generateHashedValue(String strTobeHashed) {
		if (NullCheckUtils.isNullOrEmpty(strTobeHashed)) {
			return null;
		}
		try {
			MessageDigest md;
			md = MessageDigest.getInstance("SHA-256");
			md.update(strTobeHashed.getBytes());

			byte[] byteData = md.digest();

			StringBuffer sb = new StringBuffer(); //NOSONAR

			for (byte byteElement : byteData) {
				sb.append(Integer.toString((byteElement & 0xff) + 0x100, 16).substring(1));
			}

			return sb.toString();

		} catch (NoSuchAlgorithmException e) {
			throw new RuntimeException(e); //NOSONAR
		}

	}
}
