/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.validator.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.FieldError;
import org.springframework.validation.Validator;

import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.validator.PSD2Validator;

/**
 * The Class PSD2ValidatorImpl.
 */
@Component
public class PSD2ValidatorImpl implements PSD2Validator {

	private static final Logger LOG = LoggerFactory.getLogger(PSD2ValidatorImpl.class);
	
	/** The validator. */
	@Autowired
	@Qualifier("mvcValidator")
	private Validator validator;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.capgemini.psd2.validator.PSD2Validator#validate(java.lang.Object[])
	 */
	@Override
	public void validate(Object[] inputObjects) {
		if (inputObjects != null && inputObjects.length > 0)
			for (Object argument : inputObjects) {
				this.validate(argument);
			}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.capgemini.psd2.validator.PSD2Validator#validate(java.lang.Object)
	 */
	@Override
	public void validate(Object input) {
		StringBuilder errorString= getErrorString(input);
		if(errorString.length()>0)
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR, errorString.toString()));
	}

	@Override
	public void validateWithError(Object input, String errorCodeEnumString) {
			StringBuilder errorString=getErrorString(input);
			if(errorString.length()>0) {
			try {
				ErrorCodeEnum errorEnum = ErrorCodeEnum.valueOf(errorCodeEnumString);
				throw PSD2Exception.populatePSD2Exception(errorString.toString(), errorEnum);
			} catch (IllegalArgumentException | NullPointerException e) {
				LOG.error(PSD2Constants.LOGGER_STATEMENT + e);
				throw PSD2Exception.populatePSD2Exception(errorString.toString(), ErrorCodeEnum.TECHNICAL_ERROR);
			}
		}
	}

	@Override
	public <T extends Enum<T>> void validateEnum(Class<T> enumerator, String value) {
		boolean error = Boolean.FALSE;
		for (T c : enumerator.getEnumConstants()) {
			if (c.name().equals(value)) {
				error = true;
			}
		}
		if (!error)
			throw PSD2Exception.populatePSD2Exception(value + " Enum Not Valid", ErrorCodeEnum.VALIDATION_ERROR_OUTPUT);
	}

	@Override
	public void validateWithError(Object[] inputObjects, String errorCodeEnumString) {
		if (inputObjects != null && inputObjects.length > 0)
			for (Object argument : inputObjects) {
				this.validateWithError(argument, errorCodeEnumString);
			}
	}

	@Override
	public void validateRequestWithSwagger(Object input, String errorCodeEnumString) {
		StringBuilder errorString = getErrorString(input);
		if(errorString.length()>0) {
			try {
				OBErrorCodeEnum errorEnum = OBErrorCodeEnum.valueOf(errorCodeEnumString);
				throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(errorEnum, errorString.toString(),true));
			} catch (IllegalArgumentException | NullPointerException e) {
				LOG.error(PSD2Constants.LOGGER_STATEMENT + e);
				throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR, errorString.toString()));
			}
		}
	}
	
	private StringBuilder getErrorString(Object input) {
		Errors errors = new BeanPropertyBindingResult(input, input.getClass().getName());
		validator.validate(input, errors);
		StringBuilder errorString = new StringBuilder();
		if (errors.hasErrors()) {
			for (Object error : errors.getAllErrors()) {
				if (error instanceof FieldError) {
					FieldError fieldError = (FieldError) error;
					errorString.append(fieldError.getField());
					errorString.append(PSD2Constants.COLON);
					errorString.append(fieldError.getDefaultMessage());
					errorString.append(PSD2Constants.SEMI_COLON);
				}
			}
		}
		return errorString;
	}

}
