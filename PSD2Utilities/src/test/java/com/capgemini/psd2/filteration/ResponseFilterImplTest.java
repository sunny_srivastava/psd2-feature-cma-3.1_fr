/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************//*
package com.capgemini.psd2.filteration;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.utilities.JSONUtilities;

*//**
 * The Class ResponseFilterImplTest.
 *//*
@RunWith(SpringJUnit4ClassRunner.class)
public class ResponseFilterImplTest {

	*//** The response. *//*
	String response = "{\"type\":\"CLOSINGBOOKED\",\"accountId\":\"123456\",\"amount\":{\"amount\":\"30000\",\"currency\":\"EUR\"},\"creditDebitIndicator\":\"DEBIT\"}";

	*//** The attribute. *//*
	@Mock
	private RequestHeaderAttributes attribute;

	*//** The response filter impl. *//*
	@InjectMocks
	private ResponseFilterImpl responseFilterImpl = new ResponseFilterImpl();

	*//** The mock filter. *//*
	private Map<String, Map<String, String>> mockFilter = new HashMap<>();
	
	*//** The scope permissions. *//*
	private Map<String, String> scopePermissions = new HashMap<>();

	*//**
	 * Before.
	 *//*
	@Before
	public void before() {
		scopePermissions.put("ReadBalances", "Data[*]");
		mockFilter.put("retrieveAccountBalance", scopePermissions);
		ReflectionTestUtils.setField(responseFilterImpl, "filter", mockFilter, Map.class);
		MockitoAnnotations.initMocks(this);
	}

	*//**
	 * Filter response IF permissions N ull test.
	 *//*
	@Test
	public void filterResponseIFPermissionsNUllTest() {
		BalancesGETResponseData balancesGETResponseData = JSONUtilities.getObjectFromJSONString(response,
				BalancesGETResponseData.class);
		responseFilterImpl.filterResponse(balancesGETResponseData, "retrieveAccountBalance");
	}
	
	*//**
	 * Filter response IF permissions not N ull test.
	 *//*
	@Test
	public void filterResponseIFPermissionsNotNUllTest() {
		Set<Object> claims = new HashSet<>();
		claims.add("ReadAccountsBasic");
		claims.add("ReadAccountsDetail");
		claims.add("ReadBalances");
		attribute.setClaims(claims);
		when(attribute.getClaims()).thenReturn(claims);
		BalancesGETResponseData balancesGETResponseData = JSONUtilities.getObjectFromJSONString(response,
				BalancesGETResponseData.class);
		responseFilterImpl.filterResponse(balancesGETResponseData, "retrieveAccountBalance");
		
		mockFilter = new HashMap<>();
		responseFilterImpl.filterResponse(balancesGETResponseData, "retrieveAccountBalance");
		
		claims = new HashSet<>();
		attribute.setClaims(claims);
		when(attribute.getClaims()).thenReturn(claims);
		responseFilterImpl.filterResponse(balancesGETResponseData, "retrieveAccountBalance");
		
		
		
	}
	
	
	*//**
	 * Test filter response.
	 *//*
	@Test
	public void testFilterResponse() {
		mockFilter = null;
		ReflectionTestUtils.setField(responseFilterImpl, "filter", mockFilter, Map.class);
		BalancesGETResponseData balancesGETResponseData = JSONUtilities.getObjectFromJSONString(response,
				BalancesGETResponseData.class);
		responseFilterImpl.filterResponse(balancesGETResponseData, "retrieveAccountBalance");
		
		mockFilter = new HashMap<>();
		ReflectionTestUtils.setField(responseFilterImpl, "filter", mockFilter, Map.class);
		balancesGETResponseData = JSONUtilities.getObjectFromJSONString(response,
				BalancesGETResponseData.class);
		responseFilterImpl.filterResponse(balancesGETResponseData, "retrieveAccountBalance");
	}
	
	
	
	*//**
	 * Test filter response POJO.
	 *//*
	@Test
	public void testFilterResponsePOJO() {
				
		responseFilterImpl.setFilter(mockFilter);		
		assertTrue(mockFilter.equals(responseFilterImpl.getFilter()));
	}

}
*/