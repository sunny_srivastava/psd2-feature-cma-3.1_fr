/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.exceptions;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.mockito.InjectMocks;
import org.springframework.test.util.ReflectionTestUtils;

/**
 * The Class ErrorInfoTest.
 */
public class ErrorInfoTest {

	/**
	 * Test.
	 */
	
	@InjectMocks
	ErrorInfo errorInfo;
	
	@Test
	public void test() {
		ErrorInfo errorInfo = new ErrorInfo("error code", "error message", "detail error message", "500");
		errorInfo.setJti("jti");
		errorInfo.setOauthServerTokenRefreshURL("htps://localhost:9001/oAuthServer");
		ReflectionTestUtils.setField(errorInfo, "sendErrorPayload", false);
		ReflectionTestUtils.setField(errorInfo, "sendDetailErrorMessage", true);
		assertEquals("detail error message", errorInfo.getDetailErrorMessage());
		assertEquals(null, errorInfo.getErrorCode());
		assertEquals(null, errorInfo.getErrorMessage());
		assertEquals("jti", errorInfo.getJti());
		assertEquals("htps://localhost:9001/oAuthServer", errorInfo.getOauthServerTokenRefreshURL());
	}
	
	@Test
	public void testErrorWithArg() {
		ErrorInfo errorInfo = new ErrorInfo("error code", "error message", "500");
		assertEquals("500", errorInfo.getStatusCode());
		assertEquals("error code", errorInfo.getErrorCode());
		assertEquals("error message", errorInfo.getErrorMessage());
	}
	@Test
	public void loadMapTest(){
		Boolean sentDetailErrorMessage = true;
		Boolean sentErrorPayload = false;
		Map<String, String> map = new HashMap<>();
		errorInfo.loadMap(map , sentDetailErrorMessage, sentErrorPayload);
	}
	
	@Test
	public void setErrorMessageTest(){
		Map<String, String> map = new HashMap<>();
		map.put("statusCode", "statusCode");
		ErrorInfo errorInfo = new ErrorInfo("error code", "error message", "detail error message", "500");
		ReflectionTestUtils.setField(errorInfo, "errorMap", map);
		ReflectionTestUtils.setField(errorInfo, "statusCode", "statusCode");
		errorInfo.setErrorMessage("errorMessage");
	}
	
	@Test
	public void setStatusCodeTest(){
		ErrorInfo errorInfo = new ErrorInfo("error code", "error message", "detail error message", "500");
		errorInfo.setStatusCode("statusCode");
	}
	
	@Test
	public void getDetailErrorMessageTest(){
		ErrorInfo errorInfo = new ErrorInfo("error code", "error message", "detail error message", "500");
		ReflectionTestUtils.setField(errorInfo, "sendDetailErrorMessage",Boolean.FALSE);
		errorInfo.getDetailErrorMessage();
	}
	@Test
	public void getDetailErrorMessage1Test(){
		ErrorInfo errorInfo = new ErrorInfo("error code", "error message", "detail error message", "500");
		ReflectionTestUtils.setField(errorInfo, "sendDetailErrorMessage",Boolean.TRUE);
		errorInfo.getDetailErrorMessage();
	}
	
	@Test
	public void toStringTest(){
		ErrorInfo errorInfo = new ErrorInfo("error code", "error message", "detail error message", "500");
		errorInfo.toString();
	}
	
	@Test
	public void toMessageTest(){
		ErrorInfo errorInfo = new ErrorInfo("error code", "error message", "detail error message", "500");
		errorInfo.toMessage();
	}
	
	@Test
	public void setErrorMessageAsNullTest(){
		ErrorInfo errorInfo = new ErrorInfo("error code", "error message", "detail error message", "500");
		errorInfo.setErrorMessageAsNull();
	}
	
	@Test
	public void getActualErrorCodetest(){
		ErrorInfo errorInfo = new ErrorInfo("error code", "error message", "detail error message", "500");
		errorInfo.getActualErrorCode();
	}
	
	@Test
	public void getActualErrorMessageTest(){
		ErrorInfo errorInfo = new ErrorInfo("error code", "error message", "detail error message", "500");
		errorInfo.getActualErrorMessage();
	}
	
	@Test
	public void getActualDetailErrorMessageTest(){
		ErrorInfo errorInfo = new ErrorInfo("error code", "error message", "detail error message", "500");
		errorInfo.getActualDetailErrorMessage();
	}
	
	@Test
	public void obErrorCodeTest() {
		ErrorInfo errorInfo = new ErrorInfo();
		errorInfo.setDetailErrorMessage("error message");
		errorInfo.getDetailErrorMessage();
	}
	
	@Test
	public void constTest() {
		errorInfo = new ErrorInfo("error code", "error message", "detail error message", "test", "500");
		errorInfo.getActualDetailErrorMessage();
	}
}
