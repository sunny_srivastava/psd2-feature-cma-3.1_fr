/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.exceptions;


import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * The Class PSD2ExceptionTest.
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class PSD2ExceptionTest {
	
	@InjectMocks
	private PSD2Exception psd2Exception;

	@Before
	public void setUp() throws Exception {
		
		ExceptionDTO exdto=new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_FIELD_EXPECTED, "d");
		
		ErrorInfo errorInfo = new ErrorInfo("500");
		errorInfo.setErrorCode("110");
		errorInfo.setErrorMessage("Consent exists");
		psd2Exception =  new PSD2Exception("Consent already exists", errorInfo );
	}
	@Test
	public void testPopulatePSD2Exception() {
		assertNotNull(PSD2Exception.populatePSD2Exception(ErrorCodeEnum.CONSENT_EXISTS));
		assertNotNull(PSD2Exception.populatePSD2Exception( "Consent already exists",ErrorCodeEnum.CONSENT_EXISTS));
	}
	
	@Test
	public void testPopulateOAuth2Exception() {
		OAuthErrorInfo errorInfo = new OAuthErrorInfo();
		errorInfo.setError("error");
		errorInfo.setDetailErrorMessage("detailErrorMessage");
		errorInfo.setErrorMessage("errorMessage");
		psd2Exception.populateOAuth2Exception(errorInfo);
		
	}
	
	@Test
	public void testConstructor(){
		psd2Exception.getErrorInfo();
	}
	@Test
	public void testPopulatePSD2Exception1() {
		ExceptionDTO exdto=new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_FIELD_EXPECTED, "d");
		exdto.setErrorMessageKey("d");
		exdto.getErrorMessageKey();
		exdto.setDynamicMessage(true);
		exdto.setObErrorCodeEnum(OBErrorCodeEnum.UK_OBIE_FIELD_EXPECTED);
		ExceptionDTO exdto1=new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_FIELD_EXPECTED, "d");
		ExceptionDTO exdto2=new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR, "d",true);
		ExceptionDTO exdto3=new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_RESOURCE_INVALIDFORMAT , "d",true);
		assertNotNull(PSD2Exception.populatePSD2Exception( exdto,exdto1,exdto3));
	}

	@Test
	public void testPopulatePSD2Exception2() {
		Throwable a=new Throwable("Null pointer");
		PSD2Exception.generateResponseForInternalErrors(a);
	}
	
	@Test
	public void testPopulatePSD2Exception3() {
		psd2Exception.getOBErrorResponse1();
	}

}