package com.capgemini.psd2.exceptions;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.collections.map.MultiValueMap;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.client.RestTemplate;

import com.capgemini.psd2.pisp.domain.OBErrorResponse1;
public class OBPSD2ExceptionUtilityTest {

	@InjectMocks
	public  OBPSD2ExceptionUtility obPSD2ExceptionUtility;
	
	@Mock
	private RestTemplate restTemplate;
	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
		Map<String, String> map=new HashMap<>();
		map.put("HEADER", "header error message");
		map.put("FREQUENCY", "freq ma");
		map.put("AFTER_CUTOFF_DATE", "freq ma");
		map.put("FIELD", "freq ma");
		map.put("SIGNATURE", "freq ma");
		map.put("INCOMPATIBLE", "freq ma");
		map.put("RES_NOTFOUND", "freq ma");
		map.put("INTERNAL", "freq ma"); 
		Map<String, String> specificErrorMessageMap=new HashMap<>();
		specificErrorMessageMap.put("signature_missing", "signature header missing in request");
		specificErrorMessageMap.put("unexpected_error", "unexpected error occured");
		OBPSD2ExceptionUtility.genericErrorMessages.putAll(map);
		OBPSD2ExceptionUtility.specificErrorMessages.putAll(specificErrorMessageMap);
	}
	
	@Test
	public void test1() {
		OBErrorResponse1 errorResponse =new OBErrorResponse1();
		String errorMessage="error";
		OBPSD2Exception exception=new OBPSD2Exception(errorResponse, errorMessage);
		exception.getErrorResponse();
		OBPSD2Exception.populateExceptionWithMessage(OBErrorCodeEnum.UK_OBIE_SIGNATURE_MISSING, "signature_missing");
		OBPSD2Exception.populateOBException(OBErrorCodeEnum.UK_OBIE_SIGNATURE_MISSING, "signature_missing");
		MultiValueMap map=new MultiValueMap();
		map.put(OBErrorCodeEnum.UK_OBIE_SIGNATURE_MISSING, "signature_missing");
		OBPSD2Exception.populateOBException(map);
		
		
		// for internal errors
		
		OBPSD2Exception.populateOBException(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR, "signature_missing");
		MultiValueMap map1=new MultiValueMap();
		map1.put(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR, "signature_missing");
		OBPSD2Exception.populateOBException(map1);
	}
	
	@Test(expected = Exception.class)
	public void test2() {
		Map<String, String> map=new HashMap<>();
		map.put("HEADER", "header error message");
		ReflectionTestUtils.setField(obPSD2ExceptionUtility, "configServerUrl", "https://DIN56002130.corp.capgemini.com:8761/eureka/");
		ReflectionTestUtils.setField(obPSD2ExceptionUtility, "profile", "local");
		Map<String,Map<String,String>> errorMessageMaps = new HashMap<>(); 
		errorMessageMaps.put("test", map);
		ReflectionTestUtils.setField(obPSD2ExceptionUtility, "errorMessageMaps", errorMessageMaps);
		//ReflectionTestUtils.setField(obPSD2ExceptionUtility, "value", "dd");
		when(restTemplate.getForObject(anyString(), any())).thenReturn("test");
		obPSD2ExceptionUtility.init();
	}
}