package com.capgemini.psd2.exceptions;

import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.test.util.ReflectionTestUtils;

public class OAuthErrorInfoTest {
	
	@InjectMocks
	OAuthErrorInfo oauthErrorInfo;
	
	@Before
	public void before() {
		MockitoAnnotations.initMocks(this);
		oauthErrorInfo.setStatusCode("status");
		oauthErrorInfo.setError("error");
		oauthErrorInfo.setError_description("error_description");
	
	}
	
	@Test
	public void getError(){
		ReflectionTestUtils.setField(oauthErrorInfo, "sendErrorPayload", Boolean.TRUE);
		oauthErrorInfo.getError();
	}
	
	@Test
	public void getError_description(){
		ReflectionTestUtils.setField(oauthErrorInfo, "sendErrorPayload", true);
		oauthErrorInfo.getError_description();
	}
	@Test
	public void getError_description2(){
		ReflectionTestUtils.setField(oauthErrorInfo, "sendErrorPayload", null);
		oauthErrorInfo.getError_description();
	}
	
	@Test
	public void setError_description(){
		 Map<String, String> map = new HashMap<>();
		 map.put("statusCode", "statusCode");
		ReflectionTestUtils.setField(oauthErrorInfo, "errorMap", map);
		ReflectionTestUtils.setField(oauthErrorInfo, "statusCode", "statusCode");
		oauthErrorInfo.setError_description("error_description");
	}
	
	@Test
	public void setError_description1(){
		 Map<String, String> map = new HashMap<>();
		ReflectionTestUtils.setField(oauthErrorInfo, "errorMap", map);
		ReflectionTestUtils.setField(oauthErrorInfo, "statusCode", "statusCode");
		oauthErrorInfo.setError_description("error_description");
	}
	
	@Test
	public void statusCodeTest(){
		
		assertTrue(oauthErrorInfo.getStatusCode()== "status");
	}

	@Test
	public void getError1(){
		ReflectionTestUtils.setField(oauthErrorInfo, "sendErrorPayload", null);
		oauthErrorInfo.getError();
	}
	
	@Test
	public void setError_description2(){
		 Map<String, String> map = new HashMap<>();
		ReflectionTestUtils.setField(oauthErrorInfo, "errorMap", map);
		ReflectionTestUtils.setField(oauthErrorInfo, "statusCode", "statusCode");
		oauthErrorInfo =new OAuthErrorInfo("d");
	}
}