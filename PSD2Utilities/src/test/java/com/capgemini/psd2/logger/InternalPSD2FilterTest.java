package com.capgemini.psd2.logger;

import static org.mockito.Mockito.when;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.utilities.ValidationUtility;

@RunWith(SpringJUnit4ClassRunner.class)
public class InternalPSD2FilterTest {
	
	@Mock
	private HttpServletRequest request;
	
	@Mock
	private HttpServletResponse response;
	
	@Mock
	private FilterChain filterChain;

	@Mock
	private LoggerUtils loggerUtils;
	@Mock
	private ValidationUtility validationUtility;
	@Mock
	private RequestHeaderAttributes reqHeaderAttribute;
	
	@InjectMocks
	private InternalPSD2Filter filter = new InternalPSD2Filter();
	
	@Test(expected=Exception.class)
	public void doFilterInternalWithCorrelatioIdNotNullTest() throws ServletException, IOException{
		when(request.getHeader(PSD2Constants.CORRELATION_ID)).thenReturn("99972c6e-b292-11e8-96f8-529269fb1459");
		ReflectionTestUtils.setField(validationUtility, "uuIdRegexPattern", "[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}");
		filter.doFilterInternal(request, response, filterChain);
	}
	
	@Test(expected=Exception.class)
	public void testDoFilterInternalWithCorrelatioIdNull() throws ServletException, IOException{
		when(request.getHeader(PSD2Constants.CORRELATION_ID)).thenReturn(null);
		ReflectionTestUtils.setField(validationUtility, "uuIdRegexPattern", "[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}");
		filter.doFilterInternal(request, response, filterChain);
	}
	
	@Test
	public void doFilterInternalWithCorrelatioIdNullTest() throws ServletException, IOException{
		when(request.getHeader(PSD2Constants.CORRELATION_ID)).thenReturn("99972c6e-b292-11e8-96f8-529269fb1459");
		when(response.getWriter()).thenReturn(new PrintWriter("servletResponse"));
		filter.doFilterInternal(request, response, filterChain);
	}
	
	@Test
	public void doInternalFilterExceptionTest() throws ServletException, IOException{
		when(response.getWriter()).thenReturn(new PrintWriter("servletResponse"));
		filter.doFilterInternal(null, response, filterChain);
	}
	
	@Test
	public void shouldNotFilter() throws ServletException{
		when(request.getServletPath()).thenReturn("12345");
		filter.shouldNotFilter(request);
	}
}