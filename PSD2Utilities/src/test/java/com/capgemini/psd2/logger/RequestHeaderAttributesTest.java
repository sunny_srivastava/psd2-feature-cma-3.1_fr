/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.logger;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.token.Token;
import com.capgemini.psd2.token.TppInformationTokenData;

/**
 * The Class RequestHeaderAttributesTest.
 */
public class RequestHeaderAttributesTest {

	/** The request correlation. */
	private RequestHeaderAttributes requestCorrelation = null;

	/**
	 * Sets the up.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void setUp() throws Exception {
		requestCorrelation = new RequestHeaderAttributes();
		requestCorrelation.setCorrelationId("ba4f73f8-9a60-425b-aed8-2a7ef2509fea");
	}

	/**
	 * Test to string.
	 */
	@Test
	public void testToString() {
		assertNotNull(requestCorrelation.toString());
	}

	/**
	 * Test equals.
	 */
	@Test
	public void testEquals() {
		assertTrue(requestCorrelation.equals(requestCorrelation));
	}

	/**
	 * Test request header attribute.
	 */
	@Test
	public void testRequestHeaderAttribute() {
		requestCorrelation.setCorrelationId("ba4f73f8-9a60-425b-aed8-2a7ef2509fea");
		Token token = new Token();
		token.setClient_id("tppCID");
		TppInformationTokenData TppInformationTokenData = new TppInformationTokenData();
		TppInformationTokenData.setTppLegalEntityName("entity");
		TppInformationTokenData.setTppRegisteredId("tpp123");
		Set<String> tppRoles =new HashSet<>();
		tppRoles.add("AISP");
		tppRoles.add("PISP");
		tppRoles.add("CISP");
		TppInformationTokenData.setTppRoles(tppRoles);
		token.setTppInformation(TppInformationTokenData);
		token.setUser_name("1234");
		requestCorrelation.setAttributes(token);
		requestCorrelation.setToken(token);
		Set<Object> claims = new HashSet<>();
		claims.add(new Object());
		requestCorrelation.setClaims(claims);
		requestCorrelation.setCustomerIPAddress("10.1.1.23");
		requestCorrelation.setCustomerLastLoggedTime("10-10-2017T10:10");
		requestCorrelation.setFinancialId("finance1234");
		
		requestCorrelation.setSelfUrl("https://localhost:8080/accounts");
		
		requestCorrelation.setIdempotencyKey("idempotencyKey");
		requestCorrelation.setIntentId("intendId");
		requestCorrelation.setChannelId("channelId");
		requestCorrelation.setScopes("scopes");
		requestCorrelation.setMethodType("methodType");
		requestCorrelation.setTppCID("tppCID");
		requestCorrelation.setPsuId("psuId");
        requestCorrelation.setJwksUrlFiles("jwksUrlFiles");
        requestCorrelation.setX_ssl_client_ncaid("X_ssl_client_ncaid");
        requestCorrelation.setTenantId("BOIUK");
        requestCorrelation.setX_ssl_client_cert("X_ssl_client_cert");
        requestCorrelation.setJwsHeader("JwsHeader");
        requestCorrelation.setX_ssl_client_dn("X_ssl_client_dn");
        requestCorrelation.setX_ssl_client_cn("X_ssl_client_cn");
        requestCorrelation.setX_ssl_client_ou("setX_ssl_client_ou");
        requestCorrelation.setTppLegalEntityName("entity");
        
        
		assertEquals("https://localhost:8080/accounts", requestCorrelation.getSelfUrl());
		assertEquals("ba4f73f8-9a60-425b-aed8-2a7ef2509fea", requestCorrelation.getCorrelationId());
		assertEquals("tppCID", requestCorrelation.getTppCID());
		assertEquals("entity", requestCorrelation.getTppLegalEntityName());
		assertEquals(claims, requestCorrelation.getClaims());
		assertEquals("10.1.1.23", requestCorrelation.getCustomerIPAddress());
		assertEquals("finance1234", requestCorrelation.getFinancialId());
		assertEquals(token, requestCorrelation.getToken());
		assertEquals("10-10-2017T10:10", requestCorrelation.getCustomerLastLoggedTime());
		assertEquals("idempotencyKey", requestCorrelation.getIdempotencyKey());
		assertEquals("intendId", requestCorrelation.getIntentId());
		assertEquals("channelId", requestCorrelation.getChannelId());
		assertEquals("scopes", requestCorrelation.getScopes());
		assertEquals("methodType", requestCorrelation.getMethodType());
		assertEquals("tppCID", requestCorrelation.getTppCID());
		assertEquals("psuId", requestCorrelation.getPsuId());
		assertEquals("jwksUrlFiles", requestCorrelation.getJwksUrlFiles());
		assertEquals("X_ssl_client_ncaid", requestCorrelation.getX_ssl_client_ncaid());
		assertEquals("BOIUK", requestCorrelation.getTenantId());
		assertEquals("X_ssl_client_cert", requestCorrelation.getX_ssl_client_cert());
		assertEquals("JwsHeader", requestCorrelation.getJwsHeader());
		assertEquals("X_ssl_client_dn", requestCorrelation.getX_ssl_client_dn());
		assertEquals("X_ssl_client_cn", requestCorrelation.getX_ssl_client_cn());
		assertEquals("setX_ssl_client_ou", requestCorrelation.getX_ssl_client_ou());
		assertEquals("entity", requestCorrelation.getTppLegalEntityName());
		assertTrue(null != requestCorrelation.getRequestId());

	}

	@Test(expected=PSD2Exception.class)
	public void testRequestHeaderAttributeSetAttrbutePSD2Exception() {
		requestCorrelation.setCorrelationId("ba4f73f8-9a60-425b-aed8-2a7ef2509fea");
		Token token = new Token();
		token.setUser_name("1234");
		requestCorrelation.setAttributes(token);
	}
	
	@Test
	public void setAttributesTest(){
		Token token = new Token();
		token.setClient_id("tppCID");
		token.setUser_name("user_name");
		Map<String, Set<Object>> claims = new HashMap<>();
		Set<Object> value = new HashSet<>();
		value.add("AISP");
		claims.put("AISP", value);
		token.setClaims(claims);
		Set<String> scope = new HashSet<>();
		scope.add("AISP");
		token.setScope(scope);
		TppInformationTokenData TppInformationTokenData = new TppInformationTokenData();
		TppInformationTokenData.setTppLegalEntityName("entity");
		TppInformationTokenData.setTppRegisteredId("tpp123");
		token.setTppInformation(TppInformationTokenData);
		requestCorrelation.setAttributes(token);
	}
	
	@Test
	public void setAttributes3Test(){
		Token token = new Token();
		token.setClient_id("tppCID");
		token.setUser_name("user_name");
		Map<String, Set<Object>> claims = new HashMap<>();
		Set<Object> value = new HashSet<>();
		;
		claims.put("AISP", value);
		token.setClaims(claims);
		Set<String> scope = new HashSet<>();
		scope.add("AISP");
		token.setScope(scope);
		TppInformationTokenData TppInformationTokenData = new TppInformationTokenData();
		TppInformationTokenData.setTppLegalEntityName("entity");
		TppInformationTokenData.setTppRegisteredId("tpp123");
		token.setTppInformation(TppInformationTokenData);
		requestCorrelation.setAttributes(token);
	}
	
	@Test
	public void setAttributes2Test(){
		Token token = new Token();
		token.setClient_id("tppCID");
		token.setUser_name("user_name");
		Map<String, Set<Object>> claims = new HashMap<>();
		Set<Object> value = new HashSet<>();
		value.add("AISP");
		claims.put("AISP", value);
		token.setClaims(claims);
		Set<String> scope = new HashSet<>();
		
		token.setScope(scope);
		TppInformationTokenData TppInformationTokenData = new TppInformationTokenData();
		TppInformationTokenData.setTppLegalEntityName("entity");
		TppInformationTokenData.setTppRegisteredId("tpp123");
		token.setTppInformation(TppInformationTokenData);
		requestCorrelation.setAttributes(token);
	}
	
	@Test
	public void setAttributesTest1(){
		Token token = new Token();
		token.setClient_id("tppCID");
		token.setUser_name("user_name");
		Map<String, Set<Object>> claims = new HashMap<>();
		Set<Object> value = new HashSet<>();
		value.add("AISP");
		claims.put("AISP", value);
		token.setClaims(claims);
		token.setScope(null);
		TppInformationTokenData TppInformationTokenData = new TppInformationTokenData();
		TppInformationTokenData.setTppLegalEntityName("entity");
		TppInformationTokenData.setTppRegisteredId("tpp123");
		token.setTppInformation(TppInformationTokenData);
		requestCorrelation.setAttributes(token);
	}
	/**
	 * Tear down.
	 *
	 * @throws Exception the exception
	 */
	@After
	public void tearDown() throws Exception {
		requestCorrelation = null;
	}
	
	
}
