package com.capgemini.psd2.validator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Validator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.aisp.domain.OBAccount6;
import com.capgemini.psd2.aisp.domain.OBAccount6Account;
import com.capgemini.psd2.aisp.domain.OBExternalAccountType1Code;
import com.capgemini.psd2.aisp.domain.OBReadAccount6;
import com.capgemini.psd2.aisp.domain.OBReadAccount6Data;
import com.capgemini.psd2.config.PSD2UtilityTestConfig;
import com.capgemini.psd2.exceptions.OBPSD2ExceptionUtility;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.validator.impl.PSD2ValidatorImpl;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes=PSD2UtilityTestConfig.class)
public class PSD2ValidatorImplTest {
	
	@InjectMocks
	private PSD2ValidatorImpl validatorImpl;

	
	@Autowired
	private Validator mvcValidator;


	@Before
	public void before() {
		String name = "validator";
		MockitoAnnotations.initMocks(this);
		ReflectionTestUtils.setField(validatorImpl, name, mvcValidator);
		
		Map<String, String> map=new HashMap<>();
		map.put("HEADER", "header error message");
		map.put("FREQUENCY", "freq ma");
		map.put("AFTER_CUTOFF_DATE", "freq ma");
		map.put("FIELD", "freq ma");
		map.put("SIGNATURE", "freq ma");
		map.put("INCOMPATIBLE", "freq ma");
		map.put("RES_NOTFOUND", "freq ma");
		map.put("INTERNAL", "freq ma"); 
 
		Map<String, String> specificErrorMessageMap=new HashMap<>();
		specificErrorMessageMap.put("signature_missing", "signature header missing in request");
		specificErrorMessageMap.put("unexpected_error", "unexpected error occured");
		
		OBPSD2ExceptionUtility.genericErrorMessages.putAll(map);
		OBPSD2ExceptionUtility.specificErrorMessages.putAll(specificErrorMessageMap);
	}
	
	@Test
	public void testValidate() {
		Object[] inputObjects=new Object[1];
		OBAccount6 oBAccount2=new OBAccount6();
		oBAccount2.setAccountId("269c3ff5-d7f8-419b-a3b9-7136c5b4611a");
		List<OBAccount6Account> data2accountlist=new ArrayList<>();
		OBAccount6Account data2account=new OBAccount6Account();
		data2account.setIdentification("12345678");
		data2account.setName("avinash");
		data2account.setSchemeName("UK.OBIE.SortCodeAccountNumber");
		data2account.setSecondaryIdentification("002");
		data2accountlist.add(data2account);
		oBAccount2.setAccount(data2accountlist);
		oBAccount2.setAccountType(OBExternalAccountType1Code.BUSINESS);
		inputObjects[0]=oBAccount2;
		validatorImpl.validate(inputObjects);
	}
	
	@Test(expected=PSD2Exception.class)
	public void testValidat1() {
		Object[] inputObjects=new Object[1];
		
		OBReadAccount6 obReadAccount2 = new OBReadAccount6();
		OBReadAccount6Data obReadAccount2Data = new OBReadAccount6Data();
		List<OBAccount6> obAccount2List = new ArrayList<>();
		OBAccount6 oBAccount2=new OBAccount6();
		//oBAccount2.setAccountId("269c3ff5-d7f8-419b-a3b9-7136c5b4611a");
		List<OBAccount6Account> data2accountlist=new ArrayList<>();
		OBAccount6Account data2account=new OBAccount6Account();
		data2account.setIdentification("12345678");
		data2account.setName("avinash");
		data2account.setSchemeName("UK.OBIE.SortCodeAccountNumber");
		data2account.setSecondaryIdentification("002");
		data2accountlist.add(data2account);
		oBAccount2.setAccount(data2accountlist);
		obAccount2List.add(oBAccount2);
		obReadAccount2Data.setAccount(obAccount2List);
		
		obReadAccount2.setData(obReadAccount2Data);
		inputObjects[0]=obReadAccount2;
		validatorImpl.validateWithError(oBAccount2, "Validation error found with the provided ouput");
	}
	
	@Test(expected=PSD2Exception.class)
	public void testValidat2() {
		Object[] inputObjects=new Object[1];
		
		OBReadAccount6 obReadAccount2 = new OBReadAccount6();
		OBReadAccount6Data obReadAccount2Data = new OBReadAccount6Data();
		List<OBAccount6> obAccount2List = new ArrayList<>();
		OBAccount6 oBAccount2=new OBAccount6();
		//oBAccount2.setAccountId("269c3ff5-d7f8-419b-a3b9-7136c5b4611a");
		List<OBAccount6Account> data2accountlist=new ArrayList<>();
		OBAccount6Account data2account=new OBAccount6Account();
		data2account.setIdentification("12345678");
		data2account.setName("avinash");
		data2account.setSchemeName("SortCodeAccountNumber");
		data2account.setSecondaryIdentification("002");
		data2accountlist.add(data2account);
		oBAccount2.setAccount(data2accountlist);
		obAccount2List.add(oBAccount2);
		obReadAccount2Data.setAccount(obAccount2List);
		
		obReadAccount2.setData(obReadAccount2Data);
		inputObjects[0]=obReadAccount2;
		validatorImpl.validateWithError(oBAccount2, "VALIDATION_ERROR_OUTPUT");
	}
	
	@Test
	public void testEnum() {
		validatorImpl.validateEnum(Tes7.class, "FIRST");
	}
	
	@Test(expected=PSD2Exception.class)
	public void testEnum1() {
		validatorImpl.validateEnum(Tes7.class, "THIRD");
	}
	
	@Test
	public void testValidate3() {
		Object[] inputObjects=new Object[1];
		OBAccount6 oBAccount2=new OBAccount6();
		oBAccount2.setAccountId("269c3ff5-d7f8-419b-a3b9-7136c5b4611a");
		List<OBAccount6Account> data2accountlist=new ArrayList<>();
		OBAccount6Account data2account=new OBAccount6Account();
		data2account.setIdentification("12345678");
		data2account.setName("avinash");
		data2account.setSchemeName("UK.OBIE.SortCodeAccountNumber");
		data2account.setSecondaryIdentification("002");
		data2accountlist.add(data2account);
		oBAccount2.setAccount(data2accountlist);
		oBAccount2.setAccountType(OBExternalAccountType1Code.BUSINESS);
		inputObjects[0]=oBAccount2;
		validatorImpl.validateWithError(inputObjects, "VALIDATION_ERROR_OUTPUT");
	}
	
	@Test
	public void testValidate4() {
		
		validatorImpl.validateWithError(null, "VALIDATION_ERROR_OUTPUT");
	}
	
	@Test
	public void validate(){
		Object[] inputObjects = {"id","value"} ;
		validatorImpl.validate(inputObjects);
	}
	
	@Test
	public void validateWithErrorTest(){
		Object[] inputObjects = {"id","value"} ;
		validatorImpl.validateWithError(inputObjects, "errorCodeEnumString");
	}
	
	@Test(expected=PSD2Exception.class)
	public void validateRequestWithSwaggerNonExistentErrorTest() {
		
		OBReadAccount6 obReadAccount2 = new OBReadAccount6();
		OBReadAccount6Data obReadAccount2Data = new OBReadAccount6Data();
		List<OBAccount6> obAccount2List = new ArrayList<>();
		OBAccount6 oBAccount2=new OBAccount6();
		//oBAccount2.setAccountId("269c3ff5-d7f8-419b-a3b9-7136c5b4611a");
		List<OBAccount6Account> data2accountlist=new ArrayList<>();
		OBAccount6Account data2account=new OBAccount6Account();
		data2account.setIdentification("12345678");
		data2account.setName("avinash");
		data2account.setSchemeName("SortCodeAccountNumber");
		data2account.setSecondaryIdentification("002");
		data2accountlist.add(data2account);
		oBAccount2.setAccount(data2accountlist);
		obAccount2List.add(oBAccount2);
		obReadAccount2Data.setAccount(obAccount2List);
		
		obReadAccount2.setData(obReadAccount2Data);
		validatorImpl.validateRequestWithSwagger(oBAccount2, "VALIDATION_ERROR_OUTPUT");
	}
	
	@Test(expected=PSD2Exception.class)
	public void validateRequestWithSwaggerExistingErrorTest() {
		
		OBReadAccount6 obReadAccount2 = new OBReadAccount6();
		OBReadAccount6Data obReadAccount2Data = new OBReadAccount6Data();
		List<OBAccount6> obAccount2List = new ArrayList<>();
		OBAccount6 oBAccount2=new OBAccount6();
		//oBAccount2.setAccountId("269c3ff5-d7f8-419b-a3b9-7136c5b4611a");
		List<OBAccount6Account> data2accountlist=new ArrayList<>();
		OBAccount6Account data2account=new OBAccount6Account();
		data2account.setIdentification("12345678");
		data2account.setName("avinash");
		data2account.setSchemeName("SortCodeAccountNumber");
		data2account.setSecondaryIdentification("002");
		data2accountlist.add(data2account);
		oBAccount2.setAccount(data2accountlist);
		obAccount2List.add(oBAccount2);
		obReadAccount2Data.setAccount(obAccount2List);
		
		obReadAccount2.setData(obReadAccount2Data);
		validatorImpl.validateRequestWithSwagger(oBAccount2, "UK_OBIE_UNEXPECTEDERROR");
	}

	
	enum Tes7{
		FIRST,SECOND;
	}
	
}
