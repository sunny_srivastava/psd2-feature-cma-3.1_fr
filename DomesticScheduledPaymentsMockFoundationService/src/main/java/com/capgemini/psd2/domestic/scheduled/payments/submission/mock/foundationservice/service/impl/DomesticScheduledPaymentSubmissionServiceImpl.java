/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.domestic.scheduled.payments.submission.mock.foundationservice.service.impl;

import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.capgemini.psd2.domestic.scheduled.payments.submission.mock.foundationservice.domain.PaymentInstructionProposal;
import com.capgemini.psd2.domestic.scheduled.payments.submission.mock.foundationservice.domain.PaymentInstructionStatusCode2;
import com.capgemini.psd2.domestic.scheduled.payments.submission.mock.foundationservice.domain.ScheduledPaymentInstructionComposite;
import com.capgemini.psd2.domestic.scheduled.payments.submission.mock.foundationservice.domain.ScheduledPaymentInstructionProposal;
import com.capgemini.psd2.domestic.scheduled.payments.submission.mock.foundationservice.domain.ScheduledPaymentInstructionProposal2;
import com.capgemini.psd2.domestic.scheduled.payments.submission.mock.foundationservice.repository.DomesticScheduledPaymentSubmissionRepository;
import com.capgemini.psd2.domestic.scheduled.payments.submission.mock.foundationservice.service.DomesticScheduledPaymentSubmissionService;
import com.capgemini.psd2.foundationservice.datetime.utility.TimeZoneDateTimeAdapter;
import com.capgemini.psd2.foundationservice.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.foundationservice.exceptions.MockFoundationServiceException;
import com.capgemini.psd2.foundationservice.pisp3.domain.PaymentInstruction1;




// TODO: Auto-generated Javadoc
/**
 * The Class DomesticPaymentConsentsServiceImpl.
 */
@Service
public class DomesticScheduledPaymentSubmissionServiceImpl implements DomesticScheduledPaymentSubmissionService {

	/** The repository. */
	@Autowired
	private DomesticScheduledPaymentSubmissionRepository repository;
	@Autowired
	private TimeZoneDateTimeAdapter timeZoneDateTimeAdapter;

	@Override
	public ScheduledPaymentInstructionComposite retrieveAccountInformation(String paymentInstructionlId) throws Exception {

		ScheduledPaymentInstructionComposite response1 = repository.findByPaymentInstructionPaymentInstructionNumber(paymentInstructionlId);
		if (null == response1) {
			throw MockFoundationServiceException.populateMockFoundationServiceException(ErrorCodeEnum.SERVER_RESOURCE_NOT_FOUND_PPA_DPING);
		}

		return response1;
	}

	@Override
	public ScheduledPaymentInstructionComposite createDomesticScheduledPaymentSubmissionsResource(
			ScheduledPaymentInstructionComposite paymentInstProposalReq) {
		String consentID = null;
		String submissionID=null;
		if (null == paymentInstProposalReq) {

			throw MockFoundationServiceException.populateMockFoundationServiceException(ErrorCodeEnum.SERVER_RESOURCE_NOT_FOUND_PPA_DPING);
		}
		consentID = UUID.randomUUID().toString();
		submissionID = UUID.randomUUID().toString();
		ScheduledPaymentInstructionProposal2 instruction5=new ScheduledPaymentInstructionProposal2();
		Date date = Calendar.getInstance().getTime();  
		String strDate = timeZoneDateTimeAdapter.parseDateTimeCMA(date);
		instruction5.setInstructionIssueDate(strDate);
		instruction5.setInstructionStatusUpdateDateTime(strDate);		
		instruction5.setPaymentInstructionNumber(submissionID);
		String[] check=paymentInstProposalReq.getPaymentInstructionProposal().getInstructionReference().split(":");
		if (check.length > 1 && check[1].equalsIgnoreCase("REJECTED"))
		{
			instruction5.setPaymentInstructionStatusCode(PaymentInstructionStatusCode2.InitiationFailed);
		}
		else
		{
			instruction5.setPaymentInstructionStatusCode(PaymentInstructionStatusCode2.InitiationCompleted);
		}
		if(paymentInstProposalReq.getPaymentInstructionProposal().getInstructionEndToEndReference().equalsIgnoreCase("NOTPROVIDED"))
		{
			paymentInstProposalReq.getPaymentInstructionProposal().setInstructionEndToEndReference(null);
		}
		paymentInstProposalReq.setPaymentInstruction(instruction5);
		
		repository.save(paymentInstProposalReq);
		return paymentInstProposalReq;

	}

}
