package com.capgemini.psd2.pisp.payment.setup.mongo.db.test.adapter.impl;

import com.capgemini.psd2.pisp.domain.CustomISPaymentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomISPaymentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.OBChargeBearerType1Code;
import com.capgemini.psd2.pisp.domain.OBDomestic1InstructedAmount;
import com.capgemini.psd2.pisp.domain.OBInternationalScheduled1;
import com.capgemini.psd2.pisp.domain.OBWriteDataInternationalScheduled1;
import com.capgemini.psd2.pisp.domain.OBWriteDataInternationalScheduledResponse1;

public class ISPaymentsMongoDbAdapterMockData {

	public static CustomISPaymentsPOSTRequest request;

	public static CustomISPaymentsPOSTResponse response;

	public static CustomISPaymentsPOSTRequest getRequest() {
		request = new CustomISPaymentsPOSTRequest();
		request.setData(new OBWriteDataInternationalScheduled1());
		request.getData().setInitiation(new OBInternationalScheduled1());
		request.getData().getInitiation().setInstructedAmount(new OBDomestic1InstructedAmount());
		request.getData().getInitiation().getInstructedAmount().setAmount("235.63");
		request.getData().getInitiation().getInstructedAmount().setCurrency("EUR");
		request.getData().getInitiation().setChargeBearer(OBChargeBearerType1Code.FOLLOWINGSERVICELEVEL);

		return request;
	}

	public static CustomISPaymentsPOSTResponse getResponse() {
		response = new CustomISPaymentsPOSTResponse();
		response.setData(new OBWriteDataInternationalScheduledResponse1());
		response.getData().setInitiation(new OBInternationalScheduled1());
		response.getData().getInitiation().setInstructedAmount(new OBDomestic1InstructedAmount());
		response.getData().getInitiation().getInstructedAmount().setAmount("523.36");

		return response;
	}
}
