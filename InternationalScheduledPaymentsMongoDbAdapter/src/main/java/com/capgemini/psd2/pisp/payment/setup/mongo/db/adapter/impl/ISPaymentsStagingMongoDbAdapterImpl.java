package com.capgemini.psd2.pisp.payment.setup.mongo.db.adapter.impl;

import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Conditional;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.conditions.MongoDbMockCondition;
import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.InternalServerErrorMessage;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.adapter.InternationalScheduledPaymentsAdapter;
import com.capgemini.psd2.pisp.domain.CustomISPaymentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomISPaymentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.GenericPaymentsResponse;
import com.capgemini.psd2.pisp.domain.OBCharge1;
import com.capgemini.psd2.pisp.domain.OBChargeBearerType1Code;
import com.capgemini.psd2.pisp.domain.OBExchangeRate2;
import com.capgemini.psd2.pisp.domain.OBExternalStatus1Code;
import com.capgemini.psd2.pisp.domain.OBExternalStatus2Code;
import com.capgemini.psd2.pisp.domain.OBMultiAuthorisation1;
import com.capgemini.psd2.pisp.enums.ProcessExecutionStatusEnum;
import com.capgemini.psd2.pisp.payment.setup.mongo.db.adapter.repository.ISPaymentsFoundationRepository;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.pisp.status.PaymentConstants;
import com.capgemini.psd2.pisp.utilities.PispUtilities;
import com.capgemini.psd2.utilities.JSONUtilities;
import com.capgemini.psd2.utilities.SandboxValidationUtility;

@Conditional(MongoDbMockCondition.class)
@Component("iScheduledPaymentsMongoDbAdapter")
public class ISPaymentsStagingMongoDbAdapterImpl implements InternationalScheduledPaymentsAdapter {

	private static final Logger LOG = LoggerFactory.getLogger(ISPaymentsStagingMongoDbAdapterImpl.class);
	
	@Autowired
	private ISPaymentsFoundationRepository isPaymentsFoundationRepository;

	@Autowired
	private SandboxValidationUtility utility;

	@Autowired
	private RequestHeaderAttributes reqAttributes;

	@Value("${app.sandboxValidationPolicies.contractIdentificationPattern:^Test[a-zA-Z0-9]*}")
	private String contractIdentificationPattern;

	@Override
	public GenericPaymentsResponse processISPayments(CustomISPaymentsPOSTRequest customRequest,
			Map<String, OBExternalStatus1Code> paymentStatusMap, Map<String, String> params) {

		GenericPaymentsResponse genericPaymentsResponse = populateMockedResponse(customRequest);
		populateAndSaveDbResource(customRequest, genericPaymentsResponse, paymentStatusMap);

		return genericPaymentsResponse;
	}

	private GenericPaymentsResponse populateMockedResponse(CustomISPaymentsPOSTRequest customRequest) {
		GenericPaymentsResponse genericPaymentsResponse = new GenericPaymentsResponse();
		String currency = customRequest.getData().getInitiation().getInstructedAmount().getCurrency();
		String initiationAmount = customRequest.getData().getInitiation().getInstructedAmount().getAmount();
		OBChargeBearerType1Code reqChargeBearer = null;

		String submissionId = null;
		String expectedExecutionDateTime = utility.getMockedExpectedExecDtTm();
		String expectedSettlementDateTime = utility.getMockedExpectedSettlementDtTm();
		List<OBCharge1> charges = utility.getMockedOBChargeList(currency, initiationAmount, reqChargeBearer);
		ProcessExecutionStatusEnum processStatusEnum = utility.getMockedProcessExecStatus(currency, initiationAmount);
		OBMultiAuthorisation1 multiAuthorisation = utility.getMockedMultiAuthBlock(currency, initiationAmount);
		OBExchangeRate2 exchangeRateInformation = utility.getMockedExchangeRateInformation(
				customRequest.getData().getInitiation().getExchangeRateInformation(), initiationAmount);

		/*
		 * Failure mocked condition for sandbox - Invalid Contract Identification and
		 * Exchange Rate
		 */
		if (null != customRequest.getData().getInitiation().getExchangeRateInformation()
				&& null != customRequest.getData().getInitiation().getExchangeRateInformation()
						.getContractIdentification()
				&& isRejectionContractIdentification(customRequest.getData().getInitiation()
						.getExchangeRateInformation().getContractIdentification())) {
			processStatusEnum = ProcessExecutionStatusEnum.FAIL;
		}
		if (processStatusEnum == ProcessExecutionStatusEnum.PASS
				|| processStatusEnum == ProcessExecutionStatusEnum.FAIL
				|| processStatusEnum == ProcessExecutionStatusEnum.INCOMPLETE)
			submissionId = UUID.randomUUID().toString();

		genericPaymentsResponse.setSubmissionId(submissionId);
		genericPaymentsResponse.setCharges(charges);
		genericPaymentsResponse.setExpectedExecutionDateTime(expectedExecutionDateTime);
		genericPaymentsResponse.setExpectedSettlementDateTime(expectedSettlementDateTime);
		genericPaymentsResponse.setMultiAuthorisation(multiAuthorisation);
		genericPaymentsResponse.setProcessExecutionStatusEnum(processStatusEnum);
		genericPaymentsResponse.setExchangeRateInformation(exchangeRateInformation);
		return genericPaymentsResponse;
	}

	private void populateAndSaveDbResource(CustomISPaymentsPOSTRequest customRequest,
			GenericPaymentsResponse genericPaymentsResponse, Map<String, OBExternalStatus1Code> paymentStatusMap) {

		String paymentRequest = JSONUtilities.getJSONOutPutFromObject(customRequest);
		CustomISPaymentsPOSTResponse fsDbResource = JSONUtilities.getObjectFromJSONString(
				PispUtilities.getObjectMapper(), paymentRequest, CustomISPaymentsPOSTResponse.class);

		fsDbResource.getData().setInternationalScheduledPaymentId(genericPaymentsResponse.getSubmissionId());
		fsDbResource.getData().setCreationDateTime(customRequest.getCreatedOn());
		fsDbResource.getData().setCharges(genericPaymentsResponse.getCharges());
		fsDbResource.getData().setExpectedExecutionDateTime(genericPaymentsResponse.getExpectedExecutionDateTime());
		fsDbResource.getData().setExpectedSettlementDateTime(genericPaymentsResponse.getExpectedSettlementDateTime());
		fsDbResource.getData().setMultiAuthorisation(genericPaymentsResponse.getMultiAuthorisation());
		fsDbResource.getData().setExchangeRateInformation(genericPaymentsResponse.getExchangeRateInformation());

		if (genericPaymentsResponse.getSubmissionId() != null) {
			OBExternalStatus1Code status = calculateCMAStatus(genericPaymentsResponse.getProcessExecutionStatusEnum(),
					genericPaymentsResponse.getMultiAuthorisation(), paymentStatusMap);
			fsDbResource.getData().setStatus(status);
			fsDbResource.getData().setStatusUpdateDateTime(customRequest.getCreatedOn());
		}

		try {
			isPaymentsFoundationRepository.save(fsDbResource);
		} catch (DataAccessResourceFailureException exception) {
			LOG.info("DataAccessResourceFailureException in populateAndSaveDbResource:"+exception);
			throw PSD2Exception.populatePSD2Exception(
					new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR, exception.getMessage()));
		}
	}

	@Override
	public CustomISPaymentsPOSTResponse retrieveStagedISPaymentsResponse(
			CustomPaymentStageIdentifiers customPaymentStageIdentifiers, Map<String, String> params) {
		CustomISPaymentsPOSTResponse response = new CustomISPaymentsPOSTResponse();
		CustomISPaymentsPOSTResponse x = isPaymentsFoundationRepository
				.findOneByDataInternationalScheduledPaymentId(customPaymentStageIdentifiers.getPaymentSubmissionId());
		response.setData(x.getData());

		String initiationAmount = null;
		if (response != null && response.getData() != null && response.getData().getInitiation() != null && response.getData().getInitiation().getInstructedAmount() != null) { //NOSONAR
			initiationAmount = response.getData().getInitiation().getInstructedAmount().getAmount();
		}

		if ("GET".equals(reqAttributes.getMethodType())
				&& (utility.isValidAmount(initiationAmount, PSD2Constants.SANDBOX_PISP_GET_MOCKING))) {

			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
					InternalServerErrorMessage.SANDBOX_MOCKED_ERROR));

		}
		return response;
	}

	private OBExternalStatus1Code calculateCMAStatus(ProcessExecutionStatusEnum processExecutionStatusEnum,
			OBMultiAuthorisation1 multiAuthorisation, Map<String, OBExternalStatus1Code> paymentStatusMap) {

		OBExternalStatus1Code cmaStatus = paymentStatusMap.get(PaymentConstants.INITIAL);

		if (processExecutionStatusEnum == ProcessExecutionStatusEnum.FAIL)
			cmaStatus = paymentStatusMap.get(PaymentConstants.FAIL);
		else if (processExecutionStatusEnum == ProcessExecutionStatusEnum.PASS) {
			if (multiAuthorisation == null || multiAuthorisation.getStatus() == OBExternalStatus2Code.AUTHORISED)
				cmaStatus = paymentStatusMap.get(PaymentConstants.PASS_M_AUTH);
			else if (multiAuthorisation.getStatus() == OBExternalStatus2Code.AWAITINGFURTHERAUTHORISATION)
				cmaStatus = paymentStatusMap.get(PaymentConstants.PASS_M_AWAIT);
			else if (multiAuthorisation.getStatus() == OBExternalStatus2Code.REJECTED)
				cmaStatus = paymentStatusMap.get(PaymentConstants.PASS_M_REJECT);
		}
		return cmaStatus;
	}

	private boolean isRejectionContractIdentification(String contractIdentification) {
		return contractIdentification.matches(contractIdentificationPattern);
	}
}
