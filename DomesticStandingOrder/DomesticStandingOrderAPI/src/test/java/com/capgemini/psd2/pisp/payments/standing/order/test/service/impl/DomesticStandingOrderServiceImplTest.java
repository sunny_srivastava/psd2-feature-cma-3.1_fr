package com.capgemini.psd2.pisp.payments.standing.order.test.service.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.consent.domain.PispConsent;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.adapter.DomesticStandingOrderAdapter;
import com.capgemini.psd2.pisp.adapter.PispConsentAdapter;
import com.capgemini.psd2.pisp.domain.CustomDStandingOrderConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.CustomDStandingOrderPOSTRequest;
import com.capgemini.psd2.pisp.domain.DStandingOrderPOST201Response;
import com.capgemini.psd2.pisp.domain.OBCashAccountDebtor3;
import com.capgemini.psd2.pisp.domain.OBDomesticStandingOrder1;
import com.capgemini.psd2.pisp.domain.OBExternalStatus2Code;
import com.capgemini.psd2.pisp.domain.OBMultiAuthorisation1;
import com.capgemini.psd2.pisp.domain.OBWriteDataDomesticStandingOrder1;
import com.capgemini.psd2.pisp.domain.OBWriteDataDomesticStandingOrderConsentResponse1;
import com.capgemini.psd2.pisp.domain.OBWriteDataDomesticStandingOrderResponse1;
import com.capgemini.psd2.pisp.domain.PaymentConsentsPlatformResource;
import com.capgemini.psd2.pisp.domain.PaymentSetupPOSTResponseLinks;
import com.capgemini.psd2.pisp.domain.PaymentSetupPOSTResponseMeta;
import com.capgemini.psd2.pisp.domain.PaymentsPlatformResource;
import com.capgemini.psd2.pisp.enums.ProcessConsentStatusEnum;
import com.capgemini.psd2.pisp.payments.standing.order.comparator.DomesticStandingOrderPayloadComparator;
import com.capgemini.psd2.pisp.payments.standing.order.service.impl.DomesticStandingOrderServiceImpl;
import com.capgemini.psd2.pisp.processing.adapter.service.PaymentSubmissionProcessingAdapter;
import com.capgemini.psd2.pisp.stage.operations.adapter.DomesticStandingOrdersPaymentStagingAdapter;
import com.capgemini.psd2.pisp.status.PaymentConstants;

@RunWith(SpringJUnit4ClassRunner.class)
public class DomesticStandingOrderServiceImplTest {

	@Mock
	private DomesticStandingOrdersPaymentStagingAdapter dPaymentStagingAdapter;

	@Mock
	private DomesticStandingOrderAdapter dStandingOrderAdapter;

	@Mock
	private DomesticStandingOrderPayloadComparator comparator;
	
	@Mock
	private RequestHeaderAttributes requestHeaderAttr;

	@Mock
	private PispConsentAdapter pispConsentAdapter;

	@Mock
	private PaymentSubmissionProcessingAdapter<CustomDStandingOrderPOSTRequest, DStandingOrderPOST201Response> paymentsProcessingAdapter;

	@InjectMocks
	private DomesticStandingOrderServiceImpl service;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void contextLoads() {
	}

	@Test
	public void testcreateDomesticStandingOrderResource() {
		DStandingOrderPOST201Response resource = new DStandingOrderPOST201Response();

		OBWriteDataDomesticStandingOrderResponse1 data1 = new OBWriteDataDomesticStandingOrderResponse1();

		OBMultiAuthorisation1 multi = new OBMultiAuthorisation1();
		multi.setStatus(OBExternalStatus2Code.AUTHORISED);
		data1.setMultiAuthorisation(multi);

		resource.setData(new OBWriteDataDomesticStandingOrderResponse1());
		resource.getData().setInitiation(new OBDomesticStandingOrder1());
		resource.setLinks(new PaymentSetupPOSTResponseLinks());
		resource.setMeta(new PaymentSetupPOSTResponseMeta());
		Map<String, Object> paymentsPlatformResourceMap = new HashMap<>();

		PaymentConsentsPlatformResource payresource = new PaymentConsentsPlatformResource();
		payresource.setId("1231");

		PaymentsPlatformResource paymentsPlatformResource = new PaymentsPlatformResource();
		paymentsPlatformResource.setProccessState(PaymentConstants.INCOMPLETE);

		paymentsPlatformResourceMap.put(PaymentConstants.CONSENT, payresource);
		paymentsPlatformResourceMap.put(PaymentConstants.SUBMISSION, paymentsPlatformResource);

		CustomDStandingOrderPOSTRequest customDomesticStandingOrderPOSTRequest = new CustomDStandingOrderPOSTRequest();
		customDomesticStandingOrderPOSTRequest.setCreatedOn("30/11/18");

		OBWriteDataDomesticStandingOrder1 data = new OBWriteDataDomesticStandingOrder1();
		data.consentId("12345");
		OBDomesticStandingOrder1 intition = new OBDomesticStandingOrder1();
		intition.setFrequency("test");
		OBCashAccountDebtor3 ad = new OBCashAccountDebtor3();

		ad.setName("debtorAccount");
		intition.setDebtorAccount(ad);
		data.setInitiation(intition);
		customDomesticStandingOrderPOSTRequest.setData(data);

		// Response
		CustomDStandingOrderConsentsPOSTResponse response = new CustomDStandingOrderConsentsPOSTResponse();
		OBWriteDataDomesticStandingOrderConsentResponse1 obdata = new OBWriteDataDomesticStandingOrderConsentResponse1();
		response.setId("1234");
		response.setConsentProcessStatus(ProcessConsentStatusEnum.PASS);
		response.setData(obdata.consentId("12345"));
		response.getData().setInitiation(intition);

		Object f = new Object();
		response.setFraudScore(f);

		Mockito.when(paymentsProcessingAdapter.preSubmissionProcessing(anyObject(), anyObject(), anyObject()))
				.thenReturn(paymentsPlatformResourceMap);

		Mockito.when(dPaymentStagingAdapter.retrieveStagedDomesticStandingOrdersConsents(anyObject(), anyObject()))
				.thenReturn(response);

		Mockito.when(dStandingOrderAdapter.processDomesticStandingOrder(anyObject(), anyObject(), anyObject()))
				.thenReturn(resource);

		Mockito.when(
				paymentsProcessingAdapter.createInitialPaymentsPlatformResource(anyObject(), anyObject(), anyObject()))
				.thenReturn(paymentsPlatformResource);

		Mockito.when(paymentsProcessingAdapter.postPaymentProcessFlows(anyObject(), anyObject(), anyObject(),
				anyObject(), anyObject(), anyObject())).thenReturn(resource);

		service.createDomesticStandingOrderResource(customDomesticStandingOrderPOSTRequest);
		assertEquals(data.getConsentId(), "12345");
	}

	@Test
	public void testcreateDomesticStandingOrderResource3() {
		DStandingOrderPOST201Response resource = new DStandingOrderPOST201Response();

		OBWriteDataDomesticStandingOrderResponse1 data1 = new OBWriteDataDomesticStandingOrderResponse1();

		OBMultiAuthorisation1 multi = new OBMultiAuthorisation1();
		multi.setStatus(OBExternalStatus2Code.AUTHORISED);
		data1.setMultiAuthorisation(multi);

		resource.setData(new OBWriteDataDomesticStandingOrderResponse1());
		resource.getData().setInitiation(new OBDomesticStandingOrder1());
		resource.setLinks(new PaymentSetupPOSTResponseLinks());
		resource.setMeta(new PaymentSetupPOSTResponseMeta());
		Map<String, Object> paymentsPlatformResourceMap = new HashMap<>();

		PaymentConsentsPlatformResource payresource = new PaymentConsentsPlatformResource();
		payresource.setId("1231");

		PaymentsPlatformResource paymentsPlatformResource = new PaymentsPlatformResource();
		paymentsPlatformResource.setProccessState(PaymentConstants.INCOMPLETE);

		paymentsPlatformResourceMap.put(PaymentConstants.CONSENT, payresource);
		paymentsPlatformResourceMap.put(PaymentConstants.SUBMISSION, paymentsPlatformResource);

		CustomDStandingOrderPOSTRequest customDomesticStandingOrderPOSTRequest = new CustomDStandingOrderPOSTRequest();
		customDomesticStandingOrderPOSTRequest.setCreatedOn("30/11/18");

		OBWriteDataDomesticStandingOrder1 data = new OBWriteDataDomesticStandingOrder1();
		data.consentId("12345");
		OBDomesticStandingOrder1 intition = new OBDomesticStandingOrder1();
		intition.setFrequency("test");
		OBCashAccountDebtor3 ad = new OBCashAccountDebtor3();

		ad.setName(null);
		intition.setDebtorAccount(ad);
		data.setInitiation(intition);
		customDomesticStandingOrderPOSTRequest.setData(data);

		// Response
		CustomDStandingOrderConsentsPOSTResponse response = new CustomDStandingOrderConsentsPOSTResponse();
		OBWriteDataDomesticStandingOrderConsentResponse1 obdata = new OBWriteDataDomesticStandingOrderConsentResponse1();
		response.setId("1234");
		response.setConsentProcessStatus(ProcessConsentStatusEnum.PASS);
		response.setData(obdata.consentId("12345"));

		OBDomesticStandingOrder1 intition1 = new OBDomesticStandingOrder1();
		OBCashAccountDebtor3 ad1 = new OBCashAccountDebtor3();
		ad1.setName("suhas");
		intition1.setDebtorAccount(ad1);
		;
		response.getData().setInitiation(intition1);

		Object f = new Object();
		response.setFraudScore(f);

		Mockito.when(paymentsProcessingAdapter.preSubmissionProcessing(anyObject(), anyObject(), anyObject()))
				.thenReturn(paymentsPlatformResourceMap);

		Mockito.when(dPaymentStagingAdapter.retrieveStagedDomesticStandingOrdersConsents(anyObject(), anyObject()))
				.thenReturn(response);

		Mockito.when(dStandingOrderAdapter.processDomesticStandingOrder(anyObject(), anyObject(), anyObject()))
				.thenReturn(resource);

		Mockito.when(
				paymentsProcessingAdapter.createInitialPaymentsPlatformResource(anyObject(), anyObject(), anyObject()))
				.thenReturn(paymentsPlatformResource);

		Mockito.when(paymentsProcessingAdapter.postPaymentProcessFlows(anyObject(), anyObject(), anyObject(),
				anyObject(), anyObject(), anyObject())).thenReturn(resource);

		service.createDomesticStandingOrderResource(customDomesticStandingOrderPOSTRequest);
		assertEquals(data.getConsentId(), "12345");
	}

	@Test
	public void testcreateDomesticStandingOrderResource4() {
		DStandingOrderPOST201Response resource = new DStandingOrderPOST201Response();

		OBWriteDataDomesticStandingOrderResponse1 data1 = new OBWriteDataDomesticStandingOrderResponse1();

		OBMultiAuthorisation1 multi = new OBMultiAuthorisation1();
		multi.setStatus(OBExternalStatus2Code.AUTHORISED);
		data1.setMultiAuthorisation(multi);

		resource.setData(new OBWriteDataDomesticStandingOrderResponse1());
		resource.getData().setInitiation(new OBDomesticStandingOrder1());
		resource.setLinks(new PaymentSetupPOSTResponseLinks());
		resource.setMeta(new PaymentSetupPOSTResponseMeta());
		Map<String, Object> paymentsPlatformResourceMap = new HashMap<>();

		PaymentConsentsPlatformResource payresource = new PaymentConsentsPlatformResource();
		payresource.setId("1231");

		PaymentsPlatformResource paymentsPlatformResource = new PaymentsPlatformResource();
		paymentsPlatformResource.setProccessState(PaymentConstants.INCOMPLETE);

		paymentsPlatformResourceMap.put(PaymentConstants.CONSENT, payresource);
		paymentsPlatformResourceMap.put(PaymentConstants.SUBMISSION, paymentsPlatformResource);

		CustomDStandingOrderPOSTRequest customDomesticStandingOrderPOSTRequest = new CustomDStandingOrderPOSTRequest();
		customDomesticStandingOrderPOSTRequest.setCreatedOn("30/11/18");

		OBWriteDataDomesticStandingOrder1 data = new OBWriteDataDomesticStandingOrder1();
		data.consentId("12345");
		OBDomesticStandingOrder1 intition = new OBDomesticStandingOrder1();
		intition.setFrequency("test");

		intition.setDebtorAccount(null);
		data.setInitiation(intition);
		customDomesticStandingOrderPOSTRequest.setData(data);

		// Response
		CustomDStandingOrderConsentsPOSTResponse response = new CustomDStandingOrderConsentsPOSTResponse();
		OBWriteDataDomesticStandingOrderConsentResponse1 obdata = new OBWriteDataDomesticStandingOrderConsentResponse1();
		response.setId("1234");
		response.setConsentProcessStatus(ProcessConsentStatusEnum.PASS);
		response.setData(obdata.consentId("12345"));

		OBDomesticStandingOrder1 intition1 = new OBDomesticStandingOrder1();
		OBCashAccountDebtor3 ad1 = new OBCashAccountDebtor3();
		ad1.setName("suhas");
		intition1.setDebtorAccount(ad1);
		;
		response.getData().setInitiation(intition1);

		Object f = new Object();
		response.setFraudScore(f);

		Mockito.when(paymentsProcessingAdapter.preSubmissionProcessing(anyObject(), anyObject(), anyObject()))
				.thenReturn(paymentsPlatformResourceMap);

		Mockito.when(dPaymentStagingAdapter.retrieveStagedDomesticStandingOrdersConsents(anyObject(), anyObject()))
				.thenReturn(response);

		Mockito.when(dStandingOrderAdapter.processDomesticStandingOrder(anyObject(), anyObject(), anyObject()))
				.thenReturn(resource);

		Mockito.when(
				paymentsProcessingAdapter.createInitialPaymentsPlatformResource(anyObject(), anyObject(), anyObject()))
				.thenReturn(paymentsPlatformResource);

		Mockito.when(paymentsProcessingAdapter.postPaymentProcessFlows(anyObject(), anyObject(), anyObject(),
				anyObject(), anyObject(), anyObject())).thenReturn(resource);

		service.createDomesticStandingOrderResource(customDomesticStandingOrderPOSTRequest);
		assertEquals(data.getConsentId(), "12345");
	}

	@Test
	public void testcreateDomesticStandingOrderResource1() {
		DStandingOrderPOST201Response resource = new DStandingOrderPOST201Response();

		resource.setData(new OBWriteDataDomesticStandingOrderResponse1());
		resource.getData().setInitiation(new OBDomesticStandingOrder1());
		resource.setLinks(new PaymentSetupPOSTResponseLinks());
		resource.setMeta(new PaymentSetupPOSTResponseMeta());
		Map<String, Object> paymentsPlatformResourceMap = new HashMap<>();

		PaymentConsentsPlatformResource payresource = new PaymentConsentsPlatformResource();
		payresource.setId("1231");

		PaymentsPlatformResource paymentsPlatformResource = new PaymentsPlatformResource();
		paymentsPlatformResource.setProccessState(PaymentConstants.COMPLETED);

		paymentsPlatformResourceMap.put(PaymentConstants.CONSENT, payresource);
		paymentsPlatformResourceMap.put(PaymentConstants.SUBMISSION, paymentsPlatformResource);

		CustomDStandingOrderPOSTRequest customDomesticStandingOrderPOSTRequest = new CustomDStandingOrderPOSTRequest();
		customDomesticStandingOrderPOSTRequest.setCreatedOn("30/11/18");

		OBWriteDataDomesticStandingOrder1 data = new OBWriteDataDomesticStandingOrder1();
		data.consentId("12345");
		OBDomesticStandingOrder1 intition = new OBDomesticStandingOrder1();
		intition.setFrequency("test");
		OBCashAccountDebtor3 ad = new OBCashAccountDebtor3();

		ad.setName("debtorAccount");
		intition.setDebtorAccount(ad);
		data.setInitiation(intition);
		customDomesticStandingOrderPOSTRequest.setData(data);

		// Response
		CustomDStandingOrderConsentsPOSTResponse response = new CustomDStandingOrderConsentsPOSTResponse();
		OBWriteDataDomesticStandingOrderConsentResponse1 obdata = new OBWriteDataDomesticStandingOrderConsentResponse1();
		response.setId("1234");
		response.setConsentProcessStatus(ProcessConsentStatusEnum.PASS);
		response.setData(obdata.consentId("12345"));
		response.getData().setInitiation(intition);

		Object f = new Object();
		response.setFraudScore(f);

		Mockito.when(paymentsProcessingAdapter.preSubmissionProcessing(anyObject(), anyObject(), anyObject()))
				.thenReturn(paymentsPlatformResourceMap);

		Mockito.when(dPaymentStagingAdapter.retrieveStagedDomesticStandingOrdersConsents(anyObject(), anyObject()))
				.thenReturn(response);

		Mockito.when(dStandingOrderAdapter.retrieveStagedDomesticStandingOrder(anyObject(), anyObject()))
				.thenReturn(resource);

		Mockito.when(
				paymentsProcessingAdapter.createInitialPaymentsPlatformResource(anyObject(), anyObject(), anyObject()))
				.thenReturn(paymentsPlatformResource);

		Mockito.when(paymentsProcessingAdapter.postPaymentProcessFlows(anyObject(), anyObject(), anyObject(),
				anyObject(), anyObject(), anyObject())).thenReturn(resource);

		service.createDomesticStandingOrderResource(customDomesticStandingOrderPOSTRequest);
		assertEquals(data.getConsentId(), "12345");
	}

	@Test
	public void testcreateDomesticStandingOrderResource2() {
		DStandingOrderPOST201Response resource = new DStandingOrderPOST201Response();

		resource.setData(new OBWriteDataDomesticStandingOrderResponse1());
		resource.getData().setInitiation(new OBDomesticStandingOrder1());
		resource.setLinks(new PaymentSetupPOSTResponseLinks());
		resource.setMeta(new PaymentSetupPOSTResponseMeta());
		Map<String, Object> paymentsPlatformResourceMap = new HashMap<>();

		PaymentConsentsPlatformResource payresource = new PaymentConsentsPlatformResource();
		payresource.setId("1231");

		PaymentsPlatformResource paymentsPlatformResource = null;
		PaymentsPlatformResource paymentsPlatformResource1 = new PaymentsPlatformResource();
		paymentsPlatformResource1.setProccessState(PaymentConstants.INCOMPLETE);

		paymentsPlatformResourceMap.put(PaymentConstants.CONSENT, payresource);
		paymentsPlatformResourceMap.put(PaymentConstants.SUBMISSION, paymentsPlatformResource);

		CustomDStandingOrderPOSTRequest customDomesticStandingOrderPOSTRequest = new CustomDStandingOrderPOSTRequest();
		customDomesticStandingOrderPOSTRequest.setCreatedOn("30/11/18");

		OBWriteDataDomesticStandingOrder1 data = new OBWriteDataDomesticStandingOrder1();
		data.consentId("12345");
		OBDomesticStandingOrder1 intition = new OBDomesticStandingOrder1();
		intition.setFrequency("test");
		OBCashAccountDebtor3 ad = new OBCashAccountDebtor3();

		ad.setName("debtorAccount");
		intition.setDebtorAccount(ad);
		data.setInitiation(intition);
		customDomesticStandingOrderPOSTRequest.setData(data);

		// Response
		CustomDStandingOrderConsentsPOSTResponse response = new CustomDStandingOrderConsentsPOSTResponse();
		OBWriteDataDomesticStandingOrderConsentResponse1 obdata = new OBWriteDataDomesticStandingOrderConsentResponse1();
		response.setId("1234");
		response.setConsentProcessStatus(ProcessConsentStatusEnum.PASS);
		response.setData(obdata.consentId("12345"));
		response.getData().setInitiation(intition);

		Object f = new Object();
		response.setFraudScore(f);

		Mockito.when(paymentsProcessingAdapter.preSubmissionProcessing(anyObject(), anyObject(), anyObject()))
				.thenReturn(paymentsPlatformResourceMap);

		Mockito.when(dPaymentStagingAdapter.retrieveStagedDomesticStandingOrdersConsents(anyObject(), anyObject()))
				.thenReturn(response);

		Mockito.when(dStandingOrderAdapter.processDomesticStandingOrder(anyObject(), anyObject(), anyObject()))
				.thenReturn(resource);

		Mockito.when(
				paymentsProcessingAdapter.createInitialPaymentsPlatformResource(anyObject(), anyObject(), anyObject()))
				.thenReturn(paymentsPlatformResource1);

		Mockito.when(paymentsProcessingAdapter.postPaymentProcessFlows(anyObject(), anyObject(), anyObject(),
				anyObject(), anyObject(), anyObject())).thenReturn(resource);

		service.createDomesticStandingOrderResource(customDomesticStandingOrderPOSTRequest);
		assertEquals(data.getConsentId(), "12345");
	}

	@Test(expected = PSD2Exception.class)
	public void testcreateDomesticStandingOrderResourcePSD2Exception() {
		DStandingOrderPOST201Response resource = new DStandingOrderPOST201Response();

		resource.setData(new OBWriteDataDomesticStandingOrderResponse1());
		resource.getData().setInitiation(new OBDomesticStandingOrder1());
		resource.setLinks(new PaymentSetupPOSTResponseLinks());
		resource.setMeta(new PaymentSetupPOSTResponseMeta());
		Map<String, Object> paymentsPlatformResourceMap = new HashMap<>();

		PaymentConsentsPlatformResource payresource = new PaymentConsentsPlatformResource();
		payresource.setId("1231");

		PaymentsPlatformResource paymentsPlatformResource = new PaymentsPlatformResource();
		paymentsPlatformResource.setProccessState(PaymentConstants.INCOMPLETE);

		paymentsPlatformResourceMap.put(PaymentConstants.CONSENT, payresource);
		paymentsPlatformResourceMap.put(PaymentConstants.SUBMISSION, paymentsPlatformResource);

		CustomDStandingOrderPOSTRequest customDomesticStandingOrderPOSTRequest = new CustomDStandingOrderPOSTRequest();
		customDomesticStandingOrderPOSTRequest.setCreatedOn("30/11/18");

		OBWriteDataDomesticStandingOrder1 data = new OBWriteDataDomesticStandingOrder1();
		data.consentId("12345");
		OBDomesticStandingOrder1 intition = new OBDomesticStandingOrder1();
		intition.setFrequency("test");
		OBCashAccountDebtor3 ad = new OBCashAccountDebtor3();

		ad.setName("debtorAccount");
		intition.setDebtorAccount(ad);
		data.setInitiation(intition);
		customDomesticStandingOrderPOSTRequest.setData(data);

		// Response
		CustomDStandingOrderConsentsPOSTResponse response = new CustomDStandingOrderConsentsPOSTResponse();
		OBWriteDataDomesticStandingOrderConsentResponse1 obdata = new OBWriteDataDomesticStandingOrderConsentResponse1();
		response.setId("1234");
		response.setConsentProcessStatus(ProcessConsentStatusEnum.PASS);
		response.setData(obdata.consentId("12345"));
		response.getData().setInitiation(intition);

		Object f = new Object();
		response.setFraudScore(f);

		Mockito.when(paymentsProcessingAdapter.preSubmissionProcessing(anyObject(), anyObject(), anyObject()))
				.thenReturn(paymentsPlatformResourceMap);

		Mockito.when(dPaymentStagingAdapter.retrieveStagedDomesticStandingOrdersConsents(anyObject(), anyObject()))
				.thenReturn(response);

		Mockito.when(dStandingOrderAdapter.processDomesticStandingOrder(anyObject(), anyObject(), anyObject()))
				.thenReturn(null);

		Mockito.when(
				paymentsProcessingAdapter.createInitialPaymentsPlatformResource(anyObject(), anyObject(), anyObject()))
				.thenReturn(paymentsPlatformResource);

		Mockito.when(paymentsProcessingAdapter.postPaymentProcessFlows(anyObject(), anyObject(), anyObject(),
				anyObject(), anyObject(), anyObject())).thenReturn(resource);

		service.createDomesticStandingOrderResource(customDomesticStandingOrderPOSTRequest);
		// assertEquals(data.getConsentId(), "12345");
	}

	@Test
	public void testretrieveDomesticStandingOrderResource() {
		DStandingOrderPOST201Response resource = new DStandingOrderPOST201Response();
		OBWriteDataDomesticStandingOrderResponse1 data = new OBWriteDataDomesticStandingOrderResponse1();
		OBMultiAuthorisation1 multi = new OBMultiAuthorisation1();
		multi.setStatus(OBExternalStatus2Code.AUTHORISED);
		data.setMultiAuthorisation(multi);
		data.setConsentId("1234");

		resource.setData(data);
		resource.getData().setInitiation(new OBDomesticStandingOrder1());
		resource.setLinks(new PaymentSetupPOSTResponseLinks());
		resource.setMeta(new PaymentSetupPOSTResponseMeta());
		Map<String, Object> paymentsPlatformResourceMap = new HashMap<>();

		PaymentsPlatformResource paymentsPlatformResource = new PaymentsPlatformResource();
		paymentsPlatformResource.setProccessState(PaymentConstants.INCOMPLETE);
		paymentsPlatformResource.setPaymentConsentId("2233");

		paymentsPlatformResourceMap.put(PaymentConstants.SUBMISSION, paymentsPlatformResource);

		Mockito.when(paymentsProcessingAdapter.prePaymentProcessGETFlows(anyObject(), anyObject()))
				.thenReturn(paymentsPlatformResourceMap);

		Mockito.when(dStandingOrderAdapter.retrieveStagedDomesticStandingOrder(anyObject(), anyObject()))
				.thenReturn(resource);

		Mockito.when(paymentsProcessingAdapter.postPaymentProcessFlows(anyObject(), anyObject(), anyObject(),
				anyObject(), anyObject(), anyObject())).thenReturn(resource);
		
		PispConsent consent = new PispConsent();
		consent.setPsuId("12345");
		when(pispConsentAdapter.retrieveConsentByPaymentId(anyObject())).thenReturn(consent);


		service.retrieveDomesticStandingOrderResource("12345");
		assertEquals(data.getConsentId(), "1234");
	}

	@Test
	public void testretrieveDomesticStandingOrderResource2() {
		DStandingOrderPOST201Response resource = new DStandingOrderPOST201Response();
		OBWriteDataDomesticStandingOrderResponse1 data = new OBWriteDataDomesticStandingOrderResponse1();
		data.setMultiAuthorisation(null);
		data.setConsentId("1234");

		resource.setData(data);
		resource.getData().setInitiation(new OBDomesticStandingOrder1());
		resource.setLinks(new PaymentSetupPOSTResponseLinks());
		resource.setMeta(new PaymentSetupPOSTResponseMeta());
		Map<String, Object> paymentsPlatformResourceMap = new HashMap<>();

		PaymentsPlatformResource paymentsPlatformResource = new PaymentsPlatformResource();
		paymentsPlatformResource.setProccessState(PaymentConstants.INCOMPLETE);
		paymentsPlatformResource.setPaymentConsentId("2233");

		paymentsPlatformResourceMap.put(PaymentConstants.SUBMISSION, paymentsPlatformResource);

		Mockito.when(paymentsProcessingAdapter.prePaymentProcessGETFlows(anyObject(), anyObject()))
				.thenReturn(paymentsPlatformResourceMap);

		PispConsent consent = new PispConsent();
		consent.setPsuId("12345");
		when(pispConsentAdapter.retrieveConsentByPaymentId(anyObject())).thenReturn(consent);

		Mockito.when(dStandingOrderAdapter.retrieveStagedDomesticStandingOrder(anyObject(), anyObject()))
				.thenReturn(resource);

		Mockito.when(paymentsProcessingAdapter.postPaymentProcessFlows(anyObject(), anyObject(), anyObject(),
				anyObject(), anyObject(), anyObject())).thenReturn(resource);

		service.retrieveDomesticStandingOrderResource("12345");
		assertEquals(data.getConsentId(), "1234");
	}

	@Test(expected = PSD2Exception.class)
	public void testretrieveDomesticStandingOrderResourceException() {
		DStandingOrderPOST201Response resource = new DStandingOrderPOST201Response();
		OBWriteDataDomesticStandingOrderResponse1 data = new OBWriteDataDomesticStandingOrderResponse1();
		OBMultiAuthorisation1 multi = new OBMultiAuthorisation1();
		multi.setStatus(OBExternalStatus2Code.AUTHORISED);
		data.setMultiAuthorisation(multi);
		data.setConsentId("1234");

		resource.setData(data);
		resource.getData().setInitiation(new OBDomesticStandingOrder1());
		resource.setLinks(new PaymentSetupPOSTResponseLinks());
		resource.setMeta(new PaymentSetupPOSTResponseMeta());
		Map<String, Object> paymentsPlatformResourceMap = new HashMap<>();

		PaymentsPlatformResource paymentsPlatformResource = new PaymentsPlatformResource();
		paymentsPlatformResource.setProccessState(PaymentConstants.INCOMPLETE);
		paymentsPlatformResource.setPaymentConsentId("2233");

		paymentsPlatformResourceMap.put(PaymentConstants.SUBMISSION, paymentsPlatformResource);

		Mockito.when(paymentsProcessingAdapter.prePaymentProcessGETFlows(anyObject(), anyObject()))
				.thenReturn(paymentsPlatformResourceMap);

		Mockito.when(dStandingOrderAdapter.retrieveStagedDomesticStandingOrder(anyObject(), anyObject()))
				.thenReturn(null);

		Mockito.when(paymentsProcessingAdapter.postPaymentProcessFlows(anyObject(), anyObject(), anyObject(),
				anyObject(), anyObject(), anyObject())).thenReturn(resource);

		PispConsent consent = new PispConsent();
		consent.setPsuId("12345");
		when(pispConsentAdapter.retrieveConsentByPaymentId(anyObject())).thenReturn(consent);

		service.retrieveDomesticStandingOrderResource("12345");
		assertEquals(data.getConsentId(), "1234");
	}

}