package com.capgemini.psd2.pisp.standing.order.submission.adapter.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.adapter.DomesticStandingOrderAdapter;
import com.capgemini.psd2.pisp.domain.CustomDStandingOrderPOSTRequest;
import com.capgemini.psd2.pisp.domain.DStandingOrderPOST201Response;
import com.capgemini.psd2.pisp.domain.OBExternalStatus1Code;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.pisp.standing.order.submission.adapter.routing.DStandingOrderAdapterFactory;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Component("dStandingOrderRoutingAdapter")
public class DStandingOrderRoutingAdapterImpl implements DomesticStandingOrderAdapter {

	private DomesticStandingOrderAdapter beanInstance;

	@Autowired
	private DStandingOrderAdapterFactory factory;

	@Autowired
	private RequestHeaderAttributes reqHeaderAtrributes;

	/** The default adapter. */
	@Value("${app.paymentStagingAdapter}")
	private String paymentStagingAdapter;

	@Override
	public DStandingOrderPOST201Response processDomesticStandingOrder(CustomDStandingOrderPOSTRequest request,
			Map<String, OBExternalStatus1Code> paymentStatusMap, Map<String, String> params) {
		return getDomesticRoutingAdapter().processDomesticStandingOrder(request, paymentStatusMap,
				addHeaderParams(params));
	}

	@Override
	public DStandingOrderPOST201Response retrieveStagedDomesticStandingOrder(
			CustomPaymentStageIdentifiers customPaymentStageIdentifiers, Map<String, String> params) {
		return getDomesticRoutingAdapter().retrieveStagedDomesticStandingOrder(customPaymentStageIdentifiers,
				addHeaderParams(params));
	}

	private DomesticStandingOrderAdapter getDomesticRoutingAdapter() {
		if (beanInstance == null)
			beanInstance = factory.getDomesticStandingOrdersInstance(paymentStagingAdapter);
		return beanInstance;
	}

	private Map<String, String> addHeaderParams(Map<String, String> mapParam) {
		/*
		 * Below code will be used when update service will be called from
		 * payment submission API. For submission flow -> update, PSUId and
		 * ChannelName are mandatory fields for FS
		 */
		if (NullCheckUtils.isNullOrEmpty(mapParam))
			mapParam = new HashMap<>(); //NOSONAR

		if (null != reqHeaderAtrributes.getToken() && null != reqHeaderAtrributes.getToken().getSeviceParams()) {
			mapParam.put(PSD2Constants.CHANNEL_IN_REQ_HEADER,
					reqHeaderAtrributes.getToken().getSeviceParams().get(PSD2Constants.CHANNEL_NAME));
		}
		mapParam.put(PSD2Constants.USER_IN_REQ_HEADER, reqHeaderAtrributes.getPsuId());
		mapParam.put(PSD2Constants.CORRELATION_REQ_HEADER, reqHeaderAtrributes.getCorrelationId());
		mapParam.put(PSD2Constants.TENANT_ID, reqHeaderAtrributes.getTenantId());
		return mapParam;
	}

}
