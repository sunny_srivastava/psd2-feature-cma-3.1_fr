package com.capgemini.psd2.funds.confirmation.consent.test.mongo.db.adapter.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.cisp.adapter.CispConsentAdapter;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmationConsentResponse1;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmationConsentResponse1Data;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmationConsentResponse1Data.StatusEnum;
import com.capgemini.psd2.consent.domain.CispConsent;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.funds.confirmation.consent.mongo.db.adapter.impl.FundsConfirmationConsentMongoDbAdapterImpl;
import com.capgemini.psd2.funds.confirmation.consent.mongo.db.adapter.repository.FundsConfirmationConsentRepository;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.product.common.CompatibleVersionList;
import com.capgemini.psd2.utilities.DateUtilites;

@RunWith(SpringJUnit4ClassRunner.class)
public class FundsConfirmationConsentMongoDbAdapterImplTest {

	@Mock
	private FundsConfirmationConsentRepository fundsConfirmationConsentRepository;

	@Mock
	private CispConsentAdapter cispConsentAdapter;
	
	@Mock
	private CompatibleVersionList compatibleVersionList;

	@InjectMocks
	private FundsConfirmationConsentMongoDbAdapterImpl adapter;
	
	@Mock
	private RequestHeaderAttributes req;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		
		Mockito.when(req.getTenantId()).thenReturn("1");
	}

	@Test
	public void contextLoads() {
	}

	@Test
	public void testCreateFundsConfirmationConsentPOSTResponse() {
		OBFundsConfirmationConsentResponse1Data data = new OBFundsConfirmationConsentResponse1Data();
		data.setConsentId("1234");
		OBFundsConfirmationConsentResponse1 fundsConfirmationConsentPOSTResponse = new OBFundsConfirmationConsentResponse1();
		fundsConfirmationConsentPOSTResponse.setData(data);
		when(fundsConfirmationConsentRepository.save(any(OBFundsConfirmationConsentResponse1Data.class)))
				.thenReturn(data);
		adapter.createFundsConfirmationConsentPOSTResponse(data);
		assertNotNull(data);
	}

	@SuppressWarnings("unchecked")
	@Test(expected = PSD2Exception.class)
	public void testCreateFundsConfirmationConsentPOSTResponseWithNullData() {
	
		OBFundsConfirmationConsentResponse1Data data1 = null;
		
		OBFundsConfirmationConsentResponse1 fundsConfirmationConsentPOSTResponse = new OBFundsConfirmationConsentResponse1();
		when(fundsConfirmationConsentRepository.save(any(OBFundsConfirmationConsentResponse1Data.class)))
				.thenThrow(DataAccessResourceFailureException.class);
		fundsConfirmationConsentPOSTResponse.setData(getfundData());
		
		adapter.createFundsConfirmationConsentPOSTResponse(getfundData());
		assertNull(data1);
	}

	@Test(expected = PSD2Exception.class)
	public void testGetFundsConfirmationConsentPOSTResponse() {
		OBFundsConfirmationConsentResponse1Data data = new OBFundsConfirmationConsentResponse1Data();
		data.setTppCID("12345");
		String consentId = "1234";
		data.setConsentId(consentId);
		List<String> versionList = new ArrayList<>();
		versionList.add("2.0");
		data.setStatus(StatusEnum.AUTHORISED);
		Mockito.when(req.getTppCID()).thenReturn("1");
		when(compatibleVersionList.fetchVersionList()).thenReturn(versionList);
		OBFundsConfirmationConsentResponse1 fundsConfirmationConsentPOSTResponse = new OBFundsConfirmationConsentResponse1();
		fundsConfirmationConsentPOSTResponse.setData(data);
		when(fundsConfirmationConsentRepository.findByConsentIdAndCmaVersionIn(any(), any())).thenReturn(data);
		adapter.getFundsConfirmationConsentPOSTResponse(consentId);
		assertNotNull(data);
	}
	
	@Test(expected = PSD2Exception.class)
	public void testGetFundsConfirmationConsentPOSTResponsenull() {
		OBFundsConfirmationConsentResponse1Data data = new OBFundsConfirmationConsentResponse1Data();
		String consentId = "1234";
		data.setConsentId(consentId);
		List<String> versionList = new ArrayList<>();
		versionList.add("2.0");
		when(compatibleVersionList.fetchVersionList()).thenReturn(versionList);
		OBFundsConfirmationConsentResponse1 fundsConfirmationConsentPOSTResponse = new OBFundsConfirmationConsentResponse1();
		fundsConfirmationConsentPOSTResponse.setData(data);
		when(fundsConfirmationConsentRepository.findByConsentIdAndCmaVersionIn(any(), any())).thenReturn(null);
		adapter.getFundsConfirmationConsentPOSTResponse(consentId);
		assertNull(data);
	}
	
	

	@SuppressWarnings("unchecked")
	@Test(expected = PSD2Exception.class)
	public void testGetFundsConfirmationConsentPOSTResponseWithNoResourceFound() {
		OBFundsConfirmationConsentResponse1Data data1 = null;
		OBFundsConfirmationConsentResponse1 fundsConfirmationConsentPOSTResponse = new OBFundsConfirmationConsentResponse1();
		String consentId = "1456";
		List<String> versionList = new ArrayList<>();
		versionList.add("2.0");
		when(compatibleVersionList.fetchVersionList()).thenReturn(versionList);
		when(fundsConfirmationConsentRepository.findByConsentIdAndCmaVersionIn(any(), any()))
				.thenThrow(DataAccessResourceFailureException.class);
		fundsConfirmationConsentPOSTResponse.setData(data1);
		adapter.getFundsConfirmationConsentPOSTResponse(consentId);
		assertEquals(1456, consentId);
	}

	@SuppressWarnings("unchecked")
	@Test(expected = PSD2Exception.class)
	public void testGetFundsConfirmationConsentPOSTResponseWithNullData() {
		OBFundsConfirmationConsentResponse1Data data1 = null;
		OBFundsConfirmationConsentResponse1 fundsConfirmationConsentPOSTResponse = new OBFundsConfirmationConsentResponse1();
		String consentId = "3333";
		List<String> versionList = new ArrayList<>();
		versionList.add("2.0");
		when(compatibleVersionList.fetchVersionList()).thenReturn(versionList);
		when(fundsConfirmationConsentRepository.findByConsentIdAndCmaVersionIn(any(), any())).thenThrow(PSD2Exception.class);
		fundsConfirmationConsentPOSTResponse.setData(data1);
		adapter.getFundsConfirmationConsentPOSTResponse(null);
		assertEquals(3333, consentId);
	}

	@Test(expected = PSD2Exception.class)
	public void testGetFundsConfirmationConsentPOSTResponseWhenStatusIsDeleted() {
		OBFundsConfirmationConsentResponse1Data data1 = new OBFundsConfirmationConsentResponse1Data();
		data1.setStatus(StatusEnum.DELETED);
		String consentId = "3333";
		List<String> versionList = new ArrayList<>();
		versionList.add("2.0");
		when(compatibleVersionList.fetchVersionList()).thenReturn(versionList);
		when(fundsConfirmationConsentRepository.findByConsentIdAndCmaVersionIn(any(), any())).thenReturn(data1);
		adapter.getFundsConfirmationConsentPOSTResponse(consentId);
	}
	
	@Test
	public void testRemoveFundsConfirmationConsent() {
		OBFundsConfirmationConsentResponse1Data data1 = new OBFundsConfirmationConsentResponse1Data();
		String cId = "123456";
		data1.setTppCID(cId);
		data1.setStatusUpdateDateTime(DateUtilites.getCurrentDateInISOFormat());
		data1.setStatus(OBFundsConfirmationConsentResponse1Data.StatusEnum.AUTHORISED);
		String consentId = "11113";
		List<String> versionList = new ArrayList<>();
		versionList.add("2.0");
		when(compatibleVersionList.fetchVersionList()).thenReturn(versionList);
		when(fundsConfirmationConsentRepository.findByConsentIdAndCmaVersionIn(any(),any())).thenReturn(data1);
		when(fundsConfirmationConsentRepository.save(any(OBFundsConfirmationConsentResponse1Data.class)))
		.thenReturn(data1);
		List<CispConsent> cispConsent = new ArrayList<>();
		CispConsent consent = new CispConsent();
		consent.setChannelId("786478");
		cispConsent.add(consent);
		when(cispConsentAdapter.retrieveConsentByPsuIdAndConsentStatus(anyString(), any())).thenReturn(cispConsent);
		cispConsentAdapter.updateConsentStatusWithResponse(anyString(), any());
		adapter.removeFundsConfirmationConsent(consentId, cId);
	}
	
	@SuppressWarnings("unchecked")
	@Test(expected=PSD2Exception.class)
	public void testRemoveFundsConfirmationConsentNullcispConsent() {
		OBFundsConfirmationConsentResponse1Data data1 = new OBFundsConfirmationConsentResponse1Data();
		String cId = "123456";
		data1.setTppCID(cId);
		data1.setStatusUpdateDateTime(DateUtilites.getCurrentDateInISOFormat());
		data1.setStatus(OBFundsConfirmationConsentResponse1Data.StatusEnum.DELETED);
		String consentId = "11113";
		List<String> versionList = new ArrayList<>();
		versionList.add("2.0");
		when(compatibleVersionList.fetchVersionList()).thenReturn(versionList);
		when(fundsConfirmationConsentRepository.findByConsentIdAndCmaVersionIn(any(), any())).thenReturn(data1);
		when(fundsConfirmationConsentRepository.save(any(OBFundsConfirmationConsentResponse1Data.class)))
		.thenThrow(DataAccessResourceFailureException.class);
		when(cispConsentAdapter.retrieveConsentByPsuIdAndConsentStatus(anyString(), any())).thenReturn(null);
	    Mockito.doNothing().when(cispConsentAdapter).updateConsentStatusWithResponse(anyString(), any());
		adapter.removeFundsConfirmationConsent(consentId, cId);
	}
	
	
	
	@Test(expected = PSD2Exception.class)
	public void testRemoveFundsConfirmationConsentNullcispConsent1() {
		OBFundsConfirmationConsentResponse1Data data1 = new OBFundsConfirmationConsentResponse1Data();
		String cId = "123456";
		data1.setTppCID(cId);
		data1.setStatusUpdateDateTime(DateUtilites.getCurrentDateInISOFormat());
		data1.setStatus(OBFundsConfirmationConsentResponse1Data.StatusEnum.DELETED);
		String consentId = "11113";
		List<String> versionList = new ArrayList<>();
		versionList.add("2.0");
		when(compatibleVersionList.fetchVersionList()).thenReturn(versionList);
		when(fundsConfirmationConsentRepository.findByConsentIdAndCmaVersionIn(any(), any())).thenReturn(data1);
		when(fundsConfirmationConsentRepository.save(any(OBFundsConfirmationConsentResponse1Data.class)))
		.thenThrow(DataAccessResourceFailureException.class);
		CispConsent cis=new CispConsent();
		cis.setConsentId("12345");
		when(cispConsentAdapter.retrieveConsentByFundsIntentIdAndStatus(anyString(), any())).thenReturn(cis);
	    Mockito.doNothing().when(cispConsentAdapter).updateConsentStatusWithResponse(anyString(), any());
		adapter.removeFundsConfirmationConsent(consentId, cId);
	}
	
	@Test(expected = PSD2Exception.class)
	public void testRemoveFundsConfirmationConsentWithInvalidTppUser() {
		OBFundsConfirmationConsentResponse1Data data1 = new OBFundsConfirmationConsentResponse1Data();
		data1.setTppLegalEntityName("TPPLegalName");
		data1.setStatus(StatusEnum.AUTHORISED);
		String consentId = "11113";
		String cId = "11114";
		List<String> versionList = new ArrayList<>();
		versionList.add("2.0");
		when(compatibleVersionList.fetchVersionList()).thenReturn(versionList);
		when(fundsConfirmationConsentRepository.findByConsentIdAndCmaVersionIn(any(),any())).thenReturn(data1);
		adapter.removeFundsConfirmationConsent(consentId, cId);
	}
	
	@SuppressWarnings("unchecked")
	@Test(expected = PSD2Exception.class)
	public void testRemoveFundsConfirmationConsentWithNoResourceFound() {
		OBFundsConfirmationConsentResponse1Data data1 = null;
		String consentId = "11113";
		String cId = "11114";
		List<String> versionList = new ArrayList<>();
		versionList.add("2.0");
		when(compatibleVersionList.fetchVersionList()).thenReturn(versionList);
		when(fundsConfirmationConsentRepository.findByConsentIdAndCmaVersionIn(anyString(),any()))
				.thenThrow(DataAccessResourceFailureException.class);
		adapter.removeFundsConfirmationConsent(consentId, cId);
	}

	@SuppressWarnings("unchecked")
	@Test(expected = PSD2Exception.class)
	public void testRemoveFundsConfirmationConsentWithNullData() {
		OBFundsConfirmationConsentResponse1Data data1 = null;
		String consentId = "11113";
		List<String> versionList = new ArrayList<>();
		versionList.add("2.0");
		when(compatibleVersionList.fetchVersionList()).thenReturn(versionList);
		when(fundsConfirmationConsentRepository.findByConsentIdAndCmaVersionIn(any(),any())).thenThrow(PSD2Exception.class);
		adapter.removeFundsConfirmationConsent(null, null);
	}

	@Test(expected = PSD2Exception.class)
	public void testRemoveFundsConfirmationConsentWithDeletedStatus() {
		OBFundsConfirmationConsentResponse1Data data1 = new OBFundsConfirmationConsentResponse1Data();
		data1.setStatus(OBFundsConfirmationConsentResponse1Data.StatusEnum.DELETED);
		String consentId = "11113";
		String cId = "11114";
		List<String> versionList = new ArrayList<>();
		versionList.add("3.1");
		when(compatibleVersionList.fetchVersionList()).thenReturn(versionList);
		when(fundsConfirmationConsentRepository.findByConsentIdAndCmaVersionIn(anyString(), any()))
		.thenReturn(data1);
		adapter.removeFundsConfirmationConsent(consentId, cId);
	}
	
	@Test(expected = PSD2Exception.class)
	public void testRemoveFundsConfirmationConsentWithRejectedStatus() {
		OBFundsConfirmationConsentResponse1Data data1 = new OBFundsConfirmationConsentResponse1Data();
		data1.setStatus(OBFundsConfirmationConsentResponse1Data.StatusEnum.REJECTED);
		String consentId = "11113";
		String cId = "11114";
		List<String> versionList = new ArrayList<>();
		versionList.add("2.0");
		when(compatibleVersionList.fetchVersionList()).thenReturn(versionList);
		when(fundsConfirmationConsentRepository.findByConsentIdAndCmaVersionIn(anyString(), any()))
		.thenReturn(data1);
		adapter.removeFundsConfirmationConsent(consentId, cId);
	}

	@Test(expected = PSD2Exception.class)
	public void testRemoveFundsConfirmationConsentWithRevokedStatus() {
		OBFundsConfirmationConsentResponse1Data data1 = new OBFundsConfirmationConsentResponse1Data();
		data1.setStatus(OBFundsConfirmationConsentResponse1Data.StatusEnum.REVOKED);
		String consentId = "11113";
		String cId = "11114";
		List<String> versionList = new ArrayList<>();
		versionList.add("2.0");
		when(compatibleVersionList.fetchVersionList()).thenReturn(versionList);
		when(fundsConfirmationConsentRepository.findByConsentIdAndCmaVersionIn(anyString(), any()))
		.thenReturn(data1);
		adapter.removeFundsConfirmationConsent(consentId, cId);
	}
	
	@SuppressWarnings("unchecked")
	@Test(expected = PSD2Exception.class)
	public void testRemoveFundsConfirmationConsentWithDeletedStatusException() {
		OBFundsConfirmationConsentResponse1Data data1 = new OBFundsConfirmationConsentResponse1Data();
		data1.setStatus(OBFundsConfirmationConsentResponse1Data.StatusEnum.DELETED);
		CispConsent cispConsent = null;
		List<String> versionList = new ArrayList<>();
		versionList.add("2.0");
		when(compatibleVersionList.fetchVersionList()).thenReturn(versionList);
		cispConsentAdapter.updateConsentStatus(anyString(), any());
		when(fundsConfirmationConsentRepository.save(any(OBFundsConfirmationConsentResponse1Data.class)))
				.thenThrow(DataAccessResourceFailureException.class);
		adapter.removeFundsConfirmationConsent(null, null);

	}
	
	@Test(expected = PSD2Exception.class)
	public void testRemoveFundsConfirmationConsentWhenStatusIsDeleted() {
		OBFundsConfirmationConsentResponse1Data data1 = new OBFundsConfirmationConsentResponse1Data();
		String cId = "123456";
		data1.setTppCID(cId);
		data1.setStatusUpdateDateTime(DateUtilites.getCurrentDateInISOFormat());
		data1.setStatus(OBFundsConfirmationConsentResponse1Data.StatusEnum.DELETED);
		String consentId = "11113";
		List<String> versionList = new ArrayList<>();
		versionList.add("2.0");
		when(compatibleVersionList.fetchVersionList()).thenReturn(versionList);
		when(fundsConfirmationConsentRepository.findByConsentIdAndCmaVersionIn(any(),any())).thenReturn(data1);
		adapter.removeFundsConfirmationConsent(consentId, cId);
	}
	
	@Test
	public void testUpdateFundsConfirmationConsentResponseWithDataResource() {
		OBFundsConfirmationConsentResponse1 updateFundsConfirmationConsentPOSTResponse = new OBFundsConfirmationConsentResponse1();
		OBFundsConfirmationConsentResponse1Data data = new OBFundsConfirmationConsentResponse1Data();
		data.setTppCID("123456");
		data.setStatusUpdateDateTime(DateUtilites.getCurrentDateInISOFormat());
		data.setStatus(OBFundsConfirmationConsentResponse1Data.StatusEnum.AUTHORISED);
		String consentId = "786";
		List<String> versionList = new ArrayList<>();
		versionList.add("2.0");
		when(compatibleVersionList.fetchVersionList()).thenReturn(versionList);
		when(fundsConfirmationConsentRepository.findByConsentIdAndCmaVersionIn(any(),any())).thenReturn(data);
		try {
			when(fundsConfirmationConsentRepository.save(any(OBFundsConfirmationConsentResponse1Data.class)))
					.thenReturn(data);
		} catch (DataAccessResourceFailureException e) {
			assertEquals("CONNECTION_ERROR", e.getMessage());
		}
		updateFundsConfirmationConsentPOSTResponse.setData(data);
		adapter.updateFundsConfirmationConsentResponse(consentId, OBFundsConfirmationConsentResponse1Data.StatusEnum.AUTHORISED);
	}

	@SuppressWarnings("unchecked") // Done Completely
	@Test(expected = PSD2Exception.class)
	public void testUpdateFundsConfirmationConsentResponseAndException() {
		OBFundsConfirmationConsentResponse1 updateFundsConfirmationConsentPOSTResponse = new OBFundsConfirmationConsentResponse1();
		OBFundsConfirmationConsentResponse1Data data = null;
		String consentId = "786";
		List<String> versionList = new ArrayList<>();
		versionList.add("2.0");
		when(compatibleVersionList.fetchVersionList()).thenReturn(versionList);
		when(fundsConfirmationConsentRepository.findByConsentIdAndCmaVersionIn(any(),any()))
		.thenThrow(DataAccessResourceFailureException.class);
		updateFundsConfirmationConsentPOSTResponse.setData(data);
		adapter.updateFundsConfirmationConsentResponse(consentId, OBFundsConfirmationConsentResponse1Data.StatusEnum.AUTHORISED);
	}


	@SuppressWarnings("unchecked")
	@Test(expected = PSD2Exception.class)
	public void testUpdateFundsConfirmationConsentResponseWithDataResourceException() {
		OBFundsConfirmationConsentResponse1 updateFundsConfirmationConsentPOSTResponse = new OBFundsConfirmationConsentResponse1();
		OBFundsConfirmationConsentResponse1Data data = new OBFundsConfirmationConsentResponse1Data();
		data.setTppCID("123456");
		data.setStatusUpdateDateTime(DateUtilites.getCurrentDateInISOFormat());
		data.setStatus(OBFundsConfirmationConsentResponse1Data.StatusEnum.AUTHORISED);
		String consentId = "786";
		List<String> versionList = new ArrayList<>();
		versionList.add("2.0");
		when(compatibleVersionList.fetchVersionList()).thenReturn(versionList);
		when(fundsConfirmationConsentRepository.findByConsentIdAndCmaVersionIn(any(),any())).thenReturn(data);
		when(fundsConfirmationConsentRepository.save(any(OBFundsConfirmationConsentResponse1Data.class)))
				.thenThrow(DataAccessResourceFailureException.class);
		updateFundsConfirmationConsentPOSTResponse.setData(data);
		adapter.updateFundsConfirmationConsentResponse("dd", OBFundsConfirmationConsentResponse1Data.StatusEnum.AUTHORISED);
	}

	@Test(expected = PSD2Exception.class)
	public void testUpdateFundsConfirmationConsentResponseWhenStatusIsDeleted() {
		OBFundsConfirmationConsentResponse1Data data = new OBFundsConfirmationConsentResponse1Data();
		data.setStatus(OBFundsConfirmationConsentResponse1Data.StatusEnum.DELETED);
		String consentId = "786";
		List<String> versionList = new ArrayList<>();
		versionList.add("2.0");
		when(compatibleVersionList.fetchVersionList()).thenReturn(versionList);
		when(fundsConfirmationConsentRepository.findByConsentIdAndCmaVersionIn(any(),any())).thenReturn(data);
		adapter.updateFundsConfirmationConsentResponse(consentId, OBFundsConfirmationConsentResponse1Data.StatusEnum.AUTHORISED);
	}

	@After
	public void tearDown() throws Exception {
		fundsConfirmationConsentRepository = null;
		cispConsentAdapter = null;
		adapter = null;
	}
	
	
	public static OBFundsConfirmationConsentResponse1Data getfundData(){
	

	 OBFundsConfirmationConsentResponse1Data OBFundsConfirmationConsentResponse1Data=new OBFundsConfirmationConsentResponse1Data();	
	OBFundsConfirmationConsentResponse1Data.setConsentId("123456");
	 OBFundsConfirmationConsentResponse1Data.setCreationDateTime("kjkjn");
	 return OBFundsConfirmationConsentResponse1Data;
  }

}
