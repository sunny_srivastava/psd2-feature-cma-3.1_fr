package com.capgemini.psd2.funds.confirmation.consent.mongo.db.adapter.impl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessResourceFailureException;

import com.capgemini.psd2.cisp.adapter.CispConsentAdapter;
import com.capgemini.psd2.cisp.adapter.FundsConfirmationConsentAdapter;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmationConsentResponse1;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmationConsentResponse1Data;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmationConsentResponse1Data.StatusEnum;
import com.capgemini.psd2.consent.domain.CispConsent;
import com.capgemini.psd2.enums.ConsentStatusEnum;
import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.ErrorMapKeys;
import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.funds.confirmation.consent.mongo.db.adapter.repository.FundsConfirmationConsentRepository;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.product.common.CompatibleVersionList;
import com.capgemini.psd2.utilities.DateUtilites;
public class FundsConfirmationConsentMongoDbAdapterImpl implements FundsConfirmationConsentAdapter {

	private static final Logger LOG = LoggerFactory.getLogger(FundsConfirmationConsentMongoDbAdapterImpl.class); //NOSONAR
	
	@Autowired
	private FundsConfirmationConsentRepository fundsConfirmationConsentRepository;

	@Autowired
	private CispConsentAdapter cispConsentAdapter;
	
	@Autowired
	private CompatibleVersionList compatibleVersionList;
	
	@Autowired
	private RequestHeaderAttributes req;

	@Override
	public OBFundsConfirmationConsentResponse1 createFundsConfirmationConsentPOSTResponse(
			OBFundsConfirmationConsentResponse1Data data1) {
		OBFundsConfirmationConsentResponse1 fundsConfirmationConsentPOSTResponse = new OBFundsConfirmationConsentResponse1();
		OBFundsConfirmationConsentResponse1Data data = null;
		data1.setTenantId(req.getTenantId());
		try {
			data = fundsConfirmationConsentRepository.save(data1);
		} catch (DataAccessResourceFailureException e) {
			LOG.info("DataAccessResourceFailureException occurred in createFundsConfirmationConsentPOSTResponse(): "+e);
		 throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,e.getMessage()));
			
		}

		fundsConfirmationConsentPOSTResponse.setData(data);
		return fundsConfirmationConsentPOSTResponse;
	}

	@Override
	public OBFundsConfirmationConsentResponse1 getFundsConfirmationConsentPOSTResponse(String consentId) {
		OBFundsConfirmationConsentResponse1 fundsConfirmationConsentPOSTResponse = new OBFundsConfirmationConsentResponse1();
		OBFundsConfirmationConsentResponse1Data data = null;
		try {
			data = fundsConfirmationConsentRepository.findByConsentIdAndCmaVersionIn(consentId, compatibleVersionList.fetchVersionList());
		} catch (DataAccessResourceFailureException e) {
			LOG.info("DataAccessResourceFailureException occurred in getFundsConfirmationConsentPOSTResponse(): "+e);
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,e.getMessage()));
		}
		/*
		 * Change for v3.1.8, PSU should not be able to access/see DELETED resources.
		 */
		if (null == data || data.getStatus().equals(StatusEnum.DELETED)) {
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_RESOURCE_NOTFOUND,ErrorMapKeys.RESOURCE_NOT_FOUND));
		}
		
		/* Fix for Sandbox defect #2716 */
		String cid = req.getTppCID();
		if (null != cid && !cid.equals(data.getTppCID())) {
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.INVALID_TPP_USER_ACCESS);
		}
		
		fundsConfirmationConsentPOSTResponse.setData(data);
		return fundsConfirmationConsentPOSTResponse;
	}

	@Override
	public void removeFundsConfirmationConsent(String consentId, String cId) {
		OBFundsConfirmationConsentResponse1Data data = null;
		try {
			data = fundsConfirmationConsentRepository.findByConsentIdAndCmaVersionIn(consentId, compatibleVersionList.fetchVersionList());
		} catch (DataAccessResourceFailureException e) {
			LOG.info("DataAccessResourceFailureException occurred in removeFundsConfirmationConsent() while calling findByConsentIdAndCmaVersionIn(): "+e);
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,e.getMessage()));
		}

		/*
		 * Change for v3.1.8, PSU should not be able to access/see DELETED resources.
		 */
		if (null == data || data.getStatus().equals(StatusEnum.DELETED)) {
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_RESOURCE_NOTFOUND, ErrorMapKeys.RESOURCE_NOT_FOUND));
		}
		
		if (null != cId && !cId.equals(data.getTppCID())) {
		   throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.INVALID_TPP_USER_ACCESS);
		}
		
		data.setStatus(StatusEnum.DELETED);
		data.setStatusUpdateDateTime(DateUtilites.getCurrentDateInISOFormat());
		try {
			revokeConsent(consentId);
			fundsConfirmationConsentRepository.save(data);
		} catch (DataAccessResourceFailureException e) {
			LOG.info("DataAccessResourceFailureException occurred in removeFundsConfirmationConsent(): "+e);
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,e.getMessage()));
		}

	}

	private void revokeConsent(String consentId) {
		// Changing consent status to DELETED irrespective of the status before.
		CispConsent cispConsent = cispConsentAdapter.retrieveConsentByFundsIntentId(consentId);
		if (cispConsent == null)
			return;
		cispConsentAdapter.updateConsentStatus(cispConsent.getConsentId(), ConsentStatusEnum.DELETED);
	}

	@Override
	public OBFundsConfirmationConsentResponse1 updateFundsConfirmationConsentResponse(String consentId,
			StatusEnum statusEnum) {

		OBFundsConfirmationConsentResponse1 updateFundsConfirmationConsentPOSTResponse = new OBFundsConfirmationConsentResponse1();
		OBFundsConfirmationConsentResponse1Data data = null;
		try {
			data = fundsConfirmationConsentRepository.findByConsentIdAndCmaVersionIn(consentId, compatibleVersionList.fetchVersionList());
		} catch (DataAccessResourceFailureException e) {
			LOG.info("DataAccessResourceFailureException occurred in updateFundsConfirmationConsentResponse() while calling findByConsentIdAndCmaVersionIn(): "+e);
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,e.getMessage()));
		}
		
		/*
		 * Change for v3.1.8, PSU should not be able to access/see DELETED resources. 
		 */
		if (null == data || data.getStatus().equals(StatusEnum.DELETED)) {
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_RESOURCE_NOTFOUND, ErrorMapKeys.RESOURCE_NOT_FOUND));
			
		}
		data.setStatusUpdateDateTime(DateUtilites.getCurrentDateInISOFormat());
		data.setStatus(statusEnum);
		try {
			data = fundsConfirmationConsentRepository.save(data);
		} catch (DataAccessResourceFailureException e) {
			LOG.info("DataAccessResourceFailureException occurred in updateFundsConfirmationConsentResponse(): "+e);
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,e.getMessage()));
		}
		updateFundsConfirmationConsentPOSTResponse.setData(data);
		return updateFundsConfirmationConsentPOSTResponse;
	}

}