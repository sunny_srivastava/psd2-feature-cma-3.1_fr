package com.capgemini.psd2.funds.confirmation.consent.service.test.impl;

import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.cisp.adapter.FundsConfirmationConsentAdapter;
import com.capgemini.psd2.cisp.domain.Links;
import com.capgemini.psd2.cisp.domain.Meta;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmationConsent1;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmationConsent1Data;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmationConsent1DataDebtorAccount;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmationConsentResponse1;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmationConsentResponse1Data;
import com.capgemini.psd2.cisp.utilities.CispDateUtility;
import com.capgemini.psd2.cisp.utilities.CommonCardValidations;
import com.capgemini.psd2.funds.confirmation.consent.service.impl.FundsConfirmationConsentServiceImpl;
import com.capgemini.psd2.funds.confirmation.transformer.FundsConfirmationConsentTransformer;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.token.TPPInformation;
import com.capgemini.psd2.token.Token;

public class FundsConfirmationConsentServiceImplTest {

	@Mock
	private FundsConfirmationConsentAdapter fundsConfirmationConsentAdapter;

	@Mock
	private RequestHeaderAttributes reqHeaderAtrributes;

	@Mock
	private CommonCardValidations commonCardValidations;

	@InjectMocks
	private FundsConfirmationConsentServiceImpl fundsConfirmationConsentServiceImpl;

	@Mock
	private CispDateUtility dateUtility;
	
	@Mock
	private FundsConfirmationConsentTransformer transformer;

	@Before
	public void setUp() {

		MockitoAnnotations.initMocks(this);
		TPPInformation tppInfo = new TPPInformation();
		tppInfo.setTppLegalEntityName("entityname");
		tppInfo.setTppRegisteredId("registeredId");
		tppInfo.setTppBlock("false");
		Token token = new Token();
		token.setTppInformation(tppInfo);
		RequestHeaderAttributes headers = new RequestHeaderAttributes();
		headers.setToken(token);
		ReflectionTestUtils.setField(fundsConfirmationConsentServiceImpl, "reqHeaderAtrributes", headers);
	}

	@Test
	public void contextLoads() {
	}
	
	private OBFundsConfirmationConsent1 getFundConfirmConsent()
	{
		TPPInformation tppInfo = new TPPInformation();
		tppInfo.setTppLegalEntityName("entityname");
		tppInfo.setTppRegisteredId("registeredId");
		tppInfo.setTppBlock("false");
		Token token = new Token();
		token.setTppInformation(tppInfo);
		OBFundsConfirmationConsent1 fundsConfirmationConsentPOSTRequest = new OBFundsConfirmationConsent1();
		OBFundsConfirmationConsent1Data data = new OBFundsConfirmationConsent1Data();
		//fundsConfirmationConsentPOSTRequest = commonCardValidations.validateRequestParams(fundsConfirmationConsentPOSTRequest);
		OBFundsConfirmationConsent1DataDebtorAccount debtorAccount = new OBFundsConfirmationConsent1DataDebtorAccount();
		debtorAccount.setIdentification("12345");
		debtorAccount.setSchemeName("abc");
		debtorAccount.setName("pqr");
		data.setDebtorAccount(debtorAccount);
		data.setExpirationDateTime("2020-01-19T00:00:00+05:30");
		 fundsConfirmationConsentPOSTRequest.setData(data);
		 
		 
		 /*OBFundsConfirmationConsentResponse1 obFundsConfirmationConsentResponse1= new OBFundsConfirmationConsentResponse1();
		 OBFundsConfirmationConsentResponse1Data OBFundsConfirmationConsentResponse1Data=new OBFundsConfirmationConsentResponse1Data();
		 obFundsConfirmationConsentResponse1.setData(OBFundsConfirmationConsentResponse1Data);
		 OBFundsConfirmationConsentResponse1Data.setConsentId("jhsg");
		 OBFundsConfirmationConsentResponse1Data.setCreationDateTime("kjkjn");*/
		 return fundsConfirmationConsentPOSTRequest;
	}
	private OBFundsConfirmationConsentResponse1 getFundConfirm()
	{
		TPPInformation tppInfo = new TPPInformation();
		tppInfo.setTppLegalEntityName("entityname");
		tppInfo.setTppRegisteredId("registeredId");
		tppInfo.setTppBlock("false");
		Token token = new Token();
		token.setTppInformation(tppInfo);
		OBFundsConfirmationConsent1 fundsConfirmationConsentPOSTRequest = new OBFundsConfirmationConsent1();
		OBFundsConfirmationConsent1Data data = new OBFundsConfirmationConsent1Data();
		OBFundsConfirmationConsent1DataDebtorAccount debtorAccount = new OBFundsConfirmationConsent1DataDebtorAccount();
		debtorAccount.setIdentification("12345");
		debtorAccount.setSchemeName("abc");
		debtorAccount.setName("pqr");
		data.setDebtorAccount(debtorAccount);
		data.setExpirationDateTime("2020-01-19T00:00:00+05:30");
		 fundsConfirmationConsentPOSTRequest.setData(data);
		 
		 
		 OBFundsConfirmationConsentResponse1 obFundsConfirmationConsentResponse1= new OBFundsConfirmationConsentResponse1();
		 OBFundsConfirmationConsentResponse1Data OBFundsConfirmationConsentResponse1Data=new OBFundsConfirmationConsentResponse1Data();
		 obFundsConfirmationConsentResponse1.setData(OBFundsConfirmationConsentResponse1Data);
		 OBFundsConfirmationConsentResponse1Data.setConsentId("jhsg");
		 OBFundsConfirmationConsentResponse1Data.setCreationDateTime("kjkjn");
		 return obFundsConfirmationConsentResponse1;
	}
	
	@Test
	public void testcreateFundsConfirmationConsent() {
		OBFundsConfirmationConsentResponse1 obj = new OBFundsConfirmationConsentResponse1();
		OBFundsConfirmationConsentResponse1Data data1 = new OBFundsConfirmationConsentResponse1Data();
		obj.setData(data1);
		obj.getData().setConsentId("1234");
		Mockito.when(commonCardValidations.validateRequestParams(anyObject())).thenReturn(getFundConfirmConsent());
		Mockito.when(fundsConfirmationConsentAdapter.createFundsConfirmationConsentPOSTResponse(anyObject())).thenReturn(getFundConfirm());
		when(fundsConfirmationConsentAdapter.createFundsConfirmationConsentPOSTResponse(anyObject())).thenReturn(obj);
		when(reqHeaderAtrributes.getSelfUrl()).thenReturn("localhost");
		fundsConfirmationConsentServiceImpl.createFundsConfirmationConsent(getFundConfirmConsent());
		
	}
	
	@Test
	public void testcreateFundsConfirmationConsent1() {
		OBFundsConfirmationConsentResponse1 obj = new OBFundsConfirmationConsentResponse1();
		obj.setLinks(new Links());
		obj.setMeta(new Meta());
		OBFundsConfirmationConsentResponse1Data data1 = new OBFundsConfirmationConsentResponse1Data();
		obj.setData(data1);
		obj.getData().setConsentId("1234");
		Mockito.when(commonCardValidations.validateRequestParams(anyObject())).thenReturn(getFundConfirmConsent());
		Mockito.when(fundsConfirmationConsentAdapter.createFundsConfirmationConsentPOSTResponse(anyObject())).thenReturn(getFundConfirm());
		when(fundsConfirmationConsentAdapter.createFundsConfirmationConsentPOSTResponse(anyObject())).thenReturn(obj);
		when(reqHeaderAtrributes.getSelfUrl()).thenReturn("localhost");
		fundsConfirmationConsentServiceImpl.createFundsConfirmationConsent(getFundConfirmConsent());
		
	}
	
	@Test
	public void testRemoveFundsConfirmationConsent() {
		doNothing().when(fundsConfirmationConsentAdapter).removeFundsConfirmationConsent(anyString(), anyString());
		fundsConfirmationConsentServiceImpl.removeFundsConfirmationConsent("1234");
	}

	@Test
	public void testGetFundsConfirmationConsent() {
		when(fundsConfirmationConsentAdapter.getFundsConfirmationConsentPOSTResponse(anyString()))
				.thenReturn(new OBFundsConfirmationConsentResponse1());
		when(reqHeaderAtrributes.getSelfUrl()).thenReturn("localhost");
		fundsConfirmationConsentServiceImpl.getFundsConfirmationConsent("1234");
	}
}
