package com.capgemini.psd2.funds.confirmation.consent.test.controller;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.capgemini.psd2.cisp.domain.OBFundsConfirmationConsent1;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmationConsent1Data;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmationConsent1DataDebtorAccount;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmationConsentResponse1;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmationConsentResponse1Data;
import com.capgemini.psd2.cisp.utilities.CommonCardValidations;
import com.capgemini.psd2.funds.confirmation.consent.controller.FundsConfirmationConsentController;
import com.capgemini.psd2.funds.confirmation.consent.service.FundsConfirmationConsentService;

public class FundsConfirmationConsentControllerTest {

	@Mock
	private FundsConfirmationConsentService fundsConfirmationConsentService;

	@Mock
	CommonCardValidations commonCardValidations;

	@InjectMocks
	private FundsConfirmationConsentController controller;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testCreateFundsConfirmationForValidationIBAN() {
		OBFundsConfirmationConsent1 fundsConfirmationPOSTRequest = new OBFundsConfirmationConsent1();
		OBFundsConfirmationConsent1Data data = new OBFundsConfirmationConsent1Data();
		data.setExpirationDateTime("2019-05-02T00:00:00+00:00");
		OBFundsConfirmationConsent1DataDebtorAccount debtorAccount = new OBFundsConfirmationConsent1DataDebtorAccount();
		debtorAccount.setSchemeName("IBAN");
		debtorAccount.setIdentification("FR1420041010050500013M02606");
		data.setDebtorAccount(debtorAccount);
		fundsConfirmationPOSTRequest.setData(data);
		controller.createFundsConfirmationConsent(fundsConfirmationPOSTRequest);
	}

	@Test
	public void testCreateFundsConfirmationForValidationSortCodeAccountNumber() {
		OBFundsConfirmationConsent1 fundsConfirmationPOSTRequest = new OBFundsConfirmationConsent1();
		OBFundsConfirmationConsent1Data data = new OBFundsConfirmationConsent1Data();
		data.setExpirationDateTime("2019-05-02T00:00:00+00:00");
		OBFundsConfirmationConsent1DataDebtorAccount debtorAccount = new OBFundsConfirmationConsent1DataDebtorAccount();
		debtorAccount.setSchemeName("SortCodeAccountNumber");
		debtorAccount.setIdentification("FR1420041010050500013M02606");
		data.setDebtorAccount(debtorAccount);
		fundsConfirmationPOSTRequest.setData(data);
		controller.createFundsConfirmationConsent(fundsConfirmationPOSTRequest);
	}

	@Test
	public void testCreateFundsConfirmationForValidationPAN() {
		OBFundsConfirmationConsent1 fundsConfirmationPOSTRequest = new OBFundsConfirmationConsent1();
		OBFundsConfirmationConsent1Data data = new OBFundsConfirmationConsent1Data();
		data.setExpirationDateTime("2019-05-02T00:00:00+00:00");
		OBFundsConfirmationConsent1DataDebtorAccount debtorAccount = new OBFundsConfirmationConsent1DataDebtorAccount();
		debtorAccount.setSchemeName("PAN");
		debtorAccount.setIdentification("FR1420041010050500013M02606");
		data.setDebtorAccount(debtorAccount);
		fundsConfirmationPOSTRequest.setData(data);
		commonCardValidations.validatePAN(anyString(), any());
		controller.createFundsConfirmationConsent(fundsConfirmationPOSTRequest);
	}

	@Test
	public void testGetFundsConfirmation() {
		OBFundsConfirmationConsentResponse1 response = new OBFundsConfirmationConsentResponse1();
		response.setData(new OBFundsConfirmationConsentResponse1Data());
		response.getData().setConsentId("234");
		when(fundsConfirmationConsentService.getFundsConfirmationConsent(anyString())).thenReturn(response);
		controller.getFundsConfirmationConsent("234");
	}

	@Test
	public void testDeleteFundsConfirmation() {
		doNothing().when(fundsConfirmationConsentService).removeFundsConfirmationConsent(anyString());
		controller.deleteFundsConfirmationConsent("234");
	}
}
