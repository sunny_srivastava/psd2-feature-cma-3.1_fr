package com.capgemini.psd2.funds.confirmation.consent.service.impl;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.capgemini.psd2.cisp.adapter.FundsConfirmationConsentAdapter;
import com.capgemini.psd2.cisp.domain.Links;
import com.capgemini.psd2.cisp.domain.Meta;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmationConsent1;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmationConsentResponse1;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmationConsentResponse1Data;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmationConsentResponse1Data.StatusEnum;
import com.capgemini.psd2.cisp.utilities.CommonCardValidations;
import com.capgemini.psd2.funds.confirmation.consent.service.FundsConfirmationConsentService;
import com.capgemini.psd2.funds.confirmation.transformer.FundsConfirmationConsentTransformer;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.token.TppInformationTokenData;
import com.capgemini.psd2.utilities.DateUtilites;
import com.capgemini.psd2.utilities.JSONUtilities;

@Service
public class FundsConfirmationConsentServiceImpl implements FundsConfirmationConsentService {

	@Autowired
	@Qualifier("fundsConfirmationConsentRoutingAdapter")
	private FundsConfirmationConsentAdapter fundsConfirmationConsentAdapter;

	@Autowired
	private RequestHeaderAttributes reqHeaderAtrributes;

	@Autowired
	private CommonCardValidations commonCardValidations;

	@Autowired
	private FundsConfirmationConsentTransformer transformer;
	
	@Value("${cmaVersion}")
	private String cmaVersion;
	

	@Override
	public OBFundsConfirmationConsentResponse1 createFundsConfirmationConsent(
			OBFundsConfirmationConsent1 fundsConfirmationConsentPOSTRequest) {

		//Custom Validation on Request Parameters and Body
		fundsConfirmationConsentPOSTRequest = commonCardValidations.validateRequestParams(fundsConfirmationConsentPOSTRequest);

		String str = JSONUtilities.getJSONOutPutFromObject(fundsConfirmationConsentPOSTRequest);
		OBFundsConfirmationConsentResponse1 fundsConfirmationConsentPOSTResponse = JSONUtilities
				.getObjectFromJSONString(str, OBFundsConfirmationConsentResponse1.class);

		OBFundsConfirmationConsentResponse1Data data = fundsConfirmationConsentPOSTResponse.getData();
		data.setConsentId(UUID.randomUUID().toString());
		data.setStatus(StatusEnum.AWAITINGAUTHORISATION);
		data.setCreationDateTime(DateUtilites.getCurrentDateInISOFormat());
		data.setStatusUpdateDateTime(DateUtilites.getCurrentDateInISOFormat());
		data.setTppCID(reqHeaderAtrributes.getTppCID());
		data.setTenantId(reqHeaderAtrributes.getTenantId());
		data.setCmaVersion(cmaVersion);
		
		TppInformationTokenData tppInformationTokenData = reqHeaderAtrributes.getToken().getTppInformation();
		data.setTppLegalEntityName(tppInformationTokenData.getTppLegalEntityName());

		fundsConfirmationConsentPOSTResponse = fundsConfirmationConsentAdapter.createFundsConfirmationConsentPOSTResponse(data);

		if (fundsConfirmationConsentPOSTResponse.getLinks() == null) {
			fundsConfirmationConsentPOSTResponse.setLinks(new Links());
		}
		if (fundsConfirmationConsentPOSTResponse.getMeta() == null) {
			fundsConfirmationConsentPOSTResponse.setMeta(new Meta());
		}
			
		fundsConfirmationConsentPOSTResponse.getLinks().setSelf(reqHeaderAtrributes.getSelfUrl() + PSD2Constants.SLASH
				+ fundsConfirmationConsentPOSTResponse.getData().getConsentId());

		//Custom Validation on Response Data
		transformer.fundsConfirmationConsentResponseTransformer(fundsConfirmationConsentPOSTResponse);
		commonCardValidations.validateResponseParams(fundsConfirmationConsentPOSTResponse);
		 
		return fundsConfirmationConsentPOSTResponse;
	}

	@Override
	public OBFundsConfirmationConsentResponse1 getFundsConfirmationConsent(String consentId) {
		//Validate Consent ID 
		commonCardValidations.validateUniqueUUID(consentId);
		OBFundsConfirmationConsentResponse1 fundsConfirmationConsentPOSTResponse = fundsConfirmationConsentAdapter
				.getFundsConfirmationConsentPOSTResponse(consentId);
		if (fundsConfirmationConsentPOSTResponse.getLinks() == null) {
			fundsConfirmationConsentPOSTResponse.setLinks(new Links());
		}
		if (fundsConfirmationConsentPOSTResponse.getMeta() == null) {
			fundsConfirmationConsentPOSTResponse.setMeta(new Meta());
		}
		fundsConfirmationConsentPOSTResponse.getLinks().setSelf(reqHeaderAtrributes.getSelfUrl());
		//Validate Response Parameters
		commonCardValidations.validateResponseParams(fundsConfirmationConsentPOSTResponse);
		
		return fundsConfirmationConsentPOSTResponse;
	}

	@Override
	public void removeFundsConfirmationConsent(String consentId) {
		//Validate Consent ID 
		commonCardValidations.validateUniqueUUID(consentId);
		fundsConfirmationConsentAdapter.removeFundsConfirmationConsent(consentId, reqHeaderAtrributes.getTppCID());
	}

}
