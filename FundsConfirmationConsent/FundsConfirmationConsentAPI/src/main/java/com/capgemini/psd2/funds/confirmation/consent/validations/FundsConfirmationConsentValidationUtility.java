package com.capgemini.psd2.funds.confirmation.consent.validations;

import java.time.OffsetDateTime;

import org.iban4j.IbanUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.cisp.domain.OBFundsConfirmationConsent1DataDebtorAccount;
import com.capgemini.psd2.cisp.utilities.CispDateUtility;
import com.capgemini.psd2.exceptions.ErrorMapKeys;
import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.utilities.DateUtilites;
import com.capgemini.psd2.utilities.NullCheckUtils;
import com.capgemini.psd2.utilities.ValidationUtility;

@Component
public class FundsConfirmationConsentValidationUtility {

	@Autowired
	public static CispDateUtility dateUtility;
	
	public static void validateExpirationDateTime(String expirationDateTime) {

		if (!NullCheckUtils.isNullOrEmpty(expirationDateTime)) {
			
			dateUtility.validateDateTimeInRequest(expirationDateTime);
			expirationDateTime = dateUtility.transformDateTimeInRequest(expirationDateTime);
			OffsetDateTime parsedExpirationDate = OffsetDateTime.parse(expirationDateTime);

			if (null != parsedExpirationDate && !NullCheckUtils.isNullOrEmpty(parsedExpirationDate)) {
         if (DateUtilites.isOffsetDateComparisonPassed(parsedExpirationDate, OffsetDateTime.now()))
					
        	 throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_FIELD_INVALIDDATE,ErrorMapKeys.INVALID_DATE));
				}
		}
	}

	public static void validateConsentId(String consentId, String validConsentIdCharsRegEx) {
		if (NullCheckUtils.isNullOrEmpty(consentId)) {
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_RESOURCE_NOTFOUND,ErrorMapKeys.RES_NOTFOUND));
		}  
		
		ValidationUtility.isValidField(consentId, validConsentIdCharsRegEx, 1, 128);
	}

	public static void validateIBAN(String iban) {

	  if (NullCheckUtils.isNullOrEmpty(iban))
			 throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNSUPPORTED_SCHEME, ErrorMapKeys.INVALID_SCHEME));
			
		try {
			IbanUtil.validate(iban.trim());
		} catch (Exception exception) {
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR, exception.getMessage()));
		}

	}

	/*
	 * Remove below code and reuse code written in payment validation for scheme
	 * and identification if possible
	 */
	 public static void validateIdentification(OBFundsConfirmationConsent1DataDebtorAccount debtorAccount) {
		if (debtorAccount.getSchemeName() != null) {
			if ((debtorAccount.getSchemeName().equals("SortCodeAccountNumber")
					|| debtorAccount.getSchemeName().equals("UK.OBIE.SortCodeAccountNumber"))
					&& debtorAccount.getIdentification() != null
					&& !debtorAccount.getIdentification().matches("^[0-9]{6}[0-9]{8}$")) {
			 throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNSUPPORTED_SCHEME, ErrorMapKeys.INVALID_SCHEME));
			}
			if ((debtorAccount.getSchemeName().equals("IBAN")
					|| debtorAccount.getSchemeName().equals("UK.OBIE.IBAN"))
					&& debtorAccount.getIdentification() != null) {
				validateIBAN(debtorAccount.getIdentification());
			}
		} else {
			 throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNSUPPORTED_SCHEME, ErrorMapKeys.INVALID_SCHEME));
			
		}
	}

}
