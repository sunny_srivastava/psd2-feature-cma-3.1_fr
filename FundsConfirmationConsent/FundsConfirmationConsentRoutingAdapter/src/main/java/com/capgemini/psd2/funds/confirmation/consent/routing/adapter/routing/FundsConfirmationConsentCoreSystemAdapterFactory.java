package com.capgemini.psd2.funds.confirmation.consent.routing.adapter.routing;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.cisp.adapter.FundsConfirmationConsentAdapter;

@Component
public class FundsConfirmationConsentCoreSystemAdapterFactory
		implements ApplicationContextAware, FundsConfirmationConsentAdapterFactory {

	private ApplicationContext applicationContext;

	@Override
	public FundsConfirmationConsentAdapter getAdapterInstance(String adapterName) {
		return (FundsConfirmationConsentAdapter) applicationContext.getBean(adapterName);
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}
}
