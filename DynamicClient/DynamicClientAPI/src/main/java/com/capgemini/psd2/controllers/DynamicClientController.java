/**
 * 
 */
package com.capgemini.psd2.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.psd2.service.DynamicClientService;
import com.capgemini.tpp.registration.model.OBRegistrationProperties1;
import com.capgemini.tpp.registration.model.SSAWrapper;

/**
 * @author nagoswam
 *
 */

@RestController
public class DynamicClientController {

	@Autowired
	DynamicClientService dynamicClientService;

	@RequestMapping(value = "/register", method = RequestMethod.POST, consumes = {"application/jose", "application/jwt"}, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseStatus(HttpStatus.CREATED)
	public OBRegistrationProperties1 clientRegistration(@RequestBody String registerJWT) {
		SSAWrapper ssaWrapper = dynamicClientService.validateRegistrationRequest(registerJWT,null);

		return dynamicClientService.clientRegistration(ssaWrapper,Boolean.FALSE);
	}
	
	@RequestMapping(value = "/register/{ClientId}", method = RequestMethod.PUT, consumes = { "application/jose", "application/jwt", "text/plain"}, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseStatus(HttpStatus.OK)
	public OBRegistrationProperties1 clientRegistrationUpdate(@RequestBody String registerJWT,@PathVariable("ClientId") String clientId) {
		SSAWrapper ssaWrapper = dynamicClientService.validateRegistrationRequest(registerJWT,clientId);

		return dynamicClientService.clientRegistration(ssaWrapper,Boolean.TRUE);
	}
}
