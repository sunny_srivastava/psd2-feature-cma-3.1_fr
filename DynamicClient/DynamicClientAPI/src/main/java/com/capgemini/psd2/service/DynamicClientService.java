package com.capgemini.psd2.service;

import com.capgemini.tpp.registration.model.OBRegistrationProperties1;
import com.capgemini.tpp.registration.model.SSAWrapper;

public interface DynamicClientService {

	public OBRegistrationProperties1 clientRegistration(SSAWrapper ssaWrapper,boolean updateFlag);
	
	public SSAWrapper validateRegistrationRequest(String registerJWT,String clientId) ;
}
