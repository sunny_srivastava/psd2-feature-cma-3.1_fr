/**
 * 
 */
package com.capgemini.psd2.validator;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.text.ParseException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.enums.ClientRegistrationErrorCodeEnum;
import com.capgemini.psd2.exception.ClientRegistrationException;
import com.capgemini.psd2.jwt.JWKSHelper;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.service.PortalService;
import com.capgemini.psd2.utilities.NullCheckUtils;
import com.capgemini.psd2.validator.enums.CertificateTypes;
import com.capgemini.tpp.ssa.model.SSAModel;
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JWSVerifier;
import com.nimbusds.jwt.JWT;
import com.nimbusds.jwt.SignedJWT;

/**
 * @author nagoswam
 *
 */
@Component
public class RegistrationRequestValidator {
	private static final Logger LOGGER = LoggerFactory.getLogger(RegistrationRequestValidator.class);

	@Autowired
	RequestHeaderAttributes requestHeaderAttributes;
	@Autowired
	PortalService portalService;
	
	public void headerValidations() {
		if (NullCheckUtils.isNullOrEmpty(requestHeaderAttributes.getX_ssl_client_cn())
				|| NullCheckUtils.isNullOrEmpty(requestHeaderAttributes.getX_ssl_client_ou())) {
			throw ClientRegistrationException
					.populateDCRException(ClientRegistrationErrorCodeEnum.INVALID_CERTIFICATE);
		}

	}
	
	public void headerSSAValidation(SSAModel ssaModel){
		if(!requestHeaderAttributes.getX_ssl_client_cn().equals(ssaModel.getSoftware_id())){
			throw ClientRegistrationException
			.populateDCRException(ClientRegistrationErrorCodeEnum.SSA_CERTIFICATE_MISMATCH_CN);
		}
		if(!requestHeaderAttributes.getX_ssl_client_ou().equals(ssaModel.getOrg_id())){
			throw ClientRegistrationException
			.populateDCRException(ClientRegistrationErrorCodeEnum.SSA_CERTIFICATE_MISMATCH_OU);
		}
		
	}
	
	public void registrationRequestSignatureValidation(JWT token, String jwksEndpoint,String jwks) {
		SignedJWT signedJWT = (SignedJWT) token;
		JWSVerifier verifier;
		try {
			if (CertificateTypes.NON_OB_EIDAS_CERT.name().equals(requestHeaderAttributes.getChannelId())) {
				verifier = JWKSHelper.getVerifier(jwks, signedJWT);
			} else {
				verifier = JWKSHelper.getVerifier(portalService.getOBPublicKey(jwksEndpoint), signedJWT);
			}
			if (!signedJWT.verify(verifier))
				throw ClientRegistrationException.populateDCRException(ClientRegistrationErrorCodeEnum.INVALID_JWT);
		} catch (InvalidKeySpecException | NoSuchAlgorithmException | JOSEException | ParseException e) {
			LOGGER.error("InvalidKeySpecException or NoSuchAlgorithmException or JOSEException or ParseException in registrationRequestSignatureValidation"+e);
			throw ClientRegistrationException.populateDCRException(e.getMessage(),
					ClientRegistrationErrorCodeEnum.INVALID_JWT);
		}
	}
}
