package com.capgemini.psd2.validator;


import com.capgemini.psd2.config.WellKnownConfig;
import com.capgemini.psd2.constant.ClientRegistarionConstants;
import com.capgemini.psd2.enums.ClientRegistrationErrorCodeEnum;
import com.capgemini.psd2.exception.ClientRegistrationException;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.utilities.ValidationUtility;
import com.capgemini.psd2.validator.enums.ValidationType;
import com.capgemini.tpp.registration.model.OBClientRegistration1;
import com.capgemini.tpp.ssa.model.SSAModel;

public enum SSAValidationStrategy implements ValidationStrategy {

	REDIRECT_URIS_SSA(ValidationType.REDIRECT_URIS_SSA) {
		@Override
		public <S extends SSAModel> void validate(S s) {

			if (!s.getSoftware_redirect_uris().stream().allMatch(x -> x.matches(ClientRegistarionConstants.REDIRECT_URI_REGEX))) {
				throw ClientRegistrationException
						.populateDCRException(ClientRegistrationErrorCodeEnum.INVALID_REDIRECT_URIS);

			}

		}
	},

	SOFTWARE_ID_LENGTH_SSA(ValidationType.SOFTWARE_ID_LENGTH_SSA) {
		@Override
		public <S extends SSAModel> void validate(S s) {

			if (null != s.getSoftware_id() && s.getSoftware_id().length() > 36)
				throw ClientRegistrationException
						.populateDCRException(ClientRegistrationErrorCodeEnum.SOFTWAREID_TOOLONG);
			if(null==s.getSoftware_id())
				throw ClientRegistrationException
						.populateDCRException(ClientRegistrationErrorCodeEnum.SOFTWAREID_MANDATORY);
		}
	},
	SOFTWARE_CLIENT_ID_LENGTH_SSA(ValidationType.SOFTWARE_CLIENT_ID_LENGTH_SSA) {
		@Override
		public <S extends SSAModel> void validate(S s) {
			ValidationUtility.isValidUUID(s.getSoftware_client_id());
			if (null == s.getSoftware_client_id()) {
				throw ClientRegistrationException
						.populateDCRException(ClientRegistrationErrorCodeEnum.SOFTWARE_CLIENTID_MANDATORY);
			}
			else if (s.getSoftware_client_id().length() > 36)
				throw ClientRegistrationException
						.populateDCRException(ClientRegistrationErrorCodeEnum.SOFTWARECLIENTID_TOOLONG);
		}
	},
	SSA_MANADATORY_FILEDS(ValidationType.SSA_MANADATORY_FILEDS) {
		@Override
		public <S extends SSAModel> void validate(S s) {
			if (s.getOrganisation_competent_authority_claims() == null
					|| s.getOrganisation_competent_authority_claims().getAuthorisations().isEmpty())
				throw ClientRegistrationException
						.populateDCRException(ClientRegistrationErrorCodeEnum.ORG_COMPETENT_AUTH_CLAIMS_MISSING);
			if (s.getSoftware_roles() == null || s.getSoftware_roles().isEmpty())
				throw ClientRegistrationException
						.populateDCRException(ClientRegistrationErrorCodeEnum.SOFTWAREROLES_MISSING);
		}
	}, UNSUPPORTED(ValidationType.UNSUPPORTED) {
		@Override
		public <S extends SSAModel> void validate(S s) {
			/*
			 * Unsupported Validation Type
			 */
		}
		
	};

	@Override
	public <T extends OBClientRegistration1, S extends SSAModel, Q extends WellKnownConfig, K extends RequestHeaderAttributes> void validate(
			T t, S s, Q q, K k) {
		throw new UnsupportedOperationException();
	}

	@Override
	public <T extends RequestHeaderAttributes, S extends SSAModel> void validate(T t, S s) {
		throw new UnsupportedOperationException();
	}

	private ValidationType validationType;

	private SSAValidationStrategy(ValidationType validationType) {
		this.validationType = validationType;
	}

	@Override
	public ValidationType getValidationType() {
		return validationType;
	}

}
