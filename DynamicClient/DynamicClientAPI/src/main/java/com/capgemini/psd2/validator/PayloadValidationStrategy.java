package com.capgemini.psd2.validator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.apache.commons.lang.StringUtils;
import org.springframework.util.CollectionUtils;

import com.capgemini.psd2.config.WellKnownConfig;
import com.capgemini.psd2.constant.ClientRegistarionConstants;
import com.capgemini.psd2.enums.ClientRegistrationErrorCodeEnum;
import com.capgemini.psd2.exception.ClientRegistrationException;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.utilities.DCRUtil;
import com.capgemini.psd2.validator.enums.CertificateTypes;
import com.capgemini.psd2.validator.enums.Roles;
import com.capgemini.psd2.validator.enums.ValidationType;
import com.capgemini.tpp.registration.model.OBClientRegistration1;
import com.capgemini.tpp.registration.model.OBClientRegistration1.ResponseTypesEnum;
import com.capgemini.tpp.ssa.model.SSAModel;
/**
 * 
 * @author nagoswam
 *
 */
public enum PayloadValidationStrategy implements ValidationStrategy {

	ISSUER(ValidationType.ISSUER) {
		@Override
		public <T extends OBClientRegistration1, S extends SSAModel, Q extends WellKnownConfig,K extends RequestHeaderAttributes > void validate(T t, S s,
				Q q,K k) {
			if (!t.getIss().equals(s.getSoftware_id())) {
				throw ClientRegistrationException
						.populateDCRException(ClientRegistrationErrorCodeEnum.ISSUER_MISMATCH);
			}
		}
	},
	AUDIENCE(ValidationType.AUDIENCE) {
		@Override
		public <T extends OBClientRegistration1, S extends SSAModel, Q extends WellKnownConfig,K extends RequestHeaderAttributes> void validate(T t, S s,
				Q q,K k) {
			if (!t.getAud().equals(q.getIssuer(k.getTenantId()))) {
				throw ClientRegistrationException.populateDCRException(ClientRegistrationErrorCodeEnum.AUD_MISMATCH);
			}
		}
	},
	REDIRECT_URIS(ValidationType.REDIRECT_URIS) {
		@Override
		public <T extends OBClientRegistration1, S extends SSAModel, Q extends WellKnownConfig, K extends RequestHeaderAttributes> void validate(
				T t, S s, Q q, K k) {
			if (null == s.getSoftware_redirect_uris() || s.getSoftware_redirect_uris().isEmpty()) {
				throw ClientRegistrationException
						.populateDCRException(ClientRegistrationErrorCodeEnum.REDIRECT_URI_IS_MANDATORY);
			} else {
				if (t.getRedirectUris()==null || t.getRedirectUris().isEmpty() ||  !s.getSoftware_redirect_uris().containsAll(t.getRedirectUris())) {
					throw ClientRegistrationException
							.populateDCRException(ClientRegistrationErrorCodeEnum.REDIRECT_URIS_MISMATCH);
				}
				
				if (!s.getSoftware_redirect_uris().stream()
						.allMatch(x -> x.matches(ClientRegistarionConstants.REDIRECT_URI_REGEX))) {
					throw ClientRegistrationException
							.populateDCRException(ClientRegistrationErrorCodeEnum.INVALID_REDIRECT_URIS);
				}
			}
		}
	},
	TOKEN_ENDPOINTS_AUTH_METHODS(ValidationType.TOKEN_ENDPOINTS_AUTH_METHODS) {
		@Override
		public <T extends OBClientRegistration1, S extends SSAModel, Q extends WellKnownConfig,K extends RequestHeaderAttributes> void validate(T t, S s,
				Q q,K k) {
			if (!q.getToken_endpoint_auth_methods_supported().contains(t.getTokenEndpointAuthMethod().toString())) {
				throw ClientRegistrationException
						.populateDCRException(ClientRegistrationErrorCodeEnum.AUTH_METHODS_NOT_SUPPORTED);
			}

		}
	},
	GRANT_TYPES(ValidationType.GRANT_TYPES) {
		@Override
		public <T extends OBClientRegistration1, S extends SSAModel, Q extends WellKnownConfig,K extends RequestHeaderAttributes> void validate(T t, S s,
				Q q,K k) {
			if(CollectionUtils.isEmpty(t.getGrantTypes()) || t.getGrantTypes().contains(null))
			{
				throw ClientRegistrationException
				.populateDCRException(ClientRegistrationErrorCodeEnum.GRANT_TYPES_NOT_SUPPORTED);
			}
			List<String> grantList =q.getGrant_types_supported().stream().filter(x-> t.getGrantTypes().contains(OBClientRegistration1.GrantTypesEnum.fromValue(x))).collect(Collectors.toList());
			if (!q.getGrant_types_supported().containsAll(grantList)) {
				throw ClientRegistrationException
						.populateDCRException(ClientRegistrationErrorCodeEnum.GRANT_TYPES_NOT_SUPPORTED);
			}

		}
	},
	RESPONSE_TYPES(ValidationType.RESPONSE_TYPES) {
		@Override
		public <T extends OBClientRegistration1, S extends SSAModel, Q extends WellKnownConfig,K extends RequestHeaderAttributes> void validate(T t, S s,
				Q q,K k) {
			if (CollectionUtils.isEmpty(t.getResponseTypes())) {
				List<ResponseTypesEnum> responseTypes = new ArrayList<>();
				responseTypes.add(ResponseTypesEnum.CODE_ID_TOKEN);
				t.setResponseTypes(responseTypes);
			}
			List<String> list = q.getResponse_types_supported().stream()
					.filter(x -> (t.getResponseTypes() != null
							&& t.getResponseTypes().contains(OBClientRegistration1.ResponseTypesEnum.fromValue(x))))
					.collect(Collectors.toList());
			if (CollectionUtils.isEmpty(list))
				throw ClientRegistrationException
						.populateDCRException(ClientRegistrationErrorCodeEnum.RESPONSE_TYPES_NOT_SUPPORTED);

		}
	},
	SOFTWARE_ID(ValidationType.SOFTWARE_ID) {
		@Override
		public <T extends OBClientRegistration1, S extends SSAModel, Q extends WellKnownConfig,K extends RequestHeaderAttributes> void validate(T t, S s,
				Q q,K k) {
			if (t.getSoftwareId() != null && !s.getSoftware_id().equals(t.getSoftwareId())) {
				throw ClientRegistrationException
						.populateDCRException(ClientRegistrationErrorCodeEnum.SOFTWARE_ID_MISMATCH);
			}
		}

	},
	SCOPES(ValidationType.SCOPES) {
		@Override
		public <T extends OBClientRegistration1, S extends SSAModel, Q extends WellKnownConfig,K extends RequestHeaderAttributes> void validate(T t, S s,
				Q q,K k) {
			if (t.getScope() != null) {
				String[] scopes = StringUtils.split(t.getScope());
				List<String> scopeList = Arrays.asList(scopes);
				if (CollectionUtils.isEmpty(scopeList) || !q.getScopes().containsAll(scopeList)
						|| !"openid".equals(scopes[0])) {
					throw ClientRegistrationException
							.populateDCRException(ClientRegistrationErrorCodeEnum.SCOPES_NOT_SUPPORTED);
				}
				scopes = Arrays.stream(Roles.values()).map(Enum::name)
						.filter(m -> scopeList.stream().anyMatch(x -> x.equalsIgnoreCase(Roles.valueOf(m).getRole())))
						.toArray(String[]::new);
				List<String> scopeList1 = Arrays.asList(scopes);
				if (scopeList1.isEmpty()) {
					throw ClientRegistrationException
							.populateDCRException(ClientRegistrationErrorCodeEnum.SCOPES_NOT_SUPPORTED);
				}
				if (null != s.getSoftware_roles() && !s.getSoftware_roles().containsAll(scopeList1)){
					
					throw ClientRegistrationException
					.populateDCRException(ClientRegistrationErrorCodeEnum.SCOPES_NOT_SUPPORTED);
				}
				//set the subset of roles between SSA roles and registration request wrapper
				s.setSoftware_roles(new ArrayList<String>(scopeList1));
			}
		}
	},
	APPLICATION_TYPE(ValidationType.APPLICATION_TYPE) {
		@Override
		public <T extends OBClientRegistration1, S extends SSAModel, Q extends WellKnownConfig,K extends RequestHeaderAttributes> void validate(T t, S s,
				Q q,K k) {
			if (null == t.getApplicationType() || !"WEB".equalsIgnoreCase(t.getApplicationType().name())) {
				throw ClientRegistrationException
						.populateDCRException(ClientRegistrationErrorCodeEnum.APP_TYPE_NOT_SUPPORTED);
			}

		}
	},
	ID_TOKEN_SIGNING_ALG(ValidationType.ID_TOKEN_SIGNING_ALG) {
		@Override
		public <T extends OBClientRegistration1, S extends SSAModel, Q extends WellKnownConfig,K extends RequestHeaderAttributes> void validate(T t, S s,
				Q q,K k) {
			if (!q.getId_token_signing_alg_values_supported().contains(t.getIdTokenSignedResponseAlg().name())) {
				throw ClientRegistrationException
						.populateDCRException(ClientRegistrationErrorCodeEnum.ID_TOKEN_ALG_NOT_SUPPORTED);
			}

		}
	},
	REQUEST_OBJECT_SIGNING_ALG(ValidationType.REQUEST_OBJECT_SIGNING_ALG) {
		@Override
		public <T extends OBClientRegistration1, S extends SSAModel, Q extends WellKnownConfig,K extends RequestHeaderAttributes> void validate(T t, S s,
				Q q,K k) {
			if (!q.getRequest_object_signing_alg_values_supported().contains(t.getRequestObjectSigningAlg().name())) {
				throw ClientRegistrationException
						.populateDCRException(ClientRegistrationErrorCodeEnum.REQUEST_OBJECT_ALG_NOT_SUPPORTED);
			}

		}
	},
	TLS_CLIENT_AUTH_DN(ValidationType.TLS_CLIENT_AUTH_DN) {
		@Override
		public <T extends OBClientRegistration1, S extends SSAModel, Q extends WellKnownConfig,K extends RequestHeaderAttributes> void validate(T t, S s,
				Q q, K k) {
			if (CertificateTypes.OB_EXISTING_CERT.name().equals(k.getChannelId())) {
				if ((!k.getX_ssl_client_cn().equals(DCRUtil.extractPattren(t.getTlsClientAuthSubjectDn(), PSD2Constants.CN))
						 || !(Pattern.matches(PSD2Constants.CN+"=[a-zA-Z0-9]*,"+PSD2Constants.OU+"=[a-zA-Z0-9]*,"+PSD2Constants.O+"=.*,"+PSD2Constants.C+"=GB$", t.getTlsClientAuthSubjectDn().replaceAll("\\s+",""))) || !"POST".equals(k.getMethodType()))
						&& !(Pattern.matches(
								"CN=" + s.getOrg_id() + ",OID.2.5.4.97=[A-Z]*-"
										+ s.getOrganisation_competent_authority_claims().getAuthority_id().substring(0,
												3)
										+ "-" + s.getOrganisation_competent_authority_claims().getRegistration_id()
										+ ",O=.*,C=GB$",
								t.getTlsClientAuthSubjectDn().replaceAll("\\s+", "")))) {
					throw ClientRegistrationException
							.populateDCRException(ClientRegistrationErrorCodeEnum.TLS_CLIENT_AUTH_DN);
				}
			} else if (CertificateTypes.OB_EIDAS_CERT.name().equals(k.getChannelId())) {
				if (!(Pattern
						.matches(
								"CN=" + s.getOrg_id() + ",OID.2.5.4.97=[A-Z]*-"
										+ s.getOrganisation_competent_authority_claims().getAuthority_id().substring(0,
												3)
										+ "-" + s.getOrganisation_competent_authority_claims().getRegistration_id()
										+ ",O=.*,C=GB$",
								t.getTlsClientAuthSubjectDn().replaceAll("\\s+", "")))) {
					throw ClientRegistrationException
							.populateDCRException(ClientRegistrationErrorCodeEnum.TLS_CLIENT_AUTH_DN);
				}
			} else if (!k.getX_ssl_client_ncaid()
					.equals(DCRUtil.extractPattren(t.getTlsClientAuthSubjectDn(), PSD2Constants.NCA_ID_IDENTIFIER))) {
				throw ClientRegistrationException
						.populateDCRException(ClientRegistrationErrorCodeEnum.TLS_CLIENT_AUTH_DN);
			}

		}
	},
	UNSUPPORTED(ValidationType.UNSUPPORTED) {
		@Override
		public <T extends OBClientRegistration1, S extends SSAModel, Q extends WellKnownConfig,K extends RequestHeaderAttributes> void validate(T t, S s,
				Q q,K k) {
			/*
			 * Unsupported Validation Type
			 */
		}
	};

	private ValidationType validationType;

	private PayloadValidationStrategy(ValidationType validationType) {
		this.validationType = validationType;
	}

	@Override
	public ValidationType getValidationType() {
		return validationType;
	}

	@Override
	public <T extends RequestHeaderAttributes, S extends SSAModel> void validate(T t, S s) {
		throw new UnsupportedOperationException();
	}
	
	@Override
	public <S extends SSAModel> void validate(S s) {
		throw new UnsupportedOperationException();
	}
}
