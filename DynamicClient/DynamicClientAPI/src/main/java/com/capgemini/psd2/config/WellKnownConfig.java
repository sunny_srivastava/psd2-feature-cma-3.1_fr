package com.capgemini.psd2.config;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Configuration
@ConfigurationProperties(prefix = "wellKnownConfig")
@JsonIgnoreProperties(ignoreUnknown=true)
public class WellKnownConfig {

	private Map<String, String>  issuer = new HashMap<>();
	
	private ArrayList<String> scopes;
	
	private ArrayList<String> response_types_supported;
	
	private ArrayList<String> grant_types_supported;
	
	private ArrayList<String> id_token_signing_alg_values_supported;
	
	private ArrayList<String> token_endpoint_auth_methods_supported;
	
	private ArrayList<String> request_object_signing_alg_values_supported;
	
	private String wellKnowEndpoint;
	
	public String getWellKnowEndpoint() {
		return wellKnowEndpoint;
	}

	public void setWellKnowEndpoint(String wellKnowEndpoint) {
		this.wellKnowEndpoint = wellKnowEndpoint;
	}

	public Map<String,String> getIssuer() {
		return issuer;
	}
	public String getIssuer(String key) {
		return issuer.get(key);
	}

	public ArrayList<String> getScopes() {
		return scopes;
	}

	public void setScopes(ArrayList<String> scopes) {
		this.scopes = scopes;
	}

	public ArrayList<String> getResponse_types_supported() {
		return response_types_supported;
	}

	public void setResponse_types_supported(ArrayList<String> response_types_supported) {
		this.response_types_supported = response_types_supported;
	}

	public ArrayList<String> getGrant_types_supported() {
		return grant_types_supported;
	}

	public void setGrant_types_supported(ArrayList<String> grant_types_supported) {
		this.grant_types_supported = grant_types_supported;
	}

	public ArrayList<String> getId_token_signing_alg_values_supported() {
		return id_token_signing_alg_values_supported;
	}

	public void setId_token_signing_alg_values_supported(ArrayList<String> id_token_signing_alg_values_supported) {
		this.id_token_signing_alg_values_supported = id_token_signing_alg_values_supported;
	}

	public ArrayList<String> getToken_endpoint_auth_methods_supported() {
		return token_endpoint_auth_methods_supported;
	}

	public void setToken_endpoint_auth_methods_supported(ArrayList<String> token_endpoint_auth_methods_supported) {
		this.token_endpoint_auth_methods_supported = token_endpoint_auth_methods_supported;
	}

	public ArrayList<String> getRequest_object_signing_alg_values_supported() {
		return request_object_signing_alg_values_supported;
	}

	public void setRequest_object_signing_alg_values_supported(ArrayList<String> request_object_signing_alg_values_supported) {
		this.request_object_signing_alg_values_supported = request_object_signing_alg_values_supported;
	}
	
}
