package com.capgemini.psd2;

import java.util.EnumSet;
import java.util.HashSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.mongo.MongoDataAutoConfiguration;
import org.springframework.boot.autoconfigure.data.mongo.MongoRepositoriesAutoConfiguration;
import org.springframework.boot.autoconfigure.data.web.SpringDataWebAutoConfiguration;
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.retry.annotation.EnableRetry;

import com.capgemini.psd2.exceptions.OBPSD2ExceptionUtility;
import com.capgemini.psd2.filter.DynamicClientFilter;
import com.capgemini.psd2.jwt.JWSVerifierImpl;
import com.capgemini.psd2.response.validator.ResponseValidator;
import com.capgemini.psd2.utilities.AbstractJWSVerifier;
import com.capgemini.psd2.utilities.DCRUtil;
import com.capgemini.psd2.validator.HeaderValidationContext;
import com.capgemini.psd2.validator.HeaderValidationStrategy;
import com.capgemini.psd2.validator.PayloadValidationContext;
import com.capgemini.psd2.validator.PayloadValidationStrategy;
import com.capgemini.psd2.validator.SSAValidationContext;
import com.capgemini.psd2.validator.SSAValidationStrategy;
import com.capgemini.psd2.validator.impl.PSD2ValidatorImpl;

@SpringBootApplication(exclude = { SpringDataWebAutoConfiguration.class, MongoAutoConfiguration.class,
		MongoDataAutoConfiguration.class, MongoRepositoriesAutoConfiguration.class })
@EnableEurekaClient
@ComponentScan(basePackages = "com.capgemini", excludeFilters = {
		@ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, value = { PSD2ValidatorImpl.class,
				ResponseValidator.class, OBPSD2ExceptionUtility.class }),
		@ComponentScan.Filter(type = FilterType.REGEX, pattern = { "com.capgemini.psd2.mongo.config.*",
				"com.capgemini.psd2.aisp..*" }) })
@EnableRetry
public class DynamicClientApiApplication {
	private static final Logger LOGGER = LoggerFactory.getLogger(DynamicClientApiApplication.class);
	static ConfigurableApplicationContext context = null;

	public static void main(String[] args) {
		try {
			context = SpringApplication.run(DynamicClientApiApplication.class, args);
		} catch (Exception e) {
			LOGGER.info("context" + e);
			if (context != null) {
				SpringApplication.exit(context, () -> 1);
			}
			System.exit(1);
		}
	}

	@Bean
	public DynamicClientFilter clientFilter() {
		return new DynamicClientFilter();
	}

	@Bean
	public PayloadValidationContext payloadValidationContext() {
		return new PayloadValidationContext(new HashSet<>(EnumSet.allOf(PayloadValidationStrategy.class)),
				new HashSet<>(EnumSet.allOf(PayloadValidationStrategy.class)));
	}

	@Bean
	public HeaderValidationContext headerValidationContext() {
		return new HeaderValidationContext(new HashSet<>(EnumSet.allOf(HeaderValidationStrategy.class)));
	}
	
	@Bean(name="SSAValidationContext")
	public SSAValidationContext sSAValidationContext() {
		return new SSAValidationContext(new HashSet<>(EnumSet.allOf(SSAValidationStrategy.class)));
	}


	@Bean
	public AbstractJWSVerifier jwsVerifier() {
		return new JWSVerifierImpl();
	}
}
