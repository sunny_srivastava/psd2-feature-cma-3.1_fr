package com.capgemini.psd2.utilities;

import java.text.ParseException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.enums.ClientRegistrationErrorCodeEnum;
import com.capgemini.psd2.exception.ClientRegistrationException;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.validator.enums.CertificateTypes;
import com.nimbusds.jose.Header;
import com.nimbusds.jwt.JWT;
import com.nimbusds.jwt.JWTParser;

@Component
public class GetCertificateType {

	@Value("${jwt.issuer}")
	private String issuer;
	@Autowired
	private RequestHeaderAttributes attributes;

	public void setCertType(JWT registrationJwt, String ssaIssuer, String ssaToken,List<String> roles) {
		JWT byRefJwt=null;
		try {
			byRefJwt = JWTParser.parse(ssaToken);
		} catch (ParseException e1) {
			throw ClientRegistrationException
			.populateDCRException(ClientRegistrationErrorCodeEnum.INVALID_SSA);
		}
		if (byRefJwt != null) {
			setCertificateType(registrationJwt, ssaIssuer, byRefJwt, roles);
		}else{
			//INVALID_SSA
			throw ClientRegistrationException
			.populateDCRException(ClientRegistrationErrorCodeEnum.INVALID_SSA);
		}
	}


	/**
	 * @param registrationJwt
	 * @param ssaModel
	 * @param byRefJwt
	 * @param roles
	 */
	private void setCertificateType(JWT registrationJwt, String ssaIssuer, JWT byRefJwt, List<String> eIDASroles) {
		Header ssaheader = byRefJwt.getHeader();
		if ("NONE".equalsIgnoreCase(ssaheader.getAlgorithm().getName())
				&& registrationJwt.getHeader().getIncludedParams().contains(PSD2Constants.X5C)
				&& !issuer.equalsIgnoreCase(ssaIssuer) && null!=eIDASroles) {
			// NON_OB_EIDAS_CERT case
			attributes.setChannelId(CertificateTypes.NON_OB_EIDAS_CERT.name());
		} else if (issuer.equalsIgnoreCase(ssaIssuer)) {

			if (null != eIDASroles && !eIDASroles.isEmpty()) {
				// OB SSA+eidas case
				attributes.setChannelId(CertificateTypes.OB_EIDAS_CERT.name());
			} else if (null == eIDASroles) {
				// OB SSA+ existing cert
				attributes.setChannelId(CertificateTypes.OB_EXISTING_CERT.name());
			}

		} else {
			// default case: none of the above
			// throw exception with logging of cert and SSA
			throw ClientRegistrationException
					.populateDCRException(ClientRegistrationErrorCodeEnum.INVALID_CERT_SSA);
		}
	}

}
