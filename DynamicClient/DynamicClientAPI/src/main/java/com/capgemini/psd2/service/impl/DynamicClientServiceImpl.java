/**
 * 
 */
package com.capgemini.psd2.service.impl;

import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.security.spec.InvalidKeySpecException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.naming.NamingException;
import javax.naming.directory.BasicAttributes;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.capgemini.psd2.config.PFClientRegConfig;
import com.capgemini.psd2.constant.ClientRegistarionConstants;
import com.capgemini.psd2.enums.ClientRegistrationErrorCodeEnum;
import com.capgemini.psd2.exception.ClientRegistrationException;
import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.jwt.JWTReader;
import com.capgemini.psd2.jwt.JWTUtil;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.model.OBThirdPartyProviders;
import com.capgemini.psd2.service.DynamicClientService;
import com.capgemini.psd2.service.PingDirectoryService;
import com.capgemini.psd2.service.PortalService;
import com.capgemini.psd2.utilities.DCRUtil;
import com.capgemini.psd2.utilities.GetCertificateType;
import com.capgemini.psd2.utilities.JwtDecoder;
import com.capgemini.psd2.utilities.NullCheckUtils;
import com.capgemini.psd2.utilities.TppStatusCheck;
import com.capgemini.psd2.utilities.dcr.RolesUtil;
import com.capgemini.psd2.validator.HeaderValidationContext;
import com.capgemini.psd2.validator.PayloadValidationContext;
import com.capgemini.psd2.validator.RegistrationRequestValidator;
import com.capgemini.psd2.validator.SSAValidationContext;
import com.capgemini.psd2.validator.enums.CertificateTypes;
import com.capgemini.tpp.dtos.GrantScopes;
import com.capgemini.tpp.registration.model.OBClientRegistration1;
import com.capgemini.tpp.registration.model.OBRegistrationProperties1;
import com.capgemini.tpp.registration.model.OBRegistrationProperties1.TokenEndpointAuthMethodEnum;
import com.capgemini.tpp.registration.model.SSAWrapper;
import com.capgemini.tpp.registration.model.SupportedAlgorithms;
import com.capgemini.tpp.ssa.model.SSAModel;
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.jwk.JWKSet;
import com.nimbusds.jose.util.Base64;
import com.nimbusds.jose.util.X509CertUtils;
import com.nimbusds.jwt.JWT;

/**
 * @author nagoswam
 *
 */
@Service
public class DynamicClientServiceImpl implements DynamicClientService {
	private static final Logger LOGGER = LoggerFactory.getLogger(DynamicClientServiceImpl.class);
	
	@Autowired
	RegistrationRequestValidator registrationRequestValidator;
	
	@Autowired
	PortalService portalService;
	
	@Autowired
	private PingDirectoryService pingDirectoryService;
	
	@Autowired
	RequestHeaderAttributes requestHeaderAttributes;

	@Autowired
	SSAValidationContext ssaValidationContext;
	@Autowired
	PayloadValidationContext payloadValidationContext;
	
	@Autowired
	HeaderValidationContext headerValidationContext;
	
	@Autowired
	private PFClientRegConfig pfClientRegConfig;
	
	@Autowired
	private TppStatusCheck tppStatusCheck;
	
	@Autowired
	private GetCertificateType certUtil;	
	

	@Autowired
	private RolesUtil roleUtil;
	
	@Value("${openbanking.passportingcheck:false}")
	private boolean passportingcheck;
	

	@Value("${app.jwk.prefix}")
	private String jwkPrefix;
	
	@Value("${app.selfsignssa}")
	private boolean selfsignssa;
	
	@Value("${app.legacyUnsupported:false}")
	private boolean legacyUnsupported;
	
	@Override
	public SSAWrapper validateRegistrationRequest(String registerJWT,String clientId){
		
		OBClientRegistration1 obClientRegistration1;
		SSAModel ssaModel;
		String ssaOriginalToken = null;
		SSAModel ssaModelOriginal = null;
		String jwks=null;
		JWT jwt=JWTUtil.checkJWTValidity(registerJWT);

		
		if(clientId != null && clientId.trim().length() > 0){
			
			ssaOriginalToken=fetchSSAForClient(clientId);
			

			
		}
		
		obClientRegistration1=JwtDecoder.decodeTokenIntoRegistrationRequest(jwt);
		dcrSwaggerValidation(obClientRegistration1);
		if(obClientRegistration1!=null && obClientRegistration1.getTlsClientAuthSubjectDn()!=null && !obClientRegistration1.getTlsClientAuthSubjectDn().contains("OID.")) {
			obClientRegistration1.setTlsClientAuthSubjectDn(obClientRegistration1.getTlsClientAuthSubjectDn().replace("2.5.4", "OID.2.5.4"));
		}
		ssaModel=JWTReader.decodeTokentoSSAModel(obClientRegistration1.getSoftwareStatement());
		
		
		X509Certificate cert = X509CertUtils.parse(requestHeaderAttributes.getX_ssl_client_cert());
		

		List<String> eIDASroles=roleUtil.extractRolesFromCertificate(cert);
		certUtil.setCertType(jwt, ssaModel.getIss(),obClientRegistration1.getSoftwareStatement(),eIDASroles);
		headerValidationContext.execute(ssaModel);
		
		if(CertificateTypes.OB_EXISTING_CERT.name().equals(requestHeaderAttributes.getChannelId()) && "POST".equalsIgnoreCase(requestHeaderAttributes.getMethodType()) && legacyUnsupported)
			throw ClientRegistrationException.populateDCRException(ClientRegistrationErrorCodeEnum.LEGACY_CERTS_NOT_SUPPORTED);
		
		if(CertificateTypes.NON_OB_EIDAS_CERT.name().equals(requestHeaderAttributes.getChannelId())) {
			
			if(!selfsignssa){
				throw ClientRegistrationException.populateDCRException(ClientRegistrationErrorCodeEnum.SELF_SIGNED_SSA_NOT_SUPPORTED);	
			}
			ssaValidationContext.execute(ssaModel);
			String generateClientId=ssaModel.getSoftware_client_id().concat(requestHeaderAttributes.getX_ssl_client_ncaid());
			if(generateClientId.length()>36) {
				ssaModel.setSoftware_client_id(generateClientId.substring(generateClientId.length()-36, generateClientId.length()));
			}else {
				ssaModel.setSoftware_client_id(generateClientId);
			}

			List<Base64> listx5c=(List<Base64>)jwt.getHeader().toJSONObject().get(PSD2Constants.X5C);
			JWKSet jwkset=DCRUtil.generateJwk(listx5c.get(0));	
			jwks=jwkset.toString();
			ssaModel.setPublicKey(jwks);
			ssaModel.setSoftware_jwks_endpoint(jwkPrefix+ssaModel.getOrg_id()+PSD2Constants.SLASH+ssaModel.getSoftware_client_id()+".jwks?tenantId="+requestHeaderAttributes.getTenantId());
		}
		registrationRequestValidator.registrationRequestSignatureValidation(jwt,ssaModel.getSoftware_jwks_endpoint(),jwks);
		if(!CertificateTypes.NON_OB_EIDAS_CERT.name().equals(requestHeaderAttributes.getChannelId())){
			verifyOBSSA(obClientRegistration1.getSoftwareStatement());
		}
		headerValidations();

		
		
		if(ssaOriginalToken != null && ssaOriginalToken.trim().length() > 0){
			ssaModelOriginal = updateClientFlow(obClientRegistration1, ssaOriginalToken);
			if((!CertificateTypes.NON_OB_EIDAS_CERT.name().equals(requestHeaderAttributes.getChannelId()) && portalService.verifySubjectDN(obClientRegistration1,ssaModel.getOrg_jwks_endpoint())==null)) {
				throw ClientRegistrationException.populateDCRException(ClientRegistrationErrorCodeEnum.TLS_CLIENT_AUTH_DN);
			}
		}	
		ssaModel.setClientCertSubjectDn(obClientRegistration1.getTlsClientAuthSubjectDn());
		
		payloadValidationContext.execute(obClientRegistration1, ssaModel,ssaModelOriginal != null);
		if (passportingcheck && !CertificateTypes.NON_OB_EIDAS_CERT.name().equals(requestHeaderAttributes.getChannelId())) {
			OBThirdPartyProviders tppDetails=tppStatusCheck.isTppActive(ssaModel.getOrg_id());
			List<String> finalRoles=tppStatusCheck.passportingCheck(tppDetails, ssaModel.getSoftware_roles(), eIDASroles,
					ssaModel.getOrganisation_competent_authority_claims().getAuthorisations());
			ssaModel.setSoftware_roles((ArrayList<String>) finalRoles);
		}
		
		if (CertificateTypes.NON_OB_EIDAS_CERT.name().equals(requestHeaderAttributes.getChannelId()) && !eIDASroles.containsAll(ssaModel.getSoftware_roles())) {
				throw ClientRegistrationException.populateDCRException(ClientRegistrationErrorCodeEnum.SCOPES_NOT_SUPPORTED);
		}
		SSAWrapper ssaWrapper=new SSAWrapper();
		ssaModel.setId_token_signing_alg(obClientRegistration1.getIdTokenSignedResponseAlg().getValue());

		ssaWrapper.setSsaModel(ssaModel);
		ssaWrapper.setSsaToken(obClientRegistration1.getSoftwareStatement());
		ssaWrapper.setObClientRegistration1(obClientRegistration1);
		return ssaWrapper;
	}


	/**
	 * @param registrationRequest
	 */
	private void verifyOBSSA(String ssaToken) {
		try {
			portalService.verifyToken(ssaToken);
		} catch (InvalidKeySpecException | NoSuchAlgorithmException | ParseException | JOSEException e) {
			LOGGER.error("InvalidKeySpecException or NoSuchAlgorithmException or ParseException or JOSEException in verifyOBSSA"+e);
			throw ClientRegistrationException.populateDCRException(e.getMessage(), ClientRegistrationErrorCodeEnum.SSA_SIGNATURE_FAILED);
		}
	}


	/**
	 * @param registrationRequest
	 * @param ssaOriginalToken
	 * @return
	 */
	private SSAModel updateClientFlow(OBClientRegistration1 obClientRegistration1, String ssaOriginalToken) {
		SSAModel ssaModelOriginal;
		ssaModelOriginal = JWTReader.decodeTokentoSSAModel(ssaOriginalToken);
		SSAModel ssaCurrentModel=JWTReader.decodeTokentoSSAModel(obClientRegistration1.getSoftwareStatement());
		boolean matchResult = ssaCurrentModel.equals(ssaModelOriginal);
		if(!matchResult){
			
			throw ClientRegistrationException.populateDCRException(ClientRegistrationErrorCodeEnum.CLIENT_UPDATE_OP_FAILED);				
		}
		return ssaModelOriginal;
	}

	
	@Override
	public OBRegistrationProperties1 clientRegistration(SSAWrapper ssaWrap,boolean updateFlag) {
		OBRegistrationProperties1 obRegistrationProperties1=new OBRegistrationProperties1();
		try {
			setgrantTypesFromRequest(ssaWrap);
			if(updateFlag){
				portalService.updateTppApplication(ssaWrap.getSsaModel(),ssaWrap.getSsaToken());
			}
			else {
				portalService.createTppApplication(ssaWrap.getSsaModel(), ssaWrap.getSsaModel().getOrg_id(), ssaWrap.getSsaToken());				
			}
			obRegistrationProperties1.setApplicationType(OBRegistrationProperties1.ApplicationTypeEnum.valueOf(ssaWrap.getObClientRegistration1().getApplicationType().name()));
			List<OBRegistrationProperties1.GrantTypesEnum> list = new ArrayList<>();
			ssaWrap.getObClientRegistration1().getGrantTypes().forEach(g -> list.add(OBRegistrationProperties1.GrantTypesEnum.valueOf(g.name())));
			obRegistrationProperties1.setGrantTypes(list);
			
			
			obRegistrationProperties1.setIdTokenSignedResponseAlg(SupportedAlgorithms.valueOf(ssaWrap.getObClientRegistration1().getIdTokenSignedResponseAlg().name()));
			obRegistrationProperties1.setRedirectUris(ssaWrap.getObClientRegistration1().getRedirectUris());
			obRegistrationProperties1.setScope(ssaWrap.getObClientRegistration1().getScope());
			obRegistrationProperties1.setSoftwareStatement(ssaWrap.getObClientRegistration1().getSoftwareStatement());
			obRegistrationProperties1.setSoftwareId(ssaWrap.getObClientRegistration1().getSoftwareId());
			obRegistrationProperties1.setRequestObjectSigningAlg(SupportedAlgorithms.valueOf(ssaWrap.getObClientRegistration1().getRequestObjectSigningAlg().name()));
			obRegistrationProperties1.setTokenEndpointAuthMethod(TokenEndpointAuthMethodEnum.valueOf((ssaWrap.getObClientRegistration1().getTokenEndpointAuthMethod().name())));
			obRegistrationProperties1.setClientId(ssaWrap.getSsaModel().getSoftware_client_id());
			List<OBRegistrationProperties1.ResponseTypesEnum> responseTypes = new ArrayList<>();
			ssaWrap.getObClientRegistration1().getResponseTypes().forEach(obj->responseTypes.add(OBRegistrationProperties1.ResponseTypesEnum.valueOf(obj.name())));
			obRegistrationProperties1.setResponseTypes(responseTypes);
			obRegistrationProperties1.setTlsClientAuthSubjectDn(ssaWrap.getObClientRegistration1().getTlsClientAuthSubjectDn());
		} catch (Exception e) {
			LOGGER.error("Exception in clientRegistration"+e);
			throw ClientRegistrationException.populateDCRException(e.getMessage(), ClientRegistrationErrorCodeEnum.UNAPPROVED_SSA_ERROR);
		}
		
		return obRegistrationProperties1;
	}
	
	private void headerValidations() {
		if (CertificateTypes.OB_EXISTING_CERT.name().equals(requestHeaderAttributes.getChannelId())
				&& (NullCheckUtils.isNullOrEmpty(requestHeaderAttributes.getX_ssl_client_cn())
						|| NullCheckUtils.isNullOrEmpty(requestHeaderAttributes.getX_ssl_client_ou()))) {
			throw ClientRegistrationException
					.populateDCRException(ClientRegistrationErrorCodeEnum.INVALID_CERTIFICATE);
		}
	}
	
	public String fetchSSAForClient(String clientId){
		List<Object> listObj = pingDirectoryService.fetchTppAppMappingForClient(clientId);
		BasicAttributes object = ((BasicAttributes) listObj.get(0));
		try {
			return getAttributeValue(object, "ssaToken");
		} catch (NamingException e) {
			LOGGER.error("NamingException in fetchSSAForClient"+e);
			throw PSD2Exception.populatePSD2Exception(e.getMessage(), ErrorCodeEnum.NO_CLIENT_APP_DATA_FOUND);
		}		
	}
	
	
	private String getAttributeValue(BasicAttributes tppApplication, String ldapAttr) throws NamingException {
		String attributeValue = null;
		if (tppApplication.get(ldapAttr) != null && tppApplication.get(ldapAttr).get() != null) {
			attributeValue = tppApplication.get(ldapAttr).get().toString();
		}
		return attributeValue;
	}	
	private void setgrantTypesFromRequest(SSAWrapper ssaWrap){
		List<String> roles = ssaWrap.getSsaModel().getSoftware_roles();
		Map<String, GrantScopes> map=pfClientRegConfig.getGrantsandscopes();
		for (String role : roles) {
			List<String> getGranttypes=map.get(role).getGranttypes();
			getGranttypes.clear();
			List<String> grantTypes=new ArrayList<>();
			ssaWrap.getObClientRegistration1().getGrantTypes().forEach(obj ->grantTypes.add(OBRegistrationProperties1.GrantTypesEnum.valueOf(obj.name()).toString()));

			getGranttypes.addAll(grantTypes.stream().map(String::toUpperCase).collect(Collectors.toList()));
			getGranttypes.add(ClientRegistarionConstants.IMPLICIT_GRANT_SCOPE);
		}
	}
	private <T> void dcrSwaggerValidation(T validateObject) {
		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		Validator validator = factory.getValidator();
		Set<ConstraintViolation<T>> violations = validator.validate((T)validateObject);
		if (!violations.isEmpty()) {
          for (ConstraintViolation<T> violation : violations) {
              LOGGER.error(violation.getPropertyPath()+":"+violation.getInvalidValue()+"-"+violation.getMessage()); 
          }
          
			throw ClientRegistrationException
			.populateDCRException(ClientRegistrationErrorCodeEnum.MANDATORY_REQUEST_PARAMS_MISSING);
		}
	}

}
