package com.capgemini.psd2.utilities;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.config.OBConfig;
import com.capgemini.psd2.enums.ClientRegistrationErrorCodeEnum;
import com.capgemini.psd2.exception.ClientRegistrationException;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.model.OBThirdPartyProviders;
import com.capgemini.psd2.service.ITppDetailsService;
import com.capgemini.psd2.service.PassPortingCheck;
import com.capgemini.psd2.utilities.dcr.RolesUtil;

@Component
public class TppStatusCheck {
	private static final Logger LOGGER = LoggerFactory.getLogger(TppStatusCheck.class);

	@Autowired
	ITppDetailsService iTppDetailsService;
	@Autowired
	RequestHeaderAttributes requestHeader;
	@Value("${openbanking.scim.attributes}")
	private String attributes;
	@Autowired
	PassPortingCheck passPortingCheck;
	@Autowired
	OBConfig config;
	@Autowired
	RolesUtil rolesUtil;

	public OBThirdPartyProviders isTppActive(String tppid) {
		LOGGER.info("{\"Enter\":\"{}()\",\"{}\"}", "isTppActive", tppid);
		OBThirdPartyProviders oBThirdPartyProviders = iTppDetailsService.checkTppStatus(tppid, attributes);
		if (null != oBThirdPartyProviders && null != oBThirdPartyProviders.getUrnopenbankingorganisation10()&& "Active".equalsIgnoreCase(oBThirdPartyProviders.getUrnopenbankingorganisation10().getStatus()) 
				 ) {
			LOGGER.info("{\"Exit\":\"{}()\"}", "isTppActive");
			return oBThirdPartyProviders;
		} else
			throw ClientRegistrationException.populateDCRException(ClientRegistrationErrorCodeEnum.TPP_INACTIVE);
	}

	public List<String> passportingCheck(OBThirdPartyProviders oBThirdPartyProviders, List<String> ssaRoles,
			List<String> eIDASRoles, List<com.capgemini.tpp.ssa.model.Authorisation> ssaOrgRoles) {
		LOGGER.info("{\"Enter\":\"{}()\"}", "passportingCheck");
		String memberState=config.getPassporting().get(requestHeader.getTenantId()).get(0).getMember_state();
		List<String> obRole=passPortingCheck.roleListFromOB(oBThirdPartyProviders,
				memberState);
		List<ArrayList<String>> orgRoles=ssaOrgRoles.stream().filter(value-> value.getMember_state().equals(memberState)).map(value->value.getRoles()).collect(Collectors.toList());
		List<String> subSetRoles=rolesUtil.extract(ssaRoles, eIDASRoles, orgRoles.get(0), obRole);
		if (subSetRoles.isEmpty()) {
			throw ClientRegistrationException
					.populateDCRException(ClientRegistrationErrorCodeEnum.PASSPORTING_CHECK_FAILED);
		}
		LOGGER.info("{\"Exit\":\"{}()\",\"{}\"}", "passportingCheck", subSetRoles);
		return subSetRoles;
	}
}
