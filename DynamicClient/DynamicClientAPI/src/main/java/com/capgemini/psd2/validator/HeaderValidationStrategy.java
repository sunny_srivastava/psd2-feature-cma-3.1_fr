package com.capgemini.psd2.validator;

import com.capgemini.psd2.config.WellKnownConfig;
import com.capgemini.psd2.enums.ClientRegistrationErrorCodeEnum;
import com.capgemini.psd2.exception.ClientRegistrationException;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.validator.enums.CertificateTypes;
import com.capgemini.psd2.validator.enums.ValidationType;
import com.capgemini.tpp.registration.model.OBClientRegistration1;
import com.capgemini.tpp.ssa.model.SSAModel;

/**
 * 
 * @author nagoswam
 *
 */
public enum HeaderValidationStrategy implements ValidationStrategy {

	SOFTWARE_ID_CN(ValidationType.SOFTWARE_ID_CN) {
		@Override
		public <T extends RequestHeaderAttributes, S extends SSAModel> void validate(T t, S s) {
			if (CertificateTypes.OB_EXISTING_CERT.name().equals(t.getChannelId())
					&& !t.getX_ssl_client_cn().equals(s.getSoftware_id())) {
				throw ClientRegistrationException
						.populateDCRException(ClientRegistrationErrorCodeEnum.SSA_CERTIFICATE_MISMATCH_CN);
			}
		}
	},
	ORG_ID_OU(ValidationType.ORG_ID_OU) {
		@Override
		public <T extends RequestHeaderAttributes, S extends SSAModel> void validate(T t, S s) {
			if (CertificateTypes.OB_EXISTING_CERT.name().equals(t.getChannelId())
					&& !t.getX_ssl_client_ou().equals(s.getOrg_id())) {
				throw ClientRegistrationException
						.populateDCRException(ClientRegistrationErrorCodeEnum.SSA_CERTIFICATE_MISMATCH_OU);
			}
		}
	},
	UNSUPPORTED(ValidationType.UNSUPPORTED) {
		@Override
		public <T extends RequestHeaderAttributes, S extends SSAModel> void validate(T t, S s) {
			/*
			 * Unsupported Validation Type
			 */
		}
	};

	private ValidationType validationType;

	private HeaderValidationStrategy(ValidationType validationType) {
		this.validationType = validationType;
	}

	@Override
	public ValidationType getValidationType() {
		return validationType;
	}

	@Override
	public <T extends OBClientRegistration1, S extends SSAModel, Q extends WellKnownConfig,K extends RequestHeaderAttributes> void validate(T t, S s, Q q,K k) {
		throw new UnsupportedOperationException();
	}
	
	@Override
	public <S extends SSAModel> void validate(S s) {
		throw new UnsupportedOperationException();
	}
}
