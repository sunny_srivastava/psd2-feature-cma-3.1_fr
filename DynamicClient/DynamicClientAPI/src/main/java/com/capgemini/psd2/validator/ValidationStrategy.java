package com.capgemini.psd2.validator;

import com.capgemini.psd2.config.WellKnownConfig;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.validator.enums.ValidationType;
import com.capgemini.tpp.registration.model.OBClientRegistration1;
import com.capgemini.tpp.ssa.model.SSAModel;
/**
 * 
 * @author nagoswam
 *
 */
public interface ValidationStrategy {

	<T extends OBClientRegistration1, S extends SSAModel, Q extends WellKnownConfig,K extends RequestHeaderAttributes> void validate(T t, S s, Q q,K k);

	<T extends RequestHeaderAttributes,S extends SSAModel> void validate(T t,S s);
	
	<S extends SSAModel> void validate(S s);
	
	ValidationType getValidationType();
}
