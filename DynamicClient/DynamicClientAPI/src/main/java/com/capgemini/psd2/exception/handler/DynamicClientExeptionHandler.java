package com.capgemini.psd2.exception.handler;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.capgemini.psd2.dcr.domain.RegistrationError;
import com.capgemini.psd2.enums.ClientRegistrationErrorCodeEnum;
import com.capgemini.psd2.exception.ClientRegistrationException;

import com.capgemini.psd2.exceptions.PSD2Exception;

@ControllerAdvice
public class DynamicClientExeptionHandler extends ResponseEntityExceptionHandler{
	
	 /**
	 * Handle invalid request.
	 *
	 * @param e the e
	 * @param request the request
	 * @return the response entity
	 */
	@ExceptionHandler({Exception.class })
	protected ResponseEntity<Object> handleInvalidRequest(Exception e, WebRequest request) {
		RegistrationError error;
		RegistrationError registrationError;
		HttpStatus status;
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		if (e != null && e instanceof ClientRegistrationException) {
		error = ((PSD2Exception) e).getRegistrationError();
			registrationError=new RegistrationError();
			registrationError.setCode(error.getCode());
			registrationError.setError(error.getError());
			registrationError.setErrorDescription(ClientRegistrationErrorCodeEnum.fromString(error.getCode()).getDetailErrorMessage());
			registrationError.setStatusCode(error.getStatusCode());
		
				status = HttpStatus.valueOf(Integer.parseInt(error.getStatusCode()));
		} else {
			ClientRegistrationErrorCodeEnum info = ClientRegistrationErrorCodeEnum.UNAPPROVED_SSA_ERROR;
		
			registrationError=new RegistrationError();
			registrationError.setCode(info.getErrorCode());
			registrationError.setError(RegistrationError.ErrorEnum.UNAPPROVED_SOFTWARE_STATEMENT);
			registrationError.setErrorDescription(info.getDetailErrorMessage());
			status = HttpStatus.valueOf(Integer.parseInt(info.getStatusCode()));
		}
		return handleExceptionInternal(e, registrationError, headers, status, request);
	}

}
