package com.capgemini.psd2.validator;

import java.util.Iterator;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.tpp.ssa.model.SSAModel;
@Component
public class SSAValidationContext {
	private static final Logger LOGGER = LoggerFactory.getLogger(SSAValidationContext.class);
	
	@Autowired
	RequestHeaderAttributes headerAttributes;
	
	private Set<ValidationStrategy> sSAValidationStrategies;
	

	public SSAValidationContext(Set<ValidationStrategy> ssaValidationStrategies) {
		this.sSAValidationStrategies = ssaValidationStrategies;
	}

	
	/* * This method performs validation for fields one by one and return the
	 * invalid one if found. Otherwise, it will continue validating remaining
	 * fields. If all the fields are valid then will return null.*/
	 
	public void execute(SSAModel ssaModel) {
		LOGGER.info("Enter SSA validations");
		ValidationStrategy headerValidation;
		LOGGER.info("Exit headerValidation");
		for (Iterator<ValidationStrategy> iterator = sSAValidationStrategies.iterator(); iterator.hasNext();) {
			LOGGER.info("Enter in for loop");
			headerValidation = iterator.next();
			headerValidation.validate(ssaModel);
			LOGGER.info("Exit validate",headerValidation);
		}
		LOGGER.info("Exit SSA validations successfully");
	}

}
