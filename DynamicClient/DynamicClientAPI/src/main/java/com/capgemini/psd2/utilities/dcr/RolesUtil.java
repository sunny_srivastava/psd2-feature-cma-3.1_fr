package com.capgemini.psd2.utilities.dcr;

import java.io.IOException;
import java.security.cert.X509Certificate;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.enums.ClientRegistrationErrorCodeEnum;
import com.capgemini.psd2.exception.ClientRegistrationException;
import com.capgemini.psd2.utilities.EidasCertificateValidator;

@Component
public final class RolesUtil {
	private static final Logger LOGGER = LoggerFactory.getLogger(RolesUtil.class);
	public List<String> extract(List<String> ssaRoles, List<String> eIDASRoles, List<String> ssaOrgRoles,
			List<String> passportingRoles) {
		return ssaRoles.stream()
				.filter(value -> eIDASRoles == null || eIDASRoles.contains(value))
				.filter(ssaOrgRoles::contains)
				.filter(passportingRoles::contains)
				.collect(Collectors.toList());
	}

	/**
	 * @param cert
	 * @param roles
	 * @return
	  */
	public List<String> extractRolesFromCertificate(X509Certificate cert) {
		List<String> roles=null;
		try {
			roles = EidasCertificateValidator.eIDASRoles(cert);
		} catch (IOException e) {
			LOGGER.error("IOException" + e);
			throw ClientRegistrationException.populateDCRException(e.getMessage(),
					ClientRegistrationErrorCodeEnum.INVALID_EIDAS_CERTIFICATE);
		}
		return roles;
	}
}
	
