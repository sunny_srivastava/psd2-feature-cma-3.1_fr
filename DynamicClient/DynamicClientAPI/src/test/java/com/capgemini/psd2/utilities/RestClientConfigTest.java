package com.capgemini.psd2.utilities;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import com.capgemini.psd2.config.RestClientConfig;
import com.capgemini.psd2.rest.client.exception.handler.ExceptionHandler;
import com.capgemini.psd2.rest.client.sync.RestClientSync;

@RunWith(SpringJUnit4ClassRunner.class)
public class RestClientConfigTest {

	@InjectMocks
	private RestClientConfig restClientConfig;

	@Before 
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this); 
	}

	@Test
	public void restClientSyncImplTest() {
		Assert.isInstanceOf(RestClientSync.class, restClientConfig.restClientSyncImpl());
	}

	@Test
	public void exceptionHandlerImplTest() {
		Assert.isInstanceOf(ExceptionHandler.class, restClientConfig.exceptionHandlImpl());
	}

}
