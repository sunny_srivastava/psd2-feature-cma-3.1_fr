package com.capgemini.psd2.service.impl;


import static org.hamcrest.CoreMatchers.any;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.config.OBConfig;
import com.capgemini.psd2.exception.ClientRegistrationException;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.mock.data.DynamicClientMockData;
import com.capgemini.psd2.model.Authorisation;
import com.capgemini.psd2.model.OBThirdPartyProviders;
import com.capgemini.psd2.model.OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10;
import com.capgemini.psd2.model.OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations;
import com.capgemini.psd2.model.Organisation;
import com.capgemini.psd2.service.ITppDetailsService;
import com.capgemini.psd2.service.PassPortingCheck;
import com.capgemini.psd2.utilities.TppStatusCheck;
import com.capgemini.psd2.utilities.dcr.RolesUtil;

@RunWith(SpringJUnit4ClassRunner.class)
public class TppStatusCheckTest {

	@Mock
	ITppDetailsService iTppDetailsService;
	
	@Mock
	RequestHeaderAttributes requestHeader;
	
	@InjectMocks
	TppStatusCheck tppStatusCheck;
	
	@Mock
	PassPortingCheck passPortingCheck;
	
	@Mock
	private OBConfig obConfig;
	
	@Mock
	RolesUtil rolesUtil;
	

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		
	}
	
	@Test
	public void testIsTppActive() {
		OBThirdPartyProviders ob=new OBThirdPartyProviders();
		Organisation urnopenbankingorganisation10 = new Organisation();
		ob.setId("123456");
		urnopenbankingorganisation10.setStatus("Active");
		ob.setUrnopenbankingorganisation10(urnopenbankingorganisation10);
		Mockito.when(iTppDetailsService.checkTppStatus("123456", null)).thenReturn(ob);
		tppStatusCheck.isTppActive("123456");
	}
	
	@Test(expected=ClientRegistrationException.class)
	public void testIsTppNotActive() {
		OBThirdPartyProviders ob=new OBThirdPartyProviders();
		Organisation urnopenbankingorganisation10 = new Organisation();
		ob.setId("123456");
		urnopenbankingorganisation10.setStatus("InActive");
		ob.setUrnopenbankingorganisation10(urnopenbankingorganisation10);
		Mockito.when(iTppDetailsService.checkTppStatus("123456", null)).thenReturn(ob);
		tppStatusCheck.isTppActive("123456");
	}
	@Test(expected=ClientRegistrationException.class)
	public void testIsOBDetailsErrorElse1() {
		Mockito.when(iTppDetailsService.checkTppStatus("123456", null)).thenReturn(null);
		tppStatusCheck.isTppActive("123456");
	}
	
	@Test(expected=ClientRegistrationException.class)
	public void testIsOBDetailsErrorElse() {
		OBThirdPartyProviders ob=new OBThirdPartyProviders();
		ob.setUrnopenbankingorganisation10(null);
		Mockito.when(iTppDetailsService.checkTppStatus("123456", null)).thenReturn(ob);
		tppStatusCheck.isTppActive("123456");
	}

/*	@Test(expected = ClientRegistrationException.class)
	public void testIsPassportingCheck() {
		Mockito.when(requestHeader.getTenantId()).thenReturn("BOI");
		String key = "BOI";
		List<Authorisation> authList = new ArrayList<>();
		Authorisation E1 = DynamicClientMockData.getAuthorisation1();
		Authorisation E2 = DynamicClientMockData.getAuthorisation2();
		Authorisation E3 = DynamicClientMockData.getAuthorisation3();
		authList.add(E1);
		authList.add(E2);
		authList.add(E3);

		OBConfig obConfig = new OBConfig();
		obConfig.getPassporting().put("BOI", authList);
		ReflectionTestUtils.setField(tppStatusCheck, "obConfig", obConfig);

		OBThirdPartyProviders oBThirdPartyProviders = new OBThirdPartyProviders();
		OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10 urnopenbankingcompetentauthorityclaims10 = new OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10();
		List<OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations> authorisations1 = new ArrayList<>();
		OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations e1 = DynamicClientMockData
				.getAuthorisationsA1();
		OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations e2 = DynamicClientMockData
				.getAuthorisationsA2();
		OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations e3 = DynamicClientMockData
				.getAuthorisationsA3();
		OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations e4 = DynamicClientMockData
				.getAuthorisationsA4();
		OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations e5 = DynamicClientMockData
				.getAuthorisationsA5();

		authorisations1.add(e1);
		authorisations1.add(e2);
		authorisations1.add(e3);
		authorisations1.add(e4);
		authorisations1.add(e5);

		urnopenbankingcompetentauthorityclaims10.setAuthorisations(authorisations1);

		oBThirdPartyProviders.setUrnopenbankingcompetentauthorityclaims10(urnopenbankingcompetentauthorityclaims10);
		tppStatusCheck.passportingCheck(oBThirdPartyProviders);

	}
*/
	@Test(expected=ClientRegistrationException.class)
	public void passportingTest()
	{
		OBThirdPartyProviders oBThirdPartyProviders=new OBThirdPartyProviders();
		OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10 urnopenbankingcompetentauthorityclaims10 = new OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10();
		List<OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations> authorisations1 = new ArrayList<>();
		OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations e1 = DynamicClientMockData
				.getAuthorisationsA1();
		OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations e2 = DynamicClientMockData
				.getAuthorisationsA2();
		OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations e3 = DynamicClientMockData
				.getAuthorisationsA3();
		OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations e4 = DynamicClientMockData
				.getAuthorisationsA4();
		OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations e5 = DynamicClientMockData
				.getAuthorisationsA5();

		authorisations1.add(e1);
		authorisations1.add(e2);
		authorisations1.add(e3);
		authorisations1.add(e4);
		authorisations1.add(e5);

		urnopenbankingcompetentauthorityclaims10.setAuthorisations(authorisations1);

		oBThirdPartyProviders.setUrnopenbankingcompetentauthorityclaims10(urnopenbankingcompetentauthorityclaims10);

		
		List<String> roles=new ArrayList<>();
		roles.add("PISP");
		String tenantid = "aspsp";
		Mockito.when(iTppDetailsService.checkPassporting(oBThirdPartyProviders, roles, tenantid)).thenReturn(false);
		List<String> eIDASRoles=new ArrayList<>();
		eIDASRoles.add("PISP");
		
		ArrayList<com.capgemini.tpp.ssa.model.Authorisation> ssaOrgRoles=new ArrayList<>();
		com.capgemini.tpp.ssa.model.Authorisation e=new com.capgemini.tpp.ssa.model.Authorisation();
		e.setMember_state("GB");
		ArrayList<String> rolesP=new ArrayList<>();
		rolesP.add("PISP");
		e.setRoles(rolesP);
		ssaOrgRoles.add(e);
		
		List<String> ssaRoles=new ArrayList<>();
		List<String> passportingRoles=new ArrayList<>();
		List<String> ssaOrgRoles1=new ArrayList<>();
		//ssaOrgRoles.add(e);
		List<String> listres=new ArrayList<>();
		listres.add("PISP");
		List<ArrayList<String>> orgRoles=new ArrayList<>();
		
		
		Mockito.when(rolesUtil.extract(ssaRoles, eIDASRoles, ssaOrgRoles1, passportingRoles)).thenReturn(listres);
		
		
		RequestHeaderAttributes requestHeader1=new RequestHeaderAttributes();
		requestHeader1.setTenantId("aspsp");
		ReflectionTestUtils.setField(tppStatusCheck, "requestHeader", requestHeader1);
		Mockito.when(requestHeader.getTenantId()).thenReturn("aspsp");
		List<Authorisation> authList = new ArrayList<>();
		Authorisation E1 = DynamicClientMockData.getAuthorisation1();
		Authorisation E2 = DynamicClientMockData.getAuthorisation2();
		Authorisation E3 = DynamicClientMockData.getAuthorisation3();
		authList.add(E1);
		authList.add(E2);
		authList.add(E3);

		OBConfig obConfig = new OBConfig();
		obConfig.getPassporting().put("aspsp", authList);
		ReflectionTestUtils.setField(tppStatusCheck, "config", obConfig);
		String memberState="GB";
		List<String> reslist=new ArrayList<>();
		reslist.add("PISP");
		Mockito.when(passPortingCheck.roleListFromOB(oBThirdPartyProviders, memberState)).thenReturn(reslist);
	//	Mockito.when(obConfig.getPassporting().get(requestHeader.getTenantId()).get(0).getMember_state()).thenReturn("EUR");
		tppStatusCheck.passportingCheck(oBThirdPartyProviders, roles,eIDASRoles,ssaOrgRoles);
	}
	
	@Test(expected=ClientRegistrationException.class)
	public void passportingTestSuccess()
	{
		OBThirdPartyProviders oBThirdPartyProviders=new OBThirdPartyProviders();
		OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10 urnopenbankingcompetentauthorityclaims10 = new OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10();
		List<OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations> authorisations1 = new ArrayList<>();
		OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations e1 = DynamicClientMockData
				.getAuthorisationsA1();
		OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations e2 = DynamicClientMockData
				.getAuthorisationsA2();
		OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations e3 = DynamicClientMockData
				.getAuthorisationsA3();
		OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations e4 = DynamicClientMockData
				.getAuthorisationsA4();
		OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations e5 = DynamicClientMockData
				.getAuthorisationsA5();

		authorisations1.add(e1);
		authorisations1.add(e2);
		authorisations1.add(e3);
		authorisations1.add(e4);
		authorisations1.add(e5);

		urnopenbankingcompetentauthorityclaims10.setAuthorisations(authorisations1);

		oBThirdPartyProviders.setUrnopenbankingcompetentauthorityclaims10(urnopenbankingcompetentauthorityclaims10);

		
		List<String> roles=new ArrayList<>();
		roles.add("PISP");
		String tenantid = "aspsp";
		Mockito.when(iTppDetailsService.checkPassporting(oBThirdPartyProviders, roles, tenantid)).thenReturn(false);
		List<String> eIDASRoles=new ArrayList<>();
		eIDASRoles.add("PISP");
		ArrayList<com.capgemini.tpp.ssa.model.Authorisation> ssaOrgRoles=new ArrayList<>();
		
		com.capgemini.tpp.ssa.model.Authorisation e=new com.capgemini.tpp.ssa.model.Authorisation();
		e.setMember_state("GB");
		ArrayList<String> rolesP=new ArrayList<>();
		rolesP.add("PISP");
		e.setRoles(rolesP);
		ssaOrgRoles.add(e);
		List<String> ssaRoles=new ArrayList<>();
		List<String> passportingRoles=new ArrayList<>();
		List<String> ssaOrgRoles1=new ArrayList<>();
		ssaOrgRoles.add(e);
		List<String> listres=new ArrayList<>();
		listres.add("PISP");
		List<ArrayList<String>> orgRoles=new ArrayList<>();
		
		
		Mockito.when(rolesUtil.extract(ssaRoles, eIDASRoles, ssaOrgRoles1, passportingRoles)).thenReturn(listres);
	//	doNothing().when(rolesUtil.extract(ssaRoles, eIDASRoles, ssaOrgRoles1, passportingRoles));		
		
		RequestHeaderAttributes requestHeader1=new RequestHeaderAttributes();
		requestHeader1.setTenantId("aspsp");
		ReflectionTestUtils.setField(tppStatusCheck, "requestHeader", requestHeader1);
		Mockito.when(requestHeader.getTenantId()).thenReturn("aspsp");
		List<Authorisation> authList = new ArrayList<>();
		Authorisation E1 = DynamicClientMockData.getAuthorisation1();
		Authorisation E2 = DynamicClientMockData.getAuthorisation2();
		Authorisation E3 = DynamicClientMockData.getAuthorisation3();
		authList.add(E1);
		authList.add(E2);
		authList.add(E3);

		OBConfig obConfig = new OBConfig();
		obConfig.getPassporting().put("aspsp", authList);
		ReflectionTestUtils.setField(tppStatusCheck, "config", obConfig);
		String memberState="GB";
		List<String> reslist=new ArrayList<>();
		reslist.add("PISP");
		Mockito.when(passPortingCheck.roleListFromOB(oBThirdPartyProviders, memberState)).thenReturn(reslist);
	//	Mockito.when(obConfig.getPassporting().get(requestHeader.getTenantId()).get(0).getMember_state()).thenReturn("GBP");
		tppStatusCheck.passportingCheck(oBThirdPartyProviders, roles,eIDASRoles,ssaOrgRoles);
	}
	
	
}
