package com.capgemini.psd2.validator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.config.WellKnownConfig;
import com.capgemini.psd2.exception.ClientRegistrationException;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.tpp.registration.model.OBClientRegistration1;
import com.capgemini.tpp.ssa.model.Authorisation;
import com.capgemini.tpp.ssa.model.OrganisationCompetentAuthorityClaims;
import com.capgemini.tpp.ssa.model.SSAModel;

@RunWith(SpringJUnit4ClassRunner.class)
public class SSAValidatorTest {
	
	
Set<ValidationStrategy> ssaStrategies=null;
	
	OBClientRegistration1 regRequest=null;
	OBClientRegistration1 regRequest1=null;
	
	SSAModel ssaModel=null;
	 
	@InjectMocks
	private SSAValidationContext ssaValidationContext;
	
	@Mock
	private WellKnownConfig wellKnownConfig;
	
	@Mock
	RequestHeaderAttributes requestHeaderAttributes;
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(SSAValidationContext.class);
		ssaStrategies = new LinkedHashSet<ValidationStrategy>(EnumSet.allOf(SSAValidationStrategy.class));
		regRequest = new OBClientRegistration1();
		ssaModel = new SSAModel();
		ssaModel.setIss("OpenBanking Ltd");
		ssaModel.setOrg_id("0015800000jfQ9aAAE");
		ssaModel.setSoftware_id("4cpi8P4dkTk200Inv1cPvO");
		ssaModel.setSoftware_client_id("4cpi8P4dkTk200Inv1cPvO");
		ssaModel.setSoftware_roles(new ArrayList<String>(Arrays.asList("AISP", "PISP")));
		ssaModel.setSoftware_redirect_uris(new ArrayList<String>(Arrays.asList("https://test.com")));
		Authorisation orgAuth=new Authorisation();
		orgAuth.setMember_state("GB");
		orgAuth.setRoles(new ArrayList<String>(Arrays.asList("PISP","AISP","CBPII")));
		OrganisationCompetentAuthorityClaims orgClaims = new OrganisationCompetentAuthorityClaims();
		orgClaims.setAuthority_id("abcd");
		orgClaims.setRegistration_id("erewr");
		orgClaims.setAuthorisations(new ArrayList<Authorisation>(Arrays.asList(orgAuth)));
		orgClaims.setStatus("active");
		ssaModel.setOrganisation_competent_authority_claims(orgClaims);
		WellKnownConfig wellKnownConfig = new WellKnownConfig();
		Map<String, String>  issuer = new HashMap<>();
		issuer.put("BOI", "https://auth-test.apibank-cma2plus.in");
		issuer.put("BOIUK", "https://auth-test.apibank-cma2plus.in");
		wellKnownConfig.getIssuer().putAll(issuer);
		wellKnownConfig.setScopes(new ArrayList<String>(Arrays.asList("openid", "payments", "accounts")));
		wellKnownConfig.setResponse_types_supported(new ArrayList<String>(Arrays.asList("code", "id_token")));
		wellKnownConfig.setGrant_types_supported(
				new ArrayList<String>(Arrays.asList("authorization_code", "refresh_token", "client_credentials")));
		wellKnownConfig.setId_token_signing_alg_values_supported(new ArrayList<String>(Arrays.asList("RS256")));
		wellKnownConfig
				.setToken_endpoint_auth_methods_supported(new ArrayList<String>(Arrays.asList("client_secret_basic")));
		wellKnownConfig
				.setRequest_object_signing_alg_values_supported(new ArrayList<String>(Arrays.asList("RS256", "PS256")));
		ssaValidationContext = new SSAValidationContext(ssaStrategies);
		RequestHeaderAttributes requestheaderAttributes=new RequestHeaderAttributes();
		requestheaderAttributes.setChannelId("OB_EXISTING_CERT");
		requestheaderAttributes.setX_ssl_client_cn("7Jk3WzvUwVYm2rEeLFK7hb");
		requestheaderAttributes.setX_ssl_client_ncaid("7Jk3WzvUwVYm2rEeLFK7hb");
		requestheaderAttributes.setTenantId("BOI");
		ReflectionTestUtils.setField(ssaValidationContext, "headerAttributes", requestheaderAttributes);
	}
	
	@Test
	public void testValidationForAllValid(){
		RequestHeaderAttributes requestheaderAttributes=new RequestHeaderAttributes();
		requestheaderAttributes.setChannelId("OB_EXISTING_CERT");
		requestheaderAttributes.setX_ssl_client_cn("7Jk3WzvUwVYm2rEeLFK7hb");
		requestheaderAttributes.setX_ssl_client_ncaid("7Jk3WzvUwVYm2rEeLFK7hb");
		requestheaderAttributes.setTenantId("BOI");
		ReflectionTestUtils.setField(ssaValidationContext, "headerAttributes", requestheaderAttributes);
		System.out.println(ssaModel.toString());
		ssaValidationContext.execute(ssaModel);
	}
	

	@Test(expected = ClientRegistrationException.class)
	public void testValidationForLongSoftwareClientId(){
		Set<ValidationStrategy> ssaStrategies1=new LinkedHashSet<ValidationStrategy>();
		ssaStrategies1.add(SSAValidationStrategy.SOFTWARE_CLIENT_ID_LENGTH_SSA);
		ReflectionTestUtils.setField(ssaValidationContext, "headerAttributes", requestHeaderAttributes);
		SSAValidationContext cnxt=new SSAValidationContext(ssaStrategies1);
		SSAModel ssaModel=new SSAModel();
		ssaModel.setSoftware_client_id("11234534534531123453453453112345345345311234534534531123453453453");
		cnxt.execute(ssaModel);
	}

	@Test(expected = ClientRegistrationException.class)
	public void testValidationForRedirectUri(){
		Set<ValidationStrategy> ssaStrategies1=new LinkedHashSet<ValidationStrategy>();
		ssaStrategies1.add(SSAValidationStrategy.REDIRECT_URIS_SSA);
		ReflectionTestUtils.setField(ssaValidationContext, "headerAttributes", requestHeaderAttributes);
		SSAValidationContext cnxt=new SSAValidationContext(ssaStrategies1);
		SSAModel ssaModel=new SSAModel();
		ssaModel.setSoftware_redirect_uris(new ArrayList<String>(Arrays.asList("google.com","demo.com")));
		cnxt.execute(ssaModel);
	}
	
	@Test(expected = ClientRegistrationException.class)
	public void testValidationForSSAFields(){
		Set<ValidationStrategy> ssaStrategies1=new LinkedHashSet<ValidationStrategy>();
		ssaStrategies1.add(SSAValidationStrategy.SSA_MANADATORY_FILEDS);
		ReflectionTestUtils.setField(ssaValidationContext, "headerAttributes", requestHeaderAttributes);
		SSAValidationContext cnxt=new SSAValidationContext(ssaStrategies1);
		SSAModel ssaModel=new SSAModel();
		cnxt.execute(ssaModel);
	}
	
	@Test(expected = ClientRegistrationException.class)
	public void testValidationForLongSoftwareId(){
		Set<ValidationStrategy> ssaStrategies1=new LinkedHashSet<ValidationStrategy>();
		ssaStrategies1.add(SSAValidationStrategy.SOFTWARE_ID_LENGTH_SSA);
		ReflectionTestUtils.setField(ssaValidationContext, "headerAttributes", requestHeaderAttributes);
		SSAValidationContext cnxt=new SSAValidationContext(ssaStrategies1);
		SSAModel ssaModel=new SSAModel();
		ssaModel.setSoftware_id("11234534534531123453453453112345345345311234534534531123453453453");
		cnxt.execute(ssaModel);
	}



}
