package com.capgemini.psd2.controllers;

import static org.mockito.ArgumentMatchers.anyString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.net.URI;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.capgemini.psd2.mock.data.DynamicClientMockData;
import com.capgemini.psd2.service.DynamicClientService;
import com.capgemini.tpp.registration.model.OBClientRegistrationResponse;
import com.capgemini.tpp.registration.model.OBRegistrationProperties1;
import com.capgemini.tpp.registration.model.SSAWrapper;

@RunWith(SpringJUnit4ClassRunner.class)
public class DynamicClientControllerTest {

	@Mock
	private DynamicClientService dynamicClientService;

	@InjectMocks
	private DynamicClientController dynamicClientController;

	private MockMvc mockMvc;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		this.mockMvc = MockMvcBuilders.standaloneSetup(dynamicClientController).dispatchOptions(true).build();

	}

	@Test
	public void testClientRegistrationSuccess() throws Exception {

		String jwtToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c";

		SSAWrapper ssaWrapper = DynamicClientMockData.getSSAWrapper();

		OBRegistrationProperties1 registrationResponse = DynamicClientMockData.getRegistrationResponse();

		Mockito.when(dynamicClientService.validateRegistrationRequest(anyString(), anyString())).thenReturn(ssaWrapper);
		Mockito.when(dynamicClientService.clientRegistration(ssaWrapper,Boolean.FALSE)).thenReturn(registrationResponse);

		this.mockMvc.perform(post("/register").contentType("application/jwt").content(jwtToken))
				.andExpect(status().isCreated());

	}

	@Test
	public void testClientRegistrationUpdateSuccess() throws Exception {

		String jwtToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c";

		SSAWrapper ssaWrapper = DynamicClientMockData.getSSAWrapper();

		OBRegistrationProperties1 registrationResponse = DynamicClientMockData.getRegistrationResponse();

		Mockito.when(dynamicClientService.validateRegistrationRequest(anyString(), anyString())).thenReturn(ssaWrapper);
		Mockito.when(dynamicClientService.clientRegistration(ssaWrapper,Boolean.FALSE)).thenReturn(registrationResponse);

		this.mockMvc.perform(put("/register/{ClientId}", 1234).contentType("application/jwt").content(jwtToken))
				.andExpect(status().isOk());
	}

	@After
	public void tearDown() throws Exception {
		dynamicClientController = null;
		mockMvc = null;
		dynamicClientService = null;
	}

}
