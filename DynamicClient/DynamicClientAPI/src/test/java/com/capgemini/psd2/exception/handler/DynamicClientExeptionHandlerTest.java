package com.capgemini.psd2.exception.handler;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.context.request.WebRequest;

import com.capgemini.psd2.dcr.domain.RegistrationError;
import com.capgemini.psd2.enums.ClientRegistrationErrorCodeEnum;
import com.capgemini.psd2.exception.ClientRegistrationException;
import com.capgemini.psd2.exceptions.PSD2Exception;

@RunWith(SpringJUnit4ClassRunner.class)
public class DynamicClientExeptionHandlerTest {
	
/** The web request. */
@Mock
private WebRequest webRequest;

/** The http client error exception. */
@Mock
private HttpClientErrorException httpClientErrorException;

/** The psd 2 exception. */
@Mock
private PSD2Exception psd2Exception;

@Mock
private ClientRegistrationException clientRegistrationException;

/** The handler. */
@InjectMocks
private DynamicClientExeptionHandler dynamicHandler;

/**
 * Sets the up.
 */
@Before
public void setUp() {
	MockitoAnnotations.initMocks(this);
	when(httpClientErrorException.getStatusCode()).thenReturn(HttpStatus.ACCEPTED);
}

/**
 * Test.
 */
@Test
public void test() {
	dynamicHandler.handleInvalidRequest(httpClientErrorException, webRequest);
	
	RegistrationError registrationError=new RegistrationError(); 
	registrationError.setCode("9018");
	registrationError.setError(RegistrationError.ErrorEnum.INVALID_CLIENT_METADATA);
	registrationError.setErrorDescription("testexception");
	registrationError.setStatusCode("400");
	Mockito.when(clientRegistrationException.getRegistrationError()).thenReturn(registrationError);
	Mockito.when(psd2Exception.getRegistrationError()).thenReturn(registrationError);
	dynamicHandler.handleInvalidRequest(clientRegistrationException, webRequest);
	dynamicHandler.handleInvalidRequest(psd2Exception, webRequest);
	dynamicHandler.handleInvalidRequest(new Exception(), webRequest);
	
	dynamicHandler.handleInvalidRequest(null, webRequest);
}

@Test
public void test1()
{
	assertNotNull(ClientRegistrationErrorCodeEnum.fromString("9028"));
	assertNull(ClientRegistrationErrorCodeEnum.fromString("90676728"));
	
}

}
