package com.capgemini.psd2.mock.data;

import java.util.ArrayList;
import java.util.List;

import com.capgemini.psd2.model.Authorisation;
import com.capgemini.psd2.model.OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations;
import com.capgemini.tpp.dtos.GrantScopes;
import com.capgemini.tpp.registration.model.OBClientRegistration1;
import com.capgemini.tpp.registration.model.OBClientRegistration1.ApplicationTypeEnum;
import com.capgemini.tpp.registration.model.OBClientRegistration1.GrantTypesEnum;
import com.capgemini.tpp.registration.model.OBClientRegistration1.ResponseTypesEnum;
import com.capgemini.tpp.registration.model.OBClientRegistration1.TokenEndpointAuthMethodEnum;
import com.capgemini.tpp.registration.model.OBClientRegistrationResponse;
import com.capgemini.tpp.registration.model.OBRegistrationProperties1;
import com.capgemini.tpp.registration.model.SSAWrapper;
import com.capgemini.tpp.registration.model.SupportedAlgorithms;
import com.capgemini.tpp.ssa.model.SSAModel;

public class DynamicClientMockData {

	public static SSAWrapper getSSAWrapper() {

		SSAWrapper ssaWrapper = new SSAWrapper();
	
		ssaWrapper.setSsaToken(
				"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c");

		List<String> redirectUris = new ArrayList();
		redirectUris.add("https://demo.com");
		SSAModel ssaModel = setSSAModel();
		ssaWrapper.setSsaModel(ssaModel);

		List<GrantTypesEnum> grantTypes = new ArrayList();
		grantTypes.add(GrantTypesEnum.CLIENT_CREDENTIALS);
		grantTypes.add(GrantTypesEnum.AUTHORIZATION_CODE);
		grantTypes.add(GrantTypesEnum.REFRESH_TOKEN);
		
		List<ResponseTypesEnum> responseTypes = new ArrayList();
		responseTypes.add(ResponseTypesEnum.CODE_ID_TOKEN);
		
		OBClientRegistration1 registrationRequest = setRegistrationRequest();
		registrationRequest.setGrantTypes(grantTypes);
		registrationRequest.setApplicationType(ApplicationTypeEnum.WEB);
		registrationRequest.aud("https://auth.test.apiplatform.in");
		registrationRequest.setClientId("q2NAzTFbbY9ELcN4GdDEQh");
		registrationRequest.setIdTokenSignedResponseAlg(SupportedAlgorithms.PS256);
		registrationRequest.setRedirectUris(redirectUris);
		registrationRequest.setScope("openid accounts payments fundsconfirmations");
		registrationRequest.setSoftwareStatement("eyJhbGciOiJQUzI1NiIsImtpZCI6Imo4SFdZMDBhSUJtS0ExT1c3WW50dnRLVU0ycnVueDdvQWdiS2hJRE1IM0k9IiwidHlwIjoiSldUIn0.eyJpc3MiOiJPcGVuQmFua2luZyBMdGQiLCJpYXQiOjE2MTg0MTI2MjQsImp0aSI6IjFiMzY2ZTgyODI5NjRhYjQiLCJzb2Z0d2FyZV9lbnZpcm9ubWVudCI6InNhbmRib3giLCJzb2Z0d2FyZV9tb2RlIjoiVGVzdCIsInNvZnR3YXJlX2lkIjoicTJOQXpURmJiWTlFTGNONEdkREVRaCIsInNvZnR3YXJlX2NsaWVudF9pZCI6InEyTkF6VEZiYlk5RUxjTjRHZERFUWgiLCJzb2Z0d2FyZV9jbGllbnRfbmFtZSI6IkNsaWVudF8xMyIsInNvZnR3YXJlX2NsaWVudF9kZXNjcmlwdGlvbiI6IkNsaWVudF8xMyIsInNvZnR3YXJlX3ZlcnNpb24iOjEuMSwic29mdHdhcmVfY2xpZW50X3VyaSI6Imh0dHBzOi8vZGVtby5jb20iLCJzb2Z0d2FyZV9yZWRpcmVjdF91cmlzIjpbImh0dHBzOi8vZGVtby5jb20iXSwic29mdHdhcmVfcm9sZXMiOlsiUElTUCIsIkNCUElJIiwiQUlTUCJdLCJvcmdhbmlzYXRpb25fY29tcGV0ZW50X2F1dGhvcml0eV9jbGFpbXMiOnsiYXV0aG9yaXR5X2lkIjoiRkNBR0JSIiwicmVnaXN0cmF0aW9uX2lkIjoiNTEyOTU2Iiwic3RhdHVzIjoiQWN0aXZlIiwiYXV0aG9yaXNhdGlvbnMiOlt7Im1lbWJlcl9zdGF0ZSI6IkdCIiwicm9sZXMiOlsiUElTUCIsIkNCUElJIiwiQUlTUCIsIkFTUFNQIl19LHsibWVtYmVyX3N0YXRlIjoiSUUiLCJyb2xlcyI6WyJDQlBJSSIsIkFJU1AiLCJBU1BTUCIsIlBJU1AiXX0seyJtZW1iZXJfc3RhdGUiOiJOTCIsInJvbGVzIjpbIlBJU1AiLCJDQlBJSSIsIkFJU1AiLCJBU1BTUCJdfV19LCJzb2Z0d2FyZV9sb2dvX3VyaSI6Imh0dHBzOi8vZGVtby5jb20iLCJvcmdfc3RhdHVzIjoiQWN0aXZlIiwib3JnX2lkIjoiMDAxNTgwMDAwMGpmUTlhQUFFIiwib3JnX25hbWUiOiJCYW5rIG9mIElyZWxhbmQgKFVLKSBQbGMiLCJvcmdfY29udGFjdHMiOlt7Im5hbWUiOiJUZWNobmljYWwiLCJlbWFpbCI6Ik9CVGVjaG5pY2FsUXVlcmllc0BCT0kuQ09NIiwicGhvbmUiOiIrMzUzIDg2MDY4MTc2MiIsInR5cGUiOiJUZWNobmljYWwifSx7Im5hbWUiOiJCdXNpbmVzcyIsImVtYWlsIjoiT0JCdXNpbmVzc1F1ZXJpZXNAQk9JLkNPTSIsInBob25lIjoiKzQ0IDc1ODQgMjE0ODMwIiwidHlwZSI6IkJ1c2luZXNzIn1dLCJvcmdfandrc19lbmRwb2ludCI6Imh0dHBzOi8va2V5c3RvcmUub3BlbmJhbmtpbmd0ZXN0Lm9yZy51ay8wMDE1ODAwMDAwamZROWFBQUUvMDAxNTgwMDAwMGpmUTlhQUFFLmp3a3MiLCJvcmdfandrc19yZXZva2VkX2VuZHBvaW50IjoiaHR0cHM6Ly9rZXlzdG9yZS5vcGVuYmFua2luZ3Rlc3Qub3JnLnVrLzAwMTU4MDAwMDBqZlE5YUFBRS9yZXZva2VkLzAwMTU4MDAwMDBqZlE5YUFBRS5qd2tzIiwic29mdHdhcmVfandrc19lbmRwb2ludCI6Imh0dHBzOi8va2V5c3RvcmUub3BlbmJhbmtpbmd0ZXN0Lm9yZy51ay8wMDE1ODAwMDAwamZROWFBQUUvcTJOQXpURmJiWTlFTGNONEdkREVRaC5qd2tzIiwic29mdHdhcmVfandrc19yZXZva2VkX2VuZHBvaW50IjoiaHR0cHM6Ly9rZXlzdG9yZS5vcGVuYmFua2luZ3Rlc3Qub3JnLnVrLzAwMTU4MDAwMDBqZlE5YUFBRS9yZXZva2VkL3EyTkF6VEZiYlk5RUxjTjRHZERFUWguandrcyIsInNvZnR3YXJlX3BvbGljeV91cmkiOiJodHRwczovL2RlbW8uY29tIiwic29mdHdhcmVfdG9zX3VyaSI6Imh0dHBzOi8vZGVtby5jb20iLCJzb2Z0d2FyZV9vbl9iZWhhbGZfb2Zfb3JnIjoiIn0.AejTg4OdFci2sk2LTmLnCN8ii18bgCZqPm2tuEE8uqiyl_zmuCESzTq_VgYp3cXA9z3Wv3MFLlO8rbhYbaApSzh1j4yvfxiSyM28uI6-W66TeuLgnv8yrJauXtfrXHiT4y5pmMHDI81WzKB_t_CllWj0dv_lakmZ0WhxdTWJDBkL7h2GOjIygb0f2faVSByaLkp9SZAFeCzX8Q3HwOmWdOfLYDQXgyreseI-E7iJ09JtXVBrF6iFOJIq_UXMIWKe9YVzFE22jWzkuHn8pPxkemuGrDD3icYnEBGrEGnc8z8EfFxT0qJmrH5HKo1Qbsev0Yew5j5V0htLAR8EjbxKow");
		registrationRequest.setSoftwareId("q2NAzTFbbY9ELcN4GdDEQh");
		registrationRequest.setRequestObjectSigningAlg(SupportedAlgorithms.PS256);
		registrationRequest.setTokenEndpointAuthMethod(TokenEndpointAuthMethodEnum.TLS_CLIENT_AUTH);
		registrationRequest.setResponseTypes(responseTypes);
		registrationRequest.setTlsClientAuthSubjectDn("CN = kzSKsxr5NYPMvjkVFPCXE2, OU = 0015800000jfQ9aAAE, O = OpenBanking,C = GB");
		ssaWrapper.setObClientRegistration1(registrationRequest);

		return ssaWrapper;
	}

	private static OBClientRegistration1 setRegistrationRequest() {

		ArrayList<String> redirect_uris = new ArrayList<>();
		redirect_uris.add("https://test.com");
		ArrayList<String> grant_types = new ArrayList<>();
		grant_types.add("authorization_code");
		grant_types.add("refresh_token");
		grant_types.add("client_credentials");

		OBClientRegistration1 registrationRequest = new OBClientRegistration1();
		
		return registrationRequest;
	}

	public static SSAModel setSSAModel() {

		SSAModel ssaModel = new SSAModel();
		ssaModel.setIat(1537257843);
		ssaModel.setIss("1wwLDFoVlHfnlK0Dgu71lB");
		ssaModel.setSoftware_id("1wwLDFoVlHfnlK0Dgu71lB");
		ssaModel.setJti("7aac9164-eeac-47d6-8042-9a24610a984e");
		ssaModel.setOrg_id("0015800000jfQ9aAAE");
		ssaModel.setOrg_jwks_endpoint("https://auth-test.apibank-cma2plus.in");
		ssaModel.setSoftware_jwks_endpoint("https://auth-test.apibank-cma2plus.in");
		ssaModel.setSoftware_client_id("1wwLDFoVlHfnlK0Dgu71lB");
		ssaModel.setSoftware_client_name("CG_TPP_DCR");
		ssaModel.setSoftware_client_uri("https://auth-test.apibank-cma2plus.in");
		ssaModel.setSoftware_logo_uri("https://auth-test.apibank-cma2plus.in");
		ArrayList<String> software_roles = new ArrayList<>();
		software_roles.add("aisp");
		software_roles.add("pisp");
		
		ssaModel.setSoftware_roles(software_roles);
		
		return ssaModel;
	}

	public static OBRegistrationProperties1 getRegistrationResponse() {
		OBRegistrationProperties1 registrationResponse = new OBRegistrationProperties1();

		return registrationResponse;
	}
	
	public static OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations getAuthorisationsA1()
	{
		OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations a1=new OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations();
		a1.setActive(true);
		a1.setMemberState("GB");
		a1.setPsd2Role("PISP");
		return a1;
	}
	
	public static OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations getAuthorisationsA2()
	{
		OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations a1=new OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations();
		a1.setActive(true);
		a1.setMemberState("GB");
		a1.setPsd2Role("AISP");
		return a1;
	}
	
	
	public static OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations getAuthorisationsA3()
	{
		OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations a1=new OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations();
		a1.setActive(true);
		a1.setMemberState("IR");
		a1.setPsd2Role("AISP");
		return a1;
	}
	
	public static OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations getAuthorisationsA4()
	{
		OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations a1=new OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations();
		a1.setActive(false);
		a1.setMemberState("IR");
		a1.setPsd2Role("PISP");
		return a1;
	}
	
	public static OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations getAuthorisationsA5()
	{
		OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations a1=new OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations();
		a1.setActive(true);
		a1.setMemberState("NR");
		a1.setPsd2Role("AISP");
		return a1;
	}
	
	
	public static Authorisation getAuthorisation1() {
	
		Authorisation C1=new Authorisation();
		C1.setMember_state("GB");
		ArrayList<String> roles=new ArrayList<>();
		roles.add("AISP");
		roles.add("PISP");
		//roles.add("CISP");
		C1.setRoles(roles);
		return C1;
	}
	
	public static Authorisation getAuthorisation2() {
		
		Authorisation C1=new Authorisation();
		C1.setMember_state("IR");
		ArrayList<String> roles=new ArrayList<>();
		roles.add("AISP");
		roles.add("PISP");
		C1.setRoles(roles);
		
		return C1;
		
	}
	
public static Authorisation getAuthorisation3() {
		
		Authorisation C1=new Authorisation();
		C1.setMember_state("NR");
		ArrayList<String> roles=new ArrayList<>();
		roles.add("AISP");
		roles.add("NISP");
		C1.setRoles(roles);
		
		return C1;
		
	}
}
