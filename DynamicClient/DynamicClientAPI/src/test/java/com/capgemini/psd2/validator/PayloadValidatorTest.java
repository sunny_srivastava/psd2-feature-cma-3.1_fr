package com.capgemini.psd2.validator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.config.WellKnownConfig;
import com.capgemini.psd2.exception.ClientRegistrationException;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.tpp.registration.model.OBClientRegistration1;
import com.capgemini.tpp.registration.model.OBClientRegistration1.ApplicationTypeEnum;
import com.capgemini.tpp.registration.model.OBClientRegistration1.GrantTypesEnum;
import com.capgemini.tpp.registration.model.OBClientRegistration1.TokenEndpointAuthMethodEnum;
import com.capgemini.tpp.registration.model.SupportedAlgorithms;
import com.capgemini.tpp.ssa.model.Authorisation;
import com.capgemini.tpp.ssa.model.OrganisationCompetentAuthorityClaims;
import com.capgemini.tpp.ssa.model.SSAModel;


@RunWith(SpringJUnit4ClassRunner.class)
public class PayloadValidatorTest {
	Set<ValidationStrategy> payloadstrategies=null;
	
	OBClientRegistration1 regRequest=null;
	OBClientRegistration1 regRequest1=null;
	
	SSAModel ssaModel=null;
	
	 RequestHeaderAttributes headerAttributes=null;
	@InjectMocks
	private PayloadValidationContext payloadValidationContext;
	
	@Mock
	private WellKnownConfig wellKnownConfig;
	
	@InjectMocks
	private WellKnownConfig wellKnownConfig1;
	
	@Mock
	RequestHeaderAttributes requestHeaderAttributes;
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(PayloadValidationContext.class);
		payloadstrategies = new LinkedHashSet<ValidationStrategy>(EnumSet.allOf(PayloadValidationStrategy.class));
		regRequest = new OBClientRegistration1();
		ssaModel = new SSAModel();
		ssaModel.setIss("OpenBanking Ltd");
		ssaModel.setOrg_id("0015800000jfQ9aAAE");
		ssaModel.setSoftware_id("4cpi8P4dkTk200Inv1cPvO");
		ssaModel.setSoftware_client_id("4cpi8P4dkTk200Inv1cPvO");
		ssaModel.setSoftware_roles(new ArrayList<String>(Arrays.asList("AISP", "PISP")));
		ssaModel.setSoftware_redirect_uris(new ArrayList<String>(Arrays.asList("https://test.com")));
		WellKnownConfig wellKnownConfig = new WellKnownConfig();
		Map<String, String>  issuer = new HashMap<>();
		issuer.put("BOI", "https://auth-test.apibank-cma2plus.in");
		issuer.put("BOIUK", "https://auth-test.apibank-cma2plus.in");
		wellKnownConfig.getIssuer().putAll(issuer);
		wellKnownConfig.setScopes(new ArrayList<String>(Arrays.asList("openid", "payments", "accounts")));
		wellKnownConfig.setResponse_types_supported(new ArrayList<String>(Arrays.asList("code", "id_token")));
		wellKnownConfig.setGrant_types_supported(
				new ArrayList<String>(Arrays.asList("authorization_code", "refresh_token", "client_credentials")));
		wellKnownConfig.setId_token_signing_alg_values_supported(new ArrayList<String>(Arrays.asList("RS256")));
		wellKnownConfig
				.setToken_endpoint_auth_methods_supported(new ArrayList<String>(Arrays.asList("client_secret_basic")));
		wellKnownConfig
				.setRequest_object_signing_alg_values_supported(new ArrayList<String>(Arrays.asList("RS256", "PS256")));
	//	wellKnownConfig.setTls_client_auth_dn("CN=7Jk3WzvUwVYm2rEeLFK7hb, OU=0015800000jfQ9aAAE, O=OpenBanking, C=GB");
		payloadValidationContext = new PayloadValidationContext(payloadstrategies,null);
		ReflectionTestUtils.setField(payloadValidationContext, "wellKnownConfig", wellKnownConfig);
		RequestHeaderAttributes requestheaderAttributes=new RequestHeaderAttributes();
		requestheaderAttributes.setChannelId("OB_EXISTING_CERT");
		requestheaderAttributes.setX_ssl_client_cn("7Jk3WzvUwVYm2rEeLFK7hb");
		requestheaderAttributes.setX_ssl_client_ncaid("7Jk3WzvUwVYm2rEeLFK7hb");
		requestheaderAttributes.setTenantId("BOI");
		ReflectionTestUtils.setField(payloadValidationContext, "headerAttributes", requestheaderAttributes);
	}
	
	@Test(expected = ClientRegistrationException.class)
	public void testValidationForIssuerMismatch(){
		Set<ValidationStrategy> payloadstrategies1 = new LinkedHashSet<ValidationStrategy>();
		payloadstrategies1.add(PayloadValidationStrategy.ISSUER);
		payloadValidationContext = new PayloadValidationContext(payloadstrategies1,null);
		ReflectionTestUtils.setField(payloadValidationContext, "wellKnownConfig", wellKnownConfig);
		regRequest.setIss("");
		payloadValidationContext.execute(regRequest, ssaModel,false);
	}
	@Test
	public void testValidationForIssuer(){
		Set<ValidationStrategy> payloadstrategies1 = new LinkedHashSet<ValidationStrategy>();
		payloadstrategies1.add(PayloadValidationStrategy.ISSUER);
		payloadValidationContext = new PayloadValidationContext(payloadstrategies1,null);
		ReflectionTestUtils.setField(payloadValidationContext, "wellKnownConfig", wellKnownConfig);
		regRequest.setIss("4cpi8P4dkTk200Inv1cPvO");
		payloadValidationContext.execute(regRequest, ssaModel,false);
	}
	@Test(expected = ClientRegistrationException.class)
	public void testValidationForSubjectDn(){
		Set<ValidationStrategy> payloadstrategies1 = new LinkedHashSet<ValidationStrategy>();
		payloadstrategies1.add(PayloadValidationStrategy.TLS_CLIENT_AUTH_DN);
		payloadValidationContext = new PayloadValidationContext(payloadstrategies1,null);
		ReflectionTestUtils.setField(payloadValidationContext, "wellKnownConfig", wellKnownConfig);
		regRequest.setTlsClientAuthSubjectDn("CN = 0015800000jfQ9aAAE,2.5.4.97 = PSDGB-FCA-512956,O = Bank of Ireland (UK) Plc,C = GB");
		
		RequestHeaderAttributes requestheaderAttributes=new RequestHeaderAttributes();
		
		requestheaderAttributes.setX_ssl_client_cn("7Jk3WzvUwVYm2rEeLFK7hb");
		requestheaderAttributes.setX_ssl_client_ncaid("2.5.4.97");
		requestheaderAttributes.setTenantId("BOI");
		ReflectionTestUtils.setField(payloadValidationContext, "headerAttributes", requestheaderAttributes);
		
		payloadValidationContext.execute(regRequest, ssaModel,false);
	}
	@Test(expected = ClientRegistrationException.class)
	public void testValidationForAudMismatch() {
		Set<ValidationStrategy> payloadstrategies1 = new LinkedHashSet<ValidationStrategy>();
		payloadstrategies1.add(PayloadValidationStrategy.AUDIENCE);
		payloadValidationContext = new PayloadValidationContext(payloadstrategies1,null);
		ReflectionTestUtils.setField(payloadValidationContext, "wellKnownConfig", wellKnownConfig);
		regRequest.setAud("1234");
		RequestHeaderAttributes requestheaderAttributes=new RequestHeaderAttributes();
		requestheaderAttributes.setChannelId("OB_EXISTING_CERT");
		requestheaderAttributes.setX_ssl_client_cn("7Jk3WzvUwVYm2rEeLFK7hb");
		requestheaderAttributes.setX_ssl_client_ncaid("7Jk3WzvUwVYm2rEeLFK7hb");
		requestheaderAttributes.setTenantId("BOI");
		ReflectionTestUtils.setField(payloadValidationContext, "headerAttributes", requestheaderAttributes);
		payloadValidationContext.execute(regRequest, ssaModel,false);
	}
	
	@Test(expected = ClientRegistrationException.class)
	public void testValidationForRedirectMismatch() {
		Set<ValidationStrategy> payloadstrategies1 = new LinkedHashSet<ValidationStrategy>();
		payloadstrategies1.add(PayloadValidationStrategy.REDIRECT_URIS);
		payloadValidationContext = new PayloadValidationContext(payloadstrategies1,null);
		ReflectionTestUtils.setField(payloadValidationContext, "wellKnownConfig", wellKnownConfig);
		regRequest.setRedirectUris(new ArrayList<String>(Arrays.asList("https://testmismatch.com")));
		payloadValidationContext.execute(regRequest, ssaModel,false);
	}
	@Test(expected = ClientRegistrationException.class)
	public void testValidationForRedirectMismandatory() {
		Set<ValidationStrategy> payloadstrategies1 = new LinkedHashSet<ValidationStrategy>();
		payloadstrategies1.add(PayloadValidationStrategy.REDIRECT_URIS);
		payloadValidationContext = new PayloadValidationContext(payloadstrategies1,null);
		ReflectionTestUtils.setField(payloadValidationContext, "wellKnownConfig", wellKnownConfig);
		regRequest.setRedirectUris(new ArrayList<String>(Arrays.asList("")));
		payloadValidationContext.execute(regRequest, ssaModel,false);
	}
	
	@Test(expected = ClientRegistrationException.class)
	public void testValidationForTokeEndpointMismatch() {
		Set<ValidationStrategy> payloadstrategies1 = new LinkedHashSet<ValidationStrategy>();
		payloadstrategies1.add(PayloadValidationStrategy.TOKEN_ENDPOINTS_AUTH_METHODS);
		payloadValidationContext = new PayloadValidationContext(payloadstrategies1,null);
		ReflectionTestUtils.setField(payloadValidationContext, "wellKnownConfig", wellKnownConfig);
		regRequest.setTokenEndpointAuthMethod(TokenEndpointAuthMethodEnum.valueOf("PRIVATE_KEY_JWT"));
		payloadValidationContext.execute(regRequest, ssaModel,false);
	}
	
	
	@Test(expected = ClientRegistrationException.class)
	public void testValidationForGrantTypesMismatch1() {
		Set<ValidationStrategy> payloadstrategies1 = new LinkedHashSet<ValidationStrategy>();
		payloadstrategies1.add(PayloadValidationStrategy.GRANT_TYPES);
		payloadValidationContext = new PayloadValidationContext(payloadstrategies1,null);
		ReflectionTestUtils.setField(payloadValidationContext, "wellKnownConfig", wellKnownConfig);
		regRequest.setGrantTypes((
				new ArrayList<GrantTypesEnum>(Arrays.asList( ))));
		payloadValidationContext.execute(regRequest, ssaModel,false);
	}

	@Test
	public void testValidationForGrantTypes() {
		Set<ValidationStrategy> payloadstrategies1 = new LinkedHashSet<ValidationStrategy>();
		payloadstrategies1.add(PayloadValidationStrategy.GRANT_TYPES);
		payloadValidationContext = new PayloadValidationContext(payloadstrategies1,null);
		ReflectionTestUtils.setField(payloadValidationContext, "wellKnownConfig", wellKnownConfig);
		regRequest.setGrantTypes((
				new ArrayList<GrantTypesEnum>(Arrays.asList(GrantTypesEnum.AUTHORIZATION_CODE, GrantTypesEnum.REFRESH_TOKEN, GrantTypesEnum.CLIENT_CREDENTIALS ))));
		payloadValidationContext.execute(regRequest, ssaModel,false);
	}
	@Test(expected = ClientRegistrationException.class)
	public void testValidationForResponseTypesMismatch() {
		Set<ValidationStrategy> payloadstrategies1 = new LinkedHashSet<ValidationStrategy>();
		payloadstrategies1.add(PayloadValidationStrategy.RESPONSE_TYPES);
		payloadValidationContext = new PayloadValidationContext(payloadstrategies1,null);
		ReflectionTestUtils.setField(payloadValidationContext, "wellKnownConfig", wellKnownConfig);
		regRequest.setGrantTypes((
				new ArrayList<GrantTypesEnum>(Arrays.asList(GrantTypesEnum.AUTHORIZATION_CODE, GrantTypesEnum.REFRESH_TOKEN, GrantTypesEnum.CLIENT_CREDENTIALS))));
		payloadValidationContext.execute(regRequest, ssaModel,false);
	}
	
	@Test(expected = ClientRegistrationException.class)
	public void testValidationForSoftwareIdMismatch() {
		Set<ValidationStrategy> payloadstrategies1 = new LinkedHashSet<ValidationStrategy>();
		payloadstrategies1.add(PayloadValidationStrategy.SOFTWARE_ID);
		payloadValidationContext = new PayloadValidationContext(payloadstrategies1,null);
		ReflectionTestUtils.setField(payloadValidationContext, "wellKnownConfig", wellKnownConfig);
		regRequest.setSoftwareId("4cpi8P4dkTk200Inv1cPvO123");
		payloadValidationContext.execute(regRequest, ssaModel,false);
	}
	@Test
	public void testValidationForSoftwareId() {
		Set<ValidationStrategy> payloadstrategies1 = new LinkedHashSet<ValidationStrategy>();
		payloadstrategies1.add(PayloadValidationStrategy.SOFTWARE_ID);
		payloadValidationContext = new PayloadValidationContext(payloadstrategies1,null);
		ReflectionTestUtils.setField(payloadValidationContext, "wellKnownConfig", wellKnownConfig);
		regRequest.setSoftwareId("4cpi8P4dkTk200Inv1cPvO");
		payloadValidationContext.execute(regRequest, ssaModel,false);
	}
	@Test(expected = ClientRegistrationException.class)
	public void testValidationForScopes() {
		Set<ValidationStrategy> payloadstrategies1 = new LinkedHashSet<ValidationStrategy>();
		payloadstrategies1.add(PayloadValidationStrategy.SCOPES);
		payloadValidationContext = new PayloadValidationContext(payloadstrategies1,null);
		ReflectionTestUtils.setField(payloadValidationContext, "wellKnownConfig", wellKnownConfig);
		regRequest.setScope("payments");
		payloadValidationContext.execute(regRequest, ssaModel,false);
	}
	@Test
	public void testValidationForScopes1() {
		Set<ValidationStrategy> payloadstrategies1 = new LinkedHashSet<ValidationStrategy>();
		payloadstrategies1.add(PayloadValidationStrategy.SCOPES);
		payloadValidationContext = new PayloadValidationContext(payloadstrategies1,null);
		ReflectionTestUtils.setField(payloadValidationContext, "wellKnownConfig", wellKnownConfig);
		regRequest.setScope(null);
		payloadValidationContext.execute(regRequest, ssaModel,false);
	}
	
	@Test(expected = ClientRegistrationException.class)
	public void testValidationForAppType() {
		Set<ValidationStrategy> payloadstrategies1 = new LinkedHashSet<ValidationStrategy>();
		payloadstrategies1.add(PayloadValidationStrategy.APPLICATION_TYPE);
		payloadValidationContext = new PayloadValidationContext(payloadstrategies1,null);
		ReflectionTestUtils.setField(payloadValidationContext, "wellKnownConfig", wellKnownConfig);
		regRequest.setApplicationType(ApplicationTypeEnum.valueOf("MOBILE"));
		payloadValidationContext.execute(regRequest, ssaModel,false);
	}
	
	@Test(expected = ClientRegistrationException.class)
	public void testValidationForSigningAlg() {
		Set<ValidationStrategy> payloadstrategies1 = new LinkedHashSet<ValidationStrategy>();
		payloadstrategies1.add(PayloadValidationStrategy.ID_TOKEN_SIGNING_ALG);
		payloadValidationContext = new PayloadValidationContext(payloadstrategies1,null);
		ReflectionTestUtils.setField(payloadValidationContext, "wellKnownConfig", wellKnownConfig);
		regRequest.setIdTokenSignedResponseAlg(SupportedAlgorithms.valueOf("RS256"));
		payloadValidationContext.execute(regRequest, ssaModel,false);
	}
	
	
	@Test(expected = ClientRegistrationException.class)
	public void testValidationForRequestAlg() {
		Set<ValidationStrategy> payloadstrategies1 = new LinkedHashSet<ValidationStrategy>();
		payloadstrategies1.add(PayloadValidationStrategy.REQUEST_OBJECT_SIGNING_ALG);
		payloadValidationContext = new PayloadValidationContext(payloadstrategies1,null);
		ReflectionTestUtils.setField(payloadValidationContext, "wellKnownConfig", wellKnownConfig);
		regRequest.setRequestObjectSigningAlg(SupportedAlgorithms.valueOf("ES256"));
		payloadValidationContext.execute(regRequest, ssaModel,false);
	}
	
	@Test(expected = UnsupportedOperationException.class)
	public void testValidationForUnsupportedOperation() {
		ValidationStrategy payloadValidation = PayloadValidationStrategy.UNSUPPORTED;
		payloadValidation.validate(headerAttributes, ssaModel);
	}
	
	   @Test
       public void testValidationForSubjectDnLegacy(){
           Set<ValidationStrategy> payloadstrategies1 = new LinkedHashSet<ValidationStrategy>();
           payloadstrategies1.add(PayloadValidationStrategy.TLS_CLIENT_AUTH_DN);
           payloadValidationContext = new PayloadValidationContext(payloadstrategies1,null);
           ReflectionTestUtils.setField(payloadValidationContext, "wellKnownConfig", wellKnownConfig);
           regRequest.setTlsClientAuthSubjectDn("CN=7Jk3WzvUwVYm2rEeLFK7hb, OU=0015800000jfQ9aAAE, O=OpenBanking, C=GB");
           Authorisation orgAuth=new Authorisation();
           orgAuth.setMember_state("GB");
           orgAuth.setRoles(new ArrayList<String>(Arrays.asList("PISP","AISP","CBPII")));
           OrganisationCompetentAuthorityClaims orgClaims = new OrganisationCompetentAuthorityClaims();
           orgClaims.setAuthority_id("abcd");
           orgClaims.setRegistration_id("512956");
           orgClaims.setAuthorisations(new ArrayList<Authorisation>(Arrays.asList(orgAuth)));
           orgClaims.setStatus("active");
           ssaModel.setOrganisation_competent_authority_claims(orgClaims);
           RequestHeaderAttributes requestheaderAttributes=new RequestHeaderAttributes();
           requestheaderAttributes.setChannelId("OB_EXISTING_CERT");
           requestheaderAttributes.setX_ssl_client_cn("7Jk3WzvUwVYm2rEeLFK7hb");
           requestheaderAttributes.setX_ssl_client_ncaid("7Jk3WzvUwVYm2rEeLFK7hb");
           requestheaderAttributes.setMethodType("POST");
           requestheaderAttributes.setTenantId("BOI");
           ReflectionTestUtils.setField(payloadValidationContext, "headerAttributes", requestheaderAttributes);
         
           payloadValidationContext.execute(regRequest, ssaModel,false);
       }
	   
	   @Test(expected= ClientRegistrationException.class)
       public void testValidationForSubjectDnLegacyError(){
           Set<ValidationStrategy> payloadstrategies1 = new LinkedHashSet<ValidationStrategy>();
           payloadstrategies1.add(PayloadValidationStrategy.TLS_CLIENT_AUTH_DN);
           payloadValidationContext = new PayloadValidationContext(payloadstrategies1,null);
           ReflectionTestUtils.setField(payloadValidationContext, "wellKnownConfig", wellKnownConfig);
           regRequest.setTlsClientAuthSubjectDn("CN=7Jk3WzvUwVYm2rEeLFK7hb, OU=0015800000jfQ9aAAE, O=OpenBanking, C=GA");
           Authorisation orgAuth=new Authorisation();
           orgAuth.setMember_state("GB");
           orgAuth.setRoles(new ArrayList<String>(Arrays.asList("PISP","AISP","CBPII")));
           OrganisationCompetentAuthorityClaims orgClaims = new OrganisationCompetentAuthorityClaims();
           orgClaims.setAuthority_id("abcd");
           orgClaims.setRegistration_id("512956");
           orgClaims.setAuthorisations(new ArrayList<Authorisation>(Arrays.asList(orgAuth)));
           orgClaims.setStatus("active");
           ssaModel.setOrganisation_competent_authority_claims(orgClaims);
           RequestHeaderAttributes requestheaderAttributes=new RequestHeaderAttributes();
           requestheaderAttributes.setChannelId("OB_EXISTING_CERT");
           requestheaderAttributes.setX_ssl_client_cn("7Jk3WzvUwVYm2rEeLFK7hb");
           requestheaderAttributes.setX_ssl_client_ncaid("7Jk3WzvUwVYm2rEeLFK7hb");
           requestheaderAttributes.setMethodType("POST");
           requestheaderAttributes.setTenantId("BOI");
           ReflectionTestUtils.setField(payloadValidationContext, "headerAttributes", requestheaderAttributes);
         
           payloadValidationContext.execute(regRequest, ssaModel,false);
       }

	   @Test
       public void testValidationForSubjectDnLegacyMigrationSpecialChararcterSuccess(){
           Set<ValidationStrategy> payloadstrategies1 = new LinkedHashSet<ValidationStrategy>();
           payloadstrategies1.add(PayloadValidationStrategy.TLS_CLIENT_AUTH_DN);
           payloadValidationContext = new PayloadValidationContext(payloadstrategies1,null);
           ReflectionTestUtils.setField(payloadValidationContext, "wellKnownConfig", wellKnownConfig);
           regRequest.setTlsClientAuthSubjectDn("CN=0015800000jfQ9aAAE, OID.2.5.4.97=PSDGB-FCA-512956, O=OpenBanking.,\\//$čníÓ;&^#@$, C=GB");
           Authorisation orgAuth=new Authorisation();
           orgAuth.setMember_state("GB");
           orgAuth.setRoles(new ArrayList<String>(Arrays.asList("PISP","AISP","CBPII")));
           OrganisationCompetentAuthorityClaims orgClaims = new OrganisationCompetentAuthorityClaims();
           orgClaims.setAuthority_id("FCAGBR");
           orgClaims.setRegistration_id("512956");
           orgClaims.setAuthorisations(new ArrayList<Authorisation>(Arrays.asList(orgAuth)));
           orgClaims.setStatus("active");
           ssaModel.setOrg_id("0015800000jfQ9aAAE");
           ssaModel.setOrganisation_competent_authority_claims(orgClaims);
           RequestHeaderAttributes requestheaderAttributes=new RequestHeaderAttributes();
           requestheaderAttributes.setChannelId("OB_EXISTING_CERT");
           requestheaderAttributes.setX_ssl_client_cn("7Jk3WzvUwVYm2rEeLFK7hb");
           requestheaderAttributes.setX_ssl_client_ncaid("7Jk3WzvUwVYm2rEeLFK7hb");
           requestheaderAttributes.setMethodType("PUT");
           requestheaderAttributes.setTenantId("BOI");
           ReflectionTestUtils.setField(payloadValidationContext, "headerAttributes", requestheaderAttributes);
         
           payloadValidationContext.execute(regRequest, ssaModel,false);
       }

	   @Test(expected=ClientRegistrationException.class)
       public void testValidationForSubjectDnLegacyMigrationSpecialChararcterError(){
           Set<ValidationStrategy> payloadstrategies1 = new LinkedHashSet<ValidationStrategy>();
           payloadstrategies1.add(PayloadValidationStrategy.TLS_CLIENT_AUTH_DN);
           payloadValidationContext = new PayloadValidationContext(payloadstrategies1,null);
           ReflectionTestUtils.setField(payloadValidationContext, "wellKnownConfig", wellKnownConfig);
           regRequest.setTlsClientAuthSubjectDn("CN=0015800000jfQ9aAAE, OID.2.5.4.97=PSDGB-FCA-512956, O=OpenBanking.,\\//$čníÓ;&^#@$GB$, C=GA");
           Authorisation orgAuth=new Authorisation();
           orgAuth.setMember_state("GB");
           orgAuth.setRoles(new ArrayList<String>(Arrays.asList("PISP","AISP","CBPII")));
           OrganisationCompetentAuthorityClaims orgClaims = new OrganisationCompetentAuthorityClaims();
           orgClaims.setAuthority_id("FCAGBR");
           orgClaims.setRegistration_id("512956");
           orgClaims.setAuthorisations(new ArrayList<Authorisation>(Arrays.asList(orgAuth)));
           orgClaims.setStatus("active");
           ssaModel.setOrg_id("0015800000jfQ9aAAE");
           ssaModel.setOrganisation_competent_authority_claims(orgClaims);
           RequestHeaderAttributes requestheaderAttributes=new RequestHeaderAttributes();
           requestheaderAttributes.setChannelId("OB_EXISTING_CERT");
           requestheaderAttributes.setX_ssl_client_cn("7Jk3WzvUwVYm2rEeLFK7hb");
           requestheaderAttributes.setX_ssl_client_ncaid("7Jk3WzvUwVYm2rEeLFK7hb");
           requestheaderAttributes.setMethodType("PUT");
           requestheaderAttributes.setTenantId("BOI");
           ReflectionTestUtils.setField(payloadValidationContext, "headerAttributes", requestheaderAttributes);
         
           payloadValidationContext.execute(regRequest, ssaModel,false);
       }

       @Test
       public void testValidationForLegacyMigration(){
           Set<ValidationStrategy> payloadstrategies1 = new LinkedHashSet<ValidationStrategy>();
           payloadstrategies1.add(PayloadValidationStrategy.TLS_CLIENT_AUTH_DN);
           payloadValidationContext = new PayloadValidationContext(payloadstrategies1,null);
           ReflectionTestUtils.setField(payloadValidationContext, "wellKnownConfig", wellKnownConfig);
           regRequest.setTlsClientAuthSubjectDn("CN=0015800000jfQ9aAAE,OID.2.5.4.97=PSDGB-abc-512956 ,O=OpenBanking, C=GB");
           Authorisation orgAuth=new Authorisation();
           orgAuth.setMember_state("GB");
           orgAuth.setRoles(new ArrayList<String>(Arrays.asList("PISP","AISP","CBPII")));
           OrganisationCompetentAuthorityClaims orgClaims = new OrganisationCompetentAuthorityClaims();
           orgClaims.setAuthority_id("abcd");
           orgClaims.setRegistration_id("512956");
           orgClaims.setAuthorisations(new ArrayList<Authorisation>(Arrays.asList(orgAuth)));
           orgClaims.setStatus("active");
           ssaModel.setOrganisation_competent_authority_claims(orgClaims);
           RequestHeaderAttributes requestheaderAttributes=new RequestHeaderAttributes();
           requestheaderAttributes.setChannelId("OB_EXISTING_CERT");
           requestheaderAttributes.setX_ssl_client_cn("7Jk3WzvUwVYm2rEeLFK7hb");
           requestheaderAttributes.setX_ssl_client_ncaid("7Jk3WzvUwVYm2rEeLFK7hb");
           requestheaderAttributes.setMethodType("PUT");
           requestheaderAttributes.setTenantId("BOI");
           ReflectionTestUtils.setField(payloadValidationContext, "headerAttributes", requestheaderAttributes);
          
           payloadValidationContext.execute(regRequest, ssaModel,false);
       }
       
       @Test
       public void testValidationForQWACMigration(){
           Set<ValidationStrategy> payloadstrategies1 = new LinkedHashSet<ValidationStrategy>();
           payloadstrategies1.add(PayloadValidationStrategy.TLS_CLIENT_AUTH_DN);
           payloadValidationContext = new PayloadValidationContext(payloadstrategies1,null);
           ReflectionTestUtils.setField(payloadValidationContext, "wellKnownConfig", wellKnownConfig);
           regRequest.setTlsClientAuthSubjectDn("CN=0015800000jfQ9aAAE,OID.2.5.4.97=PSDGB-abc-512956 ,O=OpenBanking, C=GB");
           Authorisation orgAuth=new Authorisation();
           orgAuth.setMember_state("GB");
           orgAuth.setRoles(new ArrayList<String>(Arrays.asList("PISP","AISP","CBPII")));
           OrganisationCompetentAuthorityClaims orgClaims = new OrganisationCompetentAuthorityClaims();
           orgClaims.setAuthority_id("abcd");
           orgClaims.setRegistration_id("512956");
           orgClaims.setAuthorisations(new ArrayList<Authorisation>(Arrays.asList(orgAuth)));
           orgClaims.setStatus("active");
           ssaModel.setOrganisation_competent_authority_claims(orgClaims);
           RequestHeaderAttributes requestheaderAttributes=new RequestHeaderAttributes();
           requestheaderAttributes.setChannelId("OB_EIDAS_CERT");
           requestheaderAttributes.setX_ssl_client_cn("7Jk3WzvUwVYm2rEeLFK7hb");
           requestheaderAttributes.setX_ssl_client_ncaid("7Jk3WzvUwVYm2rEeLFK7hb");
           requestheaderAttributes.setMethodType("PUT");
           requestheaderAttributes.setTenantId("BOI");
           ReflectionTestUtils.setField(payloadValidationContext, "headerAttributes", requestheaderAttributes);
          
           payloadValidationContext.execute(regRequest, ssaModel,false);
       }

   
	   @Test(expected = ClientRegistrationException.class)
	   public void testValidationForEmptyRedirectUri(){
	       Set<ValidationStrategy> payloadstrategies1=new LinkedHashSet<ValidationStrategy>();
	       payloadstrategies1.add(PayloadValidationStrategy.REDIRECT_URIS);
	       ReflectionTestUtils.setField(payloadValidationContext, "headerAttributes", requestHeaderAttributes);
	       PayloadValidationContext cnxt=new PayloadValidationContext(payloadstrategies1,null);
	       SSAModel ssaModel=new SSAModel();
	       ssaModel.setSoftware_redirect_uris(new ArrayList<String>(Arrays.asList()));
	       cnxt.execute(regRequest, ssaModel,false);
	   }
	   
	   @Test(expected = ClientRegistrationException.class)
	   public void testValidationForInvalidRedirectUri(){
	       Set<ValidationStrategy> payloadstrategies1=new LinkedHashSet<ValidationStrategy>();
	       payloadstrategies1.add(PayloadValidationStrategy.REDIRECT_URIS);
	       ReflectionTestUtils.setField(payloadValidationContext, "headerAttributes", requestHeaderAttributes);
	       PayloadValidationContext cnxt=new PayloadValidationContext(payloadstrategies1,null);
	       SSAModel ssaModel=new SSAModel();
	       ssaModel.setSoftware_redirect_uris(new ArrayList<String>(Arrays.asList("google.com")));
	       cnxt.execute(regRequest, ssaModel,false);
	   }
	   
		@Test
		public void testValidationForScopes2() {
			Set<ValidationStrategy> payloadstrategies1 = new LinkedHashSet<ValidationStrategy>();
			payloadstrategies1.add(PayloadValidationStrategy.SCOPES);
			payloadValidationContext = new PayloadValidationContext(payloadstrategies1,null);
			Map<String, String>  issuer = new HashMap<>();
			issuer.put("BOI", "https://auth-test.apibank-cma2plus.in");
			issuer.put("BOIUK", "https://auth-test.apibank-cma2plus.in");
			wellKnownConfig1.getIssuer().putAll(issuer);
			wellKnownConfig1.setScopes(new ArrayList<String>(Arrays.asList("openid", "payments", "accounts")));
			wellKnownConfig1.setResponse_types_supported(new ArrayList<String>(Arrays.asList("code", "id_token")));
			wellKnownConfig1.setGrant_types_supported(
					new ArrayList<String>(Arrays.asList("authorization_code", "refresh_token", "client_credentials")));
			wellKnownConfig1.setId_token_signing_alg_values_supported(new ArrayList<String>(Arrays.asList("RS256")));
			wellKnownConfig1
					.setToken_endpoint_auth_methods_supported(new ArrayList<String>(Arrays.asList("client_secret_basic")));
			wellKnownConfig1
					.setRequest_object_signing_alg_values_supported(new ArrayList<String>(Arrays.asList("RS256", "PS256")));

			ReflectionTestUtils.setField(payloadValidationContext, "wellKnownConfig", wellKnownConfig1);
			regRequest.setScope("openid payments");
			payloadValidationContext.execute(regRequest, ssaModel,false);
			Assert.assertTrue(ssaModel.getSoftware_roles().contains("PISP"));
		}
}
