package com.capgemini.psd2.international.payments.reject.mock.foundationservice.service;

import com.capgemini.psd2.international.payments.reject.mock.foundationservice.domain.PaymentInstructionProposalInternational;
import com.capgemini.psd2.international.payments.reject.mock.foundationservice.exception.handler.RecordNotFoundException;

public interface InternationalPaymentRejectService {

	public PaymentInstructionProposalInternational createInternationalPaymentRejectResource(
			String paymentInstructionProposalId) throws RecordNotFoundException;

}
