package com.capgemini.psd2.international.payments.reject.mock.foundationservice.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.capgemini.psd2.international.payments.reject.mock.foundationservice.constants.InternationalPaymentRejectFoundationConstants;
import com.capgemini.psd2.international.payments.reject.mock.foundationservice.domain.PaymentInstructionProposalInternational;
import com.capgemini.psd2.international.payments.reject.mock.foundationservice.domain.ProposalStatus;
import com.capgemini.psd2.international.payments.reject.mock.foundationservice.exception.handler.RecordNotFoundException;
import com.capgemini.psd2.international.payments.reject.mock.foundationservice.repository.InternationalPaymentRejectRepository;
import com.capgemini.psd2.international.payments.reject.mock.foundationservice.service.InternationalPaymentRejectService;

@Service
public class InternationalPaymentRejectServiceImpl implements InternationalPaymentRejectService {

	@Autowired
	InternationalPaymentRejectRepository internationalPaymentRejectRepository;

	@Override
	public PaymentInstructionProposalInternational createInternationalPaymentRejectResource(
			String paymentInstructionProposalId) throws RecordNotFoundException {
		PaymentInstructionProposalInternational paymentInstructionProposalInternational = internationalPaymentRejectRepository
				.findByPaymentInstructionProposalId(paymentInstructionProposalId);
		if (null == paymentInstructionProposalInternational) {
			throw new RecordNotFoundException(InternationalPaymentRejectFoundationConstants.RECORD_NOT_FOUND);
		}
		else
			paymentInstructionProposalInternational.setProposalStatus(ProposalStatus.REJECTED);
		
		    internationalPaymentRejectRepository.save(paymentInstructionProposalInternational);

		return paymentInstructionProposalInternational;
	}

}