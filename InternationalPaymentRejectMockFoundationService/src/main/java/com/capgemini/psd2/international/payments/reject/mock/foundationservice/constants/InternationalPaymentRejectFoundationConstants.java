package com.capgemini.psd2.international.payments.reject.mock.foundationservice.constants;

public class InternationalPaymentRejectFoundationConstants {
	
	public static final String RECORD_NOT_FOUND = "Payment Instruction Proposal Record Not Found";
	public static final String INSTRUCTION_ENDTOEND_REF="Rejected";
}
