package com.capgemini.psd2.international.payments.reject.mock.foundationservice.exception.handler;

public class InvalidParameterRequestException extends Exception{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	public InvalidParameterRequestException(String s){
		super(s);
		
	}

}
