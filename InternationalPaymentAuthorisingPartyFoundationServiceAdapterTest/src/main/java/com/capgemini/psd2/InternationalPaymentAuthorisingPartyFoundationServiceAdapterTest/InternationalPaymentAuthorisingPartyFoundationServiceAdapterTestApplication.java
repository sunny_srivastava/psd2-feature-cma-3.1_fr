package com.capgemini.psd2.InternationalPaymentAuthorisingPartyFoundationServiceAdapterTest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("com.capgemini.psd2")
public class InternationalPaymentAuthorisingPartyFoundationServiceAdapterTestApplication {
	public static void main(String[] args) {
		SpringApplication.run(InternationalPaymentAuthorisingPartyFoundationServiceAdapterTestApplication.class, args);

	}
}
