package com.capgemini.psd2.InternationalPaymentAuthorisingPartyFoundationServiceAdapterTest;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.HttpStatus;

import com.capgemini.psd2.foundationservice.international.payment.authorisingparty.boi.adapter.InternationalPaymentAuthorisingPartyFoundationServiceAdapter;
import com.capgemini.psd2.foundationservice.international.payment.authorisingparty.boi.adapter.domain.PaymentInstructionProposalAuthorisingParty;
import com.capgemini.psd2.pisp.domain.CustomIPaymentConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.OBCashAccountDebtor3;
import com.capgemini.psd2.pisp.domain.OBInternational1;
import com.capgemini.psd2.pisp.domain.OBWriteDataInternationalConsentResponse1;
import com.capgemini.psd2.pisp.domain.OBWriteInternationalConsentResponse1;
import com.capgemini.psd2.pisp.enums.PaymentTypeEnum;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;

@RestController
@ResponseBody
public class TestInternationalPaymentAuthorisingPartyFoundationServiceAdapter {
	@Autowired
	private InternationalPaymentAuthorisingPartyFoundationServiceAdapter iPaymentAuthoriseFoundationServiceAdapter;
	
	@RequestMapping(value = "/testInternationalPaymentAuthorisingParty", method = RequestMethod.POST)
	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	public CustomIPaymentConsentsPOSTResponse internationalPaymentConsentsRetrieve(
		PaymentInstructionProposalAuthorisingParty paymentInstructionProposalAuthorisingParty) {
		
		CustomPaymentStageIdentifiers customPaymentStageIdentifiers = new CustomPaymentStageIdentifiers();
		customPaymentStageIdentifiers.setPaymentConsentId("0fca37a5-40fa-41fe-91c7-9e48453d9581");
		customPaymentStageIdentifiers.setPaymentSetupVersion("3.0");
		customPaymentStageIdentifiers.setPaymentTypeEnum(PaymentTypeEnum.INTERNATIONAL_PAY);
	  	
		//request body
		OBWriteInternationalConsentResponse1 oBWriteInternationalConsentResponse1 = new OBWriteInternationalConsentResponse1();
		OBWriteDataInternationalConsentResponse1 oBWriteDataInternationalConsentResponse1 = new OBWriteDataInternationalConsentResponse1();
		OBInternational1 oBInternational1 = new OBInternational1();
		OBCashAccountDebtor3 oBCashAccountDebtor3 = new OBCashAccountDebtor3();
		oBCashAccountDebtor3.setSchemeName("UK.OBIE.SortCodeAccountNumber");
		oBCashAccountDebtor3.setName("Debtor Account Name");
		oBCashAccountDebtor3.setIdentification("08080021325698");
		oBCashAccountDebtor3.setSecondaryIdentification("555555");
		oBInternational1.setDebtorAccount(oBCashAccountDebtor3);
		oBWriteDataInternationalConsentResponse1.setInitiation(oBInternational1);
		oBWriteInternationalConsentResponse1.setData(oBWriteDataInternationalConsentResponse1);
		
		
		
		Map<String, String> params = new HashMap<String, String>();
		params.put("X-TRANSACTION-ID","PISP");
		params.put("X-CORRELATION-ID","BOL");
		params.put("X-SOURCE-SYSTEM","BOI999");
		params.put("X-SOURCE-USER","platform");
		params.put("X-CHANNEL-CODE","87654321");
		params.put("X-BOI-CHANNEL","BOIUK");
		params.put("tenant_id","BOIUK");
		params.put("X-PARTY-SOURCE-ID-NUMBER","BOIUK");
		params.put("X-BOI-USER","BOIUK");
		
		return iPaymentAuthoriseFoundationServiceAdapter.createStagingInternationalPaymentConsents(oBWriteInternationalConsentResponse1, customPaymentStageIdentifiers, params);
	}
}
