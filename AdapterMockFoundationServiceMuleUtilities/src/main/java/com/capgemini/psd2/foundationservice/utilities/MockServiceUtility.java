package com.capgemini.psd2.foundationservice.utilities;

import java.util.HashMap;
import java.util.Map;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Component;

@EnableConfigurationProperties
@ConfigurationProperties("mockFoundationService")
@Component
public class MockServiceUtility {

	private Map<String, String> errorCode = new HashMap<>();

	private Map<String, String> validationError = new HashMap<>();
	
	private Map<String, String> errorField = new HashMap<>();

	public Map<String, String> getErrorField() {
		return errorField;
	}

	public void setErrorField(Map<String, String> errorField) {
		this.errorField = errorField;
	}

	public Map<String, String> getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(Map<String, String> errorCode) {
		this.errorCode = errorCode;
	}

	public Map<String, String> getValidationError() {
		return validationError;
	}

	public void setValidationError(Map<String, String> validationError) {
		this.validationError = validationError;
	}

}
