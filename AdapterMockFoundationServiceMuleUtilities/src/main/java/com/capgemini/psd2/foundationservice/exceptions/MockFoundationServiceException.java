package com.capgemini.psd2.foundationservice.exceptions;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.capgemini.psd2.foundationservice.exceptions.domain.CommonResponse;
import com.capgemini.psd2.foundationservice.exceptions.domain.CommonResponse.TypeEnum;
import com.capgemini.psd2.foundationservice.exceptions.domain.CommonResponseAdditionalInformation;
import com.capgemini.psd2.foundationservice.exceptions.domain.CommonResponseAdditionalInformation.TypeEnum1;
import com.capgemini.psd2.foundationservice.exceptions.domain.ValidationViolation;
import com.capgemini.psd2.foundationservice.exceptions.domain.ValidationViolations;
import com.capgemini.psd2.foundationservice.exceptions.domain.ValidationWrapper;
import com.capgemini.psd2.foundationservice.utilities.NullCheckUtils;

public class MockFoundationServiceException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	private final CommonResponse commonResponse;

	public MockFoundationServiceException(String message, CommonResponse commonResponse) {
		super(message);
		this.commonResponse = commonResponse;
	}

	public CommonResponse getErrorInfo() {
		return commonResponse;
	}

	public static MockFoundationServiceException populateMockFoundationServiceException(ErrorCodeEnum errorCodeEnum) {

		CommonResponse commonResponse = new CommonResponse();
		commonResponse.setApi(errorCodeEnum.getApi());
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		commonResponse.setTimestamp(format.format(new Date()));
		commonResponse.setCode(errorCodeEnum.getErrorCode());
		commonResponse.setType(TypeEnum.ERROR);
		commonResponse.setSummary(errorCodeEnum.getSummary());
		CommonResponseAdditionalInformation additionalInformation = null;

		if (NullCheckUtils.isNullOrEmpty(errorCodeEnum.getFsErrorCode())) {
			additionalInformation = new CommonResponseAdditionalInformation();
			additionalInformation.setType(TypeEnum1.TEXT);
			additionalInformation.setDetails(errorCodeEnum.getDescription());
			commonResponse.setAdditionalInformation(additionalInformation);
		} else {
			additionalInformation = new CommonResponseAdditionalInformation();
			additionalInformation.setType(TypeEnum1.ERROR);
			ValidationWrapper validationWrapper = new ValidationWrapper();
			ValidationViolations validationViolations = new ValidationViolations();
			ValidationViolation validationViolation = new ValidationViolation();
			validationViolation.setErrorCode(errorCodeEnum.getFsErrorCode());
			validationViolation.setErrorText(errorCodeEnum.getDescription());
			validationViolations.setValidationViolation(validationViolation);
			validationWrapper.setValidationViolations(validationViolations);
			additionalInformation.setDetails(validationWrapper);
			commonResponse.setAdditionalInformation(additionalInformation);
		}
		return new MockFoundationServiceException(errorCodeEnum.getStatus().toString(), commonResponse);
	}
	
	public static MockFoundationServiceException populateMockFoundationServiceException(ErrorCodeEnum errorCodeEnum,String field) {

		CommonResponse commonResponse = new CommonResponse();
		commonResponse.setApi(errorCodeEnum.getApi());
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		commonResponse.setTimestamp(format.format(new Date()));
		commonResponse.setCode(errorCodeEnum.getErrorCode());
		commonResponse.setType(TypeEnum.ERROR);
		commonResponse.setSummary(errorCodeEnum.getSummary());
		CommonResponseAdditionalInformation additionalInformation = null;

		if (NullCheckUtils.isNullOrEmpty(errorCodeEnum.getFsErrorCode())) {
			additionalInformation = new CommonResponseAdditionalInformation();
			additionalInformation.setType(TypeEnum1.TEXT);
			additionalInformation.setDetails(errorCodeEnum.getDescription());
			commonResponse.setAdditionalInformation(additionalInformation);
		} else {
			additionalInformation = new CommonResponseAdditionalInformation();
			additionalInformation.setType(TypeEnum1.ERROR);
			ValidationWrapper validationWrapper = new ValidationWrapper();
			ValidationViolations validationViolations = new ValidationViolations();
			ValidationViolation validationViolation = new ValidationViolation();
			validationViolation.setErrorCode(errorCodeEnum.getFsErrorCode());
			validationViolation.setErrorField(field);
			validationViolation.setErrorText(errorCodeEnum.getDescription());
			validationViolations.setValidationViolation(validationViolation);
			validationWrapper.setValidationViolations(validationViolations);
			additionalInformation.setDetails(validationWrapper);
			commonResponse.setAdditionalInformation(additionalInformation);
		}
		return new MockFoundationServiceException(errorCodeEnum.getStatus().toString(), commonResponse);
	}
}
