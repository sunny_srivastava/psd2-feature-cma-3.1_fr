package com.capgemini.psd2.foundationservice.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.foundationservice.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.foundationservice.exceptions.MockFoundationServiceException;
import com.capgemini.psd2.foundationservice.exceptions.MuleOutofBoxPolicyException;
import com.capgemini.psd2.foundationservice.utilities.MockServiceUtility;
import com.capgemini.psd2.foundationservice.utilities.NullCheckUtils;

@Component
public class ValidationUtility {

	@Autowired
	private MockServiceUtility mockServiceUtility;

	public void validateErrorCode(String correlationId) throws Exception{

		if (!NullCheckUtils.isNullOrEmpty(correlationId)) {
			String code = correlationId.substring(correlationId.length() - 4);
			//Mule out box policy handling
			if(code.equalsIgnoreCase("0401")){
				throw new MuleOutofBoxPolicyException("UNAUTHORIZED");
			}else if(code.equalsIgnoreCase("0429")){
				throw new MuleOutofBoxPolicyException("TOO_MANY_REQUESTS");
			}
			else if(code.equalsIgnoreCase("0415")){
				throw new MuleOutofBoxPolicyException("UNSUPPORTED_MEDIA_TYPE");
			}
			else{
				System.out.println("code======"+code);
				String errorCode = mockServiceUtility.getErrorCode().get(code);
				System.out.println("errorCode======"+errorCode);
				ErrorCodeEnum errorCodeEnum= null;
				if(errorCode !=null){
					errorCodeEnum = ErrorCodeEnum.valueOf(errorCode);
					System.out.println("errorCodeEnum========"+errorCodeEnum);
					throw MockFoundationServiceException.populateMockFoundationServiceException(errorCodeEnum);
			  }
			}	
		  }
	}
	
	public void validateMockBusinessValidations(String endToEndIdentification) {


		if (!NullCheckUtils.isNullOrEmpty(endToEndIdentification)) {
			 String code = endToEndIdentification.substring(endToEndIdentification.length() - 11);
			 String errorCode = mockServiceUtility.getErrorCode().get(code);
			 ErrorCodeEnum errorCodeEnum = null;
			
			//String validationError = mockServiceUtility.getValidationError().get(endToEndIdentification);
			
			if (!NullCheckUtils.isNullOrEmpty(errorCode)) {
				errorCodeEnum = ErrorCodeEnum.valueOf(errorCode);
				throw MockFoundationServiceException.populateMockFoundationServiceException(errorCodeEnum);
			}
			
		}
	}
	
	public void validateBOLUser(String userId){
		String errorCode = mockServiceUtility.getValidationError().get(userId);
		ErrorCodeEnum errorCodeEnum= null;
		if(errorCode !=null){
			errorCodeEnum = ErrorCodeEnum.valueOf(errorCode);
			throw MockFoundationServiceException.populateMockFoundationServiceException(errorCodeEnum);
	}
	}
	
	
	public void validateErrorCodeForConsent(String userId) throws Exception{

		if (!NullCheckUtils.isNullOrEmpty(userId)) {
			if(userId.equalsIgnoreCase("10000401"))
				throw new MuleOutofBoxPolicyException("UNAUTHORIZED");
			else if(userId.equalsIgnoreCase("10000429"))
				throw new MuleOutofBoxPolicyException("TOO_MANY_REQUESTS");
			else if(userId.equalsIgnoreCase("10000415"))
				throw new MuleOutofBoxPolicyException("UNSUPPORTED_MEDIA_TYPE");
			
			else{
				String errorCode = mockServiceUtility.getValidationError().get(userId);
				ErrorCodeEnum errorCodeEnum= null;
				if(errorCode !=null){
					errorCodeEnum = ErrorCodeEnum.valueOf(errorCode);
					throw MockFoundationServiceException.populateMockFoundationServiceException(errorCodeEnum);
			}
		  }
		}		
	}

	public void validateExecutionErrorCode(String instructionReference) throws Exception {

		if (!NullCheckUtils.isNullOrEmpty(instructionReference)) {
			if (instructionReference.equalsIgnoreCase("401"))
				throw new MuleOutofBoxPolicyException("UNAUTHORIZED");
			else if (instructionReference.equalsIgnoreCase("429"))
				throw new MuleOutofBoxPolicyException("TOO_MANY_REQUESTS");
			else {
				String validationError = mockServiceUtility.getValidationError().get(instructionReference);

				ErrorCodeEnum errorCodeEnum = null;

				if (validationError != null) {
					errorCodeEnum = ErrorCodeEnum.valueOf(validationError);
					throw MockFoundationServiceException.populateMockFoundationServiceException(errorCodeEnum);
				}
			}
		}

	}

	public void validateExecutionErrorCodeForMuleOutOfBoxPolicy(String instructionIdentification) throws Exception  {
		if (!NullCheckUtils.isNullOrEmpty(instructionIdentification)) {
			if (instructionIdentification.equalsIgnoreCase("601"))
				throw new MuleOutofBoxPolicyException("UNAUTHORIZED");
			else if (instructionIdentification.equalsIgnoreCase("629"))
				throw new MuleOutofBoxPolicyException("TOO_MANY_REQUESTS");
		}
	}
	public void validateExecutionErrorCodeForMuleOutOfBoxPolicyForUpdate(String instructionIdentification) throws Exception  {
		if (!NullCheckUtils.isNullOrEmpty(instructionIdentification)) {
			if (instructionIdentification.equalsIgnoreCase("801"))
				throw new MuleOutofBoxPolicyException("UNAUTHORIZED");
			else if (instructionIdentification.equalsIgnoreCase("829"))
				throw new MuleOutofBoxPolicyException("TOO_MANY_REQUESTS");
		}
	}
	
	public void validateMockBusinessValidationsForSchedulePayment(String instructionIdentification,String field) throws Exception {

		if (!NullCheckUtils.isNullOrEmpty(instructionIdentification)) {
			if (instructionIdentification.equalsIgnoreCase("401"))
				throw new MuleOutofBoxPolicyException("UNAUTHORIZED");
			else if (instructionIdentification.equalsIgnoreCase("429"))
				throw new MuleOutofBoxPolicyException("TOO_MANY_REQUESTS");
			else {
				String errorCode = mockServiceUtility.getErrorCode().get(instructionIdentification);
				ErrorCodeEnum errorCodeEnum = null;

				if (!NullCheckUtils.isNullOrEmpty(errorCode)) {
					errorCodeEnum = ErrorCodeEnum.valueOf(errorCode);
					if(instructionIdentification.equals("FS_SOPSV_001"))
						throw MockFoundationServiceException.populateMockFoundationServiceException(errorCodeEnum, field);
						else
					throw MockFoundationServiceException.populateMockFoundationServiceException(errorCodeEnum);
				}
			}
		}
	}
	
	public void validateMockBusinessValidationsForDSOSubmission(String instructionIdentification,String field) throws Exception {

		if (!NullCheckUtils.isNullOrEmpty(instructionIdentification)) {
			
				String errorCode = mockServiceUtility.getValidationError().get(instructionIdentification);
				ErrorCodeEnum errorCodeEnum = null;

				if (!NullCheckUtils.isNullOrEmpty(errorCode)) {
					errorCodeEnum = ErrorCodeEnum.valueOf(errorCode);
					if(instructionIdentification.equals("FS_SOE_001"))
						throw MockFoundationServiceException.populateMockFoundationServiceException(errorCodeEnum, field);
					else
					throw MockFoundationServiceException.populateMockFoundationServiceException(errorCodeEnum);
				}
			
		}
	}

	
	public void validateMockBusinessValidationsForSchedulePayment(String instructionIdentification) throws Exception {

		if (!NullCheckUtils.isNullOrEmpty(instructionIdentification)) {
			if (instructionIdentification.equalsIgnoreCase("401"))
				throw new MuleOutofBoxPolicyException("UNAUTHORIZED");
			else if (instructionIdentification.equalsIgnoreCase("429"))
				throw new MuleOutofBoxPolicyException("TOO_MANY_REQUESTS");
			else {
				String errorCode = mockServiceUtility.getErrorCode().get(instructionIdentification);
				ErrorCodeEnum errorCodeEnum = null;

				if (!NullCheckUtils.isNullOrEmpty(errorCode)) {
					errorCodeEnum = ErrorCodeEnum.valueOf(errorCode);
					throw MockFoundationServiceException.populateMockFoundationServiceException(errorCodeEnum);
				}
			}
		}
	}
	
	public void validateMockBusinessValidationsForAuthorisingParty(String instructionIdentification) throws Exception {

		if (!NullCheckUtils.isNullOrEmpty(instructionIdentification)) {
			if (instructionIdentification.equalsIgnoreCase("40000401"))
				throw new MuleOutofBoxPolicyException("UNAUTHORIZED");
			else if (instructionIdentification.equalsIgnoreCase("40000429"))
				throw new MuleOutofBoxPolicyException("TOO_MANY_REQUESTS");
			else {
				String errorCode = mockServiceUtility.getErrorCode().get(instructionIdentification);
				ErrorCodeEnum errorCodeEnum = null;

				if (!NullCheckUtils.isNullOrEmpty(errorCode)) {
					errorCodeEnum = ErrorCodeEnum.valueOf(errorCode);
					throw MockFoundationServiceException.populateMockFoundationServiceException(errorCodeEnum);
				}
			}
		}
	}
	
	 
	public void sca_authentication_errorcode_Validation(String userId) throws Exception  {
			
				String errorCode = mockServiceUtility.getErrorCode().get(userId);
				ErrorCodeEnum errorCodeEnum = null;

				if (!NullCheckUtils.isNullOrEmpty(errorCode)) {
					errorCodeEnum = ErrorCodeEnum.valueOf(errorCode);
					throw MockFoundationServiceException.populateMockFoundationServiceException(errorCodeEnum);
					
				}
			
		} 
}
