package com.capgemini.psd2.account.product.routing.adapter.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.account.product.routing.adapter.routing.AccountProductAdapterFactory;
import com.capgemini.psd2.aisp.adapter.AccountProductsAdapter;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountProductsResponse;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Component("accountProductRoutingAdapter")
public class AccountProductRoutingAdapter implements AccountProductsAdapter {
	/** The account product adapter factory. */
	@Autowired
	private AccountProductAdapterFactory accountProductAdapterFactory;
	
	@Autowired
	private RequestHeaderAttributes reqHeaderAtrributes;

	/** The default adapter. */
	@Value("${app.defaultAccountProductsAdapter}")
	private String defaultAdapter;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.capgemini.psd2.aisp.adapter.AccountInformationAdapter#
	 * retrieveAccountInformation(com.capgemini.psd2.aisp.domain.AccountMapping,
	 * java.util.Map)
	 */
	@Override
	public PlatformAccountProductsResponse retrieveAccountProducts(AccountMapping accountMapping,
			Map<String, String> params) {
		AccountProductsAdapter accountProductsAdapter = accountProductAdapterFactory.getAdapterInstance(defaultAdapter);

		return accountProductsAdapter.retrieveAccountProducts(accountMapping, addHeaderParams(params));
	}
	
	private Map<String, String> addHeaderParams(Map<String, String> mapParam) {
		Map<String,String> mapParamResult;
		if (NullCheckUtils.isNullOrEmpty(mapParam))
			 mapParamResult = new HashMap<>();
        else mapParamResult=mapParam;

		if (null != reqHeaderAtrributes.getToken() && null != reqHeaderAtrributes.getToken().getSeviceParams()) {
			mapParamResult.put(PSD2Constants.CHANNEL_IN_REQ_HEADER,
					reqHeaderAtrributes.getToken().getSeviceParams().get(PSD2Constants.CHANNEL_NAME));
		}
		mapParamResult.put(PSD2Constants.USER_IN_REQ_HEADER, reqHeaderAtrributes.getPsuId());
		mapParamResult.put(PSD2Constants.CORRELATION_REQ_HEADER, reqHeaderAtrributes.getCorrelationId());
		mapParamResult.put(PSD2Constants.TENANT_ID, reqHeaderAtrributes.getTenantId());
		return mapParamResult;
	}

}
