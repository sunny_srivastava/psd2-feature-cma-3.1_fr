package com.capgemini.psd2.account.product.routing.adapter.test.routing;


import java.util.ArrayList;
import java.util.List;

import com.capgemini.psd2.aisp.domain.OBReadProduct2;
import com.capgemini.psd2.aisp.domain.OBReadProduct2Data;
import com.capgemini.psd2.aisp.domain.OBReadProduct2DataProduct;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountProductsResponse;

public class AccountProductRoutingAdapterTestMockData 
{
	public static PlatformAccountProductsResponse getMockProductGETResponse() {
		PlatformAccountProductsResponse platformAccountProductsResponse = new PlatformAccountProductsResponse();
		OBReadProduct2Data data = new OBReadProduct2Data();
		List<OBReadProduct2DataProduct> product = new ArrayList<>();
		data.setProduct(product);
		OBReadProduct2 obReadProduct2 = new OBReadProduct2();
		obReadProduct2.setData(data);
		platformAccountProductsResponse.setoBReadProduct2(obReadProduct2);
		return platformAccountProductsResponse;
	}
}