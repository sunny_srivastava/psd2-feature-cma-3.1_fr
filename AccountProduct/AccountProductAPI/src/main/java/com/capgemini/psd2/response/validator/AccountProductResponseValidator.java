package com.capgemini.psd2.response.validator;

import java.util.List;

import org.springframework.stereotype.Component;

import com.capgemini.psd2.aisp.domain.OBReadProduct2;
import com.capgemini.psd2.aisp.domain.OBReadProduct2DataProduct;
import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.InternalServerErrorMessage;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;

@Component
public class AccountProductResponseValidator extends ResponseValidator {

	@Override
	public void performPlatformValidation(Object tppProductsResponse) {

		OBReadProduct2 input = (OBReadProduct2) tppProductsResponse;
		List<OBReadProduct2DataProduct> productList=input.getData().getProduct();
		if(productList.isEmpty())
			return;
		OBReadProduct2DataProduct product = input.getData().getProduct().get(0);

		if ((product.getBCA() != null)
				&& product.getProductType() != OBReadProduct2DataProduct.ProductTypeEnum.BUSINESSCURRENTACCOUNT) {
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
					InternalServerErrorMessage.INVALID_PRODUCT_TYPE));
		}

		if ((product.getPCA() != null)
				&& product.getProductType() != OBReadProduct2DataProduct.ProductTypeEnum.PERSONALCURRENTACCOUNT) {
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
					InternalServerErrorMessage.INVALID_PRODUCT_TYPE));
		}

		if ((product.getOtherProductType() != null) && product.getProductType() != OBReadProduct2DataProduct.ProductTypeEnum.OTHER) {
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
					InternalServerErrorMessage.INVALID_PRODUCT_TYPE));
		}

		if (product.getProductType() == OBReadProduct2DataProduct.ProductTypeEnum.OTHER && product.getOtherProductType() == null) {
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
					InternalServerErrorMessage.MISSING_OTHER_PRODUCT_TYPE));
		}
	}
}
