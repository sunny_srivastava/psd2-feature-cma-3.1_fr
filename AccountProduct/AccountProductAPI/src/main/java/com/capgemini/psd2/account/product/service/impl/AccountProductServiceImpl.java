package com.capgemini.psd2.account.product.service.impl;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.capgemini.psd2.account.product.service.AccountProductService;
import com.capgemini.psd2.aisp.account.mapping.adapter.AccountMappingAdapter;
import com.capgemini.psd2.aisp.adapter.AccountProductsAdapter;
import com.capgemini.psd2.aisp.adapter.AispConsentAdapter;
import com.capgemini.psd2.aisp.domain.Links;
import com.capgemini.psd2.aisp.domain.Meta;
import com.capgemini.psd2.aisp.domain.OBReadProduct2;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountProductsResponse;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.consent.domain.AispConsent;
import com.capgemini.psd2.logger.LoggerUtils;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.aisp.validation.adapter.AISPCustomValidator;

@Service
public class AccountProductServiceImpl implements AccountProductService {

	/** The req header atrributes. */
	@Autowired
	private RequestHeaderAttributes reqHeaderAtrributes;

	/** The aisp consent adapter. */
	@Autowired
	private AispConsentAdapter aispConsentAdapter;

	/** The account mapping adapter. */
	@Autowired
	private AccountMappingAdapter accountMappingAdapter;

	/** The account product adapter. */
	@Autowired
	@Qualifier("accountProductRoutingAdapter")
	private AccountProductsAdapter accountProductAdapter;

	@SuppressWarnings("rawtypes")
	@Autowired
	@Qualifier("accountsRoutingValidator")
	private AISPCustomValidator<?, OBReadProduct2> accountsCustomValidator;
	
	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(AccountProductServiceImpl.class);

	/** The logger utils. */
	@Autowired
	private LoggerUtils loggerUtils;

	@Override
	public OBReadProduct2 retrieveAccountProduct(String accountId) {

		accountsCustomValidator.validateUniqueId(accountId);
		
		// Get consents by accountId and validate.
		AispConsent aispConsent = aispConsentAdapter.validateAndRetrieveConsentByAccountId(accountId,
				reqHeaderAtrributes.getToken().getConsentTokenData().getConsentId());

		// Get account mapping.
		AccountMapping accountMapping = accountMappingAdapter.retrieveAccountMappingDetails(accountId, aispConsent);

		Map<String,String> params=reqHeaderAtrributes.getToken().getSeviceParams();
		params.put(PSD2Constants.CMAVERSION, aispConsent.getCmaVersion());
		params.put(PSD2Constants.CO_RELATION_ID, reqHeaderAtrributes.getCorrelationId());
		
		// Retrieve account product.
		PlatformAccountProductsResponse platformProductsResponse = accountProductAdapter
				.retrieveAccountProducts(accountMapping, params);

		// Get CMA2 product response.
		OBReadProduct2 tppProductsResponse = platformProductsResponse.getObReadProduct2();

		// update platform response.
		updatePlatformResponse(tppProductsResponse);

		// Apply platform and CMA2-swagger validations.
		accountsCustomValidator.validateResponseParams(tppProductsResponse);

		return tppProductsResponse;
	}

	private void updatePlatformResponse(OBReadProduct2 tppProductsResponse) {

		LOG.info("{\"Enter\":\"{}\",\"{}\"}",
				"com.capgemini.psd2.account.product.service.impl.updatePlatformResponse()",
				loggerUtils.populateLoggerData("updatePlatformResponse"));

		if (tppProductsResponse.getLinks() == null)
			tppProductsResponse.setLinks(new Links());
		tppProductsResponse.getLinks().setSelf(reqHeaderAtrributes.getSelfUrl());

		if (tppProductsResponse.getMeta() == null)
			tppProductsResponse.setMeta(new Meta());
		tppProductsResponse.getMeta().setTotalPages(1);

		LOG.info("{\"Exit\":\"{}\",\"{}\"}",
				"com.capgemini.psd2.account.product.service.impl.updatePlatformResponse()",
				loggerUtils.populateLoggerData("updatePlatformResponse"));
	}
}