package com.capgemini.psd2.account.product.test.mock.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.capgemini.psd2.aisp.domain.Links;
import com.capgemini.psd2.aisp.domain.Meta;
import com.capgemini.psd2.aisp.domain.OBReadProduct2;
import com.capgemini.psd2.aisp.domain.OBReadProduct2Data;
import com.capgemini.psd2.aisp.domain.OBReadProduct2DataProduct;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountProductsResponse;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.consent.domain.AispConsent;
import com.capgemini.psd2.enums.ConsentStatusEnum;
import com.capgemini.psd2.logger.LoggerAttribute;
import com.capgemini.psd2.token.ConsentTokenData;
import com.capgemini.psd2.token.Token;

public class AccountProductMockData {

	/** The mock token. */
	public static Token mockToken;

	/**
	 * Gets the token.
	 *
	 * @return the token
	 */
	public static Token getToken() {
		mockToken = new Token();
		ConsentTokenData consentTokenData = new ConsentTokenData();
		consentTokenData.setConsentExpiry("1509348259877L");
		consentTokenData.setConsentId("12345");
		mockToken.setConsentTokenData(consentTokenData);
		Map<String,String> map=new HashMap<>();
		mockToken.setSeviceParams(map);
		return mockToken;
	}

	/**
	 * Gets the mock account mapping.
	 *
	 * @return the mock account mapping
	 */
	public static AccountMapping getMockAccountMapping() {
		AccountMapping mapping = new AccountMapping();
		mapping.setTppCID("6443e15975554bce8099e35b88b40465");
		mapping.setPsuId("88888888");

		List<AccountDetails> selectedAccounts = new ArrayList<>();
		AccountDetails accountRequest = new AccountDetails();

		accountRequest.setAccountId("f4483fda-81be-4873-b4a6-20375b7f55c1");
		accountRequest.setAccountNSC("903779");
		accountRequest.setAccountNumber("76528776");
		selectedAccounts.add(accountRequest);
		mapping.setAccountDetails(selectedAccounts);
		return mapping;
	}

	public static AispConsent getMockAispConsent() {
		AispConsent aispConsent = new AispConsent();
		aispConsent.setTppCId("6443e15975554bce8099e35b88b40465");
		aispConsent.setPsuId("88888888");
		aispConsent.setConsentId("39032481");
		aispConsent.setAccountRequestId("ae8c4441-f783-4e80-8810-254241bed98c");
		aispConsent.setChannelId("B365");
		aispConsent.setStartDate("2017-08-09T06:44:31.250Z");
		aispConsent.setEndDate("2018-09-02T00:00:00.875");
		aispConsent.setTransactionFromDateTime("2015-05-03T00:00:00.800");
		aispConsent.setTransactionToDateTime("2018-12-03T00:00:00.345");
		aispConsent.setStatus(ConsentStatusEnum.AUTHORISED);

		List<AccountDetails> selectedAispConsentAccountDetails = new ArrayList<>();
		AccountDetails accountRequest = new AccountDetails();

		accountRequest.setAccountId("f4483fda-81be-4873-b4a6-20375b7f55c1");
		accountRequest.setAccountNSC("903779");
		accountRequest.setAccountNumber("76528776");

		selectedAispConsentAccountDetails.add(accountRequest);
		aispConsent.setAccountDetails(selectedAispConsentAccountDetails);

		return aispConsent;
	}
	
	public static LoggerAttribute getMockLoggerData() {
		LoggerAttribute x = new LoggerAttribute();
		x.setApiId("testApiID");
		return x;
	}

	public static PlatformAccountProductsResponse getMockPlatformResponseWithoutLinksMeta() {
		PlatformAccountProductsResponse platformAccountProductsResponse = new PlatformAccountProductsResponse();
		OBReadProduct2Data data = new OBReadProduct2Data();
		List<OBReadProduct2DataProduct> product = new ArrayList<>();
		OBReadProduct2DataProduct p1 = new OBReadProduct2DataProduct();
		p1.setAccountId("123");
		p1.setProductId("TestProductId");
		product.add(p1);
		data.setProduct(product);
		OBReadProduct2 obReadProduct2 = new OBReadProduct2();
		obReadProduct2.setData(data);
		platformAccountProductsResponse.setoBReadProduct2(obReadProduct2);
		return platformAccountProductsResponse;
	}

	public static PlatformAccountProductsResponse getMockPlatformResponseWithLinksMeta() {
		PlatformAccountProductsResponse platformAccountProductsResponse = new PlatformAccountProductsResponse();
		OBReadProduct2Data data = new OBReadProduct2Data();
		List<OBReadProduct2DataProduct> productList = new ArrayList<>();
		OBReadProduct2DataProduct p1 = new OBReadProduct2DataProduct();
		p1.setAccountId("f4483fda-81be-4873-b4a6-20375b7f55c1");
		p1.setProductId("TestProductId");
		productList.add(p1);
		data.setProduct(productList);
		OBReadProduct2 obReadProduct2 = new OBReadProduct2();
		obReadProduct2.setData(data);

		Links links = new Links();
		obReadProduct2.setLinks(links);
		Meta meta = new Meta();
		obReadProduct2.setMeta(meta);

		platformAccountProductsResponse.setoBReadProduct2(obReadProduct2);
		return platformAccountProductsResponse;
	}

	

	public static OBReadProduct2 getMockExpectedResponseWithoutLinkMeta() {
		OBReadProduct2 product1 = new OBReadProduct2();
		OBReadProduct2Data data = new OBReadProduct2Data();
		List<OBReadProduct2DataProduct> productList = new ArrayList<>();
		OBReadProduct2DataProduct pr1 = new OBReadProduct2DataProduct();
		pr1.setAccountId("123");
		pr1.setProductId("TestProductId");
		productList.add(pr1);
		data.setProduct(productList);
		product1.setData(data);
		Links links = new Links();
		Meta meta = new Meta();
		meta.setTotalPages(1);
		product1.setLinks(links);
		product1.setMeta(meta);
		return product1;
	}

	public static Object getMockExpectedResponseWithLinksMeta() {
		OBReadProduct2 product1 = new OBReadProduct2();
		OBReadProduct2Data data = new OBReadProduct2Data();
		List<OBReadProduct2DataProduct> productList = new ArrayList<>();
		OBReadProduct2DataProduct pr1 = new OBReadProduct2DataProduct();
		pr1.setAccountId("f4483fda-81be-4873-b4a6-20375b7f55c1");
		pr1.setProductId("TestProductId");
		productList.add(pr1);
		data.setProduct(productList);
		product1.setData(data);
		Links links = new Links();
		Meta meta = new Meta();
		meta.setTotalPages(1);
		product1.setLinks(links);
		product1.setMeta(meta);
		return product1;
	}

}
