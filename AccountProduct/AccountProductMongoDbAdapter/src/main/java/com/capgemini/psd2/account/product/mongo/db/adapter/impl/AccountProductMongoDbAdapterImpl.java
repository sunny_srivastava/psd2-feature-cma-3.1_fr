package com.capgemini.psd2.account.product.mongo.db.adapter.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.account.product.mongo.db.adapter.repository.AccountProductRepository;
import com.capgemini.psd2.aisp.adapter.AccountProductsAdapter;
import com.capgemini.psd2.aisp.domain.OBReadProduct2;
import com.capgemini.psd2.aisp.domain.OBReadProduct2Data;
import com.capgemini.psd2.aisp.domain.OBReadProduct2DataProduct;
import com.capgemini.psd2.aisp.mongo.db.adapter.domain.AccountProductCMA2;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountProductsResponse;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.InternalServerErrorMessage;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.LoggerUtils;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.utilities.SandboxValidationUtility;

@Component
public class AccountProductMongoDbAdapterImpl implements AccountProductsAdapter {

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(AccountProductMongoDbAdapterImpl.class);
	
	@Autowired
	private AccountProductRepository repository; 
	
	/** The logger utils. */
	@Autowired
	private LoggerUtils loggerUtils;
	 
	@Autowired
	private SandboxValidationUtility utility;

	@Override
	public PlatformAccountProductsResponse retrieveAccountProducts(AccountMapping accountMapping,
			Map<String, String> params) {

		LOG.info("{\"Enter\":\"{}\",\"{}\"}",
				"com.capgemini.psd2.account.product.mongo.db.adapter.impl.retrieveAccountProducts()",
				loggerUtils.populateLoggerData("retrieveAccountProducts"));
		
		PlatformAccountProductsResponse platformAccountProductsResponse = new PlatformAccountProductsResponse();
		OBReadProduct2Data oBReadProduct2Data = new OBReadProduct2Data();
		OBReadProduct2 obReadProduct2 = new OBReadProduct2();
		AccountProductCMA2 mockProduct = null;
		LOG.info("TenantId : " + params.get(PSD2Constants.TENANT_ID));
		
		try {
			if(utility.isValidPsuId(accountMapping.getPsuId())){
				throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,InternalServerErrorMessage.SANDBOX_MOCKED_ERROR));
			}
			mockProduct = repository.findByAccountNumberAndAccountNSC(
					accountMapping.getAccountDetails().get(0).getAccountNumber(),
					accountMapping.getAccountDetails().get(0).getAccountNSC());
		} catch (DataAccessResourceFailureException e) {
			LOG.error(e.getMessage(),e);
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,InternalServerErrorMessage.CONNECTION_ERROR));
		}

		List<OBReadProduct2DataProduct> productList = new ArrayList<>();
		if(mockProduct!=null){
			mockProduct.setAccountId(accountMapping.getAccountDetails().get(0).getAccountId());
			productList.add(mockProduct);
		}
		
		oBReadProduct2Data.setProduct(productList);
		obReadProduct2.setData(oBReadProduct2Data);
		platformAccountProductsResponse.setoBReadProduct2(obReadProduct2);
		
		LOG.info("{\"Exit\":\"{}\",\"{}\"}",
				"com.capgemini.psd2.account.product.mongo.db.adapter.impl.retrieveAccountProducts()",
				loggerUtils.populateLoggerData("retrieveAccountProducts"));
		
		return platformAccountProductsResponse;
		
	}
}