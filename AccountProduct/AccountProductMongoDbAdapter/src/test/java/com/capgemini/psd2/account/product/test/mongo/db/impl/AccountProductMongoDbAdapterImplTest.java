package com.capgemini.psd2.account.product.test.mongo.db.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyString;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.collections.map.HashedMap;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.account.product.mongo.adapter.test.mock.data.AccountProductMockData;
import com.capgemini.psd2.account.product.mongo.db.adapter.impl.AccountProductMongoDbAdapterImpl;
import com.capgemini.psd2.account.product.mongo.db.adapter.repository.AccountProductRepository;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountBalanceResponse;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountProductsResponse;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.exceptions.OBPSD2ExceptionUtility;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.LoggerUtils;
import com.capgemini.psd2.utilities.SandboxValidationUtility;

@RunWith(SpringJUnit4ClassRunner.class)
public class AccountProductMongoDbAdapterImplTest {
	/**
	 * Sets the up.
	 *
	 * @throws Exception
	 *             the exception
	 */
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		Map<String, String> map=new HashMap<>();
		map.put("SIGNATURE", "Something wrong with x-jws-signature request header");
		Map<String, String> specificErrorMessageMap=new HashMap<>();
		specificErrorMessageMap.put("signature_invalid_content", "Invalid content prsent in joseheader of x-jws-signature header");
		specificErrorMessageMap.put("signature_missing", "signature header missing in request");
		OBPSD2ExceptionUtility.genericErrorMessages.putAll(map);
		OBPSD2ExceptionUtility.specificErrorMessages.putAll(specificErrorMessageMap);
	}

	/** The account product mongo DB adapter impl. */
	@InjectMocks
	private AccountProductMongoDbAdapterImpl accountProductMongoDbAdapterImpl;

	@Mock
	AccountProductRepository accountProductRepository;

	@Mock
	LoggerUtils loggerUtils;
	
	@Mock
	SandboxValidationUtility utility;

	/**
	 * Test retrieve blank response account product success flow.
	 */

	@Test
	public void testRetrieveAccountProductSuccessFlow() {

		AccountMapping accountMapping = new AccountMapping();
		ArrayList<AccountDetails> accntDetails = new ArrayList<>();
		AccountDetails accountDetails = new AccountDetails();
		Map<String, String> params = new HashMap<>();

		accountDetails.setAccountId("f4483fda-81be-4873-b4a6-20375b7f55c1");
		accntDetails.add(accountDetails);
		accountMapping.setAccountDetails(accntDetails);
		
		Mockito.when(utility.isValidPsuId(accountMapping.getPsuId())).thenReturn(false);

		Mockito.when(accountProductRepository.findByAccountNumberAndAccountNSC(anyString(), anyString()))
				.thenReturn(AccountProductMockData.getMockAccountDetails());

		Mockito.when(loggerUtils.populateLoggerData(anyString()))
				.thenReturn(AccountProductMockData.getMockLoggerData());

		PlatformAccountProductsResponse actualResponse = accountProductMongoDbAdapterImpl
				.retrieveAccountProducts(accountMapping, params);

		assertEquals(AccountProductMockData.getMockExpectedAccountProductResponse(),
				actualResponse.getObReadProduct2());
	}

	@Test(expected = PSD2Exception.class)
	public void testRetrieveAccountProductDbExceptionFlow() {

		AccountMapping accountMapping = new AccountMapping();
		ArrayList<AccountDetails> accntDetails = new ArrayList<>();
		AccountDetails accountDetails = new AccountDetails();
		Map<String, String> params = new HashMap<>();

		accountDetails.setAccountId("f4483fda-81be-4873-b4a6-20375b7f55c1");
		accntDetails.add(accountDetails);
		accountMapping.setAccountDetails(accntDetails);
		
		Mockito.when(utility.isValidPsuId(accountMapping.getPsuId())).thenReturn(false);

		Mockito.when(accountProductRepository.findByAccountNumberAndAccountNSC(anyString(), anyString()))
				.thenThrow(DataAccessResourceFailureException.class);

		Mockito.when(loggerUtils.populateLoggerData(anyString()))
				.thenReturn(AccountProductMockData.getMockLoggerData());

		PlatformAccountProductsResponse actualResponse = accountProductMongoDbAdapterImpl
				.retrieveAccountProducts(accountMapping, params);

		assertEquals(AccountProductMockData.getMockExpectedAccountProductResponse(),
				actualResponse.getObReadProduct2());
	}

	@Test
	public void testRetrieveAccountProductNullProductFlow() {

		AccountMapping accountMapping = new AccountMapping();
		ArrayList<AccountDetails> accntDetails = new ArrayList<>();
		AccountDetails accountDetails = new AccountDetails();
		Map<String, String> params = new HashMap<>();

		accountDetails.setAccountId("f4483fda-81be-4873-b4a6-20375b7f55c1");
		accntDetails.add(accountDetails);
		accountMapping.setAccountDetails(accntDetails);
		
		Mockito.when(utility.isValidPsuId(accountMapping.getPsuId())).thenReturn(false);

		Mockito.when(accountProductRepository.findByAccountNumberAndAccountNSC(anyString(), anyString()))
				.thenReturn(null);

		Mockito.when(loggerUtils.populateLoggerData(anyString()))
				.thenReturn(AccountProductMockData.getMockLoggerData());

		PlatformAccountProductsResponse actualResponse = accountProductMongoDbAdapterImpl
				.retrieveAccountProducts(accountMapping, params);

	}

	/*
	 * @Test public void testRetrieveAccountProductSuccessFlow() { AccountMapping
	 * accountMapping = new AccountMapping(); ArrayList<AccountDetails> accntDetails
	 * = new ArrayList<>(); AccountDetails accountDetails = new AccountDetails();
	 * Map<String, String> params = new HashMap<>();
	 * 
	 * accntDetails.add(accountDetails);
	 * accountMapping.setAccountDetails(accntDetails);
	 * accountDetails.setAccountId("1234"); ProductGETResponse productResponse =
	 * accountProductMongoDbAdapterImpl.retrieveAccountProducts(accountMapping,
	 * params); PlatformAccountProductsResponse platformAccountProductResponse =
	 * accountProductMongoDbAdapterImpl.retrieveAccountProducts(accountMapping,
	 * params); assertNotNull(platformAccountProductResponse);
	 * 
	 * }
	 */
	
	@Test(expected = PSD2Exception.class)
	public void testRetrieveAccountWhenIsValidPsuId() {
		AccountMapping accountMapping = new AccountMapping();
		ArrayList<AccountDetails> accountDetailsList = new ArrayList<>();
		AccountDetails accountDetails = new AccountDetails();
		Map<String, String> params = new HashedMap();
		
		accountDetails.setAccountId("123");
		accountDetailsList.add(accountDetails);
		accountMapping.setAccountDetails(accountDetailsList);
		
		Mockito.when(utility.isValidPsuId(accountMapping.getPsuId())).thenReturn(true);

		PlatformAccountProductsResponse actualResponse = accountProductMongoDbAdapterImpl
				.retrieveAccountProducts(accountMapping, params);

	}
}
