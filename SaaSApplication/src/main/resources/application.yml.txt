app:
 name: SaaS
 saas.security.tokenSigningKey: 0123456789abcdef
 platform: platform
saas.security.jwt:
  tokenExpirationTime: 15 # Number of minutes
  refreshTokenExpTime: 60 # Minutes
  tokenIssuer: http://SaaS.com
  tokenSigningKey: 0123456789abcdef
foundationService:
  authenticationApplicationBaseURL: http://localhost:8081/fs-login-business-service/services/loginServiceBusiness/channel/business/login
  userInReqHeader: X-BOI-USER
  channelInReqHeader: X-BOI-CHANNEL
  platformInReqHeader: X-BOI-PLATFORM
  correlationIdInReqHeader: X-CORRELATION-ID
  errormap:
    FS_BOL_001: BAD_REQUEST
    FS_BOL_002: AUTHENTICATION_FAILURE_ERROR
    FS_BOL_003: BAD_REQUEST
    FS_BOL_004: TECHNICAL_ERROR
spring:
  application:
    name: test
server: 
 port: 8096
 context-path: /SaaS
 ssl:
    enabled: true
    key-store: classpath:alphasigninserverkeystore.jks
    key-store-password: server
    key-password: server
eureka:
  client:
    registryFetchIntervalSeconds: 10
    serviceUrl:
      defaultZone: http://localhost:8761/eureka/   
  instance:
    secure-port: 8096
    secure-port-enabled: true
    lease-expiration-duration-in-seconds: 10
    lease-renewal-interval-in-seconds: 10
    metadataMap: 
      instanceId: ${spring.application.name}:${spring.application.instance_id:${random.value}}
endpoints:
  restart:
    enabled: true