package com.capgemini.psd2.security.saas.services;

import com.capgemini.psd2.scaconsenthelper.models.PickupDataModel;
/***
 * Interface for Service layer for SCA Integration done with ASPSP
 * 
 * @author parpatid
 *
 */
public interface AuthenticationService {

	public String createJwtToken(PickupDataModel pickUpData);

	public String successfulScaJourney(String token);
	
	public String unsuccessfulScaJourney(String token);

}
