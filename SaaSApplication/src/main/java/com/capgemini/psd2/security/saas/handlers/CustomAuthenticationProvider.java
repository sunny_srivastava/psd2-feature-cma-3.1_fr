/*************************************************************************
 * 
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 * 
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 */

package com.capgemini.psd2.security.saas.handlers;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.aisp.adapter.AispConsentAdapter;
import com.capgemini.psd2.authentication.adapter.AuthenticationAdapter;
import com.capgemini.psd2.consent.domain.AispConsent;
import com.capgemini.psd2.enums.ConsentStatusEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.rest.client.sync.RestClientSync;
import com.capgemini.psd2.scaconsenthelper.config.PFConfig;
import com.capgemini.psd2.scaconsenthelper.config.helpers.CustomAuthenticationService;
import com.capgemini.psd2.scaconsenthelper.constants.PFConstants;
import com.capgemini.psd2.scaconsenthelper.services.SCAConsentHelperService;
import com.capgemini.psd2.security.exceptions.PSD2AuthenticationException;
import com.capgemini.psd2.security.exceptions.SCAConsentErrorCodeEnum;
import com.capgemini.psd2.utilities.NullCheckUtils;
import com.capgemini.psd2.utilities.SandboxConfig;

@Component
public class CustomAuthenticationProvider implements AuthenticationProvider {

	@Autowired
	@Qualifier("saaSRoutingAdapter")
	private AuthenticationAdapter authenticaitonAdapter;

	@Autowired
	private AispConsentAdapter aispConsentAdapter;

	@Autowired
	private RestClientSync restClientSyncImpl;

	@Autowired
	private PFConfig pfConfig;

	@Autowired
	private HttpServletRequest request;

	@Autowired
	private RequestHeaderAttributes requestHeaderAttributes;

	@Autowired
	private SandboxConfig sandboxConfig;

	@Value("${app.regex.username}")
	private String userNameRegex;

	@Value("${app.regex.password}")
	private String passwordRegex;

    @Autowired
	private SCAConsentHelperService helperService;

	@Autowired
	private CustomAuthenticationService customAuthenticationService;

	/**
	 * Its implemented method of AuthenticationProvider to perform MFA of the
	 * bank's customer, one by normal authentication with Ping Dir.(LDAP) & Ping
	 * ID (Mobile Notification).
	 */
	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException{
		try {
			if(Boolean.valueOf(sandboxConfig.isSandboxEnabled())){
				if (NullCheckUtils.isNullOrEmpty(authentication.getPrincipal())) {
					throw PSD2AuthenticationException.populateAuthenticationFailedException(
							SCAConsentErrorCodeEnum.VALIDATION_ERROR, "UserName is blank or invalid");
				}
			}else{
				if (NullCheckUtils.isNullOrEmpty(authentication.getPrincipal())
						|| NullCheckUtils.isNullOrEmpty(authentication.getCredentials())
						|| !customAuthenticationService.validUserAndPassword(authentication)) {
					throw PSD2AuthenticationException.populateAuthenticationFailedException(
							SCAConsentErrorCodeEnum.VALIDATION_ERROR, "UserName or Password is blank or invalid");
				}
			}
			AispConsent consent = aispConsentAdapter.retrieveConsentByAccountRequestIdAndStatus(
					requestHeaderAttributes.getIntentId(), ConsentStatusEnum.AUTHORISED);
			
			if (consent != null && !consent.getPsuId().equalsIgnoreCase((String) authentication.getPrincipal())) {
				throw PSD2AuthenticationException.populateAuthenticationFailedException(
						SCAConsentErrorCodeEnum.UN_AUTHORIZED_USER_REFRESH_TOKEN_FLOW);
			}
			
			requestHeaderAttributes.setPsuId(authentication.getPrincipal().toString());
			authentication = customAuthenticationService.authenticateUser(authentication);
			
			SimpleDateFormat sdf = new SimpleDateFormat(PFConstants.DATE_FORMAT);
			String currentDate = sdf.format(new Date());

			if (requestHeaderAttributes.isBypassConsentPage()) {
				customAuthenticationService.createConsentOnSCA();
			}

			helperService.dropOffOnSCA(authentication.getPrincipal().toString(), currentDate);

		} catch (PSD2AuthenticationException e) {
			throw e;
		} catch (PSD2Exception e) {
			throw PSD2AuthenticationException.populateAuthenticationFailedException(e.getErrorInfo());
		} catch (Exception e) {
			throw PSD2AuthenticationException
			.populateAuthenticationFailedException(SCAConsentErrorCodeEnum.TECHNICAL_ERROR, e.getMessage());
		}

		return authentication;
	}

	/**
	 * 
	 */
	@Override
	public boolean supports(Class<?> authentication) {
		return true;
	}

}
