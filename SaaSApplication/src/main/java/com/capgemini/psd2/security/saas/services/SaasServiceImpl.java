package com.capgemini.psd2.security.saas.services;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.naming.NamingException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.ui.ModelMap;

import com.capgemini.psd2.aisp.adapter.AccountRequestAdapter;
import com.capgemini.psd2.aisp.adapter.AispConsentAdapter;
import com.capgemini.psd2.aisp.domain.OBAccount6;
import com.capgemini.psd2.aisp.domain.OBReadAccount6;
import com.capgemini.psd2.aisp.domain.OBReadAccount6Data;
import com.capgemini.psd2.aisp.domain.OBReadConsentResponse1;
import com.capgemini.psd2.consent.domain.AispConsent;
import com.capgemini.psd2.enums.ConsentStatusEnum;
import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.LoggerUtils;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.mask.DataMask;
import com.capgemini.psd2.pisp.domain.CustomDPaymentConsentsPOSTResponse;
import com.capgemini.psd2.pisp.stage.domain.CustomConsentAppViewData;
import com.capgemini.psd2.pisp.stage.domain.CustomDebtorDetails;
import com.capgemini.psd2.scaconsenthelper.config.helpers.ConsentViewHelper;
import com.capgemini.psd2.scaconsenthelper.config.helpers.CookieHelper;
import com.capgemini.psd2.scaconsenthelper.config.helpers.SCAConsentAdapterHelper;
import com.capgemini.psd2.scaconsenthelper.config.helpers.SCAConsentHelper;
import com.capgemini.psd2.scaconsenthelper.config.helpers.SaasService;
import com.capgemini.psd2.scaconsenthelper.models.PickupDataModel;
import com.capgemini.psd2.security.constants.PSD2SecurityConstants;
import com.capgemini.psd2.security.saas.cancel.controller.SaaSCancelController;
import com.capgemini.psd2.utilities.JSONUtilities;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Service
public class SaasServiceImpl implements SaasService {

	private static final Logger LOG = LoggerFactory.getLogger(SaaSCancelController.class);
	
	@Autowired
	private LoggerUtils loggerUtils;
	
	@Autowired
	private HttpServletRequest request;

	@Autowired
	private HttpServletResponse response;

	@Autowired
	private ConsentViewHelper consentViewHelper;

	@Autowired
	private AispConsentAdapter aispConsentAdapter;

	@Autowired
	private RequestHeaderAttributes requestHeaderAttributes;

	@Autowired
	private SCAConsentAdapterHelper scaConsentAdapterHelper;

	@Autowired
	private CookieHelper cookieHelper;

	@Autowired
	private DataMask dataMask;

	public ModelMap getAccountsDataForConsentView() {
		ModelMap mv = new ModelMap();
		PickupDataModel pickupDataModel = SCAConsentHelper.populatePickupDataModel(request);
		String accountInfoList;
		OBReadAccount6 consentAccounts = new OBReadAccount6();

		AispConsent aispConsent = aispConsentAdapter.retrieveConsentByAccountRequestIdAndStatus(
				pickupDataModel.getIntentId(), ConsentStatusEnum.AUTHORISED);
		
		// Adding for onward provisioning
		mv.addAttribute(PSD2Constants.CLIENT_ID, pickupDataModel.getClientId());

		if (aispConsent != null) {
			OBReadAccount6Data accountData = new OBReadAccount6Data();
			List<OBAccount6> accountList = new ArrayList<>();
			accountData.setAccount(accountList);

			List<OBAccount6> accountListFromAcccountDetails = ConsentViewHelper.populateAccountListFromAccountDetails(aispConsent);
			accountData.getAccount().addAll(accountListFromAcccountDetails);
			consentAccounts.setData(accountData);
			
		}

		if (consentAccounts != null && consentAccounts.getData() != null
				&& !consentAccounts.getData().getAccount().isEmpty()) {
			
			accountInfoList = dataMask.maskResponseGenerateString(consentAccounts, "account");

			OBReadConsentResponse1 accountReq = scaConsentAdapterHelper
					.getAccountRequestSetupData(pickupDataModel.getIntentId());

			if (accountReq == null) {
				throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.NO_ACCOUNT_REQUEST_DATA_FOUND);
			}

			Collections.sort(accountReq.getData().getPermissions());

			String setUpResponseData = JSONUtilities.getJSONOutPutFromObject(accountReq);

			SCAConsentHelper.invalidateCookie(response);
			requestHeaderAttributes.setBypassConsentPage(true);

			pickupDataModel.setBypassConsentPage(true);
			Cookie cookie = cookieHelper.createNewSessionCookie(pickupDataModel);
			response.addCookie(cookie);
			
			mv.addAttribute(PSD2SecurityConstants.CUSTOMER_ACCOUNT_LIST, accountInfoList);
			mv.addAttribute(PSD2SecurityConstants.CONSENT_SETUP_DATA, setUpResponseData);
			mv.addAttribute(PSD2Constants.CONSENT_FLOW_TYPE, pickupDataModel.getIntentTypeEnum().getIntentType());
			mv.addAttribute(PSD2SecurityConstants.USER_ID, pickupDataModel.getUserId());
			mv.addAttribute(PSD2Constants.CLIENT_ID, pickupDataModel.getClientId());
			try {
				mv.addAttribute(PSD2SecurityConstants.TPP_INFO,
						consentViewHelper.getTppInformationByClientId(pickupDataModel.getClientId()));
			} catch (JSONException | NamingException e) {
				LOG.error("{\"Error\":\"{}\", \"JSONException | NamingException\":{}}",
						"com.capgemini.psd2.security.saas.services.getAccountsDataForConsentView()",
						loggerUtils.populateLoggerData("getAccountsDataForConsentView"),
						JSONUtilities.getJSONOutPutFromObject(e));
			}
		}
		return mv;
	}

	public ModelMap getPaymentsDataForConsentView() {
		ModelMap mv = new ModelMap();
		PickupDataModel pickupDataModel = SCAConsentHelper.populatePickupDataModel(request);

		CustomDebtorDetails customDebtorDetails = consentViewHelper.retrieveConsentDetails(pickupDataModel);

		if (!NullCheckUtils.isNullOrEmpty(customDebtorDetails)) {
			CustomConsentAppViewData consentAppPaymentSetupData;
			CustomDPaymentConsentsPOSTResponse setUpResponseDataforUI;

			consentAppPaymentSetupData = consentViewHelper.getConsentAppViewData(pickupDataModel);

			setUpResponseDataforUI = consentViewHelper.populateLimitedSetUpDataforUI(consentAppPaymentSetupData);

			SCAConsentHelper.invalidateCookie(response);

			requestHeaderAttributes.setBypassConsentPage(true);
			pickupDataModel.setBypassConsentPage(true);
			Cookie cookie = cookieHelper.createNewSessionCookie(pickupDataModel);
			response.addCookie(cookie);

			mv.addAttribute(PSD2Constants.PAYMENT_CONSENT_DATA,
					JSONUtilities.getJSONOutPutFromObject(consentAppPaymentSetupData));

			mv.addAttribute(PSD2SecurityConstants.CONSENT_SETUP_DATA,
					JSONUtilities.getJSONOutPutFromObject(setUpResponseDataforUI));

			mv.addAttribute(PSD2Constants.CONSENT_FLOW_TYPE, pickupDataModel.getIntentTypeEnum().getIntentType());
			mv.addAttribute(PSD2SecurityConstants.USER_ID, pickupDataModel.getUserId());

			try {
				mv.addAttribute(PSD2SecurityConstants.TPP_INFO,
						consentViewHelper.getTppInformationByClientId(pickupDataModel.getClientId()));
			} catch (JSONException | NamingException e) {
				LOG.error("{\"Error\":\"{}\", \"JSONException | NamingException\":{}}",
						"com.capgemini.psd2.security.saas.services.getPaymentsDataForConsentView()",
						loggerUtils.populateLoggerData("getPaymentsDataForConsentView"),
						JSONUtilities.getJSONOutPutFromObject(e));
			
			}
		}

		return mv;
	}

}
