/*************************************************************************
* 
 * CAPGEMINI CONFIDENTIAL
* __________________
* 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
* 
 * NOTICE:  All information contained herein is, and remains
* the property of CAPGEMINI GROUP.
* The intellectual and technical concepts contained herein
* are proprietary to CAPGEMINI GROUP and may be covered
* by patents, patents in process, and are protected by trade secret
* or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from CAPGEMINI GROUP.
*/

package com.capgemini.psd2.security.saas.application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.web.context.request.RequestContextListener;

import com.capgemini.psd2.aisp.adapter.CustomerAccountListAdapter;
import com.capgemini.psd2.customer.account.list.routing.adapter.impl.CustomerAccountListRoutingAdapter;
import com.capgemini.psd2.rest.client.exception.handler.ExceptionHandlerImpl;
import com.capgemini.psd2.rest.client.sync.RestClientSync;
import com.capgemini.psd2.rest.client.sync.impl.RestClientSyncImpl;
import com.capgemini.psd2.scaconsenthelper.filters.CookieHandlerFilter;
import com.capgemini.psd2.scaconsenthelper.filters.PSD2SecurityFilter;
import com.capgemini.psd2.scaconsenthelper.filters.SaaSCookieHandlerFilter;

@SpringBootApplication
@ComponentScan(basePackages = { "com.capgemini.psd2" })
@EnableMongoRepositories(basePackages = { "com.capgemini.psd2" })
@EnableEurekaClient
public class SaasApplication {
	static ConfigurableApplicationContext context = null;

	public static void main(String[] args) {
		try {
			context = SpringApplication.run(SaasApplication.class, args);
		} catch (Exception e) {
			if (context != null) {
				SpringApplication.exit(context, () -> 1);
			}
			System.exit(1);
		}

	}

	@Bean()
	public RestClientSync restClientSyncImpl() {
		return new RestClientSyncImpl(new ExceptionHandlerImpl());
	}

	
	@Bean
	public RequestContextListener requestContextListener() {
		return new RequestContextListener();
	}

	@Bean
	public FilterRegistrationBean saasFilter() {
		FilterRegistrationBean filterRegBean = new FilterRegistrationBean(buildPSD2SecurityFilter());
		filterRegBean.addUrlPatterns("/index.jsp");
		return filterRegBean;
	}

	@Bean
	public PSD2SecurityFilter buildPSD2SecurityFilter() {
		return new PSD2SecurityFilter();
	}

	@Bean
	public CookieHandlerFilter buildCookieHandlerFilter() {
		return new SaaSCookieHandlerFilter("SCA");
	}
	
	@Bean(name = "CustomerAccountListAdapter")
	public CustomerAccountListAdapter customeAccountListAdapter() {
		return new CustomerAccountListRoutingAdapter();
	}	
}
