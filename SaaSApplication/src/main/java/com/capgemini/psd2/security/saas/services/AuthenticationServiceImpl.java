package com.capgemini.psd2.security.saas.services;

import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

import com.capgemini.psd2.aisp.adapter.AccountRequestAdapter;
import com.capgemini.psd2.aisp.adapter.AispConsentAdapter;
import com.capgemini.psd2.aisp.domain.OBReadConsentResponse1;
import com.capgemini.psd2.aisp.domain.OBReadConsentResponse1Data;
import com.capgemini.psd2.consent.domain.AispConsent;
import com.capgemini.psd2.consent.domain.PSD2AccountsAdditionalInfo;
import com.capgemini.psd2.enums.ConsentStatusEnum;
import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.fraudsystem.utilities.FraudSystemUtilities;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.RestClientSync;
import com.capgemini.psd2.scaconsenthelper.config.AuthenticationConfig;
import com.capgemini.psd2.scaconsenthelper.config.PFConfig;
import com.capgemini.psd2.scaconsenthelper.config.helpers.CookieHelper;
import com.capgemini.psd2.scaconsenthelper.config.helpers.DropOffDataService;
import com.capgemini.psd2.scaconsenthelper.config.helpers.JwtTokenUtility;
import com.capgemini.psd2.scaconsenthelper.config.helpers.SCAConsentAdapterHelper;
import com.capgemini.psd2.scaconsenthelper.config.helpers.SCAConsentHelper;
import com.capgemini.psd2.scaconsenthelper.constants.PFConstants;
import com.capgemini.psd2.scaconsenthelper.constants.SCAConsentHelperConstants;
import com.capgemini.psd2.scaconsenthelper.models.DropOffRequest;
import com.capgemini.psd2.scaconsenthelper.models.PickupDataModel;
import com.capgemini.psd2.utilities.JSONUtilities;
/***
 * Service layer for SCA Integration done with ASPSP
 * 
 * @author parpatid
 *
 */
@Service
public class AuthenticationServiceImpl implements AuthenticationService{
	
	private static final Logger LOG = LoggerFactory.getLogger(AuthenticationServiceImpl.class);
	
	@Autowired
	private HttpServletRequest request;
	
	@Autowired
	private HttpServletResponse response;

	@Autowired
	private RequestHeaderAttributes headerAttributes;

	@Autowired
	private AuthenticationConfig config;

	@Autowired
	private DropOffDataService dropOffService;

	@Autowired
	private CookieHelper cookieHelper;	
	
	@Autowired
	private PFConfig pfConfig;
	
	@Autowired
	private RestClientSync restClientSyncImpl;
	
	@Autowired
	private AispConsentAdapter aispConsentAdapter;
	
	@Autowired
	private SCAConsentAdapterHelper scaConsentAdapterHelper;
	
	@Value("${app.consentApplicationUrl.prefix:#{null}}")
	private String consentApplicationUrl;

	@Value("${app.skipConsentFlow:#{false}}")
	private boolean skipConsentFlow;

	/***
	 * Create JWE and append it to the Authentication URL of ASPSP
	 * 
	 * @param pickUpData
	 * @return redirectUrl of ASPSP
	 */
	@Override
	public String createJwtToken(PickupDataModel pickUpData) {
		String scope = headerAttributes.getScopes();
		if (skipConsentFlow && PSD2Constants.ACCOUNTS.equals(scope)) {
			
			AispConsent aispConsent = aispConsentAdapter.retrieveConsentByAccountRequestIdAndStatus(
					pickUpData.getIntentId(), ConsentStatusEnum.AUTHORISED);
			
			if(aispConsent != null) {
				pickUpData.setBypassConsentPage(true);
				SCAConsentHelper.invalidateCookie(response);
				headerAttributes.setBypassConsentPage(true);
				Cookie cookie = cookieHelper.createNewSessionCookie(pickUpData);
				response.addCookie(cookie);
			}
		}
		String jwtToken = JwtTokenUtility.createJwtToken(config.getTokenIssuer(), config.getTokenExpirationTime(),
				config.getTokenSigningKey(), config.getJweDecryptionKey(), getClaimsSet(pickUpData));
		String authenticationUrl = this.genarateBankRedirectUrl(jwtToken);
		return authenticationUrl;
	}

	/***
	 * Parse the token received from ASPSP after authentication 
	 * 
	 * @param token
	 * @return resumePath for PF to continue the flow
	 */
	@Override
	public String successfulScaJourney(String token) {
		DropOffRequest dropOffRequest = JwtTokenUtility.parseAndReturnClaims(token, config.getJweDecryptionKey());
		// Changes to Skip Consent Page during Consent Renewal Flow
		if ((dropOffRequest.isBypassConsentPage())) {
			AispConsent consent = aispConsentAdapter.retrieveConsentByAccountRequestIdAndStatus(
					headerAttributes.getIntentId(), ConsentStatusEnum.AUTHORISED);
			/** If the consent userId and SCA userID doesn't match */
			if (consent != null && !consent.getPsuId().equalsIgnoreCase(dropOffRequest.getUsername())) {
				//Error Logger to be added
				return this.unsuccessfulScaJourney(token);
			}
			this.createConsentOnSCA();
		}
		String refId = dropOffService.dropOffOnSuccess(dropOffRequest);
		return generateResumePathUrl(dropOffRequest, refId);
	}
	
	/***
	 * Method executed when any error occurred at SCA
	 * @param token
	 * @return
	 */
	@Override
	public String unsuccessfulScaJourney(String token) {
		DropOffRequest dropOffRequest = JwtTokenUtility.parseAndReturnClaims(token, config.getJweDecryptionKey());
		String refId = dropOffService.dropOffOnCancel();
		if(!dropOffRequest.isBypassConsentPage()) {
			cancelAispConsentAndSetup(dropOffRequest.getIntentId());	
		}
		return generateResumePathUrl(dropOffRequest, refId);
	}
	
	
	/***
	 *  Method to Create Consent on SCA in case of Consent renewal 
	 *  Applicable only when SCA Integration is needed with ASPSP
	 */
	private void createConsentOnSCA() {
		//Entry Logger to be added
		String scope = headerAttributes.getScopes();
		String consentURL = null;
		PSD2AccountsAdditionalInfo consentSubmissionData = new PSD2AccountsAdditionalInfo();
		consentSubmissionData.setHeaders(Base64.getEncoder()
				.encodeToString(FraudSystemUtilities.populateFraudSystemHeaders(request).getBytes()));
		 
		if (scope.equals(PSD2Constants.ACCOUNTS)) {
			consentURL = consentApplicationUrl.concat(PSD2Constants.AISP_CONSENT + PSD2Constants.QUESTIONMARK + PSD2Constants.RESUME_PATH
							+ PSD2Constants.AMPERSAND + PSD2Constants.REFRESHTOKEN_RENEWALFLOW);
		}
		Cookie cookie = cookieHelper.setBypassConsentCookie(request);
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(consentURL);
		HttpHeaders httpHeaders = SCAConsentHelper.populateCookieHeaders(cookie.getValue());
		try{
			restClientSyncImpl.callForPost(requestInfo, consentSubmissionData, String.class, httpHeaders);
		}catch(Exception e) {
			LOG.info("Exception occurred in createConsentOnSCA(): "+ e);
		}
		//Exit logger to be added
	}
	
	private String generateResumePathUrl(DropOffRequest dropOffRequest, String refId) {
		String tenantId = headerAttributes.getTenantId() != null ? headerAttributes.getTenantId() : PSD2Constants.DEFAULT;
		return UriComponentsBuilder.fromHttpUrl(pfConfig.getTenantSpecificResumePathBaseUrl(tenantId)
						.concat(dropOffRequest.getResumePath())).queryParam(PFConstants.REF, refId).toUriString();
	}

	private String genarateBankRedirectUrl(String token) {
		String tenantId = headerAttributes.getTenantId() != null ? headerAttributes.getTenantId() : PSD2Constants.DEFAULT;
		return UriComponentsBuilder.fromHttpUrl(config.getTenantSpecificAutheticationUrl(tenantId))
				.queryParam(SCAConsentHelperConstants.TOKEN, token).queryParam(SCAConsentHelperConstants.REDIRECT_URL, config.getAspspRedirectUrl()).toUriString();
	}
	
	private void cancelAispConsentAndSetup(String intentId) {
		//Entry logger to be added
		OBReadConsentResponse1 accountSetupResponse = scaConsentAdapterHelper.getAccountRequestSetupData(intentId);
		AispConsent aispConsent = aispConsentAdapter.retrieveConsentByAccountRequestId(intentId);
		
		if (accountSetupResponse != null && accountSetupResponse.getData() != null) {
			if (accountSetupResponse.getData().getStatus()
					.equals(OBReadConsentResponse1Data.StatusEnum.AUTHORISED)
					&& (aispConsent == null || !aispConsent.getStatus().equals(ConsentStatusEnum.AUTHORISED))) {
				throw PSD2Exception.populatePSD2Exception("Consent is not in correct status", ErrorCodeEnum.INTENT_ID_VALIDATION_ERROR);
			}

			if (accountSetupResponse.getData().getStatus().equals(OBReadConsentResponse1Data.StatusEnum.AWAITINGAUTHORISATION)
					&& (aispConsent != null && !aispConsent.getStatus().equals(ConsentStatusEnum.AWAITINGAUTHORISATION))) {
				throw PSD2Exception.populatePSD2Exception("Consent is not in correct status", ErrorCodeEnum.INTENT_ID_VALIDATION_ERROR);
			}
		}
		
		if (aispConsent != null && aispConsent.getStatus().equals(ConsentStatusEnum.AWAITINGAUTHORISATION)) {
			aispConsentAdapter.updateConsentStatus(aispConsent.getConsentId(), ConsentStatusEnum.REJECTED);
		}
		scaConsentAdapterHelper.updateAccountRequestSetupData(intentId, OBReadConsentResponse1Data.StatusEnum.REJECTED);
		//Exit logger to be added
	}
	
	private Map<String, String> getClaimsSet(PickupDataModel pickUpData) {
		Map<String, String> claimSet = new HashMap<>();
		String intentDataJSON = JSONUtilities.getJSONOutPutFromObject(pickUpData);
		claimSet.put(SCAConsentHelperConstants.INTENT_DATA, intentDataJSON);
		claimSet.put(SCAConsentHelperConstants.USER_ID, headerAttributes.getPsuId());
		claimSet.put(SCAConsentHelperConstants.TENANT_ID, headerAttributes.getTenantId());
		claimSet.entrySet().stream().forEach(System.out::println);
		return claimSet;
	}
}
