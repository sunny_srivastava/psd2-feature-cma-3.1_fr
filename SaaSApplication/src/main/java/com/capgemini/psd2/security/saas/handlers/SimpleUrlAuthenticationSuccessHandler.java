/*************************************************************************
 * 
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 * 
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 */

package com.capgemini.psd2.security.saas.handlers;

import java.io.IOException;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;
import org.springframework.web.util.UriComponentsBuilder;

import com.capgemini.psd2.fraudsystem.constants.FraudSystemConstants;
import com.capgemini.psd2.fraudsystem.helper.FraudSystemHelper;
import com.capgemini.psd2.fraudsystem.request.handler.impl.FraudSystemRequestMapping;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.scaconsenthelper.config.PFConfig;
import com.capgemini.psd2.scaconsenthelper.config.helpers.SCAConsentHelper;
import com.capgemini.psd2.scaconsenthelper.constants.PFConstants;
import com.capgemini.psd2.scaconsenthelper.models.PickupDataModel;
import com.capgemini.psd2.utilities.DateUtilites;

@Component
public class SimpleUrlAuthenticationSuccessHandler implements AuthenticationSuccessHandler {

	private static final Logger LOGGER = LoggerFactory.getLogger(SimpleUrlAuthenticationSuccessHandler.class);

	@Autowired
	private RequestHeaderAttributes requestHeaderAttribute;
	
	@Autowired
	private FraudSystemHelper fraudSystemHelper;
	
	@Autowired
	private PFConfig pfConfig;
		

	/**
	 * This is a helper method of onAuthenticationSuccess method.
	 * It takes the ID token as a param and sends this param at the time of redirecting back to
	 * the OAuth server's end-point , through this way its injecting the ID token to OAuth server to 
	 * prove the successful authentication of the customer.
	 * @param accessToken
	 * @param request
	 * @param response
	 * @param authentication
	 * @throws IOException
	 */
	protected void handle(HttpServletRequest request, HttpServletResponse response,
			Authentication authentication) throws IOException {
		LOGGER.info("{\"Enter\":\"com.capgemini.psd2.security.alpha.saas.handlers.SimpleUrlAuthenticationSuccessHandler.handle()\",\"timestamp\":\"{}\",\"correlationId\": \"{}\"}",DateUtilites.generateCurrentTimeStamp(),requestHeaderAttribute.getCorrelationId());
		
		String resumePath = request.getParameter(PFConstants.RESUME_PATH);
		String refId = (String)request.getAttribute(PFConstants.REF);
		
		LOGGER.info("{\"Exit\":\"com.capgemini.psd2.security.alpha.saas.handlers.SimpleUrlAuthenticationSuccessHandler.handle()\",\"timestamp\":\"{}\",\"correlationId\": \"{}\"}",DateUtilites.generateCurrentTimeStamp(),requestHeaderAttribute.getCorrelationId());
		resumePath = pfConfig.getTenantSpecificResumePathBaseUrl(requestHeaderAttribute.getTenantId()).concat(resumePath);
		
		String redirectURI = UriComponentsBuilder.fromHttpUrl(resumePath).queryParam(PFConstants.REF, refId).toUriString();
		SCAConsentHelper.invalidateCookie(response);
		response.sendRedirect(redirectURI);
	}

	/**
	 * onAuthenticationSuccess is a overridden method of
	 * AuthenticationSuccessHandler class, it will get it invoked on the
	 * successful authentication of the customer. It generates the signed ID token in
	 * the form of JWT to contain the user's identity information with the help of one tokenFactory.
	 * It invokes the handle method to redirect back to OAuth server's end-point with ID token. 
	 */
	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
			Authentication authentication) throws IOException, ServletException {
		LOGGER.info("{\"Enter\":\"com.capgemini.psd2.security.alpha.saas.handlers.SimpleUrlAuthenticationSuccessHandler.onAuthenticationSuccess()\",\"timestamp\":\"{}\",\"correlationId\": \"{}\"}",DateUtilites.generateCurrentTimeStamp(),requestHeaderAttribute.getCorrelationId());
		
		// Fraudnet Call
		PickupDataModel pickUpDataModel = SCAConsentHelper.populatePickupDataModel(request);
		Map<String, String> fraudHelperParam = new HashMap<>();
		fraudHelperParam.put(PSD2Constants.FLOWTYPE, pickUpDataModel.getIntentTypeEnum().getIntentType());
		fraudHelperParam.put(PSD2Constants.USERNAME, authentication.getPrincipal().toString());
		
		//Decoding FS Headers
		if (request.getParameter(FraudSystemRequestMapping.FS_HEADERS) != null) {
			fraudHelperParam.put(FraudSystemRequestMapping.FS_HEADERS, new String(Base64.getDecoder().decode(request.getParameter(FraudSystemRequestMapping.FS_HEADERS))));
		} 
		
		fraudHelperParam.put(PSD2Constants.OUTCOME, FraudSystemConstants.OUTCOME_SUCCESS);
		fraudSystemHelper.captureFraudEvent(fraudHelperParam);
		
		LOGGER.info("{\"Exit\":\"com.capgemini.psd2.security.alpha.saas.handlers.SimpleUrlAuthenticationSuccessHandler.onAuthenticationSuccess()\",\"timestamp\":\"{}\",\"correlationId\": \"{}\"}",DateUtilites.generateCurrentTimeStamp(),requestHeaderAttribute.getCorrelationId());
		if(!response.isCommitted())
 	    handle(request, response, authentication);
	}
}