package com.capgemini.psd2.security.saas.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import com.capgemini.psd2.aisp.domain.OBAccount6Account;
import com.capgemini.psd2.aisp.domain.OBReadAccount6;
import com.capgemini.psd2.authentication.adapter.AuthenticationAdapter;
import com.capgemini.psd2.consent.domain.PSD2Account;
import com.capgemini.psd2.consent.domain.PSD2AccountsAdditionalInfo;
import com.capgemini.psd2.fraudsystem.request.handler.impl.FraudSystemRequestMapping;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.stage.domain.CustomConsentAppViewData;
import com.capgemini.psd2.pisp.stage.domain.CustomDebtorDetails;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.RestClientSync;
import com.capgemini.psd2.scaconsenthelper.config.helpers.CookieHelper;
import com.capgemini.psd2.scaconsenthelper.config.helpers.CustomAuthenticationService;
import com.capgemini.psd2.scaconsenthelper.config.helpers.SCAConsentHelper;
import com.capgemini.psd2.utilities.JSONUtilities;
import com.capgemini.psd2.utilities.StringUtils;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class CustomAuthenticationServiceImpl implements CustomAuthenticationService{

	@Autowired
	private HttpServletRequest request;

	@Autowired
	private CookieHelper cookieHelper;
	
	@Autowired
	private RestClientSync restClientSyncImpl;
	
	@Autowired
	@Qualifier("saaSRoutingAdapter")
	private AuthenticationAdapter authenticaitonAdapter;

	@Autowired
	private RequestHeaderAttributes requestHeaderAttributes;
	
	@Value("${app.regex.username}")
	private String userNameRegex;

	@Value("${app.regex.password}")
	private String passwordRegex;

	@Value("${app.consentApplicationUrl.prefix:#{null}}")
	private String consentApplicationUrl;
	
	@Override
	public Authentication authenticateUser(Authentication authentication) {
		Map<String, String> headers = new HashMap<>();
		headers.put(PSD2Constants.CORRELATION_ID, requestHeaderAttributes.getCorrelationId());
		headers.put(PSD2Constants.USER_IN_REQ_HEADER, authentication.getPrincipal().toString());
		headers.put(PSD2Constants.CHANNEL_IN_REQ_HEADER, requestHeaderAttributes.getChannelId());
		return authenticaitonAdapter.authenticate(authentication, headers);
	}

	@Override
	public boolean validUserAndPassword(Authentication authentication) {
		Boolean isValid = Boolean.FALSE;
		Pattern usenamePattern = Pattern.compile(userNameRegex);
		Pattern passPattern = Pattern.compile(passwordRegex);
		Matcher m = usenamePattern.matcher((String) authentication.getPrincipal());
		Matcher m1 = passPattern.matcher((String) authentication.getCredentials());
		if (m.matches() && m1.matches()) {
			isValid = true;
		}
		return isValid;
	}

	@Override
	public void createConsentOnSCA() {
		String scope = requestHeaderAttributes.getScopes();
		String consentURL = null;
		PSD2AccountsAdditionalInfo consentSubmissionData = new PSD2AccountsAdditionalInfo();
		
		consentSubmissionData.setHeaders(request.getParameter(FraudSystemRequestMapping.FS_HEADERS));
		 
		if (scope.equals(PSD2Constants.ACCOUNTS)) {

			getAISPConsentSubmissionRequest(consentSubmissionData);
			
			consentURL = consentApplicationUrl
					.concat(PSD2Constants.AISP_CONSENT + PSD2Constants.QUESTIONMARK + PSD2Constants.RESUME_PATH
							+ PSD2Constants.AMPERSAND + PSD2Constants.REFRESHTOKEN_RENEWALFLOW);

		} else if (scope.equals(PSD2Constants.PAYMENTS)) {
			
			 
			getPISPConsentSubmissionData(consentSubmissionData);
			consentURL = consentApplicationUrl.concat(
					PSD2Constants.PISP_CONSENT + PSD2Constants.QUESTIONMARK + PSD2Constants.RESUME_PATH);

		}

		Cookie cookie = cookieHelper.setBypassConsentCookie(request);
		/*
		 * Cookie[] cookies = request.getCookies(); Cookie
		 * customSessionControllCookie =
		 * cookieHelper.getCustomSessionCookie(cookies);
		 * 
		 * RawAccessJwtToken token = new
		 * RawAccessJwtToken(customSessionControllCookie.getValue(),
		 * requestHeaderAttributes);
		 * 
		 * String intentDataStr =
		 * token.parseAndReturnClaims(jweDecryptionKey);
		 * 
		 * PickupDataModel intentData = (PickupDataModel)
		 * JSONUtilities.getObjectFromJSONString(intentDataStr,
		 * PickupDataModel.class);
		 * 
		 * intentData.setUserId(requestHeaderAttributes.getPsuId());
		 * intentData.setBypassConsentPage(requestHeaderAttributes.
		 * isBypassConsentPage()); Cookie cookie =
		 * cookieHelper.createNewSessionCookie(intentData);
		 */
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(consentURL);
		HttpHeaders httpHeaders = SCAConsentHelper.populateCookieHeaders(cookie.getValue());
		restClientSyncImpl.callForPost(requestInfo, consentSubmissionData, String.class, httpHeaders);
	}

	private void getPISPConsentSubmissionData(PSD2AccountsAdditionalInfo consentSubmissionData) {
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		CustomConsentAppViewData paymentConsentData = JSONUtilities.getObjectFromJSONString(objectMapper,
				request.getParameter(PSD2Constants.PAYMENT_SETUP), CustomConsentAppViewData.class);
		
		CustomDebtorDetails debtorDetails = paymentConsentData.getDebtorDetails();
		List<OBAccount6Account> accountInfoList = new ArrayList<>();

		List<PSD2Account> setUpAccount = new ArrayList<>();
		PSD2Account detailedAccountInfo = new PSD2Account();
		OBAccount6Account accountInfo = JSONUtilities
				.getObjectFromJSONString(objectMapper,JSONUtilities.getJSONOutPutFromObject(debtorDetails), OBAccount6Account.class);
		accountInfoList.add(accountInfo);

		detailedAccountInfo.setAccount(accountInfoList);
		detailedAccountInfo.setHashedValue(StringUtils.generateHashedValue(accountInfo.getIdentification()));

		setUpAccount.add(detailedAccountInfo);

		consentSubmissionData.setAccountdetails(setUpAccount);
	}

	private void getAISPConsentSubmissionRequest(PSD2AccountsAdditionalInfo consentSubmissionData) {
		ObjectMapper objectMapper = new ObjectMapper();
		objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		OBReadAccount6 customerAccountList = JSONUtilities.getObjectFromJSONString(objectMapper,
				request.getParameter(PSD2Constants.PSU_ACCOUNTS), OBReadAccount6.class);

		consentSubmissionData
				.setAccountdetails((List<PSD2Account>) (Object) customerAccountList.getData().getAccount());
	}
}