/*************************************************************************
 * 
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 * 
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 */
package com.capgemini.psd2.security.saas.controllers;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.Map;

import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.nio.protocol.PipeliningClientExchangeHandler;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.WebAttributes;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.UriUtils;

import com.capgemini.psd2.aisp.adapter.AccountRequestAdapter;
import com.capgemini.psd2.exceptions.ErrorInfo;
import com.capgemini.psd2.fraudsystem.request.handler.impl.FraudSystemRequestMapping;
import com.capgemini.psd2.fraudsystem.utilities.FraudSystemUtilities;
import com.capgemini.psd2.integration.adapter.TPPInformationAdaptor;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.scaconsenthelper.config.CdnConfig;
import com.capgemini.psd2.scaconsenthelper.config.helpers.SCAConsentHelper;
import com.capgemini.psd2.scaconsenthelper.config.helpers.SaasService;
import com.capgemini.psd2.scaconsenthelper.constants.OIDCConstants;
import com.capgemini.psd2.scaconsenthelper.constants.PFConstants;
import com.capgemini.psd2.scaconsenthelper.constants.SCAConsentHelperConstants;
import com.capgemini.psd2.scaconsenthelper.models.PickupDataModel;
import com.capgemini.psd2.security.constants.PSD2SecurityConstants;
import com.capgemini.psd2.security.exceptions.PSD2AuthenticationException;
import com.capgemini.psd2.security.exceptions.PSD2SecurityException;
import com.capgemini.psd2.security.exceptions.SCAConsentErrorCodeEnum;
import com.capgemini.psd2.ui.content.utility.controller.UIStaticContentUtilityController;
import com.capgemini.psd2.utilities.JSONUtilities;
import com.capgemini.psd2.utilities.SandboxConfig;
import com.capgemini.psd2.utilities.ValidationUtility;

/**
 * ConsentView Controller
 * 
 * @author Capgemini
 *
 */
@Controller
public class SaaSController {

	@Autowired
	private RequestHeaderAttributes requestHeaderAttributes;

	@Value("${app.fraudSystem.resUrl}")
	private String resUrl;

	@Value("${app.fraudSystem.hdmUrl}")
	private String hdmUrl;

	@Value("${app.fraudSystem.hdmInputName}")
	private String hdmInputName;

	@Value("${app.fraudSystem.jsc-file-path}")
	private String jscFilePath;

	@Value("${app.skipConsentFlow:#{false}}")
	private boolean skipConsentFlow;

	@Value("${spring.application.name}")
	private String applicationName;

	@Autowired
	private UIStaticContentUtilityController uiController;	
	
	@Autowired
	private SandboxConfig sandboxConfig;
	
	@Autowired
	private HttpServletRequest request;
	
	@Autowired
	private HttpServletResponse response;
	
	@Autowired
	private SaasService saasService;
	
	@Autowired
	private TPPInformationAdaptor tppInformationAdaptor;

	@RequestMapping(value = "/errors", method = RequestMethod.PUT)
	@ResponseBody
	public String handleSessionTimeOutOnCancel(Model model) {
		error(model);
		return JSONUtilities.getJSONOutPutFromObject(model);
	}

	@RequestMapping("/sessionerrors")
	public String viewErrors(Map<String, Object> model){
		model.putAll(CdnConfig.populateCdnAttributes());
		model.put(PSD2SecurityConstants.LOGO_URL,uiController.retrieveUiParamValue(PSD2SecurityConstants.SCA_LOGO_URL_PATH));
		model.put(PSD2SecurityConstants.ERROR_GENERIC_MSG,uiController.retrieveUiParamValue(PSD2SecurityConstants.GENERIC_ERROR_PATH));
		model.put(PSD2SecurityConstants.ERROR_GENERIC_DESCRIPTION,uiController.retrieveUiParamValue(PSD2SecurityConstants.GENERIC_ERROR_DESCRIPTION_PATH));
		model.put(OIDCConstants.CHANNEL_ID, requestHeaderAttributes.getChannelId());
		model.put(PSD2Constants.NETSCALER_TENANT_ID_HEADER_NAME, request.getHeader(PSD2Constants.NETSCALER_TENANT_ID_HEADER_NAME));
		return "viewerrors";
	}


	@RequestMapping("/errors")
	public String error(Model model) {
		String statusCode = null;
		AuthenticationException exception = (AuthenticationException) request
				.getAttribute(WebAttributes.AUTHENTICATION_EXCEPTION);

		ErrorInfo errorInfo = (ErrorInfo) request.getAttribute(SCAConsentHelperConstants.EXCEPTION);

		if (exception != null && exception instanceof PSD2AuthenticationException) {
			ErrorInfo authenticationErrorInfo = ((PSD2AuthenticationException) exception).getErrorInfo();
			statusCode = authenticationErrorInfo.getStatusCode();
		}else if (errorInfo != null) {
			model.addAttribute(SCAConsentHelperConstants.EXCEPTION, errorInfo);
			errorInfo.setDetailErrorMessage(null);
			statusCode = errorInfo.getStatusCode();
		}
		
		boolean serverError = Boolean.FALSE;
		if (statusCode != null && !statusCode.isEmpty()) {
			serverError = statusCode.equalsIgnoreCase(Integer.toString(HttpStatus.INTERNAL_SERVER_ERROR.value()))
					|| Integer.parseInt(statusCode) > HttpStatus.INTERNAL_SERVER_ERROR.value();
		}
		model.addAttribute(PSD2Constants.SERVER_ERROR_FLAG_ATTR, serverError);
		
		String resumePath = (String) request.getAttribute(PFConstants.RESUME_PATH);
		if (resumePath == null || resumePath.isEmpty()) {
			if (request.getParameter(PFConstants.RESUME_PATH) != null
					&& !request.getParameter(PFConstants.RESUME_PATH).isEmpty()) {
				resumePath = request.getParameter(PFConstants.RESUME_PATH);
			} else if (request.getParameter(SCAConsentHelperConstants.OAUTH_URL_PARAM) != null
					&& !request.getParameter(SCAConsentHelperConstants.OAUTH_URL_PARAM).isEmpty()) {
				resumePath = request.getParameter(SCAConsentHelperConstants.OAUTH_URL_PARAM);
			}
		}
		model.addAttribute(PSD2SecurityConstants.REDIRECT_URI_MODEL_ATTRIBUTE, resumePath);

		String currentCorrId =  requestHeaderAttributes.getCorrelationId();

		if(currentCorrId == null || currentCorrId.isEmpty()){
			currentCorrId = request.getParameter(PSD2Constants.CO_RELATION_ID);
			ValidationUtility.isValidUUID(currentCorrId);			
		}
		model.addAttribute(PSD2Constants.CO_RELATION_ID, currentCorrId);

		model.addAttribute(PSD2SecurityConstants.SKIP_CONSENT_FLOW, skipConsentFlow);
		ModelMap map = null;
		String scope = requestHeaderAttributes.getScopes();
		if (skipConsentFlow && PSD2Constants.PAYMENTS.equals(scope)) {
			map = saasService.getPaymentsDataForConsentView();
		} else if (skipConsentFlow && PSD2Constants.ACCOUNTS.equals(scope)) {
			map = saasService.getAccountsDataForConsentView();
		}
		model.addAllAttributes(map);
				
		if (exception != null && exception instanceof PSD2AuthenticationException) {
			ErrorInfo authenticationErrorInfo = ((PSD2AuthenticationException) exception).getErrorInfo();
			statusCode = authenticationErrorInfo.getStatusCode();
		}

		else if (errorInfo != null) {
			model.addAttribute(SCAConsentHelperConstants.EXCEPTION, errorInfo);
			errorInfo.setDetailErrorMessage(null);
			statusCode = errorInfo.getStatusCode();
		}

		if (statusCode != null && !statusCode.isEmpty()) {
			serverError = statusCode.equalsIgnoreCase(Integer.toString(HttpStatus.INTERNAL_SERVER_ERROR.value()))
					|| Integer.parseInt(statusCode) > HttpStatus.INTERNAL_SERVER_ERROR.value();
		}

		model.addAttribute(PSD2Constants.SERVER_ERROR_FLAG_ATTR, serverError);

		model.addAttribute(PSD2Constants.CO_RELATION_ID, currentCorrId);

		model.addAttribute(PSD2SecurityConstants.REDIRECT_URI_MODEL_ATTRIBUTE, resumePath);
		model.addAttribute(PSD2Constants.JS_MSG,
				uiController.retrieveUiParamValue(PSD2Constants.ERROR_MESSAGE, PSD2Constants.JS_MSG));
		model.addAttribute(OIDCConstants.CHANNEL_ID, requestHeaderAttributes.getChannelId());
		model.addAttribute(PSD2Constants.NETSCALER_TENANT_ID_HEADER_NAME, (null == request.getHeader(PSD2Constants.NETSCALER_TENANT_ID_HEADER_NAME)) ?
				requestHeaderAttributes.getTenantId():request.getHeader(PSD2Constants.NETSCALER_TENANT_ID_HEADER_NAME));
		model.addAttribute(FraudSystemRequestMapping.FS_HEADERS, (Base64.getEncoder()
				.encodeToString(FraudSystemUtilities.populateFraudSystemHeaders(request).getBytes())));
		model.addAttribute(PSD2SecurityConstants.JSC_FILE_PATH, jscFilePath);
		model.addAttribute(PSD2Constants.APPLICATION_NAME,applicationName);
		model.addAttribute(PSD2Constants.IS_SANDBOX_ENABLED,sandboxConfig.isSandboxEnabled());
		model.addAttribute(PSD2Constants.UICONTENT, uiController.getConfigVariable());
		model.addAllAttributes(CdnConfig.populateCdnAttributes());
		return "index";
	}

	@RequestMapping(value = "/signin")
	public ModelAndView signIn(@RequestParam(name = "REF") String ref) throws JSONException, NamingException {

		ModelAndView mv = new ModelAndView();
		String scope = requestHeaderAttributes.getScopes();
		ModelMap map = null;
		mv.addObject(PSD2SecurityConstants.SKIP_CONSENT_FLOW, skipConsentFlow);
		if (skipConsentFlow && PSD2Constants.PAYMENTS.equals(scope)) {
			
			map = saasService.getPaymentsDataForConsentView();
		
		} else if (skipConsentFlow && PSD2Constants.ACCOUNTS.equals(scope)) {
			
			map = saasService.getAccountsDataForConsentView();

		} 
		
		JSONObject fraudHdmInfo = new JSONObject();
		try {
			fraudHdmInfo.put(PSD2SecurityConstants.RES_URL, UriUtils.encode(resUrl, StandardCharsets.UTF_8.toString()));
			fraudHdmInfo.put(PSD2SecurityConstants.HDM_URL, UriUtils.encode(hdmUrl, StandardCharsets.UTF_8.toString()));
		} catch (UnsupportedEncodingException e) {
			throw PSD2SecurityException.populatePSD2SecurityException(e.getMessage(),
					SCAConsentErrorCodeEnum.TECHNICAL_ERROR);
		}
		fraudHdmInfo.put(PSD2SecurityConstants.HDM_INPUT_NAME, hdmInputName);
		mv.addAllObjects(map);
		mv.addObject(PSD2Constants.CO_RELATION_ID, requestHeaderAttributes.getCorrelationId());
		mv.addObject(PSD2SecurityConstants.FRAUD_HDM_INFO, fraudHdmInfo.toString());

		mv.addObject(PSD2Constants.APPLICATION_NAME, applicationName);

		mv.addObject(PSD2SecurityConstants.JSC_FILE_PATH, jscFilePath);
		mv.addObject(OIDCConstants.CHANNEL_ID, requestHeaderAttributes.getChannelId());

		mv.addObject(PSD2Constants.NETSCALER_TENANT_ID_HEADER_NAME,
				request.getHeader(PSD2Constants.NETSCALER_TENANT_ID_HEADER_NAME));
		if(null==request.getHeader(PSD2Constants.NETSCALER_TENANT_ID_HEADER_NAME)){
			mv.addObject(PSD2Constants.NETSCALER_TENANT_ID_HEADER_NAME, requestHeaderAttributes.getTenantId());
		}

		mv.addObject(FraudSystemRequestMapping.FS_HEADERS, (Base64.getEncoder()
				.encodeToString(FraudSystemUtilities.populateFraudSystemHeaders(request).getBytes())));

		mv.addObject(PSD2Constants.UICONTENT, uiController.getConfigVariable());
		mv.addObject(PSD2Constants.JS_MSG,
				uiController.retrieveUiParamValue(PSD2Constants.ERROR_MESSAGE, PSD2Constants.JS_MSG));

		// Added for onward provisioning
		String clientId = null;
		if(!skipConsentFlow) {
			// When skipConsentFlow is set to false clientId will not be available at this stage.
			// We will pick it from the pickupDataModel in the request.
			PickupDataModel pickupDataModel = SCAConsentHelper.populatePickupDataModel(request);
			clientId = pickupDataModel.getClientId();
		} else {
			clientId = (String) map.get(PSD2Constants.CLIENT_ID);
		}
		String softwareOnBehalfOfOrg = fetchSoftwareOnBehalfOfOrg(clientId);
		
		mv.addObject(PSD2SecurityConstants.SOFTWARE_ON_BEHALF_OF_ORG, softwareOnBehalfOfOrg);
		
		mv.addAllObjects(CdnConfig.populateCdnAttributes());
		mv.addObject(PSD2Constants.IS_SANDBOX_ENABLED,sandboxConfig.isSandboxEnabled());
		mv.setViewName("index");
		response.setHeader("x-fapi-interaction-id", requestHeaderAttributes.getCorrelationId());
		return mv;
	}

	/**
	 * This method returns the software_on_behalf_of_org value from ssa of the client
	 * This is or onward provisioning.
	 * 
	 * @param clientId
	 * @return software_on_behalf_of_org
	 * @throws NamingException
	 */
	private String fetchSoftwareOnBehalfOfOrg(String clientId) throws NamingException {
		if(clientId!=null && !clientId.isEmpty()) {
			return tppInformationAdaptor.fetchSoftwareOnBehalfOfOrg(clientId);
		}
		return null;
	}

}