package com.capgemini.psd2.security.saas.test.services;

import static org.mockito.Mockito.mock;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.security.core.Authentication;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.aisp.domain.OBAccount6;
import com.capgemini.psd2.aisp.domain.OBAccount6Account;
import com.capgemini.psd2.aisp.domain.OBExternalAccountType1Code;
import com.capgemini.psd2.aisp.domain.OBReadAccount6;
import com.capgemini.psd2.aisp.domain.OBReadAccount6Data;
import com.capgemini.psd2.authentication.adapter.AuthenticationAdapter;
import com.capgemini.psd2.fraudsystem.request.handler.impl.FraudSystemRequestMapping;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.stage.domain.CustomConsentAppViewData;
import com.capgemini.psd2.pisp.stage.domain.CustomDebtorDetails;
import com.capgemini.psd2.rest.client.sync.RestClientSync;
import com.capgemini.psd2.scaconsenthelper.config.helpers.CookieHelper;
import com.capgemini.psd2.security.saas.services.CustomAuthenticationServiceImpl;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class CustomAuthenticationServiceImplTest {

	@InjectMocks
	CustomAuthenticationServiceImpl customAuthenticationServiceImpl;
	
	@Mock
	HttpServletRequest request;
	
	@Mock
	HttpServletResponse response;
	
	@Mock
	RequestHeaderAttributes requestHeaderAttributes;
	
	@Mock
	CookieHelper cookieHelper;
	
	@Mock
	RestClientSync restClientSyncImpl;
	@Mock
	AuthenticationAdapter authenticaitonAdapter;
	
	@Before
	public void setup(){
		MockitoAnnotations.initMocks(this);
		request = mock(HttpServletRequest.class);
		response = mock(HttpServletResponse.class);
		ReflectionTestUtils.setField(customAuthenticationServiceImpl,"userNameRegex","[a-zA-Z]");
		ReflectionTestUtils.setField(customAuthenticationServiceImpl,"passwordRegex","[a-zA-Z]");
		ReflectionTestUtils.setField(customAuthenticationServiceImpl, "consentApplicationUrl", "https://pfadmin.webservices.com:9998/pf-admin-api");
	
	}
	
	@Test
	public void createConsentOnSCATest_AISP(){
		
		requestHeaderAttributes = new RequestHeaderAttributes();
		requestHeaderAttributes.setIntentId("801e14a8872e4e7aae0146d74f853840");
		requestHeaderAttributes.setScopes("accounts");
		ReflectionTestUtils.setField(customAuthenticationServiceImpl, "requestHeaderAttributes", requestHeaderAttributes);
		
		ObjectMapper mapper = new ObjectMapper();
		OBReadAccount6 oBReadAccount = new OBReadAccount6();
		OBReadAccount6Data data = new OBReadAccount6Data();
		List<OBAccount6> accountList = new ArrayList<>();
		OBAccount6 acc1 = new OBAccount6();
		acc1.setAccountId("138aaa38-5517-486b-88ed-a0e39dff612f");
		acc1.setAccountType(OBExternalAccountType1Code.BUSINESS);
		List<OBAccount6Account> accounts = new ArrayList<>();
		OBAccount6Account account1 = new OBAccount6Account();
		accounts.add(account1);
		acc1.setAccount(accounts);
		accountList.add(acc1);
		data.setAccount(accountList);
		oBReadAccount.setData(data);
		
		Cookie cookie = new Cookie("SessionControllCookie", "eyJhbGciOiJSU0EtT0FFUC0yNTYiLCJlbmMiOiJBMjU2R0NNIn0.lZ2nqCeiPzsPmJShsrDD3uA55-06A649CMtwOyuY9nNzMtUGyzV-G8qc4w4ui1uWrtzypBs5Eyq4GfjnTtVHbcDVkS1HVc3tfxNAPY8dfjVrWNz59HyKt4bCjBdqqhBOdZezLtWB9aoWIwZoHLf4D8aUcVUtDsFELVcScmiQNtzHwvpDHZb4oxRfPl-OuOTkKA23C8lnnDMO1KUy8ZXHD4p0jQKAcaV877gYm8NbHDwOBEf-ItWJOGx2jV60apWd0hKqwfFR2QKD9wmGgXpbFZ08ro7X2fj8rTgKWhDgoBT_JVZdVFhVI4T4RLRDrCJqkyeciXhLm7W_xNhWBXAMrA.94SuB596ZLuUtw53wrofwN5jZXfT5f-ZarJhQc9Mj0M.0Ow5DXfilYX3ty49H4lNMNPljlWAFASc49zljhRSIIUSlmUHLZo0SAezn-n_FdxexAIYLk_FtRgnkMHDEyxJ1V1yHhqa1Jvdb36lTYyptqCJhMkOV1XGn58L4Z9QQmdrIZnn5iHxZ9-N1Jfjs0eoKiLBgR9O7ZEcs7QrWZVT6n_HrGrIloYQu_lFgmk5O7k47_15CVXaFqIohpHXETejoHEwjQj-iTToNRaHWNFAKvlpUBz4mUgk9RSIQCxK1GxxS8wxP44w5G4HdOIjFNwTsRDXeSZy0mU9zTNUCmDEUT9MFESfmVU1nPurdT-VoiPvVklbJZW8Sas0hWgqQkdQdP35nFY1sjCgfMB9iYUeEU-TCE219wkm1XXrLJwLEYZclL_4ckl4zExo2wb3Czwd8f5iO9fBQQWZ4mdwThK4VtZaPs1JEkxwGLI0SHA8Jr-e2PsDrkGEnxs74FsJ5MKluU2ZKvKcGXyQPaaTRa0ecJLD5-YYBuTtxOnU3gM_5aZm97pd_wiPk_h81r5aiwjSfRF3Ihxp37KNPfNOMJoA9xe2F51m1AvmjrOUgSM156LwmFyJFebVfarb9NPtJ_q1wU891sCu2Vmv520BR4QfIc-ayIwTVxLgZSN-BP7PhEJb_x8.XhZpINBxRdFFEgwPTcAgJg");
		
		Mockito.when(request.getParameter(FraudSystemRequestMapping.FS_HEADERS)).thenReturn("x-fapi-financial-id:0015800000jfQ9aAAE,Content-Type:application/json,Authorization:Bearer UwwygQ5Wpsc0Khd48JnM3HNQpsR2");
		String obReadAccount3String = null;
		try {
			obReadAccount3String = mapper.writeValueAsString(oBReadAccount);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		Mockito.when(request.getParameter(PSD2Constants.PSU_ACCOUNTS)).thenReturn(obReadAccount3String);
		ReflectionTestUtils.setField(customAuthenticationServiceImpl, "request", request);
		Mockito.when(cookieHelper.setBypassConsentCookie(request)).thenReturn(cookie);
		customAuthenticationServiceImpl.createConsentOnSCA();
	}
	
	@Test
	public void createConsentOnSCATest_PISP(){
		requestHeaderAttributes = new RequestHeaderAttributes();
		requestHeaderAttributes.setIntentId("801e14a8872e4e7aae0146d74f853840");
		requestHeaderAttributes.setScopes("payments");
		ReflectionTestUtils.setField(customAuthenticationServiceImpl, "requestHeaderAttributes", requestHeaderAttributes);
		
		ObjectMapper mapper = new ObjectMapper();
		CustomConsentAppViewData customConsentAppViewData = new CustomConsentAppViewData();
		CustomDebtorDetails customDebtorDetails = new CustomDebtorDetails();
		customDebtorDetails.setSchemeName("UK.OBIE.IBAN");
		customDebtorDetails.setIdentification("FR1420041010050500013M02606");
		customDebtorDetails.setName("testScheme");
		customDebtorDetails.setSecondaryIdentification("123");
		customConsentAppViewData.setDebtorDetails(customDebtorDetails);
		
		Cookie cookie = new Cookie("SessionControllCookie", "eyJhbGciOiJSU0EtT0FFUC0yNTYiLCJlbmMiOiJBMjU2R0NNIn0.lZ2nqCeiPzsPmJShsrDD3uA55-06A649CMtwOyuY9nNzMtUGyzV-G8qc4w4ui1uWrtzypBs5Eyq4GfjnTtVHbcDVkS1HVc3tfxNAPY8dfjVrWNz59HyKt4bCjBdqqhBOdZezLtWB9aoWIwZoHLf4D8aUcVUtDsFELVcScmiQNtzHwvpDHZb4oxRfPl-OuOTkKA23C8lnnDMO1KUy8ZXHD4p0jQKAcaV877gYm8NbHDwOBEf-ItWJOGx2jV60apWd0hKqwfFR2QKD9wmGgXpbFZ08ro7X2fj8rTgKWhDgoBT_JVZdVFhVI4T4RLRDrCJqkyeciXhLm7W_xNhWBXAMrA.94SuB596ZLuUtw53wrofwN5jZXfT5f-ZarJhQc9Mj0M.0Ow5DXfilYX3ty49H4lNMNPljlWAFASc49zljhRSIIUSlmUHLZo0SAezn-n_FdxexAIYLk_FtRgnkMHDEyxJ1V1yHhqa1Jvdb36lTYyptqCJhMkOV1XGn58L4Z9QQmdrIZnn5iHxZ9-N1Jfjs0eoKiLBgR9O7ZEcs7QrWZVT6n_HrGrIloYQu_lFgmk5O7k47_15CVXaFqIohpHXETejoHEwjQj-iTToNRaHWNFAKvlpUBz4mUgk9RSIQCxK1GxxS8wxP44w5G4HdOIjFNwTsRDXeSZy0mU9zTNUCmDEUT9MFESfmVU1nPurdT-VoiPvVklbJZW8Sas0hWgqQkdQdP35nFY1sjCgfMB9iYUeEU-TCE219wkm1XXrLJwLEYZclL_4ckl4zExo2wb3Czwd8f5iO9fBQQWZ4mdwThK4VtZaPs1JEkxwGLI0SHA8Jr-e2PsDrkGEnxs74FsJ5MKluU2ZKvKcGXyQPaaTRa0ecJLD5-YYBuTtxOnU3gM_5aZm97pd_wiPk_h81r5aiwjSfRF3Ihxp37KNPfNOMJoA9xe2F51m1AvmjrOUgSM156LwmFyJFebVfarb9NPtJ_q1wU891sCu2Vmv520BR4QfIc-ayIwTVxLgZSN-BP7PhEJb_x8.XhZpINBxRdFFEgwPTcAgJg");
		
		Mockito.when(request.getParameter(FraudSystemRequestMapping.FS_HEADERS)).thenReturn("x-fapi-financial-id:0015800000jfQ9aAAE,Content-Type:application/json,Authorization:Bearer UwwygQ5Wpsc0Khd48JnM3HNQpsR2");
		String customConsentAppViewDataString = null;
		try {
			customConsentAppViewDataString = mapper.writeValueAsString(customConsentAppViewData);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Mockito.when(request.getParameter(PSD2Constants.PAYMENT_SETUP)).thenReturn(customConsentAppViewDataString);
		ReflectionTestUtils.setField(customAuthenticationServiceImpl, "request", request);
		Mockito.when(cookieHelper.setBypassConsentCookie(request)).thenReturn(cookie);
		customAuthenticationServiceImpl.createConsentOnSCA();
	}
	@Test
	public void authenticateUserTest(){
		Authentication authentication =mock(Authentication.class);
		 Map<String, String> headers = new HashMap<>();
		 String corrId ="assd";
		 requestHeaderAttributes.setCorrelationId(corrId);
		 Mockito.when(requestHeaderAttributes.getCorrelationId()).thenReturn(corrId);
	        headers.put(PSD2Constants.CORRELATION_ID, corrId);
	       Mockito.when(authentication.getPrincipal()).thenReturn("Sdfsd");
	       
	        headers.put(PSD2Constants.USER_IN_REQ_HEADER, "Sdfsd");
	        Mockito.when(requestHeaderAttributes.getChannelId()).thenReturn("fdds");
	        headers.put(PSD2Constants.CHANNEL_IN_REQ_HEADER, "fdds");
	        Mockito.when(authenticaitonAdapter.authenticate(authentication, headers)).thenReturn(authentication);
		customAuthenticationServiceImpl.authenticateUser(authentication);
	}
	
	@Test
	public void validateUserAndPassTest(){
		Authentication authentication =mock(Authentication.class);
		 Mockito.when(authentication.getPrincipal()).thenReturn("Sdfsd");
		 Mockito.when(authentication.getCredentials()).thenReturn("sdfiPsf");	       
		customAuthenticationServiceImpl.validUserAndPassword(authentication);
	}
	@Test
	public void validateUserAndPassWithInvalidTest(){
		Authentication authentication =mock(Authentication.class);
		 Mockito.when(authentication.getPrincipal()).thenReturn("423");
		 Mockito.when(authentication.getCredentials()).thenReturn("327");	       
		customAuthenticationServiceImpl.validUserAndPassword(authentication);
	}
}
