package com.capgemini.psd2.security.saas.test.controllers;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.naming.NamingException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.security.web.WebAttributes;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;

import com.capgemini.psd2.aisp.adapter.AccountRequestAdapter;
import com.capgemini.psd2.exceptions.ErrorInfo;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.LoggerUtils;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.scaconsenthelper.config.CdnConfig;
import com.capgemini.psd2.scaconsenthelper.config.helpers.SCAConsentAdapterHelper;
import com.capgemini.psd2.scaconsenthelper.constants.PFConstants;
import com.capgemini.psd2.scaconsenthelper.constants.SCAConsentHelperConstants;
import com.capgemini.psd2.scaconsenthelper.models.SecurityRequestAttributes;
import com.capgemini.psd2.security.exceptions.PSD2AuthenticationException;
import com.capgemini.psd2.security.saas.controllers.SaaSController;
import com.capgemini.psd2.ui.content.utility.controller.UIStaticContentUtilityController;
import com.capgemini.psd2.utilities.SandboxConfig;

@RunWith(SpringJUnit4ClassRunner.class)
public class SaaSControllerTest {

	private MockMvc mockMvc;

	@Mock
	private LoggerUtils loggerUtils;

	@Mock
	private RequestHeaderAttributes requestHeaderAttributes;

	@Mock
	private HttpServletRequest request;

	@Mock
	private HttpServletResponse response;

	@Mock
	private SecurityRequestAttributes securityRequestAttributes;

	@Mock
	private SCAConsentAdapterHelper scaConsentAdapterHelper;

	@Mock
	private UIStaticContentUtilityController uiController;

	@Mock
	private SandboxConfig sandboxConfig;

	@InjectMocks
	private SaaSController SaaSController;

	@Mock
	private ErrorInfo errorInfo;

	@Mock 
	private Model model= new  ExtendedModelMap();
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		mockMvc = MockMvcBuilders.standaloneSetup(SaaSController).dispatchOptions(true).build();
	}

	@Test(expected = PSD2Exception.class)
	public void handleSessionTimeOutOnCancelTest() throws Exception {
		Model model = mock(Model.class);
		mockMvc.perform(get("/signin").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isBadRequest());
		ErrorInfo errorInfo = new ErrorInfo("500");
		errorInfo.setErrorCode("123");
		Enumeration<String> fsHeader = new Enumeration<String>() {

			@Override
			public String nextElement() {
				return null;
			}

			@Override
			public boolean hasMoreElements() {
				return false;
			}
		};
		when(request.getHeaderNames()).thenReturn(fsHeader);
		SaaSController.handleSessionTimeOutOnCancel(model);
	}

	@Test
	public void errorWithResumePathTest() throws Exception {
		Model model = mock(Model.class);
		mockMvc.perform(get("/signin").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isBadRequest());
		ErrorInfo errorInfo = new ErrorInfo("500");
		errorInfo.setErrorCode("123");
		errorInfo.setStatusCode("123");
		when(request.getAttribute(WebAttributes.AUTHENTICATION_EXCEPTION)).thenReturn(null);
		when(request.getAttribute(SCAConsentHelperConstants.EXCEPTION)).thenReturn(errorInfo);
		when(request.getParameter(PFConstants.RESUME_PATH)).thenReturn("abcd");
		Enumeration<String> fsHeader = new Enumeration<String>() {

			@Override
			public String nextElement() {
				return null;
			}

			@Override
			public boolean hasMoreElements() {
				return false;
			}
		};
		when(request.getHeaderNames()).thenReturn(fsHeader);
		assertNotNull(SaaSController.error(model));
	}

	@Test
	public void errorWithoutResumePathTest() throws Exception {
		Model model = mock(Model.class);
		mockMvc.perform(get("/signin").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isBadRequest());
		ErrorInfo errorInfo = new ErrorInfo("500");
		errorInfo.setErrorCode("123");
		errorInfo.setStatusCode("123");
		when(request.getAttribute(WebAttributes.AUTHENTICATION_EXCEPTION)).thenReturn(null);
		when(request.getAttribute(SCAConsentHelperConstants.EXCEPTION)).thenReturn(errorInfo);
		when(request.getAttribute(PFConstants.RESUME_PATH)).thenReturn(null);		
		when(request.getParameter(PFConstants.RESUME_PATH)).thenReturn(null);
		Enumeration<String> fsHeader = new Enumeration<String>() {

			@Override
			public String nextElement() {
				return null;
			}

			@Override
			public boolean hasMoreElements() {
				return false;
			}
		};
		when(request.getHeaderNames()).thenReturn(fsHeader);
		assertNotNull(SaaSController.error(model));
	}
	
	@Test
	public void errorWithoutResumePathTest2() throws Exception {
		Model model = mock(Model.class);
		mockMvc.perform(get("/signin").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isBadRequest());
		ErrorInfo errorInfo = new ErrorInfo("500");
		errorInfo.setErrorCode("123");
		errorInfo.setStatusCode("123");
		when(request.getAttribute(WebAttributes.AUTHENTICATION_EXCEPTION)).thenReturn(null);
		when(request.getAttribute(SCAConsentHelperConstants.EXCEPTION)).thenReturn(errorInfo);
		when(request.getAttribute(PFConstants.RESUME_PATH)).thenReturn(null);
		when(request.getParameter(PFConstants.RESUME_PATH)).thenReturn(PFConstants.RESUME_PATH);
		Enumeration<String> fsHeader = new Enumeration<String>() {

			@Override
			public String nextElement() {
				return null;
			}

			@Override
			public boolean hasMoreElements() {
				return false;
			}
		};
		when(request.getHeaderNames()).thenReturn(fsHeader);
		assertNotNull(SaaSController.error(model));
	}

	@Test
	public void errorWithstatusCode() throws Exception {
		//Model model = mock(Model.class);
		mockMvc.perform(get("/signin").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isBadRequest());
		//ErrorInfo errorInfo = new ErrorInfo("500");
		errorInfo.setErrorCode("123");
		errorInfo.setStatusCode("123");
		when(errorInfo.getStatusCode()).thenReturn("500");
		PSD2AuthenticationException exception = new PSD2AuthenticationException("123", errorInfo);
		when(request.getAttribute(WebAttributes.AUTHENTICATION_EXCEPTION)).thenReturn(exception);
		when(request.getAttribute(SCAConsentHelperConstants.EXCEPTION)).thenReturn(errorInfo);
		when(request.getParameter(SCAConsentHelperConstants.OAUTH_URL_PARAM)).thenReturn("abcd");
		Enumeration<String> fsHeader = new Enumeration<String>() {

			@Override
			public String nextElement() {
				return null;
			}

			@Override
			public boolean hasMoreElements() {
				return false;
			}
		};
		when(request.getHeaderNames()).thenReturn(fsHeader);
		assertNotNull(SaaSController.error(model));
	}
	@Test
	public void errorWithstatusCode2() throws Exception {
		//Model model = mock(Model.class);
		mockMvc.perform(get("/signin").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isBadRequest());
		//ErrorInfo errorInfo = new ErrorInfo("500");
		errorInfo.setErrorCode("123");
		errorInfo.setStatusCode("123");
		when(errorInfo.getStatusCode()).thenReturn("503");
		PSD2AuthenticationException exception = new PSD2AuthenticationException("123", errorInfo);
		when(request.getAttribute(WebAttributes.AUTHENTICATION_EXCEPTION)).thenReturn(exception);
		when(request.getAttribute(SCAConsentHelperConstants.EXCEPTION)).thenReturn(errorInfo);
		when(request.getParameter(SCAConsentHelperConstants.OAUTH_URL_PARAM)).thenReturn("abcd");
		Enumeration<String> fsHeader = new Enumeration<String>() {

			@Override
			public String nextElement() {
				return null;
			}

			@Override
			public boolean hasMoreElements() {
				return false;
			}
		};
		when(request.getHeaderNames()).thenReturn(fsHeader);
		assertNotNull(SaaSController.error(model));
	}
	
	@Test
	public void errorWithoutCorrelationId() throws Exception {
		//Model model = mock(Model.class);
		mockMvc.perform(get("/signin").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isBadRequest());
		//ErrorInfo errorInfo = new ErrorInfo("500");
		errorInfo.setErrorCode("123");
		errorInfo.setStatusCode("123");
		when(errorInfo.getStatusCode()).thenReturn("503");
		PSD2AuthenticationException exception = new PSD2AuthenticationException("123", errorInfo);
		when(request.getAttribute(WebAttributes.AUTHENTICATION_EXCEPTION)).thenReturn(exception);
		when(request.getAttribute(SCAConsentHelperConstants.EXCEPTION)).thenReturn(errorInfo);
		when(request.getParameter(SCAConsentHelperConstants.OAUTH_URL_PARAM)).thenReturn("abcd");
		when( requestHeaderAttributes.getCorrelationId()).thenReturn(null);
		Enumeration<String> fsHeader = new Enumeration<String>() {

			@Override
			public String nextElement() {
				return null;
			}

			@Override
			public boolean hasMoreElements() {
				return false;
			}
		};
		when(request.getHeaderNames()).thenReturn(fsHeader);
		assertNotNull(SaaSController.error(model));
	}

	@Test
	public void errorWithOAuthURLParamTest() throws Exception {
		Model model = mock(Model.class);
		mockMvc.perform(get("/signin").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isBadRequest());
		ErrorInfo errorInfo = new ErrorInfo("500");
		errorInfo.setErrorCode("123");
		errorInfo.setStatusCode("123");
		PSD2AuthenticationException exception = new PSD2AuthenticationException("123", errorInfo);
		when(request.getAttribute(WebAttributes.AUTHENTICATION_EXCEPTION)).thenReturn(exception);
		when(request.getAttribute(SCAConsentHelperConstants.EXCEPTION)).thenReturn(errorInfo);
		when(request.getParameter(SCAConsentHelperConstants.OAUTH_URL_PARAM)).thenReturn("abcd");
		Enumeration<String> fsHeader = new Enumeration<String>() {

			@Override
			public String nextElement() {
				return null;
			}

			@Override
			public boolean hasMoreElements() {
				return false;
			}
		};
		when(request.getHeaderNames()).thenReturn(fsHeader);
		assertNotNull(SaaSController.error(model));
	}

	@Test
	public void viewErrorsTest() {
		CdnConfig cdn = mock(CdnConfig.class);
		ReflectionTestUtils.setField(cdn, "cdnBaseURL", "abcd");
		Map<String, Object> model = new HashMap<>();
		SaaSController.viewErrors(model);
	}

	@Test
	public void consentViewWithNoOAuthUrlParam() throws Exception {
		mockMvc.perform(get("/signin").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isBadRequest());
	}

	@Ignore
	@Test
	public void signIntest() throws JSONException, NamingException {
		when(sandboxConfig.isSandboxEnabled()).thenReturn(true);
		when(requestHeaderAttributes.getCorrelationId()).thenReturn("12345");
		Enumeration<String> fsHeader = new Enumeration<String>() {

			@Override
			public String nextElement() {
				return null;
			}

			@Override
			public boolean hasMoreElements() {
				return false;
			}
		};
		when(request.getHeaderNames()).thenReturn(fsHeader);
		SaaSController.signIn("abcd");
	}
}
