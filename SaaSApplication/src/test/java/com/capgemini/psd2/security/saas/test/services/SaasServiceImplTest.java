package com.capgemini.psd2.security.saas.test.services;

import static org.mockito.Mockito.mock;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.aisp.adapter.AccountRequestAdapter;
import com.capgemini.psd2.aisp.adapter.AispConsentAdapter;
import com.capgemini.psd2.aisp.domain.OBAccount6;
import com.capgemini.psd2.aisp.domain.OBAccount6Account;
import com.capgemini.psd2.aisp.domain.OBExternalAccountType1Code;
import com.capgemini.psd2.aisp.domain.OBReadAccount6;
import com.capgemini.psd2.aisp.domain.OBReadConsentResponse1;
import com.capgemini.psd2.aisp.domain.OBReadConsentResponse1Data;
import com.capgemini.psd2.aisp.domain.OBReadConsentResponse1Data.StatusEnum;
import com.capgemini.psd2.enums.ConsentStatusEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.mask.DataMask;
import com.capgemini.psd2.pisp.domain.CustomDPaymentConsentsPOSTResponse;
import com.capgemini.psd2.pisp.stage.domain.CustomConsentAppViewData;
import com.capgemini.psd2.pisp.stage.domain.CustomDebtorDetails;
import com.capgemini.psd2.scaconsenthelper.config.helpers.ConsentViewHelper;
import com.capgemini.psd2.scaconsenthelper.config.helpers.CookieHelper;
import com.capgemini.psd2.scaconsenthelper.config.helpers.SCAConsentAdapterHelper;
import com.capgemini.psd2.scaconsenthelper.constants.SCAConsentHelperConstants;
import com.capgemini.psd2.scaconsenthelper.models.IntentTypeEnum;
import com.capgemini.psd2.scaconsenthelper.models.PickupDataModel;
import com.capgemini.psd2.security.saas.services.SaasServiceImpl;
import com.capgemini.psd2.security.saas.test.mock.data.SaaSMockData;

public class SaasServiceImplTest {
	
	@InjectMocks
	SaasServiceImpl saasServiceImpl;
	
	@Mock
	ConsentViewHelper consentViewHelper;

	@Mock
	AispConsentAdapter aispConsentAdapter;

	@Mock
	RequestHeaderAttributes requestHeaderAttributes;

	@Mock
	SCAConsentAdapterHelper scaConsentAdapterHelper;

	@Mock
	CookieHelper cookieHelper;

	@Mock
	DataMask dataMask;
	
	@Mock
	HttpServletRequest request;
	
	@Mock
	HttpServletResponse response;
	
	
	@Before
	public void setup(){
		MockitoAnnotations.initMocks(this);
		request = mock(HttpServletRequest.class);
		response = mock(HttpServletResponse.class);
	
	}
	
	@Test
	public void getAccountsDataForConsentViewTest(){
		
		
		PickupDataModel model = new PickupDataModel();
		model.setChannelId("123");
		model.setIntentId("11737fe5d8284fe89f400d608db8dc25");
		model.setUserId("88888888");
		model.setIntentTypeEnum(IntentTypeEnum.PISP_INTENT_TYPE);
		model.getIntentTypeEnum().setIntentType("DOMESTIC_PAY");
		model.setClientId("5pRXn4FJ7bSbKwb7x5daKu");
		request.setAttribute(SCAConsentHelperConstants.INTENT_DATA, model);
		
		List<OBAccount6Account> accountList = new ArrayList<>();
		OBAccount6Account obAccount3Account = new OBAccount6Account();
		obAccount3Account.setSchemeName("UK.OBIE.IBAN");
		obAccount3Account.setIdentification("FR1420041010050500013M02606");
		accountList.add(obAccount3Account);
		
		List<OBAccount6> accountListFromAcccountDetails = new ArrayList<>();
		OBAccount6 account1 = new OBAccount6();
		account1.setAccountId("8a96256d-fea5-4538-beba-21300553a6f3");
		account1.setAccountType(OBExternalAccountType1Code.BUSINESS);
		account1.setAccount(accountList);
		
		OBReadConsentResponse1 accountReq = new OBReadConsentResponse1();
		OBReadConsentResponse1Data data =new OBReadConsentResponse1Data();
		data.setConsentId("11737fe5d8284fe89f400d608db8dc25");
		data.setAccountRequestId("0e9c7b31-0de7-482e-963d-509118c24747");
		data.setCmaVersion("3.1");
		data.setStatus(StatusEnum.AWAITINGAUTHORISATION);
	
		accountReq.setData(data);
		
		Cookie cookie = new Cookie("SessionControllCookie", "eyJhbGciOiJSU0EtT0FFUC0yNTYiLCJlbmMiOiJBMjU2R0NNIn0.lZ2nqCeiPzsPmJShsrDD3uA55-06A649CMtwOyuY9nNzMtUGyzV-G8qc4w4ui1uWrtzypBs5Eyq4GfjnTtVHbcDVkS1HVc3tfxNAPY8dfjVrWNz59HyKt4bCjBdqqhBOdZezLtWB9aoWIwZoHLf4D8aUcVUtDsFELVcScmiQNtzHwvpDHZb4oxRfPl-OuOTkKA23C8lnnDMO1KUy8ZXHD4p0jQKAcaV877gYm8NbHDwOBEf-ItWJOGx2jV60apWd0hKqwfFR2QKD9wmGgXpbFZ08ro7X2fj8rTgKWhDgoBT_JVZdVFhVI4T4RLRDrCJqkyeciXhLm7W_xNhWBXAMrA.94SuB596ZLuUtw53wrofwN5jZXfT5f-ZarJhQc9Mj0M.0Ow5DXfilYX3ty49H4lNMNPljlWAFASc49zljhRSIIUSlmUHLZo0SAezn-n_FdxexAIYLk_FtRgnkMHDEyxJ1V1yHhqa1Jvdb36lTYyptqCJhMkOV1XGn58L4Z9QQmdrIZnn5iHxZ9-N1Jfjs0eoKiLBgR9O7ZEcs7QrWZVT6n_HrGrIloYQu_lFgmk5O7k47_15CVXaFqIohpHXETejoHEwjQj-iTToNRaHWNFAKvlpUBz4mUgk9RSIQCxK1GxxS8wxP44w5G4HdOIjFNwTsRDXeSZy0mU9zTNUCmDEUT9MFESfmVU1nPurdT-VoiPvVklbJZW8Sas0hWgqQkdQdP35nFY1sjCgfMB9iYUeEU-TCE219wkm1XXrLJwLEYZclL_4ckl4zExo2wb3Czwd8f5iO9fBQQWZ4mdwThK4VtZaPs1JEkxwGLI0SHA8Jr-e2PsDrkGEnxs74FsJ5MKluU2ZKvKcGXyQPaaTRa0ecJLD5-YYBuTtxOnU3gM_5aZm97pd_wiPk_h81r5aiwjSfRF3Ihxp37KNPfNOMJoA9xe2F51m1AvmjrOUgSM156LwmFyJFebVfarb9NPtJ_q1wU891sCu2Vmv520BR4QfIc-ayIwTVxLgZSN-BP7PhEJb_x8.XhZpINBxRdFFEgwPTcAgJg");
		
		Mockito.when(request.getAttribute(SCAConsentHelperConstants.INTENT_DATA)).thenReturn(model);
		Mockito.when(aispConsentAdapter.retrieveConsentByAccountRequestIdAndStatus(
				"11737fe5d8284fe89f400d608db8dc25", ConsentStatusEnum.AUTHORISED)).thenReturn(SaaSMockData.getMockAispConsent());
		Mockito.when(dataMask.maskResponseGenerateString(new OBReadAccount6(), "account")).thenReturn("accountInfoList");
		Mockito.when(scaConsentAdapterHelper
					.getAccountRequestSetupData(model.getIntentId())).thenReturn(accountReq);
		Mockito.when(cookieHelper.createNewSessionCookie(model)).thenReturn(cookie);
		
		ReflectionTestUtils.setField(saasServiceImpl, "request", request);
		saasServiceImpl.getAccountsDataForConsentView();
	}
	@Test(expected=PSD2Exception.class)
	public void getAccountsDataForConsentViewTest2(){
		
		
		PickupDataModel model = new PickupDataModel();
		model.setChannelId("123");
		model.setIntentId("11737fe5d8284fe89f400d608db8dc25");
		model.setUserId("88888888");
		model.setIntentTypeEnum(IntentTypeEnum.PISP_INTENT_TYPE);
		model.getIntentTypeEnum().setIntentType("DOMESTIC_PAY");
		model.setClientId("5pRXn4FJ7bSbKwb7x5daKu");
		request.setAttribute(SCAConsentHelperConstants.INTENT_DATA, model);
		
		List<OBAccount6Account> accountList = new ArrayList<>();
		OBAccount6Account obAccount3Account = new OBAccount6Account();
		obAccount3Account.setSchemeName("UK.OBIE.IBAN");
		obAccount3Account.setIdentification("FR1420041010050500013M02606");
		accountList.add(obAccount3Account);
		
		List<OBAccount6> accountListFromAcccountDetails = new ArrayList<>();
		OBAccount6 account1 = new OBAccount6();
		account1.setAccountId("8a96256d-fea5-4538-beba-21300553a6f3");
		account1.setAccountType(OBExternalAccountType1Code.BUSINESS);
		account1.setAccount(accountList);
		
		OBReadConsentResponse1 accountReq = new OBReadConsentResponse1();
		OBReadConsentResponse1Data data =new OBReadConsentResponse1Data();
		data.setConsentId("11737fe5d8284fe89f400d608db8dc25");
		data.setAccountRequestId("0e9c7b31-0de7-482e-963d-509118c24747");
		data.setCmaVersion("3.1");
		data.setStatus(StatusEnum.AWAITINGAUTHORISATION);
	
		accountReq.setData(data);
		
		Cookie cookie = new Cookie("SessionControllCookie", "eyJhbGciOiJSU0EtT0FFUC0yNTYiLCJlbmMiOiJBMjU2R0NNIn0.lZ2nqCeiPzsPmJShsrDD3uA55-06A649CMtwOyuY9nNzMtUGyzV-G8qc4w4ui1uWrtzypBs5Eyq4GfjnTtVHbcDVkS1HVc3tfxNAPY8dfjVrWNz59HyKt4bCjBdqqhBOdZezLtWB9aoWIwZoHLf4D8aUcVUtDsFELVcScmiQNtzHwvpDHZb4oxRfPl-OuOTkKA23C8lnnDMO1KUy8ZXHD4p0jQKAcaV877gYm8NbHDwOBEf-ItWJOGx2jV60apWd0hKqwfFR2QKD9wmGgXpbFZ08ro7X2fj8rTgKWhDgoBT_JVZdVFhVI4T4RLRDrCJqkyeciXhLm7W_xNhWBXAMrA.94SuB596ZLuUtw53wrofwN5jZXfT5f-ZarJhQc9Mj0M.0Ow5DXfilYX3ty49H4lNMNPljlWAFASc49zljhRSIIUSlmUHLZo0SAezn-n_FdxexAIYLk_FtRgnkMHDEyxJ1V1yHhqa1Jvdb36lTYyptqCJhMkOV1XGn58L4Z9QQmdrIZnn5iHxZ9-N1Jfjs0eoKiLBgR9O7ZEcs7QrWZVT6n_HrGrIloYQu_lFgmk5O7k47_15CVXaFqIohpHXETejoHEwjQj-iTToNRaHWNFAKvlpUBz4mUgk9RSIQCxK1GxxS8wxP44w5G4HdOIjFNwTsRDXeSZy0mU9zTNUCmDEUT9MFESfmVU1nPurdT-VoiPvVklbJZW8Sas0hWgqQkdQdP35nFY1sjCgfMB9iYUeEU-TCE219wkm1XXrLJwLEYZclL_4ckl4zExo2wb3Czwd8f5iO9fBQQWZ4mdwThK4VtZaPs1JEkxwGLI0SHA8Jr-e2PsDrkGEnxs74FsJ5MKluU2ZKvKcGXyQPaaTRa0ecJLD5-YYBuTtxOnU3gM_5aZm97pd_wiPk_h81r5aiwjSfRF3Ihxp37KNPfNOMJoA9xe2F51m1AvmjrOUgSM156LwmFyJFebVfarb9NPtJ_q1wU891sCu2Vmv520BR4QfIc-ayIwTVxLgZSN-BP7PhEJb_x8.XhZpINBxRdFFEgwPTcAgJg");
		
		Mockito.when(request.getAttribute(SCAConsentHelperConstants.INTENT_DATA)).thenReturn(model);
		Mockito.when(aispConsentAdapter.retrieveConsentByAccountRequestIdAndStatus(
				"11737fe5d8284fe89f400d608db8dc25", ConsentStatusEnum.AUTHORISED)).thenReturn(SaaSMockData.getMockAispConsent());
		Mockito.when(dataMask.maskResponseGenerateString(new OBReadAccount6(), "account")).thenReturn("accountInfoList");
		Mockito.when(scaConsentAdapterHelper.getAccountRequestSetupData(model.getIntentId())).thenReturn(null);
		Mockito.when(cookieHelper.createNewSessionCookie(model)).thenReturn(cookie);
		
		ReflectionTestUtils.setField(saasServiceImpl, "request", request);
		saasServiceImpl.getAccountsDataForConsentView();
	}
	
	@Test
	public void getAccountsDataForConsentViewNullTest(){
		
		
		PickupDataModel model = new PickupDataModel();
		model.setChannelId("123");
		model.setIntentId("11737fe5d8284fe89f400d608db8dc25");
		model.setUserId("88888888");
		model.setIntentTypeEnum(IntentTypeEnum.PISP_INTENT_TYPE);
		model.getIntentTypeEnum().setIntentType("DOMESTIC_PAY");
		model.setClientId("5pRXn4FJ7bSbKwb7x5daKu");
		request.setAttribute(SCAConsentHelperConstants.INTENT_DATA, model);
		
		List<OBAccount6Account> accountList = new ArrayList<>();
		OBAccount6Account obAccount3Account = new OBAccount6Account();
		obAccount3Account.setSchemeName("UK.OBIE.IBAN");
		obAccount3Account.setIdentification("FR1420041010050500013M02606");
		accountList.add(obAccount3Account);
		
		List<OBAccount6> accountListFromAcccountDetails = new ArrayList<>();
		OBAccount6 account1 = new OBAccount6();
		account1.setAccountId("8a96256d-fea5-4538-beba-21300553a6f3");
		account1.setAccountType(OBExternalAccountType1Code.BUSINESS);
		account1.setAccount(accountList);
		
		OBReadConsentResponse1 accountReq = new OBReadConsentResponse1();
		OBReadConsentResponse1Data data =new OBReadConsentResponse1Data();
		data.setConsentId("11737fe5d8284fe89f400d608db8dc25");
		data.setAccountRequestId("0e9c7b31-0de7-482e-963d-509118c24747");
		data.setCmaVersion("3.1");
		data.setStatus(StatusEnum.AWAITINGAUTHORISATION);
	
		accountReq.setData(data);
		
		Cookie cookie = new Cookie("SessionControllCookie", "eyJhbGciOiJSU0EtT0FFUC0yNTYiLCJlbmMiOiJBMjU2R0NNIn0.lZ2nqCeiPzsPmJShsrDD3uA55-06A649CMtwOyuY9nNzMtUGyzV-G8qc4w4ui1uWrtzypBs5Eyq4GfjnTtVHbcDVkS1HVc3tfxNAPY8dfjVrWNz59HyKt4bCjBdqqhBOdZezLtWB9aoWIwZoHLf4D8aUcVUtDsFELVcScmiQNtzHwvpDHZb4oxRfPl-OuOTkKA23C8lnnDMO1KUy8ZXHD4p0jQKAcaV877gYm8NbHDwOBEf-ItWJOGx2jV60apWd0hKqwfFR2QKD9wmGgXpbFZ08ro7X2fj8rTgKWhDgoBT_JVZdVFhVI4T4RLRDrCJqkyeciXhLm7W_xNhWBXAMrA.94SuB596ZLuUtw53wrofwN5jZXfT5f-ZarJhQc9Mj0M.0Ow5DXfilYX3ty49H4lNMNPljlWAFASc49zljhRSIIUSlmUHLZo0SAezn-n_FdxexAIYLk_FtRgnkMHDEyxJ1V1yHhqa1Jvdb36lTYyptqCJhMkOV1XGn58L4Z9QQmdrIZnn5iHxZ9-N1Jfjs0eoKiLBgR9O7ZEcs7QrWZVT6n_HrGrIloYQu_lFgmk5O7k47_15CVXaFqIohpHXETejoHEwjQj-iTToNRaHWNFAKvlpUBz4mUgk9RSIQCxK1GxxS8wxP44w5G4HdOIjFNwTsRDXeSZy0mU9zTNUCmDEUT9MFESfmVU1nPurdT-VoiPvVklbJZW8Sas0hWgqQkdQdP35nFY1sjCgfMB9iYUeEU-TCE219wkm1XXrLJwLEYZclL_4ckl4zExo2wb3Czwd8f5iO9fBQQWZ4mdwThK4VtZaPs1JEkxwGLI0SHA8Jr-e2PsDrkGEnxs74FsJ5MKluU2ZKvKcGXyQPaaTRa0ecJLD5-YYBuTtxOnU3gM_5aZm97pd_wiPk_h81r5aiwjSfRF3Ihxp37KNPfNOMJoA9xe2F51m1AvmjrOUgSM156LwmFyJFebVfarb9NPtJ_q1wU891sCu2Vmv520BR4QfIc-ayIwTVxLgZSN-BP7PhEJb_x8.XhZpINBxRdFFEgwPTcAgJg");
		
		Mockito.when(request.getAttribute(SCAConsentHelperConstants.INTENT_DATA)).thenReturn(model);
		Mockito.when(aispConsentAdapter.retrieveConsentByAccountRequestIdAndStatus(
				"11737fe5d8284fe89f400d608db8dc25", ConsentStatusEnum.AUTHORISED)).thenReturn(null);
		Mockito.when(dataMask.maskResponseGenerateString(new OBReadAccount6(), "account")).thenReturn("accountInfoList");
		Mockito.when(scaConsentAdapterHelper.getAccountRequestSetupData(model.getIntentId())).thenReturn(null);
		Mockito.when(cookieHelper.createNewSessionCookie(model)).thenReturn(cookie);
		
		ReflectionTestUtils.setField(saasServiceImpl, "request", request);
		saasServiceImpl.getAccountsDataForConsentView();
	}
	
	@Test
	public void getPaymentsDataForConsentViewTest(){
		

		PickupDataModel model = new PickupDataModel();
		model.setChannelId("123");
		model.setIntentId("11737fe5d8284fe89f400d608db8dc25");
		model.setUserId("88888888");
		model.setIntentTypeEnum(IntentTypeEnum.PISP_INTENT_TYPE);
		model.getIntentTypeEnum().setIntentType("DOMESTIC_PAY");
		model.setClientId("5pRXn4FJ7bSbKwb7x5daKu");
		request.setAttribute(SCAConsentHelperConstants.INTENT_DATA, model);
		ReflectionTestUtils.setField(saasServiceImpl, "request", request);
		
		CustomDebtorDetails customDebtorDetails = new CustomDebtorDetails();
		customDebtorDetails.setSchemeName("UK.OBIE.IBAN");
		customDebtorDetails.setIdentification("FR1420041010050500013M02606");
		customDebtorDetails.setName("Iban Scheme");
		customDebtorDetails.setSecondaryIdentification("123");
		Mockito.when(consentViewHelper.retrieveConsentDetails(model)).thenReturn(customDebtorDetails);
		Mockito.when(consentViewHelper.populateLimitedSetUpDataforUI(new CustomConsentAppViewData())).thenReturn(new CustomDPaymentConsentsPOSTResponse());
		Mockito.when(consentViewHelper.getConsentAppViewData(model)).thenReturn(new CustomConsentAppViewData());
		Mockito.when(request.getAttribute(SCAConsentHelperConstants.INTENT_DATA)).thenReturn(model);
		
		Cookie cookie = new Cookie("SessionControllCookie", "eyJhbGciOiJSU0EtT0FFUC0yNTYiLCJlbmMiOiJBMjU2R0NNIn0.lZ2nqCeiPzsPmJShsrDD3uA55-06A649CMtwOyuY9nNzMtUGyzV-G8qc4w4ui1uWrtzypBs5Eyq4GfjnTtVHbcDVkS1HVc3tfxNAPY8dfjVrWNz59HyKt4bCjBdqqhBOdZezLtWB9aoWIwZoHLf4D8aUcVUtDsFELVcScmiQNtzHwvpDHZb4oxRfPl-OuOTkKA23C8lnnDMO1KUy8ZXHD4p0jQKAcaV877gYm8NbHDwOBEf-ItWJOGx2jV60apWd0hKqwfFR2QKD9wmGgXpbFZ08ro7X2fj8rTgKWhDgoBT_JVZdVFhVI4T4RLRDrCJqkyeciXhLm7W_xNhWBXAMrA.94SuB596ZLuUtw53wrofwN5jZXfT5f-ZarJhQc9Mj0M.0Ow5DXfilYX3ty49H4lNMNPljlWAFASc49zljhRSIIUSlmUHLZo0SAezn-n_FdxexAIYLk_FtRgnkMHDEyxJ1V1yHhqa1Jvdb36lTYyptqCJhMkOV1XGn58L4Z9QQmdrIZnn5iHxZ9-N1Jfjs0eoKiLBgR9O7ZEcs7QrWZVT6n_HrGrIloYQu_lFgmk5O7k47_15CVXaFqIohpHXETejoHEwjQj-iTToNRaHWNFAKvlpUBz4mUgk9RSIQCxK1GxxS8wxP44w5G4HdOIjFNwTsRDXeSZy0mU9zTNUCmDEUT9MFESfmVU1nPurdT-VoiPvVklbJZW8Sas0hWgqQkdQdP35nFY1sjCgfMB9iYUeEU-TCE219wkm1XXrLJwLEYZclL_4ckl4zExo2wb3Czwd8f5iO9fBQQWZ4mdwThK4VtZaPs1JEkxwGLI0SHA8Jr-e2PsDrkGEnxs74FsJ5MKluU2ZKvKcGXyQPaaTRa0ecJLD5-YYBuTtxOnU3gM_5aZm97pd_wiPk_h81r5aiwjSfRF3Ihxp37KNPfNOMJoA9xe2F51m1AvmjrOUgSM156LwmFyJFebVfarb9NPtJ_q1wU891sCu2Vmv520BR4QfIc-ayIwTVxLgZSN-BP7PhEJb_x8.XhZpINBxRdFFEgwPTcAgJg");
		Mockito.when(cookieHelper.createNewSessionCookie(model)).thenReturn(cookie);
		saasServiceImpl.getPaymentsDataForConsentView();
		
	}

}
