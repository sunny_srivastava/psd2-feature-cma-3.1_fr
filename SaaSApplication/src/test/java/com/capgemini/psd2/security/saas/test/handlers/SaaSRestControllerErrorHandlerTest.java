package com.capgemini.psd2.security.saas.test.handlers;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.exceptions.ErrorInfo;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.scaconsenthelper.constants.PFConstants;
import com.capgemini.psd2.scaconsenthelper.constants.SCAConsentHelperConstants;
import com.capgemini.psd2.security.saas.handlers.SaaSRestControllerErrorHandler;
import com.capgemini.psd2.ui.content.utility.controller.UIStaticContentUtilityController;

@RunWith(SpringJUnit4ClassRunner.class)
public class SaaSRestControllerErrorHandlerTest {
	
	@Mock
	private RequestHeaderAttributes requestHeaderAttributes;
	
	@Mock
	private UIStaticContentUtilityController uiController;
	
	@Mock
	private HttpServletRequest request;
	
	@Mock
	private HttpServletResponse response;
	@Mock
	ErrorInfo info;
	
	@InjectMocks
	private SaaSRestControllerErrorHandler handler = new SaaSRestControllerErrorHandler();
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void handleCustomExceptionTestWithPFConstants() throws JSONException{
		ErrorInfo info = new ErrorInfo();
		info.setStatusCode("500");
		PSD2Exception exception = new PSD2Exception("abcd",info);
		when(request.getAttribute(PFConstants.RESUME_PATH)).thenReturn(null);
		when(request.getParameter(PFConstants.RESUME_PATH)).thenReturn("abcd");
		handler.handleCustomException(exception, request, response);
	}
	
	@Test
	public void handleCustomExceptionTestWithNullResumePath() throws JSONException{
		ErrorInfo mock = new ErrorInfo();
		mock.setStatusCode("500");
		info.setStatusCode("501");
		when(info.getStatusCode()).thenReturn("501");
		PSD2Exception exception =new PSD2Exception("abcd",mock);
		when(request.getAttribute(PFConstants.RESUME_PATH)).thenReturn(null);
		when(requestHeaderAttributes.getCorrelationId()).thenReturn("");
		//when(request.getParameter(PFConstants.RESUME_PATH)).thenReturn("abcd");
		handler.handleCustomException(exception, request, response);
	}
	
	@Test
	public void handleCustomExceptionTestWithSCAConsentHelperConstants() throws JSONException{
		ErrorInfo info = new ErrorInfo();
		info.setStatusCode("500");
		PSD2Exception exception = new PSD2Exception("abcd",info);
		when(request.getAttribute(PFConstants.RESUME_PATH)).thenReturn(null);
		when(request.getParameter(SCAConsentHelperConstants.OAUTH_URL_PARAM)).thenReturn("abcd");
		handler.handleCustomException(exception, request, response);
	}
}
