package com.capgemini.psd2.security.saas.test.services;

import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.json.simple.JSONObject;

import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.scaconsenthelper.constants.OIDCConstants;
import com.capgemini.psd2.security.saas.services.SaaSPickupDataService;
import com.capgemini.psd2.ui.content.utility.controller.UIStaticContentUtilityController;
import com.google.gson.JsonSerializationContext;

@RunWith(SpringJUnit4ClassRunner.class)
public class SaaSPickupDataServiceTest {
	
	@Mock
	private RequestHeaderAttributes requestHeaderAttributes;
	
	@Mock
	private UIStaticContentUtilityController uiController;
	
	@Mock
	private HttpServletRequest request;
	
	@Mock
	private HttpServletResponse response;
	
	@InjectMocks
	private SaaSPickupDataService service = new SaaSPickupDataService();
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void populateIntentDataTest() throws ParseException{
		String jsonResponse = "{\"correlationId\":\"12345\",\"request\":\"eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6Ijh4Y0JUYjN3anY3b05od1JpQ2dfc2ZxNXNGOCJ9.eyJpc3MiOiI0ejdsaU5SS25CMlBQQ2VFS29wejlXIiwiYXVkIjoiaHR0cHM6Ly9hdXRoLmludC5hcGllenkuY29tIiwicmVzcG9uc2VfdHlwZSI6ImNvZGUgaWRfdG9rZW4iLCJjbGllbnRfaWQiOiI0ejdsaU5SS25CMlBQQ2VFS29wejlXIiwicmVkaXJlY3RfdXJpIjoiaHR0cHM6Ly93d3cuZ2V0cG9zdG1hbi5jb20vb2F1dGgyL2NhbGxiYWNrIiwic2NvcGUiOiJvcGVuaWQgYWNjb3VudHMiLCJzdGF0ZSI6ImFmMGlmanNsZGtqIiwibm9uY2UiOiJuLTBTNl9XekEyTWoiLCJtYXhfYWdlIjo4NjQwMCwiY2xhaW1zIjp7ImlkX3Rva2VuIjp7Im9wZW5iYW5raW5nX2ludGVudF9pZCI6eyJ2YWx1ZSI6ImVlMWVhOTExLTcyMDUtNDE2ZS1iYzIyLTk2NmE5ZTY2NmRkZSIsImVzc2VudGlhbCAiOnRydWV9fX19.RdGfKpupWb9piEsXqyQXB17Ncz0-SmQgb0aMcT7uQz74DAn8oNkTkHENcPxmZbTH-mtQpjI9Om24vxVEzjgnWKhKegn0dmXReom1hbx1usBHKQJWoIw_AZcrp3KW2oBvqb3F2Sfr2zQWX2Ls3dnPN1wEsGVtSfeeTg7Ube6v6zEcCYz8xwp8mPT7x64T0mgG4JzMkHKD4gwKZzS7RNDuu3NOGT-0AMWqJ4YJSydmSxZQAIVpgtt7lH8xt8IRvsOKYfoRKOXBIeZSTdh4ct8kKr6Z4AlOlGQL6_d3hKGe5vOZdSW4en-rgpVz1UkhbWqHD8o_kze6f3eyS_qPzBoPCw\"}";
		JSONObject jsonObject = mock(JSONObject.class);
		JSONObject intentJsonObject = mock(JSONObject.class);
		service.populateIntentData(jsonResponse);
	}
	
	@Test
	public void populateIntentDataTest2() throws ParseException{
		String jsonResponse = "{\"correlationId\":\"12345\",\"request\":\"eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6Ijh4Y0JUYjN3anY3b05od1JpQ2dfc2ZxNXNGOCJ9.eyJpc3MiOiI0ejdsaU5SS25CMlBQQ2VFS29wejlXIiwiYXVkIjoiaHR0cHM6Ly9hdXRoLmludC5hcGllenkuY29tIiwicmVzcG9uc2VfdHlwZSI6ImNvZGUgaWRfdG9rZW4iLCJjbGllbnRfaWQiOiI0ejdsaU5SS25CMlBQQ2VFS29wejlXIiwicmVkaXJlY3RfdXJpIjoiaHR0cHM6Ly93d3cuZ2V0cG9zdG1hbi5jb20vb2F1dGgyL2NhbGxiYWNrIiwic2NvcGUiOiJvcGVuaWQgYWNjb3VudHMiLCJzdGF0ZSI6ImFmMGlmanNsZGtqIiwibm9uY2UiOiJuLTBTNl9XekEyTWoiLCJtYXhfYWdlIjo4NjQwMCwiY2xhaW1zIjp7ImlkX3Rva2VuIjp7Im9wZW5iYW5raW5nX2ludGVudF9pZCI6eyJ2YWx1ZSI6ImVlMWVhOTExLTcyMDUtNDE2ZS1iYzIyLTk2NmE5ZTY2NmRkZSIsImVzc2VudGlhbCAiOnRydWV9fX19.RdGfKpupWb9piEsXqyQXB17Ncz0-SmQgb0aMcT7uQz74DAn8oNkTkHENcPxmZbTH-mtQpjI9Om24vxVEzjgnWKhKegn0dmXReom1hbx1usBHKQJWoIw_AZcrp3KW2oBvqb3F2Sfr2zQWX2Ls3dnPN1wEsGVtSfeeTg7Ube6v6zEcCYz8xwp8mPT7x64T0mgG4JzMkHKD4gwKZzS7RNDuu3NOGT-0AMWqJ4YJSydmSxZQAIVpgtt7lH8xt8IRvsOKYfoRKOXBIeZSTdh4ct8kKr6Z4AlOlGQL6_d3hKGe5vOZdSW4en-rgpVz1UkhbWqHD8o_kze6f3eyS_qPzBoPCw\",\"tenant_id\":\"3249923492932999edsif\"}";
		JSONObject jsonObject = mock(JSONObject.class);
		JSONObject intentJsonObject = mock(JSONObject.class);
		//when(intentJsonObject.get(anyString())).thenReturn(null);
		service.populateIntentData(jsonResponse);
	}
	
	@Test
	public void populateIntentDataTest3() throws ParseException{
		String jsonResponse = "{\"correlationId\":\"12345\",\"request\":\"eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6Ijh4Y0JUYjN3anY3b05od1JpQ2dfc2ZxNXNGOCJ9.eyJpc3MiOiI0ejdsaU5SS25CMlBQQ2VFS29wejlXIiwiYXVkIjoiaHR0cHM6Ly9hdXRoLmludC5hcGllenkuY29tIiwicmVzcG9uc2VfdHlwZSI6ImNvZGUgaWRfdG9rZW4iLCJjbGllbnRfaWQiOiI0ejdsaU5SS25CMlBQQ2VFS29wejlXIiwicmVkaXJlY3RfdXJpIjoiaHR0cHM6Ly93d3cuZ2V0cG9zdG1hbi5jb20vb2F1dGgyL2NhbGxiYWNrIiwic2NvcGUiOiJvcGVuaWQgYWNjb3VudHMiLCJzdGF0ZSI6ImFmMGlmanNsZGtqIiwibm9uY2UiOiJuLTBTNl9XekEyTWoiLCJtYXhfYWdlIjo4NjQwMCwiY2xhaW1zIjp7ImlkX3Rva2VuIjp7Im9wZW5iYW5raW5nX2ludGVudF9pZCI6eyJ2YWx1ZSI6ImVlMWVhOTExLTcyMDUtNDE2ZS1iYzIyLTk2NmE5ZTY2NmRkZSIsImVzc2VudGlhbCAiOnRydWV9fX19.RdGfKpupWb9piEsXqyQXB17Ncz0-SmQgb0aMcT7uQz74DAn8oNkTkHENcPxmZbTH-mtQpjI9Om24vxVEzjgnWKhKegn0dmXReom1hbx1usBHKQJWoIw_AZcrp3KW2oBvqb3F2Sfr2zQWX2Ls3dnPN1wEsGVtSfeeTg7Ube6v6zEcCYz8xwp8mPT7x64T0mgG4JzMkHKD4gwKZzS7RNDuu3NOGT-0AMWqJ4YJSydmSxZQAIVpgtt7lH8xt8IRvsOKYfoRKOXBIeZSTdh4ct8kKr6Z4AlOlGQL6_d3hKGe5vOZdSW4en-rgpVz1UkhbWqHD8o_kze6f3eyS_qPzBoPCw\",\"tenant_id\":\"3249923492932999edsif\"}";
		JSONObject jsonObject = mock(JSONObject.class);
		JSONObject intentJsonObject = mock(JSONObject.class);
		when(jsonObject.get(PSD2Constants.CORRELATION_ID)).thenReturn(null);
		
		when(intentJsonObject.get(OIDCConstants.SCOPE)).thenReturn(OIDCConstants.OPENID);
		service.populateIntentData(jsonResponse);
	}
	
}
