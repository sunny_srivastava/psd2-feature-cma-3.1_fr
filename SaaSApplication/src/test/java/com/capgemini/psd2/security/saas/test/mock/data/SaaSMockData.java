package com.capgemini.psd2.security.saas.test.mock.data;

import static org.mockito.Mockito.mock;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;

import com.capgemini.psd2.aisp.adapter.AccountRequestAdapter;
import com.capgemini.psd2.aisp.domain.OBAccount6Account;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AispConsent;
import com.capgemini.psd2.security.constants.PSD2SecurityConstants;


public class SaaSMockData {

	public static final String tokenSigningKey = "0123456789abcdef";

	public static Map<String, String> getMockParamMap() {
		Map<String, String> mockParamMap = new HashMap<String, String>();
		mockParamMap.put("channelId", "BOL");
		mockParamMap.put(PSD2SecurityConstants.CID_PARAM, "testcid");
		return mockParamMap;
	}

	public static Authentication getMockAuthentication() {
		Authentication mockAuthentication = new AuthenticationImpl();
		return mockAuthentication;
	}

	public static void setSecurityContextHolder() {
		Set<GrantedAuthority> authorities = new HashSet<GrantedAuthority>();
		User user = new User("abc", "123", authorities);
		Authentication mockAuthentication = new UsernamePasswordAuthenticationToken(user.getUsername(),
				user.getPassword());
		SecurityContextHolder.getContext().setAuthentication(mockAuthentication);
	}

	/*public static String getMockEncOAuthUrl() {
		String oAuthUrl = "/authorize?cid=1234&AccountRequestId=32fcbc79-821f-47c1-88bb-88a504e3cdc7&brandId=ROI&channelId=BOL";
		String encOAuthUrl = CryptoUtilities.encPayLoad(oAuthUrl, tokenSigningKey);
		return encOAuthUrl;
	}*/

	public static HttpServletRequest getMockHttpServletRequest() {
		return mock(HttpServletRequest.class);
	}

	public static HttpServletResponse getMockHttpServletResponse() {
		return mock(HttpServletResponse.class);
	}

	/*public static JwtSettings getMockJwtSettings() {
		return mock(JwtSettings.class);
	}*/

	public static String getSaaSUrl() {
		String saasUrl = "https://localhost:8096/SaaS/saaslogin?oAuthUrl=ZeFd1fUQy82i5SUjwDb6/U/0CR5Epj+hxQJLNnqyF13+TJ61bj/0+GSAEy0CAwlJdC9+CbujbHcIdSnuNaR0sMIpqrKMzkcOCarWasWfjZE=";
		return saasUrl;
	}

	public static RequestDispatcher getMockRequestDispatcher() {
		return mock(RequestDispatcher.class);
	}

	public static HttpSession getMockSession() {
		return mock(HttpSession.class);
	}

	public static AispConsent getMockAispConsent(){
		AispConsent aispConsent = new AispConsent();
		aispConsent.setAccountRequestId("11737fe5d8284fe89f400d608db8dc25");
		aispConsent.setCmaVersion("3.1");
		
		List<AccountDetails> accountDetailsList = new ArrayList<>();
		AccountDetails accountDetail = new AccountDetails();
		accountDetail.setCurrency("USD");
		accountDetail.setNickname("test");
		accountDetail.setAccountId("11737fe5d8284fe89f400d608db8dc25");
		accountDetail.setAccountType("savings");
		
		OBAccount6Account obAccount3Account = new OBAccount6Account();
		obAccount3Account.setSchemeName("UK.OBIE.IBAN");
		obAccount3Account.setIdentification("FR1420041010050500013M02606");
		accountDetail.setAccount(obAccount3Account);
		accountDetailsList.add(accountDetail);
		
		aispConsent.setAccountDetails(accountDetailsList);
		return aispConsent;
	}
}
