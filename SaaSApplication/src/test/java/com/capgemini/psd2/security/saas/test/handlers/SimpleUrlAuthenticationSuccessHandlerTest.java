package com.capgemini.psd2.security.saas.test.handlers;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.security.core.Authentication;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.fraudsystem.helper.FraudSystemHelper;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.scaconsenthelper.config.PFConfig;
import com.capgemini.psd2.scaconsenthelper.constants.PFConstants;
import com.capgemini.psd2.scaconsenthelper.constants.SCAConsentHelperConstants;
import com.capgemini.psd2.scaconsenthelper.models.IntentTypeEnum;
import com.capgemini.psd2.scaconsenthelper.models.PickupDataModel;
import com.capgemini.psd2.security.saas.handlers.SimpleUrlAuthenticationSuccessHandler;
//import com.capgemini.psd2.security.saas.model.JwtSettings;
import com.capgemini.psd2.security.saas.test.mock.data.SaaSMockData;

@RunWith(SpringJUnit4ClassRunner.class)
public class SimpleUrlAuthenticationSuccessHandlerTest {
	
	@Mock
	private HttpServletRequest request;
	
	@Mock
	HttpServletResponse response;
	
	Authentication authentication = SaaSMockData.getMockAuthentication();
	
	
	@Mock
	private RequestHeaderAttributes requestHeaderAttribute;
	
	@InjectMocks
	private SimpleUrlAuthenticationSuccessHandler successHandler = new SimpleUrlAuthenticationSuccessHandler();
	
	@Mock
	private FraudSystemHelper fraudSystemHelper;
	
	@Mock
	private PFConfig pfConfig;
	
	private String correlationID = "12345";

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		SaaSMockData.setSecurityContextHolder();
	}
	
	@Test
	public void onAuthenticationSuccessTest() throws ServletException,IOException {
		when(requestHeaderAttribute.getCorrelationId()).thenReturn(correlationID);
		PickupDataModel model = mock(PickupDataModel.class);
		request.setAttribute(SCAConsentHelperConstants.INTENT_DATA, model);
		when(request.getAttribute(SCAConsentHelperConstants.INTENT_DATA)).thenReturn(model);
		when(request.getAttribute(PSD2Constants.NETSCALER_TENANT_ID_HEADER_NAME)).thenReturn("tenant2");
		when(model.getIntentTypeEnum()).thenReturn(IntentTypeEnum.AISP_INTENT_TYPE);
		
		when(request.getParameter(PFConstants.RESUME_PATH)).thenReturn("abcd");
		request.setAttribute(PFConstants.REF, "13245");
		when(request.getAttribute(PFConstants.REF)).thenReturn("12345");
		when(pfConfig.getTenantSpecificResumePathBaseUrl(anyString())).thenReturn("http://localhost:8");
		successHandler.onAuthenticationSuccess(request, response, authentication);
		assert(true);
	}
	
	@Test
	public void onAuthenticationSuccessWithResponseNotCommittedTest() throws IOException, ServletException{
		when(requestHeaderAttribute.getCorrelationId()).thenReturn(correlationID);
		when(response.isCommitted()).thenReturn(true);
		PickupDataModel model = mock(PickupDataModel.class);
		request.setAttribute(SCAConsentHelperConstants.INTENT_DATA, model);
		when(request.getAttribute(SCAConsentHelperConstants.INTENT_DATA)).thenReturn(model);
		when(model.getIntentTypeEnum()).thenReturn(IntentTypeEnum.AISP_INTENT_TYPE);
		successHandler.onAuthenticationSuccess(request, response, authentication);
		assert(true);
	}
	
	
	/**
	 * Tear down.
	 *
	 * @throws Exception the exception
	 */
	@After
	public void tearDown() throws Exception {
		successHandler = null;
	}


}
