package com.capgemini.psd2.security.saas.test.services;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.aisp.adapter.AispConsentAdapter;
import com.capgemini.psd2.consent.domain.AispConsent;
import com.capgemini.psd2.enums.ConsentStatusEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.scaconsenthelper.config.AuthenticationConfig;
import com.capgemini.psd2.scaconsenthelper.config.PFConfig;
import com.capgemini.psd2.scaconsenthelper.config.helpers.CookieHelper;
import com.capgemini.psd2.scaconsenthelper.config.helpers.DropOffDataService;
import com.capgemini.psd2.scaconsenthelper.config.helpers.JwtTokenUtility;
import com.capgemini.psd2.scaconsenthelper.models.DropOffRequest;
import com.capgemini.psd2.scaconsenthelper.models.DropOffResponse;
import com.capgemini.psd2.scaconsenthelper.models.IntentTypeEnum;
import com.capgemini.psd2.scaconsenthelper.models.PickupDataModel;
import com.capgemini.psd2.security.saas.services.AuthenticationServiceImpl;
import com.capgemini.psd2.utilities.JSONUtilities;

import net.minidev.json.JSONObject;

@RunWith(SpringJUnit4ClassRunner.class)
public class AuthenticationServiceImplTest {
	@InjectMocks
	AuthenticationServiceImpl service;
	@Mock
	DropOffRequest dropOffRequest;
	@Mock
	RequestHeaderAttributes headerAttributes;
	@Mock
	HttpServletRequest request;
	
	@Mock
	HttpServletResponse response;
	@Mock
	CookieHelper cookieHelper;	
	@Mock
	 AuthenticationConfig config;
	@Mock
	DropOffDataService dropOffService;
	@Mock
	AispConsentAdapter aispConsentAdapter;
	@Mock 
	PFConfig pfConfig;
	
	@Before
	public void setup(){
		MockitoAnnotations.initMocks(this);
		request = mock(HttpServletRequest.class);
		response = mock(HttpServletResponse.class);
		ReflectionTestUtils.setField(service,"skipConsentFlow",true);
		
	
	}
	
	@Test(expected=PSD2Exception.class)
	public void testunsuccessfulScaJourney1() {
  		 String token="eyJhbGciOiJQUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6IlY2ak9jVTJtaVE5LXZmUklsb1RSQjZkcnZkayJ9.eyJpc3MiOiJYU3VkZkRZUGxlc1RRVW0zZEtCbEpXIiwiYXVkIjoiaHR0cHM6Ly9hdXRoLnRlc3QuYXBpcGxhdGZvcm0uaW4iLCJyZXNwb25zZV90eXBlIjoiY29kZSBpZF90b2tlbiIsImNsaWVudF.9pZCI6IlhTdWRmRFlQbGVzVFFVbTNkS0JsSlciLCJyZWRpcmVjdF91cmkiOiJodHRwczovL2RlbW8uY29tIiwic2NvcGUiOiJvcGVuaWQgZnVuZHNjb25maXJtYXRpb25zIiwic3RhdGUiOiJhZjBpZmpzbGRraiIsImV4cCI6MTY4MDM4ODE1OSwibm9uY2UiOiJuLTBTNl9XekEyTWoiLCJtYXhfYWdlIjo4NjQwMCwiY2xhaW1zIjp7ImlkX3Rva2VuIjp7Im9wZW5iYW5raW5nX2ludGVudF9pZCI6eyJ2YWx1ZSI6ImU3MDJhNzllLTU5ZWYtNDc5NC1hZGFkLTBiNjM0N2I1ODQ4NyIsImVzc2VudGlhbCAiOnRydWV9fX0sImlhdCI6MTYyMDgxODU2Nn0.LC5OCEm0quJpXeZH3HfbkI7bMUBCYzpBI4cd6mQ9tVN-RUdIsGM3nZlCJUNYPHJxAj9rQx_XHpD7m0n4bUAvD7v2EHcg5uOXq1i7VAXm5SRr8YIChActHIglSZNcdopaE2SUl_PXX28YNb6Hdd_llMlmEe.LOskqgxa88AvSNhKtJjs9aT6mQu71OgWIWgV0gTyTOmmC-5GBnvua1tdXw3kWGdxU53ovyrhXEGdT-n7wWDmGNxW5LlRj9gS4DJIEU_US1DxnqFsFsRanB6CeYj0m-uj542dJSdRMQn1paELRsrdXHxFsiwIWCq3l0r8r31Vmq5YvXagPlaOcr9xaLhQ";
		System.out.println(token.split("\\.").length);
		 assertTrue(token.split("\\.").length==5);
		 String refId="sdfjsd";
		 when(dropOffService.dropOffOnCancel()).thenReturn(refId);
		String resumePath="ksadiad"; 
		Boolean bypassConsentPage=true;
		when(config.getJweDecryptionKey()).thenReturn("sdfhds");
		
		//dropOffRequest=JwtTokenUtility.parseAndReturnClaims(token, "dsjfdsj");
		//when(JwtTokenUtility.parseAndReturnClaims(token, "dsjfdsj")).thenReturn(dropOffRequest);
		dropOffRequest.setResumePath(resumePath);
		 dropOffRequest.setBypassConsentPage(bypassConsentPage);
		 assertNotNull(dropOffRequest);
		 when(dropOffRequest.getResumePath()).thenReturn(resumePath);
		 when(dropOffRequest.isBypassConsentPage()).thenReturn(bypassConsentPage);
		 assertNotNull(dropOffRequest.getResumePath());
		 assertNotNull(dropOffRequest.isBypassConsentPage());
		 JSONObject json = mock(JSONObject.class);
		 when(json.containsKey("enc")).thenReturn(true);
		
		 when(headerAttributes.getTenantId()).thenReturn(null);
		
		 assertNotNull(service.unsuccessfulScaJourney(token));
	}
	
	@Test(expected=PSD2Exception.class)
	public void testSuccessfulScaJourney1() {
  		 String token="eyJhbGciOiJQUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6IlY2ak9jVTJtaVE5LXZmUklsb1RSQjZkcnZkayJ9.eyJpc3MiOiJYU3VkZkRZUGxlc1RRVW0zZEtCbEpXIiwiYXVkIjoiaHR0cHM6Ly9hdXRoLnRlc3QuYXBpcGxhdGZvcm0uaW4iLCJyZXNwb25zZV90eXBlIjoiY29kZSBpZF90b2tlbiIsImNsaWVudF.9pZCI6IlhTdWRmRFlQbGVzVFFVbTNkS0JsSlciLCJyZWRpcmVjdF91cmkiOiJodHRwczovL2RlbW8uY29tIiwic2NvcGUiOiJvcGVuaWQgZnVuZHNjb25maXJtYXRpb25zIiwic3RhdGUiOiJhZjBpZmpzbGRraiIsImV4cCI6MTY4MDM4ODE1OSwibm9uY2UiOiJuLTBTNl9XekEyTWoiLCJtYXhfYWdlIjo4NjQwMCwiY2xhaW1zIjp7ImlkX3Rva2VuIjp7Im9wZW5iYW5raW5nX2ludGVudF9pZCI6eyJ2YWx1ZSI6ImU3MDJhNzllLTU5ZWYtNDc5NC1hZGFkLTBiNjM0N2I1ODQ4NyIsImVzc2VudGlhbCAiOnRydWV9fX0sImlhdCI6MTYyMDgxODU2Nn0.LC5OCEm0quJpXeZH3HfbkI7bMUBCYzpBI4cd6mQ9tVN-RUdIsGM3nZlCJUNYPHJxAj9rQx_XHpD7m0n4bUAvD7v2EHcg5uOXq1i7VAXm5SRr8YIChActHIglSZNcdopaE2SUl_PXX28YNb6Hdd_llMlmEe.LOskqgxa88AvSNhKtJjs9aT6mQu71OgWIWgV0gTyTOmmC-5GBnvua1tdXw3kWGdxU53ovyrhXEGdT-n7wWDmGNxW5LlRj9gS4DJIEU_US1DxnqFsFsRanB6CeYj0m-uj542dJSdRMQn1paELRsrdXHxFsiwIWCq3l0r8r31Vmq5YvXagPlaOcr9xaLhQ";
		System.out.println(token.split("\\.").length);
		 assertTrue(token.split("\\.").length==5);
		 String refId="sdfjsd";
		 when(dropOffService.dropOffOnSuccess(dropOffRequest)).thenReturn(refId);
		String resumePath="ksadiad"; 
		Boolean bypassConsentPage=true;
		when(config.getJweDecryptionKey()).thenReturn("sdfhds");
		DropOffResponse dropOffResponse = new DropOffResponse();
		dropOffResponse.setRef(refId);
		//dropOffRequest=JwtTokenUtility.parseAndReturnClaims(token, "dsjfdsj");
		//when(JwtTokenUtility.parseAndReturnClaims(token, "dsjfdsj")).thenReturn(dropOffRequest);
		dropOffRequest.setResumePath(resumePath);
		 dropOffRequest.setBypassConsentPage(bypassConsentPage);
		 assertNotNull(dropOffRequest);
		 when(dropOffRequest.getResumePath()).thenReturn(resumePath);
		 when(dropOffRequest.isBypassConsentPage()).thenReturn(bypassConsentPage);
		 assertNotNull(dropOffRequest.getResumePath());
		 assertNotNull(dropOffRequest.isBypassConsentPage());
		 JSONObject json = mock(JSONObject.class);
		 when(json.containsKey("enc")).thenReturn(true);
		when(pfConfig.getTenantSpecificDropOffUrl(anyString())).thenReturn("sjfsjejieo");
		when(pfConfig.getScainstanceusername()).thenReturn("uendesdfe");
		when(pfConfig.getScainstancepassword()).thenReturn("pasdewudnkadaij");
		when(pfConfig.getScainstanceId()).thenReturn("isnceufasidadj");
		 when(headerAttributes.getTenantId()).thenReturn(null);
		
		 assertNotNull(service.successfulScaJourney(token));
	}
	
	@Test(expected=PSD2Exception.class)
	public void testCreateJwtTokenWithNullAISP(){
		PickupDataModel pickUpDataModel = new PickupDataModel();
		String intentId ="123";
		String channelId ="BOI";
		String clientId="6795ee9ca8e3407694f866725303db37";
		IntentTypeEnum enumType =IntentTypeEnum.AISP_INTENT_TYPE;
		
		//JSONUtilities json =mock(JSONUtilities.class);
		pickUpDataModel.setIntentId(intentId);
		
		pickUpDataModel.setChannelId(channelId);
		
		pickUpDataModel.setClientId(clientId);
		
		pickUpDataModel.setIntentTypeEnum(enumType);
		
		
		headerAttributes.setScopes(PSD2Constants.ACCOUNTS);
		when(headerAttributes.getScopes()).thenReturn(PSD2Constants.ACCOUNTS);
		when(config.getAspspRedirectUrl()).thenReturn("sdfhsddfjsdssdfs");
		when(config.getTokenIssuer()).thenReturn("sdfjsweisfs");
		when(config.getTokenExpirationTime()).thenReturn(9324);
		when(config.getTokenSigningKey()).thenReturn("sdfjs");
		when(config.getJweDecryptionKey()).thenReturn("weirisd");
		when(headerAttributes.getPsuId()).thenReturn("sdfjsd");
		when(headerAttributes.getTenantId()).thenReturn("2349-sdfjdsf-3sdnfs");
	when(aispConsentAdapter.retrieveConsentByAccountRequestIdAndStatus(intentId, ConsentStatusEnum.AUTHORISED)).thenReturn(null);
		service.createJwtToken(pickUpDataModel);
	}
	
	@Test(expected=PSD2Exception.class)
	public void testCreateJwtToken(){
		PickupDataModel pickUpDataModel = new PickupDataModel();//get proper data
		String intentId ="123";
		String channelId ="BOI";
		String clientId="6795ee9ca8e3407694f866725303db37";
		IntentTypeEnum enumType =IntentTypeEnum.AISP_INTENT_TYPE;
		
		//JSONUtilities json =mock(JSONUtilities.class);
		pickUpDataModel.setIntentId(intentId);
		
		pickUpDataModel.setChannelId(channelId);
		
		pickUpDataModel.setClientId(clientId);
		pickUpDataModel.setBypassConsentPage(true);
		pickUpDataModel.setConsentExpiryInMinutes(new Long(99201));
		pickUpDataModel.setCorrelationId("kdkksowxx");
		pickUpDataModel.setOriginatorRefId("dfksodkkf");
		pickUpDataModel.setPaymentType("dksokckd");
		pickUpDataModel.setResumePath("dkoekdkodkokodkokdkkxcd");
		pickUpDataModel.setScope("aisp");
		pickUpDataModel.setTenant_id("dkdkdksk");
		pickUpDataModel.setIntentTypeEnum(enumType);
		Cookie cookie = new Cookie("SessionControllCookie", "eyJhbGciOiJSU0EtT0FFUC0yNTYiLCJlbmMiOiJBMjU2R0NNIn0.lZ2nqCeiPzsPmJShsrDD3uA55-06A649CMtwOyuY9nNzMtUGyzV-G8qc4w4ui1uWrtzypBs5Eyq4GfjnTtVHbcDVkS1HVc3tfxNAPY8dfjVrWNz59HyKt4bCjBdqqhBOdZezLtWB9aoWIwZoHLf4D8aUcVUtDsFELVcScmiQNtzHwvpDHZb4oxRfPl-OuOTkKA23C8lnnDMO1KUy8ZXHD4p0jQKAcaV877gYm8NbHDwOBEf-ItWJOGx2jV60apWd0hKqwfFR2QKD9wmGgXpbFZ08ro7X2fj8rTgKWhDgoBT_JVZdVFhVI4T4RLRDrCJqkyeciXhLm7W_xNhWBXAMrA.94SuB596ZLuUtw53wrofwN5jZXfT5f-ZarJhQc9Mj0M.0Ow5DXfilYX3ty49H4lNMNPljlWAFASc49zljhRSIIUSlmUHLZo0SAezn-n_FdxexAIYLk_FtRgnkMHDEyxJ1V1yHhqa1Jvdb36lTYyptqCJhMkOV1XGn58L4Z9QQmdrIZnn5iHxZ9-N1Jfjs0eoKiLBgR9O7ZEcs7QrWZVT6n_HrGrIloYQu_lFgmk5O7k47_15CVXaFqIohpHXETejoHEwjQj-iTToNRaHWNFAKvlpUBz4mUgk9RSIQCxK1GxxS8wxP44w5G4HdOIjFNwTsRDXeSZy0mU9zTNUCmDEUT9MFESfmVU1nPurdT-VoiPvVklbJZW8Sas0hWgqQkdQdP35nFY1sjCgfMB9iYUeEU-TCE219wkm1XXrLJwLEYZclL_4ckl4zExo2wb3Czwd8f5iO9fBQQWZ4mdwThK4VtZaPs1JEkxwGLI0SHA8Jr-e2PsDrkGEnxs74FsJ5MKluU2ZKvKcGXyQPaaTRa0ecJLD5-YYBuTtxOnU3gM_5aZm97pd_wiPk_h81r5aiwjSfRF3Ihxp37KNPfNOMJoA9xe2F51m1AvmjrOUgSM156LwmFyJFebVfarb9NPtJ_q1wU891sCu2Vmv520BR4QfIc-ayIwTVxLgZSN-BP7PhEJb_x8.XhZpINBxRdFFEgwPTcAgJg");
		Mockito.when(cookieHelper.setBypassConsentCookie(request)).thenReturn(cookie);
		AispConsent aispConsent= new  AispConsent();
		headerAttributes.setScopes(PSD2Constants.ACCOUNTS);
		when(headerAttributes.getScopes()).thenReturn(PSD2Constants.ACCOUNTS);
		when(config.getAspspRedirectUrl()).thenReturn("sdfhsddfjsdssdfs");
		when(config.getTokenIssuer()).thenReturn("sdfjsweisfs");
		when(config.getTokenExpirationTime()).thenReturn(9324);
		when(config.getTokenSigningKey()).thenReturn("sdfjs");
		when(config.getJweDecryptionKey()).thenReturn("weirisd");
		when(headerAttributes.getPsuId()).thenReturn("sdfjsd");
		when(headerAttributes.getTenantId()).thenReturn("2349-sdfjdsf-3sdnfs");
	    when(aispConsentAdapter.retrieveConsentByAccountRequestIdAndStatus(intentId, ConsentStatusEnum.AUTHORISED)).thenReturn(aispConsent);
		
	    service.createJwtToken(pickUpDataModel);
	}
	/*
	 * 
	         String username="dshfdsh";
			 String acr="fhds";
			 String channel_id="fhdasd"; 
			 String authnInst="djsfdsj"; 
			 String subject="sfjsj";
			 String correlationId="iweoefjc";
			 String client_id="riaimad"; 
			 String scope="sdeicm";
			 String intent_type="owecmjkd"; 
			 String tenant_id="sdkkad"; 
			 String consent_expiry="kekcmskd"; 
			 String resumePath="ksadiad";
			 String intentId="asdkkc";
			 Boolean bypassConsentPage=true;
	 * */
	 

}
