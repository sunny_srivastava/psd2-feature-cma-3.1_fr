package com.capgemini.psd2.foundationservice.dso.sca.consent.boi.adapter.test;

import static org.mockito.Matchers.any;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpHeaders;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.RestTemplate;

import com.capgemini.psd2.foundationservice.dso.sca.consent.boi.adapter.client.DomesticStandingOrderScaConsentFoundationServiceAdapterClient;
import com.capgemini.psd2.foundationservice.dso.sca.consent.boi.adapter.delegate.DomesticStandingOrderScaConsentFoundationServiceAdapterDelegate;
import com.capgemini.psd2.foundationservice.dso.sca.consent.boi.adapter.raml.domain.PaymentInstructionProposalAuthorisingParty;
import com.capgemini.psd2.foundationservice.dso.sca.consent.boi.adapter.raml.domain.StandingOrderInstructionProposal;
import com.capgemini.psd2.foundationservice.dso.sca.consent.boi.adapter.transformer.DomesticStandingOrderScaConsentFoundationServiceAdapterTransformer;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.RestClientSync;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringJUnit4ClassRunner.class)
public class DomesticStandingOrderScaConsentFoundationServiceDelegateRestCallTest {
	@Mock
	private DomesticStandingOrderScaConsentFoundationServiceAdapterDelegate delegate;

	@InjectMocks
	private DomesticStandingOrderScaConsentFoundationServiceAdapterClient domesticStandingOrderScaConsentFoundationServiceClient;

	@Mock
	private DomesticStandingOrderScaConsentFoundationServiceAdapterClient domesticStandingOrderScaConsentFoundationServiceClientMock;
	
	@Mock
	private DomesticStandingOrderScaConsentFoundationServiceAdapterTransformer domesticStandingOrderScaConsentFoundationServiceTransformer;

	@Mock
	private RestTemplate restTemplate;
	
	@Mock
	private RestClientSync restClient;

	/**
	 * 
	 * Sets the up.
	 * 
	 */

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * 
	 * Context loads.
	 * 
	 */

	@Test
	public void contextLoads() {
	}

	@Test
	public void testDomesticStandingOrderScaConsentFoundationServiceAuthorisingPartyClient() {
		RequestInfo reqInfo = new RequestInfo();
		HttpHeaders headers = new HttpHeaders();
		PaymentInstructionProposalAuthorisingParty paymentInstructionProposalAuthorisingParty = new PaymentInstructionProposalAuthorisingParty();
		Mockito.when(restClient.callForGet(any(), any(), any()))
				.thenReturn(new PaymentInstructionProposalAuthorisingParty());
		domesticStandingOrderScaConsentFoundationServiceClient
				.restTransportForDomesticStandingOrderFoundationServicePostAuthorisingParty(reqInfo,
						paymentInstructionProposalAuthorisingParty, StandingOrderInstructionProposal.class, headers);
	}

	@Test
	public void testDomesticStandingOrderScaConsentFoundationServiceClient_Reject() {
		RequestInfo reqInfo = new RequestInfo();
		HttpHeaders headers = new HttpHeaders();
		String standingorderinstructionproposalResponseAfterTransform = "";
		Mockito.when(restClient.callForGet(any(), any(), any()))
				.thenReturn(new PaymentInstructionProposalAuthorisingParty());
		domesticStandingOrderScaConsentFoundationServiceClient
				.restTransportForDomesticStandingOrderFoundationServiceReject(reqInfo,
						standingorderinstructionproposalResponseAfterTransform, StandingOrderInstructionProposal.class,
						headers);
	}

	@Test
	public void testDomesticStandingOrderScaConsentFoundationServiceAuthoriseClient() {
		RequestInfo reqInfo = new RequestInfo();
		HttpHeaders headers = new HttpHeaders();
		headers.add("USER_ID", "X-API-SOURCE-USER");
		headers.add("PARTY_SOURCE_ID", "X-API-PARTY-SOURCE-ID-NUMBER");
		headers.add("TRANSACTION_ID", "X-API-TRANSACTION-ID");
		PaymentInstructionProposalAuthorisingParty paymentInstructionProposalAuthorisingParty = new PaymentInstructionProposalAuthorisingParty();
		domesticStandingOrderScaConsentFoundationServiceClient.restTransportForDomesticStandingOrderSCAConsentAuthorise(
				reqInfo, paymentInstructionProposalAuthorisingParty, StandingOrderInstructionProposal.class, headers);
	}

}
