package com.capgemini.psd2.foundationservice.dso.sca.consent.boi.adapter.test;

import static org.mockito.Matchers.any;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.foundationservice.dso.sca.consent.boi.adapter.DomesticStandingOrderScaConsentFoundationServiceAdapter;
import com.capgemini.psd2.foundationservice.dso.sca.consent.boi.adapter.client.DomesticStandingOrderScaConsentFoundationServiceAdapterClient;
import com.capgemini.psd2.foundationservice.dso.sca.consent.boi.adapter.delegate.DomesticStandingOrderScaConsentFoundationServiceAdapterDelegate;
import com.capgemini.psd2.foundationservice.dso.sca.consent.boi.adapter.raml.domain.PaymentInstructionProposalAuthorisingParty;
import com.capgemini.psd2.foundationservice.dso.sca.consent.boi.adapter.raml.domain.StandingOrderInstructionProposal;
import com.capgemini.psd2.foundationservice.dso.sca.consent.boi.adapter.transformer.DomesticStandingOrderScaConsentFoundationServiceAdapterTransformer;
import com.capgemini.psd2.pisp.enums.PaymentTypeEnum;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageUpdateData;
import com.capgemini.psd2.rest.client.sync.impl.RestClientSyncImpl;

@RunWith(SpringJUnit4ClassRunner.class)
public class DomesticStandingOrderScaConsentFoundationServiceAdapterTest {
	@InjectMocks
	private DomesticStandingOrderScaConsentFoundationServiceAdapter domesticStandingOrderScaConsentFoundationServiceAdapter;

	@Mock
	private DomesticStandingOrderScaConsentFoundationServiceAdapterDelegate domesticStandingOrderScaConsentFoundationServiceDelegate;

	@Mock
	private DomesticStandingOrderScaConsentFoundationServiceAdapterTransformer domesticStandingOrderScaConsentFoundationServiceTransformer;

	@Mock
	private DomesticStandingOrderScaConsentFoundationServiceAdapterClient domesticStandingOrderScaConsentFoundationServiceClient;

	/** The rest client. */
	@Mock
	private RestClientSyncImpl restClient;

	/**
	 * Sets the up.
	 */
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * Context loads.
	 */
	@Test
	public void contextLoads() {
	}
	@Test
	public void testDomesticStandingOrderScaConsentFoundationServiceAdapter() {
		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();
		CustomPaymentStageUpdateData updateData=new CustomPaymentStageUpdateData();
		Map<String, String> params = new HashMap<>();
		stageIdentifiers.setPaymentConsentId("e15cffc5-46de-4694-9852-16ebad3a9568");
		stageIdentifiers.setPaymentSetupVersion("v3.0");
		stageIdentifiers.setPaymentTypeEnum(PaymentTypeEnum.DOMESTIC_PAY);
		HttpHeaders httpHeaders = new HttpHeaders();

		params.put("USER_ID", "X-API-SOURCE-USER");
		params.put("PARTY_SOURCE_ID", "X-API-PARTY-SOURCE-ID-NUMBER");
		params.put("TRANSACTION_ID", "X-API-TRANSACTION-ID");
		params.put("X-BOI-CHANNEL", "B365");

		
		updateData.setSetupStatus("AwaitingAuthorisation");
		
		Mockito.when(domesticStandingOrderScaConsentFoundationServiceDelegate.createDomesticStandingOrderSCAConsentRequestHeaders(any(), any()))
		.thenReturn(httpHeaders);
		Mockito.when(domesticStandingOrderScaConsentFoundationServiceDelegate.getpostAuthorisingPartyHeadersForDSOURL(any(), any()))
		.thenReturn(
				"http://localhost:9088/group-payments/p/payments-service//v3.0/domestic/standing-orders/payment-instruction-proposals/e15cffc5-46de-4694-9852-16ebad3a9568/authorising-party");
		Mockito.when(domesticStandingOrderScaConsentFoundationServiceClient.restTransportForDomesticStandingOrderFoundationServicePostAuthorisingParty(any(), any(),any(),
				any())).thenReturn(new StandingOrderInstructionProposal());
		Mockito.when(domesticStandingOrderScaConsentFoundationServiceTransformer.transformDomesticStandingOrderForSCAConsentB365(any(),any()))
		.thenReturn(new PaymentInstructionProposalAuthorisingParty());

		domesticStandingOrderScaConsentFoundationServiceAdapter.retrieveConsentAppDebtorDetails(stageIdentifiers, updateData, params);

	}
	
	@Test
	public void testDomesticStandingOrderScaConsentFoundationServiceAdapter_Reject() {
		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();
		CustomPaymentStageUpdateData updateData=new CustomPaymentStageUpdateData();
		Map<String, String> params = new HashMap<>();
		stageIdentifiers.setPaymentConsentId("e15cffc5-46de-4694-9852-16ebad3a9568");
		stageIdentifiers.setPaymentSetupVersion("v3.0");
		stageIdentifiers.setPaymentTypeEnum(PaymentTypeEnum.DOMESTIC_PAY);
		HttpHeaders httpHeaders = new HttpHeaders();

		params.put("USER_ID", "X-API-SOURCE-USER");
		params.put("PARTY_SOURCE_ID", "X-API-PARTY-SOURCE-ID-NUMBER");
		params.put("TRANSACTION_ID", "X-API-TRANSACTION-ID");

		
		updateData.setSetupStatus("AwaitingAuthorisation");
		
		Mockito.when(domesticStandingOrderScaConsentFoundationServiceDelegate.createDomesticStandingOrderSCAConsentRequestHeaders(any(), any()))
		.thenReturn(httpHeaders);
		Mockito.when(domesticStandingOrderScaConsentFoundationServiceDelegate.rejectDomesticStandingOrderFoundationServiceURL(any(), any()))
		.thenReturn(
				"http://localhost:9088/group-payments/p/payments-service//v3.0/domestic/standing-orders/payment-instruction-proposals/e15cffc5-46de-4694-9852-16ebad3a9568/reject");
		Mockito.when(domesticStandingOrderScaConsentFoundationServiceClient.restTransportForDomesticStandingOrderFoundationServicePostAuthorisingParty(any(), any(),any(),
				any())).thenReturn(new StandingOrderInstructionProposal());


		domesticStandingOrderScaConsentFoundationServiceAdapter.rejectstatusforDSO(stageIdentifiers, updateData, params);

	}
	
	@Test
	public void testDomesticStandingOrderScaConsentFoundationServiceAdapterAuthorise() {
		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();
		CustomPaymentStageUpdateData updateData=new CustomPaymentStageUpdateData();
		Map<String, String> params = new HashMap<>();
		stageIdentifiers.setPaymentConsentId("7118ae3f-233c-4722-b649-1db5f9b766da");
		stageIdentifiers.setPaymentSetupVersion("v3.0");
		stageIdentifiers.setPaymentTypeEnum(PaymentTypeEnum.DOMESTIC_PAY);
		HttpHeaders httpHeaders = new HttpHeaders();
		params.put("USER_ID", "X-API-SOURCE-USER");
		params.put("PARTY_SOURCE_ID", "X-API-PARTY-SOURCE-ID-NUMBER");
		params.put("TRANSACTION_ID", "X-API-TRANSACTION-ID");		
		updateData.setSetupStatus("AwaitingAuthorisation");		
		Mockito.when(domesticStandingOrderScaConsentFoundationServiceDelegate.createDomesticStandingOrderSCAConsentRequestHeaders(any(), any()))
		.thenReturn(httpHeaders);
		Mockito.when(domesticStandingOrderScaConsentFoundationServiceDelegate.createDomesticStandingOrderAuthorisePostBaseURL(any(), any()))
		.thenReturn(
				"http://localhost:9088/group-payments/p/payments-service/v3.0/domestic/standing-orders/payment-instruction-proposals/e15cffc5-46de-4694-9852-16ebad3a9568/Authorise");
		Mockito.when(domesticStandingOrderScaConsentFoundationServiceClient.restTransportForDomesticStandingOrderSCAConsentAuthorise(any(), any(),any(),
				any())).thenReturn(new StandingOrderInstructionProposal());
		domesticStandingOrderScaConsentFoundationServiceAdapter.authoriseforDSO(stageIdentifiers, updateData, params);
	}
	
}