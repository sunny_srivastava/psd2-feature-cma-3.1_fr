package com.capgemini.psd2.foundationservice.dso.sca.consent.boi.adapter.test;

import static org.junit.Assert.assertNotNull;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.foundationservice.dso.sca.consent.boi.adapter.delegate.DomesticStandingOrderScaConsentFoundationServiceAdapterDelegate;
import com.capgemini.psd2.foundationservice.dso.sca.consent.boi.adapter.transformer.DomesticStandingOrderScaConsentFoundationServiceAdapterTransformer;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;

@RunWith(SpringJUnit4ClassRunner.class)
public class DomesticStandingOrderScaConsentFoundationServiceDelegateTest {
	@InjectMocks
	private DomesticStandingOrderScaConsentFoundationServiceAdapterDelegate delegate;

	@Mock
	private DomesticStandingOrderScaConsentFoundationServiceAdapterTransformer domesticStandingOrderScaConsentFoundationServiceTransformer;

	
	@Test
	public void contextLoads() {
	}
	
	
	@Test
	public void testDomesticStandingOrderScaConsentFoundationServiceDelegate() {

		ReflectionTestUtils.setField(delegate, "transactioReqHeader", "transactioReqHeader");
		ReflectionTestUtils.setField(delegate, "correlationMuleReqHeader", "correlation");
		ReflectionTestUtils.setField(delegate, "sourceUserReqHeader", "sorceuser");
		ReflectionTestUtils.setField(delegate, "sourceSystemReqHeader", "source");
		ReflectionTestUtils.setField(delegate, "apiChannelCode", "channel");
		ReflectionTestUtils.setField(delegate, "partysourceReqHeader", "partysource");
		ReflectionTestUtils.setField(delegate, "domesticStandingOrderConsentBaseURL", "http:/localhost:9088/group-payments/p/payments-service");
		ReflectionTestUtils.setField(delegate, "systemApiVersion", "http:/localhost:9088/group-payments/p/payments-service");
		ReflectionTestUtils.setField(delegate, "channelBrand", "tenant_id");
		Map<String, String> params = new HashMap<>();
		CustomPaymentStageIdentifiers stageIden=new CustomPaymentStageIdentifiers();
		stageIden.setPaymentSetupVersion("1.1");
		params.put("X-API-SOURCE-SYSTEM", "fgrd");
		params.put("X-API-SOURCE-USER", "USER_ID");
		params.put("X-API-PARTY-SOURCE-ID-NUMBER", "PARTY_SOURCE_ID");
		params.put("X-API-TRANSACTION-ID", "TRANSACTION_ID");
		params.put("BOL", "BOL");
		params.put("correlationMuleReqHeader", "correlationMuleReqHeader");
		params.put("x-api-party-source-id-number", "x-api-party-source-id-number");
		params.put("X-CORRELATION-ID", "X-CORRELATION-ID");
		params.put("X-BOI-USER", "X-BOI-USER");
		params.put("X-BOI-CHANNEL", "BOL");
		params.put("tenant_id", "BOIUK");
		delegate.createDomesticStandingOrderSCAConsentRequestHeaders(stageIden, params);
	}


	@Test
	public void testdomesticStandingOrderScaConsentFoundationServiceDelegateForUrlAuthoriseParty() {

		String version = "v3.0";
		String PaymentInstuctionProposalId = "e15cffc5-46de-4694-9852-16ebad3a9568";
		ReflectionTestUtils.setField(delegate, "domesticStandingOrderConsentBaseURL", "http:/localhost:9088/group-payments/p/payments-service");
		delegate.getpostAuthorisingPartyHeadersForDSOURL(version, PaymentInstuctionProposalId);
		assertNotNull(delegate);
	}
	
	
	@Test
	public void testdomesticStandingOrderScaConsentFoundationServiceDelegateForUrlRejerct() {

		String version = "v3.0";
		String PaymentInstuctionProposalId = "e15cffc5-46de-4694-9852-16ebad3a9568";
		ReflectionTestUtils.setField(delegate, "domesticStandingOrderConsentBaseURL", "http:/localhost:9088/group-payments/p/payments-service");
		delegate.rejectDomesticStandingOrderFoundationServiceURL(version, PaymentInstuctionProposalId);
		assertNotNull(delegate);
	}
	
	
	@Test
	public void testdomesticStandingOrderScaConsentFoundationServiceDelegateForUrlAuthorise() {

		String version = "v3.0";
		String PaymentInstuctionProposalId = "7118ae3f-233c-4722-b649-1db5f9b766da";
		ReflectionTestUtils.setField(delegate, "domesticStandingOrderConsentBaseURL", "http:/localhost:9088/group-payments/p/payments-service");
		delegate.createDomesticStandingOrderAuthorisePostBaseURL(version, PaymentInstuctionProposalId);
		assertNotNull(delegate);
	}	
	
	@Test
	public void testDomesticStandingOrderScaConsentFoundationServiceDelegateChangeValues() {
		ReflectionTestUtils.setField(delegate, "transactioReqHeader", "transactioReqHeader");
		ReflectionTestUtils.setField(delegate, "correlationMuleReqHeader", "correlation");
		ReflectionTestUtils.setField(delegate, "sourceUserReqHeader", "sorceuser");
		ReflectionTestUtils.setField(delegate, "sourceSystemReqHeader", "source");
		ReflectionTestUtils.setField(delegate, "apiChannelCode", "channel");
		ReflectionTestUtils.setField(delegate, "partysourceReqHeader", "partysource");
		ReflectionTestUtils.setField(delegate, "domesticStandingOrderConsentBaseURL", "http:/localhost:9088/group-payments/p/payments-service");
		ReflectionTestUtils.setField(delegate, "systemApiVersion", "http:/localhost:9088/group-payments/p/payments-service");
		ReflectionTestUtils.setField(delegate, "channelBrand", "tenant_id");
		Map<String, String> params = new HashMap<>();
		CustomPaymentStageIdentifiers stageIden=new CustomPaymentStageIdentifiers();
		stageIden.setPaymentSetupVersion("1.1");
		params.put("X-API-SOURCE-SYSTEM", "fgrd");
		params.put("X-API-SOURCE-USER", "USER_ID");
		params.put("X-API-PARTY-SOURCE-ID-NUMBER", "PARTY_SOURCE_ID");
		params.put("X-API-TRANSACTION-ID", "TRANSACTION_ID");
		params.put("BOL", "BOL");
		params.put("correlationMuleReqHeader", "correlationMuleReqHeader");
		params.put("x-api-party-source-id-number", "x-api-party-source-id-number");
		params.put("X-CORRELATION-ID", "X-CORRELATION-ID");
		params.put("X-BOI-USER", "X-BOI-USER");
		params.put("X-BOI-CHANNEL", "B365");
		params.put("tenant_id", "BOIROI");
		delegate.createDomesticStandingOrderSCAConsentRequestHeaders(stageIden, params);
	}

	@Test(expected = AdapterException.class)
	public void testdomesticStandingOrderScaConsentFoundationServiceDelegateForUrlAuthorisePartyVersionNullCheck() {
		String version = null;
		String PaymentInstuctionProposalId = "7118ae3f-233c-4722-b649-1db5f9b766da";
		ReflectionTestUtils.setField(delegate, "domesticStandingOrderConsentBaseURL", "http:/localhost:9088/group-payments/p/payments-service");
		delegate.getpostAuthorisingPartyHeadersForDSOURL(version, PaymentInstuctionProposalId);		
	}

	
	@Test(expected = AdapterException.class)
	public void testdomesticStandingOrderScaConsentFoundationServiceDelegateForUrlAuthorisePartyIdNullCheck() {
		String version = "3.0";
		String PaymentInstuctionProposalId = null;
		ReflectionTestUtils.setField(delegate, "domesticStandingOrderConsentBaseURL", "http:/localhost:9088/group-payments/p/payments-service");
		delegate.getpostAuthorisingPartyHeadersForDSOURL(version, PaymentInstuctionProposalId);
	}
	
	
	@Test(expected = AdapterException.class)
	public void testdomesticStandingOrderScaConsentFoundationServiceDelegateForUrlRejerctVersionNullCheck() {

		String version = null;
		String PaymentInstuctionProposalId = "e15cffc5-46de-4694-9852-16ebad3a9568";
		ReflectionTestUtils.setField(delegate, "domesticStandingOrderConsentBaseURL", "http:/localhost:9088/group-payments/p/payments-service");
		delegate.rejectDomesticStandingOrderFoundationServiceURL(version, PaymentInstuctionProposalId);		
	}
	
	@Test(expected = AdapterException.class)
	public void testdomesticStandingOrderScaConsentFoundationServiceDelegateForUrlRejerctIdNullCheck() {

		String version = "3.0";
		String PaymentInstuctionProposalId = null;
		ReflectionTestUtils.setField(delegate, "domesticStandingOrderConsentBaseURL", "http:/localhost:9088/group-payments/p/payments-service");
		delegate.rejectDomesticStandingOrderFoundationServiceURL(version, PaymentInstuctionProposalId);		
	}
	
	
}
