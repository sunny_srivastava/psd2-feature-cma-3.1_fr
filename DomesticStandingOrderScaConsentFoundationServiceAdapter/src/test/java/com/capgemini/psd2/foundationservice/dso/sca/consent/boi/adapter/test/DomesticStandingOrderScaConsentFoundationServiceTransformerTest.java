package com.capgemini.psd2.foundationservice.dso.sca.consent.boi.adapter.test;

import java.util.HashMap;
import java.util.Map;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import com.capgemini.psd2.adapter.fraudnet.domain.DecisionType;
import com.capgemini.psd2.adapter.fraudnet.domain.FraudServiceResponse;
import com.capgemini.psd2.foundationservice.dso.sca.consent.boi.adapter.raml.domain.Currency;
import com.capgemini.psd2.foundationservice.dso.sca.consent.boi.adapter.raml.domain.FinancialEventAmount;
import com.capgemini.psd2.foundationservice.dso.sca.consent.boi.adapter.raml.domain.PaymentTransaction;
import com.capgemini.psd2.foundationservice.dso.sca.consent.boi.adapter.raml.domain.StandingOrderInstructionProposal;
import com.capgemini.psd2.foundationservice.dso.sca.consent.boi.adapter.transformer.DomesticStandingOrderScaConsentFoundationServiceAdapterTransformer;
import com.capgemini.psd2.pisp.domain.OBCashAccountDebtor3;
import com.capgemini.psd2.pisp.enums.PaymentTypeEnum;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageUpdateData;

@RunWith(SpringJUnit4ClassRunner.class)
public class DomesticStandingOrderScaConsentFoundationServiceTransformerTest {

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void testTransformDomesticStandingOrderForSCAConsent() {		
		DomesticStandingOrderScaConsentFoundationServiceAdapterTransformer domesticStandingOrderScaConsentFoundationServiceTransformer;
		domesticStandingOrderScaConsentFoundationServiceTransformer = new DomesticStandingOrderScaConsentFoundationServiceAdapterTransformer();
		Map<String, String> params = new HashMap<>();
		params.put("x-api-correlation-id", "s758affv8v-acjnjs7as-najcnj7-akjbjv");
		params.put("X-API-TRANSACTION-ID", "s758affv8v-acjnjs7as-najcnj7-akjbjv");
		params.put("x-api-source-system", "PSD2API");
		params.put("x-api-source-user", "TPP");
		params.put("x-api-channel-code", "BOL");
		params.put("x-api-channel-brand", "BOI-NIGB");
		params.put("x-system-api-version", "3.0");
		params.put("x-api-party-source-id-number", "52679");
		params.put("X-BOI-CHANNEL","BOL");
		params.put("tenant_id","BOIROI");		
		params.put("partyIdentifier","100001");
		params.put("gcisCustomertype","INDIVIDUAL");
		params.put("partyActiveIndicator","true");
		params.put("partyName","David Darsh");
		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();
		stageIdentifiers.setPaymentConsentId("e15cffc5-46de-4694-9852-16ebad3a9568");
		stageIdentifiers.setPaymentSetupVersion("v3.0");
		stageIdentifiers.setPaymentTypeEnum(PaymentTypeEnum.DOMESTIC_PAY);
		CustomPaymentStageUpdateData updateData=new CustomPaymentStageUpdateData();
		updateData.setSetupStatus("AwaitingAuthorisation");
		updateData.setDebtorDetailsUpdated(true);
		updateData.setFraudScoreUpdated(true);
		OBCashAccountDebtor3 oBCashAccountDebtor3 = new OBCashAccountDebtor3();
		oBCashAccountDebtor3.setSchemeName("UK.OBIE.IBAN");
		oBCashAccountDebtor3.setIdentification("BE71096123456769");
		oBCashAccountDebtor3.setName("John Debitor");
		oBCashAccountDebtor3.setSecondaryIdentification("00003400004443");
		updateData.setDebtorDetails(oBCashAccountDebtor3);
		FraudServiceResponse FraudServiceResponse = new FraudServiceResponse();
		FraudServiceResponse.setDecisionScore(123);		
		FraudServiceResponse.setDecisionType(DecisionType.APPROVE);
		updateData.setFraudScore(FraudServiceResponse);
		domesticStandingOrderScaConsentFoundationServiceTransformer.transformDomesticStandingOrderForSCAConsentB365(updateData, params);
	}
	
	@Test
	public void testTransformresposnseAuthorisingPartyFDtoAPI() {
		DomesticStandingOrderScaConsentFoundationServiceAdapterTransformer domesticStandingOrderScaConsentFoundationServiceTransformer;
		domesticStandingOrderScaConsentFoundationServiceTransformer = new DomesticStandingOrderScaConsentFoundationServiceAdapterTransformer();
		StandingOrderInstructionProposal inputBalanceObjStanding = new StandingOrderInstructionProposal();
		Currency c1 = new Currency();		
		c1.setIsoAlphaCode("IE");		
		PaymentTransaction payment = new PaymentTransaction();
		FinancialEventAmount amount = new FinancialEventAmount();
		amount.setTransactionCurrency(12.00);
		payment.setFinancialEventAmount(amount);
		Currency c = new Currency();
		c.isoAlphaCode("EUR");
		payment.setTransactionCurrency(c);
		inputBalanceObjStanding.setFirstPaymentAmount(payment);		
		amount.setTransactionCurrency(24.00);
		c.isoAlphaCode("EUR");		
		amount.setTransactionCurrency(24.00);
		c.isoAlphaCode("EUR");
		domesticStandingOrderScaConsentFoundationServiceTransformer.transformresposnseAuthorisingPartyFDtoAPI(inputBalanceObjStanding);		
	}
	
	@Test
	public void testTransformDomesticStandingOrderForSCAConsentNullCheck() {
		DomesticStandingOrderScaConsentFoundationServiceAdapterTransformer domesticStandingOrderScaConsentFoundationServiceTransformer;
		domesticStandingOrderScaConsentFoundationServiceTransformer = new DomesticStandingOrderScaConsentFoundationServiceAdapterTransformer();
		Map<String, String> params = new HashMap();
		CustomPaymentStageUpdateData updateData=new CustomPaymentStageUpdateData(); 
		domesticStandingOrderScaConsentFoundationServiceTransformer.transformDomesticStandingOrderForSCAConsentB365(updateData, params);
	}	
	
	
	@Test
	public void testTransformDomesticStandingOrderForSCAConsentChangeValues() {		
		DomesticStandingOrderScaConsentFoundationServiceAdapterTransformer domesticStandingOrderScaConsentFoundationServiceTransformer;
		domesticStandingOrderScaConsentFoundationServiceTransformer = new DomesticStandingOrderScaConsentFoundationServiceAdapterTransformer();
		Map<String, String> params = new HashMap<>();
		params.put("x-api-correlation-id", "s758affv8v-acjnjs7as-najcnj7-akjbjv");
		params.put("X-API-TRANSACTION-ID", "s758affv8v-acjnjs7as-najcnj7-akjbjv");
		params.put("x-api-source-system", "PSD2API");
		params.put("x-api-source-user", "TPP");
		params.put("x-api-channel-code", "B365");
		params.put("x-api-channel-brand", "BOI-NIGB");
		params.put("x-system-api-version", "3.0");
		params.put("x-api-party-source-id-number", "52679");
		params.put("X-BOI-CHANNEL","B365");
		params.put("X-BOI-USER","B365");
		params.put("tenant_id","BOIUK");		
		params.put("partyIdentifier","100001");
		params.put("gcisCustomertype","INDIVIDUAL");
		params.put("partyActiveIndicator","true");
		params.put("partyName","David Darsh");
		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();
		stageIdentifiers.setPaymentConsentId("e15cffc5-46de-4694-9852-16ebad3a9568");
		stageIdentifiers.setPaymentSetupVersion("v3.0");
		stageIdentifiers.setPaymentTypeEnum(PaymentTypeEnum.DOMESTIC_PAY);
		CustomPaymentStageUpdateData updateData=new CustomPaymentStageUpdateData();
		updateData.setSetupStatus("AwaitingAuthorisation");
		updateData.setDebtorDetailsUpdated(true);
		updateData.setFraudScoreUpdated(true);
		OBCashAccountDebtor3 oBCashAccountDebtor3 = new OBCashAccountDebtor3();
		oBCashAccountDebtor3.setSchemeName(null);
		oBCashAccountDebtor3.setIdentification(null);
		oBCashAccountDebtor3.setName(null);
		oBCashAccountDebtor3.setSecondaryIdentification(null);
		updateData.setDebtorDetails(oBCashAccountDebtor3);
		FraudServiceResponse FraudServiceResponse = new FraudServiceResponse();
		FraudServiceResponse.setDecisionScore(123);		
		FraudServiceResponse.setDecisionType(DecisionType.INVESTIGATE_WAIT);
		updateData.setFraudScore(FraudServiceResponse);
		domesticStandingOrderScaConsentFoundationServiceTransformer.transformDomesticStandingOrderForSCAConsentB365(updateData, params);
		FraudServiceResponse.setDecisionType(DecisionType.INVESTIGATE_CANCEL);
		updateData.setFraudScore(FraudServiceResponse);
		domesticStandingOrderScaConsentFoundationServiceTransformer.transformDomesticStandingOrderForSCAConsentB365(updateData, params);
		FraudServiceResponse.setDecisionType(DecisionType.REJECT);
		updateData.setFraudScore(FraudServiceResponse);
		domesticStandingOrderScaConsentFoundationServiceTransformer.transformDomesticStandingOrderForSCAConsentB365(updateData, params);
		FraudServiceResponse.setDecisionType(DecisionType.INVESTIGATE_APPROVE);
		updateData.setFraudScore(FraudServiceResponse);
		domesticStandingOrderScaConsentFoundationServiceTransformer.transformDomesticStandingOrderForSCAConsentB365(updateData, params);
		
	}
	
}

