package com.capgemini.psd2.foundationservice.dso.sca.consent.boi.adapter;


import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.adapter.exceptions.AdapterErrorCodeEnum;
import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.adapter.exceptions.domain.CommonResponse;
import com.capgemini.psd2.adapter.exceptions.domain.ValidationWrapper;
import com.capgemini.psd2.adapter.utility.AdapterUtility;
import com.capgemini.psd2.foundationservice.dso.sca.consent.boi.adapter.client.DomesticStandingOrderScaConsentFoundationServiceAdapterClient;
import com.capgemini.psd2.foundationservice.dso.sca.consent.boi.adapter.constants.DomesticStandingOrderScaConsentFoundationServiceAdapterConstants;
import com.capgemini.psd2.foundationservice.dso.sca.consent.boi.adapter.delegate.DomesticStandingOrderScaConsentFoundationServiceAdapterDelegate;
import com.capgemini.psd2.foundationservice.dso.sca.consent.boi.adapter.raml.domain.ChannelCode1;
import com.capgemini.psd2.foundationservice.dso.sca.consent.boi.adapter.raml.domain.PaymentInstructionProposalAuthorisingParty;
import com.capgemini.psd2.foundationservice.dso.sca.consent.boi.adapter.raml.domain.StandingOrderInstructionProposal;
import com.capgemini.psd2.foundationservice.dso.sca.consent.boi.adapter.transformer.DomesticStandingOrderScaConsentFoundationServiceAdapterTransformer;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.pisp.stage.domain.CustomConsentAppViewData;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageUpdateData;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.utilities.NullCheckUtils;
import com.google.gson.Gson;

@Component
public class DomesticStandingOrderScaConsentFoundationServiceAdapter{
	@Autowired
	private DomesticStandingOrderScaConsentFoundationServiceAdapterClient pispScaConsClient;

	@Autowired
	private DomesticStandingOrderScaConsentFoundationServiceAdapterDelegate pispScaConsDelgate;

	@Autowired
	private DomesticStandingOrderScaConsentFoundationServiceAdapterTransformer pispScaConsTransformer;
	
	@Autowired
	private AdapterUtility adapterUtility;
		
	public CustomConsentAppViewData retrieveConsentAppDebtorDetails(CustomPaymentStageIdentifiers stageIdentifiers,
			CustomPaymentStageUpdateData updateData, Map<String, String> paramsMap) {
		CustomConsentAppViewData customConsentAppViewData = new CustomConsentAppViewData();
		
		if(paramsMap.get(PSD2Constants.CHANNEL_IN_REQ_HEADER).equalsIgnoreCase(ChannelCode1.B365.toString())) {
			RequestInfo requestinfo = new RequestInfo();
			  
				    HttpHeaders httpHeaders = pispScaConsDelgate.createDomesticStandingOrderSCAConsentRequestHeaders(stageIdentifiers,paramsMap);
				
				    PaymentInstructionProposalAuthorisingParty transformtoPaymentInstructionProposalAuthorisingParty = pispScaConsTransformer
							.transformDomesticStandingOrderForSCAConsentB365(updateData,paramsMap);
					String postAuthorisingPartyHeadersForDSOURL = pispScaConsDelgate.getpostAuthorisingPartyHeadersForDSOURL(
							stageIdentifiers.getPaymentSetupVersion(), stageIdentifiers.getPaymentConsentId());
					requestinfo.setUrl(postAuthorisingPartyHeadersForDSOURL);
										
					//added new condition for error handling  
					CommonResponse commonResponse= null; 
					try {
						StandingOrderInstructionProposal standingOrderInstructionProposal =pispScaConsClient.restTransportForDomesticStandingOrderFoundationServicePostAuthorisingParty(requestinfo,
								transformtoPaymentInstructionProposalAuthorisingParty,
						StandingOrderInstructionProposal.class, httpHeaders);
						customConsentAppViewData=pispScaConsTransformer.transformresposnseAuthorisingPartyFDtoAPI(standingOrderInstructionProposal);
					} catch (AdapterException e) {
						if ((String.valueOf(e.getFoundationError()).trim()
								.equals(DomesticStandingOrderScaConsentFoundationServiceAdapterConstants.MULE_OUT_OF_BOX_POLICY_ERROR1))
								|| (String.valueOf(e.getFoundationError()).trim()
										.equals(DomesticStandingOrderScaConsentFoundationServiceAdapterConstants.MULE_OUT_OF_BOX_POLICY_ERROR2))) {
							throw AdapterException.populatePSD2Exception(e.getMessage(),
									AdapterErrorCodeEnum.BAD_REQUEST);
						}
						commonResponse = (CommonResponse) e.getFoundationError();
						if (commonResponse.getCode().equals(DomesticStandingOrderScaConsentFoundationServiceAdapterConstants.AUTHORISING_PARTY)
								&& commonResponse.getAdditionalInformation().getDetails().toString()
										.contains(DomesticStandingOrderScaConsentFoundationServiceAdapterConstants.VALIDATION_VIOLATIONS)) {
							Gson gson = new Gson();
							ValidationWrapper validationViolations = gson.fromJson(
									gson.toJson(commonResponse.getAdditionalInformation().getDetails()),
									ValidationWrapper.class);
							String fsErrorCode = validationViolations.getValidationViolations().getValidationViolation()
									.getErrorCode();
							String errorMapping = adapterUtility.getUiErrorMap().get(fsErrorCode);
							if (!NullCheckUtils.isNullOrEmpty(errorMapping)) {
								AdapterErrorCodeEnum errorCodeEnum = AdapterErrorCodeEnum.valueOf(errorMapping);
								throw AdapterException.populatePSD2Exception(e.getMessage(), errorCodeEnum);
							}
						}
						throw e;
					}
					
		}
		else if(paramsMap.get(PSD2Constants.CHANNEL_IN_REQ_HEADER).equalsIgnoreCase(ChannelCode1.BOL.toString())) {
			return customConsentAppViewData;
		}  
		
		return customConsentAppViewData;
	}
	
	public void rejectstatusforDSO(CustomPaymentStageIdentifiers stageIdentifiers,
			CustomPaymentStageUpdateData updateData, Map<String, String> params) {		
		RequestInfo requestinfo = new RequestInfo();		
			HttpHeaders httpHeaders = pispScaConsDelgate.createDomesticStandingOrderSCAConsentRequestHeaders(stageIdentifiers,params);
			String pispScaConUrlForReject = pispScaConsDelgate
					.rejectDomesticStandingOrderFoundationServiceURL(
							stageIdentifiers.getPaymentSetupVersion(), stageIdentifiers.getPaymentConsentId());
			requestinfo.setUrl(pispScaConUrlForReject);
			String paymentInstructioProposalId=stageIdentifiers.getPaymentConsentId();
			//Added to implement Mule out of Box policies
			try {
			pispScaConsClient.restTransportForDomesticStandingOrderFoundationServiceReject(requestinfo,paymentInstructioProposalId,
							StandingOrderInstructionProposal.class, httpHeaders);	
			}catch (AdapterException e) {
				if ((String.valueOf(e.getFoundationError()).trim()
						.equals(DomesticStandingOrderScaConsentFoundationServiceAdapterConstants.MULE_OUT_OF_BOX_POLICY_ERROR1))
						|| (String.valueOf(e.getFoundationError()).trim()
								.equals(DomesticStandingOrderScaConsentFoundationServiceAdapterConstants.MULE_OUT_OF_BOX_POLICY_ERROR2))) {
					throw AdapterException.populatePSD2Exception(e.getMessage(),
							AdapterErrorCodeEnum.BAD_REQUEST);
				}
				throw e;
				}
	}
	
	// DSO Authorise
			public void authoriseforDSO(CustomPaymentStageIdentifiers stageIdentifiers,
					CustomPaymentStageUpdateData updateData, Map<String, String> params) {				
				RequestInfo requestInfo = new RequestInfo();			
				HttpHeaders httpHeaders = pispScaConsDelgate.createDomesticStandingOrderSCAConsentRequestHeaders(stageIdentifiers,params);			
				String domesticPaymentPostURL = pispScaConsDelgate.createDomesticStandingOrderAuthorisePostBaseURL(stageIdentifiers.getPaymentSetupVersion(),stageIdentifiers.getPaymentConsentId());
				requestInfo.setUrl(domesticPaymentPostURL);			
				PaymentInstructionProposalAuthorisingParty paymentInstructionProposalAuthorisingParty = pispScaConsTransformer.transformDomesticStandingOrderForSCAConsentB365(updateData, params);
				
				//added new condition for error handling 
				CommonResponse commonResponse= null; 
				try {
					pispScaConsClient.restTransportForDomesticStandingOrderSCAConsentAuthorise(requestInfo, paymentInstructionProposalAuthorisingParty, StandingOrderInstructionProposal.class, httpHeaders);			
				} catch (AdapterException e) {
					if ((String.valueOf(e.getFoundationError()).trim()
							.equals(DomesticStandingOrderScaConsentFoundationServiceAdapterConstants.MULE_OUT_OF_BOX_POLICY_ERROR1))
							|| (String.valueOf(e.getFoundationError()).trim()
									.equals(DomesticStandingOrderScaConsentFoundationServiceAdapterConstants.MULE_OUT_OF_BOX_POLICY_ERROR2))) {
						throw AdapterException.populatePSD2Exception(e.getMessage(),
								AdapterErrorCodeEnum.TECHNICAL_ERROR_PPS_PIPV);
					}
					commonResponse = (CommonResponse) e.getFoundationError();
					if ((commonResponse.getCode().equals(DomesticStandingOrderScaConsentFoundationServiceAdapterConstants.AUTHORISED) || commonResponse.getCode().equals(DomesticStandingOrderScaConsentFoundationServiceAdapterConstants.AUTHORISED_1))
							&& commonResponse.getAdditionalInformation().getDetails().toString()
									.contains(DomesticStandingOrderScaConsentFoundationServiceAdapterConstants.VALIDATION_VIOLATIONS)) {
						Gson gson = new Gson();
						ValidationWrapper validationViolations = gson.fromJson(
								gson.toJson(commonResponse.getAdditionalInformation().getDetails()),
								ValidationWrapper.class);
						String fsErrorCode = validationViolations.getValidationViolations().getValidationViolation()
								.getErrorCode();
						String errorMapping = adapterUtility.getUiErrorMap().get(fsErrorCode);
						if (!NullCheckUtils.isNullOrEmpty(errorMapping)) {
							AdapterErrorCodeEnum errorCodeEnum = AdapterErrorCodeEnum.valueOf(errorMapping);
							throw AdapterException.populatePSD2Exception(e.getMessage(), errorCodeEnum);
						}
					}
					throw e;
				}
			}
	
}
