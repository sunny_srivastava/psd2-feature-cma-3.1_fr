package com.capgemini.psd2.foundationservice.dso.sca.consent.boi.adapter.transformer;

import java.util.Map;

import org.springframework.stereotype.Component;

import com.capgemini.psd2.adapter.fraudnet.domain.DecisionType;
import com.capgemini.psd2.adapter.fraudnet.domain.FraudServiceResponse;
import com.capgemini.psd2.foundationservice.dso.sca.consent.boi.adapter.raml.domain.AccountInformation;
import com.capgemini.psd2.foundationservice.dso.sca.consent.boi.adapter.raml.domain.BrandCode3;
import com.capgemini.psd2.foundationservice.dso.sca.consent.boi.adapter.raml.domain.Channel;
import com.capgemini.psd2.foundationservice.dso.sca.consent.boi.adapter.raml.domain.ChannelCode1;
import com.capgemini.psd2.foundationservice.dso.sca.consent.boi.adapter.raml.domain.DigitalUser1;
import com.capgemini.psd2.foundationservice.dso.sca.consent.boi.adapter.raml.domain.FraudSystemResponse;
import com.capgemini.psd2.foundationservice.dso.sca.consent.boi.adapter.raml.domain.PaymentInstructionProposalAuthorisingParty;
import com.capgemini.psd2.foundationservice.dso.sca.consent.boi.adapter.raml.domain.StandingOrderInstructionProposal;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.pisp.stage.domain.AmountDetails;
import com.capgemini.psd2.pisp.stage.domain.CustomConsentAppViewData;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageUpdateData;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Component
public class DomesticStandingOrderScaConsentFoundationServiceAdapterTransformer {
	public PaymentInstructionProposalAuthorisingParty transformDomesticStandingOrderForSCAConsentB365(
			CustomPaymentStageUpdateData updateData, Map<String, String> params) {
		PaymentInstructionProposalAuthorisingParty paymentInstructionProposalAuthorisingParty = new PaymentInstructionProposalAuthorisingParty();
		if (!NullCheckUtils.isNullOrEmpty(updateData)) {

			if ((updateData.getDebtorDetailsUpdated())
					&& (!NullCheckUtils.isNullOrEmpty(updateData.getDebtorDetails()))) {
				AccountInformation authorisingPartyAccount = new AccountInformation();
				if (!NullCheckUtils.isNullOrEmpty(updateData.getDebtorDetails().getSchemeName())) {

					authorisingPartyAccount.setSchemeName(updateData.getDebtorDetails().getSchemeName());
				}
				if (!NullCheckUtils.isNullOrEmpty(updateData.getDebtorDetails().getIdentification())) {
					authorisingPartyAccount.setAccountIdentification(updateData.getDebtorDetails().getIdentification());
				}
				if (!NullCheckUtils.isNullOrEmpty(updateData.getDebtorDetails().getName())) {
					authorisingPartyAccount.setAccountName(updateData.getDebtorDetails().getName());
				}
				if (!NullCheckUtils.isNullOrEmpty(updateData.getDebtorDetails().getSecondaryIdentification())) {
					authorisingPartyAccount
							.setSecondaryIdentification(updateData.getDebtorDetails().getSecondaryIdentification());
				}
				paymentInstructionProposalAuthorisingParty.setAuthorisingPartyAccount(authorisingPartyAccount);
			}

		}
		
		Channel channel = new Channel();
		
		if (!NullCheckUtils.isNullOrEmpty(params.get(PSD2Constants.CHANNEL_IN_REQ_HEADER))) {
			channel.setChannelCode(ChannelCode1.B365);
		}		
		else {
			channel.setChannelCode(ChannelCode1.API);
		}
			
		
		if (!NullCheckUtils.isNullOrEmpty(params.get(PSD2Constants.TENANT_ID))) {
			if (params.get(PSD2Constants.TENANT_ID).equalsIgnoreCase("BOIUK")) {
				channel.setBrandCode(BrandCode3.NIGB);
			}
			if (params.get(PSD2Constants.TENANT_ID).equalsIgnoreCase("BOIROI")) {
				channel.setBrandCode(BrandCode3.ROI);
			}
		}
		paymentInstructionProposalAuthorisingParty.setChannel(channel);
		if (!NullCheckUtils.isNullOrEmpty(params.get(PSD2Constants.USER_IN_REQ_HEADER))) {
			DigitalUser1 digitaluser = new DigitalUser1();
			digitaluser.setDigitalUserIdentifier(params.get(PSD2Constants.USER_IN_REQ_HEADER));
			paymentInstructionProposalAuthorisingParty.setAuthorisingUser(digitaluser);
		}

		if (!NullCheckUtils.isNullOrEmpty(updateData.getFraudScore())) {
			FraudServiceResponse fraud = (FraudServiceResponse) updateData.getFraudScore();

			if (updateData.getFraudScoreUpdated()) {
				if (fraud.getDecisionType().equals(DecisionType.APPROVE)) {
					paymentInstructionProposalAuthorisingParty.setFraudSystemResponse(FraudSystemResponse.APPROVE);
				} else if (fraud.getDecisionType().equals(DecisionType.INVESTIGATE_APPROVE)) {
					paymentInstructionProposalAuthorisingParty.setFraudSystemResponse(FraudSystemResponse.INVESTIGATE);
				} else if (fraud.getDecisionType().equals(DecisionType.INVESTIGATE_CANCEL)) {
					paymentInstructionProposalAuthorisingParty.setFraudSystemResponse(FraudSystemResponse.INVESTIGATE);
				} else if (fraud.getDecisionType().equals(DecisionType.INVESTIGATE_WAIT)) {
					paymentInstructionProposalAuthorisingParty.setFraudSystemResponse(FraudSystemResponse.INVESTIGATE);
				} else if (fraud.getDecisionType().equals(DecisionType.REJECT)) {
					paymentInstructionProposalAuthorisingParty.setFraudSystemResponse(FraudSystemResponse.REJECT);
				}
			}

		}

		return paymentInstructionProposalAuthorisingParty;
	}

	public CustomConsentAppViewData transformresposnseAuthorisingPartyFDtoAPI(
			StandingOrderInstructionProposal standingOrderInstructionProposal) {
		CustomConsentAppViewData customConsentAppViewData = new CustomConsentAppViewData();
		AmountDetails amountDetails = new AmountDetails();
		amountDetails.setAmount(standingOrderInstructionProposal.getFirstPaymentAmount().getFinancialEventAmount()
				.getTransactionCurrency().toString());
		amountDetails.setCurrency(
				standingOrderInstructionProposal.getFirstPaymentAmount().getTransactionCurrency().getIsoAlphaCode());
		customConsentAppViewData.setAmountDetails(amountDetails);

		return customConsentAppViewData;
	}

	
	
}
