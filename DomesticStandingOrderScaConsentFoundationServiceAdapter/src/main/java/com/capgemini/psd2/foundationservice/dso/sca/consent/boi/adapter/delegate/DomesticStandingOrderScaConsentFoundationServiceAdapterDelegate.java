package com.capgemini.psd2.foundationservice.dso.sca.consent.boi.adapter.delegate;

import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.adapter.exceptions.AdapterErrorCodeEnum;
import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.foundationservice.dso.sca.consent.boi.adapter.constants.DomesticStandingOrderScaConsentFoundationServiceAdapterConstants;
import com.capgemini.psd2.foundationservice.dso.sca.consent.boi.adapter.raml.domain.BrandCode3;
import com.capgemini.psd2.foundationservice.dso.sca.consent.boi.adapter.raml.domain.ChannelCode1;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Component
public class DomesticStandingOrderScaConsentFoundationServiceAdapterDelegate {
	@Value("${foundationService.sourceSystemReqHeader:#{X-API-SOURCE-SYSTEM}}")
	private String sourceSystemReqHeader;

	@Value("${foundationService.apiChannelCode:#{X-API-CHANNEL-CODE}}")
	private String apiChannelCode;

	@Value("${foundationService.sourceUserReqHeader:#{X-API-SOURCE-USER}}")
	private String sourceUserReqHeader;

	@Value("${foundationService.partysourceReqHeader:#{X-API-PARTY-SOURCE-ID-NUMBER}}")
	private String partysourceReqHeader;

	@Value("${foundationService.transactioReqHeader:#{X-API-TRANSACTION-ID}}")
	private String transactioReqHeader;

	@Value("${foundationService.correlationMuleReqHeader:#{X-API-CORRELATION-ID}}")
	private String correlationMuleReqHeader;

	@Value("${foundationService.systemApiVersion:#{X-SYSTEM-API-VERSION}}")
	private String systemApiVersion;

	@Value("${foundationService.channelBrand:#{X-API-CHANNEL-BRAND}}")
	private String channelBrand;

	@Value("${foundationService.domesticStandingOrderConsentBaseURL}")
	private String domesticStandingOrderConsentBaseURL;

	private String psd2API = "PSD2API";

	public HttpHeaders createDomesticStandingOrderSCAConsentRequestHeaders(
			CustomPaymentStageIdentifiers stageIdentifiers, Map<String, String> params) {
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setContentType(
				new MediaType(DomesticStandingOrderScaConsentFoundationServiceAdapterConstants.APPLICATION, "json"));
		httpHeaders.add(sourceSystemReqHeader, psd2API);

		if (!NullCheckUtils.isNullOrEmpty(
				params.get(DomesticStandingOrderScaConsentFoundationServiceAdapterConstants.CORRELATION_REQ_HEADER))) {
			httpHeaders.add(transactioReqHeader, params
					.get(DomesticStandingOrderScaConsentFoundationServiceAdapterConstants.CORRELATION_REQ_HEADER));
		}
		if (!NullCheckUtils.isNullOrEmpty(params.get(PSD2Constants.CHANNEL_IN_REQ_HEADER))
				&& params.get(PSD2Constants.CHANNEL_IN_REQ_HEADER).equalsIgnoreCase(ChannelCode1.BOL.toString())) {
			if (!NullCheckUtils.isNullOrEmpty(
					params.get(DomesticStandingOrderScaConsentFoundationServiceAdapterConstants.PARTY_SOURCE_ID))) {
				httpHeaders.add(partysourceReqHeader,
						params.get(DomesticStandingOrderScaConsentFoundationServiceAdapterConstants.PARTY_SOURCE_ID));
			}
		}
		if (!NullCheckUtils.isNullOrEmpty(params.get(PSD2Constants.TENANT_ID))) {
			if (params.get(PSD2Constants.TENANT_ID).equalsIgnoreCase("BOIUK")) {
				httpHeaders.add(channelBrand, BrandCode3.NIGB.toString());
			}
			if (params.get(PSD2Constants.TENANT_ID).equalsIgnoreCase("BOIROI")) {
				httpHeaders.add(channelBrand, BrandCode3.ROI.toString());
			}
		}
		if (!NullCheckUtils.isNullOrEmpty(params.get(PSD2Constants.USER_IN_REQ_HEADER))) {
			httpHeaders.add(sourceUserReqHeader, params.get(PSD2Constants.USER_IN_REQ_HEADER));
		}
		if (!NullCheckUtils.isNullOrEmpty(params.get(PSD2Constants.CHANNEL_IN_REQ_HEADER))) {
			if (params.get(PSD2Constants.CHANNEL_IN_REQ_HEADER).equalsIgnoreCase("BOL")) {
				httpHeaders.add(apiChannelCode, ChannelCode1.BOL.toString());
			}
			if (params.get(PSD2Constants.CHANNEL_IN_REQ_HEADER).equalsIgnoreCase("B365")) {
				httpHeaders.add(apiChannelCode, ChannelCode1.B365.toString());
			}
		}
		if (!NullCheckUtils.isNullOrEmpty(stageIdentifiers.getPaymentSetupVersion())) {
			httpHeaders.add(systemApiVersion, "3.0");
		}
		return httpHeaders;

	}

	public String getpostAuthorisingPartyHeadersForDSOURL(String version, String paymentInstuctionProposalId) {

		if (StringUtils.isBlank(version) || StringUtils.isBlank(paymentInstuctionProposalId)) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.NO_ACCOUNT_DATA_FOUND);
		}

		return domesticStandingOrderConsentBaseURL + "/"+DomesticStandingOrderScaConsentFoundationServiceAdapterConstants.VERSION_V1 + "/"
				+ "domestic/standing-orders/payment-instruction-proposals" + "/" + paymentInstuctionProposalId
				+ "/authorising-party";
	}

	public String rejectDomesticStandingOrderFoundationServiceURL(String version, String paymentInstuctionProposalId) {

		if (StringUtils.isBlank(version) || StringUtils.isBlank(paymentInstuctionProposalId)) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.NO_ACCOUNT_DATA_FOUND);
		}

		return domesticStandingOrderConsentBaseURL + "/"+DomesticStandingOrderScaConsentFoundationServiceAdapterConstants.VERSION_V1 + "/"
				+ "domestic/standing-orders/payment-instruction-proposals" + "/" + paymentInstuctionProposalId
				+ "/reject";
	}

	public String createDomesticStandingOrderAuthorisePostBaseURL(String version, String paymentInstuctionProposalId) {
		return domesticStandingOrderConsentBaseURL + "/"+DomesticStandingOrderScaConsentFoundationServiceAdapterConstants.VERSION_V1 + "/"
				+ "domestic/standing-orders/payment-instruction-proposals" + "/" + paymentInstuctionProposalId
				+ "/authorise";
	}

}
