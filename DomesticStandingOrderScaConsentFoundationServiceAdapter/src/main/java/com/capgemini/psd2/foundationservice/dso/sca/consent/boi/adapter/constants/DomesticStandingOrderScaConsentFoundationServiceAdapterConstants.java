package com.capgemini.psd2.foundationservice.dso.sca.consent.boi.adapter.constants;

public class DomesticStandingOrderScaConsentFoundationServiceAdapterConstants {
	public static final String CORRELATION_REQ_HEADER = "X-CORRELATION-ID";
	public static final String PARTY_SOURCE_ID = "x-api-party-source-id-number";
	public static final String APPLICATION = "application";
	public static final String VERSION_V1 ="v1.0";
	public static final String MULE_OUT_OF_BOX_POLICY_ERROR1 = "401";
	public static final String MULE_OUT_OF_BOX_POLICY_ERROR2 = "429";
	public static final String VALIDATION_VIOLATIONS = "validationViolations";
	public static final String AUTHORISING_PARTY = "PPA_SOAPP_019";
	public static final String AUTHORISED = "PPA_SOAUP_019";
	public static final String AUTHORISED_1 = "PPA_SOAUP_999";
	
}
