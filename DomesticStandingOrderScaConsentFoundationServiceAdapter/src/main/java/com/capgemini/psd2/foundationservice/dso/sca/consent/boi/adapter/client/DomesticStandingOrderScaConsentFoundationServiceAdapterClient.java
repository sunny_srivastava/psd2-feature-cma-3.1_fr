package com.capgemini.psd2.foundationservice.dso.sca.consent.boi.adapter.client;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.capgemini.psd2.foundationservice.dso.sca.consent.boi.adapter.raml.domain.PaymentInstructionProposalAuthorisingParty;
import com.capgemini.psd2.foundationservice.dso.sca.consent.boi.adapter.raml.domain.StandingOrderInstructionProposal;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.RestClientSync;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.ObjectMapper;
@Component
public class DomesticStandingOrderScaConsentFoundationServiceAdapterClient {
	@Autowired
	@Qualifier("restClientFoundation")
	private RestClientSync restClient;
	
	@Autowired
	private RestTemplate restTemplate;
	
	@PostConstruct
	private void init() {
		MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(Include.NON_NULL);
        converter.setObjectMapper(mapper);
        restTemplate.getMessageConverters().add(0,converter);
	}
	
	public StandingOrderInstructionProposal restTransportForDomesticStandingOrderFoundationServicePostAuthorisingParty(
			RequestInfo requestInfo, PaymentInstructionProposalAuthorisingParty standingOrderInstructionProposalRequest,
			Class<StandingOrderInstructionProposal> class1, HttpHeaders httpHeaders) {
		return restClient.callForPost(requestInfo, standingOrderInstructionProposalRequest, class1, httpHeaders);
	}
	
	public StandingOrderInstructionProposal restTransportForDomesticStandingOrderFoundationServiceReject(RequestInfo requestinfo,
			String paymentInstreuctionProposalID,
				Class<StandingOrderInstructionProposal> responseType, HttpHeaders httpHeaders) {
		return restClient.callForPost(requestinfo,paymentInstreuctionProposalID, responseType, httpHeaders);	
	
			
		}
	
	public StandingOrderInstructionProposal restTransportForDomesticStandingOrderSCAConsentAuthorise(RequestInfo requestInfo,
			PaymentInstructionProposalAuthorisingParty paymentInstructionProposalAuthorisingParty, Class<StandingOrderInstructionProposal> responseType,
			HttpHeaders httpHeaders) {		
		return restClient.callForPost(requestInfo, paymentInstructionProposalAuthorisingParty, responseType, httpHeaders);
	}
	
}


