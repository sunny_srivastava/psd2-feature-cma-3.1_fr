/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.foundationservice.account.transactions.boi.adapter;

import java.text.SimpleDateFormat;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import com.capgemini.psd2.adapter.datetime.utility.TransactionDateRange;
import com.capgemini.psd2.adapter.exceptions.AdapterErrorCodeEnum;
import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.aisp.adapter.AccountTransactionAdapter;
import com.capgemini.psd2.aisp.domain.OBReadTransaction6;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountTransactionResponse;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.exceptions.ErrorInfo;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.foundationservice.account.transactions.boi.adapter.client.AccountTransactionsFoundationServiceClient;
import com.capgemini.psd2.foundationservice.account.transactions.boi.adapter.constants.AccountTransactionsFoundationServiceConstants;
import com.capgemini.psd2.foundationservice.account.transactions.boi.adapter.delegate.AccountTransactionsFoundationServiceDelegate;
import com.capgemini.psd2.foundationservice.account.transactions.boi.adapter.raml.domain.TransactionList;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.utilities.NullCheckUtils;

/**
 * The Class AccountTransactionsFoundationServiceAdapter.
 */
@EnableConfigurationProperties
@ConfigurationProperties("foundationService")
@Component
public class AccountTransactionsFoundationServiceAdapter implements AccountTransactionAdapter {

	@Value("${foundationService.consentFlowType}")
	private String consentFlowType;

	@Value("${foundationService.singleAccountCreditCardTransactionBaseURL}")
	private String singleAccountCreditCardTransactionBaseURL;

	@Value("${foundationService.singleAccountTransactionBaseURL}")
	private String singleAccountTransactionBaseURL;

	@Value("${foundationService.version}")
	private String version;

	/** The account transactions foundation service delegate. */
	@Autowired
	private AccountTransactionsFoundationServiceDelegate accountTransactionsFoundationServiceDelegate;

	/** The account transactions foundation service client. */
	@Autowired
	private AccountTransactionsFoundationServiceClient accountTransactionsFoundationServiceClient;

	private PlatformAccountTransactionResponse platformAccountTransactionResponse;

	@Override
	public PlatformAccountTransactionResponse retrieveAccountTransaction(AccountMapping accountMapping,
			Map<String, String> params) {

		AccountDetails accountDetails;
		if (accountMapping.getAccountDetails() != null && !accountMapping.getAccountDetails().isEmpty()) {
			accountDetails = accountMapping.getAccountDetails().get(0);
			if (NullCheckUtils.isNullOrEmpty(accountDetails.getAccountId())) {
				throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.BAD_REQUEST);
			}
			params.put(AccountTransactionsFoundationServiceConstants.ACCOUNT_ID, accountDetails.getAccountId());
			params.put(AccountTransactionsFoundationServiceConstants.ACCOUNT_SUBTYPE,
					accountDetails.getAccountSubType().toString());
		} else
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.BAD_REQUEST);

		RequestInfo requestInfo = new RequestInfo();
		ErrorInfo errorInfo = null;
		TransactionList accountTransactions = null;

		HttpHeaders httpHeaders = null;
		String fromBookingDateTimeInString = null;
		String toBookingDateTimeInString = null;
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		TransactionDateRange transactionDateRange = accountTransactionsFoundationServiceDelegate
				.createTransactionDateRange(params);
		TransactionDateRange decision = accountTransactionsFoundationServiceDelegate.fsCallFilter(transactionDateRange);
		if (decision.isEmptyResponse()) {
			//SITdefect2350-Transaction Null Response
			OBReadTransaction6 oBReadTransaction3 = new OBReadTransaction6();
			PlatformAccountTransactionResponse platformAccountTransactionResponse = new PlatformAccountTransactionResponse();
			platformAccountTransactionResponse.setoBReadTransaction6(oBReadTransaction3);
			return platformAccountTransactionResponse;
		} else {
			fromBookingDateTimeInString = formatter.format(decision.getNewFilterFromDate());
			toBookingDateTimeInString = formatter.format(decision.getNewFilterToDate());
		}

		MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<>();
		queryParams.add(AccountTransactionsFoundationServiceConstants.FROM_DATE, fromBookingDateTimeInString);
		queryParams.add(AccountTransactionsFoundationServiceConstants.TO_DATE, toBookingDateTimeInString);

		if (params.get(AccountTransactionsFoundationServiceConstants.REQUESTED_TXN_FILTER)
				.equalsIgnoreCase(AccountTransactionsFoundationServiceConstants.CREDIT))
			queryParams.add(AccountTransactionsFoundationServiceConstants.CREDIT_DEBIT_INDICATOR,
					(AccountTransactionsFoundationServiceConstants.CR).toString());
		else if (params.get(AccountTransactionsFoundationServiceConstants.REQUESTED_TXN_FILTER)
				.equalsIgnoreCase(AccountTransactionsFoundationServiceConstants.DEBIT))
			queryParams.add(AccountTransactionsFoundationServiceConstants.CREDIT_DEBIT_INDICATOR,
					(AccountTransactionsFoundationServiceConstants.DR).toString());
		else

		queryParams.add(AccountTransactionsFoundationServiceConstants.ORDER, "desc");
		queryParams.add(AccountTransactionsFoundationServiceConstants.TRANSACTION_RETRIEVAL_KEY,
				params.get(AccountTransactionsFoundationServiceConstants.REQUESTED_TXN_KEY));
		queryParams.add(AccountTransactionsFoundationServiceConstants.PAGE_SIZE,
				params.get(AccountTransactionsFoundationServiceConstants.REQUESTED_PAGE_SIZE));
		queryParams.add(AccountTransactionsFoundationServiceConstants.PAGE_NUMBER,
				params.get(AccountTransactionsFoundationServiceConstants.REQUESTED_PAGE_NUMBER));

		if ((NullCheckUtils.isNullOrEmpty(accountDetails.getAccountSubType())
				&& params.get(PSD2Constants.CMAVERSION) == null)
				|| accountDetails.getAccountSubType().toString().contentEquals("CurrentAccount")
				|| accountDetails.getAccountSubType().toString().contentEquals("Savings")) {
			String finalURL = accountTransactionsFoundationServiceDelegate.getFoundationServiceURL(version,
					accountDetails.getAccountNSC(), accountDetails.getAccountNumber(), singleAccountTransactionBaseURL);
			requestInfo.setUrl(finalURL);
			httpHeaders = accountTransactionsFoundationServiceDelegate.createRequestHeaders(requestInfo, accountMapping,
					params);
		} else if (accountDetails.getAccountSubType().toString().contentEquals("CreditCard")) {
			String customerReference = accountDetails.getAccount().getSecondaryIdentification();
			String finalURL = accountTransactionsFoundationServiceDelegate.getCreditCardFoundationServiceURL(version,
					customerReference, accountDetails.getAccountNumber(), singleAccountCreditCardTransactionBaseURL);
			requestInfo.setUrl(finalURL);
			params.put("maskedPan", accountDetails.getAccount().getIdentification()
					.substring(accountDetails.getAccount().getIdentification().length() - 4));
			httpHeaders = accountTransactionsFoundationServiceDelegate.createRamlRequestHeaders(requestInfo,
					accountMapping, params);
		}
		accountTransactions = accountTransactionsFoundationServiceClient.restTransportForSingleAccountTransactions(
				requestInfo, TransactionList.class, queryParams, httpHeaders);
		
		if (NullCheckUtils.isNullOrEmpty(accountTransactions.getTransactionsList()))
			accountTransactions = new TransactionList();
			
		return platformAccountTransactionResponse = accountTransactionsFoundationServiceDelegate
				.transformResponseFromFDToAPI(accountTransactions, params);
	}

}
