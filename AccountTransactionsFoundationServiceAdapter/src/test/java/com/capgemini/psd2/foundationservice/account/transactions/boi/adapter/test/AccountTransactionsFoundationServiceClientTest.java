package com.capgemini.psd2.foundationservice.account.transactions.boi.adapter.test;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.anyObject;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import com.capgemini.psd2.foundationservice.account.transactions.boi.adapter.client.AccountTransactionsFoundationServiceClient;
import com.capgemini.psd2.foundationservice.account.transactions.boi.adapter.raml.domain.TransactionList;
import com.capgemini.psd2.mask.DataMask;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.RestClientSync;

@RunWith(SpringJUnit4ClassRunner.class)
public class AccountTransactionsFoundationServiceClientTest {
	
	@InjectMocks
	private AccountTransactionsFoundationServiceClient accountTransactionsFoundationServiceClient;
	
	@Mock
	//@Qualifier("restClientFoundation")
	private RestClientSync restClient;
	@Mock
	private DataMask dataMask;
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void testRestTransportForSingleAccountTransactions()
	{
		RequestInfo requestInfo = new RequestInfo();
		TransactionList accounts = new TransactionList();
		HttpHeaders httpHeaders = new HttpHeaders();
		MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<String, String>();
		queryParams.add("accountNSC", "901343");
		queryParams.add("accountNumber", "25369621");
		httpHeaders.add("X-BOI-USER", "12345678");
		httpHeaders.add("X-BOI-CHANNEL", "BOL");
		httpHeaders.add("X-BOI-PLATFORM", "PSD2API");
		httpHeaders.add("x-fapi-interaction-id", "Correlation");
		//ReflectionTestUtils.setField(accountTransactionsFoundationServiceClient, "correlationReqHeader", "x-fapi-interaction-id");
		queryParams.add("RequestedToConsentDateTime", "2017-12-01T23:59:59");
		queryParams.add("endDate","2018-03-31T00:00:00");
		queryParams.add("startDate", "2016-02-01T23:59:59");
		queryParams.add("pageSize", "5");
		queryParams.add("transactionRetrievalKey", "trnxKey");
		queryParams.add("txnType", "CREDIT");
		queryParams.add("pageNumber", "1");
		requestInfo.setUrl("http://localhost:9094/fs-abt-service/services/abt/accounts/901343/25369621/transactions");
		
		Mockito.when(dataMask.maskResponse(anyObject(), anyObject())).thenReturn(accounts);
//		maskResponse is true
	//	Mockito.when(accountTransactionsFoundationServiceClient.restTransportForSingleAccountTransactions(anyObject(),anyObject(),anyObject(),anyObject())).thenReturn(accounts);
		TransactionList response = accountTransactionsFoundationServiceClient.restTransportForSingleAccountTransactions(requestInfo, TransactionList.class, queryParams, httpHeaders);
		Mockito.when(restClient.callForGet(anyObject(), anyObject(), anyObject())).thenReturn(response);
		//Assert.assertNotNull("Null", response);
		//assertThat(response).isEqualTo(accounts);
//		
//		maskResponse is false
		//ReflectionTestUtils.setField(accountTransactionsFoundationServiceClient, "maskBeneficiariesResponse", false);
//		response = accountTransactionsFoundationServiceClient.restTransportForSingleAccountTransactions(requestInfo, Accounts.class, queryParams, httpHeaders);
//		assertThat(response).isEqualTo(accounts);
		assertNotNull(httpHeaders);
	}

@Test
public void testRestTransportForCreditCardTransactions()
{
		RequestInfo requestInfo = new RequestInfo();
		TransactionList accounts = new TransactionList();
		HttpHeaders httpHeaders = new HttpHeaders();
		MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<String, String>();
		queryParams.add("accountNSC", "901343");
	queryParams.add("accountNumber", "25369621");
	httpHeaders.add("X-BOI-USER", "12345678");
	httpHeaders.add("X-BOI-CHANNEL", "BOL");
		httpHeaders.add("X-BOI-PLATFORM", "PSD2API");
		httpHeaders.add("X-CORRELATION-ID", "Correlation");
		queryParams.add("RequestedToConsentDateTime", "2017-12-01T23:59:59");
	queryParams.add("endDate","2018-03-31T00:00:00");
	queryParams.add("startDate", "2016-02-01T23:59:59");
		queryParams.add("pageSize", "5");
		queryParams.add("transactionRetrievalKey", "trnxKey");
		queryParams.add("txnType", "CREDIT");
		queryParams.add("pageNumber", "1");
		requestInfo.setUrl("http://localhost:9094/fs-abt-service/services/abt/accounts/901343/25369621/transactions");
//		//Mockito.when(restClient.callForGet(anyObject(), anyObject(), anyObject())).thenReturn(accounts);
	Mockito.when(dataMask.maskResponse(anyObject(), anyObject())).thenReturn(accounts);
////		maskResponse is true
	TransactionList response = accountTransactionsFoundationServiceClient.restTransportForSingleAccountTransactions(requestInfo, TransactionList.class, queryParams, httpHeaders);
		Mockito.when(restClient.callForGet(anyObject(), anyObject(), anyObject())).thenReturn(response);
		assertNotNull(httpHeaders);
}

}

