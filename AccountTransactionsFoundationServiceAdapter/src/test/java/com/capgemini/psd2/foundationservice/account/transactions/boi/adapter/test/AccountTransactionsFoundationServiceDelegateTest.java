package com.capgemini.psd2.foundationservice.account.transactions.boi.adapter.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.when;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import com.capgemini.psd2.adapter.datetime.utility.TimeZoneDateTimeAdapter;
import com.capgemini.psd2.adapter.datetime.utility.TransactionDateRange;
import com.capgemini.psd2.adapter.exceptions.AdapterErrorCodeEnum;
import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.adapter.utility.AdapterUtility;
import com.capgemini.psd2.aisp.domain.OBExternalAccountSubType1Code;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountTransactionResponse;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.foundationservice.account.transactions.boi.adapter.client.AccountTransactionsFoundationServiceClient;
import com.capgemini.psd2.foundationservice.account.transactions.boi.adapter.constants.AccountTransactionsFoundationServiceConstants;
import com.capgemini.psd2.foundationservice.account.transactions.boi.adapter.delegate.AccountTransactionsFoundationServiceDelegate;
import com.capgemini.psd2.foundationservice.account.transactions.boi.adapter.raml.domain.Transaction;
import com.capgemini.psd2.foundationservice.account.transactions.boi.adapter.raml.domain.TransactionList;
import com.capgemini.psd2.foundationservice.account.transactions.boi.adapter.transformer.AccountTransactionsFoundationServiceTransformer;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.impl.RestClientSyncImpl;

@RunWith(SpringJUnit4ClassRunner.class)
public class AccountTransactionsFoundationServiceDelegateTest {
	
	/** The delegate. */
	@InjectMocks
	private AccountTransactionsFoundationServiceDelegate delegate;

	/** The account transactions foundation service client. */
	@InjectMocks
	private AccountTransactionsFoundationServiceClient accountTransactionsFoundationServiceClient;

	/** The account transactions FS transformer. */
	@Mock
	private AccountTransactionsFoundationServiceTransformer accountTransactionsFSTransformer;

	/** The rest client. */
	@Mock
	private RestClientSyncImpl restClient;
	
	@Mock
	private AdapterUtility adapterUtility;
	
	@Mock
	private TimeZoneDateTimeAdapter timeZoneDateTimeAdapter;

	/**
	 * Sets the up.
	 */
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * Context loads.
	 */
	

	/**
	 * Test rest transport for single account transactions.
	 */
	@Test
	public void testRestTransportForSingleTransactions() {
		Map<String, String> params = new HashMap<>();
		params.put("accountId", "12345");
		params.put("channelId", "BOL");
		TransactionList accounts = new TransactionList();
		AccountMapping accountMapping = new AccountMapping();
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
		AccountDetails accDet = new AccountDetails();
		accDet.setAccountId("1234");
		accDet.setAccountNSC("US2345");
		accDet.setAccountNumber("1234");
		accDetList.add(accDet);
		accountMapping.setAccountDetails(accDetList);
		accountMapping.setTppCID("test");
		accountMapping.setPsuId("test");
		accountMapping.setCorrelationId("test");
		
		MultiValueMap<String, String> queryParams = new LinkedMultiValueMap<String, String>();
		queryParams.add("FromBookingDateTime", "2016-12-31T23:59:59");
		queryParams.add("ToBookingDateTime", "2015-01-01T00:00:00");
		queryParams.add("RequestedPageNumber", "1");
		queryParams.add("TransactionFilter", "CREDIT");
		queryParams.add("transactionRetrievalKey", "trnxKey");

		Mockito.when(restClient.callForGet(any(), any(), any())).thenReturn(accounts);

		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl("http://localhost:8081/fs-abt-service/services/abt/accounts");

		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.add("X-BOI-USER", "header user");
		httpHeaders.add("X-BOI-CHANNEL", "header channel");
		httpHeaders.add("X-CORRELATION-ID", "header correlation id");
		httpHeaders.add("X-BOI-PLATFORM", "header platform");

		TransactionList res = accountTransactionsFoundationServiceClient.restTransportForSingleAccountTransactions(requestInfo, TransactionList.class, queryParams, httpHeaders);
		assertNotNull(res);
	}

	
	@Test
	public void testCreateTransactionDateRangeIfCases(){
		Map<String, String> params = new HashMap<>();
		params.put(AccountTransactionsFoundationServiceConstants.CONSENT_EXPIRATION_DATETIME, "2015-01-01T05:22:11");
		params.put(AccountTransactionsFoundationServiceConstants.END_DATE, "2015-01-01T00:00:00");
		params.put(AccountTransactionsFoundationServiceConstants.FROM_BOOKING_DATE_TIME, "2015-01-01T00:00:00");
		params.put(AccountTransactionsFoundationServiceConstants.REQUESTED_FROM_CONSENT_DATETIME, "2015-01-01T00:00:00");
		params.put(AccountTransactionsFoundationServiceConstants.REQUESTED_FROM_DATETIME, "2015-01-01T00:00:00");
		params.put(AccountTransactionsFoundationServiceConstants.REQUESTED_TO_CONSENT_DATETIME, "2017-01-01T09:33:22");
		params.put(AccountTransactionsFoundationServiceConstants.REQUESTED_TO_DATETIME, "2017-01-01T09:33:22");
		Mockito.when(timeZoneDateTimeAdapter.parseDateTimeFS(anyObject())).thenReturn(new Date());
		Mockito.when(timeZoneDateTimeAdapter.parseDateFS(anyObject())).thenReturn(new Date());
		ReflectionTestUtils.setField(delegate, "defaultTransactionDateRange", 1);
		TransactionDateRange res = delegate.createTransactionDateRange(params);
		assertNotNull(res);
	}	
	
	@Test
	public void testCreateTransactionDateRangeIfCases1(){
		Map<String, String> params = new HashMap<>();
		params.put(AccountTransactionsFoundationServiceConstants.CONSENT_EXPIRATION_DATETIME, null);
		params.put(AccountTransactionsFoundationServiceConstants.END_DATE, null);
		params.put(AccountTransactionsFoundationServiceConstants.FROM_BOOKING_DATE_TIME, null);
		params.put(AccountTransactionsFoundationServiceConstants.REQUESTED_FROM_CONSENT_DATETIME, null);
		params.put(AccountTransactionsFoundationServiceConstants.REQUESTED_FROM_DATETIME, null);
		params.put(AccountTransactionsFoundationServiceConstants.REQUESTED_TO_CONSENT_DATETIME, null);
		params.put(AccountTransactionsFoundationServiceConstants.REQUESTED_TO_DATETIME, null);
		Mockito.when(timeZoneDateTimeAdapter.parseDateTimeFS(anyObject())).thenReturn(new Date());
		Mockito.when(timeZoneDateTimeAdapter.parseDateFS(anyObject())).thenReturn(new Date());
		ReflectionTestUtils.setField(delegate, "defaultTransactionDateRange", 1);
		TransactionDateRange res = delegate.createTransactionDateRange(params);
		assertNotNull(res);
	}	
	
	@Test(expected = AdapterException.class)
	public void testFsCallFilterException(){
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		TransactionDateRange transactionDateRange = new TransactionDateRange();
		try {
			transactionDateRange.setRequestDateTime(dateFormat.parse("2017-01-01T00:00:00"));
			transactionDateRange.setConsentExpiryDateTime(dateFormat.parse("2015-01-01T00:00:00"));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		delegate.fsCallFilter(transactionDateRange);
	}
	
	@Test(expected = AdapterException.class)
	public void testFsCallFilterException1(){
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		TransactionDateRange transactionDateRange = new TransactionDateRange();
		try {
			transactionDateRange.setRequestDateTime(dateFormat.parse("2017-01-01T00:00:00"));
			transactionDateRange.setConsentExpiryDateTime(dateFormat.parse("2018-01-01T00:00:00"));
			transactionDateRange.setFilterFromDate(dateFormat.parse("2017-01-01T00:00:00"));
			transactionDateRange.setFilterToDate(dateFormat.parse("2016-01-01T00:00:00"));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		delegate.fsCallFilter(transactionDateRange);
	}
	
	@Test
	public void testFsCallFilter1(){
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		TransactionDateRange transactionDateRange = new TransactionDateRange();
		try {
			transactionDateRange.setRequestDateTime(dateFormat.parse("2014-01-01T00:00:00"));
			transactionDateRange.setConsentExpiryDateTime(dateFormat.parse("2015-01-01T00:00:00"));
			transactionDateRange.setFilterFromDate(dateFormat.parse("2015-01-01T00:00:00"));
			transactionDateRange.setFilterToDate(dateFormat.parse("2017-01-01T00:00:00"));
			transactionDateRange.setTransactionFromDateTime(dateFormat.parse("2015-01-02T00:00:00"));
			transactionDateRange.setTransactionToDateTime(dateFormat.parse("2017-01-01T00:00:00"));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		TransactionDateRange res = delegate.fsCallFilter(transactionDateRange);
		assertEquals(true, res.isEmptyResponse());
	}
	
	@Test
	public void testFsCallFilter2(){
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		TransactionDateRange transactionDateRange = new TransactionDateRange();
		try {
			transactionDateRange.setRequestDateTime(dateFormat.parse("2014-01-01T00:00:00"));
			transactionDateRange.setConsentExpiryDateTime(dateFormat.parse("2015-01-01T00:00:00"));
			transactionDateRange.setFilterFromDate(dateFormat.parse("2015-01-01T00:00:00"));
			transactionDateRange.setFilterToDate(dateFormat.parse("2017-01-01T00:00:00"));
			transactionDateRange.setTransactionFromDateTime(dateFormat.parse("2014-01-01T00:00:00"));
			transactionDateRange.setTransactionToDateTime(dateFormat.parse("2017-01-01T00:00:00"));
			transactionDateRange.setFilterToDate(dateFormat.parse("2015-01-01T00:00:00"));
			transactionDateRange.setFilterFromDate(dateFormat.parse("2015-01-01T00:00:00"));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		delegate.fsCallFilter(transactionDateRange);
		assertNotNull(transactionDateRange);
	}
	
	@Test
	public void testFsCallFilter3(){
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		TransactionDateRange transactionDateRange = new TransactionDateRange();
		try {
			transactionDateRange.setRequestDateTime(dateFormat.parse("2014-01-01T00:00:00"));
			transactionDateRange.setConsentExpiryDateTime(dateFormat.parse("2015-01-01T00:00:00"));
			transactionDateRange.setFilterFromDate(dateFormat.parse("2015-01-01T00:00:00"));
			transactionDateRange.setFilterToDate(dateFormat.parse("2017-01-02T00:00:00"));
			transactionDateRange.setTransactionFromDateTime(dateFormat.parse("2014-01-01T00:00:00"));
			transactionDateRange.setTransactionToDateTime(dateFormat.parse("2017-01-01T00:00:00"));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		delegate.fsCallFilter(transactionDateRange);
		assertNotNull(transactionDateRange);
	}
	
	@Test
	public void testFsCallFilter4(){
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		TransactionDateRange transactionDateRange = new TransactionDateRange();
		try {
			transactionDateRange.setRequestDateTime(dateFormat.parse("2015-01-01T00:00:00"));
			transactionDateRange.setConsentExpiryDateTime(dateFormat.parse("2015-01-02T00:00:00"));
			transactionDateRange.setFilterFromDate(dateFormat.parse("2015-01-01T00:00:00"));
			transactionDateRange.setFilterToDate(dateFormat.parse("2015-01-02T00:00:00"));
			transactionDateRange.setTransactionFromDateTime(dateFormat.parse("2015-01-01T02:00:00"));
			transactionDateRange.setTransactionToDateTime(dateFormat.parse("2015-01-02T01:00:00"));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		delegate.fsCallFilter(transactionDateRange);
		
		try {
			transactionDateRange.setFilterToDate(dateFormat.parse("2015-01-02T02:00:00"));
			transactionDateRange.setTransactionToDateTime(dateFormat.parse("2015-01-02T01:00:00"));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		delegate.fsCallFilter(transactionDateRange);
		assertNotNull(transactionDateRange);
	}
	/**
	 * Test get foundation service URL.
	 */
	@Test
	public void testGetFoundationServiceURL() {

		String accountNSC = "number";
		String accountNumber = "accountNumber";
		String channelId = "BOL";
		String baseURL = "http://localhost:8081/fs-abt-service/services/abt/accounts";
		Mockito.when(adapterUtility.retriveFoundationServiceURL(any())).thenReturn(baseURL);
		String finalURL = "http://localhost:8081/fs-abt-service/services/abt/accounts/number/accountNumber/transactions";
		String testURL = delegate.getFoundationServiceURL(channelId, accountNSC, accountNumber, baseURL);
		//assertEquals("Test for building URL failed.", finalURL, testURL);
		assertNotNull(testURL);
	}

	/**
	 * Test get foundation service with invalid URL.
	 */
	@Test
	public void testGetFoundationServiceWithInvalidURL() {

		String accountNSC = "number";
		String accountNumber = "accountNumber";
		String channelId = "BOL";
		String baseURL = "http://localhost:8081/fs-abt-service/services/abt/accounts/invalid";
		Mockito.when(adapterUtility.retriveFoundationServiceURL(any())).thenReturn(baseURL);
		String finalURL = "http://localhost:8081/fs-abt-service/services/abt/accounts/number/accountNumber/transactions";
		String testURL = delegate.getFoundationServiceURL(channelId, accountNSC, accountNumber, baseURL);
		assertNotEquals("Test for building URL failed.", finalURL, testURL);
		assertNotNull(testURL);
	}

	/**
	 * Test get foundation service with account NSC as null.
	 */
	@Test(expected = AdapterException.class)
	public void testGetFoundationServiceWithAccountNSCAsNull() {

		String accountNSC = null;
		String accountNumber = "number";
		String channelId = "BOL";
		String baseURL = "http://localhost:8081/fs-abt-service/services/abt/accounts";
		Mockito.when(adapterUtility.retriveFoundationServiceURL(any())).thenReturn(baseURL);
		delegate.getFoundationServiceURL(channelId, accountNSC, accountNumber, baseURL);
		fail("Invalid account NSC");
	}

	/**
	 * Test get foundation service with account number as null.
	 */
	@Test(expected = AdapterException.class)
	public void testGetFoundationServiceWithAccountNumberAsNull() {

		String accountNSC = "nsc";
		String accountNumber = null;
		String baseURL = "http://localhost:8081/fs-abt-service/services/abt/accounts";
		String channelId = "BOL";
		Mockito.when(adapterUtility.retriveFoundationServiceURL(any())).thenReturn(baseURL);
		delegate.getFoundationServiceURL(channelId, accountNSC, accountNumber, baseURL);

		fail("Invalid Account Number");
	}

	/**
	 * Test get foundation service with base URL as null.
	 */
	@Test
	public void testGetFoundationServiceWithBaseURLAsNull() {

		String accountNSC = "nsc";
		String accountNumber = "number";
		String channelId = "BOL";
		Mockito.when(adapterUtility.retriveFoundationServiceURL(any())).thenReturn(null);
		delegate.getFoundationServiceURL(channelId, accountNSC, accountNumber, null);
		assertNotNull(accountNumber);
//		fail("Invalid base URL");
		
	}

	/**
	 * Test create request headers actual.
	 */
	@Test
	public void testCreateRequestHeadersActual() {

		AccountMapping accountMapping = new AccountMapping();
		accountMapping.setPsuId("userId");
		accountMapping.setCorrelationId("correlationId");
		Map<String, String> params = new HashMap<String, String>();
		params.put("channelId", "BOL");

		HttpHeaders httpHeaders = new HttpHeaders();
		ReflectionTestUtils.setField(delegate, "transactioReqHeader", "X-API-TRANSACTION-ID");
		ReflectionTestUtils.setField(delegate, "correlationMuleReqHeader", "X-API-CORRELATION-ID");
		ReflectionTestUtils.setField(delegate, "sourcesystem", "X-API-SOURCE-SYSTEM");
		ReflectionTestUtils.setField(delegate, "sourceUserReqHeader", "X-API-SOURCE-USER");
		ReflectionTestUtils.setField(delegate, "apiChannelCode", "X-API-CHANNEL-CODE");
		ReflectionTestUtils.setField(delegate, "partySourceNumber", "X-API-PARTY-SOURCE-ID-NUMBER");
		httpHeaders = delegate.createRequestHeaders(new RequestInfo(), accountMapping, params);

		assertNotNull(httpHeaders);
	}
	@Test
	public void testRamlCreateRequestHeadersActual() {

		AccountMapping accountMapping = new AccountMapping();
		accountMapping.setPsuId("userId");
		accountMapping.setCorrelationId("correlationId");
		Map<String, String> params = new HashMap<String, String>();
		params.put("channelId", "BOL");

		HttpHeaders httpHeaders = new HttpHeaders();
		ReflectionTestUtils.setField(delegate, "transactioReqHeader", "X-API-TRANSACTION-ID");
		ReflectionTestUtils.setField(delegate, "correlationMuleReqHeader", "X-API-CORRELATION-ID");
		ReflectionTestUtils.setField(delegate, "sourcesystem", "X-API-SOURCE-SYSTEM");
		ReflectionTestUtils.setField(delegate, "sourceUserReqHeader", "X-API-SOURCE-USER");
		ReflectionTestUtils.setField(delegate, "apiChannelCode", "X-API-CHANNEL-CODE");
		ReflectionTestUtils.setField(delegate, "maskedPan", "X-MASKED-PAN");
		ReflectionTestUtils.setField(delegate, "partySourceNumber", "X-API-PARTY-SOURCE-ID-NUMBER");
		httpHeaders = delegate.createRamlRequestHeaders(new RequestInfo(), accountMapping, params);

		assertNotNull(httpHeaders);
	}

	/**
	 * Test transform response from FD to API -- Mayank.
	 */
	@Test
	public void testTransformResponseFromFDToAPI() {
		Map<String, String> params = new HashMap<>();
		params.put("accountId", "12345");
		params.put("channelId", "BOL");
		params.put(AccountTransactionsFoundationServiceConstants.ACCOUNT_SUBTYPE,AccountTransactionsFoundationServiceConstants.CURRENT_ACCOUNT);
		PlatformAccountTransactionResponse accountTransactionResponse=new PlatformAccountTransactionResponse();
		TransactionList response=new TransactionList();
		Transaction baseType=new Transaction();
		
		
		
		AccountMapping accountMapping = new AccountMapping();
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
		AccountDetails accDet = new AccountDetails();
		accDet.setAccountId("1234");
		accDet.setAccountNSC("US2345");
		accDet.setAccountNumber("1234");
		accDet.setAccountSubType(OBExternalAccountSubType1Code.CURRENTACCOUNT);
		accDetList.add(accDet);
		accountMapping.setAccountDetails(accDetList);
		accountMapping.setTppCID("test");
		accountMapping.setPsuId("test");
		accountMapping.setCorrelationId("test");
		response.getTransactionsList().add(baseType);
		//Mockito.when(delegate.transformResponseFromFDToAPI(any(), any())).thenReturn(accountTransactionResponse);
		delegate.transformResponseFromFDToAPI(response, params);
		//Mockito.when(any()).thenReturn(accountTransactionResponse);
	    Mockito.when(delegate.transformResponseFromFDToAPI(response, params)).thenReturn(new PlatformAccountTransactionResponse());
	    Mockito.when(accountTransactionsFSTransformer.transformAccountTransaction(response, params)).thenReturn(new PlatformAccountTransactionResponse());
	    PlatformAccountTransactionResponse resr = delegate.transformResponseFromFDToAPI(response, params);
		assertNotNull(resr);
	}
	
	@Test
	public void testTransformResponseFromFDToAPICreditCard() {
		Map<String, String> params = new HashMap<>();
		params.put("accountId", "12345");
		params.put("channelId", "BOL");
		params.put(AccountTransactionsFoundationServiceConstants.ACCOUNT_SUBTYPE,AccountTransactionsFoundationServiceConstants.CREDIT_CARD);
		PlatformAccountTransactionResponse accountTransactionResponse=new PlatformAccountTransactionResponse();
		TransactionList response=new TransactionList();
		Transaction baseType=new Transaction();
		
		
		
		AccountMapping accountMapping = new AccountMapping();
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
		AccountDetails accDet = new AccountDetails();
		accDet.setAccountId("1234");
		accDet.setAccountNSC("US2345");
		accDet.setAccountNumber("1234");
		accDet.setAccountSubType(OBExternalAccountSubType1Code.CURRENTACCOUNT);
		accDetList.add(accDet);
		accountMapping.setAccountDetails(accDetList);
		accountMapping.setTppCID("test");
		accountMapping.setPsuId("test");
		accountMapping.setCorrelationId("test");
		response.getTransactionsList().add(baseType);
		//Mockito.when(delegate.transformResponseFromFDToAPI(any(), any())).thenReturn(accountTransactionResponse);
		PlatformAccountTransactionResponse res = delegate.transformResponseFromFDToAPI(response, params);
	//	Mockito.when(any()).thenReturn(accountTransactionResponse);
	    Mockito.when(delegate.transformResponseFromFDToAPI(response, params)).thenReturn(new PlatformAccountTransactionResponse());
	    Mockito.when(accountTransactionsFSTransformer.transformAccountTransaction(response, params)).thenReturn(new PlatformAccountTransactionResponse());
	    PlatformAccountTransactionResponse resr = delegate.transformResponseFromFDToAPI(response, params);
		assertNotNull(resr);
	}
	
	@Test
	public void testGetFoundationServiceURLBaseURLEmpty() {
		when(adapterUtility.retriveFoundationServiceURL("test")).thenReturn(null);
		delegate.getFoundationServiceURL("test", "test", "test","test");
		assertNull(null);
	}
	
	@Test
	public void testGetCreditCardFoundationServiceURL() {

		String accountId = "number";
		String accountSubType = "accountSubType";
		String channelId = "B365";
		String baseURL = "http://localhost:8081/fs-abt-service/services/abt/accounts";
		Mockito.when(adapterUtility.retriveFoundationServiceURL(any())).thenReturn(baseURL);
		String finalURL = "http://localhost:8081/fs-abt-service/services/abt/accounts/cards/transactions";
		String testURL = delegate.getCreditCardFoundationServiceURL(accountSubType, channelId , accountId, baseURL);
		//assertEquals("Test for building URL failed.", finalURL, testURL);
		assertNotNull(testURL);
	}
	
	@Test
	public void testGetCreditCardFoundationServiceURLError() {

		String accountId = "number";
		String accountSubType = "accountSubType";
		String channelId = "B365";
		String baseURL = "http://localhost:8081/fs-abt-service/services/abt/accounts";
		Mockito.when(adapterUtility.retriveFoundationServiceURL(any())).thenReturn(baseURL);
		String finalURL = "http://localhost:8081/fs-abt-service/services/abt/accounts/cards/transactions";
		String testURL = delegate.getCreditCardFoundationServiceURL(accountSubType, channelId , accountId, baseURL);
		//assertEquals("Test for building URL failed.", finalURL, testURL);
		assertNotNull(testURL);
	}

	@Test(expected = AdapterException.class)
	public void testGetCreditCardFoundationServiceBaseURLNULL() {

		String accountId = "number";
		String accountSubType = "accountSubType";
		String channelId = "B365";
		String baseURL = null;
		Mockito.when(adapterUtility.retriveFoundationServiceURL(any())).thenThrow(AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.BAD_REQUEST));
		String finalURL = "http://localhost:8081/fs-abt-service/services/abt/accounts/cards/transactions";
		String testURL = delegate.getCreditCardFoundationServiceURL(accountSubType, channelId , accountId, null);
		//assertEquals("Test for building URL failed.", finalURL, testURL);
//		assertNotNull(testURL);
	}
	@Test(expected = AdapterException.class)
	public void testGetCreditCardFoundationServiceCreditNumberNULL() {
		String plApplld="23424";
		String creditCardNumber=null;
//		String accountId = "number";
//		String accountSubType = "accountSubType";
		String channelId = "B365";
		String baseURL = "http://localhost:8081/fs-abt-service/services/abt/accounts";
		Mockito.when(adapterUtility.retriveFoundationServiceURL(any())).thenThrow(AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.NO_ACCOUNT_DATA_FOUND));
		String finalURL = "http://localhost:8081/fs-abt-service/services/abt/accounts/cards/transactions";
		String testURL = delegate.getCreditCardFoundationServiceURL(null, creditCardNumber , channelId, baseURL);
		//assertEquals("Test for building URL failed.", finalURL, testURL);
//		assertNotNull(testURL);
	}
	
	@Test(expected = AdapterException.class)
	public void testGetCreditCardFoundationServicePlAppIdNULL() {
		String plApplld=null;
		String creditCardNumber="23424";
		String channelId = "B365";
		String version = "v1.0";
		String baseURL = "http://localhost:8081/fs-abt-service/services/abt/accounts";
		Mockito.when(adapterUtility.retriveFoundationServiceURL(any())).thenThrow(AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.NO_ACCOUNT_DATA_FOUND));
		String finalURL = "http://localhost:8081/fs-abt-service/services/abt/accounts/cards/transactions";
		String testURL = delegate.getCreditCardFoundationServiceURL(version, creditCardNumber, plApplld, baseURL);
		
	}

}

