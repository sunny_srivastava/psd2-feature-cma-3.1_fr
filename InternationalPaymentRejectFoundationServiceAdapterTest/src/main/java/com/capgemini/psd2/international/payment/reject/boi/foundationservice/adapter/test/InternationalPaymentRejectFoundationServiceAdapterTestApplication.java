package com.capgemini.psd2.international.payment.reject.boi.foundationservice.adapter.test;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.psd2.foundationservice.international.payment.reject.boi.adapter.InternationalPaymentRejectFoundationServiceAdapter;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.pisp.domain.CustomIPaymentConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomIPaymentConsentsPOSTResponse;
import com.capgemini.psd2.pisp.enums.PaymentTypeEnum;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;

/**
 * Hello world!
 *
 */

@SpringBootApplication
@ComponentScan("com.capgemini.psd2")
public class InternationalPaymentRejectFoundationServiceAdapterTestApplication {
	public static void main(String[] args) {
		SpringApplication.run(InternationalPaymentRejectFoundationServiceAdapterTestApplication.class, args);
	}
}

@RestController
@ResponseBody
class TestInternationalPaymentRejectFoundationServiceAdapterTestApplication {

	@Autowired
	private InternationalPaymentRejectFoundationServiceAdapter iPaymentConsentsFoundationServiceAdapter;

	@RequestMapping(value = "/testInternationalPaymentConsentsUpdate/{consentId}", method = RequestMethod.POST, produces = {
			MediaType.APPLICATION_JSON_UTF8_VALUE })

	public CustomIPaymentConsentsPOSTResponse internationalPaymentConsentsCreate(
			@PathVariable("consentId") String consentId ) {
		CustomPaymentStageIdentifiers customPaymentStageIdentifiers = new CustomPaymentStageIdentifiers();
		customPaymentStageIdentifiers.setPaymentConsentId(consentId);
		customPaymentStageIdentifiers.setPaymentSetupVersion("3.0");
		customPaymentStageIdentifiers.setPaymentTypeEnum(PaymentTypeEnum.INTERNATIONAL_PAY);
		Map<String, String> params = new HashMap<String, String>();
		params.put("consentFlowType", "PISP");
		params.put("x-channel-id", "BOL");
		params.put("x-user-id", "BOI999");
		params.put("X-BOI-PLATFORM", "platform");
		params.put("x-fapi-interaction-id", "87654321");
		params.put("X-API-SOURCE-SYSTEM", "sdsa");
		params.put("tenant_id", "BOIUK");
		params.put("X-API-CHANNEL-BRAND", "channelBrand");
		params.put(PSD2Constants.USER_IN_REQ_HEADER,"45");
		params.put(PSD2Constants.CORRELATION_REQ_HEADER,"145");
		params.put(PSD2Constants.CHANNEL_IN_REQ_HEADER,"42345");
		params.put(PSD2Constants.TENANT_ID,"BOIROI");
		//X-BOI-CHANNEL,tenant_id

		return iPaymentConsentsFoundationServiceAdapter.createStagingInternationalPaymentRejectConsents(
				customPaymentStageIdentifiers, params);
	}
}