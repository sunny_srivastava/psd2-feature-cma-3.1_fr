package com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.filterartion;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.constants.CustomerAccountsFilterFoundationServiceConstants;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.AccountEntitlements;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.PartyEntitlements;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.PISP.AccountEntitlements2;
import com.capgemini.psd2.utilities.NullCheckUtils;

public class PermissionFilter implements FilterationChain {

	private FilterationChain nextInChain;
	
	@Override
	public void setNext(FilterationChain next) {
		nextInChain = next;

	}

	@Override
	public List<AccountEntitlements> process(List<AccountEntitlements> accounts, PartyEntitlements partyEntitlements, String consentFlowType,
			Map<String, Map<String, List<String>>> accountFiltering) {	
		boolean partyEntitlementFalg = false;
		List<AccountEntitlements> accountList = new ArrayList<>();
		Map<String, List<String>> permissionList = accountFiltering.get(CustomerAccountsFilterFoundationServiceConstants.PERMISSION);
		
		if (!accounts.isEmpty() || accounts.size()>0) {
			if(permissionList != null && !permissionList.isEmpty()){
				List<String> partyPermisssion = (ArrayList<String>) permissionList.get("partyPermisssion");
				if(!NullCheckUtils.isNullOrEmpty(partyPermisssion) && consentFlowType.equalsIgnoreCase(CustomerAccountsFilterFoundationServiceConstants.AISP)){
					if(partyEntitlements.getEntitlements().stream().anyMatch(pper -> partyPermisssion.contains(pper.trim())))
						partyEntitlementFalg = true;
					
					List<String> permissionForFlow = (ArrayList<String>) permissionList.get(consentFlowType);					
					for(AccountEntitlements account : accounts){
						boolean accountEntitlementFalg =  account.getEntitlements().stream().anyMatch(per -> permissionForFlow.contains(per.trim()));
						if(accountEntitlementFalg || partyEntitlementFalg)
							accountList.add(account);
					}

				}else if(!NullCheckUtils.isNullOrEmpty(partyPermisssion) && consentFlowType.equalsIgnoreCase(CustomerAccountsFilterFoundationServiceConstants.CISP)){
					List<String> excludeCispPermissions = (ArrayList<String>) permissionList.get("excludeCispPermissions");
					List<String> permissionForFlow = (ArrayList<String>) permissionList.get(consentFlowType);					
					for(AccountEntitlements account : accounts){
						boolean accountEntitlementFalg =  account.getEntitlements().stream().anyMatch(per -> permissionForFlow.contains(per.trim()));
						boolean accEntFlag =  account.getEntitlements().stream().anyMatch(accEnt->excludeCispPermissions.contains(accEnt.trim()));					
						if(accountEntitlementFalg && !accEntFlag ) 
							accountList.add(account);
					}
				}				
			}else{
				return accountList;
			}
		}
		return accountList;
	}
	
	@Override
	public List<AccountEntitlements2> processPISP(List<AccountEntitlements2> accounts, com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.PISP.PartyEntitlements partyEntitlements, String consentFlowType,
			Map<String, Map<String, List<String>>> accountFiltering) {	
		boolean partyEntitlementFalg = false;
		List<AccountEntitlements2> accountList = new ArrayList<>();
		Map<String, List<String>> permissionList = accountFiltering.get(CustomerAccountsFilterFoundationServiceConstants.PERMISSION);
		
		if (!accounts.isEmpty() || accounts.size()>0) {
			if(permissionList != null && !permissionList.isEmpty()){
				List<String> partyPermisssion = (ArrayList<String>) permissionList.get("partyPermisssion");
				if(!NullCheckUtils.isNullOrEmpty(partyPermisssion) && consentFlowType.equalsIgnoreCase(CustomerAccountsFilterFoundationServiceConstants.AISP)){
					if(partyEntitlements.getEntitlements().stream().anyMatch(pper -> partyPermisssion.contains(pper.trim())))
						partyEntitlementFalg = true;
				}
				List<String> permissionForFlow = (ArrayList<String>) permissionList.get(consentFlowType);
				for(AccountEntitlements2 account : accounts){
					boolean accountEntitlementFalg =  account.getEntitlements().stream().anyMatch(per -> permissionForFlow.contains(per.trim()));
					if(accountEntitlementFalg || partyEntitlementFalg)
						accountList.add(account);
				}
			}else{
				return accountList;
			}
		}
		return accountList;
	}
}
