
package com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.test;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.CustomerAccountsFilterFoundationServiceAdapter;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.client.CustomerAccountsFilterFoundationServiceClient;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.constants.CustomerAccountsFilterFoundationServiceConstants;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.delegate.CustomerAccountsFilterFoundationServiceDelegate;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnt;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnts;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.AccountEntitlements;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.DigitalUser;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.DigitalUserProfile;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.PartyBasicInformation;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.PartyEntitlements;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.PISP.AccountEntitlements2;
import com.capgemini.psd2.logger.PSD2Constants;

@RunWith(SpringJUnit4ClassRunner.class)
public class CustomerAccountsFilterFoundationServiceAdapterTest {

	@InjectMocks
	CustomerAccountsFilterFoundationServiceAdapter adapter;
	
	@Mock
	CustomerAccountsFilterFoundationServiceDelegate customerAccountsFilterFoundationServiceDelegate;
	
	@Mock
	CustomerAccountsFilterFoundationServiceClient customerAccountsFilterFoundationServiceClient;
	
	@Before
	public void setUp(){
		MockitoAnnotations.initMocks(this);
	}
	
	
	@Test
	public void contextLoads() {
	}
	
	
	@Test
	public void testRetrieveCustomerAccountListAISP(){
		Map<String, String> params = new HashMap<>();
		params.put(PSD2Constants.CONSENT_FLOW_TYPE, "AISP");
		params.put(PSD2Constants.TENANT_ID,"BOIROI");
		DigitalUserProfile diprofile=new DigitalUserProfile();
		List<AccountEntitlements> listAcc=new ArrayList<AccountEntitlements>();
		PartyEntitlements partyEntitlements = new PartyEntitlements();
		partyEntitlements.setEntitlements(Arrays.asList("AISP"));
		DigitalUser digitalUser = new DigitalUser();
		PartyBasicInformation partyBasicInformation = new PartyBasicInformation(); 
		partyBasicInformation.setPartyActiveIndicator(Boolean.TRUE);
		digitalUser.setDigitalUserLockedOutIndicator(Boolean.FALSE);
		diprofile.setPartyEntitlements(partyEntitlements);
		diprofile.setDigitalUser(digitalUser);
		diprofile.setPartyInformation(partyBasicInformation);
		listAcc.add(new AccountEntitlements());
		diprofile.setAccountEntitlements(listAcc);
		HttpHeaders headers = new HttpHeaders();
		Accnts accounts = new Accnts();
		Accnt accnt = new Accnt();
		accnt.setAccountNSC("123");
		accounts.getAccount().add(accnt);
		when(customerAccountsFilterFoundationServiceDelegate.createRequestHeadersAISP(anyObject())).thenReturn(headers);
		when(customerAccountsFilterFoundationServiceDelegate.getFoundationServiceURLAISP(anyObject(), anyObject())).thenReturn("test");
		Mockito.when(customerAccountsFilterFoundationServiceClient.restTransportForCustomerAccountProfile(anyObject(),anyObject(), anyObject(), anyObject())).thenReturn(diprofile);
		Mockito.when(customerAccountsFilterFoundationServiceDelegate.getFilteredAccounts(anyObject(),anyObject(), anyObject())).thenReturn(listAcc);
		DigitalUserProfile accnts = adapter.retrieveCustomerAccountList("test", params);
		assertNotNull(accnts);
		params.put(PSD2Constants.USER_ID, "x-user-id");
		params.put(PSD2Constants.CORRELATION_ID, "x-fapi-interaction-id");
		params.put(PSD2Constants.CHANNEL_IN_REQ_HEADER, "test");
		params.put(CustomerAccountsFilterFoundationServiceConstants.ACCOUNTNSCNUMBER, "test");
		params.put(CustomerAccountsFilterFoundationServiceConstants.ACCOUNTNUMBER, "test");
		accnts = adapter.retrieveCustomerAccountList("test", params);
		assertNotNull(accnts);	
	  }
	
	@Test(expected = AdapterException.class)
	public void testRetrieveCustomerAccountListAISPEntitlmentChange(){
		Map<String, String> params = new HashMap<>();
		params.put(PSD2Constants.CONSENT_FLOW_TYPE, "AISP");
		params.put(PSD2Constants.TENANT_ID,"BOIROI");
		DigitalUserProfile diprofile=new DigitalUserProfile();
		List<AccountEntitlements> listAcc=new ArrayList<AccountEntitlements>();
		PartyEntitlements partyEntitlements = new PartyEntitlements();
		partyEntitlements.setEntitlements(Arrays.asList("CISP"));
		DigitalUser digitalUser = new DigitalUser();
		PartyBasicInformation partyBasicInformation = new PartyBasicInformation(); 
		partyBasicInformation.setPartyActiveIndicator(Boolean.TRUE);
		digitalUser.setDigitalUserLockedOutIndicator(Boolean.FALSE);
		diprofile.setPartyEntitlements(partyEntitlements);
		diprofile.setDigitalUser(digitalUser);
		diprofile.setPartyInformation(partyBasicInformation);
		listAcc.add(new AccountEntitlements());
		diprofile.setAccountEntitlements(listAcc);
		HttpHeaders headers = new HttpHeaders();
		Accnts accounts = new Accnts();
		Accnt accnt = new Accnt();
		accnt.setAccountNSC("123");
		accounts.getAccount().add(accnt);
		when(customerAccountsFilterFoundationServiceDelegate.createRequestHeadersAISP(anyObject())).thenReturn(headers);
		when(customerAccountsFilterFoundationServiceDelegate.getFoundationServiceURLAISP(anyObject(), anyObject())).thenReturn("test");
		Mockito.when(customerAccountsFilterFoundationServiceClient.restTransportForCustomerAccountProfile(anyObject(),anyObject(), anyObject(), anyObject())).thenReturn(diprofile);
		Mockito.when(customerAccountsFilterFoundationServiceDelegate.getFilteredAccounts(anyObject(),anyObject(), anyObject())).thenReturn(listAcc);
		DigitalUserProfile accnts = adapter.retrieveCustomerAccountList("test", params);
		assertNotNull(accnts);
		params.put(PSD2Constants.USER_ID, "x-user-id");
		params.put(PSD2Constants.CORRELATION_ID, "x-fapi-interaction-id");
		params.put(PSD2Constants.CHANNEL_IN_REQ_HEADER, "test");
		params.put(CustomerAccountsFilterFoundationServiceConstants.ACCOUNTNSCNUMBER, "test");
		params.put(CustomerAccountsFilterFoundationServiceConstants.ACCOUNTNUMBER, "test");
		accnts = adapter.retrieveCustomerAccountList("test", params);
		assertNotNull(accnts);	
	  }
	
	@Test(expected = AdapterException.class)
	public void testRetrieveCustomerAccountListAISPDigitalUserProfileNullException(){
		Map<String, String> params = new HashMap<>();
		params.put(PSD2Constants.CONSENT_FLOW_TYPE, "AISP");
		params.put(PSD2Constants.TENANT_ID,"BOIUK");
		DigitalUserProfile diprofile=new DigitalUserProfile();
		List<AccountEntitlements> listAcc=new ArrayList<AccountEntitlements>();
		PartyEntitlements partyEntitlements = new PartyEntitlements();
		partyEntitlements.setEntitlements(Arrays.asList("AISP"));
		DigitalUser digitalUser = new DigitalUser();
		PartyBasicInformation partyBasicInformation = new PartyBasicInformation(); 
		partyBasicInformation.setPartyActiveIndicator(Boolean.TRUE);
		digitalUser.setDigitalUserLockedOutIndicator(Boolean.FALSE);		
		diprofile.setPartyEntitlements(partyEntitlements);
		diprofile.setDigitalUser(digitalUser);
		diprofile.setPartyInformation(partyBasicInformation);
		listAcc.add(new AccountEntitlements());
		diprofile.setAccountEntitlements(listAcc);		
		HttpHeaders headers = new HttpHeaders();
		Accnts accounts = new Accnts();
		Accnt accnt = new Accnt();
		accnt.setAccountNSC("123");
		accounts.getAccount().add(accnt);
		when(customerAccountsFilterFoundationServiceDelegate.createRequestHeadersAISP(anyObject())).thenReturn(headers);
		when(customerAccountsFilterFoundationServiceDelegate.getFoundationServiceURLAISP(anyObject(), anyObject())).thenReturn("test");
		Mockito.when(customerAccountsFilterFoundationServiceClient.restTransportForCustomerAccountProfile(anyObject(),anyObject(), anyObject(), anyObject())).thenReturn(null);
		Mockito.when(customerAccountsFilterFoundationServiceDelegate.getFilteredAccounts(anyObject(),anyObject(), anyObject())).thenReturn(listAcc);
		DigitalUserProfile accnts = adapter.retrieveCustomerAccountList("test", params);
		assertNotNull(accnts);
		params.put(PSD2Constants.USER_ID, "x-user-id");
		params.put(PSD2Constants.CORRELATION_ID, "x-fapi-interaction-id");
		params.put(PSD2Constants.CHANNEL_IN_REQ_HEADER, "test");
		params.put(CustomerAccountsFilterFoundationServiceConstants.ACCOUNTNSCNUMBER, "test");
		params.put(CustomerAccountsFilterFoundationServiceConstants.ACCOUNTNUMBER, "test");
		params.put(PSD2Constants.TENANT_ID,"BOIUK");
		accnts = adapter.retrieveCustomerAccountList("test", params);
		assertNotNull(accnts);	
	}
	
	@Test
	public void testRetrieveCustomerAccountListPISP(){
		Map<String, String> params = new HashMap<>();
		params.put(PSD2Constants.CONSENT_FLOW_TYPE, "AISP");
		params.put(PSD2Constants.TENANT_ID,"BOIROI");
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.PISP.DigitalUserProfile diprofile=new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.PISP.DigitalUserProfile();
		List<AccountEntitlements2> listAcc=new ArrayList<AccountEntitlements2>();
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.PISP.PartyEntitlements partyEntitlements = new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.PISP.PartyEntitlements();
		partyEntitlements.setEntitlements(Arrays.asList("AISP"));
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.PISP.DigitalUser digitalUser = new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.PISP.DigitalUser();
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.PISP.PartyBasicInformation partyBasicInformation = new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.PISP.PartyBasicInformation(); 
		partyBasicInformation.setPartyActiveIndicator(Boolean.TRUE);
		digitalUser.setDigitalUserLockedOutIndicator(Boolean.FALSE);
		diprofile.setPartyEntitlements(partyEntitlements);
		diprofile.setDigitalUser(digitalUser);
		diprofile.setPartyInformation(partyBasicInformation);
		listAcc.add(new AccountEntitlements2());
		diprofile.setAccountBalanceEntitlements(listAcc);
		HttpHeaders headers = new HttpHeaders();
		Accnts accounts = new Accnts();
		Accnt accnt = new Accnt();
		accnt.setAccountNSC("123");
		accounts.getAccount().add(accnt);
		params.put(PSD2Constants.USER_ID, "x-user-id");
		params.put(PSD2Constants.CORRELATION_ID, "x-fapi-interaction-id");
		when(customerAccountsFilterFoundationServiceDelegate.createRequestHeadersPISP(anyObject())).thenReturn(headers);
		when(customerAccountsFilterFoundationServiceDelegate.getFoundationServiceURLPISP(anyObject(), anyObject())).thenReturn("test");
		Mockito.when(customerAccountsFilterFoundationServiceClient.restTransportForCustomerAccountProfilePISP(anyObject(),anyObject(), anyObject(), anyObject())).thenReturn(diprofile);
		Mockito.when(customerAccountsFilterFoundationServiceDelegate.getFilteredAccountsPISP(anyObject(),anyObject(), anyObject())).thenReturn(listAcc);
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.PISP.DigitalUserProfile accnts = adapter.retrieveCustomerAccountListPISP("test", params);
		assertNotNull(accnts);
		params.put(PSD2Constants.USER_ID, "x-user-id");
		params.put(PSD2Constants.CORRELATION_ID, "x-fapi-interaction-id");
		params.put(PSD2Constants.CHANNEL_IN_REQ_HEADER, "test");
		params.put(CustomerAccountsFilterFoundationServiceConstants.ACCOUNTNSCNUMBER, "test");
		params.put(CustomerAccountsFilterFoundationServiceConstants.ACCOUNTNUMBER, "test");
		params.put(PSD2Constants.TENANT_ID,"BOIUK");
		accnts = adapter.retrieveCustomerAccountListPISP("test", params);
		assertNotNull(accnts);	
	  }
	
	@Test(expected = AdapterException.class)
	public void testRetrieveCustomerAccountListPISPDigitalUserProfileNullException(){
		Map<String, String> params = new HashMap<>();
		params.put(PSD2Constants.CONSENT_FLOW_TYPE, "AISP");
		params.put(PSD2Constants.TENANT_ID,"BOIROI");
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.PISP.DigitalUserProfile diprofile=new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.PISP.DigitalUserProfile();
		List<AccountEntitlements2> listAcc=new ArrayList<AccountEntitlements2>();
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.PISP.PartyEntitlements partyEntitlements = new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.PISP.PartyEntitlements();
		partyEntitlements.setEntitlements(Arrays.asList("AISP"));
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.PISP.DigitalUser digitalUser = new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.PISP.DigitalUser();
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.PISP.PartyBasicInformation partyBasicInformation = new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.PISP.PartyBasicInformation(); 
		partyBasicInformation.setPartyActiveIndicator(Boolean.TRUE);
		digitalUser.setDigitalUserLockedOutIndicator(Boolean.FALSE);
		diprofile.setPartyEntitlements(partyEntitlements);
		diprofile.setDigitalUser(digitalUser);
		diprofile.setPartyInformation(partyBasicInformation);
		listAcc.add(new AccountEntitlements2());
		diprofile.setAccountBalanceEntitlements(listAcc);
		HttpHeaders headers = new HttpHeaders();
		Accnts accounts = new Accnts();
		Accnt accnt = new Accnt();
		accnt.setAccountNSC("123");
		accounts.getAccount().add(accnt);
		params.put(PSD2Constants.USER_ID, "x-user-id");
		params.put(PSD2Constants.CORRELATION_ID, "");
		when(customerAccountsFilterFoundationServiceDelegate.createRequestHeadersPISP(anyObject())).thenReturn(headers);
		when(customerAccountsFilterFoundationServiceDelegate.getFoundationServiceURLPISP(anyObject(), anyObject())).thenReturn("test");
		Mockito.when(customerAccountsFilterFoundationServiceClient.restTransportForCustomerAccountProfilePISP(anyObject(),anyObject(), anyObject(), anyObject())).thenReturn(null);
		Mockito.when(customerAccountsFilterFoundationServiceDelegate.getFilteredAccountsPISP(anyObject(),anyObject(), anyObject())).thenReturn(listAcc);
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.PISP.DigitalUserProfile accnts = adapter.retrieveCustomerAccountListPISP("test", params);
		assertNotNull(accnts);
		params.put(PSD2Constants.USER_ID, "x-user-id");
		params.put(PSD2Constants.CORRELATION_ID, "x-fapi-interaction-id");
		params.put(PSD2Constants.CHANNEL_IN_REQ_HEADER, "test");
		params.put(CustomerAccountsFilterFoundationServiceConstants.ACCOUNTNSCNUMBER, "test");
		params.put(CustomerAccountsFilterFoundationServiceConstants.ACCOUNTNUMBER, "test");
		params.put(PSD2Constants.TENANT_ID,"BOIUK");
		accnts = adapter.retrieveCustomerAccountListPISP("test", params);
		assertNotNull(accnts);	
	  }
	
	@Test(expected = AdapterException.class)
	public void retrieveCustomerAccountListPISPParamsNull() {
		Map<String, String> params = null;
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.PISP.DigitalUserProfile digitalUserProfile = adapter.retrieveCustomerAccountListPISP("test", params);
		assertNotNull(digitalUserProfile);
	}
	
	@Test(expected = AdapterException.class)
	public void retrieveCustomerAccountListPISPUserIdNull() {
		Map<String, String> params = new HashMap();
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.PISP.DigitalUserProfile digitalUserProfile = adapter.retrieveCustomerAccountListPISP(null, params);
		assertNotNull(digitalUserProfile);
	}
	
	@Test(expected = AdapterException.class)
	public void testRetrieveCustomerAccountListPISPEntitlementChange(){
		Map<String, String> params = new HashMap<>();
		params.put(PSD2Constants.CONSENT_FLOW_TYPE, "PISP");
		params.put(PSD2Constants.TENANT_ID,"BOIROI");
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.PISP.DigitalUserProfile diprofile=new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.PISP.DigitalUserProfile();
		List<AccountEntitlements2> listAcc=new ArrayList<AccountEntitlements2>();
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.PISP.PartyEntitlements partyEntitlements = new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.PISP.PartyEntitlements();
		partyEntitlements.setEntitlements(Arrays.asList("CISP"));
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.PISP.DigitalUser digitalUser = new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.PISP.DigitalUser();
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.PISP.PartyBasicInformation partyBasicInformation = new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.PISP.PartyBasicInformation(); 
		partyBasicInformation.setPartyActiveIndicator(Boolean.TRUE);
		digitalUser.setDigitalUserLockedOutIndicator(Boolean.FALSE);
		diprofile.setPartyEntitlements(partyEntitlements);
		diprofile.setDigitalUser(digitalUser);
		diprofile.setPartyInformation(partyBasicInformation);
		listAcc.add(new AccountEntitlements2());
		diprofile.setAccountBalanceEntitlements(listAcc);
		HttpHeaders headers = new HttpHeaders();
		Accnts accounts = new Accnts();
		Accnt accnt = new Accnt();
		accnt.setAccountNSC("123");
		accounts.getAccount().add(accnt);
		params.put(PSD2Constants.USER_ID, "x-user-id");
		params.put(PSD2Constants.CORRELATION_ID, "x-fapi-interaction-id");
		when(customerAccountsFilterFoundationServiceDelegate.createRequestHeadersPISP(anyObject())).thenReturn(headers);
		when(customerAccountsFilterFoundationServiceDelegate.getFoundationServiceURLPISP(anyObject(), anyObject())).thenReturn("test");
		Mockito.when(customerAccountsFilterFoundationServiceClient.restTransportForCustomerAccountProfilePISP(anyObject(),anyObject(), anyObject(), anyObject())).thenReturn(diprofile);
		Mockito.when(customerAccountsFilterFoundationServiceDelegate.getFilteredAccountsPISP(anyObject(),anyObject(), anyObject())).thenReturn(listAcc);
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.PISP.DigitalUserProfile accnts = adapter.retrieveCustomerAccountListPISP("test", params);
		assertNotNull(accnts);
		params.put(PSD2Constants.USER_ID, "x-user-id");
		params.put(PSD2Constants.CORRELATION_ID, "x-fapi-interaction-id");
		params.put(PSD2Constants.CHANNEL_IN_REQ_HEADER, "test");
		params.put(CustomerAccountsFilterFoundationServiceConstants.ACCOUNTNSCNUMBER, "test");
		params.put(CustomerAccountsFilterFoundationServiceConstants.ACCOUNTNUMBER, "test");
		params.put(PSD2Constants.TENANT_ID,"BOIUK");
		accnts = adapter.retrieveCustomerAccountListPISP("test", params);
		assertNotNull(accnts);	
	  }
	
	@Test
	public void testRetrieveCustomerAccountListCISP(){
		Map<String, String> params = new HashMap<>();
		params.put(PSD2Constants.CONSENT_FLOW_TYPE, "CISP");
		params.put(PSD2Constants.TENANT_ID,"BOIROI");
		DigitalUserProfile diprofile=new DigitalUserProfile();
		List<AccountEntitlements> listAcc=new ArrayList<AccountEntitlements>();
		PartyEntitlements partyEntitlements = new PartyEntitlements();
		partyEntitlements.setEntitlements(Arrays.asList("CISP"));
		DigitalUser digitalUser = new DigitalUser();
		PartyBasicInformation partyBasicInformation = new PartyBasicInformation(); 
		partyBasicInformation.setPartyActiveIndicator(Boolean.TRUE);
		digitalUser.setDigitalUserLockedOutIndicator(Boolean.FALSE);
		diprofile.setPartyEntitlements(partyEntitlements);
		diprofile.setDigitalUser(digitalUser);
		diprofile.setPartyInformation(partyBasicInformation);
		listAcc.add(new AccountEntitlements());
		diprofile.setAccountEntitlements(listAcc);
		HttpHeaders headers = new HttpHeaders();
		Accnts accounts = new Accnts();
		Accnt accnt = new Accnt();
		accnt.setAccountNSC("123");
		accounts.getAccount().add(accnt);
		when(customerAccountsFilterFoundationServiceDelegate.createRequestHeadersAISP(anyObject())).thenReturn(headers);
		when(customerAccountsFilterFoundationServiceDelegate.getFoundationServiceURLAISP(anyObject(), anyObject())).thenReturn("test");
		Mockito.when(customerAccountsFilterFoundationServiceClient.restTransportForCustomerAccountProfile(anyObject(),anyObject(), anyObject(), anyObject())).thenReturn(diprofile);
		Mockito.when(customerAccountsFilterFoundationServiceDelegate.getFilteredAccounts(anyObject(),anyObject(), anyObject())).thenReturn(listAcc);
		DigitalUserProfile accnts = adapter.retrieveCustomerAccountListCISP("test", params);
		assertNotNull(accnts);
		params.put(PSD2Constants.USER_ID, "x-user-id");
		params.put(PSD2Constants.CORRELATION_ID, "x-fapi-interaction-id");
		params.put(PSD2Constants.CHANNEL_IN_REQ_HEADER, "test");
		params.put(CustomerAccountsFilterFoundationServiceConstants.ACCOUNTNSCNUMBER, "test");
		params.put(CustomerAccountsFilterFoundationServiceConstants.ACCOUNTNUMBER, "test");
		params.put(PSD2Constants.TENANT_ID,"BOIUK");
		accnts = adapter.retrieveCustomerAccountListCISP("test", params);
		assertNotNull(accnts);	
	  }
	
	@Test(expected = AdapterException.class)
	public void testRetrieveCustomerAccountListCISPDigitalUserProfileNullException(){
		Map<String, String> params = new HashMap<>();
		params.put(PSD2Constants.CONSENT_FLOW_TYPE, "CISP");
		params.put(PSD2Constants.TENANT_ID,"BOIROI");
		DigitalUserProfile diprofile=new DigitalUserProfile();
		List<AccountEntitlements> listAcc=new ArrayList<AccountEntitlements>();
		PartyEntitlements partyEntitlements = new PartyEntitlements();
		partyEntitlements.setEntitlements(Arrays.asList("CISP"));
		DigitalUser digitalUser = new DigitalUser();
		PartyBasicInformation partyBasicInformation = new PartyBasicInformation(); 
		partyBasicInformation.setPartyActiveIndicator(Boolean.TRUE);
		digitalUser.setDigitalUserLockedOutIndicator(Boolean.FALSE);
		diprofile.setPartyEntitlements(partyEntitlements);
		diprofile.setDigitalUser(digitalUser);
		diprofile.setPartyInformation(partyBasicInformation);
		listAcc.add(new AccountEntitlements());
		diprofile.setAccountEntitlements(listAcc);
		HttpHeaders headers = new HttpHeaders();
		Accnts accounts = new Accnts();
		Accnt accnt = new Accnt();
		accnt.setAccountNSC("123");
		accounts.getAccount().add(accnt);
		when(customerAccountsFilterFoundationServiceDelegate.createRequestHeadersAISP(anyObject())).thenReturn(headers);
		when(customerAccountsFilterFoundationServiceDelegate.getFoundationServiceURLAISP(anyObject(), anyObject())).thenReturn("test");
		Mockito.when(customerAccountsFilterFoundationServiceClient.restTransportForCustomerAccountProfile(anyObject(),anyObject(), anyObject(), anyObject())).thenReturn(null);
		Mockito.when(customerAccountsFilterFoundationServiceDelegate.getFilteredAccounts(anyObject(),anyObject(), anyObject())).thenReturn(listAcc);
		DigitalUserProfile accnts = adapter.retrieveCustomerAccountListCISP("test", params);
		assertNotNull(accnts);
		params.put(PSD2Constants.USER_ID, "x-user-id");
		params.put(PSD2Constants.CORRELATION_ID, "x-fapi-interaction-id");
		params.put(PSD2Constants.CHANNEL_IN_REQ_HEADER, "test");
		params.put(CustomerAccountsFilterFoundationServiceConstants.ACCOUNTNSCNUMBER, "test");
		params.put(CustomerAccountsFilterFoundationServiceConstants.ACCOUNTNUMBER, "test");
		accnts = adapter.retrieveCustomerAccountList("test", params);
		assertNotNull(accnts);	
	  }

	@Test(expected = AdapterException.class)
	public void testRetrieveCustomerAccountListCISPEntitlementChange(){
		Map<String, String> params = new HashMap<>();
		params.put(PSD2Constants.CONSENT_FLOW_TYPE, "CISP");
		params.put(PSD2Constants.TENANT_ID,"BOIROI");
		DigitalUserProfile diprofile=new DigitalUserProfile();
		List<AccountEntitlements> listAcc=new ArrayList<AccountEntitlements>();
		PartyEntitlements partyEntitlements = new PartyEntitlements();
		partyEntitlements.setEntitlements(Arrays.asList("AISP"));
		DigitalUser digitalUser = new DigitalUser();
		PartyBasicInformation partyBasicInformation = new PartyBasicInformation(); 
		partyBasicInformation.setPartyActiveIndicator(Boolean.TRUE);
		digitalUser.setDigitalUserLockedOutIndicator(Boolean.FALSE);
		diprofile.setPartyEntitlements(partyEntitlements);
		diprofile.setDigitalUser(digitalUser);
		diprofile.setPartyInformation(partyBasicInformation);
		listAcc.add(new AccountEntitlements());
		diprofile.setAccountEntitlements(listAcc);
		HttpHeaders headers = new HttpHeaders();
		Accnts accounts = new Accnts();
		Accnt accnt = new Accnt();
		accnt.setAccountNSC("123");
		accounts.getAccount().add(accnt);
		when(customerAccountsFilterFoundationServiceDelegate.createRequestHeadersAISP(anyObject())).thenReturn(headers);
		when(customerAccountsFilterFoundationServiceDelegate.getFoundationServiceURLAISP(anyObject(), anyObject())).thenReturn("test");
		Mockito.when(customerAccountsFilterFoundationServiceClient.restTransportForCustomerAccountProfile(anyObject(),anyObject(), anyObject(), anyObject())).thenReturn(diprofile);
		Mockito.when(customerAccountsFilterFoundationServiceDelegate.getFilteredAccounts(anyObject(),anyObject(), anyObject())).thenReturn(listAcc);
		DigitalUserProfile accnts = adapter.retrieveCustomerAccountListCISP("test", params);
		assertNotNull(accnts);
		params.put(PSD2Constants.USER_ID, "x-user-id");
		params.put(PSD2Constants.CORRELATION_ID, "x-fapi-interaction-id");
		params.put(PSD2Constants.CHANNEL_IN_REQ_HEADER, "test");
		params.put(CustomerAccountsFilterFoundationServiceConstants.ACCOUNTNSCNUMBER, "test");
		params.put(CustomerAccountsFilterFoundationServiceConstants.ACCOUNTNUMBER, "test");
		params.put(PSD2Constants.TENANT_ID,"BOIUK");
		accnts = adapter.retrieveCustomerAccountListCISP("test", params);
		assertNotNull(accnts);	
	  }
	
	
	@Test(expected = AdapterException.class)
	public void testRetrieveCustomerAccountListCISPParamsNull(){
		Map<String, String> params = null;
		adapter.retrieveCustomerAccountListCISP("test", params);
	}
	
	@Test(expected = AdapterException.class)
	public void testRetrieveCustomerAccountListCISPUserIdNull(){
		Map<String, String> params = new HashMap();
		adapter.retrieveCustomerAccountListCISP(null, params);
	}
	
}
