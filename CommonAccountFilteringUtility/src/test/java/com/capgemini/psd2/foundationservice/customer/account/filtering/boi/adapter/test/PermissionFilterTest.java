package com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.test;

import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.filterartion.FilterationChain;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.filterartion.PermissionFilter;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.Account;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.AccountEntitlements;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.DigitalUser;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.PartyBasicInformation;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.PartyEntitlements;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.PISP.AccountEntitlements2;

@RunWith(SpringJUnit4ClassRunner.class)
public class PermissionFilterTest {


	@InjectMocks
	private PermissionFilter permissionFilter;

	@Mock
	private FilterationChain nextInChain;

	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testAccountTypeFilterProcessForPartyPermisssion() {
		List<AccountEntitlements> acenList =new ArrayList<AccountEntitlements>();
		Account acc=new Account();
		Account acc1=new Account();
		AccountEntitlements acen = new AccountEntitlements();
		acen.setAccount(acc);
		acen.setAccount(acc1);
		acenList.add(acen);
		Map<String, List<String>> permissionList =new HashMap<String, List<String>>();
		List<String> li=new  ArrayList<String>();
		permissionList.put("1", li);
		String consentFlowType="AISP";
		PartyEntitlements partyEntitlements = new PartyEntitlements();
		partyEntitlements.setEntitlements(Arrays.asList("AISP"));
		DigitalUser digitalUser = new DigitalUser();
		PartyBasicInformation partyBasicInformation = new PartyBasicInformation(); 
		partyBasicInformation.setPartyActiveIndicator(Boolean.TRUE);
		digitalUser.setDigitalUserLockedOutIndicator(Boolean.FALSE);
		Map<String, Map<String, List<String>>> accountFiltering = processPartyPermisssion();
		List<String> stringList = Arrays.asList("partyPermisssion");
		Map<String, List<String>> map = new HashMap();
		map.put("partyPermisssion", stringList);
		accountFiltering.put("partyPermisssion", map);
		permissionFilter.setNext(nextInChain);
		//Mockito.when(nextInChain.process(any(),any(), any(), any())).thenReturn(acenList);
		acenList =	permissionFilter.process( acenList,partyEntitlements,consentFlowType, accountFiltering);
		assertNotNull(acenList);
	}
	
	@Test
	public void testAccountTypeFilterProcessForPartyEntitlementFlag() {
		List<AccountEntitlements> acenList =new ArrayList<AccountEntitlements>();
		Account acc=new Account();
		Account acc1=new Account();
		AccountEntitlements acen = new AccountEntitlements();
		acen.setAccount(acc);
		acen.setAccount(acc1);
		acenList.add(acen);
		Map<String, List<String>> permissionList =new HashMap<String, List<String>>();
		List<String> li=new  ArrayList<String>();
		permissionList.put("1", li);
		String consentFlowType="AISP";
		PartyEntitlements partyEntitlements = new PartyEntitlements();
		partyEntitlements.setEntitlements(Arrays.asList("partyPermisssion"));
		DigitalUser digitalUser = new DigitalUser();
		PartyBasicInformation partyBasicInformation = new PartyBasicInformation(); 
		partyBasicInformation.setPartyActiveIndicator(Boolean.TRUE);
		digitalUser.setDigitalUserLockedOutIndicator(Boolean.FALSE);
		Map<String, Map<String, List<String>>> accountFiltering = processPartyPermisssion();
		List<String> stringList = Arrays.asList("partyPermisssion");
		Map<String, List<String>> map = new HashMap();
		map.put("partyPermisssion", stringList);
		accountFiltering.put("partyPermisssion", map);
		permissionFilter.setNext(nextInChain);
		//Mockito.when(nextInChain.process(any(),any(), any(), any())).thenReturn(acenList);
		acenList =	permissionFilter.process( acenList,partyEntitlements,consentFlowType, accountFiltering);
		assertNotNull(acenList);
	}

	@Test
	public void testAccountTypeFilterProcessForExcludeCispPermissions() {
		List<AccountEntitlements> acenList =new ArrayList<AccountEntitlements>();
		Account acc=new Account();
		Account acc1=new Account();
		AccountEntitlements acen = new AccountEntitlements();
		acen.setAccount(acc);
		acen.setAccount(acc1);
		acenList.add(acen);
		Map<String, List<String>> permissionList =new HashMap<String, List<String>>();
		List<String> li=new  ArrayList<String>();
		permissionList.put("1", li);
		String consentFlowType="CISP";
		PartyEntitlements partyEntitlements = new PartyEntitlements();
		partyEntitlements.setEntitlements(Arrays.asList("AISP"));
		DigitalUser digitalUser = new DigitalUser();
		PartyBasicInformation partyBasicInformation = new PartyBasicInformation(); 
		partyBasicInformation.setPartyActiveIndicator(Boolean.TRUE);
		digitalUser.setDigitalUserLockedOutIndicator(Boolean.FALSE);
		Map<String, Map<String, List<String>>> accountFiltering = processExcludeCispPermissions();
		List<String> stringList = Arrays.asList("excludeCispPermissions");
		Map<String, List<String>> map = new HashMap();
		map.put("partyPermisssion", stringList);
		accountFiltering.put("partyPermisssion", map);
		permissionFilter.setNext(nextInChain);
		//Mockito.when(nextInChain.process(any(),any(), any(), any())).thenReturn(acenList);
		acenList =	permissionFilter.process( acenList,partyEntitlements,consentFlowType, accountFiltering);
		assertNotNull(acenList);
	}
	
	@Test
	public void testAccountTypeFilterprocessPISPEntitlementFlag() {
		List<AccountEntitlements2> acenList =new ArrayList<AccountEntitlements2>();
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.PISP.Account acc=new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.PISP.Account();
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.PISP.Account acc1=new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.PISP.Account();
		AccountEntitlements2 acen = new AccountEntitlements2();
		acen.setAccount(acc);
		acen.setAccount(acc1);
		acenList.add(acen);
		Map<String, List<String>> permissionList =new HashMap<String, List<String>>();
		List<String> li=new  ArrayList<String>();
		permissionList.put("1", li);
		String consentFlowType="AISP";
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.PISP.PartyEntitlements partyEntitlements = new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.PISP.PartyEntitlements();
		partyEntitlements.setEntitlements(Arrays.asList("partyPermisssion"));
		DigitalUser digitalUser = new DigitalUser();
		PartyBasicInformation partyBasicInformation = new PartyBasicInformation(); 
		partyBasicInformation.setPartyActiveIndicator(Boolean.TRUE);
		digitalUser.setDigitalUserLockedOutIndicator(Boolean.FALSE);
		Map<String, Map<String, List<String>>> accountFiltering = processPartyPermisssion();
		List<String> stringList = Arrays.asList("partyPermisssion");
		Map<String, List<String>> map = new HashMap();
		map.put("partyPermisssion", stringList);
		accountFiltering.put("partyPermisssion", map);
		permissionFilter.setNext(nextInChain);
		//Mockito.when(nextInChain.process(any(),any(), any(), any())).thenReturn(acenList);
		acenList =	permissionFilter.processPISP( acenList,partyEntitlements,consentFlowType, accountFiltering);
		assertNotNull(acenList);
	}
	
	
	@Test
	public void testAccountTypeFilterprocessPISPPartyPermisssion() {
		List<AccountEntitlements2> acenList =new ArrayList<AccountEntitlements2>();
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.PISP.Account acc=new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.PISP.Account();
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.PISP.Account acc1=new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.PISP.Account();
		AccountEntitlements2 acen = new AccountEntitlements2();
		acen.setAccount(acc);
		acen.setAccount(acc1);
		acenList.add(acen);
		Map<String, List<String>> permissionList =new HashMap<String, List<String>>();
		List<String> li=new  ArrayList<String>();
		permissionList.put("1", li);
		String consentFlowType="AISP";
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.PISP.PartyEntitlements partyEntitlements = new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.PISP.PartyEntitlements();
		partyEntitlements.setEntitlements(Arrays.asList("AISP"));
		DigitalUser digitalUser = new DigitalUser();
		PartyBasicInformation partyBasicInformation = new PartyBasicInformation(); 
		partyBasicInformation.setPartyActiveIndicator(Boolean.TRUE);
		digitalUser.setDigitalUserLockedOutIndicator(Boolean.FALSE);
		Map<String, Map<String, List<String>>> accountFiltering = processPartyPermisssion();
		List<String> stringList = Arrays.asList("partyPermisssion");
		Map<String, List<String>> map = new HashMap();
		map.put("partyPermisssion", stringList);
		accountFiltering.put("partyPermisssion", map);
		permissionFilter.setNext(nextInChain);
		//Mockito.when(nextInChain.process(any(),any(), any(), any())).thenReturn(acenList);
		acenList =	permissionFilter.processPISP( acenList,partyEntitlements,consentFlowType, accountFiltering);
		assertNotNull(acenList);
	}

	public Map<String, Map<String, List<String>>> processPartyPermisssion() {
		Map<String, Map<String, List<String>>> accountFiltering = new HashMap<>();
		Map<String, List<String>> map1 = new HashMap<String, List<String>>();
		List<String> accountType = new ArrayList<>();
		accountType.add("Current Account");
		map1.put("AISP", accountType);
		map1.put("CISP", accountType);
		map1.put("PISP", accountType);
		accountFiltering.put("accountType", map1);
		Map<String, List<String>> map2 = new HashMap<String, List<String>>();
		List<String> jurisdictionList = new ArrayList<>();
		jurisdictionList.add("NORTHERN_IRELAND.SchemeName=BBAN");
		jurisdictionList.add("NORTHERN_IRELAND.Identification=IBAN");
		jurisdictionList.add("NORTHERN_IRELAND.ServicerSchemeName=UKSORTCODE");
		jurisdictionList.add("NORTHERN_IRELAND.ServicerIdentification=BIC");
		jurisdictionList.add("GREAT_BRITAIN.SchemeName=BBAN");
		jurisdictionList.add("GREAT_BRITAIN.Identification=IBAN");
		jurisdictionList.add("GREAT_BRITAIN.ServicerSchemeName=UKSORTCODE");
		jurisdictionList.add("GREAT_BRITAIN.ServicerIdentification=BIC");
		map2.put("AISP", jurisdictionList);
		map2.put("CISP", jurisdictionList);
		map2.put("PISP", jurisdictionList);
		accountFiltering.put("jurisdiction", map2);
		Map<String, List<String>> map3 = new HashMap<String, List<String>>();
		List<String> permissionList1 = new ArrayList<>();
		permissionList1.add("partyPermisssion");
		permissionList1.add("partyPermisssion");
		List<String> permissionList2 = new ArrayList<>();
		permissionList2.add("A");
		permissionList2.add("X");
		map3.put("partyPermisssion", permissionList1);
		map3.put("CISP", permissionList1);
		map3.put("PISP", permissionList2);
		accountFiltering.put("permission", map3);
		return accountFiltering;
	}


	public Map<String, Map<String, List<String>>> processExcludeCispPermissions() {
		Map<String, Map<String, List<String>>> accountFiltering = new HashMap<>();
		Map<String, List<String>> map1 = new HashMap<String, List<String>>();
		List<String> accountType = new ArrayList<>();
		accountType.add("Current Account");
		map1.put("AISP", accountType);
		map1.put("CISP", accountType);
		map1.put("PISP", accountType);
		accountFiltering.put("accountType", map1);
		Map<String, List<String>> map2 = new HashMap<String, List<String>>();
		List<String> jurisdictionList = new ArrayList<>();
		jurisdictionList.add("NORTHERN_IRELAND.SchemeName=BBAN");
		jurisdictionList.add("NORTHERN_IRELAND.Identification=IBAN");
		jurisdictionList.add("NORTHERN_IRELAND.ServicerSchemeName=UKSORTCODE");
		jurisdictionList.add("NORTHERN_IRELAND.ServicerIdentification=BIC");
		jurisdictionList.add("GREAT_BRITAIN.SchemeName=BBAN");
		jurisdictionList.add("GREAT_BRITAIN.Identification=IBAN");
		jurisdictionList.add("GREAT_BRITAIN.ServicerSchemeName=UKSORTCODE");
		jurisdictionList.add("GREAT_BRITAIN.ServicerIdentification=BIC");
		map2.put("AISP", jurisdictionList);
		map2.put("CISP", jurisdictionList);
		map2.put("PISP", jurisdictionList);
		accountFiltering.put("jurisdiction", map2);
		Map<String, List<String>> map3 = new HashMap<String, List<String>>();
		List<String> permissionList1 = new ArrayList<>();
		permissionList1.add("excludeCispPermissions");
		permissionList1.add("excludeCispPermissions");
		List<String> permissionList2 = new ArrayList<>();
		permissionList2.add("A");
		permissionList2.add("X");
		map3.put("partyPermisssion", permissionList1);
		map3.put("CISP", permissionList1);
		map3.put("PISP", permissionList2);
		accountFiltering.put("permission", map3);
		return accountFiltering;
	}


}
