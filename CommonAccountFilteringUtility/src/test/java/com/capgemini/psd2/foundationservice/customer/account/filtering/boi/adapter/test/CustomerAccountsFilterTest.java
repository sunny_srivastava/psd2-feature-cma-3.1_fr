package com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.test;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.anyObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.filterartion.CustomerAccountsFilter;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.AccountEntitlements;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.PISP.AccountEntitlements2;
import com.capgemini.psd2.logger.PSD2Constants;

@RunWith(SpringJUnit4ClassRunner.class)
public class CustomerAccountsFilterTest {

	@InjectMocks
	CustomerAccountsFilter customerAccountsFilter;
	
	@Before
	public void setUp(){
		MockitoAnnotations.initMocks(this);
	}
	
	
	@Test
	public void contextLoads() {
	}
	
	/*
	@Test
	public void testGetFilteredAccounts(){
		
		List<AccountEntitlements> inputResObject = new ArrayList<AccountEntitlements>();
		Map<String, String> params = new HashMap<>();
		Map<String, Map<String, List<String>>> map = new HashMap<>();
		params.put(PSD2Constants.CONSENT_FLOW_TYPE, "test");
		customerAccountsFilter.setAccountFiltering(map);
		customerAccountsFilter.getAccountFiltering();
		List<AccountEntitlements> accounts = customerAccountsFilter.getFilteredAccounts(inputResObject,anyObject(), params);
		assertNotNull(accounts);
	}
	
	@Test
	public void testGetFilteredAccountsPISP(){
		
		List<AccountEntitlements2> inputResObject = new ArrayList<AccountEntitlements2>();
		Map<String, String> params = new HashMap<>();
		Map<String, Map<String, List<String>>> map = new HashMap<>();
		params.put(PSD2Constants.CONSENT_FLOW_TYPE, "test");
		customerAccountsFilter.setAccountFiltering(map);
		customerAccountsFilter.getAccountFiltering();
		List<AccountEntitlements2> accounts = customerAccountsFilter.getFilteredAccountsPISP(inputResObject,anyObject(), params);
		
		assertNotNull(accounts);
	}
	*/
}