package com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.test;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.when;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;
import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.adapter.utility.AdapterUtility;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.delegate.CustomerAccountsFilterFoundationServiceDelegate;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnts;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accounts;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.filterartion.CustomerAccountsFilter;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.AccountEntitlements;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.PISP.AccountEntitlements2;
import com.capgemini.psd2.logger.PSD2Constants;
@RunWith(SpringJUnit4ClassRunner.class)
public class CustomerAccountsFilterFoundationServiceDelegateTest {

	@InjectMocks
	CustomerAccountsFilterFoundationServiceDelegate customerAccountsFilterFoundationServiceDelegate;

	@Mock
	AdapterUtility adapterUtility;

	@Mock
	CustomerAccountsFilter CustAccntProfileSerTrans;

	@Before
	public void setUp(){
		MockitoAnnotations.initMocks(this);
	}


	@Test
	public void contextLoads() {
	}

	@Test
	public void testGetFoundationServiceUrlAISP(){
		Map<String, String> params = new HashMap<>();
		params.put("x-channel-id","BOIROI");
		Mockito.when(adapterUtility.retriveCustProfileFoundationServiceURLAISP(anyObject())).thenReturn("http://localhost:9094/fsabtservice/services/abt");
		String result = customerAccountsFilterFoundationServiceDelegate.getFoundationServiceURLAISP(params, "1111111111");
		assertNotNull(result);
	}

	@Test(expected = AdapterException.class)
	public void testGetFoundationServiceAISPBaseUrlNullException(){
		Map<String, String> params = new HashMap<>();
		params.put("x-channel-id","BOIROI");
		Mockito.when(adapterUtility.retriveCustProfileFoundationServiceURLAISP(anyObject())).thenReturn(null);
		//String baseURL = adapterUtility.retriveCustProfileFoundationServiceURL((params.get(PSD2Constants.CHANNEL_ID)).toUpperCase());
		String result = customerAccountsFilterFoundationServiceDelegate.getFoundationServiceURLAISP(params, "111111111");
		assertNotNull(result);
	}
	
	@Test
	public void testGetFoundationServiceURLPISP(){
		Map<String, String> params = new HashMap<>();
		params.put("x-channel-id","BOIROI");
		Mockito.when(adapterUtility.retriveCustProfileFoundationServiceURLPISP(anyObject())).thenReturn("http://localhost:9094/fsabtservice/services/abt");
		String result = customerAccountsFilterFoundationServiceDelegate.getFoundationServiceURLPISP(params, "1111111111");
		assertNotNull(result);
	}

	@Test(expected = AdapterException.class)
	public void testGetFoundationServiceURLPISPBaseUrlNullException(){
		Map<String, String> params = new HashMap<>();
		params.put("x-channel-id","BOIROI");
		Mockito.when(adapterUtility.retriveCustProfileFoundationServiceURLPISP(anyObject())).thenReturn(null);
		String result = customerAccountsFilterFoundationServiceDelegate.getFoundationServiceURLPISP(params, "test");
		assertNotNull(result);

	}

	@Test
	public void getFoundationServiceURLforPISP(){
		Mockito.when(adapterUtility.retriveFoundationServiceURL(anyObject())).thenReturn("http://localhost:9094/fsabtservice/services/abt");
		customerAccountsFilterFoundationServiceDelegate.getFoundationServiceURLforPISP("BOIROI","11111111" ,"11111123");
	}

	@Test(expected = AdapterException.class)
	public void getFoundationServiceURLforPISPForNullChannelId(){
		Mockito.when(adapterUtility.retriveFoundationServiceURL(anyObject())).thenReturn("http://localhost:9094/fsabtservice/services/abt");
		customerAccountsFilterFoundationServiceDelegate.getFoundationServiceURLforPISP(null,"11111111" ,"11111123");
	}
	
	@Test(expected = AdapterException.class)
	public void getFoundationServiceURLforPISPForNullAccountNSC(){
		Mockito.when(adapterUtility.retriveFoundationServiceURL(anyObject())).thenReturn("http://localhost:9094/fsabtservice/services/abt");
		customerAccountsFilterFoundationServiceDelegate.getFoundationServiceURLforPISP("BOIROI",null ,"11111123");
	}
	
	@Test(expected = AdapterException.class)
	public void getFoundationServiceURLforPISPForNullAccountNumber(){
		Mockito.when(adapterUtility.retriveFoundationServiceURL(anyObject())).thenReturn("http://localhost:9094/fsabtservice/services/abt");
		customerAccountsFilterFoundationServiceDelegate.getFoundationServiceURLforPISP("BOIROI","11111111" ,null);
	}
	
	@Test
	public void testGetFilteredAccounts(){
		List<AccountEntitlements> accounts = new ArrayList<AccountEntitlements>();
		Mockito.when(CustAccntProfileSerTrans.getFilteredAccounts(anyObject(),anyObject(), anyObject())).thenReturn(accounts);
		List<AccountEntitlements> result = customerAccountsFilterFoundationServiceDelegate.getFilteredAccounts(anyObject(), anyObject(), anyObject());
		assertNotNull(result);
	}

	@Test
	public void testGetFilteredAccountsPISP(){
		List<AccountEntitlements2> accounts = new ArrayList<AccountEntitlements2>();
		Mockito.when(CustAccntProfileSerTrans.getFilteredAccountsPISP(anyObject(),anyObject(), anyObject())).thenReturn(accounts);
		List<AccountEntitlements2> result = customerAccountsFilterFoundationServiceDelegate.getFilteredAccountsPISP(anyObject(), anyObject(), anyObject());
		assertNotNull(result);
	}
	
	@Test
	public void testCreateRequestHeadersAISP(){
		ReflectionTestUtils.setField(customerAccountsFilterFoundationServiceDelegate, "sourceUserReqHeader", "user");
		ReflectionTestUtils.setField(customerAccountsFilterFoundationServiceDelegate, "sourceSystemReqHeader", "channel");
		ReflectionTestUtils.setField(customerAccountsFilterFoundationServiceDelegate, "correlationMuleReqHeader", "platform");
		ReflectionTestUtils.setField(customerAccountsFilterFoundationServiceDelegate, "transactioReqHeader", "correlation");
		ReflectionTestUtils.setField(customerAccountsFilterFoundationServiceDelegate, "platform", "platform");
		Map<String, String> params = new HashMap<>();
		params.put(PSD2Constants.USER_ID, "user");
		params.put(PSD2Constants.CHANNEL_ID, "channel");
		params.put(PSD2Constants.CORRELATION_ID, "correlation");
		HttpHeaders headers = customerAccountsFilterFoundationServiceDelegate.createRequestHeadersAISP(params);
		assertNotNull(headers);
	}
	
	@Test
	public void testCreateRequestHeadersForPISP(){
		ReflectionTestUtils.setField(customerAccountsFilterFoundationServiceDelegate, "userInReqHeader", "user");
		ReflectionTestUtils.setField(customerAccountsFilterFoundationServiceDelegate, "channelInReqHeader", "channel");
		ReflectionTestUtils.setField(customerAccountsFilterFoundationServiceDelegate, "platformInReqHeader", "platform");
		ReflectionTestUtils.setField(customerAccountsFilterFoundationServiceDelegate, "correlationReqHeader", "correlation");
		ReflectionTestUtils.setField(customerAccountsFilterFoundationServiceDelegate, "platform", "platform");
		Map<String, String> params = new HashMap<>();
		params.put(PSD2Constants.USER_IN_REQ_HEADER, "user");
		params.put(PSD2Constants.CHANNEL_IN_REQ_HEADER, "channel");
		params.put(PSD2Constants.CORRELATION_REQ_HEADER, "correlation");
		HttpHeaders headers = customerAccountsFilterFoundationServiceDelegate.createRequestHeadersForPISP(params);
		assertNotNull(headers);
	}

	@Test
	public void testCreateRequestHeadersPISP(){
		ReflectionTestUtils.setField(customerAccountsFilterFoundationServiceDelegate, "sourceUserReqHeader", "user");
		ReflectionTestUtils.setField(customerAccountsFilterFoundationServiceDelegate, "sourceSystemReqHeader", "sourceSystem");
		ReflectionTestUtils.setField(customerAccountsFilterFoundationServiceDelegate, "correlationMuleReqHeader", "correlation");
		ReflectionTestUtils.setField(customerAccountsFilterFoundationServiceDelegate, "transactioReqHeader", "correlation");
		ReflectionTestUtils.setField(customerAccountsFilterFoundationServiceDelegate, "partySourceNumber", "partySourceNumber");
		Map<String, String> params = new HashMap<>();
		params.put(PSD2Constants.USER_ID,"x-user-id");
		params.put(PSD2Constants.CORRELATION_ID,"x-fapi-interaction-id");
		params.put(PSD2Constants.CHANNEL_ID,"BOIROI");
		params.put(PSD2Constants.PARTY_IDENTIFIER,"");
		params.put(PSD2Constants.USER_IN_REQ_HEADER, "user");
		params.put(PSD2Constants.CHANNEL_IN_REQ_HEADER, "channel");
		params.put(PSD2Constants.CORRELATION_REQ_HEADER, "correlation");
		HttpHeaders headers = customerAccountsFilterFoundationServiceDelegate.createRequestHeadersPISP(params);
		assertNotNull(headers);
	}
	
	@Test(expected = AdapterException.class)
	public void testCreateRequestHeadersUserIdNull(){

		Map<String, String> params = new HashMap<>();
		params.put(PSD2Constants.USER_ID, null);
		params.put(PSD2Constants.CHANNEL_ID, "channel");
		params.put(PSD2Constants.CORRELATION_ID, "correlation");

		customerAccountsFilterFoundationServiceDelegate.createRequestHeadersAISP(params);
	}

	@Test(expected = AdapterException.class)
	public void testCreateRequestHeadersCorrelationIdNull(){

		Map<String, String> params = new HashMap<>();
		params.put(PSD2Constants.USER_ID, "test");
		params.put(PSD2Constants.CHANNEL_ID, "channel");
		params.put(PSD2Constants.CORRELATION_ID, null);

		customerAccountsFilterFoundationServiceDelegate.createRequestHeadersAISP(params);
	}



	

	

	@Test(expected = AdapterException.class)
	public void testCreateRequestHeadersPISPUserIdNull(){

		Map<String, String> params = new HashMap<>();
		params.put(PSD2Constants.USER_IN_REQ_HEADER, null);
		params.put(PSD2Constants.CHANNEL_IN_REQ_HEADER, "channel");
		params.put(PSD2Constants.CORRELATION_REQ_HEADER, "correlation");

		customerAccountsFilterFoundationServiceDelegate.createRequestHeadersForPISP(params);
	}

	@Test(expected = AdapterException.class)
	public void testCreateRequestHeadersPISPChannelIdNull(){

		Map<String, String> params = new HashMap<>();
		params.put(PSD2Constants.USER_IN_REQ_HEADER, "test");
		params.put(PSD2Constants.CHANNEL_IN_REQ_HEADER, null);
		params.put(PSD2Constants.CORRELATION_REQ_HEADER, "correlation");

		customerAccountsFilterFoundationServiceDelegate.createRequestHeadersForPISP(params);
	}
	@Test(expected = AdapterException.class)
	public void testCreateRequestHeadersPISPCorrelationIdNull(){

		Map<String, String> params = new HashMap<>();
		params.put(PSD2Constants.USER_IN_REQ_HEADER, "test");
		params.put(PSD2Constants.CHANNEL_IN_REQ_HEADER, "channel");
		params.put(PSD2Constants.CORRELATION_REQ_HEADER, null);

		customerAccountsFilterFoundationServiceDelegate.createRequestHeadersForPISP(params);
	}

	@Test(expected = AdapterException.class)
	public void testCreateRequestHeadersPISPPlatformNull(){

		Map<String, String> params = new HashMap<>();
		params.put(PSD2Constants.USER_IN_REQ_HEADER, "test");
		params.put(PSD2Constants.CHANNEL_IN_REQ_HEADER, "channel");
		params.put(PSD2Constants.CORRELATION_REQ_HEADER, "correlation");
		ReflectionTestUtils.setField(customerAccountsFilterFoundationServiceDelegate, "platform", null);
		customerAccountsFilterFoundationServiceDelegate.createRequestHeadersForPISP(params);
	}
}
