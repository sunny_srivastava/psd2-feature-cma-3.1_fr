package com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.test;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnt;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnts;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.FilteredAccounts;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Jurisdiction;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.filterartion.AccountTypeFilter;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.filterartion.FilterationChain;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.Account;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.AccountEntitlements;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.DigitalUserProfile;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.PartyEntitlements;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.RetailNonRetailCode;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.SourceSystemAccountType;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.PISP.AccountEntitlements2;

@RunWith(SpringJUnit4ClassRunner.class)
public class AccountTypeFilterTest {

	@InjectMocks
	private AccountTypeFilter accountTypeFilter;

	@Mock
	private FilterationChain nextInChain;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void contextLoads() {
	}

	@Test
	public void testAccountTypeFilterProcess() {
		String consentFlowType="AISP";
		Account acc = new Account();
		acc.setAccountName("Current Account");
		acc.setAccountNumber("2345");
		acc.retailNonRetailCode(RetailNonRetailCode.RETAIL);
		acc.setSourceSystemAccountType(SourceSystemAccountType.CURRENT_ACCOUNT);
		AccountEntitlements ae= new AccountEntitlements();
		ae.setAccount(acc);
		List<String> ent = new ArrayList<String>();
		ent.add("capg");
		ent.add("boi");
		ae.setEntitlements(ent);
		PartyEntitlements partyEntitlements= new PartyEntitlements();
		List<AccountEntitlements> accounts = new ArrayList<AccountEntitlements>();
		accounts.add(ae);	
		List<AccountEntitlements> filteredAccounts=new ArrayList<AccountEntitlements>();
		Map<String, Map<String, List<String>>> accountFiltering= createAccountFilteringList();
		accountTypeFilter.setNext(nextInChain);
		Mockito.when(nextInChain.process(any(), any(), any(),any())).thenReturn(filteredAccounts);
		filteredAccounts =	accountTypeFilter.process(accounts, partyEntitlements,consentFlowType, accountFiltering);
		assertNotNull(filteredAccounts);
	}
	
	@Test
	public void testAccountTypeFilterProcessPisp() {
		String consentFlowType="PISP";
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.PISP.Account acc = new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.PISP.Account();
		acc.setAccountName("Current Account");
		acc.setAccountNumber("2345");
		acc.setRetailNonRetailCode(com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.PISP.RetailNonRetailCode.RETAIL);
		acc.setSourceSystemAccountType(com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.PISP.SourceSystemAccountType.CURRENT_ACCOUNT);
		AccountEntitlements2 ae= new AccountEntitlements2();
		ae.setAccount(acc);
		List<String> ent = new ArrayList<String>();
		ent.add("capg");
		ent.add("boi");
		ae.setEntitlements(ent);
		com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.PISP.PartyEntitlements partyEntitlements= new com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.PISP.PartyEntitlements();
		List<AccountEntitlements2> accounts = new ArrayList<AccountEntitlements2>();
		accounts.add(ae);	
		List<AccountEntitlements2> filteredAccounts=new ArrayList<AccountEntitlements2>();
		Map<String, Map<String, List<String>>> accountFiltering= createAccountFilteringList();
		accountTypeFilter.setNext(nextInChain);
		Mockito.when(nextInChain.processPISP(any(), any(), any(),any())).thenReturn(filteredAccounts);
		filteredAccounts =	accountTypeFilter.processPISP(accounts, partyEntitlements,consentFlowType, accountFiltering);
		assertNotNull(filteredAccounts);
	}

	public Map<String, Map<String, List<String>>> createAccountFilteringList() {
		Map<String, Map<String, List<String>>> accountFiltering = new HashMap<>();
		Map<String, List<String>> map1 = new HashMap<String, List<String>>();
		List<String> accountType = new ArrayList<>();
		accountType.add("Current Account");
		map1.put("AISP", accountType);
		map1.put("CISP", accountType);
		map1.put("PISP", accountType);
		accountFiltering.put("accountType", map1);
		Map<String, List<String>> map2 = new HashMap<String, List<String>>();
		List<String> jurisdictionList = new ArrayList<>();
		jurisdictionList.add("NORTHERN_IRELAND.SchemeName=BBAN");
		jurisdictionList.add("NORTHERN_IRELAND.Identification=IBAN");
		jurisdictionList.add("NORTHERN_IRELAND.ServicerSchemeName=UKSORTCODE");
		jurisdictionList.add("NORTHERN_IRELAND.ServicerIdentification=BIC");
		jurisdictionList.add("GREAT_BRITAIN.SchemeName=BBAN");
		jurisdictionList.add("GREAT_BRITAIN.Identification=IBAN");
		jurisdictionList.add("GREAT_BRITAIN.ServicerSchemeName=UKSORTCODE");
		jurisdictionList.add("GREAT_BRITAIN.ServicerIdentification=BIC");
		map2.put("AISP", jurisdictionList);
		map2.put("CISP", jurisdictionList);
		map2.put("PISP", jurisdictionList);
		accountFiltering.put("jurisdiction", map2);
		Map<String, List<String>> map3 = new HashMap<String, List<String>>();
		List<String> permissionList1 = new ArrayList<>();
		permissionList1.add("A");
		permissionList1.add("V");
		List<String> permissionList2 = new ArrayList<>();
		permissionList2.add("A");
		permissionList2.add("X");
		map3.put("AISP", permissionList1);
		map3.put("CISP", permissionList1);
		map3.put("PISP", permissionList2);
		accountFiltering.put("permission", map3);
		return accountFiltering;
	}
		
}
