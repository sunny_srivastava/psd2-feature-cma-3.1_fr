package com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.test;

import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.aisp.domain.OBAccount6Account;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.Accnt;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.domain.CreditCardAccnt;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.filterartion.AdapterFilterUtility;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.Account;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.AccountEntitlements;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.Brand;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.BrandCode;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.Card;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.CreditCardAccount;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.CurrentAccount;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.DigitalUserProfile;
import com.capgemini.psd2.foundationservice.customer.account.filtering.boi.adapter.raml.domain.SourceSystemAccountType;




@RunWith(SpringJUnit4ClassRunner.class)
public class AdapterFilterUtilityTest {

	@InjectMocks
	AdapterFilterUtility adapterFilterUtility;


	@Before
	public void setUp(){
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void contextLoads() {
	}


	@Test
	public void testPrevalidateAccountsCurrentAccount(){
		DigitalUserProfile accGetResponse = new DigitalUserProfile();
		AccountEntitlements accountEntitlements = new AccountEntitlements();
		Account account = new Account();
		account.setSourceSystemAccountType(SourceSystemAccountType.CURRENT_ACCOUNT);
		CurrentAccount currentAccount = new CurrentAccount();
		currentAccount.setInternationalBankAccountNumberIBAN("123");
		currentAccount.setParentNationalSortCodeNSCNumber("123");
		account.setCurrentAccountInformation(currentAccount);
		account.setAccountNumber("123");
		accountEntitlements.setAccount(account);
		AccountDetails accountDetails1 = new AccountDetails();
		accountDetails1.setAccountNSC("123");
		accountDetails1.setAccountNumber("123");
		CreditCardAccnt ccAccount = new CreditCardAccnt();
		ccAccount.setPlApplId("123");
		ccAccount.setCreditCardNumber("111111");
		//accGetResponse.getCreditCardAccount().add(ccAccount);
		accountDetails1.setAccountNSC("123");
		accountDetails1.setAccountNumber("123");
		AccountMapping accountMapping = new AccountMapping();
		accountMapping.setAccountDetails(new ArrayList<AccountDetails>());
		accountMapping.getAccountDetails().add(accountDetails1);
		accGetResponse.setAccountEntitlements(Arrays.asList(accountEntitlements));
		List<AccountDetails> accDetails1=new ArrayList<AccountDetails>();
		accDetails1.add(accountDetails1);
		OBAccount6Account obAcc=new OBAccount6Account();
		obAcc.setIdentification("111111");
		accountDetails1.setAccount(obAcc);
		List<CreditCardAccnt> list1=new ArrayList<CreditCardAccnt>();
		list1.add(ccAccount);
		accountMapping.setAccountDetails(new ArrayList<AccountDetails>());
		accountMapping.getAccountDetails().add(accountDetails1);		
		AccountMapping accountMapping1 = adapterFilterUtility.prevalidateAccounts(accGetResponse, accountMapping);
		assertNotNull(accountMapping1); 
	}

	@Test
	public void testPrevalidateAccountsSavingsAccount(){
		DigitalUserProfile accGetResponse = new DigitalUserProfile();
		AccountEntitlements accountEntitlements = new AccountEntitlements();
		Account account = new Account();
		account.setSourceSystemAccountType(SourceSystemAccountType.SAVINGS_ACCOUNT);
		CurrentAccount currentAccount = new CurrentAccount();
		currentAccount.setInternationalBankAccountNumberIBAN("123");
		currentAccount.setParentNationalSortCodeNSCNumber("123");
		account.setSavingsAccountInformation(currentAccount);
		account.setAccountNumber("123");
		accountEntitlements.setAccount(account);
		AccountDetails accountDetails1 = new AccountDetails();
		accountDetails1.setAccountNSC("123");
		accountDetails1.setAccountNumber("123");
		CreditCardAccnt ccAccount = new CreditCardAccnt();
		ccAccount.setPlApplId("123");
		ccAccount.setCreditCardNumber("111111");
		//accGetResponse.getCreditCardAccount().add(ccAccount);
		accountDetails1.setAccountNSC("123");
		accountDetails1.setAccountNumber("123");
		AccountMapping accountMapping = new AccountMapping();
		accountMapping.setAccountDetails(new ArrayList<AccountDetails>());
		accountMapping.getAccountDetails().add(accountDetails1);
		accGetResponse.setAccountEntitlements(Arrays.asList(accountEntitlements));
		List<AccountDetails> accDetails1=new ArrayList<AccountDetails>();
		accDetails1.add(accountDetails1);
		OBAccount6Account obAcc=new OBAccount6Account();
		obAcc.setIdentification("111111");
		accountDetails1.setAccount(obAcc);
		List<CreditCardAccnt> list1=new ArrayList<CreditCardAccnt>();
		list1.add(ccAccount);
		accountMapping.setAccountDetails(new ArrayList<AccountDetails>());
		accountMapping.getAccountDetails().add(accountDetails1);		
		AccountMapping accountMapping1 = adapterFilterUtility.prevalidateAccounts(accGetResponse, accountMapping);
		assertNotNull(accountMapping1); 
	}

	@Test
	public void testPrevalidateAccountsCreditCardAccount(){
		DigitalUserProfile accGetResponse = new DigitalUserProfile();
		AccountEntitlements accountEntitlements = new AccountEntitlements();
		Account account = new Account();
		account.setSourceSystemAccountType(SourceSystemAccountType.CREDIT_CARD);
		CreditCardAccount creditCardAccount = new CreditCardAccount();
		Card card = new Card();
		card.setMaskedCardPANNumber("111111");
		creditCardAccount.setCard(card);
		account.setCreditCardAccountInformation(creditCardAccount);
		account.setAccountNumber("123");
		accountEntitlements.setAccount(account);
		AccountDetails accountDetails1 = new AccountDetails();
		accountDetails1.setAccountNSC("123");
		accountDetails1.setAccountNumber("123");
		CreditCardAccnt ccAccount = new CreditCardAccnt();
		ccAccount.setPlApplId("123");
		ccAccount.setCreditCardNumber("111111");
		//accGetResponse.getCreditCardAccount().add(ccAccount);
		accountDetails1.setAccountNSC("123");
		accountDetails1.setAccountNumber("123");
		AccountMapping accountMapping = new AccountMapping();
		accountMapping.setAccountDetails(new ArrayList<AccountDetails>());
		accountMapping.getAccountDetails().add(accountDetails1);
		accGetResponse.setAccountEntitlements(Arrays.asList(accountEntitlements));
		List<AccountDetails> accDetails1=new ArrayList<AccountDetails>();
		accDetails1.add(accountDetails1);
		OBAccount6Account obAcc=new OBAccount6Account();
		obAcc.setIdentification("111111");
		accountDetails1.setAccount(obAcc);
		List<CreditCardAccnt> list1=new ArrayList<CreditCardAccnt>();
		list1.add(ccAccount);
		accountMapping.setAccountDetails(new ArrayList<AccountDetails>());
		accountMapping.getAccountDetails().add(accountDetails1);		
		AccountMapping accountMapping1 = adapterFilterUtility.prevalidateAccounts(accGetResponse, accountMapping);
		assertNotNull(accountMapping1); 
	}
	
	
	@Test (expected= AdapterException.class)
	public void testPrevalidateAccountsForException(){
		DigitalUserProfile accGetResponse = new DigitalUserProfile();
		Accnt accnt1 = new Accnt();
		accnt1.setAccountNSC("253");
		accnt1.setAccountNumber("123");
		CreditCardAccnt ccAccount = new CreditCardAccnt();
		ccAccount.setPlApplId("123");
		ccAccount.setCreditCardNumber("111111");
		//accGetResponse.getCreditCardAccount().add(ccAccount);
		AccountDetails accountDetails1 = new AccountDetails();
		accountDetails1.setAccountNSC("567");
		accountDetails1.setAccountNumber("123");
		AccountMapping accountMapping = new AccountMapping();
		accountMapping.setAccountDetails(new ArrayList<AccountDetails>());
		accountMapping.getAccountDetails().add(accountDetails1);
		List<AccountDetails> accDetails1=new ArrayList<AccountDetails>();
		accDetails1.add(accountDetails1);
		OBAccount6Account obAcc=new OBAccount6Account();
		obAcc.setIdentification("111111");
		accountDetails1.setAccount(obAcc);
		List<CreditCardAccnt> list1=new ArrayList<CreditCardAccnt>();
		list1.add(ccAccount);
		AccountMapping filteredAccounts;
		filteredAccounts =	adapterFilterUtility.prevalidateAccounts(accGetResponse, accountMapping);
		assertNotNull(filteredAccounts); 
	}
	
	
	@Test
	public void testPrevalidateJurisdiction(){
		DigitalUserProfile  digiUserProfile = new DigitalUserProfile();
		Brand brand = new Brand();
		brand.setBrandCode(BrandCode.AA);
		Account acc=new Account();
		acc.setAccountBrand(brand);
		AccountEntitlements accnt =new AccountEntitlements();
		accnt.setAccount(acc);
		List<AccountEntitlements> list=new ArrayList<AccountEntitlements>();
		digiUserProfile.setAccountEntitlements(list);
		List<String> listString = new ArrayList<String>();
		accnt.setEntitlements(listString);
		list.add(accnt);
		String jurisdictionType=BrandCode.AA.toString();
		digiUserProfile=adapterFilterUtility.prevalidateJurisdiction(digiUserProfile, jurisdictionType);
		assertNotNull(digiUserProfile);
	}


	@Test
	public void testPrevalidateJurisdictionForNull(){
		DigitalUserProfile  digiUserProfile = new DigitalUserProfile();
		Brand brand = new Brand();
		brand.setBrandCode(BrandCode.AA);
		Account acc=new Account();
		acc.setAccountBrand(brand);
		AccountEntitlements accnt =new AccountEntitlements();
		accnt.setAccount(acc);
		List<AccountEntitlements> list=new ArrayList<AccountEntitlements>();
		digiUserProfile.setAccountEntitlements(list);
		List<String> listString = new ArrayList<String>();
		accnt.setEntitlements(listString);
		list.add(accnt);
		String jurisdictionType="brandCode";
		digiUserProfile=adapterFilterUtility.prevalidateJurisdiction(digiUserProfile, jurisdictionType);
		assertNotNull(digiUserProfile);

	}


	@Test
	public void testPrevalidateJurisdictionForNotContain(){
		DigitalUserProfile  digiUserProfile = new DigitalUserProfile();
		Brand brand = new Brand();
		brand.setBrandCode(BrandCode.BOI_GB);
		Account acc=new Account();
		acc.setAccountBrand(brand);
		AccountEntitlements accnt =new AccountEntitlements();
		accnt.setAccount(acc);
		List<AccountEntitlements> list=new ArrayList<AccountEntitlements>();
		digiUserProfile.setAccountEntitlements(list);
		List<String> listString = new ArrayList<String>();
		accnt.setEntitlements(listString);
		list.add(accnt);
		String jurisdictionType="brandCode";
		digiUserProfile=adapterFilterUtility.prevalidateJurisdiction(digiUserProfile, jurisdictionType);
		assertNotNull(digiUserProfile);

	}
}