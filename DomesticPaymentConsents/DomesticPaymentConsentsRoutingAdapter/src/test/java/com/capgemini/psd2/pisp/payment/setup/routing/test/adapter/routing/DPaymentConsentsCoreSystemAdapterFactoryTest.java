
package com.capgemini.psd2.pisp.payment.setup.routing.test.adapter.routing;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.pisp.payment.setup.routing.adapter.impl.DPaymentConsentsStagingRoutingAdapterImpl;
import com.capgemini.psd2.pisp.payment.setup.routing.adapter.routing.DPaymentConsentsCoreSystemAdapterFactory;
import com.capgemini.psd2.pisp.stage.operations.adapter.DomesticPaymentStagingAdapter;

@RunWith(SpringJUnit4ClassRunner.class)
public class DPaymentConsentsCoreSystemAdapterFactoryTest {
	@InjectMocks
	DPaymentConsentsCoreSystemAdapterFactory factory;

	@Mock
	private ApplicationContext applicationContext;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void contextLoads() {
	}

	@Test
	public void getDomesticPaymentSetupStagingInstanceTest() {
		DomesticPaymentStagingAdapter adapter = new DPaymentConsentsStagingRoutingAdapterImpl();
		when(applicationContext.getBean(anyString())).thenReturn(adapter);
		
		DomesticPaymentStagingAdapter dAdapter =  factory.getDomesticPaymentSetupStagingInstance("test");
		assertNotNull(dAdapter);
	}

	@Test
	public void testSetApplicationContext() {
		factory.setApplicationContext(new ApplicationContextMock());
	}

	@After
	public void tearDown() throws Exception {
		factory = null;
		applicationContext = null;
	}

}
