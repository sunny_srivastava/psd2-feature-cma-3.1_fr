package com.capgemini.psd2.pisp.payment.setup.mongo.db.test.adapter.impl;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.pisp.payment.setup.mongo.db.cma1.domain.CustomDomesticPaymentSetupCma1POSTResponse;
import com.capgemini.psd2.pisp.payment.setup.mongo.db.cma1.domain.OBWriteDataDomesticConsentResponse1;
import com.capgemini.psd2.pisp.payment.setup.mongo.db.transformer.DPaymentConsentsStageTransformer;

@RunWith(SpringJUnit4ClassRunner.class)
public class DPaymentConsentsStageTransformerTest {

	@Spy
	@InjectMocks
	DPaymentConsentsStageTransformer transformer;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);

	}

	@Test
	public void contextLoads() {
	}

	@Test
	public void transformCma1ToCma3ResponseTest() {
		CustomDomesticPaymentSetupCma1POSTResponse paymentCma1BankResource = new CustomDomesticPaymentSetupCma1POSTResponse();
		OBWriteDataDomesticConsentResponse1 data = new OBWriteDataDomesticConsentResponse1();

		paymentCma1BankResource.setData(data);
		paymentCma1BankResource.getData();
		data.setConsentId("1234");
		data.setStatus("teststatus");
		data.setConsentId("9999");
		paymentCma1BankResource.setData(data);
		paymentCma1BankResource.getData();
		paymentCma1BankResource.getData().getStatus();
		paymentCma1BankResource.getData().setStatus("AwaitingAuthorisation");

		transformer.transformCma1ToCma3Response(paymentCma1BankResource);
		assertNotNull(paymentCma1BankResource);

	}

	@Ignore
	@Test
	public void transformCma1ToCma3ResponseTest_Null() throws Exception {
		CustomDomesticPaymentSetupCma1POSTResponse paymentCma1BankResource = new CustomDomesticPaymentSetupCma1POSTResponse();
		paymentCma1BankResource = null;

		when(transformer.transformCma1ToCma3Response(paymentCma1BankResource))
				.thenThrow(PSD2Exception.populatePSD2Exception(ErrorCodeEnum.PISP_INCOMPATIBLE_SETUP_FOUND_IN_SYSTEM));
	}

	@After
	public void tearDown() throws Exception {
		transformer = null;

	}

}
