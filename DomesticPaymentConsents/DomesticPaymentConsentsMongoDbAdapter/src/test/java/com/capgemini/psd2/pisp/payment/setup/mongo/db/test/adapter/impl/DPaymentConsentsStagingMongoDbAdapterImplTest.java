package com.capgemini.psd2.pisp.payment.setup.mongo.db.test.adapter.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.exceptions.OBPSD2ExceptionUtility;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.domain.CustomDPaymentConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomDPaymentConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.OBCashAccountCreditor2;
import com.capgemini.psd2.pisp.domain.OBCharge1;
import com.capgemini.psd2.pisp.domain.OBCharge1Amount;
import com.capgemini.psd2.pisp.domain.OBDomestic1;
import com.capgemini.psd2.pisp.domain.OBDomestic1InstructedAmount;
import com.capgemini.psd2.pisp.domain.OBExternalConsentStatus1Code;
import com.capgemini.psd2.pisp.domain.OBWriteDataDomesticConsent1;
import com.capgemini.psd2.pisp.domain.OBWriteDataDomesticConsentResponse1;
import com.capgemini.psd2.pisp.enums.PaymentTypeEnum;
import com.capgemini.psd2.pisp.enums.ProcessConsentStatusEnum;
import com.capgemini.psd2.pisp.payment.setup.mongo.db.adapter.impl.DPaymentConsentsStagingMongoDbAdapterImpl;
import com.capgemini.psd2.pisp.payment.setup.mongo.db.adapter.repository.DPaymentConsentsFoundationRepository;
import com.capgemini.psd2.pisp.payment.setup.mongo.db.cma1.adapter.repository.DPaymentV1SetupFoundationRepository;
import com.capgemini.psd2.pisp.payment.setup.mongo.db.cma1.domain.CustomDomesticPaymentSetupCma1POSTResponse;
import com.capgemini.psd2.pisp.payment.setup.mongo.db.transformer.DPaymentConsentsStageTransformer;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.pisp.status.PaymentConstants;
import com.capgemini.psd2.utilities.SandboxValidationUtility;
import com.capgemini.psd2.validator.impl.PSD2ValidatorImpl;

@RunWith(SpringJUnit4ClassRunner.class)
public class DPaymentConsentsStagingMongoDbAdapterImplTest {

	@Mock
	private DPaymentV1SetupFoundationRepository dPaymentSetupBankRepositoryCma1;

	@Mock
	private DPaymentConsentsFoundationRepository dPaymentConsentsBankRepository;

	@Mock
	private PSD2ValidatorImpl psd2Validator;

	@Mock
	private DPaymentConsentsStageTransformer dPaymentConsentsStageTransformer;

	@Mock
	private SandboxValidationUtility utility;

	@Mock
	private RequestHeaderAttributes reqAttributes;

	@Mock
	private OBPSD2ExceptionUtility util;

	String secondaryIdentificationMockAmount;

	PSD2Exception obPSD2Exception;

	@InjectMocks
	private List<OBCharge1> chargesList = new ArrayList<OBCharge1>();

	@InjectMocks
	private DPaymentConsentsStagingMongoDbAdapterImpl DPaymentConsentsStagingMongoDbAdapterImpl;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);

		Map<String, String> map = new HashMap<>();
		map.put("HEADER", "header error message");
		map.put("FREQUENCY", "freq ma");
		map.put("AFTER_CUTOFF_DATE", "freq ma");
		map.put("FIELD", "freq ma");
		map.put("SIGNATURE", "freq ma");
		map.put("INCOMPATIBLE", "freq ma");
		map.put("RES_NOTFOUND", "freq ma");
		map.put("INTERNAL", "freq ma");

		Map<String, String> map1 = new HashMap<>();
		map1.put("signature_missing", "drtyrty");

		OBPSD2ExceptionUtility.genericErrorMessages.putAll(map);
		OBPSD2ExceptionUtility.specificErrorMessages.putAll(map1);

	}

	@Ignore
	@SuppressWarnings("unchecked")
	@Test(expected = PSD2Exception.class)
	public void testProcessDomesticPaymentConsentsWithFalse() {

		CustomDPaymentConsentsPOSTRequest request = new CustomDPaymentConsentsPOSTRequest();
		CustomPaymentStageIdentifiers customStageIdentifiers = new CustomPaymentStageIdentifiers();
		customStageIdentifiers.setPaymentSetupVersion("3.0");
		OBWriteDataDomesticConsent1 data = new OBWriteDataDomesticConsent1();
		OBDomestic1 initiation = new OBDomestic1();
		initiation.setEndToEndIdentification("endtest");
		data.setInitiation(initiation);
		request.setData(data);
		OBDomestic1InstructedAmount instructedAmount = new OBDomestic1InstructedAmount();
		instructedAmount.setAmount("1.11");
		instructedAmount.setCurrency("XYZ");
		initiation.setInstructedAmount(instructedAmount);
		OBCashAccountCreditor2 creditorAccount12 = new OBCashAccountCreditor2();
		initiation.setCreditorAccount(creditorAccount12);

		CustomDPaymentConsentsPOSTResponse response = new CustomDPaymentConsentsPOSTResponse();
		response.setConsentPorcessStatus(ProcessConsentStatusEnum.FAIL);
		OBWriteDataDomesticConsentResponse1 data1 = new OBWriteDataDomesticConsentResponse1();
		data1.setStatus(OBExternalConsentStatus1Code.REJECTED);

		OBCharge1 charges = new OBCharge1();

		List<OBCharge1> chargesList = new ArrayList<OBCharge1>();
		OBCharge1Amount amount = new OBCharge1Amount();

		amount.setAmount("1.11");
		amount.setCurrency("GBP");
		charges.setAmount(amount);
		charges.setType("Initial Amount");

		chargesList.add(charges);
		chargesList.set(0, charges);

		chargesList.get(0).setAmount(amount);
		chargesList.get(0).getAmount().getCurrency();
		data1.setCharges(chargesList);

		OBCharge1 value1 = new OBCharge1();
		value1.setType("ACTUAl");
		value1.setAmount(amount);
		Map<String, String> params = new HashMap<>();
		params.put("key1", "value1");
		when(dPaymentConsentsBankRepository.save(any(CustomDPaymentConsentsPOSTResponse.class)))
				.thenThrow(DataAccessResourceFailureException.class);
		DPaymentConsentsStagingMongoDbAdapterImpl.processDomesticPaymentConsents(request, customStageIdentifiers,
				params, OBExternalConsentStatus1Code.AWAITINGAUTHORISATION, OBExternalConsentStatus1Code.REJECTED);
		assertNotNull(request);
		assertNotNull(response);
	}


	@Ignore
	@SuppressWarnings("unchecked")
	@Test(expected = PSD2Exception.class)
	public void testProcessDomesticPaymentConsents() {

		CustomDPaymentConsentsPOSTRequest request = new CustomDPaymentConsentsPOSTRequest();
		CustomPaymentStageIdentifiers customStageIdentifiers = new CustomPaymentStageIdentifiers();
		customStageIdentifiers.setPaymentSetupVersion("3.0");
		OBWriteDataDomesticConsent1 data = new OBWriteDataDomesticConsent1();
		OBDomestic1 initiation = new OBDomestic1();
		initiation.setEndToEndIdentification("endtest");
		data.setInitiation(initiation);
		request.setData(data);
		OBDomestic1InstructedAmount instructedAmount = new OBDomestic1InstructedAmount();
		instructedAmount.setAmount("1.11");
		instructedAmount.setCurrency("XYZ");
		initiation.setInstructedAmount(instructedAmount);
		OBCashAccountCreditor2 creditorAccount12 = new OBCashAccountCreditor2();
		
		initiation.setCreditorAccount(creditorAccount12);

		CustomDPaymentConsentsPOSTResponse response = new CustomDPaymentConsentsPOSTResponse();
		response.setConsentPorcessStatus(ProcessConsentStatusEnum.FAIL);
		OBWriteDataDomesticConsentResponse1 data1 = new OBWriteDataDomesticConsentResponse1();
		data1.setStatus(OBExternalConsentStatus1Code.REJECTED);

		OBCharge1 charges = new OBCharge1();

		List<OBCharge1> chargesList = new ArrayList<OBCharge1>();
		OBCharge1Amount amount = new OBCharge1Amount();

		amount.setAmount("1.11");
		amount.setCurrency("GBP");
		charges.setAmount(amount);
		charges.setType("Initial Amount");

		chargesList.add(charges);
		chargesList.set(0, charges);

		chargesList.get(0).setAmount(amount);
		chargesList.get(0).getAmount().getCurrency();
		data1.setCharges(chargesList);

		OBCharge1 value1 = new OBCharge1();
		value1.setType("ACTUAl");
		value1.setAmount(amount);
		Map<String, String> params = new HashMap<>();
		params.put("key1", "value1");
		when(dPaymentConsentsBankRepository.save(any(CustomDPaymentConsentsPOSTResponse.class)))
				.thenThrow(DataAccessResourceFailureException.class);
		when(utility.getMockedConsentProcessExecStatus(anyString(), anyString())).thenReturn(ProcessConsentStatusEnum.PASS);
		DPaymentConsentsStagingMongoDbAdapterImpl.processDomesticPaymentConsents(request, customStageIdentifiers,
				params, OBExternalConsentStatus1Code.AWAITINGAUTHORISATION, OBExternalConsentStatus1Code.REJECTED);
		assertNotNull(request);
		assertNotNull(response);
	}
	
	@Ignore
	@SuppressWarnings({ "unchecked", "unused" })
	@Test(expected = PSD2Exception.class)
	public void testRetrieveStagedDomesticPaymentConsentsVersion3_Exception() {
		CustomPaymentStageIdentifiers customStageIdentifiers = new CustomPaymentStageIdentifiers();
		customStageIdentifiers.setPaymentSetupVersion(PaymentConstants.CMA_THIRD_VERSION);
		customStageIdentifiers.setPaymentConsentId("123456");
		customStageIdentifiers.setPaymentTypeEnum(PaymentTypeEnum.DOMESTIC_PAY);

		Map<String, String> params = new HashMap<>();
		params.put("2", "Authorised");

		CustomDPaymentConsentsPOSTResponse response = new CustomDPaymentConsentsPOSTResponse();
		response.setId(null);
		OBWriteDataDomesticConsentResponse1 data = new OBWriteDataDomesticConsentResponse1();
		data.consentId("ID1");
		OBDomestic1 initiation = new OBDomestic1();
		data.setInitiation(initiation);
		OBDomestic1InstructedAmount instructedAmount = new OBDomestic1InstructedAmount();
		instructedAmount.setAmount("1721");
		instructedAmount.setCurrency("EUR");
		initiation.setInstructedAmount(instructedAmount);
		response.setData(data);

		when(utility.isValidAmount(anyString(), anyString())).thenReturn(true);
		Mockito.when(reqAttributes.getMethodType()).thenReturn("GET");

		Mockito.when(dPaymentSetupBankRepositoryCma1.findOneByDataPaymentId(anyString()))
				.thenThrow(DataAccessResourceFailureException.class);

		Mockito.when(dPaymentConsentsBankRepository.findOneByDataConsentId(anyString()))
				.thenThrow(DataAccessResourceFailureException.class);

		DPaymentConsentsStagingMongoDbAdapterImpl.retrieveStagedDomesticPaymentConsents(customStageIdentifiers, params);
		assertNull("NO DATA PRESENT", response);
		assertEquals("INTERNAL_SERVER_ERROR", "700", instructedAmount.getAmount());
	}

	@Test
	public void testRetrieveStagedDomesticPaymentConsentsVersion1NotNull() {

		CustomPaymentStageIdentifiers customStageIdentifiers = new CustomPaymentStageIdentifiers();
		customStageIdentifiers.setPaymentSetupVersion(PaymentConstants.CMA_FIRST_VERSION);
		customStageIdentifiers.setPaymentConsentId("123456");
		customStageIdentifiers.setPaymentTypeEnum(PaymentTypeEnum.DOMESTIC_PAY);

		Map<String, String> params = new HashMap<>();
		params.put("2", "Authorised");

		CustomDomesticPaymentSetupCma1POSTResponse paymentCma1BankResource = dPaymentSetupBankRepositoryCma1
				.findOneByDataPaymentId(anyString());

		CustomDPaymentConsentsPOSTResponse response = new CustomDPaymentConsentsPOSTResponse();
		OBWriteDataDomesticConsentResponse1 data = new OBWriteDataDomesticConsentResponse1();
		data.consentId("ID1");
		OBDomestic1 initiation = new OBDomestic1();
		data.setInitiation(initiation);
		OBDomestic1InstructedAmount instructedAmount = new OBDomestic1InstructedAmount();
		instructedAmount.setAmount("1721");
		instructedAmount.setCurrency("EUR");
		initiation.setInstructedAmount(instructedAmount);
		response.setData(data);
		when(utility.isValidAmount(anyString(), anyString())).thenReturn(false);
		Mockito.when(reqAttributes.getMethodType()).thenReturn("GET");
		Mockito.when(dPaymentConsentsStageTransformer.transformCma1ToCma3Response(paymentCma1BankResource))
				.thenReturn(response);
		Mockito.when(dPaymentConsentsBankRepository.findOneByDataConsentId(anyString())).thenReturn(response);

		DPaymentConsentsStagingMongoDbAdapterImpl.retrieveStagedDomesticPaymentConsents(customStageIdentifiers, params);
		assertNotNull(response);
	}

	@Test//(expected = PSD2Exception.class)
	public void testRetrieveStagedDomesticPaymentConsentsVersion1NullResponse() {

		CustomPaymentStageIdentifiers customStageIdentifiers = new CustomPaymentStageIdentifiers();
		customStageIdentifiers.setPaymentSetupVersion(PaymentConstants.CMA_FIRST_VERSION);
		customStageIdentifiers.setPaymentConsentId("123456");
		customStageIdentifiers.setPaymentTypeEnum(PaymentTypeEnum.DOMESTIC_PAY);

		Map<String, String> params = new HashMap<>();
		params.put("2", "Authorised");

		CustomDomesticPaymentSetupCma1POSTResponse paymentCma1BankResource = dPaymentSetupBankRepositoryCma1
				.findOneByDataPaymentId(anyString());

		CustomDPaymentConsentsPOSTResponse response = new CustomDPaymentConsentsPOSTResponse();
		OBWriteDataDomesticConsentResponse1 data = new OBWriteDataDomesticConsentResponse1();
		data.consentId("ID1");
		OBDomestic1 initiation = new OBDomestic1();
		data.setInitiation(initiation);
		OBDomestic1InstructedAmount instructedAmount = new OBDomestic1InstructedAmount();
		instructedAmount.setAmount("1721");
		instructedAmount.setCurrency("EUR");
		initiation.setInstructedAmount(instructedAmount);
		response.setData(data);
		when(utility.isValidAmount(anyString(), anyString())).thenReturn(false);
		Mockito.when(reqAttributes.getMethodType()).thenReturn("GET");
		Mockito.when(dPaymentConsentsStageTransformer.transformCma1ToCma3Response(paymentCma1BankResource))
				.thenReturn(response);

		DPaymentConsentsStagingMongoDbAdapterImpl.retrieveStagedDomesticPaymentConsents(customStageIdentifiers, params);
		assertNotNull(response);
	}

	@Ignore
	@Test(expected = PSD2Exception.class)
	public void testRetrieveStagedDomesticPaymentConsentsVersion1SandboxException() {

		CustomPaymentStageIdentifiers customStageIdentifiers = new CustomPaymentStageIdentifiers();
		customStageIdentifiers.setPaymentSetupVersion(PaymentConstants.CMA_FIRST_VERSION);
		customStageIdentifiers.setPaymentConsentId("123456");
		customStageIdentifiers.setPaymentTypeEnum(PaymentTypeEnum.DOMESTIC_PAY);

		Map<String, String> params = new HashMap<>();
		params.put("2", "Authorised");

		CustomDomesticPaymentSetupCma1POSTResponse paymentCma1BankResource = dPaymentSetupBankRepositoryCma1
				.findOneByDataPaymentId(anyString());

		Mockito.when(dPaymentSetupBankRepositoryCma1.findOneByDataPaymentId(anyString()))
				.thenReturn(paymentCma1BankResource);

		CustomDPaymentConsentsPOSTResponse response = new CustomDPaymentConsentsPOSTResponse();
		OBWriteDataDomesticConsentResponse1 data = new OBWriteDataDomesticConsentResponse1();
		data.consentId("ID1");
		OBDomestic1 initiation = new OBDomestic1();
		data.setInitiation(initiation);
		OBDomestic1InstructedAmount instructedAmount = new OBDomestic1InstructedAmount();
		instructedAmount.setAmount("1721");
		instructedAmount.setCurrency("EUR");
		initiation.setInstructedAmount(instructedAmount);
		response.setData(data);
		when(utility.isValidAmount(anyString(), anyString())).thenReturn(true);
		Mockito.when(reqAttributes.getMethodType()).thenReturn("GET");
		Mockito.when(dPaymentConsentsStageTransformer.transformCma1ToCma3Response(paymentCma1BankResource))
				.thenReturn(response);
		Mockito.when(dPaymentConsentsBankRepository.findOneByDataConsentId(anyString())).thenReturn(response);
		DPaymentConsentsStagingMongoDbAdapterImpl.retrieveStagedDomesticPaymentConsents(customStageIdentifiers, params);
		assertNotNull(response);
	}
	@Test
	public void testRetrieveStagedDomesticPaymentConsentsVersion1PaymentBankResource() {

		CustomPaymentStageIdentifiers customStageIdentifiers = new CustomPaymentStageIdentifiers();
		customStageIdentifiers.setPaymentSetupVersion(PaymentConstants.CMA_FIRST_VERSION);
		customStageIdentifiers.setPaymentConsentId("123456");
		customStageIdentifiers.setPaymentTypeEnum(PaymentTypeEnum.DOMESTIC_PAY);

		Map<String, String> params = new HashMap<>();
		params.put("2", "Authorised");

		CustomDomesticPaymentSetupCma1POSTResponse paymentCma1BankResource = dPaymentSetupBankRepositoryCma1
				.findOneByDataPaymentId(anyString());

		Mockito.when(dPaymentSetupBankRepositoryCma1.findOneByDataPaymentId(anyString()))
				.thenReturn(paymentCma1BankResource);

		CustomDPaymentConsentsPOSTResponse response = new CustomDPaymentConsentsPOSTResponse();
		OBWriteDataDomesticConsentResponse1 data = new OBWriteDataDomesticConsentResponse1();
		data.consentId("ID1");
		OBDomestic1 initiation = new OBDomestic1();
		data.setInitiation(initiation);
		OBDomestic1InstructedAmount instructedAmount = new OBDomestic1InstructedAmount();
		instructedAmount.setAmount("1721");
		instructedAmount.setCurrency("EUR");
		initiation.setInstructedAmount(instructedAmount);
		response.setData(data);
		Mockito.when(dPaymentConsentsStageTransformer.transformCma1ToCma3Response(paymentCma1BankResource))
				.thenReturn(response);
		Mockito.when(dPaymentConsentsBankRepository.findOneByDataConsentId(anyString())).thenReturn(response);
		Mockito.when(reqAttributes.getMethodType()).thenReturn("POST");
		when(utility.isValidAmount(anyString(), anyString())).thenReturn(true);
		DPaymentConsentsStagingMongoDbAdapterImpl.retrieveStagedDomesticPaymentConsents(customStageIdentifiers, params);
		assertNotNull(response);
	}
	
}
