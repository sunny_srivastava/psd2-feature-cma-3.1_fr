package com.capgemini.psd2.pisp.payment.setup.mongo.db.adapter.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.capgemini.psd2.pisp.domain.CustomDPaymentConsentsPOSTResponse;

public interface DPaymentConsentsFoundationRepository extends MongoRepository<CustomDPaymentConsentsPOSTResponse, String>{

	  public CustomDPaymentConsentsPOSTResponse findOneByDataConsentId(String consentId );	
}
