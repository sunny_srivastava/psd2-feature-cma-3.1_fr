package com.capgemini.psd2.pisp.payment.setup.service.impl;

import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMethod;
import com.capgemini.psd2.exceptions.ErrorMapKeys;
import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.domain.CustomDPaymentConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomDPaymentConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.CustomPaymentSetupPlatformDetails;
import com.capgemini.psd2.pisp.domain.OBExternalConsentStatus1Code;
import com.capgemini.psd2.pisp.domain.OBPostalAddress6;
import com.capgemini.psd2.pisp.domain.OBRemittanceInformation1;
import com.capgemini.psd2.pisp.domain.OBRisk1DeliveryAddress;
import com.capgemini.psd2.pisp.domain.PaymentConsentsPlatformResource;
import com.capgemini.psd2.pisp.domain.PaymentResponseInfo;
import com.capgemini.psd2.pisp.domain.PaymentRetrieveGetRequest;
import com.capgemini.psd2.pisp.enums.PaymentTypeEnum;
import com.capgemini.psd2.pisp.payment.setup.comparator.DPaymentConsentsPayloadComparator;
import com.capgemini.psd2.pisp.payment.setup.service.DPaymentConsentsService;
import com.capgemini.psd2.pisp.processing.adapter.service.PaymentConsentProcessingAdapter;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.pisp.stage.operations.adapter.DomesticPaymentStagingAdapter;
import com.capgemini.psd2.pisp.status.PaymentConstants;
import com.capgemini.psd2.pisp.utilities.PispUtilities;
import com.capgemini.psd2.utilities.JSONUtilities;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Service
public class DPaymentConsentsServiceImpl implements DPaymentConsentsService {

	@Autowired
	private DPaymentConsentsPayloadComparator dPaymentConsentsComparator;

	@Autowired
	@Qualifier("dPaymentConsentsStagingRoutingAdapter")
	private DomesticPaymentStagingAdapter dPaymentStagingAdapter;

	@Autowired
	private PaymentConsentProcessingAdapter<CustomDPaymentConsentsPOSTRequest, CustomDPaymentConsentsPOSTResponse> paymemtRequestAdapter;

	@Value("${cmaVersion}")
	private String cmaVersion;
	
	@Autowired
	private RequestHeaderAttributes requestHeaderAttr;

	@Override
	public CustomDPaymentConsentsPOSTResponse createDomesticPaymentConsentsResource(
			CustomDPaymentConsentsPOSTRequest domesticPaymentConsentsRequest) {

		CustomDPaymentConsentsPOSTResponse paymentConsentsFoundationResponse;
		String requestType = PSD2Constants.PAYMENT_RESOURCE_IDEMPOTENT;

		PaymentConsentsPlatformResource paymentConsentsPlatformResponse = paymemtRequestAdapter.preConsentProcessFlows(
				domesticPaymentConsentsRequest, populatePlatformDetails(domesticPaymentConsentsRequest));

		/* Non Idempotent Request */
		if (paymentConsentsPlatformResponse.getPaymentConsentId() == null) {
			requestType = PSD2Constants.PAYMENT_RESOURCE_NON_IDEMPOTENT;
			/* Create stage call to Adapter */
			paymentConsentsFoundationResponse = processDomesticPaymentStagingResource(domesticPaymentConsentsRequest,
					paymentConsentsPlatformResponse.getCreatedAt());
			paymentConsentsFoundationResponse = transformIntoTPPResponse(domesticPaymentConsentsRequest, paymentConsentsFoundationResponse, paymentConsentsPlatformResponse);
		}

		/* Idempotent Request */
		else
			paymentConsentsFoundationResponse = retrieveAndCompareDomesticStageResponse(domesticPaymentConsentsRequest,
					paymentConsentsPlatformResponse);

		PaymentResponseInfo stageDetails = populateStageDetails(paymentConsentsFoundationResponse);
		stageDetails.setRequestType(requestType);

		/*
		 * Post Process Flows : Update Platform resource(for Non Idempotent), Transform
		 * response, Validate response
		 */

		/*
		 * Fix for SIT issue #2272 Tpp providing Blank remittance info, and FS is
		 * returning null, Thus in such case returning same Blank remittance info to
		 * TPP. 27.02.2019
		 */
		OBRemittanceInformation1 requestRemittanceInfo = domesticPaymentConsentsRequest.getData().getInitiation()
				.getRemittanceInformation();
		OBPostalAddress6 reqCreditorPostalAddress = domesticPaymentConsentsRequest.getData().getInitiation()
				.getCreditorPostalAddress();
		OBRisk1DeliveryAddress reqRiskDeliveryAddress = domesticPaymentConsentsRequest.getRisk().getDeliveryAddress();
		paymentConsentsFoundationResponse.getData().getInitiation().setRemittanceInformation(requestRemittanceInfo);
		paymentConsentsFoundationResponse.getData().getInitiation().setCreditorPostalAddress(reqCreditorPostalAddress);
		paymentConsentsFoundationResponse.getRisk().setDeliveryAddress(reqRiskDeliveryAddress);
		return paymemtRequestAdapter.postConsentProcessFlows(stageDetails, paymentConsentsFoundationResponse,
				paymentConsentsPlatformResponse, RequestMethod.POST.toString());

	}

	private CustomDPaymentConsentsPOSTResponse transformIntoTPPResponse(CustomDPaymentConsentsPOSTRequest scheduledPaymentRequest,
			CustomDPaymentConsentsPOSTResponse consentFoundationGenericCreateResponse,
			PaymentConsentsPlatformResource paymentConsentsPlatformResponse) {
		String request = JSONUtilities.getJSONOutPutFromObject(scheduledPaymentRequest);
		CustomDPaymentConsentsPOSTResponse paymentConsentsFoundationResponse = JSONUtilities
				.getObjectFromJSONString(PispUtilities.getObjectMapper(), request, CustomDPaymentConsentsPOSTResponse.class);

		paymentConsentsFoundationResponse.getData().setConsentId(consentFoundationGenericCreateResponse.getData().getConsentId());
		paymentConsentsFoundationResponse.getData().setCreationDateTime(paymentConsentsPlatformResponse.getCreatedAt());
		paymentConsentsFoundationResponse.getData()
				.setExpectedExecutionDateTime(consentFoundationGenericCreateResponse.getData().getExpectedExecutionDateTime() );
		paymentConsentsFoundationResponse.getData()
				.setExpectedSettlementDateTime(consentFoundationGenericCreateResponse.getData().getExpectedSettlementDateTime());
		paymentConsentsFoundationResponse.getData().setCharges(consentFoundationGenericCreateResponse.getData().getCharges() );
		paymentConsentsFoundationResponse
				.setConsentPorcessStatus(consentFoundationGenericCreateResponse.getConsentPorcessStatus());
	paymentConsentsFoundationResponse.getData().setCutOffDateTime(consentFoundationGenericCreateResponse.getData().getCutOffDateTime());
		return paymentConsentsFoundationResponse;
	}
	
	private CustomPaymentSetupPlatformDetails populatePlatformDetails(CustomDPaymentConsentsPOSTRequest request) {
		CustomPaymentSetupPlatformDetails standingConsentsPlatformDetails = new CustomPaymentSetupPlatformDetails();
		standingConsentsPlatformDetails.setPaymentType(PaymentTypeEnum.DOMESTIC_PAY);
		standingConsentsPlatformDetails.setSetupCmaVersion(cmaVersion);
		standingConsentsPlatformDetails = checkDebtorDetails(request, standingConsentsPlatformDetails);
		return standingConsentsPlatformDetails;
	}

	public CustomPaymentSetupPlatformDetails checkDebtorDetails(CustomDPaymentConsentsPOSTRequest request,
			CustomPaymentSetupPlatformDetails platformDetails) {
		String tppDebtorDetailsStatus = String.valueOf(Boolean.FALSE);
		String tppDebtorNameDetailsStatus = String.valueOf(Boolean.FALSE);
		if (request.getData() != null && request.getData().getInitiation() != null
				&& request.getData().getInitiation().getDebtorAccount() != null) {
			tppDebtorDetailsStatus = String.valueOf(Boolean.TRUE);
			if (!NullCheckUtils.isNullOrEmpty(request.getData().getInitiation().getDebtorAccount().getName()))
				tppDebtorNameDetailsStatus = String.valueOf(Boolean.TRUE);
		}

		platformDetails.setTppDebtorDetails(tppDebtorDetailsStatus);
		platformDetails.setTppDebtorNameDetails(tppDebtorNameDetailsStatus);
		return platformDetails;
	}

	private CustomDPaymentConsentsPOSTResponse processDomesticPaymentStagingResource(
			CustomDPaymentConsentsPOSTRequest domesticPaymentConsentsRequest, String setupCreationDate) {

		/* new fields added */
		domesticPaymentConsentsRequest.setCreatedOn(setupCreationDate);

		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();
		stageIdentifiers.setPaymentSetupVersion(cmaVersion);
		stageIdentifiers.setPaymentTypeEnum(PaymentTypeEnum.DOMESTIC_PAY);

		Map<String, String> paramMap = new HashMap<String, String>();
		paramMap.put(PSD2Constants.CO_RELATION_ID, requestHeaderAttr.getCorrelationId());
		return dPaymentStagingAdapter.processDomesticPaymentConsents(domesticPaymentConsentsRequest, stageIdentifiers,
				paramMap, OBExternalConsentStatus1Code.AWAITINGAUTHORISATION, OBExternalConsentStatus1Code.REJECTED);

	}

	public PaymentResponseInfo populateStageDetails(CustomDPaymentConsentsPOSTResponse domesticStagingResource) {
		PaymentResponseInfo stageDetails = new PaymentResponseInfo();
		stageDetails.setConsentProcessStatus(domesticStagingResource.getConsentPorcessStatus());
		stageDetails.setPaymentConsentId(domesticStagingResource.getData().getConsentId());
		return stageDetails;
	}

	private CustomDPaymentConsentsPOSTResponse retrieveAndCompareDomesticStageResponse(
			CustomDPaymentConsentsPOSTRequest paymentConsentsRequest,
			PaymentConsentsPlatformResource paymentConsentsPlatformResponse) {
		CustomDPaymentConsentsPOSTResponse paymentConsentsFoundationResponse;

		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();
		stageIdentifiers.setPaymentConsentId(paymentConsentsPlatformResponse.getPaymentConsentId());
		stageIdentifiers.setPaymentTypeEnum(PaymentTypeEnum.DOMESTIC_PAY);
		stageIdentifiers.setPaymentSetupVersion(paymentConsentsPlatformResponse.getSetupCmaVersion());
		
		Map<String, String> paramMap = new HashMap<String, String>();
		paramMap.put(PSD2Constants.CO_RELATION_ID, requestHeaderAttr.getCorrelationId());
		
		paymentConsentsFoundationResponse = dPaymentStagingAdapter
				.retrieveStagedDomesticPaymentConsents(stageIdentifiers, paramMap);

		if (dPaymentConsentsComparator.comparePaymentDetails(paymentConsentsFoundationResponse, paymentConsentsRequest,
				paymentConsentsPlatformResponse) > 0)
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_RESOURCE_CONSENTMISMATCH,
					ErrorMapKeys.RESOURCE_CONSENT_MISMATCH));

		return paymentConsentsFoundationResponse;
	}

	/* GET END POINT */
	@Override
	public CustomDPaymentConsentsPOSTResponse retrieveDomesticPaymentConsentsResource(
			PaymentRetrieveGetRequest request) {

		request.setPaymentTypeEnum(PaymentTypeEnum.DOMESTIC_PAY);
		/* Retrieve cma1 and cma2 domestic setup resource */
		PaymentConsentsPlatformResource paymentConsentsPlatformResponse = paymemtRequestAdapter
				.preConsentProcessGETFlow(request);

		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();
		stageIdentifiers.setPaymentConsentId(request.getConsentId());
		stageIdentifiers.setPaymentTypeEnum(PaymentTypeEnum.DOMESTIC_PAY);
		if (NullCheckUtils.isNullOrEmpty(paymentConsentsPlatformResponse.getSetupCmaVersion()))
			stageIdentifiers.setPaymentSetupVersion(PaymentConstants.CMA_FIRST_VERSION);
		else
			stageIdentifiers.setPaymentSetupVersion(paymentConsentsPlatformResponse.getSetupCmaVersion());

		CustomDPaymentConsentsPOSTResponse paymentConsentsFoundationResponse = dPaymentStagingAdapter
				.retrieveStagedDomesticPaymentConsents(stageIdentifiers, null);

		PaymentResponseInfo stageDetails = new PaymentResponseInfo();
		stageDetails.setRequestType(PSD2Constants.PAYMENT_RESOURCE_IDEMPOTENT);
		return paymemtRequestAdapter.postConsentProcessFlows(stageDetails, paymentConsentsFoundationResponse,
				paymentConsentsPlatformResponse, RequestMethod.GET.toString());
	}

}
