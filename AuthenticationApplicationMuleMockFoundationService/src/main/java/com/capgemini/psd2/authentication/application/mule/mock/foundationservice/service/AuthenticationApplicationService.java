package com.capgemini.psd2.authentication.application.mule.mock.foundationservice.service;

import java.util.Map;

import com.capgemini.psd2.authentication.application.mule.mock.foundationservice.domain.AuthenticationRequest;
import com.capgemini.psd2.authentication.application.mule.mock.foundationservice.domain.Login;
import com.capgemini.psd2.authentication.application.mule.mock.foundationservice.domain.LoginResponse;


public interface AuthenticationApplicationService {

	
	public AuthenticationRequest retrieveB365UserCredentials(String userName, String password);
	
	public LoginResponse validatenewBolUser(Login login,Map<String, String> params);

}
