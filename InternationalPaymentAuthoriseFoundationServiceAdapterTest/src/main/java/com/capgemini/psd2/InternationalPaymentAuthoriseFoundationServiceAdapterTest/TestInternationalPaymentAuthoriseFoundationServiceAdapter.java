package com.capgemini.psd2.InternationalPaymentAuthoriseFoundationServiceAdapterTest;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.psd2.foundationservice.international.payment.authorise.boi.adapter.InternationalPaymentAuthoriseFoundationServiceAdapter;
import com.capgemini.psd2.foundationservice.international.payment.authorise.boi.adapter.domain.AccountInformation;
import com.capgemini.psd2.foundationservice.international.payment.authorise.boi.adapter.domain.BrandCode3;
import com.capgemini.psd2.foundationservice.international.payment.authorise.boi.adapter.domain.Channel;
import com.capgemini.psd2.foundationservice.international.payment.authorise.boi.adapter.domain.ChannelCode1;
import com.capgemini.psd2.foundationservice.international.payment.authorise.boi.adapter.domain.DigitalUser1;
import com.capgemini.psd2.foundationservice.international.payment.authorise.boi.adapter.domain.FraudSystemResponse;
import com.capgemini.psd2.foundationservice.international.payment.authorise.boi.adapter.domain.GcisCustomertype;
import com.capgemini.psd2.foundationservice.international.payment.authorise.boi.adapter.domain.PartyBasicInformation;
import com.capgemini.psd2.foundationservice.international.payment.authorise.boi.adapter.domain.PaymentInstructionProposalAuthorisingParty;
import com.capgemini.psd2.pisp.domain.CustomIPaymentConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.OBWriteInternationalConsentResponse1;
import com.capgemini.psd2.pisp.enums.PaymentTypeEnum;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;

@RestController
@ResponseBody
class TestInternationalPaymentAuthoriseFoundationServiceAdapter {

	@Autowired
	private InternationalPaymentAuthoriseFoundationServiceAdapter iPaymentAuthoriseFoundationServiceAdapter;

	@RequestMapping(value = "/testInternationalPaymentAuthorise", method = RequestMethod.POST)
	@ResponseBody
	@ResponseStatus(HttpStatus.OK)
	public CustomIPaymentConsentsPOSTResponse internationalPaymentConsentsRetrieve(
		@RequestBody OBWriteInternationalConsentResponse1 requestObj) {
	   
		CustomPaymentStageIdentifiers customPaymentStageIdentifiers = new CustomPaymentStageIdentifiers();
		customPaymentStageIdentifiers.setPaymentConsentId("0fca37a5-40fa-41fe-91c7-9e48453d9581");
		customPaymentStageIdentifiers.setPaymentSetupVersion("3.0");
		customPaymentStageIdentifiers.setPaymentTypeEnum(PaymentTypeEnum.INTERNATIONAL_PAY);
	  	
		Map<String, String> params = new HashMap<String, String>();
		params.put("X-TRANSACTION-ID","PISP");
		params.put("X-CORRELATION-ID","BOL");
		params.put("X-SOURCE-SYSTEM","BOI999");
		params.put("X-SOURCE-USER","platform");
		params.put("X-CHANNEL-CODE","87654321");
		params.put("X-BOI-CHANNEL","BOIUK");
		params.put("tenant_id","BOIUK");
		params.put("X-PARTY-SOURCE-ID-NUMBER","BOIUK");
		params.put("X-BOI-USER","BOIUK");
		
		return iPaymentAuthoriseFoundationServiceAdapter.
				createStagingInternationalPaymentConsents(requestObj, customPaymentStageIdentifiers, params);
			
	}
}
