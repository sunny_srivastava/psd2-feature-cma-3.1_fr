package com.capgemini.psd2.InternationalPaymentAuthoriseFoundationServiceAdapterTest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("com.capgemini.psd2")
public class InternationalPaymentAuthoriseFoundationServiceAdapterTestApplication {
	public static void main(String[] args) {
		SpringApplication.run(InternationalPaymentAuthoriseFoundationServiceAdapterTestApplication.class, args);
	}
}

