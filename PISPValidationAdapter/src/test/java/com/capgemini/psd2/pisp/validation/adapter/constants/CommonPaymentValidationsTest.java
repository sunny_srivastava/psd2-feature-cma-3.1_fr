package com.capgemini.psd2.pisp.validation.adapter.constants;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.OBPSD2ExceptionUtility;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.domain.OBBranchAndFinancialInstitutionIdentification3;
import com.capgemini.psd2.pisp.domain.OBExchangeRate1;
import com.capgemini.psd2.pisp.domain.OBExchangeRateType2Code;
import com.capgemini.psd2.pisp.domain.OBExternalPaymentContext1Code;
import com.capgemini.psd2.pisp.domain.OBExternalStatus2Code;
import com.capgemini.psd2.pisp.domain.OBInternational1;
import com.capgemini.psd2.pisp.domain.OBMultiAuthorisation1;
import com.capgemini.psd2.pisp.domain.OBPostalAddress6;
import com.capgemini.psd2.pisp.domain.OBRisk1;
import com.capgemini.psd2.pisp.domain.OBRisk1DeliveryAddress;
import com.capgemini.psd2.pisp.domain.OBWriteDataInternationalConsent1;
import com.capgemini.psd2.pisp.domain.OBWriteInternationalConsent1;
import com.capgemini.psd2.pisp.utilities.PispDateUtility;

@RunWith(SpringJUnit4ClassRunner.class)
public class CommonPaymentValidationsTest {

	@InjectMocks
	private CommonPaymentValidations commonPaymentValidations;

	@Mock
	private RequestHeaderAttributes reqHeaderAtrributes;

	@Mock
	private PispDateUtility pispDateUtility;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Before
	public void initializeErrorMessages() {
		Map<String, String> genericErrorMessages = new HashMap<>();
		genericErrorMessages.put("FIELD", "freq ma");

		Map<String, String> specificErrorMessageMap = new HashMap<>();
		specificErrorMessageMap.put("FIELD", "freq ma");

		OBPSD2ExceptionUtility.genericErrorMessages.putAll(genericErrorMessages);
		OBPSD2ExceptionUtility.specificErrorMessages.putAll(specificErrorMessageMap);

	}

	@Before
	public void initializeLists() {
		String[] validSchemeNames = { "IBAN", "SortCodeAccountNumber", "PAN", "UK.OBIE.BBAN", "UK.OBIE.PAN",
				"UK.OBIE.IBAN", "UK.OBIE.SortCodeAccountNumber", "UK.OBIE.Paym" };
		List<String> validSchemeNameList = Arrays.asList(validSchemeNames);
		commonPaymentValidations.getValidSchemeNameList().addAll(validSchemeNameList);

		String[] validLocalInstruments = { "UK.OBIE.FPS", "UK.OBIE.BACS", "UK.OBIE.CHAPS", "UK.OBIE.Paym",
				"UK.OBIE.BalanceTransfer", "UK.OBIE.MoneyTransfer", "UK.OBIE.Link", "UK.OBIE.SEPACreditTransfer",
				"UK.OBIE.SEPAInstantCreditTransfer", "UK.OBIE.SWIFT", "UK.OBIE.Target2", "UK.OBIE.Euro1" };
		List<String> validLocalInstrumentList = Arrays.asList(validLocalInstruments);
		commonPaymentValidations.getValidLocalInstrumentList().addAll(validLocalInstrumentList);

		String[] validFileTypes = { "UK.OBIE.pain.001.001.08", "UK.OBIE.PaymentInitiation.3.0" };
		List<String> validFileTypeList = Arrays.asList(validFileTypes);
		commonPaymentValidations.getValidFileTypeList().addAll(validFileTypeList);
		ReflectionTestUtils.setField(commonPaymentValidations, "validPanIdentification", "\\d+");

		String[] validAgentSchemeList = { "UK.OBIE.BICFI", "BICFI" };
		List<String> validAgentSchemeNameList = Arrays.asList(validAgentSchemeList);
		commonPaymentValidations.getValidAgentSchemeList().addAll(validAgentSchemeNameList);
	}

	@Test(expected = PSD2Exception.class)
	public void validateFileType_invalidFileType_errorCodePresent() {
		commonPaymentValidations.validateFileType("UK.OBIE.Invalid", ErrorCodeEnum.PISP_FILETYPE_NOT_VALID);
	}

	@Test(expected = PSD2Exception.class)
	public void validateFileType_invalidFileType_errorCodeAbsent() {
		commonPaymentValidations.validateFileType("UK.OBIE.Invalid", null);
	}

	@Test(expected = PSD2Exception.class)
	public void validateExchangeRate_OBExchangeRateAgreed_contractIdentificationAndExchangeRateAbsent() {
		OBExchangeRate1 obExchangeRate1 = new OBExchangeRate1();
		obExchangeRate1.setRateType(OBExchangeRateType2Code.AGREED);
		commonPaymentValidations.validateExchangeRate(obExchangeRate1);
	}

	@Test(expected = PSD2Exception.class)
	public void validateExchangeRate_OBExchangeRateActual() {
		OBExchangeRate1 obExchangeRate1 = new OBExchangeRate1();
		obExchangeRate1.setRateType(OBExchangeRateType2Code.ACTUAL);
		obExchangeRate1.setContractIdentification("contractIdentification");
		obExchangeRate1.setExchangeRate(new BigDecimal(1));
		commonPaymentValidations.validateExchangeRate(obExchangeRate1);
	}

	@Test(expected = PSD2Exception.class)
	public void validateExchangeRate_OBExchangeRateActual_contractIdentificationPresent() {
		OBExchangeRate1 obExchangeRate1 = new OBExchangeRate1();
		obExchangeRate1.setRateType(OBExchangeRateType2Code.ACTUAL);
		obExchangeRate1.setContractIdentification("contractIdentification");
		commonPaymentValidations.validateExchangeRate(obExchangeRate1);
	}

	@Test(expected = PSD2Exception.class)
	public void validateExchangeRate_OBExchangeRateActual_exchangeRatePresent() {
		OBExchangeRate1 obExchangeRate1 = new OBExchangeRate1();
		obExchangeRate1.setRateType(OBExchangeRateType2Code.ACTUAL);
		obExchangeRate1.setExchangeRate(new BigDecimal(1));
		commonPaymentValidations.validateExchangeRate(obExchangeRate1);
	}

	@Test(expected = PSD2Exception.class)
	public void validateExchangeRate_OBExchangeRateIndicative() {
		OBExchangeRate1 obExchangeRate1 = new OBExchangeRate1();
		obExchangeRate1.setRateType(OBExchangeRateType2Code.INDICATIVE);
		obExchangeRate1.setContractIdentification("contractIdentification");
		obExchangeRate1.setExchangeRate(new BigDecimal(1));
		commonPaymentValidations.validateExchangeRate(obExchangeRate1);
	}

	@Test(expected = PSD2Exception.class)
	public void isValidCurrency_emptyCurrencyCode_errorCodePresent() {
		commonPaymentValidations.isValidCurrency("", OBErrorCodeEnum.UK_OBIE_UNSUPPORTED_CURRENCY);
	}

	@Test(expected = PSD2Exception.class)
	public void isValidCurrency_emptyCurrencyCode_errorCodeAbsent() {
		commonPaymentValidations.isValidCurrency("", null);
	}

	@Test
	public void isValidCurrency_validCurrencyCode() {
		Assert.assertTrue(commonPaymentValidations.isValidCurrency("INR", null));
	}

	@Test(expected = PSD2Exception.class)
	public void isValidCurrency_invalidCurrencyCode_errorCodePresent() {
		commonPaymentValidations.isValidCurrency("Invalid", OBErrorCodeEnum.UK_OBIE_UNSUPPORTED_CURRENCY);
	}

	@Test(expected = PSD2Exception.class)
	public void isValidCurrency_invalidCurrencyCode_errorCodeAbsent() {
		commonPaymentValidations.isValidCurrency("Invalid", null);
	}

	@Test(expected = PSD2Exception.class)
	public void validateIdentification_invalidLocalInstrument_emptyIdentification() {
		commonPaymentValidations.validateIdentification("");
	}

	@Test(expected = PSD2Exception.class)
	public void validateLocalInstrument_invalidLocalInstrument_errorCodePresent() {
		commonPaymentValidations.validateLocalInstrument("UK.OBIE.Invalid",
				OBErrorCodeEnum.UK_OBIE_UNSUPPORTED_LOCALINSTRUMENT);
	}

	@Test(expected = PSD2Exception.class)
	public void validateLocalInstrument_invalidLocalInstrument_errorCodeAbsent() {
		commonPaymentValidations.validateLocalInstrument("UK.OBIE.Invalid", null);
	}

	@Test(expected = PSD2Exception.class)
	public void validateHeaders_emptyIdempotencyKey() {
		Mockito.when(reqHeaderAtrributes.getIdempotencyKey()).thenReturn("");
		commonPaymentValidations.validateHeaders();
	}

	// Pass invalid Idempotency Key
	@Test(expected = PSD2Exception.class)
	public void validateHeaders_invalidIdempotencyKey() {
		Mockito.when(reqHeaderAtrributes.getIdempotencyKey()).thenReturn("");
		commonPaymentValidations.validateHeaders();
	}

	@Test(expected = PSD2Exception.class)
	public void validateHeaders_invalidIdempotencyKeyLength() {
		Mockito.when(reqHeaderAtrributes.getIdempotencyKey()).thenReturn("zsddxdfhdhSDGHre000zsddxdfhdhSDGHre000zsd");
		commonPaymentValidations.validateHeaders();
	}

	@Test(expected = PSD2Exception.class)
	public void validateSchemeNameWithIdentification_invalidInputScheme_errorCodePresent() {
		commonPaymentValidations.validateSchemeNameWithIdentification("Invalid", "",
				OBErrorCodeEnum.UK_OBIE_UNSUPPORTED_SCHEME);
	}

	@Test(expected = PSD2Exception.class)
	public void validateSchemeNameWithIdentification_invalidInputScheme_errorCodeAbsent() {
		commonPaymentValidations.validateSchemeNameWithIdentification("Invalid", "", null);
	}

	@Test(expected = PSD2Exception.class)
	public void validateSchemeNameWithIdentification_invalidIBAN_errorCodePresent() {
		commonPaymentValidations.validateSchemeNameWithIdentification(PaymentSetupConstants.IBAN, "Invalid",
				OBErrorCodeEnum.UK_OBIE_UNSUPPORTED_ACCOUNTIDENTIFIER);
	}

	@Test(expected = PSD2Exception.class)
	public void validateSchemeNameWithIdentification_invalidIBAN_errorCodeAbsent() {
		commonPaymentValidations.validateSchemeNameWithIdentification(PaymentSetupConstants.IBAN, "Invalid", null);
	}

	@Test(expected = PSD2Exception.class)
	public void validateSchemeNameWithIdentification_invalidSortCodeAccountNumber_errorCodeAbsent() {
		ReflectionTestUtils.setField(commonPaymentValidations, "sortCodeAccountNumberPattern", "^[0-9]{6}[0-9]{8}$");
		commonPaymentValidations.validateSchemeNameWithIdentification(PaymentSetupConstants.SORTCODEACCOUNTNUMBER,
				"Invalid", null);
	}

	@Test(expected = PSD2Exception.class)
	public void validateSchemeNameWithIdentification_invalidSortCodeAccountNumber_errorCodePresent() {
		ReflectionTestUtils.setField(commonPaymentValidations, "sortCodeAccountNumberPattern", "^[0-9]{6}[0-9]{8}$");
		commonPaymentValidations.validateSchemeNameWithIdentification(PaymentSetupConstants.SORTCODEACCOUNTNUMBER,
				"Invalid", OBErrorCodeEnum.UK_OBIE_UNSUPPORTED_ACCOUNTIDENTIFIER);
	}

	@Test(expected = PSD2Exception.class)
	public void validateSchemeNameWithIdentification_invalidBBAN_errorCodePresent() {
		commonPaymentValidations.validateSchemeNameWithIdentification(PaymentSetupConstants.UK_OBIE_BBAN, "Invalid",
				OBErrorCodeEnum.UK_OBIE_UNSUPPORTED_ACCOUNTIDENTIFIER);
	}

	@Test(expected = PSD2Exception.class)
	public void validateSchemeNameWithIdentification_invalidBBAN_errorCodeAbsent() {
		OBErrorCodeEnum errorCodeEnum = null;
		commonPaymentValidations.validateSchemeNameWithIdentification(PaymentSetupConstants.UK_OBIE_BBAN, "Invalid",
				errorCodeEnum);
	}

	@Test(expected = PSD2Exception.class)
	public void validateSchemeNameWithIdentification_invalidPAN() {
		commonPaymentValidations.validateSchemeNameWithIdentification(PaymentSetupConstants.PAN, "Invalid",
				OBErrorCodeEnum.UK_OBIE_UNSUPPORTED_ACCOUNTIDENTIFIER);
	}

	@Test(expected = PSD2Exception.class)
	public void validateSchemeNameWithIdentification_invalidUKPAN() {
		commonPaymentValidations.validateSchemeNameWithIdentification(PaymentSetupConstants.UK_OBIE_PAN, "Invalid",
				OBErrorCodeEnum.UK_OBIE_UNSUPPORTED_ACCOUNTIDENTIFIER);
	}

	@Test(expected = PSD2Exception.class)
	public void validateSchemeNameWithIdentification_invalidPAN_InRequest() {
		commonPaymentValidations.validateSchemeNameWithIdentification(PaymentSetupConstants.UK_OBIE_PAN, "Invalid",
				null);
	}

	@Test(expected = PSD2Exception.class)
	public void validateSchemeNameWithIdentification_invalidUKPAN_InRequest() {
		commonPaymentValidations.validateSchemeNameWithIdentification(PaymentSetupConstants.PAN, "Invalid", null);
	}

	@Test
	public void validateSchemeNameWithIdentification_invalidPAYM() {
		commonPaymentValidations.validateSchemeNameWithIdentification(PaymentSetupConstants.UK_OBIE_PAYM, "Invalid",
				OBErrorCodeEnum.UK_OBIE_UNSUPPORTED_ACCOUNTIDENTIFIER);
	}

	@Test(expected = PSD2Exception.class)
	public void validateSchemeNameWithSecondaryIdentification_invalidInputScheme() {
		commonPaymentValidations.validateSchemeNameWithSecondaryIdentification("Invalid", "");
	}

	@Test(expected = PSD2Exception.class)
	public void validateSchemeNameWithSecondaryIdentification_validInputScheme_emptySecondaryIdentification() {
		commonPaymentValidations
				.validateSchemeNameWithSecondaryIdentification(PaymentSetupConstants.SORTCODEACCOUNTNUMBER, "");
	}

	@Test(expected = PSD2Exception.class)
	public void validateSchemeNameWithSecondaryIdentification_validInputScheme_invalidSecondaryIdentificationLength() {
		commonPaymentValidations.validateSchemeNameWithSecondaryIdentification(
				PaymentSetupConstants.SORTCODEACCOUNTNUMBER, "zsddxdfhdhSDGHre000zsddxdfhdhSDGHre");
	}

	@Test(expected = PSD2Exception.class)
	public void validateDomesticRiskDeliveryAddress_invalidAddressLine() {
		OBRisk1DeliveryAddress obRisk1DeliveryAddress = new OBRisk1DeliveryAddress();
		List<String> addressLine = new ArrayList<>();
		addressLine.add("");
		obRisk1DeliveryAddress.setAddressLine(addressLine);
		commonPaymentValidations.validateDomesticRiskDeliveryAddress(obRisk1DeliveryAddress,
				OBErrorCodeEnum.UK_OBIE_FIELD_INVALID);
	}

	@Test(expected = PSD2Exception.class)
	public void validateDomesticRiskDeliveryAddress_invalidAddressLineLength() {
		OBRisk1DeliveryAddress obRisk1DeliveryAddress = new OBRisk1DeliveryAddress();
		List<String> addressLine = new ArrayList<>();
		addressLine.add("zsddxdfhdhSDGHre000zsddxdfhdhSDGHre000zsddxdfhdhSDGHre000zsddxdfhdhSDGH");
		obRisk1DeliveryAddress.setAddressLine(addressLine);
		commonPaymentValidations.validateDomesticRiskDeliveryAddress(obRisk1DeliveryAddress,
				OBErrorCodeEnum.UK_OBIE_FIELD_INVALID);
	}

	@Ignore // As validateDomesticCountrySubDivision() method is not being invoked
	@Test(expected = PSD2Exception.class)
	public void validateDomesticRiskDeliveryAddress_invalidSubDivisionLength() {
		OBRisk1DeliveryAddress obRisk1DeliveryAddress = new OBRisk1DeliveryAddress();
		obRisk1DeliveryAddress.setAddressLine(null);
		String countrySubDivision = "zsddxdfhdhSDGHre000";
		obRisk1DeliveryAddress.setCountrySubDivision(countrySubDivision);
		commonPaymentValidations.validateDomesticRiskDeliveryAddress(obRisk1DeliveryAddress,
				OBErrorCodeEnum.UK_OBIE_FIELD_INVALID);
	}

	@Ignore // As validateDomesticCountrySubDivision() method is not being invoked
	@Test(expected = PSD2Exception.class)
	public void validateDomesticRiskDeliveryAddress_invalidCountryCode() {
		OBRisk1DeliveryAddress obRisk1DeliveryAddress = new OBRisk1DeliveryAddress();
		obRisk1DeliveryAddress.setAddressLine(null);
		obRisk1DeliveryAddress.setCountrySubDivision(null);
		obRisk1DeliveryAddress.setCountry("ZZ");
		commonPaymentValidations.validateDomesticRiskDeliveryAddress(obRisk1DeliveryAddress,
				OBErrorCodeEnum.UK_OBIE_FIELD_INVALID);
	}

	@Test(expected = PSD2Exception.class)
	public void validateDomesticCreditorPostalAddress_invalidAddressLine() {
		OBPostalAddress6 obPostalAddress6 = new OBPostalAddress6();
		List<String> addressLine = new ArrayList<>();
		addressLine.add("");
		obPostalAddress6.setAddressLine(addressLine);
		commonPaymentValidations.validateDomesticCreditorPostalAddress(obPostalAddress6, null);
	}

	@Test(expected = PSD2Exception.class)
	public void validateDomesticCreditorPostalAddress_invalidAddressLineLength() {
		OBPostalAddress6 obPostalAddress6 = new OBPostalAddress6();
		List<String> addressLine = new ArrayList<>();
		addressLine.add("zsddxdfhdhSDGHre000zsddxdfhdhSDGHre000zsddxdfhdhSDGHre000zsddxdfhdhSDGH");
		obPostalAddress6.setAddressLine(addressLine);
		commonPaymentValidations.validateDomesticCreditorPostalAddress(obPostalAddress6, null);
	}

	@Test(expected = PSD2Exception.class)
	public void validateDomesticCreditorPostalAddress_invalidSubDivisionLength() {
		OBPostalAddress6 obPostalAddress6 = new OBPostalAddress6();
		obPostalAddress6.setAddressLine(null);
		obPostalAddress6.setCountrySubDivision("zsddxdfhdhSDGHre000zsddxdfhdhSDGHre000");
		commonPaymentValidations.validateDomesticCreditorPostalAddress(obPostalAddress6, null);
	}

	@Test(expected = PSD2Exception.class)
	public void validateDomesticCreditorPostalAddress_invalidCountryCode() {
		OBPostalAddress6 obPostalAddress6 = new OBPostalAddress6();
		obPostalAddress6.setAddressLine(null);
		obPostalAddress6.setCountrySubDivision(null);
		obPostalAddress6.setCountry("ZZ");
		commonPaymentValidations.validateDomesticCreditorPostalAddress(obPostalAddress6, null);
	}

	@Test(expected = PSD2Exception.class)
	public void validateInternationalRiskDeliveryAddress_invalidAddressLine() {
		OBRisk1DeliveryAddress obRisk1DeliveryAddress = new OBRisk1DeliveryAddress();
		List<String> addressLine = new ArrayList<>();
		addressLine.add("");
		obRisk1DeliveryAddress.setAddressLine(addressLine);
		commonPaymentValidations.validateInternationalRiskDeliveryAddress(obRisk1DeliveryAddress,
				OBErrorCodeEnum.UK_OBIE_FIELD_INVALID);
	}

	@Test(expected = PSD2Exception.class)
	public void validateInternationalRiskDeliveryAddress_invalidAddressLineLength() {
		OBRisk1DeliveryAddress obRisk1DeliveryAddress = new OBRisk1DeliveryAddress();
		List<String> addressLine = new ArrayList<>();
		addressLine.add("zsddxdfhdhSDGHre000zsddxdfhdhSDGHre000zsddxdfhdhSDGHre000zsddxdfhdhSDGH");
		obRisk1DeliveryAddress.setAddressLine(addressLine);
		commonPaymentValidations.validateInternationalRiskDeliveryAddress(obRisk1DeliveryAddress,
				OBErrorCodeEnum.UK_OBIE_FIELD_INVALID);
	}

	@Ignore // As validateDomesticCountrySubDivision() method is not being invoked
	@Test(expected = PSD2Exception.class)
	public void validateInternationalRiskDeliveryAddress_invalidSubDivisionLength() {
		OBRisk1DeliveryAddress obRisk1DeliveryAddress = new OBRisk1DeliveryAddress();
		obRisk1DeliveryAddress.setAddressLine(null);
		String countrySubDivision = "zsddxdfhdhSDGHre000";
		obRisk1DeliveryAddress.setCountrySubDivision(countrySubDivision);
		commonPaymentValidations.validateInternationalRiskDeliveryAddress(obRisk1DeliveryAddress, null);
	}

	@Test(expected = PSD2Exception.class)
	public void validateInternationalRiskDeliveryAddress_invalidCountryCode() {
		OBRisk1DeliveryAddress obRisk1DeliveryAddress = new OBRisk1DeliveryAddress();
		obRisk1DeliveryAddress.setAddressLine(null);
		obRisk1DeliveryAddress.setCountrySubDivision(null);
		obRisk1DeliveryAddress.setCountry("ZZ");
		commonPaymentValidations.validateInternationalRiskDeliveryAddress(obRisk1DeliveryAddress,
				OBErrorCodeEnum.UK_OBIE_FIELD_INVALID);
	}

	@Test(expected = PSD2Exception.class)
	public void validatePaymentContext_merchantCategoryCodeAbsent() {
		OBRisk1 obRisk1 = new OBRisk1();
		obRisk1.setPaymentContextCode(OBExternalPaymentContext1Code.ECOMMERCEGOODS);
		commonPaymentValidations.validatePaymentContext(obRisk1);
	}

	@Test(expected = PSD2Exception.class)
	public void validatePaymentContext_MerchantCustomerIdentificationAbsent() {
		OBRisk1 obRisk1 = new OBRisk1();
		obRisk1.setMerchantCategoryCode("merchantCategoryCode");
		obRisk1.setPaymentContextCode(OBExternalPaymentContext1Code.ECOMMERCESERVICES);
		commonPaymentValidations.validatePaymentContext(obRisk1);
	}

	@Test(expected = PSD2Exception.class)
	public void validatePaymentContext_deliveryAddressAbsent() {
		OBRisk1 obRisk1 = new OBRisk1();
		obRisk1.setPaymentContextCode(OBExternalPaymentContext1Code.ECOMMERCEGOODS);
		obRisk1.setMerchantCategoryCode("merchantCategoryCode");
		obRisk1.setMerchantCustomerIdentification("merchantCustomerIdentification");
		commonPaymentValidations.validatePaymentContext(obRisk1);
	}

	@Test(expected = PSD2Exception.class)
	public void validateInternationalPaymentContext_merchantCategoryCodeAbsent() {
		OBRisk1 obRisk1 = new OBRisk1();
		obRisk1.setPaymentContextCode(OBExternalPaymentContext1Code.ECOMMERCEGOODS);
		commonPaymentValidations.validateInternationalPaymentContext(obRisk1);
	}

	@Test(expected = PSD2Exception.class)
	public void validateInternationalPaymentContext_MerchantCustomerIdentificationAbsent() {
		OBRisk1 obRisk1 = new OBRisk1();
		obRisk1.setMerchantCategoryCode("merchantCategoryCode");
		obRisk1.setPaymentContextCode(OBExternalPaymentContext1Code.ECOMMERCESERVICES);
		commonPaymentValidations.validateInternationalPaymentContext(obRisk1);
	}

	@Test(expected = PSD2Exception.class)
	public void validateInternationalPaymentContext_deliveryAddressAbsent() {
		OBRisk1 obRisk1 = new OBRisk1();
		obRisk1.setPaymentContextCode(OBExternalPaymentContext1Code.ECOMMERCEGOODS);
		obRisk1.setMerchantCategoryCode("merchantCategoryCode");
		obRisk1.setMerchantCustomerIdentification("merchantCustomerIdentification");
		commonPaymentValidations.validateInternationalPaymentContext(obRisk1);
	}

	@Test(expected = PSD2Exception.class)
	public void validateCompletionDateTime_expiredDateTime() {
		String transformedDate = "1986-03-20T06:06:06+00:00";
		when(pispDateUtility.transformDateTimeInRequest(anyString())).thenReturn(transformedDate);
		commonPaymentValidations.validateDateTime(transformedDate);
	}

	@Test
	public void validateandParseDateTimeForResponse() {
		commonPaymentValidations.validateAndParseDateTimeFormatForResponse("1981-03-20T06:06:06Z");
	}

	@Test
	public void validateandParseDateTime() {
		commonPaymentValidations.validateAndParseDateTimeFormat("1981-03-20T06:06:06Z");
	}

	@Test
	public void testValidateDomesticCountrySubDivision() {
		List<String> countrySubDivision = new ArrayList<>();
		commonPaymentValidations.validateDomesticCountrySubDivision(countrySubDivision,
				OBErrorCodeEnum.UK_OBIE_FIELD_EXPECTED);
	}

	@Test(expected = PSD2Exception.class)
	public void testValidateDomesticCountrySubDivision_ErrorCodeEnumNull() {
		List<String> countrySubDivision = new ArrayList<>();
		String countrySubDivision1 = "abcdefghijklmnopqrstuvwxyzqwertyuiop";
		countrySubDivision.add(countrySubDivision1);
		commonPaymentValidations.validateDomesticCountrySubDivision(countrySubDivision, null);
	}

	@Test(expected = PSD2Exception.class)
	public void validateInstructionPriority_localInstrumentAbsent() {
		OBInternational1 initiation = new OBInternational1();
		initiation.setInstructionPriority(null);
		initiation.setLocalInstrument("localInstrument");
		initiation.setExchangeRateInformation(new OBExchangeRate1());
		initiation.getExchangeRateInformation().setUnitCurrency("EUR");
		initiation.getExchangeRateInformation().setRateType(OBExchangeRateType2Code.AGREED);
		OBWriteDataInternationalConsent1 data = new OBWriteDataInternationalConsent1();
		data.setInitiation(initiation);
		OBWriteInternationalConsent1 consent = new OBWriteInternationalConsent1();
		consent.setData(data);
		commonPaymentValidations.validateInstructionPriority(consent);
	}

	@Test
	public void validateInstructionPriority_ACTUALExchangeRateType() {
		OBExchangeRate1 exchangeRate = new OBExchangeRate1();
		exchangeRate.setRateType(OBExchangeRateType2Code.ACTUAL);
		OBInternational1 initiation = new OBInternational1();
		initiation.setExchangeRateInformation(null);
		OBWriteDataInternationalConsent1 data = new OBWriteDataInternationalConsent1();
		data.setInitiation(initiation);
		OBWriteInternationalConsent1 consent = new OBWriteInternationalConsent1();
		consent.setData(data);
		commonPaymentValidations.validateInstructionPriority(consent);
	}

	@Test(expected = PSD2Exception.class)
	public void testvalidateDomesticRiskDeliveryAddress() {
		OBRisk1DeliveryAddress deliveryAddress = null;
		List<String> addressLineList = new ArrayList<>();
		String addressLine = null;
		addressLineList.add(addressLine);

		commonPaymentValidations.validateISOCountry("GBP", OBErrorCodeEnum.UK_OBIE_FIELD_EXPECTED);
		commonPaymentValidations.validateDomesticAddressLine(addressLineList, OBErrorCodeEnum.UK_OBIE_FIELD_EXPECTED);
		commonPaymentValidations.validateDomesticRiskDeliveryAddress(deliveryAddress,
				OBErrorCodeEnum.UK_OBIE_FIELD_EXPECTED);
	}

	@Test(expected = PSD2Exception.class)
	public void testvalidateDomesticRiskDeliveryAddress_NullErrorCode() {
		OBRisk1DeliveryAddress deliveryAddress = null;
		List<String> addressLineList = new ArrayList<>();
		String addressLine = null;
		addressLineList.add(addressLine);

		commonPaymentValidations.validateISOCountry("GBP", null);
		commonPaymentValidations.validateDomesticAddressLine(addressLineList, null);
		commonPaymentValidations.validateDomesticRiskDeliveryAddress(deliveryAddress,
				OBErrorCodeEnum.UK_OBIE_FIELD_EXPECTED);
	}

	@Test(expected = PSD2Exception.class)
	public void testvalidateDomesticRiskDeliveryAddress_AddressLine() {
		OBRisk1DeliveryAddress deliveryAddress = null;
		List<String> addressLineList = new ArrayList<>();
		String addressLine = "abcdefghijklmnopqrstuvwxyzqwertyuiopabcdefghijklmnopqrstuvwxyzqwertyuiop";
		addressLineList.add(addressLine);

		commonPaymentValidations.validateISOCountry("GBP", null);
		commonPaymentValidations.validateDomesticAddressLine(addressLineList, null);
		commonPaymentValidations.validateDomesticRiskDeliveryAddress(deliveryAddress,
				OBErrorCodeEnum.UK_OBIE_FIELD_EXPECTED);
	}

	@Test
	public void testvalidateMerchantCategoryCode_Null() {
		commonPaymentValidations.validateMerchantCategoryCode(null, null);
	}

	@Test(expected = PSD2Exception.class)
	public void testvalidateMerchantCategoryCode_NotNull_NotEqual() {
		String merchantCategoryCode = "equal";
		String merchantCategoryCodeRegexValidator = "notequal";
		commonPaymentValidations.validateMerchantCategoryCode(merchantCategoryCode, merchantCategoryCodeRegexValidator);
	}

	@Test
	public void testvalidateMerchantCategoryCode_NotNull_Equal() {
		String merchantCategoryCode = "equal";
		String merchantCategoryCodeRegexValidator = "equal";
		commonPaymentValidations.validateMerchantCategoryCode(merchantCategoryCode, merchantCategoryCodeRegexValidator);
	}

	@Test
	public void testvalidatePaymentContextforResponse() {
		OBRisk1 obRisk1 = new OBRisk1();
		obRisk1.setPaymentContextCode(null);
		commonPaymentValidations.validatePaymentContextforResponse(obRisk1);
	}

	@Test(expected = PSD2Exception.class)
	public void testvalidatePaymentContextforResponse_MerchantCode() {
		OBRisk1 obRisk1 = new OBRisk1();
		obRisk1.setMerchantCategoryCode(null);
		obRisk1.setPaymentContextCode(OBExternalPaymentContext1Code.ECOMMERCEGOODS);
		commonPaymentValidations.validatePaymentContextforResponse(obRisk1);
	}

	@Test(expected = PSD2Exception.class)
	public void testvalidatePaymentContextforResponse_DeliveryAddress() {
		OBRisk1 obRisk1 = new OBRisk1();
		obRisk1.setMerchantCategoryCode("Merchant");
		obRisk1.setPaymentContextCode(OBExternalPaymentContext1Code.ECOMMERCEGOODS);
		commonPaymentValidations.validatePaymentContextforResponse(obRisk1);
	}

	@Test(expected = PSD2Exception.class)
	public void validateCreditorAgent_blankSchemeName() {

		OBBranchAndFinancialInstitutionIdentification3 creditorAgent = new OBBranchAndFinancialInstitutionIdentification3();
		creditorAgent.setSchemeName("");
		creditorAgent.setIdentification("Test");
		commonPaymentValidations.validateCreditorAgent(creditorAgent, null);

	}

	@Test(expected = PSD2Exception.class)
	public void validateCreditorAgent_nullSchemeName() {

		OBBranchAndFinancialInstitutionIdentification3 creditorAgent = new OBBranchAndFinancialInstitutionIdentification3();
		creditorAgent.setSchemeName(null);
		creditorAgent.setIdentification("Test");
		commonPaymentValidations.validateCreditorAgent(creditorAgent, null);

	}

	@Test(expected = PSD2Exception.class)
	public void validateCreditorAgent_blankId() {

		OBBranchAndFinancialInstitutionIdentification3 creditorAgent = new OBBranchAndFinancialInstitutionIdentification3();
		creditorAgent.setSchemeName("BICFI");
		creditorAgent.setIdentification("");
		commonPaymentValidations.validateCreditorAgent(creditorAgent, null);

	}

	@Test(expected = PSD2Exception.class)
	public void validateCreditorAgent_nullId() {

		OBBranchAndFinancialInstitutionIdentification3 creditorAgent = new OBBranchAndFinancialInstitutionIdentification3();
		creditorAgent.setSchemeName("BICFI");
		creditorAgent.setIdentification(null);
		commonPaymentValidations.validateCreditorAgent(creditorAgent, null);

	}

	@Test(expected = PSD2Exception.class)
	public void validateCreditorAgent() {
		OBBranchAndFinancialInstitutionIdentification3 creditorAgent = new OBBranchAndFinancialInstitutionIdentification3();
		creditorAgent.setSchemeName("BICFI");
		creditorAgent.setIdentification("");
		commonPaymentValidations.validateCreditorAgent(creditorAgent, null);

	}

	@Test(expected = PSD2Exception.class)
	public void validateCreditorAgent_InvalidSchemeName() {
		OBBranchAndFinancialInstitutionIdentification3 creditorAgent = new OBBranchAndFinancialInstitutionIdentification3();
		creditorAgent.setSchemeName("IBAN");
		commonPaymentValidations.validateCreditorAgent(creditorAgent, null);

	}

	@Test(expected = PSD2Exception.class)
	public void validateCreditorAgent_NullCreditorAgent() {
		OBBranchAndFinancialInstitutionIdentification3 creditorAgent = new OBBranchAndFinancialInstitutionIdentification3();
		commonPaymentValidations.validateCreditorAgent(creditorAgent, null);

	}

	@Test(expected = PSD2Exception.class)
	public void validateCreditorAgent_NullCreditorAgentErrorCode() {
		OBBranchAndFinancialInstitutionIdentification3 creditorAgent = new OBBranchAndFinancialInstitutionIdentification3();
		commonPaymentValidations.validateCreditorAgent(creditorAgent, OBErrorCodeEnum.UK_OBIE_FIELD_MISSING);

	}

	@Test(expected = PSD2Exception.class)
	public void validateCreditorAgent_BlankSchemeName() {
		OBBranchAndFinancialInstitutionIdentification3 creditorAgent = new OBBranchAndFinancialInstitutionIdentification3();
		creditorAgent.setSchemeName("");
		creditorAgent.setIdentification("");
		commonPaymentValidations.validateCreditorAgent(creditorAgent, null);

	}

	@Test

	public void validateCreditorAgent_CreditorAgentFull() {

		OBBranchAndFinancialInstitutionIdentification3 creditorAgent = new OBBranchAndFinancialInstitutionIdentification3();
		creditorAgent.setSchemeName("BICFI");
		creditorAgent.setIdentification("Testing1234");
		commonPaymentValidations.validateCreditorAgent(creditorAgent, null);

	}

	@Test(expected = PSD2Exception.class)
	public void validateCreditorAgent_CreditorAgentWithNameErrorCode() {

		OBBranchAndFinancialInstitutionIdentification3 creditorAgent = new OBBranchAndFinancialInstitutionIdentification3();
		creditorAgent.setSchemeName("BICFI");
		creditorAgent.setIdentification("Testing1234");
		creditorAgent.setName("Test Name");
		commonPaymentValidations.validateCreditorAgent(creditorAgent, OBErrorCodeEnum.UK_OBIE_FIELD_EXPECTED);

	}

	@Test
	public void validateCreditorAgent_CreditorAgentNameAndPAddFull() {

		OBBranchAndFinancialInstitutionIdentification3 creditorAgent = new OBBranchAndFinancialInstitutionIdentification3();
		creditorAgent.setSchemeName("BICFI");
		creditorAgent.setIdentification("Testing1234");
		creditorAgent.setName("Testing Name");
		OBPostalAddress6 obPostalAddress6 = new OBPostalAddress6();
		List<String> addressLine = new ArrayList<>();
		addressLine.add("zsddxdfhdhSDGHre000zsddxdfhdhSDGHre000zsddxdfhdhSDGHre000zsddxdfhdhSDGH");
		obPostalAddress6.setAddressLine(addressLine);
		creditorAgent.setPostalAddress(obPostalAddress6);
		commonPaymentValidations.validateCreditorAgent(creditorAgent, null);
	}

	@Test(expected = PSD2Exception.class)
	public void validateCreditorAgent_CreditorAgentPAddFull() {

		OBBranchAndFinancialInstitutionIdentification3 creditorAgent = new OBBranchAndFinancialInstitutionIdentification3();
		creditorAgent.setSchemeName("BICFI");
		creditorAgent.setIdentification("Testing1234");
		OBPostalAddress6 obPostalAddress6 = new OBPostalAddress6();
		List<String> addressLine = new ArrayList<>();
		addressLine.add("zsddxdfhdhSDGHre000zsddxdfhdhSDGHre000zsddxdfhdhSDGHre000zsddxdfhdhSDGH");
		obPostalAddress6.setAddressLine(addressLine);
		creditorAgent.setPostalAddress(obPostalAddress6);
		commonPaymentValidations.validateCreditorAgent(creditorAgent, null);
	}

	@Test(expected = PSD2Exception.class)
	public void validateMultiAuth() {
		OBMultiAuthorisation1 multiAuth = new OBMultiAuthorisation1();
		commonPaymentValidations.validateMultiAuth(multiAuth);
	}

	@Test
	public void validateMultiAuth_Null() {

		OBMultiAuthorisation1 multiAuth = null;
		commonPaymentValidations.validateMultiAuth(multiAuth);
	}

	@Test
	public void validateMultiAuth_NumberReqEqNumberRec_AwaitingAuth() {

		OBMultiAuthorisation1 multiAuth = new OBMultiAuthorisation1();
		multiAuth.setStatus(OBExternalStatus2Code.AWAITINGFURTHERAUTHORISATION);
		multiAuth.setNumberReceived(Integer.valueOf(10));
		multiAuth.setNumberRequired(Integer.valueOf(10));
		multiAuth.setExpirationDateTime("1986-03-20T06:06:06+00:00");
		multiAuth.setLastUpdateDateTime("1986-03-20T06:06:06+00:00");
		commonPaymentValidations.validateMultiAuth(multiAuth);
	}

	@Test
	public void validateMultiAuth_NumberReqEqNumberRec() {

		OBMultiAuthorisation1 multiAuth = new OBMultiAuthorisation1();
		multiAuth.setStatus(OBExternalStatus2Code.AUTHORISED);
		multiAuth.setNumberReceived(Integer.valueOf(10));
		multiAuth.setNumberRequired(Integer.valueOf(10));
		commonPaymentValidations.validateMultiAuth(multiAuth);
	}

	@Test
	public void validateMultiAuth_NumberReqLtNumberRec() {
		OBMultiAuthorisation1 multiAuth = new OBMultiAuthorisation1();
		multiAuth.setStatus(OBExternalStatus2Code.AUTHORISED);
		multiAuth.setNumberReceived(Integer.valueOf(15));
		multiAuth.setNumberRequired(Integer.valueOf(10));
		commonPaymentValidations.validateMultiAuth(multiAuth);
	}

	@Test(expected = PSD2Exception.class)
	public void validateMultiAuth_NumberReqNumberRecZero() {
		OBMultiAuthorisation1 multiAuth = new OBMultiAuthorisation1();
		multiAuth.setStatus(OBExternalStatus2Code.AUTHORISED);
		multiAuth.setNumberReceived(Integer.valueOf(0));
		multiAuth.setNumberRequired(Integer.valueOf(0));
		commonPaymentValidations.validateMultiAuth(multiAuth);
	}

	@Test

	public void validateMultiAuth_NumberReqNumberRecZero_Rejected() {
		OBMultiAuthorisation1 multiAuth = new OBMultiAuthorisation1();
		multiAuth.setStatus(OBExternalStatus2Code.REJECTED);
		multiAuth.setNumberReceived(Integer.valueOf(0));
		multiAuth.setNumberRequired(Integer.valueOf(0));
		commonPaymentValidations.validateMultiAuth(multiAuth);
	}

	@Test(expected = PSD2Exception.class)
	public void validateMultiAuth_StatusNull() {
		OBMultiAuthorisation1 multiAuth = new OBMultiAuthorisation1();
		multiAuth.setStatus(null);
		commonPaymentValidations.validateMultiAuth(multiAuth);

	}
	
	@Test
	public void validateSchemeNameWithIdentification_validIBAN_XK() {
		commonPaymentValidations.validateSchemeNameWithIdentification(PaymentSetupConstants.IBAN, "XK051212012345678906",
				null);
	}
	
	@Test
	public void validateSchemeNameWithIdentification_validIBAN_VG() {
		commonPaymentValidations.validateSchemeNameWithIdentification(PaymentSetupConstants.IBAN, "VG96VPVG0000012345678901",
				null);
	}
	
	@Test
	public void validateSchemeNameWithIdentification_validIBAN_UA() {
		commonPaymentValidations.validateSchemeNameWithIdentification(PaymentSetupConstants.IBAN, "UA213223130000026007233566001",
				null);
	}
	
	@Test
	public void validateSchemeNameWithIdentification_validIBAN_TR() {
		commonPaymentValidations.validateSchemeNameWithIdentification(PaymentSetupConstants.IBAN, "TR330006100519786457841326",
				null);
	}
	
	@Test
	public void validateSchemeNameWithIdentification_validIBAN_TN() {
		commonPaymentValidations.validateSchemeNameWithIdentification(PaymentSetupConstants.IBAN, "TN5910006035183598478831",
				null);
	}
	
	@Test
	public void validateSchemeNameWithIdentification_validIBAN_TL() {
		commonPaymentValidations.validateSchemeNameWithIdentification(PaymentSetupConstants.IBAN, "TL380080012345678910157",
				null);
	}
	
	@Test
	public void validateSchemeNameWithIdentification_validIBAN_ST() {
		commonPaymentValidations.validateSchemeNameWithIdentification(PaymentSetupConstants.IBAN, "ST68000100010051845310112",
				null);
	}
	
	@Test
	public void validateSchemeNameWithIdentification_validIBAN_SM() {
		commonPaymentValidations.validateSchemeNameWithIdentification(PaymentSetupConstants.IBAN, "SM86U0322509800000000270100",
				null);
	}
	
	@Test
	public void validateSchemeNameWithIdentification_validIBAN_SK() {
		commonPaymentValidations.validateSchemeNameWithIdentification(PaymentSetupConstants.IBAN, "SK3112000000198742637541",
				null);
	}
	
	@Test
	public void validateSchemeNameWithIdentification_validIBAN_SI() {
		commonPaymentValidations.validateSchemeNameWithIdentification(PaymentSetupConstants.IBAN, "SI56263300012039086",
				null);
	}
	
	@Test
	public void validateSchemeNameWithIdentification_validIBAN_SE() {
		commonPaymentValidations.validateSchemeNameWithIdentification(PaymentSetupConstants.IBAN, "SE4550000000058398257466",
				null);
	}
	
	@Test
	public void validateSchemeNameWithIdentification_validIBAN_SC() {
		commonPaymentValidations.validateSchemeNameWithIdentification(PaymentSetupConstants.IBAN, "SC18SSCB11010000000000001497USD",
				null);
	}
	
	@Test
	public void validateSchemeNameWithIdentification_validIBAN_SA() {
		commonPaymentValidations.validateSchemeNameWithIdentification(PaymentSetupConstants.IBAN, "SA0380000000608010167519",
				null);
	}
	
	@Test
	public void validateSchemeNameWithIdentification_validIBAN_RS() {
		commonPaymentValidations.validateSchemeNameWithIdentification(PaymentSetupConstants.IBAN, "RS35260005601001611379",
				null);
	}
	
	@Test
	public void validateSchemeNameWithIdentification_validIBAN_RO() {
		commonPaymentValidations.validateSchemeNameWithIdentification(PaymentSetupConstants.IBAN, "RO49AAAA1B31007593840000",
				null);
	}
	
	@Test
	public void validateSchemeNameWithIdentification_validIBAN_QA() {
		commonPaymentValidations.validateSchemeNameWithIdentification(PaymentSetupConstants.IBAN, "QA58DOHB00001234567890ABCDEFG",
				null);
	}
	
	@Test
	public void validateSchemeNameWithIdentification_validIBAN_PT() {
		commonPaymentValidations.validateSchemeNameWithIdentification(PaymentSetupConstants.IBAN, "PT50000201231234567890154",
				null);
	}
	
	@Test
	public void validateSchemeNameWithIdentification_validIBAN_PS() {
		commonPaymentValidations.validateSchemeNameWithIdentification(PaymentSetupConstants.IBAN, "PS92PALS000000000400123456702",
				null);
	}
	
	@Test
	public void validateSchemeNameWithIdentification_validIBAN_PL() {
		commonPaymentValidations.validateSchemeNameWithIdentification(PaymentSetupConstants.IBAN, "PL61109010140000071219812874",
				null);
	}
	
	@Test
	public void validateSchemeNameWithIdentification_validIBAN_PK() {
		commonPaymentValidations.validateSchemeNameWithIdentification(PaymentSetupConstants.IBAN, "PK36SCBL0000001123456702",
				null);
	}

	@Test
	public void validateIBAN_Andorra() {
		commonPaymentValidations.validateSchemeNameWithIdentification(PaymentSetupConstants.IBAN, "AD1200012030200359100100",
				null);
	}
	
	@Test
	public void validateIBAN_United_Arab_Emirates() {
		commonPaymentValidations.validateSchemeNameWithIdentification(PaymentSetupConstants.IBAN, "AE070331234567890123456",
				null);
	}
	
	@Test
	public void validateIBAN_Albania() {
		commonPaymentValidations.validateSchemeNameWithIdentification(PaymentSetupConstants.IBAN, "AL47212110090000000235698741",
				null);
	}
	
	@Test
	public void validateIBAN_Austria() {
		commonPaymentValidations.validateSchemeNameWithIdentification(PaymentSetupConstants.IBAN, "AT611904300234573201",
				null);
	}
	
	@Test
	public void validateIBAN_Republic_Of_Azerbaijan() {
		commonPaymentValidations.validateSchemeNameWithIdentification(PaymentSetupConstants.IBAN, "AZ21NABZ00000000137010001944",
				null);
	}
	
	@Test
	public void validateIBAN_Bosnia_Herzegovina() {
		commonPaymentValidations.validateSchemeNameWithIdentification(PaymentSetupConstants.IBAN, "BA391290079401028494",
				null);
	}
	
	@Test
	public void validateIBAN_Belgium() {
		commonPaymentValidations.validateSchemeNameWithIdentification(PaymentSetupConstants.IBAN, "BE68539007547034",
				null);
	}
	
	@Test
	public void validateIBAN_Bulgaria() {
		commonPaymentValidations.validateSchemeNameWithIdentification(PaymentSetupConstants.IBAN, "BG80BNBG96611020345678",
				null);
	}
	
	@Test
	public void validateIBAN_Bahrain() {
		commonPaymentValidations.validateSchemeNameWithIdentification(PaymentSetupConstants.IBAN, "BH67BMAG00001299123456",
				null);
	}
	
	@Test
	public void validateIBAN_Brazil() {
		commonPaymentValidations.validateSchemeNameWithIdentification(PaymentSetupConstants.IBAN, "BR9700360305000010009795493P1",
				null);
	}
	
	@Test
	public void validateIBAN_Belarus() {
		commonPaymentValidations.validateSchemeNameWithIdentification(PaymentSetupConstants.IBAN, "BY13NBRB3600900000002Z00AB00",
				null);
	}
	
	@Test
	public void validateIBAN_Switzerland() {
		commonPaymentValidations.validateSchemeNameWithIdentification(PaymentSetupConstants.IBAN, "CH9300762011623852957",
				null);
	}
	
	@Test
	public void validateIBAN_Costa_Rica() {
		commonPaymentValidations.validateSchemeNameWithIdentification(PaymentSetupConstants.IBAN, "CR05015202001026284066",
				null);
	}
	
	@Test
	public void validateIBAN_Cyprus() {
		commonPaymentValidations.validateSchemeNameWithIdentification(PaymentSetupConstants.IBAN, "CY17002001280000001200527600",
				null);
	}
	
	@Test
	public void validateIBAN_CzechRepublic() {
		commonPaymentValidations.validateSchemeNameWithIdentification(PaymentSetupConstants.IBAN, "CZ6508000000192000145399",
				null);
	}
	
	@Test
	public void validateIBAN_Germany() {
		commonPaymentValidations.validateSchemeNameWithIdentification(PaymentSetupConstants.IBAN, "DE89370400440532013000",
				null);
	}
	
	@Test
	public void validateIBAN_Denmark() {
		commonPaymentValidations.validateSchemeNameWithIdentification(PaymentSetupConstants.IBAN, "DK5000400440116243",
				null);
	}
	
	@Test
	public void validateIBAN_Dominican_Republic() {
		commonPaymentValidations.validateSchemeNameWithIdentification(PaymentSetupConstants.IBAN, "DO28BAGR00000001212453611324",
				null);
	}
	
	@Test
	public void validateIBAN_Estonia() {
		commonPaymentValidations.validateSchemeNameWithIdentification(PaymentSetupConstants.IBAN, "EE382200221020145685",
				null);
	}
	
	@Test
	public void validateIBAN_Spain() {
		commonPaymentValidations.validateSchemeNameWithIdentification(PaymentSetupConstants.IBAN, "ES9121000418450200051332",
				null);
	}
	
	@Test
	public void validateIBAN_Finland() {
		commonPaymentValidations.validateSchemeNameWithIdentification(PaymentSetupConstants.IBAN, "FI2112345600000785",
				null);
	}
	
	@Test
	public void validateIBAN_France() {
		commonPaymentValidations.validateSchemeNameWithIdentification(PaymentSetupConstants.IBAN, "FR1420041010050500013M02606",
				null);
	}
	
	@Test
	public void validateIBAN_UnitedKingdom() {
		commonPaymentValidations.validateSchemeNameWithIdentification(PaymentSetupConstants.IBAN, "GB29NWBK60161331926819",
				null);
	}
	
	@Test
	public void validateIBAN_Georgia() {
		commonPaymentValidations.validateSchemeNameWithIdentification(PaymentSetupConstants.IBAN, "GE29NB0000000101904917",
				null);
	}
	
	@Test
	public void validateIBAN_Gibraltar() {
		commonPaymentValidations.validateSchemeNameWithIdentification(PaymentSetupConstants.IBAN, "GI75NWBK000000007099453",
				null);
	}
	
	@Test
	public void validateIBAN_Greece() {
		commonPaymentValidations.validateSchemeNameWithIdentification(PaymentSetupConstants.IBAN, "GR1601101250000000012300695",
				null);
	}
	
	@Test
	public void validateIBAN_Guatemala() {
		commonPaymentValidations.validateSchemeNameWithIdentification(PaymentSetupConstants.IBAN, "GT82TRAJ01020000001210029690",
				null);
	}
	
	@Test
	public void validateIBAN_Croatia() {
		commonPaymentValidations.validateSchemeNameWithIdentification(PaymentSetupConstants.IBAN, "HR1210010051863000160",
				null);
	}
	
	@Test
	public void validateIBAN_Hungary() {
		commonPaymentValidations.validateSchemeNameWithIdentification(PaymentSetupConstants.IBAN, "HU42117730161111101800000000",
				null);
	}
	
	@Test
	public void validateIBAN_Israel() {
		commonPaymentValidations.validateSchemeNameWithIdentification(PaymentSetupConstants.IBAN, "IL620108000000099999999",
				null);
	}
	@Test
	public void validateIBAN_Iceland() {
		commonPaymentValidations.validateSchemeNameWithIdentification(PaymentSetupConstants.IBAN, "IS140159260076545510730339",
				null);
	}
	@Test
	public void validateIBAN_Italy() {
		commonPaymentValidations.validateSchemeNameWithIdentification(PaymentSetupConstants.IBAN, "IT60X0542811101000000123456",
				null);
	}
	
	@Test
	public void validateIBAN_Iraq() {
		commonPaymentValidations.validateSchemeNameWithIdentification(PaymentSetupConstants.IBAN, "IQ98NBIQ850123456789012",
				null);
	}
	
	@Test
	public void validateIBAN_Jordan() {
		commonPaymentValidations.validateSchemeNameWithIdentification(PaymentSetupConstants.IBAN, "JO94CBJO0010000000000131000302",
				null);
	}
	
	@Test
	public void validateIBAN_Kuwait() {
		commonPaymentValidations.validateSchemeNameWithIdentification(PaymentSetupConstants.IBAN, "KW81CBKU0000000000001234560101",
				null);
	}
	
	@Test
	public void validateIBAN_Kazakhstan() {
		commonPaymentValidations.validateSchemeNameWithIdentification(PaymentSetupConstants.IBAN, "KZ86125KZT5004100100",
				null);
	}
	
	@Test
	public void validateIBAN_Lebanon() {
		commonPaymentValidations.validateSchemeNameWithIdentification(PaymentSetupConstants.IBAN, "LB62099900000001001901229114",
				null);
	}
	
	@Test
	public void validateIBAN_Saint_Lucia() {
		commonPaymentValidations.validateSchemeNameWithIdentification(PaymentSetupConstants.IBAN, "LC55HEMM000100010012001200023015",
				null);
	}
	
	@Test
	public void validateIBAN_Liechtenstein() {
		commonPaymentValidations.validateSchemeNameWithIdentification(PaymentSetupConstants.IBAN, "LI21088100002324013AA",
				null);
	}
	
	@Test
	public void validateIBAN_Lithuania() {
		commonPaymentValidations.validateSchemeNameWithIdentification(PaymentSetupConstants.IBAN, "LT121000011101001000",
				null);
	}
	
	@Test
	public void validateIBAN_Luxembourg() {
		commonPaymentValidations.validateSchemeNameWithIdentification(PaymentSetupConstants.IBAN, "LU280019400644750000",
				null);
	}
	
	@Test
	public void validateIBAN_Latvia() {
		commonPaymentValidations.validateSchemeNameWithIdentification(PaymentSetupConstants.IBAN, "LV80BANK0000435195001",
				null);
	}
	
	@Test
	public void validateIBAN_Monaco() {
		commonPaymentValidations.validateSchemeNameWithIdentification(PaymentSetupConstants.IBAN, "MC5811222000010123456789030",
				null);
	}
	
	@Test
	public void validateIBAN_Moldova() {
		commonPaymentValidations.validateSchemeNameWithIdentification(PaymentSetupConstants.IBAN, "MD24AG000225100013104168",
				null);
	}
	
	@Test
	public void validateIBAN_Montenegro() {
		commonPaymentValidations.validateSchemeNameWithIdentification(PaymentSetupConstants.IBAN, "ME25505000012345678951",
				null);
	}
	
	@Test
	public void validateIBAN_Macedonia() {
		commonPaymentValidations.validateSchemeNameWithIdentification(PaymentSetupConstants.IBAN, "MK07250120000058984",
				null);
	}
	
	@Test
	public void validateIBAN_Mauritania() {
		commonPaymentValidations.validateSchemeNameWithIdentification(PaymentSetupConstants.IBAN, "MR1300020001010000123456753",
				null);
	}
	
	@Test
	public void validateIBAN_Malta() {
		commonPaymentValidations.validateSchemeNameWithIdentification(PaymentSetupConstants.IBAN, "MT84MALT011000012345MTLCAST001S",
				null);
	}
	
	@Test
	public void validateIBAN_Mauritius() {
		commonPaymentValidations.validateSchemeNameWithIdentification(PaymentSetupConstants.IBAN, "MU17BOMM0101101030300200000MUR",
				null);
	}
	
	@Test
	public void validateIBAN_Netherlands() {
		commonPaymentValidations.validateSchemeNameWithIdentification(PaymentSetupConstants.IBAN, "NL91ABNA0417164300",
				null);
	}
	
	@Test
	public void validateIBAN_Norway() {
		commonPaymentValidations.validateSchemeNameWithIdentification(PaymentSetupConstants.IBAN, "NO9386011117947",
				null);
	}
	
	@Test
	public void validateIBAN_Denmark_Faroe_Islands() {
		commonPaymentValidations.validateSchemeNameWithIdentification(PaymentSetupConstants.IBAN, "FO6264600001631634",
				null);
	}
	
	@Test
	public void validateIBAN_ELSalvador() {
		commonPaymentValidations.validateSchemeNameWithIdentification(PaymentSetupConstants.IBAN, "SV62CENR00000000000000700025",
				null);
	}

	@Test
	public void validateIBAN_Vatican() {
		commonPaymentValidations.validateSchemeNameWithIdentification(PaymentSetupConstants.IBAN, "VA59001123000012345678",
				null);
	}
	
	@Test(expected = PSD2Exception.class)
	public void validateSchemeNameWithIdentification_invalidIBAN_BIC_SV() {
		commonPaymentValidations.validateSchemeNameWithIdentification(PaymentSetupConstants.UK_OBIE_IBAN, "SV62CEMN00000000000000700025", null);
	}
	
	@Test(expected = PSD2Exception.class)
	public void validateSchemeNameWithIdentification_invalidIBAN_Length_VA() {
		commonPaymentValidations.validateSchemeNameWithIdentification(PaymentSetupConstants.UK_OBIE_IBAN, "VA5900112300001234567812344", null);
	}
	
	@Test(expected = PSD2Exception.class)
	public void validateSchemeNameWithIdentification_invalidIBAN_Length_null() {
		commonPaymentValidations.validateSchemeNameWithIdentification(PaymentSetupConstants.UK_OBIE_IBAN, null, null);
	}
	
	@Test(expected = PSD2Exception.class)
	public void validateSchemeNameWithIdentification_invalidIBAN_Length_blank() {
		commonPaymentValidations.validateSchemeNameWithIdentification(PaymentSetupConstants.UK_OBIE_IBAN, "", null);
	}
	
	@Test(expected = PSD2Exception.class)
	public void validateSchemeNameWithIdentification_invalidIBAN_Spaces_SV() {
		commonPaymentValidations.validateSchemeNameWithIdentification(PaymentSetupConstants.UK_OBIE_IBAN, "SV62CENR0  00000 00000000700025", null);
	}
	
	@Test
	public void validateSchemeNameWithIdentification_invalidIBAN_LeadingAndTrailingSpaces_SV() {
		commonPaymentValidations.validateSchemeNameWithIdentification(PaymentSetupConstants.UK_OBIE_IBAN, "  SV62CENR00000000000000700025     ", null);
	}
	
	@Test
	public void validateSchemeNameWithIdentification_validIBAN() {
		commonPaymentValidations.validateSchemeNameWithIdentification(PaymentSetupConstants.IBAN, "GB33BUKB20201555555555",
				null);
	}
	
	@Test
	public void validateSchemeNameWithIdentification_validIBAN_IE() {
		commonPaymentValidations.validateSchemeNameWithIdentification(PaymentSetupConstants.IBAN, "IE64IRCE92050112345678",
				null);
	}
	
	@Test(expected = PSD2Exception.class)
	public void validateSchemeNameWithIdentification_invalidIBAN_LengthInvalid_IE() {
		commonPaymentValidations.validateSchemeNameWithIdentification(PaymentSetupConstants.IBAN, "IE64ARCE920501123456780", null);
	}
	
	@Test(expected = PSD2Exception.class)
	public void validateSchemeNameWithIdentification_invalidIBAN_FormatInvalid_IE() {
		commonPaymentValidations.validateSchemeNameWithIdentification(PaymentSetupConstants.IBAN, "IE64ARCE92050112345@#$", null);
	}
	
	@Test(expected = PSD2Exception.class)
	public void validateSchemeNameWithIdentification_invalidIBAN_BICInvalid_GB() {
		commonPaymentValidations.validateSchemeNameWithIdentification(PaymentSetupConstants.IBAN, "GB33BAKB20201555555555", null);
	}
	
	@Test(expected = PSD2Exception.class)
	public void validateSchemeNameWithIdentification_invalidIBAN_LengthInvalid_GB() {
		commonPaymentValidations.validateSchemeNameWithIdentification(PaymentSetupConstants.IBAN, "GB33BUKB202015555555556", null);
	}
	
	@Test(expected = PSD2Exception.class)
	public void validateSchemeNameWithIdentification_invalidIBAN_CheckDigitInvalid_GB() {
		commonPaymentValidations.validateSchemeNameWithIdentification(PaymentSetupConstants.IBAN, "GB30BUKB20201555555555", null);
	}

	@Test(expected = PSD2Exception.class)
	public void validateSchemeNameWithIdentification_invalidIBAN_LengthInvalid_HR() {
		commonPaymentValidations.validateSchemeNameWithIdentification(PaymentSetupConstants.IBAN, "HR17236000011012345651", null);
	}
	
	@Test(expected = PSD2Exception.class)
	public void validateSchemeNameWithIdentification_invalidIBAN_BICInvalid_HR() {
		commonPaymentValidations.validateSchemeNameWithIdentification(PaymentSetupConstants.IBAN, "HR1723500001101234565", null);
	}
	
	@Test(expected = PSD2Exception.class)
	public void validateSchemeNameWithIdentification_invalidIBAN_CheckDigitInvalid_HR() {
		commonPaymentValidations.validateSchemeNameWithIdentification(PaymentSetupConstants.IBAN, "HR1223600001101234565", null);
	}
	
	@Test(expected = PSD2Exception.class)
	public void validateSchemeNameWithIdentification_invalidIBAN_CountryInvalid() {
		commonPaymentValidations.validateSchemeNameWithIdentification(PaymentSetupConstants.IBAN, "BB33BUKB20201555555555", null);
	}

	@Test(expected = PSD2Exception.class)
	public void validateSchemeNameWithIdentification_invalidUKOBIEIBAN_errorCodeAbsent() {
		commonPaymentValidations.validateSchemeNameWithIdentification(PaymentSetupConstants.UK_OBIE_IBAN, "Invalid", null);
	}


}