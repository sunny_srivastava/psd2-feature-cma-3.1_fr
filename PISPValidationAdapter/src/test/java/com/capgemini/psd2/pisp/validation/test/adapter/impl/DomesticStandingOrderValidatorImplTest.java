package com.capgemini.psd2.pisp.validation.test.adapter.impl;

import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doNothing;

import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.exceptions.OBPSD2ExceptionUtility;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.pisp.domain.CustomDStandingOrderConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomDStandingOrderConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.CustomDStandingOrderPOSTRequest;
import com.capgemini.psd2.pisp.domain.DStandingOrderPOST201Response;
import com.capgemini.psd2.pisp.domain.OBAuthorisation1;
import com.capgemini.psd2.pisp.domain.OBCashAccountCreditor2;
import com.capgemini.psd2.pisp.domain.OBCashAccountDebtor3;
import com.capgemini.psd2.pisp.domain.OBCharge1;
import com.capgemini.psd2.pisp.domain.OBCharge1Amount;
import com.capgemini.psd2.pisp.domain.OBDomestic1InstructedAmount;
import com.capgemini.psd2.pisp.domain.OBDomesticStandingOrder1;
import com.capgemini.psd2.pisp.domain.OBDomesticStandingOrder1FinalPaymentAmount;
import com.capgemini.psd2.pisp.domain.OBDomesticStandingOrder1FirstPaymentAmount;
import com.capgemini.psd2.pisp.domain.OBDomesticStandingOrder1RecurringPaymentAmount;
import com.capgemini.psd2.pisp.domain.OBExternalStatus1Code;
import com.capgemini.psd2.pisp.domain.OBExternalStatus2Code;
import com.capgemini.psd2.pisp.domain.OBMultiAuthorisation1;
import com.capgemini.psd2.pisp.domain.OBRisk1;
import com.capgemini.psd2.pisp.domain.OBRisk1DeliveryAddress;
import com.capgemini.psd2.pisp.domain.OBWriteDataDomesticStandingOrder1;
import com.capgemini.psd2.pisp.domain.OBWriteDataDomesticStandingOrderConsent1;
import com.capgemini.psd2.pisp.domain.OBWriteDataDomesticStandingOrderConsentResponse1;
import com.capgemini.psd2.pisp.domain.OBWriteDataDomesticStandingOrderResponse1;
import com.capgemini.psd2.pisp.domain.PaymentRetrieveGetRequest;
import com.capgemini.psd2.pisp.domain.PaymentSetupPOSTResponseLinks;
import com.capgemini.psd2.pisp.domain.PaymentSetupPOSTResponseMeta;
import com.capgemini.psd2.pisp.validation.adapter.constants.CommonPaymentValidations;
import com.capgemini.psd2.pisp.validation.adapter.impl.DomesticStandingOrderValidatorImpl;
import com.capgemini.psd2.utilities.DateUtilitiesCMA2;
import com.capgemini.psd2.validator.PSD2Validator;

public class DomesticStandingOrderValidatorImplTest {

	@InjectMocks
	private DomesticStandingOrderValidatorImpl domesticStandingOrderValidatorImpl;

	@Mock
	private PSD2Validator psd2Validator;

	@Mock
	private CommonPaymentValidations commonPaymentValidations;

	@Mock
	private DateUtilitiesCMA2 dateUtilitiesCMA2;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		Map<String,String> genericErrorMessages=new HashMap<>();
		Map<String,String> specificErrorMessages=new HashMap<>();
		genericErrorMessages.put("INTERNAL", "INTERNAL Error");
		specificErrorMessages.put("Idempotency", "Idempotency duration is not configured in correct format.");
		OBPSD2ExceptionUtility.genericErrorMessages.putAll(genericErrorMessages);
		OBPSD2ExceptionUtility.specificErrorMessages.putAll(specificErrorMessages);

	}


	@Test
	public void validateConsentsGETRequest() {
		doNothing().when(psd2Validator).validate(any(PaymentRetrieveGetRequest.class));
		assertTrue(domesticStandingOrderValidatorImpl.validateConsentsGETRequest(new PaymentRetrieveGetRequest()));
	}

	@Test
	public void validateConsentRequestTest() {

		ReflectionTestUtils.setField(domesticStandingOrderValidatorImpl, "reqValidationEnabled", true);
		ReflectionTestUtils.setField(domesticStandingOrderValidatorImpl, "merchantCategoryCodeRegexValidator", "\\d+");
		doNothing().when(commonPaymentValidations).validateHeaders();
		assertTrue(domesticStandingOrderValidatorImpl.validateConsentRequest(getRequest()));
	}

	@Test
	public void validatePaymentConsentResponse() {

		ReflectionTestUtils.setField(domesticStandingOrderValidatorImpl, "resValidationEnabled", true);
		ReflectionTestUtils.setField(domesticStandingOrderValidatorImpl, "merchantCategoryCodeRegexValidator", "\\d+");
		doNothing().when(commonPaymentValidations).validateHeaders();
		Mockito.when(commonPaymentValidations.isValidCurrency(any(), any())).thenReturn(true);
		assertTrue(domesticStandingOrderValidatorImpl.validatePaymentConsentResponse(getResponse()));
	}

	@Test
	public void validatePaymentsPOSTRequest(){

		ReflectionTestUtils.setField(domesticStandingOrderValidatorImpl, "reqValidationEnabled", true);
		ReflectionTestUtils.setField(domesticStandingOrderValidatorImpl, "paymentIdRegexValidator", "[a-zA-Z0-9-]{1,40}");
		domesticStandingOrderValidatorImpl.validatePaymentsPOSTRequest(getSubmissionRequest());
	}
	
	@Test(expected = PSD2Exception.class)
	public void validatePaymentsPOSTRequest_invalidConsentId() {

		ReflectionTestUtils.setField(domesticStandingOrderValidatorImpl, "reqValidationEnabled", true);
		ReflectionTestUtils.setField(domesticStandingOrderValidatorImpl, "paymentIdRegexValidator",
				"[a-zA-Z0-9-]{1,40}");
		ReflectionTestUtils.setField(domesticStandingOrderValidatorImpl, "merchantCategoryCodeRegexValidator", "\\d+");
		CustomDStandingOrderPOSTRequest subReq = getSubmissionRequest();
		subReq.getData().setConsentId("invalid!");
		domesticStandingOrderValidatorImpl.validatePaymentsPOSTRequest(subReq);
	}
	
	@Test
	public void validatePaymentsResponseTest(){
		ReflectionTestUtils.setField(domesticStandingOrderValidatorImpl, "resValidationEnabled", true);
		ReflectionTestUtils.setField(domesticStandingOrderValidatorImpl, "paymentIdRegexValidator", "[a-zA-Z0-9-]{1,40}");
		domesticStandingOrderValidatorImpl.validatePaymentsResponse(getSubmissionResponse());
	}

	private CustomDStandingOrderConsentsPOSTRequest getRequest() {
		OBDomesticStandingOrder1FirstPaymentAmount firstPaymentAmount = new OBDomesticStandingOrder1FirstPaymentAmount();

		firstPaymentAmount.setCurrency("currency");
		OBDomestic1InstructedAmount recurringPaymentAmount = new OBDomestic1InstructedAmount();
		recurringPaymentAmount.setCurrency("currency");
		OBDomestic1InstructedAmount finalPaymentAmount = new OBDomestic1InstructedAmount();
		finalPaymentAmount.setCurrency("currency");
		OBCashAccountCreditor2 creditorAccount = new OBCashAccountCreditor2();
		creditorAccount.setSchemeName("schemeName");
		creditorAccount.setIdentification("identification");
		creditorAccount.setSecondaryIdentification("secondaryIdentification");
		OBCashAccountDebtor3 debtorAccount = new OBCashAccountDebtor3();
		debtorAccount.setSchemeName("schemeName");
		debtorAccount.setIdentification("identification");
		debtorAccount.setSecondaryIdentification("secondaryIdentification");
		OBDomesticStandingOrder1 initiation = new OBDomesticStandingOrder1();
		initiation.setFirstPaymentAmount(firstPaymentAmount);
		initiation.setFirstPaymentDateTime(OffsetDateTime.now().plusDays(1).toString());
		// initiation.setRecurringPaymentAmount(recurringPaymentAmount);
		initiation.setRecurringPaymentDateTime(OffsetDateTime.now().plusDays(2).toString());
		// initiation.setFinalPaymentAmount(finalPaymentAmount);
		initiation.setFinalPaymentDateTime(OffsetDateTime.now().plusDays(3).toString());
		initiation.setCreditorAccount(creditorAccount);
		initiation.setDebtorAccount(debtorAccount);
		initiation.setRecurringPaymentAmount(new OBDomesticStandingOrder1RecurringPaymentAmount());
		initiation.getRecurringPaymentAmount().setCurrency("EUR");
		initiation.setFinalPaymentAmount(new OBDomesticStandingOrder1FinalPaymentAmount());
		initiation.getFinalPaymentAmount().setCurrency("EUR");
		OBAuthorisation1 authorisation = new OBAuthorisation1();
		authorisation.setCompletionDateTime("completionDateTime");
		OBWriteDataDomesticStandingOrderConsent1 data = new OBWriteDataDomesticStandingOrderConsent1();
		data.setInitiation(initiation);
		data.setAuthorisation(authorisation);
		OBRisk1DeliveryAddress deliveryAddress = new OBRisk1DeliveryAddress();
		OBRisk1 risk = new OBRisk1();
		risk.setDeliveryAddress(deliveryAddress);
		CustomDStandingOrderConsentsPOSTRequest request = new CustomDStandingOrderConsentsPOSTRequest();
		request.setData(data);
		request.setRisk(risk);
		return request;
	}

	private CustomDStandingOrderConsentsPOSTResponse getResponse() {
		OBDomesticStandingOrder1FirstPaymentAmount firstPaymentAmount = new OBDomesticStandingOrder1FirstPaymentAmount();
		firstPaymentAmount.setCurrency("currency");
		OBDomestic1InstructedAmount recurringPaymentAmount = new OBDomestic1InstructedAmount();
		recurringPaymentAmount.setCurrency("currency");
		OBDomestic1InstructedAmount finalPaymentAmount = new OBDomestic1InstructedAmount();
		finalPaymentAmount.setCurrency("currency");
		OBCashAccountCreditor2 creditorAccount = new OBCashAccountCreditor2();
		creditorAccount.setSchemeName("schemeName");
		creditorAccount.setIdentification("identification");
		creditorAccount.setSecondaryIdentification("secondaryIdentification");
		OBCashAccountDebtor3 debtorAccount = new OBCashAccountDebtor3();
		debtorAccount.setSchemeName("schemeName");
		debtorAccount.setIdentification("identification");
		debtorAccount.setSecondaryIdentification("secondaryIdentification");
		OBDomesticStandingOrder1 initiation = new OBDomesticStandingOrder1();
		// initiation.setFirstPaymentAmount(firstPaymentAmount);
		initiation.setFirstPaymentDateTime(LocalDateTime.now().toString());
		// initiation.setRecurringPaymentAmount(recurringPaymentAmount);
		initiation.setRecurringPaymentDateTime(LocalDateTime.now().toString());
		// initiation.setFinalPaymentAmount(finalPaymentAmount);
		initiation.setFinalPaymentDateTime(LocalDateTime.now().toString());
		initiation.setCreditorAccount(creditorAccount);
		initiation.setDebtorAccount(debtorAccount);
		OBDomestic1InstructedAmount amount1 = new OBDomestic1InstructedAmount();
		amount1.setAmount("GBP");
		initiation.setFirstPaymentAmount(firstPaymentAmount);
		OBAuthorisation1 authorisation = new OBAuthorisation1();
		authorisation.setCompletionDateTime("completionDateTime");
		OBCharge1Amount amount = new OBCharge1Amount();
		amount.setCurrency("currency");
		OBCharge1 charge = new OBCharge1();
		charge.setAmount(amount);
		List<OBCharge1> charges = new ArrayList<>();
		charges.add(charge);
		OBWriteDataDomesticStandingOrderConsentResponse1 data = new OBWriteDataDomesticStandingOrderConsentResponse1();
		data.setInitiation(initiation);
		data.setAuthorisation(authorisation);
		data.setCharges(charges);
		OBRisk1DeliveryAddress deliveryAddress = new OBRisk1DeliveryAddress();
		OBRisk1 risk = new OBRisk1();
		risk.setDeliveryAddress(deliveryAddress);
		CustomDStandingOrderConsentsPOSTResponse request = new CustomDStandingOrderConsentsPOSTResponse();
		request.setData(data);
		request.setRisk(risk);
		return request;
	}

	private CustomDStandingOrderPOSTRequest getSubmissionRequest(){
		CustomDStandingOrderPOSTRequest request = new CustomDStandingOrderPOSTRequest();

		request.setData(new OBWriteDataDomesticStandingOrder1());
		request.getData().setInitiation(new OBDomesticStandingOrder1());
		request.getData().getInitiation().setFirstPaymentAmount(new OBDomesticStandingOrder1FirstPaymentAmount());
		request.getData().getInitiation().getFirstPaymentAmount().setAmount("153");
		request.getData().getInitiation()
		.setRecurringPaymentAmount(new OBDomesticStandingOrder1RecurringPaymentAmount());
		request.getData().getInitiation().getRecurringPaymentAmount().setAmount("154");
		request.getData().getInitiation().setFinalPaymentAmount(new OBDomesticStandingOrder1FinalPaymentAmount());
		request.getData().getInitiation().getFinalPaymentAmount().setAmount("160");
		request.getData().getInitiation().setDebtorAccount(new OBCashAccountDebtor3());
		request.getData().getInitiation().getDebtorAccount().setName("test1");
		request.getData().setConsentId("92d09ff5-2bd9-45c0-9ad1-157e3d36d0e9");
		request.setRisk(new OBRisk1());
		request.getRisk().setDeliveryAddress(new OBRisk1DeliveryAddress());
		return request;

	}

	private DStandingOrderPOST201Response getSubmissionResponse(){
		DStandingOrderPOST201Response submissionResponse = new DStandingOrderPOST201Response();
		submissionResponse.setData(new OBWriteDataDomesticStandingOrderResponse1());
		submissionResponse.getData().setInitiation(new OBDomesticStandingOrder1());
		submissionResponse.setLinks(new PaymentSetupPOSTResponseLinks());
		submissionResponse.setMeta(new PaymentSetupPOSTResponseMeta());
		submissionResponse.getData().setConsentId("92d09ff5-2bd9-45c0-9ad1-157e3d36d0e9");
		submissionResponse.getData().setDomesticStandingOrderId("29d09ff5-2bd9-45c0-9ad1-157e3d36d0e9");

		OBDomesticStandingOrder1FirstPaymentAmount firstPaymentAmount = new OBDomesticStandingOrder1FirstPaymentAmount();

		firstPaymentAmount.setCurrency("currency");
		OBDomesticStandingOrder1RecurringPaymentAmount recurringPaymentAmount = new OBDomesticStandingOrder1RecurringPaymentAmount();
		recurringPaymentAmount.setCurrency("currency");
		OBDomesticStandingOrder1FinalPaymentAmount finalPaymentAmount = new OBDomesticStandingOrder1FinalPaymentAmount();
		finalPaymentAmount.setCurrency("currency");

		submissionResponse.getData().getInitiation().setFirstPaymentAmount(firstPaymentAmount);
		submissionResponse.getData().getInitiation().setRecurringPaymentAmount(recurringPaymentAmount);
		submissionResponse.getData().getInitiation().setFinalPaymentAmount(finalPaymentAmount);
		submissionResponse.getData().getInitiation().setFirstPaymentDateTime(OffsetDateTime.now().toString());
		submissionResponse.getData().getInitiation().setFinalPaymentDateTime(OffsetDateTime.now().toString());
		submissionResponse.getData().getInitiation().setRecurringPaymentDateTime(OffsetDateTime.now().toString());
		submissionResponse.getData().getInitiation().setDebtorAccount(new OBCashAccountDebtor3());
		submissionResponse.getData().getInitiation().getDebtorAccount().setSchemeName("schemeName");
		submissionResponse.getData().getInitiation().getDebtorAccount().setIdentification("identification");
		submissionResponse.getData().getInitiation().setCreditorAccount(new OBCashAccountCreditor2());
		submissionResponse.getData().getInitiation().getCreditorAccount().setSchemeName("schemeName");
		submissionResponse.getData().getInitiation().getCreditorAccount().setIdentification("identification");
		submissionResponse.getData().setMultiAuthorisation(new OBMultiAuthorisation1());
		submissionResponse.getData().getMultiAuthorisation().setStatus(OBExternalStatus2Code.AWAITINGFURTHERAUTHORISATION);
		submissionResponse.getData().setStatus(OBExternalStatus1Code.INITIATIONPENDING);
		OBCharge1Amount amount = new OBCharge1Amount();
		amount.setCurrency("currency");
		OBCharge1 charge = new OBCharge1();
		charge.setAmount(amount);
		List<OBCharge1> charges = new ArrayList<>();
		charges.add(charge);
		submissionResponse.getData().setCharges(charges);
		return submissionResponse;
	}
	
	@Test(expected=PSD2Exception.class)
	public void executeDomesticSubmissionResponseCustomValidationsTest(){
		ReflectionTestUtils.setField(domesticStandingOrderValidatorImpl, "paymentIdRegexValidator", "[a-zA-Z0-9-]{1,40}");
		DStandingOrderPOST201Response response = new DStandingOrderPOST201Response();
		OBWriteDataDomesticStandingOrderResponse1 data = new OBWriteDataDomesticStandingOrderResponse1();
		data.setConsentId("ABC145");
		data.setDomesticStandingOrderId("ABC145");
		response.setData(data );
		domesticStandingOrderValidatorImpl.executeDomesticSubmissionResponseCustomValidations(response);
	}

	@Test(expected=PSD2Exception.class)
	public void executeDomesticSubmissionResponseCustomValidations1Test(){
		ReflectionTestUtils.setField(domesticStandingOrderValidatorImpl, "paymentIdRegexValidator", "[a-zA-Z0-9-]{1,40}");
		DStandingOrderPOST201Response response = new DStandingOrderPOST201Response();
		OBWriteDataDomesticStandingOrderResponse1 data = new OBWriteDataDomesticStandingOrderResponse1();
		data.setConsentId("ConsentList");
		data.setDomesticStandingOrderId("DomesticStandingOrderId");
		OBDomesticStandingOrder1 initiation = new OBDomesticStandingOrder1();
		OBCashAccountDebtor3 debtorAccount = new OBCashAccountDebtor3();
		debtorAccount.setSchemeName("IBAN");
		debtorAccount.setIdentification("1234dgfkkm");
		initiation.setDebtorAccount(debtorAccount );
		OBCashAccountCreditor2 creditorAccount = new OBCashAccountCreditor2();
		creditorAccount.setSchemeName("IBAN");
		creditorAccount.setIdentification("12344");
		List<OBCharge1> charges = new ArrayList<>();
		OBCharge1 e = new OBCharge1();
		OBCharge1Amount amount = new OBCharge1Amount();
		amount.setCurrency("INR");
		e.setAmount(amount );
		charges.add(e );
		data.setCharges(charges);
		OBMultiAuthorisation1 multiAuthorisation = new OBMultiAuthorisation1();
		multiAuthorisation.setStatus(OBExternalStatus2Code.AWAITINGFURTHERAUTHORISATION);
		data.setStatus(OBExternalStatus1Code.INITIATIONCOMPLETED);
		data.setMultiAuthorisation(multiAuthorisation );

		initiation.setCreditorAccount(creditorAccount  );
		data.setInitiation(initiation );
		response.setData(data );
		domesticStandingOrderValidatorImpl.executeDomesticSubmissionResponseCustomValidations(response);
	}

	@Test(expected=PSD2Exception.class)
	public void executeDomesticSubmissionResponseCustomValidations2Test(){
		ReflectionTestUtils.setField(domesticStandingOrderValidatorImpl, "paymentIdRegexValidator", "[a-zA-Z0-9-]{1,40}");
		DStandingOrderPOST201Response response = new DStandingOrderPOST201Response();
		OBWriteDataDomesticStandingOrderResponse1 data = new OBWriteDataDomesticStandingOrderResponse1();
		data.setConsentId("ConsentList");
		data.setDomesticStandingOrderId("DomesticStandingOrderId");
		OBDomesticStandingOrder1 initiation = new OBDomesticStandingOrder1();
		OBCashAccountDebtor3 debtorAccount = new OBCashAccountDebtor3();
		debtorAccount.setSchemeName("IBAN");
		debtorAccount.setIdentification("1234dgfkkm");
		initiation.setDebtorAccount(debtorAccount );
		OBCashAccountCreditor2 creditorAccount = new OBCashAccountCreditor2();
		creditorAccount.setSchemeName("IBAN");
		creditorAccount.setIdentification("12344");
		List<OBCharge1> charges = new ArrayList<>();
		OBCharge1 e = new OBCharge1();
		OBCharge1Amount amount = new OBCharge1Amount();
		amount.setCurrency("INR");
		e.setAmount(amount );
		charges.add(e );
		data.setCharges(charges);
		OBMultiAuthorisation1 multiAuthorisation = new OBMultiAuthorisation1();
		multiAuthorisation.setStatus(OBExternalStatus2Code.AWAITINGFURTHERAUTHORISATION);
		data.setStatus(OBExternalStatus1Code.INITIATIONFAILED);
		data.setMultiAuthorisation(multiAuthorisation );
		OBDomesticStandingOrder1FinalPaymentAmount finalPaymentAmount = new OBDomesticStandingOrder1FinalPaymentAmount();
		finalPaymentAmount.setCurrency("INR");
		OBDomesticStandingOrder1RecurringPaymentAmount recurringPaymentAmount = new OBDomesticStandingOrder1RecurringPaymentAmount();
		recurringPaymentAmount.currency("INR");

		initiation.setFirstPaymentDateTime(LocalDateTime.now().toString());
		initiation.setFinalPaymentDateTime(LocalDateTime.now().toString());
		initiation.setNumberOfPayments("5");
		initiation.setRecurringPaymentAmount(recurringPaymentAmount );
		initiation.setFinalPaymentAmount(finalPaymentAmount );
		OBDomesticStandingOrder1FirstPaymentAmount firstPaymentAmount = new OBDomesticStandingOrder1FirstPaymentAmount();

		firstPaymentAmount.setCurrency("INR");
		initiation.setFirstPaymentAmount(firstPaymentAmount );
		initiation.setCreditorAccount(creditorAccount  );
		data.setInitiation(initiation );
		response.setData(data );

		domesticStandingOrderValidatorImpl.executeDomesticSubmissionResponseCustomValidations(response);
	}

	@Test
	public void executeDomesticSubmissionResponseCustomValidations3Test(){
		ReflectionTestUtils.setField(domesticStandingOrderValidatorImpl, "paymentIdRegexValidator", "[a-zA-Z0-9-]{1,40}");
		DStandingOrderPOST201Response response = new DStandingOrderPOST201Response();
		OBWriteDataDomesticStandingOrderResponse1 data = new OBWriteDataDomesticStandingOrderResponse1();
		data.setConsentId("ConsentList");
		data.setDomesticStandingOrderId("DomesticStandingOrderId");
		OBDomesticStandingOrder1 initiation = new OBDomesticStandingOrder1();
		OBCashAccountDebtor3 debtorAccount = new OBCashAccountDebtor3();
		debtorAccount.setSchemeName("IBAN");
		debtorAccount.setIdentification("1234dgfkkm");
		initiation.setDebtorAccount(debtorAccount );
		OBCashAccountCreditor2 creditorAccount = new OBCashAccountCreditor2();
		creditorAccount.setSchemeName("IBAN");
		creditorAccount.setIdentification("12344");
		List<OBCharge1> charges = new ArrayList<>();
		OBCharge1 e = new OBCharge1();
		OBCharge1Amount amount = new OBCharge1Amount();
		amount.setCurrency("INR");
		e.setAmount(amount );
		charges.add(e );
		data.setCharges(charges);
		OBMultiAuthorisation1 multiAuthorisation = new OBMultiAuthorisation1();
		multiAuthorisation.setStatus(OBExternalStatus2Code.AWAITINGFURTHERAUTHORISATION);
		data.setStatus(OBExternalStatus1Code.INITIATIONFAILED);
		data.setMultiAuthorisation(multiAuthorisation );
		OBDomesticStandingOrder1FinalPaymentAmount finalPaymentAmount = new OBDomesticStandingOrder1FinalPaymentAmount();
		finalPaymentAmount.setCurrency("INR");
		OBDomesticStandingOrder1RecurringPaymentAmount recurringPaymentAmount = new OBDomesticStandingOrder1RecurringPaymentAmount();
		recurringPaymentAmount.currency("INR");
		OBDomesticStandingOrder1FirstPaymentAmount firstPaymentAmount = new OBDomesticStandingOrder1FirstPaymentAmount();

		firstPaymentAmount.setCurrency("INR");
		initiation.setFirstPaymentAmount(firstPaymentAmount );
		initiation.setFirstPaymentDateTime(LocalDateTime.now().toString());
		initiation.setFinalPaymentDateTime(LocalDateTime.now().toString());
		initiation.setFinalPaymentAmount(finalPaymentAmount );	
		initiation.setRecurringPaymentDateTime(LocalDateTime.now().toString() );
		initiation.setCreditorAccount(creditorAccount  );
		data.setInitiation(initiation );
		response.setData(data );

		domesticStandingOrderValidatorImpl.executeDomesticSubmissionResponseCustomValidations(response);
	}

}