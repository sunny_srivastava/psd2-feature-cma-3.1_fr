package com.capgemini.psd2.pisp.validation.adapter.constants;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.Currency;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.iban4j.CountryCode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.ErrorMapKeys;
import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.InternalServerErrorMessage;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.constraints.EnumPaymentConstraint;
import com.capgemini.psd2.pisp.domain.OBBranchAndFinancialInstitutionIdentification3;
import com.capgemini.psd2.pisp.domain.OBExchangeRate1;
import com.capgemini.psd2.pisp.domain.OBExchangeRateType2Code;
import com.capgemini.psd2.pisp.domain.OBExternalPaymentContext1Code;
import com.capgemini.psd2.pisp.domain.OBExternalStatus2Code;
import com.capgemini.psd2.pisp.domain.OBMultiAuthorisation1;
import com.capgemini.psd2.pisp.domain.OBPostalAddress6;
import com.capgemini.psd2.pisp.domain.OBRisk1;
import com.capgemini.psd2.pisp.domain.OBRisk1DeliveryAddress;
import com.capgemini.psd2.pisp.domain.OBWriteInternationalConsent1;
import com.capgemini.psd2.pisp.utilities.PispDateUtility;
import com.capgemini.psd2.utilities.NullCheckUtils;
import com.capgemini.psd2.utilities.PSD2IBANUtil;

@Lazy
@Component
@ConfigurationProperties("app")
public class CommonPaymentValidations {

	@Autowired
	private RequestHeaderAttributes reqHeaderAtrributes;

	@Lazy
	@Autowired
	private PispDateUtility pispDateUtility;

	@Value("${app.sortCodeAccountNumberPattern:^[0-9]{6}[0-9]{8}$}")
	private String sortCodeAccountNumberPattern;
	
	@Value("${app.validPanIdentification:[a-zA-Z0-9]+}")
	private String validPanIdentification;
	
	private final Pattern bbanPattern = Pattern.compile("^[A-Z]{4}\\d{14}$");

	private List<String> validSchemeNameList = new ArrayList<>();
	private List<String> validLocalInstrumentList = new ArrayList<>();
	private List<String> validFileTypeList = new ArrayList<>();
	private List<String> validAgentSchemeList = new ArrayList<>();

	public List<String> getValidSchemeNameList() {
		return validSchemeNameList;
	}

	public List<String> getValidLocalInstrumentList() {
		return validLocalInstrumentList;
	}

	public List<String> getValidFileTypeList() {
		return validFileTypeList;
	}

	public List<String> getValidAgentSchemeList() {
		return validAgentSchemeList;
	}

	public void validateSchemeNameWithIdentification(String inputScheme, String identification,
			OBErrorCodeEnum errorCodeEnum) {

		if (validSchemeNameList.contains(inputScheme) == false)

			if (errorCodeEnum == null) {
				throw PSD2Exception.populatePSD2Exception(
						new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNSUPPORTED_SCHEME, ErrorMapKeys.INVALID_SCHEME));
			} else {
				throw PSD2Exception.populatePSD2Exception(
						new ExceptionDTO(errorCodeEnum, InternalServerErrorMessage.INVALID_SCHEME_RESPONSE));
			}

		switch (inputScheme) {
		case PaymentSetupConstants.UK_OBIE_IBAN:
		case PaymentSetupConstants.IBAN:
			validateIBAN(identification, errorCodeEnum);
			break;
		case PaymentSetupConstants.UK_OBIE_SORTCODEACCOUNTNUMBER:
		case PaymentSetupConstants.SORTCODEACCOUNTNUMBER:
			validateSortCodeAccountNumber(identification, errorCodeEnum);
			break;
		case PaymentSetupConstants.UK_OBIE_PAN:
		case PaymentSetupConstants.PAN:
			validatePAN(identification, errorCodeEnum);
			break;
		case PaymentSetupConstants.UK_OBIE_BBAN:
			validateBBAN(identification, errorCodeEnum);
			break;
		case PaymentSetupConstants.UK_OBIE_PAYM:
			validatePaym(identification, errorCodeEnum);
			break;
		}
	}

	private void validateIBAN(String iban, OBErrorCodeEnum errorCodeEnum) {
		try {
			PSD2IBANUtil.validate(iban.trim());
		} catch (Exception exception) {
			if (errorCodeEnum == null) {
				throw PSD2Exception
						.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNSUPPORTED_ACCOUNTIDENTIFIER,
								ErrorMapKeys.ACCOUNT_IDENTIFIER_INVALID));
			} else {
				throw PSD2Exception.populatePSD2Exception(
						new ExceptionDTO(errorCodeEnum, InternalServerErrorMessage.IBAN_VALIDATION_FAILED));
			}
		}

	}

	private void validateSortCodeAccountNumber(String identification, OBErrorCodeEnum errorCodeEnum) {
		if (!identification.matches(sortCodeAccountNumberPattern)) {
			if (errorCodeEnum == null) {
				throw PSD2Exception
						.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNSUPPORTED_ACCOUNTIDENTIFIER,
								ErrorMapKeys.ACCOUNT_IDENTIFIER_INVALID));
			} else {
				throw PSD2Exception.populatePSD2Exception(
						new ExceptionDTO(errorCodeEnum, InternalServerErrorMessage.INVALID_SORTCODE_NUMBER));
			}
		}
	}

	private void validateBBAN(String bban, OBErrorCodeEnum errorCodeEnum) {
		try {
			Matcher matcher = bbanPattern.matcher(bban);
			if (!matcher.find()) {
				if (errorCodeEnum == null) {
					throw PSD2Exception.populatePSD2Exception(
							new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNSUPPORTED_ACCOUNTIDENTIFIER,
									ErrorMapKeys.ACCOUNT_IDENTIFIER_INVALID));
				} else {
					throw PSD2Exception.populatePSD2Exception(
							new ExceptionDTO(errorCodeEnum, InternalServerErrorMessage.BBAN_VALIDATION_FAILED));
				}
			}
		} catch (Exception exception) {
			if (errorCodeEnum == null) {
				throw PSD2Exception
						.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNSUPPORTED_ACCOUNTIDENTIFIER,
								ErrorMapKeys.ACCOUNT_IDENTIFIER_INVALID));
			} else {
				throw PSD2Exception.populatePSD2Exception(
						new ExceptionDTO(errorCodeEnum, InternalServerErrorMessage.BBAN_VALIDATION_FAILED));
			}
		}
	}

	private void validatePAN(String identification, OBErrorCodeEnum errorCodeEnum) {
		if (!identification.matches(validPanIdentification)) {
			if (errorCodeEnum == null) {
				throw PSD2Exception
						.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNSUPPORTED_ACCOUNTIDENTIFIER,
								ErrorMapKeys.ACCOUNT_IDENTIFIER_INVALID));
			} else {
				throw PSD2Exception.populatePSD2Exception(
						new ExceptionDTO(errorCodeEnum, InternalServerErrorMessage.INVALID_PAN_NUMBER));
			}
		}
	}

	private void validatePaym(String identification, OBErrorCodeEnum errorCode) {

	}

	public void validateISOCountry(String countryCode, OBErrorCodeEnum errorCode) {
		if (null != countryCode && CountryCode.getByCode(countryCode) == null) {
			if (errorCode == null)
				throw PSD2Exception.populatePSD2Exception(
						new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_FIELD_INVALID, ErrorMapKeys.INVALID_COUNTRY_CODE));
			else
				throw PSD2Exception.populatePSD2Exception(
						new ExceptionDTO(errorCode, InternalServerErrorMessage.INVALID_COUNTRY_CODE));
		}
	}

	public boolean isValidCurrency(String currencyCode, OBErrorCodeEnum errorCode) {

		if (isEmpty(currencyCode)) {
			if (errorCode == null)
				throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNSUPPORTED_CURRENCY,
						ErrorMapKeys.CURRENCY_NOT_SUPPORTED));
			else
				throw PSD2Exception.populatePSD2Exception(
						new ExceptionDTO(errorCode, InternalServerErrorMessage.UNSUPPORTED_CURRENCY));
		}
		try {
			Currency.getInstance(currencyCode.trim());
			return true;
		} catch (Exception exception) {
			if (errorCode == null)
				throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNSUPPORTED_CURRENCY,
						ErrorMapKeys.CURRENCY_NOT_SUPPORTED));
			else
				throw PSD2Exception.populatePSD2Exception(
						new ExceptionDTO(errorCode, InternalServerErrorMessage.UNSUPPORTED_CURRENCY));
		}
	}

	private boolean isEmpty(String data) {
		boolean status = false;
		if (data == null || data.trim().length() == 0)
			status = true;

		return status;
	}

	public void validateSchemeNameWithSecondaryIdentification(String inputScheme, String secondaryIdentification) {
		if (validSchemeNameList.contains(inputScheme) == false)
			throw PSD2Exception.populatePSD2Exception(
					new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNSUPPORTED_SCHEME, ErrorMapKeys.INVALID_SCHEME));

		switch (inputScheme) {
		case PaymentSetupConstants.UK_OBIE_IBAN:
		case PaymentSetupConstants.IBAN:
		case PaymentSetupConstants.UK_OBIE_SORTCODEACCOUNTNUMBER:
		case PaymentSetupConstants.SORTCODEACCOUNTNUMBER:
			validateSecondaryIdentification(secondaryIdentification);
			break;
		}
	}

	private void validateSecondaryIdentification(String secondaryIdentification) {
		if (NullCheckUtils.isNullOrEmpty(secondaryIdentification)
				|| secondaryIdentification.length() > EnumPaymentConstraint.SECONDARYIDENTIFCATION_LENGTH.getSize())
			throw PSD2Exception.populatePSD2Exception(
					new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNSUPPORTED_ACCOUNTSECONDARYIDENTIFIER,
							ErrorMapKeys.ACCOUNT_SECONDARYIDENTIFIER_INVALID));

	}

	public void validateIdentification(String identification) {
		if (NullCheckUtils.isNullOrEmpty(identification))
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(
					OBErrorCodeEnum.UK_OBIE_UNSUPPORTED_ACCOUNTIDENTIFIER, ErrorMapKeys.ACCOUNT_IDENTIFIER_INVALID));
	}

	public void validateDomesticAddressLine(List<String> addressLine, OBErrorCodeEnum errorCodeEnum) {
		if (!NullCheckUtils.isNullOrEmpty(addressLine)) {
			for (String addressLineValue : addressLine) {
				if (!NullCheckUtils.isNullOrEmpty(addressLineValue)) {
					if (addressLineValue.length() > EnumPaymentConstraint.ADDRESSLINE_LENGTH.getSize()) {
						if (errorCodeEnum == null) {
							throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(
									OBErrorCodeEnum.UK_OBIE_FIELD_INVALID, ErrorMapKeys.INVALID_ADDRESS_LINE));
						} else {
							throw PSD2Exception.populatePSD2Exception(
									new ExceptionDTO(errorCodeEnum, InternalServerErrorMessage.INVALID_ADDRESS_LENGTH));
						}
					}
				} else {
					if (errorCodeEnum == null) {
						throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(
								OBErrorCodeEnum.UK_OBIE_FIELD_INVALID, ErrorMapKeys.INVALID_ADDRESS_LINE));
					} else {
						throw PSD2Exception.populatePSD2Exception(
								new ExceptionDTO(errorCodeEnum, InternalServerErrorMessage.INVALID_ADDRESS_LENGTH));
					}
				}
			}
		}
	}

	public void validateDomesticCountrySubDivision(String countrySubDivision) {
		if (!NullCheckUtils.isNullOrEmpty(countrySubDivision)
				&& countrySubDivision.length() > EnumPaymentConstraint.COUNTRYSUBDIVISION_LENGTH.getSize())
			throw PSD2Exception.populatePSD2Exception(
					new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_FIELD_INVALID, ErrorMapKeys.INVALID_COUNTRY_SUBDIVISION));

	}

	public void validateDomesticCountrySubDivision(List<String> countrySubDivision, OBErrorCodeEnum errorCodeEnum) {
		if (!NullCheckUtils.isNullOrEmpty(countrySubDivision)) {
			int countrySubDivisionLength = 0;
			for (String countrySubDivisionValue : countrySubDivision) {
				if (!NullCheckUtils.isNullOrEmpty(countrySubDivisionValue)) {
					countrySubDivisionLength += countrySubDivisionValue.length();
				}
			}
			if (countrySubDivisionLength > EnumPaymentConstraint.COUNTRYSUBDIVISION_LENGTH.getSize()) {
				if (errorCodeEnum == null) {
					throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_FIELD_INVALID,
							ErrorMapKeys.INVALID_COUNTRY_SUBDIVISION));
				} else {
					throw PSD2Exception.populatePSD2Exception(
							new ExceptionDTO(errorCodeEnum, InternalServerErrorMessage.INVALID_COUNTRY_SUBDIVISION));
				}
			}
		}
	}

	public void validateLocalInstrument(String localInstrument, OBErrorCodeEnum errorCode) {

		if (null != localInstrument && validLocalInstrumentList.contains(localInstrument) == false) {
			if (errorCode == null)
				throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(
						OBErrorCodeEnum.UK_OBIE_UNSUPPORTED_LOCALINSTRUMENT, ErrorMapKeys.LOCALINSTRUMENT_INVALID));
			else
				throw PSD2Exception.populatePSD2Exception(
						new ExceptionDTO(errorCode, InternalServerErrorMessage.INVALID_LOCAL_INSTRUMENT));
		}
	}

	public void validateDomesticRiskDeliveryAddress(OBRisk1DeliveryAddress obRisk1DeliveryAddress,
			OBErrorCodeEnum errorCodeEnum) {
		if (!NullCheckUtils.isNullOrEmpty(obRisk1DeliveryAddress)) {
			validateDomesticAddressLine(obRisk1DeliveryAddress.getAddressLine(), errorCodeEnum);
			validateISOCountry(obRisk1DeliveryAddress.getCountry(), errorCodeEnum);
		}
	}

	public void validateDomesticCreditorPostalAddress(OBPostalAddress6 creditorPostalAddress,
			OBErrorCodeEnum errorCodeEnum) {
		if (!NullCheckUtils.isNullOrEmpty(creditorPostalAddress)) {
			validateDomesticAddressLine(creditorPostalAddress.getAddressLine(), errorCodeEnum);
			validateDomesticCountrySubDivision(creditorPostalAddress.getCountrySubDivision());
			validateISOCountry(creditorPostalAddress.getCountry(), errorCodeEnum);
		}
	}

	/*
	 * Method name should be changed to validatePaymentContext, and needs to be used
	 * in all apis.
	 */
	public void validatePaymentContext(OBRisk1 obRisk1) {
		if (obRisk1.getPaymentContextCode() != null) {

			if (NullCheckUtils.isNullOrEmpty(obRisk1.getMerchantCategoryCode())
					|| NullCheckUtils.isNullOrEmpty(obRisk1.getMerchantCustomerIdentification())) {

				if (OBExternalPaymentContext1Code.ECOMMERCEGOODS.equals(obRisk1.getPaymentContextCode())
						|| OBExternalPaymentContext1Code.ECOMMERCESERVICES.equals(obRisk1.getPaymentContextCode())) {
					throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_FIELD_INVALID,
							ErrorMapKeys.PAYMENT_CONTEXT_ERROR));
				}
			}

			if (OBExternalPaymentContext1Code.ECOMMERCEGOODS.equals(obRisk1.getPaymentContextCode())
					&& NullCheckUtils.isNullOrEmpty(obRisk1.getDeliveryAddress())) {

				throw PSD2Exception.populatePSD2Exception(
						new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_FIELD_INVALID, ErrorMapKeys.PAYMENT_CONTEXT_ERROR));
			}
		}
	}

	/*
	 * Method name should be changed to validatePaymentContextforResponse, and needs to be used
	 * in all apis.
	 */
	public void validatePaymentContextforResponse(OBRisk1 obRisk1) {
		if (obRisk1.getPaymentContextCode() != null) {

			if (NullCheckUtils.isNullOrEmpty(obRisk1.getMerchantCategoryCode())
					|| NullCheckUtils.isNullOrEmpty(obRisk1.getMerchantCustomerIdentification())) {

				if (OBExternalPaymentContext1Code.ECOMMERCEGOODS.equals(obRisk1.getPaymentContextCode())
						|| OBExternalPaymentContext1Code.ECOMMERCESERVICES.equals(obRisk1.getPaymentContextCode())) {
					throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
							ErrorMapKeys.PAYMENT_CONTEXT_ERROR));
				}
			}

			if (OBExternalPaymentContext1Code.ECOMMERCEGOODS.equals(obRisk1.getPaymentContextCode())
					&& NullCheckUtils.isNullOrEmpty(obRisk1.getDeliveryAddress())) {

				throw PSD2Exception.populatePSD2Exception(
						new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR, ErrorMapKeys.PAYMENT_CONTEXT_ERROR));
			}
		}
	}

	public void validateHeaders() {
		if (NullCheckUtils.isNullOrEmpty(reqHeaderAtrributes.getIdempotencyKey()))
			throw PSD2Exception.populatePSD2Exception(
					new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_HEADER_MISSING, ErrorMapKeys.HEADER));
		if (!reqHeaderAtrributes.getIdempotencyKey().matches("^(?!\\s)(.*)(\\S)$"))
			throw PSD2Exception.populatePSD2Exception(
					new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_HEADER_INVALID, ErrorMapKeys.HEADER));
		if (reqHeaderAtrributes.getIdempotencyKey().trim().length() > EnumPaymentConstraint.IDEMPOTENCY_KEY_LENGTH
				.getSize())
			throw PSD2Exception.populatePSD2Exception(
					new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_HEADER_INVALID, ErrorMapKeys.HEADER));
	}

	/*
	 * Method should be removed, duplicate method
	 */
	public void validateInternationalPaymentContext(OBRisk1 risk) {
		if (risk.getPaymentContextCode() != null) {

			if (NullCheckUtils.isNullOrEmpty(risk.getMerchantCategoryCode())
					|| NullCheckUtils.isNullOrEmpty(risk.getMerchantCustomerIdentification())) {

				if (OBExternalPaymentContext1Code.ECOMMERCEGOODS.equals(risk.getPaymentContextCode())
						|| OBExternalPaymentContext1Code.ECOMMERCESERVICES.equals(risk.getPaymentContextCode())) {
					throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_FIELD_INVALID,
							ErrorMapKeys.PAYMENT_CONTEXT_ERROR));
				}
			}

			if (OBExternalPaymentContext1Code.ECOMMERCEGOODS.equals(risk.getPaymentContextCode())
					&& NullCheckUtils.isNullOrEmpty(risk.getDeliveryAddress())) {

				throw PSD2Exception.populatePSD2Exception(
						new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_FIELD_INVALID, ErrorMapKeys.PAYMENT_CONTEXT_ERROR));
			}
		}

	}

	public void validateInternationalRiskDeliveryAddress(OBRisk1DeliveryAddress deliveryAddress,
			OBErrorCodeEnum errorCodeEnum) {
		if (!NullCheckUtils.isNullOrEmpty(deliveryAddress)) {
			validateDomesticAddressLine(deliveryAddress.getAddressLine(), errorCodeEnum);
			validateISOCountry(deliveryAddress.getCountry(), errorCodeEnum);
		}
	}

	public void validateExchangeRate(OBExchangeRate1 exchangeRateInformation) {
		if (OBExchangeRateType2Code.AGREED.equals(exchangeRateInformation.getRateType())) {
			if (NullCheckUtils.isNullOrEmpty(exchangeRateInformation.getContractIdentification())
					|| NullCheckUtils.isNullOrEmpty(exchangeRateInformation.getExchangeRate())) {
				throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_FIELD_EXPECTED,
						ErrorMapKeys.EXPECTED_EXCHANGE_DETAILS));
			}
		}

		if (OBExchangeRateType2Code.ACTUAL.equals(exchangeRateInformation.getRateType())
				|| OBExchangeRateType2Code.INDICATIVE.equals(exchangeRateInformation.getRateType())) {
			if (!NullCheckUtils.isNullOrEmpty(exchangeRateInformation.getContractIdentification())
					|| !NullCheckUtils.isNullOrEmpty(exchangeRateInformation.getExchangeRate())) {
				throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_FIELD_UNEXPECTED,
						ErrorMapKeys.UNEXPECTED_EXCHANGE_DETAILS));
			}
		}

	}

	public void validateFileType(String fileType, ErrorCodeEnum errorCode) {

		if (null != fileType && validFileTypeList.contains(fileType) == false) {
			if (errorCode == null)
				throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_FIELD_INVALID,
						ErrorMapKeys.PISP_FILETYPE_NOT_VALID));
			else
				throw PSD2Exception.populatePSD2Exception(errorCode);
		}
	}

	/* validate and compare */
	public void validateDateTime(String dateTime) {

		pispDateUtility.validateDateTimeInRequest(dateTime);

		String transformedDate = pispDateUtility.transformDateTimeInRequest(dateTime);
		if (OffsetDateTime.parse(transformedDate).isBefore(OffsetDateTime.now())) {
			throw PSD2Exception.populatePSD2Exception(
					new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_FIELD_INVALIDDATE, ErrorMapKeys.INVALID_DATE));
		}
	}

	/* validate for request dates */
	public void validateAndParseDateTimeFormat(String dateTimeString) {

		pispDateUtility.validateDateTimeInRequest(dateTimeString);

	}

	/* validate for response dates */
	public void validateAndParseDateTimeFormatForResponse(String dateTimeString) {

		pispDateUtility.validateDateTimeInResponse(dateTimeString);

	}

	public void validateInstructionPriority(OBWriteInternationalConsent1 request) {
		if (null == request.getData().getInitiation().getInstructionPriority()) {
			if (null != request.getData().getInitiation().getExchangeRateInformation()
					&& (null != request.getData().getInitiation().getExchangeRateInformation().getRateType())
					&& OBExchangeRateType2Code.AGREED
							.equals(request.getData().getInitiation().getExchangeRateInformation().getRateType())) {
				throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_FIELD_UNEXPECTED,
						ErrorMapKeys.INSTRUCTION_PRIORITY_RATE_TYPE));
			}
		}
	}

	public void validateMerchantCategoryCode(String merchantCategoryCode, String merchantCategoryCodeRegexValidator) {
		if (!NullCheckUtils.isNullOrEmpty(merchantCategoryCode)) {
			if (!merchantCategoryCode.matches(merchantCategoryCodeRegexValidator)) {
				throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_FIELD_INVALID,
						ErrorMapKeys.MERCHANT_CATEGORY_CODE_INVALID));
			}
		}
	}

	/**
	 * @param creditorAgent
	 * 
	 *            CreditorAgent must at least have either or both of these pairs
	 *            - Scheme Name and Identification or Name and Postal Address
	 * @param ukObieUnexpectederror
	 */
	public void validateCreditorAgent(OBBranchAndFinancialInstitutionIdentification3 creditorAgent,
			OBErrorCodeEnum errorCodeEnum) {
		// validating Scheme Name with the allowed list (bicfi...)
		if (null != creditorAgent.getSchemeName()) {
			if (validAgentSchemeList.contains(creditorAgent.getSchemeName()) == false) {
				if (errorCodeEnum == null) {
					throw PSD2Exception.populatePSD2Exception(
							new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNSUPPORTED_SCHEME, ErrorMapKeys.INVALID_SCHEME));
				} else {
					throw PSD2Exception.populatePSD2Exception(
							new ExceptionDTO(errorCodeEnum, InternalServerErrorMessage.INVALID_SCHEME_RESPONSE));
				}
			}
		}

		// validating Scheme Name and Identification pair
		if ((NullCheckUtils.isNullOrEmpty(creditorAgent.getSchemeName())
				&& !NullCheckUtils.isNullOrEmpty(creditorAgent.getIdentification()))
				|| (NullCheckUtils.isNullOrEmpty(creditorAgent.getIdentification())
						&& !NullCheckUtils.isNullOrEmpty(creditorAgent.getSchemeName()))) {
			if (errorCodeEnum == null) {
				throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_FIELD_MISSING,
						ErrorMapKeys.CREDITOR_AGENT_INCOMPLETE));
			} else {
				throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(errorCodeEnum,
						InternalServerErrorMessage.CREDITOR_OR_DEBTOR_AGENT_INCOMPLETE));
			}
		}

		// validating Name and Postal Address pair
		else if ((NullCheckUtils.isNullOrEmpty(creditorAgent.getName())
				&& !NullCheckUtils.isNullOrEmpty(creditorAgent.getPostalAddress()))
				|| (NullCheckUtils.isNullOrEmpty(creditorAgent.getPostalAddress())
						&& !NullCheckUtils.isNullOrEmpty(creditorAgent.getName()))) {
			if (errorCodeEnum == null) {
				throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_FIELD_MISSING,
						ErrorMapKeys.CREDITOR_AGENT_INCOMPLETE));
			} else {
				throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(errorCodeEnum,
						InternalServerErrorMessage.CREDITOR_OR_DEBTOR_AGENT_INCOMPLETE));
			}

		}
		// validating the presence of atleast one pair
		else if (NullCheckUtils.isNullOrEmpty(creditorAgent.getSchemeName())
				&& NullCheckUtils.isNullOrEmpty(creditorAgent.getIdentification())
				&& NullCheckUtils.isNullOrEmpty(creditorAgent.getName())
				&& NullCheckUtils.isNullOrEmpty(creditorAgent.getPostalAddress())) {
			if (errorCodeEnum == null) {
				throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_FIELD_MISSING,
						ErrorMapKeys.CREDITOR_AGENT_INCOMPLETE));
			} else {
				throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(errorCodeEnum,
						InternalServerErrorMessage.CREDITOR_OR_DEBTOR_AGENT_INCOMPLETE));
			}

		}
	}

	public void validateMultiAuth(OBMultiAuthorisation1 multiAuth) {
		if (!NullCheckUtils.isNullOrEmpty(multiAuth)) {
			if (!NullCheckUtils.isNullOrEmpty(multiAuth.getStatus())) {
				Integer required = multiAuth.getNumberRequired();
				Integer recieved = multiAuth.getNumberReceived();
				boolean flag = false;
				if (!NullCheckUtils.isNullOrEmpty(recieved) && !NullCheckUtils.isNullOrEmpty(required)) {
					if (Integer.valueOf(recieved) > 0 && Integer.valueOf(required) > 0) {
						if (Integer.valueOf(recieved) > Integer.valueOf(required)) {
							flag = true;
						} else if (recieved.equals(required)) {
							if (!multiAuth.getStatus().equals(OBExternalStatus2Code.AUTHORISED)) {
								flag = true;
							}
						} else {
							if (!multiAuth.getStatus().equals(OBExternalStatus2Code.AWAITINGFURTHERAUTHORISATION)) {
								flag = true;
							}
						}
					} else {
						if (!multiAuth.getStatus().equals(OBExternalStatus2Code.REJECTED)) {
							flag = true;
						}

						if (flag) {
							throw PSD2Exception
									.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
											InternalServerErrorMessage.MULTI_AUTH_ISSUE));
						}
					}
				}

				if (!NullCheckUtils.isNullOrEmpty(multiAuth.getExpirationDateTime()))
					validateAndParseDateTimeFormatForResponse(multiAuth.getExpirationDateTime());
				if (!NullCheckUtils.isNullOrEmpty(multiAuth.getLastUpdateDateTime()))
					validateAndParseDateTimeFormatForResponse(multiAuth.getLastUpdateDateTime());
			} else {
				throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
						InternalServerErrorMessage.MULTI_AUTH_STATUS_MISSING));
			}
		}
	}

	// For payment to be international, COT != UC - removed validateCurrencyCombination
}