package com.capgemini.psd2.pisp.validation.adapter.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.exceptions.ErrorMapKeys;
import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.pisp.domain.CustomDPaymentConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomDPaymentConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.CustomDPaymentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.DPaymentsRetrieveGetRequest;
import com.capgemini.psd2.pisp.domain.OBCharge1;
import com.capgemini.psd2.pisp.domain.OBWriteDomesticConsent1;
import com.capgemini.psd2.pisp.domain.PaymentDomesticSubmitPOST201Response;
import com.capgemini.psd2.pisp.domain.PaymentRetrieveGetRequest;
import com.capgemini.psd2.pisp.processing.adapter.PaymentConsentsCustomValidator;
import com.capgemini.psd2.pisp.processing.adapter.PaymentSubmissionCustomValidator;
import com.capgemini.psd2.pisp.validation.adapter.constants.CommonPaymentValidations;
import com.capgemini.psd2.utilities.NullCheckUtils;
import com.capgemini.psd2.validator.PSD2Validator;


@Component("domesticPaymentValidator")
@ConfigurationProperties("app")
public class DomesticPaymentValidatorImpl implements
		PaymentConsentsCustomValidator<CustomDPaymentConsentsPOSTRequest, CustomDPaymentConsentsPOSTResponse>,
		PaymentSubmissionCustomValidator<CustomDPaymentsPOSTRequest, PaymentDomesticSubmitPOST201Response> {

	@Autowired
	private PSD2Validator psd2Validator; //NOSONAR

	@Value("${app.regex.paymentId:#{null}}")
	private String paymentIdRegexValidator; //NOSONAR

	@Value("${app.swaggerValidations.request:#{true}}")
	private Boolean reqValidationEnabled; //NOSONAR

	@Value("${app.swaggerValidations.response:#{true}}")
	private Boolean resValidationEnabled; //NOSONAR

	@Value("${app.pattern.merchantCategoryCode:\\d+}")
	private String merchantCategoryCodeRegexValidator; //NOSONAR

	@Autowired
	private CommonPaymentValidations commonPaymentValidations; //NOSONAR

	// Interfaces for DomesticPayment Payments v3.0 API
	@Override
	public boolean validateConsentRequest(CustomDPaymentConsentsPOSTRequest paymentConsentsRequest) {
		commonPaymentValidations.validateHeaders();
		if (reqValidationEnabled)
			executeDomesticConsentsRequestSwaggerValidations(paymentConsentsRequest);
		executeDomesticConsentsRequestCustomValidations(paymentConsentsRequest);
		return Boolean.TRUE;
	}

	@Override
	public boolean validatePaymentConsentResponse(CustomDPaymentConsentsPOSTResponse paymentConsentsResponse) {
		if (resValidationEnabled)
			executeDomesticConsentsResponseSwaggerValidations(paymentConsentsResponse);
		executeDomesticConsentsResponseCustomValidations(paymentConsentsResponse);
		return true;
	}

	@Override
	public void validatePaymentsPOSTRequest(CustomDPaymentsPOSTRequest paymentSubmissionRequest) {
		commonPaymentValidations.validateHeaders();

		executeDomesticSubmissionRequestCustomValidations(paymentSubmissionRequest);
	}

	@Override
	public boolean validateConsentsGETRequest(PaymentRetrieveGetRequest paymentRetrieveRequest) {
		psd2Validator.validateRequestWithSwagger(paymentRetrieveRequest,
				OBErrorCodeEnum.UK_OBIE_FIELD_INVALID.toString());
		return Boolean.TRUE;
	}

	@Override // GET
	public void validatePaymentsResponse(PaymentDomesticSubmitPOST201Response paymentSubmissionResponse) {
		if (resValidationEnabled)
			executeDomesticSubmissionResponseSwaggerValidations(paymentSubmissionResponse);
		executeDomesticSubmissionResponseCustomValidations(paymentSubmissionResponse);
	}

	public boolean validateDomesticPaymentsGETRequest(DPaymentsRetrieveGetRequest submissionRetrieveRequest) {
		// validate request parameter domestic payment id
		psd2Validator.validate(submissionRetrieveRequest);
		return Boolean.TRUE;
	}

	private void executeDomesticConsentsRequestSwaggerValidations(OBWriteDomesticConsent1 paymentConsentsRequest) {
		psd2Validator.validateRequestWithSwagger(paymentConsentsRequest,
				OBErrorCodeEnum.UK_OBIE_FIELD_INVALID.toString());
	}
	
	private void executeDomesticConsentsRequestCustomValidations(OBWriteDomesticConsent1 paymentConsentsRequest) {
		// validate currency
		commonPaymentValidations.isValidCurrency(
				paymentConsentsRequest.getData().getInitiation().getInstructedAmount().getCurrency(), null);

		// validate creditorAccount SchemeName Identification
		if (paymentConsentsRequest.getData().getInitiation().getCreditorAccount() != null) {
			commonPaymentValidations.validateSchemeNameWithIdentification(
					paymentConsentsRequest.getData().getInitiation().getCreditorAccount().getSchemeName(),
					paymentConsentsRequest.getData().getInitiation().getCreditorAccount().getIdentification(), null);

			if (!NullCheckUtils.isNullOrEmpty(paymentConsentsRequest.getData().getInitiation().getCreditorAccount()
					.getSecondaryIdentification())) {
				commonPaymentValidations.validateSchemeNameWithSecondaryIdentification(
						paymentConsentsRequest.getData().getInitiation().getCreditorAccount().getSchemeName(),
						paymentConsentsRequest.getData().getInitiation().getCreditorAccount()
								.getSecondaryIdentification());
			}
		}

		// validate debtorAccount SchemeName Identification
		if (paymentConsentsRequest.getData().getInitiation().getDebtorAccount() != null) {
			commonPaymentValidations.validateSchemeNameWithIdentification(
					paymentConsentsRequest.getData().getInitiation().getDebtorAccount().getSchemeName(),
					paymentConsentsRequest.getData().getInitiation().getDebtorAccount().getIdentification(), null);

			if (!NullCheckUtils.isNullOrEmpty(
					paymentConsentsRequest.getData().getInitiation().getDebtorAccount().getSecondaryIdentification())) {
				commonPaymentValidations.validateSchemeNameWithSecondaryIdentification(
						paymentConsentsRequest.getData().getInitiation().getDebtorAccount().getSchemeName(),
						paymentConsentsRequest.getData().getInitiation().getDebtorAccount()
								.getSecondaryIdentification());
			}
		}

		// validate Payment Context
		commonPaymentValidations.validatePaymentContext(paymentConsentsRequest.getRisk());

		// validate Risk Delivery Address fields
		commonPaymentValidations
				.validateDomesticRiskDeliveryAddress(paymentConsentsRequest.getRisk().getDeliveryAddress(), null);

		// validate Creditor Postal Address fields
		commonPaymentValidations.validateDomesticCreditorPostalAddress(
				paymentConsentsRequest.getData().getInitiation().getCreditorPostalAddress(), null);

		// validate LocalInstrument
		commonPaymentValidations
				.validateLocalInstrument(paymentConsentsRequest.getData().getInitiation().getLocalInstrument(), null);

		// validate authorisation completion date time
		if (null != paymentConsentsRequest.getData().getAuthorisation()
				&& null != paymentConsentsRequest.getData().getAuthorisation().getCompletionDateTime()) {
			commonPaymentValidations
					.validateDateTime(paymentConsentsRequest.getData().getAuthorisation().getCompletionDateTime());
		}
	}

	private void executeDomesticSubmissionRequestCustomValidations(
			CustomDPaymentsPOSTRequest paymentSubmissionRequest) {

		// validate consent id
		validateDomesticPaymentId(paymentSubmissionRequest.getData().getConsentId());

		
	}

	/*
	 * Added on 14-July-2017
	 */

	private void validateDomesticPaymentId(String paymentId) {
		if (!paymentId.matches(paymentIdRegexValidator)) {
			throw PSD2Exception.populatePSD2Exception(
					new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_RESOURCE_NOTFOUND, ErrorMapKeys.INVALID_PAYMENT_ID));
		}
	}

	private void executeDomesticConsentsResponseCustomValidations(
			CustomDPaymentConsentsPOSTResponse paymentConsentsResponse) {
		commonPaymentValidations.validateDomesticRiskDeliveryAddress(
				paymentConsentsResponse.getRisk().getDeliveryAddress(), OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR);
		// validate Creditor Postal Address fields
		commonPaymentValidations.validateDomesticCreditorPostalAddress(
				paymentConsentsResponse.getData().getInitiation().getCreditorPostalAddress(),
				OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR);

		// validate debtorAccount SchemeName Identification
		if (paymentConsentsResponse.getData().getInitiation().getDebtorAccount() != null) {
			commonPaymentValidations.validateSchemeNameWithIdentification(
					paymentConsentsResponse.getData().getInitiation().getDebtorAccount().getSchemeName(),
					paymentConsentsResponse.getData().getInitiation().getDebtorAccount().getIdentification(),
					OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR);
		}

		// validate creditor SchemeName Identification
		if (paymentConsentsResponse.getData().getInitiation().getCreditorAccount() != null) {
			commonPaymentValidations.validateSchemeNameWithIdentification(
					paymentConsentsResponse.getData().getInitiation().getCreditorAccount().getSchemeName(),
					paymentConsentsResponse.getData().getInitiation().getCreditorAccount().getIdentification(),
					OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR);
		}

		if (paymentConsentsResponse.getData().getCharges() != null
				&& !paymentConsentsResponse.getData().getCharges().isEmpty()) {

			for (OBCharge1 charges : paymentConsentsResponse.getData().getCharges()) {
				commonPaymentValidations.isValidCurrency(charges.getAmount().getCurrency(),
						OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR);
			}
		}
		commonPaymentValidations.isValidCurrency(
				paymentConsentsResponse.getData().getInitiation().getInstructedAmount().getCurrency(),
				OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR);

		commonPaymentValidations.validateLocalInstrument(
				paymentConsentsResponse.getData().getInitiation().getLocalInstrument(),
				OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR);

		// validate authorisation completion date time
		if (null != paymentConsentsResponse.getData().getAuthorisation()
				&& null != paymentConsentsResponse.getData().getAuthorisation().getCompletionDateTime()) {
			commonPaymentValidations.validateAndParseDateTimeFormatForResponse(
					paymentConsentsResponse.getData().getAuthorisation().getCompletionDateTime());
		}

		// validate payment context
		commonPaymentValidations.validatePaymentContext(paymentConsentsResponse.getRisk());

		// Validate Merchant Category Code
		if (null != paymentConsentsResponse.getRisk()
				&& null != paymentConsentsResponse.getRisk().getMerchantCategoryCode()
				&& null != merchantCategoryCodeRegexValidator) {
			commonPaymentValidations.validateMerchantCategoryCode(
					paymentConsentsResponse.getRisk().getMerchantCategoryCode(), merchantCategoryCodeRegexValidator);
		}

		// Validate Date Fields in Response

		// 1. Status Update Date Time
		if (null != paymentConsentsResponse.getData().getStatusUpdateDateTime()) {
			commonPaymentValidations.validateAndParseDateTimeFormatForResponse(
					paymentConsentsResponse.getData().getStatusUpdateDateTime());
		}

		// 2.Expected Execution Date Time
		if (null != paymentConsentsResponse.getData().getExpectedExecutionDateTime()) {
			commonPaymentValidations.validateAndParseDateTimeFormatForResponse(
					paymentConsentsResponse.getData().getExpectedExecutionDateTime());
		}

		// 3. Expected Settlement Date Time
		if (null != paymentConsentsResponse.getData().getExpectedSettlementDateTime()) {
			commonPaymentValidations.validateAndParseDateTimeFormatForResponse(
					paymentConsentsResponse.getData().getExpectedSettlementDateTime());
		}

		// 4. Cut Off Date Time
		if (null != paymentConsentsResponse.getData().getCutOffDateTime()) {
			commonPaymentValidations
					.validateAndParseDateTimeFormatForResponse(paymentConsentsResponse.getData().getCutOffDateTime());
		}

	}

	private void executeDomesticConsentsResponseSwaggerValidations(
			CustomDPaymentConsentsPOSTResponse paymentConsentsResponse) {
		psd2Validator.validate(paymentConsentsResponse);
	}

	private void executeDomesticSubmissionResponseCustomValidations(
			PaymentDomesticSubmitPOST201Response paymentSubmissionResponse) {

		// validate Creditor Postal Address fields
		commonPaymentValidations.validateDomesticCreditorPostalAddress(
				paymentSubmissionResponse.getData().getInitiation().getCreditorPostalAddress(),
				OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR);

		// validate debtorAccount SchemeName Identification
		if (paymentSubmissionResponse.getData().getInitiation().getDebtorAccount() != null) {
			commonPaymentValidations.validateSchemeNameWithIdentification(
					paymentSubmissionResponse.getData().getInitiation().getDebtorAccount().getSchemeName(),
					paymentSubmissionResponse.getData().getInitiation().getDebtorAccount().getIdentification(),
					OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR);
		}

		// validate creditor SchemeName Identification
		if (paymentSubmissionResponse.getData().getInitiation().getCreditorAccount() != null) {
			commonPaymentValidations.validateSchemeNameWithIdentification(
					paymentSubmissionResponse.getData().getInitiation().getCreditorAccount().getSchemeName(),
					paymentSubmissionResponse.getData().getInitiation().getCreditorAccount().getIdentification(),
					OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR);
		}

		if (paymentSubmissionResponse.getData().getCharges() != null
				&& !paymentSubmissionResponse.getData().getCharges().isEmpty()) {

			for (OBCharge1 charges : paymentSubmissionResponse.getData().getCharges()) {
				commonPaymentValidations.isValidCurrency(charges.getAmount().getCurrency(),
						OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR);
			}
		}
		commonPaymentValidations.isValidCurrency(
				paymentSubmissionResponse.getData().getInitiation().getInstructedAmount().getCurrency(),
				OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR);

		commonPaymentValidations.validateLocalInstrument(
				paymentSubmissionResponse.getData().getInitiation().getLocalInstrument(),
				OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR);

		// Validate Date Fields in Response

		// 1. Status Update Date Time
		if (null != paymentSubmissionResponse.getData().getStatusUpdateDateTime()) {
			commonPaymentValidations.validateAndParseDateTimeFormatForResponse(
					paymentSubmissionResponse.getData().getStatusUpdateDateTime());
		}

		// 2.Expected Execution Date Time
		if (null != paymentSubmissionResponse.getData().getExpectedExecutionDateTime()) {
			commonPaymentValidations.validateAndParseDateTimeFormatForResponse(
					paymentSubmissionResponse.getData().getExpectedExecutionDateTime());
		}

		// 3. Expected Settlement Date Time
		if (null != paymentSubmissionResponse.getData().getExpectedSettlementDateTime()) {
			commonPaymentValidations.validateAndParseDateTimeFormatForResponse(
					paymentSubmissionResponse.getData().getExpectedSettlementDateTime());
		}

		// validate MultiAuth
		commonPaymentValidations.validateMultiAuth(paymentSubmissionResponse.getData().getMultiAuthorisation());

	}

	private void executeDomesticSubmissionResponseSwaggerValidations(
			PaymentDomesticSubmitPOST201Response paymentSubmissionResponse) {
		psd2Validator.validate(paymentSubmissionResponse);

	}
}