package com.capgemini.psd2.pisp.validation.adapter.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.exceptions.ErrorMapKeys;
import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.pisp.domain.CustomISPConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomISPConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.CustomISPaymentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomISPaymentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.OBCharge1;
import com.capgemini.psd2.pisp.domain.OBWriteInternationalScheduledConsent1;
import com.capgemini.psd2.pisp.domain.PaymentRetrieveGetRequest;
import com.capgemini.psd2.pisp.processing.adapter.PaymentConsentsCustomValidator;
import com.capgemini.psd2.pisp.processing.adapter.PaymentSubmissionCustomValidator;
import com.capgemini.psd2.pisp.validation.adapter.constants.CommonPaymentValidations;
import com.capgemini.psd2.utilities.NullCheckUtils;
import com.capgemini.psd2.validator.PSD2Validator;


@Component("internationalScheduledPaymentValidator")
@ConfigurationProperties("app")
public class InternationalScheduledPaymentValidatorImpl
		implements PaymentConsentsCustomValidator<CustomISPConsentsPOSTRequest, CustomISPConsentsPOSTResponse>,
		PaymentSubmissionCustomValidator<CustomISPaymentsPOSTRequest, CustomISPaymentsPOSTResponse> {

	@Autowired
	private PSD2Validator psd2Validator; //NOSONAR

	@Value("${app.regex.paymentId:#{null}}")
	private String paymentIdRegexValidator; //NOSONAR

	@Value("${app.swaggerValidations.request:#{true}}")
	private Boolean reqValidationEnabled; //NOSONAR

	@Value("${app.swaggerValidations.response:#{true}}")
	private Boolean resValidationEnabled; //NOSONAR

	@Value("${app.pattern.merchantCategoryCode:\\d+}")
	private String merchantCategoryCodeRegexValidator; //NOSONAR

	@Autowired
	private CommonPaymentValidations commonPaymentValidations; //NOSONAR

	@Override
	public boolean validateConsentRequest(CustomISPConsentsPOSTRequest scheduledPaymentConsentsRequest) {
		commonPaymentValidations.validateHeaders();
		if (reqValidationEnabled)
			executeInternationalConsentsRequestSwaggerValidations(scheduledPaymentConsentsRequest);
		executeInternationalConsentsRequestCustomValidations(scheduledPaymentConsentsRequest);
		return Boolean.TRUE;
	}

	@Override
	public boolean validateConsentsGETRequest(PaymentRetrieveGetRequest paymentRetrieveRequest) {
		psd2Validator.validateRequestWithSwagger(paymentRetrieveRequest,
				OBErrorCodeEnum.UK_OBIE_FIELD_INVALID.toString());
		return Boolean.TRUE;
	}

	@Override
	public boolean validatePaymentConsentResponse(CustomISPConsentsPOSTResponse scheduledPaymentConsentsResponse) {
		if (resValidationEnabled)
			executeInternationalConsentsResponseSwaggerValidations(scheduledPaymentConsentsResponse);
		executeInternationalConsentsResponseCustomValidations(scheduledPaymentConsentsResponse);
		return true;
	}

	@Override
	public void validatePaymentsPOSTRequest(CustomISPaymentsPOSTRequest submissionRequestBody) {
		commonPaymentValidations.validateHeaders();
		executeInternationalSubmissionRequestCustomValidations(submissionRequestBody);
	}

	@Override
	public void validatePaymentsResponse(CustomISPaymentsPOSTResponse paymentSubmissionResponse) {
		if (resValidationEnabled)
			executeInternationalSubmissionResponseSwaggerValidations(paymentSubmissionResponse);
		executeInternationalSubmissionResponseCustomValidations(paymentSubmissionResponse);
	}

	private void executeInternationalConsentsRequestSwaggerValidations(
			OBWriteInternationalScheduledConsent1 scheduledPaymentConsentsRequest) {
		psd2Validator.validateRequestWithSwagger(scheduledPaymentConsentsRequest,
				OBErrorCodeEnum.UK_OBIE_FIELD_INVALID.toString());
	}
	
	/*  Consent Request Validations:
		1 validate local instrument
		2 validate payment context
		3 validate Risk delivery address fields
		4 validate Creditor Postal Address fields
		5 validate CreditorAgent Postal Address fields
		6 validate Creditor Agent block
		7 validate debtorAccount SchemeName Identification
		8 validate creditorAccount SchemeName Identification
		9 validate ExchangeRateType combination fields
		10 Validate currency combinations
		11 Validate currencies (3) instructed, exchangeRate-UC, COT
		12 Validate Date Fields in Request (2) i.e. reqeuestedExecution, completion
	*/
	private void executeInternationalConsentsRequestCustomValidations(
			OBWriteInternationalScheduledConsent1 scheduledPaymentConsentsRequest) {

		// 1 validate LocalInstrument
		commonPaymentValidations.validateLocalInstrument(
				scheduledPaymentConsentsRequest.getData().getInitiation().getLocalInstrument(), null);

		// 2 validate Payment Context
		commonPaymentValidations.validateInternationalPaymentContext(scheduledPaymentConsentsRequest.getRisk());

		// 3 validate Risk Delivery Address fields
		commonPaymentValidations.validateDomesticRiskDeliveryAddress(
				scheduledPaymentConsentsRequest.getRisk().getDeliveryAddress(), null);

		// 4 validate Creditor Postal Address fields
		if (scheduledPaymentConsentsRequest.getData().getInitiation().getCreditor() != null)
			commonPaymentValidations.validateDomesticCreditorPostalAddress(
					scheduledPaymentConsentsRequest.getData().getInitiation().getCreditor().getPostalAddress(), null);

		// 5 validate CreditorAgent Postal Address fields
		if (scheduledPaymentConsentsRequest.getData().getInitiation().getCreditorAgent() != null)
			commonPaymentValidations.validateDomesticCreditorPostalAddress(
					scheduledPaymentConsentsRequest.getData().getInitiation().getCreditorAgent().getPostalAddress(),
					null);

		// 6 validate Creditor Agent
		if (null != scheduledPaymentConsentsRequest.getData().getInitiation().getCreditorAgent()) {
			commonPaymentValidations.validateCreditorAgent(
					scheduledPaymentConsentsRequest.getData().getInitiation().getCreditorAgent(), null);
		}

		// 7 validate debtorAccount SchemeName Identification
		if (scheduledPaymentConsentsRequest.getData().getInitiation().getDebtorAccount() != null) {
			commonPaymentValidations.validateSchemeNameWithIdentification(
					scheduledPaymentConsentsRequest.getData().getInitiation().getDebtorAccount().getSchemeName(),
					scheduledPaymentConsentsRequest.getData().getInitiation().getDebtorAccount().getIdentification(),
					null);

			if (!NullCheckUtils.isNullOrEmpty(scheduledPaymentConsentsRequest.getData().getInitiation()
					.getDebtorAccount().getSecondaryIdentification())) {
				commonPaymentValidations.validateSchemeNameWithSecondaryIdentification(
						scheduledPaymentConsentsRequest.getData().getInitiation().getDebtorAccount().getSchemeName(),
						scheduledPaymentConsentsRequest.getData().getInitiation().getDebtorAccount()
								.getSecondaryIdentification());
			}
		}

		// 8 validate creditorAccount SchemeName Identification
		if (scheduledPaymentConsentsRequest.getData().getInitiation().getCreditorAccount() != null) {
			commonPaymentValidations.validateSchemeNameWithIdentification(
					scheduledPaymentConsentsRequest.getData().getInitiation().getCreditorAccount().getSchemeName(),
					scheduledPaymentConsentsRequest.getData().getInitiation().getCreditorAccount().getIdentification(),
					null);

			if (!NullCheckUtils.isNullOrEmpty(scheduledPaymentConsentsRequest.getData().getInitiation()
					.getCreditorAccount().getSecondaryIdentification())) {
				commonPaymentValidations.validateSchemeNameWithSecondaryIdentification(
						scheduledPaymentConsentsRequest.getData().getInitiation().getCreditorAccount().getSchemeName(),
						scheduledPaymentConsentsRequest.getData().getInitiation().getCreditorAccount()
								.getSecondaryIdentification());
			}
		}

		// 9 validate ExchangeRateType combination fields
		if (null != scheduledPaymentConsentsRequest.getData().getInitiation().getExchangeRateInformation()) {
			commonPaymentValidations.validateExchangeRate(
					scheduledPaymentConsentsRequest.getData().getInitiation().getExchangeRateInformation());
		}

		// 10 Validate currency combinations -removed

		// 1 validate instructed currency
		commonPaymentValidations.isValidCurrency(
				scheduledPaymentConsentsRequest.getData().getInitiation().getInstructedAmount().getCurrency(), null);

		// 2 validate unit currency initiation exchange rate block
		if (scheduledPaymentConsentsRequest.getData().getInitiation().getExchangeRateInformation() != null)
			commonPaymentValidations.isValidCurrency(scheduledPaymentConsentsRequest.getData().getInitiation()
					.getExchangeRateInformation().getUnitCurrency(), null);

		// 3 validate currency of transfer
		commonPaymentValidations.isValidCurrency(
				scheduledPaymentConsentsRequest.getData().getInitiation().getCurrencyOfTransfer(), null);

		// 12 Validate Date Fields in Request
		// 1 Requested Execution Date Time
		commonPaymentValidations.validateDateTime(
				scheduledPaymentConsentsRequest.getData().getInitiation().getRequestedExecutionDateTime());

		// 2 validate authorisation completion date time
		if (null != scheduledPaymentConsentsRequest.getData().getAuthorisation()
				&& null != scheduledPaymentConsentsRequest.getData().getAuthorisation().getCompletionDateTime()) {
			commonPaymentValidations.validateDateTime(
					scheduledPaymentConsentsRequest.getData().getAuthorisation().getCompletionDateTime());
		}
	}

	private void executeInternationalConsentsResponseSwaggerValidations(
			CustomISPConsentsPOSTResponse scheduledPaymentConsentsResponse) {
		psd2Validator.validate(scheduledPaymentConsentsResponse);
	}

	/*  Consent Response Validations:
	1 validate local instrument
	2 validate payment context
	3 validate Risk delivery address fields
	4 validate Creditor Postal Address fields
	5 validate CreditorAgent Postal Address fields
	6 validate Creditor Agent block
	7 validate debtorAccount SchemeName Identification
	8 validate creditorAccount SchemeName Identification
	9 Validate currency combinations
	10 Validate currencies (5) i.e instructed, charge, exchangeRate-UC, data.exchangeRate-UC, COT
	11 Validate Date Fields in Response (7) i.e. statusUpdate, expectedExecution, settlement, cutOff, reqeuestedExecution, completion, expiration
    */
	private void executeInternationalConsentsResponseCustomValidations(
			CustomISPConsentsPOSTResponse scheduledPaymentConsentsResponse) {

		// 1 validate local instrument
		commonPaymentValidations.validateLocalInstrument(
				scheduledPaymentConsentsResponse.getData().getInitiation().getLocalInstrument(),
				OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR);

		// 2 validate payment context
		commonPaymentValidations.validateInternationalPaymentContext(scheduledPaymentConsentsResponse.getRisk());

		// 3 validate Risk delivery address fields
		commonPaymentValidations.validateDomesticRiskDeliveryAddress(
				scheduledPaymentConsentsResponse.getRisk().getDeliveryAddress(),
				OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR);

		// 4 validate Creditor Postal Address fields
		if (scheduledPaymentConsentsResponse.getData().getInitiation().getCreditor() != null)
			commonPaymentValidations.validateDomesticCreditorPostalAddress(
					scheduledPaymentConsentsResponse.getData().getInitiation().getCreditor().getPostalAddress(),
					OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR);

		// 5 validate CreditorAgent Postal Address fields
		if (scheduledPaymentConsentsResponse.getData().getInitiation().getCreditorAgent() != null)
			commonPaymentValidations.validateDomesticCreditorPostalAddress(
					scheduledPaymentConsentsResponse.getData().getInitiation().getCreditorAgent().getPostalAddress(),
					OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR);

		// 6 validate Creditor Agent block
		if (null != scheduledPaymentConsentsResponse.getData().getInitiation().getCreditorAgent()) {
			commonPaymentValidations.validateCreditorAgent(
					scheduledPaymentConsentsResponse.getData().getInitiation().getCreditorAgent(),
					OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR);
		}

		// 7 validate debtorAccount SchemeName Identification
		if (scheduledPaymentConsentsResponse.getData().getInitiation().getDebtorAccount() != null) {
			commonPaymentValidations.validateSchemeNameWithIdentification(
					scheduledPaymentConsentsResponse.getData().getInitiation().getDebtorAccount().getSchemeName(),
					scheduledPaymentConsentsResponse.getData().getInitiation().getDebtorAccount().getIdentification(),
					OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR);
		}

		// 8 validate creditorAccount SchemeName Identification
		if (scheduledPaymentConsentsResponse.getData().getInitiation().getCreditorAccount() != null) {
			commonPaymentValidations.validateSchemeNameWithIdentification(
					scheduledPaymentConsentsResponse.getData().getInitiation().getCreditorAccount().getSchemeName(),
					scheduledPaymentConsentsResponse.getData().getInitiation().getCreditorAccount().getIdentification(),
					OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR);
		}

		// 9 Validate currency combinations - removed

		// 1 validate currency in charges block
		if (scheduledPaymentConsentsResponse.getData().getCharges() != null
				&& !scheduledPaymentConsentsResponse.getData().getCharges().isEmpty()) {
			for (OBCharge1 charges : scheduledPaymentConsentsResponse.getData().getCharges()) {
				commonPaymentValidations.isValidCurrency(charges.getAmount().getCurrency(),
						OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR);
			}
		}

		// 2 validate instructed currency
		commonPaymentValidations.isValidCurrency(
				scheduledPaymentConsentsResponse.getData().getInitiation().getInstructedAmount().getCurrency(),
				OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR);

		// 3 validate unit currency in exchange rate block
		if (scheduledPaymentConsentsResponse.getData().getExchangeRateInformation() != null)
			commonPaymentValidations.isValidCurrency(
					scheduledPaymentConsentsResponse.getData().getExchangeRateInformation().getUnitCurrency(),
					OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR);

		// 4 validate unit currency initiation exchange rate block
		if (scheduledPaymentConsentsResponse.getData().getInitiation().getExchangeRateInformation() != null)
			commonPaymentValidations.isValidCurrency(scheduledPaymentConsentsResponse.getData().getInitiation()
					.getExchangeRateInformation().getUnitCurrency(), OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR);

		// 5 validate currency of transfer
		commonPaymentValidations.isValidCurrency(
				scheduledPaymentConsentsResponse.getData().getInitiation().getCurrencyOfTransfer(),
				OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR);

		// 11 Validate Date Fields in Response
		// 1. Status Update Date Time
		if (null != scheduledPaymentConsentsResponse.getData().getStatusUpdateDateTime()) {
			commonPaymentValidations.validateAndParseDateTimeFormatForResponse(
					scheduledPaymentConsentsResponse.getData().getStatusUpdateDateTime());
		}

		// 2.Expected Execution Date Time
		if (null != scheduledPaymentConsentsResponse.getData().getExpectedExecutionDateTime()) {
			commonPaymentValidations.validateAndParseDateTimeFormatForResponse(
					scheduledPaymentConsentsResponse.getData().getExpectedExecutionDateTime());
		}

		// 3. Expected Settlement Date Time
		if (null != scheduledPaymentConsentsResponse.getData().getExpectedSettlementDateTime()) {
			commonPaymentValidations.validateAndParseDateTimeFormatForResponse(
					scheduledPaymentConsentsResponse.getData().getExpectedSettlementDateTime());
		}

		// 4. Cut Off Date Time
		if (null != scheduledPaymentConsentsResponse.getData().getCutOffDateTime()) {
			commonPaymentValidations.validateAndParseDateTimeFormatForResponse(
					scheduledPaymentConsentsResponse.getData().getCutOffDateTime());
		}

		// 5. Requested Execution Date Time
		if (null != scheduledPaymentConsentsResponse.getData().getInitiation().getRequestedExecutionDateTime()) {
			commonPaymentValidations.validateAndParseDateTimeFormatForResponse(
					scheduledPaymentConsentsResponse.getData().getInitiation().getRequestedExecutionDateTime());
		}

		// 6 Authorisation completion date time
		if (null != scheduledPaymentConsentsResponse.getData().getAuthorisation()
				&& null != scheduledPaymentConsentsResponse.getData().getAuthorisation().getCompletionDateTime()) {
			commonPaymentValidations.validateAndParseDateTimeFormatForResponse(
					scheduledPaymentConsentsResponse.getData().getAuthorisation().getCompletionDateTime());
		}
		
		// 7. Exchange Rate Expiration Date Time
		if (null != scheduledPaymentConsentsResponse.getData().getExchangeRateInformation()
				&& null != scheduledPaymentConsentsResponse.getData().getExchangeRateInformation().getExpirationDateTime()) {
			commonPaymentValidations.validateAndParseDateTimeFormatForResponse(
					scheduledPaymentConsentsResponse.getData().getExchangeRateInformation().getExpirationDateTime());
		}

	}

	/*  Submission Request Validations:
	1 validate consent id 
	
*/
	private void executeInternationalSubmissionRequestCustomValidations(
			CustomISPaymentsPOSTRequest paymentSubmissionRequest) {

		// 1 validate consent id
		validateInternationalPaymentId(paymentSubmissionRequest.getData().getConsentId());
	}

	private void validateInternationalPaymentId(String paymentId) {
		if (!paymentId.matches(paymentIdRegexValidator)) {
			throw PSD2Exception.populatePSD2Exception(
					new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_RESOURCE_NOTFOUND, ErrorMapKeys.INVALID_PAYMENT_ID));
		}
	}

	/*  Submission Response Validations:
	1 validate local instrument
	2 validate Creditor Postal Address fields
	3 validate CreditorAgent Postal Address fields
	4 validate Creditor Agent block
	5 validate debtorAccount SchemeName Identification
	6 validate creditorAccount SchemeName Identification
	7 Validate currency combinations
	8 Validate currencies (5) i.e instructed, charge, exchangeRate-UC, data.exchangeRate-UC, COT
	9 Validate Date Fields in Response (5) i.e. statusUpdate, expectedExecution, settlement, reqeuestedExecution, expiration
	10 Validate MultiAuth
    */
	private void executeInternationalSubmissionResponseCustomValidations(
			CustomISPaymentsPOSTResponse paymentSubmissionResponse) {

		// 1 validate local instrument
		commonPaymentValidations.validateLocalInstrument(
				paymentSubmissionResponse.getData().getInitiation().getLocalInstrument(),
				OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR);

		// 2 validate Creditor Postal Address fields
		if (paymentSubmissionResponse.getData().getInitiation().getCreditor() != null)
			commonPaymentValidations.validateDomesticCreditorPostalAddress(
					paymentSubmissionResponse.getData().getInitiation().getCreditor().getPostalAddress(),
					OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR);

		// 3 validate CreditorAgent Postal Address fields
		if (paymentSubmissionResponse.getData().getInitiation().getCreditorAgent() != null)
			commonPaymentValidations.validateDomesticCreditorPostalAddress(
					paymentSubmissionResponse.getData().getInitiation().getCreditorAgent().getPostalAddress(),
					OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR);

		// 4 validate Creditor Agent block
		if (null != paymentSubmissionResponse.getData().getInitiation().getCreditorAgent()) {
			commonPaymentValidations.validateCreditorAgent(
					paymentSubmissionResponse.getData().getInitiation().getCreditorAgent(),
					OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR);
		}

		// 5 validate debtorAccount SchemeName Identification
		if (paymentSubmissionResponse.getData().getInitiation().getDebtorAccount() != null) {
			commonPaymentValidations.validateSchemeNameWithIdentification(
					paymentSubmissionResponse.getData().getInitiation().getDebtorAccount().getSchemeName(),
					paymentSubmissionResponse.getData().getInitiation().getDebtorAccount().getIdentification(),
					OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR);
		}

		// 6 validate creditorAccount SchemeName Identification
		if (paymentSubmissionResponse.getData().getInitiation().getCreditorAccount() != null) {
			commonPaymentValidations.validateSchemeNameWithIdentification(
					paymentSubmissionResponse.getData().getInitiation().getCreditorAccount().getSchemeName(),
					paymentSubmissionResponse.getData().getInitiation().getCreditorAccount().getIdentification(),
					OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR);
		}

		// 7 Validate currency combinations - removed 
		// 1 validate currency in charges block
		if (paymentSubmissionResponse.getData().getCharges() != null
				&& !paymentSubmissionResponse.getData().getCharges().isEmpty()) {
			for (OBCharge1 charges : paymentSubmissionResponse.getData().getCharges()) {
				commonPaymentValidations.isValidCurrency(charges.getAmount().getCurrency(),
						OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR);
			}
		}

		// 2 validate instructed currency
		commonPaymentValidations.isValidCurrency(
				paymentSubmissionResponse.getData().getInitiation().getInstructedAmount().getCurrency(),
				OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR);

		// 3 validate unit currency in exchange rate block
		if (paymentSubmissionResponse.getData().getExchangeRateInformation() != null)
			commonPaymentValidations.isValidCurrency(
					paymentSubmissionResponse.getData().getExchangeRateInformation().getUnitCurrency(),
					OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR);

		// 4 validate unit currency initiation exchange rate block
		if (paymentSubmissionResponse.getData().getInitiation().getExchangeRateInformation() != null)
			commonPaymentValidations.isValidCurrency(
					paymentSubmissionResponse.getData().getInitiation().getExchangeRateInformation().getUnitCurrency(),
					OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR);

		// 5 validate currency of transfer
		commonPaymentValidations.isValidCurrency(
				paymentSubmissionResponse.getData().getInitiation().getCurrencyOfTransfer(),
				OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR);

		// 9 Validate Date Fields in Response
		// 1. Status Update Date Time
		if (null != paymentSubmissionResponse.getData().getStatusUpdateDateTime()) {
			commonPaymentValidations.validateAndParseDateTimeFormatForResponse(
					paymentSubmissionResponse.getData().getStatusUpdateDateTime());
		}

		// 2.Expected Execution Date Time
		if (null != paymentSubmissionResponse.getData().getExpectedExecutionDateTime()) {
			commonPaymentValidations.validateAndParseDateTimeFormatForResponse(
					paymentSubmissionResponse.getData().getExpectedExecutionDateTime());
		}

		// 3. Expected Settlement Date Time
		if (null != paymentSubmissionResponse.getData().getExpectedSettlementDateTime()) {
			commonPaymentValidations.validateAndParseDateTimeFormatForResponse(
					paymentSubmissionResponse.getData().getExpectedSettlementDateTime());
		}

		// 4. Requested Execution Date Time
		if (null != paymentSubmissionResponse.getData().getInitiation().getRequestedExecutionDateTime()) {
			commonPaymentValidations.validateAndParseDateTimeFormatForResponse(
					paymentSubmissionResponse.getData().getInitiation().getRequestedExecutionDateTime());
		}

		// 5. Exchange Rate Expiration Date Time
		if (null != paymentSubmissionResponse.getData().getExchangeRateInformation()
				&& null != paymentSubmissionResponse.getData().getExchangeRateInformation().getExpirationDateTime()) {
			commonPaymentValidations.validateAndParseDateTimeFormatForResponse(
					paymentSubmissionResponse.getData().getExchangeRateInformation().getExpirationDateTime());
		}

		// 10 validate MultiAuth
		commonPaymentValidations.validateMultiAuth(paymentSubmissionResponse.getData().getMultiAuthorisation());
	}

	private void executeInternationalSubmissionResponseSwaggerValidations(
			CustomISPaymentsPOSTResponse paymentSubmissionResponse) {
		psd2Validator.validate(paymentSubmissionResponse);

	}
}
