package com.capgemini.psd2.pisp.validation.adapter.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.exceptions.ErrorMapKeys;
import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.pisp.domain.CustomIPaymentConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomIPaymentConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.CustomIPaymentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.OBCharge1;
import com.capgemini.psd2.pisp.domain.OBWriteInternationalConsent1;
import com.capgemini.psd2.pisp.domain.PaymentInternationalSubmitPOST201Response;
import com.capgemini.psd2.pisp.domain.PaymentRetrieveGetRequest;
import com.capgemini.psd2.pisp.processing.adapter.PaymentConsentsCustomValidator;
import com.capgemini.psd2.pisp.processing.adapter.PaymentSubmissionCustomValidator;
import com.capgemini.psd2.pisp.validation.adapter.constants.CommonPaymentValidations;
import com.capgemini.psd2.utilities.NullCheckUtils;
import com.capgemini.psd2.validator.PSD2Validator;


@Component("internationalPaymentValidator")
@ConfigurationProperties("app")
public class InternationalPaymentValidatorImpl implements
		PaymentConsentsCustomValidator<CustomIPaymentConsentsPOSTRequest, CustomIPaymentConsentsPOSTResponse>,
		PaymentSubmissionCustomValidator<CustomIPaymentsPOSTRequest, PaymentInternationalSubmitPOST201Response> {

	@Autowired
	private PSD2Validator psd2Validator; //NOSONAR

	@Value("${app.regex.paymentId:#{null}}")
	private String paymentIdRegexValidator; //NOSONAR

	@Value("${app.swaggerValidations.request:#{true}}")
	private Boolean reqValidationEnabled; //NOSONAR

	@Value("${app.swaggerValidations.response:#{true}}")
	private Boolean resValidationEnabled; //NOSONAR

	@Value("${app.pattern.merchantCategoryCode:\\d+}")
	private String merchantCategoryCodeRegexValidator; //NOSONAR

	@Autowired
	private CommonPaymentValidations commonPaymentValidations; //NOSONAR

	// Validation for International Payment Consents v3.0 API
	@Override
	public boolean validateConsentRequest(CustomIPaymentConsentsPOSTRequest paymentConsentsRequest) {
		commonPaymentValidations.validateHeaders();
		if (reqValidationEnabled)
			executeInternationalConsentsRequestSwaggerValidations(paymentConsentsRequest);
		executeInternationalConsentsRequestCustomValidations(paymentConsentsRequest);
		return Boolean.TRUE;
	}

	@Override
	public void validatePaymentsPOSTRequest(CustomIPaymentsPOSTRequest paymentSubmissionRequest) {
		commonPaymentValidations.validateHeaders();
		executeInternationalSubmissionRequestCustomValidations(paymentSubmissionRequest);
	}

	@Override
	public boolean validateConsentsGETRequest(PaymentRetrieveGetRequest paymentRetrieveRequest) {
		psd2Validator.validateRequestWithSwagger(paymentRetrieveRequest,
				OBErrorCodeEnum.UK_OBIE_FIELD_INVALID.toString());
		return Boolean.TRUE;
	}

	@Override
	public void validatePaymentsResponse(PaymentInternationalSubmitPOST201Response paymentSubmissionResponse) {
		if (resValidationEnabled)
			executeInternationalSubmissionResponseSwaggerValidations(paymentSubmissionResponse);
		executeInternationalSubmissionResponseCustomValidations(paymentSubmissionResponse);
	}

	@Override
	public boolean validatePaymentConsentResponse(CustomIPaymentConsentsPOSTResponse paymentConsentsResponse) {
		if (resValidationEnabled)
			executeInternationalConsentsResponseSwaggerValidations(paymentConsentsResponse);
		executeInternationalConsentsResponseCustomValidations(paymentConsentsResponse);
		return true;
	}

	private void executeInternationalConsentsRequestSwaggerValidations(
			OBWriteInternationalConsent1 paymentConsentsRequest) {
		psd2Validator.validateRequestWithSwagger(paymentConsentsRequest,
				OBErrorCodeEnum.UK_OBIE_FIELD_INVALID.toString());
	}

	/*  Consent Request Validations:
	1 validate local instrument
	2 validate payment context
	3 validate Risk delivery address fields
	4 validate Creditor Postal Address fields
	5 validate CreditorAgent Postal Address fields
	6 validate Creditor Agent block
	7 validate debtorAccount SchemeName Identification
	8 validate creditorAccount SchemeName Identification
	9 validate ExchangeRateType combination fields
	10 Validate currency combinations
	11 Validate currencies (3) instructed, exchangeRate-UC, COT
	12 Validate Date Fields in Request (1) i.e. completion
	13 Validate Merchant Category Code
	 */
	private void executeInternationalConsentsRequestCustomValidations(
			OBWriteInternationalConsent1 paymentConsentsRequest) {

		// 1 validate LocalInstrument
				commonPaymentValidations
						.validateLocalInstrument(paymentConsentsRequest.getData().getInitiation().getLocalInstrument(), null);
		// 2 validate Payment Context
		commonPaymentValidations.validateInternationalPaymentContext(paymentConsentsRequest.getRisk());

		// 3 validate Risk Delivery Address fields
		commonPaymentValidations
				.validateInternationalRiskDeliveryAddress(paymentConsentsRequest.getRisk().getDeliveryAddress(), null);

		// 4 validate Creditor Postal Address fields
		if (paymentConsentsRequest.getData().getInitiation().getCreditor() != null)
			commonPaymentValidations.validateDomesticCreditorPostalAddress(
					paymentConsentsRequest.getData().getInitiation().getCreditor().getPostalAddress(), null);

		// 5 validate CreditorAgent Postal Address fields
		if (paymentConsentsRequest.getData().getInitiation().getCreditorAgent() != null)
			commonPaymentValidations.validateDomesticCreditorPostalAddress(
					paymentConsentsRequest.getData().getInitiation().getCreditorAgent().getPostalAddress(), null);

		// 6 validate Creditor Agent
		if (null != paymentConsentsRequest.getData().getInitiation().getCreditorAgent()) {
			commonPaymentValidations
					.validateCreditorAgent(paymentConsentsRequest.getData().getInitiation().getCreditorAgent(), null);
		}
		
		// 7 validate debtorAccount SchemeName Identification
		if (paymentConsentsRequest.getData().getInitiation().getDebtorAccount() != null) {
			commonPaymentValidations.validateSchemeNameWithIdentification(
					paymentConsentsRequest.getData().getInitiation().getDebtorAccount().getSchemeName(),
					paymentConsentsRequest.getData().getInitiation().getDebtorAccount().getIdentification(), null);

			if (!NullCheckUtils.isNullOrEmpty(
					paymentConsentsRequest.getData().getInitiation().getDebtorAccount().getSecondaryIdentification())) {
				commonPaymentValidations.validateSchemeNameWithSecondaryIdentification(
						paymentConsentsRequest.getData().getInitiation().getDebtorAccount().getSchemeName(),
						paymentConsentsRequest.getData().getInitiation().getDebtorAccount()
								.getSecondaryIdentification());
			}
		}

		// 8 validate creditorAccount SchemeName Identification and country code
		if (paymentConsentsRequest.getData().getInitiation().getCreditorAccount() != null) {
			commonPaymentValidations.validateSchemeNameWithIdentification(
					paymentConsentsRequest.getData().getInitiation().getCreditorAccount().getSchemeName(),
					paymentConsentsRequest.getData().getInitiation().getCreditorAccount().getIdentification(), null);

			if (!NullCheckUtils.isNullOrEmpty(paymentConsentsRequest.getData().getInitiation().getCreditorAccount()
					.getSecondaryIdentification())) {
				commonPaymentValidations.validateSchemeNameWithSecondaryIdentification(
						paymentConsentsRequest.getData().getInitiation().getCreditorAccount().getSchemeName(),
						paymentConsentsRequest.getData().getInitiation().getCreditorAccount()
								.getSecondaryIdentification());
			}

			if (paymentConsentsRequest.getData().getInitiation().getCreditor() != null) {
				if (paymentConsentsRequest.getData().getInitiation().getCreditor().getPostalAddress() != null) {
					commonPaymentValidations.validateISOCountry(paymentConsentsRequest.getData().getInitiation()
							.getCreditor().getPostalAddress().getCountry(), null);
				}
			}

			if (paymentConsentsRequest.getData().getInitiation().getCreditorAgent() != null) {
				if (paymentConsentsRequest.getData().getInitiation().getCreditorAgent().getPostalAddress() != null) {
					commonPaymentValidations.validateISOCountry(paymentConsentsRequest.getData().getInitiation()
							.getCreditorAgent().getPostalAddress().getCountry(), null);
				}
			}
		}

		// 9 validate ExchangeRateType combination fields
		if (null != paymentConsentsRequest.getData().getInitiation().getExchangeRateInformation()) {
			commonPaymentValidations.validateExchangeRate(
					paymentConsentsRequest.getData().getInitiation().getExchangeRateInformation());
		}
		
		// 10 Validate currency combinations- removed

		// 1 validate instructed currency
		commonPaymentValidations.isValidCurrency(
				paymentConsentsRequest.getData().getInitiation().getInstructedAmount().getCurrency(), null);

		// 2 validate unit currency initiation exchange rate block
		if (paymentConsentsRequest.getData().getInitiation().getExchangeRateInformation() != null)
			commonPaymentValidations.isValidCurrency(paymentConsentsRequest.getData().getInitiation()
					.getExchangeRateInformation().getUnitCurrency(), null);

		// 3 validate currency of transfer
		commonPaymentValidations.isValidCurrency(
				paymentConsentsRequest.getData().getInitiation().getCurrencyOfTransfer(), null);

		// 12 Validate Date Fields in Request
		// 1 validate authorisation completion date time
		if (null != paymentConsentsRequest.getData().getAuthorisation()
				&& null != paymentConsentsRequest.getData().getAuthorisation().getCompletionDateTime()) {
			commonPaymentValidations
					.validateDateTime(paymentConsentsRequest.getData().getAuthorisation().getCompletionDateTime());
		}

		// 13 Validate Merchant Category Code
		if (null != paymentConsentsRequest.getRisk()
				&& null != paymentConsentsRequest.getRisk().getMerchantCategoryCode()
				&& null != merchantCategoryCodeRegexValidator) {
			commonPaymentValidations.validateMerchantCategoryCode(
					paymentConsentsRequest.getRisk().getMerchantCategoryCode(), merchantCategoryCodeRegexValidator);
		}


	}

	/*  Submission Request Validations:
	1 validate consent id
	 */
	private void executeInternationalSubmissionRequestCustomValidations(
			CustomIPaymentsPOSTRequest paymentSubmissionRequest) {
		// 1 validate consent id
		validateInternationalPaymentId(paymentSubmissionRequest.getData().getConsentId());

		
	}

	private void validateInternationalPaymentId(String paymentId) {
		if (!paymentId.matches(paymentIdRegexValidator)) {
			throw PSD2Exception.populatePSD2Exception(
					new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_RESOURCE_NOTFOUND, ErrorMapKeys.INVALID_PAYMENT_ID));
		}
	}

	/*  Consent Response Validations:
	1 validate payment context
	3 validate Risk delivery address fields
	4 validate Creditor Postal Address fields
	5 validate CreditorAgent Postal Address fields
	6 validate Creditor Agent block
	7 validate debtorAccount SchemeName Identification
	8 validate creditorAccount SchemeName Identification
	9 Validate currency combinations
	10 Validate currencies (5) i.e instructed, charge, exchangeRate-UC, data.exchangeRate-UC, COT
	11 Validate Date Fields in Response (6) i.e. statusUpdate, expectedExecution, settlement, cutOff, completion, expiration
    */
	private void executeInternationalConsentsResponseCustomValidations(
			CustomIPaymentConsentsPOSTResponse paymentConsentsResponse) {

		// 1 validate payment context
		commonPaymentValidations.validatePaymentContext(paymentConsentsResponse.getRisk());

		// 2 validate risk delivery address
		commonPaymentValidations.validateInternationalRiskDeliveryAddress(
				paymentConsentsResponse.getRisk().getDeliveryAddress(), OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR);

		// 3 validate Creditor Postal Country Code
		if (paymentConsentsResponse.getData().getInitiation().getCreditor() != null) {
			if (paymentConsentsResponse.getData().getInitiation().getCreditor().getPostalAddress() != null) {
				commonPaymentValidations.validateISOCountry(
						paymentConsentsResponse.getData().getInitiation().getCreditor().getPostalAddress().getCountry(),
						OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR);
			}
		}
		// 4 validate Creditor Agent Postal Country Code
		if (paymentConsentsResponse.getData().getInitiation().getCreditorAgent() != null) {
			if (paymentConsentsResponse.getData().getInitiation().getCreditorAgent().getPostalAddress() != null) {
				commonPaymentValidations.validateISOCountry(paymentConsentsResponse.getData().getInitiation()
						.getCreditorAgent().getPostalAddress().getCountry(), OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR);
			}
		}

		// 6 validate Creditor Agent
		if (null != paymentConsentsResponse.getData().getInitiation().getCreditorAgent()) {
			commonPaymentValidations.validateCreditorAgent(
					paymentConsentsResponse.getData().getInitiation().getCreditorAgent(),
					OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR);
		}

		// 7 validate debtorAccount SchemeName Identification
		if (paymentConsentsResponse.getData().getInitiation().getDebtorAccount() != null) {
			commonPaymentValidations.validateSchemeNameWithIdentification(
					paymentConsentsResponse.getData().getInitiation().getDebtorAccount().getSchemeName(),
					paymentConsentsResponse.getData().getInitiation().getDebtorAccount().getIdentification(),
					OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR);
		}

		// 8 validate creditor SchemeName Identification
		if (paymentConsentsResponse.getData().getInitiation().getCreditorAccount() != null) {
			commonPaymentValidations.validateSchemeNameWithIdentification(
					paymentConsentsResponse.getData().getInitiation().getCreditorAccount().getSchemeName(),
					paymentConsentsResponse.getData().getInitiation().getCreditorAccount().getIdentification(),
					OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR);
		}
		
		// 9 Validate currency combinations-removed

		// 1 validate currency in charges block
		if (paymentConsentsResponse.getData().getCharges() != null
				&& !paymentConsentsResponse.getData().getCharges().isEmpty()) {
			for (OBCharge1 charges : paymentConsentsResponse.getData().getCharges()) {
				commonPaymentValidations.isValidCurrency(charges.getAmount().getCurrency(),
						OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR);
			}
		}

		// 2 validate instructed currency
		commonPaymentValidations.isValidCurrency(
				paymentConsentsResponse.getData().getInitiation().getInstructedAmount().getCurrency(),
				OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR);

		// 3 validate unit currency in exchange rate block
		if (paymentConsentsResponse.getData().getExchangeRateInformation() != null)
			commonPaymentValidations.isValidCurrency(
					paymentConsentsResponse.getData().getExchangeRateInformation().getUnitCurrency(),
					OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR);

		// 4 validate unit currency initiation exchange rate block
		if (paymentConsentsResponse.getData().getInitiation().getExchangeRateInformation() != null)
			commonPaymentValidations.isValidCurrency(
					paymentConsentsResponse.getData().getInitiation().getExchangeRateInformation().getUnitCurrency(),
					OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR);

		// 5 validate currency of transfer
		commonPaymentValidations.isValidCurrency(
				paymentConsentsResponse.getData().getInitiation().getCurrencyOfTransfer(),
				OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR);

		// 11 Validate Date Fields in Response
		// 1. Status Update Date Time
		if (null != paymentConsentsResponse.getData().getStatusUpdateDateTime()) {
			commonPaymentValidations.validateAndParseDateTimeFormatForResponse(
					paymentConsentsResponse.getData().getStatusUpdateDateTime());
		}

		// 2.Expected Execution Date Time
		if (null != paymentConsentsResponse.getData().getExpectedExecutionDateTime()) {
			commonPaymentValidations.validateAndParseDateTimeFormatForResponse(
					paymentConsentsResponse.getData().getExpectedExecutionDateTime());
		}

		// 3. Expected Settlement Date Time
		if (null != paymentConsentsResponse.getData().getExpectedSettlementDateTime()) {
			commonPaymentValidations.validateAndParseDateTimeFormatForResponse(
					paymentConsentsResponse.getData().getExpectedSettlementDateTime());
		}

		// 4. Cut Off Date Time
		if (null != paymentConsentsResponse.getData().getCutOffDateTime()) {
			commonPaymentValidations
					.validateAndParseDateTimeFormatForResponse(paymentConsentsResponse.getData().getCutOffDateTime());
		}

		// 5 Authorisation completion date time
		if (null != paymentConsentsResponse.getData().getAuthorisation()
				&& null != paymentConsentsResponse.getData().getAuthorisation().getCompletionDateTime()) {
			commonPaymentValidations.validateAndParseDateTimeFormatForResponse(
					paymentConsentsResponse.getData().getAuthorisation().getCompletionDateTime());
		}

		// 6. Exchange Rate Expiration Date Time
		if (null != paymentConsentsResponse.getData().getExchangeRateInformation()
				&& null != paymentConsentsResponse.getData().getExchangeRateInformation().getExpirationDateTime()) {
			commonPaymentValidations.validateAndParseDateTimeFormatForResponse(
					paymentConsentsResponse.getData().getExchangeRateInformation().getExpirationDateTime());
		}
	}

	private void executeInternationalConsentsResponseSwaggerValidations(
			CustomIPaymentConsentsPOSTResponse paymentConsentsResponse) {
		psd2Validator.validate(paymentConsentsResponse);
	}

	private void executeInternationalSubmissionResponseSwaggerValidations(
			PaymentInternationalSubmitPOST201Response paymentSubmissionResponse) {
		psd2Validator.validate(paymentSubmissionResponse);

	}

	/*  Submission Response Validations:
	1 validate payment context
	2 validate Creditor Postal Address fields
	3 validate CreditorAgent Postal Address fields
	4 validate Creditor Agent block
	5 validate debtorAccount SchemeName Identification
	6 validate creditorAccount SchemeName Identification
	7 Validate currency combinations
	8 Validate currencies (5) i.e instructed, charge, exchangeRate-UC, data.exchangeRate-UC, COT
	9 Validate Date Fields in Response (4) i.e. statusUpdate, expectedExecution, settlement, expiration
	10 validate MultiAuth
    */
	private void executeInternationalSubmissionResponseCustomValidations(
			PaymentInternationalSubmitPOST201Response paymentSubmissionResponse) {

		// 1 validate local instrument
		commonPaymentValidations.validateLocalInstrument(
				paymentSubmissionResponse.getData().getInitiation().getLocalInstrument(),
				OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR);

		// 2 validate creditor address code
		if (paymentSubmissionResponse.getData().getInitiation().getCreditor() != null) {
			if (paymentSubmissionResponse.getData().getInitiation().getCreditor().getPostalAddress() != null) {
				commonPaymentValidations.validateISOCountry(paymentSubmissionResponse.getData().getInitiation()
						.getCreditor().getPostalAddress().getCountry(), OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR);
			}
		}

		// 3 validate Creditor Agent Country Code
		if (paymentSubmissionResponse.getData().getInitiation().getCreditorAgent() != null) {
			if (paymentSubmissionResponse.getData().getInitiation().getCreditorAgent().getPostalAddress() != null) {
				commonPaymentValidations.validateISOCountry(paymentSubmissionResponse.getData().getInitiation()
						.getCreditorAgent().getPostalAddress().getCountry(), OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR);
			}
		}

		// 4 validate Creditor Agent
		if (null != paymentSubmissionResponse.getData().getInitiation().getCreditorAgent()) {
			commonPaymentValidations.validateCreditorAgent(
					paymentSubmissionResponse.getData().getInitiation().getCreditorAgent(),
					OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR);
		}
		
		// 5 validate debtorAccount SchemeName Identification
		if (paymentSubmissionResponse.getData().getInitiation().getDebtorAccount() != null) {
			commonPaymentValidations.validateSchemeNameWithIdentification(
					paymentSubmissionResponse.getData().getInitiation().getDebtorAccount().getSchemeName(),
					paymentSubmissionResponse.getData().getInitiation().getDebtorAccount().getIdentification(),
					OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR);
		}

		// 6 validate creditor SchemeName Identification
		if (paymentSubmissionResponse.getData().getInitiation().getCreditorAccount() != null) {
			commonPaymentValidations.validateSchemeNameWithIdentification(
					paymentSubmissionResponse.getData().getInitiation().getCreditorAccount().getSchemeName(),
					paymentSubmissionResponse.getData().getInitiation().getCreditorAccount().getIdentification(),
					OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR);
		}

		// 7 Validate currency combinations- removed
		
		// 1 validate currency in charges block
		if (paymentSubmissionResponse.getData().getCharges() != null
				&& !paymentSubmissionResponse.getData().getCharges().isEmpty()) {
			for (OBCharge1 charges : paymentSubmissionResponse.getData().getCharges()) {
				commonPaymentValidations.isValidCurrency(charges.getAmount().getCurrency(),
						OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR);
			}
		}

		// 2 validate instructed currency
		commonPaymentValidations.isValidCurrency(
				paymentSubmissionResponse.getData().getInitiation().getInstructedAmount().getCurrency(),
				OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR);

		// 3 validate unit currency in exchange rate block
		if (paymentSubmissionResponse.getData().getExchangeRateInformation() != null)
			commonPaymentValidations.isValidCurrency(
					paymentSubmissionResponse.getData().getExchangeRateInformation().getUnitCurrency(),
					OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR);

		// 4 validate unit currency initiation exchange rate block
		if (paymentSubmissionResponse.getData().getInitiation().getExchangeRateInformation() != null)
			commonPaymentValidations.isValidCurrency(
					paymentSubmissionResponse.getData().getInitiation().getExchangeRateInformation().getUnitCurrency(),
					OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR);

		// 5 validate currency of transfer
		commonPaymentValidations.isValidCurrency(
				paymentSubmissionResponse.getData().getInitiation().getCurrencyOfTransfer(),
				OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR);

		// 11 Validate Date Fields in Response
		// 1. Status Update Date Time
		if (null != paymentSubmissionResponse.getData().getStatusUpdateDateTime()) {
			commonPaymentValidations.validateAndParseDateTimeFormatForResponse(
					paymentSubmissionResponse.getData().getStatusUpdateDateTime());
		}

		// 2.Expected Execution Date Time
		if (null != paymentSubmissionResponse.getData().getExpectedExecutionDateTime()) {
			commonPaymentValidations.validateAndParseDateTimeFormatForResponse(
					paymentSubmissionResponse.getData().getExpectedExecutionDateTime());
		}

		// 3. Expected Settlement Date Time
		if (null != paymentSubmissionResponse.getData().getExpectedSettlementDateTime()) {
			commonPaymentValidations.validateAndParseDateTimeFormatForResponse(
					paymentSubmissionResponse.getData().getExpectedSettlementDateTime());
		}

		// 4. Exchange Rate Expiration Date Time
		if (null != paymentSubmissionResponse.getData().getExchangeRateInformation()
				&& null != paymentSubmissionResponse.getData().getExchangeRateInformation().getExpirationDateTime()) {
			commonPaymentValidations.validateAndParseDateTimeFormatForResponse(
					paymentSubmissionResponse.getData().getExchangeRateInformation().getExpirationDateTime());
		}

		// 10 validate MultiAuth
		commonPaymentValidations.validateMultiAuth(paymentSubmissionResponse.getData().getMultiAuthorisation());
	}

}
