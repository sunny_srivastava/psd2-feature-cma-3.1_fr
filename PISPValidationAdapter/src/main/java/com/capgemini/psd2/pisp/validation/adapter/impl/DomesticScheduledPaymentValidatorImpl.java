package com.capgemini.psd2.pisp.validation.adapter.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.exceptions.ErrorMapKeys;
import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.pisp.domain.CustomDSPConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomDSPConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.CustomDSPaymentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomDSPaymentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.OBCharge1;
import com.capgemini.psd2.pisp.domain.OBPostalAddress6;
import com.capgemini.psd2.pisp.domain.OBWriteDomesticScheduledConsent1;
import com.capgemini.psd2.pisp.domain.PaymentRetrieveGetRequest;
import com.capgemini.psd2.pisp.processing.adapter.PaymentConsentsCustomValidator;
import com.capgemini.psd2.pisp.processing.adapter.PaymentSubmissionCustomValidator;
import com.capgemini.psd2.pisp.validation.adapter.constants.CommonPaymentValidations;
import com.capgemini.psd2.utilities.NullCheckUtils;
import com.capgemini.psd2.validator.PSD2Validator;

@Component("domesticSchedulePaymentValidator")
@ConfigurationProperties("app")
public class DomesticScheduledPaymentValidatorImpl
		implements PaymentConsentsCustomValidator<CustomDSPConsentsPOSTRequest, CustomDSPConsentsPOSTResponse>,
		PaymentSubmissionCustomValidator<CustomDSPaymentsPOSTRequest, CustomDSPaymentsPOSTResponse> {

	
	@Autowired
	private PSD2Validator psd2Validator; //NOSONAR

	@Value("${app.regex.paymentId:#{null}}")
	private String paymentIdRegexValidator; //NOSONAR

	@Value("${app.swaggerValidations.request:#{true}}")
	private Boolean reqValidationEnabled; //NOSONAR

	@Value("${app.swaggerValidations.response:#{true}}")
	private Boolean resValidationEnabled; //NOSONAR

	@Value("${app.pattern.merchantCategoryCode:\\d+}")
	private String merchantCategoryCodeRegexValidator; //NOSONAR

	@Autowired
	private CommonPaymentValidations commonPaymentValidations; //NOSONAR

	@Override
	public boolean validateConsentRequest(CustomDSPConsentsPOSTRequest scheduledPaymentConsentsRequest) {
		commonPaymentValidations.validateHeaders();
		if (reqValidationEnabled)
			executeDomesticConsentsRequestSwaggerValidations(scheduledPaymentConsentsRequest);
		executeDomesticConsentsRequestCustomValidations(scheduledPaymentConsentsRequest);
		return Boolean.TRUE;
	}

	@Override
	public boolean validatePaymentConsentResponse(CustomDSPConsentsPOSTResponse scheduledPaymentConsentsResponse) {
		if (resValidationEnabled)
			executeDomesticConsentsResponseSwaggerValidations(scheduledPaymentConsentsResponse);
		executeDomesticConsentsResponseCustomValidations(scheduledPaymentConsentsResponse);
		return true;
	}

	@Override
	public boolean validateConsentsGETRequest(PaymentRetrieveGetRequest paymentRetrieveRequest) {
		psd2Validator.validateRequestWithSwagger(paymentRetrieveRequest,
				OBErrorCodeEnum.UK_OBIE_FIELD_INVALID.toString());
		return Boolean.TRUE;
	}

	private void executeDomesticConsentsRequestSwaggerValidations(
			OBWriteDomesticScheduledConsent1 scheduledPaymentConsentsRequest) {
		psd2Validator.validateRequestWithSwagger(scheduledPaymentConsentsRequest,
				OBErrorCodeEnum.UK_OBIE_FIELD_INVALID.toString());
	}

	private void executeDomesticConsentsRequestCustomValidations(
			OBWriteDomesticScheduledConsent1 scheduledPaymentConsentsRequest) {
		// validate currency
		commonPaymentValidations.isValidCurrency(
				scheduledPaymentConsentsRequest.getData().getInitiation().getInstructedAmount().getCurrency(), null);

		// validate creditorAccount SchemeName Identification
		if (scheduledPaymentConsentsRequest.getData().getInitiation().getCreditorAccount() != null) {
			commonPaymentValidations.validateSchemeNameWithIdentification(
					scheduledPaymentConsentsRequest.getData().getInitiation().getCreditorAccount().getSchemeName(),
					scheduledPaymentConsentsRequest.getData().getInitiation().getCreditorAccount().getIdentification(),
					null);

			if (!NullCheckUtils.isNullOrEmpty(scheduledPaymentConsentsRequest.getData().getInitiation()
					.getCreditorAccount().getSecondaryIdentification())) {
				commonPaymentValidations.validateSchemeNameWithSecondaryIdentification(
						scheduledPaymentConsentsRequest.getData().getInitiation().getCreditorAccount().getSchemeName(),
						scheduledPaymentConsentsRequest.getData().getInitiation().getCreditorAccount()
								.getSecondaryIdentification());
			}
		}  

		// validate debtorAccount SchemeName Identification
		if (scheduledPaymentConsentsRequest.getData().getInitiation().getDebtorAccount() != null) {
			commonPaymentValidations.validateSchemeNameWithIdentification(
					scheduledPaymentConsentsRequest.getData().getInitiation().getDebtorAccount().getSchemeName(),
					scheduledPaymentConsentsRequest.getData().getInitiation().getDebtorAccount().getIdentification(),
					null);

			if (!NullCheckUtils.isNullOrEmpty(scheduledPaymentConsentsRequest.getData().getInitiation()
					.getDebtorAccount().getSecondaryIdentification())) {
				commonPaymentValidations.validateSchemeNameWithSecondaryIdentification(
						scheduledPaymentConsentsRequest.getData().getInitiation().getDebtorAccount().getSchemeName(),
						scheduledPaymentConsentsRequest.getData().getInitiation().getDebtorAccount()
								.getSecondaryIdentification());
			}
		}

		// validate Payment Context
		commonPaymentValidations.validatePaymentContext(scheduledPaymentConsentsRequest.getRisk());

		// validate Risk Delivery Address fields
		commonPaymentValidations.validateDomesticRiskDeliveryAddress(
				scheduledPaymentConsentsRequest.getRisk().getDeliveryAddress(), null);

		// validate Creditor Postal Address fields
		validateDomesticCreditorPostalAddress(
				scheduledPaymentConsentsRequest.getData().getInitiation().getCreditorPostalAddress(), null);

		// validate LocalInstrument
		commonPaymentValidations.validateLocalInstrument(
				scheduledPaymentConsentsRequest.getData().getInitiation().getLocalInstrument(), null);

		// validate authorisation completion date time
		if (null != scheduledPaymentConsentsRequest.getData().getAuthorisation()
				&& null != scheduledPaymentConsentsRequest.getData().getAuthorisation().getCompletionDateTime()) {
			commonPaymentValidations.validateDateTime(
					scheduledPaymentConsentsRequest.getData().getAuthorisation().getCompletionDateTime());
		}

		// Validate Merchant Category Code
		if (null != scheduledPaymentConsentsRequest.getRisk()
				&& null != scheduledPaymentConsentsRequest.getRisk().getMerchantCategoryCode()
				&& null != merchantCategoryCodeRegexValidator) {
			commonPaymentValidations.validateMerchantCategoryCode(
					scheduledPaymentConsentsRequest.getRisk().getMerchantCategoryCode(),
					merchantCategoryCodeRegexValidator);
		}

		commonPaymentValidations.validateDateTime(
				scheduledPaymentConsentsRequest.getData().getInitiation().getRequestedExecutionDateTime());
	}

	private void executeDomesticConsentsResponseSwaggerValidations(
			CustomDSPConsentsPOSTResponse scheduledPaymentConsentsResponse) {
		psd2Validator.validate(scheduledPaymentConsentsResponse);
	}

	private void executeDomesticConsentsResponseCustomValidations(
			CustomDSPConsentsPOSTResponse scheduledPaymentConsentsResponse) {
		commonPaymentValidations.validateDomesticRiskDeliveryAddress(
				scheduledPaymentConsentsResponse.getRisk().getDeliveryAddress(),
				OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR);
		// validate Creditor Postal Address fields
		validateDomesticCreditorPostalAddress(
				scheduledPaymentConsentsResponse.getData().getInitiation().getCreditorPostalAddress(),
				OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR);

		// validate debtorAccount SchemeName Identification
		if (scheduledPaymentConsentsResponse.getData().getInitiation().getDebtorAccount() != null) {
			commonPaymentValidations.validateSchemeNameWithIdentification(
					scheduledPaymentConsentsResponse.getData().getInitiation().getDebtorAccount().getSchemeName(),
					scheduledPaymentConsentsResponse.getData().getInitiation().getDebtorAccount().getIdentification(),
					OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR);
		}

		// validate creditor SchemeName Identification
		if (scheduledPaymentConsentsResponse.getData().getInitiation().getCreditorAccount() != null) {
			commonPaymentValidations.validateSchemeNameWithIdentification(
					scheduledPaymentConsentsResponse.getData().getInitiation().getCreditorAccount().getSchemeName(),
					scheduledPaymentConsentsResponse.getData().getInitiation().getCreditorAccount().getIdentification(),
					OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR);
		}

		if (scheduledPaymentConsentsResponse.getData().getCharges() != null
				&& !scheduledPaymentConsentsResponse.getData().getCharges().isEmpty()) {

			for (OBCharge1 charges : scheduledPaymentConsentsResponse.getData().getCharges()) {
				commonPaymentValidations.isValidCurrency(charges.getAmount().getCurrency(),
						OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR);
			}
		}
		commonPaymentValidations.isValidCurrency(
				scheduledPaymentConsentsResponse.getData().getInitiation().getInstructedAmount().getCurrency(),
				OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR);
		commonPaymentValidations.validateLocalInstrument(
				scheduledPaymentConsentsResponse.getData().getInitiation().getLocalInstrument(),
				OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR);

		// validate authorisation completion date time
		if (null != scheduledPaymentConsentsResponse.getData().getAuthorisation()
				&& null != scheduledPaymentConsentsResponse.getData().getAuthorisation().getCompletionDateTime()) {
			commonPaymentValidations.validateAndParseDateTimeFormatForResponse(
					scheduledPaymentConsentsResponse.getData().getAuthorisation().getCompletionDateTime());
		}

		// Validate Date Fields in Response

		// 1. Status Update Date Time
		if (null != scheduledPaymentConsentsResponse.getData().getStatusUpdateDateTime()) {
			commonPaymentValidations.validateAndParseDateTimeFormatForResponse(
					scheduledPaymentConsentsResponse.getData().getStatusUpdateDateTime());
		}

		// 2.Expected Execution Date Time
		if (null != scheduledPaymentConsentsResponse.getData().getExpectedExecutionDateTime()) {
			commonPaymentValidations.validateAndParseDateTimeFormatForResponse(
					scheduledPaymentConsentsResponse.getData().getExpectedExecutionDateTime());
		}

		// 3. Expected Settlement Date Time
		if (null != scheduledPaymentConsentsResponse.getData().getExpectedSettlementDateTime()) {
			commonPaymentValidations.validateAndParseDateTimeFormatForResponse(
					scheduledPaymentConsentsResponse.getData().getExpectedSettlementDateTime());
		}

		// 4. Cut Off Date Time
		if (null != scheduledPaymentConsentsResponse.getData().getCutOffDateTime()) {
			commonPaymentValidations.validateAndParseDateTimeFormatForResponse(
					scheduledPaymentConsentsResponse.getData().getCutOffDateTime());
		}

		// 4.Requested execution Date Time
		commonPaymentValidations.validateAndParseDateTimeFormatForResponse(
				scheduledPaymentConsentsResponse.getData().getInitiation().getRequestedExecutionDateTime());

		// Validate Merchant Category Code
		if (null != scheduledPaymentConsentsResponse.getRisk()
				&& null != scheduledPaymentConsentsResponse.getRisk().getMerchantCategoryCode()
				&& null != merchantCategoryCodeRegexValidator) {
			commonPaymentValidations.validateMerchantCategoryCode(
					scheduledPaymentConsentsResponse.getRisk().getMerchantCategoryCode(),
					merchantCategoryCodeRegexValidator);
		}

		// validate payment context
		commonPaymentValidations.validatePaymentContext(scheduledPaymentConsentsResponse.getRisk());

	}

	private void validateDomesticCreditorPostalAddress(OBPostalAddress6 creditorPostalAddress,
			OBErrorCodeEnum errorCodeEnum) {
		if (!NullCheckUtils.isNullOrEmpty(creditorPostalAddress)) {
			commonPaymentValidations.validateDomesticAddressLine(creditorPostalAddress.getAddressLine(), errorCodeEnum);
			commonPaymentValidations.validateDomesticCountrySubDivision(creditorPostalAddress.getCountrySubDivision());
			commonPaymentValidations.validateISOCountry(creditorPostalAddress.getCountry(), errorCodeEnum);
		}
	}

	@Override
	public void validatePaymentsPOSTRequest(CustomDSPaymentsPOSTRequest submissionRequestBody) {
		commonPaymentValidations.validateHeaders();
		executeDomesticSubmissionRequestCustomValidations(submissionRequestBody);
	}

	private void executeDomesticSubmissionRequestCustomValidations(
			CustomDSPaymentsPOSTRequest paymentSubmissionRequest) {

		// validate consent id
		validateDomesticPaymentId(paymentSubmissionRequest.getData().getConsentId());
	
	}

	private void validateDomesticPaymentId(String paymentId) {
		if (!paymentId.matches(paymentIdRegexValidator)) {
			throw PSD2Exception.populatePSD2Exception(
					new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_RESOURCE_NOTFOUND, ErrorMapKeys.INVALID_PAYMENT_ID));
		}
	}

	@Override
	public void validatePaymentsResponse(CustomDSPaymentsPOSTResponse paymentSubmissionResponse) {
		if (resValidationEnabled)
			executeDomesticSubmissionResponseSwaggerValidations(paymentSubmissionResponse);
		executeDomesticSubmissionResponseCustomValidations(paymentSubmissionResponse);
	}

	private void executeDomesticSubmissionResponseCustomValidations(
			CustomDSPaymentsPOSTResponse paymentSubmissionResponse) {
		// validate Creditor Postal Address fields
		commonPaymentValidations.validateDomesticCreditorPostalAddress(
				paymentSubmissionResponse.getData().getInitiation().getCreditorPostalAddress(),
				OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR);

		// validate debtorAccount SchemeName Identification
		if (paymentSubmissionResponse.getData().getInitiation().getDebtorAccount() != null) {
			commonPaymentValidations.validateSchemeNameWithIdentification(
					paymentSubmissionResponse.getData().getInitiation().getDebtorAccount().getSchemeName(),
					paymentSubmissionResponse.getData().getInitiation().getDebtorAccount().getIdentification(),
					OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR);
		}

		// validate creditor SchemeName Identification
		if (paymentSubmissionResponse.getData().getInitiation().getCreditorAccount() != null) {
			commonPaymentValidations.validateSchemeNameWithIdentification(
					paymentSubmissionResponse.getData().getInitiation().getCreditorAccount().getSchemeName(),
					paymentSubmissionResponse.getData().getInitiation().getCreditorAccount().getIdentification(),
					OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR);
		}

		if (paymentSubmissionResponse.getData().getCharges() != null
				&& !paymentSubmissionResponse.getData().getCharges().isEmpty()) {

			for (OBCharge1 charges : paymentSubmissionResponse.getData().getCharges()) {
				commonPaymentValidations.isValidCurrency(charges.getAmount().getCurrency(),
						OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR);
			}
		}
		commonPaymentValidations.isValidCurrency(
				paymentSubmissionResponse.getData().getInitiation().getInstructedAmount().getCurrency(),
				OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR);

		commonPaymentValidations.validateLocalInstrument(
				paymentSubmissionResponse.getData().getInitiation().getLocalInstrument(),
				OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR);

		// Validate Date Fields in Response

		// 1. Status Update Date Time
		if (null != paymentSubmissionResponse.getData().getStatusUpdateDateTime()) {
			commonPaymentValidations.validateAndParseDateTimeFormatForResponse(
					paymentSubmissionResponse.getData().getStatusUpdateDateTime());
		}

		// 2.Expected Execution Date Time
		if (null != paymentSubmissionResponse.getData().getExpectedExecutionDateTime()) {
			commonPaymentValidations.validateAndParseDateTimeFormatForResponse(
					paymentSubmissionResponse.getData().getExpectedExecutionDateTime());
		}

		// 3. Expected Settlement Date Time
		if (null != paymentSubmissionResponse.getData().getExpectedSettlementDateTime()) {
			commonPaymentValidations.validateAndParseDateTimeFormatForResponse(
					paymentSubmissionResponse.getData().getExpectedSettlementDateTime());
		}

		// 4. requested execution date time
		commonPaymentValidations.validateAndParseDateTimeFormatForResponse(
				paymentSubmissionResponse.getData().getInitiation().getRequestedExecutionDateTime());

		// validate MultiAuth
		commonPaymentValidations.validateMultiAuth(paymentSubmissionResponse.getData().getMultiAuthorisation());
	}

	private void executeDomesticSubmissionResponseSwaggerValidations(
			CustomDSPaymentsPOSTResponse paymentSubmissionResponse) {
		psd2Validator.validate(paymentSubmissionResponse);

	}
}