package com.capgemini.psd2.pisp.validation.adapter.impl;

import java.time.OffsetDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.exceptions.ErrorMapKeys;
import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.InternalServerErrorMessage;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.pisp.domain.CustomDStandingOrderConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomDStandingOrderConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.CustomDStandingOrderPOSTRequest;
import com.capgemini.psd2.pisp.domain.DStandingOrderPOST201Response;
import com.capgemini.psd2.pisp.domain.OBCharge1;
import com.capgemini.psd2.pisp.domain.OBDomesticStandingOrder1FinalPaymentAmount;
import com.capgemini.psd2.pisp.domain.OBDomesticStandingOrder1RecurringPaymentAmount;
import com.capgemini.psd2.pisp.domain.OBExternalStatus1Code;
import com.capgemini.psd2.pisp.domain.OBExternalStatus2Code;
import com.capgemini.psd2.pisp.domain.OBMultiAuthorisation1;
import com.capgemini.psd2.pisp.domain.PaymentRetrieveGetRequest;
import com.capgemini.psd2.pisp.processing.adapter.PaymentConsentsCustomValidator;
import com.capgemini.psd2.pisp.processing.adapter.PaymentSubmissionCustomValidator;
import com.capgemini.psd2.pisp.validation.adapter.constants.CommonPaymentValidations;
import com.capgemini.psd2.utilities.NullCheckUtils;
import com.capgemini.psd2.validator.PSD2Validator;


@Lazy
@Component("domesticStandingOrderValidator")
@ConfigurationProperties("app")
public class DomesticStandingOrderValidatorImpl implements
		PaymentConsentsCustomValidator<CustomDStandingOrderConsentsPOSTRequest, CustomDStandingOrderConsentsPOSTResponse>,
		PaymentSubmissionCustomValidator<CustomDStandingOrderPOSTRequest, DStandingOrderPOST201Response> {

	@Autowired
	private PSD2Validator psd2Validator;

	@Value("${app.regex.paymentId:#{null}}")
	private String paymentIdRegexValidator;

	@Value("${app.swaggerValidations.request:#{true}}")
	private Boolean reqValidationEnabled;

	@Value("${app.swaggerValidations.response:#{true}}")
	private Boolean resValidationEnabled;

	@Value("${app.pattern.merchantCategoryCode:\\d+}")
	private String merchantCategoryCodeRegexValidator;

	@Autowired
	private CommonPaymentValidations commonPaymentValidations;

	@Override
	public boolean validateConsentRequest(
			CustomDStandingOrderConsentsPOSTRequest customDomesticStandingOrderPOSTRequest) {
		commonPaymentValidations.validateHeaders();
		if (reqValidationEnabled)
			executeDomesticConsentsRequestSwaggerValidations(customDomesticStandingOrderPOSTRequest);
		executeDomesticConsentsRequestCustomValidations(customDomesticStandingOrderPOSTRequest);
		return Boolean.TRUE;
	}

	@Override
	public boolean validatePaymentConsentResponse(
			CustomDStandingOrderConsentsPOSTResponse customDomesticStandingOrderPOSTResponse) {
		// validate request parameter domestic standing order id
		if (resValidationEnabled)
			executeDomesticConsentsResponseSwaggerValidations(customDomesticStandingOrderPOSTResponse);
		executeDomesticConsentsResponseCustomValidations(customDomesticStandingOrderPOSTResponse);
		return true;
	}

	@Override
	public void validatePaymentsPOSTRequest(CustomDStandingOrderPOSTRequest submissionRequest) {
		executeDomesticSubmissionRequestCustomValidation(submissionRequest);
	}

	@Override
	public void validatePaymentsResponse(DStandingOrderPOST201Response submissionResponse) {
		if (resValidationEnabled)
			executeDomesticSubmissionResponseSwaggerValidations(submissionResponse);
		executeDomesticSubmissionResponseCustomValidations(submissionResponse);
	}

	private void executeDomesticConsentsResponseSwaggerValidations(
			CustomDStandingOrderConsentsPOSTResponse customDomesticStandingOrderPOSTResponse) {
		psd2Validator.validate(customDomesticStandingOrderPOSTResponse);
	}

	private void executeDomesticConsentsRequestSwaggerValidations(
			CustomDStandingOrderConsentsPOSTRequest customDomesticStandingOrderPOSTRequest) {
		psd2Validator.validateRequestWithSwagger(customDomesticStandingOrderPOSTRequest,
				OBErrorCodeEnum.UK_OBIE_FIELD_INVALID.toString());
	}

	private void executeDomesticConsentsRequestCustomValidations(
			CustomDStandingOrderConsentsPOSTRequest customDomesticStandingOrderPOSTRequest) {

		// validate Currency
		// FirstPaymentAmountCurrencyValidation
		commonPaymentValidations.isValidCurrency(
				customDomesticStandingOrderPOSTRequest.getData().getInitiation().getFirstPaymentAmount().getCurrency(),
				null);

		// RecurringPaymentAmountCurrencyValidation
		OBDomesticStandingOrder1RecurringPaymentAmount recurringPaymentAmount = customDomesticStandingOrderPOSTRequest
				.getData().getInitiation().getRecurringPaymentAmount();
		if (!NullCheckUtils.isNullOrEmpty(recurringPaymentAmount)) {
			commonPaymentValidations.isValidCurrency(recurringPaymentAmount.getCurrency(), null);
		}

		// FinalPaymentAmountCurrencyValidation
		OBDomesticStandingOrder1FinalPaymentAmount finalPaymentAmount = customDomesticStandingOrderPOSTRequest.getData()
				.getInitiation().getFinalPaymentAmount();
		if (!NullCheckUtils.isNullOrEmpty(finalPaymentAmount)) {
			commonPaymentValidations.isValidCurrency(finalPaymentAmount.getCurrency(), null);
		}

		// validate first Payment date time
		String firstPaymentDateTime = customDomesticStandingOrderPOSTRequest.getData().getInitiation()
				.getFirstPaymentDateTime();
		if (!NullCheckUtils.isNullOrEmpty(firstPaymentDateTime))
			commonPaymentValidations.validateAndParseDateTimeFormat(firstPaymentDateTime);

		// validate final payment date time
		String finalPaymentDateTime = customDomesticStandingOrderPOSTRequest.getData().getInitiation()
				.getFinalPaymentDateTime();
		if (!NullCheckUtils.isNullOrEmpty(finalPaymentDateTime))
			commonPaymentValidations.validateAndParseDateTimeFormat(finalPaymentDateTime);

		// validate recurring payment date time
		String recurringPaymentDateTime = customDomesticStandingOrderPOSTRequest.getData().getInitiation()
				.getRecurringPaymentDateTime();
		if (!NullCheckUtils.isNullOrEmpty(recurringPaymentDateTime))
			commonPaymentValidations.validateAndParseDateTimeFormat(recurringPaymentDateTime);

		if (!NullCheckUtils.isNullOrEmpty(customDomesticStandingOrderPOSTRequest.getData().getAuthorisation())
				&& !NullCheckUtils.isNullOrEmpty(
						customDomesticStandingOrderPOSTRequest.getData().getAuthorisation().getCompletionDateTime())) {
			commonPaymentValidations.validateAndParseDateTimeFormat(
					customDomesticStandingOrderPOSTRequest.getData().getAuthorisation().getCompletionDateTime());
		}

		checkIfLessthanRequiredDate(firstPaymentDateTime, OffsetDateTime.now().toString());

		if (!NullCheckUtils.isNullOrEmpty(finalPaymentDateTime)) {
			checkIfLessthanRequiredDate(finalPaymentDateTime, OffsetDateTime.now().toString());
			checkIfLessthanAndEqualRequiredDate(finalPaymentDateTime, firstPaymentDateTime);

			if (!NullCheckUtils.isNullOrEmpty(
					customDomesticStandingOrderPOSTRequest.getData().getInitiation().getNumberOfPayments())) {
				throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_FIELD_UNEXPECTED,
						ErrorMapKeys.INVALID_FIELDS_COMBINATION));
			}
		}

		if (!NullCheckUtils.isNullOrEmpty(recurringPaymentDateTime)) {
			checkIfLessthanRequiredDate(recurringPaymentDateTime, OffsetDateTime.now().toString());
			checkIfLessthanAndEqualRequiredDate(recurringPaymentDateTime, firstPaymentDateTime);

			if (recurringPaymentDateTime.equals(firstPaymentDateTime)) {
				throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_FIELD_UNEXPECTED,
						ErrorMapKeys.INVALID_DATE_FIELDS_PRESENT));
			}
		}

		if (!NullCheckUtils.isNullOrEmpty(recurringPaymentDateTime)
				&& !NullCheckUtils.isNullOrEmpty(finalPaymentDateTime)) {
			checkIfLessthanRequiredDate(finalPaymentDateTime, recurringPaymentDateTime);
		}

		// validate creditorAccount SchemeName Identification
		if (customDomesticStandingOrderPOSTRequest.getData().getInitiation().getCreditorAccount() != null) {
			commonPaymentValidations.validateSchemeNameWithIdentification(
					customDomesticStandingOrderPOSTRequest.getData().getInitiation().getCreditorAccount()
							.getSchemeName(),
					customDomesticStandingOrderPOSTRequest.getData().getInitiation().getCreditorAccount()
							.getIdentification(),
					null);

			if (!NullCheckUtils.isNullOrEmpty(customDomesticStandingOrderPOSTRequest.getData().getInitiation()
					.getCreditorAccount().getSecondaryIdentification())) {
				commonPaymentValidations.validateSchemeNameWithSecondaryIdentification(
						customDomesticStandingOrderPOSTRequest.getData().getInitiation().getCreditorAccount()
								.getSchemeName(),
						customDomesticStandingOrderPOSTRequest.getData().getInitiation().getCreditorAccount()
								.getSecondaryIdentification());
			}
		}

		// validate debtorAccount SchemeName Identification
		if (customDomesticStandingOrderPOSTRequest.getData().getInitiation().getDebtorAccount() != null) {
			commonPaymentValidations.validateSchemeNameWithIdentification(
					customDomesticStandingOrderPOSTRequest.getData().getInitiation().getDebtorAccount().getSchemeName(),
					customDomesticStandingOrderPOSTRequest.getData().getInitiation().getDebtorAccount()
							.getIdentification(),
					null);

			if (!NullCheckUtils.isNullOrEmpty(customDomesticStandingOrderPOSTRequest.getData().getInitiation()
					.getDebtorAccount().getSecondaryIdentification())) {
				commonPaymentValidations.validateSchemeNameWithSecondaryIdentification(
						customDomesticStandingOrderPOSTRequest.getData().getInitiation().getDebtorAccount()
								.getSchemeName(),
						customDomesticStandingOrderPOSTRequest.getData().getInitiation().getDebtorAccount()
								.getSecondaryIdentification());
			}
		}

		// validate Payment Context
		commonPaymentValidations.validatePaymentContext(customDomesticStandingOrderPOSTRequest.getRisk());

		// validate Risk Delivery Address fields
		commonPaymentValidations.validateDomesticRiskDeliveryAddress(
				customDomesticStandingOrderPOSTRequest.getRisk().getDeliveryAddress(), null);

		// validate authorisation completion date time
		if (null != customDomesticStandingOrderPOSTRequest.getData().getAuthorisation()
				&& null != customDomesticStandingOrderPOSTRequest.getData().getAuthorisation()
						.getCompletionDateTime()) {
			commonPaymentValidations.validateDateTime(
					customDomesticStandingOrderPOSTRequest.getData().getAuthorisation().getCompletionDateTime());
		}
		
		

		// Validate Merchant Category Code
		if (null != customDomesticStandingOrderPOSTRequest.getRisk()
				&& null != customDomesticStandingOrderPOSTRequest.getRisk().getMerchantCategoryCode()
				&& null != merchantCategoryCodeRegexValidator) {
			commonPaymentValidations.validateMerchantCategoryCode(
					customDomesticStandingOrderPOSTRequest.getRisk().getMerchantCategoryCode(),
					merchantCategoryCodeRegexValidator);
		}
	}

	private void executeDomesticConsentsResponseCustomValidations(
			CustomDStandingOrderConsentsPOSTResponse paymentConsentsResponse) {
			commonPaymentValidations.validateDomesticRiskDeliveryAddress(
					paymentConsentsResponse.getRisk().getDeliveryAddress(), OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR);
	
			// validate debtorAccount SchemeName Identification
			if (paymentConsentsResponse.getData().getInitiation().getDebtorAccount() != null) {
				commonPaymentValidations.validateSchemeNameWithIdentification(
						paymentConsentsResponse.getData().getInitiation().getDebtorAccount().getSchemeName(),
						paymentConsentsResponse.getData().getInitiation().getDebtorAccount().getIdentification(),
						OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR);
			}
	
			// validate creditor SchemeName Identification
			if (paymentConsentsResponse.getData().getInitiation().getCreditorAccount() != null) {
				commonPaymentValidations.validateSchemeNameWithIdentification(
						paymentConsentsResponse.getData().getInitiation().getCreditorAccount().getSchemeName(),
						paymentConsentsResponse.getData().getInitiation().getCreditorAccount().getIdentification(),
						OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR);
			}
	
			if (paymentConsentsResponse.getData().getCharges() != null
					&& !paymentConsentsResponse.getData().getCharges().isEmpty()) {
	
				for (OBCharge1 charges : paymentConsentsResponse.getData().getCharges()) {
					commonPaymentValidations.isValidCurrency(charges.getAmount().getCurrency(),
							OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR);
				}
			}
			// validate Currency
			// FirstPaymentAmountCurrencyValidation
			commonPaymentValidations.isValidCurrency(
					paymentConsentsResponse.getData().getInitiation().getFirstPaymentAmount().getCurrency(), OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR);
	
			// RecurringPaymentAmountCurrencyValidation
			OBDomesticStandingOrder1RecurringPaymentAmount recurringPaymentAmount = paymentConsentsResponse.getData()
					.getInitiation().getRecurringPaymentAmount();
			if (!NullCheckUtils.isNullOrEmpty(recurringPaymentAmount)) {
				commonPaymentValidations.isValidCurrency(recurringPaymentAmount.getCurrency(), OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR);
			}
	
			// FinalPaymentAmountCurrencyValidation
			OBDomesticStandingOrder1FinalPaymentAmount finalPaymentAmount = paymentConsentsResponse.getData()
					.getInitiation().getFinalPaymentAmount();
			if (!NullCheckUtils.isNullOrEmpty(finalPaymentAmount)) {
				commonPaymentValidations.isValidCurrency(finalPaymentAmount.getCurrency(), OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR);
			}
	
			// Validate Merchant Category Code
			if (null != paymentConsentsResponse.getRisk()
					&& null != paymentConsentsResponse.getRisk().getMerchantCategoryCode()
					&& null != merchantCategoryCodeRegexValidator) {
				commonPaymentValidations.validateMerchantCategoryCode(
						paymentConsentsResponse.getRisk().getMerchantCategoryCode(), merchantCategoryCodeRegexValidator);
			}
	
			// validate Payment Context
			commonPaymentValidations.validatePaymentContextforResponse(paymentConsentsResponse.getRisk());
	
			commonPaymentValidations.validateDomesticRiskDeliveryAddress(
					paymentConsentsResponse.getRisk().getDeliveryAddress(), OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR);
	
			// validate authorisation completion date time
			if (null != paymentConsentsResponse.getData().getAuthorisation()
					&& null != paymentConsentsResponse.getData().getAuthorisation().getCompletionDateTime()) {
				commonPaymentValidations.validateAndParseDateTimeFormatForResponse(
						paymentConsentsResponse.getData().getAuthorisation().getCompletionDateTime());
	
			}
	
			
				commonPaymentValidations.validateAndParseDateTimeFormatForResponse(
						paymentConsentsResponse.getData().getInitiation().getFirstPaymentDateTime());
	
			if (paymentConsentsResponse.getData().getInitiation().getFinalPaymentDateTime() != null) {
				commonPaymentValidations.validateAndParseDateTimeFormatForResponse(
						paymentConsentsResponse.getData().getInitiation().getFinalPaymentDateTime());
			}
	
			if (paymentConsentsResponse.getData().getInitiation().getRecurringPaymentDateTime() != null) {
				commonPaymentValidations.validateAndParseDateTimeFormatForResponse(
						paymentConsentsResponse.getData().getInitiation().getRecurringPaymentDateTime());
			}
	
			if (paymentConsentsResponse.getData().getCutOffDateTime() != null) {
				commonPaymentValidations
						.validateAndParseDateTimeFormatForResponse(paymentConsentsResponse.getData().getCutOffDateTime());
			}
		

	}

	@Override
	public boolean validateConsentsGETRequest(PaymentRetrieveGetRequest paymentRetrieveRequest) {
		// validate request parameter payment consent id
		psd2Validator.validate(paymentRetrieveRequest);
		return Boolean.TRUE;
	}

	private void executeDomesticSubmissionRequestCustomValidation(CustomDStandingOrderPOSTRequest submissionRequest) {
		// validate consent id
		validateDomesticPaymentId(submissionRequest.getData().getConsentId(), null, null);

		
	}

	private void executeDomesticSubmissionResponseSwaggerValidations(
			DStandingOrderPOST201Response paymentSubmissionResponse) {
		psd2Validator.validate(paymentSubmissionResponse);

	}

	private void validateDomesticPaymentId(String paymentId, OBErrorCodeEnum errorCodeEnum, String keys) {
		if (!paymentId.matches(paymentIdRegexValidator)) {
			if (NullCheckUtils.isNullOrEmpty(errorCodeEnum)) {
				throw PSD2Exception.populatePSD2Exception(
						new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_RESOURCE_NOTFOUND, ErrorMapKeys.INVALID_PAYMENT_ID));
			} else {
				throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(errorCodeEnum, keys));
			}
		}
	}

	public void executeDomesticSubmissionResponseCustomValidations(
			DStandingOrderPOST201Response paymentSubmissionResponse) {
		
			validateDomesticPaymentId(paymentSubmissionResponse.getData().getConsentId(),
					OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR, InternalServerErrorMessage.PISP_INVALID_ID);
			validateDomesticPaymentId(paymentSubmissionResponse.getData().getDomesticStandingOrderId(),
					OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR, InternalServerErrorMessage.PISP_INVALID_ID);
	
			if (paymentSubmissionResponse.getData().getConsentId()
					.equals(paymentSubmissionResponse.getData().getDomesticStandingOrderId())) {
				throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
						InternalServerErrorMessage.PISP_INVALID_ID));
			}
	
			// validate debtorAccount SchemeName Identification
			if (paymentSubmissionResponse.getData().getInitiation().getDebtorAccount() != null) {
				commonPaymentValidations.validateSchemeNameWithIdentification(
						paymentSubmissionResponse.getData().getInitiation().getDebtorAccount().getSchemeName(),
						paymentSubmissionResponse.getData().getInitiation().getDebtorAccount().getIdentification(),
						OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR);
			}
	
			// validate creditor SchemeName Identification
			if (paymentSubmissionResponse.getData().getInitiation().getCreditorAccount() != null) {
				commonPaymentValidations.validateSchemeNameWithIdentification(
						paymentSubmissionResponse.getData().getInitiation().getCreditorAccount().getSchemeName(),
						paymentSubmissionResponse.getData().getInitiation().getCreditorAccount().getIdentification(),
						OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR);
			}
	
			if (paymentSubmissionResponse.getData().getCharges() != null
					&& !paymentSubmissionResponse.getData().getCharges().isEmpty()) {
	
				for (OBCharge1 charges : paymentSubmissionResponse.getData().getCharges()) {
					commonPaymentValidations.isValidCurrency(charges.getAmount().getCurrency(),
							OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR);
				}
			}
	
			// validate MultiAuth
			commonPaymentValidations.validateMultiAuth(paymentSubmissionResponse.getData().getMultiAuthorisation());
	
			OBMultiAuthorisation1 multiAuthBlock = paymentSubmissionResponse.getData().getMultiAuthorisation();
			if (!NullCheckUtils.isNullOrEmpty(multiAuthBlock)) {
				if (multiAuthBlock.getStatus().equals(OBExternalStatus2Code.AWAITINGFURTHERAUTHORISATION)) {
					if (paymentSubmissionResponse.getData().getStatus().equals(OBExternalStatus1Code.INITIATIONCOMPLETED)) {
						throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
								InternalServerErrorMessage.PISP_INVALID_STATUS));
					}
				}
			}
	
			// validate Currency
			commonPaymentValidations.isValidCurrency(
					paymentSubmissionResponse.getData().getInitiation().getFirstPaymentAmount().getCurrency(),
					OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR);
	
			if (!NullCheckUtils
					.isNullOrEmpty(paymentSubmissionResponse.getData().getInitiation().getFinalPaymentAmount())) {
				commonPaymentValidations.isValidCurrency(
						paymentSubmissionResponse.getData().getInitiation().getFinalPaymentAmount().getCurrency(),
						OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR);
			}
	
			if (!NullCheckUtils
					.isNullOrEmpty(paymentSubmissionResponse.getData().getInitiation().getRecurringPaymentAmount())) {
				commonPaymentValidations.isValidCurrency(
						paymentSubmissionResponse.getData().getInitiation().getRecurringPaymentAmount().getCurrency(),
						OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR);
			}
						
			// Validate first payment date time
			if (paymentSubmissionResponse.getData().getInitiation().getFirstPaymentDateTime() != null) {
				commonPaymentValidations.validateAndParseDateTimeFormatForResponse(
						paymentSubmissionResponse.getData().getInitiation().getFirstPaymentDateTime());
			}
	
			// Validate final payment date time
			if (paymentSubmissionResponse.getData().getInitiation().getFinalPaymentDateTime() != null) {
				commonPaymentValidations.validateAndParseDateTimeFormatForResponse(
						paymentSubmissionResponse.getData().getInitiation().getFinalPaymentDateTime());
				if (!NullCheckUtils
						.isNullOrEmpty(paymentSubmissionResponse.getData().getInitiation().getNumberOfPayments())) {
					throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
							InternalServerErrorMessage.PISP_INVALID_COMBINATION));
				}
			}
	
			// Validate recurring payment date time
			if (paymentSubmissionResponse.getData().getInitiation().getRecurringPaymentDateTime() != null) {
				commonPaymentValidations.validateAndParseDateTimeFormatForResponse(
						paymentSubmissionResponse.getData().getInitiation().getRecurringPaymentDateTime());
			}
			
			// Validate statusUpdateDateTime
			if (paymentSubmissionResponse.getData().getStatusUpdateDateTime() != null) {
				commonPaymentValidations.validateAndParseDateTimeFormatForResponse(
					    paymentSubmissionResponse.getData().getStatusUpdateDateTime());
			}
		
	}

	private void checkIfLessthanRequiredDate(String incomingDate, String comparedDate) {
		OffsetDateTime incomingDateTime = OffsetDateTime.parse(incomingDate);
		if (incomingDateTime.isBefore(OffsetDateTime.parse(comparedDate))) {
			throw PSD2Exception.populatePSD2Exception(
					new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_FIELD_INVALIDDATE, ErrorMapKeys.INVALID_DATE));
		}
	}

	private void checkIfLessthanAndEqualRequiredDate(String incomingDate, String comparedDate) {
		OffsetDateTime incomingDateTime = OffsetDateTime.parse(incomingDate);
		if (!incomingDateTime.isAfter(OffsetDateTime.parse(comparedDate)) && !incomingDateTime.isEqual(OffsetDateTime.parse(comparedDate)) ) {
			throw PSD2Exception.populatePSD2Exception(
					new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_FIELD_INVALIDDATE, ErrorMapKeys.INVALID_PRIOR_DATE));
		}
	}
}
