package com.capgemini.psd2.pisp.validation.adapter.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.exceptions.ErrorMapKeys;
import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.domain.CustomFilePaymentConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.CustomFilePaymentSetupPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomFilePaymentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.FPaymentsRetrieveGetRequest;
import com.capgemini.psd2.pisp.domain.OBWriteFileConsent1;
import com.capgemini.psd2.pisp.domain.PaymentConsentsPlatformResource;
import com.capgemini.psd2.pisp.domain.PaymentFileSubmitPOST201Response;
import com.capgemini.psd2.pisp.domain.PaymentRetrieveGetRequest;
import com.capgemini.psd2.pisp.payment.setup.platform.adapter.PaymentSetupPlatformAdapter;
import com.capgemini.psd2.pisp.processing.adapter.PaymentSubmissionCustomValidator;
import com.capgemini.psd2.pisp.validation.adapter.FilePaymentValidator;
import com.capgemini.psd2.pisp.validation.adapter.constants.CommonPaymentValidations;
import com.capgemini.psd2.utilities.NullCheckUtils;
import com.capgemini.psd2.validator.PSD2Validator;


@Component("filePaymentsValidator")
@ConfigurationProperties("app")
public class FilePaymentValidatorImpl implements FilePaymentValidator,
		PaymentSubmissionCustomValidator<CustomFilePaymentsPOSTRequest, PaymentFileSubmitPOST201Response> {

	@Autowired
	private CommonPaymentValidations commonPaymentValidations;
	
	@Autowired
	private PaymentSetupPlatformAdapter paymentSetupPlatformAdapter;
	
	@Value("${app.regex.paymentId:#{null}}")
	private String paymentIdRegexValidator;

	@Value("${app.swaggerValidations.request:#{true}}")
	private Boolean reqValidationEnabled;

	@Value("${app.swaggerValidations.response:#{true}}")
	private Boolean resValidationEnabled;

	@Autowired
	private PSD2Validator psd2Validator;
	
	@Autowired
	private RequestHeaderAttributes reqAttrib;

	@Override
	public boolean validateFilePaymentSetupPOSTRequest(CustomFilePaymentSetupPOSTRequest filePaymentSetupRequest) {

		commonPaymentValidations.validateHeaders();
		if (reqValidationEnabled)
			executeFileConsentsRequestSwaggerValidations(filePaymentSetupRequest);
		executeFileConsentsRequestCustomValidations(filePaymentSetupRequest);
		return Boolean.TRUE;
	}

	@Override
	public boolean validateFilePaymentSetupPOSTResponse(
			CustomFilePaymentConsentsPOSTResponse customFilePaymentConsentsPOSTResponse) {
		if (resValidationEnabled)
			executeFileConsentsResponseSwaggerValidations(customFilePaymentConsentsPOSTResponse);
		executeFileConsentsResponseCustomValidations(customFilePaymentConsentsPOSTResponse);
		return Boolean.TRUE;
	}

	@Override
	public boolean validateFilePaymentConsentsGETRequest(PaymentRetrieveGetRequest paymentRetrieveRequest) {
		psd2Validator.validateRequestWithSwagger(paymentRetrieveRequest,
				OBErrorCodeEnum.UK_OBIE_FIELD_INVALID.toString());
		return Boolean.TRUE;
	}

	@Override
	public boolean validateFPaymentsDownloadRequest(FPaymentsRetrieveGetRequest paymentsRetrieveGetRequest) {
		psd2Validator.validateRequestWithSwagger(paymentsRetrieveGetRequest,
				OBErrorCodeEnum.UK_OBIE_FIELD_INVALID.toString());
		return Boolean.TRUE;
	}

	@Override
	public boolean validateFilePaymentConsentsPOSTRequest(PaymentRetrieveGetRequest paymentRetrieveRequest) {
		psd2Validator.validateRequestWithSwagger(paymentRetrieveRequest,
				OBErrorCodeEnum.UK_OBIE_FIELD_INVALID.toString());
		PaymentConsentsPlatformResource paymentConsentsPlatformResponse = paymentSetupPlatformAdapter
				.retrievePaymentSetupPlatformResource(paymentRetrieveRequest.getConsentId());

		if (paymentConsentsPlatformResponse != null && !reqAttrib.getIdempotencyKey()
				.equalsIgnoreCase(paymentConsentsPlatformResponse.getIdempotencyKey())) {
			throw PSD2Exception.populatePSD2Exception(
					new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_HEADER_INVALID, ErrorMapKeys.INVALID_IDEMPOTENCY));
		}
		return Boolean.TRUE;
	}
	
	@Override
	public void validatePaymentsPOSTRequest(CustomFilePaymentsPOSTRequest submissionRequestBody) {
		commonPaymentValidations.validateHeaders();
		executeFileSubmissionRequestCustomValidations(submissionRequestBody);
	}

	@Override
	public void validatePaymentsResponse(PaymentFileSubmitPOST201Response submissionResponse) {
		if (resValidationEnabled)
			executeFileConsentsResponseSwaggerValidations(submissionResponse);
		executeFilePaymentsResponseCustomValidations(submissionResponse);
	}

	private void executeFileConsentsRequestSwaggerValidations(OBWriteFileConsent1 paymentConsentsRequest) {
		psd2Validator.validateRequestWithSwagger(paymentConsentsRequest,
				OBErrorCodeEnum.UK_OBIE_FIELD_INVALID.toString());

	}

	private void executeFileConsentsRequestCustomValidations(OBWriteFileConsent1 paymentConsentsRequest) {

		// validate debtorAccount SchemeName Identification
		if (paymentConsentsRequest.getData().getInitiation().getDebtorAccount() != null) {
			commonPaymentValidations.validateSchemeNameWithIdentification(
					paymentConsentsRequest.getData().getInitiation().getDebtorAccount().getSchemeName(),
					paymentConsentsRequest.getData().getInitiation().getDebtorAccount().getIdentification(), null);

			if (!NullCheckUtils.isNullOrEmpty(
					paymentConsentsRequest.getData().getInitiation().getDebtorAccount().getSecondaryIdentification())) {
				commonPaymentValidations.validateSchemeNameWithSecondaryIdentification(
						paymentConsentsRequest.getData().getInitiation().getDebtorAccount().getSchemeName(),
						paymentConsentsRequest.getData().getInitiation().getDebtorAccount()
								.getSecondaryIdentification());
			}
		}

		// validate LocalInstrument
		commonPaymentValidations
				.validateLocalInstrument(paymentConsentsRequest.getData().getInitiation().getLocalInstrument(), null);

		// validate fileType
		commonPaymentValidations.validateFileType(paymentConsentsRequest.getData().getInitiation().getFileType(), null);

		// validate Datetime
		if (paymentConsentsRequest.getData().getAuthorisation() != null
				&& paymentConsentsRequest.getData().getAuthorisation().getCompletionDateTime() != null)
			commonPaymentValidations
					.validateDateTime(paymentConsentsRequest.getData().getAuthorisation().getCompletionDateTime());

		if (paymentConsentsRequest.getData().getInitiation() != null
				&& paymentConsentsRequest.getData().getInitiation().getRequestedExecutionDateTime() != null)
			commonPaymentValidations
					.validateDateTime(paymentConsentsRequest.getData().getInitiation().getRequestedExecutionDateTime());
	}

	private void executeFileConsentsResponseSwaggerValidations(
			CustomFilePaymentConsentsPOSTResponse customFilePaymentConsentsPOSTResponse) {
		psd2Validator.validate(customFilePaymentConsentsPOSTResponse);
	}

	private void executeFileConsentsResponseCustomValidations(
			CustomFilePaymentConsentsPOSTResponse customFileConsentsResponse) {

		// validate debtorAccount SchemeName Identification
		if (customFileConsentsResponse.getData().getInitiation().getDebtorAccount() != null) {
			commonPaymentValidations.validateSchemeNameWithIdentification(
					customFileConsentsResponse.getData().getInitiation().getDebtorAccount().getSchemeName(),
					customFileConsentsResponse.getData().getInitiation().getDebtorAccount().getIdentification(), null);

			if (!NullCheckUtils.isNullOrEmpty(customFileConsentsResponse.getData().getInitiation().getDebtorAccount()
					.getSecondaryIdentification())) {
				commonPaymentValidations.validateSchemeNameWithSecondaryIdentification(
						customFileConsentsResponse.getData().getInitiation().getDebtorAccount().getSchemeName(),
						customFileConsentsResponse.getData().getInitiation().getDebtorAccount()
								.getSecondaryIdentification());
			}
		}

		// validate LocalInstrument
		commonPaymentValidations.validateLocalInstrument(
				customFileConsentsResponse.getData().getInitiation().getLocalInstrument(), null);

		// validate fileType
		commonPaymentValidations.validateFileType(customFileConsentsResponse.getData().getInitiation().getFileType(),
				null);

		// validate Datetime
		if (customFileConsentsResponse.getData().getAuthorisation() != null
				&& customFileConsentsResponse.getData().getAuthorisation().getCompletionDateTime() != null)
			commonPaymentValidations.validateAndParseDateTimeFormatForResponse(
					customFileConsentsResponse.getData().getAuthorisation().getCompletionDateTime());

		if (customFileConsentsResponse.getData().getInitiation().getRequestedExecutionDateTime() != null)
			commonPaymentValidations.validateAndParseDateTimeFormatForResponse(
					customFileConsentsResponse.getData().getInitiation().getRequestedExecutionDateTime());

		if (null != customFileConsentsResponse.getData().getStatusUpdateDateTime()) {
			commonPaymentValidations.validateAndParseDateTimeFormatForResponse(
					customFileConsentsResponse.getData().getStatusUpdateDateTime());
		}

		if (null != customFileConsentsResponse.getData().getCutOffDateTime()) {
			commonPaymentValidations.validateAndParseDateTimeFormatForResponse(
					customFileConsentsResponse.getData().getCutOffDateTime());
		}

		if (null != customFileConsentsResponse.getData().getCreationDateTime()) {
			commonPaymentValidations.validateAndParseDateTimeFormatForResponse(
					customFileConsentsResponse.getData().getCreationDateTime());
		}

	}


	private void executeFileSubmissionRequestCustomValidations(CustomFilePaymentsPOSTRequest paymentSubmissionRequest) {

		// validate consent id
				validateFilePaymentId(paymentSubmissionRequest.getData().getConsentId(), null, null);
		
		
	}

	private void executeFileConsentsResponseSwaggerValidations(PaymentFileSubmitPOST201Response submissionResponse) {
		psd2Validator.validate(submissionResponse);
	}

	private void executeFilePaymentsResponseCustomValidations(
			PaymentFileSubmitPOST201Response customFileConsentsResponse) {

		// validate debtorAccount SchemeName Identification
		if (customFileConsentsResponse.getData().getInitiation().getDebtorAccount() != null) {
			commonPaymentValidations.validateSchemeNameWithIdentification(
					customFileConsentsResponse.getData().getInitiation().getDebtorAccount().getSchemeName(),
					customFileConsentsResponse.getData().getInitiation().getDebtorAccount().getIdentification(), null);

			if (!NullCheckUtils.isNullOrEmpty(customFileConsentsResponse.getData().getInitiation().getDebtorAccount()
					.getSecondaryIdentification())) {
				commonPaymentValidations.validateSchemeNameWithSecondaryIdentification(
						customFileConsentsResponse.getData().getInitiation().getDebtorAccount().getSchemeName(),
						customFileConsentsResponse.getData().getInitiation().getDebtorAccount()
								.getSecondaryIdentification());
			}
		}

		// validate LocalInstrument
		commonPaymentValidations.validateLocalInstrument(
				customFileConsentsResponse.getData().getInitiation().getLocalInstrument(), null);

		// validate fileType
		commonPaymentValidations.validateFileType(customFileConsentsResponse.getData().getInitiation().getFileType(),
				null);

		// validate Datetime
		if (customFileConsentsResponse.getData().getInitiation().getRequestedExecutionDateTime() != null)
			commonPaymentValidations.validateAndParseDateTimeFormatForResponse(
					customFileConsentsResponse.getData().getInitiation().getRequestedExecutionDateTime());

		if (null != customFileConsentsResponse.getData().getStatusUpdateDateTime()) {
			commonPaymentValidations.validateAndParseDateTimeFormatForResponse(
					customFileConsentsResponse.getData().getStatusUpdateDateTime());
		}

		if (null != customFileConsentsResponse.getData().getCreationDateTime())
			commonPaymentValidations.validateAndParseDateTimeFormatForResponse(
					customFileConsentsResponse.getData().getCreationDateTime());

		if (!NullCheckUtils.isNullOrEmpty(customFileConsentsResponse.getData().getMultiAuthorisation())) {
			if (!NullCheckUtils.isNullOrEmpty(
					customFileConsentsResponse.getData().getMultiAuthorisation().getExpirationDateTime()))
				commonPaymentValidations.validateAndParseDateTimeFormatForResponse(
						customFileConsentsResponse.getData().getMultiAuthorisation().getExpirationDateTime());
			if (!NullCheckUtils.isNullOrEmpty(
					customFileConsentsResponse.getData().getMultiAuthorisation().getLastUpdateDateTime()))
				commonPaymentValidations.validateAndParseDateTimeFormatForResponse(
						customFileConsentsResponse.getData().getMultiAuthorisation().getLastUpdateDateTime());
		}
	}
	
	private void validateFilePaymentId(String paymentId, OBErrorCodeEnum errorCodeEnum, String keys) {
		if (!paymentId.matches(paymentIdRegexValidator)) {
			if (NullCheckUtils.isNullOrEmpty(errorCodeEnum)) {
				throw PSD2Exception.populatePSD2Exception(
						new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_RESOURCE_NOTFOUND, ErrorMapKeys.INVALID_PAYMENT_ID));
			} else {
				throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(errorCodeEnum, keys));
			}
		}
	}
}
