/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.exception;

import com.capgemini.psd2.dcr.domain.RegistrationError;
import com.capgemini.psd2.dcr.domain.RegistrationError.ErrorEnum;
import com.capgemini.psd2.enums.ClientRegistrationErrorCodeEnum;
import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.ErrorInfo;
import com.capgemini.psd2.exceptions.PSD2Exception;

/**
 * The Class PortalException.
 */
public class ClientRegistrationException extends PSD2Exception {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Instantiates a new adapter exception.
	 *
	 * @param message
	 *            the message
	 * @param errorInfo
	 *            the error info
	 */
	public ClientRegistrationException(String message, ErrorInfo errorInfo) {
		super(message, errorInfo);
	}

	public ClientRegistrationException(String message, RegistrationError registrationError) {
		super(message, registrationError);
	}
	/**
	 * Populate PSD 2 exception.
	 *
	 * @param detailErrorMessage
	 *            the detail error message
	 * @param errorCodeEnum
	 *            the error code enum
	 * @return the adapter exception
	 */
	public static ClientRegistrationException populatePortalException(String detailErrorMessage, ErrorCodeEnum errorCodeEnum) {
		ErrorInfo errorInfo = new ErrorInfo();
		errorInfo.setErrorCode(errorCodeEnum.getErrorCode());
		errorInfo.setErrorMessage(errorCodeEnum.getErrorMessage());
		errorInfo.setDetailErrorMessage(detailErrorMessage);
		errorInfo.setStatusCode(errorCodeEnum.getStatusCode());
		return new ClientRegistrationException(detailErrorMessage, errorInfo);
	}

	/**
	 * Populate PSD 2 exception.
	 *
	 * @param errorCodeEnum
	 *            the error code enum
	 * @return the adapter exception
	 */
	public static ClientRegistrationException populatePortalException(ErrorCodeEnum errorCodeEnum) {
		ErrorInfo errorInfo = new ErrorInfo();
		errorInfo.setErrorCode(errorCodeEnum.getErrorCode());
		errorInfo.setErrorMessage(errorCodeEnum.getErrorMessage());
		errorInfo.setDetailErrorMessage(errorCodeEnum.getDetailErrorMessage());
		errorInfo.setStatusCode(errorCodeEnum.getStatusCode());
		return new ClientRegistrationException(errorCodeEnum.getErrorMessage(), errorInfo);
	}

	public static ClientRegistrationException populatePortalException(String detailErrorMessage,
			ClientRegistrationErrorCodeEnum errorCodeEnum) {
		ErrorInfo errorInfo = new ErrorInfo();
		errorInfo.setErrorCode(errorCodeEnum.getErrorCode());
		errorInfo.setErrorMessage(errorCodeEnum.getErrorMessage());
		errorInfo.setDetailErrorMessage(detailErrorMessage);
		errorInfo.setStatusCode(errorCodeEnum.getStatusCode());
		return new ClientRegistrationException(detailErrorMessage, errorInfo);
	}

	/**
	 * Populate PSD 2 exception.
	 *
	 * @param errorCodeEnum
	 *            the error code enum
	 * @return the adapter exception
	 */
	public static ClientRegistrationException populatePortalException(ClientRegistrationErrorCodeEnum errorCodeEnum) {
		ErrorInfo errorInfo = new ErrorInfo();
		errorInfo.setErrorCode(errorCodeEnum.getErrorCode());
		errorInfo.setErrorMessage(errorCodeEnum.getErrorMessage());
		errorInfo.setDetailErrorMessage(errorCodeEnum.getDetailErrorMessage());
		errorInfo.setStatusCode(errorCodeEnum.getStatusCode());
		return new ClientRegistrationException(errorCodeEnum.getErrorMessage(), errorInfo);
	}

	public static ClientRegistrationException populatePortalException(ClientRegistrationErrorCodeEnum errorCodeEnum,String detailErrorMessage) {
		ErrorInfo errorInfo = new ErrorInfo();
		errorInfo.setErrorCode(errorCodeEnum.getErrorCode());
		errorInfo.setErrorMessage(errorCodeEnum.getErrorMessage());
		errorInfo.setDetailErrorMessage(errorCodeEnum.getDetailErrorMessage());
		errorInfo.setStatusCode(errorCodeEnum.getStatusCode());
		//errorInfo.se
		return new ClientRegistrationException(detailErrorMessage, errorInfo);
	}
	public static ClientRegistrationException populateDCRException(String detailErrorMessage, ClientRegistrationErrorCodeEnum errorCodeEnum) {
		RegistrationError error= new RegistrationError();
		error.setCode(errorCodeEnum.getErrorCode());
		error.setStatusCode(errorCodeEnum.getStatusCode());
		error.setError(ErrorEnum.fromValue(errorCodeEnum.getErrorMessage()));
		error.setErrorDescription(detailErrorMessage);
		return new ClientRegistrationException(detailErrorMessage, error);
	}
	
	public static ClientRegistrationException populateDCRException(ClientRegistrationErrorCodeEnum errorCodeEnum) {
		RegistrationError error= new RegistrationError();
		error.setCode(errorCodeEnum.getErrorCode());
		error.setStatusCode(errorCodeEnum.getStatusCode());
		error.setError(ErrorEnum.fromValue(errorCodeEnum.getErrorMessage()));
		error.setErrorDescription(errorCodeEnum.getDetailErrorMessage());
		return new ClientRegistrationException(errorCodeEnum.getErrorMessage(), error);
	}
}
