package com.capgemini.psd2.jwt;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.text.ParseException;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.jwt.Jwt;
import org.springframework.security.jwt.JwtHelper;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.enums.ClientRegistrationErrorCodeEnum;
import com.capgemini.psd2.exception.ClientRegistrationException;
import com.capgemini.psd2.utilities.AbstractJWSVerifier;
import com.capgemini.psd2.utilities.JSONUtilities;
import com.capgemini.tpp.ssa.model.SSAModel;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jwt.JWT;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.JWTParser;
import com.nimbusds.jwt.SignedJWT;

@Component
public final class JWTReader {

	@Autowired
	private AbstractJWSVerifier abstractJWSVer;
	
	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(JWTReader.class);

	private static 	AbstractJWSVerifier abstractJWSVerifier;
	
	private JWTReader() {

	}
	
	@PostConstruct
	public void init(){
		//abstractJWSVerifier=abstractJWSVer;
	setAbstractJWSVerifier(abstractJWSVer);
	}
	
	private static void setAbstractJWSVerifier (AbstractJWSVerifier jwsVerifier){
		abstractJWSVerifier=jwsVerifier;
	}
	
	/**
	 * Decodes the token, checks its type Get the algorithm from OB Public Key
	 * Creates a JWTverifier from the above algorithm and issuer(from yml file)
	 * verifies the token from the above JWTVerifier
	 * 
	 * @param issuer
	 * @param token
	 * @param jwks
	 * @return SSAModel
	 * @throws JOSEException
	 * @throws ParseException
	 * @throws NoSuchAlgorithmException
	 * @throws InvalidKeySpecException
	 * @throws Exception
	 */
	public static SSAModel decodeAndVerifyToken(String issuer, String token, String jwks)
			throws InvalidKeySpecException, NoSuchAlgorithmException, ParseException, JOSEException {
		
		verifyJwt(issuer, token, jwks);
		return decodeTokenIntoSSAModel(token);
	}


	/**
	 * Creates a JWTverifier from the above algorithm and issuer(from yml file)
	 * verifies the token from the above JWTVerifier
	 * 
	 * @param issuer
	 * @param token
	 * @param jwks
	 * @throws ParseException
	 * @throws InvalidKeySpecException
	 * @throws NoSuchAlgorithmException
	 * @throws JOSEException
	 */
	public static void verifyJwt(String issuer, String token, String jwks)
			throws ParseException, InvalidKeySpecException, NoSuchAlgorithmException, JOSEException {
		
		SignedJWT signedJWT = SignedJWT.parse(token);
		// issuer value will be null at the time of login verification.
		if (issuer != null) {
			if (!("JWT").equals(signedJWT.getHeader().getType().getType())) {
				throw ClientRegistrationException.populateDCRException(ClientRegistrationErrorCodeEnum.TOKEN_TYPE_INVALID);
			}
			JWTClaimsSet claimsSet = signedJWT.getJWTClaimsSet();

			if (!claimsSet.getIssuer().equalsIgnoreCase(issuer)) {
				throw ClientRegistrationException.populateDCRException(ClientRegistrationErrorCodeEnum.INVALID_SSA_ISSUER);
			}
		}
		
		 if (!abstractJWSVerifier.verifyJWS(jwks, signedJWT))
		 
			throw ClientRegistrationException.populateDCRException(ClientRegistrationErrorCodeEnum.INVALID_SSA_SIGNATURE);
		
		// SSA Token does not have expiry for now.. hence removing the check
	}

	/**
	 * Decodes the token(String) into Map of string, Object
	 * 
	 * @param token
	 * @return Map containing token elements as Key:Valur pair
	 */
	public static Map<String, Object> decodeTokenIntoMap(String token) {
		Jwt byRefJwt = JwtHelper.decode(token);
		return (Map<String, Object>) JSONUtilities.getObjectFromJSONString(byRefJwt.getClaims(), Map.class);
	}

	/**
	 * Decodes the token(String) and sets all the Token properties into SSAModel
	 * class
	 * 
	 * @param token
	 * @return SSAModel (class with all the token properties)
	 */
	public static SSAModel decodeTokenIntoSSAModel(String token) {
		Jwt byRefJwt = JwtHelper.decode(token);
		return JSONUtilities.getObjectFromJSONString(byRefJwt.getClaims(), SSAModel.class);
	}
	
	public static SSAModel decodeTokentoSSAModel(String token) {

		try {
			JWT byRefJwt = JWTParser.parse(token);
			ObjectMapper mappers = new ObjectMapper();
			mappers.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
			return mappers.readValue(byRefJwt.getJWTClaimsSet().toString(), SSAModel.class);
		} catch (IOException | ParseException e) {
			LOG.info("Exception in decodeTokentoSSAModel(): "+e);
			throw ClientRegistrationException.populateDCRException(e.getMessage(),
					ClientRegistrationErrorCodeEnum.INVALID_SSA);
		}
	}

	/**
	 * Decodes the token, checks its type Get the algorithm from OB Public Key
	 * Creates a JWTverifier from the above algorithm. verifies the token from
	 * the above JWTVerifier
	 * 
	 * @param token
	 * @param jwks
	 * @return SSAModel
	 * @throws InvalidKeySpecException
	 * @throws NoSuchAlgorithmException
	 * @throws JOSEException
	 * @throws ParseException
	 */
	public static Map<String, Object> verifyAndDecodeTokenIntoMap(String token, String jwks)
			throws InvalidKeySpecException, NoSuchAlgorithmException, ParseException, JOSEException {
		verifyJwt(null, token, jwks);
		return decodeTokenIntoMap(token);
	}
	
}
