package com.capgemini.psd2.jwt;

import java.security.Key;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.capgemini.psd2.enums.ClientRegistrationErrorCodeEnum;
import com.capgemini.psd2.exception.ClientRegistrationException;
import com.capgemini.psd2.utilities.AbstractJWSVerifier;
import com.nimbusds.jose.Header;
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JWSHeader;
import com.nimbusds.jose.JWSVerifier;
import com.nimbusds.jose.crypto.factories.DefaultJWSVerifierFactory;
import com.nimbusds.jose.proc.JWSVerifierFactory;

public class JWSVerifierImpl extends AbstractJWSVerifier{

	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(JWSVerifierImpl.class);
	
	@Override
	public JWSVerifier createJWSVerifier(Key key,Header header) {
		JWSVerifierFactory jwsVerifierFactory = new DefaultJWSVerifierFactory();
		try {
			return jwsVerifierFactory.createJWSVerifier((JWSHeader) header,key);
		} catch (JOSEException e) {
			LOG.info("JOSEException in createJWSVerifier(): "+e);
			throw ClientRegistrationException.populateDCRException(ClientRegistrationErrorCodeEnum.INVALID_SSA_SIGNATURE);
		}
	}

}
