package com.capgemini.psd2.jwt;

import java.io.StringReader;
import java.math.BigInteger;
import java.security.Key;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.Security;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.RSAPublicKeySpec;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonValue;

import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JWSVerifier;
import com.nimbusds.jose.crypto.bc.BouncyCastleProviderSingleton;
import com.nimbusds.jose.crypto.factories.DefaultJWSVerifierFactory;
import com.nimbusds.jose.jwk.ECKey;
import com.nimbusds.jose.proc.JWSVerifierFactory;
import com.nimbusds.jwt.SignedJWT;

public final class JWKSHelper {

	private JWKSHelper() {
	}

	static {
		org.apache.xml.security.Init.init();
		Security.addProvider(BouncyCastleProviderSingleton.getInstance());
	}

	public static JWSVerifier getVerifier(String jwks, SignedJWT signedJWT) throws InvalidKeySpecException, NoSuchAlgorithmException, JOSEException, ParseException { //NOSONAR
		JsonObject jsonObj = getJWKSJsonObject(jwks);
		Map<String, Key> keyMap = buildKeyMap(jsonObj);
		String kid = signedJWT.getHeader().getKeyID();
		JWSVerifierFactory jwsVerifierFactory = new DefaultJWSVerifierFactory();
		return jwsVerifierFactory.createJWSVerifier(signedJWT.getHeader(), keyMap.get(kid));
	}
	
	private static Map<String, Key> buildKeyMap(final JsonObject obj)
			throws InvalidKeySpecException, NoSuchAlgorithmException, ParseException, JOSEException {
		JsonObject keyJson;
		String kid;
		PublicKey key;
		Map<String, Key> keyMap = new HashMap<>();
		for (final JsonValue v : obj.getJsonArray("keys")) {
			keyJson = (JsonObject) v;
			kid = keyJson.getString("kid");
				key = buildPublicKey(keyJson);
				if (key != null) {
					keyMap.put(kid, key);
				}
		}
		return keyMap;
	}

	private static PublicKey buildPublicKey(final JsonObject keyJson)
			throws InvalidKeySpecException, NoSuchAlgorithmException, ParseException, JOSEException {
		PublicKey publicKey = null;
		String kty = keyJson.getString("kty");
		if ("RSA".equals(kty)) {
			publicKey = buildRSAPublicKey(keyJson);
		}
		else if ("EC".equals(kty)) {
			publicKey = buildECPublicKey(keyJson);
		}
		return publicKey;
	}

	private static PublicKey buildRSAPublicKey(final JsonObject keyJson)
			throws InvalidKeySpecException, NoSuchAlgorithmException {

		BigInteger modulus = new BigInteger(1,
				org.apache.commons.codec.binary.Base64.decodeBase64(keyJson.getString("n")));
		BigInteger publicExponent = new BigInteger(1,
				org.apache.commons.codec.binary.Base64.decodeBase64(keyJson.getString("e")));
		return KeyFactory.getInstance("RSA").generatePublic(new RSAPublicKeySpec(modulus, publicExponent));
	}

	private static PublicKey buildECPublicKey(final JsonObject keyJson)
			throws ParseException, JOSEException {
		ECKey ec = ECKey.parse(keyJson.toString());
		return ec.toECPublicKey();
	}

	private static JsonObject getJWKSJsonObject(String jwks) {
		JsonReader reader = Json.createReader(new StringReader(jwks));
		JsonObject jwksJsonObject = reader.readObject();
		reader.close();
		return jwksJsonObject;
	}
	
}
