package com.capgemini.psd2.service.impl;

import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.security.spec.InvalidKeySpecException;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

import javax.json.JsonObject;
import javax.json.JsonValue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.capgemini.psd2.config.PFClientRegConfig;
import com.capgemini.psd2.enums.ClientRegistrationErrorCodeEnum;
import com.capgemini.psd2.exception.ClientRegistrationException;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.helper.TppSoftwareHelper;
import com.capgemini.psd2.jwt.JWTReader;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.RestClientSync;
import com.capgemini.psd2.service.MuleService;
import com.capgemini.psd2.service.PingDirectoryService;
import com.capgemini.psd2.service.PingFederateService;
import com.capgemini.psd2.service.PortalService;
import com.capgemini.psd2.utilities.GenerateUniqueIdUtilities;
import com.capgemini.psd2.utilities.JSONUtilities;
import com.capgemini.psd2.utilities.JWKSUtility;
import com.capgemini.psd2.utilities.NullCheckUtils;
import com.capgemini.tpp.dtos.AddApplicationAttributes;
import com.capgemini.tpp.dtos.AddApplicationStageEnum;
import com.capgemini.tpp.dtos.DeleteApplicationStage;
import com.capgemini.tpp.dtos.DeleteApplicationStageEnum;
import com.capgemini.tpp.dtos.TPPDetails;
import com.capgemini.tpp.ldap.client.model.ClientModel;
import com.capgemini.tpp.muleapi.schema.ApisResponse;
import com.capgemini.tpp.muleapi.schema.Application;
import com.capgemini.tpp.muleapi.schema.LoginResponse;
import com.capgemini.tpp.muleapi.schema.NewApplicationMuleApiResponse;
import com.capgemini.tpp.muleapi.schema.ResetSecretResponse;
import com.capgemini.tpp.muleapi.schema.ShowSecretResponse;
import com.capgemini.tpp.registration.model.OBClientRegistration1;
import com.capgemini.tpp.registration.model.PfErrorModel;
import com.capgemini.tpp.ssa.model.SSAModel;
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.util.X509CertUtils;

@Service
public class PortalServiceImpl implements PortalService {

	private static final Logger LOGGER = LoggerFactory.getLogger(PortalServiceImpl.class);

	@Autowired
	private MuleService muleService;

	@Autowired
	private PingFederateService pingFederateService;
	
	@Autowired
	private PFClientRegConfig pfClientRegConfig;
	

	@Autowired
	private PingDirectoryService pingDirectoryService;

	@Value("${jwt.issuer}")
	private String issuer;

	@Value("${openbanking.jwks.endpoint:null}")
	private String obJWKSEndpoint;

	@Value("${muleclientReg.createContractForAllApis}")
	private Boolean createContractForAllApis;

	@Value("${muleclientReg.isContractRequired:#{false}}")
	private Boolean isContractRequired;
	
	@Autowired
	@Qualifier("tppPortalRestClient")
	private RestClientSync restClient;
	
	@Value("${dynamicClient:#{false}}")
	private Boolean isDynamicClient;
	
	/*
	 * (non-Javadoc)
	 * 
	 * Get the OB Public Key by calling an exposed OB endpoint verifies and
	 * decodes the token data into a model class named SSAModel
	 * 
	 * @see
	 * com.capgemini.tppportal.services.PortalService#decodeAndVerifyToken(java.
	 * lang.String)
	 */
	@Override
	public SSAModel decodeAndVerifyToken(String token) throws InvalidKeySpecException, NoSuchAlgorithmException, ParseException, JOSEException {
		String jwks = getOBPublicKey(obJWKSEndpoint);
		return JWTReader.decodeAndVerifyToken(issuer, token, jwks);
	}
	
	@Override
	public void verifyToken(String token) throws InvalidKeySpecException, NoSuchAlgorithmException, ParseException, JOSEException {
		String jwks = getOBPublicKey(obJWKSEndpoint);
		JWTReader.verifyJwt(issuer, token, jwks);
	}

	/*
	 * The Following Steps are executed here: 
	 * 1. The Application is Registered
	 * in Ping Federate and the entry goes into Ping directory(PD) at
	 * [ou=oauthclients,ou=application,dc=boi,dc=co.uk,dc=bank,dc=com] 
	 * 2. Mule
	 * Login api is called and accessToken is returned which is used to access
	 * the other apis of Mule 
	 * 3. The Application is Registered in Mule and an in
	 * return an Application Id is created 
	 * 4. List of All the Organization Apis
	 * is fetched (and there may exist multiple versions of an Api) 
	 * 5. Now
	 * Contract is created between the Apis (and all its versions) and the
	 * Application registered in Mule in step 3. 
	 * 6. Before creating contract
	 * Tiers of each of the apis is fetched, If the Tiers is > 1 then an
	 * exception is thrown and all the above steps are rolled back. 
	 * 7. if
	 * Contracts is successfully created, we will update/insert the Tpp
	 * Organisation details in PD () 
	 * 8. then we associate the Tpp Application
	 * with the Tpp Organisation In PD at
	 * [ou=tpp,ou=groups,dc=boi,dc=co.uk,dc=bank,dc=com] 
	 * 9. Last Step is
	 * creating an entry of Tpp Application with its details in PD at
	 * [ou=clientAppMapping,ou=groups,dc=boi,dc=co.uk,dc=bank,dc=com] 
	 * 10. If In
	 * any of the above steps exception occurs then RollBack is executed.
	 * 
	 * @see
	 * com.capgemini.tppportal.services.PortalService#createTppApplication(com.
	 * capgemini.tppportal.ssa.model.SSAModel, java.lang.String,
	 * java.lang.String)
	 */
	@Override
	public void createTppApplication(SSAModel ssaDataElements, String tppOrgId, String ssaToken)
			throws InvalidKeySpecException, NoSuchAlgorithmException {
		AddApplicationAttributes addApplicationAttributes = new AddApplicationAttributes();
		String clientSecret = GenerateUniqueIdUtilities.generateRandomClientSeceret();
		try {
			// refer Step 1
			ClientModel clientRegResponse = pingFederateService.registerTPPApplicationinPF(ssaDataElements,
					clientSecret);

			// setting stage for RollBack Execution
			addApplicationAttributes.setStage(AddApplicationStageEnum.PF_APP_REG_SUCCESS);
			// setting PF clientId for RollBack of Ping Federate
			addApplicationAttributes.setClientId(clientRegResponse.getClientId());

			// refer Step 2
			NewApplicationMuleApiResponse applicationResponse;
			if(isDynamicClient){
				applicationResponse = new NewApplicationMuleApiResponse();
				//set response id as -1, coz mule call is bypassed if channel is dynamic client.
				applicationResponse.setId("-1");
			}
			else{
				//if channel is TppPortal
			LoginResponse loginResponse = muleService.gatewayMuleAuthentication();

			// setting Mule AccessToken for RollBack from Mule
			addApplicationAttributes.setLoginResponse(loginResponse);

			// refer Step 3
			applicationResponse = muleService
					.registerTPPApplicationInMule(clientRegResponse, ssaDataElements, clientSecret, loginResponse);

			addApplicationAttributes.setStage(AddApplicationStageEnum.MULE_APP_REG_SUCCESS);
			// setting MuleApplicationId for RollBack of Mule Application
			// Registration
			addApplicationAttributes.setApplicationId(applicationResponse.getId());
			
			ApisResponse apisResponse = null;
			if(createContractForAllApis){
			// refer step 4
				apisResponse = muleService.fetchListOfAllOrgApis(loginResponse);

			if (NullCheckUtils.isNullOrEmpty(apisResponse))
				throw ClientRegistrationException.populatePortalException(ClientRegistrationErrorCodeEnum.MULE_ORGANISATION_APIS_NOT_AVAILABLE);
			}
			
			// refer step 5,6
			if(isContractRequired)
				muleService.doApplicationContract(apisResponse,Integer.valueOf(applicationResponse.getId()), loginResponse);
			
			}
			// LDAP Operations
			// refer step 7
			boolean tppExist = pingDirectoryService.isTPPExist(tppOrgId);
			
			// refer step 7
			if (tppExist)
				pingDirectoryService.registerOrUpdateTPPOrg(tppOrgId, ssaDataElements, Boolean.FALSE);
			else
				pingDirectoryService.registerOrUpdateTPPOrg(tppOrgId, ssaDataElements, Boolean.TRUE);

			// for RollBack Execution
			// if Tpp already existed then rollBack The updation of Tpp Org
			// Details
			// else if Tpp was newly inserted then delete the Tpp Org from PD
			addApplicationAttributes.setIsTppExist(tppExist);

			// for RollBack of Tpp Application Association with TPP &
			// registerOrUpdateTPPOrg rollback
			addApplicationAttributes.setTppOrgId(tppOrgId);
			addApplicationAttributes.setStage(AddApplicationStageEnum.PD_TPP_ORG_REG_SUCCESS);

			// refer step 8
			pingDirectoryService.associateTPPApplicationWithTPP(tppOrgId, clientRegResponse);

			addApplicationAttributes.setStage(AddApplicationStageEnum.PD_TPP_APP_ASS_SUCCESS);

			// refer step 9
			pingDirectoryService.mapClientApp(clientRegResponse.getClientId(), applicationResponse.getId(), tppOrgId,
					ssaToken,ssaDataElements.getSoftware_jwks_endpoint(),ssaDataElements.getPublicKey());
		} catch (ClientRegistrationException e) {
			LOGGER.info("Exception Occurred in createTppApplication(): "+e);
			rollbackTppApplicationFlow(addApplicationAttributes);
		}
	}

	/*
	 * RollBack is executed based on the Attributes in the
	 * AddApplicationAttributes class RollBack is done in the opposite flow of
	 * create application.
	 * 
	 * The stage of create application at which the exception occurred is stored
	 * in stage Enum. When the Exception is catched this method is passed all
	 * the Attributes required to execute the rollback with the stage at which
	 * it failed.
	 * 
	 * secondly, If the RollBack also fails to execute, then it will be handled
	 * through manual intervention. To help Manual intervention: Logging with
	 * the Attributes required to execute the rollback steps manually is done.
	 * To distinguish the logs for RollBack Failure -- Keyword FATAL is used
	 * while Logging.
	 * 
	 * @see
	 * com.capgemini.tppportal.services.PortalService#rollbackTppApplicationFlow
	 * (com.capgemini.tppportal.dtos.AddApplicationAttributes)
	 */
	@Override
	public void rollbackTppApplicationFlow(AddApplicationAttributes addApplicationAttributes) {
		Boolean rollBackStatus = false;
		if (!NullCheckUtils.isNullOrEmpty(addApplicationAttributes.getStage())) {
			try {
				rollBackStatus = rollBackTPPApplicationAssociationWithTPP(addApplicationAttributes, rollBackStatus);
				rollBackStatus = rollBackTppOrgRegistrationOrUpdation(addApplicationAttributes, rollBackStatus);
				if(!isDynamicClient)
				{
				rollBackStatus = rollBackTppAppRegistrationInMule(addApplicationAttributes, rollBackStatus);
				}
				rollBackPFAppRegistration(addApplicationAttributes, rollBackStatus);

			} catch (ClientRegistrationException e) {
				LOGGER.info("Exception Occurred in rollbackTppApplicationFlow(): "+e);
				if (NullCheckUtils.isNullOrEmpty(addApplicationAttributes.getApplicationId())) {
					LOGGER.error(
							"{\"FATAL: Exception In RollBack\":\"{}\",\"Attributes Required for clean up are Client Id \":\"{}\"}",
							e.getMessage(), addApplicationAttributes.getClientId());
				} else {
					LOGGER.error(
							"{\"FATAL: Exception In RollBack\":\"{}\",\"Attributes Required for clean up are Client Id \":\"{}\",\" Application Id \":\"{}\"}",
							e.getMessage(), addApplicationAttributes.getClientId(),
							addApplicationAttributes.getApplicationId());
				}
				throw ClientRegistrationException.populateDCRException(e.getMessage(), ClientRegistrationErrorCodeEnum.TPP_ERROR_GENRIC);
			}
		}
		throw ClientRegistrationException.populateDCRException(ClientRegistrationErrorCodeEnum.TPP_ERROR_GENRIC);
	}

	private boolean rollBackTPPApplicationAssociationWithTPP(AddApplicationAttributes addApplicationAttributes,
			Boolean rollBackStatus) {
		if (addApplicationAttributes.getStage().equals(AddApplicationStageEnum.PD_TPP_APP_ASS_SUCCESS)) {
			pingDirectoryService.deleteTPPApplicationAssociationWithTPP(addApplicationAttributes.getTppOrgId(),
					addApplicationAttributes.getClientId());
			rollBackStatus = true; //NOSONAR
		}
		return rollBackStatus;
	}

	private boolean rollBackTppOrgRegistrationOrUpdation(AddApplicationAttributes addApplicationAttributes,
			Boolean rollBackStatus) {
		if (addApplicationAttributes.getStage().equals(AddApplicationStageEnum.PD_TPP_ORG_REG_SUCCESS)
				|| rollBackStatus) {
			if (!addApplicationAttributes.getIsTppExist()) {
				// as discussed with Mohit removed TppOrg updation
				pingDirectoryService.deleteTppOrgRegistration(addApplicationAttributes.getTppOrgId());
			}
			rollBackStatus = true; //NOSONAR
		}
		return rollBackStatus;
	}

	private boolean rollBackTppAppRegistrationInMule(AddApplicationAttributes addApplicationAttributes,
			Boolean rollBackStatus) {
		if (addApplicationAttributes.getStage().equals(AddApplicationStageEnum.MULE_APP_REG_SUCCESS)
				|| rollBackStatus) {
			muleService.deleteTppAppRegisterationInMule(addApplicationAttributes.getLoginResponse(),
					addApplicationAttributes.getApplicationId());
			rollBackStatus = true; //NOSONAR
		}
		return rollBackStatus;
	}

	private void rollBackPFAppRegistration(AddApplicationAttributes addApplicationAttributes, Boolean rollBackStatus) {
		if (addApplicationAttributes.getStage().equals(AddApplicationStageEnum.PF_APP_REG_SUCCESS) || rollBackStatus)
			pingFederateService.deletePFAppRegisteration(addApplicationAttributes.getClientId());
	}

	/*
	 * Steps are as follows: 
	 * 1. Delete the application from PF 
	 * 2. if successfully deleted from PF then , delete the app from ou=tpp 
	 * 3. else show the error message 
	 * 4. if successfully deleted from PD at ou=tpp, then
	 * delete the app from ou=clientAppMapping 
	 * 5. else show the error message 
	 * 6. if successfully deleted from PD at ou=clientAppMapping then delete the
	 * application from Mule. 
	 * 7. else do the logging for manual deletion and
	 * show Success message on the screen
	 *
	 * Scenerio: In case the application was deleted from PF and error message
	 * was shown due to unsuccessful attempt at deletion from PD. Now the second
	 * time the user attempts to delete the application we will ignore the
	 * exception thrown from PF for the application not being found.
	 * 
	 *
	 * @see
	 * com.capgemini.tppportal.services.PortalService#deleteTppApplication(java.
	 * lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public void deleteTppApplication(String clientId, String tppOrgId, String applicationId) {
		DeleteApplicationStage deleteApplicationStage = new DeleteApplicationStage();

		try {
			deleteApplicationStage.setStage(DeleteApplicationStageEnum.PF_APP_DELETE_FAILURE);
			pingFederateService.deletePFAppRegisteration(clientId);
		} catch (Exception e) {
			LOGGER.info("Exception Occurred in deleteTppApplication(): "+e);
			// Trying to delete the application the second time after exception
			// was thrown while deleting from PD at ou=clientAppMapping
			if (deleteApplicationStage.getStage().equals(DeleteApplicationStageEnum.PF_APP_DELETE_FAILURE)
					&& "404 Not Found".equals(e.getMessage()))
				LOGGER.error(
						"{\"Application was already deleted from Ping Federate in first attempt.. continuing to delete from PD and Mule \"}");
			else
				throw ClientRegistrationException.populateDCRException(e.getMessage(), ClientRegistrationErrorCodeEnum.TPP_ERROR_GENRIC);
		}
		deleteTppApplicationFromMuleAndPD(clientId, tppOrgId, applicationId, deleteApplicationStage);

	}

	public void deleteTppApplicationFromMuleAndPD(String clientId, String tppOrgId, String applicationId,
			DeleteApplicationStage deleteApplicationStage) {
		try {
			deleteApplicationStage.setStage(DeleteApplicationStageEnum.PD_CLIENT_ASS_DELETE_FAILURE);
			pingDirectoryService.deleteTPPApplicationAssociationWithTPP(tppOrgId, clientId);

			deleteApplicationStage.setStage(DeleteApplicationStageEnum.PD_CLIENT_APP_MAPPING_DELETE_FAILURE);
			pingDirectoryService.deleteMapClientApp(clientId);

			deleteApplicationStage.setStage(DeleteApplicationStageEnum.MULE_APP_DELETE_FAILURE);
			LoginResponse loginResponse = muleService.gatewayMuleAuthentication();
			muleService.deleteTppAppRegisterationInMule(loginResponse, applicationId);
		} catch (Exception e) {
			LOGGER.info("Exception Occurred in deleteTppApplicationFromMuleAndPD(): "+e);
			if (deleteApplicationStage.getStage().equals(DeleteApplicationStageEnum.MULE_APP_DELETE_FAILURE))
				LOGGER.error(
						"{\"FATAL: Exception In Application Deletion \":\"{}\",\" Attributes Required for Manual deletion are Application Id \":\"{}\"}",
						deleteApplicationStage.getStage().getDescription(), applicationId);
			else
				LOGGER.error(
						"{\"FATAL: Exception In Application Deletion \":\"{}\",\" Attributes Required for Manual deletion is Client Id\":\"{}\",\" ApplicationId\":\"{}\"}",
						deleteApplicationStage.getStage().getDescription(), clientId, applicationId);

			if (deleteApplicationStage.getStage().equals(DeleteApplicationStageEnum.PD_CLIENT_ASS_DELETE_FAILURE))
				throw ClientRegistrationException.populateDCRException(e.getMessage(), ClientRegistrationErrorCodeEnum.TPP_ERROR_GENRIC);
		}
	}

	/*
	 * After Uploading the SSA Token, Fields required to be populated on screen
	 * are fetched from SSAToken and passed as TPPDetails
	 * 
	 * @see com.capgemini.tppportal.services.PortalService#
	 * fetchTppDetailsFromSSAToken(com.capgemini.tppportal.ssa.model.SSAModel)
	 */
	@Override
	public TPPDetails fetchTppDetailsFromSSAToken(SSAModel ssaModel) {
		TPPDetails tppDetails = new TPPDetails();
		tppDetails.setApplicationName(ssaModel.getSoftware_client_name());
		tppDetails.setApplicationDescription(ssaModel.getSoftware_client_description());
		tppDetails.setClientId(ssaModel.getSoftware_client_id());
		tppDetails.setTermOfServiceURI(ssaModel.getSoftware_tos_uri());
		tppDetails.setPolicyURI(ssaModel.getSoftware_policy_uri());
		tppDetails.setJWKSEndPoint(ssaModel.getSoftware_jwks_endpoint());
		tppDetails.setRedirectUris((List<String>) ssaModel.getSoftware_redirect_uris());
		return tppDetails;
	}

	/**
	 * Fetch client secret for requested application from mule
	 * 
	 * @param clientId
	 * @param applicationId
	 * @return ResetSecretResponse
	 */
	@Override
	public ShowSecretResponse fetchClientSecret(String clientId, String applicationId) {
		String clientSecret = null;
		ShowSecretResponse showSecretResponse = new ShowSecretResponse();

		// Login to Mule and get login response
		LoginResponse loginResponse = muleService.gatewayMuleAuthentication();

		// Fetch client secret using loginResponse for given applicationId
		Application application = muleService.fetchClientSecret(loginResponse, applicationId);

		// Check if application and clietId receved are valid, set client secret
		// otherwise throw exception
		if (null == application) {
			throw ClientRegistrationException.populateDCRException(ClientRegistrationErrorCodeEnum.MULE_RESPONSE_NOT_FOUND);
		} else if (application.getClientId().equals(clientId)) {
			clientSecret = application.getClientSecret();
		} else {
			ClientRegistrationException.populateDCRException(ClientRegistrationErrorCodeEnum.TPP_ERROR_GENRIC);
		}
		showSecretResponse.setSecret(clientSecret);
		return showSecretResponse;
	}

	/**
	 * Resets client secret for requested application.
	 * 
	 * @param clientId
	 * @param applicationId
	 * @return ResetSecretResponse
	 */
	@Override
	public ResetSecretResponse resetClientSecret(String clientId, String applicationId) {
		ResetSecretResponse resetSecretResponse;
		/* Fetch the ClientId details from pingFderate */
		ClientModel clientModel = pingFederateService.fetchClientDetailsByIdFromPF(clientId);

		/*
		 * Execute the mule reset secret operation with given mule application
		 * id
		 */
		LoginResponse loginResponse = muleService.gatewayMuleAuthentication();
		resetSecretResponse = muleService.resetClientSecret(loginResponse, applicationId);
		if (null == resetSecretResponse) {
			throw ClientRegistrationException.populateDCRException(ClientRegistrationErrorCodeEnum.MULE_RESPONSE_NOT_FOUND);
		} else {
			ClientRegistrationException.populateDCRException(ClientRegistrationErrorCodeEnum.TPP_ERROR_GENRIC);
		}

		/*
		 * Updates the OAuth client details for new client secret attribute into
		 * pingFderate
		 */
		pingFederateService.updateTPPApplicationinPF(clientModel, clientId, resetSecretResponse.getClientSecret());
		return resetSecretResponse;
	}

	/**
	  * This method is used to retrieve the public key from  Openbanking 
	  */
	
	public String getOBPublicKey(String obJWKSEndpoint) {
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(obJWKSEndpoint);
		return restClient.callForGet(requestInfo, String.class, null);
	}
	
	
	public void importCAinPF(X509Certificate cert) {
		try {
			pingFederateService.addTrustedCAinPF(cert);
			pingFederateService.replicatePFConfiguration();
		} catch (Exception ex) {
			LOGGER.info("Exception Occurred in importCAinPF(): "+ex);
			// by pass if duplicate certificate import exception.
			PfErrorModel pf = JSONUtilities.getObjectFromJSONString(ex.getMessage(), PfErrorModel.class);
			if ("cert_import_duplication_error".equals(pf.getValidationErrors()[0].getErrorId())) {
				return;
			}
			throw ClientRegistrationException
					.populateDCRException(ClientRegistrationErrorCodeEnum.PING_CERTIFICATE_IMPORT_FAILED);
		}
	}

	
	public void updateTppApplication(SSAModel ssaDataElements,String ssaToken) {
		
		ClientModel clientRegRequest = TppSoftwareHelper.populateClientRequest(ssaDataElements, pfClientRegConfig,
				null);
		ClientModel storedClient = pingFederateService.fetchTPPApplicationFromPF(clientRegRequest, clientRegRequest.getClientId(), clientRegRequest.getClientAuth().getSecret());
		if(!clientRegRequest.equals(storedClient))
			throw ClientRegistrationException.populateDCRException(ClientRegistrationErrorCodeEnum.INVALID_REQUEST);
		try {
			pingFederateService.updateTPPApplicationinPF(clientRegRequest, clientRegRequest.getClientId(), clientRegRequest.getClientAuth().getSecret());
			pingDirectoryService.mapClientAppUpdate(ssaDataElements.getSoftware_client_id(),  ssaDataElements.getOrg_id(), ssaDataElements.getPublicKey(),ssaToken);
			}catch(PSD2Exception e) {
				LOGGER.error("PD Update failed failed:"+e);
				try {
				// If update in PD fails then rollback will be triggered in PF with stored client details
				pingFederateService.updateTPPApplicationinPF(storedClient, storedClient.getClientId(), storedClient.getClientAuth().getSecret());
				}catch(PSD2Exception e1) {
					LOGGER.error("PF Rollback failed:"+e1);
					throw e1;
				}
				throw e;
			}
	}
	
	public String verifySubjectDN(OBClientRegistration1 obClientRegistration1,String jwksEndpoint) {
		String subjectDN=obClientRegistration1.getTlsClientAuthSubjectDn();
		String jwksDetails=getOBPublicKey(jwksEndpoint);
		JsonObject jsonObj = JWKSUtility.getJWKSJsonObject(jwksDetails);
		JsonObject keyJson;
		Boolean key;
		StringBuilder cert=new StringBuilder();
		X509Certificate clientCert;
		Date currentDate=new Date();	
		for (final JsonValue v : jsonObj.getJsonArray("keys")) {
			keyJson = (JsonObject) v;
 			if("tls".equals(keyJson.getJsonString("use").toString().replaceAll("\"", ""))) {
 				for(JsonValue s :keyJson.getJsonArray("x5c")) {
 					cert.append("-----BEGIN CERTIFICATE-----\r\n").append(s.toString()).append("\r\n-----END CERTIFICATE-----\r\n");
 					clientCert = X509CertUtils.parse(cert.toString());
 					key = ((subjectDN.replaceAll("\\s+","")).equals(clientCert.getSubjectDN().toString().replaceAll("\\s+","")) &&  clientCert.getNotAfter().after(currentDate));//buildPublicKey(keyJson);;
 					cert.delete(0, cert.length());
 					
 					if(key) { 
 						obClientRegistration1.setTlsClientAuthSubjectDn(clientCert.getSubjectDN().toString());
 						return clientCert.getSubjectDN().toString();
 					}
 				}
 			}
		}
		return null;
	}

	
	}