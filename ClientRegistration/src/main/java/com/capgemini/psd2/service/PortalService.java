package com.capgemini.psd2.service;

import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.security.spec.InvalidKeySpecException;
import java.text.ParseException;

import com.capgemini.tpp.dtos.AddApplicationAttributes;
import com.capgemini.tpp.dtos.TPPDetails;
import com.capgemini.tpp.muleapi.schema.ResetSecretResponse;
import com.capgemini.tpp.muleapi.schema.ShowSecretResponse;
import com.capgemini.tpp.registration.model.OBClientRegistration1;
import com.capgemini.tpp.ssa.model.SSAModel;
import com.nimbusds.jose.JOSEException;

public interface PortalService {

	public void createTppApplication(SSAModel ssaDataElements, String tppName, String ssaToken) throws InvalidKeySpecException, NoSuchAlgorithmException; //NOSONAR
	
	public void updateTppApplication(SSAModel ssaDataElements,String ssaToken);

	public TPPDetails fetchTppDetailsFromSSAToken(SSAModel tokenMap);

	public SSAModel decodeAndVerifyToken(String token) throws InvalidKeySpecException, NoSuchAlgorithmException, ParseException, JOSEException; //NOSONAR

	public ShowSecretResponse fetchClientSecret(String clientId, String applicationId);

	public void rollbackTppApplicationFlow(AddApplicationAttributes addApplicationAttributes);

	public void deleteTppApplication(String clientId, String tppId, String applicationId);

	public ResetSecretResponse resetClientSecret(String clientId, String applicationId);

	public String getOBPublicKey(String obJWKSEndpoint);
	
	public void importCAinPF(X509Certificate cert);
	
	public void verifyToken(String token) throws InvalidKeySpecException, NoSuchAlgorithmException, ParseException, JOSEException; //NOSONAR

	public String verifySubjectDN(OBClientRegistration1 obClientRegistration1, String jwksEndpoint);
}
