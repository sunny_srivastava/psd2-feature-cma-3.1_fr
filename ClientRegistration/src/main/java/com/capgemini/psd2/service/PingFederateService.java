package com.capgemini.psd2.service;

import java.io.IOException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import com.capgemini.tpp.ldap.client.model.ClientModel;
import com.capgemini.tpp.ssa.model.SSAModel;

public interface PingFederateService {

	public ClientModel registerTPPApplicationinPF(SSAModel ssaDataElements, String clientSecret);

	public void deletePFAppRegisteration(String clientId);

	public ClientModel updateTPPApplicationinPF(ClientModel clientModel, String clientId, String clientSecret);

	public ClientModel fetchClientDetailsByIdFromPF(String clientId);
	
	public void addTrustedCAinPF(X509Certificate cert) throws CertificateException, IOException; //NOSONAR
	
	public void replicatePFConfiguration();

	public ClientModel fetchTPPApplicationFromPF(ClientModel clientModel, String clientId, String clientSecret);
	
}
