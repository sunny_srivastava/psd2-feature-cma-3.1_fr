package com.capgemini.psd2.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.retry.annotation.Recover;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Service;

import com.capgemini.psd2.config.MuleClientRegConfig;
import com.capgemini.psd2.enums.ClientRegistrationErrorCodeEnum;
import com.capgemini.psd2.exception.ClientRegistrationException;
import com.capgemini.psd2.helper.MuleClientRegistrationHelper;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.RestClientSync;
import com.capgemini.psd2.service.MuleService;
import com.capgemini.psd2.utilities.NullCheckUtils;
import com.capgemini.tpp.dtos.ApiDetail;
import com.capgemini.tpp.ldap.client.model.ClientModel;
import com.capgemini.tpp.muleapi.schema.Api;
import com.capgemini.tpp.muleapi.schema.ApisResponse;
import com.capgemini.tpp.muleapi.schema.Application;
import com.capgemini.tpp.muleapi.schema.LoginRequest;
import com.capgemini.tpp.muleapi.schema.LoginResponse;
import com.capgemini.tpp.muleapi.schema.NewApplicationMuleApiRequest;
import com.capgemini.tpp.muleapi.schema.NewApplicationMuleApiResponse;
import com.capgemini.tpp.muleapi.schema.NewContractsRequest;
import com.capgemini.tpp.muleapi.schema.NewContractsResponse;
import com.capgemini.tpp.muleapi.schema.ResetSecretResponse;
import com.capgemini.tpp.muleapi.schema.Tiers;
import com.capgemini.tpp.muleapi.schema.Version;
import com.capgemini.tpp.ssa.model.SSAModel;

@Service
public class MuleServiceImpl implements MuleService {

	@Autowired
	@Qualifier("tppPortalRestClient")
	private RestClientSync restClientSync;

	@Autowired
	private MuleClientRegConfig muleClientRegConfig;
	

	/*
	 * Mule login Api Post call to get Access Token from Mule (can be compared
	 * similar to login into Mule)
	 * 
	 * @see
	 * com.capgemini.tppportal.services.MuleService#gatewayMuleAuthentication()
	 */
	@Override
	@Retryable(value = { Exception.class }, maxAttempts = 1)
	public LoginResponse gatewayMuleAuthentication() {
		LoginRequest loginRequest = MuleClientRegistrationHelper.populateLoginRequest(muleClientRegConfig);
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(muleClientRegConfig.getLoginUrl());
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(new MediaType("application", "json"));
		return restClientSync.callForPost(requestInfo, loginRequest, LoginResponse.class, headers);
	}

	/**
	 * 
	 * This Recovery method is called in case of failure of
	 * gatewayMuleAuthentication() method even after the maxAttempt is
	 * completed. and custom Exception is thrown
	 * 
	 * @param Exception
	 * @throws ClientRegistrationException
	 */
	@Recover
	public LoginResponse recoverGatewayMuleAuthentication(Exception e) {
		throw ClientRegistrationException.populateDCRException(e.getMessage(), ClientRegistrationErrorCodeEnum.MULE_LOGIN_FAILURE);
	}

	/*
	 * The application is registered in Mule through an POST Api call The Body
	 * of the request is NewApplicationMuleApiRequest object which is populated
	 * from SSAToken values.
	 * 
	 * @see
	 * com.capgemini.tppportal.services.MuleService#registerTPPApplicationInMule
	 * (com.capgemini.tppportal.ldap.client.model.ClientModel,
	 * com.capgemini.tppportal.ssa.model.SSAModel, java.lang.String,
	 * com.capgemini.tppportal.muleapi.schema.LoginResponse)
	 */
	@Override
	@Retryable(value = { Exception.class }, maxAttempts = 1)
	public NewApplicationMuleApiResponse registerTPPApplicationInMule(ClientModel clientModel, SSAModel ssaDataElements,
			String clientSecret, LoginResponse loginResponse) {
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(muleClientRegConfig.getApplicationUrl());
		NewApplicationMuleApiRequest newApplicationMuleApiRequest = MuleClientRegistrationHelper
				.populateApplicationMuleApiRequest(ssaDataElements, clientSecret);
		HttpHeaders headers = populateHttpHeaders(loginResponse);
		return restClientSync.callForPost(requestInfo, newApplicationMuleApiRequest,
				NewApplicationMuleApiResponse.class, headers);
	}

	/**
	 * This Recovery method is called in case of failure of
	 * registerTPPApplicationInMule() method even after the maxAttempt is
	 * completed.
	 * 
	 * @param e
	 *            throws PortalException
	 */
	@Recover
	public NewApplicationMuleApiResponse recoverRegisterTPPApplicationInMule(Exception e) {
		throw ClientRegistrationException.populateDCRException(e.getMessage(),
				ClientRegistrationErrorCodeEnum.MULE_API_REGISTERATION_CALL_FAILURE);
	}

	/*
	 * To get the List of All the Apis of the Organisation GET Api call is
	 * executed
	 * 
	 * @see
	 * com.capgemini.tppportal.services.MuleService#fetchListOfAllOrgApis(com.
	 * capgemini.tppportal.muleapi.schema.LoginResponse) retruns ApisResponse
	 */
	@Override
	@Retryable(value = { Exception.class }, maxAttempts = 1)
	public ApisResponse fetchListOfAllOrgApis(LoginResponse loginResponse) {
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(muleClientRegConfig.getApiUrl());
		HttpHeaders headers = populateHttpHeaders(loginResponse);
		return restClientSync.callForGet(requestInfo, ApisResponse.class, headers);
	}

	/**
	 * This Recovery method is called in case of failure of
	 * recoverFetchListOfAllOrgApis() method even after the maxAttempt is
	 * completed.
	 * 
	 * @param Exception
	 *            throws PortalException
	 */
	@Recover
	public ApisResponse recoverFetchListOfAllOrgApis(Exception e) {
		throw ClientRegistrationException.populateDCRException(e.getMessage(),
				ClientRegistrationErrorCodeEnum.MULE_FETCH_LIST_OF_ALL_ORG_APIS_FAILURE);
	}

	/*
	 * To get the List of Tiers for each Api Versions Validation: Tier should
	 * not be more than 1
	 * 
	 * @see com.capgemini.tppportal.services.MuleService#
	 * fetchListOfTiersForApiVersions(int, int,
	 * com.capgemini.tppportal.muleapi.schema.LoginResponse)
	 */
	@Override
	@Retryable(value = { Exception.class }, maxAttempts = 1)
	public Tiers fetchListOfTiersForApiVersions(int apiId, int apiVersionId, LoginResponse loginResponse) {
		RequestInfo requestInfo = new RequestInfo();
		HttpHeaders headers = populateHttpHeaders(loginResponse);
		String tierUrl = muleClientRegConfig.getTierUrl();
		tierUrl = tierUrl.replaceAll("\\{apiId\\}", Integer.toString(apiId));
		tierUrl = tierUrl.replaceAll("\\{apiVersionId\\}", Integer.toString(apiVersionId));
		requestInfo.setUrl(tierUrl);
		Tiers tier = restClientSync.callForGet(requestInfo, Tiers.class, headers);
		if (tier.getTiers().size() > 1) {
			throw ClientRegistrationException.populateDCRException(ClientRegistrationErrorCodeEnum.TIER_MORE_THAN_ONE);
		}
		return tier;
	}

	/**
	 * This Recovery method is called in case of failure of
	 * fetchListOfTiersForApiVersions() method even after the maxAttempt is
	 * completed.
	 * 
	 * @param Exception
	 *            throws PortalException
	 */
	@Recover
	public Tiers recoverFetchListOfTiersForApiVersions(Exception e) {
		throw ClientRegistrationException.populateDCRException(e.getMessage(),
				ClientRegistrationErrorCodeEnum.MULE_FETCH_LIST_OF_TIERS_FOR_API_VERSIONS_FAILURE);
	}

	/*
	 * Application Contracts is created through POST api call for each version
	 * of api if tier for that api version is 0 or 1.
	 * 
	 * Body of the POST call is NewContractsRequest object
	 * 
	 * @see
	 * com.capgemini.tppportal.services.MuleService#doApplicationContract(com.
	 * capgemini.tppportal.muleapi.schema.ApisResponse,
	 * com.capgemini.tppportal.muleapi.schema.NewApplicationMuleApiResponse,
	 * com.capgemini.tppportal.muleapi.schema.LoginResponse)
	 */
	@Override
	@Retryable(value = { Exception.class }, maxAttempts = 1)
	public void doApplicationContract(ApisResponse apisResponse, Integer applicationId, LoginResponse loginResponse) {

		NewContractsRequest newContractRequest;
		Map<Integer, Integer> apiVersionIdTierIdMap;

		if (apisResponse != null)
			apiVersionIdTierIdMap = getApiVersionIdTierIdMapAllApis(apisResponse, applicationId, loginResponse);
		else
			apiVersionIdTierIdMap = getApiVersionIdTierIdMapSelectApis(applicationId, loginResponse);

		RequestInfo requestInfo = new RequestInfo();
		String apiContractUrl = muleClientRegConfig.getApiContractUrl();
		apiContractUrl = apiContractUrl.replaceAll("\\{applicationId\\}", String.valueOf(applicationId));
		requestInfo.setUrl(apiContractUrl);

		HttpHeaders headers = populateHttpHeaders(loginResponse);

		for (Map.Entry<Integer, Integer> e : apiVersionIdTierIdMap.entrySet()) {
			Integer versionId = e.getKey();
			Integer tierId = e.getValue();
			newContractRequest = MuleClientRegistrationHelper.populateNewContractRequest(applicationId, versionId,
					tierId);
			restClientSync.callForPost(requestInfo, newContractRequest, NewContractsResponse.class, headers);
		}
	}

	private Map<Integer, Integer> getApiVersionIdTierIdMapAllApis(ApisResponse apisResponse, Integer applicationId, LoginResponse loginResponse) { //NOSONAR

		List<Version> versionList;
		List<Api> apiList = apisResponse.getApis();
		Tiers tier;
		Map<Integer, Integer> apiVersionIdTierIdMap = new HashMap<>();

		for (Api api : apiList) {
			versionList = api.getVersions();
			if (versionList == null || versionList.isEmpty()) {
				continue;
			}
			for (Version version : versionList) {
				// tiers for each api version is fetched
				tier = fetchListOfTiersForApiVersions(api.getId(), version.getId(), loginResponse);
				if (tier.getTotal() == 1)
					apiVersionIdTierIdMap.put(version.getId(), tier.getTiers().get(0).getId());
				else
					apiVersionIdTierIdMap.put(version.getId(), null);
			}
		}
		return apiVersionIdTierIdMap;
	}

	private Map<Integer, Integer> getApiVersionIdTierIdMapSelectApis(Integer applicationId, LoginResponse loginResponse) { //NOSONAR
		Tiers tier;
		Map<Integer, Integer> apiVersionIdTierIdMap = new HashMap<>();
		Map<String, ApiDetail> muleContractApiDetails = muleClientRegConfig.getMuleContractApi();
		if(muleContractApiDetails==null || muleContractApiDetails.isEmpty())
			throw ClientRegistrationException.populateDCRException(ClientRegistrationErrorCodeEnum.CONTRACT_API_FLAG_INVALID);
		
		for (Map.Entry<String, ApiDetail> contractApiDetail : muleContractApiDetails.entrySet()) {
			ApiDetail apiDetail = contractApiDetail.getValue();

			tier = fetchListOfTiersForApiVersions(apiDetail.getApiId(), apiDetail.getVersionId(), loginResponse);
			if (tier.getTotal() == 1)
				apiVersionIdTierIdMap.put(apiDetail.getVersionId(), tier.getTiers().get(0).getId());
			else
				apiVersionIdTierIdMap.put(apiDetail.getVersionId(), null);
		}
		return apiVersionIdTierIdMap;
	}

	/**
	 * This Recovery method is called in case of failure of
	 * doApplicationContract() method even after the maxAttempt is completed.
	 * 
	 * @param Exception
	 *            throws PortalException
	 */
	@Recover
	public List<NewContractsResponse> recoverDoApplicationContract(Exception e) {
		throw ClientRegistrationException.populateDCRException(e.getMessage(),
				ClientRegistrationErrorCodeEnum.MULE_CREATE_APP_CONTRACT_FAILURE);
	}

	/*
	 * The Application is Deleted from Mule and all the Contracts created with
	 * reference to this application are deleted
	 * 
	 * @see com.capgemini.tppportal.services.MuleService#
	 * deleteTppAppRegisterationInMule(com.capgemini.tppportal.muleapi.schema.
	 * LoginResponse, java.lang.String)
	 */
	@Override
	@Retryable(value = { Exception.class }, maxAttempts = 1)
	public void deleteTppAppRegisterationInMule(LoginResponse loginResponse, String applicationId) {
		RequestInfo requestInfo = new RequestInfo();
		String muleRollBackUrl = muleClientRegConfig.getApplicationRollBackUrl();
		muleRollBackUrl = muleRollBackUrl.replaceAll("\\{applicationId\\}", applicationId);
		requestInfo.setUrl(muleRollBackUrl);
		HttpHeaders headers = populateHttpHeaders(loginResponse);
		restClientSync.callForDelete(requestInfo, Void.class, headers);
	}

	/**
	 * This Recovery method is called in case of failure of
	 * deleteTppAppRegisterationInMule() method even after the maxAttempt is
	 * completed.
	 * 
	 * @param Exception
	 *            throws PortalException
	 */
	@Recover
	public void recoverDeleteMuleAppRegisteration(Exception e) {
		throw ClientRegistrationException.populateDCRException(e.getMessage(),
				ClientRegistrationErrorCodeEnum.MULE_API_REGISTERATION_DELETE_CALL_FAILURE);
	}

	/**
	 * Fetch client secret for requested application using mule login response.
	 * 
	 * @param loginResponse
	 * @param applicationId
	 * @return {@link Application}
	 */
	@Override
	public Application fetchClientSecret(LoginResponse loginResponse, String applicationId) {
		RequestInfo requestInfo = new RequestInfo();
		StringBuilder url = new StringBuilder();
		url.append(muleClientRegConfig.getShowSecretUrl());
		String requestUrl = url.toString().replaceAll("\\{applicationId\\}", applicationId);
		requestInfo.setUrl(requestUrl);
		HttpHeaders headers = populateHttpHeaders(loginResponse);
		return restClientSync.callForGet(requestInfo, Application.class, headers);
	}

	/**
	 * Resets client secret for requested application.
	 * 
	 * @param loginResponse
	 * @param applicationId
	 * @return {@link ResetSecretResponse)
	 */
	@Override
	public ResetSecretResponse resetClientSecret(LoginResponse loginResponse, String applicationId) {
		RequestInfo requestInfo = new RequestInfo();
		StringBuilder url = new StringBuilder();
		url.append(muleClientRegConfig.getResetSecretUrl());
		String requestUrl = url.toString().replaceAll("\\{applicationId\\}", applicationId);
		requestInfo.setUrl(requestUrl);
		HttpHeaders headers = populateHttpHeaders(loginResponse);
		return restClientSync.callForPost(requestInfo, null, ResetSecretResponse.class, headers);
	}

	/**
	 * Headers Required for Mule Api calls are set into HttpHeaders in this
	 * method
	 * 
	 * @param loginResponse
	 * @return
	 */
	private HttpHeaders populateHttpHeaders(LoginResponse loginResponse) {
		HttpHeaders requestHeaders = new HttpHeaders();
		requestHeaders.setContentType(new MediaType("application", "json"));
		if (!NullCheckUtils.isNullOrEmpty(loginResponse)
				&& !NullCheckUtils.isNullOrEmpty(loginResponse.getAccessToken())) {
			requestHeaders.add("Authorization", "Bearer ".concat(loginResponse.getAccessToken()));
		}
		return requestHeaders;
	}
}