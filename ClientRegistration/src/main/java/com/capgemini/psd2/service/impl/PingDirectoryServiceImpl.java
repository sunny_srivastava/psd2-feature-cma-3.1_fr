/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.service.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.naming.Name;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.BasicAttribute;
import javax.naming.directory.BasicAttributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.ModificationItem;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ldap.core.AttributesMapper;
import org.springframework.ldap.core.DirContextAdapter;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.query.LdapQuery;
import org.springframework.ldap.query.LdapQueryBuilder;
import org.springframework.stereotype.Service;

import com.capgemini.psd2.config.PortalLdapConfiguration;
import com.capgemini.psd2.constant.ClientRegistarionConstants;
import com.capgemini.psd2.constants.LdapConstants;
import com.capgemini.psd2.enums.ClientRegistrationErrorCodeEnum;
import com.capgemini.psd2.exception.ClientRegistrationException;
import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.helper.TppSoftwareHelper;
import com.capgemini.psd2.jwt.JWTReader;
import com.capgemini.psd2.logger.LoggerUtils;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.service.PingDirectoryService;
import com.capgemini.psd2.utilities.NullCheckUtils;
import com.capgemini.tpp.ldap.client.model.ClientModel;
import com.capgemini.tpp.ldap.client.model.TPPApplication;
import com.capgemini.tpp.ssa.model.Authorisation;
import com.capgemini.tpp.ssa.model.OrganisationCompetentAuthorityClaims;
import com.capgemini.tpp.ssa.model.SSAModel;


/**
 * The Class PingDirectoryServiceImpl.
 */

@Service
public class PingDirectoryServiceImpl implements PingDirectoryService {
	private static final Logger LOGGER = LoggerFactory.getLogger(PingDirectoryServiceImpl.class);
	@Autowired
	private LdapTemplate ldapTemplate;

	@Autowired
	private PortalLdapConfiguration portalLdapConfiguration;
	

	private static final String ABSOLUTE_METHOD_NAME = "com.capgemini.psd2.service.impl.PingDirectoryServiceImpl.registerOrUpdateTPPOrg()"; 

	@Autowired
	private LoggerUtils loggerUtils; 

	@Autowired
	RequestHeaderAttributes attr;


	/*
	 * checks if the Tpp Organisation already existed in PD. If yes then it
	 * returns TRUE
	 * 
	 * ou=tpp,ou=groups,dc=boi,dc=co.uk,dc=bank,dc=com
	 * 
	 * @see
	 * com.capgemini.tppportal.services.PingDirectoryService#isTPPExist(java.
	 * lang.String)
	 */
	@Override
	public boolean isTPPExist(String tppOrgId) {
		boolean isTPPExist;
		if (tppOrgId == null || tppOrgId.isEmpty()) {
			throw ClientRegistrationException.populateDCRException(ClientRegistrationErrorCodeEnum.NO_TPP_NAME_PROVIDED);
		}

		LdapQuery ldapQuery = TppSoftwareHelper.getLdapQuery(portalLdapConfiguration.getTppGroupBasedn(attr.getTenantId()), tppOrgId);
		List<Object> tppDetails = null;
		try {
			tppDetails = getLdapSearchResult(ldapQuery);
		} catch (org.springframework.ldap.NamingException ex) {
			LOGGER.info("Exception Occurred in isTPPExist(): "+ex);
			throw ClientRegistrationException.populateDCRException(ex.getMessage(),
					ClientRegistrationErrorCodeEnum.PD_IS_TPP_EXIST_CALL_FAILED);
		}
		isTPPExist = tppDetails != null && !tppDetails.isEmpty();
		return isTPPExist;
	}

	/*
	 * If Tpp Exists then it updates the TPP Org Details in PD from SSA Token
	 * else it inserts the Tpp Org with its details
	 * 
	 * ou=tpp,ou=groups,dc=boi,dc=co.uk,dc=bank,dc=com
	 * 
	 * @see com.capgemini.tppportal.services.PingDirectoryService#
	 * registerOrUpdateTPPOrg(java.lang.String,
	 * com.capgemini.tppportal.ssa.model.SSAModel, boolean)
	 */
	@Override
	public void registerOrUpdateTPPOrg(String tppOrgId, SSAModel ssaElements, boolean add) {
		
		LOGGER.info("{\"Enter\":\"{}\",\"{}\",\" TppAddFlag\":\"{}\",\" Roles\":\"{}\",\" OrgId\":\"{}\",\" OrgName\":\"{}\" }",
				ABSOLUTE_METHOD_NAME,
				loggerUtils.populateLoggerData("registerOrUpdateTPPOrg"),add,ssaElements.getSoftware_roles(),ssaElements.getOrg_id(), ssaElements.getOrg_name()); 
		String rolesStr;
		Set<String> roles = new HashSet<>();

		OrganisationCompetentAuthorityClaims authorityClaims = ssaElements.getOrganisation_competent_authority_claims();
		List<Authorisation> authorisations = authorityClaims != null ? authorityClaims.getAuthorisations() : null;

		if (authorisations != null && !authorisations.isEmpty()) {
			for (Authorisation authorisation : authorisations) {
				if (authorisation.getRoles() != null && !authorisation.getRoles().isEmpty()) {
					roles.addAll(authorisation.getRoles());
				}
			}
		}

		String tppDN = portalLdapConfiguration.getTppGroupBasedn(attr.getTenantId());
		String tppCN = ClientRegistarionConstants.CN + tppOrgId + ClientRegistarionConstants.COMMA + tppDN;

		DirContextAdapter context = new DirContextAdapter(tppCN);
		context.setAttributeValues("objectclass", new String[] { "groupOfUniqueNames", "top", "tppGroups" });
		context.setAttributeValue("cn", ssaElements.getOrg_id());
		context.setAttributeValue("o", ssaElements.getOrg_name());
		context.setAttributeValue("x-block", "false");
		if (!roles.isEmpty()) {
			rolesStr = String.join(ClientRegistarionConstants.COMMA, roles);
			context.setAttributeValue("x-roles", rolesStr);
		}else{
			throw ClientRegistrationException
			.populateDCRException(ClientRegistrationErrorCodeEnum.ORG_COMPETENT_AUTH_CLAIMS_MISSING);
		}

		try {
			// if tpp does not exist
			if (add) {
				ldapTemplate.bind(context);
			} else {
				context.setUpdateMode(Boolean.TRUE);
				ldapTemplate.modifyAttributes(context);
			}
		} catch (org.springframework.ldap.NamingException e) {
			LOGGER.info("Exception Occurred in registerOrUpdateTPPOrg(): "+e); //NOSONAR
			throw ClientRegistrationException.populateDCRException(e.getMessage(),
					ClientRegistrationErrorCodeEnum.PD_TPP_ORG_UPDATE_OR_INSERT_FAILURE);
		}
	}

	/*
	 * Deletes the Tpp Organisation from PD from baseDn defined in the yml file
	 * 
	 * ou=tpp,ou=groups,dc=boi,dc=co.uk,dc=bank,dc=com
	 * 
	 * @see com.capgemini.tppportal.services.PingDirectoryService#
	 * deleteTppOrgRegistration(java.lang.String)
	 */
	@Override
	public void deleteTppOrgRegistration(String tppOrgId) {
		String tppDN = portalLdapConfiguration.getTppGroupBasedn(attr.getTenantId());
		String tppCN = ClientRegistarionConstants.CN + tppOrgId + ClientRegistarionConstants.COMMA + tppDN;
		try {
			ldapTemplate.unbind(tppCN);
		} catch (org.springframework.ldap.NamingException e) {
			LOGGER.info("Exception Occurred in deleteTppOrgRegistration(): "+e); //NOSONAR
			throw ClientRegistrationException.populateDCRException(e.getMessage(),
					ClientRegistrationErrorCodeEnum.PD_TPP_ORG_UPDATE_OR_INSERT_DELETE_FAILURE);
		}
	}

	/*
	 * Removed this method from rollback after discussion with Mohit
	 * 
	 * If the Tpp details were updated then at the time of Rollback the Tpp
	 * details needs to be set to their old values.
	 * 
	 * For that we first get a uniqueMember if uniqueMember is present then we
	 * take the clientId and search for the token of that clientId in
	 * clientAppMapping then update the Tpp details as per that token
	 * 
	 * else Nothing will happen
	 * 
	 * @see com.capgemini.tppportal.services.PingDirectoryService#
	 * deleteTppOrgUpdation(java.lang.String)
	 */
	@Override
	public void deleteTppOrgUpdation(String tppOrgId) {
		List<Object> tppDetails = null;
		List<String> uniqueMemberString = null;
		String clientId = null;
		LdapQuery ldapQuery = TppSoftwareHelper.getLdapQuery(portalLdapConfiguration.getTppGroupBasedn(attr.getTenantId()), tppOrgId);
		try {
			tppDetails = getLdapSearchResult(ldapQuery);

			if (!NullCheckUtils.isNullOrEmpty(tppDetails) && !tppDetails.isEmpty()) {
				uniqueMemberString = TppSoftwareHelper.getBasicAttributes((BasicAttributes) tppDetails.get(0),
						LdapConstants.UNIQUE_MEMBER);

				if (!uniqueMemberString.isEmpty()) {
					if (uniqueMemberString.get(0).contains(portalLdapConfiguration.getTppApplicationBasedn())) {
						clientId = TppSoftwareHelper.getObjectString(uniqueMemberString.get(0),
								ClientRegistarionConstants.CLIENTID, ClientRegistarionConstants.COMMA, ClientRegistarionConstants.EQUAS);
					}

					String ssaToken = getAttributeValuesFromClientAppMapping(clientId, "ssaToken");
					registerOrUpdateTPPOrg(tppOrgId, JWTReader.decodeTokenIntoSSAModel(ssaToken), Boolean.FALSE);
				}
			} else {
				throw ClientRegistrationException
						.populateDCRException(ClientRegistrationErrorCodeEnum.PD_TPP_ORG_UPDATE_OR_INSERT_DELETE_FAILURE);
			}
		} catch (org.springframework.ldap.NamingException | NamingException | ClientRegistrationException e) {
			LOGGER.info("Exception Occurred in deleteTppOrgUpdation(): "+e); //NOSONAR
			throw ClientRegistrationException.populateDCRException(e.getMessage(),
					ClientRegistrationErrorCodeEnum.PD_TPP_ORG_UPDATE_OR_INSERT_DELETE_FAILURE);
		}
	}

	/*
	 * Common Method to fetch attributes from PD at baseDN:
	 * ou=clientAppMapping,ou=groups,dc=boi,dc=co.uk,dc=bank,dc=com
	 * 
	 * @see com.capgemini.tppportal.services.PingDirectoryService#
	 * getAttributeValuesFromClientAppMapping(java.lang.String,
	 * java.lang.String)
	 */
	@Override
	public String getAttributeValuesFromClientAppMapping(String clientId, String attribute) throws NamingException {
		String tppCN = ClientRegistarionConstants.CN + clientId + ClientRegistarionConstants.COMMA
				+ portalLdapConfiguration.getClientAppgroupBasedn(attr.getTenantId());
		DirContextAdapter dx = (DirContextAdapter) ldapTemplate.lookup(tppCN);
		Attribute attrs = dx.getAttributes().get(attribute);
		return (String) attrs.get();
	}

	/*
	 * This method adds a unique member i.e Client/TppApplication to PD at
	 * [ou=tpp,ou=groups,dc=boi,dc=co.uk,dc=bank,dc=com]
	 * 
	 * @see com.capgemini.tppportal.services.PingDirectoryService#
	 * associateTPPApplicationWithTPP(java.lang.String,
	 * com.capgemini.tppportal.ldap.client.model.ClientModel)
	 */
	@Override
	public void associateTPPApplicationWithTPP(String tppOrgId, ClientModel clientModel) {
		List<ModificationItem> mods = new ArrayList<>();

		String tppDN = portalLdapConfiguration.getTppGroupBasedn(attr.getTenantId());
		String tppCN = ClientRegistarionConstants.CN + tppOrgId + ClientRegistarionConstants.COMMA + tppDN;
		String applicationCN = portalLdapConfiguration.getClientdn() + clientModel.getClientId() + ClientRegistarionConstants.COMMA
				+ portalLdapConfiguration.getTppApplicationBasedn();
		Name tppBaseDn = TppSoftwareHelper.buildBaseDN(tppCN);
		try {
			DirContextAdapter dx = (DirContextAdapter) ldapTemplate.lookup(tppBaseDn);

			if (TppSoftwareHelper.isAttributeExist(dx, LdapConstants.UNIQUE_MEMBER)) {
				dx.getAttributes().get(LdapConstants.UNIQUE_MEMBER).add(applicationCN);
				dx.setUpdateMode(Boolean.FALSE);
				ldapTemplate.rebind(dx);
			} else {
				mods.add(new ModificationItem(DirContext.ADD_ATTRIBUTE,
						new BasicAttribute(LdapConstants.UNIQUE_MEMBER, applicationCN)));
				ldapTemplate.modifyAttributes(tppBaseDn, mods.toArray(new ModificationItem[mods.size()]));
			}
		} catch (org.springframework.ldap.NamingException e) {
			LOGGER.info("Exception Occurred in associateTPPApplicationWithTPP(): "+e); //NOSONAR
			throw ClientRegistrationException.populateDCRException(e.getMessage(),
					ClientRegistrationErrorCodeEnum.PD_TPP_APP_ASSOCIATION_FAILURE);
		}
	}
	

	public List<Object> fetchTppAppMappingForClient(String clientId) {
		
		String tppCN = ClientRegistarionConstants.CN + clientId + ClientRegistarionConstants.COMMA
				+ portalLdapConfiguration.getClientAppgroupBasedn(attr.getTenantId());
		
		LdapQuery ldapQuery = LdapQueryBuilder.query().attributes("*", "+").base(tppCN)
				.filter("(cn=" + clientId + ")");
		List<Object> tppAppDetails =null;
		try {
	    tppAppDetails = ldapTemplate.search(ldapQuery, new AttributesMapper<Object>() {
			@Override
			public Object mapFromAttributes(Attributes attrs){
				return attrs;
			}
		});
			} catch (Exception e) {
			LOGGER.info("Exception Occurred in fetchTppAppMappingForClient(): "+e);
			throw ClientRegistrationException.populateDCRException(e.getMessage(),ClientRegistrationErrorCodeEnum.NO_CLIENT_APP_DATA_FOUND);
		}
		if (tppAppDetails == null || tppAppDetails.isEmpty()) {
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.NO_CLIENT_APP_DATA_FOUND);
		}
		LOGGER.info("PingDirectoryServiceImpl.fetchTppAppMappingForClient() : tppAppDetails from LDAP");
		return tppAppDetails;
	} 	

	/*
	 * This method removes the uniqueMember i.e the client/Application from PD
	 * at ou=tpp,ou=groups,dc=boi,dc=co.uk,dc=bank,dc=com
	 * 
	 * @see com.capgemini.tppportal.services.PingDirectoryService#
	 * deleteTPPApplicationAssociationWithTPP(java.lang.String,
	 * java.lang.String)
	 */
	@Override
	public void deleteTPPApplicationAssociationWithTPP(String tppOrgId, String clientId) {
		String tppDN = portalLdapConfiguration.getTppGroupBasedn(attr.getTenantId());
		String tppCN = ClientRegistarionConstants.CN + tppOrgId + ClientRegistarionConstants.COMMA + tppDN;
		String applicationCN = portalLdapConfiguration.getClientdn() + clientId + ClientRegistarionConstants.COMMA
				+ portalLdapConfiguration.getTppApplicationBasedn();
		Name tppBaseDn = TppSoftwareHelper.buildBaseDN(tppCN);
		List<ModificationItem> mods = new ArrayList<>();
		try {
			DirContextAdapter dx = (DirContextAdapter) ldapTemplate.lookup(tppBaseDn);

			if (dx.attributeExists(LdapConstants.UNIQUE_MEMBER)
					&& dx.getAttributes().get(LdapConstants.UNIQUE_MEMBER).contains(applicationCN)) {
				mods.add(new ModificationItem(DirContext.REMOVE_ATTRIBUTE,
						new BasicAttribute(LdapConstants.UNIQUE_MEMBER, applicationCN)));

			}
			ldapTemplate.modifyAttributes(tppBaseDn, mods.toArray(new ModificationItem[mods.size()]));
		} catch (org.springframework.ldap.NamingException e) {
			LOGGER.info("Exception Occurred in deleteTPPApplicationAssociationWithTPP(): "+e); //NOSONAR
			throw ClientRegistrationException.populateDCRException(e.getMessage(),
					ClientRegistrationErrorCodeEnum.PD_TPP_APP_ASSOCIATION_DELETE_FAILURE);
		}
	}

	/*
	 * Put an entry of the client/Tpp Application into PD at
	 * ou=clientAppMapping,ou=groups,dc=boi,dc=co.uk,dc=bank,dc=com with the
	 * details from SSA Token
	 * 
	 * @see
	 * com.capgemini.tppportal.services.PingDirectoryService#mapClientApp(java.
	 * lang.String, java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public void mapClientApp(String clientId, String appId, String tppOrgId, String ssaToken,String softwareJwks, String publicKey) {
		String tppCN = ClientRegistarionConstants.CN + clientId + ClientRegistarionConstants.COMMA
				+ portalLdapConfiguration.getClientAppgroupBasedn(attr.getTenantId());

		String tppDN = portalLdapConfiguration.getTppGroupBasedn(attr.getTenantId());
		String clientTppInfoUniqueMember = ClientRegistarionConstants.CN + tppOrgId + ClientRegistarionConstants.COMMA + tppDN;

		DirContextAdapter context = new DirContextAdapter(tppCN);

		context.setAttributeValues("objectclass", new String[] { "appMappingsGroup", "top", "groupOfUniqueNames" });
		context.setAttributeValue("cn", clientId);
		context.setAttributeValue("uniqueMember", clientTppInfoUniqueMember);
		context.setAttributeValue("appId", appId);
		context.setAttributeValue("ssaToken", ssaToken);
		context.setAttributeValue("status", "Approved");
		context.setAttributeValue("softwareJwksEndpoint",softwareJwks);
		if(null!=publicKey){
		context.setAttributeValue("publicKey", publicKey);
		}
		try {
			ldapTemplate.bind(context);
		} catch (org.springframework.ldap.NamingException e) {
			LOGGER.info("Exception Occurred in mapClientApp(): "+e); //NOSONAR
			throw ClientRegistrationException.populateDCRException(e.getMessage(),
					ClientRegistrationErrorCodeEnum.PD_CLIENT_APP_MAPPING_FAILURE);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.capgemini.tppportal.services.PingDirectoryService#
	 * fetchListOfTPPApplications(java.lang.String)
	 */
	@Override
	public List<TPPApplication> fetchListOfTPPApplications(String tppOrgId) {
		List<Object> tppDetails = null;
		List<Object> tppApplication = null;
		List<String> uniqueMemberString = null;
		List<TPPApplication> tppApplicationList = new ArrayList<>();

		// Ldap query to retrieve DN from ou=tpp in ou=groups
		LdapQuery ldapQuery = TppSoftwareHelper.getLdapQuery(portalLdapConfiguration.getTppGroupBasedn(attr.getTenantId()), tppOrgId);
		try {

			tppDetails = getLdapSearchResult(ldapQuery);

			if (tppDetails == null || tppDetails.isEmpty()) {
				return tppApplicationList;
			}

			uniqueMemberString = TppSoftwareHelper.getBasicAttributes((BasicAttributes) tppDetails.get(0),
					LdapConstants.UNIQUE_MEMBER);
			if (!NullCheckUtils.isNullOrEmpty(uniqueMemberString)) {
				for (String uniqueMemberDn : uniqueMemberString) {

					// check if uniqueMemberDn contains the same basedn as
					// TppApplicationBasedn or not
					if (uniqueMemberDn.contains(portalLdapConfiguration.getTppApplicationBasedn())) {

						// query for obtaining the basedn for clientAppMapping
						// and appending the clientId
						String clientId = TppSoftwareHelper.getObjectString(uniqueMemberDn, ClientRegistarionConstants.CLIENTID,
								ClientRegistarionConstants.COMMA, ClientRegistarionConstants.EQUAS);
						ldapQuery = TppSoftwareHelper.getLdapQuery(portalLdapConfiguration.getClientAppgroupBasedn(attr.getTenantId()),
								clientId);
						tppApplication = getLdapSearchResult(ldapQuery);
						tppApplicationList.add(
								TppSoftwareHelper.populateTPPApplication(((BasicAttributes) tppApplication.get(0))));	
					}
				}
			}
		} catch (org.springframework.ldap.NamingException | ClientRegistrationException e) {
			LOGGER.info("Exception Occurred in fetchListOfTPPApplications(): "+e); //NOSONAR
			throw ClientRegistrationException.populateDCRException(e.getMessage(),
					ClientRegistrationErrorCodeEnum.LDAP_ATTRIBUTE_NOT_FOUND_IN_TPP);
		}
		return tppApplicationList;
	}

	/*
	 * check the attribute x-block from
	 * ou=tpp,ou=groups,dc=boi,dc=co.uk,dc=bank,dc=com and sends its values
	 * 
	 * @see
	 * com.capgemini.tppportal.services.PingDirectoryService#isTppBlockEnabled(
	 * java.lang.String)
	 */
	@Override
	public Boolean isTppBlockEnabled(String tppOrgId) {
		List<Object> tppDetails = null;
		Boolean isBlocked = false;
		LdapQuery ldapQuery = TppSoftwareHelper.getLdapQuery(portalLdapConfiguration.getTppGroupBasedn(attr.getTenantId()), tppOrgId);
		try {
			tppDetails = getLdapSearchResult(ldapQuery);
		} catch (org.springframework.ldap.NamingException | ClientRegistrationException e) {
			LOGGER.info("Exception Occurred in isTppBlockEnabled(): "+e); //NOSONAR
			throw ClientRegistrationException.populateDCRException(e.getMessage(),
					ClientRegistrationErrorCodeEnum.PING_DIRECTORY_CALL_FAILED);
		}

		if (!NullCheckUtils.isNullOrEmpty(tppDetails) && tppDetails.size() == 1) {
			List<String> block = TppSoftwareHelper.getBasicAttributes((BasicAttributes) tppDetails.get(0),
					LdapConstants.XBLOCK);
			isBlocked = Boolean.valueOf(block.get(0).trim());

		}
		return isBlocked;

	}

	@Override
	public List<String> getClientID(String tppOrgId) {

		String clientId;
		List<String> clienIds = new ArrayList<>();
		List<Object> tppDetails = null;
		List<String> uniqueMemberString;

		LdapQuery ldapQuery = TppSoftwareHelper.getLdapQuery(portalLdapConfiguration.getTppGroupBasedn(attr.getTenantId()), tppOrgId);
		try {
			tppDetails = getLdapSearchResult(ldapQuery);
		} catch (org.springframework.ldap.NamingException | ClientRegistrationException e) {
			LOGGER.info("Exception Occurred in getClientID(): "+e); //NOSONAR
			throw ClientRegistrationException.populateDCRException(e.getMessage(),
					ClientRegistrationErrorCodeEnum.PING_DIRECTORY_CALL_FAILED);
		}

		if (tppDetails == null || tppDetails.isEmpty()) {
			return clienIds;
		}
		for (Object obj : tppDetails) {
			uniqueMemberString = TppSoftwareHelper.getBasicAttributes((BasicAttributes) obj,
					LdapConstants.UNIQUE_MEMBER);
			for (String uniqueMemberDn : uniqueMemberString) {
				if (uniqueMemberDn.contains(portalLdapConfiguration.getTppApplicationBasedn())) {
					clientId = TppSoftwareHelper.getObjectString(uniqueMemberDn, ClientRegistarionConstants.CLIENTID,
							ClientRegistarionConstants.COMMA, ClientRegistarionConstants.EQUAS);
					clienIds.add(clientId);
				}
			}
		}
		return clienIds;
	}

	/**
	 * 
	 * Common utility method to get execute the LdapQuery and get List of
	 * Objects
	 * 
	 * @param ldapQuery
	 * @return
	 */
	public List<Object> getLdapSearchResult(LdapQuery ldapQuery) {
		return ldapTemplate.search(ldapQuery, new AttributesMapper<Object>() {
			@Override
			public Object mapFromAttributes(Attributes attrs) throws NamingException {
				return attrs;
			}
		});
	}

	/*
	 * Delete an entry of the client/Tpp Application from PD at
	 * ou=clientAppMapping,ou=groups,dc=boi,dc=co.uk,dc=bank,dc=com
	 * 
	 * @see
	 * com.capgemini.tppportal.services.PingDirectoryService#deleteMapClientApp(
	 * java.lang.String)
	 */
	@Override
	public void deleteMapClientApp(String clientId) {
		String tppDN = portalLdapConfiguration.getClientAppgroupBasedn(attr.getTenantId());
		String tppCN = ClientRegistarionConstants.CN + clientId + ClientRegistarionConstants.COMMA + tppDN;
		try {
			ldapTemplate.unbind(tppCN);
		} catch (org.springframework.ldap.NamingException e) {
			LOGGER.info("Exception Occurred in deleteMapClientApp(): "+e); //NOSONAR
			throw ClientRegistrationException.populateDCRException(e.getMessage(),
					ClientRegistrationErrorCodeEnum.PD_TPP_ORG_UPDATE_OR_INSERT_DELETE_FAILURE);
		}
	}
	
	public void mapClientAppUpdate(String clientId, String tppOrgId ,String publicKey,String ssaToken) {
		/*String tppCN = ClientRegistarionConstants.CN + clientId + ClientRegistarionConstants.COMMA
				+ portalLdapConfiguration.getClientAppgroupBasedn(attr.getTenantId());*/
		String tppDN = portalLdapConfiguration.getClientAppgroupBasedn(attr.getTenantId());
		String tppCN = ClientRegistarionConstants.CN + clientId + ClientRegistarionConstants.COMMA + tppDN;
		DirContextAdapter context =  (DirContextAdapter) ldapTemplate.lookup(tppCN);
		   context.setUpdateMode(Boolean.TRUE);
		   context.setAttributeValue("publicKey", publicKey);
		   context.setAttributeValue("ssaToken", ssaToken);
		   ldapTemplate.modifyAttributes(context);
		   
		
	}
}