package com.capgemini.psd2.service.impl;

import java.io.IOException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.retry.annotation.Recover;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Service;

import com.capgemini.psd2.config.PFClientRegConfig;
import com.capgemini.psd2.enums.ClientRegistrationErrorCodeEnum;
import com.capgemini.psd2.exception.ClientRegistrationException;
import com.capgemini.psd2.helper.CertExtractor;
import com.capgemini.psd2.helper.TppSoftwareHelper;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.RestClientSync;
import com.capgemini.psd2.service.PingFederateService;
import com.capgemini.psd2.utilities.GenerateUniqueIdUtilities;
import com.capgemini.tpp.ldap.client.model.ClientModel;
import com.capgemini.tpp.muleapi.schema.Portal;
import com.capgemini.tpp.ssa.model.SSAModel;

/**
 * PingFederateServiceImpl.java is server to invoke the PingFederate api.
 * PingFederate API 1. Add Client Application 2. Delete Client Application 3.
 * Get Client Application By Id 4. Update the Client Application By Id
 * 
 * 
 */
@Service
public class PingFederateServiceImpl implements PingFederateService {
	private static final Logger LOGGER = LoggerFactory.getLogger(PingFederateServiceImpl.class);
	@Autowired
	@Qualifier("tppPortalRestClient")
	private RestClientSync restClientSync;

	@Autowired
	private PFClientRegConfig pfClientRegConfig;
	@Autowired
	private RequestHeaderAttributes requestHeaderAttributes;
	@Autowired
	private CertExtractor certExtractor;
	

	/**
	 * Application is registered in PF through a POST api call The Body of the
	 * Post call is populated from the values from SSAModel and client secret
	 * 
	 * If the call fails , retry attempts are made till the maxAttempts is
	 * reached
	 * 
	 * @see com.capgemini.tppportal.services.PingFederateService#
	 *      registerTPPApplicationinPF(com.capgemini.tpp.ssa.model.SSAModel,
	 *      java.lang.String)
	 */
	@Override
	@Retryable(value = { Exception.class }, maxAttempts = 1)
	public ClientModel registerTPPApplicationinPF(SSAModel ssaDataElements, String clientSecret) {

		ClientModel clientRegRequest = TppSoftwareHelper.populateClientRequest(ssaDataElements, pfClientRegConfig,
				clientSecret);

		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(pfClientRegConfig.getClientsUrl(requestHeaderAttributes.getTenantId()));

		HttpHeaders httpHeaders = populateHttpHeaders();
		return restClientSync.callForPost(requestInfo, clientRegRequest, ClientModel.class, httpHeaders);
	}

	/**
	 * If the maximum retry attempts are completed and the call still fails then
	 * the recover method is called
	 * 
	 * The return type of the recovery method should be same as that of the
	 * method it is call against
	 * 
	 * @param e
	 * @throws Portal
	 *             Exception
	 */
	@Recover
	public ClientModel recoverRegisterTPPApplicationinPF(Exception e) {
		throw ClientRegistrationException.populateDCRException(e.getMessage(), ClientRegistrationErrorCodeEnum.PF_REGISTER_API_CALL_FAILURE);
	}

	/**
	 * The Application is deleted from PF through a DELETE api call The URL is
	 * fetched from properties file and the client id is then replaced by the
	 * client of the application to be deleted
	 * 
	 * @see com.capgemini.tppportal.services.PingFederateService#
	 *      deletePFAppRegisteration(java.lang.String)
	 */
	@Override
	@Retryable(value = { Exception.class }, maxAttempts = 1)
	public void deletePFAppRegisteration(String clientId) {
		RequestInfo requestInfo = new RequestInfo();
		String pfRollBackUrl = pfClientRegConfig.getClientByIdUrl(requestHeaderAttributes.getTenantId());
		pfRollBackUrl = pfRollBackUrl.replaceAll("\\{clientId\\}", clientId);
		requestInfo.setUrl(pfRollBackUrl);
		HttpHeaders httpHeaders = populateHttpHeaders();
		restClientSync.callForDelete(requestInfo, Void.class, httpHeaders);
	}

	@Recover
	public void recoverDeletePFAppRegisteration(Exception e) {
		throw ClientRegistrationException.populateDCRException(e.getMessage(),
				ClientRegistrationErrorCodeEnum.PF_REGISTER_API_CALL_DELETE_FAILURE);
	}

	private HttpHeaders populateHttpHeaders() {
		HttpHeaders requestHeaders = new HttpHeaders();
		requestHeaders.setContentType(new MediaType("application", "json"));
		requestHeaders.add("X-XSRF-Header", GenerateUniqueIdUtilities.generateRandomUniqueID());
		requestHeaders.add("Authorization", "Basic " + TppSoftwareHelper.encodeCredentials(pfClientRegConfig));
		return requestHeaders;
	}

	/**
	 * The Application is update from PF through a PUT api call .The URL is
	 * fetched from properties file and the client id is then replaced by the
	 * client of the application to be update
	 * 
	 * @see com.capgemini.tppportal.services.PingFederateService#
	 *      updateTPPApplicationinPF(com.capgemini.tpp.ldap.client.model.ClientModel,java.lang.String,
	 *      java.lang.String)
	 */
	@Override
	@Retryable(value = { Exception.class }, maxAttempts = 1)
	public ClientModel updateTPPApplicationinPF(ClientModel clientModel, String clientId, String clientSecret) {

		ClientModel clientRegRequest = TppSoftwareHelper.populateClientRequestForClientSecret(clientModel,
				pfClientRegConfig, clientSecret);
		RequestInfo requestInfo = new RequestInfo();
		String pfOAuthClientByIdUrl = pfClientRegConfig.getClientByIdUrl(requestHeaderAttributes.getTenantId());
		pfOAuthClientByIdUrl = pfOAuthClientByIdUrl.replaceAll("\\{clientId\\}", clientId);
		requestInfo.setUrl(pfOAuthClientByIdUrl);

		HttpHeaders httpHeaders = populateHttpHeaders();
		return restClientSync.callForPut(requestInfo, clientRegRequest, ClientModel.class, httpHeaders);
	}

	/**
	 * The Application is fetch from PF through a GET api call. The URL is
	 * fetched from properties file and the client id is then replaced by the
	 * client of the application to be get
	 * 
	 * @see com.capgemini.tppportal.services.PingFederateService#
	 *  fetchClientDetailsByIdFromPF(java.lang.String)
	 */
	@Override
	@Retryable(value = { Exception.class }, maxAttempts = 1)
	public ClientModel fetchClientDetailsByIdFromPF(String clientId) {
		RequestInfo requestInfo = new RequestInfo();
		String pfOAuthClientByIdUrl = pfClientRegConfig.getClientByIdUrl(requestHeaderAttributes.getTenantId());
		pfOAuthClientByIdUrl = pfOAuthClientByIdUrl.replaceAll("\\{clientId\\}", clientId);
		requestInfo.setUrl(pfOAuthClientByIdUrl);
		HttpHeaders httpHeaders = populateHttpHeaders();
		return restClientSync.callForGet(requestInfo, ClientModel.class, httpHeaders);
	}
	

	public void addTrustedCAinPF(X509Certificate cert) throws CertificateException, IOException {
		HashMap<String, String> map=new HashMap<>();
		map.put("fileData", certExtractor.intermediateCertificate(cert));
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(pfClientRegConfig.getCaImportUrl(requestHeaderAttributes.getTenantId()));
		HttpHeaders httpHeaders = populateHttpHeaders();
		Map certParams= restClientSync.callForPost(requestInfo, map, Map.class, httpHeaders);
		LOGGER.info("Trust Certificate imports details {}", certParams);
	}
	

	public void replicatePFConfiguration() {
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(pfClientRegConfig.getReplicateUrl(requestHeaderAttributes.getTenantId()));
		HttpHeaders httpHeaders = populateHttpHeaders();
		Map certParams= restClientSync.callForPost(requestInfo, null, Map.class, httpHeaders);
		LOGGER.info("Replicate Service response {}", certParams);
	}
	
	@Override
	@Retryable(value = { Exception.class }, maxAttempts = 1)
	public ClientModel fetchTPPApplicationFromPF(ClientModel clientModel, String clientId, String clientSecret) {

		TppSoftwareHelper.populateClientRequestForClientSecret(clientModel,
				pfClientRegConfig, clientSecret);
		RequestInfo requestInfo = new RequestInfo();
		String pfOAuthClientByIdUrl = pfClientRegConfig.getClientByIdUrl(requestHeaderAttributes.getTenantId());
		pfOAuthClientByIdUrl = pfOAuthClientByIdUrl.replaceAll("\\{clientId\\}", clientId);
		requestInfo.setUrl(pfOAuthClientByIdUrl);

		HttpHeaders httpHeaders = populateHttpHeaders();
		return restClientSync.callForGet(requestInfo, ClientModel.class, httpHeaders);
	}

}
