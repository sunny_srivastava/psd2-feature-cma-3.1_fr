/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.helper;

import java.net.URI;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import javax.annotation.PostConstruct;
import javax.naming.Name;
import javax.naming.NamingException;
import javax.naming.directory.BasicAttributes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ldap.core.DirContextAdapter;
import org.springframework.ldap.query.LdapQuery;
import org.springframework.ldap.query.LdapQueryBuilder;
import org.springframework.ldap.support.LdapNameBuilder;
import org.springframework.stereotype.Component;
import com.capgemini.psd2.config.PFClientRegConfig;
import com.capgemini.psd2.constant.ClientRegistarionConstants;
import com.capgemini.psd2.constants.LdapConstants;
import com.capgemini.psd2.enums.ClientRegistrationErrorCodeEnum;
import com.capgemini.psd2.exception.ClientRegistrationException;
import com.capgemini.psd2.jwt.JWTReader;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.utilities.JSONUtilities;
import com.capgemini.psd2.utilities.StringUtils;
import com.capgemini.tpp.dtos.GrantScopes;
import com.capgemini.tpp.ldap.client.model.ClientAuth;
import com.capgemini.tpp.ldap.client.model.ClientModel;
import com.capgemini.tpp.ldap.client.model.DefaultAccessTokenManagerRef;
import com.capgemini.tpp.ldap.client.model.JwksSettings;
import com.capgemini.tpp.ldap.client.model.OidcPolicy;
import com.capgemini.tpp.ldap.client.model.PolicyGroup;
import com.capgemini.tpp.ldap.client.model.TPPApplication;
import com.capgemini.tpp.ssa.model.SSAModel;

/**
 * The Class TppSoftwareHelper.
 */

@Component
public final class TppSoftwareHelper {
	
	private static final Logger LOG = LoggerFactory.getLogger(TppSoftwareHelper.class);

	@Autowired
	private RequestHeaderAttributes attributes;
	
	private static RequestHeaderAttributes attributes1;
	
	private TppSoftwareHelper() {

	}
	
	@PostConstruct
	public void init(){
		//attributes1=attributes;
		setAttributes(attributes);
	}
	
	private static void setAttributes(RequestHeaderAttributes attributes){
		attributes1 = attributes;
	}
	public static ClientModel populateClientRequest(SSAModel ssaElements, PFClientRegConfig pfClientRegConfig,
			String secret) {
		ClientModel clientModel = new ClientModel();
		ClientAuth clientAuth = populateClientAuth(pfClientRegConfig, secret,ssaElements.getClientCertSubjectDn());
		DefaultAccessTokenManagerRef defaultAccessTokenManagerRef = populateAccessTokenMgrRef(pfClientRegConfig);
		JwksSettings jwksSettings = populateJwksSettings(ssaElements);
		OidcPolicy oidcPolicy = populateOidcPolicy(pfClientRegConfig,ssaElements);

		clientModel.setClientAuth(clientAuth);
		clientModel.setOidcPolicy(oidcPolicy);
		clientModel.setJwksSettings(jwksSettings);
		clientModel.setDefaultAccessTokenManagerRef(defaultAccessTokenManagerRef);

		clientModel.setClientId(StringUtils.defaultVal(ssaElements.getSoftware_client_id()));
		clientModel.setBypassApprovalPage(Boolean.toString(Boolean.TRUE));
		clientModel.setDescription(StringUtils.defaultVal(ssaElements.getSoftware_client_description()));
		clientModel.setRequestObjectSigningAlgorithm(pfClientRegConfig.getRequestObjectSigningAlgorithm());

		populateSoftwareGrantTypesScopes(ssaElements, pfClientRegConfig, clientModel);

		URI softwareLogoURI = null;

		try {
			if (ssaElements.getSoftware_logo_uri() != null) {
				softwareLogoURI = URI.create(ssaElements.getSoftware_logo_uri());
				clientModel.setLogoUrl(softwareLogoURI != null ? softwareLogoURI.toString()
						: org.apache.commons.lang3.StringUtils.EMPTY);
			}
		} catch (IllegalArgumentException ie) {
			// for now software_client_uri is not implemented from OpenBanking. 
			// when it is incorrect we need to check if we need to throw exception or not.
			LOG.info("IllegalArgumentException in populateClientRequest(): "+ie);
		}
		clientModel.setName(StringUtils.defaultVal(ssaElements.getSoftware_client_name()));

		if (ssaElements.getSoftware_redirect_uris() != null && !(ssaElements.getSoftware_redirect_uris()).isEmpty()) {
			clientModel.setRedirectUris((List<String>) ssaElements.getSoftware_redirect_uris());
		}
		clientModel.setRequireSignedRequests(Boolean.toString(Boolean.TRUE));
		clientModel.setValidateUsingAllEligibleAtms(Boolean.toString(Boolean.FALSE));
		clientModel.setRefreshRolling(pfClientRegConfig.getRefreshRolling());
		clientModel.setPersistentGrantExpirationType(pfClientRegConfig.getPersistentGrantExpirationType());
		clientModel
				.setPersistentGrantExpirationTime(String.valueOf(pfClientRegConfig.getPersistentGrantExpirationTime()));
		clientModel.setExclusiveScopes(new ArrayList<>());
		clientModel.setRestrictedResponseTypes(pfClientRegConfig.getRestrictedResponseTypes());
		clientModel.setPersistentGrantExpirationTimeUnit(pfClientRegConfig.getPersistentGrantExpirationTimeUnit());
		clientModel.setBypassApprovalPage(Boolean.toString(Boolean.TRUE));
		return clientModel;
	}

	public static String encodeCredentials(PFClientRegConfig pfClientRegConfig) {
		String credentials = pfClientRegConfig.getAdminuser(attributes1.getTenantId()).concat(":").concat(pfClientRegConfig.getAdminpwd(attributes1.getTenantId()));
		return Base64.getEncoder().encodeToString(credentials.getBytes());
	}

	private static OidcPolicy populateOidcPolicy(PFClientRegConfig pfClientRegConfig,SSAModel ssaElements) {
		OidcPolicy oidcPolicy = new OidcPolicy();
		PolicyGroup policyGroup = new PolicyGroup();
		policyGroup.setId(pfClientRegConfig.getPolicyGroupId());
		policyGroup.setLocation(org.apache.commons.lang3.StringUtils.EMPTY);
		oidcPolicy.setIdTokenSigningAlgorithm(ssaElements.getId_token_signing_alg());
		oidcPolicy.setPolicyGroup(policyGroup);
		oidcPolicy.setGrantAccessSessionRevocationApi(Boolean.toString(Boolean.FALSE));
		oidcPolicy.setPingAccessLogoutCapable(Boolean.toString(Boolean.FALSE));
		return oidcPolicy;
	}

	private static JwksSettings populateJwksSettings(SSAModel ssaElements) {
		if (ssaElements.getSoftware_jwks_endpoint() == null
				|| (ssaElements.getSoftware_jwks_endpoint()).trim().isEmpty()) {
			return null;
		}
		JwksSettings jwksSettings = new JwksSettings();
		jwksSettings.setJwks(null);
		jwksSettings.setJwksUrl(ssaElements.getSoftware_jwks_endpoint());
		return jwksSettings;
	}

	public static Name buildBaseDN(String baseDn) {
		return LdapNameBuilder.newInstance().add(baseDn).build();
	}

	/**
	 * @param baseDn
	 * @param searchAttribute
	 * @return
	 */
	public static LdapQuery getLdapQuery(String baseDn, String searchAttribute) {
		return LdapQueryBuilder.query().attributes(ClientRegistarionConstants.ASTERISK, ClientRegistarionConstants.PLUS).base(baseDn)
				.filter("(cn=" + searchAttribute + ")");
	}

	public static boolean isAttributeExist(DirContextAdapter dx, String attr) {
		boolean isAttributeExist = false;
		if (dx.attributeExists(attr)) {
			isAttributeExist = true;
		}
		return isAttributeExist;
	}

	public static String getAttributeValue(BasicAttributes tppApplication, String ldapAttr) {
		String attributeValue = null;
		try {
			if (tppApplication.get(ldapAttr) != null && tppApplication.get(ldapAttr).get() != null) {
				attributeValue = tppApplication.get(ldapAttr).get().toString();
			}
		} catch (NamingException ex) {
			LOG.info("NamingException in getAttributeValue(): "+ex);
			throw ClientRegistrationException.populateDCRException(ex.getMessage(),
					ClientRegistrationErrorCodeEnum.LDAP_ATTRIBUTE_NOT_FOUND_IN_TPP);
		}

		return attributeValue;
	}

	/**
	 * @param ldapObject
	 * @return
	 */
	public static TPPApplication populateTPPApplication(BasicAttributes ldapObject) {

		TPPApplication tppApplication = new TPPApplication();

		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
		Date timestamp;
		try {
			timestamp = sdf.parse(getAttributeValue(ldapObject, LdapConstants.CREATE_TIMESTAMP));
			tppApplication.setCreateTimestamp(timestamp);
		} catch (ParseException e) {
			LOG.info("ParseException in populateTPPApplication(): "+e);
			throw ClientRegistrationException.populateDCRException(e.getMessage(), ClientRegistrationErrorCodeEnum.UNABLE_TO_PARSE);
		}

		tppApplication.setMuleAppId(getAttributeValue(ldapObject, LdapConstants.APP_ID));
		String ssaToken = getAttributeValue(ldapObject, LdapConstants.SSA_TOKEN);

		Map jwtMap = JWTReader.decodeTokenIntoMap(ssaToken);
		String jsonString = JSONUtilities.getJSONOutPutFromObject(jwtMap);
		SSAModel ssaModel = JSONUtilities.getObjectFromJSONString(jsonString, SSAModel.class);
		tppApplication.setClientId(ssaModel.getSoftware_client_id());
		tppApplication.setCn(ssaModel.getSoftware_client_name());
		tppApplication.setSoftwareClientDescription(ssaModel.getSoftware_client_description());
		tppApplication.setStatus(getAttributeValue(ldapObject, LdapConstants.STATUS));

		tppApplication.setPolicyURI(ssaModel.getSoftware_policy_uri());
		tppApplication.setTermOfServiceURI(ssaModel.getSoftware_tos_uri());

		tppApplication.setSoftwareVersion(ssaModel.getSoftware_version());

		tppApplication.setRedirectUrl(ssaModel.getSoftware_redirect_uris());
		tppApplication.setSoftwareRoles(ssaModel.getSoftware_roles());

		tppApplication.setJwksUrl(ssaModel.getSoftware_jwks_endpoint());
		tppApplication.setSoftwareClientName(ssaModel.getSoftware_client_name());

		return tppApplication;
	}

	/**
	 * @param object
	 * @param ldapAttr
	 * @return
	 */
	public static List<String> getBasicAttributes(BasicAttributes object, String ldapAttr) {

		String[] str = null;

		List<String> retrunValue = null;
		try {
			if (object.get(ldapAttr) != null && object.get(ldapAttr).get() != null) {
				str = object.get(ldapAttr).toString().split(":");
				retrunValue = Arrays.asList(str[1].split(", "));
				return retrunValue;
			}
		} catch (org.springframework.ldap.NamingException | NamingException e) {
			LOG.info("NamingException in populateTPPApplication(): "+e);
			throw ClientRegistrationException.populateDCRException(e.getMessage(),
					ClientRegistrationErrorCodeEnum.LDAP_ATTRIBUTE_NOT_FOUND_IN_TPP);

		}
		return retrunValue;
	}

	private static void populateSoftwareGrantTypesScopes(SSAModel ssaElements, PFClientRegConfig pfClientRegConfig,
			ClientModel clientModel) {
		GrantScopes grantScopes;
		Set<String> grantTypes = new HashSet<>();
		Set<String> scopes = new HashSet<>();
		if (ssaElements.getSoftware_roles() == null || (ssaElements.getSoftware_roles()).isEmpty()) {
			return;
		}
		List<String> roles = ssaElements.getSoftware_roles();
		for (String role : roles) {
			grantScopes = pfClientRegConfig.getGrantsandscopes().get(role);
			if (grantScopes != null) {
				grantTypes.addAll(grantScopes.getGranttypes());
				scopes.addAll(grantScopes.getScopes());
			}
		}
		if (!grantTypes.isEmpty()) {
			String[] grantTypesArray = new String[grantTypes.size()];
			clientModel.setGrantTypes(Arrays.asList(grantTypes.toArray(grantTypesArray)));
		}
		if (!scopes.isEmpty()) {
			String[] scopesArray = new String[scopes.size()];
			clientModel.setRestrictedScopes(Arrays.asList(scopes.toArray(scopesArray)));
			clientModel.setRestrictScopes(Boolean.toString(Boolean.TRUE));
		}
	}

	private static ClientAuth populateClientAuth(PFClientRegConfig pfClientRegConfig, String secret,String certdn) {
		ClientAuth clientAuth = new ClientAuth();
		clientAuth.setType(pfClientRegConfig.getSecretType());
		clientAuth.setSecret(secret);
		clientAuth.setEncryptedSecret(org.apache.commons.lang3.StringUtils.EMPTY);
		clientAuth.setClientCertIssuerDn(pfClientRegConfig.getCertIssuer());
		clientAuth.setClientCertSubjectDn(certdn);
		clientAuth.setEnforceReplayPrevention(Boolean.toString(Boolean.FALSE));
		return clientAuth;
	}

	private static DefaultAccessTokenManagerRef populateAccessTokenMgrRef(PFClientRegConfig pfClientRegConfig) {
		DefaultAccessTokenManagerRef defaultAccessTokenManagerRef = new DefaultAccessTokenManagerRef();
		defaultAccessTokenManagerRef.setId(pfClientRegConfig.getTokenMgrRefId(attributes1.getTenantId()));
		defaultAccessTokenManagerRef.setLocation(org.apache.commons.lang3.StringUtils.EMPTY);
		return defaultAccessTokenManagerRef;
	}

	/**
	 * @param inputString
	 * @param key
	 * @param firstSeperator
	 * @param secondSeperator
	 * @return
	 */
	public static String getObjectString(String inputString, String key, String firstSeperator,
			String secondSeperator) {
		Map<String, String> map = new HashMap<>();
		String[] parts = inputString.split(firstSeperator);

		for (int i = 0; i < parts.length; i++) {
			String[] secondParts = parts[i].split(secondSeperator);

			map.put(secondParts[0], secondParts[1]);
		}
		if (map.get(key) != null) {
			return map.get(key) != null ? map.get(key) : null;
		} else {
			String keys = key.trim();
			return map.get(keys) != null ? map.get(keys) : null;
		}

	}

	/**
	 * Update the ClientModel object for requested client secret attribute
	 * 
	 * @param clientModel
	 * @param pfClientRegConfig
	 * @param secret
	 * @return ClientModel
	 */
	public static ClientModel populateClientRequestForClientSecret(ClientModel clientModel,
			PFClientRegConfig pfClientRegConfig, String secret) {
		String certdn = clientModel.getClientAuth() == null || clientModel.getClientAuth().getClientCertSubjectDn() == null || clientModel.getClientAuth().getClientCertSubjectDn().trim().length() == 0 ? "" : clientModel.getClientAuth().getClientCertSubjectDn();
		ClientAuth clientAuth = populateClientAuth(pfClientRegConfig, secret,certdn);
		clientModel.setClientAuth(clientAuth);
		return clientModel;
	}
}
