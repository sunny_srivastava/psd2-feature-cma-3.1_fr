package com.capgemini.psd2.helper;

import java.net.URI;

import com.capgemini.psd2.config.MuleClientRegConfig;
import com.capgemini.psd2.utilities.StringUtils;
import com.capgemini.tpp.muleapi.schema.LoginRequest;
import com.capgemini.tpp.muleapi.schema.NewApplicationMuleApiRequest;
import com.capgemini.tpp.muleapi.schema.NewContractsRequest;
import com.capgemini.tpp.ssa.model.SSAModel;

public final class MuleClientRegistrationHelper {

	private MuleClientRegistrationHelper() {

	}

	public static LoginRequest populateLoginRequest(MuleClientRegConfig muleClientRegConfig) {
		LoginRequest loginRequest = new LoginRequest();
		loginRequest.setUsername(muleClientRegConfig.getUser());
		loginRequest.setPassword(muleClientRegConfig.getPassword());
		return loginRequest;
	}

	public static NewApplicationMuleApiRequest populateApplicationMuleApiRequest(SSAModel ssaDataElements,
			String clientSecret) {
		NewApplicationMuleApiRequest muleApiRequest = new NewApplicationMuleApiRequest();
		muleApiRequest.setClientId(StringUtils.defaultVal(ssaDataElements.getSoftware_client_id()));
		muleApiRequest.setDescription(StringUtils.defaultVal(ssaDataElements.getSoftware_client_description()));
		muleApiRequest.setName(StringUtils.defaultVal(ssaDataElements.getSoftware_client_name()));
		muleApiRequest.setClientSecret(clientSecret);
		if (ssaDataElements.getSoftware_redirect_uris() != null
				&& !(ssaDataElements.getSoftware_redirect_uris()).isEmpty()) {
			muleApiRequest.setRedirectUri(ssaDataElements.getSoftware_redirect_uris());
		}

		URI softwareClientURI = null;

		try {
			if (ssaDataElements.getSoftware_client_uri() != null
					&& !"not implemented".equals(ssaDataElements.getSoftware_client_uri()))
				softwareClientURI = URI.create((String) ssaDataElements.getSoftware_client_uri());

		} catch (IllegalArgumentException ie) { //NOSONAR
			// for now software_client_uri is not implemented from OpenBanking. 
			// when it is incorrect we need to check if we need to throw exception or not.
		}
		muleApiRequest.setUrl(
				softwareClientURI != null ? softwareClientURI.toString() : org.apache.commons.lang3.StringUtils.EMPTY);
		return muleApiRequest;
	}

	public static NewContractsRequest populateNewContractRequest(Integer applicationId,
			Integer apiVersionId, Integer tierId) {
		NewContractsRequest newContractRequest = new NewContractsRequest();
		newContractRequest.setAcceptedTerms(Boolean.TRUE);
		newContractRequest.setApiVersionId(apiVersionId);
		newContractRequest.setApplicationId(applicationId);
		newContractRequest.setPartyId(org.apache.commons.lang3.StringUtils.EMPTY);
		newContractRequest.setPartyName(org.apache.commons.lang3.StringUtils.EMPTY);
		newContractRequest.setRequestedTierId(tierId);
		return newContractRequest;
	}
}