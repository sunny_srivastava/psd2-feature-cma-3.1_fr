package com.capgemini.psd2.helper;

import java.io.IOException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

import org.bouncycastle.asn1.x509.AccessDescription;
import org.bouncycastle.asn1.x509.AuthorityInformationAccess;
import org.bouncycastle.asn1.x509.Extension;
import org.bouncycastle.asn1.x509.GeneralName;
import org.bouncycastle.asn1.x509.X509ObjectIdentifiers;
import org.bouncycastle.util.encoders.Base64;
import org.bouncycastle.x509.extension.X509ExtensionUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.RestClientSync;
import com.capgemini.psd2.utilities.GenerateUniqueIdUtilities;

@Component
public class CertExtractor {
	@Autowired
	@Qualifier("tppPortalRestClient")
	private RestClientSync restClientSync;

	public String intermediateCertificate(X509Certificate cert) throws CertificateException, IOException { //NOSONAR

		byte[] extVal = cert.getExtensionValue(Extension.authorityInfoAccess.getId());
		CertificateFactory certificatefactory = CertificateFactory.getInstance("X.509");
		AuthorityInformationAccess aia = AuthorityInformationAccess
				.getInstance(X509ExtensionUtil.fromExtensionValue(extVal));
		String pemCertPre = null;
		AccessDescription[] descriptions = aia.getAccessDescriptions();
		for (AccessDescription ad : descriptions) {
			if (ad.getAccessMethod().equals(X509ObjectIdentifiers.id_ad_caIssuers)) {
				GeneralName location = ad.getAccessLocation();
				if (location.getTagNo() == GeneralName.uniformResourceIdentifier) {
					String issuerUrl = location.getName().toString();
					X509Certificate issuer = (X509Certificate) certificatefactory
							.generateCertificate(getCertResource(issuerUrl).getInputStream());
					pemCertPre = new String(Base64.encode(issuer.getEncoded()));
					return pemCertPre;
				}
			}
		}
		return pemCertPre;
	}

	private Resource getCertResource(String issuerUrl) {
		RequestInfo requestInfo = new RequestInfo();
		requestInfo.setUrl(issuerUrl);
		HttpHeaders requestHeaders = new HttpHeaders();
		requestHeaders.add("X-XSRF-Header", GenerateUniqueIdUtilities.generateRandomUniqueID());
		return restClientSync.callForGet(requestInfo, Resource.class, requestHeaders);
	}

}
