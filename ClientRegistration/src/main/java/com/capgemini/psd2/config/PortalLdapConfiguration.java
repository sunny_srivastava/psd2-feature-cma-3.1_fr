package com.capgemini.psd2.config;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.ldap.core.ContextSource;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.core.support.LdapContextSource;

@Configuration
@ConfigurationProperties(prefix = "ldap")
public class PortalLdapConfiguration {

	@Value("${ldap.config.url}")
	private String url;

	@Value("${ldap.config.userDn}")
	private String userDn;

	@Value("${ldap.tppapplication.clientdn}")
	private String clientdn;
	
	@Value("${ldap.enhanced.password}")
	private String ldapPassword;

	@Value("${ldap.tppapplication.basedn}")
	private String tppApplicationBasedn;

	private Map<String, String> tppgroupbasedn = new HashMap<>();
	
	private Map<String, String> clientAppgroupbasedn = new HashMap<>();
	
	public String getTppGroupBasedn(String key) {
		return tppgroupbasedn.get(key);
	}

	public String getClientAppgroupBasedn(String key) {
		return clientAppgroupbasedn.get(key);
	}
	

	public Map<String, String> getTppgroupbasedn() {  //NOSONAR
		return tppgroupbasedn;
	}

	public void setTppgroupbasedn(Map<String, String> tppgroupbasedn) {
		this.tppgroupbasedn = tppgroupbasedn;
	}


	public Map<String, String> getClientAppgroupbasedn() { //NOSONAR
		return clientAppgroupbasedn;
	}

	public void setClientAppgroupbasedn(Map<String, String> clientAppgroupbasedn) {
		this.clientAppgroupbasedn = clientAppgroupbasedn;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getUserDn() {
		return userDn;
	}
	
	public String getClientdn() {
		return clientdn;
	}

	public void setClientdn(String clientdn) {
		this.clientdn = clientdn;
	}

	public void setUserDn(String userDn) {
		this.userDn = userDn;
	}

	public String getLdapPassword() {
		return ldapPassword;
	}

	public void setLdapPassword(String ldapPassword) {
		this.ldapPassword = ldapPassword;
	}

	@Bean
	public ContextSource contextSource() {
		LdapContextSource contextSource = new LdapContextSource();
		contextSource.setUrl(url);
		contextSource.setUserDn(userDn);
		contextSource.setPassword(ldapPassword);
		return contextSource;
	}

	@Bean
	public LdapTemplate ldapTemplate() {
		return new LdapTemplate(contextSource());
	}

	public String getTppApplicationBasedn() {
		return tppApplicationBasedn;
	}


	public void setTppApplicationBasedn(String tppApplicationBasedn) {
		this.tppApplicationBasedn = tppApplicationBasedn;
	}

}