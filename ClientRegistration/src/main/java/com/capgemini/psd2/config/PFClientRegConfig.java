package com.capgemini.psd2.config;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import com.capgemini.tpp.dtos.GrantScopes;

@Configuration
@ConfigurationProperties(prefix = "pfclientreg")
public class PFClientRegConfig {

	private Map<String, String> prefix = new HashMap<>();
	private String clientsUrl;
	private String clientByIdUrl;
	private String secretType;
	private Map<String, String> tokenMgrRefId = new HashMap<>();
	private Map<String, String>  adminuser = new HashMap<>();
	private Map<String, String>  adminpwdV2 = new HashMap<>();
	private Map<String, GrantScopes> grantsandscopes = new HashMap<>();
	private String idTokenSigningAlgorithm;
	private String policyGroupId;
	private List<String> restrictedResponseTypes = new ArrayList<>();
	private String refreshRolling;
	private Integer persistentGrantExpirationTime = 0;
	private String persistentGrantExpirationType;
	private String persistentGrantExpirationTimeUnit;
	private String clientCertIssuerDn;
	private String caImportUrl;
	private String certIssuer;
	private String replicateUrl;
	
	private String requestObjectSigningAlgorithm;

	public String getRequestObjectSigningAlgorithm() {
		return requestObjectSigningAlgorithm;
	}

	public void setRequestObjectSigningAlgorithm(String requestObjectSigningAlgorithm) {
		this.requestObjectSigningAlgorithm = requestObjectSigningAlgorithm;
	}

	public String getAdminuser(String key) {
		return adminuser.get(key);
	}

	public Map<String,String> getAdminuser() {
		return adminuser;
	}
	
	public Map<String,String> getTokenMgrRefId() {
		return tokenMgrRefId;
	}
	
	public String getTokenMgrRefId(String key) {
		return tokenMgrRefId.get(key);
	}
	public Map<String,String> getAdminpwdV2() {
		return adminpwdV2;
	}
	public String getAdminpwd(String key) {
		return adminpwdV2.get(key);
	}

	public String getPrefix(String key) {
		return prefix.get(key);
	}
	
	public Map<String,String> getPrefix() {
		return prefix;
	}

	public String getReplicateUrl(String id) {
		return getPrefix(id) +replicateUrl;
	}

	public void setReplicateUrl(String replicateUrl) {
		this.replicateUrl = replicateUrl;
	}

	public String getCertIssuer() {
		return certIssuer;
	}

	public void setCertIssuer(String certIssuer) {
		this.certIssuer = certIssuer;
	}

	public String getCaImportUrl(String id) {
		return getPrefix(id) + caImportUrl;
	}

	public void setCaImportUrl(String caImportUrl) {
		this.caImportUrl = caImportUrl;
	}

	public String getClientCertIssuerDn() {
		return clientCertIssuerDn;
	}

	public void setClientCertIssuerDn(String clientCertIssuerDn) {
		this.clientCertIssuerDn = clientCertIssuerDn;
	}

	public String getPolicyGroupId() {
		return policyGroupId;
	}

	public void setPolicyGroupId(String policyGroupId) {
		this.policyGroupId = policyGroupId;
	}

	public List<String> getRestrictedResponseTypes() {
		return restrictedResponseTypes;
	}

	public void setRestrictedResponseTypes(List<String> restrictedResponseTypes) {
		this.restrictedResponseTypes = restrictedResponseTypes;
	}

	public void setSecretType(String secretType) {
		this.secretType = secretType;
	}

	public String getClientByIdUrl(String id) {
		return getPrefix(id) + clientByIdUrl;
	}

	public void setClientByIdUrl(String clientByIdUrl) {
		this.clientByIdUrl = clientByIdUrl;
	}

	public String getClientsUrl(String id) {
		return getPrefix(id) + clientsUrl;
	}

	public void setClientsUrl(String clientsUrl) {
		this.clientsUrl = clientsUrl;
	}

	public void setIdTokenSigningAlgorithm(String idTokenSigningAlgorithm) {
		this.idTokenSigningAlgorithm = idTokenSigningAlgorithm;
	}

	public void setRefreshRolling(String refreshRolling) {
		this.refreshRolling = refreshRolling;
	}

	public void setPersistentGrantExpirationTime(Integer persistentGrantExpirationTime) {
		this.persistentGrantExpirationTime = persistentGrantExpirationTime;
	}

	public void setPersistentGrantExpirationType(String persistentGrantExpirationType) {
		this.persistentGrantExpirationType = persistentGrantExpirationType;
	}

	public void setPersistentGrantExpirationTimeUnit(String persistentGrantExpirationTimeUnit) {
		this.persistentGrantExpirationTimeUnit = persistentGrantExpirationTimeUnit;
	}

	public String getSecretType() {
		return secretType;
	}

	public Map<String, GrantScopes> getGrantsandscopes() {
		return grantsandscopes;
	}

	public String getIdTokenSigningAlgorithm() {
		return idTokenSigningAlgorithm;
	}

	public String getRefreshRolling() {
		return refreshRolling;
	}

	public String getPersistentGrantExpirationType() {
		return persistentGrantExpirationType;
	}

	public String getPersistentGrantExpirationTimeUnit() {
		return persistentGrantExpirationTimeUnit;
	}

	public Integer getPersistentGrantExpirationTime() {
		return persistentGrantExpirationTime;
	}
}