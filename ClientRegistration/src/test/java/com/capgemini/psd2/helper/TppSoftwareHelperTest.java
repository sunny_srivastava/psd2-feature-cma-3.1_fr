package com.capgemini.psd2.helper;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.config.PFClientRegConfig;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.tpp.ssa.model.SSAModel;

@RunWith(SpringJUnit4ClassRunner.class)
public class TppSoftwareHelperTest {

	@InjectMocks
	private TppSoftwareHelper helper;
	
	@Mock
	private RequestHeaderAttributes attributes;
	
	@Mock
	PFClientRegConfig pfClientRegConfig = new PFClientRegConfig();
	
	@Before
	public void setUp() throws Exception {
		pfClientRegConfig.setClientByIdUrl("https://LIN51003588.corp.capgemini.com:9999/pf-admin-api/v1/oauth/clients/{clientId}");
		pfClientRegConfig.setClientsUrl("https://LIN51003588.corp.capgemini.com:9999/pf-admin-api/v1/oauth/clients");
		pfClientRegConfig.getAdminuser().put("Admin", "ApiAdmin");
		pfClientRegConfig.getAdminpwdV2().put("pwd", "Test@2017");
		pfClientRegConfig.getAdminpwd("pwd");
		pfClientRegConfig.getAdminuser("Admin");
		pfClientRegConfig.getTokenMgrRefId().put("tenant1","INTREFATM");
		pfClientRegConfig.setCaImportUrl("https://test.com");
		pfClientRegConfig.setReplicateUrl("https://replicateurl");
		pfClientRegConfig.getPrefix().put("PSD2", "https://localhost:8000/123455");
		pfClientRegConfig.getPrefix("PSD2");
		
		attributes.setTenantId("testString");
	
		MockitoAnnotations.initMocks(this);
		helper.init();
	}
	
	@Test
	public void populateClientRequestTest() {
		
		SSAModel ssaModel = new SSAModel();
		ssaModel.setIss("OpenBanking Ltd");
		ssaModel.setOrg_id("0015800000jfQ9aAAE");
		ssaModel.setSoftware_id("4cpi8P4dkTk200Inv1cPvO");
		ssaModel.setSoftware_client_id("4cpi8P4dkTk200Inv1cPvO");
		ssaModel.setSoftware_roles(new ArrayList<String>(Arrays.asList("AISP", "PISP")));
		ssaModel.setSoftware_logo_uri("https://demo.com");
		ssaModel.setSoftware_redirect_uris(new ArrayList<String>(Arrays.asList("https://test.com")));
		
		Assert.assertNotNull(TppSoftwareHelper.populateClientRequest(ssaModel, pfClientRegConfig, "testString"));
	}
	
	
	@Test
	public void populateClientRequestExceptionTest() {
	
		SSAModel ssaModel = new SSAModel();
		ssaModel.setIss("OpenBanking Ltd");
		ssaModel.setOrg_id("0015800000jfQ9aAAE");
		ssaModel.setSoftware_id("4cpi8P4dkTk200Inv1cPvO");
		ssaModel.setSoftware_client_id("4cpi8P4dkTk200Inv1cPvO");
		ssaModel.setSoftware_roles(new ArrayList<String>(Arrays.asList("AISP", "PISP")));
		String url ="http://someurl.com/passfail?parameter={}";
		ssaModel.setSoftware_logo_uri(url);
		ssaModel.setSoftware_redirect_uris(new ArrayList<String>(Arrays.asList("https://test.com")));
		Assert.assertNull((TppSoftwareHelper.populateClientRequest(ssaModel, pfClientRegConfig, "testString")).getLogoUrl());
	}
	
}