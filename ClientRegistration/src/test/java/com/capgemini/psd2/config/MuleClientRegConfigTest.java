package com.capgemini.psd2.config;

import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.tpp.dtos.ApiDetail;

@RunWith(SpringJUnit4ClassRunner.class)
public class MuleClientRegConfigTest {
	
	
	@InjectMocks
	private MuleClientRegConfig muleClientRegConfig;
	
	@Before
	public void setUp() {
		
	}
	
	
	@Test
	public void muleClientRegConfigSuccessTest() throws Exception {
		
		muleClientRegConfig.setApiContractUrl("https://localhost:8000/123455");
		muleClientRegConfig.setApiUrl("https://localhost:8000");
		muleClientRegConfig.setApiUrlPrefix("");
		muleClientRegConfig.setApplicationRollBackUrl("https://rollback");
		muleClientRegConfig.setApplicationUrl("https://localhost:8000/api");
		muleClientRegConfig.setLoginUrl("https://localhost:8000/login");
		muleClientRegConfig.setPassword("password");
		muleClientRegConfig.setResetSecretUrl("https://localhost:8000/reset");
		muleClientRegConfig.setShowSecretUrl("https://localhost:8000/show");
		muleClientRegConfig.setTierUrl("https://localhost:8000");
		muleClientRegConfig.setUser("user");
		Map<String, ApiDetail> muleContractApi = new HashMap<>();
		ApiDetail apiDetail = new ApiDetail();
		apiDetail.setApiId(123);
		muleContractApi.put("key", apiDetail);
		muleClientRegConfig.setMuleContractApi(muleContractApi);
		muleClientRegConfig.setPrefix("");
		
		assertTrue(muleClientRegConfig.getPrefix().equals(""));
		assertTrue(muleClientRegConfig.getApiContractUrl().equals("https://localhost:8000/123455"));
		assertTrue(muleClientRegConfig.getApiUrl().equals("https://localhost:8000"));
		assertTrue(muleClientRegConfig.getApiUrlPrefix().equals(""));
		assertTrue(muleClientRegConfig.getApplicationRollBackUrl().equals("https://rollback"));
		assertTrue(muleClientRegConfig.getApplicationUrl().equals("https://localhost:8000/api"));
		assertTrue(muleClientRegConfig.getLoginUrl().equals("https://localhost:8000/login"));
		assertTrue(muleClientRegConfig.getPassword().equals("password"));
		assertTrue(muleClientRegConfig.getResetSecretUrl().equals("https://localhost:8000/reset"));
		assertTrue(muleClientRegConfig.getShowSecretUrl().equals("https://localhost:8000/show"));
		assertTrue(muleClientRegConfig.getTierUrl().equals("https://localhost:8000"));
		assertTrue(muleClientRegConfig.getUser().equals("user"));
		assertTrue(muleClientRegConfig.getMuleContractApi().get("key").getApiId() == 123);
		
	}
	
}