package com.capgemini.psd2.config;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
public class PortalLdapConfigurationTest {
	
	@InjectMocks
	private PortalLdapConfiguration portalLdapConfiguration;
	
	@Before
	public void setUp() {
		
	}
	
	@Test
	public void portalLdapConfigurationSuccessTest() throws Exception {

		Map<String, String> clientAppgroupbasedn=new HashMap<String,String>();
		clientAppgroupbasedn.put("1", "Alipay");
		portalLdapConfiguration.setClientAppgroupbasedn(clientAppgroupbasedn);
		portalLdapConfiguration.setClientdn("testClient");
		portalLdapConfiguration.setLdapPassword("password");
		portalLdapConfiguration.setTppApplicationBasedn("tppApplication");
		Map<String, String> tppgroupbasedn=new HashMap<String,String>();
		tppgroupbasedn.put("2", "ABC");
		portalLdapConfiguration.setTppgroupbasedn(tppgroupbasedn);
		portalLdapConfiguration.setUrl("https://localhost:8000");
		portalLdapConfiguration.setUserDn("cn=dirAdmin");
		portalLdapConfiguration.setTppgroupbasedn(tppgroupbasedn);
		
		
		assertNotNull(tppgroupbasedn);
		assertNotNull(clientAppgroupbasedn);
		assertEquals(clientAppgroupbasedn, portalLdapConfiguration.getClientAppgroupbasedn());
		assertEquals(tppgroupbasedn, portalLdapConfiguration.getTppgroupbasedn());
		assertEquals("Alipay", portalLdapConfiguration.getClientAppgroupBasedn("1"));
		assertEquals("ABC", portalLdapConfiguration.getTppGroupBasedn("2"));
		assertEquals("testClient", portalLdapConfiguration.getClientdn());
		assertEquals("password", portalLdapConfiguration.getLdapPassword());
		assertEquals("tppApplication", portalLdapConfiguration.getTppApplicationBasedn());
		assertEquals("https://localhost:8000", portalLdapConfiguration.getUrl());
		assertEquals("cn=dirAdmin", portalLdapConfiguration.getUserDn());
		
	}
	
	@Test
	public void portalLdapConfigSuccessTest() throws Exception {
		portalLdapConfiguration.ldapTemplate();
	}
	
	
}