package com.capgemini.psd2.services.impl;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.naming.Name;
import javax.naming.NamingException;
import javax.naming.directory.BasicAttributes;
import javax.naming.directory.DirContext;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.ldap.core.AttributesMapper;
import org.springframework.ldap.core.DirContextAdapter;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.query.LdapQuery;
import org.springframework.ldap.query.LdapQueryBuilder;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.capgemini.psd2.config.PortalLdapConfiguration;
import com.capgemini.psd2.exception.ClientRegistrationException;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.helper.TppSoftwareHelper;
import com.capgemini.psd2.logger.LoggerUtils;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.mockdata.TppPortalMockData;
import com.capgemini.psd2.service.impl.PingDirectoryServiceImpl;
import com.capgemini.tpp.ldap.client.model.ClientModel;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
public class PingDirectoryServiceImplTest {	

	@InjectMocks
	private PingDirectoryServiceImpl pingDirectoryServiceImpl;
	
	@Mock
	private LdapQuery ldapQuery;
	
	@Mock
	private LdapQueryBuilder ldapQueryBuilder;
	
	@Mock
	private PortalLdapConfiguration portalLdapConfiguration;
	
	@Mock
	private LdapTemplate ldapTemplate;
	
	@Mock
	private TppSoftwareHelper tppSoftwareHelper;
	
	@Mock
	RequestHeaderAttributes attr;
	
	@Mock
	LoggerUtils utils;
	
	
	@Before
	public void setupMock() throws ClientRegistrationException {
		MockitoAnnotations.initMocks(this);
		
		Mockito.when(attr.getTenantId()).thenReturn("PSD2");
	}
	
	@Test
	public void fetchListOfTPPApplicationsTppTest() throws NamingException{
		String tppOrgId="Q5wqFjpnTAeCtDc1Qx";
		String clientSecret="123";
		String clientId = "2Qvi7zAnzjk5eQxH0ircDx";
		String ldapAttr = "uniqueMember";
		String ldapAttr1 = "createTimestamp";
		List<Object> tppApp= new ArrayList<>();
		List<Object> tppDetails =new ArrayList<>();
		LdapQuery ldapQuery1 = TppSoftwareHelper.getLdapQuery("ou=tpp,ou=groups,dc=aspsp,dc=co.uk,dc=bank,dc=com", tppOrgId);
	    LdapQuery ldapQuery2 = TppSoftwareHelper.getLdapQuery("ou=clientAppMapping,ou=groups,dc=aspsp,dc=co.uk,dc=bank,dc=com", clientId);
		
		
		BasicAttributes tppApplication = new BasicAttributes(ldapAttr, "clientId=2Qvi7zAnzjk5eQxH0ircDx,ou=oauthclients,ou=application,dc=aspsp,dc=co.uk,dc=bank,dc=com");
		tppDetails.add(tppApplication);
		BasicAttributes tppApplication1 = new BasicAttributes(ldapAttr1, "20180112121346.113Z");//uniquemember=uniqueMember: cn=Q5wqFjpnTAeCtDc1Qx,ou=tpp,ou=groups,dc=aspsp,dc=co.uk,dc=bank,dc=com, ds-entry-checksum=ds-entry-checksum: 7281377324, entryuuid=entryUUID: 171ed2d2-d957-418b-a291-9f89aefa717d, status=status: Approved, objectclass=objectClass: top, groupOfUniqueNames, appMappingsGroup, modifytimestamp=modifyTimestamp: 20180112105449.670Z, modifiersname=modifiersName: cn=Directory Manager,cn=Root DNs,cn=config, cn=cn: 2Qvi7zAnzjk5eQxH0ircDx, creatorsname=creatorsName: cn=Directory Manager,cn=Root DNs,cn=config, createtimestamp=createTimestamp: 20180112105449.670Z, appid=appId: 115754, subschemasubentry=subschemaSubentry: cn=schema, ssatoken=ssaToken: eyJhbGciOiJSUzI1NiIsImtpZCI6IkVxYzBQSnlJVDEzS3NNQ3Z5aDJyQm9aeHlxWUhuM2J3MllkMlhFdF90R2MiLCJ0eXAiOiJKV1QifQ.eyJpYXQiOjE1MTU0MjI3MDksImlzcyI6Ik9wZW5CYW5raW5nIEx0ZCIsImp0aSI6IjZJbUl6M0RLNndJcHlMdVNPTW5TR00iLCJvYl9yZWdpc3RyeV90b3MiOiJodHRwczovL3JlZ2lzdHJ5Lm9wZW5iYW5raW5nLm9yZy51ay90b3MuaHRtbCIsIm9yZ19jb250YWN0cyI6W10sIm9yZ19jcmVhdGVkX2RhdGUiOm51bGwsIm9yZ19pZCI6Im5CUmNZQWNBQ25naGJHRk9CayIsIm9yZ19qd2tzX2VuZHBvaW50IjoiaHR0cHM6Ly9rZXlzdG9yZS5taXQub3BlbmJhbmtpbmcucWEvbkJSY1lBY0FDbmdoYkdGT0JrL25CUmNZQWNBQ25naGJHRk9Cay5qd2tzIiwib3JnX2p3a3NfcmV2b2tlZF9lbmRwb2ludCI6Imh0dHBzOi8va2V5c3RvcmUubWl0Lm9wZW5iYW5raW5nLnFhL25CUmNZQWNBQ25naGJHRk9Cay9yZXZva2VkL25CUmNZQWNBQ25naGJHRk9Cay5qd2tzIiwib3JnX2xhc3RfbW9kaWZpZWRfZGF0ZSI6bnVsbCwib3JnX25hbWUiOiJCYW5rIG9mIElyZWxhbmQgVFBQIC0gQUlTUCBhbmQgUElTUCIsIm9yZ19zdGF0dXMiOiJBY3RpdmUiLCJvcmdhbmlzYXRpb25fY29tcGV0ZW50X2F1dGhvcml0eV9jbGFpbXMiOnsiYXV0aG9yaXNhdGlvbnMiOlt7Im1lbWJlcl9zdGF0ZSI6IkdCIiwicm9sZXMiOlsiUElTUCIsIkFJU1AiXX1dLCJhdXRob3JpdHlfaWQiOiJGQ0EiLCJyZWdpc3RyYXRpb25faWQiOiJCYW5rb2ZJcmVsYW5kUExDMyIsInN0YXR1cyI6IkFjdGl2ZSJ9LCJzb2Z0d2FyZV9jbGllbnRfZGVzY3JpcHRpb24iOiJUZXN0IERlYyIsInNvZnR3YXJlX2NsaWVudF9pZCI6IjJRdmk3ekFuemprNWVReEgwaXJjRHgiLCJzb2Z0d2FyZV9jbGllbnRfbmFtZSI6IlRlc3QgRGVjIiwic29mdHdhcmVfY2xpZW50X3VyaSI6Im5vdCBpbXBsZW1lbnRlZCIsInNvZnR3YXJlX2Vudmlyb25tZW50IjoibWl0Iiwic29mdHdhcmVfaWQiOiIyUXZpN3pBbnpqazVlUXhIMGlyY0R4Iiwic29mdHdhcmVfandrc19lbmRwb2ludCI6Imh0dHBzOi8va2V5c3RvcmUubWl0Lm9wZW5iYW5raW5nLnFhL25CUmNZQWNBQ25naGJHRk9Cay8yUXZpN3pBbnpqazVlUXhIMGlyY0R4Lmp3a3MiLCJzb2Z0d2FyZV9qd2tzX3Jldm9rZWRfZW5kcG9pbnQiOiJodHRwczovL2tleXN0b3JlLm1pdC5vcGVuYmFua2luZy5xYS9uQlJjWUFjQUNuZ2hiR0ZPQmsvcmV2b2tlZC8yUXZpN3pBbnpqazVlUXhIMGlyY0R4Lmp3a3MiLCJzb2Z0d2FyZV9sb2dvX3VyaSI6Im5vdCBpbXBsZW1lbnRlZCIsInNvZnR3YXJlX21vZGUiOiJUZXN0Iiwic29mdHdhcmVfb25fYmVoYWxmX29mX29yZyI6Im5CUmNZQWNBQ25naGJHRk9CayIsInNvZnR3YXJlX3BvbGljeV91cmkiOiJodHRwczovL2dvb2dsZS5jb20vcG9saWN5Iiwic29mdHdhcmVfcmVkaXJlY3RfdXJpcyI6WyJodHRwczovL2dvb2dsZS5jb20vcmVkaXJlY3QxIiwiaHR0cHM6Ly9nb29nbGUuY29tL3JlZGlyZWN0MiJdLCJzb2Z0d2FyZV9yb2xlcyI6WyJQSVNQIiwiQUlTUCJdLCJzb2Z0d2FyZV90b3NfdXJpIjoiaHR0cHM6Ly9nb29nbGUuY29tL3RvcyIsInNvZnR3YXJlX3ZlcnNpb24iOjU1LjU1fQ.I0WwEZd9SkyrviLAniyMAm9c3l59Noocbzpkg_6j8NZzLUg7Mkv4TZ3vAlNj-MAxyuQrf9meblAbdXMKrzO-XMD_P3COqA0HaJeHVPdz05FmGnbWKCytu_0hkDoR5dsi73GrgUj4R634VUQWcSXdRsgVQp-pFsBlaIqOJBLSrYd5aSF3YByJ9bTVLHlPhvGCcSxn6VxoXENEoSkEHS4Nv_unF2o8jMaC3SBbKmIhx223MxtR_CFwJXPbLK9ZSxIEWwFcB61vsLPXNm6YZmYKxFYk9wjaWAOmxBqNUZ-_ta2cm-FVDosKHDxXDuRa4sbbJ9itn0RZwD4xvBGODl_8aQ");
		tppApplication1.put("ssaToken","eyJhbGciOiJSUzI1NiIsImtpZCI6IkVxYzBQSnlJVDEzS3NNQ3Z5aDJyQm9aeHlxWUhuM2J3MllkMlhFdF90R2MiLCJ0eXAiOiJKV1QifQ.eyJpYXQiOjE1MTU0MjI3MDksImlzcyI6Ik9wZW5CYW5raW5nIEx0ZCIsImp0aSI6IjZJbUl6M0RLNndJcHlMdVNPTW5TR00iLCJvYl9yZWdpc3RyeV90b3MiOiJodHRwczovL3JlZ2lzdHJ5Lm9wZW5iYW5raW5nLm9yZy51ay90b3MuaHRtbCIsIm9yZ19jb250YWN0cyI6W10sIm9yZ19jcmVhdGVkX2RhdGUiOm51bGwsIm9yZ19pZCI6Im5CUmNZQWNBQ25naGJHRk9CayIsIm9yZ19qd2tzX2VuZHBvaW50IjoiaHR0cHM6Ly9rZXlzdG9yZS5taXQub3BlbmJhbmtpbmcucWEvbkJSY1lBY0FDbmdoYkdGT0JrL25CUmNZQWNBQ25naGJHRk9Cay5qd2tzIiwib3JnX2p3a3NfcmV2b2tlZF9lbmRwb2ludCI6Imh0dHBzOi8va2V5c3RvcmUubWl0Lm9wZW5iYW5raW5nLnFhL25CUmNZQWNBQ25naGJHRk9Cay9yZXZva2VkL25CUmNZQWNBQ25naGJHRk9Cay5qd2tzIiwib3JnX2xhc3RfbW9kaWZpZWRfZGF0ZSI6bnVsbCwib3JnX25hbWUiOiJCYW5rIG9mIElyZWxhbmQgVFBQIC0gQUlTUCBhbmQgUElTUCIsIm9yZ19zdGF0dXMiOiJBY3RpdmUiLCJvcmdhbmlzYXRpb25fY29tcGV0ZW50X2F1dGhvcml0eV9jbGFpbXMiOnsiYXV0aG9yaXNhdGlvbnMiOlt7Im1lbWJlcl9zdGF0ZSI6IkdCIiwicm9sZXMiOlsiUElTUCIsIkFJU1AiXX1dLCJhdXRob3JpdHlfaWQiOiJGQ0EiLCJyZWdpc3RyYXRpb25faWQiOiJCYW5rb2ZJcmVsYW5kUExDMyIsInN0YXR1cyI6IkFjdGl2ZSJ9LCJzb2Z0d2FyZV9jbGllbnRfZGVzY3JpcHRpb24iOiJUZXN0IERlYyIsInNvZnR3YXJlX2NsaWVudF9pZCI6IjJRdmk3ekFuemprNWVReEgwaXJjRHgiLCJzb2Z0d2FyZV9jbGllbnRfbmFtZSI6IlRlc3QgRGVjIiwic29mdHdhcmVfY2xpZW50X3VyaSI6Im5vdCBpbXBsZW1lbnRlZCIsInNvZnR3YXJlX2Vudmlyb25tZW50IjoibWl0Iiwic29mdHdhcmVfaWQiOiIyUXZpN3pBbnpqazVlUXhIMGlyY0R4Iiwic29mdHdhcmVfandrc19lbmRwb2ludCI6Imh0dHBzOi8va2V5c3RvcmUubWl0Lm9wZW5iYW5raW5nLnFhL25CUmNZQWNBQ25naGJHRk9Cay8yUXZpN3pBbnpqazVlUXhIMGlyY0R4Lmp3a3MiLCJzb2Z0d2FyZV9qd2tzX3Jldm9rZWRfZW5kcG9pbnQiOiJodHRwczovL2tleXN0b3JlLm1pdC5vcGVuYmFua2luZy5xYS9uQlJjWUFjQUNuZ2hiR0ZPQmsvcmV2b2tlZC8yUXZpN3pBbnpqazVlUXhIMGlyY0R4Lmp3a3MiLCJzb2Z0d2FyZV9sb2dvX3VyaSI6Im5vdCBpbXBsZW1lbnRlZCIsInNvZnR3YXJlX21vZGUiOiJUZXN0Iiwic29mdHdhcmVfb25fYmVoYWxmX29mX29yZyI6Im5CUmNZQWNBQ25naGJHRk9CayIsInNvZnR3YXJlX3BvbGljeV91cmkiOiJodHRwczovL2dvb2dsZS5jb20vcG9saWN5Iiwic29mdHdhcmVfcmVkaXJlY3RfdXJpcyI6WyJodHRwczovL2dvb2dsZS5jb20vcmVkaXJlY3QxIiwiaHR0cHM6Ly9nb29nbGUuY29tL3JlZGlyZWN0MiJdLCJzb2Z0d2FyZV9yb2xlcyI6WyJQSVNQIiwiQUlTUCJdLCJzb2Z0d2FyZV90b3NfdXJpIjoiaHR0cHM6Ly9nb29nbGUuY29tL3RvcyIsInNvZnR3YXJlX3ZlcnNpb24iOjU1LjU1fQ.I0WwEZd9SkyrviLAniyMAm9c3l59Noocbzpkg_6j8NZzLUg7Mkv4TZ3vAlNj-MAxyuQrf9meblAbdXMKrzO-XMD_P3COqA0HaJeHVPdz05FmGnbWKCytu_0hkDoR5dsi73GrgUj4R634VUQWcSXdRsgVQp-pFsBlaIqOJBLSrYd5aSF3YByJ9bTVLHlPhvGCcSxn6VxoXENEoSkEHS4Nv_unF2o8jMaC3SBbKmIhx223MxtR_CFwJXPbLK9ZSxIEWwFcB61vsLPXNm6YZmYKxFYk9wjaWAOmxBqNUZ-_ta2cm-FVDosKHDxXDuRa4sbbJ9itn0RZwD4xvBGODl_8aQ");
		tppApp.add(tppApplication1);
		Mockito.when(ldapTemplate.search(any(LdapQuery.class),any(AttributesMapper.class))).thenReturn(tppDetails).thenReturn(tppApp);
		portalLdapConfiguration.getTppgroupbasedn().put("PSD2", "CMA");
		portalLdapConfiguration.getClientAppgroupbasedn().put("PSD2", "CMA");
		
		Mockito.when(attr.getTenantId()).thenReturn("PSD2");
		
		Mockito.when(portalLdapConfiguration.getTppGroupBasedn("PSD2")).thenReturn("ou=tpp,ou=groups,dc=aspsp,dc=co.uk,dc=bank,dc=com");
		Mockito.when(portalLdapConfiguration.getTppApplicationBasedn()).thenReturn("ou=oauthclients,ou=application,dc=aspsp,dc=co.uk,dc=bank,dc=com");
		Mockito.when(portalLdapConfiguration.getClientAppgroupBasedn("PSD2")).thenReturn("ou=clientAppMapping,ou=groups,dc=aspsp,dc=co.uk,dc=bank,dc=com");
		tppSoftwareHelper.getBasicAttributes(tppApplication, ldapAttr);
		
		
	    assertNotNull(pingDirectoryServiceImpl.fetchListOfTPPApplications(tppOrgId));
	}
	@Test
	public void registerOrUpdateTPPOrgTest(){
		String tppOrgId="Q5wqFjpnTAeCtDc1Qx";
		boolean add = false;
		Mockito.when(portalLdapConfiguration.getTppGroupBasedn("PSD2")).thenReturn("ou=tpp,ou=groups,dc=aspsp,dc=co.uk,dc=bank,dc=com");
		Mockito.when(attr.getTenantId()).thenReturn("PSD2");
		pingDirectoryServiceImpl.registerOrUpdateTPPOrg(tppOrgId, TppPortalMockData.getSSADataElements(), add);
	}
	
	@Test(expected = ClientRegistrationException.class)
	public void registerOrUpdateTPPOrgTest1(){
		String tppOrgId="Q5wqFjpnTAeCtDc1Qx";
		boolean add = false;
		Mockito.when(portalLdapConfiguration.getTppGroupBasedn("PSD2")).thenReturn("ou=tpp,ou=groups,dc=aspsp,dc=co.uk,dc=bank,dc=com");
		Mockito.when(attr.getTenantId()).thenReturn("PSD2");
		pingDirectoryServiceImpl.registerOrUpdateTPPOrg(tppOrgId, TppPortalMockData.getSSADataElement1(), add);
	}
	
	@Test
	public void isTppBlockEnabledTest() throws NamingException{
          String  tppOrgId="Q5wqFjpnTAeCtDc1Qx";
          
		   Mockito.when(portalLdapConfiguration.getTppGroupBasedn("PSD2")).thenReturn("ou=tpp,ou=groups,dc=aspsp,dc=co.uk,dc=bank,dc=com");
			List<Object> tppDetails = new ArrayList<>();
			Mockito.when(ldapTemplate.search(any(LdapQuery.class),any(AttributesMapper.class))).thenReturn(tppDetails);
			String attributeValue = "x-block";
			String ldapAttr = "x-block";
			BasicAttributes tppApplication = new BasicAttributes(ldapAttr, "false");
			
			tppDetails.add(tppApplication);
				
			tppSoftwareHelper.getAttributeValue(tppApplication, attributeValue);
			assertNotNull(pingDirectoryServiceImpl.isTppBlockEnabled(tppOrgId));
		    
	}
	
	@Test
	public void fetchListOfTPPApplicationsTppNullTest() throws NamingException{
		String tppName="cn";
		
		Mockito.when(portalLdapConfiguration.getTppGroupBasedn("PSD2")).thenReturn("ou=tpp,ou=groups,dc=aspsp,dc=co.uk,dc=bank,dc=com");
			
			AttributesMapper<Object> mapper = null;
			List<Object> tppDetails = null;
			Mockito.when(ldapTemplate.search(ldapQuery, mapper)).thenReturn(tppDetails);
		    pingDirectoryServiceImpl.fetchListOfTPPApplications(tppName);
	
	}
	
	@Test
	public void isTPPExistTest() {
		String tppOrgId="Q5wqFjpnTAeCtDc1Qx";
		
	    Mockito.when(portalLdapConfiguration.getTppGroupBasedn("PSD2")).thenReturn("ou=tpp,ou=groups,dc=aspsp,dc=co.uk,dc=bank,dc=com");
	    
		List<Object> tppDetails =new ArrayList<>();
		Mockito.when(ldapTemplate.search(any(LdapQuery.class),any(AttributesMapper.class))).thenReturn(tppDetails);
		
		String attributeValue = "uniqueMember";
		String ldapAttr = "uniqueMember";
		
		
		BasicAttributes tppApplication = new BasicAttributes(ldapAttr, "clientId=2Qvi7zAnzjk5eQxH0ircDx,ou=oauthclients,ou=application,dc=aspsp,dc=co.uk,dc=bank,dc=com");
		
		tppDetails.add(tppApplication);
		
		Mockito.when(portalLdapConfiguration.getTppApplicationBasedn()).thenReturn("ou=oauthclients,ou=application,dc=aspsp,dc=co.uk,dc=bank,dc=com");
		Mockito.when(portalLdapConfiguration.getClientAppgroupBasedn("")).thenReturn("ou=clientAppMapping,ou=groups,dc=aspsp,dc=co.uk,dc=bank,dc=com");
		
		tppSoftwareHelper.getBasicAttributes(tppApplication, attributeValue);
		
		List<Object> tppApp= new ArrayList<>();
		tppApp.add("uniquemember=uniqueMember: cn=Q5wqFjpnTAeCtDc1Qx,ou=tpp,ou=groups,dc=aspsp,dc=co.uk,dc=bank,dc=com, ds-entry-checksum=ds-entry-checksum: 7281377324, entryuuid=entryUUID: 171ed2d2-d957-418b-a291-9f89aefa717d, status=status: approved, objectclass=objectClass: top, groupOfUniqueNames, appMappingsGroup, modifytimestamp=modifyTimestamp: 20180112105449.670Z, modifiersname=modifiersName: cn=Directory Manager,cn=Root DNs,cn=config, cn=cn: 2Qvi7zAnzjk5eQxH0ircDx, creatorsname=creatorsName: cn=Directory Manager,cn=Root DNs,cn=config, createtimestamp=createTimestamp: 20180112105449.670Z, appid=appId: 115754, subschemasubentry=subschemaSubentry: cn=schema, ssatoken=ssaToken: eyJhbGciOiJSUzI1NiIsImtpZCI6IkVxYzBQSnlJVDEzS3NNQ3Z5aDJyQm9aeHlxWUhuM2J3MllkMlhFdF90R2MiLCJ0eXAiOiJKV1QifQ.eyJpYXQiOjE1MTU0MjI3MDksImlzcyI6Ik9wZW5CYW5raW5nIEx0ZCIsImp0aSI6IjZJbUl6M0RLNndJcHlMdVNPTW5TR00iLCJvYl9yZWdpc3RyeV90b3MiOiJodHRwczovL3JlZ2lzdHJ5Lm9wZW5iYW5raW5nLm9yZy51ay90b3MuaHRtbCIsIm9yZ19jb250YWN0cyI6W10sIm9yZ19jcmVhdGVkX2RhdGUiOm51bGwsIm9yZ19pZCI6Im5CUmNZQWNBQ25naGJHRk9CayIsIm9yZ19qd2tzX2VuZHBvaW50IjoiaHR0cHM6Ly9rZXlzdG9yZS5taXQub3BlbmJhbmtpbmcucWEvbkJSY1lBY0FDbmdoYkdGT0JrL25CUmNZQWNBQ25naGJHRk9Cay5qd2tzIiwib3JnX2p3a3NfcmV2b2tlZF9lbmRwb2ludCI6Imh0dHBzOi8va2V5c3RvcmUubWl0Lm9wZW5iYW5raW5nLnFhL25CUmNZQWNBQ25naGJHRk9Cay9yZXZva2VkL25CUmNZQWNBQ25naGJHRk9Cay5qd2tzIiwib3JnX2xhc3RfbW9kaWZpZWRfZGF0ZSI6bnVsbCwib3JnX25hbWUiOiJCYW5rIG9mIElyZWxhbmQgVFBQIC0gQUlTUCBhbmQgUElTUCIsIm9yZ19zdGF0dXMiOiJBY3RpdmUiLCJvcmdhbmlzYXRpb25fY29tcGV0ZW50X2F1dGhvcml0eV9jbGFpbXMiOnsiYXV0aG9yaXNhdGlvbnMiOlt7Im1lbWJlcl9zdGF0ZSI6IkdCIiwicm9sZXMiOlsiUElTUCIsIkFJU1AiXX1dLCJhdXRob3JpdHlfaWQiOiJGQ0EiLCJyZWdpc3RyYXRpb25faWQiOiJCYW5rb2ZJcmVsYW5kUExDMyIsInN0YXR1cyI6IkFjdGl2ZSJ9LCJzb2Z0d2FyZV9jbGllbnRfZGVzY3JpcHRpb24iOiJUZXN0IERlYyIsInNvZnR3YXJlX2NsaWVudF9pZCI6IjJRdmk3ekFuemprNWVReEgwaXJjRHgiLCJzb2Z0d2FyZV9jbGllbnRfbmFtZSI6IlRlc3QgRGVjIiwic29mdHdhcmVfY2xpZW50X3VyaSI6Im5vdCBpbXBsZW1lbnRlZCIsInNvZnR3YXJlX2Vudmlyb25tZW50IjoibWl0Iiwic29mdHdhcmVfaWQiOiIyUXZpN3pBbnpqazVlUXhIMGlyY0R4Iiwic29mdHdhcmVfandrc19lbmRwb2ludCI6Imh0dHBzOi8va2V5c3RvcmUubWl0Lm9wZW5iYW5raW5nLnFhL25CUmNZQWNBQ25naGJHRk9Cay8yUXZpN3pBbnpqazVlUXhIMGlyY0R4Lmp3a3MiLCJzb2Z0d2FyZV9qd2tzX3Jldm9rZWRfZW5kcG9pbnQiOiJodHRwczovL2tleXN0b3JlLm1pdC5vcGVuYmFua2luZy5xYS9uQlJjWUFjQUNuZ2hiR0ZPQmsvcmV2b2tlZC8yUXZpN3pBbnpqazVlUXhIMGlyY0R4Lmp3a3MiLCJzb2Z0d2FyZV9sb2dvX3VyaSI6Im5vdCBpbXBsZW1lbnRlZCIsInNvZnR3YXJlX21vZGUiOiJUZXN0Iiwic29mdHdhcmVfb25fYmVoYWxmX29mX29yZyI6Im5CUmNZQWNBQ25naGJHRk9CayIsInNvZnR3YXJlX3BvbGljeV91cmkiOiJodHRwczovL2dvb2dsZS5jb20vcG9saWN5Iiwic29mdHdhcmVfcmVkaXJlY3RfdXJpcyI6WyJodHRwczovL2dvb2dsZS5jb20vcmVkaXJlY3QxIiwiaHR0cHM6Ly9nb29nbGUuY29tL3JlZGlyZWN0MiJdLCJzb2Z0d2FyZV9yb2xlcyI6WyJQSVNQIiwiQUlTUCJdLCJzb2Z0d2FyZV90b3NfdXJpIjoiaHR0cHM6Ly9nb29nbGUuY29tL3RvcyIsInNvZnR3YXJlX3ZlcnNpb24iOjU1LjU1fQ.I0WwEZd9SkyrviLAniyMAm9c3l59Noocbzpkg_6j8NZzLUg7Mkv4TZ3vAlNj-MAxyuQrf9meblAbdXMKrzO-XMD_P3COqA0HaJeHVPdz05FmGnbWKCytu_0hkDoR5dsi73GrgUj4R634VUQWcSXdRsgVQp-pFsBlaIqOJBLSrYd5aSF3YByJ9bTVLHlPhvGCcSxn6VxoXENEoSkEHS4Nv_unF2o8jMaC3SBbKmIhx223MxtR_CFwJXPbLK9ZSxIEWwFcB61vsLPXNm6YZmYKxFYk9wjaWAOmxBqNUZ-_ta2cm-FVDosKHDxXDuRa4sbbJ9itn0RZwD4xvBGODl_8aQ");
		
		Mockito.when(pingDirectoryServiceImpl.getLdapSearchResult(ldapQuery)).thenReturn(tppApp);
		pingDirectoryServiceImpl.isTPPExist(tppOrgId);
		
	}
		
	@Test(expected = ClientRegistrationException.class)
	public void isTPPExistNullTPPIdTest() {
		String tppOrgId = null;			    
		pingDirectoryServiceImpl.isTPPExist(tppOrgId);		
	}
	
	@Test(expected = ClientRegistrationException.class)
	public void isTPPExistEmptyTPPIdTest() {
		String tppOrgId = "";			    
		pingDirectoryServiceImpl.isTPPExist(tppOrgId);		
	}
	
	@Test
	public void isTPPExistNullTPPDetailsTest() {
		String tppOrgId="Q5wqFjpnTAeCtDc1Qx";
		
	    Mockito.when(portalLdapConfiguration.getTppGroupBasedn("PSD2")).thenReturn("ou=tpp,ou=groups,dc=aspsp,dc=co.uk,dc=bank,dc=com");
	    
		List<Object> tppDetails =new ArrayList<>();
		Mockito.when(ldapTemplate.search(any(LdapQuery.class),any(AttributesMapper.class))).thenReturn(null);
		
		String attributeValue = "uniqueMember";
		String ldapAttr = "uniqueMember";
		
		
		BasicAttributes tppApplication = new BasicAttributes(ldapAttr, "clientId=2Qvi7zAnzjk5eQxH0ircDx,ou=oauthclients,ou=application,dc=aspsp,dc=co.uk,dc=bank,dc=com");
		
		tppDetails.add(tppApplication);
		
		Mockito.when(portalLdapConfiguration.getTppApplicationBasedn()).thenReturn("ou=oauthclients,ou=application,dc=aspsp,dc=co.uk,dc=bank,dc=com");
		Mockito.when(portalLdapConfiguration.getClientAppgroupBasedn("")).thenReturn("ou=clientAppMapping,ou=groups,dc=aspsp,dc=co.uk,dc=bank,dc=com");
		
		tppSoftwareHelper.getBasicAttributes(tppApplication, attributeValue);
		
		List<Object> tppApp= new ArrayList<>();
		tppApp.add("uniquemember=uniqueMember: cn=Q5wqFjpnTAeCtDc1Qx,ou=tpp,ou=groups,dc=aspsp,dc=co.uk,dc=bank,dc=com, ds-entry-checksum=ds-entry-checksum: 7281377324, entryuuid=entryUUID: 171ed2d2-d957-418b-a291-9f89aefa717d, status=status: approved, objectclass=objectClass: top, groupOfUniqueNames, appMappingsGroup, modifytimestamp=modifyTimestamp: 20180112105449.670Z, modifiersname=modifiersName: cn=Directory Manager,cn=Root DNs,cn=config, cn=cn: 2Qvi7zAnzjk5eQxH0ircDx, creatorsname=creatorsName: cn=Directory Manager,cn=Root DNs,cn=config, createtimestamp=createTimestamp: 20180112105449.670Z, appid=appId: 115754, subschemasubentry=subschemaSubentry: cn=schema, ssatoken=ssaToken: eyJhbGciOiJSUzI1NiIsImtpZCI6IkVxYzBQSnlJVDEzS3NNQ3Z5aDJyQm9aeHlxWUhuM2J3MllkMlhFdF90R2MiLCJ0eXAiOiJKV1QifQ.eyJpYXQiOjE1MTU0MjI3MDksImlzcyI6Ik9wZW5CYW5raW5nIEx0ZCIsImp0aSI6IjZJbUl6M0RLNndJcHlMdVNPTW5TR00iLCJvYl9yZWdpc3RyeV90b3MiOiJodHRwczovL3JlZ2lzdHJ5Lm9wZW5iYW5raW5nLm9yZy51ay90b3MuaHRtbCIsIm9yZ19jb250YWN0cyI6W10sIm9yZ19jcmVhdGVkX2RhdGUiOm51bGwsIm9yZ19pZCI6Im5CUmNZQWNBQ25naGJHRk9CayIsIm9yZ19qd2tzX2VuZHBvaW50IjoiaHR0cHM6Ly9rZXlzdG9yZS5taXQub3BlbmJhbmtpbmcucWEvbkJSY1lBY0FDbmdoYkdGT0JrL25CUmNZQWNBQ25naGJHRk9Cay5qd2tzIiwib3JnX2p3a3NfcmV2b2tlZF9lbmRwb2ludCI6Imh0dHBzOi8va2V5c3RvcmUubWl0Lm9wZW5iYW5raW5nLnFhL25CUmNZQWNBQ25naGJHRk9Cay9yZXZva2VkL25CUmNZQWNBQ25naGJHRk9Cay5qd2tzIiwib3JnX2xhc3RfbW9kaWZpZWRfZGF0ZSI6bnVsbCwib3JnX25hbWUiOiJCYW5rIG9mIElyZWxhbmQgVFBQIC0gQUlTUCBhbmQgUElTUCIsIm9yZ19zdGF0dXMiOiJBY3RpdmUiLCJvcmdhbmlzYXRpb25fY29tcGV0ZW50X2F1dGhvcml0eV9jbGFpbXMiOnsiYXV0aG9yaXNhdGlvbnMiOlt7Im1lbWJlcl9zdGF0ZSI6IkdCIiwicm9sZXMiOlsiUElTUCIsIkFJU1AiXX1dLCJhdXRob3JpdHlfaWQiOiJGQ0EiLCJyZWdpc3RyYXRpb25faWQiOiJCYW5rb2ZJcmVsYW5kUExDMyIsInN0YXR1cyI6IkFjdGl2ZSJ9LCJzb2Z0d2FyZV9jbGllbnRfZGVzY3JpcHRpb24iOiJUZXN0IERlYyIsInNvZnR3YXJlX2NsaWVudF9pZCI6IjJRdmk3ekFuemprNWVReEgwaXJjRHgiLCJzb2Z0d2FyZV9jbGllbnRfbmFtZSI6IlRlc3QgRGVjIiwic29mdHdhcmVfY2xpZW50X3VyaSI6Im5vdCBpbXBsZW1lbnRlZCIsInNvZnR3YXJlX2Vudmlyb25tZW50IjoibWl0Iiwic29mdHdhcmVfaWQiOiIyUXZpN3pBbnpqazVlUXhIMGlyY0R4Iiwic29mdHdhcmVfandrc19lbmRwb2ludCI6Imh0dHBzOi8va2V5c3RvcmUubWl0Lm9wZW5iYW5raW5nLnFhL25CUmNZQWNBQ25naGJHRk9Cay8yUXZpN3pBbnpqazVlUXhIMGlyY0R4Lmp3a3MiLCJzb2Z0d2FyZV9qd2tzX3Jldm9rZWRfZW5kcG9pbnQiOiJodHRwczovL2tleXN0b3JlLm1pdC5vcGVuYmFua2luZy5xYS9uQlJjWUFjQUNuZ2hiR0ZPQmsvcmV2b2tlZC8yUXZpN3pBbnpqazVlUXhIMGlyY0R4Lmp3a3MiLCJzb2Z0d2FyZV9sb2dvX3VyaSI6Im5vdCBpbXBsZW1lbnRlZCIsInNvZnR3YXJlX21vZGUiOiJUZXN0Iiwic29mdHdhcmVfb25fYmVoYWxmX29mX29yZyI6Im5CUmNZQWNBQ25naGJHRk9CayIsInNvZnR3YXJlX3BvbGljeV91cmkiOiJodHRwczovL2dvb2dsZS5jb20vcG9saWN5Iiwic29mdHdhcmVfcmVkaXJlY3RfdXJpcyI6WyJodHRwczovL2dvb2dsZS5jb20vcmVkaXJlY3QxIiwiaHR0cHM6Ly9nb29nbGUuY29tL3JlZGlyZWN0MiJdLCJzb2Z0d2FyZV9yb2xlcyI6WyJQSVNQIiwiQUlTUCJdLCJzb2Z0d2FyZV90b3NfdXJpIjoiaHR0cHM6Ly9nb29nbGUuY29tL3RvcyIsInNvZnR3YXJlX3ZlcnNpb24iOjU1LjU1fQ.I0WwEZd9SkyrviLAniyMAm9c3l59Noocbzpkg_6j8NZzLUg7Mkv4TZ3vAlNj-MAxyuQrf9meblAbdXMKrzO-XMD_P3COqA0HaJeHVPdz05FmGnbWKCytu_0hkDoR5dsi73GrgUj4R634VUQWcSXdRsgVQp-pFsBlaIqOJBLSrYd5aSF3YByJ9bTVLHlPhvGCcSxn6VxoXENEoSkEHS4Nv_unF2o8jMaC3SBbKmIhx223MxtR_CFwJXPbLK9ZSxIEWwFcB61vsLPXNm6YZmYKxFYk9wjaWAOmxBqNUZ-_ta2cm-FVDosKHDxXDuRa4sbbJ9itn0RZwD4xvBGODl_8aQ");
		
		Mockito.when(pingDirectoryServiceImpl.getLdapSearchResult(ldapQuery)).thenReturn(tppApp);
		pingDirectoryServiceImpl.isTPPExist(tppOrgId);
		
	}
			
	@Test
	public void rollBackTppOrgRegistrationTest(){
		String tppOrgId="Q5wqFjpnTAeCtDc1Qx";
		Mockito.when(portalLdapConfiguration.getTppGroupBasedn("PSD2")).thenReturn("ou=tpp,ou=groups,dc=aspsp,dc=co.uk,dc=bank,dc=com");
		pingDirectoryServiceImpl.deleteTppOrgRegistration(tppOrgId);
		
	}
	@Test
	public void getClientIDNullTest(){
		String tppOrgId="Q5wqFjpnTAeCtDc1Qx";
		String clientId="2Qvi7zAnzjk5eQxH0ircDx";
		Mockito.when(portalLdapConfiguration.getTppGroupBasedn("PSD2")).thenReturn("ou=tpp,ou=groups,dc=aspsp,dc=co.uk,dc=bank,dc=com");
		List<Object> tppDetails = new ArrayList<>();
		String attributeValue = "uniqueMember";
		String ldapAttr = "uniqueMember";
		BasicAttributes tppApplication = new BasicAttributes(ldapAttr, "clientId=2Qvi7zAnzjk5eQxH0ircDx,ou=oauthclients,ou=application,dc=aspsp,dc=co.uk,dc=bank,dc=com");
		tppDetails.add(tppApplication);
		tppSoftwareHelper.getBasicAttributes(tppApplication, attributeValue);
		Mockito.when(portalLdapConfiguration.getTppApplicationBasedn()).thenReturn("ou=oauthclients,ou=application,dc=aspsp,dc=co.uk,dc=bank,dc=com");
		pingDirectoryServiceImpl.getClientID(tppOrgId);
		
	}
	@Test
	public void getClientIDTest(){
		 String  tppOrgId="Q5wqFjpnTAeCtDc1Qx";
         
		   Mockito.when(portalLdapConfiguration.getTppGroupBasedn("PSD2")).thenReturn("");
			String clientId;
		
			List<Object> tppDetails = new ArrayList<>();
		
			
			Mockito.when(ldapTemplate.search(any(LdapQuery.class),any(AttributesMapper.class))).thenReturn(tppDetails);
			String attributeValue = "uniqueMember";
			String ldapAttr = "uniqueMember";
			BasicAttributes tppApplication = new BasicAttributes(ldapAttr, "ou=oauthclients,ou=application,dc=aspsp,dc=co.uk,dc=bank,dc=com");
			
			tppDetails.add(tppApplication);
		tppSoftwareHelper.getBasicAttributes(tppApplication, attributeValue);
		Mockito.when(portalLdapConfiguration.getTppApplicationBasedn()).thenReturn("ou=oauthclients,ou=application,dc=aspsp,dc=co.uk,dc=bank,dc=com");
		pingDirectoryServiceImpl.getClientID(tppOrgId);
		
	}
	@Test
	public void mapClientAppTest(){
		String clientId="2Qvi7zAnzjk5eQxH0ircDx";
		String appId ="1234";
		String tppOrgId="Q5wqFjpnTAeCtDc1Qx";
		String softwareJwks="https://keystore.openbankingtest.org.uk/0015800000jfQ9aAAE/1i0OveEsyhv7aNX9gQGRig.jwks";
		String ssaToken="eyJhbGciOiJSUzI1NiIsImtpZCI6IkVxYzBQSnlJVDEzS3NNQ3Z5aDJyQm9aeHlxWUhuM2J3MllkMlhFdF90R2MiLCJ0eXAiOiJKV1QifQ.eyJpYXQiOjE1MTU0MjI3MDksImlzcyI6Ik9wZW5CYW5raW5nIEx0ZCIsImp0aSI6IjZJbUl6M0RLNndJcHlMdVNPTW5TR00iLCJvYl9yZWdpc3RyeV90b3MiOiJodHRwczovL3JlZ2lzdHJ5Lm9wZW5iYW5raW5nLm9yZy51ay90b3MuaHRtbCIsIm9yZ19jb250YWN0cyI6W10sIm9yZ19jcmVhdGVkX2RhdGUiOm51bGwsIm9yZ19pZCI6Im5CUmNZQWNBQ25naGJHRk9CayIsIm9yZ19qd2tzX2VuZHBvaW50IjoiaHR0cHM6Ly9rZXlzdG9yZS5taXQub3BlbmJhbmtpbmcucWEvbkJSY1lBY0FDbmdoYkdGT0JrL25CUmNZQWNBQ25naGJHRk9Cay5qd2tzIiwib3JnX2p3a3NfcmV2b2tlZF9lbmRwb2ludCI6Imh0dHBzOi8va2V5c3RvcmUubWl0Lm9wZW5iYW5raW5nLnFhL25CUmNZQWNBQ25naGJHRk9Cay9yZXZva2VkL25CUmNZQWNBQ25naGJHRk9Cay5qd2tzIiwib3JnX2xhc3RfbW9kaWZpZWRfZGF0ZSI6bnVsbCwib3JnX25hbWUiOiJCYW5rIG9mIElyZWxhbmQgVFBQIC0gQUlTUCBhbmQgUElTUCIsIm9yZ19zdGF0dXMiOiJBY3RpdmUiLCJvcmdhbmlzYXRpb25fY29tcGV0ZW50X2F1dGhvcml0eV9jbGFpbXMiOnsiYXV0aG9yaXNhdGlvbnMiOlt7Im1lbWJlcl9zdGF0ZSI6IkdCIiwicm9sZXMiOlsiUElTUCIsIkFJU1AiXX1dLCJhdXRob3JpdHlfaWQiOiJGQ0EiLCJyZWdpc3RyYXRpb25faWQiOiJCYW5rb2ZJcmVsYW5kUExDMyIsInN0YXR1cyI6IkFjdGl2ZSJ9LCJzb2Z0d2FyZV9jbGllbnRfZGVzY3JpcHRpb24iOiJUZXN0IERlYyIsInNvZnR3YXJlX2NsaWVudF9pZCI6IjJRdmk3ekFuemprNWVReEgwaXJjRHgiLCJzb2Z0d2FyZV9jbGllbnRfbmFtZSI6IlRlc3QgRGVjIiwic29mdHdhcmVfY2xpZW50X3VyaSI6Im5vdCBpbXBsZW1lbnRlZCIsInNvZnR3YXJlX2Vudmlyb25tZW50IjoibWl0Iiwic29mdHdhcmVfaWQiOiIyUXZpN3pBbnpqazVlUXhIMGlyY0R4Iiwic29mdHdhcmVfandrc19lbmRwb2ludCI6Imh0dHBzOi8va2V5c3RvcmUubWl0Lm9wZW5iYW5raW5nLnFhL25CUmNZQWNBQ25naGJHRk9Cay8yUXZpN3pBbnpqazVlUXhIMGlyY0R4Lmp3a3MiLCJzb2Z0d2FyZV9qd2tzX3Jldm9rZWRfZW5kcG9pbnQiOiJodHRwczovL2tleXN0b3JlLm1pdC5vcGVuYmFua2luZy5xYS9uQlJjWUFjQUNuZ2hiR0ZPQmsvcmV2b2tlZC8yUXZpN3pBbnpqazVlUXhIMGlyY0R4Lmp3a3MiLCJzb2Z0d2FyZV9sb2dvX3VyaSI6Im5vdCBpbXBsZW1lbnRlZCIsInNvZnR3YXJlX21vZGUiOiJUZXN0Iiwic29mdHdhcmVfb25fYmVoYWxmX29mX29yZyI6Im5CUmNZQWNBQ25naGJHRk9CayIsInNvZnR3YXJlX3BvbGljeV91cmkiOiJodHRwczovL2dvb2dsZS5jb20vcG9saWN5Iiwic29mdHdhcmVfcmVkaXJlY3RfdXJpcyI6WyJodHRwczovL2dvb2dsZS5jb20vcmVkaXJlY3QxIiwiaHR0cHM6Ly9nb29nbGUuY29tL3JlZGlyZWN0MiJdLCJzb2Z0d2FyZV9yb2xlcyI6WyJQSVNQIiwiQUlTUCJdLCJzb2Z0d2FyZV90b3NfdXJpIjoiaHR0cHM6Ly9nb29nbGUuY29tL3RvcyIsInNvZnR3YXJlX3ZlcnNpb24iOjU1LjU1fQ.I0WwEZd9SkyrviLAniyMAm9c3l59Noocbzpkg_6j8NZzLUg7Mkv4TZ3vAlNj-MAxyuQrf9meblAbdXMKrzO-XMD_P3COqA0HaJeHVPdz05FmGnbWKCytu_0hkDoR5dsi73GrgUj4R634VUQWcSXdRsgVQp-pFsBlaIqOJBLSrYd5aSF3YByJ9bTVLHlPhvGCcSxn6VxoXENEoSkEHS4Nv_unF2o8jMaC3SBbKmIhx223MxtR_CFwJXPbLK9ZSxIEWwFcB61vsLPXNm6YZmYKxFYk9wjaWAOmxBqNUZ-_ta2cm-FVDosKHDxXDuRa4sbbJ9itn0RZwD4xvBGODl_8aQ";
		Mockito.when(portalLdapConfiguration.getClientAppgroupBasedn("PSD2")).thenReturn("ou=clientAppMapping,ou=groups,dc=aspsp,dc=co.uk,dc=bank,dc=com");
		Mockito.when(portalLdapConfiguration.getTppGroupBasedn("PSD2")).thenReturn("ou=tpp,ou=groups,dc=aspsp,dc=co.uk,dc=bank,dc=com");
		String publickey="key";
		pingDirectoryServiceImpl.mapClientApp(clientId, appId, tppOrgId, ssaToken,softwareJwks,publickey);
		
	}
			
	@Test
	public void rollBackTPPApplicationAssociationWithTPPTest(){
		String clientId="2Qvi7zAnzjk5eQxH0ircDx";
		String tppOrgId="Q5wqFjpnTAeCtDc1Qx";
		
		Mockito.when(portalLdapConfiguration.getTppGroupBasedn("PSD2")).thenReturn("ou=tpp,ou=groups,dc=aspsp,dc=co.uk,dc=bank,dc=com");
		Mockito.when(portalLdapConfiguration.getClientdn()).thenReturn("clientId=");
		Mockito.when(portalLdapConfiguration.getTppApplicationBasedn()).thenReturn("ou=oauthclients,ou=application,dc=aspsp,dc=co.uk,dc=bank,dc=com");
		DirContextAdapter dx= new DirContextAdapter();
		dx.addAttributeValue("uniqueMember", "uniqueMember");
		Mockito.when(ldapTemplate.lookup(any(Name.class))).thenReturn(dx);
		pingDirectoryServiceImpl.deleteTPPApplicationAssociationWithTPP(tppOrgId, clientId);
		
	}
	
	@Test
	public void rollBackTppOrgUpdation(){
		String tppOrgId="Q5wqFjpnTAeCtDc1Qx";
		
		Mockito.when(portalLdapConfiguration.getTppGroupBasedn("PSD2")).thenReturn("ou=tpp,ou=groups,dc=aspsp,dc=co.uk,dc=bank,dc=com");
	
		List<Object> tppDetails = new ArrayList<>();
		String attributeValue = "uniqueMember";
		String ldapAttr = "uniqueMember";
		BasicAttributes tppApplication = new BasicAttributes(ldapAttr, "uniqueMember");
		tppDetails.add(tppApplication);
		Mockito.when(portalLdapConfiguration.getTppApplicationBasedn()).thenReturn("ou=oauthclients,ou=application,dc=aspsp,dc=co.uk,dc=bank,dc=com");
		Mockito.when(portalLdapConfiguration.getClientAppgroupBasedn("PSD2")).thenReturn("ou=clientAppMapping,ou=groups,dc=aspsp,dc=co.uk,dc=bank,dc=com");
		DirContextAdapter dx= new DirContextAdapter();
		dx.addAttributeValue("uniqueMember", "uniqueMember");
		dx.addAttributeValue("ssaToken", "eyJhbGciOiJSUzI1NiIsImtpZCI6IkVxYzBQSnlJVDEzS3NNQ3Z5aDJyQm9aeHlxWUhuM2J3MllkMlhFdF90R2MiLCJ0eXAiOiJKV1QifQ.eyJpYXQiOjE1MTU0MjI3MDksImlzcyI6Ik9wZW5CYW5raW5nIEx0ZCIsImp0aSI6IjZJbUl6M0RLNndJcHlMdVNPTW5TR00iLCJvYl9yZWdpc3RyeV90b3MiOiJodHRwczovL3JlZ2lzdHJ5Lm9wZW5iYW5raW5nLm9yZy51ay90b3MuaHRtbCIsIm9yZ19jb250YWN0cyI6W10sIm9yZ19jcmVhdGVkX2RhdGUiOm51bGwsIm9yZ19pZCI6Im5CUmNZQWNBQ25naGJHRk9CayIsIm9yZ19qd2tzX2VuZHBvaW50IjoiaHR0cHM6Ly9rZXlzdG9yZS5taXQub3BlbmJhbmtpbmcucWEvbkJSY1lBY0FDbmdoYkdGT0JrL25CUmNZQWNBQ25naGJHRk9Cay5qd2tzIiwib3JnX2p3a3NfcmV2b2tlZF9lbmRwb2ludCI6Imh0dHBzOi8va2V5c3RvcmUubWl0Lm9wZW5iYW5raW5nLnFhL25CUmNZQWNBQ25naGJHRk9Cay9yZXZva2VkL25CUmNZQWNBQ25naGJHRk9Cay5qd2tzIiwib3JnX2xhc3RfbW9kaWZpZWRfZGF0ZSI6bnVsbCwib3JnX25hbWUiOiJCYW5rIG9mIElyZWxhbmQgVFBQIC0gQUlTUCBhbmQgUElTUCIsIm9yZ19zdGF0dXMiOiJBY3RpdmUiLCJvcmdhbmlzYXRpb25fY29tcGV0ZW50X2F1dGhvcml0eV9jbGFpbXMiOnsiYXV0aG9yaXNhdGlvbnMiOlt7Im1lbWJlcl9zdGF0ZSI6IkdCIiwicm9sZXMiOlsiUElTUCIsIkFJU1AiXX1dLCJhdXRob3JpdHlfaWQiOiJGQ0EiLCJyZWdpc3RyYXRpb25faWQiOiJCYW5rb2ZJcmVsYW5kUExDMyIsInN0YXR1cyI6IkFjdGl2ZSJ9LCJzb2Z0d2FyZV9jbGllbnRfZGVzY3JpcHRpb24iOiJUZXN0IERlYyIsInNvZnR3YXJlX2NsaWVudF9pZCI6IjJRdmk3ekFuemprNWVReEgwaXJjRHgiLCJzb2Z0d2FyZV9jbGllbnRfbmFtZSI6IlRlc3QgRGVjIiwic29mdHdhcmVfY2xpZW50X3VyaSI6Im5vdCBpbXBsZW1lbnRlZCIsInNvZnR3YXJlX2Vudmlyb25tZW50IjoibWl0Iiwic29mdHdhcmVfaWQiOiIyUXZpN3pBbnpqazVlUXhIMGlyY0R4Iiwic29mdHdhcmVfandrc19lbmRwb2ludCI6Imh0dHBzOi8va2V5c3RvcmUubWl0Lm9wZW5iYW5raW5nLnFhL25CUmNZQWNBQ25naGJHRk9Cay8yUXZpN3pBbnpqazVlUXhIMGlyY0R4Lmp3a3MiLCJzb2Z0d2FyZV9qd2tzX3Jldm9rZWRfZW5kcG9pbnQiOiJodHRwczovL2tleXN0b3JlLm1pdC5vcGVuYmFua2luZy5xYS9uQlJjWUFjQUNuZ2hiR0ZPQmsvcmV2b2tlZC8yUXZpN3pBbnpqazVlUXhIMGlyY0R4Lmp3a3MiLCJzb2Z0d2FyZV9sb2dvX3VyaSI6Im5vdCBpbXBsZW1lbnRlZCIsInNvZnR3YXJlX21vZGUiOiJUZXN0Iiwic29mdHdhcmVfb25fYmVoYWxmX29mX29yZyI6Im5CUmNZQWNBQ25naGJHRk9CayIsInNvZnR3YXJlX3BvbGljeV91cmkiOiJodHRwczovL2dvb2dsZS5jb20vcG9saWN5Iiwic29mdHdhcmVfcmVkaXJlY3RfdXJpcyI6WyJodHRwczovL2dvb2dsZS5jb20vcmVkaXJlY3QxIiwiaHR0cHM6Ly9nb29nbGUuY29tL3JlZGlyZWN0MiJdLCJzb2Z0d2FyZV9yb2xlcyI6WyJQSVNQIiwiQUlTUCJdLCJzb2Z0d2FyZV90b3NfdXJpIjoiaHR0cHM6Ly9nb29nbGUuY29tL3RvcyIsInNvZnR3YXJlX3ZlcnNpb24iOjU1LjU1fQ.I0WwEZd9SkyrviLAniyMAm9c3l59Noocbzpkg_6j8NZzLUg7Mkv4TZ3vAlNj-MAxyuQrf9meblAbdXMKrzO-XMD_P3COqA0HaJeHVPdz05FmGnbWKCytu_0hkDoR5dsi73GrgUj4R634VUQWcSXdRsgVQp-pFsBlaIqOJBLSrYd5aSF3YByJ9bTVLHlPhvGCcSxn6VxoXENEoSkEHS4Nv_unF2o8jMaC3SBbKmIhx223MxtR_CFwJXPbLK9ZSxIEWwFcB61vsLPXNm6YZmYKxFYk9wjaWAOmxBqNUZ-_ta2cm-FVDosKHDxXDuRa4sbbJ9itn0RZwD4xvBGODl_8aQ");
		
		Mockito.when(ldapTemplate.search(any(LdapQuery.class),any(AttributesMapper.class))).thenReturn(tppDetails);
		
		Mockito.when(ldapTemplate.lookup(any(String.class))).thenReturn(dx);
		
		pingDirectoryServiceImpl.deleteTppOrgUpdation(tppOrgId);
		
	}
	
	@Test(expected = ClientRegistrationException.class)
	public void rollBackTppOrgNullTppDetailsUpdation(){
		String tppOrgId="Q5wqFjpnTAeCtDc1Qx";
		
		Mockito.when(portalLdapConfiguration.getTppGroupBasedn("PSD2")).thenReturn("ou=tpp,ou=groups,dc=aspsp,dc=co.uk,dc=bank,dc=com");
			
		Mockito.when(portalLdapConfiguration.getTppApplicationBasedn()).thenReturn("ou=oauthclients,ou=application,dc=aspsp,dc=co.uk,dc=bank,dc=com");
		Mockito.when(portalLdapConfiguration.getClientAppgroupBasedn("PSD2")).thenReturn("ou=clientAppMapping,ou=groups,dc=aspsp,dc=co.uk,dc=bank,dc=com");
		DirContextAdapter dx= new DirContextAdapter();
		dx.addAttributeValue("uniqueMember", "uniqueMember");
		dx.addAttributeValue("ssaToken", "eyJhbGciOiJSUzI1NiIsImtpZCI6IkVxYzBQSnlJVDEzS3NNQ3Z5aDJyQm9aeHlxWUhuM2J3MllkMlhFdF90R2MiLCJ0eXAiOiJKV1QifQ.eyJpYXQiOjE1MTU0MjI3MDksImlzcyI6Ik9wZW5CYW5raW5nIEx0ZCIsImp0aSI6IjZJbUl6M0RLNndJcHlMdVNPTW5TR00iLCJvYl9yZWdpc3RyeV90b3MiOiJodHRwczovL3JlZ2lzdHJ5Lm9wZW5iYW5raW5nLm9yZy51ay90b3MuaHRtbCIsIm9yZ19jb250YWN0cyI6W10sIm9yZ19jcmVhdGVkX2RhdGUiOm51bGwsIm9yZ19pZCI6Im5CUmNZQWNBQ25naGJHRk9CayIsIm9yZ19qd2tzX2VuZHBvaW50IjoiaHR0cHM6Ly9rZXlzdG9yZS5taXQub3BlbmJhbmtpbmcucWEvbkJSY1lBY0FDbmdoYkdGT0JrL25CUmNZQWNBQ25naGJHRk9Cay5qd2tzIiwib3JnX2p3a3NfcmV2b2tlZF9lbmRwb2ludCI6Imh0dHBzOi8va2V5c3RvcmUubWl0Lm9wZW5iYW5raW5nLnFhL25CUmNZQWNBQ25naGJHRk9Cay9yZXZva2VkL25CUmNZQWNBQ25naGJHRk9Cay5qd2tzIiwib3JnX2xhc3RfbW9kaWZpZWRfZGF0ZSI6bnVsbCwib3JnX25hbWUiOiJCYW5rIG9mIElyZWxhbmQgVFBQIC0gQUlTUCBhbmQgUElTUCIsIm9yZ19zdGF0dXMiOiJBY3RpdmUiLCJvcmdhbmlzYXRpb25fY29tcGV0ZW50X2F1dGhvcml0eV9jbGFpbXMiOnsiYXV0aG9yaXNhdGlvbnMiOlt7Im1lbWJlcl9zdGF0ZSI6IkdCIiwicm9sZXMiOlsiUElTUCIsIkFJU1AiXX1dLCJhdXRob3JpdHlfaWQiOiJGQ0EiLCJyZWdpc3RyYXRpb25faWQiOiJCYW5rb2ZJcmVsYW5kUExDMyIsInN0YXR1cyI6IkFjdGl2ZSJ9LCJzb2Z0d2FyZV9jbGllbnRfZGVzY3JpcHRpb24iOiJUZXN0IERlYyIsInNvZnR3YXJlX2NsaWVudF9pZCI6IjJRdmk3ekFuemprNWVReEgwaXJjRHgiLCJzb2Z0d2FyZV9jbGllbnRfbmFtZSI6IlRlc3QgRGVjIiwic29mdHdhcmVfY2xpZW50X3VyaSI6Im5vdCBpbXBsZW1lbnRlZCIsInNvZnR3YXJlX2Vudmlyb25tZW50IjoibWl0Iiwic29mdHdhcmVfaWQiOiIyUXZpN3pBbnpqazVlUXhIMGlyY0R4Iiwic29mdHdhcmVfandrc19lbmRwb2ludCI6Imh0dHBzOi8va2V5c3RvcmUubWl0Lm9wZW5iYW5raW5nLnFhL25CUmNZQWNBQ25naGJHRk9Cay8yUXZpN3pBbnpqazVlUXhIMGlyY0R4Lmp3a3MiLCJzb2Z0d2FyZV9qd2tzX3Jldm9rZWRfZW5kcG9pbnQiOiJodHRwczovL2tleXN0b3JlLm1pdC5vcGVuYmFua2luZy5xYS9uQlJjWUFjQUNuZ2hiR0ZPQmsvcmV2b2tlZC8yUXZpN3pBbnpqazVlUXhIMGlyY0R4Lmp3a3MiLCJzb2Z0d2FyZV9sb2dvX3VyaSI6Im5vdCBpbXBsZW1lbnRlZCIsInNvZnR3YXJlX21vZGUiOiJUZXN0Iiwic29mdHdhcmVfb25fYmVoYWxmX29mX29yZyI6Im5CUmNZQWNBQ25naGJHRk9CayIsInNvZnR3YXJlX3BvbGljeV91cmkiOiJodHRwczovL2dvb2dsZS5jb20vcG9saWN5Iiwic29mdHdhcmVfcmVkaXJlY3RfdXJpcyI6WyJodHRwczovL2dvb2dsZS5jb20vcmVkaXJlY3QxIiwiaHR0cHM6Ly9nb29nbGUuY29tL3JlZGlyZWN0MiJdLCJzb2Z0d2FyZV9yb2xlcyI6WyJQSVNQIiwiQUlTUCJdLCJzb2Z0d2FyZV90b3NfdXJpIjoiaHR0cHM6Ly9nb29nbGUuY29tL3RvcyIsInNvZnR3YXJlX3ZlcnNpb24iOjU1LjU1fQ.I0WwEZd9SkyrviLAniyMAm9c3l59Noocbzpkg_6j8NZzLUg7Mkv4TZ3vAlNj-MAxyuQrf9meblAbdXMKrzO-XMD_P3COqA0HaJeHVPdz05FmGnbWKCytu_0hkDoR5dsi73GrgUj4R634VUQWcSXdRsgVQp-pFsBlaIqOJBLSrYd5aSF3YByJ9bTVLHlPhvGCcSxn6VxoXENEoSkEHS4Nv_unF2o8jMaC3SBbKmIhx223MxtR_CFwJXPbLK9ZSxIEWwFcB61vsLPXNm6YZmYKxFYk9wjaWAOmxBqNUZ-_ta2cm-FVDosKHDxXDuRa4sbbJ9itn0RZwD4xvBGODl_8aQ");
		
		Mockito.when(ldapTemplate.search(any(LdapQuery.class),any(AttributesMapper.class))).thenReturn(null);
		
		Mockito.when(ldapTemplate.lookup(any(String.class))).thenReturn(dx);
		
		pingDirectoryServiceImpl.deleteTppOrgUpdation(tppOrgId);
		
	}

	@Test
	public void getLdapSearchResult(){
		
		List<Object> tppDetails = new ArrayList<>();
		Object cn="cn";
		tppDetails.add(cn);
		Mockito.when(ldapTemplate.search(any(LdapQuery.class),any(AttributesMapper.class))).thenReturn(tppDetails);
		assertNotNull(pingDirectoryServiceImpl.getLdapSearchResult(ldapQuery));
	}
	
	@Test
	public void associateTPPApplicationWithTPPIfConditionTest()  {
		String tppOrgId = "Q5wqFjpnTAeCtDc1Qx";
		ClientModel clientModel  = new ClientModel();
		clientModel.setClientId("1234");
		String tppDN="abcd";
		 Date date = new Date();
		 String str = String.format("Current Date/Time : %tc", date );
		 str = "21081994";
		 
		Mockito.when(portalLdapConfiguration.getTppGroupBasedn("PSD2")).thenReturn("ou=tpp,ou=groups,dc=aspsp,dc=co.uk,dc=bank,dc=com");
		Mockito.when(portalLdapConfiguration.getClientdn()).thenReturn("clientId=");
		Mockito.when(portalLdapConfiguration.getTppApplicationBasedn()).thenReturn("ou=oauthclients,ou=application,dc=aspsp,dc=co.uk,dc=bank,dc=com");
		
		DirContextAdapter dx= new DirContextAdapter();
		dx.addAttributeValue("uniqueMember", "uniqueMember");
		Mockito.when(ldapTemplate.lookup(any(Name.class))).thenReturn(dx);
		pingDirectoryServiceImpl.associateTPPApplicationWithTPP(tppOrgId, clientModel);
		
	}
	@Test
	public void associateTPPApplicationWithTPPElseConditionTest()  {
		String tppOrgId = "Q5wqFjpnTAeCtDc1Qx";
		ClientModel clientModel  = new ClientModel();
		clientModel.setClientId("1234");
		String tppDN="abcd";
		 Date date = new Date();
		 String str = String.format("Current Date/Time : %tc", date );
		 str = "21081994";
		 
		Mockito.when(portalLdapConfiguration.getTppGroupBasedn("PSD2")).thenReturn("ou=tpp,ou=groups,dc=aspsp,dc=co.uk,dc=bank,dc=com");
		Mockito.when(portalLdapConfiguration.getClientdn()).thenReturn("clientId=");
		Mockito.when(portalLdapConfiguration.getTppApplicationBasedn()).thenReturn("ou=oauthclients,ou=application,dc=aspsp,dc=co.uk,dc=bank,dc=com");
		
		DirContext dx= new DirContextAdapter();
		Mockito.when(ldapTemplate.lookup(any(Name.class))).thenReturn(dx);
		pingDirectoryServiceImpl.associateTPPApplicationWithTPP(tppOrgId, clientModel);
		
	}
	
    @Test
    public void deleteMapClientAppTest(){
	String clientId="2Qvi7zAnzjk5eQxH0ircDx";
	pingDirectoryServiceImpl.deleteMapClientApp(clientId);
   }
    
    @Test
    public void deleteMapClientAppCatchTest(){
    	String clientId="2Qvi7zAnzjk5eQxH0ircDx";
    	String tppCN="abcd";
    	doThrow(ClientRegistrationException.class).when(ldapTemplate).unbind(tppCN);
    	pingDirectoryServiceImpl.deleteMapClientApp(clientId);
    }
	
    @Test(expected = PSD2Exception.class)
    public void testfetchTppAppMappingForClientm() {
    
    	Mockito.when(ldapTemplate.search(any(LdapQuery.class),any(AttributesMapper.class))).thenReturn(null);
    	Mockito.when(attr.getTenantId()).thenReturn("PSD2");
    	Mockito.when(portalLdapConfiguration.getClientAppgroupBasedn("PSD2")).thenReturn("ou=clientAppMapping,ou=groups,dc=aspsp,dc=co.uk,dc=bank,dc=com");
    	pingDirectoryServiceImpl.fetchTppAppMappingForClient("ABC1234");
    }
    
    @Test
    public void testfetchTppAppMappingForClient() {
    
    	List<Object> tppDetails = new ArrayList<>();
		Object cn="cn";
		tppDetails.add(cn);
		Mockito.when(ldapTemplate.search(any(LdapQuery.class),any(AttributesMapper.class))).thenReturn(tppDetails);
    	Mockito.when(attr.getTenantId()).thenReturn("PSD2");
    	Mockito.when(portalLdapConfiguration.getClientAppgroupBasedn("PSD2")).thenReturn("ou=clientAppMapping,ou=groups,dc=aspsp,dc=co.uk,dc=bank,dc=com");
    	pingDirectoryServiceImpl.fetchTppAppMappingForClient("ABC1234");
    }
    
    @Test
    public void testfetchTppAppMappingForClient1() {
    	DirContextAdapter dx= new DirContextAdapter();
    	dx.setUpdateMode(true);
		dx.addAttributeValue("uniqueMember", "uniqueMember");
		Mockito.when(ldapTemplate.lookup(any(Name.class))).thenReturn(dx);
		Mockito.when(ldapTemplate.lookup(any(String.class))).thenReturn(dx);
    	Mockito.when(portalLdapConfiguration.getClientAppgroupBasedn("")).thenReturn("ou=clientAppMapping,ou=groups,dc=aspsp,dc=co.uk,dc=bank,dc=com");
    	pingDirectoryServiceImpl.mapClientAppUpdate("1234", "tpporgid", "public key", "test");;
    }
    
    
    @Test(expected = ClientRegistrationException.class)
    public void testfetchTppAppMappingForClientException() {
    
    	List<Object> tppDetails = new ArrayList<>();
		Object cn="cn";
		tppDetails.add(cn);
		Mockito.when(ldapTemplate.search(any(LdapQuery.class),any(AttributesMapper.class))).thenThrow(ClientRegistrationException.class);
    	Mockito.when(attr.getTenantId()).thenReturn("PSD2");
    	Mockito.when(portalLdapConfiguration.getClientAppgroupBasedn("PSD2")).thenReturn("ou=clientAppMapping,ou=groups,dc=aspsp,dc=co.uk,dc=bank,dc=com");
    	pingDirectoryServiceImpl.fetchTppAppMappingForClient("ABC1234");
    }
	
	@After
	public void tearDown() throws ClientRegistrationException {
		pingDirectoryServiceImpl = null;

	}
}
