package com.capgemini.psd2.services.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.text.ParseException;

import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.exception.ClientRegistrationException;
import com.capgemini.psd2.exceptions.ErrorInfo;
import com.capgemini.psd2.mockdata.TppPortalMockData;
import com.capgemini.psd2.rest.client.sync.RestClientSync;
import com.capgemini.psd2.service.MuleService;
import com.capgemini.psd2.service.PingDirectoryService;
import com.capgemini.psd2.service.PingFederateService;
import com.capgemini.psd2.service.impl.PortalServiceImpl;
import com.capgemini.tpp.dtos.AddApplicationAttributes;
import com.capgemini.tpp.dtos.AddApplicationStageEnum;
import com.capgemini.tpp.dtos.DeleteApplicationStage;
import com.capgemini.tpp.dtos.DeleteApplicationStageEnum;
import com.capgemini.tpp.dtos.TPPDetails;
import com.capgemini.tpp.muleapi.schema.ApisResponse;
import com.capgemini.tpp.muleapi.schema.Application;
import com.capgemini.tpp.muleapi.schema.LoginResponse;
import com.capgemini.tpp.muleapi.schema.ResetSecretResponse;
import com.capgemini.tpp.ssa.model.SSAModel;
import com.nimbusds.jose.JOSEException;

@RunWith(SpringJUnit4ClassRunner.class)
public class PortalServiceImplTest {


	@Mock
	private MuleService muleService;
	

	@Mock
	private PingFederateService pingFederateService;

	@Mock
	private PingDirectoryService pingDirectoryService;

	@InjectMocks
	private PortalServiceImpl portalService;

	@Mock
	HttpServletRequest request;

	@Mock
	HttpServletResponse response;
	
	@Mock
	DeleteApplicationStage deleteApplicationStage;
	
	@Mock
	@Qualifier("tppPortalRestClient")
	private RestClientSync restClient;
	
	@Value("${openbanking.jwks.endpoint}")
	private String obJWKSEndpoint;

	private SSAModel SSADataElements;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		SSADataElements = TppPortalMockData.getSSADataElements();
	}

	
	

	@Test
	public void testCreateTppApplication() {
		try {
			when(pingFederateService.registerTPPApplicationinPF(any(), any()))
					.thenReturn(TppPortalMockData.getClientModel());
			when(muleService.gatewayMuleAuthentication()).thenReturn(TppPortalMockData.getLoginResponse());
			when(muleService.registerTPPApplicationInMule(any(), any(), any(), any()))
					.thenReturn(TppPortalMockData.getNewApplicationMuleApiResponse());
			ApisResponse apisResponse = TppPortalMockData.getApisResponse();
			when(muleService.fetchListOfAllOrgApis(any())).thenReturn(apisResponse);
			when(muleService.fetchListOfTiersForApiVersions(apisResponse.getApis().get(0).getId(),
					apisResponse.getApis().get(0).getVersions().get(0).getId(), TppPortalMockData.getLoginResponse()))
							.thenReturn(TppPortalMockData.getOneTiers());
			doNothing().when(muleService).doApplicationContract(any(), any(), any());
			doNothing().when(pingDirectoryService).associateTPPApplicationWithTPP(any(), any());
			doNothing().when(pingDirectoryService).registerOrUpdateTPPOrg(any(), any(), any(Boolean.class));
			doNothing().when(pingDirectoryService).mapClientApp(any(), any(), any(), any(), any(),any());
			when(pingDirectoryService.isTPPExist(any())).thenReturn(Boolean.TRUE);
			ReflectionTestUtils.setField(portalService, "createContractForAllApis", Boolean.TRUE);
			ReflectionTestUtils.setField(portalService, "isContractRequired", Boolean.TRUE);
			ReflectionTestUtils.setField(portalService, "isDynamicClient", Boolean.FALSE);
			portalService.createTppApplication(SSADataElements, TppPortalMockData.getTppOrgId(),
					TppPortalMockData.getSSAToken());

		} catch (InvalidKeySpecException | NoSuchAlgorithmException e) {
			e.printStackTrace();
		}

	}

	@Test
	public void testCreateTppApplicationTppNotExist() throws Exception {
		when(pingFederateService.registerTPPApplicationinPF(any(), any()))
				.thenReturn(TppPortalMockData.getClientModel());
		when(muleService.gatewayMuleAuthentication()).thenReturn(TppPortalMockData.getLoginResponse());
		when(muleService.registerTPPApplicationInMule(any(), any(), any(), any()))
				.thenReturn(TppPortalMockData.getNewApplicationMuleApiResponse());
		ApisResponse apisResponse = TppPortalMockData.getApisResponse();
		when(muleService.fetchListOfAllOrgApis(any())).thenReturn(apisResponse);
		when(muleService.fetchListOfTiersForApiVersions(apisResponse.getApis().get(0).getId(),
				apisResponse.getApis().get(0).getVersions().get(0).getId(), TppPortalMockData.getLoginResponse()))
						.thenReturn(TppPortalMockData.getOneTiers());
		doNothing().when(muleService).doApplicationContract(any(), any(), any());
		doNothing().when(pingDirectoryService).associateTPPApplicationWithTPP(any(), any());
		doNothing().when(pingDirectoryService).registerOrUpdateTPPOrg(any(), any(), any(Boolean.class));
		doNothing().when(pingDirectoryService).mapClientApp(any(), any(), any(), any(),any(),any());
		when(pingDirectoryService.isTPPExist(any())).thenReturn(Boolean.FALSE);
		ReflectionTestUtils.setField(portalService, "createContractForAllApis", Boolean.FALSE);
		ReflectionTestUtils.setField(portalService, "isContractRequired", Boolean.FALSE);
		ReflectionTestUtils.setField(portalService, "isDynamicClient", Boolean.FALSE);
		portalService.createTppApplication(SSADataElements, TppPortalMockData.getTppOrgId(),
				TppPortalMockData.getSSAToken());
	}

	@Test(expected = ClientRegistrationException.class)
	public void testCreateTppApplicationMapClientAppException() throws Exception {
		when(pingFederateService.registerTPPApplicationinPF(any(), any()))
				.thenReturn(TppPortalMockData.getClientModel());
		when(muleService.gatewayMuleAuthentication()).thenReturn(TppPortalMockData.getLoginResponse());
		when(muleService.registerTPPApplicationInMule(any(), any(), any(), any()))
				.thenReturn(TppPortalMockData.getNewApplicationMuleApiResponse());
		ApisResponse apisResponse = TppPortalMockData.getApisResponse();
		when(muleService.fetchListOfAllOrgApis(any())).thenReturn(apisResponse);
		when(muleService.fetchListOfTiersForApiVersions(apisResponse.getApis().get(0).getId(),
				apisResponse.getApis().get(0).getVersions().get(0).getId(), TppPortalMockData.getLoginResponse()))
						.thenReturn(TppPortalMockData.getOneTiers());
		doNothing().when(muleService).doApplicationContract(any(), any(), any());
		doNothing().when(pingDirectoryService).associateTPPApplicationWithTPP(any(), any());
		doNothing().when(pingDirectoryService).registerOrUpdateTPPOrg(any(), any(), any(Boolean.class));

		doThrow(ClientRegistrationException.class).when(pingDirectoryService).mapClientApp(any(),any(), any(),any(), any(), any());

		when(pingDirectoryService.isTPPExist(any())).thenReturn(Boolean.TRUE);
		ReflectionTestUtils.setField(portalService, "createContractForAllApis", Boolean.FALSE);
		ReflectionTestUtils.setField(portalService, "isContractRequired", Boolean.FALSE);
		ReflectionTestUtils.setField(portalService, "isDynamicClient", Boolean.FALSE);
		doThrow(ClientRegistrationException.class).when(pingFederateService).deletePFAppRegisteration(any());
		portalService.createTppApplication(SSADataElements, TppPortalMockData.getTppOrgId(),
				TppPortalMockData.getSSAToken());
	}

	@Test(expected = ClientRegistrationException.class)
	public void testCreateTppApplicationPFRollbackFlowTest() throws Exception {

		when(pingFederateService.registerTPPApplicationinPF(any(), any()))
				.thenReturn(TppPortalMockData.getClientModel());
		when(muleService.gatewayMuleAuthentication()).thenReturn(TppPortalMockData.getLoginResponse());
		when(muleService.registerTPPApplicationInMule(any(), any(), any(), any()))
				.thenReturn(TppPortalMockData.getNewApplicationMuleApiResponse());
		ApisResponse apisResponse = TppPortalMockData.getApisResponse();
		when(muleService.fetchListOfAllOrgApis(any())).thenReturn(apisResponse);
		when(muleService.fetchListOfTiersForApiVersions(apisResponse.getApis().get(0).getId(),
				apisResponse.getApis().get(0).getVersions().get(0).getId(), TppPortalMockData.getLoginResponse()))
						.thenReturn(TppPortalMockData.getOneTiers());
		doNothing().when(muleService).doApplicationContract(any(), any(), any());
		doNothing().when(pingDirectoryService).associateTPPApplicationWithTPP(any(), any());
		doNothing().when(pingDirectoryService).registerOrUpdateTPPOrg(any(), any(), any(Boolean.class));
		doThrow(ClientRegistrationException.class).when(pingDirectoryService).mapClientApp(any(),any(),any(), any(), any(), any());
		when(pingDirectoryService.isTPPExist(any())).thenReturn(Boolean.TRUE);
		doNothing().when(pingDirectoryService).deleteTPPApplicationAssociationWithTPP(any(), any());
		doNothing().when(pingDirectoryService).deleteTppOrgUpdation(any());
		doNothing().when(muleService).deleteTppAppRegisterationInMule(any(), any());
		doNothing().when(pingFederateService).deletePFAppRegisteration(any());
		ReflectionTestUtils.setField(portalService, "createContractForAllApis", Boolean.FALSE);
		ReflectionTestUtils.setField(portalService, "isContractRequired", Boolean.FALSE);
		ReflectionTestUtils.setField(portalService, "isDynamicClient", Boolean.FALSE);
		portalService.createTppApplication(SSADataElements, TppPortalMockData.getTppOrgId(),
				TppPortalMockData.getSSAToken());

	}

	@Test(expected = ClientRegistrationException.class)
	public void testCreateTppApplicationPFRollbackFlowException() throws Exception {
		when(pingFederateService.registerTPPApplicationinPF(any(), any()))
				.thenReturn(TppPortalMockData.getClientModel());
		when(muleService.gatewayMuleAuthentication()).thenReturn(TppPortalMockData.getLoginResponse());
		when(muleService.registerTPPApplicationInMule(any(), any(), any(), any()))
				.thenReturn(TppPortalMockData.getNewApplicationMuleApiResponse());
		ApisResponse apisResponse = TppPortalMockData.getApisResponse();
		when(muleService.fetchListOfAllOrgApis(any())).thenReturn(apisResponse);
		when(muleService.fetchListOfTiersForApiVersions(apisResponse.getApis().get(0).getId(),
				apisResponse.getApis().get(0).getVersions().get(0).getId(), TppPortalMockData.getLoginResponse()))
						.thenReturn(TppPortalMockData.getOneTiers());
		doNothing().when(muleService).doApplicationContract(any(), any(), any());
		doNothing().when(pingDirectoryService).associateTPPApplicationWithTPP(any(), any());
		doNothing().when(pingDirectoryService).registerOrUpdateTPPOrg(any(), any(), any(Boolean.class));
		doThrow(ClientRegistrationException.class).when(pingDirectoryService).mapClientApp(any(),any(),any(), any(), any(), any());
		when(pingDirectoryService.isTPPExist(any())).thenReturn(Boolean.TRUE);
		doNothing().when(pingDirectoryService).deleteTPPApplicationAssociationWithTPP(any(), any());
		doNothing().when(pingDirectoryService).deleteTppOrgUpdation(any());
		doNothing().when(muleService).deleteTppAppRegisterationInMule(any(), any());
		doThrow(ClientRegistrationException.class).when(pingFederateService).deletePFAppRegisteration(any());
		ReflectionTestUtils.setField(portalService, "createContractForAllApis", Boolean.FALSE);
		ReflectionTestUtils.setField(portalService, "isContractRequired", Boolean.FALSE);
		ReflectionTestUtils.setField(portalService, "isDynamicClient", Boolean.FALSE);
		portalService.createTppApplication(SSADataElements, TppPortalMockData.getTppOrgId(),
				TppPortalMockData.getSSAToken());
	}

	@Test
	public void fetchClientSecretTest() {
		String clientId = "1234";
		String applicationId = "Q5wqFjpnTAeCtDc1Qx";
		LoginResponse loginResponse = new LoginResponse();

		Mockito.when(muleService.gatewayMuleAuthentication()).thenReturn(loginResponse);
		Application application = new Application();
		application.setClientId(clientId);
		Mockito.when(muleService.fetchClientSecret(loginResponse, applicationId)).thenReturn(application);
		assertNotNull(portalService.fetchClientSecret(clientId, applicationId));
	}

	@Test
	public void fetchClientSecret1Test() {
		String clientId = "1234";
		String applicationId = "Q5wqFjpnTAeCtDc1Qx";
		LoginResponse loginResponse = new LoginResponse();

		Mockito.when(muleService.gatewayMuleAuthentication()).thenReturn(loginResponse);
		Application application = new Application();
		application.setClientId("2456");
		Mockito.when(muleService.fetchClientSecret(loginResponse, applicationId)).thenReturn(application);
		portalService.fetchClientSecret(clientId, applicationId);
	}

	

	@Test(expected = ClientRegistrationException.class)
	public void fetchClientSecretNullTest() {
		String clientId = "1234";
		String applicationId = "Q5wqFjpnTAeCtDc1Qx";
		LoginResponse loginResponse = new LoginResponse();

		Mockito.when(muleService.gatewayMuleAuthentication()).thenReturn(loginResponse);
		Mockito.when(muleService.fetchClientSecret(loginResponse, applicationId)).thenReturn(null);
		portalService.fetchClientSecret(clientId, applicationId);
	}

	@Test(expected = ClientRegistrationException.class)
	public void createTppApplicationNullTest() throws InvalidKeySpecException, NoSuchAlgorithmException {
		when(pingFederateService.registerTPPApplicationinPF(any(), any()))
				.thenReturn(TppPortalMockData.getClientModel());
		when(muleService.gatewayMuleAuthentication()).thenReturn(TppPortalMockData.getLoginResponse());
		when(muleService.registerTPPApplicationInMule(any(), any(), any(), any()))
				.thenReturn(TppPortalMockData.getNewApplicationMuleApiResponse());
		ReflectionTestUtils.setField(portalService, "createContractForAllApis", Boolean.TRUE);
		ReflectionTestUtils.setField(portalService, "isDynamicClient", Boolean.FALSE);
		when(muleService.fetchListOfAllOrgApis(any())).thenReturn(null);
		portalService.createTppApplication(SSADataElements, TppPortalMockData.getTppOrgId(),
				TppPortalMockData.getSSAToken());
	}

	@Test
	public void fetchTppDetailsFromSSAToken1Test() {
		SSAModel ssaModel = TppPortalMockData.getSSADataElements();
		TPPDetails tppDetails = portalService.fetchTppDetailsFromSSAToken(ssaModel);
		assertNotNull(tppDetails);
		assertEquals(ssaModel.getSoftware_client_name(), tppDetails.getApplicationName());
		assertEquals(ssaModel.getSoftware_client_id(), tppDetails.getClientId());
	}

	@Test(expected = ClientRegistrationException.class)
	public void rollbackTppApplicationFlow1Test() {
		AddApplicationAttributes addApplicationAttributes = new AddApplicationAttributes();
		portalService.rollbackTppApplicationFlow(addApplicationAttributes);
	}

	@Test(expected = ClientRegistrationException.class)
	public void rollbackTppApplicationFlowTest() {
		AddApplicationAttributes addApplicationAttributes = new AddApplicationAttributes();
		addApplicationAttributes.setStage(AddApplicationStageEnum.PD_TPP_ORG_REG_SUCCESS);
		addApplicationAttributes.setIsTppExist(Boolean.FALSE);
		ReflectionTestUtils.setField(portalService, "isDynamicClient", Boolean.FALSE);
    	portalService.rollbackTppApplicationFlow(addApplicationAttributes);
    }

    
    @Test
	public void resetClientSecretTest(){
		String clientId = "1234";
		String clientSecret = "abcd";
		String applicationId = "Q5wqFjpnTAeCtDc1Qx";
		LoginResponse loginResponse = new LoginResponse();
		
		Mockito.when(muleService.gatewayMuleAuthentication()).thenReturn(loginResponse);
		ResetSecretResponse apiResponse = new ResetSecretResponse();
		Mockito.when(muleService.resetClientSecret(loginResponse, applicationId)).thenReturn(apiResponse );
		assertNotNull(portalService.resetClientSecret(clientId , applicationId ));
	}
    
    @Test(expected = ClientRegistrationException.class)
	public void resetClientSecretNullTest(){
		String clientId = "1234";
		String clientSecret = "abcd";
		String applicationId = "Q5wqFjpnTAeCtDc1Qx";
		LoginResponse loginResponse = new LoginResponse();
		
		Mockito.when(muleService.gatewayMuleAuthentication()).thenReturn(loginResponse);
		Mockito.when(muleService.resetClientSecret(loginResponse, applicationId)).thenReturn(null);
		portalService.resetClientSecret(clientId , applicationId );
	}
   @Test
   public void deleteTppApplicationTest(){
	String clientId="4567";
	String tppOrgId="Q5wqFjpnTAeCtDc1Qx";
	String applicationId = "32";
	portalService.deleteTppApplication(clientId, tppOrgId,applicationId);
  } 
   @Test
   public void deleteTppApplicationFlagTest() throws NamingException{
	   String clientId="4567";
		String tppOrgId="Q5wqFjpnTAeCtDc1Qx";
		String applicationId = "32";	
		portalService.deleteTppApplication(clientId, tppOrgId,applicationId);
   }
   
   @Test()
   public void deleteTppApplicationFlagCatchTest(){
	   String clientId="4567";
		String tppOrgId="Q5wqFjpnTAeCtDc1Qx";
		String applicationId = "32";
		
		ErrorInfo errorInfo = new ErrorInfo();
		deleteApplicationStage.setStage(DeleteApplicationStageEnum.PF_APP_DELETE_FAILURE);
		doThrow(new ClientRegistrationException("404 Not Found", errorInfo )).when(pingFederateService).deletePFAppRegisteration(clientId);
		portalService.deleteTppApplication(clientId, tppOrgId,applicationId);
		
   }
   
   @Test(expected = ClientRegistrationException.class)
   public void deleteTppApplicationFlagCatch1Test(){
	   String clientId="4567";
		String tppOrgId="Q5wqFjpnTAeCtDc1Qx";
		String applicationId = "32";
		
		ErrorInfo errorInfo = new ErrorInfo();
		deleteApplicationStage.setStage(DeleteApplicationStageEnum.PF_APP_DELETE_FAILURE);
		doThrow(new ClientRegistrationException("404 ", errorInfo )).when(pingFederateService).deletePFAppRegisteration(clientId);
		portalService.deleteTppApplication(clientId, tppOrgId,applicationId);
		
   }
   
   @Test
   public void deleteTppApplicationFromMuleAndPDTest(){
	   String clientId="4567";
		String tppOrgId="Q5wqFjpnTAeCtDc1Qx";
		String applicationId = "32";
		DeleteApplicationStage deleteApplicationstage= new DeleteApplicationStage();
		deleteApplicationstage.setStage(DeleteApplicationStageEnum.MULE_APP_DELETE_FAILURE);
		ErrorInfo errorInfo = new ErrorInfo();
		deleteApplicationStage.setStage(DeleteApplicationStageEnum.MULE_APP_DELETE_FAILURE);
		doThrow(new ClientRegistrationException("404 Not Found", errorInfo )).when(pingDirectoryService).deleteMapClientApp(clientId);
		portalService.deleteTppApplicationFromMuleAndPD(clientId, tppOrgId,applicationId,deleteApplicationstage);
		
   }
   @Test(expected = ClientRegistrationException.class)
   public void deleteTppApplicationFromMuleAnd1PDTest(){
	   String clientId="4567";
		String tppOrgId="Q5wqFjpnTAeCtDc1Qx";
		String applicationId = "32";
		DeleteApplicationStage deleteApplicationstage= new DeleteApplicationStage();
		deleteApplicationstage.setStage(DeleteApplicationStageEnum.PD_CLIENT_ASS_DELETE_FAILURE);
		ErrorInfo errorInfo = new ErrorInfo();
		deleteApplicationStage.setStage(DeleteApplicationStageEnum.PD_CLIENT_ASS_DELETE_FAILURE);
		doThrow(new ClientRegistrationException("404 Not Found", errorInfo )).when(pingDirectoryService).deleteTPPApplicationAssociationWithTPP(tppOrgId, clientId);
		portalService.deleteTppApplicationFromMuleAndPD(clientId, tppOrgId,applicationId,deleteApplicationstage);
		
   }  
   
   @Test
	public void testGetOBPublicKey(){
		String OBPublicKey=TppPortalMockData.getOBPublicKey();
		when(restClient.callForGet(any(),any(),any())).thenReturn(OBPublicKey);
		assertEquals(OBPublicKey, portalService.getOBPublicKey("https://keystore-stable.openbanking.xyz/keystore/openbanking.jwks"));
	
   }
   

	@Test(expected=Exception.class)
	public void decodeAndVerifyTokenTest() throws InvalidKeySpecException, NoSuchAlgorithmException, ParseException, JOSEException {
		portalService.decodeAndVerifyToken("testString");
	}
	
	@Test(expected=Exception.class)
	public void verifyTokenTest() throws InvalidKeySpecException, NoSuchAlgorithmException, ParseException, JOSEException {
		portalService.verifyToken("testString");
	}	
	
}