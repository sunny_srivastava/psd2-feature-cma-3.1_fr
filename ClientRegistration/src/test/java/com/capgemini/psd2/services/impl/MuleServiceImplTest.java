package com.capgemini.psd2.services.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.config.MuleClientRegConfig;
import com.capgemini.psd2.exception.ClientRegistrationException;
import com.capgemini.psd2.mockdata.TppPortalMockData;
import com.capgemini.psd2.rest.client.sync.RestClientSync;
import com.capgemini.psd2.service.impl.MuleServiceImpl;
import com.capgemini.tpp.muleapi.schema.ApisResponse;
import com.capgemini.tpp.muleapi.schema.LoginResponse;
import com.capgemini.tpp.muleapi.schema.NewApplicationMuleApiResponse;
import com.capgemini.tpp.muleapi.schema.Tiers;

@RunWith(SpringJUnit4ClassRunner.class)
public class MuleServiceImplTest {

	@Mock
	private RestClientSync restClient;

	@InjectMocks
	private MuleServiceImpl muleService;

	@Mock
	MuleClientRegConfig muleClientRegConfig;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		MuleClientRegConfig muleClientRegConfig = new MuleClientRegConfig();
		muleClientRegConfig.setUser("nisargd_boi");
		muleClientRegConfig.setPassword("Nisar12345");
		muleClientRegConfig.setLoginUrl("https://anypoint.mulesoft.com/accounts/login");
		muleClientRegConfig.setApplicationUrl("https://anypoint.mulesoft.com/applications");
		muleClientRegConfig.setApiUrl("https://anypoint.mulesoft.com/apis");
		muleClientRegConfig.setApiContractUrl("https://anypoint.mulesoft.com/applications/{applicationId}/contracts");
		muleClientRegConfig.setApplicationRollBackUrl("https://anypoint.mulesoft.com/applications/{applicationId}");
		muleClientRegConfig.setTierUrl("https://anypoint.mulesoft.com/apis/{apiId}/versions/{apiVersionId}/tiers");
		muleClientRegConfig.setMuleContractApi(TppPortalMockData.getSelectApisForContract());
		ReflectionTestUtils.setField(muleService, "muleClientRegConfig", muleClientRegConfig);

	}

	@Test
	public void testGatewayMuleAuthentication() {
		LoginResponse loginResponseExpected = TppPortalMockData.getLoginResponse();
		when(restClient.callForPost(any(), any(), any(), any())).thenReturn(loginResponseExpected);
		LoginResponse loginResponseActual = muleService.gatewayMuleAuthentication();
		assertEquals(loginResponseExpected.getAccessToken(), loginResponseActual.getAccessToken());
		assertEquals(loginResponseExpected.getTokenType(), loginResponseExpected.getTokenType());
	}

	@Test(expected = ClientRegistrationException.class)
	public void testRecoverGatewayMuleAuthentication() {
		Exception e = new Exception();
		muleService.recoverGatewayMuleAuthentication(e);
	}

	@Test(expected = ClientRegistrationException.class)
	public void testRecoverRegisterTPPApplicationInMule() {
		Exception e = new Exception();
		muleService.recoverRegisterTPPApplicationInMule(e);
	}

	@Test(expected = ClientRegistrationException.class)
	public void testRecoverFetchListOfTiersForApiVersions() {
		Exception e = new Exception();
		muleService.recoverFetchListOfTiersForApiVersions(e);
	}

	@Test(expected = ClientRegistrationException.class)
	public void testRecoverFetchListOfAllOrgApis() {
		Exception e = new Exception();
		muleService.recoverFetchListOfAllOrgApis(e);
	}

	@Test(expected = ClientRegistrationException.class)
	public void testRecoverDoApplicationContract() {
		Exception e = new Exception();
		muleService.recoverDoApplicationContract(e);
	}

	@Test(expected = ClientRegistrationException.class)
	public void testRecoverRollBackMuleAppRegisteration() {
		Exception e = new Exception();
		muleService.recoverDeleteMuleAppRegisteration(e);
	}

	@Test
	public void testRegisterTPPApplicationInMule() {
		NewApplicationMuleApiResponse newApplicationMuleApiResponseExpected = TppPortalMockData
				.getNewApplicationMuleApiResponse();
		when(restClient.callForPost(any(), any(), any(), any())).thenReturn(newApplicationMuleApiResponseExpected);
		NewApplicationMuleApiResponse newApplicationMuleApiResponse = muleService.registerTPPApplicationInMule(
				TppPortalMockData.getClientModel(), TppPortalMockData.getSSADataElements(),
				TppPortalMockData.getClientSecret(), TppPortalMockData.getLoginResponse());
		assertEquals(newApplicationMuleApiResponseExpected.getClientId(), newApplicationMuleApiResponse.getClientId());
		assertEquals(newApplicationMuleApiResponseExpected.getClientSecret(),
				newApplicationMuleApiResponse.getClientSecret());
		assertEquals(newApplicationMuleApiResponseExpected.getName(), newApplicationMuleApiResponse.getName());
	}

	@Test
	public void testFetchListOfAllOrgApis() {
		ApisResponse apisResponseExpected = TppPortalMockData.getApisResponse();
		when(restClient.callForGet(any(), any(), any())).thenReturn(apisResponseExpected);
		ApisResponse apisResponse = muleService.fetchListOfAllOrgApis(TppPortalMockData.getLoginResponse());
		assertEquals(apisResponseExpected.getApis().get(0).getId(), apisResponse.getApis().get(0).getId());
		assertEquals(apisResponseExpected.getTotal(), apisResponse.getTotal());
		assertEquals(apisResponseExpected.getApis().get(0).getName(), apisResponse.getApis().get(0).getName());
		assertEquals(apisResponseExpected.getApis().get(0).getVersions().get(0).getApiId(),
				apisResponse.getApis().get(0).getVersions().get(0).getApiId());
		assertEquals(apisResponseExpected.getApis().get(0).getVersions().get(0).getId(),
				apisResponse.getApis().get(0).getVersions().get(0).getId());
	}

	@Test(expected = ClientRegistrationException.class)
	public void testFetchListOfTiersForApiVersionsMoreThan1TierException() {
		when(restClient.callForGet(any(), any(), any())).thenReturn(TppPortalMockData.getMoreThanOneTiers());
		muleService.fetchListOfTiersForApiVersions(TppPortalMockData.getApisResponse().getApis().get(0).getId(),
				TppPortalMockData.getApisResponse().getApis().get(0).getVersions().get(0).getId(),
				TppPortalMockData.getLoginResponse());
	}

	@Test
	public void testFetchListOfTiersForApiVersions() {
		Tiers tiersExpected = TppPortalMockData.getOneTiers();
		when(restClient.callForGet(any(), any(), any())).thenReturn(tiersExpected);
		Tiers tiers = muleService.fetchListOfTiersForApiVersions(
				TppPortalMockData.getApisResponse().getApis().get(0).getId(),
				TppPortalMockData.getApisResponse().getApis().get(0).getVersions().get(0).getId(),
				TppPortalMockData.getLoginResponse());
		assertEquals(tiersExpected.getTiers().get(0).getId(), tiers.getTiers().get(0).getId());
		assertEquals(tiersExpected.getTiers().get(0).getApiVersionId(), tiers.getTiers().get(0).getApiVersionId());
		assertEquals(tiersExpected.getTiers().get(0).getName(), tiers.getTiers().get(0).getName());
	}

	@Test
	public void testRollBackTppAppRegisterationInMule() {
		when(restClient.callForDelete(any(), any(), any())).thenReturn(Void.class);
	}

	@Test
	public void doApplicationContractForAllApis() {
		when(restClient.callForGet(any(), any(), any())).thenReturn(TppPortalMockData.getOneTiers());
		when(restClient.callForPost(any(), any(), any(), any())).thenReturn(Void.class);
		muleService.doApplicationContract(TppPortalMockData.getApisResponse(),
				Integer.valueOf(TppPortalMockData.getNewApplicationMuleApiResponse().getId()), TppPortalMockData.getLoginResponse());
	}

	@Test
	public void doApplicationContractForSelectedApis() {
		when(restClient.callForGet(any(), any(), any())).thenReturn(TppPortalMockData.getOneTiers());
		when(restClient.callForPost(any(), any(), any(), any())).thenReturn(Void.class);
		muleService.doApplicationContract(null,
				Integer.valueOf(TppPortalMockData.getNewApplicationMuleApiResponse().getId()), TppPortalMockData.getLoginResponse());
	}
	
	@Test(expected=ClientRegistrationException.class)
	public void doApplicationContractForSelectedApisException() {
		when(restClient.callForGet(any(), any(), any())).thenReturn(TppPortalMockData.getOneTiers());
		when(restClient.callForPost(any(), any(), any(), any())).thenReturn(Void.class);
		muleClientRegConfig.setMuleContractApi(null);
		ReflectionTestUtils.setField(muleService, "muleClientRegConfig", muleClientRegConfig);
		muleService.doApplicationContract(null,
				Integer.valueOf(TppPortalMockData.getNewApplicationMuleApiResponse().getId()), TppPortalMockData.getLoginResponse());
	}
	
	@Test
	public void doApplicationContractForAllApisNullVersionList() {
		when(restClient.callForGet(any(), any(), any())).thenReturn(TppPortalMockData.getOneTiers());
		when(restClient.callForPost(any(), any(), any(), any())).thenReturn(Void.class);
		muleService.doApplicationContract(TppPortalMockData.getApisResponseNullVersionList(),
				Integer.valueOf(TppPortalMockData.getNewApplicationMuleApiResponse().getId()), TppPortalMockData.getLoginResponse());
	}

	@Test
	public void doApplicationContractNoTiers() {
		when(restClient.callForGet(any(), any(), any())).thenReturn(TppPortalMockData.getNoTiers());
		when(restClient.callForPost(any(), any(), any(), any())).thenReturn(Void.class);
		muleService.doApplicationContract(TppPortalMockData.getApisResponse(),
				Integer.valueOf(TppPortalMockData.getNewApplicationMuleApiResponse().getId()), TppPortalMockData.getLoginResponse());
	}

	@Test
	public void fetchClientSecretTest() {
		String applicationId = "1234";
		LoginResponse loginResponse = new LoginResponse();
		String url = "www.abcd.com\\applicationId";
		Mockito.when(muleClientRegConfig.getShowSecretUrl()).thenReturn(url);
		muleService.fetchClientSecret(loginResponse, applicationId);
	}

	@Test
	public void resetClientSecretTest() {
		String applicationId = "1234";
		LoginResponse loginResponse = new LoginResponse();
		String url = "www.abcd.com\\applicationId\\secret\reset";
		Mockito.when(muleClientRegConfig.getResetSecretUrl()).thenReturn(url);
		muleService.resetClientSecret(loginResponse, applicationId);
	}

	@Test(expected = ClientRegistrationException.class)
	public void recoverRollBackMuleAppRegisterationTest() {
		Exception e = new Exception();
		muleService.recoverDeleteMuleAppRegisteration(e);
	}

	@Test
	public void rollBackTppAppRegisterationInMuleTest() {
		String applicationId = "1234";
		String muleRollBackUrl = "www.abcd.com\\applicationId";
		Mockito.when(muleClientRegConfig.getApplicationRollBackUrl()).thenReturn(muleRollBackUrl);
		LoginResponse loginResponse = new LoginResponse();
		muleService.deleteTppAppRegisterationInMule(loginResponse, applicationId);
	}

}
