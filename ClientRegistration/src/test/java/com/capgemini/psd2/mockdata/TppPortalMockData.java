package com.capgemini.psd2.mockdata;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.capgemini.psd2.jwt.JWTReader;
import com.capgemini.tpp.dtos.ApiDetail;
import com.capgemini.tpp.dtos.TPPDetails;
import com.capgemini.tpp.dtos.ThirdPartyProvidersDetails;
import com.capgemini.tpp.ldap.client.model.ClientAuth;
import com.capgemini.tpp.ldap.client.model.ClientModel;
import com.capgemini.tpp.ldap.client.model.DefaultAccessTokenManagerRef;
import com.capgemini.tpp.muleapi.schema.Api;
import com.capgemini.tpp.muleapi.schema.ApisResponse;
import com.capgemini.tpp.muleapi.schema.LoginResponse;
import com.capgemini.tpp.muleapi.schema.NewApplicationMuleApiResponse;
import com.capgemini.tpp.muleapi.schema.Tier;
import com.capgemini.tpp.muleapi.schema.Tiers;
import com.capgemini.tpp.muleapi.schema.Version;
import com.capgemini.tpp.ob.model.OBThirdPartyProviders;
import com.capgemini.tpp.ob.model.OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10;
import com.capgemini.tpp.ob.model.OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations;
import com.capgemini.tpp.ob.model.OBThirdPartyProvidersUrnopenbankingsoftwarestatement10;
import com.capgemini.tpp.ob.model.OBThirdPartyProvidersUrnopenbankingsoftwarestatement10SoftwareStatements;
import com.capgemini.tpp.ob.model.Organisation;
import com.capgemini.tpp.ssa.model.SSAModel;


public class TppPortalMockData {

	public static String getTokenSigningKey() {
		return "123456789abcdef";
	}

	public static String getJweDecryptionKey() {
		return "abcdef1234567890";
	}

	/*public static JwtToken getJwtToken(PTCInfo user) {
		return SessionCookieUtils.createSessionJwtToken(user);
	}

	public static String getJwtTokenString(PTCInfo ptcInfo){
		return createSessionJwtToken(ptcInfo).getToken();
	}
	
	public static AccessJwtToken createSessionJwtToken(PTCInfo ptcInfo) {
		DateTime currentTime = new DateTime();
		JWTClaimsSet jwtClaims = new JWTClaimsSet.Builder().issuer("http://tppportal.com")
				.claim(ClientRegistarionConstants.PTC_NAME_KEY, ptcInfo.getPtcName())
				.claim(ClientRegistarionConstants.TPP_ASSOCIATIONS_LIST, ptcInfo.getOrganizationIds())
				.claim(ClientRegistarionConstants.PTC_ID_KEY, ptcInfo.getPtcId())
				.claim(ClientRegistarionConstants.CORELATION_ID, new Date().getTime())
				.claim(ClientRegistarionConstants.STATE, ClientRegistarionConstants.FALSE)
				.expirationTime(currentTime.plusSeconds(1800).toDate())
				.notBeforeTime(currentTime.toDate()).issueTime(currentTime.toDate()).jwtID(UUID.randomUUID().toString())
				.build();


		JWEObject jweObject = null;
		String jweString = null;
		// This block encrypts Session Cookie JWT Token with JWE
		try {
			// Sign JWT token with signing key
			JWSSigner signer = new MACSigner("2K4M5N7Q8R9TBUCVEXFYG2J3K4N6P7Q9SATBUDWEXFZH2J3M5N6P8R9SAUqwerty");
			SignedJWT signedJWT = new SignedJWT(new JWSHeader(JWSAlgorithm.HS512), jwtClaims);
			signedJWT.sign(signer);
			signedJWT.getParsedString();

			// Encrpty JWT token
			JWEHeader header = new JWEHeader(JWEAlgorithm.DIR, EncryptionMethod.A128GCM);
			Payload payload = new Payload(signedJWT);
			jweObject = new JWEObject(header, payload);
			byte[] key = SessionCookieUtils.getJweDecryptionKey("GmnfWICOPrsxsxmb");
			jweObject.encrypt(new DirectEncrypter(key));
			jweString = jweObject.serialize();
		} catch (KeyLengthException e) {
			throw ClientRegistrationException.populateDCRException(e.getMessage(), ClientRegistrationErrorCodeEnum.TPP_ERROR_GENRIC);
		} catch (JOSEException e) {
			throw ClientRegistrationException.populateDCRException(e.getMessage(), ClientRegistrationErrorCodeEnum.TPP_ERROR_GENRIC);
		} catch (Exception e) {
			throw ClientRegistrationException.populateDCRException(e.getMessage(), ClientRegistrationErrorCodeEnum.TPP_ERROR_GENRIC);
		}

		return new AccessJwtToken(jweString);
	}*/
	
	public static ThirdPartyProvidersDetails getThirdPartyDetails() {
		ThirdPartyProvidersDetails thirdPartyProvidersDetail = new ThirdPartyProvidersDetails();
		thirdPartyProvidersDetail.setOrganizationName("Bank Of Ireland-PISP");
		thirdPartyProvidersDetail.setTppOrganizationId("20232343");
		thirdPartyProvidersDetail.setTppMemberStateCompetentAuthority("GB");
		return thirdPartyProvidersDetail;
	}

	public static List<String> getTppOrgList() {
		List<String> tppOrgIdList = new ArrayList<>();
		tppOrgIdList.add("Q5wqFjpnTAeCtDc1Qx");
		return tppOrgIdList;
	}

	public static String getTppOrgId() {
		return "Q5wqFjpnTAeCtDc1Qx";
	}

	public static String getClientId() {
		return "6di2De88s8D2zVXgyxm0b2";
	}

	public static String getOBKeyId() {
		return "xl16BDxw57JN-3PtvrmyA-zWTgM";
	}

	public static String getTokenUrl() {
		return "https://auth.mit.openbanking.qa/as/token.oauth2";
	}

	public static String getScopeValue() {
		return "TPPReadAll";
	}

	public static String getAccessToken() {
		return "eyJhbGciOiJSUzUxMiIsImtpZCI6IjEifQ.eyJzY29wZSI6WyJUUFBSZWFkQWxsIl0sImNsaWVudF9pZCI6IjZkaTJEZTg4czhEMnpWWGd5eG0wYjIiLCJleHAiOjE1MTUwNTM4MDR9.ar1dtABYsBhGueUI0OlvRHfNL9XdRbBe51GKb3StvBlKp1dEjiVi6wp6a0PEp8Hp1HFLp1wfBPiOcgjHkJ6G_AFTiySsS8VFPDQBL7bX79eBb_uQuI80H994czLf4TOcAZ0Yjw7S_6z2d_QdKFLTC41xmHbUMJlvfVVcOzMPcSzmCcVM-qyE7UImXLdAeDVx7ieMrl8V_Rpy2nqO832xBb9mLprcdUcVmuGbX7ScYlCwUQaSXSSRfR4_FBVDUle44xhyBKsU5vjAH8TeC6__RxanyF-M1YCPAZI6sJcq6_h0lLftQzTnwPaKY4hyHQmBwIaLzyLWOmJeZL4xYeG9tw";
	}

	public static String getClientSecret() {
		return "12345678";
	}

	public static OBThirdPartyProviders getOBThirdPartyProvidersDetails() {
		OBThirdPartyProviders thirdPartyProviders = new OBThirdPartyProviders();
		thirdPartyProviders.setId("123242");
		Organisation urnopenbankingorganisation10 = new Organisation();
		urnopenbankingorganisation10.setObOrganisationId("obOrganisationId");
		urnopenbankingorganisation10.setOrganisationCommonName("organisationCommonName");
		thirdPartyProviders.setUrnopenbankingorganisation10(urnopenbankingorganisation10);
		OBThirdPartyProvidersUrnopenbankingsoftwarestatement10 urnopenbankingsoftwarestatement10 = new OBThirdPartyProvidersUrnopenbankingsoftwarestatement10();
		List<OBThirdPartyProvidersUrnopenbankingsoftwarestatement10SoftwareStatements> softwareStatements = new ArrayList<>();
		OBThirdPartyProvidersUrnopenbankingsoftwarestatement10SoftwareStatements softwareStatement = new OBThirdPartyProvidersUrnopenbankingsoftwarestatement10SoftwareStatements();
		softwareStatement.setClientId("clientId");
		softwareStatements.add(softwareStatement);
		urnopenbankingsoftwarestatement10.setSoftwareStatements(softwareStatements);
		thirdPartyProviders.setUrnopenbankingsoftwarestatement10(urnopenbankingsoftwarestatement10);
		OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10 urnopenbankingcompetentauthorityclaims10 = new OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10();
		urnopenbankingcompetentauthorityclaims10.setAuthorityId("authorityId");
		List<OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations> authorisations = new ArrayList<>();
		OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations authorisation = new OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations();
		authorisation.setPsd2Role("AISP");
		authorisations.add(authorisation);
		urnopenbankingcompetentauthorityclaims10.setAuthorisations(authorisations);
		thirdPartyProviders.setUrnopenbankingcompetentauthorityclaims10(urnopenbankingcompetentauthorityclaims10);
		return thirdPartyProviders;
	}

	public static LoginResponse getLoginResponse() {
		LoginResponse loginResponse = new LoginResponse();
		loginResponse.setAccessToken("e25b7faf-e605-4bf6-800c-e52719a96d21");
		loginResponse.setRedirectUrl("/home/");
		loginResponse.setTokenType("bearer");
		return loginResponse;
	}

	public static ClientModel getClientModel() {
		ClientModel clientModel = new ClientModel();
		clientModel.setClientId("2Qvi7zAnzjk5eQxH0ircDx");
		clientModel.setDescription("Test Dec");
		clientModel.setName("Test Dec");
		clientModel.setBypassApprovalPage(Boolean.toString(Boolean.TRUE));
		DefaultAccessTokenManagerRef defaultAccessTokenManagerRef = new DefaultAccessTokenManagerRef();
		defaultAccessTokenManagerRef.setId("INTREFATM");
		clientModel.setDefaultAccessTokenManagerRef(defaultAccessTokenManagerRef);
		ClientAuth clientAuth = new ClientAuth();
		clientAuth.setSecret(getClientSecret());
		clientModel.setClientAuth(clientAuth);
		return clientModel;
	}

	public static SSAModel getSSADataElements() {
		String token = "eyJhbGciOiJSUzI1NiIsImtpZCI6IkVxYzBQSnlJVDEzS3NNQ3Z5aDJyQm9aeHlxWUhuM2J3MllkMlhFdF90R2MiLCJ0eXAiOiJKV1QifQ.eyJpYXQiOjE1MTU0MjI3MDksImlzcyI6Ik9wZW5CYW5raW5nIEx0ZCIsImp0aSI6IjZJbUl6M0RLNndJcHlMdVNPTW5TR00iLCJvYl9yZWdpc3RyeV90b3MiOiJodHRwczovL3JlZ2lzdHJ5Lm9wZW5iYW5raW5nLm9yZy51ay90b3MuaHRtbCIsIm9yZ19jb250YWN0cyI6W10sIm9yZ19jcmVhdGVkX2RhdGUiOm51bGwsIm9yZ19pZCI6Im5CUmNZQWNBQ25naGJHRk9CayIsIm9yZ19qd2tzX2VuZHBvaW50IjoiaHR0cHM6Ly9rZXlzdG9yZS5taXQub3BlbmJhbmtpbmcucWEvbkJSY1lBY0FDbmdoYkdGT0JrL25CUmNZQWNBQ25naGJHRk9Cay5qd2tzIiwib3JnX2p3a3NfcmV2b2tlZF9lbmRwb2ludCI6Imh0dHBzOi8va2V5c3RvcmUubWl0Lm9wZW5iYW5raW5nLnFhL25CUmNZQWNBQ25naGJHRk9Cay9yZXZva2VkL25CUmNZQWNBQ25naGJHRk9Cay5qd2tzIiwib3JnX2xhc3RfbW9kaWZpZWRfZGF0ZSI6bnVsbCwib3JnX25hbWUiOiJCYW5rIG9mIElyZWxhbmQgVFBQIC0gQUlTUCBhbmQgUElTUCIsIm9yZ19zdGF0dXMiOiJBY3RpdmUiLCJvcmdhbmlzYXRpb25fY29tcGV0ZW50X2F1dGhvcml0eV9jbGFpbXMiOnsiYXV0aG9yaXNhdGlvbnMiOlt7Im1lbWJlcl9zdGF0ZSI6IkdCIiwicm9sZXMiOlsiUElTUCIsIkFJU1AiXX1dLCJhdXRob3JpdHlfaWQiOiJGQ0EiLCJyZWdpc3RyYXRpb25faWQiOiJCYW5rb2ZJcmVsYW5kUExDMyIsInN0YXR1cyI6IkFjdGl2ZSJ9LCJzb2Z0d2FyZV9jbGllbnRfZGVzY3JpcHRpb24iOiJUZXN0IERlYyIsInNvZnR3YXJlX2NsaWVudF9pZCI6IjJRdmk3ekFuemprNWVReEgwaXJjRHgiLCJzb2Z0d2FyZV9jbGllbnRfbmFtZSI6IlRlc3QgRGVjIiwic29mdHdhcmVfY2xpZW50X3VyaSI6Im5vdCBpbXBsZW1lbnRlZCIsInNvZnR3YXJlX2Vudmlyb25tZW50IjoibWl0Iiwic29mdHdhcmVfaWQiOiIyUXZpN3pBbnpqazVlUXhIMGlyY0R4Iiwic29mdHdhcmVfandrc19lbmRwb2ludCI6Imh0dHBzOi8va2V5c3RvcmUubWl0Lm9wZW5iYW5raW5nLnFhL25CUmNZQWNBQ25naGJHRk9Cay8yUXZpN3pBbnpqazVlUXhIMGlyY0R4Lmp3a3MiLCJzb2Z0d2FyZV9qd2tzX3Jldm9rZWRfZW5kcG9pbnQiOiJodHRwczovL2tleXN0b3JlLm1pdC5vcGVuYmFua2luZy5xYS9uQlJjWUFjQUNuZ2hiR0ZPQmsvcmV2b2tlZC8yUXZpN3pBbnpqazVlUXhIMGlyY0R4Lmp3a3MiLCJzb2Z0d2FyZV9sb2dvX3VyaSI6Im5vdCBpbXBsZW1lbnRlZCIsInNvZnR3YXJlX21vZGUiOiJUZXN0Iiwic29mdHdhcmVfb25fYmVoYWxmX29mX29yZyI6Im5CUmNZQWNBQ25naGJHRk9CayIsInNvZnR3YXJlX3BvbGljeV91cmkiOiJodHRwczovL2dvb2dsZS5jb20vcG9saWN5Iiwic29mdHdhcmVfcmVkaXJlY3RfdXJpcyI6WyJodHRwczovL2dvb2dsZS5jb20vcmVkaXJlY3QxIiwiaHR0cHM6Ly9nb29nbGUuY29tL3JlZGlyZWN0MiJdLCJzb2Z0d2FyZV9yb2xlcyI6WyJQSVNQIiwiQUlTUCJdLCJzb2Z0d2FyZV90b3NfdXJpIjoiaHR0cHM6Ly9nb29nbGUuY29tL3RvcyIsInNvZnR3YXJlX3ZlcnNpb24iOjU1LjU1fQ.I0WwEZd9SkyrviLAniyMAm9c3l59Noocbzpkg_6j8NZzLUg7Mkv4TZ3vAlNj-MAxyuQrf9meblAbdXMKrzO-XMD_P3COqA0HaJeHVPdz05FmGnbWKCytu_0hkDoR5dsi73GrgUj4R634VUQWcSXdRsgVQp-pFsBlaIqOJBLSrYd5aSF3YByJ9bTVLHlPhvGCcSxn6VxoXENEoSkEHS4Nv_unF2o8jMaC3SBbKmIhx223MxtR_CFwJXPbLK9ZSxIEWwFcB61vsLPXNm6YZmYKxFYk9wjaWAOmxBqNUZ-_ta2cm-FVDosKHDxXDuRa4sbbJ9itn0RZwD4xvBGODl_8aQ";
		return JWTReader.decodeTokenIntoSSAModel(token);
	}

	public static SSAModel getSSADataElement1() {
		String token = "eyJhbGciOiJQUzI1NiIsImtpZCI6ImRTM0hFenN5VkpPTHpRVkhJVWtPSkUySXFrbTN5SGI0QllfUGJCRVRXalk9IiwidHlwIjoiSldUIn0.ew0KICAiaXNzIjogIlBTRElFLUNCSS0xMDM0NTA3ODkwIiwNCiAgInNvZnR3YXJlX2NsaWVudF9kZXNjcmlwdGlvbiI6ICJOb24gT0IgU1NBQzMiLA0KICAic29mdHdhcmVfaWQiOiAiOTk5OU9Fd1lBS1NTQTEyMzQiLA0KICAic29mdHdhcmVfcm9sZXMiOiBbDQogICAgIkFJU1AiLA0KICAgICJQSVNQIiwNCiAgICAiQ0JQSUkiDQogIF0sDQogICJpYXQiOiAxNTUwNzc0ODIwLA0KICAianRpIjogIjUxYTE1MzA4LTMxOTMtNDcwMi1hMWUwLTViYzQyMWUwZDg4YSIsDQogICJzb2Z0d2FyZV9jbGllbnRfbmFtZSI6ICJOT05fT0JfU1NBQzMxMjM0IiwNCiAgInNvZnR3YXJlX2NsaWVudF9pZCI6ICI0Mjc2YjQxZi1hNjkxLTQyNWUtOTA4MS0wNzM5MzAyOWQyZDciLA0KICAic29mdHdhcmVfcmVkaXJlY3RfdXJpcyI6IFsNCiAgICAiaHR0cHM6Ly9nb29nbGUuY29tIg0KICBdLA0KICAib3JnX2lkIjogIlBTRElFLUNCSS0xMDM0NTA3ODkwIiwNCiAgIm9yZ19uYW1lIjogIlRlc3QgVFBQIGZvciBOb24gT0IgU1NBIg0KfQ==.ithRR8as3qmURe3wqrTG5aLT-HeU7aPGg1RNzV9JopAVr-hTSKBv2X6pMfH9eRxRinDWtuMeNlEO1EMIxRa2nTRMiW893aQCesBafgZhopWNtyVH-R0wCnyKBcZgGSc8E0qO9GG88mMjl2PZZttUrEvuWMlAc_PFE1E1jMCQnd1mjy3NbkF5jNJUnSI46X9ieinLfZFRYbX3KrtbKbiiIwAiwm0qi0-3T1HeTcCgqazUJYbCXAlZ0Xl4pz01DsuN3jbvTPsQdthES6d73U6WIzkkbB_83cfG36nU7jLtknj5I4S3Wk0r-8gbPFarby11Hwbnmimw7PGA8ZM8uqiQNA";
		return JWTReader.decodeTokenIntoSSAModel(token);
	}

	
	public static String getSSAToken() {
		return "eyJhbGciOiJSUzI1NiIsImtpZCI6IkVxYzBQSnlJVDEzS3NNQ3Z5aDJyQm9aeHlxWUhuM2J3MllkMlhFdF90R2MiLCJ0eXAiOiJKV1QifQ.eyJpYXQiOjE1MTU0MjI3MDksImlzcyI6Ik9wZW5CYW5raW5nIEx0ZCIsImp0aSI6IjZJbUl6M0RLNndJcHlMdVNPTW5TR00iLCJvYl9yZWdpc3RyeV90b3MiOiJodHRwczovL3JlZ2lzdHJ5Lm9wZW5iYW5raW5nLm9yZy51ay90b3MuaHRtbCIsIm9yZ19jb250YWN0cyI6W10sIm9yZ19jcmVhdGVkX2RhdGUiOm51bGwsIm9yZ19pZCI6Im5CUmNZQWNBQ25naGJHRk9CayIsIm9yZ19qd2tzX2VuZHBvaW50IjoiaHR0cHM6Ly9rZXlzdG9yZS5taXQub3BlbmJhbmtpbmcucWEvbkJSY1lBY0FDbmdoYkdGT0JrL25CUmNZQWNBQ25naGJHRk9Cay5qd2tzIiwib3JnX2p3a3NfcmV2b2tlZF9lbmRwb2ludCI6Imh0dHBzOi8va2V5c3RvcmUubWl0Lm9wZW5iYW5raW5nLnFhL25CUmNZQWNBQ25naGJHRk9Cay9yZXZva2VkL25CUmNZQWNBQ25naGJHRk9Cay5qd2tzIiwib3JnX2xhc3RfbW9kaWZpZWRfZGF0ZSI6bnVsbCwib3JnX25hbWUiOiJCYW5rIG9mIElyZWxhbmQgVFBQIC0gQUlTUCBhbmQgUElTUCIsIm9yZ19zdGF0dXMiOiJBY3RpdmUiLCJvcmdhbmlzYXRpb25fY29tcGV0ZW50X2F1dGhvcml0eV9jbGFpbXMiOnsiYXV0aG9yaXNhdGlvbnMiOlt7Im1lbWJlcl9zdGF0ZSI6IkdCIiwicm9sZXMiOlsiUElTUCIsIkFJU1AiXX1dLCJhdXRob3JpdHlfaWQiOiJGQ0EiLCJyZWdpc3RyYXRpb25faWQiOiJCYW5rb2ZJcmVsYW5kUExDMyIsInN0YXR1cyI6IkFjdGl2ZSJ9LCJzb2Z0d2FyZV9jbGllbnRfZGVzY3JpcHRpb24iOiJUZXN0IERlYyIsInNvZnR3YXJlX2NsaWVudF9pZCI6IjJRdmk3ekFuemprNWVReEgwaXJjRHgiLCJzb2Z0d2FyZV9jbGllbnRfbmFtZSI6IlRlc3QgRGVjIiwic29mdHdhcmVfY2xpZW50X3VyaSI6Im5vdCBpbXBsZW1lbnRlZCIsInNvZnR3YXJlX2Vudmlyb25tZW50IjoibWl0Iiwic29mdHdhcmVfaWQiOiIyUXZpN3pBbnpqazVlUXhIMGlyY0R4Iiwic29mdHdhcmVfandrc19lbmRwb2ludCI6Imh0dHBzOi8va2V5c3RvcmUubWl0Lm9wZW5iYW5raW5nLnFhL25CUmNZQWNBQ25naGJHRk9Cay8yUXZpN3pBbnpqazVlUXhIMGlyY0R4Lmp3a3MiLCJzb2Z0d2FyZV9qd2tzX3Jldm9rZWRfZW5kcG9pbnQiOiJodHRwczovL2tleXN0b3JlLm1pdC5vcGVuYmFua2luZy5xYS9uQlJjWUFjQUNuZ2hiR0ZPQmsvcmV2b2tlZC8yUXZpN3pBbnpqazVlUXhIMGlyY0R4Lmp3a3MiLCJzb2Z0d2FyZV9sb2dvX3VyaSI6Im5vdCBpbXBsZW1lbnRlZCIsInNvZnR3YXJlX21vZGUiOiJUZXN0Iiwic29mdHdhcmVfb25fYmVoYWxmX29mX29yZyI6Im5CUmNZQWNBQ25naGJHRk9CayIsInNvZnR3YXJlX3BvbGljeV91cmkiOiJodHRwczovL2dvb2dsZS5jb20vcG9saWN5Iiwic29mdHdhcmVfcmVkaXJlY3RfdXJpcyI6WyJodHRwczovL2dvb2dsZS5jb20vcmVkaXJlY3QxIiwiaHR0cHM6Ly9nb29nbGUuY29tL3JlZGlyZWN0MiJdLCJzb2Z0d2FyZV9yb2xlcyI6WyJQSVNQIiwiQUlTUCJdLCJzb2Z0d2FyZV90b3NfdXJpIjoiaHR0cHM6Ly9nb29nbGUuY29tL3RvcyIsInNvZnR3YXJlX3ZlcnNpb24iOjU1LjU1fQ.I0WwEZd9SkyrviLAniyMAm9c3l59Noocbzpkg_6j8NZzLUg7Mkv4TZ3vAlNj-MAxyuQrf9meblAbdXMKrzO-XMD_P3COqA0HaJeHVPdz05FmGnbWKCytu_0hkDoR5dsi73GrgUj4R634VUQWcSXdRsgVQp-pFsBlaIqOJBLSrYd5aSF3YByJ9bTVLHlPhvGCcSxn6VxoXENEoSkEHS4Nv_unF2o8jMaC3SBbKmIhx223MxtR_CFwJXPbLK9ZSxIEWwFcB61vsLPXNm6YZmYKxFYk9wjaWAOmxBqNUZ-_ta2cm-FVDosKHDxXDuRa4sbbJ9itn0RZwD4xvBGODl_8aQ";
	}

	public static String getInvalidOrgStatusToken() {
		return "eyJhbGciOiJSUzI1NiIsImtpZCI6IkVxYzBQSnlJVDEzS3NNQ3Z5aDJyQm9aeHlxWUhuM2J3MllkMlhFdF90R2MiLCJ0eXAiOiJKV1QifQ.eyJpYXQiOjE1MTU0MjI3MDksImlzcyI6Ik9wZW5CYW5raW5nIEx0ZCIsImp0aSI6IjZJbUl6M0RLNndJcHlMdVNPTW5TR00iLCJvYl9yZWdpc3RyeV90b3MiOiJodHRwczovL3JlZ2lzdHJ5Lm9wZW5iYW5raW5nLm9yZy51ay90b3MuaHRtbCIsIm9yZ19jb250YWN0cyI6W10sIm9yZ19jcmVhdGVkX2RhdGUiOm51bGwsIm9yZ19pZCI6Im5CUmNZQWNBQ25naGJHRk9CayIsIm9yZ19qd2tzX2VuZHBvaW50IjoiaHR0cHM6Ly9rZXlzdG9yZS5taXQub3BlbmJhbmtpbmcucWEvbkJSY1lBY0FDbmdoYkdGT0JrL25CUmNZQWNBQ25naGJHRk9Cay5qd2tzIiwib3JnX2p3a3NfcmV2b2tlZF9lbmRwb2ludCI6Imh0dHBzOi8va2V5c3RvcmUubWl0Lm9wZW5iYW5raW5nLnFhL25CUmNZQWNBQ25naGJHRk9Cay9yZXZva2VkL25CUmNZQWNBQ25naGJHRk9Cay5qd2tzIiwib3JnX2xhc3RfbW9kaWZpZWRfZGF0ZSI6bnVsbCwib3JnX25hbWUiOiJCYW5rIG9mIElyZWxhbmQgVFBQIC0gQUlTUCBhbmQgUElTUCIsIm9yZ19zdGF0dXMiOiJTQUEiLCJvcmdhbmlzYXRpb25fY29tcGV0ZW50X2F1dGhvcml0eV9jbGFpbXMiOnsiYXV0aG9yaXNhdGlvbnMiOlt7Im1lbWJlcl9zdGF0ZSI6IkdCIiwicm9sZXMiOlsiUElTUCIsIkFJU1AiXX1dLCJhdXRob3JpdHlfaWQiOiJGQ0EiLCJyZWdpc3RyYXRpb25faWQiOiJCYW5rb2ZJcmVsYW5kUExDMyIsInN0YXR1cyI6IkFjdGl2ZSJ9LCJzb2Z0d2FyZV9jbGllbnRfZGVzY3JpcHRpb24iOiJUZXN0IERlYyIsInNvZnR3YXJlX2NsaWVudF9pZCI6IjJRdmk3ekFuemprNWVReEgwaXJjRHgiLCJzb2Z0d2FyZV9jbGllbnRfbmFtZSI6IlRlc3QgRGVjIiwic29mdHdhcmVfY2xpZW50X3VyaSI6Im5vdCBpbXBsZW1lbnRlZCIsInNvZnR3YXJlX2Vudmlyb25tZW50IjoibWl0Iiwic29mdHdhcmVfaWQiOiIyUXZpN3pBbnpqazVlUXhIMGlyY0R4Iiwic29mdHdhcmVfandrc19lbmRwb2ludCI6Imh0dHBzOi8va2V5c3RvcmUubWl0Lm9wZW5iYW5raW5nLnFhL25CUmNZQWNBQ25naGJHRk9Cay8yUXZpN3pBbnpqazVlUXhIMGlyY0R4Lmp3a3MiLCJzb2Z0d2FyZV9qd2tzX3Jldm9rZWRfZW5kcG9pbnQiOiJodHRwczovL2tleXN0b3JlLm1pdC5vcGVuYmFua2luZy5xYS9uQlJjWUFjQUNuZ2hiR0ZPQmsvcmV2b2tlZC8yUXZpN3pBbnpqazVlUXhIMGlyY0R4Lmp3a3MiLCJzb2Z0d2FyZV9sb2dvX3VyaSI6Im5vdCBpbXBsZW1lbnRlZCIsInNvZnR3YXJlX21vZGUiOiJUZXN0Iiwic29mdHdhcmVfb25fYmVoYWxmX29mX29yZyI6Im5CUmNZQWNBQ25naGJHRk9CayIsInNvZnR3YXJlX3BvbGljeV91cmkiOiJodHRwczovL2dvb2dsZS5jb20vcG9saWN5Iiwic29mdHdhcmVfcmVkaXJlY3RfdXJpcyI6WyJodHRwczovL2dvb2dsZS5jb20vcmVkaXJlY3QxIiwiaHR0cHM6Ly9nb29nbGUuY29tL3JlZGlyZWN0MiJdLCJzb2Z0d2FyZV9yb2xlcyI6WyJQSVNQIiwiQUlTUCJdLCJzb2Z0d2FyZV90b3NfdXJpIjoiaHR0cHM6Ly9nb29nbGUuY29tL3RvcyIsInNvZnR3YXJlX3ZlcnNpb24iOjU1LjU1fQ.T3LOx_k0Oy3O96HHXi5gs8LTVuE81FSuWjc0AnKp5wc";
	}

	public static String getOBPublicKey() {
		return "{\"keys\": [{\"e\": \"AQAB\", \"kid\": \"Eqc0PJyIT13KsMCvyh2rBoZxyqYHn3bw2Yd2XEt_tGc\", \"use\": \"sig\", \"kty\": \"RSA\", \"alg\": \"RS256\", \"n\": \"wgnAhZ82qn95lQToK0qsY19o-HAnbwO19Q_NCD5IYarDg5pcU_6iLYn3HuKFLn0GyXw6OXZFg1_VsOSX3MKYddy-AE9wAA4ft0RftJc0rqT4VPeKK8LJqcVsI6jl0jbtJqxOV8AbEjCSndjg79NrAnzQ8G6ntLY2kn7q4IJiR5qQEhOUgBmbJz3gpQ9ryHcJmYsnR7JM630-5CI0YDSe7zNzzDoCFH7Gy7oY0QnFB2WHkb0GaOpJl1YeWYHNu7Jug_zyrpnaOJ-A3UXhUfwKiL4yWVQog3yb7UjqwzIhFl-uBApEdDccxUS-01LoGMXh1K-6TktEVLJOlNYa1USnQw\"}]}";
	}

	public static NewApplicationMuleApiResponse getNewApplicationMuleApiResponse() {
		NewApplicationMuleApiResponse newApplicationMuleApiResponse = new NewApplicationMuleApiResponse();
		newApplicationMuleApiResponse.setClientId("2Qvi7zAnzjk5eQxH0ircDx");
		newApplicationMuleApiResponse.setClientSecret("12345678");
		newApplicationMuleApiResponse.setDescription("Test Dec");
		newApplicationMuleApiResponse.setName("Test Dec");
		newApplicationMuleApiResponse.setId("24");
		return newApplicationMuleApiResponse;
	}

	public static ApisResponse getApisResponse() {
		ApisResponse apisResponse = new ApisResponse();
		List<Api> apisList = new ArrayList();
		Api api = new Api();
		api.setId(1);
		api.setName("Account Balance And Transaction Api");
		List<Version> versionList = new ArrayList();
		Version version1 = new Version();
		version1.setId(63034);
		version1.setApiId(1);
		versionList.add(version1);
		Version version2 = new Version();
		version2.setId(63035);
		version2.setApiId(1);
		versionList.add(version2);
		api.setVersions(versionList);
		apisList.add(api);
		apisResponse.setApis(apisList);
		apisResponse.setTotal(1);
		return apisResponse;
	}

	public static ApisResponse getApisResponseNullVersionList() {
		ApisResponse apisResponse = new ApisResponse();
		List<Api> apisList = new ArrayList();
		Api api = new Api();
		api.setId(1);
		api.setName("Account Balance And Transaction Api");
		apisList.add(api);
		apisResponse.setApis(apisList);
		apisResponse.setTotal(1);
		return apisResponse;
	}

	public static Map<String, ApiDetail> getSelectApisForContract(){
		Map<String, ApiDetail> selectedApis = new HashMap<>();
		ApiDetail paymentApi = new ApiDetail();
		paymentApi.setApiId(138316854);
		paymentApi.setVersionId(9164788);
		selectedApis.put("Payment Submission", paymentApi);
		return selectedApis;
	}
	
	public static Tiers getOneTiers() {
		Tiers tiers = new Tiers();
		List<Tier> tierList = new ArrayList();
		Tier tier = new Tier();
		tier.setId(30456);
		tier.setApiVersionId(63034);
		tier.setName("Premium Tier");
		tierList.add(tier);
		tiers.setTotal(1);
		tiers.setTiers(tierList);
		return tiers;
	}

	public static Tiers getNoTiers() {
		Tiers tiers = new Tiers();
		List<Tier> tier = new ArrayList();
		tiers.setTiers(tier);
		return tiers;
	}

	public static Tiers getMoreThanOneTiers() {
		Tiers tiers = new Tiers();
		List<Tier> tierList = new ArrayList();
		Tier tier = new Tier();
		tier.setId(30456);
		tier.setApiVersionId(63034);
		tier.setName("Premium Tier");
		Tier tier2 = new Tier();
		tier2.setId(30455);
		tier2.setApiVersionId(63034);
		tier2.setName("Basic Tier");
		tierList.add(tier);
		tierList.add(tier2);
		tiers.setTiers(tierList);
		return tiers;
	}

	public static TPPDetails getTppDetails() {
		TPPDetails tppDetails = new TPPDetails();
		tppDetails.setApplicationName("Test Dec");
		tppDetails.setClientId("2Qvi7zAnzjk5eQxH0ircDx");
		return tppDetails;
	}
}
