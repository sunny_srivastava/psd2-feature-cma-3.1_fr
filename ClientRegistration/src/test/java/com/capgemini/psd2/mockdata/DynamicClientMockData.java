package com.capgemini.psd2.mockdata;

import java.math.BigInteger;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.RSAPublicKeySpec;
import java.text.ParseException;
import java.util.ArrayList;

import com.capgemini.tpp.ob.model.OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations;
import com.capgemini.tpp.registration.model.SSAWrapper;
import com.capgemini.tpp.ssa.model.Authorisation;
import com.capgemini.tpp.ssa.model.SSAModel;
import com.nimbusds.jwt.JWT;
import com.nimbusds.jwt.JWTParser;
import com.nimbusds.jwt.SignedJWT;

public class DynamicClientMockData {

	public static SSAWrapper getSSAWrapper() {

		SSAWrapper ssaWrapper = new SSAWrapper();

		ssaWrapper.setSsaToken(
				"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c");

		SSAModel ssaModel = setSSAModel();
		ssaWrapper.setSsaModel(ssaModel);
		return ssaWrapper;
	}

	public static SSAModel setSSAModel() {

		SSAModel ssaModel = new SSAModel();
		ssaModel.setIat(1537257843);
		ssaModel.setIss("1wwLDFoVlHfnlK0Dgu71lB");
		ssaModel.setSoftware_id("1wwLDFoVlHfnlK0Dgu71lB");
		ssaModel.setJti("7aac9164-eeac-47d6-8042-9a24610a984e");
		ssaModel.setOrg_id("0015800000jfQ9aAAE");
		ssaModel.setOrg_jwks_endpoint("https://auth-test.apibank-cma2plus.in");
		ssaModel.setSoftware_jwks_endpoint("https://auth-test.apibank-cma2plus.in");
		ssaModel.setSoftware_client_id("1wwLDFoVlHfnlK0Dgu71lB");
		ssaModel.setSoftware_client_name("CG_TPP_DCR");
		ssaModel.setSoftware_client_uri("https://auth-test.apibank-cma2plus.in");
		ssaModel.setSoftware_logo_uri("https://auth-test.apibank-cma2plus.in");
		ArrayList<String> software_roles = new ArrayList<>();
		software_roles.add("aisp");
		software_roles.add("pisp");
		
		ssaModel.setSoftware_roles(software_roles);
		
		return ssaModel;
	}

	public static String certInStringFormat() {
		return "-----BEGIN CERTIFICATE-----\r\nMIIFODCCBCCgAwIBAgIEWcVNODANBgkqhkiG9w0BAQsFADBTMQswCQYDVQQGEwJH\r\nQjEUMBIGA1UEChMLT3BlbkJhbmtpbmcxLjAsBgNVBAMTJU9wZW5CYW5raW5nIFBy\r\nZS1Qcm9kdWN0aW9uIElzc3VpbmcgQ0EwHhcNMTgwOTIwMDUzNTU5WhcNMTkxMDIw\r\nMDYwNTU5WjBhMQswCQYDVQQGEwJHQjEUMBIGA1UEChMLT3BlbkJhbmtpbmcxGzAZ\r\nBgNVBAsTEjAwMTU4MDAwMDBqZlE5YUFBRTEfMB0GA1UEAxMWNzlCYTRPUlNhSmNK\r\nbnNKRU1Gd2czQzCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAKcCukMh\r\nqEwIevh292gBTKPlO3wH0ZppRmdy1bUVfkuhboc1uDkoapjfa08vPD1HoTKgXLcm\r\nbuahpVip+e3Cj1BYVE+kNswcu/1LUvPkJrUTkHy+jpgfsOQ4/CM+Mi2J8Y4O74EV\r\nrigWQSvtPGjkcxo6726ICR+lyekw49nbpDR1I/7fGiYlPpo75ExYUQBhHXSXooUE\r\nXGWN7wWmnXjZpPhQTftbZbBsPg2SwH9e8YW7xtGlu29POLZlRuXzX5LC4Dox30Ha\r\nJWwoLktyt8hCWoVqf9/Rh66YLEss35QldhsJc5p25cMWhdsIajJOZyZFLYtFqCV3\r\ndQUr7MUGxMeQUZECAwEAAaOCAgQwggIAMA4GA1UdDwEB/wQEAwIHgDAgBgNVHSUB\r\nAf8EFjAUBggrBgEFBQcDAQYIKwYBBQUHAwIwgeAGA1UdIASB2DCB1TCB0gYLKwYB\r\nBAGodYEGAWQwgcIwKgYIKwYBBQUHAgEWHmh0dHA6Ly9vYi50cnVzdGlzLmNvbS9w\r\nb2xpY2llczCBkwYIKwYBBQUHAgIwgYYMgYNVc2Ugb2YgdGhpcyBDZXJ0aWZpY2F0\r\nZSBjb25zdGl0dXRlcyBhY2NlcHRhbmNlIG9mIHRoZSBPcGVuQmFua2luZyBSb290\r\nIENBIENlcnRpZmljYXRpb24gUG9saWNpZXMgYW5kIENlcnRpZmljYXRlIFByYWN0\r\naWNlIFN0YXRlbWVudDBtBggrBgEFBQcBAQRhMF8wJgYIKwYBBQUHMAGGGmh0dHA6\r\nLy9vYi50cnVzdGlzLmNvbS9vY3NwMDUGCCsGAQUFBzAChilodHRwOi8vb2IudHJ1\r\nc3Rpcy5jb20vb2JfcHBfaXNzdWluZ2NhLmNydDA6BgNVHR8EMzAxMC+gLaArhilo\r\ndHRwOi8vb2IudHJ1c3Rpcy5jb20vb2JfcHBfaXNzdWluZ2NhLmNybDAfBgNVHSME\r\nGDAWgBRQc5HGIXLTd/T+ABIGgVx5eW4/UDAdBgNVHQ4EFgQU6q11L6hdMztY3Fp1\r\ngbecOwpbK4kwDQYJKoZIhvcNAQELBQADggEBAB/H/Xa1TyCTtWjCDjsWNdR875lN\r\nyCbyjV7b8eVJpANaGTy9ZsjCSeMFeZXlzlDDl0p6KzPNJv2knhuR1nXfCC1rn1XM\r\ncJ2YiFlJxtGCq1K7ejHGSK39iCFNGmqlGaztK+CDO0AmM6xslc5pbEy1DM8+CQ8F\r\nl0FPGhuIAPJbu8EQShxBbzdUN4pXlVA5Qg0MfBN9KMqKNlbAMstLWi5NMG33zEo1\r\n4sAxPdXqdM025QXakzKFQkFOdvmLnro8jpO9MZnAXO+MOR8gantDY5DcuhXZ+6VX\r\n/xZF5ChoF0Rq2w4vDU+arDwMeXKCE+GL3ijXXXQKxzwdQv7lT3/JBz0TjRE=\r\n-----END CERTIFICATE-----";
	}
	
	public static SignedJWT signedJWT() throws ParseException{
		String jwtToken = "eyJhbGciOiJSUzI1NiIsImtpZCI6ImpfT1BYZTh0Y2hXdWhRM2dWTi1TT09PVHlEWSIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE1Mzc0Mjk1ODQsImlzcyI6Ik9wZW5CYW5raW5nIEx0ZCIsImp0aSI6IjZNVDRCNkhWR003VXcwTm9wa3dEaXMiLCJvYl9yZWdpc3RyeV90b3MiOiJodHRwczovL3JlZ2lzdHJ5Lm9wZW5iYW5raW5ndGVzdC5vcmcudWsvdG9zLmh0bWwiLCJvcmdfY29udGFjdHMiOlt7ImVtYWlsIjoiT0JUZWNobmljYWxRdWVyaWVzQEJPSS5DT00iLCJuYW1lIjoiVGVjaG5pY2FsIiwicGhvbmUiOiIwODYwNjgxNzYyIiwidHlwZSI6IlRlY2huaWNhbCJ9LHsiZW1haWwiOiJPQkJ1c2luZXNzUXVlcmllc0BCT0kuQ09NIiwibmFtZSI6IkJ1c2luZXNzIiwicGhvbmUiOiIwNzU4NCAyMTQ4MzAiLCJ0eXBlIjoiQnVzaW5lc3MifV0sIm9yZ19pZCI6IjAwMTU4MDAwMDBqZlE5YUFBRSIsIm9yZ19qd2tzX2VuZHBvaW50IjoiaHR0cHM6Ly9rZXlzdG9yZS5vcGVuYmFua2luZ3Rlc3Qub3JnLnVrLzAwMTU4MDAwMDBqZlE5YUFBRS8wMDE1ODAwMDAwamZROWFBQUUuandrcyIsIm9yZ19qd2tzX3Jldm9rZWRfZW5kcG9pbnQiOiJodHRwczovL2tleXN0b3JlLm9wZW5iYW5raW5ndGVzdC5vcmcudWsvMDAxNTgwMDAwMGpmUTlhQUFFL3Jldm9rZWQvMDAxNTgwMDAwMGpmUTlhQUFFLmp3a3MiLCJvcmdfbmFtZSI6IkJhbmsgb2YgSXJlbGFuZCAoVUspIFBsYyIsIm9yZ19zdGF0dXMiOiJBY3RpdmUiLCJvcmdhbmlzYXRpb25fY29tcGV0ZW50X2F1dGhvcml0eV9jbGFpbXMiOnsiYXV0aG9yaXNhdGlvbnMiOlt7Im1lbWJlcl9zdGF0ZSI6IkdCIiwicm9sZXMiOlsiQUlTUCIsIlBJU1AiXX1dLCJhdXRob3JpdHlfaWQiOiJGQ0FHQlIiLCJyZWdpc3RyYXRpb25faWQiOiI1MTI5NTYiLCJzdGF0dXMiOiJBY3RpdmUifSwic29mdHdhcmVfY2xpZW50X2Rlc2NyaXB0aW9uIjoiQ0dfQ01BVEVTVF9EQ1IiLCJzb2Z0d2FyZV9jbGllbnRfaWQiOiI0Y3BpOFA0ZGtUazIwMEludjFjUHZPIiwic29mdHdhcmVfY2xpZW50X25hbWUiOiJDR19DTUFURVNUX0RDUiIsInNvZnR3YXJlX2NsaWVudF91cmkiOiJodHRwczovL3Rlc3QuY29tIiwic29mdHdhcmVfZW52aXJvbm1lbnQiOiJzYW5kYm94Iiwic29mdHdhcmVfaWQiOiI0Y3BpOFA0ZGtUazIwMEludjFjUHZPIiwic29mdHdhcmVfandrc19lbmRwb2ludCI6Imh0dHBzOi8va2V5c3RvcmUub3BlbmJhbmtpbmd0ZXN0Lm9yZy51ay8wMDE1ODAwMDAwamZROWFBQUUvNGNwaThQNGRrVGsyMDBJbnYxY1B2Ty5qd2tzIiwic29mdHdhcmVfandrc19yZXZva2VkX2VuZHBvaW50IjoiaHR0cHM6Ly9rZXlzdG9yZS5vcGVuYmFua2luZ3Rlc3Qub3JnLnVrLzAwMTU4MDAwMDBqZlE5YUFBRS9yZXZva2VkLzRjcGk4UDRka1RrMjAwSW52MWNQdk8uandrcyIsInNvZnR3YXJlX2xvZ29fdXJpIjoiaHR0cHM6Ly90ZXN0LmNvbSIsInNvZnR3YXJlX21vZGUiOiJMaXZlIiwic29mdHdhcmVfb25fYmVoYWxmX29mX29yZyI6IkJPSSIsInNvZnR3YXJlX3BvbGljeV91cmkiOiJodHRwczovL3Rlc3QuY29tIiwic29mdHdhcmVfcmVkaXJlY3RfdXJpcyI6WyJodHRwczovL3Rlc3QuY29tIl0sInNvZnR3YXJlX3JvbGVzIjpbIkFJU1AiLCJQSVNQIl0sInNvZnR3YXJlX3Rvc191cmkiOiJodHRwczovL3Rlc3QuY29tIiwic29mdHdhcmVfdmVyc2lvbiI6MS4xfQ.sPSxGAkuWQJISQX_jE4JOKW2Mjs1ragzvEZGbfaF28MXA5Dvx0mlb6gzCmhbigHY4RkLYJyTDL_gmVQbRg_ddunOO1D0uA_lnzlZB_ZSFUWANrGxOxKGHucz8RR4G2X0mK26ezLORq2IZKbkI4pRCIU8qRnAI5F4DnBAURF1SCj3XqSooMmG-JHITlZxFO_7T9vSulSFDQmGNBK2j38NCmL-VH3zqF96J0tqqCbgIeJ5v4YY5H2Qw7v1BOBS18S4-QJgE3q6vRiTFzPpjjTXRjTaehDBPSEsiT4psQZ5as-yUTALlnsfmuLojM0b7t6MjyyDZ0u8CdRUtTF_mocyLQ";
	    //String jwks = "{\"keys\": [{\"e\": \"AQAB\", \"kid\": \"j_OPXe8tchWuhQ3gVN-SOOOTyDY\", \"kty\": \"RSA\", \"n\": \"s5DZbL7-JI9p1doLsrldcNfuTPvhdLixuOEuDk_55blNI2luWAy81YYqj9rPGtqH6wmk9yHe4RBQyrzZTSNYsIXGyKiqgulhXlgzEjrRmTDSFPowKdkK3WQQnGnnKfqjSrVSPjB9bF6soy9DDg4-dPIACczmElHaJmPSjt6h8G-MhuV72D7fMBNxI0UmKZzPP8yENbGFwB3dl1dSxyu-y6tWVyxHBH3aZ-HMoXoKCOjomUbl5i_dmUdlk3BjK2UnOhPXcZj_tf9iNymxr3JHIdAch_GPcnfs55WoGYNN-8kBN2q7_ipY57bqEMYyfDWkjRMeRhOwVNqofv9GvBsGYw\", \"x5u\": \"https://keystore.openbankingtest.org.uk/0015800000jfQ9aAAE/AzKwG3O2D6AMxr2TEwXA459l_Fw.pem\", \"x5c\": [\"MIIFODCCBCCgAwIBAgIEWcVNOzANBgkqhkiG9w0BAQsFADBTMQswCQYDVQQGEwJHQjEUMBIGA1UEChMLT3BlbkJhbmtpbmcxLjAsBgNVBAMTJU9wZW5CYW5raW5nIFByZS1Qcm9kdWN0aW9uIElzc3VpbmcgQ0EwHhcNMTgwOTIwMDcxNDQ0WhcNMTkxMDIwMDc0NDQ0WjBhMQswCQYDVQQGEwJHQjEUMBIGA1UEChMLT3BlbkJhbmtpbmcxGzAZBgNVBAsTEjAwMTU4MDAwMDBqZlE5YUFBRTEfMB0GA1UEAxMWNGNwaThQNGRrVGsyMDBJbnYxY1B2TzCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBALOQ2Wy+/iSPadXaC7K5XXDX7kz74XS4sbjhLg5P+eW5TSNpblgMvNWGKo/azxrah+sJpPch3uEQUMq82U0jWLCFxsioqoLpYV5YMxI60Zkw0hT6MCnZCt1kEJxp5yn6o0q1Uj4wfWxerKMvQw4OPnTyAAnM5hJR2iZj0o7eofBvjIble9g+3zATcSNFJimczz/MhDWxhcAd3ZdXUscrvsurVlcsRwR92mfhzKF6Cgjo6JlG5eYv3ZlHZZNwYytlJzoT13GY/7X/Yjcpsa9yRyHQHIfxj3J37OeVqBmDTfvJATdqu/4qWOe26hDGMnw1pI0THkYTsFTaqH7/RrwbBmMCAwEAAaOCAgQwggIAMA4GA1UdDwEB/wQEAwIHgDAgBgNVHSUBAf8EFjAUBggrBgEFBQcDAQYIKwYBBQUHAwIwgeAGA1UdIASB2DCB1TCB0gYLKwYBBAGodYEGAWQwgcIwKgYIKwYBBQUHAgEWHmh0dHA6Ly9vYi50cnVzdGlzLmNvbS9wb2xpY2llczCBkwYIKwYBBQUHAgIwgYYMgYNVc2Ugb2YgdGhpcyBDZXJ0aWZpY2F0ZSBjb25zdGl0dXRlcyBhY2NlcHRhbmNlIG9mIHRoZSBPcGVuQmFua2luZyBSb290IENBIENlcnRpZmljYXRpb24gUG9saWNpZXMgYW5kIENlcnRpZmljYXRlIFByYWN0aWNlIFN0YXRlbWVudDBtBggrBgEFBQcBAQRhMF8wJgYIKwYBBQUHMAGGGmh0dHA6Ly9vYi50cnVzdGlzLmNvbS9vY3NwMDUGCCsGAQUFBzAChilodHRwOi8vb2IudHJ1c3Rpcy5jb20vb2JfcHBfaXNzdWluZ2NhLmNydDA6BgNVHR8EMzAxMC+gLaArhilodHRwOi8vb2IudHJ1c3Rpcy5jb20vb2JfcHBfaXNzdWluZ2NhLmNybDAfBgNVHSMEGDAWgBRQc5HGIXLTd/T+ABIGgVx5eW4/UDAdBgNVHQ4EFgQUGVLYQs5tgtS69MN3k30Tm+FpTHowDQYJKoZIhvcNAQELBQADggEBAI5XS3VJFu/Cmae7AhiMsohFnOfTn5kHHPFXgn9GiR/USIbK8Jk88yv6a3Pt77/rgXa8gS4WU8RxL36FB8hSBpg+5MdRLEtBUrjmACH3/Vinv+SvbakoFdRsUlxtPRhtYyUxZVehG9K3SSPRtPd/b0EmEm2NBjnFbSCKBQHaZG34s9j/Wo4O0Nx4/nNSI6aCQNMf6DvC1z6pk41cj4mbJ8bx7xQW7bnd84hWVHbSb3a6cJMeVJuDjAARSZSXETM7cPSV8XRmYDBCaMhrzmqZbHFFth4XTdxEuOuUi+a0Vy5uSs/S5i85ICx69i2GTFLmhUSTfRrnZNPFCDuZv8DEuyE=\"], \"x5t\": \"D3ySD6QHUldFxjDkMs5X4yV8NcQ=\", \"x5t#256\": \"6nDQNI7NZ7ngB_cj3pYE0sUU5k7JLqm_v9fwIBDMKMM=\", \"use\": \"enc\"}, {\"e\": \"AQAB\", \"kid\": \"j_OPXe8tchWuhQ3gVN-SOOOTyDY\", \"kty\": \"RSA\", \"n\": \"v_-VWeiPHUX4_yBmUeAbi09wpR4fL2sX1vWlVYT1VPSM1lVS2xiReB_naiw4-SH4SmwNPLXlZBoi1XqDY3U3xLXeBd8Nd78pg2IezQUgrisuyQ6juKWAohNctHzfIHKIyqkyBlUzPC35SwcwsKiEbwvXshbLXhKcDkM6EfH_Z4XcavBUaDBeaA2uOnJZlAa22k1tD3X5uH4Xd4RwdwVEJl4I_Y2lX0MmF1sonxyHd44cXIvhWR26lterLgUg1a4NTyqUjx0RPjADFZ2cVKtNY8hn0qTWD1niKuX-5FjhHXpTtU0be6se19M9Rrizb9bmqTDZrc5vdo199qWPXdrVzw\", \"x5u\": \"https://keystore.openbankingtest.org.uk/0015800000jfQ9aAAE/XeKRtiT7Fhg0bdKRu3kpExfoy9Y.pem\", \"x5c\": [\"MIIFLTCCBBWgAwIBAgIEWcVNPDANBgkqhkiG9w0BAQsFADBTMQswCQYDVQQGEwJHQjEUMBIGA1UEChMLT3BlbkJhbmtpbmcxLjAsBgNVBAMTJU9wZW5CYW5raW5nIFByZS1Qcm9kdWN0aW9uIElzc3VpbmcgQ0EwHhcNMTgwOTIwMDcxNDQ4WhcNMTkxMDIwMDc0NDQ4WjBhMQswCQYDVQQGEwJHQjEUMBIGA1UEChMLT3BlbkJhbmtpbmcxGzAZBgNVBAsTEjAwMTU4MDAwMDBqZlE5YUFBRTEfMB0GA1UEAxMWNGNwaThQNGRrVGsyMDBJbnYxY1B2TzCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAL//lVnojx1F+P8gZlHgG4tPcKUeHy9rF9b1pVWE9VT0jNZVUtsYkXgf52osOPkh+EpsDTy15WQaItV6g2N1N8S13gXfDXe/KYNiHs0FIK4rLskOo7ilgKITXLR83yByiMqpMgZVMzwt+UsHMLCohG8L17IWy14SnA5DOhHx/2eF3GrwVGgwXmgNrjpyWZQGttpNbQ91+bh+F3eEcHcFRCZeCP2NpV9DJhdbKJ8ch3eOHFyL4VkdupbXqy4FINWuDU8qlI8dET4wAxWdnFSrTWPIZ9Kk1g9Z4irl/uRY4R16U7VNG3urHtfTPUa4s2/W5qkw2a3Ob3aNffalj13a1c8CAwEAAaOCAfkwggH1MA4GA1UdDwEB/wQEAwIGwDAVBgNVHSUEDjAMBgorBgEEAYI3CgMMMIHgBgNVHSAEgdgwgdUwgdIGCysGAQQBqHWBBgFkMIHCMCoGCCsGAQUFBwIBFh5odHRwOi8vb2IudHJ1c3Rpcy5jb20vcG9saWNpZXMwgZMGCCsGAQUFBwICMIGGDIGDVXNlIG9mIHRoaXMgQ2VydGlmaWNhdGUgY29uc3RpdHV0ZXMgYWNjZXB0YW5jZSBvZiB0aGUgT3BlbkJhbmtpbmcgUm9vdCBDQSBDZXJ0aWZpY2F0aW9uIFBvbGljaWVzIGFuZCBDZXJ0aWZpY2F0ZSBQcmFjdGljZSBTdGF0ZW1lbnQwbQYIKwYBBQUHAQEEYTBfMCYGCCsGAQUFBzABhhpodHRwOi8vb2IudHJ1c3Rpcy5jb20vb2NzcDA1BggrBgEFBQcwAoYpaHR0cDovL29iLnRydXN0aXMuY29tL29iX3BwX2lzc3VpbmdjYS5jcnQwOgYDVR0fBDMwMTAvoC2gK4YpaHR0cDovL29iLnRydXN0aXMuY29tL29iX3BwX2lzc3VpbmdjYS5jcmwwHwYDVR0jBBgwFoAUUHORxiFy03f0/gASBoFceXluP1AwHQYDVR0OBBYEFHA0+kRCKZBplVAz5XCTDTC84eXDMA0GCSqGSIb3DQEBCwUAA4IBAQCHn+/m1Vhpf3Qeekh4Rnn2Dl4yyMMNqIKqjmxzDR2/mHKYHz2YOYbRsmfK4XmoXYGqcsaaXpwMVbrLV7dt0MmtMn888nx9qynvRiCtNQHItBMJ7dRLdAXV8/vraPMAWLV2JZaTJO/cc8KvOYetTRc7sgoUdwXp5SwRiJn7RUF+/40rMAPk93AsAe2Zcq4SgEyZoMp259JTFT0pzXr184uEVrVYgL7XkQ2m2zKHzITFHpLuhd1DmbJ9yPTzUI2zmBlqWURL0H1GYKYk1RRtJXSoSIS32vQfHeKX63diaDiMytM7FZ/OZGr1VMpNR/EkHOv+Dhvc+e6PxlkvDvnILoEq\"], \"x5t\": \"_J99rtmj3Q5o5an0rlLd7b84ctY=\", \"x5t#256\": \"i6rFwhD2WUohGXnp1KwLxLs0NDckvPr4rIy-a990qVQ=\", \"use\": \"sig\"}]}";
		JWT jwt = JWTParser.parse(jwtToken);
		SignedJWT signedJWT = (SignedJWT) jwt;
		return signedJWT;
	}
	
	public static PublicKey rsaKeyFormat(){
		BigInteger modulus = new BigInteger(1,
				org.apache.commons.codec.binary.Base64.decodeBase64("s5DZbL7-JI9p1doLsrldcNfuTPvhdLixuOEuDk_55blNI2luWAy81YYqj9rPGtqH6wmk9yHe4RBQyrzZTSNYsIXGyKiqgulhXlgzEjrRmTDSFPowKdkK3WQQnGnnKfqjSrVSPjB9bF6soy9DDg4-dPIACczmElHaJmPSjt6h8G-MhuV72D7fMBNxI0UmKZzPP8yENbGFwB3dl1dSxyu-y6tWVyxHBH3aZ-HMoXoKCOjomUbl5i_dmUdlk3BjK2UnOhPXcZj_tf9iNymxr3JHIdAch_GPcnfs55WoGYNN-8kBN2q7_ipY57bqEMYyfDWkjRMeRhOwVNqofv9GvBsGYw"));
		BigInteger publicExponent = new BigInteger(1,
				org.apache.commons.codec.binary.Base64.decodeBase64("AQAB"));
		 try {
			return KeyFactory.getInstance("RSA").generatePublic(new RSAPublicKeySpec(modulus, publicExponent));
		} catch (InvalidKeySpecException e) {
		} catch (NoSuchAlgorithmException e) {
		}
		return null;
	}
	
	public static OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations getAuthorisationsA1()
	{
		OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations a1=new OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations();
		a1.setActive(true);
		a1.setMemberState("GB");
		a1.setPsd2Role("PISP");
		return a1;
	}
	
	public static OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations getAuthorisationsA2()
	{
		OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations a1=new OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations();
		a1.setActive(true);
		a1.setMemberState("GB");
		a1.setPsd2Role("AISP");
		return a1;
	}
	
	
	public static OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations getAuthorisationsA3()
	{
		OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations a1=new OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations();
		a1.setActive(true);
		a1.setMemberState("IR");
		a1.setPsd2Role("AISP");
		return a1;
	}
	
	public static OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations getAuthorisationsA4()
	{
		OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations a1=new OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations();
		a1.setActive(false);
		a1.setMemberState("IR");
		a1.setPsd2Role("PISP");
		return a1;
	}
	
	public static OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations getAuthorisationsA5()
	{
		OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations a1=new OBThirdPartyProvidersUrnopenbankingcompetentauthorityclaims10Authorisations();
		a1.setActive(true);
		a1.setMemberState("NR");
		a1.setPsd2Role("AISP");
		return a1;
	}
	
	
	public static Authorisation getAuthorisation1() {
	
		Authorisation C1=new Authorisation();
		C1.setMember_state("GB");
		ArrayList<String> roles=new ArrayList<>();
		roles.add("AISP");
		roles.add("PISP");
		//roles.add("CISP");
		C1.setRoles(roles);
		return C1;
	}
	
	public static Authorisation getAuthorisation2() {
		
		Authorisation C1=new Authorisation();
		C1.setMember_state("IR");
		ArrayList<String> roles=new ArrayList<>();
		roles.add("AISP");
		roles.add("PISP");
		C1.setRoles(roles);
		
		return C1;
		
	}
	
public static Authorisation getAuthorisation3() {
		
		Authorisation C1=new Authorisation();
		C1.setMember_state("NR");
		ArrayList<String> roles=new ArrayList<>();
		roles.add("AISP");
		roles.add("NISP");
		C1.setRoles(roles);
		
		return C1;
		
	}
	
}