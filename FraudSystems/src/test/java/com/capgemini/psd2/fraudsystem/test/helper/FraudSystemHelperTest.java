package com.capgemini.psd2.fraudsystem.test.helper;

import static org.mockito.Matchers.anyMap;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.capgemini.psd2.aisp.domain.OBAccount6;
import com.capgemini.psd2.aisp.domain.OBAccount6Account;
import com.capgemini.psd2.consent.domain.PSD2Account;
import com.capgemini.psd2.fraudsystem.helper.FraudSystemHelper;
import com.capgemini.psd2.fraudsystem.request.handler.impl.FraudSystemRequestMapping;
import com.capgemini.psd2.fraudsystem.service.FraudSystemService;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.domain.OBDomestic1;
import com.capgemini.psd2.pisp.domain.OBDomestic1InstructedAmount;
import com.capgemini.psd2.pisp.domain.OBRemittanceInformation1;
import com.capgemini.psd2.pisp.domain.OBWriteDataDomesticConsentResponse1;
import com.capgemini.psd2.pisp.domain.OBWriteDomesticConsentResponse1;
import com.capgemini.psd2.pisp.stage.domain.CreditorDetails;
import com.capgemini.psd2.pisp.stage.domain.CustomFraudSystemPaymentData;

public class FraudSystemHelperTest {

	@Mock
	private RequestHeaderAttributes requestHeaderAttributes;

	@Mock
	private FraudSystemService fraudSystemService;

	@InjectMocks
	FraudSystemHelper fraudSystemHelper = new FraudSystemHelper();

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test(expected = Exception.class)
	public void testCaptureFraudEvent() {
		String dummyUserId = "12345678";

		OBAccount6 customerAccount = new OBAccount6();
		OBWriteDomesticConsentResponse1 paymentSetupPOSTResponse = new OBWriteDomesticConsentResponse1();
		OBWriteDataDomesticConsentResponse1 data = new OBWriteDataDomesticConsentResponse1();
		OBDomestic1 initiation = new OBDomestic1();
		OBDomestic1InstructedAmount instructedAmount = new OBDomestic1InstructedAmount();
		OBRemittanceInformation1 remittanceInformation = new OBRemittanceInformation1();
		remittanceInformation.setReference("dummyref");
		instructedAmount.setAmount("99999");
		instructedAmount.setCurrency("EUR");

		initiation.setInstructedAmount(instructedAmount);
		initiation.setRemittanceInformation(remittanceInformation);
		data.setInitiation(initiation);
		data.setCreationDateTime("2017-10-05");
		paymentSetupPOSTResponse.setData(data);
		
		CustomFraudSystemPaymentData fraudSystemPaymentData = new CustomFraudSystemPaymentData();
		fraudSystemPaymentData.setPaymentType("DomesticPayments");
		fraudSystemPaymentData.setCreditorDetails(new CreditorDetails());
		fraudSystemPaymentData.getCreditorDetails().setIdentification("Identification");
		
		Map<String, String> paramsMap = new HashMap<>();
		fraudSystemHelper.captureFraudEvent(dummyUserId, customerAccount, fraudSystemPaymentData, paramsMap);
	}
	
	@Test(expected = Exception.class)
	public void testCaptureFraudEvent_FilePayments() {
		String dummyUserId = "12345678";

		OBAccount6 customerAccount = new OBAccount6();
		OBWriteDomesticConsentResponse1 paymentSetupPOSTResponse = new OBWriteDomesticConsentResponse1();
		OBWriteDataDomesticConsentResponse1 data = new OBWriteDataDomesticConsentResponse1();
		OBDomestic1 initiation = new OBDomestic1();
		OBDomestic1InstructedAmount instructedAmount = new OBDomestic1InstructedAmount();
		OBRemittanceInformation1 remittanceInformation = new OBRemittanceInformation1();
		remittanceInformation.setReference("dummyref");
		instructedAmount.setAmount("99999");
		instructedAmount.setCurrency("EUR");

		initiation.setInstructedAmount(instructedAmount);
		initiation.setRemittanceInformation(remittanceInformation);
		data.setInitiation(initiation);
		data.setCreationDateTime("2017-10-05");
		paymentSetupPOSTResponse.setData(data);
		
		CustomFraudSystemPaymentData fraudSystemPaymentData = new CustomFraudSystemPaymentData();
		fraudSystemPaymentData.setPaymentType("FilePayments");
		
		Map<String, String> paramsMap = new HashMap<>();
		fraudSystemHelper.captureFraudEvent(dummyUserId, customerAccount, fraudSystemPaymentData, paramsMap);
	}

	@SuppressWarnings("unchecked")
	@Test
	public void testCaptureFraudEvent2() {
		String dummyUserId = "12345678";
		PSD2Account customerAccount = new PSD2Account();
		List<OBAccount6Account> accountList = new ArrayList<>();
		OBAccount6Account mockData2Account = new OBAccount6Account();
		mockData2Account.setIdentification("99999");
		customerAccount.setAccount(accountList);
		Map<String, String> paramsMap = new HashMap<>();
		paramsMap.put("flowType", "AISP");
		paramsMap.put("channelId", "B365");
		paramsMap.put("paymentId", "123456");
		paramsMap.put(FraudSystemRequestMapping.FS_HEADERS, "dummy");
		Map<String, Object> fraudSystemParams = new HashMap<>();
		when(fraudSystemService.callFraudSystemService(anyMap())).thenReturn(new Object());
		fraudSystemHelper.captureFraudEvent(dummyUserId, customerAccount, paramsMap, fraudSystemParams);
	}

	@Test
	public void testCaptureFraudEvent3() {
		String dummyUserId = "12345678";
		List<PSD2Account> customerAccountList = new ArrayList<>();
		Map<String, String> paramsMap = new HashMap<>();
		paramsMap.put("flowType", "AISP");
		paramsMap.put("channelId", "B365");
		paramsMap.put(FraudSystemRequestMapping.FS_HEADERS, "dummy");


		PSD2Account accObj = new PSD2Account();
		List<OBAccount6Account> accountList = new ArrayList<>();
		OBAccount6Account mockData2Account = new OBAccount6Account();
		mockData2Account.setIdentification("99999");
		accountList.add(mockData2Account);
		accObj.setAccount(accountList);

		customerAccountList.add(accObj);

		fraudSystemHelper.captureFraudEvent(dummyUserId, customerAccountList, paramsMap);
	}

	@SuppressWarnings("unchecked")
	@Test
	public void testCaptureFraudEvent4() {
		Map<String, String> paramsMap = new HashMap<>();
		paramsMap.put("flowType", "AISP");
		paramsMap.put("userName", "12345678");
		paramsMap.put("outcome", "Success");
		
		when(fraudSystemService.callFraudSystemService(anyMap())).thenReturn(new Object());
		fraudSystemHelper.captureFraudEvent(paramsMap);
	}

}
