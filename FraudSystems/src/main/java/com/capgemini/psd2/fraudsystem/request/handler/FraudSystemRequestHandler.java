package com.capgemini.psd2.fraudsystem.request.handler;

import java.util.Map;

import com.capgemini.psd2.aisp.domain.OBReadAccount6;

public interface FraudSystemRequestHandler {
	public void captureDeviceEventInfo(Map<String, Object> params);
	public OBReadAccount6 captureContactInfo(Map<String, Object> params);
	public void captureFinanacialAccountsInfo(OBReadAccount6 custInfo, Map<String, Object> params);
	public void captureTransferInfo(Map<String, Object> params);
} 