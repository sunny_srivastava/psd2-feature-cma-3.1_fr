/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.fraudsystem.service.impl;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.capgemini.psd2.aisp.domain.OBReadAccount6;
import com.capgemini.psd2.fraudsystem.adapter.FraudSystemAdapter;
import com.capgemini.psd2.fraudsystem.constants.FraudSystemConstants;
import com.capgemini.psd2.fraudsystem.policy.impl.FraudSystemStoragetPolicy;
import com.capgemini.psd2.fraudsystem.policy.impl.FraudSystemUsagePolicy;
import com.capgemini.psd2.fraudsystem.request.handler.FraudSystemRequestAttributes;
import com.capgemini.psd2.fraudsystem.request.handler.impl.FraudSystemRequestHandlerImpl;
import com.capgemini.psd2.fraudsystem.service.FraudSystemService;

@Service
public class FraudSystemServiceImpl implements FraudSystemService {
	
	private static final Logger LOG = LoggerFactory.getLogger(FraudSystemServiceImpl.class);
	
	@Autowired
	@Qualifier("FraudSystemAdapter")
	private FraudSystemAdapter fraudSystemAdapter;

	@Autowired
	private FraudSystemRequestHandlerImpl fraudSystemDataHandler;

	@Autowired
	private FraudSystemRequestAttributes fraudSystemRequestAttributes;

	@Autowired
	private FraudSystemStoragetPolicy fraudSystemStoragetPolicy;

	@Autowired
	private FraudSystemUsagePolicy fraudSystemUsagePolicy;

	@Override
	public <T> T callFraudSystemService(Map<String, Object> params) {
		T t = null;
		try {
			t = processService(params);
		} catch (Exception e) {
			LOG.error("Exception Occurred: "+e);
			return t;
		}
		return t;
	}

	private <T> T processService(Map<String, Object> params) {

		OBReadAccount6 custAccountList;

		// Capture Device Event Info - Call EveryTime
		fraudSystemDataHandler.captureDeviceEventInfo(params);
		// Capture Contact & Account info on successful SCA & on Consent
			custAccountList = fraudSystemDataHandler.captureContactInfo(params);
		
		// Capture Financial Accounts info on consent page
		if (params.get("pageType").equals(FraudSystemConstants.CONSENT_PAGE)
				&& params.get("consentStatus").equals(FraudSystemConstants.CONSENT_SUCCESS))
			fraudSystemDataHandler.captureFinanacialAccountsInfo(custAccountList, params);
		
		// Capture Financial Accounts info on PISP consent page
		if (params.get("pageType").equals(FraudSystemConstants.CONSENT_PAGE)
				&& params.get("consentStatus").equals(FraudSystemConstants.CONSENT_SUCCESS)
				&& params.get("flowType").toString().equalsIgnoreCase(FraudSystemConstants.PISP_FLOW))
			fraudSystemDataHandler.captureTransferInfo(params);
		
		// Call Fraudnet Service
		Map<String, Map<String, Object>> fraudnetRequest = fraudSystemRequestAttributes.getFraudSystemRequest();
		T fraudnetResponse = fraudSystemAdapter.retrieveFraudScore(fraudnetRequest);
		
		// Store Fraudnet Response
		fraudSystemStoragetPolicy.applyPolicy(fraudnetRequest, fraudnetResponse);

		// Use Fraudnet Response
		fraudSystemUsagePolicy.applyPolicy(fraudnetRequest, fraudnetResponse);
		return fraudnetResponse;
	}
}
