package com.capgemini.psd2.fraudsystem.constants;

public class FraudSystemConstants {
	
	public static final String FIELD_USERNAME = "username";

	public static final String LOGIN_SUCCESS = "loginSuccess";
	public static final String LOGIN_FAIL = "loginFailure";
	public static final String CONSENT_SUCCESS = "consentSuccess";
	public static final String CONSENT_FAIL = "consentFailure";
	

	public static final String LOGIN_PAGE = "Login";
	public static final String CONSENT_PAGE = "Consent";
	
	public static final String ACCOUNT_PAGE = "Account"; 
	public static final String PAYMENT_PAGE = "Payment"; 
	
	public static final String AUTHENTICATE = "Authenticate"; 
	public static final String AUTHORIZE = "Authorize"; 
	
	public static final String OUTCOME_KEY = "outcome";
	public static final String OUTCOME_SUCCESS = "Success"; 
	public static final String OUTCOME_FAILED = "Failed"; 
	
	public static final String ACTION_KEY="action";
	
	public static final String PAGETYPE_KEY="pageType";
	public static final String FLOWTYPE_KEY="flowType";
	public static final String PSUID_KEY="psuId";

	public static final String CONTACT_ID = "CONTACT1";
	public static final String FLOW_TYPE = "consentFlowType";
	public static final String USERID = "x-user-id";
	public static final String CORRELATION_ID = "x-fapi-interaction-id";
	public static final String CHANNEL_ID = "x-channel-id";
	public static final String PISP_FLOW = "PISP";
	public static final String PAYMENT_ID = "paymentId";
	// UI Fields
	public static final String EVENT_TIME_TIMESTAMP_SAAS = "timestamp";
	public static final String EVENT_TIME_TIMESTAMP_CONSENT = "X-fn-timestamp";
	public static final String EVENT_TIME_PARAMETER = "event-time";
	public static final String SESSION_DURATION = "event-time";
	public static final String DEVICE_JSC_PARAMETER = "device-jsc";
	public static final String DEVICE_PAYLOAD_PARAMETER = "device-hdim-payload";
	public static final String EVENT_ID_PARAMETER = "correlationId";
	public static final String SESSION_ID_PARAMETER = "SessionControllCookie";
	public static final String TRANS_TIME_PARAMETER = "X-fn-event-time";
	public static final String TRANS_JSC_PARAMETER = "X-fn-device-jsc";
	public static final String TRANS_HDIM_PARAMETER = "X-fn-device-hdim-payload";
	
	private FraudSystemConstants() {
	}
}