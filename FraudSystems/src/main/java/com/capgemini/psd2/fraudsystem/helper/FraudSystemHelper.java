package com.capgemini.psd2.fraudsystem.helper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.aisp.domain.OBAccount6;
import com.capgemini.psd2.consent.domain.PSD2Account;
import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.FraudSystemException;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.fraudsystem.constants.FraudSystemConstants;
import com.capgemini.psd2.fraudsystem.request.handler.impl.FraudSystemRequestMapping;
import com.capgemini.psd2.fraudsystem.service.FraudSystemService;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.enums.PaymentTypeEnum;
import com.capgemini.psd2.pisp.stage.domain.CustomFraudSystemPaymentData;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Component
public class FraudSystemHelper {

	@Autowired
	private FraudSystemService fraudSystemService;

	@Autowired
	private RequestHeaderAttributes requestHeaderAttributes;

	public void captureFraudEvent(Map<String, String> paramsMap) {
		Map<String, Object> params = new HashMap<>();
		params.put(PSD2Constants.FLOWTYPE, paramsMap.get(PSD2Constants.FLOWTYPE));
		params.put(PSD2Constants.PAGETYPE, FraudSystemConstants.LOGIN_PAGE);
		params.put(PSD2Constants.OUTCOME, paramsMap.get(PSD2Constants.OUTCOME));

		params.put(FraudSystemRequestMapping.DEVICE_HEADERS, paramsMap.get(FraudSystemRequestMapping.FS_HEADERS));
		params.put(PSD2Constants.AUTHENTICATIONSTATUS,
				"Success".equalsIgnoreCase(paramsMap.get(PSD2Constants.OUTCOME)) ? FraudSystemConstants.LOGIN_SUCCESS
						: FraudSystemConstants.LOGIN_FAIL);
		params.put(PSD2Constants.PSUID, paramsMap.get(PSD2Constants.USERNAME));
		fraudSystemService.callFraudSystemService(params);
	}

	public void captureFraudEvent(String userId, List<PSD2Account> customerAccountList, Map<String, String> paramsMap) {
		Map<String, Object> fraudSystemParams = new HashMap<String, Object>();
		if (customerAccountList != null) {
			List<String> hashedIdentificationList = customerAccountList.stream().map(PSD2Account::getHashedValue)
					.collect(Collectors.toList());
			fraudSystemParams.put("hashedIdentificationList", hashedIdentificationList);
		}
		fraudSystemParams.put(PSD2Constants.FLOWTYPE, paramsMap.get(PSD2Constants.FLOWTYPE));
		fraudSystemParams.put(PSD2Constants.PAGETYPE, FraudSystemConstants.CONSENT_PAGE);
		fraudSystemParams.put(PSD2Constants.AUTHENTICATIONSTATUS, FraudSystemConstants.LOGIN_SUCCESS);
		fraudSystemParams.put(PSD2Constants.CONSENTSTATUS,
				(userId != null) ? FraudSystemConstants.CONSENT_SUCCESS : FraudSystemConstants.CONSENT_FAIL);
		fraudSystemParams.put(PSD2Constants.PSUID, userId);
		fraudSystemParams.put(FraudSystemConstants.CHANNEL_ID, paramsMap.get(PSD2Constants.CHANNEL_NAME));
		fraudSystemParams.put(FraudSystemConstants.CORRELATION_ID, requestHeaderAttributes.getCorrelationId());
		fraudSystemParams.put(FraudSystemRequestMapping.DEVICE_HEADERS,
				paramsMap.get(FraudSystemRequestMapping.FS_HEADERS));

		fraudSystemService.callFraudSystemService(fraudSystemParams);
	}

	public Object captureFraudEvent(String userId, OBAccount6 customerAccount, Map<String, String> paramsMap,
			Map<String, Object> fraudSystemParam) {
		Map<String, Object> fraudSystemParams;
		
		if (fraudSystemParam == null) {
			fraudSystemParams = new HashMap<String, Object>();
		} else {
			fraudSystemParams = fraudSystemParam;
		}

		if (customerAccount != null) {
			List<String> hashedIdentificationList = new ArrayList<>();
			hashedIdentificationList.add(((PSD2Account) customerAccount).getHashedValue());
			fraudSystemParams.put("hashedIdentificationList", hashedIdentificationList);
		}
		fraudSystemParams.put(PSD2Constants.FLOWTYPE, paramsMap.get(PSD2Constants.FLOWTYPE));
		fraudSystemParams.put(PSD2Constants.PAGETYPE, FraudSystemConstants.CONSENT_PAGE);
		fraudSystemParams.put(PSD2Constants.AUTHENTICATIONSTATUS, FraudSystemConstants.LOGIN_SUCCESS);
		fraudSystemParams.put("consentStatus",
				(userId != null) ? FraudSystemConstants.CONSENT_SUCCESS : FraudSystemConstants.CONSENT_FAIL);
		fraudSystemParams.put(PSD2Constants.PSUID, userId);
		fraudSystemParams.put(FraudSystemConstants.CHANNEL_ID, paramsMap.get(PSD2Constants.CHANNEL_NAME));
		fraudSystemParams.put(FraudSystemConstants.CORRELATION_ID, requestHeaderAttributes.getCorrelationId());
		fraudSystemParams.put(FraudSystemConstants.PAYMENT_ID, paramsMap.get("paymentId"));
		fraudSystemParams.put(FraudSystemRequestMapping.DEVICE_HEADERS,
				paramsMap.get(FraudSystemRequestMapping.FS_HEADERS));

		return fraudSystemService.callFraudSystemService(fraudSystemParams);
	}

	public Object captureFraudEvent(String userId, OBAccount6 customerAccount,
			CustomFraudSystemPaymentData fraudSystemPaymentData, Map<String, String> paramsMap) {
		Map<String, Object> fraudSystemParams = new HashMap<String, Object>();
		if (!NullCheckUtils.isNullOrEmpty(fraudSystemPaymentData)) {
			fraudSystemParams.put(FraudSystemRequestMapping.TRANSFER_AMNT, fraudSystemPaymentData.getTransferAmount());
			fraudSystemParams.put(FraudSystemRequestMapping.TRANSFER_CURRENCY,
					fraudSystemPaymentData.getTransferCurrency());
			fraudSystemParams.put(FraudSystemRequestMapping.TRANSFER_TIME, fraudSystemPaymentData.getTransferTime());
			fraudSystemParams.put(FraudSystemRequestMapping.TRANSFER_MEMO, fraudSystemPaymentData.getTransferMemo());

			// Creditor Details are not available for File Payments
			if (!fraudSystemPaymentData.getPaymentType().equalsIgnoreCase(PaymentTypeEnum.FILE_PAY.getPaymentType()))
				fraudSystemParams.put(FraudSystemRequestMapping.TRANSFER_ACCOUNT_NUMBER,
						fraudSystemPaymentData.getCreditorDetails().getIdentification());
		}

		return captureFraudEvent(userId, customerAccount, paramsMap, fraudSystemParams);
	}

	public FraudSystemException throwFraudSystemException(ErrorCodeEnum errorCodeEnum, Map<String, String> params) {
		PSD2Exception psd2Exception = PSD2Exception.populatePSD2Exception(errorCodeEnum);
		return new FraudSystemException(psd2Exception.getMessage(), psd2Exception.getErrorInfo(), params);
	}
}