package com.capgemini.psd2.adapter.exceptions;

import org.springframework.http.HttpStatus;

public enum AdapterOBErrorCodeEnum {

	OBIE_TECHNICAL_ERROR("500 Internal Server Error","UK.OBIE.UnexpectedError", HttpStatus.INTERNAL_SERVER_ERROR, "Technical Error. Please try again later", "There was a problem processing your request. Please try again later"),
	
	OBIE_TECHNICAL_ERROR1("400 Bad Request","UK.OBIE.Field.Invalid", HttpStatus.BAD_REQUEST, "Technical Error. Please try again later", "Bad Request - The request parameters passed are invalid."),
	OBIE_TECHNICAL_ERROR2("400 Bad Request","UK.OBIE.Field.Invalid", HttpStatus.BAD_REQUEST, "Technical Error. Please try again later", " The request parameters passed are invalid"),
	
	OBIE_BAD_REQUEST("400 Bad Request","UK.OBIE.Resource.NotFound", HttpStatus.BAD_REQUEST, "Account Not Found", "Account Not Found"),
	
	OBIE_FORBIDDEN("403 Forbidden","UK.OBIE.Resource.Forbidden",HttpStatus.FORBIDDEN,"Access to requested resource denied","Forbidden"),
	
	OBIE_BAD_REQUEST_PISP("400 Bad Request","UK.OBIE.Resource.NotFound", HttpStatus.BAD_REQUEST, "Resource Not Found", "Resource Not Found."),
	UKIE_BAD_REQUEST_PISP1("400 Bad Request","IE.BOI.PaymentRejected", HttpStatus.BAD_REQUEST, "Payment Setup request has failed", "Payment Setup request has failed"),
	
	UKIE_BAD_REQUEST_PISP2("400 Bad Request","IE.BOI.PaymentRejected", HttpStatus.BAD_REQUEST, "Payment Execution request has failed", "Payment Execution request has failed"),
	
	OBIE_BAD_REQUEST_RECURRINGPAYMENTDATETIME("400 Bad Request","UK.OBIE.Field.Invalid", HttpStatus.BAD_REQUEST, "Something wrong with the request body.", "RecurringPaymentDateTime not supported"),
	
	OBIE_BAD_REQUEST_RECURRINGPAYMENTAMOUNT("400 Bad Request","UK.OBIE.Field.Invalid", HttpStatus.BAD_REQUEST, "Something wrong with the request body.", "RecurringPaymentAmount not supported"),
	
	OBIE_BAD_REQUEST_FINALPAYMENTAMOUNT("400 Bad Request","UK.OBIE.Field.Invalid", HttpStatus.BAD_REQUEST, "Something wrong with the request body.", "FinalPaymentAmount not supported"),
	
	OBIE_BAD_REQUEST_FINALPAYMENTDATETIME("400 Bad Request","UK.OBIE.Field.Invalid", HttpStatus.BAD_REQUEST, "Something wrong with the request body.", "FinalPaymentDateTime not supported"),
	
	OBIE_BAD_REQUEST_NUMBEROFPAYMENTS("400 Bad Request","UK.OBIE.Field.Invalid", HttpStatus.BAD_REQUEST, "Something wrong with the request body.", "NumberOfPayments not supported"),
	
	INV_FIELD_TPP_REQUEST("400 Bad Request","UK.OBIE.Field.Invalid",HttpStatus.BAD_REQUEST,"Technical Error. Please try again later","Invalid <errorField name in TPP Request> provided"),
	
	PAYMENT_DATE_INVALID("400 Bad Request","UK.OBIE.Field.InvalidDate",HttpStatus.BAD_REQUEST,"Technical Error. Please try again later","FirstPaymentDateTime  is invalid."),
	
	PAYEE_SCHEME_INVALID("400 Bad Request","UK.OBIE.Field.Invalid",HttpStatus.BAD_REQUEST,"Technical Error. Please try again later","CreditorAccount.SchemeName is invalid."),
	
	PAYMENT_CURRENCY_INVALID("400 Bad Request","UK.OBIE.Field.Invalid",HttpStatus.BAD_REQUEST,"Technical Error. Please try again later","FirstPaymentAmount.Currency is invalid."),
	
	PAYMENT_AMOUNT_INVALID("400 Bad Request","UK.OBIE.Field.Invalid",HttpStatus.BAD_REQUEST,"Technical Error. Please try again later","FirstPaymentAmount.Amount is invalid."),
	
	SCHEME_NAME_NOT_SUPPORTED("400 Bad Request","UK.OBIE.Unsupported.Scheme",HttpStatus.BAD_REQUEST,"Technical Error. Please try again later","SchemeName not supported."),
	
	BAD_REQUEST("400 Bad Request","UK.OBIE.Field.Invalid",HttpStatus.BAD_REQUEST,"Technical Error. Please try again later","Bad request. Please check your request."),
	
	CHANNEL_BRAND_HEADER_INVALID("400 Bad Request","UK.OBIE.Header.Invalid",HttpStatus.BAD_REQUEST,"Technical Error. Please try again later","Invalid X-BOI-BRAND header value"),
	
	OBIE_BAD_REQUEST_INVALID_REQUEST_PARAMETERS("400 Bad Request","UK.OBIE.Field.Invalid",HttpStatus.BAD_REQUEST,"Technical Error. Please try again later","Bad Request - The request parameters passed are invalid."),
	
	Invalid_payment_details_provided("400 Bad Request","UK.OBIE.Field.Invalid",HttpStatus.BAD_REQUEST,"Technical Error. Please try again later","Invalid payment details provided. Request cannot be processed."),
	
	Invalid_payment_details_provided_500("500 Internal Server Error","UK.OBIE.UnexpectedError", HttpStatus.INTERNAL_SERVER_ERROR, "Technical Error. Please try again later","There was a problem processing your request. Please try again later"),
	
	Frequency_is_invalid("400 Bad Request","UK.OBIE.Unsupported.Frequency",HttpStatus.BAD_REQUEST,"Technical Error. Please try again later","This frequency is not supported.");
	private String code;
	
	private String obErrorCode;
	
	private HttpStatus httpStatusCode;
	
	private String generalErrorMessage;

	private String specificErrorMessage;
	
	public String getCode() {
		return code;
	}

	public String getObErrorCode() {
		return obErrorCode;
	}

	public HttpStatus getHttpStatusCode() {
		return httpStatusCode;
	}

	public String getGeneralErrorMessage() {
		return generalErrorMessage;
	}

	public String getSpecificErrorMessage() {
		return specificErrorMessage;
	}
	
	public void setSpecificErrorMessage(String specificErrorMessage) {
		this.specificErrorMessage = specificErrorMessage;
	}

	private AdapterOBErrorCodeEnum(String code, String obErrorCode, HttpStatus httpStatusCode, String generalErrorMessage, String specificErrorMessage) {
		this.code = code;
		this.obErrorCode = obErrorCode;
		this.httpStatusCode = httpStatusCode;
		this.generalErrorMessage = generalErrorMessage;
		this.specificErrorMessage = specificErrorMessage;
	}
	
}
