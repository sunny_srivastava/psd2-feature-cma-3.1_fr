package com.capgemini.psd2.adapter.frequency.utility;

public class StandingOrderFrequencyConstant {
	
	public static final String YEARLY = "Yearly";
	public static final String MONTHLY = "Monthly";
	public static final String FORTNIGHTLY = "Fortnightly";
	public static final String WEEKLY = "Weekly";
	public static final String IntervalWeekDay="IntrvlWkDay:01";
	public static final String IntervalBiWeekday="IntrvlWkDay:02";
	public static final String IntervalMonthDay="IntrvlMnthDay:01";
	public static final String IntervalAnnualday="IntrvlMnthDay:12";
	public static final String IntervalWeekDayOutput="IntrvlWkDay:01:0";
	public static final String IntervalBiWeekdayOutput="IntrvlWkDay:02:0";
	public static final String IntervalMonthDayOutput="IntrvlMnthDay:01:0";
	public static final String IntervalAnnualdayOutput="IntrvlMnthDay:12:0";
	public static final String IntervalMonthDayOutput_1="IntrvlMnthDay:01:";
	public static final String IntervalAnnualdayOutput_1="IntrvlMnthDay:12:";

}
