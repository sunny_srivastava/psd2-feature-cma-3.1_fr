/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.adapter.exceptions;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.ResourceAccessException;

import com.capgemini.psd2.adapter.exceptions.domain.CommonResponse;
import com.capgemini.psd2.adapter.utility.AdapterUtility;
import com.capgemini.psd2.rest.client.exception.handler.ExceptionHandler;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * The Class AdapterExceptionHandlerImpl.
 */
@Configuration
@EnableConfigurationProperties
@ConfigurationProperties(prefix="foundationService")
public class AdapterExceptionHandlerImpl implements ExceptionHandler{

	/** The errormap. */
	private Map<String, String> errormap = new HashMap<>();	
	
	@Value("${foundationService.uiFlag:#{false}}")
	private boolean uiFlag; 
	
	@Autowired
	private AdapterUtility adapterUtility;

	/* (non-Javadoc)
	 * @see com.capgemini.psd2.rest.client.exception.handler.ExceptionHandler#handleException(org.springframework.web.client.HttpServerErrorException)
	 */
	@Override
	public void handleException(HttpServerErrorException e) {
		String responseBody = e.getResponseBodyAsString().trim();
		if (responseBody != null && responseBody.contains("code") && responseBody.contains("summary")) {
			CommonResponse errorInfoObj;
			ObjectMapper mapper = new ObjectMapper();
			try {
				errorInfoObj = mapper.readValue(responseBody, CommonResponse.class);
			} catch (IOException e1) {
				throw AdapterException.populatePSD2Exception(e1.getMessage(), AdapterErrorCodeEnum.TECHNICAL_ERROR);
			}
			if (uiFlag) {
				String errorMapping = adapterUtility.getUiErrorMap().get(errorInfoObj.getCode());
				AdapterErrorCodeEnum errorCodeEnum = AdapterErrorCodeEnum.valueOf(errorMapping);
				throw AdapterException.populatePSD2Exception(errorInfoObj.getSummary(), errorCodeEnum, errorInfoObj);
			} else {
				String errorMapping = errormap.get(errorInfoObj.getCode());
				AdapterOBErrorCodeEnum errorCodeEnum = AdapterOBErrorCodeEnum.valueOf(errorMapping);
				throw AdapterException.populatePSD2Exception(errorInfoObj.getSummary(), errorCodeEnum, errorInfoObj);
			}

		} else if (responseBody != null) {
			throw AdapterException.populatePSD2Exception(e.getMessage(), AdapterErrorCodeEnum.TECHNICAL_ERROR, e.getMessage());		
		}
		throw AdapterException.populatePSD2Exception(e.getMessage(), AdapterErrorCodeEnum.TECHNICAL_ERROR);
	}

	/* (non-Javadoc)
	 * @see com.capgemini.psd2.rest.client.exception.handler.ExceptionHandler#handleException(org.springframework.web.client.HttpClientErrorException)
	 */
	@Override
	public void handleException(HttpClientErrorException e) {
		String responseBody = e.getResponseBodyAsString();
		if (responseBody != null && responseBody.contains("code") && responseBody.contains("summary")) {
			CommonResponse errorInfoObj;
			ObjectMapper mapper = new ObjectMapper();
			try {
				errorInfoObj = mapper.readValue(responseBody, CommonResponse.class);
			} catch (IOException e1) {
				throw AdapterException.populatePSD2Exception(e1.getMessage(), AdapterErrorCodeEnum.TECHNICAL_ERROR);
			}
			if (uiFlag) {
				String errorMapping = adapterUtility.getUiErrorMap().get(errorInfoObj.getCode());
				AdapterErrorCodeEnum errorCodeEnum = AdapterErrorCodeEnum.valueOf(errorMapping);
				throw AdapterException.populatePSD2Exception(errorInfoObj.getSummary(), errorCodeEnum, errorInfoObj);
			} else {
				String errorMapping = errormap.get(errorInfoObj.getCode());
				AdapterOBErrorCodeEnum errorCodeEnum = AdapterOBErrorCodeEnum.valueOf(errorMapping);
				throw AdapterException.populatePSD2Exception(errorInfoObj.getSummary(), errorCodeEnum, errorInfoObj);
			}
		} else if (responseBody != null) {
			throw AdapterException.populatePSD2Exception(e.getMessage().trim(), AdapterErrorCodeEnum.TECHNICAL_ERROR, e.getMessage().trim());
		}
		throw AdapterException.populatePSD2Exception(e.getMessage().trim(), AdapterErrorCodeEnum.TECHNICAL_ERROR);
	}

	/* (non-Javadoc)
	 * @see com.capgemini.psd2.rest.client.exception.handler.ExceptionHandler#handleException(org.springframework.web.client.ResourceAccessException)
	 */
	@Override
	public void handleException(ResourceAccessException e) {
		throw AdapterException.populatePSD2Exception(e.getMessage(), AdapterErrorCodeEnum.TECHNICAL_ERROR);
	}

	/**
	 * Gets the errormap.
	 *
	 * @return the errormap
	 */
	public Map<String, String> getErrormap() {
		return errormap;
	}

	/**
	 * Sets the errormap.
	 *
	 * @param errormap the errormap
	 */
	public void setErrormap(Map<String, String> errormap) {
		this.errormap = errormap;
	}		
}
