package com.capgemini.psd2.pisp.international.payments.mongo.db.adapter.impl;

import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Conditional;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.conditions.MongoDbMockCondition;
import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.InternalServerErrorMessage;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.adapter.InternationalPaymentsAdapter;
import com.capgemini.psd2.pisp.domain.CustomIPaymentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.IPaymentsFoundationResource;
import com.capgemini.psd2.pisp.domain.OBCharge1;
import com.capgemini.psd2.pisp.domain.OBChargeBearerType1Code;
import com.capgemini.psd2.pisp.domain.OBExchangeRate2;
import com.capgemini.psd2.pisp.domain.OBExternalStatus2Code;
import com.capgemini.psd2.pisp.domain.OBMultiAuthorisation1;
import com.capgemini.psd2.pisp.domain.OBTransactionIndividualStatus1Code;
import com.capgemini.psd2.pisp.domain.PaymentInternationalSubmitPOST201Response;
import com.capgemini.psd2.pisp.enums.ProcessExecutionStatusEnum;
import com.capgemini.psd2.pisp.international.payments.mongo.db.adapter.repository.InternationalPaymentsFoundationRepository;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.pisp.status.PaymentConstants;
import com.capgemini.psd2.pisp.utilities.PispUtilities;
import com.capgemini.psd2.utilities.JSONUtilities;
import com.capgemini.psd2.utilities.NullCheckUtils;
import com.capgemini.psd2.utilities.SandboxValidationUtility;

@Conditional(MongoDbMockCondition.class)
@Component("internationalPaymentsMongoDbAdapter")
public class InternationalPaymentsMongoDbAdapterImpl implements InternationalPaymentsAdapter {

	private static final Logger LOG = LoggerFactory.getLogger(InternationalPaymentsMongoDbAdapterImpl.class);
	
	@Autowired
	private InternationalPaymentsFoundationRepository iPaymentSubmissionFoundationRepository;

	@Autowired
	private SandboxValidationUtility utility;

	@Autowired
	private RequestHeaderAttributes reqAttributes;

	@Value("${app.sandboxValidationPolicies.contractIdentificationPattern:^Test[a-zA-Z0-9]*}")
	private String contractIdentificationPattern;

	@Override
	public PaymentInternationalSubmitPOST201Response processPayments(CustomIPaymentsPOSTRequest customRequest,
			Map<String, OBTransactionIndividualStatus1Code> paymentStatusMap, Map<String, String> params) {

		String paymentRequest = JSONUtilities.getJSONOutPutFromObject(customRequest);
		IPaymentsFoundationResource bankResponse = JSONUtilities.getObjectFromJSONString(
				PispUtilities.getObjectMapper(), paymentRequest, IPaymentsFoundationResource.class);

		String currency = customRequest.getData().getInitiation().getInstructedAmount().getCurrency();
		String initiationAmount = customRequest.getData().getInitiation().getInstructedAmount().getAmount();
		OBChargeBearerType1Code reqChargeBearer = customRequest.getData().getInitiation().getChargeBearer();

		String submissionId = null;
		String expectedExecutionDateTime = utility.getMockedExpectedExecDtTm();
		String expectedSettlementDateTime = utility.getMockedExpectedSettlementDtTm();

		List<OBCharge1> charges = utility.getMockedOBChargeList(currency, initiationAmount, reqChargeBearer);
		OBExchangeRate2 exchangeRateInformation = utility.getMockedExchangeRateInformation(
				customRequest.getData().getInitiation().getExchangeRateInformation(), initiationAmount);
		ProcessExecutionStatusEnum processStatusEnum = utility.getMockedProcessExecStatus(currency, initiationAmount);
		OBMultiAuthorisation1 multiAuthorisation = utility.getMockedMultiAuthBlock(currency, initiationAmount);

		/*
		 * Failure mocked condition for sandbox - Invalid Contract Identification and
		 * Exchange Rate
		 */
		if (null != customRequest.getData().getInitiation().getExchangeRateInformation()
				&& null != customRequest.getData().getInitiation().getExchangeRateInformation()
						.getContractIdentification()
				&& isRejectionContractIdentification(customRequest.getData().getInitiation()
						.getExchangeRateInformation().getContractIdentification())) {
			processStatusEnum = ProcessExecutionStatusEnum.FAIL;
		}
		if (processStatusEnum == ProcessExecutionStatusEnum.PASS
				|| processStatusEnum == ProcessExecutionStatusEnum.FAIL
				|| processStatusEnum == ProcessExecutionStatusEnum.INCOMPLETE)
			submissionId = UUID.randomUUID().toString();

		bankResponse.getData().setInternationalPaymentId(submissionId);
		bankResponse.getData().setCreationDateTime(customRequest.getCreatedOn());
		bankResponse.getData().setCharges(charges);
		bankResponse.getData().setExpectedExecutionDateTime(expectedExecutionDateTime);
		bankResponse.getData().setExpectedSettlementDateTime(expectedSettlementDateTime);
		bankResponse.getData().setExchangeRateInformation(exchangeRateInformation);
		bankResponse.setProcessExecutionStatus(processStatusEnum);
		bankResponse.getData().setMultiAuthorisation(multiAuthorisation);

		if (submissionId != null) {
			OBTransactionIndividualStatus1Code status = calculateCMAStatus(processStatusEnum, multiAuthorisation,
					paymentStatusMap);
			bankResponse.getData().setStatus(status);
			bankResponse.getData().setStatusUpdateDateTime(customRequest.getCreatedOn());
		}

		try {
			iPaymentSubmissionFoundationRepository.save(bankResponse);
		} catch (DataAccessResourceFailureException exception) {
			LOG.error("DataAccessResourceFailureException in processPayments: "+exception);
			throw PSD2Exception.populatePSD2Exception(
					new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR, exception.getMessage()));
		}
		return bankResponse;
	}

	@Override
	public PaymentInternationalSubmitPOST201Response retrieveStagedPaymentsResponse(
			CustomPaymentStageIdentifiers customPaymentStageIdentifiers, Map<String, String> params) {
		PaymentInternationalSubmitPOST201Response response = null;
		IPaymentsFoundationResource x = iPaymentSubmissionFoundationRepository
				.findOneByDataInternationalPaymentId(customPaymentStageIdentifiers.getPaymentSubmissionId());
		if (!NullCheckUtils.isNullOrEmpty(x)) {
			response = new PaymentInternationalSubmitPOST201Response();
			response.setData(x.getData());
		}

		String initiationAmount = null;
		if (response != null && response.getData() != null && response.getData().getInitiation() != null
				&& response.getData().getInitiation().getInstructedAmount() != null) {
			initiationAmount = response.getData().getInitiation().getInstructedAmount().getAmount();
		}

		if ("GET".equals(reqAttributes.getMethodType())
				&& (utility.isValidAmount(initiationAmount, PSD2Constants.SANDBOX_PISP_GET_MOCKING))) {

			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
					InternalServerErrorMessage.SANDBOX_MOCKED_ERROR));

		}
		return response;
	}

	private OBTransactionIndividualStatus1Code calculateCMAStatus(ProcessExecutionStatusEnum processExecutionStatusEnum,
			OBMultiAuthorisation1 multiAuthorisation,
			Map<String, OBTransactionIndividualStatus1Code> paymentStatusMap) {

		OBTransactionIndividualStatus1Code cmaStatus = paymentStatusMap.get(PaymentConstants.INITIAL);

		if (processExecutionStatusEnum == ProcessExecutionStatusEnum.FAIL)
			cmaStatus = paymentStatusMap.get(PaymentConstants.FAIL);
		else if (processExecutionStatusEnum == ProcessExecutionStatusEnum.PASS) {
			if (multiAuthorisation == null || multiAuthorisation.getStatus() == OBExternalStatus2Code.AUTHORISED)
				cmaStatus = paymentStatusMap.get(PaymentConstants.PASS_M_AUTH);
			else if (multiAuthorisation.getStatus() == OBExternalStatus2Code.AWAITINGFURTHERAUTHORISATION)
				cmaStatus = paymentStatusMap.get(PaymentConstants.PASS_M_AWAIT);
			else if (multiAuthorisation.getStatus() == OBExternalStatus2Code.REJECTED)
				cmaStatus = paymentStatusMap.get(PaymentConstants.PASS_M_REJECT);
		}
		return cmaStatus;
	}

	private boolean isRejectionContractIdentification(String contractIdentification) {
		return contractIdentification.matches(contractIdentificationPattern);
	}
}
