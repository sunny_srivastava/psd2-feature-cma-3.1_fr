package com.capgemini.psd2.aisp.validation.test.adapter.impl;

import static org.junit.Assert.assertTrue;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.aisp.domain.OBReadOffer1;
import com.capgemini.psd2.aisp.validation.adapter.impl.AccountOffersValidatorImpl;
import com.capgemini.psd2.aisp.validation.adapter.utilities.CommonAccountValidations;
import com.capgemini.psd2.aisp.validation.test.mock.data.AccountOffersMockData;
import com.capgemini.psd2.exceptions.OBPSD2ExceptionUtility;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.validator.PSD2Validator;

@RunWith(SpringJUnit4ClassRunner.class)
public class AccountOffersValidatorImplTest {
	@Mock
	private PSD2Validator psd2Validator;

	@Mock
	private CommonAccountValidations commonAccountValidations;

	@Mock
	private RequestHeaderAttributes reqHeaderAtrributes;
	
	@InjectMocks
	private AccountOffersValidatorImpl accountOffersValidatorImpl = new AccountOffersValidatorImpl();

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		Map<String,String> genericErrorMessages=new HashMap<>();
		Map<String,String> specificErrorMessages=new HashMap<>();
		genericErrorMessages.put("INTERNAL", "INTERNAL Error");
		specificErrorMessages.put("Idempotency", "Idempotency duration is not configured in correct format.");
		OBPSD2ExceptionUtility.genericErrorMessages.putAll(genericErrorMessages);
		OBPSD2ExceptionUtility.specificErrorMessages.putAll(specificErrorMessages);
		
	}
	
	@Test
	public void validateResponseParamsTestTrue() {
		OBReadOffer1 obReadOffer1 = AccountOffersMockData.getMockExpectedAccountOffersResponse();
		
		Field reqValidationEnabled = null;
		try {
			reqValidationEnabled = AccountOffersValidatorImpl.class.getDeclaredField("resValidationEnabled");
			reqValidationEnabled.setAccessible(true);
			reqValidationEnabled.set(accountOffersValidatorImpl, true);
		} catch (NoSuchFieldException | SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		  
		accountOffersValidatorImpl.validateResponseParams(obReadOffer1);
	}
	

	@Test
	public void validateResponseParamsTestFalse() {
		Field reqValidationEnabled = null;
		try {
			reqValidationEnabled = AccountOffersValidatorImpl.class.getDeclaredField("resValidationEnabled");
			reqValidationEnabled.setAccessible(true);
			reqValidationEnabled.set(accountOffersValidatorImpl, false);
		} catch (NoSuchFieldException | SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		assertTrue(accountOffersValidatorImpl.validateResponseParams(AccountOffersMockData.getMockExpectedAccountOffersResponse()));
	}
	@Test(expected=PSD2Exception.class)
	public void testValidateUniqueIdNull(){
		accountOffersValidatorImpl.validateUniqueId(null);
	}
	
	@Test(expected=PSD2Exception.class)
	public void testValidateUniqueIdEmpty(){
		accountOffersValidatorImpl.validateUniqueId("");
	}

	@Test
	public void testValidateUniqueIdNotNull(){
		Mockito.doNothing().when(commonAccountValidations).validateUniqueUUID("c154ad51-4cd7-400f-9d84-532347f87360");
		accountOffersValidatorImpl.validateUniqueId("c154ad51-4cd7-400f-9d84-532347f87360");
	} 
		

	@Test
	public void validateRequestParamsTest(){
		OBReadOffer1 obReadOffer1 = new OBReadOffer1();
				
		Assert.assertNull(accountOffersValidatorImpl.validateRequestParams(obReadOffer1));
	}

}
