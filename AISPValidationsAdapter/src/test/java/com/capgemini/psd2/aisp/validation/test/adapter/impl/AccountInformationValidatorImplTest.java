package com.capgemini.psd2.aisp.validation.test.adapter.impl;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.aisp.domain.OBAccount6;
import com.capgemini.psd2.aisp.domain.OBAccount6Account;
import com.capgemini.psd2.aisp.domain.OBReadAccount6;
import com.capgemini.psd2.aisp.domain.OBReadAccount6Data;
import com.capgemini.psd2.aisp.mongo.db.adapter.domain.AccountInformationCMA2;
import com.capgemini.psd2.aisp.validation.adapter.impl.AccountInformationValidatorImpl;
import com.capgemini.psd2.aisp.validation.adapter.utilities.CommonAccountValidations;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.validator.PSD2Validator;

@RunWith(SpringJUnit4ClassRunner.class)
public class AccountInformationValidatorImplTest {
	

	@InjectMocks
	private AccountInformationValidatorImpl accInformationValidatorImpl;
	
	@Mock
	private CommonAccountValidations commonAccountValidations;

	@Mock
	private PSD2Validator psd2Validator;
	
			@Before
		    public void setValues() {
				MockitoAnnotations.initMocks(this);
		
	        }

		public static OBReadAccount6 getMockExpectedAccountInformationResponse() {
			OBReadAccount6Data obReadAccount2Data = new OBReadAccount6Data();
			OBReadAccount6 obReadAccount2 = new OBReadAccount6();
			List<OBAccount6> obAccount2List = new ArrayList<>();
			OBAccount6 acctInfoCma = new AccountInformationCMA2();
			
			AccountMapping accountMapping = new AccountMapping();
			List<AccountDetails> accountDetailsList = new ArrayList<>();
			AccountDetails accountDetails = new AccountDetails();

			accountDetails.setAccountId("f4483fda-81be-4873-b4a6-20375b7f55c1");
			accountDetailsList.add(accountDetails);
			accountMapping.setAccountDetails(accountDetailsList);
			
			acctInfoCma.setAccountId("f4483fda-81be-4873-b4a6-20375b7f55c1");
			acctInfoCma.setCurrency("GBP");
			
			List<OBAccount6Account> accountList=new ArrayList<>();
			OBAccount6Account account2=new OBAccount6Account();
			account2.setSchemeName("UK.OBIE.SortCodeAccountNumber");
			account2.setIdentification("123");
			account2.setSecondaryIdentification("45457");
			account2.setName("avin");
			accountList.add(account2);
			acctInfoCma.setAccount(accountList);
			
			obAccount2List.add(acctInfoCma);
			obReadAccount2Data.setAccount(obAccount2List);
			obReadAccount2.setData(obReadAccount2Data);
		
			return obReadAccount2;
		}

			
	@Test
	public void validateUniqueIdTest() {
		accInformationValidatorImpl.validateUniqueId("8727ee44-fc51-4f71-ae1c-c85f0c9c4126");
	}

	@Test(expected = PSD2Exception.class)
	public void validateUniqueIdExceptionTest() {
		accInformationValidatorImpl.validateUniqueId(null);
	}

	@Test
	public void validateRequestParamsTest() {

		accInformationValidatorImpl.validateRequestParams(getMockExpectedAccountInformationResponse());

	}

	@Test
	public void validateResponseParamsTest() {

		ReflectionTestUtils.setField(accInformationValidatorImpl, "resValidationEnabled", true);
		accInformationValidatorImpl.validateResponseParams(getMockExpectedAccountInformationResponse());

	}

	@Test
	public void validateResponseParamsresValidationEnabledfalseTest() {

		ReflectionTestUtils.setField(accInformationValidatorImpl, "resValidationEnabled", false);
		accInformationValidatorImpl.validateResponseParams(getMockExpectedAccountInformationResponse());

	}

	@Test
	public void validateResponseParamsresNullTest() {
		OBReadAccount6 obReadAccount2 = null;
		ReflectionTestUtils.setField(accInformationValidatorImpl, "resValidationEnabled", true);
		accInformationValidatorImpl.validateResponseParams(obReadAccount2);

	}
	
	
	@Test
	public void validateobReadAccount2getDataNullTest() {
		OBReadAccount6Data obReadAccount2Data = new OBReadAccount6Data();
		OBReadAccount6 obReadAccount2 = new OBReadAccount6();
		obReadAccount2Data.setAccount(null);
		obReadAccount2.setData(null);

		ReflectionTestUtils.setField(accInformationValidatorImpl, "resValidationEnabled", true);
		accInformationValidatorImpl.validateResponseParams(obReadAccount2);

	}
	

	@Test
	public void validateobReadAccount2getDataAccountNullTest() {
		OBReadAccount6Data obReadAccount2Data = new OBReadAccount6Data();
		OBReadAccount6 obReadAccount2 = new OBReadAccount6();
		obReadAccount2Data.setAccount(null);
		obReadAccount2.setData(obReadAccount2Data);

		ReflectionTestUtils.setField(accInformationValidatorImpl, "resValidationEnabled", true);
		accInformationValidatorImpl.validateResponseParams(obReadAccount2);

	}
	
	@Test
	public void validateobReadAccount2getDataNullTest1() {
		OBReadAccount6Data obReadAccount2Data = new OBReadAccount6Data();
		OBReadAccount6 obReadAccount2 = new OBReadAccount6();
		List<OBAccount6> obAccount2List = new ArrayList<>();
		OBAccount6 acctInfoCma = new AccountInformationCMA2();
		
		AccountMapping accountMapping = new AccountMapping();
		List<AccountDetails> accountDetailsList = new ArrayList<>();
		AccountDetails accountDetails = new AccountDetails();

		accountDetails.setAccountId("f4483fda-81be-4873-b4a6-20375b7f55c1");
		accountDetailsList.add(accountDetails);
		accountMapping.setAccountDetails(accountDetailsList);
		
		acctInfoCma.setAccountId("f4483fda-81be-4873-b4a6-20375b7f55c1");
		acctInfoCma.setCurrency("GBP");

		List<OBAccount6Account> accountList=new ArrayList<>();
		OBAccount6Account account2=new OBAccount6Account();
		account2.setSchemeName(null);
		account2.setIdentification(null);
		account2.setSecondaryIdentification(null);
		account2.setName("avin");
		accountList.add(account2);
		acctInfoCma.setAccount(accountList);
		
		obAccount2List.add(acctInfoCma);
		obReadAccount2Data.setAccount(obAccount2List);
		obReadAccount2.setData(obReadAccount2Data);

		ReflectionTestUtils.setField(accInformationValidatorImpl, "resValidationEnabled", true);
		accInformationValidatorImpl.validateResponseParams(obReadAccount2);

	}
	
	
	@Test
	public void validateobReadAccount2getDataNullTest2() {
		OBReadAccount6Data obReadAccount2Data = new OBReadAccount6Data();
		OBReadAccount6 obReadAccount2 = new OBReadAccount6();
		List<OBAccount6> obAccount2List = new ArrayList<>();
		OBAccount6 acctInfoCma = new AccountInformationCMA2();
		
		AccountMapping accountMapping = new AccountMapping();
		List<AccountDetails> accountDetailsList = new ArrayList<>();
		AccountDetails accountDetails = new AccountDetails();
		
		accountDetails.setAccountId("f4483fda-81be-4873-b4a6-20375b7f55c1");
		accountDetailsList.add(accountDetails);
		accountMapping.setAccountDetails(accountDetailsList);
		
		acctInfoCma.setAccountId("f4483fda-81be-4873-b4a6-20375b7f55c1");
		acctInfoCma.setCurrency("GBP");
		
		List<OBAccount6Account> accountList=new ArrayList<>();
		OBAccount6Account account2=new OBAccount6Account();
		account2.setSchemeName(null);
		account2.setIdentification(null);
		account2.setSecondaryIdentification("UK.OBIE.SortCodeAccountNumber");
		account2.setName("avin");
		accountList.add(account2);
		acctInfoCma.setAccount(null);
		
		obAccount2List.add(acctInfoCma);
		obReadAccount2Data.setAccount(obAccount2List);
		obReadAccount2.setData(obReadAccount2Data);

		ReflectionTestUtils.setField(accInformationValidatorImpl, "resValidationEnabled", true);
		accInformationValidatorImpl.validateResponseParams(obReadAccount2);

	}
		

	@Test
	public void validateobReadAccount2getDataNullTest3() {
		OBReadAccount6Data obReadAccount2Data = new OBReadAccount6Data();
		OBReadAccount6 obReadAccount2 = new OBReadAccount6();
		List<OBAccount6> obAccount2List = new ArrayList<>();
		OBAccount6 acctInfoCma = new AccountInformationCMA2();
		
		AccountMapping accountMapping = new AccountMapping();
		List<AccountDetails> accountDetailsList = new ArrayList<>();
		AccountDetails accountDetails = new AccountDetails();

		accountDetails.setAccountId("f4483fda-81be-4873-b4a6-20375b7f55c1");
		accountDetailsList.add(accountDetails);
		accountMapping.setAccountDetails(accountDetailsList);
		
		acctInfoCma.setAccountId("f4483fda-81be-4873-b4a6-20375b7f55c1");
		acctInfoCma.setCurrency("GBP");
		
		List<OBAccount6Account> accountList=new ArrayList<>();
		OBAccount6Account account2=new OBAccount6Account();
		account2.setSchemeName(null);
		account2.setIdentification(null);
		account2.setSecondaryIdentification(null);
		account2.setName("avin");
		accountList.add(account2);
		acctInfoCma.setAccount(accountList);
		
		obAccount2List.add(acctInfoCma);
		obReadAccount2Data.setAccount(obAccount2List);
		obReadAccount2.setData(obReadAccount2Data);

		ReflectionTestUtils.setField(accInformationValidatorImpl, "resValidationEnabled", true);
		accInformationValidatorImpl.validateResponseParams(obReadAccount2);

	}
}
