package com.capgemini.psd2.aisp.validation.test.adapter.impl;

import static org.junit.Assert.assertTrue;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.aisp.domain.Links;
import com.capgemini.psd2.aisp.domain.Meta;
import com.capgemini.psd2.aisp.domain.OBActiveOrHistoricCurrencyAndAmount5;
import com.capgemini.psd2.aisp.domain.OBActiveOrHistoricCurrencyAndAmount6;
import com.capgemini.psd2.aisp.domain.OBActiveOrHistoricCurrencyAndAmount7;
import com.capgemini.psd2.aisp.domain.OBActiveOrHistoricCurrencyAndAmount8;
import com.capgemini.psd2.aisp.domain.OBBalanceType1Code;
import com.capgemini.psd2.aisp.domain.OBBankTransactionCodeStructure1;
import com.capgemini.psd2.aisp.domain.OBCreditDebitCode0;
import com.capgemini.psd2.aisp.domain.OBCurrencyExchange5;
import com.capgemini.psd2.aisp.domain.OBEntryStatus1Code;
import com.capgemini.psd2.aisp.domain.OBExternalStatementType1Code;
import com.capgemini.psd2.aisp.domain.OBMerchantDetails1;
import com.capgemini.psd2.aisp.domain.OBPostalAddress6;
import com.capgemini.psd2.aisp.domain.OBReadDataStatement2;
import com.capgemini.psd2.aisp.domain.OBReadStatement2;
import com.capgemini.psd2.aisp.domain.OBStatement2;
import com.capgemini.psd2.aisp.domain.OBStatement2StatementAmount;
import com.capgemini.psd2.aisp.domain.OBStatement2StatementBenefit;
import com.capgemini.psd2.aisp.domain.OBStatement2StatementDateTime;
import com.capgemini.psd2.aisp.domain.OBStatement2StatementFee;
import com.capgemini.psd2.aisp.domain.OBStatement2StatementInterest;
import com.capgemini.psd2.aisp.domain.OBStatement2StatementRate;
import com.capgemini.psd2.aisp.domain.OBStatement2StatementValue;
import com.capgemini.psd2.aisp.domain.OBTransactionCashBalance;
import com.capgemini.psd2.aisp.mongo.db.adapter.domain.AccountInformationCMA2;
import com.capgemini.psd2.aisp.mongo.db.adapter.domain.AccountStatementsCMA2;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountInformationResponse;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountStatementsResponse;
import com.capgemini.psd2.aisp.validation.adapter.impl.AccountInformationValidatorImpl;
import com.capgemini.psd2.aisp.validation.adapter.impl.AccountStatementsValidatorImpl;
import com.capgemini.psd2.aisp.validation.adapter.impl.AccountTransactionsValidatorImpl;
import com.capgemini.psd2.aisp.validation.adapter.utilities.CommonAccountValidations;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.validator.PSD2Validator;

@RunWith(SpringJUnit4ClassRunner.class)
public class AccountStatementsValidatorImplTest {

	
	static OBStatement2StatementAmount statementAmount = new OBStatement2StatementAmount();
	static OBActiveOrHistoricCurrencyAndAmount5 benefitAmount = new OBActiveOrHistoricCurrencyAndAmount5();
	static OBStatement2StatementBenefit benefit = new OBStatement2StatementBenefit();
	static OBStatement2StatementFee statementFee = new OBStatement2StatementFee();
	static OBActiveOrHistoricCurrencyAndAmount6 statementAmount1 = new OBActiveOrHistoricCurrencyAndAmount6();
	static OBStatement2StatementInterest statementInterest = new OBStatement2StatementInterest();
	static OBActiveOrHistoricCurrencyAndAmount7 interestAmount = new OBActiveOrHistoricCurrencyAndAmount7();
	static OBStatement2StatementDateTime statementDateTime = new OBStatement2StatementDateTime();
	static OBStatement2StatementRate statementRate = new OBStatement2StatementRate();
	static OBStatement2StatementValue statementValue = new OBStatement2StatementValue();
	static OBActiveOrHistoricCurrencyAndAmount8 statementAmount3 = new OBActiveOrHistoricCurrencyAndAmount8();

	@InjectMocks
	private AccountStatementsValidatorImpl accStatementsValidatorImpl;

	@Mock
	private CommonAccountValidations commonAccountValidations;

	@Mock
	private PSD2Validator psd2Validator;

	@Before
	public void setValues() {
		MockitoAnnotations.initMocks(this);

	}

	public static OBReadStatement2 getAccountStatementsData() {

	    OBStatement2 data = new OBStatement2();
		OBReadStatement2 resp = new OBReadStatement2();
		OBReadDataStatement2 data3 = new OBReadDataStatement2();
		List<OBStatement2> dataList = new ArrayList<OBStatement2>();

		data.setAccountId("269c3ff5-d7f8-419b-a3b9-7136c5b4611a");
		data.setStatementId("8sfhke-sifhkeuf-97819");
		data.setStatementReference("002");
		data.setType(OBExternalStatementType1Code.REGULARPERIODIC);
		data.setStartDateTime("2020-07-31T00:00:00+00:00");
		data.setEndDateTime("2021-12-31T23:59:59+00:00");
		data.setCreationDateTime("2017-09-01T00:00:00+00:00");
		List<String> statementDescription = new ArrayList<String>();
		statementDescription.add("August 2017 Statement");
		data.setStatementDescription(statementDescription);
		List<OBStatement2StatementBenefit> benefitList = new ArrayList<OBStatement2StatementBenefit>();
		benefitAmount.setAmount("10.00");
		benefitAmount.setCurrency("GBP");
		benefit.setAmount(benefitAmount);
		benefit.setType("CASHBACK");
		benefitList.add(benefit);
		data.setStatementBenefit(benefitList);
		List<OBStatement2StatementFee> statementFeeList = new ArrayList<OBStatement2StatementFee>();
		statementAmount1.setAmount("65.98");
		statementAmount1.setCurrency("GBP");
		statementFee.setAmount(statementAmount1);
		statementFee.setCreditDebitIndicator(OBCreditDebitCode0.CREDIT);
		statementFee.setType("ANNUAL");
		statementFeeList.add(statementFee);
		data.setStatementFee(statementFeeList);
		List<OBStatement2StatementInterest> statementInterestList = new ArrayList<OBStatement2StatementInterest>();
		interestAmount.setAmount("45.09");
		interestAmount.setCurrency("GBP");
		statementInterest.setAmount(interestAmount);
		statementInterest.setCreditDebitIndicator(OBCreditDebitCode0.CREDIT);
		statementInterest.setType("BALANCETRANSFER");
		statementInterestList.add(statementInterest);
		data.setStatementInterest(statementInterestList);
		List<OBStatement2StatementDateTime> statementdateTimeList = new ArrayList<OBStatement2StatementDateTime>();
		statementDateTime.setDateTime("2018-07-23T10:29:22.765Z");
		statementDateTime.setType("BALANCETRANSFERPROMOEND");
		statementdateTimeList.add(statementDateTime);
		data.setStatementDateTime(statementdateTimeList);
		List<OBStatement2StatementRate> statementRateList = new ArrayList<OBStatement2StatementRate>();
		statementRate.setRate("1.5");
		statementRate.setType("ANNUALBALANCETRANSFER");
		statementRateList.add(statementRate);
		data.setStatementRate(statementRateList);
		List<OBStatement2StatementValue> statementValueList = new ArrayList<OBStatement2StatementValue>();
		statementValue.setValue("0");
		statementValue.setType("AIRMILESPOINTS");
		statementValueList.add(statementValue);
		data.setStatementValue(statementValueList);
		List<OBStatement2StatementAmount> statementAmountList = new ArrayList<OBStatement2StatementAmount>();
		statementAmount3.setAmount("400.00");
		statementAmount3.setCurrency("GBP");
		statementAmount.setAmount(statementAmount3);
		statementAmount.setCreditDebitIndicator(OBCreditDebitCode0.CREDIT);
		statementAmount.setType("CLOSINGBALANCE");
		statementAmountList.add(statementAmount);
		data.setStatementAmount(statementAmountList);
		dataList.add(data);

		data3.setStatement(dataList);
		resp.setData(data3);
		Links links = new Links();
		links.setFirst("1");
		links.setLast("10");
		links.setNext("5");
		links.setPrev("3");
		links.setSelf("4");
		resp.setLinks(links);
		Meta meta = new Meta();
		meta.setFirstAvailableDateTime("2020-07-31T00:00:00+00:00");
		meta.setLastAvailableDateTime("2020-07-31T00:00:00+00:00");
		resp.setMeta(meta);
		return resp;

	}

	

	@Test
	public void validateUniqueIdTest() {
		accStatementsValidatorImpl.validateUniqueId("8727ee44-fc51-4f71-ae1c-c85f0c9c4126");
	}

	@Test(expected = PSD2Exception.class)
	public void validateUniqueIdExceptionTest() {
		accStatementsValidatorImpl.validateUniqueId(null);
	}

	@Test
	public void validateRequestParamsTest() {

		accStatementsValidatorImpl.validateRequestParams(getAccountStatementsData());

	}

	@Test
	public void validateResponseParamsTest() {

		ReflectionTestUtils.setField(accStatementsValidatorImpl, "resValidationEnabled", true);
		accStatementsValidatorImpl.validateResponseParams(getAccountStatementsData());

	}

	@Test
	public void validateResponseParamsresValidationEnabledfalseTest() {

		ReflectionTestUtils.setField(accStatementsValidatorImpl, "resValidationEnabled", false);
		accStatementsValidatorImpl.validateResponseParams(getAccountStatementsData());

	}

	@Test
	public void validateResponseParamsresNullTest() {
		OBReadStatement2 resp = null;
		ReflectionTestUtils.setField(accStatementsValidatorImpl, "resValidationEnabled", true);
		accStatementsValidatorImpl.validateResponseParams(resp);

	}
	
	@Test
	public void validateResponseParamsresNullTest1() {
		OBReadStatement2 resp1 = null;
		ReflectionTestUtils.setField(accStatementsValidatorImpl, "resValidationEnabled", true);
		accStatementsValidatorImpl.validateResponseParams(resp1);

	}
	
	@Test
	public void validateResponseParamsresNullTest2() {
		OBReadStatement2 resp2 = new OBReadStatement2();
		OBReadDataStatement2 data3 = new OBReadDataStatement2();
				resp2.setData(null);
		ReflectionTestUtils.setField(accStatementsValidatorImpl, "resValidationEnabled", true);
		accStatementsValidatorImpl.validateResponseParams(resp2);

	}
	
	@Test
	public void validateResponseParamsresNullTest3() {
		OBReadStatement2 resp3 = new OBReadStatement2();
		OBReadDataStatement2 data3 = new OBReadDataStatement2();
		data3.setStatement(null);
		resp3.setData(data3);
		ReflectionTestUtils.setField(accStatementsValidatorImpl, "resValidationEnabled", true);
		accStatementsValidatorImpl.validateResponseParams(resp3);

	}
	
	
	@Test
	public void validateResponseParamsresNullTest4() {
		 OBStatement2 data = new OBStatement2();
		OBReadStatement2 resp4 = new OBReadStatement2();
		OBReadDataStatement2 data3 = new OBReadDataStatement2();
		List<OBStatement2> dataList = new ArrayList<OBStatement2>();

		data.setAccountId("269c3ff5-d7f8-419b-a3b9-7136c5b4611a");
		dataList.add(data);
		data3.setStatement(null);
		resp4.setData(data3);
		resp4.setMeta(null);
		ReflectionTestUtils.setField(accStatementsValidatorImpl, "resValidationEnabled", true);
		accStatementsValidatorImpl.validateResponseParams(resp4);

	}
	
	@Test
	public void validateResponseParamsresNullTest5() {
		OBStatement2 data = new OBStatement2();
		OBReadStatement2 resp5 = new OBReadStatement2();
		OBReadDataStatement2 data3 = new OBReadDataStatement2();
		List<OBStatement2> dataList = new ArrayList<OBStatement2>();

		data.setAccountId("269c3ff5-d7f8-419b-a3b9-7136c5b4611a");
		dataList.add(data);
		data3.setStatement(null);
		resp5.setData(data3);
		Meta meta = new Meta();
		resp5.setMeta(meta);
		ReflectionTestUtils.setField(accStatementsValidatorImpl, "resValidationEnabled", true);
		accStatementsValidatorImpl.validateResponseParams(resp5);

	}
	
	
	@Test
	public void validateResponseParamsresNullTest6() {
		 OBStatement2 data = new OBStatement2();
		OBReadStatement2 resp6 = new OBReadStatement2();
		OBReadDataStatement2 data3 = new OBReadDataStatement2();
		List<OBStatement2> dataList = new ArrayList<OBStatement2>();

		data.setAccountId("269c3ff5-d7f8-419b-a3b9-7136c5b4611a");
		data.setStatementId("8sfhke-sifhkeuf-97819");
		data.setStatementReference("002");
		data.setType(OBExternalStatementType1Code.REGULARPERIODIC);
		data.setStartDateTime("2020-07-31T00:00:00+00:00");
		data.setEndDateTime("2021-12-31T23:59:59+00:00");
		data.setCreationDateTime("2017-09-01T00:00:00+00:00");
		List<String> statementDescription = new ArrayList<String>();
		statementDescription.add("August 2017 Statement");
		data.setStatementDescription(statementDescription);
		List<OBStatement2StatementBenefit> benefitList = new ArrayList<OBStatement2StatementBenefit>();
		benefitAmount.setAmount("10.00");
		benefitAmount.setCurrency("GBP");
		benefit.setAmount(benefitAmount);
		benefit.setType("CASHBACK");
		benefitList.add(benefit);
		//data.setStatementBenefit(benefitList);
		List<OBStatement2StatementFee> statementFeeList = new ArrayList<OBStatement2StatementFee>();
		statementAmount1.setAmount("65.98");
		statementAmount1.setCurrency("GBP");
		statementFee.setAmount(statementAmount1);
		statementFee.setCreditDebitIndicator(OBCreditDebitCode0.CREDIT);
		statementFee.setType("ANNUAL");
		statementFeeList.add(statementFee);
		data.setStatementFee(statementFeeList);
		List<OBStatement2StatementInterest> statementInterestList = new ArrayList<OBStatement2StatementInterest>();
		interestAmount.setAmount("45.09");
		interestAmount.setCurrency("GBP");
		statementInterest.setAmount(interestAmount);
		statementInterest.setCreditDebitIndicator(OBCreditDebitCode0.CREDIT);
		statementInterest.setType("BALANCETRANSFER");
		statementInterestList.add(statementInterest);
		data.setStatementInterest(statementInterestList);
		List<OBStatement2StatementDateTime> statementdateTimeList = new ArrayList<OBStatement2StatementDateTime>();
		statementDateTime.setDateTime("2018-07-23T10:29:22.765Z");
		statementDateTime.setType("BALANCETRANSFERPROMOEND");
		statementdateTimeList.add(statementDateTime);
		data.setStatementDateTime(statementdateTimeList);
		List<OBStatement2StatementRate> statementRateList = new ArrayList<OBStatement2StatementRate>();
		statementRate.setRate("1.5");
		statementRate.setType("ANNUALBALANCETRANSFER");
		statementRateList.add(statementRate);
		data.setStatementRate(statementRateList);
		List<OBStatement2StatementValue> statementValueList = new ArrayList<OBStatement2StatementValue>();
		statementValue.setValue("0");
		statementValue.setType("AIRMILESPOINTS");
		statementValueList.add(statementValue);
		data.setStatementValue(statementValueList);
		List<OBStatement2StatementAmount> statementAmountList = new ArrayList<OBStatement2StatementAmount>();
		statementAmount3.setAmount("400.00");
		statementAmount3.setCurrency("GBP");
		statementAmount.setAmount(statementAmount3);
		statementAmount.setCreditDebitIndicator(OBCreditDebitCode0.CREDIT);
		statementAmount.setType("CLOSINGBALANCE");
		statementAmountList.add(statementAmount);
		data.setStatementAmount(statementAmountList);
		dataList.add(data);

		data3.setStatement(dataList);
		resp6.setData(data3);
		Links links = new Links();
		links.setFirst("1");
		links.setLast("10");
		links.setNext("5");
		links.setPrev("3");
		links.setSelf("4");
		resp6.setLinks(links);
		Meta meta = new Meta();
		meta.setFirstAvailableDateTime("2020-07-31T00:00:00+00:00");
		meta.setLastAvailableDateTime("2020-07-31T00:00:00+00:00");
		resp6.setMeta(meta);
	
		ReflectionTestUtils.setField(accStatementsValidatorImpl, "resValidationEnabled", true);
		accStatementsValidatorImpl.validateResponseParams(resp6);

	}

	
	@Test
	public void validateResponseParamsresNullTest7() {
		 OBStatement2 data = new OBStatement2();
		OBReadStatement2 resp7 = new OBReadStatement2();
		OBReadDataStatement2 data3 = new OBReadDataStatement2();
		List<OBStatement2> dataList = new ArrayList<OBStatement2>();

		data.setAccountId("269c3ff5-d7f8-419b-a3b9-7136c5b4611a");
		data.setStatementId("8sfhke-sifhkeuf-97819");
		data.setStatementReference("002");
		data.setType(OBExternalStatementType1Code.REGULARPERIODIC);
		data.setStartDateTime("2020-07-31T00:00:00+00:00");
		data.setEndDateTime("2021-12-31T23:59:59+00:00");
		data.setCreationDateTime("2017-09-01T00:00:00+00:00");
		List<String> statementDescription = new ArrayList<String>();
		statementDescription.add("August 2017 Statement");
		data.setStatementDescription(statementDescription);
		List<OBStatement2StatementBenefit> benefitList = new ArrayList<OBStatement2StatementBenefit>();
		benefitAmount.setAmount("10.00");
		benefitAmount.setCurrency("GBP");
		benefit.setAmount(null);
		benefit.setType("CASHBACK");
		benefitList.add(benefit);
		data.setStatementBenefit(benefitList);
		List<OBStatement2StatementFee> statementFeeList = new ArrayList<OBStatement2StatementFee>();
		statementAmount1.setAmount("65.98");
		statementAmount1.setCurrency("GBP");
		statementFee.setAmount(null);
		statementFee.setCreditDebitIndicator(OBCreditDebitCode0.CREDIT);
		statementFee.setType("ANNUAL");
		statementFeeList.add(statementFee);
		data.setStatementFee(statementFeeList);
		List<OBStatement2StatementInterest> statementInterestList = new ArrayList<OBStatement2StatementInterest>();
		interestAmount.setAmount("45.09");
		interestAmount.setCurrency("GBP");
		statementInterest.setAmount(null);
		statementInterest.setCreditDebitIndicator(OBCreditDebitCode0.CREDIT);
		statementInterest.setType("BALANCETRANSFER");
		statementInterestList.add(statementInterest);
		data.setStatementInterest(statementInterestList);
		List<OBStatement2StatementDateTime> statementdateTimeList = new ArrayList<OBStatement2StatementDateTime>();
		statementDateTime.setDateTime("2018-07-23T10:29:22.765Z");
		statementDateTime.setType("BALANCETRANSFERPROMOEND");
		statementdateTimeList.add(statementDateTime);
		data.setStatementDateTime(statementdateTimeList);
		List<OBStatement2StatementRate> statementRateList = new ArrayList<OBStatement2StatementRate>();
		statementRate.setRate("1.5");
		statementRate.setType("ANNUALBALANCETRANSFER");
		statementRateList.add(statementRate);
		data.setStatementRate(statementRateList);
		List<OBStatement2StatementValue> statementValueList = new ArrayList<OBStatement2StatementValue>();
		statementValue.setValue("0");
		statementValue.setType("AIRMILESPOINTS");
		statementValueList.add(statementValue);
		data.setStatementValue(statementValueList);
		List<OBStatement2StatementAmount> statementAmountList = new ArrayList<OBStatement2StatementAmount>();
		statementAmount3.setAmount("400.00");
		statementAmount3.setCurrency("GBP");
		statementAmount.setAmount(null);
		statementAmount.setCreditDebitIndicator(OBCreditDebitCode0.CREDIT);
		statementAmount.setType("CLOSINGBALANCE");
		statementAmountList.add(statementAmount);
		data.setStatementAmount(statementAmountList);
		dataList.add(data);

		data3.setStatement(dataList);
		resp7.setData(data3);
		Links links = new Links();
		links.setFirst("1");
		links.setLast("10");
		links.setNext("5");
		links.setPrev("3");
		links.setSelf("4");
		resp7.setLinks(links);
		Meta meta = new Meta();
		meta.setFirstAvailableDateTime("2020-07-31T00:00:00+00:00");
		meta.setLastAvailableDateTime("2020-07-31T00:00:00+00:00");
		resp7.setMeta(meta);
	
		ReflectionTestUtils.setField(accStatementsValidatorImpl, "resValidationEnabled", true);
		accStatementsValidatorImpl.validateResponseParams(resp7);

	}

	
	@Test
	public void validateResponseParamsresNullTest8() {
		 OBStatement2 data = new OBStatement2();
		OBReadStatement2 resp8 = new OBReadStatement2();
		OBReadDataStatement2 data3 = new OBReadDataStatement2();
		List<OBStatement2> dataList = new ArrayList<OBStatement2>();
		data.setAccountId("269c3ff5-d7f8-419b-a3b9-7136c5b4611a");
		data.setStatementId("8sfhke-sifhkeuf-97819");
		data.setStatementReference("002");
		data.setType(OBExternalStatementType1Code.REGULARPERIODIC);
		data.setStartDateTime("2020-07-31T00:00:00+00:00");
		data.setEndDateTime("2021-12-31T23:59:59+00:00");
		data.setCreationDateTime("2017-09-01T00:00:00+00:00");
		List<String> statementDescription = new ArrayList<String>();
		statementDescription.add("August 2017 Statement");
		data.setStatementDescription(statementDescription);
		List<OBStatement2StatementBenefit> benefitList = new ArrayList<OBStatement2StatementBenefit>();
		benefitAmount.setAmount("10.00");
		benefitAmount.setCurrency("GBP");
		benefit.setAmount(null);
		benefit.setType("CASHBACK");
		benefitList.add(benefit);
		data.setStatementBenefit(benefitList);
		List<OBStatement2StatementFee> statementFeeList = new ArrayList<OBStatement2StatementFee>();
		statementAmount1.setAmount("65.98");
		statementAmount1.setCurrency("GBP");
		statementFee.setAmount(null);
		statementFee.setCreditDebitIndicator(OBCreditDebitCode0.CREDIT);
		statementFee.setType("ANNUAL");
		statementFeeList.add(statementFee);
		data.setStatementFee(statementFeeList);
		List<OBStatement2StatementInterest> statementInterestList = new ArrayList<OBStatement2StatementInterest>();
		interestAmount.setAmount("45.09");
		interestAmount.setCurrency("GBP");
		statementInterest.setAmount(null);
		statementInterest.setCreditDebitIndicator(OBCreditDebitCode0.CREDIT);
		statementInterest.setType("BALANCETRANSFER");
		statementInterestList.add(statementInterest);
		data.setStatementInterest(statementInterestList);
		List<OBStatement2StatementDateTime> statementdateTimeList = new ArrayList<OBStatement2StatementDateTime>();
		statementDateTime.setDateTime("2018-07-23T10:29:22.765Z");
		statementDateTime.setType("BALANCETRANSFERPROMOEND");
		statementdateTimeList.add(statementDateTime);
		data.setStatementDateTime(statementdateTimeList);
		List<OBStatement2StatementRate> statementRateList = new ArrayList<OBStatement2StatementRate>();
		statementRate.setRate("1.5");
		statementRate.setType("ANNUALBALANCETRANSFER");
		statementRateList.add(statementRate);
		data.setStatementRate(statementRateList);
		List<OBStatement2StatementValue> statementValueList = new ArrayList<OBStatement2StatementValue>();
		statementValue.setValue("0");
		statementValue.setType("AIRMILESPOINTS");
		statementValueList.add(statementValue);
		data.setStatementValue(statementValueList);
		List<OBStatement2StatementAmount> statementAmountList = new ArrayList<OBStatement2StatementAmount>();
		statementAmount3.setAmount("400.00");
		statementAmount3.setCurrency("GBP");
		statementAmount.setAmount(null);
		statementAmount.setCreditDebitIndicator(OBCreditDebitCode0.CREDIT);
		statementAmount.setType("CLOSINGBALANCE");
		statementAmountList.add(statementAmount);
		data.setStatementAmount(statementAmountList);
		
		dataList.add(null);

		data3.setStatement(dataList);
		resp8.setData(data3);
		
	
		ReflectionTestUtils.setField(accStatementsValidatorImpl, "resValidationEnabled", true);
		accStatementsValidatorImpl.validateResponseParams(resp8);

	}


}