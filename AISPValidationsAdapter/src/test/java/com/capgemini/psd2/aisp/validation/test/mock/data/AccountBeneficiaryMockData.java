package com.capgemini.psd2.aisp.validation.test.mock.data;

import java.util.ArrayList;
import java.util.List;

import com.capgemini.psd2.aisp.domain.Links;
import com.capgemini.psd2.aisp.domain.Meta;
import com.capgemini.psd2.aisp.domain.OBBeneficiary5;
import com.capgemini.psd2.aisp.domain.OBBranchAndFinancialInstitutionIdentification60;
import com.capgemini.psd2.aisp.domain.OBCashAccount50;
import com.capgemini.psd2.aisp.domain.OBPostalAddress6;
import com.capgemini.psd2.aisp.domain.OBReadBeneficiary5;
import com.capgemini.psd2.aisp.domain.OBReadBeneficiary5Data;

public class AccountBeneficiaryMockData {

	
	public static OBReadBeneficiary5 getMockExpectedAccountBeneficiaryResponse() {
		OBReadBeneficiary5 obReadBeneficiary5 = new OBReadBeneficiary5();
		OBReadBeneficiary5Data obReadBeneficiary5Data = new OBReadBeneficiary5Data();
		List<OBBeneficiary5> obBeneficiaryList = new ArrayList<>();
		OBBeneficiary5 obBeneficiary5 = new OBBeneficiary5();
		obBeneficiary5.setAccountId("123");
		obBeneficiary5.setBeneficiaryId("TestBeneficiaryId");
	
		//Setting Creditor Agent
		OBBranchAndFinancialInstitutionIdentification60 creditorAgent = new OBBranchAndFinancialInstitutionIdentification60();
		OBPostalAddress6 postalAddress = new OBPostalAddress6();
		creditorAgent.setSchemeName("BICFI");
		creditorAgent.setIdentification("Testing Scheme");
		creditorAgent.setName("Testing Creditor Agent Block");
		creditorAgent.setPostalAddress(postalAddress);
		
		//Setting Creditor Account
		OBCashAccount50 creditorAccount = new OBCashAccount50();
		creditorAccount.setSchemeName("IBAN");
		creditorAccount.setIdentification("GB29NWBK60161331926819");
		creditorAccount.setName("Creditor Account");
		creditorAccount.setSecondaryIdentification("123");
		
		obBeneficiary5.setCreditorAgent(creditorAgent);
		obBeneficiary5.setCreditorAccount(creditorAccount);
		
		
		obBeneficiaryList.add(obBeneficiary5);
		obReadBeneficiary5Data.setBeneficiary(obBeneficiaryList);

		obReadBeneficiary5.setData(obReadBeneficiary5Data);

		Links links = new Links();
		obReadBeneficiary5.setLinks(links);
		Meta meta = new Meta();
		meta.setTotalPages(1);
		obReadBeneficiary5.setMeta(meta);

		return obReadBeneficiary5;
		
	}
	
	public static OBReadBeneficiary5 getMockExpectedAccountBeneficiaryNullResponse() {
		OBReadBeneficiary5 obReadBeneficiary5 = new OBReadBeneficiary5();
		OBReadBeneficiary5Data obReadBeneficiary5Data = new OBReadBeneficiary5Data();
		List<OBBeneficiary5> obBeneficiaryList = new ArrayList<>();
		OBBeneficiary5 obBeneficiary5 = new OBBeneficiary5();
		obBeneficiary5.setAccountId("123");
		obBeneficiary5.setBeneficiaryId("TestBeneficiaryId");

		
		obBeneficiaryList.add(null);
		obReadBeneficiary5Data.setBeneficiary(obBeneficiaryList);

		obReadBeneficiary5.setData(obReadBeneficiary5Data);

		Links links = new Links();
		obReadBeneficiary5.setLinks(links);
		Meta meta = new Meta();
		meta.setTotalPages(1);
		obReadBeneficiary5.setMeta(meta);

		return obReadBeneficiary5;
		
	}

	public static OBReadBeneficiary5 getMockExpectedAccountBeneficiaryResponseWithoutCreditorAccount() {
		OBReadBeneficiary5 obReadBeneficiary5 = new OBReadBeneficiary5();
		OBReadBeneficiary5Data obReadBeneficiary5Data = new OBReadBeneficiary5Data();
		List<OBBeneficiary5> obBeneficiaryList = new ArrayList<>();
		OBBeneficiary5 obBeneficiary5 = new OBBeneficiary5();
		obBeneficiary5.setAccountId("123");
		obBeneficiary5.setBeneficiaryId("TestBeneficiaryId");
	
		//Setting Creditor Agent
		OBBranchAndFinancialInstitutionIdentification60 creditorAgent = new OBBranchAndFinancialInstitutionIdentification60();
		OBPostalAddress6 postalAddress = new OBPostalAddress6();
		creditorAgent.setSchemeName("BICFI");
		creditorAgent.setIdentification("Testing Scheme");
		creditorAgent.setName("Testing Creditor Agent Block");
		creditorAgent.setPostalAddress(postalAddress);
		obBeneficiary5.setCreditorAgent(creditorAgent);

		obBeneficiaryList.add(obBeneficiary5);
		obReadBeneficiary5Data.setBeneficiary(obBeneficiaryList);

		obReadBeneficiary5.setData(obReadBeneficiary5Data);

		Links links = new Links();
		obReadBeneficiary5.setLinks(links);
		Meta meta = new Meta();
		meta.setTotalPages(1);
		obReadBeneficiary5.setMeta(meta);

		return obReadBeneficiary5;
		
	}
	
}
