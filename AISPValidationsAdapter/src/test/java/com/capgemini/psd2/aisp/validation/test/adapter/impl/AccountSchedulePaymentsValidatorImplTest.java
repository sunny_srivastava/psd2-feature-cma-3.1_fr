package com.capgemini.psd2.aisp.validation.test.adapter.impl;

import static org.junit.Assert.assertTrue;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.aisp.domain.OBReadConsent1Data;
import com.capgemini.psd2.aisp.validation.adapter.impl.AccountSchedulePaymentsValidatorImpl;
import com.capgemini.psd2.aisp.validation.adapter.utilities.CommonAccountValidations;
import com.capgemini.psd2.aisp.validation.test.mock.data.AccountSchedulePaymentValidatorMockdata;
import com.capgemini.psd2.exceptions.OBPSD2ExceptionUtility;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.validator.PSD2Validator;

@RunWith(SpringJUnit4ClassRunner.class)
public class AccountSchedulePaymentsValidatorImplTest {
	
	@InjectMocks
	AccountSchedulePaymentsValidatorImpl accountSchedulePaymentValidatorImpl;
	
	@Mock
	CommonAccountValidations commonAccountValidation;
	
	@Mock
	private PSD2Validator psd2Validator;
	
	@Mock
	private RequestHeaderAttributes reqHeaderAtrributes;
	
	@Before
	public void setUp() {
		Map<String,String> genericErrorMessages=new HashMap<>();
		Map<String,String> specificErrorMessages=new HashMap<>();
		genericErrorMessages.put("INTERNAL", "INTERNAL Error");
		specificErrorMessages.put("Idempotency", "Idempotency duration is not configured in correct format.");
		OBPSD2ExceptionUtility.genericErrorMessages.putAll(genericErrorMessages);
		OBPSD2ExceptionUtility.specificErrorMessages.putAll(specificErrorMessages);
	}
	
	@Test
	public void validateRequestParamsTest(){
		assertTrue(accountSchedulePaymentValidatorImpl.validateRequestParams(AccountSchedulePaymentValidatorMockdata.getAccountSchedulePaymentGETResponse())
															.getData().getScheduledPayment().get(0).getScheduledPaymentId().equals("SP01"));
	}
	
	@Test
	public void validateRequestParamsTest2() {
		Field reqValidationEnabled = null;
		try {
			reqValidationEnabled = AccountSchedulePaymentsValidatorImpl.class.getDeclaredField("resValidationEnabled");
			reqValidationEnabled.setAccessible(true);
			reqValidationEnabled.set(accountSchedulePaymentValidatorImpl, true);
		} catch (NoSuchFieldException | SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		
		assertTrue(accountSchedulePaymentValidatorImpl.validateResponseParams(AccountSchedulePaymentValidatorMockdata.getAccountSchedulePaymentGETResponse()));
	}
	
	@Test(expected=PSD2Exception.class)
	public void validateResponseParamsTestTrue() {
		Field reqValidationEnabled = null;
		try {
			reqValidationEnabled = AccountSchedulePaymentsValidatorImpl.class.getDeclaredField("resValidationEnabled");
			reqValidationEnabled.setAccessible(true);
			reqValidationEnabled.set(accountSchedulePaymentValidatorImpl, true);
		} catch (NoSuchFieldException | SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		Set<Object>claims=new HashSet<>();
		claims.add(OBReadConsent1Data.PermissionsEnum.READSCHEDULEDPAYMENTSDETAIL);
		Mockito.when(reqHeaderAtrributes.getClaims()).thenReturn(claims);  
		accountSchedulePaymentValidatorImpl.validateResponseParams(AccountSchedulePaymentValidatorMockdata.getAccountSchedulePaymentCreditorAccountGETResponse());
	}
	
	@Test
	public void validateResponseParamsClaimsNull() {
		Field reqValidationEnabled = null;
		try {
			reqValidationEnabled = AccountSchedulePaymentsValidatorImpl.class.getDeclaredField("resValidationEnabled");
			reqValidationEnabled.setAccessible(true);
			reqValidationEnabled.set(accountSchedulePaymentValidatorImpl, true);
		} catch (NoSuchFieldException | SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		Mockito.when(reqHeaderAtrributes.getClaims()).thenReturn(null);  
		assertTrue(accountSchedulePaymentValidatorImpl.validateResponseParams(AccountSchedulePaymentValidatorMockdata.getAccountSchedulePaymentCreditorAccountGETResponse()));
	}
	
	@Test(expected=PSD2Exception.class)
	public void validateResponseParamsCreditorAccountNull() {
		Field reqValidationEnabled = null;
		try {
			reqValidationEnabled = AccountSchedulePaymentsValidatorImpl.class.getDeclaredField("resValidationEnabled");
			reqValidationEnabled.setAccessible(true);
			reqValidationEnabled.set(accountSchedulePaymentValidatorImpl, true);
		} catch (NoSuchFieldException | SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		Set<Object>claims=new HashSet<>();
		claims.add(OBReadConsent1Data.PermissionsEnum.READSCHEDULEDPAYMENTSDETAIL);
		Mockito.when(reqHeaderAtrributes.getClaims()).thenReturn(claims);  
		assertTrue(accountSchedulePaymentValidatorImpl.validateResponseParams(AccountSchedulePaymentValidatorMockdata.getAccountSchedulePaymentCreditorAccountGETResponse()));
	}
	
	@Test
	public void validateResponseParamsSchedulePaymentNull() {
		Field reqValidationEnabled = null;
		try {
			reqValidationEnabled = AccountSchedulePaymentsValidatorImpl.class.getDeclaredField("resValidationEnabled");
			reqValidationEnabled.setAccessible(true);
			reqValidationEnabled.set(accountSchedulePaymentValidatorImpl, true);
		} catch (NoSuchFieldException | SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		Set<Object>claims=new HashSet<>();
		claims.add(OBReadConsent1Data.PermissionsEnum.READSCHEDULEDPAYMENTSDETAIL);
		Mockito.when(reqHeaderAtrributes.getClaims()).thenReturn(claims);  
		assertTrue(accountSchedulePaymentValidatorImpl.validateResponseParams(AccountSchedulePaymentValidatorMockdata.getAccountScheduledPaymentGETResponseNullData()));
	}
	
	@Test
	public void validateResponseParamsTestFalse() {
		Field reqValidationEnabled = null;
		try {
			reqValidationEnabled = AccountSchedulePaymentsValidatorImpl.class.getDeclaredField("resValidationEnabled");
			reqValidationEnabled.setAccessible(true);
			reqValidationEnabled.set(accountSchedulePaymentValidatorImpl, false);
		} catch (NoSuchFieldException | SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		assertTrue(accountSchedulePaymentValidatorImpl.validateResponseParams(AccountSchedulePaymentValidatorMockdata.getAccountSchedulePaymentGETResponse()));
	}
	
	@Test
	public void validateResponseParamsTestNull() {
		Field reqValidationEnabled = null;
		try {
			reqValidationEnabled = AccountSchedulePaymentsValidatorImpl.class.getDeclaredField("resValidationEnabled");
			reqValidationEnabled.setAccessible(true);
			reqValidationEnabled.set(accountSchedulePaymentValidatorImpl, true);
		} catch (NoSuchFieldException | SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		assertTrue(accountSchedulePaymentValidatorImpl.validateResponseParams(null));
	}
	
	@Test(expected=PSD2Exception.class)
	public void testValidateUniqueIdNull(){
		accountSchedulePaymentValidatorImpl.validateUniqueId(null);
	}
	
	@Test(expected=PSD2Exception.class)
	public void testValidateUniqueIdEmpty(){
		accountSchedulePaymentValidatorImpl.validateUniqueId("");
	}

	@Test
	public void testValidateUniqueIdNotNull(){
		  Mockito.doNothing().when(commonAccountValidation).validateUniqueUUID("c154ad51-4cd7-400f-9d84-532347f87360");
		accountSchedulePaymentValidatorImpl.validateUniqueId("c154ad51-4cd7-400f-9d84-532347f87360");
	}
	
}