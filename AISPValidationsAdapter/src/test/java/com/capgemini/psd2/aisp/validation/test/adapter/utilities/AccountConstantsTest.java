package com.capgemini.psd2.aisp.validation.test.adapter.utilities;

import static org.junit.Assert.assertTrue;

import java.lang.reflect.Constructor;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.aisp.validation.adapter.utilities.AccountsConstants;
import com.capgemini.psd2.exceptions.OBPSD2ExceptionUtility;

import junit.framework.Assert;

@RunWith(SpringJUnit4ClassRunner.class)
public class AccountConstantsTest {

	@Mock
	private AccountsConstants accountConstants;
	
	@Before
	public void setUp() {
		Map<String,String> genericErrorMessages=new HashMap<>();
		Map<String,String> specificErrorMessages=new HashMap<>();
		genericErrorMessages.put("INTERNAL", "INTERNAL Error");
		specificErrorMessages.put("Idempotency", "Idempotency duration is not configured in correct format.");
		OBPSD2ExceptionUtility.genericErrorMessages.putAll(genericErrorMessages);
		OBPSD2ExceptionUtility.specificErrorMessages.putAll(specificErrorMessages);
	}
	
	@Test
    public void testConstructorIsPrivate() throws Exception {
      Constructor<AccountsConstants> constructor = AccountsConstants.class.getDeclaredConstructor();
      assertTrue(Modifier.isPrivate(constructor.getModifiers()));
      constructor.setAccessible(true);
      constructor.newInstance();
    }
	
	@Test
    public void testprefixToAddSchemeNameList() throws Exception {
		
     List<String> testList = new ArrayList<>();
     testList.add(AccountsConstants.IBAN);
     testList.add(AccountsConstants.SORTCODEACCOUNTNUMBER);
     testList.add(AccountsConstants.PAN);	
     
     List<String> result = AccountsConstants.prefixToAddSchemeNameList();
     
     Assert.assertEquals(testList, result);
     
    }
}
