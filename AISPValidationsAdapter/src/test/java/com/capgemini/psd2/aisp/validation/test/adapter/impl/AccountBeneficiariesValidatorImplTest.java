package com.capgemini.psd2.aisp.validation.test.adapter.impl;

import static org.junit.Assert.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.capgemini.psd2.aisp.domain.OBReadBeneficiary5;
import com.capgemini.psd2.aisp.domain.OBReadConsent1Data;
import com.capgemini.psd2.aisp.validation.adapter.impl.AccountBeneficiariesValidatorImpl;
import com.capgemini.psd2.aisp.validation.adapter.utilities.CommonAccountValidations;
import com.capgemini.psd2.aisp.validation.test.mock.data.AccountBeneficiaryMockData;
import com.capgemini.psd2.exceptions.OBPSD2ExceptionUtility;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.validator.PSD2Validator;

@RunWith(SpringJUnit4ClassRunner.class)
public class AccountBeneficiariesValidatorImplTest {

	@Mock
	private PSD2Validator psd2Validator;

	@Mock
	private CommonAccountValidations commonAccountValidations;

	@Mock
	private RequestHeaderAttributes reqHeaderAtrributes;
	
	@InjectMocks
	private AccountBeneficiariesValidatorImpl accountBeneficiariesValidatorImpl = new AccountBeneficiariesValidatorImpl();

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		Map<String,String> genericErrorMessages=new HashMap<>();
		Map<String,String> specificErrorMessages=new HashMap<>();
		genericErrorMessages.put("INTERNAL", "INTERNAL Error");
		specificErrorMessages.put("Idempotency", "Idempotency duration is not configured in correct format.");
		OBPSD2ExceptionUtility.genericErrorMessages.putAll(genericErrorMessages);
		OBPSD2ExceptionUtility.specificErrorMessages.putAll(specificErrorMessages);
		
	}
	
	@Test(expected=PSD2Exception.class)
	public void testValidateUniqueIdNull(){
		accountBeneficiariesValidatorImpl.validateUniqueId(null);
		}
		
		@Test(expected=PSD2Exception.class)
		public void testValidateUniqueIdEmpty(){
			accountBeneficiariesValidatorImpl.validateUniqueId("");
		}

		@Test
		public void testValidateUniqueIdNotNull(){
				Mockito.doNothing().when(commonAccountValidations).validateUniqueUUID("c154ad51-4cd7-400f-9d84-532347f87360");
				accountBeneficiariesValidatorImpl.validateUniqueId("c154ad51-4cd7-400f-9d84-532347f87360");
	} 
		 

	@Test
	public void validateResponseParamsTest(){		 
		 
		 Field reqValidationEnabled = null;
			try {
				reqValidationEnabled = AccountBeneficiariesValidatorImpl.class.getDeclaredField("resValidationEnabled");
				reqValidationEnabled.setAccessible(true);
				reqValidationEnabled.set(accountBeneficiariesValidatorImpl, true);
			} catch (NoSuchFieldException | SecurityException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
				
		assertTrue(accountBeneficiariesValidatorImpl.validateResponseParams(AccountBeneficiaryMockData.getMockExpectedAccountBeneficiaryResponse()));
	}
	
	@Test
	public void validateResponseParamsTestFalse() {
		Field reqValidationEnabled = null;
		try {
			reqValidationEnabled = AccountBeneficiariesValidatorImpl.class.getDeclaredField("resValidationEnabled");
			reqValidationEnabled.setAccessible(true);
			reqValidationEnabled.set(accountBeneficiariesValidatorImpl, false);
		} catch (NoSuchFieldException | SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		assertTrue(accountBeneficiariesValidatorImpl.validateResponseParams(AccountBeneficiaryMockData.getMockExpectedAccountBeneficiaryResponse()));
	}
	
	@Test
	public void validateResponseParamsTestClaimsNull() {
			
		Field reqValidationEnabled = null;
		try {
			reqValidationEnabled = AccountBeneficiariesValidatorImpl.class.getDeclaredField("resValidationEnabled");
			reqValidationEnabled.setAccessible(true);
			reqValidationEnabled.set(accountBeneficiariesValidatorImpl, true);
		} catch (NoSuchFieldException | SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		Mockito.when(reqHeaderAtrributes.getClaims()).thenReturn(null);  
		accountBeneficiariesValidatorImpl.validateResponseParams(AccountBeneficiaryMockData.getMockExpectedAccountBeneficiaryResponse());
	}

	@Test(expected=PSD2Exception.class)
	public void validateResponseParamsCreditorAccountNull() {
		Field reqValidationEnabled = null;
		try {
			reqValidationEnabled = AccountBeneficiariesValidatorImpl.class.getDeclaredField("resValidationEnabled");
			reqValidationEnabled.setAccessible(true);
			reqValidationEnabled.set(accountBeneficiariesValidatorImpl, true);
		} catch (NoSuchFieldException | SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		Set<Object>claims=new HashSet<>();
		claims.add(OBReadConsent1Data.PermissionsEnum.READBENEFICIARIESDETAIL);
		Mockito.when(reqHeaderAtrributes.getClaims()).thenReturn(claims);  
		assertTrue(accountBeneficiariesValidatorImpl.validateResponseParams(AccountBeneficiaryMockData.getMockExpectedAccountBeneficiaryResponseWithoutCreditorAccount()));
	}
	
	@Test
	public void validateResponseParamsBeneficiaryNull() {
		Field reqValidationEnabled = null;
		try {
			reqValidationEnabled = AccountBeneficiariesValidatorImpl.class.getDeclaredField("resValidationEnabled");
			reqValidationEnabled.setAccessible(true);
			reqValidationEnabled.set(accountBeneficiariesValidatorImpl, true);
		} catch (NoSuchFieldException | SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		Set<Object>claims=new HashSet<>();
		claims.add(OBReadConsent1Data.PermissionsEnum.READBENEFICIARIESDETAIL);
		Mockito.when(reqHeaderAtrributes.getClaims()).thenReturn(claims);  
		assertTrue(accountBeneficiariesValidatorImpl.validateResponseParams(AccountBeneficiaryMockData.getMockExpectedAccountBeneficiaryNullResponse()));
	}
	
	@Test
	public void validateRequestParamsTest(){
		OBReadBeneficiary5 beneficiary = new OBReadBeneficiary5();
		Assert.assertNull(accountBeneficiariesValidatorImpl.validateRequestParams(beneficiary));
	}

}
