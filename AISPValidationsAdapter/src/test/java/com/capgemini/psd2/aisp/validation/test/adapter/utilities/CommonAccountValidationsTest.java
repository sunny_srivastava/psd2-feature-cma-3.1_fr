package com.capgemini.psd2.aisp.validation.test.adapter.utilities;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.aisp.domain.OBBranchAndFinancialInstitutionIdentification61;
import com.capgemini.psd2.aisp.domain.OBBranchAndFinancialInstitutionIdentification62;
import com.capgemini.psd2.aisp.domain.OBBranchAndFinancialInstitutionIdentification60;
import com.capgemini.psd2.aisp.domain.OBBranchAndFinancialInstitutionIdentification61;
import com.capgemini.psd2.aisp.domain.OBReadConsent1Data;
import com.capgemini.psd2.aisp.domain.OBReadConsent1Data.PermissionsEnum;
import com.capgemini.psd2.aisp.domain.OBPostalAddress6;
import com.capgemini.psd2.aisp.domain.OBReadConsent1;
import com.capgemini.psd2.aisp.domain.OBReadConsent1Data;
import com.capgemini.psd2.aisp.domain.OBReadConsent1Data;
import com.capgemini.psd2.aisp.utilities.AispDateUtility;
import com.capgemini.psd2.aisp.validation.adapter.utilities.AccountsConstants;
import com.capgemini.psd2.aisp.validation.adapter.utilities.CommonAccountValidations;
import com.capgemini.psd2.aisp.validation.adapter.utilities.PermissionsValidationRules;
import com.capgemini.psd2.aisp.validation.test.mock.data.AccountRequestMockData;
import com.capgemini.psd2.exceptions.OBPSD2ExceptionUtility;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.RequestHeaderAttributes;

@RunWith(SpringJUnit4ClassRunner.class)
public class CommonAccountValidationsTest {

	@InjectMocks
	private CommonAccountValidations commonAccountValidations;

	@Mock
	private RequestHeaderAttributes reqHeaderAtrributes;

	@Mock
	private PermissionsValidationRules permissionValidationRules;
	
	@Mock
	private AispDateUtility dateUtility;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		Map<String, String> map=new HashMap<>();
		map.put("SIGNATURE", "Something wrong with x-jws-signature request header");
		Map<String, String> specificErrorMessageMap=new HashMap<>();
		specificErrorMessageMap.put("signature_invalid_content", "Invalid content prsent in joseheader of x-jws-signature header");
		specificErrorMessageMap.put("signature_missing", "signature header missing in request");
		OBPSD2ExceptionUtility.genericErrorMessages.putAll(map);
		OBPSD2ExceptionUtility.specificErrorMessages.putAll(specificErrorMessageMap);
	}

	@Before
	public void initializeErrorMessages() {
		Map<String, String> genericErrorMessages = new HashMap<>();
		genericErrorMessages.put("FIELD", "freq ma");

		Map<String, String> specificErrorMessageMap = new HashMap<>();
		specificErrorMessageMap.put("FIELD", "freq ma");

		OBPSD2ExceptionUtility.genericErrorMessages.putAll(genericErrorMessages);
		OBPSD2ExceptionUtility.specificErrorMessages.putAll(specificErrorMessageMap);
	}

	@Before
	public void initializeLists() {
		String[] validSchemeNames = { "IBAN", "SortCodeAccountNumber", "PAN", "UK.OBIE.BBAN", "UK.OBIE.PAN",
				"UK.OBIE.IBAN", "UK.OBIE.SortCodeAccountNumber", "UK.OBIE.Paym" };
		List<String> validSchemeNameList = Arrays.asList(validSchemeNames);
		commonAccountValidations.getValidSchemeNameList().addAll(validSchemeNameList);

		String[] validAgentSchemeNames = { "BICFI", "UK.OBIE.BICFI" };
		List<String> validAgentSchemeNameList = Arrays.asList(validAgentSchemeNames);
		commonAccountValidations.setValidAgentSchemeList(validAgentSchemeNameList);

	}

	@Test(expected=PSD2Exception.class)
	public void isValidCurrency_emptyCurrencyCode() {
		commonAccountValidations.isValidCurrency("");
	}

	@Test(expected=PSD2Exception.class)
	public void isValidCurrency_nullCurrencyCode() {
		commonAccountValidations.isValidCurrency(null);
	}
	
	@Test
	public void isValidCurrency_validCurrencyCode() {
		Assert.assertTrue(commonAccountValidations.isValidCurrency("INR"));
	}

	@Test(expected=PSD2Exception.class)
	public void isValidCurrency_invalidCurrencyCode() {
		commonAccountValidations.isValidCurrency("Invalid");
	}

	@Test(expected=PSD2Exception.class)
	public void validateSchemeNameWithIdentification_invalidInputScheme() {
		commonAccountValidations.validateSchemeNameWithIdentification("Invalid", "");
	}
	
	@Test(expected=PSD2Exception.class)
	public void validateSchemeNameWithIdentification_validInputScheme() {
		commonAccountValidations.validateSchemeNameWithIdentification("Invalid", "");
	}

	@Test
	public void validateSchemeNameWithIdentification_invalidInputScheme_errorCodeAbsent() {
		commonAccountValidations.validateSchemeNameWithIdentification(null, "");
	}

	@Test
	public void validateSchemeNameWithIdentification_validSortCodeAccountNumber() {
		ReflectionTestUtils.setField(commonAccountValidations, "sortCodeAccountNumberPattern", "^[0-9]{6}[0-9]{8}$");
		commonAccountValidations.validateSchemeNameWithIdentification(AccountsConstants.SORTCODEACCOUNTNUMBER,
				"09012803745521");
	}
	
	@Test
	public void validateSchemeNameWithIdentification_validSortCodeAccountNumberFull() {
		ReflectionTestUtils.setField(commonAccountValidations, "sortCodeAccountNumberPattern", "^[0-9]{6}[0-9]{8}$");
		commonAccountValidations.validateSchemeNameWithIdentification(AccountsConstants.UK_OBIE_SORTCODEACCOUNTNUMBER,
				"09012803745521");
	}

	@Test
	public void validateSchemeNameWithIdentification_validBBAN() {
		commonAccountValidations.validateSchemeNameWithIdentification(AccountsConstants.UK_OBIE_BBAN, "ABCH12345678901112");
	}
	
	@Test(expected=PSD2Exception.class)
	public void validateSchemeNameWithIdentification_invalidBBAN() {
		commonAccountValidations.validateSchemeNameWithIdentification(AccountsConstants.UK_OBIE_BBAN, "AB12342342345678901112");
	}
	
	@Test
	public void validateSchemeNameWithIdentification_blankBBAN() {
		commonAccountValidations.validateSchemeNameWithIdentification(AccountsConstants.UK_OBIE_BBAN, "");
	}
	
	@Test
	public void validateSchemeNameWithIdentification_nullBBAN() {
		commonAccountValidations.validateSchemeNameWithIdentification(AccountsConstants.UK_OBIE_BBAN, null);
	}

	@Test
	public void validateSchemeNameWithIdentification_validPAN() {
		ReflectionTestUtils.setField(commonAccountValidations, "validPanIdentification", "\\d+");
		commonAccountValidations.validateSchemeNameWithIdentification(AccountsConstants.PAN, "123456789");
	}
	
	@Test
	public void validateSchemeNameWithIdentification_validPANFull() {
		ReflectionTestUtils.setField(commonAccountValidations, "validPanIdentification", "\\d+");
		commonAccountValidations.validateSchemeNameWithIdentification(AccountsConstants.UK_OBIE_PAN, "123456789");
	}
	
	@Test(expected=PSD2Exception.class)
	public void validateSchemeNameWithIdentification_invalidPAN() {
		ReflectionTestUtils.setField(commonAccountValidations, "validPanIdentification", "\\d+");
		commonAccountValidations.validateSchemeNameWithIdentification(AccountsConstants.PAN, "ABC123456789");
	}
	
	@Test
	public void validateSchemeNameWithIdentification_nullPAN() {
		ReflectionTestUtils.setField(commonAccountValidations, "validPanIdentification", "\\d+");
		commonAccountValidations.validateSchemeNameWithIdentification(AccountsConstants.PAN, null);
	}
	
	@Test
	public void validateSchemeNameWithIdentification_blankPAN() {
		ReflectionTestUtils.setField(commonAccountValidations, "validPanIdentification", "\\d+");
		commonAccountValidations.validateSchemeNameWithIdentification(AccountsConstants.PAN, "");
	}
	
	@Test
	public void validateSchemeNameWithIdentification_blankPAYM() {
		commonAccountValidations.validateSchemeNameWithIdentification(AccountsConstants.UK_OBIE_PAYM, "ABCH12345678901112");
	}
	
	@Test
	public void validateSchemeNameWithIdentification_nullPAYM() {
		commonAccountValidations.validateSchemeNameWithIdentification(AccountsConstants.UK_OBIE_PAYM, null);
	}

	@Test
	public void validateSchemeNameWithIdentification_PAYM() {
		commonAccountValidations.validateSchemeNameWithIdentification(AccountsConstants.UK_OBIE_PAYM, "Test");
	}

	@Test(expected=PSD2Exception.class)
	public void validateSchemeNameWithIdentification_invalidSortCodeAccountNumber() {
		ReflectionTestUtils.setField(commonAccountValidations, "sortCodeAccountNumberPattern", "^[0-9]{6}[0-9]{8}$");
		commonAccountValidations.validateSchemeNameWithIdentification(AccountsConstants.SORTCODEACCOUNTNUMBER,
				"Invalid13123");
	}
	
	@Test
	public void validateSchemeNameWithIdentification_nullSortCodeAccountNumber() {
		ReflectionTestUtils.setField(commonAccountValidations, "sortCodeAccountNumberPattern", "^[0-9]{6}[0-9]{8}$");
		commonAccountValidations.validateSchemeNameWithIdentification(AccountsConstants.SORTCODEACCOUNTNUMBER,
				null);
	}
	
	@Test(expected=PSD2Exception.class)
	public void validateSchemeNameWithSecondaryIdentification_sortCodeAccNum_emptySecondaryIdentification() {
		ReflectionTestUtils.setField(commonAccountValidations, "sortCodeAccountNumberPattern", "^[0-9]{6}[0-9]{8}$");
		commonAccountValidations
				.validateSchemeNameWithSecondaryIdentification(AccountsConstants.SORTCODEACCOUNTNUMBER, "");
	}
	
	@Test(expected=PSD2Exception.class)
	public void validateSchemeNameWithSecondaryIdentification_emptySortCodeNumSecondaryIdentification() {
		commonAccountValidations
				.validateSchemeNameWithSecondaryIdentification(AccountsConstants.UK_OBIE_SORTCODEACCOUNTNUMBER, "");
	}
	
	@Test(expected=PSD2Exception.class)
	public void validateSchemeNameWithSecondaryIdentification_nullIBAN() {
		commonAccountValidations
				.validateSchemeNameWithSecondaryIdentification(AccountsConstants.IBAN, null);
	}
	
	@Test(expected=PSD2Exception.class)
	public void validateSchemeNameWithSecondaryIdentification_nullUKOBIEIBAN() {
		commonAccountValidations
				.validateSchemeNameWithSecondaryIdentification(AccountsConstants.UK_OBIE_IBAN, null);
	}
	
	@Test(expected=PSD2Exception.class)
	public void validateSchemeNameWithSecondaryIdentification_invalidInputScheme() {
		commonAccountValidations.validateSchemeNameWithSecondaryIdentification("Invalid", "");
	}

	@Test(expected=PSD2Exception.class)
	public void validateSchemeNameWithSecondaryIdentification_validInputScheme_nullSortCodeAccNum() {
		commonAccountValidations
				.validateSchemeNameWithSecondaryIdentification(AccountsConstants.SORTCODEACCOUNTNUMBER, null);
	}

	@Test(expected=PSD2Exception.class)
	public void validateSchemeNameWithSecondaryIdentification_validInputScheme_invalidSecondaryIdentificationLength() {
		commonAccountValidations.validateSchemeNameWithSecondaryIdentification(
				AccountsConstants.SORTCODEACCOUNTNUMBER, "1234567890123456789012345678901234567890");
	}

	@Test
	public void validateSchemeNameWithSecondaryIdentification_validInputScheme_validSecondaryIdentificationLength() {
		commonAccountValidations.validateSchemeNameWithSecondaryIdentification(
				AccountsConstants.SORTCODEACCOUNTNUMBER, "1234567890123456789012345678901234");
	}
	
	@Test
	public void validateDomesticCreditorPostalAddress_invalidAddressLine() {
		OBPostalAddress6 obPostalAddress6 = new OBPostalAddress6();
		List<String> addressLine = new ArrayList<>();
		addressLine.add("");
		obPostalAddress6.setAddressLine(addressLine);
		commonAccountValidations.validateDomesticCreditorPostalAddress(obPostalAddress6);
	}
	
	@Test
	public void validateDomesticCreditorPostalAddress_nullAddress() {
		commonAccountValidations.validateDomesticCreditorPostalAddress(null);
	}
	
	@Test
	public void validateDomesticCreditorPostalAddress_newAddress() {
		OBPostalAddress6 obPostalAddress6 = new OBPostalAddress6();
		commonAccountValidations.validateDomesticCreditorPostalAddress(obPostalAddress6);
	}

	@Test(expected=PSD2Exception.class)
	public void validateDomesticCreditorPostalAddress_invalidAddressLineLength() {
		OBPostalAddress6 obPostalAddress6 = new OBPostalAddress6();
		List<String> addressLine = new ArrayList<>();
		addressLine.add("zsddxdfhdhSDGHre000zsddxdfhdhSDGHre000zsddxdfhdhSDGHre000zsddxdfhdhSDGH");
		obPostalAddress6.setAddressLine(addressLine);
		commonAccountValidations.validateDomesticCreditorPostalAddress(obPostalAddress6);
	}

	@Test(expected=PSD2Exception.class)
	public void validateDomesticCreditorPostalAddress_invalidSubDivisionLength() {
		OBPostalAddress6 obPostalAddress6 = new OBPostalAddress6();
		obPostalAddress6.setAddressLine(null);
		obPostalAddress6.setCountrySubDivision("zsddxdfhdhSDGHre000zsddxdfhdhSDGHre000");
		commonAccountValidations.validateDomesticCreditorPostalAddress(obPostalAddress6);
	}

	@Test(expected=PSD2Exception.class)
	public void validateDomesticCreditorPostalAddress_invalidCountryCode() {
		OBPostalAddress6 obPostalAddress6 = new OBPostalAddress6();
		obPostalAddress6.setAddressLine(null);
		obPostalAddress6.setCountrySubDivision(null);
		obPostalAddress6.setCountry("ZZ");
		commonAccountValidations.validateDomesticCreditorPostalAddress(obPostalAddress6);
	}
	
	@Test
	public void validateDomesticCreditorPostal_BlankCountryCode() {
		OBPostalAddress6 obPostalAddress6 = new OBPostalAddress6();
		obPostalAddress6.setAddressLine(null);
		obPostalAddress6.setCountrySubDivision(null);
		obPostalAddress6.setCountry("");
		commonAccountValidations.validateDomesticCreditorPostalAddress(obPostalAddress6);
	}
	
	@Test
	public void validateDomesticCreditorPostal_InvalidCountryCode() {
		OBPostalAddress6 obPostalAddress6 = new OBPostalAddress6();
		obPostalAddress6.setAddressLine(null);
		obPostalAddress6.setCountrySubDivision(null);
		obPostalAddress6.setCountry(null);
		commonAccountValidations.validateDomesticCreditorPostalAddress(obPostalAddress6);
	}
	
	@Test
	public void validateDomesticCreditorPostalAddress_validCountryCode() {
		OBPostalAddress6 obPostalAddress6 = new OBPostalAddress6();
		obPostalAddress6.setAddressLine(null);
		obPostalAddress6.setCountrySubDivision(null);
		obPostalAddress6.setCountry("GB");
		commonAccountValidations.validateDomesticCreditorPostalAddress(obPostalAddress6);
	}

	@Test
	public void validateCompletionDate_validDateTime() {
		commonAccountValidations.validateAndParseDateTimeFormatForResponse("1981-03-20T06:06:06+00:00");
	}

	@Test
	public void validateCompletionDateTime_offsetAbsent() {
		commonAccountValidations.validateAndParseDateTimeFormatForResponse("1981-03-20T06:06:06");
	}
	
	@Test
	public void validateCompletionDateTime_invalidDate() {
		commonAccountValidations.validateAndParseDateTimeFormatForResponse("191-03-20T06:06:06");
	}
	
	@Test
	public void accountRequestConfigtest() {
		PermissionsValidationRules permissionsValidationRules = new PermissionsValidationRules();
		Map<String, List<PermissionsEnum>> mandatoryRules = new HashMap<>();
		List<PermissionsEnum> rules = new ArrayList<>();
		rules.add(OBReadConsent1Data.PermissionsEnum.READACCOUNTSDETAIL);
		rules.add(OBReadConsent1Data.PermissionsEnum.READACCOUNTSBASIC);
		
		mandatoryRules.put("READACCOUNTSDETAIL", rules);
		permissionsValidationRules.setMandatoryRules(mandatoryRules);
		assertTrue(permissionsValidationRules.getMandatoryRules().equals(mandatoryRules));
		
		Map<String, List<PermissionsEnum>> orValidationRules = new HashMap<>();
		rules = new ArrayList<>();
		rules.add(OBReadConsent1Data.PermissionsEnum.READACCOUNTSBASIC);
		
		orValidationRules.put("READACCOUNTSBASIC", rules);
		permissionsValidationRules.setOrConditonRules(orValidationRules);
		assertTrue(permissionsValidationRules.getOrConditonRules().equals(orValidationRules));
	}
	
	/*@Test
	public void testMandatoryRules() {
		Map<String, List<PermissionsEnum>> mandatoryRules = new HashMap<>();
		List<PermissionsEnum> rules = new ArrayList<>();
		rules.add(OBReadConsent1Data.PermissionsEnum.READACCOUNTSDETAIL);
		rules.add(OBReadConsent1Data.PermissionsEnum.READACCOUNTSBASIC);
		rules.add(OBReadConsent1Data.PermissionsEnum.READBALANCES);
		
		mandatoryRules.put("READACCOUNTSDETAIL", rules);
		mandatoryRules.put("READBALANCES", rules);
		mandatoryRules.put("READACCOUNTSBASIC", rules);
		
		Mockito.when(permissionValidationRules.getMandatoryRules()).thenReturn(mandatoryRules);
		Mockito.when(permissionValidationRules.getRequiredPermission()).thenReturn(rules);
		Mockito.when(permissionValidationRules.getOrConditonRules()).thenReturn(mandatoryRules);
		
		OBReadConsent1 request = commonAccountValidations.validateAccountRequestInputs(AccountRequestMockData
				.getAccountRequestsParamatersMockData());
		
		assertTrue(request.getData().getPermissions().contains("READACCOUNTSBASIC"));
		
	}*/
	
	@Test
	public void testMandatoryRules_NullTime() {
		Map<String, List<PermissionsEnum>> mandatoryRules = new HashMap<>();
		List<PermissionsEnum> rules = new ArrayList<>();
		rules.add(OBReadConsent1Data.PermissionsEnum.READACCOUNTSDETAIL);
		rules.add(OBReadConsent1Data.PermissionsEnum.READACCOUNTSBASIC);
		rules.add(OBReadConsent1Data.PermissionsEnum.READBALANCES);
		
	 	mandatoryRules.put("READACCOUNTSDETAIL", rules);
		mandatoryRules.put("READBALANCES", rules);
		mandatoryRules.put("READACCOUNTSBASIC", rules);
		
		Mockito.when(permissionValidationRules.getMandatoryRules()).thenReturn(mandatoryRules);
		Mockito.when(permissionValidationRules.getRequiredPermission()).thenReturn(rules);
		Mockito.when(permissionValidationRules.getOrConditonRules()).thenReturn(mandatoryRules);
		
		commonAccountValidations.validateAccountRequestInputs(AccountRequestMockData
				.getAccountRequestsParamatersMockData_NullDates());
		
	}
	
	@Test
	public void testMandatoryRules_BlankTime() {
		Map<String, List<PermissionsEnum>> mandatoryRules = new HashMap<>();
		List<PermissionsEnum> rules = new ArrayList<>();
		rules.add(OBReadConsent1Data.PermissionsEnum.READACCOUNTSDETAIL);
		rules.add(OBReadConsent1Data.PermissionsEnum.READACCOUNTSBASIC);
		rules.add(OBReadConsent1Data.PermissionsEnum.READBALANCES);
		
	 	mandatoryRules.put("READACCOUNTSDETAIL", rules);
		mandatoryRules.put("READBALANCES", rules);
		mandatoryRules.put("READACCOUNTSBASIC", rules);
		
		Mockito.when(permissionValidationRules.getMandatoryRules()).thenReturn(mandatoryRules);
		Mockito.when(permissionValidationRules.getRequiredPermission()).thenReturn(rules);
		Mockito.when(permissionValidationRules.getOrConditonRules()).thenReturn(mandatoryRules);
		
		commonAccountValidations.validateAccountRequestInputs(AccountRequestMockData
				.getAccountRequestsParamatersMockData_BlankDates());
		
	}
	
	@Test
	public void testMandatoryRules_OffsetCheck() {
		Map<String, List<PermissionsEnum>> mandatoryRules = new HashMap<>();
		List<PermissionsEnum> rules = new ArrayList<>();
		rules.add(OBReadConsent1Data.PermissionsEnum.READACCOUNTSDETAIL);
		rules.add(OBReadConsent1Data.PermissionsEnum.READACCOUNTSBASIC);
		rules.add(OBReadConsent1Data.PermissionsEnum.READBALANCES);
		
	 	mandatoryRules.put("READACCOUNTSDETAIL", rules);
		mandatoryRules.put("READBALANCES", rules);
		mandatoryRules.put("READACCOUNTSBASIC", rules);
		
		Mockito.when(permissionValidationRules.getMandatoryRules()).thenReturn(mandatoryRules);
		Mockito.when(permissionValidationRules.getRequiredPermission()).thenReturn(rules);
		Mockito.when(permissionValidationRules.getOrConditonRules()).thenReturn(mandatoryRules);
		
		commonAccountValidations.validateAccountRequestInputs(AccountRequestMockData
				.getAccountRequestsParamatersMockData_NoOffsetInDates());
		
	}
	
	@Test(expected=PSD2Exception.class)
	public void testExcludedPermission() {
		Map<String, List<PermissionsEnum>> excludedPermission = new HashMap<>();
		List<PermissionsEnum> rules = new ArrayList<>();
		rules.add(OBReadConsent1Data.PermissionsEnum.READPARTY);
		rules.add(OBReadConsent1Data.PermissionsEnum.READOFFERS);
		
		excludedPermission.put("READPARTY", rules);
		excludedPermission.put("READOFFERS", rules);
		
		Mockito.when(permissionValidationRules.getExcludedPermission()).thenReturn(excludedPermission);
		OBReadConsent1 request = commonAccountValidations.validateAccountRequestInputs(AccountRequestMockData
				.getAccountRequestsParamatersMockData());
		
		assertTrue(request.getData().getPermissions().contains("READPARTY"));
		
	}
	
	
	
	@Test(expected=PSD2Exception.class)
	public void testOrConditionRulesException() {
		Map<String, List<PermissionsEnum>> mandatoryRules = new HashMap<>();
		List<PermissionsEnum> rules = new ArrayList<>();
		rules.add(OBReadConsent1Data.PermissionsEnum.READACCOUNTSDETAIL);
		rules.add(OBReadConsent1Data.PermissionsEnum.READACCOUNTSBASIC);
		mandatoryRules.put("READACCOUNTSDETAIL", rules);
		Mockito.when(permissionValidationRules.getMandatoryRules()).thenReturn(mandatoryRules);
		
		rules = new ArrayList<>();
		Map<String, List<PermissionsEnum>> orValidationRules = new HashMap<>();
		rules.add(OBReadConsent1Data.PermissionsEnum.READTRANSACTIONSCREDITS);
		rules.add(OBReadConsent1Data.PermissionsEnum.READTRANSACTIONSDEBITS);
		orValidationRules.put("READACCOUNTSBASIC", rules);
		
		Mockito.when(permissionValidationRules.getOrConditonRules()).thenReturn(orValidationRules);
		
		commonAccountValidations.validateAccountRequestInputs(AccountRequestMockData
				.getAccountRequestsParamatersExceptionMockData());
	}
	
	@Test(expected=PSD2Exception.class)
	public void testValidateEmptyRequestExceptions() {
		OBReadConsent1 request = new OBReadConsent1();
		commonAccountValidations.validateAccountRequestInputs(request);	
	}
	
	@Test(expected=PSD2Exception.class)
	public void testValidateNullRequestExceptions() {
		OBReadConsent1 request = null;
		commonAccountValidations.validateAccountRequestInputs(request);	
	}
	
	@Test(expected=PSD2Exception.class)
	public void testValidateDateRequestExceptions() {
		OBReadConsent1 request = new OBReadConsent1();
		
		OBReadConsent1Data data = new OBReadConsent1Data();
		
		Long longDate = 20170502000000L;
		data.setExpirationDateTime(longDate.toString());
			
		request.setData(data);
		request.setRisk(null);
		
		commonAccountValidations.validateAccountRequestInputs(request);
	}

	@Test(expected=PSD2Exception.class)
	public void testValidateFromToDateRequestExceptions() {
		OBReadConsent1 request = new OBReadConsent1();
		
		OBReadConsent1Data data = new OBReadConsent1Data();
		data.setExpirationDateTime("2029-05-08T00:00:00.050");			
		data.setTransactionFromDateTime("2016-05-05T00:00:00.050");
		data.setTransactionToDateTime("2017-05-03T00:00:00.060");
		
		request.setData(data);
		request.setRisk(null);
		
		commonAccountValidations.validateAccountRequestInputs(request);
	}
	
	@Test(expected=PSD2Exception.class)
	public void testValidateFromDateRequestExceptions() {
		OBReadConsent1 request = new OBReadConsent1();
		
		OBReadConsent1Data data = new OBReadConsent1Data();
		data.setTransactionFromDateTime("2016-05-05T00:00:00.050");
		data.setTransactionToDateTime("2017-05-03T00:00:00.060");
		data.setExpirationDateTime("2018-05-03T00:00:00.060");
				
		request.setData(data);
		request.setRisk(null);
		
		commonAccountValidations.validateAccountRequestInputs(request);
	}
	
	@Test(expected=PSD2Exception.class)
	public void testValidateToDateRequestExceptions() {
		OBReadConsent1 request = new OBReadConsent1();
		
		OBReadConsent1Data data = new OBReadConsent1Data();
		data.setTransactionFromDateTime("2016-05-05T00:00:00.050");
		data.setTransactionToDateTime("2017-05-03T00:00:00.060");
		data.setExpirationDateTime("2018-05-03T00:00:00.060");
		
				
		request.setData(data);
		request.setRisk(null);
		
		commonAccountValidations.validateAccountRequestInputs(request);
	}
	
	@Test(expected=PSD2Exception.class)
	public void testValidateNullExpirationDateRequestExceptions() {
		OBReadConsent1 request = new OBReadConsent1();
		
		OBReadConsent1Data data = new OBReadConsent1Data();
		data.setTransactionFromDateTime("2016-05-05T00:00:00.050");
		data.setTransactionToDateTime("2017-05-03T00:00:00.060");
		data.setExpirationDateTime("2018-05-03T00:00:00.060");
		
		request.setData(data);
		request.setRisk(null);
		
		commonAccountValidations.validateAccountRequestInputs(request);
	}
	
	@Test(expected=PSD2Exception.class)
	public void testValidateExpirationDateRequestExceptions() {
		OBReadConsent1 request = new OBReadConsent1();
		
		OBReadConsent1Data data = new OBReadConsent1Data();
		data.setExpirationDateTime("2016-05-08T00:00:00.050");			
		data.setTransactionFromDateTime("2016-05-05T00:00:00.050");
		data.setTransactionToDateTime("2019-05-03T00:00:00.060");
		
		request.setData(data);
		request.setRisk(null);
		
		commonAccountValidations.validateAccountRequestInputs(request);
	}
		
	@Test(expected=PSD2Exception.class)
	public void testValidateEmptyPermissionsRequestExceptions() {
		OBReadConsent1 request = new OBReadConsent1();
		
		OBReadConsent1Data data = new OBReadConsent1Data();
		data.setExpirationDateTime("2019-05-08T00:00:00.050");			
		data.setTransactionFromDateTime("2017-05-02T00:00:00.050");
		data.setTransactionToDateTime("2017-05-03T00:00:00.050");
		
		List<PermissionsEnum> permissions = new ArrayList<>();
		data.setPermissions(permissions);
		
		request.setData(data);
		request.setRisk(null);
		
		commonAccountValidations.validateAccountRequestInputs(request);

	}
	
	@Test(expected=PSD2Exception.class)
	public void testValidateNullPermissionsRequestExceptions() {
		OBReadConsent1 request = new OBReadConsent1();
		
		OBReadConsent1Data data = new OBReadConsent1Data();
		data.setExpirationDateTime("2019-05-08T00:00:00.050");			
		data.setTransactionFromDateTime("2017-05-02T00:00:00.050");
		data.setTransactionToDateTime("2017-05-03T00:00:00.050");
		
		data.setPermissions(null);
		request.setData(data);
		request.setRisk(null);
		commonAccountValidations.validateAccountRequestInputs(request);
	}
	
	@Test(expected=PSD2Exception.class)
	public void testValidatePermissionIncludesNullRequestExceptions() {
		OBReadConsent1 request = new OBReadConsent1();
		
		OBReadConsent1Data data = new OBReadConsent1Data();
		data.setExpirationDateTime("2019-05-08T00:00:00.050");			
		data.setTransactionFromDateTime("2017-05-02T00:00:00.050");
		data.setTransactionToDateTime("2017-05-03T00:00:00.050");
		
		List<PermissionsEnum> permissions = new ArrayList<>();
		permissions.add(null);
		data.setPermissions(permissions);
		
		request.setData(data);
		request.setRisk(null);
		commonAccountValidations.validateAccountRequestInputs(request);
	}
	
	@Test(expected=PSD2Exception.class)
	public void testValidateAccountRequestIdException() {
		String accountRequestId = "56981c81-a529-45be-9ea8-88addc01858$";
		ReflectionTestUtils.setField(commonAccountValidations, "validAccountRequestIdChars", "[^A-Za-z0-9-]+");
		commonAccountValidations.validateUniqueUUID(accountRequestId);
	}
	
	@Test
	public void testValidateAccountRequestId() {
		String accountRequestId = "56981c81-a529-45be-9ea8-88addc018585";
		ReflectionTestUtils.setField(commonAccountValidations, "validAccountRequestIdChars", "[a-zA-Z0-9-]{1,40}");
		commonAccountValidations.validateUniqueUUID(accountRequestId);
	}
	
	@Test(expected=PSD2Exception.class)
	public void testValidateAccountRequestIdInvalidLengthException() {
		String accountRequestId = "56981c81-a529-45be-9ea8-88addc0185856981c81-a529-45be-9ea8-88addc0185856981c81-a529-45be-9ea8-88addc0185856981c81-a529-45be-9ea8-88addc01858";
		commonAccountValidations.validateUniqueUUID(accountRequestId);
	}
	
	@Test(expected=PSD2Exception.class)
	public void testValidateAccountRequestIdInvalidLengthZeroException() {
		String accountRequestId = "";
		commonAccountValidations.validateUniqueUUID(accountRequestId);
	}
	
	@After
	public void tearDown() throws Exception {
		permissionValidationRules = null;
	}
	
	@Test
	public void validateCreditorOrDebtorAgent() {
		OBBranchAndFinancialInstitutionIdentification62 creditorAgent = new OBBranchAndFinancialInstitutionIdentification62();
		creditorAgent.setSchemeName("BICFI");
		creditorAgent.setIdentification("123456789");
		creditorAgent.setName("Test Name");
		
		OBPostalAddress6 obPostalAddress6 = new OBPostalAddress6();
		
		List<String> address = new ArrayList<String>();
		address.add("Test");		
		obPostalAddress6.setAddressLine(address);
		obPostalAddress6.setCountrySubDivision("Test");
		obPostalAddress6.setCountry("GB");
		creditorAgent.setPostalAddress(obPostalAddress6);
		
		commonAccountValidations.validateDebtorAgent(creditorAgent);
	}
	
	@Test(expected=PSD2Exception.class)
	public void validateCreditorOrDebtorAgent_BlankNameAndPostalAddress() {
		OBBranchAndFinancialInstitutionIdentification62 creditorAgent = new OBBranchAndFinancialInstitutionIdentification62();
		creditorAgent.setSchemeName("");
		creditorAgent.setIdentification("");
		creditorAgent.setName("");
		
		OBPostalAddress6 obPostalAddress6 = new OBPostalAddress6();
		obPostalAddress6.setAddressLine(null);
		obPostalAddress6.setCountrySubDivision(null);
		obPostalAddress6.setCountry("");
		creditorAgent.setPostalAddress(obPostalAddress6);
		
		commonAccountValidations.validateDebtorAgent(creditorAgent);
	}
	
	@Test(expected=PSD2Exception.class)
	public void validateCreditorOrDebtorAgent_BlankIdentification() {
		OBBranchAndFinancialInstitutionIdentification62 creditorAgent = new OBBranchAndFinancialInstitutionIdentification62();
		creditorAgent.setSchemeName("BICFI");
		creditorAgent.setIdentification("");
		creditorAgent.setName("");
		
		OBPostalAddress6 obPostalAddress6 = new OBPostalAddress6();
		obPostalAddress6.setAddressLine(null);
		obPostalAddress6.setCountrySubDivision(null);
		obPostalAddress6.setCountry("");
		creditorAgent.setPostalAddress(obPostalAddress6);
		
		commonAccountValidations.validateDebtorAgent(creditorAgent);
	}
	
	@Test(expected=PSD2Exception.class)
	public void validateCreditorOrDebtorAgent_Invalidscheme() {
		OBBranchAndFinancialInstitutionIdentification62 creditorAgent = new OBBranchAndFinancialInstitutionIdentification62();
		creditorAgent.setSchemeName("IBAN");
		creditorAgent.setIdentification("GB29NWBK60161331926819");
		creditorAgent.setName("Test Name");
		
		OBPostalAddress6 obPostalAddress6 = new OBPostalAddress6();
		obPostalAddress6.setAddressLine(null);
		obPostalAddress6.setCountrySubDivision(null);
		obPostalAddress6.setCountry("GB");
		creditorAgent.setPostalAddress(obPostalAddress6);
		
		commonAccountValidations.validateDebtorAgent(creditorAgent);
	}
	
	@Test
	public void validateCreditorOrDebtorAgent_null() {
		OBBranchAndFinancialInstitutionIdentification62 creditorAgent = null;
		commonAccountValidations.validateDebtorAgent(creditorAgent);
	}
	
	@Test(expected=PSD2Exception.class)
	public void validateCreditorOrDebtorAgent_blank() {
		OBBranchAndFinancialInstitutionIdentification62 creditorAgent = new OBBranchAndFinancialInstitutionIdentification62();
		commonAccountValidations.validateDebtorAgent(creditorAgent);
	}
	
	@Test(expected=PSD2Exception.class)
	public void validateCreditorAgent_blankSchemeName() {
		OBBranchAndFinancialInstitutionIdentification60 creditorAgent = new OBBranchAndFinancialInstitutionIdentification60();
		creditorAgent.setSchemeName("");
		creditorAgent.setIdentification("Test");
		commonAccountValidations.validateCreditorAgent(creditorAgent);
	}
	
	@Test(expected=PSD2Exception.class)
	public void validateCreditorAgent_nullSchemeName() {
		OBBranchAndFinancialInstitutionIdentification60 creditorAgent = new OBBranchAndFinancialInstitutionIdentification60();
		creditorAgent.setSchemeName(null);
		creditorAgent.setIdentification("Test");
		commonAccountValidations.validateCreditorAgent(creditorAgent);
	}

	@Test(expected=PSD2Exception.class)
	public void validateCreditorAgent_blankId() {
		OBBranchAndFinancialInstitutionIdentification61 creditorAgent = new OBBranchAndFinancialInstitutionIdentification61();
		creditorAgent.setSchemeName("BICFI");
		creditorAgent.setIdentification("");
		commonAccountValidations.validateCreditorAgent(creditorAgent);
	}
	@Test(expected=PSD2Exception.class)
	public void validateCreditorAgent_nullId() {
		OBBranchAndFinancialInstitutionIdentification61 creditorAgent = new OBBranchAndFinancialInstitutionIdentification61();
		creditorAgent.setSchemeName("BICFI");
		creditorAgent.setIdentification(null);
		commonAccountValidations.validateCreditorAgent(creditorAgent);
	}
	
	@Test(expected=PSD2Exception.class)
	public void validateCreditorAgent() {
		OBBranchAndFinancialInstitutionIdentification61 creditorAgent = new OBBranchAndFinancialInstitutionIdentification61();
		creditorAgent.setSchemeName("BICFI");
		creditorAgent.setIdentification("");
		commonAccountValidations.validateCreditorAgent(creditorAgent);
	}
	
	@Test(expected=PSD2Exception.class)
	public void validateCreditorAgent_InvalidSchemeName() {
		OBBranchAndFinancialInstitutionIdentification60 creditorAgent = new OBBranchAndFinancialInstitutionIdentification60();
		creditorAgent.setSchemeName("IBAN");
		commonAccountValidations.validateCreditorAgent(creditorAgent);
	}
	
	@Test
	public void validateCreditorAgent_NullCreditorAgent() {
		OBBranchAndFinancialInstitutionIdentification60 creditorAgent = null;
		commonAccountValidations.validateCreditorAgent(creditorAgent);
	}
	
	/*@Test
	public void validateCreditorAgent_BlankSchemeName() {
		OBBranchAndFinancialInstitutionIdentification61 creditorAgent = new OBBranchAndFinancialInstitutionIdentification61();
		creditorAgent.setSchemeName("");
		creditorAgent.setIdentification("");
		commonAccountValidations.validateCreditorAgent(creditorAgent);
	}*/
	
	@Test
	public void validateAmount() {
		commonAccountValidations.validateAmount("10.00"); 
	}
	
	@Test
	public void validateAmount_nullValue() {
		commonAccountValidations.validateAmount(null); 
	}
	
	@Test
	public void validateAmount_blankValue() {
		commonAccountValidations.validateAmount(""); 
	}
	
	@Test(expected=PSD2Exception.class)
	public void validateAmount_invalidValue() {
		commonAccountValidations.validateAmount("ABCDEFGH.23454");
	}
	
	@Test
	public void getValidAgentSchemeList() {
		assertNotNull(commonAccountValidations.getValidAgentSchemeList()); 
	}
	
	@Test
	public void getValidSchemeNameList() {
		assertNotNull(commonAccountValidations.getValidSchemeNameList()); 
	}
	
}