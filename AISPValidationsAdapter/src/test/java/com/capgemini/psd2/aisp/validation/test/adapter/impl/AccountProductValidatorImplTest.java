package com.capgemini.psd2.aisp.validation.test.adapter.impl;

import static org.junit.Assert.*;
import static org.mockito.Mockito.doAnswer;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.capgemini.psd2.aisp.domain.OBBCAData1;
import com.capgemini.psd2.aisp.domain.OBPCAData1;
import com.capgemini.psd2.aisp.domain.OBReadProduct2;
import com.capgemini.psd2.aisp.domain.OBReadProduct2Data;
import com.capgemini.psd2.aisp.domain.OBReadProduct2DataOtherProductType;
import com.capgemini.psd2.aisp.domain.OBReadProduct2DataProduct;
import com.capgemini.psd2.aisp.domain.OBReadProduct2DataProduct.ProductTypeEnum;
import com.capgemini.psd2.aisp.validation.adapter.impl.AccountBeneficiariesValidatorImpl;
import com.capgemini.psd2.aisp.validation.adapter.impl.AccountProductsValidatorImpl;
import com.capgemini.psd2.aisp.validation.adapter.utilities.CommonAccountValidations;
import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.InternalServerErrorMessage;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.OBPSD2ExceptionUtility;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.token.Token;
import com.capgemini.psd2.validator.PSD2Validator;

public class AccountProductValidatorImplTest {
	
	@Mock
	private PSD2Validator psd2Validator;

	@Mock
	private CommonAccountValidations commonAccountValidations;

	@Mock
	private RequestHeaderAttributes reqHeaderAtrributes;
	
	@InjectMocks
	private AccountProductsValidatorImpl accountProductValidatorImpl = new AccountProductsValidatorImpl();

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		Map<String, String> map=new HashMap<>();
		map.put("SIGNATURE", "Something wrong with x-jws-signature request header");
		Map<String, String> specificErrorMessageMap=new HashMap<>();
		specificErrorMessageMap.put("signature_invalid_content", "Invalid content prsent in joseheader of x-jws-signature header");
		specificErrorMessageMap.put("signature_missing", "signature header missing in request");
		OBPSD2ExceptionUtility.genericErrorMessages.putAll(map);
		OBPSD2ExceptionUtility.specificErrorMessages.putAll(specificErrorMessageMap);
	
		 Field reqValidationEnabled = null;
			try {
				reqValidationEnabled = AccountProductsValidatorImpl.class.getDeclaredField("resValidationEnabled");
				reqValidationEnabled.setAccessible(true);
				reqValidationEnabled.set(accountProductValidatorImpl, true);
			} catch (NoSuchFieldException | SecurityException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
		
	}

	@Test
	public void validateRequestParamsTest(){
		OBReadProduct2 obReadProduct2=new OBReadProduct2();
				
		Assert.assertNull(accountProductValidatorImpl.validateRequestParams(obReadProduct2));
	}
	@Test
	public void validateResponseParamsTestWithFalse(){
		
		OBReadProduct2 obReadProduct2=new OBReadProduct2();
		OBReadProduct2Data data=new OBReadProduct2Data();
		List<OBReadProduct2DataProduct> productList=new ArrayList<>();
		OBReadProduct2DataProduct product=new OBReadProduct2DataProduct();
		OBBCAData1 obBCAData1=new OBBCAData1();
		product.setBCA(obBCAData1);
		product.setProductType(ProductTypeEnum.BUSINESSCURRENTACCOUNT);
		productList.add(product);
		data.setProduct(productList);
		obReadProduct2.setData(data);
		
		 Field reqValidationEnabled = null;
			try {
				reqValidationEnabled = AccountProductsValidatorImpl.class.getDeclaredField("resValidationEnabled");
				reqValidationEnabled.setAccessible(true);
				reqValidationEnabled.set(accountProductValidatorImpl, false);
			} catch (NoSuchFieldException | SecurityException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
			
			assertTrue(accountProductValidatorImpl.validateResponseParams(obReadProduct2));
	}
	
	@Test
	public void validateResponseParamsTestWithEmptyProduct(){
		OBReadProduct2 obReadProduct2=new OBReadProduct2();
		OBReadProduct2Data data=new OBReadProduct2Data();
		List<OBReadProduct2DataProduct> productList=new ArrayList<>();
		productList.add(null);
		data.setProduct(productList);
		obReadProduct2.setData(data);
		
		assertTrue(accountProductValidatorImpl.validateResponseParams(obReadProduct2));
	}
	@Test(expected=PSD2Exception.class)
	public void testValidateUniqueIdNull(){
		accountProductValidatorImpl.validateUniqueId(null);
	}
	
	@Test(expected=PSD2Exception.class)
	public void testValidateUniqueIdEmpty(){
		accountProductValidatorImpl.validateUniqueId("");
	}
	
	@Test
	public void testValidateUniqueIdNotNull()
	{
		Mockito.doNothing().when(commonAccountValidations).validateUniqueUUID("abc");
		accountProductValidatorImpl.validateUniqueId("c154ad51-4cd7-400f-9d84-532347f87360");
	}
	
	@Test(expected=PSD2Exception.class)
	public void testCustomValidations() {
		OBReadProduct2 obReadProduct2=new OBReadProduct2();
		OBReadProduct2Data data=new OBReadProduct2Data();
		List<OBReadProduct2DataProduct> productList=new ArrayList<>();
		OBReadProduct2DataProduct product=new OBReadProduct2DataProduct();
		OBBCAData1 obBCAData1=new OBBCAData1();
		product.setBCA(obBCAData1);
		product.setProductType(ProductTypeEnum.COMMERCIALCREDITCARD);
		productList.add(product);
		data.setProduct(productList);
		obReadProduct2.setData(data);
		
		assertTrue(accountProductValidatorImpl.validateResponseParams(obReadProduct2));
	}
	
	@Test(expected=PSD2Exception.class)
	public void testCustomValidations1() {
		OBReadProduct2 obReadProduct2=new OBReadProduct2();
		OBReadProduct2Data data=new OBReadProduct2Data();
		List<OBReadProduct2DataProduct> productList=new ArrayList<>();
		OBReadProduct2DataProduct product=new OBReadProduct2DataProduct();
		OBPCAData1 obBCAData1=new OBPCAData1();
		product.setPCA(obBCAData1);
		product.setProductType(ProductTypeEnum.BUSINESSCURRENTACCOUNT);
		productList.add(product);
		data.setProduct(productList);
		obReadProduct2.setData(data);
		
		assertTrue(accountProductValidatorImpl.validateResponseParams(obReadProduct2));
	}
	
	@Test(expected=PSD2Exception.class)
	public void testCustomValidations2() {
		OBReadProduct2 obReadProduct2=new OBReadProduct2();
		OBReadProduct2Data data=new OBReadProduct2Data();
		List<OBReadProduct2DataProduct> productList=new ArrayList<>();
		OBReadProduct2DataProduct product=new OBReadProduct2DataProduct();
		OBReadProduct2DataOtherProductType otherProductType=new OBReadProduct2DataOtherProductType();
		product.setOtherProductType(otherProductType);
		product.setProductType(ProductTypeEnum.COMMERCIALCREDITCARD);
		productList.add(product);
		data.setProduct(productList);
		obReadProduct2.setData(data);	
		
		assertTrue(accountProductValidatorImpl.validateResponseParams(obReadProduct2));
	}
	
	@Test(expected=PSD2Exception.class)
	public void testCustomValidations3() {
		OBReadProduct2 obReadProduct2=new OBReadProduct2();
		OBReadProduct2Data data=new OBReadProduct2Data();
		List<OBReadProduct2DataProduct> productList=new ArrayList<>();
		OBReadProduct2DataProduct product=new OBReadProduct2DataProduct();
		product.setOtherProductType(null);
		product.setProductType(ProductTypeEnum.OTHER);
		productList.add(product);
		data.setProduct(productList);
		obReadProduct2.setData(data);	
		
		assertTrue(accountProductValidatorImpl.validateResponseParams(obReadProduct2));
	}
	

	public void testCustomValidationsTestWithNull(){
		Mockito.when(accountProductValidatorImpl.validateResponseParams(null)).then(null);
	}
}
