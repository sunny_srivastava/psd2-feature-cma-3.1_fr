package com.capgemini.psd2.aisp.validation.test.adapter.impl;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.aisp.domain.Links;
import com.capgemini.psd2.aisp.domain.Meta;
import com.capgemini.psd2.aisp.domain.OBReadBalance1;
import com.capgemini.psd2.aisp.domain.OBReadBalance1Data;
import com.capgemini.psd2.aisp.domain.OBReadBalance1DataAmount;
import com.capgemini.psd2.aisp.domain.OBReadBalance1DataAmount1;
import com.capgemini.psd2.aisp.domain.OBReadBalance1DataBalance;
import com.capgemini.psd2.aisp.domain.OBReadBalance1DataCreditLine;
import com.capgemini.psd2.aisp.validation.adapter.impl.AccountBalanceValidatorImpl;
import com.capgemini.psd2.aisp.validation.adapter.utilities.CommonAccountValidations;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.validator.PSD2Validator;

@RunWith(SpringJUnit4ClassRunner.class)
public class AccountBalanceValidatorTest {
	
	@InjectMocks
	AccountBalanceValidatorImpl accountBalanceValidatorImpl;
	@Mock
	private CommonAccountValidations commonAccountValidations;
	@Mock
	private PSD2Validator psd2Validator;
	
	public  OBReadBalance1 getAccountBalanceGETResponse() 
	{
		OBReadBalance1 balance1 = new OBReadBalance1();
		OBReadBalance1Data data = new OBReadBalance1Data();
		List<OBReadBalance1DataBalance> balance = new ArrayList<>();
		OBReadBalance1DataBalance p1 = new OBReadBalance1DataBalance();
		p1.setAccountId("269");
		p1.setDateTime("2017-04-05T10:43:07+00:00");

		
		OBReadBalance1DataAmount1 chargeAmount= new OBReadBalance1DataAmount1();
		chargeAmount.setAmount("asfd");
		chargeAmount.setCurrency("asdf");
		OBReadBalance1DataCreditLine creditLine1= new OBReadBalance1DataCreditLine();
		creditLine1.setIncluded(true);
		creditLine1.setAmount(chargeAmount);
		List<OBReadBalance1DataCreditLine> creditLine= new ArrayList<>();
		creditLine.add(creditLine1);
		
		
		p1.setCreditLine(creditLine);
		OBReadBalance1DataAmount oBCashBalance1Amount = new OBReadBalance1DataAmount();
		oBCashBalance1Amount.setAmount("1000.0");
		oBCashBalance1Amount.setCurrency("USD");
		p1.amount(oBCashBalance1Amount);
		balance.add(p1);
		data.setBalance(balance);
		balance1.setData(data);
		Links links = new Links();
		Meta meta = new Meta();
		meta.setTotalPages(1);
		balance1.setLinks(links);
		balance1.setMeta(meta);
		return balance1;
	}
	public  OBReadBalance1 getAccountBalanceSetIncludeNullGETResponse() 
	{
		OBReadBalance1 balance1 = new OBReadBalance1();
		OBReadBalance1Data data = new OBReadBalance1Data();
		List<OBReadBalance1DataBalance> balance = new ArrayList<>();
		OBReadBalance1DataBalance p1 = new OBReadBalance1DataBalance();
		p1.setAccountId("269");
		p1.setDateTime("2017-04-05T10:43:07+00:00");

		
		OBReadBalance1DataAmount1 chargeAmount= new OBReadBalance1DataAmount1();
		chargeAmount.setAmount("asfd");
		chargeAmount.setCurrency("asdf");
		OBReadBalance1DataCreditLine creditLine1= new OBReadBalance1DataCreditLine();
		creditLine1.setIncluded(null);
		creditLine1.setAmount(chargeAmount);
		List<OBReadBalance1DataCreditLine> creditLine= new ArrayList<>();
		creditLine.add(creditLine1);
		
		
		p1.setCreditLine(creditLine);
		OBReadBalance1DataAmount oBCashBalance1Amount = new OBReadBalance1DataAmount();
		oBCashBalance1Amount.setAmount("1000.0");
		oBCashBalance1Amount.setCurrency("USD");
		p1.amount(oBCashBalance1Amount);
		balance.add(p1);
		data.setBalance(balance);
		balance1.setData(data);
		Links links = new Links();
		Meta meta = new Meta();
		meta.setTotalPages(1);
		balance1.setLinks(links);
		balance1.setMeta(meta);
		return balance1;
	}
	public  OBReadBalance1 getAccountBalanceGetCreditLineNullGETResponse() 
	{
		OBReadBalance1 balance1 = new OBReadBalance1();
		OBReadBalance1Data data = new OBReadBalance1Data();
		List<OBReadBalance1DataBalance> balance = new ArrayList<>();
		OBReadBalance1DataBalance p1 = new OBReadBalance1DataBalance();
		p1.setAccountId("269");
		p1.setDateTime("2017-04-05T10:43:07+00:00");

		
		OBReadBalance1DataAmount1 chargeAmount= new OBReadBalance1DataAmount1();
		chargeAmount.setAmount("asfd");
		chargeAmount.setCurrency("asdf");
		OBReadBalance1DataCreditLine creditLine1= new OBReadBalance1DataCreditLine();
		creditLine1.setIncluded(true);
		creditLine1.setAmount(null);
		List<OBReadBalance1DataCreditLine> creditLine= new ArrayList<>();
		creditLine.add(creditLine1);
		
		
		p1.setCreditLine(creditLine);
		OBReadBalance1DataAmount oBCashBalance1Amount = new OBReadBalance1DataAmount();
		oBCashBalance1Amount.setAmount("1000.0");
		oBCashBalance1Amount.setCurrency("USD");
		p1.amount(oBCashBalance1Amount);
		balance.add(p1);
		data.setBalance(balance);
		balance1.setData(data);
		Links links = new Links();
		Meta meta = new Meta();
		meta.setTotalPages(1);
		balance1.setLinks(links);
		balance1.setMeta(meta);
		return balance1;
	}
	
	@Test
	public void validateResponseParamsTest() {
		Field reqValidationEnabled = null;
			try {
				reqValidationEnabled = AccountBalanceValidatorImpl.class.getDeclaredField("resValidationEnabled");
				reqValidationEnabled.setAccessible(true);
				reqValidationEnabled.set(accountBalanceValidatorImpl, true);
			} catch (NoSuchFieldException | SecurityException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
			assertTrue(accountBalanceValidatorImpl.validateResponseParams(getAccountBalanceGETResponse()));
	}
	
	@Test
	public void validateResponseParamsTestNullParam() {
		Field reqValidationEnabled = null;
			try {
				reqValidationEnabled = AccountBalanceValidatorImpl.class.getDeclaredField("resValidationEnabled");
				reqValidationEnabled.setAccessible(true);
				reqValidationEnabled.set(accountBalanceValidatorImpl, true);
			} catch (NoSuchFieldException | SecurityException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
			assertTrue(accountBalanceValidatorImpl.validateResponseParams(null));
	}
	
	@Test
	public void validateResponseParamsResValidationEnabledFalseTest() {
		Field reqValidationEnabled = null;
			try {
				reqValidationEnabled = AccountBalanceValidatorImpl.class.getDeclaredField("resValidationEnabled");
				reqValidationEnabled.setAccessible(true);
				reqValidationEnabled.set(accountBalanceValidatorImpl, false);
			} catch (NoSuchFieldException | SecurityException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
			assertTrue(accountBalanceValidatorImpl.validateResponseParams(getAccountBalanceGETResponse()));
	}
	@Test(expected = PSD2Exception.class)
	public void validateResponseParamsResValidationEnabledTestIncludeNull() {
		Field reqValidationEnabled = null;
			try {
				reqValidationEnabled = AccountBalanceValidatorImpl.class.getDeclaredField("resValidationEnabled");
				reqValidationEnabled.setAccessible(true);
				reqValidationEnabled.set(accountBalanceValidatorImpl, false);
			} catch (NoSuchFieldException | SecurityException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
			accountBalanceValidatorImpl.validateResponseParams(getAccountBalanceSetIncludeNullGETResponse());
	}
	@Test
	public void validateResponseParamsResValidationCreditLineAmountNull() {
		Field reqValidationEnabled = null;
			try {
				reqValidationEnabled = AccountBalanceValidatorImpl.class.getDeclaredField("resValidationEnabled");
				reqValidationEnabled.setAccessible(true);
				reqValidationEnabled.set(accountBalanceValidatorImpl, false);
			} catch (NoSuchFieldException | SecurityException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
			accountBalanceValidatorImpl.validateResponseParams(getAccountBalanceGetCreditLineNullGETResponse());
	}
	
	@Test(expected=PSD2Exception.class)
	public void validateUniqueIdTestForNullParam()
	{
		accountBalanceValidatorImpl.validateUniqueId(null);
	}
	@Test
	public void validateUniqueIdTest()
	{
		Mockito.doNothing().when(commonAccountValidations).validateUniqueUUID("asd");  
		accountBalanceValidatorImpl.validateUniqueId("asdf");
	}
	@Test
	public void validateRequestParamsTest()
	{
		assertNull(accountBalanceValidatorImpl.validateRequestParams(null));
	}
	
	
}
