package com.capgemini.psd2.aisp.validation.adapter.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.aisp.domain.OBAccount6;
import com.capgemini.psd2.aisp.domain.OBAccount6Account;
import com.capgemini.psd2.aisp.domain.OBReadAccount6;
import com.capgemini.psd2.aisp.validation.adapter.AISPCustomValidator;
import com.capgemini.psd2.aisp.validation.adapter.utilities.CommonAccountValidations;
import com.capgemini.psd2.exceptions.ErrorMapKeys;
import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.utilities.NullCheckUtils;
import com.capgemini.psd2.validator.PSD2Validator;

@Component("accountInformationValidator")
@ConfigurationProperties("app")
public class AccountInformationValidatorImpl implements AISPCustomValidator<OBReadAccount6, OBReadAccount6> {

	@Autowired
	private PSD2Validator psd2Validator;

	@Value("${app.swaggerValidations.response:#{true}}")
	private Boolean resValidationEnabled;

	@Autowired
	private CommonAccountValidations commonAccountValidations;

	@Override
	public OBReadAccount6 validateRequestParams(OBReadAccount6 t) {
		return null;
	}

	@Override
	public boolean validateResponseParams(OBReadAccount6 obReadAccount) {
		if (resValidationEnabled)
			executeAccountResponseSwaggerValidations(obReadAccount);
		executeAccountResponseCustomValidations(obReadAccount);
		return true;
	}

	@Override
	public void validateUniqueId(String consentId) {
		if (NullCheckUtils.isNullOrEmpty(consentId)) {
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_FIELD_INVALID,
					ErrorMapKeys.VALIDATION_ERROR));
		}
		commonAccountValidations.validateUniqueUUID(consentId);
	}

	private void executeAccountResponseSwaggerValidations(OBReadAccount6 obReadAccount) {
		psd2Validator.validate(obReadAccount);
		
	}
	
	private void executeAccountResponseCustomValidations(OBReadAccount6 obReadAccount) {

		if (obReadAccount != null && obReadAccount.getData() != null
				&& obReadAccount.getData().getAccount() != null) {

			for (OBAccount6 obAccount : obReadAccount.getData().getAccount()) {
				if(!NullCheckUtils.isNullOrEmpty(obAccount)) {		
					commonAccountValidations.isValidCurrency(obAccount.getCurrency());
					if (!NullCheckUtils.isNullOrEmpty(obAccount.getAccount())) {
						for (OBAccount6Account obCashAccount3 : obAccount.getAccount()) {
							commonAccountValidations.validateSchemeNameWithIdentification(obCashAccount3.getSchemeName(),
									obCashAccount3.getIdentification());
						
							if (!NullCheckUtils.isNullOrEmpty(obCashAccount3.getSecondaryIdentification()) && (!NullCheckUtils.isNullOrEmpty(obCashAccount3.getSchemeName()))) {
								commonAccountValidations.validateSchemeNameWithSecondaryIdentification(
									obCashAccount3.getSchemeName(), obCashAccount3.getSecondaryIdentification());
							}
						}
					}
				}
			}

		}
	}
	
}