package com.capgemini.psd2.aisp.validation.adapter.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.aisp.domain.OBReadStatement2;
import com.capgemini.psd2.aisp.domain.OBStatement2;
import com.capgemini.psd2.aisp.domain.OBStatement2StatementAmount;
import com.capgemini.psd2.aisp.domain.OBStatement2StatementBenefit;
import com.capgemini.psd2.aisp.domain.OBStatement2StatementDateTime;
import com.capgemini.psd2.aisp.domain.OBStatement2StatementFee;
import com.capgemini.psd2.aisp.domain.OBStatement2StatementInterest;
import com.capgemini.psd2.aisp.validation.adapter.AISPCustomValidator;
import com.capgemini.psd2.aisp.validation.adapter.utilities.CommonAccountValidations;
import com.capgemini.psd2.exceptions.ErrorMapKeys;
import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.utilities.NullCheckUtils;
import com.capgemini.psd2.validator.PSD2Validator;

@Component("accountStatementsValidator")
@ConfigurationProperties("app")
public class AccountStatementsValidatorImpl implements AISPCustomValidator<OBReadStatement2, OBReadStatement2> {

	@Autowired
	private PSD2Validator psd2Validator;

	@Value("${app.swaggerValidations.response:#{true}}")
	private Boolean resValidationEnabled;

	@Autowired
	private CommonAccountValidations commonAccountValidations;

	@Override
	public OBReadStatement2 validateRequestParams(OBReadStatement2 t) {
		return null;
	}

	@Override
	public boolean validateResponseParams(OBReadStatement2 obReadStatement) {
		if (resValidationEnabled)
			executeAccountResponseSwaggerValidations(obReadStatement);
		executeAccountResponseCustomValidations(obReadStatement);
		return true;
	}

	@Override
	public void validateUniqueId(String consentId) {
		if (NullCheckUtils.isNullOrEmpty(consentId)) {
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_FIELD_INVALID,
					ErrorMapKeys.VALIDATION_ERROR));
		}
		commonAccountValidations.validateUniqueUUID(consentId);
	}
	
	private void executeAccountResponseSwaggerValidations(OBReadStatement2 obReadStatement) {
		psd2Validator.validate(obReadStatement);
		
	}

	private void executeAccountResponseCustomValidations(OBReadStatement2 obReadStatement) {
	
		if(obReadStatement != null && obReadStatement.getData() != null &&  obReadStatement.getData().getStatement()!=null){

			for (OBStatement2 oBStatement : obReadStatement.getData().getStatement()) {
				if(!NullCheckUtils.isNullOrEmpty(oBStatement)) {	
					commonAccountValidations.validateAndParseDateTimeFormatForResponse(oBStatement.getStartDateTime());
					commonAccountValidations.validateAndParseDateTimeFormatForResponse(oBStatement.getEndDateTime());
					commonAccountValidations.validateAndParseDateTimeFormatForResponse(oBStatement.getCreationDateTime());
	
					if (!NullCheckUtils.isNullOrEmpty(oBStatement.getStatementDateTime())) {
						for (OBStatement2StatementDateTime obStatementDateTime : oBStatement.getStatementDateTime()) {
							commonAccountValidations.validateAndParseDateTimeFormatForResponse(obStatementDateTime.getDateTime());
						}
					}
	
					if (!NullCheckUtils.isNullOrEmpty(oBStatement.getStatementBenefit())) {
						for (OBStatement2StatementBenefit oBStatementBenefit : oBStatement.getStatementBenefit()) {
							if (!NullCheckUtils.isNullOrEmpty(oBStatementBenefit.getAmount())) {
								commonAccountValidations.validateAmount(oBStatementBenefit.getAmount().getAmount());
								commonAccountValidations.isValidCurrency(oBStatementBenefit.getAmount().getCurrency());
							}
	
						}
	
					}
	
					if (!NullCheckUtils.isNullOrEmpty(oBStatement.getStatementInterest())) {
						for (OBStatement2StatementInterest OBStatementInterest : oBStatement.getStatementInterest()) {
							if (!NullCheckUtils.isNullOrEmpty(OBStatementInterest.getAmount())) {
								commonAccountValidations.validateAmount(OBStatementInterest.getAmount().getAmount());
								commonAccountValidations.isValidCurrency(OBStatementInterest.getAmount().getCurrency());
							}
	
						}
					}
	
					if (!NullCheckUtils.isNullOrEmpty(oBStatement.getStatementFee())) {
						for (OBStatement2StatementFee obtatementFee1 : oBStatement.getStatementFee()) {
							if (!NullCheckUtils.isNullOrEmpty(obtatementFee1.getAmount())) {
								commonAccountValidations.validateAmount(obtatementFee1.getAmount().getAmount());
								commonAccountValidations.isValidCurrency(obtatementFee1.getAmount().getCurrency());
							}
	
						}
	
					}
	
					if (!NullCheckUtils.isNullOrEmpty(oBStatement.getStatementAmount())) {
						for (OBStatement2StatementAmount obStatementAmount : oBStatement.getStatementAmount()) {
							if (!NullCheckUtils.isNullOrEmpty(obStatementAmount.getAmount())) {
								commonAccountValidations.validateAmount(obStatementAmount.getAmount().getAmount());
								commonAccountValidations.isValidCurrency(obStatementAmount.getAmount().getCurrency());
							}
	
						}
	
					}
				}

			}

			if (!NullCheckUtils.isNullOrEmpty(obReadStatement.getMeta())
					&& !NullCheckUtils.isNullOrEmpty(obReadStatement.getMeta().getFirstAvailableDateTime())) {
				commonAccountValidations.validateAndParseDateTimeFormatForResponse(
						obReadStatement.getMeta().getFirstAvailableDateTime());
			}

			if (!NullCheckUtils.isNullOrEmpty(obReadStatement.getMeta())
					&& !NullCheckUtils.isNullOrEmpty(obReadStatement.getMeta().getLastAvailableDateTime())) {
				commonAccountValidations.validateAndParseDateTimeFormatForResponse(
						obReadStatement.getMeta().getLastAvailableDateTime());
			}

		}

	}

}