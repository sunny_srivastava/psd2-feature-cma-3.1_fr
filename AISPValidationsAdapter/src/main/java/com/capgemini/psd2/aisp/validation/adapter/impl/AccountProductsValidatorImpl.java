package com.capgemini.psd2.aisp.validation.adapter.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.aisp.domain.OBReadProduct2;
import com.capgemini.psd2.aisp.domain.OBReadProduct2DataProduct;
import com.capgemini.psd2.aisp.domain.OBReadProduct2DataProduct.ProductTypeEnum;
import com.capgemini.psd2.aisp.validation.adapter.AISPCustomValidator;
import com.capgemini.psd2.aisp.validation.adapter.utilities.CommonAccountValidations;
import com.capgemini.psd2.exceptions.ErrorMapKeys;
import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.InternalServerErrorMessage;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.utilities.NullCheckUtils;
import com.capgemini.psd2.validator.PSD2Validator;

@Component("accountProductsValidator")
@ConfigurationProperties("app")
public class AccountProductsValidatorImpl implements AISPCustomValidator<OBReadProduct2, OBReadProduct2> {

	@Autowired
	private PSD2Validator psd2Validator;

	@Value("${app.swaggerValidations.response:#{true}}")
	private Boolean resValidationEnabled;

	@Autowired
	private CommonAccountValidations commonAccountValidations;

	@Override
	public OBReadProduct2 validateRequestParams(OBReadProduct2 t) {
		return null;
	}

	@Override
	public boolean validateResponseParams(OBReadProduct2 v) {
		if (resValidationEnabled)
			executeAccountResponseSwaggerValidations(v);
		executeAccountResponseCustomValidations(v);
		return true;
	}

	@Override
	public void validateUniqueId(String consentId) {
		if (NullCheckUtils.isNullOrEmpty(consentId)) {
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_FIELD_INVALID,
					ErrorMapKeys.VALIDATION_ERROR));
		}
		commonAccountValidations.validateUniqueUUID(consentId);
	}


	private void executeAccountResponseCustomValidations(OBReadProduct2 v) {

		OBReadProduct2 input = v;
		List<OBReadProduct2DataProduct> productList=input.getData().getProduct();
		if((productList.isEmpty()))
			return ;
		OBReadProduct2DataProduct product = input.getData().getProduct().get(0);

		if(!NullCheckUtils.isNullOrEmpty(product) && null != product.getProductType()) {	
			if ((product.getBCA() != null)
					&& product.getProductType() != ProductTypeEnum.BUSINESSCURRENTACCOUNT) {
				throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
						InternalServerErrorMessage.INVALID_PRODUCT_TYPE));
			}
	
			if ((product.getPCA() != null)
					&& product.getProductType() != ProductTypeEnum.PERSONALCURRENTACCOUNT) {
				throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
						InternalServerErrorMessage.INVALID_PRODUCT_TYPE));
			}
	
			if(product.getProductType() != ProductTypeEnum.OTHER) {
				// ProductType != OTHER
				if ((product.getOtherProductType() != null)) {
					throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
							InternalServerErrorMessage.INVALID_PRODUCT_TYPE));
				}
			} else {
				// ProductType == OTHER 
				if (product.getOtherProductType() == null) {
					throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
							InternalServerErrorMessage.MISSING_OTHER_PRODUCT_TYPE));
				}
			}
		}
	}

	private void executeAccountResponseSwaggerValidations(OBReadProduct2 oBReadProduct) {
		psd2Validator.validate(oBReadProduct);
		
	}
}