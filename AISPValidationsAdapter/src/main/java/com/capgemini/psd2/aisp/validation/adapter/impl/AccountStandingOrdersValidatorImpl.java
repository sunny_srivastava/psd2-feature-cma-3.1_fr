package com.capgemini.psd2.aisp.validation.adapter.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.aisp.domain.OBReadConsent1Data;
import com.capgemini.psd2.aisp.domain.OBReadStandingOrder6;
import com.capgemini.psd2.aisp.domain.OBStandingOrder6;
import com.capgemini.psd2.aisp.validation.adapter.AISPCustomValidator;
import com.capgemini.psd2.aisp.validation.adapter.utilities.CommonAccountValidations;
import com.capgemini.psd2.exceptions.ErrorMapKeys;
import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.InternalServerErrorMessage;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.utilities.NullCheckUtils;
import com.capgemini.psd2.validator.PSD2Validator;

@Component("accountStandingOrdersValidator")
@ConfigurationProperties("app")
public class AccountStandingOrdersValidatorImpl
		implements AISPCustomValidator<OBReadStandingOrder6, OBReadStandingOrder6> {

	@Autowired
	private PSD2Validator psd2Validator;

	@Value("${app.swaggerValidations.response:#{true}}")
	private Boolean resValidationEnabled;

	@Autowired
	private CommonAccountValidations commonAccountValidations;

	@Autowired
	private RequestHeaderAttributes reqHeaderAtrributes;

	@Override
	public OBReadStandingOrder6 validateRequestParams(OBReadStandingOrder6 oBReadStandingOrder) {
		return oBReadStandingOrder;
	}

	@Override
	public boolean validateResponseParams(OBReadStandingOrder6 oBReadStandingOrder) {
		if (resValidationEnabled)
			executeAccountScheduledPaymentSwaggerValidations(oBReadStandingOrder);
		executeAccountResponseCustomValidations(oBReadStandingOrder);
		return true;
	}

	// swagger validation for AccountStandingOrder
	private void executeAccountScheduledPaymentSwaggerValidations(OBReadStandingOrder6 oBReadStandingOrder) {
		psd2Validator.validate(oBReadStandingOrder);
	}

	// custom validation for AccountStandingOrder
	private void executeAccountResponseCustomValidations(OBReadStandingOrder6 oBReadStandingOrder) {
		if((!NullCheckUtils.isNullOrEmpty(oBReadStandingOrder)) && (!NullCheckUtils.isNullOrEmpty(oBReadStandingOrder.getData()))
				&& (!NullCheckUtils.isNullOrEmpty(oBReadStandingOrder.getData().getStandingOrder()))){
			//Validate Permission
			validatePermissionBasedResponse(oBReadStandingOrder.getData().getStandingOrder());
			
			for (OBStandingOrder6 oBStandingOrder : oBReadStandingOrder.getData().getStandingOrder()) {
				if(!NullCheckUtils.isNullOrEmpty(oBStandingOrder)) {	
				// validate FirstPaymentDateTime
				if (!NullCheckUtils.isNullOrEmpty(oBStandingOrder.getFirstPaymentDateTime()))
					commonAccountValidations
							.validateAndParseDateTimeFormatForResponse(oBStandingOrder.getFirstPaymentDateTime());

				// validate NextPaymentDateTime
				if (!NullCheckUtils.isNullOrEmpty(oBStandingOrder.getNextPaymentDateTime()))
					commonAccountValidations
							.validateAndParseDateTimeFormatForResponse(oBStandingOrder.getNextPaymentDateTime());

				// validate FinalPaymentDateTime
				if (!NullCheckUtils.isNullOrEmpty(oBStandingOrder.getFinalPaymentDateTime()))
					commonAccountValidations
							.validateAndParseDateTimeFormatForResponse(oBStandingOrder.getFinalPaymentDateTime());
				
				//validate Creditor Agent
				if(!NullCheckUtils.isNullOrEmpty(oBStandingOrder.getCreditorAgent()))
					commonAccountValidations.validateCreditorAgent(oBStandingOrder.getCreditorAgent());

				// check CreditorAccount is null or empty
				if (!NullCheckUtils.isNullOrEmpty(oBStandingOrder.getCreditorAccount())) {
					// validate SchemeName with Identification for CreditorAccount
					if ((!NullCheckUtils.isNullOrEmpty(oBStandingOrder.getCreditorAccount().getSchemeName()))
							&& !NullCheckUtils.isNullOrEmpty(oBStandingOrder.getCreditorAccount().getIdentification()))
						commonAccountValidations.validateSchemeNameWithIdentification(
								oBStandingOrder.getCreditorAccount().getSchemeName(),
								oBStandingOrder.getCreditorAccount().getIdentification());

					// validate SchemeName with SecondryIdentification for CreditorAccount
					if ((!NullCheckUtils.isNullOrEmpty(oBStandingOrder.getCreditorAccount().getSchemeName()))
							&& !NullCheckUtils.isNullOrEmpty(oBStandingOrder.getCreditorAccount().getSecondaryIdentification()))
						commonAccountValidations.validateSchemeNameWithSecondaryIdentification(
								oBStandingOrder.getCreditorAccount().getSchemeName(),
								oBStandingOrder.getCreditorAccount().getSecondaryIdentification());
				}

				// Validate FirstPaymentAmount
				validateFirstPaymentAmount(oBStandingOrder);

				// Validate NextPaymentAmount
				validateNextPaymentAmount(oBStandingOrder);

				// Validate FinalPaymentAmount
				validateFinalPaymentAmount(oBStandingOrder);
			}
			}
		}
		
	}

	// Validate Amount & Currency for FirstPaymentAmount
	private void validateFirstPaymentAmount(OBStandingOrder6 oBStandingOrder) {
		if(!NullCheckUtils.isNullOrEmpty(oBStandingOrder)) {
			if (!NullCheckUtils.isNullOrEmpty(oBStandingOrder.getFirstPaymentAmount())) {
				// validate amount
				if (!NullCheckUtils.isNullOrEmpty(oBStandingOrder.getFirstPaymentAmount().getAmount()))
					commonAccountValidations.validateAmount(oBStandingOrder.getFirstPaymentAmount().getAmount());
				// validate currency
				if (!NullCheckUtils.isNullOrEmpty(oBStandingOrder.getFirstPaymentAmount().getCurrency()))
					commonAccountValidations.isValidCurrency(oBStandingOrder.getFirstPaymentAmount().getCurrency());
			}
		}
	}

	// Validate Amount & Currency for NextPaymentAmount
	private void validateNextPaymentAmount(OBStandingOrder6 oBStandingOrder) {
		if (!NullCheckUtils.isNullOrEmpty(oBStandingOrder.getNextPaymentAmount())) {
			// validate amount
			if (!NullCheckUtils.isNullOrEmpty(oBStandingOrder.getNextPaymentAmount().getAmount()))
				commonAccountValidations.validateAmount(oBStandingOrder.getNextPaymentAmount().getAmount());
			// validate currency
			if (!NullCheckUtils.isNullOrEmpty(oBStandingOrder.getNextPaymentAmount().getCurrency()))
				commonAccountValidations.isValidCurrency(oBStandingOrder.getNextPaymentAmount().getCurrency());
		}
	}

	// Validate Amount & Currency for FinalPaymentAmount
	private void validateFinalPaymentAmount(OBStandingOrder6 oBStandingOrder) {
		if (!NullCheckUtils.isNullOrEmpty(oBStandingOrder.getFinalPaymentAmount())) {
			// validate amount
			if (!NullCheckUtils.isNullOrEmpty(oBStandingOrder.getFinalPaymentAmount().getAmount()))
				commonAccountValidations.validateAmount(oBStandingOrder.getFinalPaymentAmount().getAmount());
			// validate currency
			if (!NullCheckUtils.isNullOrEmpty(oBStandingOrder.getFinalPaymentAmount().getCurrency()))
				commonAccountValidations.isValidCurrency(oBStandingOrder.getFinalPaymentAmount().getCurrency());
		}
	}
	
	private void validatePermissionBasedResponse(List<OBStandingOrder6> standingOrderList){
		if(!NullCheckUtils.isNullOrEmpty(reqHeaderAtrributes.getClaims())){
			if((reqHeaderAtrributes.getClaims().toString().toUpperCase()).contains(OBReadConsent1Data.PermissionsEnum.READSTANDINGORDERSDETAIL.toString().toUpperCase())){
				for(OBStandingOrder6 oBStandingOrder : standingOrderList){
					if(!NullCheckUtils.isNullOrEmpty(oBStandingOrder)){
						if(NullCheckUtils.isNullOrEmpty(oBStandingOrder.getCreditorAccount())){
							throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
								InternalServerErrorMessage.INSUFFICIENT_DATA_FOR_DETAIL_PERMISSION));
						}
						if(!NullCheckUtils.isNullOrEmpty(oBStandingOrder.getStandingOrderStatusCode())&&"ACTIVE".equalsIgnoreCase(oBStandingOrder.getStandingOrderStatusCode().toString())&&(NullCheckUtils.isNullOrEmpty(oBStandingOrder.getNextPaymentAmount())
							|| NullCheckUtils.isNullOrEmpty(oBStandingOrder.getNextPaymentDateTime()))) {
								throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
												InternalServerErrorMessage.INSUFFICIENT_DATA_FOR_DETAIL_PERMISSION));
						}
					}
				}
			}
		}
	}

	@Override	
	public void validateUniqueId(String consentId) {
		if (NullCheckUtils.isNullOrEmpty(consentId)) {
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_FIELD_INVALID,
					ErrorMapKeys.VALIDATION_ERROR));
		}
		commonAccountValidations.validateUniqueUUID(consentId);
	}
}