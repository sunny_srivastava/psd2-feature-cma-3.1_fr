package com.capgemini.psd2.aisp.validation.adapter.impl;

import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.aisp.domain.OBReadOffer1;
import com.capgemini.psd2.aisp.domain.OBReadOffer1DataAmount;
import com.capgemini.psd2.aisp.domain.OBReadOffer1DataFee;
import com.capgemini.psd2.aisp.domain.OBReadOffer1DataOffer;
import com.capgemini.psd2.aisp.validation.adapter.AISPCustomValidator;
import com.capgemini.psd2.aisp.validation.adapter.utilities.CommonAccountValidations;
import com.capgemini.psd2.exceptions.ErrorMapKeys;
import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.utilities.NullCheckUtils;
import com.capgemini.psd2.validator.PSD2Validator;

@Component("accountOffersValidator")
@ConfigurationProperties("app")
public class AccountOffersValidatorImpl implements AISPCustomValidator<OBReadOffer1, OBReadOffer1> {

	@Autowired
	private PSD2Validator psd2Validator;

	@Value("${app.swaggerValidations.response:#{true}}")
	private Boolean resValidationEnabled;

	@Autowired
	private CommonAccountValidations commonAccountValidations;

	@Override
	public OBReadOffer1 validateRequestParams(OBReadOffer1 t) {
		return null;
	}

	@Override
	public boolean validateResponseParams(OBReadOffer1 v) {
		if (resValidationEnabled)
			executeAccountResponseSwaggerValidations(v);
		executeAccountResponseCustomValidations(v);

		return true;
	}

	private void executeAccountResponseCustomValidations(OBReadOffer1 v) {

		Iterator<OBReadOffer1DataOffer> iterator;
		OBReadOffer1DataAmount offerAmount = null;
		OBReadOffer1DataFee offerFee = null;
		if (v != null && v.getData() != null){
		
			List<OBReadOffer1DataOffer> accountOffers = v.getData().getOffer();
			
			if(!NullCheckUtils.isNullOrEmpty(accountOffers)){
				iterator = accountOffers.iterator();
				
				while(iterator.hasNext()){
					OBReadOffer1DataOffer accountOffer = iterator.next();

					if(!NullCheckUtils.isNullOrEmpty(accountOffer)){
							if(!NullCheckUtils.isNullOrEmpty(accountOffer.getAmount())) {
								offerAmount = accountOffer.getAmount();
							}
							
							if(!NullCheckUtils.isNullOrEmpty(accountOffer.getFee())) {
								offerFee = accountOffer.getFee();
							}
					
							if(!NullCheckUtils.isNullOrEmpty(accountOffer.getStartDateTime())) {
								commonAccountValidations.validateAndParseDateTimeFormatForResponse(accountOffer.getStartDateTime());
							}
							
							if(!NullCheckUtils.isNullOrEmpty(accountOffer.getEndDateTime())) {
								commonAccountValidations.validateAndParseDateTimeFormatForResponse(accountOffer.getEndDateTime());
							}
					
							if(!NullCheckUtils.isNullOrEmpty(offerAmount)){
								commonAccountValidations.validateAmount(offerAmount.getAmount());
								commonAccountValidations.isValidCurrency(offerAmount.getCurrency());
							}
					
							if(!NullCheckUtils.isNullOrEmpty(offerFee)){
								commonAccountValidations.validateAmount(offerFee.getAmount());
								commonAccountValidations.isValidCurrency(offerFee.getCurrency());
							}
					 }
				 }
			}
		
		}
				
	} 

	private void executeAccountResponseSwaggerValidations(OBReadOffer1 v) {
		psd2Validator.validate(v);
		
	}

	@Override
	public void validateUniqueId(String consentId) {
		if (NullCheckUtils.isNullOrEmpty(consentId)) {
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_FIELD_INVALID,
					ErrorMapKeys.VALIDATION_ERROR));
		}
		commonAccountValidations.validateUniqueUUID(consentId);
	}



}