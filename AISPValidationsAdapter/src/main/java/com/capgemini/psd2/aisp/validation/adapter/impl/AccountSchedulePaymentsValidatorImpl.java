package com.capgemini.psd2.aisp.validation.adapter.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.aisp.domain.OBReadConsent1Data;
import com.capgemini.psd2.aisp.domain.OBReadScheduledPayment3;
import com.capgemini.psd2.aisp.domain.OBScheduledPayment3;
import com.capgemini.psd2.aisp.validation.adapter.AISPCustomValidator;
import com.capgemini.psd2.aisp.validation.adapter.utilities.CommonAccountValidations;
import com.capgemini.psd2.exceptions.ErrorMapKeys;
import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.InternalServerErrorMessage;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.utilities.NullCheckUtils;
import com.capgemini.psd2.validator.PSD2Validator;

@Component("accountSchedulePaymentsValidator")
@ConfigurationProperties("app")
public class AccountSchedulePaymentsValidatorImpl
		implements AISPCustomValidator<OBReadScheduledPayment3, OBReadScheduledPayment3> {

	@Autowired
	private PSD2Validator psd2Validator;

	@Value("${app.swaggerValidations.response:#{true}}")
	private Boolean resValidationEnabled;

	@Autowired
	private RequestHeaderAttributes reqHeaderAtrributes;
	
	@Autowired
	private CommonAccountValidations commonAccountValidations;

	@Override
	public OBReadScheduledPayment3 validateRequestParams(OBReadScheduledPayment3 obReadScheduledPayment) {
		return obReadScheduledPayment;
	}

	@Override
	public boolean validateResponseParams(OBReadScheduledPayment3 oBReadScheduledPayment) {
		if (resValidationEnabled)
			executeAccountScheduledPaymentSwaggerValidations(oBReadScheduledPayment);
		executeAccountScheduledPaymentCustomValidations(oBReadScheduledPayment);
		return true;
	}
	
	//swagger validation for AccountScheduledPayment
	private void executeAccountScheduledPaymentSwaggerValidations(OBReadScheduledPayment3 oBReadScheduledPayment) {
		psd2Validator.validate(oBReadScheduledPayment);
	}
	
	//custom validation for AccountScheduledPayment
	private void executeAccountScheduledPaymentCustomValidations(OBReadScheduledPayment3 oBReadScheduledPayment) {
		if((!NullCheckUtils.isNullOrEmpty(oBReadScheduledPayment)) && (!NullCheckUtils.isNullOrEmpty(oBReadScheduledPayment.getData()))
				&& (!NullCheckUtils.isNullOrEmpty(oBReadScheduledPayment.getData().getScheduledPayment()))){
			//Validate Permission
			validatePermissionBasedResponse(oBReadScheduledPayment.getData().getScheduledPayment());
			
			for(OBScheduledPayment3 obScheduledPayment : oBReadScheduledPayment.getData().getScheduledPayment()){
				if(!NullCheckUtils.isNullOrEmpty(obScheduledPayment)) {
					//validate CeditorAgent
					if(!NullCheckUtils.isNullOrEmpty(obScheduledPayment.getCreditorAgent()))
						commonAccountValidations.validateCreditorAgent(obScheduledPayment.getCreditorAgent());
					
					//validate date & time
					if(!NullCheckUtils.isNullOrEmpty(obScheduledPayment.getScheduledPaymentDateTime()))
						commonAccountValidations.validateAndParseDateTimeFormatForResponse(obScheduledPayment.getScheduledPaymentDateTime());
					
					//Check InstrucctedAmount is null or empty
					if(!NullCheckUtils.isNullOrEmpty(obScheduledPayment.getInstructedAmount())){
						//validate amount
						if(!NullCheckUtils.isNullOrEmpty(obScheduledPayment.getInstructedAmount().getAmount()))
							commonAccountValidations.validateAmount(obScheduledPayment.getInstructedAmount().getAmount());
						
						//validate currency
						if(!NullCheckUtils.isNullOrEmpty(obScheduledPayment.getInstructedAmount().getCurrency()))
							commonAccountValidations.isValidCurrency(obScheduledPayment.getInstructedAmount().getCurrency());
					}
					
					//check CreditorAccount is null or empty
					if(!NullCheckUtils.isNullOrEmpty(obScheduledPayment.getCreditorAccount())){
						//validate SchemeName with Identification for CreditorAccount
						if((!NullCheckUtils.isNullOrEmpty(obScheduledPayment.getCreditorAccount().getSchemeName())) && 
								!NullCheckUtils.isNullOrEmpty(obScheduledPayment.getCreditorAccount().getIdentification()))
							commonAccountValidations.validateSchemeNameWithIdentification(obScheduledPayment.getCreditorAccount().getSchemeName(),
																					obScheduledPayment.getCreditorAccount().getIdentification());
						//validate SchemeName with SecondryIdentification for CreditorAccount
						if((!NullCheckUtils.isNullOrEmpty(obScheduledPayment.getCreditorAccount().getSchemeName())) &&
								!NullCheckUtils.isNullOrEmpty(obScheduledPayment.getCreditorAccount().getSecondaryIdentification()))
							commonAccountValidations.validateSchemeNameWithSecondaryIdentification(obScheduledPayment.getCreditorAccount().getSchemeName(),
																				obScheduledPayment.getCreditorAccount().getSecondaryIdentification());
					}
				}
			}
		}
	}
	
	private void validatePermissionBasedResponse(List<OBScheduledPayment3> oBscheduledPayment1List){
		if(!NullCheckUtils.isNullOrEmpty(reqHeaderAtrributes.getClaims())){
			if((reqHeaderAtrributes.getClaims().toString().toUpperCase()).contains(OBReadConsent1Data.PermissionsEnum.READSCHEDULEDPAYMENTSDETAIL.toString().toUpperCase())){
				for(OBScheduledPayment3 oBScheduledPayment1 : oBscheduledPayment1List){
					if(!NullCheckUtils.isNullOrEmpty(oBScheduledPayment1)){
						if(NullCheckUtils.isNullOrEmpty(oBScheduledPayment1.getCreditorAccount()))
							throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
								InternalServerErrorMessage.INSUFFICIENT_DATA_FOR_DETAIL_PERMISSION));
					}
				}
			 }
		}
	}
	
	@Override
	public void validateUniqueId(String consentId) {
		if (NullCheckUtils.isNullOrEmpty(consentId))
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_FIELD_INVALID,
					ErrorMapKeys.VALIDATION_ERROR));
		commonAccountValidations.validateUniqueUUID(consentId);
	}


}