package com.capgemini.psd2.aisp.validation.adapter.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.aisp.domain.OBReadTransaction6;
import com.capgemini.psd2.aisp.domain.OBTransaction6;
import com.capgemini.psd2.aisp.validation.adapter.AISPCustomValidator;
import com.capgemini.psd2.aisp.validation.adapter.utilities.CommonAccountValidations;
import com.capgemini.psd2.exceptions.ErrorMapKeys;
import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.utilities.NullCheckUtils;
import com.capgemini.psd2.validator.PSD2Validator;

@Component("accountTransactionsValidator")
@ConfigurationProperties("app")
public class AccountTransactionsValidatorImpl implements AISPCustomValidator<OBReadTransaction6, OBReadTransaction6> {

	@Autowired
	private PSD2Validator psd2Validator;

	@Value("${app.swaggerValidations.response:#{true}}")
	private Boolean resValidationEnabled;

	@Autowired
	private CommonAccountValidations commonAccountValidations;

	@Override
	public OBReadTransaction6 validateRequestParams(OBReadTransaction6 obReadTransaction) {
		return obReadTransaction;
	}

	@Override
	public boolean validateResponseParams(OBReadTransaction6 obReadTransaction) {
		if (resValidationEnabled)
			executeAccountTransactionSwaggerValidations(obReadTransaction);
		executeAccountTransactionCustomValidations(obReadTransaction);
		return true;
	}

	@Override
	public void validateUniqueId(String consentId) {
		if (NullCheckUtils.isNullOrEmpty(consentId)) {
			throw PSD2Exception.populatePSD2Exception(
					new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_FIELD_EXPECTED, ErrorMapKeys.NO_ACCOUNT_ID_FOUND));
		}
		commonAccountValidations.validateUniqueUUID(consentId);
	}

	private void executeAccountTransactionSwaggerValidations(OBReadTransaction6 obReadTransaction) {
		psd2Validator.validate(obReadTransaction);

	}

	private void executeAccountTransactionCustomValidations(OBReadTransaction6 obReadTransaction) {
		if (!NullCheckUtils.isNullOrEmpty(obReadTransaction)) {
			if (!NullCheckUtils.isNullOrEmpty(obReadTransaction.getData())) {
				for (OBTransaction6 obTransaction : obReadTransaction.getData().getTransaction()) {
					if (!NullCheckUtils.isNullOrEmpty(obTransaction)) {
						if (!NullCheckUtils.isNullOrEmpty(obTransaction.getBookingDateTime())) {
							commonAccountValidations
									.validateAndParseDateTimeFormatForResponse(obTransaction.getBookingDateTime());
						}
						if (!NullCheckUtils.isNullOrEmpty(obTransaction.getValueDateTime())) {
							commonAccountValidations
									.validateAndParseDateTimeFormatForResponse(obTransaction.getValueDateTime());
						}
						if (!NullCheckUtils.isNullOrEmpty(obTransaction.getValueDateTime())) {
							commonAccountValidations
									.validateAndParseDateTimeFormatForResponse(obTransaction.getValueDateTime());
						}
						if (!NullCheckUtils.isNullOrEmpty(obTransaction.getAmount())) {
							if (!NullCheckUtils.isNullOrEmpty(obTransaction.getAmount().getAmount())) {
								commonAccountValidations.validateAmount(obTransaction.getAmount().getAmount());
							}
							if (!NullCheckUtils.isNullOrEmpty(obTransaction.getAmount().getCurrency())) {
								commonAccountValidations.isValidCurrency(obTransaction.getAmount().getCurrency());
							}
						}
	
						if (!NullCheckUtils.isNullOrEmpty(obTransaction.getChargeAmount())) {
							if (!NullCheckUtils.isNullOrEmpty(obTransaction.getChargeAmount().getAmount())) {
								commonAccountValidations.validateAmount(obTransaction.getAmount().getAmount());
							}
							if (!NullCheckUtils.isNullOrEmpty(obTransaction.getChargeAmount().getCurrency())) {
								commonAccountValidations.isValidCurrency(obTransaction.getAmount().getCurrency());
							}
	
						}
						if (!NullCheckUtils.isNullOrEmpty(obTransaction.getCurrencyExchange())) {
							if (!NullCheckUtils.isNullOrEmpty(obTransaction.getCurrencyExchange().getInstructedAmount())) {
								if (!NullCheckUtils.isNullOrEmpty(
										obTransaction.getCurrencyExchange().getInstructedAmount().getAmount())) {
									commonAccountValidations.validateAmount(obTransaction.getAmount().getAmount());
								}
	
								if (!NullCheckUtils.isNullOrEmpty(
										obTransaction.getCurrencyExchange().getInstructedAmount().getCurrency())) {
									commonAccountValidations.isValidCurrency(obTransaction.getAmount().getCurrency());
								}
							}
	
							if (!NullCheckUtils.isNullOrEmpty(obTransaction.getCurrencyExchange().getQuotationDate())) {
								commonAccountValidations.validateAndParseDateTimeFormatForResponse(
										obTransaction.getCurrencyExchange().getQuotationDate());
							}
						}
						if (!NullCheckUtils.isNullOrEmpty(obTransaction.getBalance())) {
							if (!NullCheckUtils.isNullOrEmpty(obTransaction.getBalance().getAmount())) {
								if (!NullCheckUtils.isNullOrEmpty(obTransaction.getBalance().getAmount().getAmount())) {
									commonAccountValidations.validateAmount(obTransaction.getAmount().getAmount());
								}
								if (!NullCheckUtils.isNullOrEmpty(obTransaction.getBalance().getAmount().getCurrency())) {
									commonAccountValidations.isValidCurrency(obTransaction.getAmount().getCurrency());
								}
							}
	
						}
	
						if (!NullCheckUtils.isNullOrEmpty(obTransaction.getCreditorAgent())) {
							commonAccountValidations.validateCreditorAgent(obTransaction.getCreditorAgent());
	
						}
	
						if (!NullCheckUtils.isNullOrEmpty(obTransaction.getDebtorAgent())) {
							commonAccountValidations.validateDebtorAgent(obTransaction.getDebtorAgent());
	
						}
	
						if (!NullCheckUtils.isNullOrEmpty(obTransaction.getDebtorAccount())) {
							if (!NullCheckUtils.isNullOrEmpty(obTransaction.getDebtorAccount().getSchemeName())
									&& !NullCheckUtils
											.isNullOrEmpty(obTransaction.getDebtorAccount().getIdentification())) {
								commonAccountValidations.validateSchemeNameWithIdentification(
										obTransaction.getDebtorAccount().getSchemeName(),
										obTransaction.getDebtorAccount().getIdentification());
							}
							if (!NullCheckUtils.isNullOrEmpty(obTransaction.getDebtorAccount().getSchemeName())
									&& !NullCheckUtils
											.isNullOrEmpty(obTransaction.getDebtorAccount().getSecondaryIdentification())) {
								commonAccountValidations.validateSchemeNameWithSecondaryIdentification(
										obTransaction.getDebtorAccount().getSchemeName(),
										obTransaction.getDebtorAccount().getSecondaryIdentification());
							}
						}
	
						if (!NullCheckUtils.isNullOrEmpty(obTransaction.getCreditorAccount())) {
							if (!NullCheckUtils.isNullOrEmpty(obTransaction.getCreditorAccount().getSchemeName())
									&& !NullCheckUtils
											.isNullOrEmpty(obTransaction.getCreditorAccount().getIdentification())) {
								commonAccountValidations.validateSchemeNameWithIdentification(
										obTransaction.getCreditorAccount().getSchemeName(),
										obTransaction.getCreditorAccount().getIdentification());
							}
							if (!NullCheckUtils.isNullOrEmpty(obTransaction.getCreditorAccount().getSchemeName())
									&& !NullCheckUtils
											.isNullOrEmpty(obTransaction.getCreditorAccount().getSecondaryIdentification())) {
								commonAccountValidations.validateSchemeNameWithSecondaryIdentification(
										obTransaction.getCreditorAccount().getSchemeName(),
										obTransaction.getCreditorAccount().getSecondaryIdentification());
							}
						}
	
					}
				}
			}
			if (!NullCheckUtils.isNullOrEmpty(obReadTransaction.getMeta())
					&& !NullCheckUtils.isNullOrEmpty(obReadTransaction.getMeta().getFirstAvailableDateTime())) {
				commonAccountValidations.validateAndParseDateTimeFormatForResponse(
						obReadTransaction.getMeta().getFirstAvailableDateTime());
			}

			if (!NullCheckUtils.isNullOrEmpty(obReadTransaction.getMeta())
					&& !NullCheckUtils.isNullOrEmpty(obReadTransaction.getMeta().getLastAvailableDateTime())) {
				commonAccountValidations.validateAndParseDateTimeFormatForResponse(
						obReadTransaction.getMeta().getLastAvailableDateTime());
			}

		}
	}
}