package com.capgemini.psd2.aisp.validation.adapter.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.aisp.domain.OBReadBalance1;
import com.capgemini.psd2.aisp.domain.OBReadBalance1DataBalance;
import com.capgemini.psd2.aisp.domain.OBReadBalance1DataCreditLine;
import com.capgemini.psd2.aisp.validation.adapter.AISPCustomValidator;
import com.capgemini.psd2.aisp.validation.adapter.utilities.CommonAccountValidations;
import com.capgemini.psd2.exceptions.ErrorMapKeys;
import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.InternalServerErrorMessage;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.utilities.NullCheckUtils;
import com.capgemini.psd2.validator.PSD2Validator;

@Component("accountBalanceValidator")
@ConfigurationProperties("app")
public class AccountBalanceValidatorImpl implements AISPCustomValidator<OBReadBalance1, OBReadBalance1> {

	@Autowired
	private PSD2Validator psd2Validator;

	@Value("${app.swaggerValidations.response:#{true}}")
	private Boolean resValidationEnabled;

	@Autowired
	private CommonAccountValidations commonAccountValidations;

	@Override
	public OBReadBalance1 validateRequestParams(OBReadBalance1 t) {
		return null;
	}

	@Override
	public boolean validateResponseParams(OBReadBalance1 oBReadBalance) {
		if (resValidationEnabled)
			executeAccountBalanceResponseSwaggerValidations(oBReadBalance);
		executeAccountBalanceResponseCustomValidations(oBReadBalance);
		return true;
	}

	private void executeAccountBalanceResponseSwaggerValidations(OBReadBalance1 oBReadBalance) {
		psd2Validator.validate(oBReadBalance);
	}

	private void executeAccountBalanceResponseCustomValidations(OBReadBalance1 obReadBalance1) {
		if (!NullCheckUtils.isNullOrEmpty(obReadBalance1) && !NullCheckUtils.isNullOrEmpty(obReadBalance1.getData())) {
			for (OBReadBalance1DataBalance obCashBalance : obReadBalance1.getData().getBalance()) {
				if (!NullCheckUtils.isNullOrEmpty(obCashBalance)) {
					if (!NullCheckUtils.isNullOrEmpty(obCashBalance.getDateTime())) {
						commonAccountValidations.validateAndParseDateTimeFormatForResponse(obCashBalance.getDateTime());
					}
					if (!NullCheckUtils.isNullOrEmpty(obCashBalance.getAmount())) {
						if (!NullCheckUtils.isNullOrEmpty(obCashBalance.getAmount().getAmount())) {
							commonAccountValidations.validateAmount(obCashBalance.getAmount().getAmount());
						}
						if (!NullCheckUtils.isNullOrEmpty(obCashBalance.getAmount().getCurrency())) {
							commonAccountValidations.isValidCurrency(obCashBalance.getAmount().getCurrency());
						}
					}
					if(!NullCheckUtils.isNullOrEmpty(obCashBalance.getCreditLine())) {
						for (OBReadBalance1DataCreditLine obCreditLine1 : obCashBalance.getCreditLine()) {
							if(NullCheckUtils.isNullOrEmpty(obCreditLine1.getIncluded()))
							{
								throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
										InternalServerErrorMessage.INVALID_VALUE_INCLUDE));
							}
							if(!NullCheckUtils.isNullOrEmpty(obCreditLine1)) {
								if (!NullCheckUtils.isNullOrEmpty(obCreditLine1.getAmount())) {
									if (!NullCheckUtils.isNullOrEmpty(obCreditLine1.getAmount().getAmount())) {
										commonAccountValidations.validateAmount(obCreditLine1.getAmount().getAmount());
									}
									if (!NullCheckUtils.isNullOrEmpty(obCreditLine1.getAmount().getCurrency())) {
										commonAccountValidations.isValidCurrency(obCreditLine1.getAmount().getCurrency());
									}
								}
							}
						}
						
					}
				}
			}
		}
	}

	@Override
	public void validateUniqueId(String consentId) {
		if (NullCheckUtils.isNullOrEmpty(consentId)) {
			throw PSD2Exception.populatePSD2Exception(
					new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_FIELD_EXPECTED, ErrorMapKeys.NO_ACCOUNT_ID_FOUND));
		}
		commonAccountValidations.validateUniqueUUID(consentId);
	}

}