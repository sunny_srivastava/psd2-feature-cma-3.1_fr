package com.capgemini.psd2.aisp.validation.adapter.utilities;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.aisp.domain.OBReadConsent1Data;

@Component
@Configuration
@EnableConfigurationProperties
@ConfigurationProperties("app.permissionValidationRules")
public class PermissionsValidationRules {
	
	private Map<String, List<OBReadConsent1Data.PermissionsEnum>> excludedPermission = new HashMap<>();
	
	/*Required Permisson included in CMA 2.0*/
	private List<OBReadConsent1Data.PermissionsEnum> requiredPermission = new ArrayList<>();
	
	/** The mandatoryRules. */
	private Map<String, List<OBReadConsent1Data.PermissionsEnum>> mandatoryRules = new HashMap<>();

	/** The orConditonRules */
	private Map<String, List<OBReadConsent1Data.PermissionsEnum>> orConditonRules = new HashMap<>();

	public Map<String, List<OBReadConsent1Data.PermissionsEnum>> getMandatoryRules() {
		return mandatoryRules;
	}

	public void setMandatoryRules(Map<String, List<OBReadConsent1Data.PermissionsEnum>> mandatoryRules) {
		this.mandatoryRules = mandatoryRules;
	}

	public Map<String, List<OBReadConsent1Data.PermissionsEnum>> getOrConditonRules() {
		return orConditonRules;
	}

	public void setOrConditonRules(Map<String, List<OBReadConsent1Data.PermissionsEnum>> orConditonRules) {
		this.orConditonRules = orConditonRules;
	}

	public List<OBReadConsent1Data.PermissionsEnum> getRequiredPermission() {
		return requiredPermission;
	}

	public void setRequiredPermission(List<OBReadConsent1Data.PermissionsEnum> requiredPermission) {
		this.requiredPermission = requiredPermission;
	}

	public Map<String, List<OBReadConsent1Data.PermissionsEnum>> getExcludedPermission() {
		return excludedPermission;
	}

	public void setExcludedPermission(Map<String, List<OBReadConsent1Data.PermissionsEnum>> excludedPermission) {
		this.excludedPermission = excludedPermission;
	}

	public List<OBReadConsent1Data.PermissionsEnum> getExcludedPermissionForSpecificTenant(String key) {
		return this.excludedPermission.get(key);
	}

}
