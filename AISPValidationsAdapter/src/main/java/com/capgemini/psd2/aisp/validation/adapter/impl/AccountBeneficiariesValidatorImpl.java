package com.capgemini.psd2.aisp.validation.adapter.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.aisp.domain.OBBeneficiary5;
import com.capgemini.psd2.aisp.domain.OBReadBeneficiary5;
import com.capgemini.psd2.aisp.domain.OBReadConsentResponse1Data;
import com.capgemini.psd2.aisp.validation.adapter.AISPCustomValidator;
import com.capgemini.psd2.aisp.validation.adapter.utilities.CommonAccountValidations;
import com.capgemini.psd2.exceptions.ErrorMapKeys;
import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.InternalServerErrorMessage;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.utilities.NullCheckUtils;
import com.capgemini.psd2.validator.PSD2Validator;


@Component("accountBeneficiariesValidator")
@ConfigurationProperties("app")
public class AccountBeneficiariesValidatorImpl implements AISPCustomValidator<OBReadBeneficiary5, OBReadBeneficiary5> {

	@Autowired
	private PSD2Validator psd2Validator;

	@Value("${app.swaggerValidations.response:#{true}}")
	private Boolean resValidationEnabled;

	@Autowired
	private CommonAccountValidations commonAccountValidations;

	@Autowired
	private RequestHeaderAttributes reqHeaderAtrributes;
	
	@Override
	public OBReadBeneficiary5 validateRequestParams(OBReadBeneficiary5 t) {
		return null;
	}

	@Override
	public boolean validateResponseParams(OBReadBeneficiary5 v) {
		if (resValidationEnabled)
			executeAccountResponseSwaggerValidations(v);
		executeAccountResponseCustomValidations(v);

		return true;
	}

private void executeAccountResponseCustomValidations(OBReadBeneficiary5 obReadBeneficiary) {
		
	
		if(obReadBeneficiary.getData() != null){

			validatePermissionBasedResponse(obReadBeneficiary.getData().getBeneficiary());
			
			for(OBBeneficiary5 beneficiary :obReadBeneficiary.getData().getBeneficiary()){
				
				if(!NullCheckUtils.isNullOrEmpty(beneficiary)){
					if(!NullCheckUtils.isNullOrEmpty(beneficiary.getCreditorAgent())){
						commonAccountValidations.validateCreditorAgent(beneficiary.getCreditorAgent());
						
						if(!NullCheckUtils.isNullOrEmpty(beneficiary.getCreditorAgent().getPostalAddress()))
							commonAccountValidations.validateDomesticCreditorPostalAddress(beneficiary.getCreditorAgent().getPostalAddress());
					}
					if(!NullCheckUtils.isNullOrEmpty(beneficiary.getCreditorAccount())){
						if((!NullCheckUtils.isNullOrEmpty(beneficiary.getCreditorAccount().getSchemeName())) 
								&& (!NullCheckUtils.isNullOrEmpty(beneficiary.getCreditorAccount().getIdentification())))
							commonAccountValidations.validateSchemeNameWithIdentification(beneficiary.getCreditorAccount().getSchemeName(), 
															beneficiary.getCreditorAccount().getIdentification());
						
						if((!NullCheckUtils.isNullOrEmpty(beneficiary.getCreditorAccount().getSchemeName())) 
								&& (!NullCheckUtils.isNullOrEmpty(beneficiary.getCreditorAccount().getSecondaryIdentification())))
							commonAccountValidations.validateSchemeNameWithSecondaryIdentification(beneficiary.getCreditorAccount().getSchemeName(), 
															beneficiary.getCreditorAccount().getSecondaryIdentification());
					}
				}
			}
		}
	} 

	private void validatePermissionBasedResponse(List<OBBeneficiary5> beneficiaryList){
		
		if(!NullCheckUtils.isNullOrEmpty(reqHeaderAtrributes.getClaims())){
	
			if((reqHeaderAtrributes.getClaims().toString().toUpperCase()).contains(OBReadConsentResponse1Data.PermissionsEnum.READBENEFICIARIESDETAIL.toString().toUpperCase())){
			
				for(OBBeneficiary5 beneficiary : beneficiaryList){
					if(!NullCheckUtils.isNullOrEmpty(beneficiary)){
						if(NullCheckUtils.isNullOrEmpty(beneficiary.getCreditorAccount()))
							throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
									InternalServerErrorMessage.INSUFFICIENT_DATA_FOR_DETAIL_PERMISSION));
					}
				}
			}
		}
	}

	private void executeAccountResponseSwaggerValidations(OBReadBeneficiary5 v) {
		psd2Validator.validate(v);
		
	}
	@Override
	public void validateUniqueId(String consentId) {
		if (NullCheckUtils.isNullOrEmpty(consentId)) {
			throw PSD2Exception.populatePSD2Exception(
					new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_FIELD_INVALID, ErrorMapKeys.VALIDATION_ERROR));
		}
		commonAccountValidations.validateUniqueUUID(consentId);
	}

}