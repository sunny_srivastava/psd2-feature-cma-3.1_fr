package com.capgemini.psd2.aisp.validation.adapter.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.aisp.domain.OBReadParty2;
import com.capgemini.psd2.aisp.validation.adapter.AISPCustomValidator;
import com.capgemini.psd2.exceptions.ErrorMapKeys;
import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.utilities.NullCheckUtils;
import com.capgemini.psd2.validator.PSD2Validator;

@Component("accountPartyValidator")
@ConfigurationProperties("app")
public class AccountPartyValidatorImpl implements AISPCustomValidator<OBReadParty2, OBReadParty2> {

	@Autowired
	private PSD2Validator psd2Validator;

	@Value("${app.swaggerValidations.response:#{true}}")
	private Boolean resValidationEnabled;

	@Override
	public OBReadParty2 validateRequestParams(OBReadParty2 t) {
		return null;
	}

	@Override
	public boolean validateResponseParams(OBReadParty2 obReadParty) {
		if (resValidationEnabled)
			executeAccountPartyResponseSwaggerValidations(obReadParty);
		return true;
	}
	
	private void executeAccountPartyResponseSwaggerValidations(OBReadParty2 obReadParty) {
		psd2Validator.validate(obReadParty);
	}

	@Override
	public void validateUniqueId(String consentId) {
		if (NullCheckUtils.isNullOrEmpty(consentId)) {
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_FIELD_INVALID,
					ErrorMapKeys.VALIDATION_ERROR));
		}
	}
}