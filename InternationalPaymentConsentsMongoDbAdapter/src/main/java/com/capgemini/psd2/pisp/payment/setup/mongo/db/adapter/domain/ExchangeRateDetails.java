package com.capgemini.psd2.pisp.payment.setup.mongo.db.adapter.domain;

import org.springframework.data.mongodb.core.mapping.Document;

import com.capgemini.psd2.pisp.domain.OBExchangeRate2;
import com.fasterxml.jackson.annotation.JsonProperty;

@Document(collection = "MockExchangeRateDetails")
public class ExchangeRateDetails extends OBExchangeRate2 {

	@JsonProperty
	private String currencyOfTransfer;

	public String getCurrencyOfTransfer() {
		return currencyOfTransfer;
	}

	public void setCurrencyOfTransfer(String currencyOfTransfer) {
		this.currencyOfTransfer = currencyOfTransfer;
	}

	@Override
	public String toString() {
		return "ExchangeRateDetails [currencyOfTransfer=" + currencyOfTransfer + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((currencyOfTransfer == null) ? 0 : currencyOfTransfer.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		ExchangeRateDetails other = (ExchangeRateDetails) obj;
		if (currencyOfTransfer == null) {
			if (other.currencyOfTransfer != null)
				return false;
		} else if (!currencyOfTransfer.equals(other.currencyOfTransfer))
			return false;
		return true;
	}
}
