/*
 * Payment Process API
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v1.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package com.capgemini.psd2.foundationservice.international.payment.authorise.boi.adapter.domain;

import java.util.Objects;
import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * Basic details of the creditor account, to which a credit entry will be posted as a result of the payment transaction.
 */
@ApiModel(description = "Basic details of the creditor account, to which a credit entry will be posted as a result of the payment transaction.")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2019-12-02T11:26:39.590+05:30")
public class ProposingPartyAccount {
  @SerializedName("accountIdentification")
  private String accountIdentification = null;

  @SerializedName("accountNumber")
  private String accountNumber = null;

  @SerializedName("schemeName")
  private String schemeName = null;

  @SerializedName("accountName")
  private String accountName = null;

  @SerializedName("secondaryIdentification")
  private String secondaryIdentification = null;

  @SerializedName("payeeCountry")
  private Country payeeCountry = null;

  public ProposingPartyAccount accountIdentification(String accountIdentification) {
    this.accountIdentification = accountIdentification;
    return this;
  }

   /**
   * Debtor BOI account identification.
   * @return accountIdentification
  **/
  @ApiModelProperty(required = true, value = "Debtor BOI account identification.")
  public String getAccountIdentification() {
    return accountIdentification;
  }

  public void setAccountIdentification(String accountIdentification) {
    this.accountIdentification = accountIdentification;
  }

  public ProposingPartyAccount accountNumber(String accountNumber) {
    this.accountNumber = accountNumber;
    return this;
  }

   /**
   * Debtor BOI account number.
   * @return accountNumber
  **/
  @ApiModelProperty(value = "Debtor BOI account number.")
  public String getAccountNumber() {
    return accountNumber;
  }

  public void setAccountNumber(String accountNumber) {
    this.accountNumber = accountNumber;
  }

  public ProposingPartyAccount schemeName(String schemeName) {
    this.schemeName = schemeName;
    return this;
  }

   /**
   * Name of the identification scheme, in a coded form as published in an external list.
   * @return schemeName
  **/
  @ApiModelProperty(required = true, value = "Name of the identification scheme, in a coded form as published in an external list.")
  public String getSchemeName() {
    return schemeName;
  }

  public void setSchemeName(String schemeName) {
    this.schemeName = schemeName;
  }

  public ProposingPartyAccount accountName(String accountName) {
    this.accountName = accountName;
    return this;
  }

   /**
   * Name of the account, as assigned by the account servicing institution.
   * @return accountName
  **/
  @ApiModelProperty(required = true, value = "Name of the account, as assigned by the account servicing institution.")
  public String getAccountName() {
    return accountName;
  }

  public void setAccountName(String accountName) {
    this.accountName = accountName;
  }

  public ProposingPartyAccount secondaryIdentification(String secondaryIdentification) {
    this.secondaryIdentification = secondaryIdentification;
    return this;
  }

   /**
   * This is secondary identification of the account, as assigned by the account servicing institution. This can be used by building societies to additionally identify accounts with a roll number (in addition to a sort code and account number combination).
   * @return secondaryIdentification
  **/
  @ApiModelProperty(value = "This is secondary identification of the account, as assigned by the account servicing institution. This can be used by building societies to additionally identify accounts with a roll number (in addition to a sort code and account number combination).")
  public String getSecondaryIdentification() {
    return secondaryIdentification;
  }

  public void setSecondaryIdentification(String secondaryIdentification) {
    this.secondaryIdentification = secondaryIdentification;
  }

  public ProposingPartyAccount payeeCountry(Country payeeCountry) {
    this.payeeCountry = payeeCountry;
    return this;
  }

   /**
   * Get payeeCountry
   * @return payeeCountry
  **/
  @ApiModelProperty(value = "")
  public Country getPayeeCountry() {
    return payeeCountry;
  }

  public void setPayeeCountry(Country payeeCountry) {
    this.payeeCountry = payeeCountry;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ProposingPartyAccount proposingPartyAccount = (ProposingPartyAccount) o;
    return Objects.equals(this.accountIdentification, proposingPartyAccount.accountIdentification) &&
        Objects.equals(this.accountNumber, proposingPartyAccount.accountNumber) &&
        Objects.equals(this.schemeName, proposingPartyAccount.schemeName) &&
        Objects.equals(this.accountName, proposingPartyAccount.accountName) &&
        Objects.equals(this.secondaryIdentification, proposingPartyAccount.secondaryIdentification) &&
        Objects.equals(this.payeeCountry, proposingPartyAccount.payeeCountry);
  }

  @Override
  public int hashCode() {
    return Objects.hash(accountIdentification, accountNumber, schemeName, accountName, secondaryIdentification, payeeCountry);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ProposingPartyAccount {\n");
    
    sb.append("    accountIdentification: ").append(toIndentedString(accountIdentification)).append("\n");
    sb.append("    accountNumber: ").append(toIndentedString(accountNumber)).append("\n");
    sb.append("    schemeName: ").append(toIndentedString(schemeName)).append("\n");
    sb.append("    accountName: ").append(toIndentedString(accountName)).append("\n");
    sb.append("    secondaryIdentification: ").append(toIndentedString(secondaryIdentification)).append("\n");
    sb.append("    payeeCountry: ").append(toIndentedString(payeeCountry)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

