package com.capgemini.psd2.foundationservice.international.payment.authorise.boi.adapter;

import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import com.capgemini.psd2.foundationservice.international.payment.authorise.boi.adapter.client.InternationalPaymentAuthoriseFoundationServiceClient;
import com.capgemini.psd2.foundationservice.international.payment.authorise.boi.adapter.delegate.InternationalPaymentAuthoriseFoundationServiceDelegate;
import com.capgemini.psd2.foundationservice.international.payment.authorise.boi.adapter.domain.PaymentInstructionProposalAuthorisingParty;
import com.capgemini.psd2.foundationservice.international.payment.authorise.boi.adapter.domain.PaymentInstructionProposalInternational;
import com.capgemini.psd2.foundationservice.international.payment.authorise.boi.adapter.transformer.InternationalPaymentAuthoriseFoundationServiceTransformer;
import com.capgemini.psd2.pisp.domain.CustomIPaymentConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.OBWriteInternationalConsentResponse1;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.rest.client.model.RequestInfo;

@Component
public class InternationalPaymentAuthoriseFoundationServiceAdapter {
	
	@Autowired
	private InternationalPaymentAuthoriseFoundationServiceDelegate intPayConsDelegate;

	@Autowired
	private InternationalPaymentAuthoriseFoundationServiceClient intPayConsClient;

	
	@Autowired
	private InternationalPaymentAuthoriseFoundationServiceTransformer intPayConsTransformer;
	
	public CustomIPaymentConsentsPOSTResponse createStagingInternationalPaymentConsents(OBWriteInternationalConsentResponse1 requestObj,			
			CustomPaymentStageIdentifiers customStageIdentifiers, Map<String, String> params) {	
		RequestInfo requestInfo = new RequestInfo();
		HttpHeaders httpHeaders = intPayConsDelegate.createPaymentRequestHeadersPost(customStageIdentifiers,params);
		PaymentInstructionProposalAuthorisingParty PaymentInstructionProposalAuthorisingParty = null;		
		String internationalPaymentAuthoriseURL = intPayConsDelegate.internationalPaymentAuthoriseConsentPostBaseURL(customStageIdentifiers.getPaymentConsentId());
		requestInfo.setUrl(internationalPaymentAuthoriseURL);
		PaymentInstructionProposalAuthorisingParty = intPayConsTransformer.transformInternationalPaymentRequest(requestObj);
		PaymentInstructionProposalInternational paymentInstructionProposalResponse = intPayConsClient.restTransportForInternationalPaymentFoundationServicePost(requestInfo, PaymentInstructionProposalAuthorisingParty, PaymentInstructionProposalInternational.class, httpHeaders);
		return intPayConsTransformer.transformInternationalPaymentResponse(paymentInstructionProposalResponse);
	}	

}
