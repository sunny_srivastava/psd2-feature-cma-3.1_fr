package com.capgemini.psd2.foundationservice.international.payment.authorise.boi.adapter.transformer;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.capgemini.psd2.foundationservice.international.payment.authorise.boi.adapter.domain.AccountInformation;
import com.capgemini.psd2.foundationservice.international.payment.authorise.boi.adapter.domain.PaymentInstructionProposalAuthorisingParty;
import com.capgemini.psd2.foundationservice.international.payment.authorise.boi.adapter.domain.PaymentInstructionProposalInternational;
import com.capgemini.psd2.foundationservice.international.payment.authorise.boi.adapter.domain.ProposalStatus;
import com.capgemini.psd2.pisp.domain.CustomIPaymentConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.OBAddressTypeCode;
import com.capgemini.psd2.pisp.domain.OBAuthorisation1;
import com.capgemini.psd2.pisp.domain.OBBranchAndFinancialInstitutionIdentification3;
import com.capgemini.psd2.pisp.domain.OBCashAccountCreditor2;
import com.capgemini.psd2.pisp.domain.OBCashAccountDebtor3;
import com.capgemini.psd2.pisp.domain.OBChargeBearerType1Code;
import com.capgemini.psd2.pisp.domain.OBDomestic1InstructedAmount;
import com.capgemini.psd2.pisp.domain.OBExchangeRate1;
import com.capgemini.psd2.pisp.domain.OBExchangeRateType2Code;
import com.capgemini.psd2.pisp.domain.OBExternalAuthorisation1Code;
import com.capgemini.psd2.pisp.domain.OBExternalConsentStatus1Code;
import com.capgemini.psd2.pisp.domain.OBExternalPaymentContext1Code;
import com.capgemini.psd2.pisp.domain.OBInternational1;
import com.capgemini.psd2.pisp.domain.OBPartyIdentification43;
import com.capgemini.psd2.pisp.domain.OBPostalAddress6;
import com.capgemini.psd2.pisp.domain.OBPriority2Code;
import com.capgemini.psd2.pisp.domain.OBRemittanceInformation1;
import com.capgemini.psd2.pisp.domain.OBRisk1;
import com.capgemini.psd2.pisp.domain.OBRisk1DeliveryAddress;
import com.capgemini.psd2.pisp.domain.OBWriteDataInternationalConsentResponse1;
import com.capgemini.psd2.pisp.domain.OBWriteInternationalConsentResponse1;
import com.capgemini.psd2.pisp.enums.ProcessConsentStatusEnum;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Component
public class InternationalPaymentAuthoriseFoundationServiceTransformer {

	public <T> CustomIPaymentConsentsPOSTResponse transformInternationalPaymentResponse(T inputBalanceObj) {

		CustomIPaymentConsentsPOSTResponse customIPaymentConsentsPOSTResponse = new CustomIPaymentConsentsPOSTResponse();
		OBWriteDataInternationalConsentResponse1 oBWriteDataInternationalConsentResponse1 = new OBWriteDataInternationalConsentResponse1();
		PaymentInstructionProposalInternational paymentInstructionProposalInternational = (PaymentInstructionProposalInternational) inputBalanceObj;
		OBInternational1 oBInternational1 = new OBInternational1();
		OBRisk1 oBRisk1 = new OBRisk1();
		// consentPorcessStatus
		ProposalStatus proposalStatus = ProposalStatus
				.fromValue(paymentInstructionProposalInternational.getProposalStatus().toString());
		if (((proposalStatus.toString()).equals("Rejected")) && (!NullCheckUtils
				.isNullOrEmpty(paymentInstructionProposalInternational.getPaymentInstructionProposalId()))) {
			customIPaymentConsentsPOSTResponse.setConsentPorcessStatus(ProcessConsentStatusEnum.FAIL);
		} else if ((proposalStatus.toString()).equals("AwaitingAuthorisation") && !NullCheckUtils
				.isNullOrEmpty(paymentInstructionProposalInternational.getPaymentInstructionProposalId())) {
			customIPaymentConsentsPOSTResponse.setConsentPorcessStatus(ProcessConsentStatusEnum.PASS);
		}
		// ConsentId
		if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational.getPaymentInstructionProposalId())) {
			oBWriteDataInternationalConsentResponse1
					.setConsentId(paymentInstructionProposalInternational.getPaymentInstructionProposalId());
		}
		// CreationDateTime
		if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational.getProposalCreationDatetime())) {
			oBWriteDataInternationalConsentResponse1
					.setCreationDateTime(paymentInstructionProposalInternational.getProposalCreationDatetime());
		}
		// status
		if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational.getProposalStatus())) {
			OBExternalConsentStatus1Code status = OBExternalConsentStatus1Code
					.fromValue(paymentInstructionProposalInternational.getProposalStatus().toString());
			oBWriteDataInternationalConsentResponse1.setStatus(status);
		}
		// StatusUpdateDateTime
		if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational.getProposalStatusUpdateDatetime())) {
			oBWriteDataInternationalConsentResponse1
					.setStatusUpdateDateTime(paymentInstructionProposalInternational.getProposalStatusUpdateDatetime());
		}
		// InstructionIdentification
		if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational.getInstructionReference())) {
			oBInternational1
					.setInstructionIdentification(paymentInstructionProposalInternational.getInstructionReference());
		}
		// EndToEndIdentification
		if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational.getInstructionEndToEndReference())) {
			oBInternational1.setEndToEndIdentification(
					paymentInstructionProposalInternational.getInstructionEndToEndReference());
		}
		// LocalInstrument
		if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational.getInstructionLocalInstrument())) {
			oBInternational1
					.setLocalInstrument(paymentInstructionProposalInternational.getInstructionLocalInstrument());
		}
		// InstructionPriority
		if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational.getPriorityCode())) {
			OBPriority2Code oBPriority2Code = OBPriority2Code
					.fromValue(paymentInstructionProposalInternational.getPriorityCode().toString());
			oBInternational1.setInstructionPriority(oBPriority2Code);
		}
		// Purpose
		if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational.getPurpose()) && 
			!NullCheckUtils
					.isNullOrEmpty(paymentInstructionProposalInternational.getPurpose().getProprietaryPurpose())) {
				oBInternational1
						.setPurpose(paymentInstructionProposalInternational.getPurpose().getProprietaryPurpose());
			
		}
		// ChargeBearer
		if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational.getCharges()) && 
			!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational.getCharges().get(0)) && 
				!NullCheckUtils
						.isNullOrEmpty(paymentInstructionProposalInternational.getCharges().get(0).getChargeBearer()) ) { 
					OBChargeBearerType1Code oBChargeBearerType1Code = OBChargeBearerType1Code.fromValue(
							paymentInstructionProposalInternational.getCharges().get(0).getChargeBearer().toString());
					oBInternational1.setChargeBearer(oBChargeBearerType1Code);
				
			
		}
		// CurrencyOfTransfer
		if (!NullCheckUtils
				.isNullOrEmpty(paymentInstructionProposalInternational.getCurrencyOfTransfer().getIsoAlphaCode())) {
			oBInternational1.setCurrencyOfTransfer(
					paymentInstructionProposalInternational.getCurrencyOfTransfer().getIsoAlphaCode());
		}
		// Amount && Currency
		if (!NullCheckUtils.isNullOrEmpty(
				paymentInstructionProposalInternational.getFinancialEventAmount().getTransactionCurrency())
				&& !NullCheckUtils.isNullOrEmpty(
						paymentInstructionProposalInternational.getTransactionCurrency().getIsoAlphaCode())) {
			OBDomestic1InstructedAmount amountInstructed = new OBDomestic1InstructedAmount();
			Double amountMule = paymentInstructionProposalInternational.getFinancialEventAmount()
					.getTransactionCurrency();
			amountInstructed.setAmount(BigDecimal.valueOf(amountMule).toPlainString());
			amountInstructed
					.setCurrency(paymentInstructionProposalInternational.getTransactionCurrency().getIsoAlphaCode());
			oBInternational1.setInstructedAmount(amountInstructed);
		}
		// ExchangeRateInformation
		OBExchangeRate1 oBExchangeRate1 = new OBExchangeRate1();
		if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational.getExchangeRateQuote())) {
			// UnitCurrency
			if (!NullCheckUtils
					.isNullOrEmpty(paymentInstructionProposalInternational.getExchangeRateQuote().getUnitCurrency())) {
				oBExchangeRate1.setUnitCurrency(paymentInstructionProposalInternational.getExchangeRateQuote()
						.getUnitCurrency().getIsoAlphaCode());
			}
			// ExchangeRate
			if (!NullCheckUtils
					.isNullOrEmpty(paymentInstructionProposalInternational.getExchangeRateQuote().getExchangeRate())) {
				oBExchangeRate1.setExchangeRate(BigDecimal
						.valueOf(paymentInstructionProposalInternational.getExchangeRateQuote().getExchangeRate()));
			}
			// RateType
			if (!NullCheckUtils
					.isNullOrEmpty(paymentInstructionProposalInternational.getExchangeRateQuote().getRateQuoteType())) {
				oBExchangeRate1.setRateType(OBExchangeRateType2Code.fromValue(
						paymentInstructionProposalInternational.getExchangeRateQuote().getRateQuoteType().toString()));
			}
			oBInternational1.setExchangeRateInformation(oBExchangeRate1);
		}
		// Debtor Account
		// SchemeName
		if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational.getAuthorisingPartyAccount())) {
			OBCashAccountDebtor3 oBCashAccountDebtor3 = new OBCashAccountDebtor3();
			if (!NullCheckUtils.isNullOrEmpty(
					paymentInstructionProposalInternational.getAuthorisingPartyAccount().getSchemeName())) {
				oBCashAccountDebtor3.setSchemeName(
						paymentInstructionProposalInternational.getAuthorisingPartyAccount().getSchemeName());
			}
			// Identification
			if (!NullCheckUtils.isNullOrEmpty(
					paymentInstructionProposalInternational.getAuthorisingPartyAccount().getAccountIdentification())) {
				oBCashAccountDebtor3.setIdentification(paymentInstructionProposalInternational
						.getAuthorisingPartyAccount().getAccountIdentification());
			}
			// Name
			if (!NullCheckUtils.isNullOrEmpty(
					paymentInstructionProposalInternational.getAuthorisingPartyAccount().getAccountName())) {
				oBCashAccountDebtor3
						.setName(paymentInstructionProposalInternational.getAuthorisingPartyAccount().getAccountName());
			}
			// SecondaryIdentification
			if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational.getAuthorisingPartyAccount()
					.getSecondaryIdentification())) {
				oBCashAccountDebtor3.setSecondaryIdentification(paymentInstructionProposalInternational
						.getAuthorisingPartyAccount().getSecondaryIdentification());
			}
			oBInternational1.setDebtorAccount(oBCashAccountDebtor3);
		}
		// Creditor Account
		if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational.getProposingPartyAccount())) {
			OBCashAccountCreditor2 oBCashAccountCreditor2 = new OBCashAccountCreditor2();
			if (!NullCheckUtils
					.isNullOrEmpty(paymentInstructionProposalInternational.getProposingParty().getPartyName())) {
				oBCashAccountCreditor2
						.setName(paymentInstructionProposalInternational.getProposingParty().getPartyName());
			}
			oBInternational1.setCreditorAccount(oBCashAccountCreditor2);
		}
		OBPostalAddress6 oBPostalAddress6 = new OBPostalAddress6();
		// Creditor Logic need to add
		OBPartyIdentification43 oBPartyIdentification43 = new OBPartyIdentification43();
		if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational.getProposingParty()) && 
			!NullCheckUtils
					.isNullOrEmpty(paymentInstructionProposalInternational.getProposingParty().getPartyName())) { 
				oBPartyIdentification43
						.setName(paymentInstructionProposalInternational.getProposingParty().getPartyName());
				oBInternational1.setCreditor(oBPartyIdentification43);
			
		}
		// Creditor Postal Address
		if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational.getProposingPartyPostalAddress())) {
			// AddressType
			if (!NullCheckUtils.isNullOrEmpty(
					paymentInstructionProposalInternational.getProposingPartyPostalAddress().getAddressType())) {
				OBAddressTypeCode addressType = OBAddressTypeCode.fromValue(
						paymentInstructionProposalInternational.getProposingPartyPostalAddress().getAddressType());
				oBPostalAddress6.setAddressType(addressType);
			}
			// Department
			if (!NullCheckUtils.isNullOrEmpty(
					paymentInstructionProposalInternational.getProposingPartyPostalAddress().getDepartment())) {
				oBPostalAddress6.setDepartment(
						paymentInstructionProposalInternational.getProposingPartyPostalAddress().getDepartment());
			}
			// SubDepartment
			if (!NullCheckUtils.isNullOrEmpty(
					paymentInstructionProposalInternational.getProposingPartyPostalAddress().getSubDepartment())) {
				oBPostalAddress6.setSubDepartment(
						paymentInstructionProposalInternational.getProposingPartyPostalAddress().getSubDepartment());
			}
			// StreetName
			if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational.getProposingPartyPostalAddress()
					.getGeoCodeBuildingName())) {
				oBPostalAddress6.setStreetName(paymentInstructionProposalInternational.getProposingPartyPostalAddress()
						.getGeoCodeBuildingName());
			}
			// BuildingNumber
			if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational.getProposingPartyPostalAddress()
					.getGeoCodeBuildingNumber())) {
				oBPostalAddress6.setBuildingNumber(paymentInstructionProposalInternational
						.getProposingPartyPostalAddress().getGeoCodeBuildingNumber());
			}
			// PostCode
			if (!NullCheckUtils.isNullOrEmpty(
					paymentInstructionProposalInternational.getProposingPartyPostalAddress().getPostCodeNumber())) {
				oBPostalAddress6.setPostCode(
						paymentInstructionProposalInternational.getProposingPartyPostalAddress().getPostCodeNumber());
			}
			// TownName
			if (!NullCheckUtils.isNullOrEmpty(
					paymentInstructionProposalInternational.getProposingPartyPostalAddress().getTownName())) {
				oBPostalAddress6.setTownName(
						paymentInstructionProposalInternational.getProposingPartyPostalAddress().getTownName());
			}
			// CountrySubDivision
			if (!NullCheckUtils.isNullOrEmpty(
					paymentInstructionProposalInternational.getProposingPartyPostalAddress().getCountrySubDivision())) {
				oBPostalAddress6.setCountrySubDivision(paymentInstructionProposalInternational
						.getProposingPartyPostalAddress().getCountrySubDivision());
			}
			// Country
			if (!NullCheckUtils.isNullOrEmpty(
					paymentInstructionProposalInternational.getProposingPartyPostalAddress().getAddressCountry())
					&& !NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational
							.getProposingPartyPostalAddress().getAddressCountry().getIsoCountryAlphaTwoCode())) {
				oBPostalAddress6.setCountry(paymentInstructionProposalInternational.getProposingPartyPostalAddress()
						.getAddressCountry().getIsoCountryAlphaTwoCode());
			}
			List<String> addLine = paymentInstructionProposalInternational.getProposingPartyPostalAddress()
					.getAddressLine();
			if (  !NullCheckUtils.isNullOrEmpty(addLine) && (!addLine.isEmpty()) ) {
				oBPostalAddress6.setAddressLine(
						paymentInstructionProposalInternational.getProposingPartyPostalAddress().getAddressLine());
			}
			oBPartyIdentification43.setPostalAddress(oBPostalAddress6);
		}
		// CreditorAgent
		if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational.getProposingPartyAgent())) {
			OBBranchAndFinancialInstitutionIdentification3 oBBranchAndFinancialInstitutionIdentification3 = new OBBranchAndFinancialInstitutionIdentification3();
			// SchemeName
			if (!NullCheckUtils
					.isNullOrEmpty(paymentInstructionProposalInternational.getProposingPartyAgent().getSchemeName())) {
				oBBranchAndFinancialInstitutionIdentification3.setSchemeName(
						paymentInstructionProposalInternational.getProposingPartyAgent().getSchemeName());
			}
			// Identification
			if (!NullCheckUtils.isNullOrEmpty(
					paymentInstructionProposalInternational.getProposingPartyAgent().getAgentIdentification())) {
				oBBranchAndFinancialInstitutionIdentification3.setIdentification(
						paymentInstructionProposalInternational.getProposingPartyAgent().getAgentIdentification());
			}
			// Name
			if (!NullCheckUtils
					.isNullOrEmpty(paymentInstructionProposalInternational.getProposingPartyAgent().getAgentName())) {
				oBBranchAndFinancialInstitutionIdentification3
						.setName(paymentInstructionProposalInternational.getProposingPartyAgent().getAgentName());
			}
			// PostalAddress of Creditor agent
			if (!NullCheckUtils.isNullOrEmpty(
					paymentInstructionProposalInternational.getProposingPartyAgent().getPartyAddress())) {
				// AddressType
				if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational.getProposingPartyAgent()
						.getPartyAddress().getAddressType())) {
					OBAddressTypeCode oBAddressTypeCode = OBAddressTypeCode
							.fromValue(paymentInstructionProposalInternational.getProposingPartyAgent()
									.getPartyAddress().getAddressType());
					oBPostalAddress6.setAddressType(oBAddressTypeCode);
				}
				// Department
				if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational.getProposingPartyAgent()
						.getPartyAddress().getDepartment())) {
					oBPostalAddress6.setDepartment(paymentInstructionProposalInternational.getProposingPartyAgent()
							.getPartyAddress().getDepartment());
				}
				// SubDepartment
				if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational.getProposingPartyAgent()
						.getPartyAddress().getSubDepartment())) {
					oBPostalAddress6.setSubDepartment(paymentInstructionProposalInternational.getProposingPartyAgent()
							.getPartyAddress().getSubDepartment());
				}
				// StreetName
				if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational.getProposingPartyAgent()
						.getPartyAddress().getGeoCodeBuildingName())) {
					oBPostalAddress6.setStreetName(paymentInstructionProposalInternational.getProposingPartyAgent()
							.getPartyAddress().getGeoCodeBuildingName());
				}
				// BuildingNumber
				if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational.getProposingPartyAgent()
						.getPartyAddress().getGeoCodeBuildingNumber())) {
					oBPostalAddress6.setBuildingNumber(paymentInstructionProposalInternational.getProposingPartyAgent()
							.getPartyAddress().getGeoCodeBuildingNumber());
				}
				// PostCode
				if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational.getProposingPartyAgent()
						.getPartyAddress().getPostCodeNumber())) {
					oBPostalAddress6.setPostCode(paymentInstructionProposalInternational.getProposingPartyAgent()
							.getPartyAddress().getPostCodeNumber());
				}
				// TownName
				if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational.getProposingPartyAgent()
						.getPartyAddress().getTownName())) {
					oBPostalAddress6.setTownName(paymentInstructionProposalInternational.getProposingPartyAgent()
							.getPartyAddress().getTownName());
				}
				// CountrySubDivision
				if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational.getProposingPartyAgent()
						.getPartyAddress().getCountrySubDivision())) {
					oBPostalAddress6.setCountrySubDivision(paymentInstructionProposalInternational
							.getProposingPartyAgent().getPartyAddress().getCountrySubDivision());
				}
				// Country
				if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational.getProposingPartyAgent()
						.getPartyAddress().getAddressCountry())
						&& !NullCheckUtils
								.isNullOrEmpty(paymentInstructionProposalInternational.getProposingPartyAgent()
										.getPartyAddress().getAddressCountry().getIsoCountryAlphaTwoCode())) {
					oBPostalAddress6.setCountry(paymentInstructionProposalInternational.getProposingPartyAgent()
							.getPartyAddress().getAddressCountry().getIsoCountryAlphaTwoCode());
				}
				// AddressLine
				if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational.getProposingPartyAgent()
						.getPartyAddress().getAddressLine())) {
					List<String> addLine = paymentInstructionProposalInternational.getProposingPartyPostalAddress()
							.getAddressLine();
					if (!NullCheckUtils.isNullOrEmpty(addLine) && (!addLine.isEmpty() )) {
						oBPostalAddress6.setAddressLine(addLine);
					}
				}
			}
			oBBranchAndFinancialInstitutionIdentification3.setPostalAddress(oBPostalAddress6);
			oBInternational1.setCreditorAgent(oBBranchAndFinancialInstitutionIdentification3);
		}
		// RemittanceInformation
		// Reference
		if ((!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational.getAuthorisingPartyReference()))
				|| (!NullCheckUtils.isNullOrEmpty(
						paymentInstructionProposalInternational.getAuthorisingPartyUnstructuredReference()))) {
			OBRemittanceInformation1 oBRemittanceInformation1 = new OBRemittanceInformation1();
			boolean isInit = false;
			if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational.getAuthorisingPartyReference())) {
				oBRemittanceInformation1
						.setReference(paymentInstructionProposalInternational.getAuthorisingPartyReference());
				isInit = true;
			}
			// Unstructured
			if (!NullCheckUtils.isNullOrEmpty(
					paymentInstructionProposalInternational.getAuthorisingPartyUnstructuredReference())) {
				oBRemittanceInformation1.setUnstructured(
						paymentInstructionProposalInternational.getAuthorisingPartyUnstructuredReference());
				isInit = true;
			}
			if (isInit)
				oBInternational1.setRemittanceInformation(oBRemittanceInformation1);
		}
		// Authorisation
		// AuthorisationType & CompletionDateTime
		if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational.getAuthorisationType())) {
			OBAuthorisation1 oBAuthorisation1 = new OBAuthorisation1();
			String authType = paymentInstructionProposalInternational.getAuthorisationType().toString();
			OBExternalAuthorisation1Code authCode = OBExternalAuthorisation1Code.fromValue(authType);
			oBAuthorisation1.setAuthorisationType(authCode);
			if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational.getAuthorisationDatetime())) {
				oBAuthorisation1
						.setCompletionDateTime(paymentInstructionProposalInternational.getAuthorisationDatetime());
			}
			oBWriteDataInternationalConsentResponse1.setAuthorisation(oBAuthorisation1);
		}
		// Risk
		//
		if (!NullCheckUtils
				.isNullOrEmpty(paymentInstructionProposalInternational.getPaymentInstructionRiskFactorReference())) {
			OBRisk1DeliveryAddress oBRisk1DeliveryAddress = null;
			// PaymentContextCode
			if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational
					.getPaymentInstructionRiskFactorReference().getPaymentContextCode())) {
				OBExternalPaymentContext1Code contextCode = OBExternalPaymentContext1Code
						.fromValue(paymentInstructionProposalInternational.getPaymentInstructionRiskFactorReference()
								.getPaymentContextCode());
				oBRisk1.setPaymentContextCode(contextCode);
			}
			// MerchantCategoryCode
			if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational
					.getPaymentInstructionRiskFactorReference().getMerchantCategoryCode())) {
				oBRisk1.setMerchantCategoryCode(paymentInstructionProposalInternational
						.getPaymentInstructionRiskFactorReference().getMerchantCategoryCode());
			}
			// MerchantCustomerIdentification
			if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational
					.getPaymentInstructionRiskFactorReference().getMerchantCustomerIdentification())) {
				oBRisk1.setMerchantCustomerIdentification(paymentInstructionProposalInternational
						.getPaymentInstructionRiskFactorReference().getMerchantCustomerIdentification());
			}
			// DeliveryAddress
			// AddressLine
			if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational
					.getPaymentInstructionRiskFactorReference().getCounterPartyAddress())) {
				oBRisk1DeliveryAddress = new OBRisk1DeliveryAddress();
				if (!NullCheckUtils.isNullOrEmpty((paymentInstructionProposalInternational
						.getPaymentInstructionRiskFactorReference().getCounterPartyAddress().getFirstAddressLine()))) {
					List<String> addressLine = new ArrayList<>();
					if (!NullCheckUtils.isNullOrEmpty(
							(paymentInstructionProposalInternational.getPaymentInstructionRiskFactorReference()
									.getCounterPartyAddress().getSecondAddressLine()))) {
						addressLine
								.add(paymentInstructionProposalInternational.getPaymentInstructionRiskFactorReference()
										.getCounterPartyAddress().getFirstAddressLine());
						addressLine
								.add(paymentInstructionProposalInternational.getPaymentInstructionRiskFactorReference()
										.getCounterPartyAddress().getSecondAddressLine());
					} else {
						addressLine
								.add(paymentInstructionProposalInternational.getPaymentInstructionRiskFactorReference()
										.getCounterPartyAddress().getFirstAddressLine());
					}
					oBRisk1DeliveryAddress.setAddressLine(addressLine);
				}
				// StreetName
				if (!NullCheckUtils.isNullOrEmpty(
						paymentInstructionProposalInternational.getPaymentInstructionRiskFactorReference()
								.getCounterPartyAddress().getGeoCodeBuildingName())) {
					oBRisk1DeliveryAddress.setStreetName(
							paymentInstructionProposalInternational.getPaymentInstructionRiskFactorReference()
									.getCounterPartyAddress().getGeoCodeBuildingName());
				}
				// BuildingNumber
				if (!NullCheckUtils.isNullOrEmpty(
						paymentInstructionProposalInternational.getPaymentInstructionRiskFactorReference()
								.getCounterPartyAddress().getGeoCodeBuildingNumber())) {
					oBRisk1DeliveryAddress.setBuildingNumber(
							paymentInstructionProposalInternational.getPaymentInstructionRiskFactorReference()
									.getCounterPartyAddress().getGeoCodeBuildingNumber());
				}
				// PostCode
				if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational
						.getPaymentInstructionRiskFactorReference().getCounterPartyAddress().getPostCodeNumber())) {
					oBRisk1DeliveryAddress.setPostCode(paymentInstructionProposalInternational
							.getPaymentInstructionRiskFactorReference().getCounterPartyAddress().getPostCodeNumber());
				}
				// TownName
				if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational
						.getPaymentInstructionRiskFactorReference().getCounterPartyAddress().getThirdAddressLine())) {
					oBRisk1DeliveryAddress.setTownName(paymentInstructionProposalInternational
							.getPaymentInstructionRiskFactorReference().getCounterPartyAddress().getThirdAddressLine());
				}
				// CountySubDivision
				if (!NullCheckUtils.isNullOrEmpty(paymentInstructionProposalInternational
						.getPaymentInstructionRiskFactorReference().getCounterPartyAddress().getFourthAddressLine())) {
					String addressLine = null;
					if (!NullCheckUtils.isNullOrEmpty(
							paymentInstructionProposalInternational.getPaymentInstructionRiskFactorReference()
									.getCounterPartyAddress().getFifthAddressLine())) {
						addressLine = paymentInstructionProposalInternational.getPaymentInstructionRiskFactorReference()
								.getCounterPartyAddress().getFourthAddressLine()
								+ paymentInstructionProposalInternational.getPaymentInstructionRiskFactorReference()
										.getCounterPartyAddress().getFifthAddressLine();
					} else {
						addressLine = paymentInstructionProposalInternational.getPaymentInstructionRiskFactorReference()
								.getCounterPartyAddress().getFourthAddressLine();
					}
					oBRisk1DeliveryAddress.setCountrySubDivision(addressLine);
				}
				// Country
				if (!NullCheckUtils.isNullOrEmpty(
						paymentInstructionProposalInternational.getPaymentInstructionRiskFactorReference()
								.getCounterPartyAddress().getAddressCountry().getIsoCountryAlphaTwoCode())) {
					oBRisk1DeliveryAddress.setCountry(
							paymentInstructionProposalInternational.getPaymentInstructionRiskFactorReference()
									.getCounterPartyAddress().getAddressCountry().getIsoCountryAlphaTwoCode());
				}
				oBRisk1.setDeliveryAddress(oBRisk1DeliveryAddress);
			}
		}
		oBWriteDataInternationalConsentResponse1.setInitiation(oBInternational1);
		customIPaymentConsentsPOSTResponse.setData(oBWriteDataInternationalConsentResponse1);
		customIPaymentConsentsPOSTResponse.setRisk(oBRisk1);
		return customIPaymentConsentsPOSTResponse;
	}

	public PaymentInstructionProposalAuthorisingParty transformInternationalPaymentRequest(
			OBWriteInternationalConsentResponse1 requestObj) {
		PaymentInstructionProposalAuthorisingParty paymentInstructionProposalAuthorisingParty = new PaymentInstructionProposalAuthorisingParty();		
		if (requestObj.getData() != null && requestObj.getData().getInitiation() != null
				&& requestObj.getData().getInitiation().getDebtorAccount() != null) {
			OBCashAccountDebtor3 debtorAccount = requestObj.getData().getInitiation().getDebtorAccount();
			AccountInformation accountInformation = new AccountInformation();
			accountInformation.setSchemeName(debtorAccount.getSchemeName());
			accountInformation.setAccountName(debtorAccount.getName());
			accountInformation.setSecondaryIdentification(debtorAccount.getSecondaryIdentification());
			accountInformation.setAccountIdentification(debtorAccount.getIdentification());
			paymentInstructionProposalAuthorisingParty.setAuthorisingPartyAccount(accountInformation);
		}
		return paymentInstructionProposalAuthorisingParty;
	}
}