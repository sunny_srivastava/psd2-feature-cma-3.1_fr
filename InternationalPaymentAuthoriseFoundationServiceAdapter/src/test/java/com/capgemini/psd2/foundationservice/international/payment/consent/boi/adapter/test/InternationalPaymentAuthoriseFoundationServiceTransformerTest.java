package com.capgemini.psd2.foundationservice.international.payment.consent.boi.adapter.test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.foundationservice.international.payment.authorise.boi.adapter.domain.AccountInformation;
import com.capgemini.psd2.foundationservice.international.payment.authorise.boi.adapter.domain.Address;
import com.capgemini.psd2.foundationservice.international.payment.authorise.boi.adapter.domain.Agent;
import com.capgemini.psd2.foundationservice.international.payment.authorise.boi.adapter.domain.Amount;
import com.capgemini.psd2.foundationservice.international.payment.authorise.boi.adapter.domain.AuthorisationType;
import com.capgemini.psd2.foundationservice.international.payment.authorise.boi.adapter.domain.Channel;
import com.capgemini.psd2.foundationservice.international.payment.authorise.boi.adapter.domain.ChargeBearer;
import com.capgemini.psd2.foundationservice.international.payment.authorise.boi.adapter.domain.Country;
import com.capgemini.psd2.foundationservice.international.payment.authorise.boi.adapter.domain.Currency;
import com.capgemini.psd2.foundationservice.international.payment.authorise.boi.adapter.domain.ExchangeRateQuote;
import com.capgemini.psd2.foundationservice.international.payment.authorise.boi.adapter.domain.FinancialEventAmount;
import com.capgemini.psd2.foundationservice.international.payment.authorise.boi.adapter.domain.PartyBasicInformation;
import com.capgemini.psd2.foundationservice.international.payment.authorise.boi.adapter.domain.PaymentInstructionCharge;
import com.capgemini.psd2.foundationservice.international.payment.authorise.boi.adapter.domain.PaymentInstructionPostalAddress;
import com.capgemini.psd2.foundationservice.international.payment.authorise.boi.adapter.domain.PaymentInstructionProposalAuthorisingParty;
import com.capgemini.psd2.foundationservice.international.payment.authorise.boi.adapter.domain.PaymentInstructionProposalInternational;
import com.capgemini.psd2.foundationservice.international.payment.authorise.boi.adapter.domain.PaymentInstrumentRiskFactor;
import com.capgemini.psd2.foundationservice.international.payment.authorise.boi.adapter.domain.PriorityCode;
import com.capgemini.psd2.foundationservice.international.payment.authorise.boi.adapter.domain.ProposalStatus;
import com.capgemini.psd2.foundationservice.international.payment.authorise.boi.adapter.domain.ProposingPartyAccount;
import com.capgemini.psd2.foundationservice.international.payment.authorise.boi.adapter.domain.Purpose;
import com.capgemini.psd2.foundationservice.international.payment.authorise.boi.adapter.domain.RateQuoteType;
import com.capgemini.psd2.foundationservice.international.payment.authorise.boi.adapter.transformer.InternationalPaymentAuthoriseFoundationServiceTransformer;
import com.capgemini.psd2.pisp.domain.CustomIPaymentConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.OBCashAccountDebtor3;
import com.capgemini.psd2.pisp.domain.OBInternational1;
import com.capgemini.psd2.pisp.domain.OBRisk1;
import com.capgemini.psd2.pisp.domain.OBWriteDataInternationalConsentResponse1;
import com.capgemini.psd2.pisp.domain.OBWriteInternationalConsentResponse1;
import com.capgemini.psd2.pisp.domain.PaymentSetupPOSTResponseLinks;
import com.capgemini.psd2.pisp.domain.PaymentSetupPOSTResponseMeta;

@RunWith(SpringJUnit4ClassRunner.class)
public class InternationalPaymentAuthoriseFoundationServiceTransformerTest {

	@InjectMocks
	private InternationalPaymentAuthoriseFoundationServiceTransformer transformer;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testTransformInternationalPaymentResponse() {

		PaymentInstructionProposalInternational paymentInstructionProposalInternational = new PaymentInstructionProposalInternational();
		paymentInstructionProposalInternational.setPaymentInstructionProposalId("58923");
		paymentInstructionProposalInternational.setProposalStatus(ProposalStatus.REJECTED);
		paymentInstructionProposalInternational.setProposalCreationDatetime("2018-10-05T15:15:13+00:00");
		paymentInstructionProposalInternational.setProposalStatusUpdateDatetime("2018-10-05T15:15:37+00:00");
		paymentInstructionProposalInternational.setInstructionReference("ACME412");
		paymentInstructionProposalInternational.setInstructionEndToEndReference("FRESCO.21302.GFX.20");
		paymentInstructionProposalInternational.setInstructionLocalInstrument("null");
		paymentInstructionProposalInternational.setPriorityCode(PriorityCode.NORMAL);
		paymentInstructionProposalInternational.setAuthorisingPartyUnstructuredReference("Payer unstructured reference");
		Purpose purpose = new Purpose();
		purpose.setPurposeCode("Payment");
		purpose.setProprietaryPurpose("Payment");
		paymentInstructionProposalInternational.setPurpose(purpose);

		Channel channel = new Channel();
		channel.setBrandCode(null);
		channel.setChannelCode(null);
		channel.setChannelName("BOL");
		paymentInstructionProposalInternational.setChannel(channel);

		Currency currency = new Currency();
		currency.setCurrencyName("EUR");
		currency.setIsoAlphaCode("AlphaCode");
		paymentInstructionProposalInternational.setTransactionCurrency(currency);

		paymentInstructionProposalInternational.setCurrencyOfTransfer(currency);

		PaymentInstructionCharge paymentInstructionCharge = new PaymentInstructionCharge();

		Amount amount = new Amount();
		amount.setAccountCurrency(Double.valueOf(500.0));
		amount.setGroupReportingCurrency(Double.valueOf(1500.0));
		amount.setLocalReportingCurrency(Double.valueOf(3500.0));
		amount.setTransactionCurrency(Double.valueOf(4500.0));
		paymentInstructionCharge.setAmount(amount);

		paymentInstructionCharge.setChargeBearer(ChargeBearer.SHARED);
		paymentInstructionCharge.setCurrency(currency);
		paymentInstructionCharge.setType("EUR");
		paymentInstructionProposalInternational.setCharges(Arrays.asList(paymentInstructionCharge));

		FinancialEventAmount financialEventAmount = new FinancialEventAmount();
		financialEventAmount.setAccountCurrency(Double.valueOf(500.0));
		financialEventAmount.setGroupReportingCurrency(Double.valueOf(1000.0));
		financialEventAmount.setLocalReportingCurrency(Double.valueOf(1500.0));
		financialEventAmount.setTransactionCurrency(Double.valueOf(2000.0));
		paymentInstructionProposalInternational.setFinancialEventAmount(financialEventAmount);

		ExchangeRateQuote exchangeRateQuote = new ExchangeRateQuote();
		exchangeRateQuote.setRateQuoteType(RateQuoteType.AGREED);
		exchangeRateQuote.setExchangeRate(Double.valueOf("75"));
		exchangeRateQuote.setUnitCurrency(currency);
		paymentInstructionProposalInternational.setExchangeRateQuote(exchangeRateQuote);

		AccountInformation accountInformation = new AccountInformation();
		accountInformation.setSchemeName("UK.OBIE.IBAN");
		accountInformation.setAccountName("John Debitor");
		accountInformation.setSecondaryIdentification("00003400004443");
		accountInformation.setAccountIdentification("BE71096123456769");
		paymentInstructionProposalInternational.setAuthorisingPartyAccount(accountInformation);

		Country country = new Country();
		country.setCountryName("UK");
		country.setIsoCountryAlphaTwoCode("IsoAlphaCode");
		
		PaymentInstructionPostalAddress paymentInstructionPostalAddress = new PaymentInstructionPostalAddress();
		paymentInstructionPostalAddress.setAddressType("Business");
		paymentInstructionPostalAddress.setDepartment("Donegal");
		paymentInstructionPostalAddress.setGeoCodeBuildingName("Ulster");
		paymentInstructionPostalAddress.setGeoCodeBuildingNumber("54");
		paymentInstructionPostalAddress.setPostCodeNumber("1023");
		paymentInstructionPostalAddress.setSubDepartment("Donegal");
		paymentInstructionPostalAddress.setTownName("Letterkenny");
		paymentInstructionPostalAddress.setCountrySubDivision("Donegal");
		paymentInstructionPostalAddress.setAddressCountry(country);
		paymentInstructionPostalAddress.setAddressLine(Arrays.asList("UK","addrLine2","addrLine3"));		
		paymentInstructionProposalInternational.setProposingPartyPostalAddress(paymentInstructionPostalAddress);
		
		Agent agent = new Agent();
		agent.setAgentIdentification("agent identification");
		agent.setAgentName("Accounts International");
		agent.setSchemeName("scheme name");
		agent.setPartyAddress(paymentInstructionPostalAddress);
		paymentInstructionProposalInternational.setProposingPartyAgent(agent);

		paymentInstructionProposalInternational.setAuthorisingPartyReference("FRESCO-101");
		paymentInstructionProposalInternational.setAuthorisationType(AuthorisationType.SINGLE);
		paymentInstructionProposalInternational.setAuthorisationDatetime("2018-10-05T15:15:13+00:00");

		PaymentInstrumentRiskFactor paymentInstrumentRiskFactor = new PaymentInstrumentRiskFactor();

		Address address = new Address();		

		address.setAddressCountry(country);
		address.setFifthAddressLine("addrLine5");
		address.setFirstAddressLine("addrLine1");
		address.setFourthAddressLine("addrLine4");
		address.setGeoCodeBuildingName("Ulster");
		address.setGeoCodeBuildingNumber("54");
		address.setPostCodeNumber("1023");
		address.setSecondAddressLine("secondAddressLine");
		address.setThirdAddressLine("addrLine3");
		paymentInstrumentRiskFactor.setCounterPartyAddress(address);
		paymentInstrumentRiskFactor.setMerchantCategoryCode("596");
		paymentInstrumentRiskFactor.setMerchantCustomerIdentification("10000274544442");
		paymentInstrumentRiskFactor.setPaymentContextCode("EcommerceGoods");
		
		PartyBasicInformation PartyBasicInformation = new PartyBasicInformation();
		PartyBasicInformation.setPartyName("tpp");
		PartyBasicInformation.setPartySourceIdNumber("BE71096123456769");
		paymentInstructionProposalInternational.setProposingParty(PartyBasicInformation);
		ProposingPartyAccount proposingPartyAccount = new ProposingPartyAccount();
		proposingPartyAccount.setAccountIdentification("BE71096123456769");
		proposingPartyAccount.setAccountName("saving");
		proposingPartyAccount.setAccountNumber("10000274544442");
		proposingPartyAccount.setPayeeCountry(country);
		proposingPartyAccount.setSchemeName("UK.OBIE.IBAN");
		proposingPartyAccount.setSecondaryIdentification("00003400004443");
		paymentInstructionProposalInternational.setProposingPartyAccount(proposingPartyAccount);	
		
		paymentInstructionProposalInternational.setPaymentInstructionRiskFactorReference(paymentInstrumentRiskFactor);
		CustomIPaymentConsentsPOSTResponse transformInternationalPaymentResponse = transformer
				.transformInternationalPaymentResponse(paymentInstructionProposalInternational);
		assertNotNull(transformInternationalPaymentResponse);

	}

	@Test
	public void testTransformInternationalPaymentRequest() {
		OBWriteInternationalConsentResponse1 obWriteInternationalConsentResponse1 = new OBWriteInternationalConsentResponse1();
		OBWriteDataInternationalConsentResponse1 obWriteDataInternationalConsentResponse1 = new OBWriteDataInternationalConsentResponse1();
		OBInternational1 obInternational1 = new OBInternational1();
		OBCashAccountDebtor3 obCashAccountDebtor3 = new OBCashAccountDebtor3();
		obWriteDataInternationalConsentResponse1.setConsentId("ofdfjghdfjg123450000gfggfhg");
		obWriteInternationalConsentResponse1.setData(obWriteDataInternationalConsentResponse1);
		OBRisk1 obRisk1 = new OBRisk1();
		obRisk1.setMerchantCategoryCode("code23455");
		obWriteInternationalConsentResponse1.setRisk(obRisk1);
		PaymentSetupPOSTResponseLinks paymentSetupPOSTResponseLinks = new PaymentSetupPOSTResponseLinks();
		paymentSetupPOSTResponseLinks.setFirst("first");
		obWriteInternationalConsentResponse1.setLinks(paymentSetupPOSTResponseLinks);
		PaymentSetupPOSTResponseMeta paymentSetupPOSTResponseMeta = new PaymentSetupPOSTResponseMeta();
		paymentSetupPOSTResponseMeta.setTotalPages(10);
		obWriteInternationalConsentResponse1.setMeta(paymentSetupPOSTResponseMeta);
		obCashAccountDebtor3.setName("John Debitor");
		obCashAccountDebtor3.setSecondaryIdentification("00003400004443");
		obCashAccountDebtor3.setIdentification("BE71096123456769");
		obInternational1.setDebtorAccount(obCashAccountDebtor3);
		obWriteDataInternationalConsentResponse1.setInitiation(obInternational1);
		obWriteInternationalConsentResponse1.setData(obWriteDataInternationalConsentResponse1);
		obCashAccountDebtor3.setSchemeName("UK.OBIE.IBAN");
		PaymentInstructionProposalAuthorisingParty transformInternationalPaymentRequest = transformer
				.transformInternationalPaymentRequest(obWriteInternationalConsentResponse1);
		assertNotNull(transformInternationalPaymentRequest);
	}
	
	@Test
	public void testTransformInternationalPaymentRequestNullCheck() {
		OBWriteInternationalConsentResponse1 obWriteInternationalConsentResponse1 = new OBWriteInternationalConsentResponse1();				
		obWriteInternationalConsentResponse1.setData(null);	
		transformer.transformInternationalPaymentRequest(obWriteInternationalConsentResponse1);
		assertNull(obWriteInternationalConsentResponse1.getData());
	}
	
	
	@Test
	public void testTransformInternationalPaymentResponseNullCheck() {

		PaymentInstructionProposalInternational paymentInstructionProposalInternational = new PaymentInstructionProposalInternational();
		paymentInstructionProposalInternational.setPaymentInstructionProposalId("58497");
		paymentInstructionProposalInternational.setProposalStatus(ProposalStatus.AWAITINGAUTHORISATION);
		paymentInstructionProposalInternational.setProposalCreationDatetime(null);
		paymentInstructionProposalInternational.setProposalStatusUpdateDatetime(null);
		paymentInstructionProposalInternational.setInstructionReference(null);
		paymentInstructionProposalInternational.setInstructionEndToEndReference(null);
		paymentInstructionProposalInternational.setInstructionLocalInstrument(null);
		paymentInstructionProposalInternational.setPriorityCode(PriorityCode.NORMAL);
		paymentInstructionProposalInternational.setAuthorisingPartyUnstructuredReference(null);
		Purpose purpose = new Purpose();
		purpose.setPurposeCode(null);
		purpose.setProprietaryPurpose(null);
		paymentInstructionProposalInternational.setPurpose(purpose);

		Channel channel = new Channel();
		channel.setBrandCode(null);
		channel.setChannelCode(null);
		channel.setChannelName("BOL");
		paymentInstructionProposalInternational.setChannel(channel);

		Currency currency = new Currency();
		currency.setCurrencyName("EUR");
		currency.setIsoAlphaCode("AlphaCode");
		paymentInstructionProposalInternational.setTransactionCurrency(currency);

		paymentInstructionProposalInternational.setCurrencyOfTransfer(currency);

		PaymentInstructionCharge paymentInstructionCharge = new PaymentInstructionCharge();

		Amount amount = new Amount();
		amount.setAccountCurrency(Double.valueOf(500.0));
		amount.setGroupReportingCurrency(Double.valueOf(1500.0));
		amount.setLocalReportingCurrency(Double.valueOf(3500.0));
		amount.setTransactionCurrency(Double.valueOf(4500.0));
		paymentInstructionCharge.setAmount(amount);

		paymentInstructionCharge.setChargeBearer(ChargeBearer.SHARED);
		paymentInstructionCharge.setCurrency(currency);
		paymentInstructionCharge.setType("EUR");
		paymentInstructionProposalInternational.setCharges(Arrays.asList(paymentInstructionCharge));

		FinancialEventAmount financialEventAmount = new FinancialEventAmount();
		financialEventAmount.setAccountCurrency(Double.valueOf(500.0));
		financialEventAmount.setGroupReportingCurrency(Double.valueOf(1000.0));
		financialEventAmount.setLocalReportingCurrency(Double.valueOf(1500.0));
		financialEventAmount.setTransactionCurrency(Double.valueOf(2000.0));
		paymentInstructionProposalInternational.setFinancialEventAmount(financialEventAmount);

		ExchangeRateQuote exchangeRateQuote = new ExchangeRateQuote();
		exchangeRateQuote.setRateQuoteType(RateQuoteType.AGREED);
		exchangeRateQuote.setExchangeRate(Double.valueOf("75"));
		exchangeRateQuote.setUnitCurrency(currency);
		paymentInstructionProposalInternational.setExchangeRateQuote(exchangeRateQuote);

		AccountInformation accountInformation = new AccountInformation();
		accountInformation.setSchemeName(null);
		accountInformation.setAccountName(null);
		accountInformation.setSecondaryIdentification(null);
		accountInformation.setAccountIdentification(null);
		paymentInstructionProposalInternational.setAuthorisingPartyAccount(accountInformation);

		Country country = new Country();
		country.setCountryName("UK");
		country.setIsoCountryAlphaTwoCode("IsoAlphaCode");
		
		PaymentInstructionPostalAddress paymentInstructionPostalAddress = new PaymentInstructionPostalAddress();
		paymentInstructionPostalAddress.setAddressType(null);
		paymentInstructionPostalAddress.setDepartment(null);
		paymentInstructionPostalAddress.setGeoCodeBuildingName(null);
		paymentInstructionPostalAddress.setGeoCodeBuildingNumber(null);
		paymentInstructionPostalAddress.setPostCodeNumber(null);
		paymentInstructionPostalAddress.setSubDepartment(null);
		paymentInstructionPostalAddress.setTownName(null);
		paymentInstructionPostalAddress.setCountrySubDivision(null);
		paymentInstructionPostalAddress.setAddressCountry(country);
		paymentInstructionPostalAddress.setAddressLine(null);		
		paymentInstructionProposalInternational.setProposingPartyPostalAddress(paymentInstructionPostalAddress);
		
		Agent agent = new Agent();
		agent.setAgentIdentification("agent identification");
		agent.setAgentName("Accounts International");
		agent.setSchemeName("scheme name");
		agent.setPartyAddress(paymentInstructionPostalAddress);
		paymentInstructionProposalInternational.setProposingPartyAgent(agent);

		paymentInstructionProposalInternational.setAuthorisingPartyReference("FRESCO-101");
		paymentInstructionProposalInternational.setAuthorisationType(AuthorisationType.SINGLE);
		paymentInstructionProposalInternational.setAuthorisationDatetime("2018-10-05T15:15:13+00:00");

		PaymentInstrumentRiskFactor paymentInstrumentRiskFactor = new PaymentInstrumentRiskFactor();

		Address address = new Address();		

		address.setAddressCountry(country);
		address.setFifthAddressLine(null);
		address.setFirstAddressLine(null);
		address.setFourthAddressLine(null);
		address.setGeoCodeBuildingName(null);
		address.setGeoCodeBuildingNumber(null);
		address.setPostCodeNumber(null);
		address.setSecondAddressLine(null);
		address.setThirdAddressLine(null);
		paymentInstrumentRiskFactor.setCounterPartyAddress(address);
		paymentInstrumentRiskFactor.setMerchantCategoryCode("596");
		paymentInstrumentRiskFactor.setMerchantCustomerIdentification("10000274544442");
		paymentInstrumentRiskFactor.setPaymentContextCode("EcommerceGoods");
		
		PartyBasicInformation PartyBasicInformation = new PartyBasicInformation();
		PartyBasicInformation.setPartyName("tpp");
		PartyBasicInformation.setPartySourceIdNumber("BE71096123456769");
		paymentInstructionProposalInternational.setProposingParty(PartyBasicInformation);
		ProposingPartyAccount proposingPartyAccount = new ProposingPartyAccount();
		proposingPartyAccount.setAccountIdentification("BE71096123456769");
		proposingPartyAccount.setAccountName("saving");
		proposingPartyAccount.setAccountNumber("10000274544442");
		proposingPartyAccount.setPayeeCountry(country);
		proposingPartyAccount.setSchemeName("UK.OBIE.IBAN");
		proposingPartyAccount.setSecondaryIdentification("00003400004443");
		paymentInstructionProposalInternational.setProposingPartyAccount(proposingPartyAccount);	
		
		paymentInstructionProposalInternational.setPaymentInstructionRiskFactorReference(paymentInstrumentRiskFactor);
		CustomIPaymentConsentsPOSTResponse transformInternationalPaymentResponse = transformer
				.transformInternationalPaymentResponse(paymentInstructionProposalInternational);
		assertNotNull(transformInternationalPaymentResponse);

	}

}
