package com.capgemini.psd2.foundationservice.international.payment.consent.boi.adapter.test;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.anyObject;

import java.lang.reflect.Method;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.RestTemplate;

import com.capgemini.psd2.foundationservice.international.payment.authorise.boi.adapter.client.InternationalPaymentAuthoriseFoundationServiceClient;
import com.capgemini.psd2.foundationservice.international.payment.authorise.boi.adapter.domain.AccountInformation;
import com.capgemini.psd2.foundationservice.international.payment.authorise.boi.adapter.domain.PaymentInstructionProposalAuthorisingParty;
import com.capgemini.psd2.foundationservice.international.payment.authorise.boi.adapter.domain.PaymentInstructionProposalInternational;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.RestClientSync;

@RunWith(SpringJUnit4ClassRunner.class)
public class InternationalPaymentAuthoriseFoundationServiceClientTest {
	
	@InjectMocks
	InternationalPaymentAuthoriseFoundationServiceClient internationalPaymentAuthoriseFoundationServiceClient ; 
		
	@Mock
	private RestClientSync restClient;

	@Mock
	private RestTemplate restTemplate;

	
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		Method postConstruct = InternationalPaymentAuthoriseFoundationServiceClient.class.getDeclaredMethod("init", null);
		postConstruct.setAccessible(true);
		postConstruct.invoke(internationalPaymentAuthoriseFoundationServiceClient);
	}
	

	@Test
	public void testRestTransportForInternationalPaymentFoundationServicePost() {
		RequestInfo requestInfo = new RequestInfo();
		String url="http://localhost:9051/group-payments/p/payments-service/v1.1/international"
				+ "/payment-instruction-proposals/58923/authorise";
		requestInfo.setUrl(url);
		
		PaymentInstructionProposalAuthorisingParty paymentInstructionProposalAuthorisingParty =
				new PaymentInstructionProposalAuthorisingParty();
		AccountInformation accountInformation = new AccountInformation();
		accountInformation.setSchemeName("UK.OBIE.IBAN");
		accountInformation.setAccountName("BE71096123456769");
		accountInformation.setSecondaryIdentification("John Debitor");
		accountInformation.setAccountIdentification("00003400004443");
		paymentInstructionProposalAuthorisingParty.setAuthorisingPartyAccount(accountInformation);
		
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.add("X-TRANSACTION-ID","PISP");
		httpHeaders.add("X-CORRELATION-ID","BOL");
		httpHeaders.add("X-SOURCE-SYSTEM","BOI999");
		httpHeaders.add("X-SOURCE-USER","platform");
		httpHeaders.add("X-CHANNEL-CODE","87654321");
		httpHeaders.add("X-BOI-CHANNEL","BOIUK");
		httpHeaders.add("tenant_id","BOIUK");
		httpHeaders.add("X-PARTY-SOURCE-ID-NUMBER","BOIUK");
		httpHeaders.add("X-BOI-USER","BOIUK");
		
		PaymentInstructionProposalInternational paymentInstructionProposalInternational = new PaymentInstructionProposalInternational();
				internationalPaymentAuthoriseFoundationServiceClient.restTransportForInternationalPaymentFoundationServicePost
		(requestInfo, paymentInstructionProposalAuthorisingParty, PaymentInstructionProposalInternational.class, httpHeaders);
		System.out.println(" *******************************************************"+paymentInstructionProposalInternational);
		assertNotNull(paymentInstructionProposalInternational);
		RequestHeaderAttributes requestHeaderAttributes = new RequestHeaderAttributes();
		requestHeaderAttributes.setChannelId("B365");
		requestHeaderAttributes.setCorrelationId("00003400004443");
		requestHeaderAttributes.setIdempotencyKey("fCjV9Trt3JxdGYQah5seGZFQ80VD");
		requestHeaderAttributes.setIntentId("0015800000jfQ9aAAE");
		requestHeaderAttributes.setPsuId("87e7417a-cde8-4ac6-bdfa-7061f222cefa");
		
		assertNotNull(requestHeaderAttributes);
		requestInfo.setRequestHeaderAttributes(requestHeaderAttributes);
		restClient.callForPost(requestInfo, paymentInstructionProposalAuthorisingParty, PaymentInstructionProposalInternational.class, httpHeaders);		
		Mockito.when(restClient.callForGet(anyObject(), anyObject(), anyObject())).thenReturn(paymentInstructionProposalInternational);
		
	}
	
	
}
