package com.capgemini.psd2.foundationservice.international.payment.consent.boi.adapter.test;

import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import com.capgemini.psd2.foundationservice.international.payment.authorise.boi.adapter.InternationalPaymentAuthoriseFoundationServiceAdapter;
import com.capgemini.psd2.foundationservice.international.payment.authorise.boi.adapter.client.InternationalPaymentAuthoriseFoundationServiceClient;
import com.capgemini.psd2.foundationservice.international.payment.authorise.boi.adapter.delegate.InternationalPaymentAuthoriseFoundationServiceDelegate;
import com.capgemini.psd2.foundationservice.international.payment.authorise.boi.adapter.domain.AccountInformation;
import com.capgemini.psd2.foundationservice.international.payment.authorise.boi.adapter.domain.PaymentInstructionProposalAuthorisingParty;
import com.capgemini.psd2.foundationservice.international.payment.authorise.boi.adapter.domain.PaymentInstructionProposalInternational;
import com.capgemini.psd2.foundationservice.international.payment.authorise.boi.adapter.transformer.InternationalPaymentAuthoriseFoundationServiceTransformer;
import com.capgemini.psd2.pisp.domain.CustomIPaymentConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.OBCashAccountDebtor3;
import com.capgemini.psd2.pisp.domain.OBRisk1;
import com.capgemini.psd2.pisp.domain.OBWriteDataInternationalConsentResponse1;
import com.capgemini.psd2.pisp.domain.OBWriteInternationalConsentResponse1;
import com.capgemini.psd2.pisp.domain.PaymentSetupPOSTResponseLinks;
import com.capgemini.psd2.pisp.domain.PaymentSetupPOSTResponseMeta;
import com.capgemini.psd2.pisp.enums.PaymentTypeEnum;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.rest.client.model.RequestInfo;

@RunWith(SpringJUnit4ClassRunner.class)
public class InternationalPaymentAuthoriseFoundationServiceAdapterTest {
	
	@InjectMocks
	InternationalPaymentAuthoriseFoundationServiceAdapter internationalPaymentAuthoriseFoundationServiceAdapter;
	
	@Mock
	InternationalPaymentAuthoriseFoundationServiceDelegate internationalPaymentAuthoriseFoundationServiceDelegate;
	
	@Mock
	InternationalPaymentAuthoriseFoundationServiceClient internationalPaymentAuthoriseFoundationServiceClient;
	
	@Mock
	InternationalPaymentAuthoriseFoundationServiceTransformer internationalPaymentAuthoriseFoundationServiceTransformer;
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void testCreateStagingInternationalPaymentConsents() {
		OBWriteInternationalConsentResponse1 requestObj = new OBWriteInternationalConsentResponse1();
		
		OBWriteDataInternationalConsentResponse1 obWriteDataInternationalConsentResponse1 = new OBWriteDataInternationalConsentResponse1();	
		obWriteDataInternationalConsentResponse1.setConsentId("qrewerqw");
		requestObj.setData(obWriteDataInternationalConsentResponse1);
		OBRisk1 obRisk1 = new OBRisk1();
		obRisk1.setMerchantCategoryCode("code23455");
		requestObj.setRisk(obRisk1);
		PaymentSetupPOSTResponseLinks paymentSetupPOSTResponseLinks  = new PaymentSetupPOSTResponseLinks();	
		paymentSetupPOSTResponseLinks.setFirst("ffrsfr");
		requestObj.setLinks(paymentSetupPOSTResponseLinks);
		PaymentSetupPOSTResponseMeta paymentSetupPOSTResponseMeta = new PaymentSetupPOSTResponseMeta();
		paymentSetupPOSTResponseMeta.setTotalPages(10);
		requestObj.setMeta(paymentSetupPOSTResponseMeta);
		OBCashAccountDebtor3 debtorAccount = new OBCashAccountDebtor3();
		AccountInformation accountInformation = new AccountInformation();
		accountInformation.setSchemeName("UK.OBIE.IBAN");
		accountInformation.setAccountName("BE71096123456769");
		accountInformation.setSecondaryIdentification("John Debitor");
		accountInformation.setAccountIdentification("00003400004443");
				
		//requestObj.getData().set
		
		CustomPaymentStageIdentifiers customPaymentStageIdentifiers = new CustomPaymentStageIdentifiers();
		customPaymentStageIdentifiers.setPaymentConsentId("58923");
		customPaymentStageIdentifiers.setPaymentSetupVersion("3.0");
		customPaymentStageIdentifiers.setPaymentTypeEnum(PaymentTypeEnum.INTERNATIONAL_PAY);
		
		Map<String, String> params = new HashMap<String, String>();
		params.put("X-TRANSACTION-ID","PISP");
		params.put("X-CORRELATION-ID","BOL");
		params.put("X-SOURCE-SYSTEM","BOI999");
		params.put("X-SOURCE-USER","platform");
		params.put("X-CHANNEL-CODE","87654321");
		params.put("X-BOI-CHANNEL","BOIUK");
		params.put("tenant_id","BOIUK");
		params.put("X-PARTY-SOURCE-ID-NUMBER","BOIUK");
		params.put("X-BOI-USER","BOIUK");
		
		RequestInfo requestInfo = new RequestInfo();
		HttpHeaders httpHeaders = new HttpHeaders();
		
		internationalPaymentAuthoriseFoundationServiceAdapter.
		createStagingInternationalPaymentConsents(requestObj, customPaymentStageIdentifiers, params);
		
		Mockito.when(internationalPaymentAuthoriseFoundationServiceDelegate.createPaymentRequestHeadersPost(anyObject(),anyObject()))
		.thenReturn(httpHeaders);
		
		Mockito.when(internationalPaymentAuthoriseFoundationServiceDelegate.internationalPaymentAuthoriseConsentPostBaseURL(anyString()))
		.thenReturn(null);
			
		requestInfo.setUrl("\"http://localhost:9051/group-payments/p/payments-service/v1.1/international\"\r\n" + 
				"				+ \"/payment-instruction-proposals/0fca37a5-40fa-41fe-91c7-9e48453d9580/authorise\"");
		
		PaymentInstructionProposalAuthorisingParty paymentInstructionProposalAuthorisingParty =
				new PaymentInstructionProposalAuthorisingParty();
		paymentInstructionProposalAuthorisingParty.setAuthorisingPartyAccount(accountInformation);
		Mockito.when(internationalPaymentAuthoriseFoundationServiceTransformer.transformInternationalPaymentRequest(anyObject())).
		thenReturn(paymentInstructionProposalAuthorisingParty);
		
		PaymentInstructionProposalInternational paymentInstructionProposalResponse = new PaymentInstructionProposalInternational();
		Mockito.when(internationalPaymentAuthoriseFoundationServiceClient.restTransportForInternationalPaymentFoundationServicePost(anyObject(),
				anyObject(),anyObject(),anyObject())).thenReturn(paymentInstructionProposalResponse);
				
		CustomIPaymentConsentsPOSTResponse customIPaymentConsentsPOSTResponse = new CustomIPaymentConsentsPOSTResponse();
		Mockito.when(internationalPaymentAuthoriseFoundationServiceTransformer.transformInternationalPaymentResponse(anyObject()))
		.thenReturn(customIPaymentConsentsPOSTResponse);
		
	}
	
	
}

