package com.capgemini.psd2.foundationservice.international.payment.consent.boi.adapter.test;

import static org.junit.Assert.assertNotNull;
import java.util.HashMap;
import java.util.Map;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpHeaders;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.foundationservice.international.payment.authorise.boi.adapter.delegate.InternationalPaymentAuthoriseFoundationServiceDelegate;
import com.capgemini.psd2.pisp.enums.PaymentTypeEnum;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;

public class InternationalPaymentAuthoriseFoundationServiceDelegateTest {
	
	@InjectMocks
	private InternationalPaymentAuthoriseFoundationServiceDelegate delegate;
	

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void testCreatePaymentRequestHeadersPost() {
		CustomPaymentStageIdentifiers customPaymentStageIdentifiers = new CustomPaymentStageIdentifiers();
		customPaymentStageIdentifiers.setPaymentConsentId("58923");
		customPaymentStageIdentifiers.setPaymentSetupVersion("3.0");
		customPaymentStageIdentifiers.setPaymentTypeEnum(PaymentTypeEnum.INTERNATIONAL_PAY);
		customPaymentStageIdentifiers.setPaymentSubmissionId("submissionId");
		
		Map<String, String> params = new HashMap<String, String>();
		params.put("X-API-TRANSACTION-ID","PISP");
		params.put("X-CORRELATION-ID","BOL");
		params.put("X-API-SOURCE-SYSTEM","BOI999");
		params.put("X-API-SOURCE-USER","platform");
		params.put("X-BOI-CHANNEL","87654321");
		params.put("X-API-CHANNEL-BRAND","BOIUK");
		params.put("X-SYSTEM-API-VERSION","3.0");
		params.put("X-PARTY-SOURCE-ID-NUMBER","BOIUK");
		params.put("X-BOI-USER","BOIUK");
		params.put("tenant_id","BOIROI");
		HttpHeaders httpHeaders = new HttpHeaders();
		ReflectionTestUtils.setField(delegate, "sourceSystemHeader", "X-API-SOURCE-SYSTEM");
		ReflectionTestUtils.setField(delegate, "sourceUserHeader", "X-API-SOURCE-USER");
		ReflectionTestUtils.setField(delegate, "transactionHeader", "X-API-TRANSACTION-ID");
		ReflectionTestUtils.setField(delegate, "apiChannelCodeHeader", "X-BOI-CHANNEL");
		ReflectionTestUtils.setField(delegate, "systemApiVersionHeader", "X-SYSTEM-API-VERSION");
		ReflectionTestUtils.setField(delegate, "apiChannelBrandHeader", "X-API-CHANNEL-BRAND");
		
		httpHeaders = delegate.createPaymentRequestHeadersPost(customPaymentStageIdentifiers, params);
		assertNotNull(httpHeaders);		
	}
	
	@Test
	public void testInternationalPaymentAuthoriseConsentPostBaseURL() {
		CustomPaymentStageIdentifiers customPaymentStageIdentifiers = new CustomPaymentStageIdentifiers();
		customPaymentStageIdentifiers.setPaymentConsentId("58923");
		customPaymentStageIdentifiers.setPaymentSetupVersion("3.0");
		customPaymentStageIdentifiers.setPaymentTypeEnum(PaymentTypeEnum.INTERNATIONAL_PAY);
		String internationalPaymentAuthoriseConsentPostBaseURL = delegate.internationalPaymentAuthoriseConsentPostBaseURL(customPaymentStageIdentifiers.getPaymentConsentId());
		assertNotNull(internationalPaymentAuthoriseConsentPostBaseURL);
	}
	
	@Test()
	public void testCreatePaymentRequestHeadersPost1() {
		
		CustomPaymentStageIdentifiers customPaymentStageIdentifiers = new CustomPaymentStageIdentifiers();
		customPaymentStageIdentifiers.setPaymentConsentId("58923");
		customPaymentStageIdentifiers.setPaymentSetupVersion("3.0");
		customPaymentStageIdentifiers.setPaymentTypeEnum(PaymentTypeEnum.INTERNATIONAL_PAY);
		customPaymentStageIdentifiers.setPaymentSubmissionId("submissionId");
		
		Map<String, String> params = new HashMap<String, String>();
		params.put("X-API-TRANSACTION-ID","PISP");
		params.put("X-CORRELATION-ID","BOL");
		params.put("X-API-SOURCE-SYSTEM","BOI999");
		params.put("X-API-SOURCE-USER","platform");
		params.put("X-API-CHANNEL-CODE","87654321");
		params.put("X-API-CHANNEL-BRAND","BOIUK");
		params.put("X-SYSTEM-API-VERSION","3.0");
		params.put("X-PARTY-SOURCE-ID-NUMBER","BOIUK");
		params.put("X-BOI-USER","BOIUK");
		params.put("tenant_id","BOI-NIGB");
		
		HttpHeaders httpHeaders = new HttpHeaders();
		ReflectionTestUtils.setField(delegate, "sourceSystemHeader", "X-API-SOURCE-SYSTEM");
		ReflectionTestUtils.setField(delegate, "sourceUserHeader", "X-API-SOURCE-USER");
		ReflectionTestUtils.setField(delegate, "transactionHeader", "X-API-TRANSACTION-ID");
		ReflectionTestUtils.setField(delegate, "apiChannelCodeHeader", "X-API-CHANNEL-CODE");
		ReflectionTestUtils.setField(delegate, "systemApiVersionHeader", "X-SYSTEM-API-VERSION");
		ReflectionTestUtils.setField(delegate, "apiChannelBrandHeader", "X-API-CHANNEL-BRAND");
		delegate.createPaymentRequestHeadersPost(customPaymentStageIdentifiers, params);
	}
	
	
}
