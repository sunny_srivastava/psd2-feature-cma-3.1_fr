package com.capgemini.psd2.international.payments.authorisingparty.mock.foundationservice.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.capgemini.psd2.international.payments.authorisingparty.mock.foundationservice.domain.PaymentInstructionProposalInternational;


public interface InternationalPaymentAuthorisingPartyRepository extends MongoRepository<PaymentInstructionProposalInternational, String> {
	/**
	 * Find by paymentInstructionProposalId
	 * @return the PaymentInstructionProposalInternational
	 */	
	public PaymentInstructionProposalInternational findByPaymentInstructionProposalId (String paymentInstructionProposalId);

	//public PaymentInstructionProposalInternational findByPaymentPaymentId(String paymentInstructionProposalId);	
	
	
}
