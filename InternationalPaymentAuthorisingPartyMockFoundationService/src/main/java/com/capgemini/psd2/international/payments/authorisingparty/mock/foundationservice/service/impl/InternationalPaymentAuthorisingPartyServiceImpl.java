package com.capgemini.psd2.international.payments.authorisingparty.mock.foundationservice.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.capgemini.psd2.international.payments.authorisingparty.mock.foundationservice.domain.AccountInformation;
import com.capgemini.psd2.international.payments.authorisingparty.mock.foundationservice.domain.PaymentInstructionProposalAuthorisingParty;
import com.capgemini.psd2.international.payments.authorisingparty.mock.foundationservice.domain.PaymentInstructionProposalInternational;
import com.capgemini.psd2.international.payments.authorisingparty.mock.foundationservice.exception.handler.RecordNotFoundException;
import com.capgemini.psd2.international.payments.authorisingparty.mock.foundationservice.repository.InternationalPaymentAuthorisingPartyRepository;
import com.capgemini.psd2.international.payments.authorisingparty.mock.foundationservice.service.InternationalPaymentAuthorisingPartyService;

@Service
public class InternationalPaymentAuthorisingPartyServiceImpl implements InternationalPaymentAuthorisingPartyService {
	
	
	@Autowired
	InternationalPaymentAuthorisingPartyRepository internationalPaymentAuthorisingPartyRepository;

	@Override
	public PaymentInstructionProposalInternational updateAuthorisingParty(String paymentInstructionProposalId,PaymentInstructionProposalAuthorisingParty paymentInstructionProposalAuthorisingPartyRequest)
			throws RecordNotFoundException {
		// TODO Auto-generated method stub
		PaymentInstructionProposalInternational paymentInstructionProposalInternational = new PaymentInstructionProposalInternational();
		AccountInformation accountInformation = new AccountInformation();
		paymentInstructionProposalInternational = internationalPaymentAuthorisingPartyRepository.findByPaymentInstructionProposalId(paymentInstructionProposalId);
		if (paymentInstructionProposalAuthorisingPartyRequest!=null) {
			if(paymentInstructionProposalAuthorisingPartyRequest.getAuthorisingPartyAccount()!=null) {
				
				accountInformation.setSchemeName(paymentInstructionProposalAuthorisingPartyRequest.getAuthorisingPartyAccount().getSchemeName());
				accountInformation.setAccountIdentification(paymentInstructionProposalAuthorisingPartyRequest.getAuthorisingPartyAccount().getAccountIdentification());
				
				if(paymentInstructionProposalAuthorisingPartyRequest.getAuthorisingPartyAccount().getAccountName()!=null) {
					accountInformation.setAccountName(paymentInstructionProposalAuthorisingPartyRequest.getAuthorisingPartyAccount().getAccountName());				
				}
				
				if(paymentInstructionProposalAuthorisingPartyRequest.getAuthorisingPartyAccount().getSecondaryIdentification()!=null) {
					accountInformation.setSecondaryIdentification(paymentInstructionProposalAuthorisingPartyRequest.getAuthorisingPartyAccount().getSecondaryIdentification());
				}
			}
		}
		
		paymentInstructionProposalInternational.setAuthorisingPartyAccount(accountInformation);
		internationalPaymentAuthorisingPartyRepository.save(paymentInstructionProposalInternational);
		return paymentInstructionProposalInternational;
	}
}
