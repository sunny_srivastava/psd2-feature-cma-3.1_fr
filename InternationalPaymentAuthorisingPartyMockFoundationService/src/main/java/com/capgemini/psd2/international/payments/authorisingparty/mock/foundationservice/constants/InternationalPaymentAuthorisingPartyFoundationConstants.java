package com.capgemini.psd2.international.payments.authorisingparty.mock.foundationservice.constants;

public class InternationalPaymentAuthorisingPartyFoundationConstants {
	public static final String RECORD_NOT_FOUND = "Payment Instruction Proposal Record Not Found";
	public static final String INSTRUCTION_ENDTOEND_REF="Rejected";
}
