package com.capgemini.psd2.international.payments.authorisingparty.mock.foundationservice.service;

import com.capgemini.psd2.international.payments.authorisingparty.mock.foundationservice.domain.PaymentInstructionProposalAuthorisingParty;
import com.capgemini.psd2.international.payments.authorisingparty.mock.foundationservice.domain.PaymentInstructionProposalInternational;
import com.capgemini.psd2.international.payments.authorisingparty.mock.foundationservice.exception.handler.RecordNotFoundException;

public interface InternationalPaymentAuthorisingPartyService {
	public PaymentInstructionProposalInternational updateAuthorisingParty(
			String paymentInstructionProposalId,PaymentInstructionProposalAuthorisingParty paymentInstructionProposalAuthorisingParty) throws RecordNotFoundException;
}
