package com.capgemini.psd2.international.payments.authorisingparty.mock.foundationservice.exception.handler;

/**
 * The Class MissingAuthenticationHeaderException.
 */
public class MissingAuthenticationHeaderException extends Exception{

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Instantiates a new missing authentication header exception.
	 *
	 * @param m the m
	 */
	public MissingAuthenticationHeaderException(String m){
		super(m);
	}
	
}
