/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/

package com.capgemini.psd2.foundationservice.account.standingorder.boi.adapter.transformer;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import com.capgemini.psd2.adapter.datetime.utility.TimeZoneDateTimeAdapter;
import com.capgemini.psd2.aisp.domain.OBActiveOrHistoricCurrencyAndAmount3;
import com.capgemini.psd2.aisp.domain.OBCashAccount51;
import com.capgemini.psd2.aisp.domain.OBReadStandingOrder6;
import com.capgemini.psd2.aisp.domain.OBReadStandingOrder6Data;
import com.capgemini.psd2.aisp.domain.OBStandingOrder6;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountStandingOrdersResponse;
import com.capgemini.psd2.aisp.transformer.AccountStandingOrdersTransformer;

import com.capgemini.psd2.foundationservice.account.standingorder.boi.adapter.constants.AccountStandingOrderFoundationServiceConstants;
import com.capgemini.psd2.foundationservice.account.standingorder.boi.adapter.raml.domain.StandingOrderFrequency;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Component
@ConfigurationProperties(prefix = "foundationService")
@Configuration
@EnableAutoConfiguration
public class AccountStandingOrderFoundationServiceTransformer implements AccountStandingOrdersTransformer {

	@Autowired
	private TimeZoneDateTimeAdapter timeZoneDateTimeAdapter;

	@Override
	public <T> PlatformAccountStandingOrdersResponse transformAccountStandingOrders(T source,
			Map<String, String> params) {

		PlatformAccountStandingOrdersResponse platformAccountStandingOrdersResponse = new PlatformAccountStandingOrdersResponse();
		OBReadStandingOrder6 oBReadStandingOrder2 = new OBReadStandingOrder6();
		OBReadStandingOrder6Data oBReadStandingOrder2Data = new OBReadStandingOrder6Data();
		List<OBStandingOrder6> standingOrdersList = new ArrayList<>();

		oBReadStandingOrder2Data.setStandingOrder(standingOrdersList);
		oBReadStandingOrder2.setData(oBReadStandingOrder2Data);
		platformAccountStandingOrdersResponse.setoBReadStandingOrder6(oBReadStandingOrder2);

		com.capgemini.psd2.foundationservice.account.standingorder.boi.adapter.raml.domain.StandingOrderScheduleInstructionsresponse standingOrderScheduleInstructionsresponse = (com.capgemini.psd2.foundationservice.account.standingorder.boi.adapter.raml.domain.StandingOrderScheduleInstructionsresponse) source;
		for (com.capgemini.psd2.foundationservice.account.standingorder.boi.adapter.raml.domain.StandingOrderScheduleInstruction standingOrderScheduleInstruction : standingOrderScheduleInstructionsresponse
				.getStandingOrdersList()) {
			OBStandingOrder6 oBStandingOrder2 = new OBStandingOrder6();

			OBActiveOrHistoricCurrencyAndAmount3 oBStandingOrder2NextPaymentAmount = new OBActiveOrHistoricCurrencyAndAmount3();
			OBCashAccount51 oBScheduledPayment1CreditorAccount = new OBCashAccount51();
			oBScheduledPayment1CreditorAccount.setSchemeName("UK.OBIE.SortCodeAccountNumber");
			oBScheduledPayment1CreditorAccount.setIdentification((standingOrderScheduleInstruction
					.getPaymentInstructionToCounterPartyBasic().get(0).getCounterpartyNationalSortCodeNSCNumber()
					+ standingOrderScheduleInstruction.getPaymentInstructionToCounterPartyBasic().get(0)
							.getCounterpartyAccountNumber()));
			oBStandingOrder2.setNextPaymentAmount(oBStandingOrder2NextPaymentAmount);
			oBStandingOrder2.setCreditorAccount(oBScheduledPayment1CreditorAccount);

			/** AccountId **/
			oBStandingOrder2.setAccountId(params.get(AccountStandingOrderFoundationServiceConstants.ACCOUNT_ID));

			/** StandingOrderId **/
			if (!NullCheckUtils.isNullOrEmpty(
					standingOrderScheduleInstruction.getPaymentInstruction().getPaymentInstructionNumber())) {
				oBStandingOrder2.setStandingOrderId(
						standingOrderScheduleInstruction.getPaymentInstruction().getPaymentInstructionNumber());
			}

			/** Reference **/
			if (!NullCheckUtils.isNullOrEmpty(standingOrderScheduleInstruction
					.getPaymentInstructionToCounterPartyBasic().get(0).getInstructingPartyNarrativeText())) {
				oBStandingOrder2.setReference(standingOrderScheduleInstruction
						.getPaymentInstructionToCounterPartyBasic().get(0).getInstructingPartyNarrativeText());
			}
			
			/** StandingOrderStatusCode **/
			if (!NullCheckUtils.isNullOrEmpty(
					standingOrderScheduleInstruction.getPaymentInstruction().getPaymentInstructionStatusCode())) {
			if (AccountStandingOrderFoundationServiceConstants.ACTIVE.equalsIgnoreCase(standingOrderScheduleInstruction
					.getPaymentInstruction().getPaymentInstructionStatusCode().toString())) {
				oBStandingOrder2.setStandingOrderStatusCode(
						com.capgemini.psd2.aisp.domain.OBExternalStandingOrderStatus1Code.ACTIVE);
			} else if (AccountStandingOrderFoundationServiceConstants.INACTIVE
					.equalsIgnoreCase(standingOrderScheduleInstruction.getPaymentInstruction()
							.getPaymentInstructionStatusCode().toString())) {
				oBStandingOrder2.setStandingOrderStatusCode(
						com.capgemini.psd2.aisp.domain.OBExternalStandingOrderStatus1Code.INACTIVE);
			}
			}
			else
				oBStandingOrder2.setStandingOrderStatusCode(
						com.capgemini.psd2.aisp.domain.OBExternalStandingOrderStatus1Code.ACTIVE);
				 

			/** Amount **/
			if (!NullCheckUtils.isNullOrEmpty(standingOrderScheduleInstruction.getStandingOrderAmount())) {
				String standingOrderScheduleInstructionAmount = null;
				standingOrderScheduleInstructionAmount = BigDecimal.valueOf(Double.valueOf(standingOrderScheduleInstruction.getStandingOrderAmount().toString())).toPlainString();
				 if (!standingOrderScheduleInstructionAmount.contains("."))
					 standingOrderScheduleInstructionAmount = standingOrderScheduleInstructionAmount + ".0";
			
				oBStandingOrder2NextPaymentAmount
						.setAmount(standingOrderScheduleInstructionAmount);
			}
			/** Frequency **/
			oBStandingOrder2.setFrequency(getFrequency(standingOrderScheduleInstruction.getStandingOrderFrequency(),
					standingOrderScheduleInstruction.getSchedule().getScheduleNextDate()));

			/** Currency **/
			if (!NullCheckUtils.isNullOrEmpty(standingOrderScheduleInstruction.getTransactionCurrency())) {
				oBStandingOrder2NextPaymentAmount
						.setCurrency(standingOrderScheduleInstruction.getTransactionCurrency().getIsoAlphaCode());
			}

			/** Name **/
			oBScheduledPayment1CreditorAccount.setName(standingOrderScheduleInstruction
					.getPaymentInstructionToCounterPartyBasic().get(0).getCounterpartyName());
			
			/** NextPaymentDateTime **/
			if (!NullCheckUtils.isNullOrEmpty(standingOrderScheduleInstruction.getSchedule().getScheduleNextDate())) {
				oBStandingOrder2.setNextPaymentDateTime(timeZoneDateTimeAdapter
						.parseDateCMA((standingOrderScheduleInstruction.getSchedule().getScheduleNextDate())));
				
			}

			/** Identification **/
			oBScheduledPayment1CreditorAccount.setIdentification(standingOrderScheduleInstruction
					.getPaymentInstructionToCounterPartyBasic().get(0).getCounterpartyNationalSortCodeNSCNumber()
					+ standingOrderScheduleInstruction.getPaymentInstructionToCounterPartyBasic().get(0)
							.getCounterpartyAccountNumber());

			standingOrdersList.add(oBStandingOrder2);

		}

		return platformAccountStandingOrdersResponse;
	}

	public String getFrequency(StandingOrderFrequency frequencyFS, Date date) {
		String frequency = "";
		int dayofweek = 0;
		int dayOfMonth = 0;
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		if (frequencyFS.toString().contains(AccountStandingOrderFoundationServiceConstants.MONTHLY)
				|| frequencyFS.toString().contains(AccountStandingOrderFoundationServiceConstants.YEARLY)) {
			dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
		} else if (frequencyFS.toString().contains(AccountStandingOrderFoundationServiceConstants.WEEKLY)
				|| frequencyFS.toString().contains(AccountStandingOrderFoundationServiceConstants.FORTNIGHTLY)) {
			Instant instant = date.toInstant();
			ZoneId defaultZoneId = ZoneId.systemDefault();
			LocalDate localDate = instant.atZone(defaultZoneId).toLocalDate();
			dayofweek = localDate.getDayOfWeek().getValue();
		}
		switch (frequencyFS) {
		case WEEKLY:
			frequency = "IntrvlWkDay:01:0" + dayofweek;
			break;
		case FORTNIGHTLY:
			frequency = "IntrvlWkDay:02:0" + dayofweek;
			break;
		case MONTHLY:
			if (dayOfMonth >=1 && dayOfMonth <= 9)
				frequency = "IntrvlMnthDay:01:0" + dayOfMonth;
			else if(dayOfMonth >=10 && dayOfMonth <= 31)
				frequency = "IntrvlMnthDay:01:" + dayOfMonth;
			break;

		case YEARLY:
			if (dayOfMonth >=1 && dayOfMonth <= 9)
				frequency = "IntrvlMnthDay:12:0" + dayOfMonth;
			else if(dayOfMonth >=10 && dayOfMonth <= 31)
				frequency = "IntrvlMnthDay:12:" + dayOfMonth;
			break;
		default:
			frequency = "EvryDay";
			break;
		}
		return frequency;
	}
}
