/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/

package com.capgemini.psd2.foundationservice.account.standingorder.boi.adapter.test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.foundationservice.account.standingorder.boi.adapter.delegate.AccountStandingOrderFoundationServiceDelegate;
import com.capgemini.psd2.foundationservice.account.standingorder.boi.adapter.raml.domain.Currency;
import com.capgemini.psd2.foundationservice.account.standingorder.boi.adapter.raml.domain.PaymentInstructionBasic;
import com.capgemini.psd2.foundationservice.account.standingorder.boi.adapter.raml.domain.PaymentInstructionCounterpartyAccountBasic;
import com.capgemini.psd2.foundationservice.account.standingorder.boi.adapter.raml.domain.Schedule;
import com.capgemini.psd2.foundationservice.account.standingorder.boi.adapter.raml.domain.StandingOrderFrequency;
import com.capgemini.psd2.foundationservice.account.standingorder.boi.adapter.raml.domain.StandingOrderScheduleInstruction;
import com.capgemini.psd2.foundationservice.account.standingorder.boi.adapter.raml.domain.StandingOrderScheduleInstructionsresponse;
import com.capgemini.psd2.foundationservice.account.standingorder.boi.adapter.transformer.AccountStandingOrderFoundationServiceTransformer;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.impl.RestClientSyncImpl;
import com.capgemini.psd2.validator.PSD2Validator;



@RunWith(SpringJUnit4ClassRunner.class)
public class AccountStandingOrdersFoundationServiceDelegateRestCallTest {
	
	/** The account information FS transformer. */
	@Mock
	private AccountStandingOrderFoundationServiceTransformer accountStandingOrderFoundationServiceTransformer;
	
	@InjectMocks
	private AccountStandingOrderFoundationServiceDelegate delegate;
	
	@Mock
	private PSD2Validator psd2Validator;
	
	@Mock
	private RestClientSyncImpl restClient;
	
	@Before
	public void setUp(){
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void contextLoads() {
	}
	
	@Test
	public void testTransformResponseFromFDToAPI() {
		
		Map<String, String> params = new HashMap<>();
		params.put("accountId", "12345");
		
		StandingOrderScheduleInstructionsresponse standingOrderScheduleInstructionsresponse = new StandingOrderScheduleInstructionsresponse();
		List<StandingOrderScheduleInstruction> standingOrdersList = new ArrayList<StandingOrderScheduleInstruction>();
		StandingOrderScheduleInstruction standingOrderScheduleInstruction = new StandingOrderScheduleInstruction();
		
		PaymentInstructionBasic PaymentInstructionBasic = new PaymentInstructionBasic();
		PaymentInstructionBasic.setPaymentInstructionNumber("12345");
		
		standingOrderScheduleInstruction.setPaymentInstruction(PaymentInstructionBasic);
		
		List<PaymentInstructionCounterpartyAccountBasic> paymentInstructionToCounterPartyBasic1 = new ArrayList<PaymentInstructionCounterpartyAccountBasic>();
		PaymentInstructionCounterpartyAccountBasic PaymentInstructionCounterpartyAccountBasic = new PaymentInstructionCounterpartyAccountBasic();
		PaymentInstructionCounterpartyAccountBasic.setCounterpartyAccountNumber("123");
		PaymentInstructionCounterpartyAccountBasic.setCounterpartyName("abc");
		PaymentInstructionCounterpartyAccountBasic.setCounterpartyNationalSortCodeNSCNumber("345");
		PaymentInstructionCounterpartyAccountBasic.setInstructingPartyNarrativeText("rfd");
		
		paymentInstructionToCounterPartyBasic1.add(PaymentInstructionCounterpartyAccountBasic);
		
		standingOrderScheduleInstruction.setPaymentInstructionToCounterPartyBasic(paymentInstructionToCounterPartyBasic1);
		
		Schedule schedule = new Schedule();
		schedule.setScheduleNextDate(new Date());
		
		standingOrderScheduleInstruction.setSchedule(schedule);
		
		standingOrderScheduleInstruction.setStandingOrderAmount(123d);
		
		StandingOrderFrequency standingOrderFrequency = StandingOrderFrequency.MONTHLY;
		
		standingOrderScheduleInstruction.setStandingOrderFrequency(standingOrderFrequency);
		
		Currency transactionCurrency = new Currency();
		transactionCurrency.setIsoAlphaCode("cdf");
		
		standingOrderScheduleInstruction.setTransactionCurrency(transactionCurrency);
		
		standingOrdersList.add(standingOrderScheduleInstruction);
		
		standingOrderScheduleInstructionsresponse.setStandingOrdersList(standingOrdersList);
		
		AccountMapping accountMapping = new AccountMapping();
		List<AccountDetails> accDetList = new ArrayList<AccountDetails>();
		AccountDetails accDet = new AccountDetails();
		accDet.setAccountId("12345");
		accDet.setAccountNSC("nsc1234");
		accDet.setAccountNumber("acct1234");
		accDetList.add(accDet);
		accountMapping.setAccountDetails(accDetList);
		accountMapping.setTppCID("test");
		accountMapping.setPsuId("test");
		accountMapping.setCorrelationId("test");
		
		delegate.transformResponseFromFDToAPI(standingOrderScheduleInstructionsresponse, params);
		
	}
	
	@Test
	public void testGetFoundationServiceURL(){
		
		
		String finalURL = "http://localhost:9086/fs-abt-service/services/account/123/accounts/123456/12345/standing-order-schedule-instructions";
		String response = delegate.getFoundationServiceURL("http://localhost:9086/fs-abt-service/services/account", "123","123456", "12345", "standing-order-schedule-instructions");
		assertThat(response).isEqualTo(finalURL);
	}
	
		
	@Test(expected = AdapterException.class)
	public void testGetFoundationServiceURLNSCNull(){
	delegate.getFoundationServiceURL("http://localhost:9086/fs-abt-service/services/account","123", null, "123456", "standing-order-schedule-instructions")	;
	}
	@Test(expected = AdapterException.class)
	public void testGetFoundationServiceURLAccountNumberNull(){
	delegate.getFoundationServiceURL("http://localhost:9086/fs-abt-service/services/account","123", "123456", null, "standingorders")	;
	}
	
	@Test
	public void testCreateRequestHeaders(){
		ReflectionTestUtils.setField(delegate, "userInReqHeadeRaml", "x-api-source-user");
		ReflectionTestUtils.setField(delegate, "channelInReqHeader", "X-BOI-CHANNEL");
		ReflectionTestUtils.setField(delegate, "platformInReqHeaderRaml", "x-api-source-system");
		ReflectionTestUtils.setField(delegate, "transactionReqHeaderRaml", "x-api-transaction-id");
		ReflectionTestUtils.setField(delegate, "correlationReqHeaderRaml", "x-api-correlation-id");
		ReflectionTestUtils.setField(delegate, "platform", "PSD2API");
		AccountMapping accountMapping = new AccountMapping();
		accountMapping.setPsuId("12345678");
		accountMapping.setCorrelationId("123");
		Map<String, String> params = new HashMap<>();
		params.put(PSD2Constants.CHANNEL_NAME, "Channel");
		params.put(PSD2Constants.PLATFORM_IN_REQ_HEADER, "PSD2API");
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.add("x-api-source-user", "12345678");
		httpHeaders.add("X-BOI-CHANNEL", "Channel");
		httpHeaders.add("x-api-source-system", "PSD2API");
		httpHeaders.add("x-api-correlation-id", "");
		httpHeaders.add("x-api-transaction-id","123");
		HttpHeaders response = delegate.createRequestHeaders(new RequestInfo(), accountMapping, params);
		//assertThat(response).isEqualTo(httpHeaders);
		assertNotNull(response);
	}
}
