package com.capgemini.psd2.pisp.payment.setup.mongo.db.adapter.impl;

import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Conditional;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.conditions.MongoDbMockCondition;
import com.capgemini.psd2.exceptions.ErrorMapKeys;
import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.InternalServerErrorMessage;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.domain.CustomISPConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomISPConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.GenericPaymentConsentResponse;
import com.capgemini.psd2.pisp.domain.OBCharge1;
import com.capgemini.psd2.pisp.domain.OBChargeBearerType1Code;
import com.capgemini.psd2.pisp.domain.OBExchangeRate2;
import com.capgemini.psd2.pisp.domain.OBExternalConsentStatus1Code;
import com.capgemini.psd2.pisp.enums.ProcessConsentStatusEnum;
import com.capgemini.psd2.pisp.payment.setup.mongo.db.adapter.repository.ISPConsentsFoundationRepository;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.pisp.stage.operations.adapter.InternationalScheduledPaymentStagingAdapter;
import com.capgemini.psd2.pisp.utilities.PispUtilities;
import com.capgemini.psd2.utilities.JSONUtilities;
import com.capgemini.psd2.utilities.SandboxValidationUtility;

@Conditional(MongoDbMockCondition.class)
@Component("iScheduledPaymentConsentsStagingMongoDbAdapter")
public class ISPConsentsStagingMongoDbAdapterImpl implements InternationalScheduledPaymentStagingAdapter {

	private static Logger LOG = LoggerFactory.getLogger(ISPConsentsStagingMongoDbAdapterImpl.class);
	
	@Autowired
	private ISPConsentsFoundationRepository iScheduledPaymentConsentsBankRepository;

	@Autowired
	private SandboxValidationUtility utility;

	@Autowired
	private RequestHeaderAttributes reqAttributes;

	@Value("${app.sandboxValidationPolicies.contractIdentificationPattern:^Test[a-zA-Z0-9]*}")
	private String contractIdentificationPattern;

	@Override
	public GenericPaymentConsentResponse processInternationalScheduledPaymentConsents(
			CustomISPConsentsPOSTRequest customRequest, CustomPaymentStageIdentifiers customStageIdentifiers,
			Map<String, String> params, OBExternalConsentStatus1Code successStatus,
			OBExternalConsentStatus1Code failureStatus) {

		String initiationAmount = customRequest.getData().getInitiation().getInstructedAmount().getAmount();

		if (utility.isValidAmount(initiationAmount, PSD2Constants.SANDBOX_PISP_POST_MOCKING)) {
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
					InternalServerErrorMessage.SANDBOX_MOCKED_ERROR));
		}

		// Sandbox Secondary Identification Validation
		String secondaryIdentification = customRequest.getData().getInitiation().getCreditorAccount()
				.getSecondaryIdentification();
		if (utility.isValidSecIdentification(secondaryIdentification)) {
			throw PSD2Exception.populatePSD2Exception(
					new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNSUPPORTED_ACCOUNTSECONDARYIDENTIFIER,
							ErrorMapKeys.ACCOUNT_SECONDARYIDENTIFIER_INVALID));
		}

		GenericPaymentConsentResponse genericPaymentsResponse = populateMockedResponse(customRequest);
		populateAndSaveDbResource(customRequest, genericPaymentsResponse, successStatus, failureStatus);

		return genericPaymentsResponse;
	}

	@Override
	public CustomISPConsentsPOSTResponse retrieveStagedInternationalScheduledPaymentConsents(
			CustomPaymentStageIdentifiers customPaymentStageIdentifiers, Map<String, String> params) {
		try {
			String paymentConsentId = customPaymentStageIdentifiers.getPaymentConsentId();
			CustomISPConsentsPOSTResponse paymentBankResource;

			paymentBankResource = iScheduledPaymentConsentsBankRepository.findOneByDataConsentId(paymentConsentId);

			if (paymentBankResource == null || paymentBankResource.getData() == null
					|| paymentBankResource.getData().getInitiation() == null)
				throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
						InternalServerErrorMessage.PISP_NO_PAYMENT_SETUP_RESOURCES_FOUND));

			if ("GET".equals(reqAttributes.getMethodType())) {
				String initiationAmount = paymentBankResource.getData().getInitiation().getInstructedAmount()
						.getAmount();
				if (utility.isValidAmount(initiationAmount, PSD2Constants.SANDBOX_PISP_GET_MOCKING)) {
					throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
							InternalServerErrorMessage.SANDBOX_MOCKED_ERROR));
				}
			}
			paymentBankResource.setId(null);

			return paymentBankResource;

		} catch (DataAccessResourceFailureException exception) {
			LOG.error("DataAccessResourceFailureException in retrieveStagedInternationalScheduledPaymentConsents: "+exception);
			throw PSD2Exception.populatePSD2Exception(
					new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR, exception.getMessage()));
		}
	}

	private GenericPaymentConsentResponse populateMockedResponse(CustomISPConsentsPOSTRequest customRequest) {

		GenericPaymentConsentResponse genericPaymentConsentsResponse = new GenericPaymentConsentResponse();

		String currency = customRequest.getData().getInitiation().getInstructedAmount().getCurrency();
		String initiationAmount = customRequest.getData().getInitiation().getInstructedAmount().getAmount();
		OBChargeBearerType1Code reqChargeBearer = customRequest.getData().getInitiation().getChargeBearer();

		String consentId = null;
		String expectedExecutionDateTime = utility.getMockedExpectedExecDtTm();
		String expectedSettlementDateTime = utility.getMockedExpectedSettlementDtTm();
		String cutOffDateTime = utility.getMockedCutOffDtTm();

		List<OBCharge1> charges = utility.getMockedOBChargeList(currency, initiationAmount, reqChargeBearer);
		ProcessConsentStatusEnum processConsentStatusEnum = utility.getMockedConsentProcessExecStatus(currency,
				initiationAmount);
		OBExchangeRate2 exchangeRateInformation = utility.getMockedExchangeRateInformation(
				customRequest.getData().getInitiation().getExchangeRateInformation(), initiationAmount);

		/*
		 * Failure mocked condition for sandbox - Invalid Contract
		 * Identification and Exchange Rate
		 */
		if (null != customRequest.getData().getInitiation().getExchangeRateInformation()
				&& null != customRequest.getData().getInitiation().getExchangeRateInformation()
						.getContractIdentification()
				&& isRejectionContractIdentification(customRequest.getData().getInitiation()
						.getExchangeRateInformation().getContractIdentification())) {
			processConsentStatusEnum = ProcessConsentStatusEnum.FAIL;
		}

		if (processConsentStatusEnum == ProcessConsentStatusEnum.PASS
				|| processConsentStatusEnum == ProcessConsentStatusEnum.FAIL)
			consentId = UUID.randomUUID().toString();

		genericPaymentConsentsResponse.setConsentId(consentId);
		genericPaymentConsentsResponse.setCharges(charges);
		genericPaymentConsentsResponse.setExpectedExecutionDateTime(expectedExecutionDateTime);
		genericPaymentConsentsResponse.setExpectedSettlementDateTime(expectedSettlementDateTime);
		genericPaymentConsentsResponse.setCutOffDateTime(cutOffDateTime);
		genericPaymentConsentsResponse.setProcessConsentStatusEnum(processConsentStatusEnum);
		genericPaymentConsentsResponse.setExchangeRateInformation(exchangeRateInformation);

		return genericPaymentConsentsResponse;
	}

	private void populateAndSaveDbResource(CustomISPConsentsPOSTRequest customRequest,
			GenericPaymentConsentResponse genericPaymentsResponse, OBExternalConsentStatus1Code successStatus,
			OBExternalConsentStatus1Code failureStatus) {

		String request = JSONUtilities.getJSONOutPutFromObject(customRequest);
		CustomISPConsentsPOSTResponse paymentConsentsFoundationResponse = JSONUtilities
				.getObjectFromJSONString(PispUtilities.getObjectMapper(), request, CustomISPConsentsPOSTResponse.class);

		paymentConsentsFoundationResponse.getData().setCreationDateTime(customRequest.getCreatedOn());
		paymentConsentsFoundationResponse.getData().setConsentId(genericPaymentsResponse.getConsentId());
		paymentConsentsFoundationResponse.getData().setCharges(genericPaymentsResponse.getCharges());
		paymentConsentsFoundationResponse.getData().setCutOffDateTime(genericPaymentsResponse.getCutOffDateTime());
		paymentConsentsFoundationResponse.getData()
				.setExpectedExecutionDateTime(genericPaymentsResponse.getExpectedExecutionDateTime());
		paymentConsentsFoundationResponse.getData()
				.setExpectedSettlementDateTime(genericPaymentsResponse.getExpectedSettlementDateTime());
		paymentConsentsFoundationResponse
				.setConsentPorcessStatus(genericPaymentsResponse.getProcessConsentStatusEnum());
		paymentConsentsFoundationResponse.getData()
				.setExchangeRateInformation(genericPaymentsResponse.getExchangeRateInformation());

		if (genericPaymentsResponse.getConsentId() != null) {
			OBExternalConsentStatus1Code status = calculateCMAStatus(
					genericPaymentsResponse.getProcessConsentStatusEnum(), successStatus, failureStatus);
			paymentConsentsFoundationResponse.getData().setStatus(status);
			paymentConsentsFoundationResponse.getData().setStatusUpdateDateTime(customRequest.getCreatedOn());
		}

		try {
			iScheduledPaymentConsentsBankRepository.save(paymentConsentsFoundationResponse);
		} catch (DataAccessResourceFailureException exception) {
			LOG.error("DataAccessResourceFailureException in populateAndSaveDbResource: "+exception);
			throw PSD2Exception.populatePSD2Exception(
					new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR, exception.getMessage()));
		}
	}

	private OBExternalConsentStatus1Code calculateCMAStatus(ProcessConsentStatusEnum processConsentStatusEnum,
			OBExternalConsentStatus1Code successStatus, OBExternalConsentStatus1Code failureStatus) {
		return (processConsentStatusEnum == ProcessConsentStatusEnum.PASS) ? successStatus : failureStatus;
	}

	private boolean isRejectionContractIdentification(String contractIdentification) {
		if (contractIdentification.matches(contractIdentificationPattern)) {
			return true;
		}
		// Default Case
		return false;
	}
}
