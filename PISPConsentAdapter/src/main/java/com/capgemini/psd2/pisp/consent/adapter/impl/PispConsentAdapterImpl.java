/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.pisp.consent.adapter.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.consent.domain.PispConsent;
import com.capgemini.psd2.enums.ConsentStatusEnum;
import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.pisp.adapter.PispConsentAdapter;
import com.capgemini.psd2.pisp.consent.adapter.repository.PispConsentMongoRepository;
import com.capgemini.psd2.product.common.CompatibleVersionList;
import com.capgemini.psd2.utilities.GenerateUniqueIdUtilities;

/**
 * The Class AispConsentAdapterImpl.
 */
@Component
public class PispConsentAdapterImpl implements PispConsentAdapter {

	private static final Logger LOG = LoggerFactory.getLogger(PispConsentAdapterImpl.class);
	
	private String exception = "Exception Occurred: ";
	
	/** The aisp consent repository. */
	@Autowired
	private PispConsentMongoRepository pispConsentMongoRepository;

	@Autowired
	private MongoTemplate mongoTemplate;
	
	@Autowired
	private CompatibleVersionList compatibleVersionList;

	@Override
	public void createConsent(PispConsent pispConsent) {
		PispConsent consent = null;
		try {
			consent = retrieveConsentByPaymentId(pispConsent.getPaymentId());
			if (consent != null) {
				if (ConsentStatusEnum.AWAITINGAUTHORISATION.equals(consent.getStatus())) {
					consent.setStatus(ConsentStatusEnum.REVOKED);
					pispConsentMongoRepository.save(consent);
				} else if (ConsentStatusEnum.AUTHORISED.equals(consent.getStatus())) {
					throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.INTENT_ID_VALIDATION_ERROR);
				}
			}
			if (pispConsent.getAccountDetails() == null) {
				throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.NO_ACCOUNT_DETAILS_FOUND);
			}
			pispConsent.setConsentId(GenerateUniqueIdUtilities.generateRandomUniqueID());
			pispConsent.setStatus(ConsentStatusEnum.AWAITINGAUTHORISATION);
			pispConsentMongoRepository.save(pispConsent);
		} catch (DataAccessResourceFailureException e) {
			LOG.error(exception +e);
			throw PSD2Exception.populatePSD2Exception(e.getMessage(), ErrorCodeEnum.CONNECTION_ERROR);
		}
	}

	@Override
	public PispConsent retrieveConsentByPaymentId(String paymentRequestId) {
		PispConsent consent = null;
		try {
			consent = pispConsentMongoRepository.findByPaymentIdAndCmaVersionIn(paymentRequestId, compatibleVersionList.fetchVersionList());
		} catch (DataAccessResourceFailureException e) {
			LOG.error(exception +e);
			throw PSD2Exception.populatePSD2Exception(e.getMessage(), ErrorCodeEnum.CONNECTION_ERROR);
		}
		return consent;
	}

	@Override
	public PispConsent retrieveConsentByPaymentId(String paymentRequestId, ConsentStatusEnum status) {
		PispConsent consent = null;
		try {
			consent = pispConsentMongoRepository.findByPaymentIdAndStatusAndCmaVersionIn(paymentRequestId, status, compatibleVersionList.fetchVersionList());
		} catch (DataAccessResourceFailureException e) {
			LOG.error(exception +e);
			throw PSD2Exception.populatePSD2Exception(e.getMessage(), ErrorCodeEnum.CONNECTION_ERROR);
		}
		return consent;
	}

	@Override
	public void updateConsentStatus(String consentId, ConsentStatusEnum statusEnum) {
		try {
			Query query = new Query(Criteria.where("consentId").is(consentId));
			Update update = new Update();
			update.set("status", statusEnum);

			mongoTemplate.updateFirst(query, update, PispConsent.class);
		} catch (DataAccessResourceFailureException e) {
			LOG.error(exception +e);
			throw PSD2Exception.populatePSD2Exception(e.getMessage(), ErrorCodeEnum.CONNECTION_ERROR);
		}
	}

	@Override
	public PispConsent retrieveConsent(String consentId) {
		PispConsent consent = null;
		try {
			consent = pispConsentMongoRepository.findByConsentIdAndCmaVersionIn(consentId,compatibleVersionList.fetchVersionList());
		} catch (DataAccessResourceFailureException e) {
			LOG.error(exception +e);
			throw PSD2Exception.populatePSD2Exception(e.getMessage(), ErrorCodeEnum.CONNECTION_ERROR);
		}
		if (consent == null)
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.NO_CONSENT_FOUND);
		return consent;
	}

	@Override
	public List<PispConsent> retrieveConsentByPsuIdAndConsentStatus(String psuId, ConsentStatusEnum statusEnum) {

		List<PispConsent> consentList = null;
		try {
			if (null == statusEnum) {
				consentList = pispConsentMongoRepository.findByPsuIdAndCmaVersionIn(psuId, compatibleVersionList.fetchVersionList());
			} else {
				consentList = pispConsentMongoRepository.findByPsuIdAndStatusAndCmaVersionIn(psuId, statusEnum, compatibleVersionList.fetchVersionList());
			}

		} catch (DataAccessResourceFailureException e) {
			LOG.error(exception +e);
			throw PSD2Exception.populatePSD2Exception(e.getMessage(), ErrorCodeEnum.CONNECTION_ERROR);
		}

		return consentList;

	}

	@Override
	public List<PispConsent> retrieveConsentByPsuIdConsentStatusAndTenantId(String psuId, ConsentStatusEnum statusEnum,
			String tenantId) {
		LOG.info(psuId+"statusenum"+statusEnum+"tenantID"+tenantId+"version"+compatibleVersionList.fetchVersionList());
		List<PispConsent> consentList = null;
		try {
			if (null == statusEnum) {
				consentList = pispConsentMongoRepository.findByPsuIdAndTenantIdAndCmaVersionIn(psuId, tenantId, compatibleVersionList.fetchVersionList());
			} else {
				consentList = pispConsentMongoRepository.findByPsuIdAndStatusAndTenantIdAndCmaVersionIn(psuId, statusEnum, tenantId,compatibleVersionList.fetchVersionList());
			}
      } catch (DataAccessResourceFailureException e) {
    	  	LOG.error(exception +e);
    	  	throw PSD2Exception.populatePSD2Exception(e.getMessage(), ErrorCodeEnum.CONNECTION_ERROR);
      }

		return consentList;
	}
	@Override
	public void updateConsentStatusWithResponse(String consentId, ConsentStatusEnum statusEnum) {
		try {
			Query query = new Query(Criteria.where("consentId").is(consentId));
			Update update = new Update();
			update.set("status", statusEnum);

			mongoTemplate.updateFirst(query, update, PispConsent.class);

		} catch (DataAccessResourceFailureException e) {
			LOG.error(exception +e);
			throw PSD2Exception.populatePSD2Exception(e.getMessage(), ErrorCodeEnum.CONNECTION_ERROR);
		}
	}
}