/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.pisp.consent.adapter.test.mock.data;

import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.PispConsent;

public class PispConsentAdapterMockData {

	private static PispConsent mockConsent;

	public static PispConsent getConsentMockData() {
		mockConsent = new PispConsent();

		mockConsent.setTppCId("tpp123");
		mockConsent.setPsuId("user123");
		mockConsent.setConsentId("a1e590ad-73dc-453a-841e-2a2ef055e878");

		AccountDetails accountDetails = new AccountDetails();
		accountDetails.setAccountId("22289");
		accountDetails.setAccountNSC("SC802001");
		accountDetails.setAccountNumber("1022675");
	
		mockConsent.setAccountDetails(accountDetails);
		mockConsent.setPaymentId("123456");

		return mockConsent;
	}
	
	public static PispConsent getConsentMockDataWithOutAccountDetails() {
		mockConsent = new PispConsent();
		mockConsent.setTppCId("tpp123");
		mockConsent.setPsuId("user123");
		mockConsent.setConsentId("a1e590ad-73dc-453a-841e-2a2ef055e878");
		return mockConsent;
	}
}
