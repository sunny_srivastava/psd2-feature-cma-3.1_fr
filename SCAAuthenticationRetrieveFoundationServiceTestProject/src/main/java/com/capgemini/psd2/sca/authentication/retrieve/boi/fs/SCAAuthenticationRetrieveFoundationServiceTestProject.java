package com.capgemini.psd2.sca.authentication.retrieve.boi.fs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@SpringBootApplication
@ComponentScan("com.capgemini.psd2")
@EnableMongoRepositories("com.capgemini.psd2")
@EnableAspectJAutoProxy(proxyTargetClass = true)
public class SCAAuthenticationRetrieveFoundationServiceTestProject {

	public static void main(String[] args) {
		SpringApplication.run(SCAAuthenticationRetrieveFoundationServiceTestProject.class, args);

	}

}
