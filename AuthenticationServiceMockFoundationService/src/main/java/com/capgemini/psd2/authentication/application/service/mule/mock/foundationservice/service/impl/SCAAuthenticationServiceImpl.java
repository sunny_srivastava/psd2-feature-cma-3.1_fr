package com.capgemini.psd2.authentication.application.service.mule.mock.foundationservice.service.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.map.HashedMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.capgemini.psd2.authentication.application.service.mule.mock.foundationservice.domain.ChannelCode;
import com.capgemini.psd2.authentication.application.service.mule.mock.foundationservice.domain.CustomLoginResponse;
import com.capgemini.psd2.authentication.application.service.mule.mock.foundationservice.domain.DigitalUser;
import com.capgemini.psd2.authentication.application.service.mule.mock.foundationservice.domain.DigitalUser13;
import com.capgemini.psd2.authentication.application.service.mule.mock.foundationservice.domain.DigitalUserSession;
import com.capgemini.psd2.authentication.application.service.mule.mock.foundationservice.domain.Login;
import com.capgemini.psd2.authentication.application.service.mule.mock.foundationservice.domain.LoginResponse;
import com.capgemini.psd2.authentication.application.service.mule.mock.foundationservice.domain.PersonBasicInformation;
import com.capgemini.psd2.authentication.application.service.mule.mock.foundationservice.domain.SecureAccessKeyPositionValue;
import com.capgemini.psd2.authentication.application.service.mule.mock.foundationservice.repository.SCAAuthenticationPOSTRepository;
import com.capgemini.psd2.authentication.application.service.mule.mock.foundationservice.service.SCAAuthenticationService;
import com.capgemini.psd2.foundationservice.validator.ValidationUtility;

@Service
public class SCAAuthenticationServiceImpl implements SCAAuthenticationService {

	@Autowired
	private SCAAuthenticationPOSTRepository repo;
	@Autowired
	private ValidationUtility validationUtility;

	@Override
	public LoginResponse createAuthenticationDetails(Login login, Map<String, String> params) {

		LocalDateTime currDate = null;
		LoginResponse loginresponse = new LoginResponse();

		CustomLoginResponse custom = repo
				.findByDigitalUserIdentifier(params.get("digitalUserId"));

		if (custom == null) {
			DigitalUser digitaluser = new DigitalUser();
			digitaluser.setDigitalUserIdentifier(params.get("digitalUserId"));
			DigitalUserSession digitalusersession = new DigitalUserSession();
			digitalusersession.setSessionInitiationFailureIndicator(true);
			digitalusersession.setSessionInitiationFailureReasonCode("Invalid Credential");
			loginresponse.setDigitalUser(digitaluser);
			loginresponse.setDigitalUserSession(digitalusersession);
			return loginresponse;
			
		} else {
			if (login.getDigitalUser().getCustomerAuthenticationSession().get(0).getAuthenticationMethodCode()
					.getValue().equalsIgnoreCase("PIN")) {
				DigitalUser13 digitalUser13 = login.getDigitalUser();
				DigitalUser digitaluser = new DigitalUser();
				digitaluser.setAuthenticationProtocolCode(custom.getAuthenticationProtocolCode());
				digitaluser.setChannelCode(ChannelCode.fromValue(params.get("channelCode")));
				digitaluser.setDigitalUserIdentifier(params.get("digitalUserId"));
				//digitaluser.setPersonInformation(custom.getPersonInformation());
				digitaluser.setSecureKeyAttemptsRemainingCount(custom.getSecureKeyAttemptsRemainingCount());
				digitaluser.setDigitalUserLockedOutIndicator(custom.getDigitalUserLockedOutIndicator());
//				PersonBasicInformation personInformation = new PersonBasicInformation();
//				personInformation.setBirthDate(custom.getPersonInformation().getBirthDate());
//				personInformation.setFirstName(custom.getPersonInformation().getFirstName());
//				personInformation.setMothersMaidenName(custom.getPersonInformation().getMothersMaidenName());
//				personInformation.setSurname(custom.getPersonInformation().getSurname());
//				personInformation.setTitleCode(custom.getPersonInformation().getTitleCode());
//				digitaluser.setPersonInformation(personInformation);
				loginresponse.setDigitalUser(digitaluser);
				DigitalUserSession digitalusersession = new DigitalUserSession();

				if (MapFunction(custom.getCustomerAuthenticationSession().get(0).getSecureAccessKeyUsed(),
						login.getDigitalUser().getCustomerAuthenticationSession().get(0).getSecureAccessKeyUsed())) {
					digitalusersession.setSessionInitiationFailureIndicator(false);
					//digitalusersession.setSessionInitiationFailureReasonCode("Valid OTP");
					currDate = LocalDateTime.now();
					digitalusersession.setSessionStartDateTime(currDate.toString());
				} else {
					digitalusersession.setSessionInitiationFailureIndicator(true);
					digitalusersession.setSessionInitiationFailureReasonCode("Invalid Credential");
				}
//				currDate = LocalDateTime.now();
//				digitalusersession.setSessionStartDateTime(currDate.toString());
				loginresponse.setDigitalUserSession(digitalusersession);

			} else if (login.getDigitalUser().getCustomerAuthenticationSession().get(0).getAuthenticationMethodCode()
					.toString().equalsIgnoreCase("DATE_OF_BIRTH")) {

				DigitalUser13 digitalUser13 = login.getDigitalUser();
				DigitalUser digitaluser = new DigitalUser();
				digitaluser.setAuthenticationProtocolCode(custom.getAuthenticationProtocolCode());
				digitaluser.setChannelCode(ChannelCode.fromValue(params.get("channelCode")));
				digitaluser.setDigitalUserIdentifier(params.get("digitalUserId"));
			//	digitaluser.setPersonInformation(custom.getPersonInformation());
				digitaluser.setSecureKeyAttemptsRemainingCount(custom.getSecureKeyAttemptsRemainingCount());
				digitaluser.setDigitalUserLockedOutIndicator(custom.getDigitalUserLockedOutIndicator());
//				PersonBasicInformation personInformation = new PersonBasicInformation();
//				personInformation.setBirthDate(custom.getPersonInformation().getBirthDate());
//				personInformation.setFirstName(custom.getPersonInformation().getFirstName());
//				personInformation.setMothersMaidenName(custom.getPersonInformation().getMothersMaidenName());
//				personInformation.setSurname(custom.getPersonInformation().getSurname());
//				personInformation.setTitleCode(custom.getPersonInformation().getTitleCode());
				//digitaluser.setPersonInformation(personInformation);
				loginresponse.setDigitalUser(digitaluser);
				DigitalUserSession digitalusersession = new DigitalUserSession();

				
				
				
				/* Date of Birth Validation */
				String d1 = login.getDigitalUser().getCustomerAuthenticationSession().get(0).getSecureAccessKeyUsed()
						.get(0).getValue();
				Date d = null;
				try {
					d = new SimpleDateFormat("dd/MM/yyyy").parse(d1);
				} catch (ParseException e1) {

					e1.printStackTrace();
				}
				long dd = d.getTime();

				Date date1 = null;
				try {
					date1 = new SimpleDateFormat("dd/MM/yyyy").parse(custom.getPersonInformation().getBirthDate());
				} catch (ParseException e) {

					e.printStackTrace();
				}
				long cudate = date1.getTime();
				if (dd == cudate) {
					digitalusersession.setSessionInitiationFailureIndicator(false);
					//digitalusersession.setSessionInitiationFailureReasonCode("Valid OTP");
					currDate = LocalDateTime.now();
					digitalusersession.setSessionStartDateTime(currDate.toString());
				} else {
					digitalusersession.setSessionInitiationFailureIndicator(true);
					digitalusersession.setSessionInitiationFailureReasonCode("Invalid Credential");
				}
//				currDate = LocalDateTime.now();
//				digitalusersession.setSessionStartDateTime(currDate.toString());
				loginresponse.setDigitalUserSession(digitalusersession);
			}
			else if (login.getDigitalUser().getCustomerAuthenticationSession().get(0).getAuthenticationMethodCode()
					.toString().equalsIgnoreCase("PASSWORD")) {

				DigitalUser13 digitalUser13 = login.getDigitalUser();
				DigitalUser digitaluser = new DigitalUser();
				digitaluser.setAuthenticationProtocolCode(custom.getAuthenticationProtocolCode());
				digitaluser.setChannelCode(ChannelCode.fromValue(params.get("channelCode")));
				digitaluser.setDigitalUserIdentifier(params.get("digitalUserId"));				
//				digitaluser.setPersonInformation(custom.getPersonInformation());
				digitaluser.setSecureKeyAttemptsRemainingCount(custom.getSecureKeyAttemptsRemainingCount());
				digitaluser.setDigitalUserLockedOutIndicator(custom.getDigitalUserLockedOutIndicator());
//				PersonBasicInformation personInformation = new PersonBasicInformation();
//				personInformation.setBirthDate(custom.getPersonInformation().getBirthDate());
//				personInformation.setFirstName(custom.getPersonInformation().getFirstName());
//				personInformation.setMothersMaidenName(custom.getPersonInformation().getMothersMaidenName());
//				personInformation.setSurname(custom.getPersonInformation().getSurname());
//				personInformation.setTitleCode(custom.getPersonInformation().getTitleCode());
//				digitaluser.setPersonInformation(personInformation);
				loginresponse.setDigitalUser(digitaluser);
				DigitalUserSession digitalusersession = new DigitalUserSession();
				
				//BOL Password Validation
				
				if (custom.getCustomerAuthenticationSession().get(0).getSecureAccessKeyUsed().toString().equalsIgnoreCase(login.getDigitalUser().getCustomerAuthenticationSession().get(0).getSecureAccessKeyUsed().toString())){
					digitalusersession.setSessionInitiationFailureIndicator(false);
					
				} else {
					digitalusersession.setSessionInitiationFailureIndicator(true);
					digitalusersession.setSessionInitiationFailureReasonCode("Invalid Credential");
				}
				currDate = LocalDateTime.now();
				digitalusersession.setSessionStartDateTime(currDate.toString());
				loginresponse.setDigitalUserSession(digitalusersession);
			}
		}
		return loginresponse;
	}

	public Boolean MapFunction(List<SecureAccessKeyPositionValue> list1, List<SecureAccessKeyPositionValue> list2) {
		Map<Number, String> map1 = new HashedMap();
		Map<Number, String> map2 = new HashedMap();
		for (SecureAccessKeyPositionValue iterator1 : list1) {

			map1.put(iterator1.getPosition(), iterator1.getValue());
		}
		for (SecureAccessKeyPositionValue iterator2 : list2) {
			map2.put(iterator2.getPosition(), iterator2.getValue());
		}
		int flag = 0;
		for (Map.Entry<Number, String> iterator3 : map1.entrySet()) {
			for (Map.Entry<Number, String> iterator4 : map2.entrySet()) {
				if (iterator3.equals(iterator4)) {
					flag++;

				}

			}

		}
		if (flag == 3) {
			return true;
		}
		System.out.println(flag);

		return false;
	}

}