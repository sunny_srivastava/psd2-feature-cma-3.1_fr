/*
 * Authentication Service API
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v2.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package com.capgemini.psd2.authentication.application.service.mule.mock.foundationservice.domain;

import java.util.Objects;

import com.google.gson.annotations.SerializedName;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * This is structure of the login credenitals result that will be returned after authentication
 */
@ApiModel(description = "This is structure of the login credenitals result that will be returned after authentication")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2019-10-14T18:22:40.133+05:30")
public class LoginResponse {
  @SerializedName("digitalUser")
  private DigitalUser digitalUser = null;

  @SerializedName("digitalUserSession")
  private DigitalUserSession digitalUserSession = null;

  public LoginResponse digitalUser(DigitalUser digitalUser) {
    this.digitalUser = digitalUser;
    return this;
  }

   /**
   * Get digitalUser
   * @return digitalUser
  **/
  @ApiModelProperty(required = true, value = "")
  public DigitalUser getDigitalUser() {
    return digitalUser;
  }

  public void setDigitalUser(DigitalUser digitalUser) {
    this.digitalUser = digitalUser;
  }

  public LoginResponse digitalUserSession(DigitalUserSession digitalUserSession) {
    this.digitalUserSession = digitalUserSession;
    return this;
  }

   /**
   * Get digitalUserSession
   * @return digitalUserSession
  **/
  @ApiModelProperty(required = true, value = "")
  public DigitalUserSession getDigitalUserSession() {
    return digitalUserSession;
  }

  public void setDigitalUserSession(DigitalUserSession digitalUserSession) {
    this.digitalUserSession = digitalUserSession;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    LoginResponse loginResponse = (LoginResponse) o;
    return Objects.equals(this.digitalUser, loginResponse.digitalUser) &&
        Objects.equals(this.digitalUserSession, loginResponse.digitalUserSession);
  }

  @Override
  public int hashCode() {
    return Objects.hash(digitalUser, digitalUserSession);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class LoginResponse {\n");
    
    sb.append("    digitalUser: ").append(toIndentedString(digitalUser)).append("\n");
    sb.append("    digitalUserSession: ").append(toIndentedString(digitalUserSession)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

