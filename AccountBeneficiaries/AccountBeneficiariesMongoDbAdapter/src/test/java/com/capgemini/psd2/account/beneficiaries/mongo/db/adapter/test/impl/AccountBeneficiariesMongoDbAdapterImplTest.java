package com.capgemini.psd2.account.beneficiaries.mongo.db.adapter.test.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyString;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.account.beneficiaries.mongo.db.adapter.impl.AccountBeneficiariesMongoDbAdapterImpl;
import com.capgemini.psd2.account.beneficiaries.mongo.db.adapter.repository.AccountBeneficiaryRepository;
import com.capgemini.psd2.account.beneficiaries.mongo.db.adapter.test.mock.data.AccountBeneficiaryMockData;
import com.capgemini.psd2.aisp.mongo.db.adapter.domain.AccountBeneficiaryCMA2;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountBeneficiariesResponse;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.exceptions.OBPSD2ExceptionUtility;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.LoggerUtils;
import com.capgemini.psd2.utilities.SandboxValidationUtility;

/**
 * The Class AccountBeneficiariesMongoDBAdapterImplTest.
 */

@RunWith(SpringJUnit4ClassRunner.class)
public class AccountBeneficiariesMongoDbAdapterImplTest {

	@Mock
	AccountBeneficiaryRepository accountBeneficicaryRepository;

	@Mock
	LoggerUtils loggerUtils;

	@InjectMocks
	private AccountBeneficiariesMongoDbAdapterImpl accountBeneficiariesMongoDbAdapterImpl;
	
	@Mock
	SandboxValidationUtility utility;

	/**
	 * Sets the up.
	 *
	 * @throws Exception
	 *             the exception
	 */
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		Map<String, String> map=new HashMap<>();
		map.put("SIGNATURE", "Something wrong with x-jws-signature request header");
		Map<String, String> specificErrorMessageMap=new HashMap<>();
		specificErrorMessageMap.put("NO_ACCOUNT_ID_FOUND", "signature_invalid_content");
		OBPSD2ExceptionUtility.genericErrorMessages.putAll(map);
		OBPSD2ExceptionUtility.specificErrorMessages.putAll(specificErrorMessageMap); 
	}

	/** The account beneficiaries mongo DB adapter impl. */

	/**
	 * Test retrieve blank response account beneficiaries success flow.
	 */

	@Test
	public void testRetrieveAccountBeneficiariesSuccessFlow() {
		AccountMapping accountMapping = new AccountMapping();
		ArrayList<AccountDetails> accountDetailsList = new ArrayList<>();
		AccountDetails accountDetails = new AccountDetails();
		Map<String, String> params = new HashMap<>();

		accountDetails.setAccountId("f4483fda-81be-4873-b4a6-20375b7f55c1");
		accountDetailsList.add(accountDetails);
		accountMapping.setAccountDetails(accountDetailsList);
		
		Mockito.when(utility.isValidPsuId(accountMapping.getPsuId())).thenReturn(false);


		List<AccountBeneficiaryCMA2> beneficiaryList = new ArrayList<>();	
		AccountBeneficiaryCMA2 beneficiaries = new AccountBeneficiaryCMA2();
		beneficiaryList.add(beneficiaries);
		Mockito.when(accountBeneficicaryRepository.findByAccountNumberAndAccountNSC(anyString(), anyString()))
				.thenReturn(beneficiaryList);

		Mockito.when(loggerUtils.populateLoggerData(anyString()))
				.thenReturn(AccountBeneficiaryMockData.getMockLoggerData());

		PlatformAccountBeneficiariesResponse actualResponse = accountBeneficiariesMongoDbAdapterImpl
				.retrieveAccountBeneficiaries(accountMapping, params);

		assertEquals(AccountBeneficiaryMockData.getMockExpectedAccountBeneficiaryResponse(),
				actualResponse.getoBReadBeneficiary5());

	}

	@Test(expected = PSD2Exception.class)
	public void testRetrieveAccountBeneficiariesDbExceptionFlow() {
		AccountMapping accountMapping = new AccountMapping();
		ArrayList<AccountDetails> accountDetailsList = new ArrayList<>();
		AccountDetails accountDetails = new AccountDetails();
		Map<String, String> params = new HashMap<>();

		accountDetails.setAccountId("f4483fda-81be-4873-b4a6-20375b7f55c1");
		accountDetailsList.add(accountDetails);
		accountMapping.setAccountDetails(accountDetailsList);
		
		Mockito.when(utility.isValidPsuId(accountMapping.getPsuId())).thenReturn(false);

		Mockito.when(accountBeneficicaryRepository.findByAccountNumberAndAccountNSC(anyString(), anyString()))
				.thenThrow(DataAccessResourceFailureException.class);

		Mockito.when(loggerUtils.populateLoggerData(anyString()))
				.thenReturn(AccountBeneficiaryMockData.getMockLoggerData());

		PlatformAccountBeneficiariesResponse actualResponse = accountBeneficiariesMongoDbAdapterImpl
				.retrieveAccountBeneficiaries(accountMapping, params);

		assertEquals(AccountBeneficiaryMockData.getMockExpectedAccountBeneficiaryResponse(),
				actualResponse.getoBReadBeneficiary5());

	}

	@Test
	public void testRetrieveAccountBeneficiariesDbNullFlow() {
		AccountMapping accountMapping = new AccountMapping();
		ArrayList<AccountDetails> accountDetailsList = new ArrayList<>();
		AccountDetails accountDetails = new AccountDetails();
		Map<String, String> params = new HashMap<>();

		accountDetails.setAccountId("f4483fda-81be-4873-b4a6-20375b7f55c1");
		accountDetailsList.add(accountDetails);
		accountMapping.setAccountDetails(accountDetailsList);
		
		Mockito.when(utility.isValidPsuId(accountMapping.getPsuId())).thenReturn(false);

		Mockito.when(accountBeneficicaryRepository.findByAccountNumberAndAccountNSC(anyString(), anyString()))
				.thenReturn(null);

		Mockito.when(loggerUtils.populateLoggerData(anyString()))
				.thenReturn(AccountBeneficiaryMockData.getMockLoggerData());

		PlatformAccountBeneficiariesResponse actualResponse = accountBeneficiariesMongoDbAdapterImpl
				.retrieveAccountBeneficiaries(accountMapping, params);

	}

}
