package com.capgemini.psd2.account.beneficiaries.mongo.db.adapter.test.mock.data;

import java.util.ArrayList;
import java.util.List;

import com.capgemini.psd2.aisp.domain.OBBeneficiary5;
import com.capgemini.psd2.aisp.domain.OBReadBeneficiary5;
import com.capgemini.psd2.aisp.domain.OBReadBeneficiary5Data;
import com.capgemini.psd2.aisp.mongo.db.adapter.domain.AccountBeneficiaryCMA2;
import com.capgemini.psd2.logger.LoggerAttribute;

public class AccountBeneficiaryMockData {
	
	public static List<AccountBeneficiaryCMA2> getMockAccountDetails()
	{
		return new ArrayList<AccountBeneficiaryCMA2>();
	}
	
	public static LoggerAttribute getMockLoggerData() {
		LoggerAttribute x = new LoggerAttribute();
		x.setApiId("testApiID");
		return x;
	}
	
	public static OBReadBeneficiary5 getMockExpectedAccountBeneficiaryResponse()
	{
		OBReadBeneficiary5Data OBReadBeneficiary5Data = new OBReadBeneficiary5Data();
		OBReadBeneficiary5 OBReadBeneficiary5 = new OBReadBeneficiary5();
		AccountBeneficiaryCMA2 mockAccountBeneficiary = new AccountBeneficiaryCMA2();
		
		mockAccountBeneficiary.setAccountId("f4483fda-81be-4873-b4a6-20375b7f55c1");
		
		List<OBBeneficiary5> beneficiaryList = new ArrayList<>();
		beneficiaryList.add(mockAccountBeneficiary);
		OBReadBeneficiary5Data.setBeneficiary(beneficiaryList);
		OBReadBeneficiary5.setData(OBReadBeneficiary5Data);
		return OBReadBeneficiary5;
	}
	

}
