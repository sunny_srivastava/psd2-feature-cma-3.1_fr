package com.capgemini.psd2.account.beneficiaries.mongo.db.adapter.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.capgemini.psd2.account.beneficiaries.mongo.db.adapter.impl.AccountBeneficiariesMongoDbAdapterImpl;

import com.capgemini.psd2.aisp.adapter.AccountBeneficiariesAdapter;

/**
 * The Class AccountBeneficiariesMongoDbAdapterConfig.
 */
@Configuration
public class AccountBeneficiariesMongoDbAdapterConfig {
	
	/**
	 * Mongo DB adapter.
	 *
	 * @return the Account beneficiaries adapter
	 */
	@Bean(name="accountBeneficiariesMongoDbAdapter")
	public AccountBeneficiariesAdapter mongoDBAdapter(){
		return new AccountBeneficiariesMongoDbAdapterImpl();
	}
	
}
