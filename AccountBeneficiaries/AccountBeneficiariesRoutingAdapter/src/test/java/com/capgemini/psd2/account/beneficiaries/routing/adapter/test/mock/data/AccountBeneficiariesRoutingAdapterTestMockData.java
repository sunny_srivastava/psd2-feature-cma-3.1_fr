package com.capgemini.psd2.account.beneficiaries.routing.adapter.test.mock.data;

import java.util.ArrayList;
import java.util.List;

import com.capgemini.psd2.aisp.domain.OBBeneficiary5;
import com.capgemini.psd2.aisp.domain.OBReadBeneficiary5;
import com.capgemini.psd2.aisp.domain.OBReadBeneficiary5Data;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountBeneficiariesResponse;

public class AccountBeneficiariesRoutingAdapterTestMockData {

	public static PlatformAccountBeneficiariesResponse getMockBeneficiariesGETResponse() {
		
		PlatformAccountBeneficiariesResponse platformAccountBeneficiariesResponse = new PlatformAccountBeneficiariesResponse();
		OBReadBeneficiary5Data obReadBeneficiary2Data = new OBReadBeneficiary5Data();
		List<OBBeneficiary5> obBeneficiaryList = new ArrayList<>();
		obReadBeneficiary2Data.setBeneficiary(obBeneficiaryList);
		
		OBReadBeneficiary5 obReadBeneficiary2 = new OBReadBeneficiary5();
		obReadBeneficiary2.setData(obReadBeneficiary2Data);
		platformAccountBeneficiariesResponse.setoBReadBeneficiary5(obReadBeneficiary2);
		return platformAccountBeneficiariesResponse;
	}
}
