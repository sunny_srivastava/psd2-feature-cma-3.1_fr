package com.capgemini.psd2.account.beneficiaries.routing.adapter.test.routing;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.context.ApplicationContext;

import com.capgemini.psd2.account.beneficiaries.routing.adapter.impl.AccountBeneficiariesRoutingAdapter;
import com.capgemini.psd2.account.beneficiaries.routing.adapter.routing.AccountBeneficiariesCoreSystemAdapterFactory;
import com.capgemini.psd2.aisp.adapter.AccountBeneficiariesAdapter;
import com.capgemini.psd2.exceptions.OBPSD2ExceptionUtility;


public class AccountBeneficiariesAdapterFactoryTest 
{
	
	/** The application context. */
	@Mock
	private ApplicationContext applicationContext;
	
	/** The account beneficiaries core system adapter factory. */
	@InjectMocks
	private AccountBeneficiariesCoreSystemAdapterFactory accountBeneficiariesCoreSystemAdapterFactory;
	
	/**
	 * Sets the up.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		Map<String, String> map=new HashMap<>();
		map.put("SIGNATURE", "Something wrong with x-jws-signature request header");
		Map<String, String> specificErrorMessageMap=new HashMap<>();
		specificErrorMessageMap.put("signature_invalid_content", "Invalid content prsent in joseheader of x-jws-signature header");
		specificErrorMessageMap.put("signature_missing", "signature header missing in request");
		OBPSD2ExceptionUtility.genericErrorMessages.putAll(map);
		OBPSD2ExceptionUtility.specificErrorMessages.putAll(specificErrorMessageMap);
	}
	
	/**
	 * Test account beneficiaries adapter.
	 */
	@Test
	public void testAccountBeneficiariesAdapter() {
		AccountBeneficiariesAdapter accountBeneficiariesAdapter = new AccountBeneficiariesRoutingAdapter();
		when(applicationContext.getBean(anyString())).thenReturn(accountBeneficiariesAdapter);
		AccountBeneficiariesAdapter accountBeneficiariesAdapterResult = (AccountBeneficiariesRoutingAdapter) accountBeneficiariesCoreSystemAdapterFactory.getAdapterInstance("accountBeneficiariesAdapter");
		assertEquals(accountBeneficiariesAdapter, accountBeneficiariesAdapterResult);
	}
	
	

}
