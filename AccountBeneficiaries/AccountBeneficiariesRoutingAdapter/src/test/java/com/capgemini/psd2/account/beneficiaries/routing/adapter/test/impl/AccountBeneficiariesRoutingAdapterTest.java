package com.capgemini.psd2.account.beneficiaries.routing.adapter.test.impl;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyString;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.capgemini.psd2.account.beneficiaries.routing.adapter.impl.AccountBeneficiariesRoutingAdapter;
import com.capgemini.psd2.account.beneficiaries.routing.adapter.routing.AccountBeneficiariesAdapterFactory;
import com.capgemini.psd2.account.beneficiaries.routing.adapter.test.adapter.AccountBeneficiariesTestRoutingAdapter;
import com.capgemini.psd2.aisp.adapter.AccountBeneficiariesAdapter;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountBeneficiariesResponse;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.token.Token;

public class AccountBeneficiariesRoutingAdapterTest {

	/** The account beneficiaries adapter factory. */
	@Mock
	private AccountBeneficiariesAdapterFactory accountBeneficiariesAdapterFactory;
	
	@Mock
	private RequestHeaderAttributes reqHeaderAtrributes;

	/** The account beneficiaries routing adapter. */
	@InjectMocks
	private AccountBeneficiariesAdapter accountBeneficiariesRoutingAdapter = new AccountBeneficiariesRoutingAdapter();

	/**
	 * Sets the up.
	 *
	 * @throws Exception
	 *             the exception
	 */
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * Retrieve account beneficiary.
	 */
	@Test
	public void retrieveAccountBenificiarytest() {
		AccountBeneficiariesAdapter accountBeneficiariesAdapter = new AccountBeneficiariesTestRoutingAdapter();

		Mockito.when(accountBeneficiariesAdapterFactory.getAdapterInstance(anyString()))
				.thenReturn(accountBeneficiariesAdapter);
		Token token = new Token();
		Map<String, String> map  = new HashMap<>();
		token.setSeviceParams(map);
		Mockito.when(reqHeaderAtrributes.getToken()).thenReturn(token);
		PlatformAccountBeneficiariesResponse platformAccountBeneficiariesResponse = accountBeneficiariesRoutingAdapter
				.retrieveAccountBeneficiaries(null, null);

		assertTrue(platformAccountBeneficiariesResponse.getoBReadBeneficiary5().getData().getBeneficiary().isEmpty());
		assertNotNull(platformAccountBeneficiariesResponse.getoBReadBeneficiary5().getData());
	}

}
