package com.capgemini.psd2.account.beneficiaries.test.service.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.account.beneficiaries.service.impl.AccountBeneficiariesServiceImpl;
import com.capgemini.psd2.account.beneficiaries.test.mock.data.AccountBeneficiariesMockData;
import com.capgemini.psd2.aisp.account.mapping.adapter.AccountMappingAdapter;
import com.capgemini.psd2.aisp.adapter.AccountBeneficiariesAdapter;
import com.capgemini.psd2.aisp.adapter.AispConsentAdapter;
import com.capgemini.psd2.aisp.domain.OBReadBeneficiary5;
import com.capgemini.psd2.aisp.validation.adapter.impl.AccountBeneficiariesValidatorImpl;
import com.capgemini.psd2.logger.LoggerUtils;
import com.capgemini.psd2.logger.RequestHeaderAttributes;


@RunWith(SpringJUnit4ClassRunner.class)
public class AccountBeneficiariesServiceImplTest {
	/** The adapter. */
	@Mock
	private AccountBeneficiariesAdapter adapter;

	@Mock
	private AccountMappingAdapter accountMappingAdapter;

	/** The consent mapping. */
	@Mock
	private AispConsentAdapter aispConsentAdapter;

	/** The header atttributes. */
	@Mock
	private RequestHeaderAttributes headerAtttributes;

	@Mock
	private LoggerUtils loggerUtils;

	/** The service. */
	@InjectMocks
	private AccountBeneficiariesServiceImpl service = new AccountBeneficiariesServiceImpl();
	
	@Mock
	AccountBeneficiariesValidatorImpl accountsCustomValidator;

	/**
	 * Sets the up.
	 *
	 * @throws Exception
	 *             the exception
	 */
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		when(headerAtttributes.getToken()).thenReturn(AccountBeneficiariesMockData.getToken());
		when(aispConsentAdapter.retrieveAccountMapping(anyString()))
				.thenReturn(AccountBeneficiariesMockData.getMockAccountMapping());

	}

	/**
	 * Retrieve account beneficiaries test (empty list).
	 */
	@Test
	public void retrieveAccountBeneficiariesSuccessWithLinksMeta() {
		
		accountsCustomValidator.validateUniqueId(anyObject());
		
		Mockito.when(aispConsentAdapter.validateAndRetrieveConsentByAccountId(anyString(), anyString()))
				.thenReturn(AccountBeneficiariesMockData.getMockAispConsent());

		Mockito.when(accountMappingAdapter.retrieveAccountMappingDetails(anyString(), anyObject()))
				.thenReturn(AccountBeneficiariesMockData.getMockAccountMapping());

		Mockito.when(adapter.retrieveAccountBeneficiaries(anyObject(), anyObject()))
				.thenReturn(AccountBeneficiariesMockData.getMockPlatformReponseWithLinksMeta());

		Mockito.when(loggerUtils.populateLoggerData(anyString()))
		.thenReturn(AccountBeneficiariesMockData.getMockLoggerData());
		accountsCustomValidator.validateResponseParams(anyObject());
		
		OBReadBeneficiary5 actualResponse = service.retrieveAccountBeneficiaries("f4483fda-81be-4873-b4a6-20375b7f55c1");
		assertEquals(AccountBeneficiariesMockData.getMockExpectedAccountBeneficiaryResponseWithLinksMeta(),actualResponse);
 

	}
	
	
	@Test
	public void retrieveAccountBeneficiariesSuccessWithoutLinksMeta() {
		Mockito.when(aispConsentAdapter.validateAndRetrieveConsentByAccountId(anyString(), anyString()))
				.thenReturn(AccountBeneficiariesMockData.getMockAispConsent());

		Mockito.when(accountMappingAdapter.retrieveAccountMappingDetails(anyString(), anyObject()))
				.thenReturn(AccountBeneficiariesMockData.getMockAccountMapping());

		Mockito.when(adapter.retrieveAccountBeneficiaries(anyObject(), anyObject()))
				.thenReturn(AccountBeneficiariesMockData.getMockPlatformReponseWithoutLinksMeta());

		Mockito.when(loggerUtils.populateLoggerData(anyString()))
		.thenReturn(AccountBeneficiariesMockData.getMockLoggerData());
		accountsCustomValidator.validateResponseParams(anyObject());
		
		OBReadBeneficiary5 actualResponse = service.retrieveAccountBeneficiaries("123");
		assertEquals(AccountBeneficiariesMockData.getMockExpectedAccountBeneficiaryResponseWithoutLinksMeta(),actualResponse);
 

	}

	/**
	 * Tear down.
	 *
	 * @throws Exception
	 *             the exception
	 */
	@After
	public void tearDown() throws Exception {
		service = null;
	}

}
