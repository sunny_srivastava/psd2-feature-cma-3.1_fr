package com.capgemini.psd2.account.beneficiaries.test.mock.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.capgemini.psd2.aisp.domain.Links;
import com.capgemini.psd2.aisp.domain.Meta;
import com.capgemini.psd2.aisp.domain.OBBeneficiary5;
import com.capgemini.psd2.aisp.domain.OBReadBeneficiary5;
import com.capgemini.psd2.aisp.domain.OBReadBeneficiary5Data;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountBeneficiariesResponse;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.consent.domain.AispConsent;
import com.capgemini.psd2.enums.ConsentStatusEnum;
import com.capgemini.psd2.logger.LoggerAttribute;
import com.capgemini.psd2.token.ConsentTokenData;
import com.capgemini.psd2.token.Token;

public class AccountBeneficiariesMockData {
	/** The mock token. */
	public static Token mockToken;

	/**
	 * Gets the token.
	 *
	 * @return the token
	 */
	public static Token getToken() {
		mockToken = new Token();
		ConsentTokenData consentTokenData = new ConsentTokenData();
		consentTokenData.setConsentExpiry("1509348259877L");
		consentTokenData.setConsentId("12345");
		mockToken.setConsentTokenData(consentTokenData);
		Map<String,String> map=new HashMap<>();
		mockToken.setSeviceParams(map);
		return mockToken;
	}

	/**
	 * Gets the mock account mapping.
	 *
	 * @return the mock account mapping
	 */
	public static AccountMapping getMockAccountMapping() {
		AccountMapping mapping = new AccountMapping();
		mapping.setTppCID("tpp123");
		mapping.setPsuId("user123");
		List<AccountDetails> selectedAccounts = new ArrayList<>();
		AccountDetails accountRequest = new AccountDetails();
		accountRequest.setAccountId("269c3ff5-d7f8-419b-a3b9-7136c5b4611a");
		accountRequest.setAccountNSC("SC802001");
		accountRequest.setAccountNumber("10203345");
		selectedAccounts.add(accountRequest);
		mapping.setAccountDetails(selectedAccounts);
		return mapping;
	}

	public static AispConsent getMockAispConsent() {
		AispConsent aispConsent = new AispConsent();
		aispConsent.setTppCId("6443e15975554bce8099e35b88b40465");
		aispConsent.setPsuId("88888888");
		aispConsent.setConsentId("39032481");
		aispConsent.setAccountRequestId("ae8c4441-f783-4e80-8810-254241bed98c");
		aispConsent.setChannelId("B365");
		aispConsent.setStartDate("2017-08-09T06:44:31.250Z");
		aispConsent.setEndDate("2018-09-02T00:00:00.875");
		aispConsent.setTransactionFromDateTime("2015-05-03T00:00:00.800");
		aispConsent.setTransactionToDateTime("2018-12-03T00:00:00.345");
		aispConsent.setStatus(ConsentStatusEnum.AUTHORISED);

		List<AccountDetails> selectedAispConsentAccountDetails = new ArrayList<>();
		AccountDetails accountRequest = new AccountDetails();

		accountRequest.setAccountId("f4483fda-81be-4873-b4a6-20375b7f55c1");
		accountRequest.setAccountNSC("903779");
		accountRequest.setAccountNumber("76528776");

		selectedAispConsentAccountDetails.add(accountRequest);
		aispConsent.setAccountDetails(selectedAispConsentAccountDetails);

		return aispConsent;
	}

	public static PlatformAccountBeneficiariesResponse getMockPlatformReponseWithLinksMeta() {
		PlatformAccountBeneficiariesResponse platformAccountBeneficiariesResponse = new PlatformAccountBeneficiariesResponse();
		OBReadBeneficiary5Data OBReadBeneficiary5Data = new OBReadBeneficiary5Data();
		List<OBBeneficiary5> obBeneficiaryList = new ArrayList<>();
		OBBeneficiary5 OBBeneficiary5 = new OBBeneficiary5();
		OBBeneficiary5.setAccountId("f4483fda-81be-4873-b4a6-20375b7f55c1");
		OBBeneficiary5.setBeneficiaryId("TestBeneficiaryId");
		obBeneficiaryList.add(OBBeneficiary5);
		OBReadBeneficiary5Data.setBeneficiary(obBeneficiaryList);

		OBReadBeneficiary5 OBReadBeneficiary5 = new OBReadBeneficiary5();
		OBReadBeneficiary5.setData(OBReadBeneficiary5Data);

		Links links = new Links();
		OBReadBeneficiary5.setLinks(links);
		Meta meta = new Meta();
		OBReadBeneficiary5.setMeta(meta);
		platformAccountBeneficiariesResponse.setoBReadBeneficiary5(OBReadBeneficiary5);
		return platformAccountBeneficiariesResponse;

	}

	public static Object getMockExpectedAccountBeneficiaryResponseWithLinksMeta() {
		OBReadBeneficiary5 OBReadBeneficiary5 = new OBReadBeneficiary5();
		OBReadBeneficiary5Data OBReadBeneficiary5Data = new OBReadBeneficiary5Data();
		List<OBBeneficiary5> obBeneficiaryList = new ArrayList<>();
		OBBeneficiary5 OBBeneficiary5 = new OBBeneficiary5();
		OBBeneficiary5.setAccountId("f4483fda-81be-4873-b4a6-20375b7f55c1");
		OBBeneficiary5.setBeneficiaryId("TestBeneficiaryId");
		obBeneficiaryList.add(OBBeneficiary5);
		OBReadBeneficiary5Data.setBeneficiary(obBeneficiaryList);

		OBReadBeneficiary5.setData(OBReadBeneficiary5Data);

		Links links = new Links();
		OBReadBeneficiary5.setLinks(links);
		Meta meta = new Meta();
		meta.setTotalPages(1);
		OBReadBeneficiary5.setMeta(meta);

		return OBReadBeneficiary5;

	}
	
	
	public static LoggerAttribute getMockLoggerData() {
		LoggerAttribute x = new LoggerAttribute();
		x.setApiId("testApiID");
		return x;
	}
	
	
	public static PlatformAccountBeneficiariesResponse getMockPlatformReponseWithoutLinksMeta() {
		PlatformAccountBeneficiariesResponse platformAccountBeneficiariesResponse = new PlatformAccountBeneficiariesResponse();
		OBReadBeneficiary5Data OBReadBeneficiary5Data = new OBReadBeneficiary5Data();
		List<OBBeneficiary5> obBeneficiaryList = new ArrayList<>();
		OBBeneficiary5 OBBeneficiary5 = new OBBeneficiary5();
		OBBeneficiary5.setAccountId("123");
		OBBeneficiary5.setBeneficiaryId("TestBeneficiaryId");
		obBeneficiaryList.add(OBBeneficiary5);
		OBReadBeneficiary5Data.setBeneficiary(obBeneficiaryList);

		OBReadBeneficiary5 OBReadBeneficiary5 = new OBReadBeneficiary5();
		OBReadBeneficiary5.setData(OBReadBeneficiary5Data);
		
		platformAccountBeneficiariesResponse.setoBReadBeneficiary5(OBReadBeneficiary5);
		return platformAccountBeneficiariesResponse;

	}
	
	public static Object getMockExpectedAccountBeneficiaryResponseWithoutLinksMeta() {
		OBReadBeneficiary5 OBReadBeneficiary5 = new OBReadBeneficiary5();
		OBReadBeneficiary5Data OBReadBeneficiary5Data = new OBReadBeneficiary5Data();
		List<OBBeneficiary5> obBeneficiaryList = new ArrayList<>();
		OBBeneficiary5 OBBeneficiary5 = new OBBeneficiary5();
		OBBeneficiary5.setAccountId("123");
		OBBeneficiary5.setBeneficiaryId("TestBeneficiaryId");
		obBeneficiaryList.add(OBBeneficiary5);
		OBReadBeneficiary5Data.setBeneficiary(obBeneficiaryList);

		OBReadBeneficiary5.setData(OBReadBeneficiary5Data);

		Links links = new Links();
		OBReadBeneficiary5.setLinks(links);
		Meta meta = new Meta();
		meta.setTotalPages(1);
		OBReadBeneficiary5.setMeta(meta);

		return OBReadBeneficiary5;
		
	}



}