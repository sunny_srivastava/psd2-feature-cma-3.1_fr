package com.capgemini.psd2.account.beneficiaries.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.psd2.account.beneficiaries.service.AccountBeneficiariesService;
import com.capgemini.psd2.aisp.domain.OBReadBeneficiary5;

@RestController
public class AccountBeneficiariesController 
{
	/** The service. */
	@Autowired
	private AccountBeneficiariesService service;
	
	@RequestMapping(value ="/accounts/{accountId}/beneficiaries", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public OBReadBeneficiary5 retrieveAccountBeneficiaries(@PathVariable("accountId") String accountId) {
		
		return service.retrieveAccountBeneficiaries(accountId);
	}
}
 