package com.capgemini.psd2.account.beneficiaries.service.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.capgemini.psd2.account.beneficiaries.service.AccountBeneficiariesService;
import com.capgemini.psd2.aisp.account.mapping.adapter.AccountMappingAdapter;
import com.capgemini.psd2.aisp.adapter.AccountBeneficiariesAdapter;
import com.capgemini.psd2.aisp.adapter.AispConsentAdapter;
import com.capgemini.psd2.aisp.domain.Links;
import com.capgemini.psd2.aisp.domain.Meta;
import com.capgemini.psd2.aisp.domain.OBReadBeneficiary5;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountBeneficiariesResponse;
import com.capgemini.psd2.aisp.validation.adapter.AISPCustomValidator;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.consent.domain.AispConsent;
import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;

@Service
public class AccountBeneficiariesServiceImpl implements AccountBeneficiariesService {

	/** The req header atrributes. */
	@Autowired
	private RequestHeaderAttributes reqHeaderAtrributes;

	/** The account beneficiaries adapter. */
	@Autowired
	@Qualifier("accountBeneficiariesRoutingAdapter")
	private AccountBeneficiariesAdapter accountBeneficiariesAdapter;

	/** The aisp consent adapter. */
	@Autowired
	private AispConsentAdapter aispConsentAdapter;
	
	/** The account mapping adapter. */
	@Autowired
	private AccountMappingAdapter accountMappingAdapter;
	
	@SuppressWarnings("rawtypes")
	@Autowired
	@Qualifier("accountsRoutingValidator")
	private AISPCustomValidator accountsCustomValidator;

	@Override
	public OBReadBeneficiary5 retrieveAccountBeneficiaries(String accountId) {

		accountsCustomValidator.validateUniqueId(accountId);
		
		// Get consents by accountId and validate.
		AispConsent aispConsent = aispConsentAdapter.validateAndRetrieveConsentByAccountId(accountId,
						reqHeaderAtrributes.getToken().getConsentTokenData().getConsentId());

		// Get account mapping.
		AccountMapping accountMapping = accountMappingAdapter.retrieveAccountMappingDetails(accountId, aispConsent);
		
		Map<String,String> params=reqHeaderAtrributes.getToken().getSeviceParams();
		params.put(PSD2Constants.CMAVERSION, aispConsent.getCmaVersion());
		params.put(PSD2Constants.CO_RELATION_ID, reqHeaderAtrributes.getCorrelationId());
		// Retrieve account beneficiaries.
		PlatformAccountBeneficiariesResponse platformBeneficiariesResponse = accountBeneficiariesAdapter.retrieveAccountBeneficiaries(accountMapping,params);
				
		if (platformBeneficiariesResponse == null)
						throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.NO_RECORD_FOUND_FOR_REQUESTED_ACCT);

		// Get CMA2 beneficiaries response.
		OBReadBeneficiary5 tppBeneficiariesResponse = platformBeneficiariesResponse.getoBReadBeneficiary5();

		if (tppBeneficiariesResponse == null)
						throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.NO_RECORD_FOUND_FOR_REQUESTED_ACCT);

		// update platform response.
		updatePlatformResponse(tppBeneficiariesResponse);

		// Apply platform and CMA2-swagger validations.
		accountsCustomValidator.validateResponseParams(tppBeneficiariesResponse);

				return tppBeneficiariesResponse;
			}

			private void updatePlatformResponse(OBReadBeneficiary5 tppBeneficiariesResponse) {
				if (tppBeneficiariesResponse.getLinks() == null)
					tppBeneficiariesResponse.setLinks(new Links());
				tppBeneficiariesResponse.getLinks().setSelf(reqHeaderAtrributes.getSelfUrl());

				if (tppBeneficiariesResponse.getMeta() == null)
					tppBeneficiariesResponse.setMeta(new Meta());
				tppBeneficiariesResponse.getMeta().setTotalPages(1);
			}

}
