/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.account.party.mongo.db.adapter.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.account.party.mongo.db.adapter.repository.AccountPartyRepository;
import com.capgemini.psd2.aisp.adapter.AccountPartyAdapter;
import com.capgemini.psd2.aisp.domain.OBParty2;
import com.capgemini.psd2.aisp.domain.OBParty2Address;
import com.capgemini.psd2.aisp.domain.OBReadParty2;
import com.capgemini.psd2.aisp.domain.OBReadParty2Data;
import com.capgemini.psd2.aisp.mongo.db.adapter.domain.AccountPartyCMA2;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountPartyResponse;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.InternalServerErrorMessage;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.LoggerUtils;
import com.capgemini.psd2.logger.PSD2Constants;

/**
 * The Class AccountBalanceMongoDbAdaptorImpl.
 */
@Component
public class AccountPartyMongoDbAdaptorImpl implements AccountPartyAdapter {

	@Autowired
	private AccountPartyRepository repository;
	
	@Autowired
	private LoggerUtils loggerUtils;
	
	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(AccountPartyMongoDbAdaptorImpl.class);

	@Override
	public PlatformAccountPartyResponse retrieveAccountParty(AccountMapping accountMapping,
			Map<String, String> params) {
		
		LOG.info("{\"Enter\":\"{}\",\"{}\"}",
				"com.capgemini.psd2.account.party.mongo.db.adapter.impl.retrieveAccountParty()",
				loggerUtils.populateLoggerData("retrieveAccountParty"));

		PlatformAccountPartyResponse platformAccountPartyResponse = new PlatformAccountPartyResponse();
		OBReadParty2 oBReadParty = new OBReadParty2();
		OBParty2 oBParty = new OBParty2();
		AccountPartyCMA2 mockParty = null;
		LOG.info("TenantId : " + params.get(PSD2Constants.TENANT_ID));
		
		try {
			mockParty = repository.findByAccountNumberAndAccountNSC(
					accountMapping.getAccountDetails().get(0).getAccountNumber(),
					accountMapping.getAccountDetails().get(0).getAccountNSC());
		} catch (DataAccessResourceFailureException e) {
			LOG.error(e.getMessage(),e);
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
					InternalServerErrorMessage.CONNECTION_ERROR));
		}
		OBReadParty2Data oBReadPartyData =new OBReadParty2Data();
		oBReadParty.setData(oBReadPartyData);
		oBReadPartyData.setParty(mockParty);
		List<OBParty2Address> oBPostalAddress8List  = new ArrayList<>();
		OBParty2Address oBPostalAddress8 = new OBParty2Address();
		oBParty.setAddress(oBPostalAddress8List);
		oBParty.addAddressItem(oBPostalAddress8);
		platformAccountPartyResponse.setoBReadParty2(oBReadParty);
		return platformAccountPartyResponse;
	}
}