package om.capgemini.psd2.account.party.mongo.db.adapter.test.mock.data;
/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/

import java.util.ArrayList;
import java.util.List;

import com.capgemini.psd2.aisp.domain.OBParty2;
import com.capgemini.psd2.aisp.domain.OBParty2Address;
import com.capgemini.psd2.aisp.domain.OBReadParty2;
import com.capgemini.psd2.aisp.domain.OBReadParty2Data;
import com.capgemini.psd2.aisp.mongo.db.adapter.domain.AccountPartyCMA2;
import com.capgemini.psd2.logger.LoggerAttribute;

/**
 * The Class AccountBalanceAdapterMockData.
 */
public class AccountPartyMockData {

	public static AccountPartyCMA2 getMockAccountDetails()
	{
		return new AccountPartyCMA2();
	}
	
	public static LoggerAttribute getMockLoggerData() {
		LoggerAttribute x = new LoggerAttribute();
		x.setApiId("testApiID");
		return x;
	}
	
	public static OBReadParty2 getMockExpectedPartyResponse()
	{
		OBReadParty2 oBReadParty = new OBReadParty2();
		OBReadParty2Data data = new OBReadParty2Data();
		
		OBParty2 oBParty = new OBParty2();
		
		List<OBParty2Address> partyList = new ArrayList<>();
		OBParty2Address oBPostalAddress8 = new OBParty2Address();
		
		partyList.add(oBPostalAddress8);
		
		oBParty.addAddressItem(oBPostalAddress8);
		oBParty.setAddress(partyList);
		oBReadParty.setData(data);
		data.setParty(oBParty);
		
		return oBReadParty;
	}
}