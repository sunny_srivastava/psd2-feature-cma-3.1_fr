/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.account.party.routing.adapter.test.impl;

import static org.mockito.Matchers.anyString;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.capgemini.psd2.account.party.routing.adapter.impl.AccountPartyRoutingAdapter;
import com.capgemini.psd2.account.party.routing.adapter.routing.AccountPartyAdapterFactory;
import com.capgemini.psd2.account.party.routing.adapter.test.adapter.AccountPartyTestRoutingAdapter;
import com.capgemini.psd2.aisp.adapter.AccountPartyAdapter;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountPartyResponse;
import com.capgemini.psd2.exceptions.OBPSD2ExceptionUtility;
import com.capgemini.psd2.logger.RequestHeaderAttributes;


/**
 * The Class AccountBalanceRoutingAdapterTest.
 */
public class AccountPartyRoutingAdapterTest {

	/** The account balance adapter factory. */
	@Mock
	private AccountPartyAdapterFactory accountPartyAdapterFactory;
	
	@Mock
	private RequestHeaderAttributes reqHeaderAtrributes;

	/** The account balance routing adapter. */
	@InjectMocks
	private AccountPartyAdapter accountPartyRoutingAdapter = new AccountPartyRoutingAdapter();

	/**
	 * Sets the up.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		Map<String, String> map=new HashMap<>();
		map.put("SIGNATURE", "Something wrong with x-jws-signature request header");
		Map<String, String> specificErrorMessageMap=new HashMap<>();
		specificErrorMessageMap.put("signature_invalid_content", "Invalid content prsent in joseheader of x-jws-signature header");
		specificErrorMessageMap.put("signature_missing", "signature header missing in request");
		OBPSD2ExceptionUtility.genericErrorMessages.putAll(map);
		OBPSD2ExceptionUtility.specificErrorMessages.putAll(specificErrorMessageMap);
	}

	/**
	 * Retrieve account partytest.
	 */
	@Test
	public void retrieveAccountPartytest() {

		AccountPartyAdapter accountPartyAdapter = new AccountPartyTestRoutingAdapter();
		Mockito.when(accountPartyAdapterFactory.getAdapterInstance(anyString()))
				.thenReturn(accountPartyAdapter);
		
		PlatformAccountPartyResponse partyGETResponse = accountPartyRoutingAdapter.retrieveAccountParty(null,
				null);
		
		//assertTrue(partyGETResponse.getobReadParty1().getData().getParty().getPartyId().isEmpty());
				

	}

}
