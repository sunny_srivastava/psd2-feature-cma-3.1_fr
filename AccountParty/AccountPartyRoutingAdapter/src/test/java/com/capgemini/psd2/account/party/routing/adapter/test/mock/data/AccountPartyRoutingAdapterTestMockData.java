/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.account.party.routing.adapter.test.mock.data;

import java.util.ArrayList;
import java.util.List;

import com.capgemini.psd2.aisp.domain.OBParty2;
import com.capgemini.psd2.aisp.domain.OBParty2Address;
import com.capgemini.psd2.aisp.domain.OBReadParty2;
import com.capgemini.psd2.aisp.domain.OBReadParty2Data;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountPartyResponse;


/**
 * The Class AccountBalanceRoutingAdapterTestMockData.
 */
public class AccountPartyRoutingAdapterTestMockData {

	public static PlatformAccountPartyResponse getMockPartyGETResponse() {
		PlatformAccountPartyResponse platformAccountPartyResponse = new PlatformAccountPartyResponse();
		OBReadParty2Data data = new OBReadParty2Data();
		OBParty2 oBParty = new OBParty2();
		data.setParty(oBParty);
		List<OBParty2Address> partyList = new ArrayList<>();
		OBParty2Address oBPostalAddress = new OBParty2Address();
		partyList.add(oBPostalAddress);
		oBParty.addAddressItem(oBPostalAddress);
		oBParty.setAddress(partyList);
		
		OBReadParty2 oBReadParty = new OBReadParty2();
		oBReadParty.setData(data);
		platformAccountPartyResponse.setoBReadParty2(oBReadParty);
		return platformAccountPartyResponse;
		}
	}
		
	
