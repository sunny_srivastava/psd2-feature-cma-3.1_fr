/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.account.party.routing.adapter.routing;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.aisp.adapter.AccountPartyAdapter;

/**
 * A factory for creating AccountBalanceCoreSystemAdapter objects.
 */
@Component
public class AccountPartyCoreSystemAdapterFactory implements ApplicationContextAware, AccountPartyAdapterFactory {

	/** The application context. */
	private ApplicationContext applicationContext;
	
	/* (non-Javadoc)
	 * @see com.capgemini.psd2.account.balance.routing.adapter.routing.AccountBalanceAdapterFactory#getAdapterInstance(java.lang.String)
	 */
	@Override
	public AccountPartyAdapter getAdapterInstance(String adapterName){
		return (AccountPartyAdapter) applicationContext.getBean(adapterName);		
	}
		
	/* (non-Javadoc)
	 * @see org.springframework.context.ApplicationContextAware#setApplicationContext(org.springframework.context.ApplicationContext)
	 */
	@Override
	public void setApplicationContext(ApplicationContext context) {
		  this.applicationContext = context;
	}
}
