/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.account.party.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.psd2.account.party.service.AccountPartyService;
import com.capgemini.psd2.aisp.domain.OBReadParty2;

/**
 * The Class AccountPartyController.
 */
@RestController
public class AccountPartyController {

	/** The account party service. */
	@Autowired
	private AccountPartyService accountPartyService;

	/**
	 * Retrieve account party.
	 *
	 * @param accountId the account id
	 * @return the party GET response
	 */
	@RequestMapping(value = "/accounts/{accountId}/party", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public OBReadParty2 retrieveAccountParty(@PathVariable("accountId") String accountId) {
		
		return accountPartyService.retrieveAccountParty(accountId);
	}
}
