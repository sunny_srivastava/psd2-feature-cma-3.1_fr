package com.capgemini.psd2.account.party.test.mock.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.capgemini.psd2.aisp.domain.Links;
import com.capgemini.psd2.aisp.domain.Meta;
import com.capgemini.psd2.aisp.domain.OBAddressTypeCode;
import com.capgemini.psd2.aisp.domain.OBExternalPartyType1Code;
import com.capgemini.psd2.aisp.domain.OBParty2;
import com.capgemini.psd2.aisp.domain.OBParty2Address;
import com.capgemini.psd2.aisp.domain.OBReadParty2;
import com.capgemini.psd2.aisp.domain.OBReadParty2Data;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountPartyResponse;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.consent.domain.AispConsent;
import com.capgemini.psd2.enums.ConsentStatusEnum;
import com.capgemini.psd2.logger.LoggerAttribute;
import com.capgemini.psd2.token.ConsentTokenData;
import com.capgemini.psd2.token.Token;

/**
 * The Class AccountDirectDebitsMockData.
 */
public class AccountPartyMockData {

	/** The mock token. */
	public static Token mockToken;

	/**
	 * Gets the token.
	 *
	 * @return the token
	 */
	public static Token getToken() {
		mockToken = new Token();
		ConsentTokenData consentTokenData = new ConsentTokenData();
		consentTokenData.setConsentExpiry("1509348259877L");
		consentTokenData.setConsentId("12345");
		mockToken.setConsentTokenData(consentTokenData);
		Map<String,String> map=new HashMap<>();
		mockToken.setSeviceParams(map);
		return mockToken;
	}

	/**
	 * Gets the mock account mapping.
	 *
	 * @return the mock account mapping
	 */
	public static AccountMapping getMockAccountMapping() {
		AccountMapping mapping = new AccountMapping();
		mapping.setTppCID("tpp123");
		mapping.setPsuId("user123");
		List<AccountDetails> selectedAccounts = new ArrayList<>();
		AccountDetails accountRequest = new AccountDetails();
		accountRequest.setAccountId("269c3ff5-d7f8-419b-a3b9-7136c5b4611a");
		accountRequest.setAccountNSC("SC802001");
		accountRequest.setAccountNumber("10203345");
		selectedAccounts.add(accountRequest);
		mapping.setAccountDetails(selectedAccounts);
		return mapping;
	}


	public static AispConsent getMockAispConsent() {
		AispConsent aispConsent = new AispConsent();
		aispConsent.setTppCId("6443e15975554bce8099e35b88b40465");
		aispConsent.setPsuId("88888888");
		aispConsent.setConsentId("39032481");
		aispConsent.setAccountRequestId("ae8c4441-f783-4e80-8810-254241bed98c");
		aispConsent.setChannelId("B365");
		aispConsent.setStartDate("2017-08-09T06:44:31.250Z");
		aispConsent.setEndDate("2018-09-02T00:00:00.875");
		aispConsent.setTransactionFromDateTime("2015-05-03T00:00:00.800");
		aispConsent.setTransactionToDateTime("2018-12-03T00:00:00.345");
		aispConsent.setStatus(ConsentStatusEnum.AUTHORISED);

		List<AccountDetails> selectedAispConsentAccountDetails = new ArrayList<>();
		AccountDetails accountRequest = new AccountDetails();

		accountRequest.setAccountId("f4483fda-81be-4873-b4a6-20375b7f55c1");
		accountRequest.setAccountNSC("903779");
		accountRequest.setAccountNumber("76528776");

		selectedAispConsentAccountDetails.add(accountRequest);
		aispConsent.setAccountDetails(selectedAispConsentAccountDetails);

		return aispConsent;
	}

	public static LoggerAttribute getMockLoggerData() {
		LoggerAttribute x = new LoggerAttribute();
		x.setApiId("testApiID");
		return x;
	}


	public static PlatformAccountPartyResponse getMockPlatformResponseWithoutLinksMeta() {
		PlatformAccountPartyResponse platformAccountPartyResponse = new PlatformAccountPartyResponse();
		OBReadParty2Data data = new OBReadParty2Data();
		
		OBParty2 party = new OBParty2();
		data.setParty(party);
		party.setPartyId("2345");
		party.setPartyNumber("024");
		party.setPartyType(OBExternalPartyType1Code.DELEGATE);
		party.setPhone("+91-8004142576");
		party.setName("Keli");
		party.setEmailAddress("keli.34@gmail.com");
		party.setMobile("+91-8004142576");
		List<OBParty2Address> partyList = new ArrayList<>();
		List<String> addressLines = new ArrayList<>();
		addressLines.add("KenCircle");
		OBParty2Address oBPartyAddress = new OBParty2Address();
		oBPartyAddress.setAddressLine(addressLines);
		oBPartyAddress.setAddressType(OBAddressTypeCode.BUSINESS);
		oBPartyAddress.setBuildingNumber("502");
		oBPartyAddress.setCountry("IN");
		oBPartyAddress.setCountrySubDivision("Atlas");
		oBPartyAddress.setPostCode("12345");
		oBPartyAddress.setStreetName("HighStreet");
		oBPartyAddress.setTownName("DownTown");
		data.getParty().addAddressItem(oBPartyAddress);
		partyList.add(oBPartyAddress);
		OBReadParty2 oBReadParty1 = new OBReadParty2();
		oBReadParty1.setData(data);
		platformAccountPartyResponse.setoBReadParty2(oBReadParty1);
		return platformAccountPartyResponse;
	}
	
	public static PlatformAccountPartyResponse getMockPlatformResponseWithLinksMeta() {
		PlatformAccountPartyResponse platformAccountPartyResponse = new PlatformAccountPartyResponse();
		OBReadParty2Data data = new OBReadParty2Data();
		
		OBParty2 party = new OBParty2();
		data.setParty(party);
		party.setPartyId("2345");
		party.setPartyNumber("024");
		party.setPartyType(OBExternalPartyType1Code.DELEGATE);
		party.setPhone("+91-8004142576");
		party.setName("Keli");
		party.setEmailAddress("keli.34@gmail.com");
		party.setMobile("+91-8004142576");
		List<OBParty2Address> partyList = new ArrayList<>();
		List<String> addressLines = new ArrayList<>();
		addressLines.add("KenCircle");
		OBParty2Address oBPartyAddress = new OBParty2Address();
		oBPartyAddress.setAddressLine(addressLines);
		oBPartyAddress.setAddressType(OBAddressTypeCode.BUSINESS);
		oBPartyAddress.setBuildingNumber("502");
		oBPartyAddress.setCountry("IN");
		oBPartyAddress.setCountrySubDivision("Atlas");
		oBPartyAddress.setPostCode("12345");
		oBPartyAddress.setStreetName("HighStreet");
		oBPartyAddress.setTownName("DownTown");
		data.getParty().addAddressItem(oBPartyAddress);
		partyList.add(oBPartyAddress);
		OBReadParty2 oBReadParty1 = new OBReadParty2();
		oBReadParty1.setData(data);				
		Links links = new Links();
		oBReadParty1.setLinks(links);
		Meta meta = new Meta();
		oBReadParty1.setMeta(meta);
		platformAccountPartyResponse.setoBReadParty2(oBReadParty1);
		return platformAccountPartyResponse;
	}
	
	public static OBReadParty2 getMockExpectedResponseWithoutLinkMeta() {
		OBReadParty2Data data = new OBReadParty2Data();
		OBParty2 party = new OBParty2();
		data.setParty(party);
		party.setPartyId("2345");
		party.setPartyNumber("024");
		party.setPartyType(OBExternalPartyType1Code.DELEGATE);
		party.setPhone("+91-8004142576");
		party.setName("Keli");
		party.setEmailAddress("keli.34@gmail.com");
		party.setMobile("+91-8004142576");
		List<OBParty2Address> partyList = new ArrayList<>();
		List<String> addressLines = new ArrayList<>();
		addressLines.add("KenCircle");
		OBParty2Address oBPartyAddress = new OBParty2Address();
		oBPartyAddress.setAddressLine(addressLines);
		oBPartyAddress.setAddressType(OBAddressTypeCode.BUSINESS);
		oBPartyAddress.setBuildingNumber("502");
		oBPartyAddress.setCountry("IN");
		oBPartyAddress.setCountrySubDivision("Atlas");
		oBPartyAddress.setPostCode("12345");
		oBPartyAddress.setStreetName("HighStreet");
		oBPartyAddress.setTownName("DownTown");
		data.getParty().addAddressItem(oBPartyAddress);
		partyList.add(oBPartyAddress);
		OBReadParty2 oBReadParty1 = new OBReadParty2();
		oBReadParty1.setData(data);
		Links links = new Links();
		Meta meta = new Meta();
		meta.setTotalPages(1);
		oBReadParty1.setLinks(links);
		oBReadParty1.setMeta(meta);
		return oBReadParty1;
	}
	public static OBReadParty2 getMockExpectedResponseWithLinksMeta() {
		OBReadParty2Data data = new OBReadParty2Data();
		OBParty2 party = new OBParty2();
		data.setParty(party);
		party.setPartyId("2345");
		party.setPartyNumber("024");
		party.setPartyType(OBExternalPartyType1Code.DELEGATE);
		party.setPhone("+91-8004142576");
		party.setName("Keli");
		party.setEmailAddress("keli.34@gmail.com");
		party.setMobile("+91-8004142576");
		List<OBParty2Address> partyList = new ArrayList<>();
		List<String> addressLines = new ArrayList<>();
		addressLines.add("KenCircle");
		OBParty2Address oBPartyAddress = new OBParty2Address();
		oBPartyAddress.setAddressLine(addressLines);
		oBPartyAddress.setAddressType(OBAddressTypeCode.BUSINESS);
		oBPartyAddress.setBuildingNumber("502");
		oBPartyAddress.setCountry("IN");
		oBPartyAddress.setCountrySubDivision("Atlas");
		oBPartyAddress.setPostCode("12345");
		oBPartyAddress.setStreetName("HighStreet");
		oBPartyAddress.setTownName("DownTown");
		data.getParty().addAddressItem(oBPartyAddress);
		partyList.add(oBPartyAddress);
		OBReadParty2 oBReadParty1 = new OBReadParty2();
		oBReadParty1.setData(data);
	Links links = new Links();
	Meta meta = new Meta();
	meta.setTotalPages(1);
	oBReadParty1.setLinks(links);
	oBReadParty1.setMeta(meta);
	return oBReadParty1;
}

}
