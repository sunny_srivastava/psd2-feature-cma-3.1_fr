package com.capgemini.psd2.revoke.consent.mongo.db.adapter.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.aisp.consent.adapter.impl.AispConsentAdapterImpl;
import com.capgemini.psd2.aisp.domain.OBReadConsentResponse1Data;
import com.capgemini.psd2.cisp.consent.adapter.impl.CispConsentAdapterImpl;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmationConsentResponse1Data;
import com.capgemini.psd2.consent.domain.AispConsent;
import com.capgemini.psd2.consent.domain.CispConsent;
import com.capgemini.psd2.consent.domain.PispConsent;
import com.capgemini.psd2.enums.ConsentStatusEnum;
import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.ErrorMapKeys;
import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.InternalServerErrorMessage;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.internal.apis.adapter.RevokeConsentAdapter;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.pisp.domain.OBExternalConsentStatus1Code;
import com.capgemini.psd2.pisp.domain.PaymentConsentsPlatformResource;
import com.capgemini.psd2.product.common.CompatibleVersionList;
import com.capgemini.psd2.revoke.consent.mongo.db.adapter.repository.RevokeAccountRequestCMA3Repository;
import com.capgemini.psd2.revoke.consent.mongo.db.adapter.repository.RevokeAccountRequestRepository;
import com.capgemini.psd2.revoke.consent.mongo.db.adapter.repository.RevokeFundsConfirmationConsentRepository;
import com.capgemini.psd2.utilities.DateUtilites;
import com.mongodb.WriteResult;

@Component("revokeConsentMongoDbAdaptor")
public class RevokeConsentMongoDbAdaptorImpl implements RevokeConsentAdapter {

	@Autowired
	private RevokeAccountRequestCMA3Repository accountRequestRepository;

	@Autowired
	private RevokeAccountRequestRepository accountRequestRepositoryV2;
	
	@Autowired
	private RevokeFundsConfirmationConsentRepository fundsConfirmationConsentRepository;

	@Autowired
	private AispConsentAdapterImpl aispConsentAdapter;
	
	@Autowired
	private CispConsentAdapterImpl cispConsentAdapter;

	@Autowired
	private MongoTemplate mongoTemplate;

	private Boolean resourceLocated;
	
	@Autowired
	private CompatibleVersionList compatibleVersionList;

	@Override
	public void revokeConsentRequest(String consentId, String cId) {
		resourceLocated = false;
		resourceLocated = fetchAndUpdateAisp(consentId, cId, resourceLocated);
		if (resourceLocated == false)
			resourceLocated = fetchAndUpdateCisp(consentId, cId, resourceLocated);

		if (resourceLocated == false) {
			//When consentId is in valid format but does not exist in the database
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.RESOURCE_DOES_NOT_EXIST);
		}

	}

	private Boolean fetchAndUpdateCisp(String consentId, String cId, Boolean resourceLocated2) {
		CispConsent cispConsent = null;
		
		OBFundsConfirmationConsentResponse1Data fundsConfirmationConsentResponse = fundsConfirmationConsentRepository
				.findByConsentIdAndCmaVersionIn(consentId, compatibleVersionList.fetchVersionList());
		if (fundsConfirmationConsentResponse != null) {
			if (fundsConfirmationConsentResponse
					.getStatus() == OBFundsConfirmationConsentResponse1Data.StatusEnum.AUTHORISED) {
				resourceLocated = Boolean.TRUE;
			} else if (fundsConfirmationConsentResponse
					.getStatus() == OBFundsConfirmationConsentResponse1Data.StatusEnum.REVOKED) {
				throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(
						OBErrorCodeEnum.UK_OBIE_RESOURCE_REVOKEDSTATUS, ErrorMapKeys.RESOURCE_REVOKEDSTATUS));
			} else {
				throw PSD2Exception
						.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_RESOURCE_CONSENTSTATUSNOTAUTHORISED,
								ErrorMapKeys.RESOURCE_CONSENTSTATUSNOTAUTHORISED));
			}
		}
		
		if (resourceLocated) {
			if (fundsConfirmationConsentResponse != null) {
				fundsConfirmationConsentResponse
						.setStatus(OBFundsConfirmationConsentResponse1Data.StatusEnum.REVOKED);
				fundsConfirmationConsentResponse.setStatusUpdateDateTime(DateUtilites.getCurrentDateInISOFormat());
				try {
					fundsConfirmationConsentResponse = fundsConfirmationConsentRepository
							.save(fundsConfirmationConsentResponse);
				} catch (DataAccessResourceFailureException e) {
					throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
							InternalServerErrorMessage.CONNECTION_ERROR));
				}

				cispConsent = cispConsentAdapter.retrieveConsentByFundsIntentId(consentId);
				if (cispConsent != null) {
					if (cispConsent.getStatus() == ConsentStatusEnum.AUTHORISED) {
						cispConsentAdapter.updateConsentStatus(consentId, ConsentStatusEnum.REVOKED);
					} else if (cispConsent.getStatus() == ConsentStatusEnum.REVOKED) {
						throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(
								OBErrorCodeEnum.UK_OBIE_RESOURCE_REVOKEDSTATUS, ErrorMapKeys.RESOURCE_REVOKEDSTATUS));
					} else {
						throw PSD2Exception.populatePSD2Exception(
								new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_RESOURCE_CONSENTSTATUSNOTAUTHORISED,
										ErrorMapKeys.RESOURCE_CONSENTSTATUSNOTAUTHORISED));
					}
				}
			}
		}
		return resourceLocated;
	}

	private Boolean fetchAndUpdatePisp(String consentId, String cId, Boolean resourceLocated2) {

		Query query = new Query();
		Criteria criteria = new Criteria().orOperator(Criteria.where("paymentConsentId").is(consentId),
				Criteria.where("paymentId").is(consentId));
		query.addCriteria(criteria);

		Update update = new Update();
		update.set("status", OBExternalConsentStatus1Code.REJECTED);
		update.set("statusUpdateDateTime", DateUtilites.getCurrentDateInISOFormat());
		update.set("statusUpdatedDescription", PSD2Constants.REVOKED_REASON);

		WriteResult result = mongoTemplate.updateFirst(query, update, PaymentConsentsPlatformResource.class);

		if (result.getN() != 0) {
			resourceLocated = true;
			query = new Query(Criteria.where("paymentId").is(consentId));
			update = new Update();
			update.set("status", ConsentStatusEnum.REVOKED);

			mongoTemplate.updateFirst(query, update, PispConsent.class);

		}
		return resourceLocated;
	}

	private Boolean fetchAndUpdateAisp(String consentId, String cId, Boolean resourceLocated2) {
			OBReadConsentResponse1Data data = fetchAndUpdateAispSetup(consentId, cId);
			if (data != null) {
				/* Update status in AISP Consent */
				fetchAndUpdateAispConsent(consentId, cId);
				resourceLocated = true;
			}
		
		return resourceLocated;
	}

	private OBReadConsentResponse1Data fetchAndUpdateAispSetup(String consentId, String cid) {
		OBReadConsentResponse1Data data = null;
		OBReadConsentResponse1Data dataV2 = null;
		try {
			data = accountRequestRepository.findByConsentIdAndCmaVersionIn(consentId,
					compatibleVersionList.fetchVersionList());

			if (data != null) {
				if (data.getStatus() == OBReadConsentResponse1Data.StatusEnum.AUTHORISED) {
					resourceLocated = Boolean.TRUE;
				} else if (data.getStatus() == OBReadConsentResponse1Data.StatusEnum.REVOKED) {
					throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(
							OBErrorCodeEnum.UK_OBIE_RESOURCE_REVOKEDSTATUS, ErrorMapKeys.RESOURCE_REVOKEDSTATUS));
				} else {
					throw PSD2Exception.populatePSD2Exception(
							new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_RESOURCE_CONSENTSTATUSNOTAUTHORISED,
									ErrorMapKeys.RESOURCE_CONSENTSTATUSNOTAUTHORISED));
				}
			} else if (data == null && !resourceLocated) {
				dataV2 = accountRequestRepositoryV2.findByAccountRequestIdAndCmaVersionIn(consentId,
						compatibleVersionList.fetchVersionList());

				if (dataV2 != null) {
					if (dataV2.getStatus() == OBReadConsentResponse1Data.StatusEnum.AUTHORISED) {
						data = transformAccountInformation(dataV2, new OBReadConsentResponse1Data(), null);
						resourceLocated = Boolean.TRUE;
					} else if (dataV2.getStatus() == OBReadConsentResponse1Data.StatusEnum.REVOKED) {
						throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(
								OBErrorCodeEnum.UK_OBIE_RESOURCE_REVOKEDSTATUS, ErrorMapKeys.RESOURCE_REVOKEDSTATUS));
					} else {
						throw PSD2Exception.populatePSD2Exception(
								new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_RESOURCE_CONSENTSTATUSNOTAUTHORISED,
										ErrorMapKeys.RESOURCE_CONSENTSTATUSNOTAUTHORISED));
					}
				}
			}
		} catch (DataAccessResourceFailureException e) {
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
					InternalServerErrorMessage.CONNECTION_ERROR));
		}

		if (resourceLocated) {
			if (data != null) {
				data.setStatus(OBReadConsentResponse1Data.StatusEnum.REVOKED);
				data.setStatusUpdateDateTime(DateUtilites.getCurrentDateInISOFormat());
			}
			try {
				data = accountRequestRepository.save(data);
			} catch (DataAccessResourceFailureException e) {
				throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
						InternalServerErrorMessage.CONNECTION_ERROR));
			}
		}
		return data;
	}

	private void fetchAndUpdateAispConsent(String consentId, String cid) {
		AispConsent consentData = null;
		consentData = aispConsentAdapter.retrieveConsentByAccountRequestId(consentId);

		if (consentData != null) {
			if(consentData.getStatus() == ConsentStatusEnum.AUTHORISED) {
				consentData.setStatus(ConsentStatusEnum.REVOKED);
				aispConsentAdapter.updateConsent(consentData);
			}else if (consentData.getStatus() == ConsentStatusEnum.REVOKED) {
				throw PSD2Exception.populatePSD2Exception(
						new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_RESOURCE_REVOKEDSTATUS, 
								ErrorMapKeys.RESOURCE_REVOKEDSTATUS));
			}else{
				throw PSD2Exception.populatePSD2Exception(
						new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_RESOURCE_CONSENTSTATUSNOTAUTHORISED, 
								ErrorMapKeys.RESOURCE_CONSENTSTATUSNOTAUTHORISED));
			}
		}
	}

	private <T> OBReadConsentResponse1Data transformAccountInformation(T source, OBReadConsentResponse1Data destination,
			Map<String, String> params) {
		OBReadConsentResponse1Data accCma2Response = (OBReadConsentResponse1Data) source;
		destination.setCmaVersion(accCma2Response.getCmaVersion());
		destination.setConsentId(accCma2Response.getAccountRequestId());
		destination.setCreationDateTime(accCma2Response.getCreationDateTime());
		destination.setExpirationDateTime(accCma2Response.getExpirationDateTime());
		destination.setStatusUpdateDateTime(accCma2Response.getStatusUpdateDateTime());
		destination.setTransactionFromDateTime(accCma2Response.getTransactionFromDateTime());
		destination.setTransactionToDateTime(accCma2Response.getTransactionToDateTime());
		List<OBReadConsentResponse1Data.PermissionsEnum> persmissions = new ArrayList<>();
		for (OBReadConsentResponse1Data.PermissionsEnum permission : accCma2Response.getPermissions()) {
			persmissions.add(OBReadConsentResponse1Data.PermissionsEnum.fromValue(permission.toString()));
		}
		destination.setPermissions(persmissions);
		destination.setStatus(OBReadConsentResponse1Data.StatusEnum.fromValue(accCma2Response.getStatus().toString()));
		return destination;
	}

}