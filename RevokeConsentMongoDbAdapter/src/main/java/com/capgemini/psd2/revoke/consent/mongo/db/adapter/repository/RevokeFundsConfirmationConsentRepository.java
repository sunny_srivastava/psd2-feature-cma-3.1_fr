package com.capgemini.psd2.revoke.consent.mongo.db.adapter.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.capgemini.psd2.cisp.domain.OBFundsConfirmationConsentResponse1Data;

public interface RevokeFundsConfirmationConsentRepository extends MongoRepository<OBFundsConfirmationConsentResponse1Data, String> {

	public OBFundsConfirmationConsentResponse1Data findByConsentIdAndCmaVersionIn(String consentId, List<String> cmaVersionList);
}
