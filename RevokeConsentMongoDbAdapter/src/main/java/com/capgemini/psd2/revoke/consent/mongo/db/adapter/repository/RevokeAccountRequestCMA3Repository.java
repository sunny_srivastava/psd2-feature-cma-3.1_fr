package com.capgemini.psd2.revoke.consent.mongo.db.adapter.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.capgemini.psd2.aisp.domain.OBReadConsentResponse1Data;

public interface RevokeAccountRequestCMA3Repository extends MongoRepository<OBReadConsentResponse1Data, String> {

	public OBReadConsentResponse1Data findByConsentIdAndCmaVersionIn(String accountId, List<String> cmaVersionList);
	
}
