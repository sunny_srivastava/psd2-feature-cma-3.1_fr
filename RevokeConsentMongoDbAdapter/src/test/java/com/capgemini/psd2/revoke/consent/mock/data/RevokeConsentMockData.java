package com.capgemini.psd2.revoke.consent.mock.data;

import java.util.ArrayList;
import java.util.List;

import com.capgemini.psd2.aisp.domain.OBReadConsentResponse1Data;
import com.capgemini.psd2.aisp.domain.OBReadConsentResponse1Data.StatusEnum;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmationConsentResponse1Data;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AispConsent;
import com.capgemini.psd2.consent.domain.CispConsent;
import com.capgemini.psd2.enums.ConsentStatusEnum;
import com.mongodb.WriteResult;

public class RevokeConsentMockData {

	public static OBReadConsentResponse1Data getAccountRequestMockDataV3() {
		OBReadConsentResponse1Data oBReadConsentResponse1Data = new OBReadConsentResponse1Data();
		oBReadConsentResponse1Data.setCmaVersion("3.0");
		oBReadConsentResponse1Data.setConsentId("12345");
		oBReadConsentResponse1Data.setStatus(OBReadConsentResponse1Data.StatusEnum.AUTHORISED);
		oBReadConsentResponse1Data.setTenantId("BOIUK");
		oBReadConsentResponse1Data.setTppCID("12345");
		oBReadConsentResponse1Data.setTppLegalEntityName("Test");

		return oBReadConsentResponse1Data;
	}
	
	public static OBFundsConfirmationConsentResponse1Data getCispMockDataV3() {
		OBFundsConfirmationConsentResponse1Data consentDataResponse1 =  new OBFundsConfirmationConsentResponse1Data();
		consentDataResponse1.setCmaVersion("3.0");
		consentDataResponse1.setConsentId("abcd1234");
		consentDataResponse1.setStatus(OBFundsConfirmationConsentResponse1Data.StatusEnum.AUTHORISED);
		consentDataResponse1.setTenantId("BOIUK");
		consentDataResponse1.setTppCID("12345");
		consentDataResponse1.setTppLegalEntityName("Test");
		return consentDataResponse1;
	}
	

	public static OBReadConsentResponse1Data getAccountRequestMockDataV2() {
		OBReadConsentResponse1Data oBReadDataResponse1 = new OBReadConsentResponse1Data();
		oBReadDataResponse1.setCmaVersion("3.0");
		oBReadDataResponse1.setAccountRequestId("12345");
		oBReadDataResponse1.setStatus(StatusEnum.AUTHORISED);
		oBReadDataResponse1.setTppCID("12345");
		oBReadDataResponse1.setTppLegalEntityName("Test");

		return oBReadDataResponse1;
	}
	
	public static AispConsent getMockAispConsentData() {
		AispConsent mockConsent = new AispConsent();

		mockConsent.setTppCId("12345");
		mockConsent.setPsuId("user123");
		mockConsent.setConsentId("12345");

		List<AccountDetails> selectedAccounts = new ArrayList<>();

		AccountDetails accountRequest1 = new AccountDetails();
		accountRequest1.setAccountId("12345");
		accountRequest1.setAccountNSC("1234");
		accountRequest1.setAccountNumber("5678");

		AccountDetails accountRequest2 = new AccountDetails();
		accountRequest2.setAccountId("4321");
		accountRequest2.setAccountNSC("8765");
		accountRequest2.setAccountNumber("GB19LOYD30961799709943");
		selectedAccounts.add(accountRequest1);
		selectedAccounts.add(accountRequest2);
		mockConsent.setAccountDetails(selectedAccounts);
		mockConsent.setAccountRequestId("12345");

		return mockConsent;
	}
	
	public static AispConsent getMockAispConsentDataAuthorised() {
		AispConsent mockConsent = new AispConsent();

		mockConsent.setTppCId("12345");
		mockConsent.setPsuId("user123");
		mockConsent.setConsentId("12345");
		mockConsent.setStatus(ConsentStatusEnum.AUTHORISED);

		List<AccountDetails> selectedAccounts = new ArrayList<>();

		AccountDetails accountRequest1 = new AccountDetails();
		accountRequest1.setAccountId("12345");
		accountRequest1.setAccountNSC("1234");
		accountRequest1.setAccountNumber("5678");

		AccountDetails accountRequest2 = new AccountDetails();
		accountRequest2.setAccountId("4321");
		accountRequest2.setAccountNSC("8765");
		accountRequest2.setAccountNumber("GB19LOYD30961799709943");
		selectedAccounts.add(accountRequest1);
		selectedAccounts.add(accountRequest2);
		mockConsent.setAccountDetails(selectedAccounts);
		mockConsent.setAccountRequestId("12345");

		return mockConsent;
	}
	
	public static AispConsent getMockAispConsentDataRevoked() {
		AispConsent mockConsent = new AispConsent();

		mockConsent.setTppCId("12345");
		mockConsent.setPsuId("user123");
		mockConsent.setConsentId("12345");
		mockConsent.setStatus(ConsentStatusEnum.REVOKED);

		List<AccountDetails> selectedAccounts = new ArrayList<>();

		AccountDetails accountRequest1 = new AccountDetails();
		accountRequest1.setAccountId("12345");
		accountRequest1.setAccountNSC("1234");
		accountRequest1.setAccountNumber("5678");

		AccountDetails accountRequest2 = new AccountDetails();
		accountRequest2.setAccountId("4321");
		accountRequest2.setAccountNSC("8765");
		accountRequest2.setAccountNumber("GB19LOYD30961799709943");
		selectedAccounts.add(accountRequest1);
		selectedAccounts.add(accountRequest2);
		mockConsent.setAccountDetails(selectedAccounts);
		mockConsent.setAccountRequestId("12345");

		return mockConsent;
	}
	
	public static CispConsent getMockCispConsentDataAuthorised() {
		CispConsent mockConsent = new CispConsent();

		mockConsent.setTppCId("12345");
		mockConsent.setPsuId("user123");
		mockConsent.setConsentId("12345");
		mockConsent.setStatus(ConsentStatusEnum.AUTHORISED);

		AccountDetails accountRequest1 = new AccountDetails();
		accountRequest1.setAccountId("12345");
		accountRequest1.setAccountNSC("1234");
		accountRequest1.setAccountNumber("5678");

		mockConsent.setAccountDetails(accountRequest1);
		return mockConsent;
	}
	
	public static WriteResult getWriteResult() {
		boolean updateOfExisting = true;
		Object upsertedId = "";
		int n = 1;
		WriteResult writeResult = new WriteResult(n, updateOfExisting, upsertedId);
		return writeResult;
	}
	
	public static WriteResult getWriteResultWithNoN() {
		boolean updateOfExisting = true;
		Object upsertedId = "";
		int n = 0;
		WriteResult writeResult = new WriteResult(n, updateOfExisting, upsertedId);
		return writeResult;
	}

}
