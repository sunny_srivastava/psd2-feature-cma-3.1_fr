package com.capgemini.psd2.revoke.consent.test;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import com.capgemini.psd2.aisp.consent.adapter.impl.AispConsentAdapterImpl;
import com.capgemini.psd2.aisp.domain.OBReadConsentResponse1Data;
import com.capgemini.psd2.cisp.consent.adapter.impl.CispConsentAdapterImpl;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmationConsentResponse1Data;
import com.capgemini.psd2.consent.domain.PispConsent;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.pisp.domain.PaymentConsentsPlatformResource;
import com.capgemini.psd2.product.common.CompatibleVersionList;
import com.capgemini.psd2.revoke.consent.mock.data.RevokeConsentMockData;
import com.capgemini.psd2.revoke.consent.mongo.db.adapter.impl.RevokeConsentMongoDbAdaptorImpl;
import com.capgemini.psd2.revoke.consent.mongo.db.adapter.repository.RevokeAccountRequestCMA3Repository;
import com.capgemini.psd2.revoke.consent.mongo.db.adapter.repository.RevokeAccountRequestRepository;
import com.capgemini.psd2.revoke.consent.mongo.db.adapter.repository.RevokeFundsConfirmationConsentRepository;

public class RevokeConsentMongoDbAdaptorImplTest {

	@Mock
	private RevokeAccountRequestCMA3Repository accountRequestRepository;

	@Mock
	private RevokeAccountRequestRepository accountRequestRepositoryV2;

	@Mock
	private RevokeFundsConfirmationConsentRepository fundsConfirmationConsentRepository;
	
	@Mock
	private AispConsentAdapterImpl aispConsentAdapter;
	
	@Mock
	private CispConsentAdapterImpl cispConsentAdapter;

	@Mock
	private MongoTemplate mongoTemplate;
	
	@Mock
	private CompatibleVersionList compatibleVersionList;
	
	@InjectMocks
	private RevokeConsentMongoDbAdaptorImpl revokeConsentMongoDbAdaptorImpl;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void testRevokeConsentRequestAispV3Setup() {
		when(accountRequestRepository.findByConsentIdAndCmaVersionIn(anyString(), any())).thenReturn(RevokeConsentMockData.getAccountRequestMockDataV3());
		when(accountRequestRepository.save(any(OBReadConsentResponse1Data.class))).thenReturn(RevokeConsentMockData.getAccountRequestMockDataV3());
		when(aispConsentAdapter.retrieveConsentByAccountRequestId(anyString())).thenReturn(RevokeConsentMockData.getMockAispConsentDataAuthorised());
		revokeConsentMongoDbAdaptorImpl.revokeConsentRequest("12345","12345");
	}
	
	@Test()
	public void testRevokeConsentRequestAispV2Setup_ConsentAuthorised() {
		when(accountRequestRepository.findByConsentIdAndCmaVersionIn(anyString(), any())).thenReturn(null);
		when(accountRequestRepositoryV2.findByAccountRequestIdAndCmaVersionIn(anyString(), any())).thenReturn(RevokeConsentMockData.getAccountRequestMockDataV2());
		when(accountRequestRepository.save(any(OBReadConsentResponse1Data.class))).thenReturn(RevokeConsentMockData.getAccountRequestMockDataV3());
		when(aispConsentAdapter.retrieveConsentByAccountRequestId(anyString())).thenReturn(RevokeConsentMockData.getMockAispConsentDataAuthorised());
		revokeConsentMongoDbAdaptorImpl.revokeConsentRequest("12345","12345");
	}
	
	@Test(expected=PSD2Exception.class)
	public void testRevokeConsentRequestAispV2Setup_ConsentRevoked() {
		when(accountRequestRepository.findByConsentIdAndCmaVersionIn(anyString(), any())).thenReturn(null);
		when(accountRequestRepositoryV2.findByAccountRequestIdAndCmaVersionIn(anyString(), any())).thenReturn(RevokeConsentMockData.getAccountRequestMockDataV2());
		when(accountRequestRepository.save(any(OBReadConsentResponse1Data.class))).thenReturn(RevokeConsentMockData.getAccountRequestMockDataV3());
		when(aispConsentAdapter.retrieveConsentByAccountRequestId(anyString())).thenReturn(RevokeConsentMockData.getMockAispConsentDataRevoked());
		revokeConsentMongoDbAdaptorImpl.revokeConsentRequest("12345","12345");
	}
	
	@Test(expected=PSD2Exception.class)
	public void testRevokeConsentRequestAispV2Setup_InvalidConsentStatus() {
		when(accountRequestRepository.findByConsentIdAndCmaVersionIn(anyString(), any())).thenReturn(null);
		when(accountRequestRepositoryV2.findByAccountRequestIdAndCmaVersionIn(anyString(), any())).thenReturn(RevokeConsentMockData.getAccountRequestMockDataV2());
		when(accountRequestRepository.save(any(OBReadConsentResponse1Data.class))).thenReturn(RevokeConsentMockData.getAccountRequestMockDataV3());
		when(aispConsentAdapter.retrieveConsentByAccountRequestId(anyString())).thenReturn(RevokeConsentMockData.getMockAispConsentData());
		revokeConsentMongoDbAdaptorImpl.revokeConsentRequest("12345","12345");
	}
	
	@SuppressWarnings("unchecked")
	@Test(expected=PSD2Exception.class)
	public void testRevokeConsentRequestAispV2SetupDataResourceException() {
		when(accountRequestRepository.findByConsentIdAndCmaVersionIn(anyString(), any())).thenReturn(null);
		when(accountRequestRepositoryV2.findByAccountRequestIdAndCmaVersionIn(anyString(), any())).thenReturn(RevokeConsentMockData.getAccountRequestMockDataV2());
		when(accountRequestRepository.save(any(OBReadConsentResponse1Data.class))).thenThrow(DataAccessResourceFailureException.class);
		when(aispConsentAdapter.retrieveConsentByAccountRequestId(anyString())).thenReturn(RevokeConsentMockData.getMockAispConsentData());
		revokeConsentMongoDbAdaptorImpl.revokeConsentRequest("12345","12345");
	}
	
	@Test(expected=PSD2Exception.class)
	public void testRevokeConsentRequestPisp() {
		when(accountRequestRepository.findByConsentIdAndCmaVersionIn(anyString(), any())).thenReturn(null);
		when(accountRequestRepositoryV2.findByAccountRequestIdAndCmaVersionIn(anyString(), any())).thenReturn(null);
		when(aispConsentAdapter.retrieveConsentByAccountRequestId(anyString())).thenReturn(RevokeConsentMockData.getMockAispConsentData());
		when(mongoTemplate.updateFirst(any(Query.class), any(Update.class), eq(PaymentConsentsPlatformResource.class))).thenReturn(RevokeConsentMockData.getWriteResult());
		when(mongoTemplate.updateFirst(any(Query.class), any(Update.class), eq(PispConsent.class))).thenReturn(RevokeConsentMockData.getWriteResult());
		revokeConsentMongoDbAdaptorImpl.revokeConsentRequest("f68b342f-49c7-4732-a221-5bb931c56315","12345");
	}
	
	@Test(expected=PSD2Exception.class)
	public void testRevokeConsentRequestCisp() {
		when(fundsConfirmationConsentRepository.findByConsentIdAndCmaVersionIn(anyString(), any())).thenReturn(null);
		when(fundsConfirmationConsentRepository.save(any(OBFundsConfirmationConsentResponse1Data.class))).thenReturn(RevokeConsentMockData.getCispMockDataV3());
		when(cispConsentAdapter.retrieveConsentByFundsIntentId(anyString())).thenReturn(RevokeConsentMockData.getMockCispConsentDataAuthorised());
		when(mongoTemplate.updateFirst(any(Query.class), any(Update.class), eq(PaymentConsentsPlatformResource.class))).thenReturn(RevokeConsentMockData.getWriteResult());
		when(mongoTemplate.updateFirst(any(Query.class), any(Update.class), eq(PispConsent.class))).thenReturn(RevokeConsentMockData.getWriteResult());
		revokeConsentMongoDbAdaptorImpl.revokeConsentRequest("f68b342f-49c7-4732-a221-5bb931c56315","12345");
	}
	
	@Test(expected=PSD2Exception.class)
	public void testRevokeConsentRequestResourceNotFoundException() {
		when(accountRequestRepository.findByConsentIdAndCmaVersionIn(anyString(), any())).thenReturn(null);
		when(accountRequestRepositoryV2.findByAccountRequestIdAndCmaVersionIn(anyString(), any())).thenReturn(null);
		when(aispConsentAdapter.retrieveConsentByAccountRequestId(anyString())).thenReturn(RevokeConsentMockData.getMockAispConsentData());
		when(mongoTemplate.updateFirst(any(Query.class), any(Update.class), eq(PaymentConsentsPlatformResource.class))).thenReturn(RevokeConsentMockData.getWriteResultWithNoN());
		when(mongoTemplate.updateFirst(any(Query.class), any(Update.class), eq(OBFundsConfirmationConsentResponse1Data.class))).thenReturn(RevokeConsentMockData.getWriteResultWithNoN());
		revokeConsentMongoDbAdaptorImpl.revokeConsentRequest("12345","12345");
	}
	
}
