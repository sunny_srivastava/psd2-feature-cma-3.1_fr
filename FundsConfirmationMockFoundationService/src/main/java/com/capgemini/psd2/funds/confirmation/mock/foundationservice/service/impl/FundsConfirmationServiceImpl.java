/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.funds.confirmation.mock.foundationservice.service.impl;

import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.util.Calendar;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.capgemini.psd2.foundationservice.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.foundationservice.exceptions.MockFoundationServiceException;
import com.capgemini.psd2.funds.confirmation.mock.foundationservice.domain.AvailableFunds;

import com.capgemini.psd2.funds.confirmation.mock.foundationservice.repository.FundsConfirmationRepository;
import com.capgemini.psd2.funds.confirmation.mock.foundationservice.service.FundsConfirmationService;

/**
 * The Class AccountInformationServiceImpl.
 */
@Service
public class FundsConfirmationServiceImpl implements FundsConfirmationService {

	/** The repository. */
	@Autowired
	private FundsConfirmationRepository repository;

	/* (non-Javadoc)
	 * @see com.capgemini.psd2.account.information.mock.foundationservice.service.AccountInformationService#retrieveAccountInformation(java.lang.String, java.lang.String)
	 */
	@Override
	public AvailableFunds retrieveAvailableFunds(String accountNumber,String accountNSC) /*throws Exception*/  {

		AvailableFunds accountBaseType= repository.findByAccountNumberAndParentNationalSortCodeNSCNumber(accountNumber, accountNSC);
		//AvailableFunds accountBaseType= new AvailableFunds();
//		Account accountBaseType = new Account();
//		
//		accountBaseType.setAccountName("Abc");
//		accountBaseType.setAccountNumber("123");
		//abt.setRetailNonRetailCode(RetailNonRetailCode.RETAIL);
		
	//	repository.save(accountBaseType);
		
    if (accountBaseType == null) {
    	throw MockFoundationServiceException
		.populateMockFoundationServiceException(ErrorCodeEnum.NO_ACOUNT_FOUND_ACC_PAC);
	}
    accountBaseType.setAvailableFundsCheckDatetime(OffsetDateTime.now());
    
   /* accountBaseType.setAccountNumber(accountNumber);
    accountBaseType.setParentNationalSortCodeNSCNumber(accountNSC);
    repository.save(accountBaseType);*/
		return accountBaseType;

	}

/*	@Override
	public Account retrieveAccountInformationRaml(String creditCardNumber, String accountNumber) throws Exception {
		
		Account accnt = repository.findByAccountNumber(accountNumber);
		
		if (accnt == null) {
			throw MockFoundationServiceException
			.populateMockFoundationServiceException(ErrorCodeEnum.NO_ACOUNT_FOUND_ACC_PAC);
		}
		

		return accnt;
	}
*/
}
