package com.capgemini.psd2.international.payment.boi.foundationservice.adapter.test;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.psd2.foundationservice.international.payments.boi.adapter.InternationalPaymentsFoundationServiceAdapter;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.pisp.domain.CustomIPaymentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.OBTransactionIndividualStatus1Code;
import com.capgemini.psd2.pisp.domain.PaymentInternationalSubmitPOST201Response;

@SpringBootApplication
@ComponentScan("com.capgemini.psd2")
public class InternationalPaymentsFoundationServiceAdapterTestApplication {
	public static void main(String[] args) {
		SpringApplication.run(InternationalPaymentsFoundationServiceAdapterTestApplication.class, args);
	}
}


@RestController
@ResponseBody
class TestInternationalPaymentsFoundationServiceAdapter{
	
	@Autowired
	private InternationalPaymentsFoundationServiceAdapter internationalPaymentsFoundationServiceAdapter;
	
	@RequestMapping("/retrieveInternationalPayment")
	public PaymentInternationalSubmitPOST201Response retrieveInternationalPayment(){
		return null;
	}
	
	@RequestMapping(value = "/postexecuteInternationalPayment", method = RequestMethod.POST)
	public ResponseEntity<PaymentInternationalSubmitPOST201Response> executeInternationalPayment(
			@RequestBody CustomIPaymentsPOSTRequest request){
		
		Map<String, String> params = new HashMap<String, String>();
		params.put("consentFlowType", "PISP");
		params.put("x-channel-id", "BOL");
		params.put("x-user-id", "BOI999");
		params.put("X-BOI-PLATFORM", "platform");
		params.put("x-fapi-interaction-id", "87654321");
		params.put("X-API-SOURCE-SYSTEM", "sdsa");
		params.put("tenant_id", "BOIUK");
		params.put("X-API-CHANNEL-BRAND", "channelBrand");
		params.put(PSD2Constants.USER_IN_REQ_HEADER,"45");
		params.put(PSD2Constants.CORRELATION_REQ_HEADER,"145");
		params.put(PSD2Constants.CHANNEL_IN_REQ_HEADER,"42345");
		params.put(PSD2Constants.TENANT_ID,"BOIROI");
		
		Map<String, OBTransactionIndividualStatus1Code> paymentStatusMap = new HashMap<String, OBTransactionIndividualStatus1Code>();
		
		PaymentInternationalSubmitPOST201Response submissionResponse = internationalPaymentsFoundationServiceAdapter.processPayments(request, paymentStatusMap, params);
		
		return new ResponseEntity<PaymentInternationalSubmitPOST201Response>(submissionResponse, HttpStatus.CREATED);

	}
	
	
}