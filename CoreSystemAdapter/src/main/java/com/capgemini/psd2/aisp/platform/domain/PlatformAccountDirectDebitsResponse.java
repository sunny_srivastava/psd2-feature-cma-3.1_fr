package com.capgemini.psd2.aisp.platform.domain;

import java.util.Map;

import com.capgemini.psd2.aisp.domain.OBReadDirectDebit2;

public class PlatformAccountDirectDebitsResponse {

	// CMA2 response
	private OBReadDirectDebit2 oBReadDirectDebit2;

	// Additional Information
	private Map<String, String> additionalInformation;

	public OBReadDirectDebit2 getoBReadDirectDebit2() {
		return oBReadDirectDebit2;
	}

	public void setoBReadDirectDebit2(OBReadDirectDebit2 oBReadDirectDebit2) {
		this.oBReadDirectDebit2 = oBReadDirectDebit2;
	}

	public Map<String, String> getAdditionalInformation() {
		return additionalInformation;
	}

	public void setAdditionalInformation(Map<String, String> additionalInformation) {
		this.additionalInformation = additionalInformation;
	}
}
