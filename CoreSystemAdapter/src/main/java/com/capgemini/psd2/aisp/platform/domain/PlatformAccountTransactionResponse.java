package com.capgemini.psd2.aisp.platform.domain;

import java.util.Map;

import com.capgemini.psd2.aisp.domain.OBReadTransaction6;

public class PlatformAccountTransactionResponse {

	// CMA2 response
	private OBReadTransaction6 oBReadTransaction6;

	// Additional Information
	private Map<String, String> additionalInformation;

	public OBReadTransaction6 getoBReadTransaction6() {
		return oBReadTransaction6;
	}

	public void setoBReadTransaction6(OBReadTransaction6 oBReadTransaction6) {
		this.oBReadTransaction6 = oBReadTransaction6;
	}

	public Map<String, String> getAdditionalInformation() {
		return additionalInformation;
	}

	public void setAdditionalInformation(Map<String, String> additionalInformation) {
		this.additionalInformation = additionalInformation;
	}
}
