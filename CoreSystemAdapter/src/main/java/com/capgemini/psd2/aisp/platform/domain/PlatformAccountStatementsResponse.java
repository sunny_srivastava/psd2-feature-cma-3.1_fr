package com.capgemini.psd2.aisp.platform.domain;

import java.util.Map;

import com.capgemini.psd2.aisp.domain.OBReadStatement2;


public class PlatformAccountStatementsResponse {
	
	// CMA2 response
		private OBReadStatement2 oBReadStatement2;

		// Additional Information
		private Map<String, String> additionalInformation;

		public OBReadStatement2 getoBReadStatement2() {
			return oBReadStatement2;
		}

		public void setoBReadStatement2(OBReadStatement2 oBReadStatement2) {
			this.oBReadStatement2 = oBReadStatement2;
		}

		public Map<String, String> getAdditionalInformation() {
			return additionalInformation;
		}

		public void setAdditionalInformation(Map<String, String> additionalInformation) {
			this.additionalInformation = additionalInformation;
		}

}
