package com.capgemini.psd2.aisp.mongo.db.adapter.domain;

import org.springframework.data.mongodb.core.mapping.Document;

import com.capgemini.psd2.aisp.domain.OBBeneficiary5;
import com.fasterxml.jackson.annotation.JsonIgnore;


@Document(collection="MockAccountBeneficiaryCMA2")
public class AccountBeneficiaryCMA2 extends OBBeneficiary5  {
	
	private String accountNumber;

	private String accountNSC;

	private String psuId;

	@JsonIgnore
	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	@JsonIgnore
	public String getAccountNSC() {
		return accountNSC;
	}

	public void setAccountNSC(String accountNSC) {
		this.accountNSC = accountNSC;
	}

	@JsonIgnore
	public String getPsuId() {
		return psuId;
	}

	public void setPsuId(String psuId) {
		this.psuId = psuId;
	}

}
