package com.capgemini.psd2.aisp.platform.domain;

import java.util.Map;


import com.capgemini.psd2.aisp.domain.OBReadStandingOrder6;

public class PlatformAccountStandingOrdersResponse {

	// CMA2 response
	private OBReadStandingOrder6 oBReadStandingOrder6;

	// Additional Information
	private Map<String, String> additionalInformation;

	public OBReadStandingOrder6 getoBReadStandingOrder6() {
		return oBReadStandingOrder6;
	}

	public void setoBReadStandingOrder6(OBReadStandingOrder6 oBReadStandingOrder6) {
		this.oBReadStandingOrder6 = oBReadStandingOrder6;
	}

	public Map<String, String> getAdditionalInformation() {
		return additionalInformation;
	}

	public void setAdditionalInformation(Map<String, String> additionalInformation) {
		this.additionalInformation = additionalInformation;
	}

}
