package com.capgemini.psd2.aisp.platform.domain;

import java.util.Map;

import com.capgemini.psd2.aisp.domain.OBReadParty2;


public class PlatformAccountPartyResponse {
	
	// CMA2 response
		private OBReadParty2 oBReadParty2;
		
		// Additional Information
		private Map<String, String> additionalInformation;
		
		public OBReadParty2 getoBReadParty2() {
			return oBReadParty2;
		}

		public void setoBReadParty2(OBReadParty2 oBReadParty2) {
			this.oBReadParty2 = oBReadParty2;
		}

		public Map<String, String> getAdditionalInformation() {
			return additionalInformation;
		}

		public void setAdditionalInformation(Map<String, String> additionalInformation) {
			this.additionalInformation = additionalInformation;
		}

}
