package com.capgemini.psd2.aisp.mongo.db.adapter.domain;

import org.springframework.data.mongodb.core.mapping.Document;

import com.capgemini.psd2.aisp.domain.OBStandingOrder6;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Document(collection="MockAccountStandingOrderCMA2")
public class AccountStandingOrderCMA2 extends OBStandingOrder6 {
	private String accountNumber;

	private String accountNSC;

	private String psuId;

	@JsonIgnore
	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	@JsonIgnore
	public String getAccountNSC() {
		return accountNSC;
	}

	public void setAccountNSC(String accountNSC) {
		this.accountNSC = accountNSC;
	}

	@JsonIgnore
	public String getPsuId() {
		return psuId;
	}

	public void setPsuId(String psuId) {
		this.psuId = psuId;
	}

}
