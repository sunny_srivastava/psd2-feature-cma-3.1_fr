package com.capgemini.psd2.aisp.platform.domain;

import java.util.Map;

import com.capgemini.psd2.aisp.domain.OBReadScheduledPayment3;

public class PlatformAccountSchedulePaymentsResponse {

	// CMA2Response
	private OBReadScheduledPayment3 oBReadScheduledPayment3;

	// Additional Information
	private Map<String, String> additionalInformation;

	public OBReadScheduledPayment3 getOBReadScheduledPayment3() {
		return oBReadScheduledPayment3;
	}

	public void setObReadScheduledPayment3(OBReadScheduledPayment3 oBReadScheduledPayment3) {
		this.oBReadScheduledPayment3 = oBReadScheduledPayment3;
	}

	public Map<String, String> getAdditionalInformation() {
		return additionalInformation;
	}

	public void setAdditionalInformation(Map<String, String> additionalInformation) {
		this.additionalInformation = additionalInformation;
	}

}
