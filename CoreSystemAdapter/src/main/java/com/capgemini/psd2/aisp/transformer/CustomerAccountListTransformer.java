package com.capgemini.psd2.aisp.transformer;

import java.util.Map;

import com.capgemini.psd2.aisp.domain.OBReadAccount6;
import com.capgemini.psd2.fraudsystem.domain.PSD2CustomerInfo;

public interface CustomerAccountListTransformer {
	public <T> OBReadAccount6 transformCustomerAccountListAdapter(T source, Map<String, String> params);
	
	public <T> PSD2CustomerInfo transformCustomerInfo(T source, Map<String, String> params);
}
