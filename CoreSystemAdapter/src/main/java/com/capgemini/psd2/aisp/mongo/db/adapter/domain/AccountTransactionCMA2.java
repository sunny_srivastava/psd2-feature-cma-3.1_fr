package com.capgemini.psd2.aisp.mongo.db.adapter.domain;

import java.time.LocalDateTime;

import org.springframework.data.mongodb.core.mapping.Document;

import com.capgemini.psd2.aisp.domain.OBTransaction6;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Document(collection="MockAccountTransactionCMA2")
public class AccountTransactionCMA2 extends OBTransaction6 {

	private String accountNumber;

	private String accountNSC;

	private String psuId;
	
	private String statementId;

	private LocalDateTime bookingDateTimeCopy;
	
	private String txnRetrievalKey;
	
	private String firstAvailableDateTime;
	
	private String lastAvailableDateTime;

	@JsonIgnore
	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	@JsonIgnore
	public String getAccountNSC() {
		return accountNSC;
	}

	public void setAccountNSC(String accountNSC) {
		this.accountNSC = accountNSC;
	}

	@JsonIgnore
	public String getPsuId() {
		return psuId;
	}

	public void setPsuId(String psuId) {
		this.psuId = psuId;
	}

	@JsonIgnore
	public String getStatementId() {
		return statementId;
	}

	public void setStatementId(String statementId) {
		this.statementId = statementId;
	}

	@JsonIgnore
	public LocalDateTime getBookingDateTimeCopy() {
		return bookingDateTimeCopy;
	}

	public void setBookingDateTimeCopy(LocalDateTime bookingDateTimeCopy) {
		this.bookingDateTimeCopy = bookingDateTimeCopy;
	}

	@JsonIgnore
	public String getTxnRetrievalKey() {
		return txnRetrievalKey;
	}

	public void setTxnRetrievalKey(String txnRetrievalKey) {
		this.txnRetrievalKey = txnRetrievalKey;
	}

	public String getFirstAvailableDateTime() {
		return firstAvailableDateTime;
	}

	public void setFirstAvailableDateTime(String firstAvailableDateTime) {
		this.firstAvailableDateTime = firstAvailableDateTime;
	}

	public String getLastAvailableDateTime() {
		return lastAvailableDateTime;
	}

	public void setLastAvailableDateTime(String lastAvailableDateTime) {
		this.lastAvailableDateTime = lastAvailableDateTime;
	}
}

