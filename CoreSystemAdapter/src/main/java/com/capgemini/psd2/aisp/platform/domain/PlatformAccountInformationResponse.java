package com.capgemini.psd2.aisp.platform.domain;

import java.util.Map;

import com.capgemini.psd2.aisp.domain.OBReadAccount6;

public class PlatformAccountInformationResponse {

	// CMA2 response
	private OBReadAccount6 oBReadAccount6;

	// Additional Information
	private Map<String, String> additionalInformation;

	public OBReadAccount6 getoBReadAccount6() {
		return oBReadAccount6;
	}

	public void setoBReadAccount6(OBReadAccount6 oBReadAccount6) {
		this.oBReadAccount6 = oBReadAccount6;
	}

	public Map<String, String> getAdditionalInformation() {
		return additionalInformation;
	}

	public void setAdditionalInformation(Map<String, String> additionalInformation) {
		this.additionalInformation = additionalInformation;
	}

}
