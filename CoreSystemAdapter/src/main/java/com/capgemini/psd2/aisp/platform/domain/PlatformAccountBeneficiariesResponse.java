package com.capgemini.psd2.aisp.platform.domain;

import java.util.Map;

import com.capgemini.psd2.aisp.domain.OBReadBeneficiary5;

public class PlatformAccountBeneficiariesResponse {

	// CMA2 response
	private OBReadBeneficiary5 oBReadBeneficiary5;

	// Additional Information
	private Map<String, String> additionalInformation;

	public OBReadBeneficiary5 getoBReadBeneficiary5() {
		return oBReadBeneficiary5;
	}

	public void setoBReadBeneficiary5(OBReadBeneficiary5 oBReadBeneficiary5) {
		this.oBReadBeneficiary5 = oBReadBeneficiary5;
	}

	public Map<String, String> getAdditionalInformation() {
		return additionalInformation;
	}

	public void setAdditionalInformation(Map<String, String> additionalInformation) {
		this.additionalInformation = additionalInformation;
	}
}