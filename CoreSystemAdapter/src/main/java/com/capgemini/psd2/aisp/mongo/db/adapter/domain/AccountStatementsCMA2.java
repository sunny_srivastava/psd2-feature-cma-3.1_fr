package com.capgemini.psd2.aisp.mongo.db.adapter.domain;

import java.time.LocalDateTime;

import org.springframework.data.mongodb.core.mapping.Document;

import com.capgemini.psd2.aisp.domain.OBStatement2;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Document(collection="MockAccountStatementsCMA2")
public class AccountStatementsCMA2 extends OBStatement2  {
	
	private String accountNumber;

	private String accountNSC;

	private String psuId;
	
	private String txnRetrievalKey;
	
	private LocalDateTime startDateTimeCopy;
	
	private LocalDateTime endDateTimeCopy;

	@JsonIgnore
	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	@JsonIgnore
	public String getAccountNSC() {
		return accountNSC;
	}

	public void setAccountNSC(String accountNSC) {
		this.accountNSC = accountNSC;
	}
	
	@JsonIgnore	
	public LocalDateTime getEndDateTimeCopy() {
		return endDateTimeCopy;
	}

	public void setEndDateTimeCopy(LocalDateTime endDateTimeCopy) {
		this.endDateTimeCopy = endDateTimeCopy;
	}

	@JsonIgnore
	public LocalDateTime getStartDateTimeCopy() {
		return startDateTimeCopy;
	}

	public void setStartDateTimeCopy(LocalDateTime startDateTimeCopy) {
		this.startDateTimeCopy = startDateTimeCopy;
	}

	@JsonIgnore
	public String getPsuId() {
		return psuId;
	}

	public void setPsuId(String psuId) {
		this.psuId = psuId;
	}
	
	@JsonIgnore
	public String getTxnRetrievalKey() {
		return txnRetrievalKey;
	}

	public void setTxnRetrievalKey(String txnRetrievalKey) {
		this.txnRetrievalKey = txnRetrievalKey;
	}

}
