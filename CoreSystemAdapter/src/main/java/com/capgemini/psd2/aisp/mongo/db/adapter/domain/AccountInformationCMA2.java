package com.capgemini.psd2.aisp.mongo.db.adapter.domain;

import org.springframework.data.mongodb.core.mapping.Document;

import com.capgemini.psd2.aisp.domain.OBAccount6;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Document(collection="MockAccountInformationCMA2")
public class AccountInformationCMA2 extends OBAccount6{

	private String psuId;
	private String accountNumber;
	private String nsc;

	@JsonIgnore
	public String getPsuId() {
		return psuId;
	}

	public void setPsuId(String psuId) {
		this.psuId = psuId;
	}

	@JsonIgnore
	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	@JsonIgnore
	public String getNsc() {
		return nsc;
	}

	public void setNsc(String nsc) {
		this.nsc = nsc;
	}
}
