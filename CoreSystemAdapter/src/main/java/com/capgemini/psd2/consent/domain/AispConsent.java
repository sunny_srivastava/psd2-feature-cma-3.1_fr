/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.consent.domain;

import java.util.List;

import org.springframework.data.mongodb.core.mapping.Document;

/**
 * The Class Consent.
 */
@Document(collection = "#{@CollectionNameLocator.getAispConsentCollectionName()}")
public class AispConsent extends Consent{

	/** The account details. */
	private List<AccountDetails> accountDetails;
	
	private String accountRequestId;
	
    private String transactionFromDateTime;

	private String transactionToDateTime;
	
	/**
	 * Gets the account details.
	 *
	 * @return the account details
	 */
	public List<AccountDetails> getAccountDetails() {
		return accountDetails;
	}

	/**
	 * Sets the account details.
	 *
	 * @param accountDetails the new account details
	 */
	public void setAccountDetails(List<AccountDetails> accountDetails) {
		this.accountDetails = accountDetails;
	}
	
	public String getAccountRequestId() {
		return accountRequestId;
	}

	public void setAccountRequestId(String accountRequestId) {
		this.accountRequestId = accountRequestId;
	}

	public String getTransactionFromDateTime() {
		return transactionFromDateTime;
	}

	public void setTransactionFromDateTime(String transactionFromDateTime) {
		this.transactionFromDateTime = transactionFromDateTime;
	}

	public String getTransactionToDateTime() {
		return transactionToDateTime;
	}

	public void setTransactionToDateTime(String transactionToDateTime) {
		this.transactionToDateTime = transactionToDateTime;
	}

	@Override
	public String toString() {
		return "AispConsent [accountDetails=" + accountDetails + ", accountRequestId=" + accountRequestId
				+ ", transactionFromDateTime=" + transactionFromDateTime + ", transactionToDateTime="
				+ transactionToDateTime + "]";
	}
	
	
	
	
}
