package com.capgemini.psd2.consent.domain;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "#{@CollectionNameLocator.getCispConsentCollectionName()}")
public class CispConsent extends Consent{

	private String fundsIntentId;
	
	/** The account details. */
	private AccountDetails accountDetails;

	public String getFundsIntentId() {
		return fundsIntentId;
	}

	public void setFundsIntentId(String fundsIntentId) {
		this.fundsIntentId = fundsIntentId;
	}

	public AccountDetails getAccountDetails() {
		return accountDetails;
	}

	public void setAccountDetails(AccountDetails accountDetails) {
		this.accountDetails = accountDetails;
	}

	@Override
	public String toString() {
		return "CispConsent [fundsIntentId=" + fundsIntentId + ", accountDetails=" + accountDetails + "]";
	}
	
	

}
