/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.consent.domain;

import com.capgemini.psd2.aisp.domain.OBAccount6Account;
import com.capgemini.psd2.aisp.domain.OBBranchAndFinancialInstitutionIdentification50;
import com.capgemini.psd2.aisp.domain.OBExternalAccountSubType1Code;

/**
 * The Class AccountDetails.
 */
public class AccountDetails {
	
	/** The account number. */
	private String accountNumber;
	
	/** The account NSC. */
	private String accountNSC;
	
	/** The account id. */
	private String accountId;
	
	/** The nickname. */
	private String nickname;
	
	/** The currency. */
	private String currency;
	
	private String accountType;

	/** The account type. */
	private OBExternalAccountSubType1Code accountSubType;
	
	private String identifier;
	
	public String getIdentifier() {
		return identifier;
	}

	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

	private String hashValue;

	 private OBAccount6Account account;
	 
	 private OBBranchAndFinancialInstitutionIdentification50 servicer;

	

	/**
	 * Gets the account number.
	 *
	 * @return the account number
	 */
	public String getAccountNumber() {
		return accountNumber;
	}

	/**
	 * Sets the account number.
	 *
	 * @param accountNumber the new account number
	 */
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	/**
	 * Gets the account NSC.
	 *
	 * @return the account NSC
	 */
	public String getAccountNSC() {
		return accountNSC;
	}

	/**
	 * Sets the account NSC.
	 *
	 * @param accountNSC the new account NSC
	 */
	public void setAccountNSC(String accountNSC) {
		this.accountNSC = accountNSC;
	}

	/**
	 * Gets the account id.
	 *
	 * @return the account id
	 */
	public String getAccountId() {
		return accountId;
	}

	/**
	 * Sets the account id.
	 *
	 * @param accountId the new account id
	 */
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public String getHashValue() {
		return hashValue;
	}

	public void setHashValue(String hashValue) {
		this.hashValue = hashValue;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public OBAccount6Account getAccount() {
		return account;
	}

	public void setAccount(OBAccount6Account account) {
		this.account = account;
	}
	
	public OBBranchAndFinancialInstitutionIdentification50 getServicer() {
		return servicer;
	}

	public void setServicer(OBBranchAndFinancialInstitutionIdentification50 servicer) {
		this.servicer = servicer;
	}

	public OBExternalAccountSubType1Code getAccountSubType() {
		return accountSubType;
	}

	public void setAccountSubType(OBExternalAccountSubType1Code accountSubType) {
		this.accountSubType = accountSubType;
	}
	
	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	@Override
	public String toString() {
		return "AccountDetails [accountNumber=" + accountNumber + ", accountNSC=" + accountNSC + ", accountId="
				+ accountId + ", nickname=" + nickname + ", currency=" + currency + ", accountType=" + accountType
				+ ", accountSubType=" + accountSubType + ", identifier=" + identifier + ", hashValue=" + hashValue
				+ ", account=" + account + ", servicer=" + servicer + "]";
	}
	
	
	

}
