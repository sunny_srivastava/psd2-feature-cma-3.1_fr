package com.capgemini.psd2.consent.domain;

import com.capgemini.psd2.pisp.stage.domain.AmountDetails;
import com.capgemini.psd2.pisp.stage.domain.ChargeDetails;
import com.capgemini.psd2.pisp.stage.domain.ExchangeRateDetails;

public class DebtorDetailsResponse {
	
	private ChargeDetails chargeDetails;
	private ExchangeRateDetails exchangeRateDetails;
	private AmountDetails baseAmountDetails;
	private AmountDetails totalAmountDetails;
	
	public ChargeDetails getChargeDetails() {
		return chargeDetails;
	}
	public void setChargeDetails(ChargeDetails chargeDetails) {
		this.chargeDetails = chargeDetails;
	}
	
	public DebtorDetailsResponse chargeDetails(ChargeDetails chargeDetails){
		this.chargeDetails = chargeDetails;
		return this;
	}
	
	public ExchangeRateDetails getExchangeRateDetails() {
		return exchangeRateDetails;
	}
	public void setExchangeRateDetails(ExchangeRateDetails exchangeRateDetails) {
		this.exchangeRateDetails = exchangeRateDetails;
	}
	
	public DebtorDetailsResponse exchangeRateDetails(ExchangeRateDetails exchangeRateDetails){
		this.exchangeRateDetails = exchangeRateDetails;
		return this;
	}
	
	public AmountDetails getBaseAmountDetails() {
		return baseAmountDetails;
	}
	public void setBaseAmountDetails(AmountDetails baseAmountDetails) {
		this.baseAmountDetails = baseAmountDetails;
	}
	
	public DebtorDetailsResponse baseAmountDetails(AmountDetails baseAmountDetails){
		this.baseAmountDetails = baseAmountDetails;
		return this;
	}
	
	public AmountDetails getTotalAmountDetails() {
		return totalAmountDetails;
	}
	public void setTotalAmountDetails(AmountDetails totalAmountDetails) {
		this.totalAmountDetails = totalAmountDetails;
	}
	
	public DebtorDetailsResponse totalAmountDetails(AmountDetails totalAmountDetails){
		this.totalAmountDetails = totalAmountDetails;
		return this;
	}
	
	@Override
	public String toString() {
		return "DebtorDetailsResponse [chargeDetails=" + chargeDetails + ", exchangeRateDetails=" + exchangeRateDetails
				+ ", baseAmountDetails=" + baseAmountDetails + ", totalAmountDetails=" + totalAmountDetails + "]";
	}
	
	
}
