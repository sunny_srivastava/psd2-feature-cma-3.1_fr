package com.capgemini.psd2.consent.domain;

import java.util.Map;

import com.capgemini.psd2.aisp.domain.OBAccount6;
import com.fasterxml.jackson.annotation.JsonProperty;

public class PSD2Account extends OBAccount6 {

	@JsonProperty("AdditionalInformation")
	private Map<String,String> additionalInformation;
	
	@JsonProperty("HashedValue")
	private String hashedValue;
	
	private Object fraudResponse;
		
	public Map<String, String> getAdditionalInformation() {
		return additionalInformation;
	}

	public void setAdditionalInformation(Map<String, String> additionalInformation) {
		this.additionalInformation = additionalInformation;
	}

	public String getHashedValue() {
		return hashedValue;
	}
	
	public void setHashedValue(String hashedValue) {
		this.hashedValue = hashedValue;
	}

	public Object getFraudResponse() {
		return fraudResponse;
	}

	public void setFraudResponse(Object fraudResponse) {
		this.fraudResponse = fraudResponse;
	}
}
