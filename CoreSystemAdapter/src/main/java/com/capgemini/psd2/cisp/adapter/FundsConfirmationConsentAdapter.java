package com.capgemini.psd2.cisp.adapter;

import com.capgemini.psd2.cisp.domain.OBFundsConfirmationConsentResponse1;
import com.capgemini.psd2.cisp.domain.OBFundsConfirmationConsentResponse1Data;

public interface FundsConfirmationConsentAdapter {

	public OBFundsConfirmationConsentResponse1 createFundsConfirmationConsentPOSTResponse(OBFundsConfirmationConsentResponse1Data data1);
	
	public OBFundsConfirmationConsentResponse1 getFundsConfirmationConsentPOSTResponse(String consentId);
	
	public void removeFundsConfirmationConsent(String consentId, String cId);

	public OBFundsConfirmationConsentResponse1 updateFundsConfirmationConsentResponse(String consentId,
			OBFundsConfirmationConsentResponse1Data.StatusEnum statusEnum);
	
}
