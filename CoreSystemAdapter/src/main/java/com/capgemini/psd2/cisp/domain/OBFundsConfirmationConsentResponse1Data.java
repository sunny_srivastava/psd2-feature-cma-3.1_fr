package com.capgemini.psd2.cisp.domain;

import java.util.Objects;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;

import io.swagger.annotations.ApiModelProperty;

/**
 * OBFundsConfirmationConsentResponse1Data
 */
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.SpringCodegen", date = "2021-05-05T17:22:49.034+05:30[Asia/Calcutta]")
@Document(collection = "#{@CollectionNameLocator.getFundsSetupCollectionName()}")
public class OBFundsConfirmationConsentResponse1Data   {
  @Id
  @JsonProperty("ConsentId")
  private String consentId;

  @JsonProperty("CreationDateTime")
  private String creationDateTime;

  /**
   * Specifies the status of consent resource in code form.
   */
  public enum StatusEnum {
    AUTHORISED("Authorised"),
    
    AWAITINGAUTHORISATION("AwaitingAuthorisation"),
    
    REJECTED("Rejected"),
    
    DELETED("Deleted"),
    
    REVOKED("Revoked");

    private String value;

    StatusEnum(String value) {
      this.value = value;
    }

    @JsonValue
    public String getValue() {
      return value;
    }

    @Override
    public String toString() {
      return String.valueOf(value);
    }

    @JsonCreator
    public static StatusEnum fromValue(String value) {
      for (StatusEnum b : StatusEnum.values()) {
        if (b.value.equals(value)) {
          return b;
        }
      }
      throw new IllegalArgumentException("Unexpected value '" + value + "'");
    }
  }

  @JsonProperty("Status")
  private StatusEnum status;

  @JsonProperty("StatusUpdateDateTime")
  private String statusUpdateDateTime;

  @JsonProperty("ExpirationDateTime")
  private String expirationDateTime;

  @JsonProperty("DebtorAccount")
  private OBFundsConfirmationConsent1DataDebtorAccount debtorAccount;
  
  @JsonIgnore
  private String tenantId = null;
	
  @JsonIgnore
  private String cmaVersion;
	
  @JsonIgnore
  private String tppCID = null;
	
  @JsonIgnore
  private String tppLegalEntityName  = null;

  public OBFundsConfirmationConsentResponse1Data consentId(String consentId) {
    this.consentId = consentId;
    return this;
  }

  /**
   * Unique identification as assigned to identify the funds confirmation consent resource.
   * @return consentId
  */
  @ApiModelProperty(required = true, value = "Unique identification as assigned to identify the funds confirmation consent resource.")
  @NotNull

@Size(min=1,max=128) 
  public String getConsentId() {
    return consentId;
  }

  public void setConsentId(String consentId) {
    this.consentId = consentId;
  }

  public OBFundsConfirmationConsentResponse1Data creationDateTime(String creationDateTime) {
    this.creationDateTime = creationDateTime;
    return this;
  }

  /**
   * Date and time at which the resource was created.All dates in the JSON payloads are represented in ISO 8601 date-time format.  All date-time fields in responses must include the timezone. An example is below: 2017-04-05T10:43:07+00:00
   * @return creationDateTime
  */
  @ApiModelProperty(required = true, value = "Date and time at which the resource was created.All dates in the JSON payloads are represented in ISO 8601 date-time format.  All date-time fields in responses must include the timezone. An example is below: 2017-04-05T10:43:07+00:00")
  @NotNull

  @Valid

  public String getCreationDateTime() {
    return creationDateTime;
  }

  public void setCreationDateTime(String creationDateTime) {
    this.creationDateTime = creationDateTime;
  }

  public OBFundsConfirmationConsentResponse1Data status(StatusEnum status) {
    this.status = status;
    return this;
  }

  /**
   * Specifies the status of consent resource in code form.
   * @return status
  */
  @ApiModelProperty(required = true, value = "Specifies the status of consent resource in code form.")
  @NotNull

	@Valid

  public StatusEnum getStatus() {
    return status;
  }

  public void setStatus(StatusEnum status) {
    this.status = status;
  }

  public OBFundsConfirmationConsentResponse1Data statusUpdateDateTime(String statusUpdateDateTime) {
    this.statusUpdateDateTime = statusUpdateDateTime;
    return this;
  }

  /**
   * Date and time at which the resource status was updated.All dates in the JSON payloads are represented in ISO 8601 date-time format.  All date-time fields in responses must include the timezone. An example is below: 2017-04-05T10:43:07+00:00
   * @return statusUpdateDateTime
  */
  @ApiModelProperty(required = true, value = "Date and time at which the resource status was updated.All dates in the JSON payloads are represented in ISO 8601 date-time format.  All date-time fields in responses must include the timezone. An example is below: 2017-04-05T10:43:07+00:00")
  @NotNull

  @Valid

  public String getStatusUpdateDateTime() {
    return statusUpdateDateTime;
  }

  public void setStatusUpdateDateTime(String statusUpdateDateTime) {
    this.statusUpdateDateTime = statusUpdateDateTime;
  }

  public OBFundsConfirmationConsentResponse1Data expirationDateTime(String expirationDateTime) {
    this.expirationDateTime = expirationDateTime;
    return this;
  }

  /**
   * Specified date and time the funds confirmation authorisation will expire. If this is not populated, the authorisation will be open ended.All dates in the JSON payloads are represented in ISO 8601 date-time format.  All date-time fields in responses must include the timezone. An example is below: 2017-04-05T10:43:07+00:00
   * @return expirationDateTime
  */
  @ApiModelProperty(value = "Specified date and time the funds confirmation authorisation will expire. If this is not populated, the authorisation will be open ended.All dates in the JSON payloads are represented in ISO 8601 date-time format.  All date-time fields in responses must include the timezone. An example is below: 2017-04-05T10:43:07+00:00")

  @Valid

  public String getExpirationDateTime() {
    return expirationDateTime;
  }

  public void setExpirationDateTime(String expirationDateTime) {
    this.expirationDateTime = expirationDateTime;
  }

  public OBFundsConfirmationConsentResponse1Data debtorAccount(OBFundsConfirmationConsent1DataDebtorAccount debtorAccount) {
    this.debtorAccount = debtorAccount;
    return this;
  }

  /**
   * Get debtorAccount
   * @return debtorAccount
  */
  @ApiModelProperty(required = true, value = "")
  @NotNull

  @Valid

  public OBFundsConfirmationConsent1DataDebtorAccount getDebtorAccount() {
    return debtorAccount;
  }

  public void setDebtorAccount(OBFundsConfirmationConsent1DataDebtorAccount debtorAccount) {
    this.debtorAccount = debtorAccount;
  }
  
  public String getTppCID() {
	  return tppCID;
  }
  
  public void setTppCID(String tppCID) {
	this.tppCID= tppCID;
  }

  public String getTppLegalEntityName() {
	return tppLegalEntityName;
  }

  public void setTppLegalEntityName(String tppLegalEntityName) {
	this.tppLegalEntityName = tppLegalEntityName;
  }

  public String getCmaVersion() {
	return cmaVersion;
  }

  public void setCmaVersion(String cmaVersion) {
	this.cmaVersion = cmaVersion;
  }

	/**
	 * Get tenantId
	 * @return tenantId
	 **/
  public String getTenantId() {
	return tenantId;
  }

  public void setTenantId(String tenantId) {
	this.tenantId = tenantId;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    OBFundsConfirmationConsentResponse1Data obFundsConfirmationConsentResponse1Data = (OBFundsConfirmationConsentResponse1Data) o;
    return Objects.equals(this.consentId, obFundsConfirmationConsentResponse1Data.consentId) &&
        Objects.equals(this.creationDateTime, obFundsConfirmationConsentResponse1Data.creationDateTime) &&
        Objects.equals(this.status, obFundsConfirmationConsentResponse1Data.status) &&
        Objects.equals(this.statusUpdateDateTime, obFundsConfirmationConsentResponse1Data.statusUpdateDateTime) &&
        Objects.equals(this.expirationDateTime, obFundsConfirmationConsentResponse1Data.expirationDateTime) &&
        Objects.equals(this.debtorAccount, obFundsConfirmationConsentResponse1Data.debtorAccount) &&
				Objects.equals(this.tenantId, obFundsConfirmationConsentResponse1Data.tenantId);
  }

  @Override
  public int hashCode() {
		return Objects.hash(consentId, creationDateTime, status, statusUpdateDateTime, expirationDateTime, debtorAccount, tenantId);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class OBFundsConfirmationConsentResponse1Data {\n");
    
    sb.append("    consentId: ").append(toIndentedString(consentId)).append("\n");
    sb.append("    creationDateTime: ").append(toIndentedString(creationDateTime)).append("\n");
    sb.append("    status: ").append(toIndentedString(status)).append("\n");
    sb.append("    statusUpdateDateTime: ").append(toIndentedString(statusUpdateDateTime)).append("\n");
    sb.append("    expirationDateTime: ").append(toIndentedString(expirationDateTime)).append("\n");
    sb.append("    debtorAccount: ").append(toIndentedString(debtorAccount)).append("\n");
    sb.append("    tenantId: ").append(toIndentedString(tenantId)).append("\n");
    sb.append("    cmaVersion: ").append(toIndentedString(cmaVersion)).append("\n");
    sb.append("    tppCID: ").append(toIndentedString(tppCID)).append("\n");
    sb.append("    tppLegalEntityName: ").append(toIndentedString(tppLegalEntityName)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

