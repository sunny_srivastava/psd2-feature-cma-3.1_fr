package com.capgemini.psd2.cisp.mock.domain;

import org.springframework.data.mongodb.core.mapping.Document;

import com.capgemini.psd2.cisp.domain.OBFundsConfirmation1DataInstructedAmount;
import com.fasterxml.jackson.annotation.JsonProperty;

@Document(collection="funds")
public class MockFundData{
	private String accountNumber;
	
	private String accountNsc;
	
	private OBFundsConfirmation1DataInstructedAmount instructedAmount;
	 
	@JsonProperty("accountNumber")
	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	@JsonProperty("accountNsc")
	public String getAccountNsc() {
		return accountNsc;
	}

	public void setAccountNsc(String accountNsc) {
		this.accountNsc = accountNsc;
	}

	@JsonProperty("InstructedAmount")
	public OBFundsConfirmation1DataInstructedAmount getInstructedAmount() {
		return instructedAmount;
	}

	public void setInstructedAmount(OBFundsConfirmation1DataInstructedAmount instructedAmount) {
		this.instructedAmount = instructedAmount;
	}
	  
}
