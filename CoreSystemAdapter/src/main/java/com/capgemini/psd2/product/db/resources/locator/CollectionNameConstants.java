package com.capgemini.psd2.product.db.resources.locator;

public interface CollectionNameConstants {
	String UK_AISP_SETUP_RES = "data1";
	String UK_AISP_CONSENT_RES = "aispConsent";
	String UK_PISP_SETUP_RES = "paymentSetupPlatformResources";
	String UK_PISP_SUB_RES = "paymentSubmissionPlatformResources";
	String UK_PISP_CONSENT_RES = "pispConsent";
	String UK_CISP_SETUP_RES = "oBFundsConfirmationConsentDataResponse1";
	String UK_CISP_CONSENT_RES = "cispConsent";
	String UK_CPNP_RES = "CPNPEntity";
	
	
	String AISP_SETUP_RES_SUFFIX = "_Account_Access_Setup";
	String AISP_CONSENT_RES_SUFFIX = "_Aisp_Consent";
	String PISP_SETUP_RES_SUFFIX = "_Payment_Setup";
	String PISP_SUB_RES_SUFFIX = "_Payment_Submission";
	String PISP_CONSENT_RES_SUFFIX = "_Pisp_Consent";
	String CISP_SETUP_RES_SUFFIX = "_Funds_Confirmation_Setup";
	String CISP_CONSENT_RES_SUFFIX = "_Cisp_Consent";
	String CPNP_RES_SUFFIX = "_CPNP_Entity";
	
	
	
}
