package com.capgemini.psd2.product.db.resources.locator;

import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;

@Component("collectionNameLocator")
public class CollectionNameLocatorImpl implements CollectionNameLocator {

	private static final String BOIUK = "BOIUK";

	@Override
	public String getAccountAccessSetupCollectionName() {
		String tenantId = fetchTenantId();
		return tenantId.equals(BOIUK) ? CollectionNameConstants.UK_AISP_SETUP_RES
				: tenantId.concat(CollectionNameConstants.AISP_SETUP_RES_SUFFIX);

	}

	@Override
	public String getAispConsentCollectionName() {
		String tenantId = fetchTenantId();
		return tenantId.equals(BOIUK) ? CollectionNameConstants.UK_AISP_CONSENT_RES
				: tenantId.concat(CollectionNameConstants.AISP_CONSENT_RES_SUFFIX);
	}

	@Override
	public String getPaymentSetupCollectionName() {
		String tenantId = fetchTenantId();
		return tenantId.equals(BOIUK) ? CollectionNameConstants.UK_PISP_SETUP_RES
				: tenantId.concat(CollectionNameConstants.PISP_SETUP_RES_SUFFIX);
	}

	@Override
	public String getPaymentSubmissionCollectionName() {
		String tenantId = fetchTenantId();
		return tenantId.equals(BOIUK) ? CollectionNameConstants.UK_PISP_SUB_RES
				: tenantId.concat(CollectionNameConstants.PISP_SUB_RES_SUFFIX);
	}

	@Override
	public String getPispConsentCollectionName() {
		String tenantId = fetchTenantId();
		return tenantId.equals(BOIUK) ? CollectionNameConstants.UK_PISP_CONSENT_RES
				: tenantId.concat(CollectionNameConstants.PISP_CONSENT_RES_SUFFIX);
	}

	@Override
	public String getFundsSetupCollectionName() {
		String tenantId = fetchTenantId();
		return tenantId.equals(BOIUK) ? CollectionNameConstants.UK_CISP_SETUP_RES
				: tenantId.concat(CollectionNameConstants.CISP_SETUP_RES_SUFFIX);
	}

	@Override
	public String getCispConsentCollectionName() {
		String tenantId = fetchTenantId();
		return tenantId.equals(BOIUK) ? CollectionNameConstants.UK_CISP_CONSENT_RES
				: tenantId.concat(CollectionNameConstants.CISP_CONSENT_RES_SUFFIX);
	}

	@Override
	public String getCPNPCollectionName() {
		String tenantId = fetchTenantId();
		return tenantId.equals(BOIUK) ? CollectionNameConstants.UK_CPNP_RES
				: tenantId.concat(CollectionNameConstants.CPNP_RES_SUFFIX);
	}

	private String fetchTenantId() {
		String tenantId = "";
		RequestAttributes springContextAttr = RequestContextHolder.getRequestAttributes();
		if (springContextAttr != null)
			tenantId = springContextAttr.getAttribute("requestTenantId", RequestAttributes.SCOPE_REQUEST).toString();
		return tenantId;
	}

}
