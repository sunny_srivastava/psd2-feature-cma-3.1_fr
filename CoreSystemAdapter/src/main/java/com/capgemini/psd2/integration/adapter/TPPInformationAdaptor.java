package com.capgemini.psd2.integration.adapter;

import java.util.List;

public interface TPPInformationAdaptor{
	
	public <T> T fetchTPPInformation(String clientId);

	public String  fetchApplicationName(String clientId);
	
	public List<Object> fetchTppAppMappingForClient(String clientId);

	public String fetchSoftwareOnBehalfOfOrg(String clientId);
	
}
