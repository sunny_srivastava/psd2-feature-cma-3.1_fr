package com.capgemini.psd2.pisp.domain;

public class CustomDPaymentConsentsPOSTRequest extends OBWriteDomesticConsent1 {

	private String createdOn;
	
	public String getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}
	@Override
	public String toString() {
		return "CustomDPaymentConsentsPOSTRequest [createdOn=" + createdOn + "]";
	}
	
}
