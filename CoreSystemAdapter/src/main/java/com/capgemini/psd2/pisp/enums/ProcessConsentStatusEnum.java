package com.capgemini.psd2.pisp.enums;

public enum ProcessConsentStatusEnum {
	PASS("PASS"), 
	FAIL("FAIL");

	private String value;

	ProcessConsentStatusEnum(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return String.valueOf(value);
	}

	private ProcessConsentStatusEnum processConsentStatus = null;

	public ProcessConsentStatusEnum getProcessConsentStatus() {
		return processConsentStatus;
	}

}
