package com.capgemini.psd2.pisp.stage.domain;

public class CustomDSOData {
	
	private String firstPaymentDateTime;
	private String frequency;
	private String finalPaymentDateTime;
	private String numberOfPayment;
	private AmountDetails finalChargeDetails;
	private AmountDetails recurringChargeDetails;
	
	public AmountDetails getRecurringChargeDetails() {
		return recurringChargeDetails;
	}
	public void setRecurringChargeDetails(AmountDetails recurringChargeDetails) {
		this.recurringChargeDetails = recurringChargeDetails;
	}
	public String getFirstPaymentDateTime() {
		return firstPaymentDateTime;
	}
	public void setFirstPaymentDateTime(String firstPaymentDateTime) {
		this.firstPaymentDateTime = firstPaymentDateTime;
	}
	public String getFrequency() {
		return frequency;
	}
	public void setFrequency(String frequency) {
		this.frequency = frequency;
	}
	
	public AmountDetails getFinalChargeDetails() {
		return finalChargeDetails;
	}
	public void setFinalChargeDetails(AmountDetails finalAmountDetails) {
		this.finalChargeDetails = finalAmountDetails;
	}
	public String getFinalPaymentDateTime() {
		return finalPaymentDateTime;
	}
	public void setFinalPaymentDateTime(String finalPaymentDateTime) {
		this.finalPaymentDateTime = finalPaymentDateTime;
	}
	public String getNumberOfPayment() {
		return numberOfPayment;
	}
	public void setNumberOfPayment(String numberOfPayment) {
		this.numberOfPayment = numberOfPayment;
	}
	@Override
	public String toString() {
		return "CustomDSOPreAuthorizeData [firstPaymentDateTime=" + firstPaymentDateTime + ", frequency=" + frequency
				+ ", finalPaymentDateTime=" + finalPaymentDateTime + ", numberOfPayment=" + numberOfPayment
				+ ", finalChargeDetails=" + finalChargeDetails + ", recurringChargeDetails=" + recurringChargeDetails
				+ "]";
	}
		
}
