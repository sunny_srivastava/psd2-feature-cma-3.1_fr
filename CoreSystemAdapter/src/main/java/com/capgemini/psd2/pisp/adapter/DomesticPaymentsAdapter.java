package com.capgemini.psd2.pisp.adapter;

import java.util.Map;

import com.capgemini.psd2.pisp.domain.CustomDPaymentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.OBTransactionIndividualStatus1Code;
import com.capgemini.psd2.pisp.domain.PaymentDomesticSubmitPOST201Response;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;

public interface DomesticPaymentsAdapter {

	// Called from Domestic Payments API to process payment
	public PaymentDomesticSubmitPOST201Response processDomesticPayments(CustomDPaymentsPOSTRequest domesticPaymentsRequest,
			Map<String, OBTransactionIndividualStatus1Code> paymentStatusMap, Map<String, String> params);

	// Called from Domestic Payments API to retrieve payment
	public PaymentDomesticSubmitPOST201Response retrieveStagedDomesticPayments(
			CustomPaymentStageIdentifiers customPaymentStageIdentifiers, Map<String, String> params);

}
