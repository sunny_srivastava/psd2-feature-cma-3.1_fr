package com.capgemini.psd2.pisp.sca.consent.operations.adapter;

import java.util.Map;

import com.capgemini.psd2.pisp.domain.OBCashAccountDebtor3;
import com.capgemini.psd2.pisp.domain.PaymentConsentsValidationResponse;
import com.capgemini.psd2.pisp.stage.domain.CustomConsentAppViewData;
import com.capgemini.psd2.pisp.stage.domain.CustomFraudSystemPaymentData;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.pisp.stage.domain.CustomPreAuthorizeAdditionalInfo;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageUpdateData;

public interface PispScaConsentOperationsAdapter {
	/* Will be called via consent controller view request */
	public CustomConsentAppViewData retrieveConsentAppStagedViewData(CustomPaymentStageIdentifiers stageIdentifiers,
			Map<String, String> params);

	/* Will be called via consent controller create request */
	public CustomFraudSystemPaymentData retrieveFraudSystemPaymentStagedData(CustomPaymentStageIdentifiers stageIdentifiers,
			Map<String, String> params);

	/* Will be called via consent controller pre-authorize request */
	public PaymentConsentsValidationResponse validatePreAuthorisation(OBCashAccountDebtor3 selectedDebtorDetails,
			CustomPreAuthorizeAdditionalInfo preAuthAdditionalInfo, CustomPaymentStageIdentifiers stageIdentifiers,
			Map<String, String> params);

	/* Will be called via SCA cancel, Consent cancel, IntOperation Update */
	public void updatePaymentStageData(CustomPaymentStageIdentifiers stageIdentifiers,
			CustomPaymentStageUpdateData updateData, Map<String, String> params);

	public CustomConsentAppViewData retrieveConsentAppDebtorDetails(CustomPaymentStageIdentifiers stageIdentifiers,
			CustomPaymentStageUpdateData updateData, Map<String, String> paramsMap);

}
