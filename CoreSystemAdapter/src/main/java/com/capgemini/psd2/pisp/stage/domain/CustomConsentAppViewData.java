package com.capgemini.psd2.pisp.stage.domain;

public class CustomConsentAppViewData {

	private String paymentType;
	private CustomDebtorDetails debtorDetails;
	private CreditorDetails creditorDetails;
	private RemittanceDetails remittanceDetails;
	private AmountDetails amountDetails;
	private ChargeDetails chargeDetails;
	private ExchangeRateDetails exchangeRateDetails;
	private CustomDSOData customDSOData;
	private CustomDSPData customDSPData;
	private CustomFileData customFileData;
	private CustomISPData customISPData;
	private AmountDetails baseAmountDetails;
	private AmountDetails totalAmountDetails;


	/**
	 * @return the paymentType
	 */
	public String getPaymentType() {
		return paymentType;
	}

	/**
	 * @param paymentType
	 *            the paymentType to set
	 */
	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	/**
	 * @return the debtorDetails
	 */
	public CustomDebtorDetails getDebtorDetails() {
		return debtorDetails;
	}

	/**
	 * @param debtorDetails
	 *            the debtorDetails to set
	 */
	public void setDebtorDetails(CustomDebtorDetails debtorDetails) {
		this.debtorDetails = debtorDetails;
	}

	/**
	 * @return the creditorDetails
	 */
	public CreditorDetails getCreditorDetails() {
		return creditorDetails;
	}

	/**
	 * @param creditorDetails
	 *            the creditorDetails to set
	 */
	public void setCreditorDetails(CreditorDetails creditorDetails) {
		this.creditorDetails = creditorDetails;
	}

	/**
	 * @return the remittanceDetails
	 */
	public RemittanceDetails getRemittanceDetails() {
		return remittanceDetails;
	}

	/**
	 * @param remittanceDetails
	 *            the remittanceDetails to set
	 */
	public void setRemittanceDetails(RemittanceDetails remittanceDetails) {
		this.remittanceDetails = remittanceDetails;
	}

	/**
	 * @return the amountDetails
	 */
	public AmountDetails getAmountDetails() {
		return amountDetails;
	}

	/**
	 * @param amountDetails
	 *            the amountDetails to set
	 */
	public void setAmountDetails(AmountDetails amountDetails) {
		this.amountDetails = amountDetails;
	}

	/**
	 * @return the chargeDetails
	 */
	public ChargeDetails getChargeDetails() {
		return chargeDetails;
	}

	/**
	 * @param chargeDetails
	 *            the chargeDetails to set
	 */
	public void setChargeDetails(ChargeDetails chargeDetails) {
		this.chargeDetails = chargeDetails;
	}

	/**
	 * @return the exchangeRateDetails
	 */
	public ExchangeRateDetails getExchangeRateDetails() {
		return exchangeRateDetails;
	}

	/**
	 * @param exchangeRateDetails
	 *            the exchangeRateDetails to set
	 */
	public void setExchangeRateDetails(ExchangeRateDetails exchangeRateDetails) {
		this.exchangeRateDetails = exchangeRateDetails;
	}

	/**
	 * @return the customeFileData
	 */
	public CustomFileData getCustomeFileData() {
		return customFileData;
	}

	/**
	 * @param customeFileData
	 *            the customeFileData to set
	 */
	public void setCustomeFileData(CustomFileData customeFileData) {
		this.customFileData = customeFileData;
	}

	public CustomDSPData getCustomDSPData() {
		return customDSPData;
	}

	public void setCustomDSPData(CustomDSPData customDSPData) {
		this.customDSPData = customDSPData;
	}

	public CustomDSOData getCustomDSOData() {
		return customDSOData;
	}

	public void setCustomDSOData(CustomDSOData customDSOData) {
		this.customDSOData = customDSOData;
	}

	public CustomISPData getCustomISPData() {
		return customISPData;
	}

	public void setCustomISPData(CustomISPData customISPData) {
		this.customISPData = customISPData;
	}
	

	public AmountDetails getBaseAmountDetails() {
		return baseAmountDetails;
	}

	public void setBaseAmountDetails(AmountDetails baseAmountDetails) {
		this.baseAmountDetails = baseAmountDetails;
	}

	public AmountDetails getTotalAmountDetails() {
		return totalAmountDetails;
	}

	public void setTotalAmountDetails(AmountDetails totalAmountDetails) {
		this.totalAmountDetails = totalAmountDetails;
	}

	@Override
	public String toString() {
		return "CustomConsentAppViewData [paymentType=" + paymentType + ", debtorDetails=" + debtorDetails
				+ ", creditorDetails=" + creditorDetails + ", remittanceDetails=" + remittanceDetails
				+ ", amountDetails=" + amountDetails + ", chargeDetails=" + chargeDetails + ", exchangeRateDetails="
				+ exchangeRateDetails + ", customDSOData=" + customDSOData + ", customDSPData=" + customDSPData
				+ ", customFileData=" + customFileData + ", customISPData=" + customISPData + ", baseAmountDetails="
				+ baseAmountDetails + ", totalAmountDetails=" + totalAmountDetails + "]";
	}

}
