package com.capgemini.psd2.pisp.domain;

public class PaymentConsentsValidationResponse {
	private OBTransactionIndividualStatus1Code paymentSetupValidationStatus;
	private String paymentSubmissionId;

	public String getPaymentSubmissionId() {
		return paymentSubmissionId;
	}

	public void setPaymentSubmissionId(String paymentSubmissionId) {
		this.paymentSubmissionId = paymentSubmissionId;
	}

	public OBTransactionIndividualStatus1Code getPaymentSetupValidationStatus() {
		return paymentSetupValidationStatus;
	}

	public void setPaymentSetupValidationStatus(OBTransactionIndividualStatus1Code paymentSetupValidationStatus) {
		this.paymentSetupValidationStatus = paymentSetupValidationStatus;
	}
}
