package com.capgemini.psd2.enums;

public enum ConsentStatusEnum {

	AWAITINGAUTHORISATION("1","AwaitingAuthorisation"),
	REJECTED("2","Rejected"),
	AUTHORISED("3","Authorised"),
	EXPIRED("4","Expired"),
	REVOKED("5","Revoked"),
	/* Added deleted status since revoked status is not applicable after v3.1.4 for AISP */
	DELETED("6","Deleted");
	
	private String statusId;
	private String statusCode;
	
	ConsentStatusEnum(String statusId, String statusCode) {
		this.statusId = statusId;
		this.statusCode = statusCode;
	}
	
    public String getStatusId() {
			return statusId;
	}
	public String getStatusCode() {
			return statusCode;
	}
	
	public final static ConsentStatusEnum findStatusFromEnum(String statusId){
		ConsentStatusEnum currentStatus = null;
		ConsentStatusEnum[] enums = ConsentStatusEnum.values();
		for(ConsentStatusEnum statusEnum : enums) {
			if(statusEnum.getStatusId().equalsIgnoreCase(statusId)) {
				currentStatus = statusEnum;
				break;
			}
		}
		return currentStatus;
	}
	
	public final static  boolean isAuthorised(String statusId){
		ConsentStatusEnum currentStatus=null;
		ConsentStatusEnum[] enums = ConsentStatusEnum.values();
		for(ConsentStatusEnum status : enums) {
            if(status.getStatusId().equalsIgnoreCase(statusId)) {
            	currentStatus = status;
                  break;
            }
     }
     return currentStatus != null && currentStatus.equals(AUTHORISED);	
	}
}
