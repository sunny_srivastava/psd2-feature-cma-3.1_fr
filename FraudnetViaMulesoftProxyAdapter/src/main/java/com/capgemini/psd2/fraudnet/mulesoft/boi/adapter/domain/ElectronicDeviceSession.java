package com.capgemini.psd2.fraudnet.mulesoft.boi.adapter.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.google.gson.annotations.SerializedName;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * The device session that originated the event
 */
@ApiModel(description = "The device session that originated the event")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2019-03-19T13:42:54.062+05:30")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ElectronicDeviceSession {
  @SerializedName("deviceAddress")
  private String deviceAddress;

  @SerializedName("deviceData")
  private List<DeviceData> deviceData = new ArrayList<DeviceData>();

  @SerializedName("deviceLocation")
  private DeviceLocation deviceLocation;

  public ElectronicDeviceSession deviceAddress(String deviceAddress) {
    this.deviceAddress = deviceAddress;
    return this;
  }

   /**
   * Four octets of the IP address.
   * @return deviceAddress
  **/
  @ApiModelProperty(required = true, value = "Four octets of the IP address.")
  public String getDeviceAddress() {
    return deviceAddress;
  }

  public void setDeviceAddress(String deviceAddress) {
    this.deviceAddress = deviceAddress;
  }

  public ElectronicDeviceSession deviceData(List<DeviceData> deviceData) {
    this.deviceData = deviceData;
    return this;
  }

  public ElectronicDeviceSession addDeviceDataItem(DeviceData deviceDataItem) {
    this.deviceData.add(deviceDataItem);
    return this;
  }

   /**
   * The device session details with mandatory information required for JSC, HDIM and atleast one Header
   * @return deviceData
  **/
  @ApiModelProperty(required = true, value = "The device session details with mandatory information required for JSC, HDIM and atleast one Header")
  public List<DeviceData> getDeviceData() {
    return deviceData;
  }

  public void setDeviceData(List<DeviceData> deviceData) {
    this.deviceData = deviceData;
  }

  public ElectronicDeviceSession deviceLocation(DeviceLocation deviceLocation) {
    this.deviceLocation = deviceLocation;
    return this;
  }

   /**
   * Get deviceLocation
   * @return deviceLocation
  **/
  @ApiModelProperty(value = "")
  public DeviceLocation getDeviceLocation() {
    return deviceLocation;
  }

  public void setDeviceLocation(DeviceLocation deviceLocation) {
    this.deviceLocation = deviceLocation;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ElectronicDeviceSession electronicDeviceSessionType = (ElectronicDeviceSession) o;
    return Objects.equals(this.deviceAddress, electronicDeviceSessionType.deviceAddress) &&
        Objects.equals(this.deviceData, electronicDeviceSessionType.deviceData) &&
        Objects.equals(this.deviceLocation, electronicDeviceSessionType.deviceLocation);
  }

  @Override
  public int hashCode() {
    return Objects.hash(deviceAddress, deviceData, deviceLocation);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ElectronicDeviceSessionType {\n");
    
    sb.append("    deviceAddress: ").append(toIndentedString(deviceAddress)).append("\n");
    sb.append("    deviceData: ").append(toIndentedString(deviceData)).append("\n");
    sb.append("    deviceLocation: ").append(toIndentedString(deviceLocation)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

