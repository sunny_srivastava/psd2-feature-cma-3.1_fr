package com.capgemini.psd2.fraudnet.mulesoft.boi.adapter.domain;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.google.gson.annotations.SerializedName;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * The payment instruction information for the transaction
 */
@ApiModel(description = "The payment instruction information for the transaction")
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2019-03-19T13:42:54.062+05:30")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PaymentInstruction {
	@SerializedName("paymentInstructionNumber")
	private String paymentInstructionNumber;

	public PaymentInstruction paymentInstructionNumber(String paymentInstructionNumber) {
		this.paymentInstructionNumber = paymentInstructionNumber;
		return this;
	}

	/**
	 * The identifying number of the Payment Instruction in the Banking System
	 * 
	 * @return paymentInstructionNumber
	 **/
	@ApiModelProperty(required = true, value = "The identifying number of the Payment Instruction in the Banking System")
	public String getPaymentInstructionNumber() {
		return paymentInstructionNumber;
	}

	public void setPaymentInstructionNumber(String paymentInstructionNumber) {
		this.paymentInstructionNumber = paymentInstructionNumber;
	}

	@Override
	public boolean equals(java.lang.Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		PaymentInstruction paymentInstruction = (PaymentInstruction) o;
		return Objects.equals(this.paymentInstructionNumber, paymentInstruction.paymentInstructionNumber);
	}

	@Override
	public int hashCode() {
		return Objects.hash(paymentInstructionNumber);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class PaymentInstruction {\n");

		sb.append("    paymentInstructionNumber: ").append(toIndentedString(paymentInstructionNumber)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 */
	private String toIndentedString(java.lang.Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}

}
