package com.capgemini.psd2.fraudnet.mulesoft.boi.adapter.domain;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModelProperty;

/**
 * AddressType
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2019-03-19T13:42:54.062+05:30")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Address {
  @SerializedName("firstAddressLine")
  private String firstAddressLine = null;

  @SerializedName("secondAddressLine")
  private String secondAddressLine = null;

  @SerializedName("thirdAddressLine")
  private String thirdAddressLine = null;

  @SerializedName("fourthAddressLine")
  private String fourthAddressLine = null;

  @SerializedName("fifthAddressLine")
  private String fifthAddressLine = null;

  @SerializedName("postCodeNumber")
  private String postCodeNumber = null;

  @SerializedName("countryCode")
  private String countryCode = null;

  public Address firstAddressLine(String firstAddressLine) {
    this.firstAddressLine = firstAddressLine;
    return this;
  }

   /**
   * Get firstAddressLine
   * @return firstAddressLine
  **/
  @ApiModelProperty(value = "")
  public String getFirstAddressLine() {
    return firstAddressLine;
  }

  public void setFirstAddressLine(String firstAddressLine) {
    this.firstAddressLine = firstAddressLine;
  }

  public Address secondAddressLine(String secondAddressLine) {
    this.secondAddressLine = secondAddressLine;
    return this;
  }

   /**
   * Get secondAddressLine
   * @return secondAddressLine
  **/
  @ApiModelProperty(value = "")
  public String getSecondAddressLine() {
    return secondAddressLine;
  }

  public void setSecondAddressLine(String secondAddressLine) {
    this.secondAddressLine = secondAddressLine;
  }

  public Address thirdAddressLine(String thirdAddressLine) {
    this.thirdAddressLine = thirdAddressLine;
    return this;
  }

   /**
   * Get thirdAddressLine
   * @return thirdAddressLine
  **/
  @ApiModelProperty(value = "")
  public String getThirdAddressLine() {
    return thirdAddressLine;
  }

  public void setThirdAddressLine(String thirdAddressLine) {
    this.thirdAddressLine = thirdAddressLine;
  }

  public Address fourthAddressLine(String fourthAddressLine) {
    this.fourthAddressLine = fourthAddressLine;
    return this;
  }

   /**
   * Get fourthAddressLine
   * @return fourthAddressLine
  **/
  @ApiModelProperty(value = "")
  public String getFourthAddressLine() {
    return fourthAddressLine;
  }

  public void setFourthAddressLine(String fourthAddressLine) {
    this.fourthAddressLine = fourthAddressLine;
  }

  public Address fifthAddressLine(String fifthAddressLine) {
    this.fifthAddressLine = fifthAddressLine;
    return this;
  }

   /**
   * Get fifthAddressLine
   * @return fifthAddressLine
  **/
  @ApiModelProperty(value = "")
  public String getFifthAddressLine() {
    return fifthAddressLine;
  }

  public void setFifthAddressLine(String fifthAddressLine) {
    this.fifthAddressLine = fifthAddressLine;
  }

  public Address postCodeNumber(String postCodeNumber) {
    this.postCodeNumber = postCodeNumber;
    return this;
  }

   /**
   * Get postCodeNumber
   * @return postCodeNumber
  **/
  @ApiModelProperty(value = "")
  public String getPostCodeNumber() {
    return postCodeNumber;
  }

  public void setPostCodeNumber(String postCodeNumber) {
    this.postCodeNumber = postCodeNumber;
  }

  public Address countryCode(String countryCode) {
    this.countryCode = countryCode;
    return this;
  }

   /**
   * Get countryCode
   * @return countryCode
  **/
  @ApiModelProperty(value = "")
  public String getCountryCode() {
    return countryCode;
  }

  public void setCountryCode(String countryCode) {
    this.countryCode = countryCode;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Address addressType = (Address) o;
    return Objects.equals(this.firstAddressLine, addressType.firstAddressLine) &&
        Objects.equals(this.secondAddressLine, addressType.secondAddressLine) &&
        Objects.equals(this.thirdAddressLine, addressType.thirdAddressLine) &&
        Objects.equals(this.fourthAddressLine, addressType.fourthAddressLine) &&
        Objects.equals(this.fifthAddressLine, addressType.fifthAddressLine) &&
        Objects.equals(this.postCodeNumber, addressType.postCodeNumber) &&
        Objects.equals(this.countryCode, addressType.countryCode);
  }

  @Override
  public int hashCode() {
    return Objects.hash(firstAddressLine, secondAddressLine, thirdAddressLine, fourthAddressLine, fifthAddressLine, postCodeNumber, countryCode);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class AddressType {\n");
    
    sb.append("    firstAddressLine: ").append(toIndentedString(firstAddressLine)).append("\n");
    sb.append("    secondAddressLine: ").append(toIndentedString(secondAddressLine)).append("\n");
    sb.append("    thirdAddressLine: ").append(toIndentedString(thirdAddressLine)).append("\n");
    sb.append("    fourthAddressLine: ").append(toIndentedString(fourthAddressLine)).append("\n");
    sb.append("    fifthAddressLine: ").append(toIndentedString(fifthAddressLine)).append("\n");
    sb.append("    postCodeNumber: ").append(toIndentedString(postCodeNumber)).append("\n");
    sb.append("    countryCode: ").append(toIndentedString(countryCode)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

