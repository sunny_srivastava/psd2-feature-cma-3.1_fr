package com.capgemini.psd2.fraudnet.mulesoft.boi.adapter.test;

import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.Cookie;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.adapter.fraudnet.domain.FraudServiceResponse;
import com.capgemini.psd2.fraudnet.mulesoft.boi.adapter.constants.FraudnetViaMulesoftProxyConstants;
import com.capgemini.psd2.fraudnet.mulesoft.boi.adapter.domain.FraudServiceRequest;
import com.capgemini.psd2.fraudnet.mulesoft.boi.adapter.transformer.FraudnetViaMulesoftProxyTransformer;


import com.capgemini.psd2.fraudsystem.domain.PSD2FinancialAccount;
import com.capgemini.psd2.fraudsystem.domain.PhoneNumbers;

@RunWith(SpringJUnit4ClassRunner.class)
public class FraudnetViaMulesoftProxyTransformerTest {

	@InjectMocks
	private FraudnetViaMulesoftProxyTransformer fraudnetViaMulesoftProxyTransformer;

	/**
	 * Sets the up.
	 */
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * Context loads.
	 */
	@Test
	public void contextLoads() {
	}

	//@Test(expected = AdapterException.class)
	public void transformPaymentSetupPOSTResponseTest1() {

		Map<String, Map<String, Object>> fraudSystemRequest = new HashMap<String, Map<String, Object>>();

		// eventMap
		Map<String, Object> eventMap = new HashMap<String, Object>();
		eventMap.put(FraudnetViaMulesoftProxyConstants.EVENT_TYPE, "Login");
		eventMap.put(FraudnetViaMulesoftProxyConstants.EVENT_TIME, "2014-02-28T15:59:23.000Z");
		eventMap.put(FraudnetViaMulesoftProxyConstants.EVENT_CHANNEL, "Online");
		eventMap.put(FraudnetViaMulesoftProxyConstants.EVENT_SOURCE_SYSTEM, "API Platform");
		eventMap.put(FraudnetViaMulesoftProxyConstants.EVENT_SOURCE_SUBSYSTEM, "PISP");
		eventMap.put(FraudnetViaMulesoftProxyConstants.TRANSFER_AMNT, "240");
		eventMap.put(FraudnetViaMulesoftProxyConstants.EVENT_ID, "Test");
		eventMap.put(FraudnetViaMulesoftProxyConstants.TRANSFER_MEMO, "Test");		
		eventMap.put(FraudnetViaMulesoftProxyConstants.TRANSFER_TIME, "2014-02-28T15:59:23.000Z");
		eventMap.put(FraudnetViaMulesoftProxyConstants.TRANSFER_ACCOUNT_NUMBER, "1928774024");
		eventMap.put(FraudnetViaMulesoftProxyConstants.TRANSFER_ID,"test");
		eventMap.put(FraudnetViaMulesoftProxyConstants.EVENT_MODEL_CODE, "AISPL");
		eventMap.put("eventOutcome", "Success");
		
		// deviceMap
		Map<String, Object> deviceMap = new HashMap<String, Object>();
		deviceMap.put(FraudnetViaMulesoftProxyConstants.DEVICE_IP, "11.5.141.202");
		deviceMap.put(FraudnetViaMulesoftProxyConstants.DEVICE_JSC, "kla44j1e3NlY5BSo9z4ofjb75PaK4Vpjt5nrU8s8vWuIUgxrmTTuCUpMcUGejYO KES5jfyEwHXXTSHCRR0QOtWyFGh8cmvSuCKzIlnY6x2KlT64K3H.ppAJZ7OQuyP BB2SCXw2SCYOvYDy25adjjftckcKyAd65hz7qTvtE0EREHQxbiyInrGfyex2uCK wQ9dvcpxUlzXJJIneGfYVAQEBEm1CdC5MQjGejuTDRNzcPiAksecXF5iTmk6eAX vIdVuxISg0QWvOe9fCMGa2hUMnGWpwoNSUC56MnGW87gq1HACVcHkxI5_1.9ihy ppAIKWbZcFKV8NTghN.nkre9zH_y3ExnJpyWVEL3NvWurk51lVB4WG.CNOt96h L._Wu_0L.BwCtOMu_Ep.ziPajoMu5.VNNW5BSuxIgtaqpRxuYIdw0xO9sarwyjJ vDOhhMETcouU.Uz8464qnvvYIw5Epir6UtTvqbRyhmgiFEjsnzxK9B5qfZvQAuZ a2bU0tzrU9juBhElbElLAUugLyTUbyATf92PIiyhqOfjVrwxN_l3yoonkJgI E_X_Qs796tlnhqvnmccbguaDcujhDna2QKlNdHlAmjJvDOhhM4XM0oGN_tvfvCS nBNleW5BNlan0QkBMfs.8gC");
		deviceMap.put(FraudnetViaMulesoftProxyConstants.DEVICE_PAYLOAD, "rO0ABXNyACdjb20udGhlNDEuY29tbW9ucy5jcnlwdG8uQ3J5cHRvRW52ZWxvcGUAAJbgqPhc8wIAA0wABWFsaWFzdAASTGphdmEvbGFuZy9TdHJpbmc7WwAMZW5jcnlwdGVkS2V5dAACW0JbABBlbmNyeXB0ZWRQYXlsb2FkcQB-");
		
		Cookie cookie = new Cookie("SessionControllCookie", "abcd");
		Cookie[] cookies = new Cookie[]{cookie};
		
		
		deviceMap.put(FraudnetViaMulesoftProxyConstants.DEVICE_COOKIES, cookies);
		deviceMap.put(FraudnetViaMulesoftProxyConstants.SESSION_ID, "49C5E3479AAA14B676609186B7D2E234");
		deviceMap.put(FraudnetViaMulesoftProxyConstants.SESSION_LOGGEDIN, true);
		deviceMap.put(FraudnetViaMulesoftProxyConstants.DEVICE_HEADERS, "{\"X-HTTP-Header\":\"header value\"}");
		// customerMap
		Map<String, Object> customerMap = new HashMap<String, Object>();
		List<PSD2FinancialAccount> psd2FinancialAccountList = new ArrayList<PSD2FinancialAccount>();
		PSD2FinancialAccount psd2FinancialAccount = new PSD2FinancialAccount();
		psd2FinancialAccount.setType("test");
		psd2FinancialAccount.setHashedAccountNumber("1234");
		psd2FinancialAccount.setRoutingNumber("1234");
		psd2FinancialAccount.setSubType("Current Account");
		psd2FinancialAccountList.add(psd2FinancialAccount);
		customerMap.put(FraudnetViaMulesoftProxyConstants.FIN_ACCNTS, psd2FinancialAccountList);
		eventMap.put(FraudnetViaMulesoftProxyConstants.FIN_ACCNTS, psd2FinancialAccountList);
		customerMap.put(FraudnetViaMulesoftProxyConstants.ACCOUNT_ID, "1234");

		fraudSystemRequest.put(FraudnetViaMulesoftProxyConstants.FRAUDREQUEST_EVENT_MAP, eventMap);
		fraudSystemRequest.put(FraudnetViaMulesoftProxyConstants.FRAUDREQUEST_DEVICE_MAP, deviceMap);
		fraudSystemRequest.put(FraudnetViaMulesoftProxyConstants.FRAUDREQUEST_CUSTOMER_MAP, customerMap);

		FraudServiceRequest responce = fraudnetViaMulesoftProxyTransformer.transformFraudnetRequest(fraudSystemRequest);

		assertNotNull(responce);
		/*
		 *  
		 */
		deviceMap.put(FraudnetViaMulesoftProxyConstants.SESSION_LOGGEDIN, false);
		responce = fraudnetViaMulesoftProxyTransformer.transformFraudnetRequest(fraudSystemRequest);
		assertNotNull(responce);
		/*
		 * 
		 */
		Map<String, Object> emptyEventMap = new HashMap<String, Object>();
		Map<String, Object> emptyDeviceMap = new HashMap<String, Object>();
		Map<String, Object> emptycustomerMap = new HashMap<String, Object>();
		fraudSystemRequest.put(FraudnetViaMulesoftProxyConstants.FRAUDREQUEST_EVENT_MAP, emptyEventMap);
		fraudSystemRequest.put(FraudnetViaMulesoftProxyConstants.FRAUDREQUEST_DEVICE_MAP, emptyDeviceMap);
		fraudSystemRequest.put(FraudnetViaMulesoftProxyConstants.FRAUDREQUEST_CUSTOMER_MAP, emptycustomerMap);
		responce = fraudnetViaMulesoftProxyTransformer.transformFraudnetRequest(fraudSystemRequest);
		assertNotNull(responce);
		/*
		 * Throwing FraudNet service error processing Header information
		 */
		fraudSystemRequest.put(FraudnetViaMulesoftProxyConstants.FRAUDREQUEST_EVENT_MAP, eventMap);
		fraudSystemRequest.put(FraudnetViaMulesoftProxyConstants.FRAUDREQUEST_DEVICE_MAP, deviceMap);
		fraudSystemRequest.put(FraudnetViaMulesoftProxyConstants.FRAUDREQUEST_CUSTOMER_MAP, customerMap);
		deviceMap.put(FraudnetViaMulesoftProxyConstants.DEVICE_HEADERS, "test");
		fraudnetViaMulesoftProxyTransformer.transformFraudnetRequest(fraudSystemRequest);
	}

//	@Test
	public void transformPaymentSetupPOSTResponseTest2() {

		Map<String, Map<String, Object>> fraudSystemRequest = new HashMap<String, Map<String, Object>>();

		// eventMap
		Map<String, Object> eventMap = new HashMap<String, Object>();
		eventMap.put(FraudnetViaMulesoftProxyConstants.EVENT_TYPE, "Consent");
		eventMap.put(FraudnetViaMulesoftProxyConstants.EVENT_TIME, "2014-02-28T15:59:23.000Z");
		eventMap.put(FraudnetViaMulesoftProxyConstants.EVENT_CHANNEL, "Online");
		eventMap.put(FraudnetViaMulesoftProxyConstants.EVENT_SOURCE_SYSTEM, "API Platform");
		eventMap.put(FraudnetViaMulesoftProxyConstants.EVENT_SOURCE_SUBSYSTEM, "PISP");
		eventMap.put(FraudnetViaMulesoftProxyConstants.TRANSFER_AMNT, "000");
		eventMap.put(FraudnetViaMulesoftProxyConstants.TRANSFER_ACCOUNT_NUMBER, "1928774024");
		eventMap.put(FraudnetViaMulesoftProxyConstants.EVENT_ID, "Test");
		eventMap.put(FraudnetViaMulesoftProxyConstants.TRANSFER_MEMO, "Test");
		eventMap.put(FraudnetViaMulesoftProxyConstants.TRANSFER_ID, "Test");
		eventMap.put(FraudnetViaMulesoftProxyConstants.TRANSFER_TIME, "2014-02-28T15:59:23.000Z");
		eventMap.put(FraudnetViaMulesoftProxyConstants.EVENT_MODEL_CODE, "PISPL");
		// deviceMap
		Map<String, Object> deviceMap = new HashMap<String, Object>();
		deviceMap.put(FraudnetViaMulesoftProxyConstants.DEVICE_IP, "11.5.141.202");
		deviceMap.put(FraudnetViaMulesoftProxyConstants.DEVICE_JSC, "kla44j1e3NlY5BSo9z4ofjb75PaK4Vpjt5nrU8s8vWuIUgxrmTTuCUpMcUGejYO KES5jfyEwHXXTSHCRR0QOtWyFGh8cmvSuCKzIlnY6x2KlT64K3H.ppAJZ7OQuyP BB2SCXw2SCYOvYDy25adjjftckcKyAd65hz7qTvtE0EREHQxbiyInrGfyex2uCK wQ9dvcpxUlzXJJIneGfYVAQEBEm1CdC5MQjGejuTDRNzcPiAksecXF5iTmk6eAX vIdVuxISg0QWvOe9fCMGa2hUMnGWpwoNSUC56MnGW87gq1HACVcHkxI5_1.9ihy ppAIKWbZcFKV8NTghN.nkre9zH_y3ExnJpyWVEL3NvWurk51lVB4WG.CNOt96h L._Wu_0L.BwCtOMu_Ep.ziPajoMu5.VNNW5BSuxIgtaqpRxuYIdw0xO9sarwyjJ vDOhhMETcouU.Uz8464qnvvYIw5Epir6UtTvqbRyhmgiFEjsnzxK9B5qfZvQAuZ a2bU0tzrU9juBhElbElLAUugLyTUbyATf92PIiyhqOfjVrwxN_l3yoonkJgI E_X_Qs796tlnhqvnmccbguaDcujhDna2QKlNdHlAmjJvDOhhM4XM0oGN_tvfvCS nBNleW5BNlan0QkBMfs.8gC");
		deviceMap.put(FraudnetViaMulesoftProxyConstants.DEVICE_PAYLOAD, "rO0ABXNyACdjb20udGhlNDEuY29tbW9ucy5jcnlwdG8uQ3J5cHRvRW52ZWxvcGUAAJbgqPhc8wIAA0wABWFsaWFzdAASTGphdmEvbGFuZy9TdHJpbmc7WwAMZW5jcnlwdGVkS2V5dAACW0JbABBlbmNyeXB0ZWRQYXlsb2FkcQB-");
		Map<String, String> headerData = new HashMap<String, String>();
		headerData.put("X-HTTP-Header", "header value");
		deviceMap.put(FraudnetViaMulesoftProxyConstants.DEVICE_HEADERS, headerData);
		Cookie cookie = new Cookie("SessionControllCookie", "abcd");
		Cookie[] cookies = new Cookie[]{cookie};
		deviceMap.put(FraudnetViaMulesoftProxyConstants.DEVICE_COOKIES, cookies);
		deviceMap.put(FraudnetViaMulesoftProxyConstants.SESSION_ID, "49C5E3479AAA14B676609186B7D2E234");
		deviceMap.put(FraudnetViaMulesoftProxyConstants.SESSION_LOGGEDIN, true);
		deviceMap.put(FraudnetViaMulesoftProxyConstants.DEVICE_HEADERS, "{\"X-HTTP-Header\":\"header value\"}");
		// customerMap
		Map<String, Object> customerMap = new HashMap<String, Object>();
		List<PSD2FinancialAccount> psd2FinancialAccountList = new ArrayList<PSD2FinancialAccount>();
		PSD2FinancialAccount psd2FinancialAccount = new PSD2FinancialAccount();
		psd2FinancialAccount.setType("test");
		psd2FinancialAccount.setHashedAccountNumber("1234");
		psd2FinancialAccount.setRoutingNumber("1234");
		psd2FinancialAccount.setSubType("Current Account");
		psd2FinancialAccountList.add(psd2FinancialAccount);
		customerMap.put(FraudnetViaMulesoftProxyConstants.FIN_ACCNTS, psd2FinancialAccountList);
		customerMap.put(FraudnetViaMulesoftProxyConstants.ACCOUNT_ID, "1234");

		fraudSystemRequest.put(FraudnetViaMulesoftProxyConstants.FRAUDREQUEST_EVENT_MAP, eventMap);
		fraudSystemRequest.put(FraudnetViaMulesoftProxyConstants.FRAUDREQUEST_DEVICE_MAP, deviceMap);
		fraudSystemRequest.put(FraudnetViaMulesoftProxyConstants.FRAUDREQUEST_CUSTOMER_MAP, customerMap);

		FraudServiceRequest responce = fraudnetViaMulesoftProxyTransformer.transformFraudnetRequest(fraudSystemRequest);

		assertNotNull(responce);
		/*
		 * 
		 */
		fraudSystemRequest.put(FraudnetViaMulesoftProxyConstants.FRAUDREQUEST_EVENT_MAP, null);
		fraudSystemRequest.put(FraudnetViaMulesoftProxyConstants.FRAUDREQUEST_DEVICE_MAP, null);
		fraudSystemRequest.put(FraudnetViaMulesoftProxyConstants.FRAUDREQUEST_CUSTOMER_MAP, null);
		//responce = fraudnetViaMulesoftProxyTransformer.transformFraudnetRequest(fraudSystemRequest);
		assertNotNull(responce);
	}
	
	@Test
	public void transformFraudnetResponse1() {

		FraudServiceResponse fraudServiceResponse = new FraudServiceResponse();
		FraudServiceResponse responce = fraudnetViaMulesoftProxyTransformer.transformFraudnetResponse(fraudServiceResponse);
		assertNotNull(responce);
	}

	@Test(expected = AdapterException.class)
	public void transformFraudnetResponse2() {
		FraudServiceResponse fraudServiceResponse = null;
		fraudnetViaMulesoftProxyTransformer.transformFraudnetResponse(fraudServiceResponse);

	}
}
