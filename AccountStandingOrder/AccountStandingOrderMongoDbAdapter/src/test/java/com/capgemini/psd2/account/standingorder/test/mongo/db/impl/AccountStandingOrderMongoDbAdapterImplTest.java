package com.capgemini.psd2.account.standingorder.test.mongo.db.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyString;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.map.HashedMap;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.account.standingorder.mongo.adapter.test.mock.data.AccountStandingOrderMockData;
import com.capgemini.psd2.account.standingorder.mongo.db.adapter.impl.AccountStandingOrderMongoDbAdapterImpl;
import com.capgemini.psd2.account.standingorder.mongo.db.adapter.repository.AccountStandingOrderRepository;
import com.capgemini.psd2.aisp.mongo.db.adapter.domain.AccountStandingOrderCMA2;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountBalanceResponse;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountStandingOrdersResponse;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.exceptions.OBPSD2ExceptionUtility;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.LoggerUtils;
import com.capgemini.psd2.utilities.SandboxValidationUtility;

@RunWith(SpringJUnit4ClassRunner.class)
public class AccountStandingOrderMongoDbAdapterImplTest {
	@Mock
	AccountStandingOrderRepository accountStandingOrderRepository;

	@Mock
	LoggerUtils loggerUtils;
	
	@Mock
	SandboxValidationUtility utility;

	/** The account standing order mongo DB adapter impl. */
	@InjectMocks
	private AccountStandingOrderMongoDbAdapterImpl accountStandingOrderMongoDbAdapterImpl;

	/**
	 * Test retrieve blank response account standing order success flow.
	 */

	/**
	 * Sets the up.
	 *
	 * @throws Exception
	 *             the exception
	 */
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		Map<String, String> map=new HashMap<>();
		map.put("SIGNATURE", "Something wrong with x-jws-signature request header");
		Map<String, String> specificErrorMessageMap=new HashMap<>();
		specificErrorMessageMap.put("signature_invalid_content", "Invalid content prsent in joseheader of x-jws-signature header");
		specificErrorMessageMap.put("signature_missing", "signature header missing in request");
		OBPSD2ExceptionUtility.genericErrorMessages.putAll(map);
		OBPSD2ExceptionUtility.specificErrorMessages.putAll(specificErrorMessageMap);
	}

	@Test
	public void testRetrieveAccountStandingOrdersSuccessFlow() {
		AccountMapping accountMapping = new AccountMapping();
		ArrayList<AccountDetails> accntDetailList = new ArrayList<>();
		AccountDetails accountDetails = new AccountDetails();
		Map<String, String> params = new HashMap<>();

		accountDetails.setAccountId("f4483fda-81be-4873-b4a6-20375b7f55c1");
		accntDetailList.add(accountDetails);
		accountMapping.setAccountDetails(accntDetailList);
		
		Mockito.when(utility.isValidPsuId(accountMapping.getPsuId())).thenReturn(false);

		List<AccountStandingOrderCMA2> standingOrderList = new ArrayList<>();
		AccountStandingOrderCMA2 standingOrder = new AccountStandingOrderCMA2();
		standingOrderList.add(standingOrder);
		Mockito.when(accountStandingOrderRepository.findByAccountNumberAndAccountNSC(anyString(), anyString()))
				.thenReturn(standingOrderList);

		Mockito.when(loggerUtils.populateLoggerData(anyString()))
				.thenReturn(AccountStandingOrderMockData.getMockLoggerData());

		PlatformAccountStandingOrdersResponse actualResponse = accountStandingOrderMongoDbAdapterImpl
				.retrieveAccountStandingOrders(accountMapping, params);

		assertEquals(AccountStandingOrderMockData.getMockExpectedStandingOrderResponse(), actualResponse.getoBReadStandingOrder6());
	}
	
	@Test(expected = PSD2Exception.class)
	public void testRetrieveAccountStandingOrdersExceptionFlow() {
		AccountMapping accountMapping = new AccountMapping();
		ArrayList<AccountDetails> accntDetailList = new ArrayList<>();
		AccountDetails accountDetails = new AccountDetails();
		Map<String, String> params = new HashMap<>();

		accountDetails.setAccountId("f4483fda-81be-4873-b4a6-20375b7f55c1");
		accntDetailList.add(accountDetails);
		accountMapping.setAccountDetails(accntDetailList);
		
		Mockito.when(utility.isValidPsuId(accountMapping.getPsuId())).thenReturn(false);

		Mockito.when(accountStandingOrderRepository.findByAccountNumberAndAccountNSC(anyString(), anyString()))
				.thenThrow(DataAccessResourceFailureException.class);

		Mockito.when(loggerUtils.populateLoggerData(anyString()))
				.thenReturn(AccountStandingOrderMockData.getMockLoggerData());

		PlatformAccountStandingOrdersResponse actualResponse = accountStandingOrderMongoDbAdapterImpl
				.retrieveAccountStandingOrders(accountMapping, params);

		assertEquals(AccountStandingOrderMockData.getMockExpectedStandingOrderResponse(), actualResponse.getoBReadStandingOrder6());
	}
	
	@Test
	public void testRetrieveAccountStandingOrdersNullFlow() {
		AccountMapping accountMapping = new AccountMapping();
		ArrayList<AccountDetails> accntDetailList = new ArrayList<>();
		AccountDetails accountDetails = new AccountDetails();
		Map<String, String> params = new HashMap<>();

		accountDetails.setAccountId("f4483fda-81be-4873-b4a6-20375b7f55c1");
		accntDetailList.add(accountDetails);
		accountMapping.setAccountDetails(accntDetailList);
		
		Mockito.when(utility.isValidPsuId(accountMapping.getPsuId())).thenReturn(false);

		Mockito.when(accountStandingOrderRepository.findByAccountNumberAndAccountNSC(anyString(), anyString()))
				.thenReturn(null);

		Mockito.when(loggerUtils.populateLoggerData(anyString()))
				.thenReturn(AccountStandingOrderMockData.getMockLoggerData());

		PlatformAccountStandingOrdersResponse actualResponse = accountStandingOrderMongoDbAdapterImpl
				.retrieveAccountStandingOrders(accountMapping, params);

	}
	
	@Test(expected = PSD2Exception.class)
	public void testRetrieveAccountWhenIsValidPsuId() {
		AccountMapping accountMapping = new AccountMapping();
		ArrayList<AccountDetails> accountDetailsList = new ArrayList<>();
		AccountDetails accountDetails = new AccountDetails();
		Map<String, String> params = new HashedMap();
		
		accountDetails.setAccountId("123");
		accountDetailsList.add(accountDetails);
		accountMapping.setAccountDetails(accountDetailsList);
		
		Mockito.when(utility.isValidPsuId(accountMapping.getPsuId())).thenReturn(true);

		PlatformAccountStandingOrdersResponse actualResponse = accountStandingOrderMongoDbAdapterImpl
				.retrieveAccountStandingOrders(accountMapping, params);

	}

}
