package com.capgemini.psd2.account.standingorder.mongo.adapter.test.mock.data;

import java.util.ArrayList;
import java.util.List;

import com.capgemini.psd2.aisp.domain.OBReadStandingOrder6;
import com.capgemini.psd2.aisp.domain.OBReadStandingOrder6Data;
import com.capgemini.psd2.aisp.domain.OBStandingOrder6;
import com.capgemini.psd2.aisp.mongo.db.adapter.domain.AccountStandingOrderCMA2;
import com.capgemini.psd2.logger.LoggerAttribute;

public class AccountStandingOrderMockData {

	 public static List<AccountStandingOrderCMA2> getMockAccountDetails()
	 {
		 return new ArrayList<AccountStandingOrderCMA2>();
	 }
 
	 public static LoggerAttribute getMockLoggerData() {
			LoggerAttribute x = new LoggerAttribute();
			x.setApiId("testApiID");
			return x;
		}
	 
	 public static OBReadStandingOrder6 getMockExpectedStandingOrderResponse()
	 {
		 OBReadStandingOrder6Data obReadStandingOrderData = new OBReadStandingOrder6Data();
		 OBReadStandingOrder6 obReadStandingOrder = new OBReadStandingOrder6();
		 AccountStandingOrderCMA2 mockStandingOrder = new AccountStandingOrderCMA2();
		 
		 mockStandingOrder.setAccountId("f4483fda-81be-4873-b4a6-20375b7f55c1");
		 
		 List<OBStandingOrder6> standingOrderList = new ArrayList<>();
		 standingOrderList.add(mockStandingOrder);
		 obReadStandingOrderData.setStandingOrder(standingOrderList);
		 obReadStandingOrder.setData(obReadStandingOrderData);
		 return obReadStandingOrder;
	 }
}
