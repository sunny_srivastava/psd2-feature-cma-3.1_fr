package com.capgemini.psd2.account.standingorder.mongo.db.adapter.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.account.standingorder.mongo.db.adapter.repository.AccountStandingOrderRepository;
import com.capgemini.psd2.aisp.adapter.AccountStandingOrdersAdapter;
import com.capgemini.psd2.aisp.domain.OBReadStandingOrder6;
import com.capgemini.psd2.aisp.domain.OBReadStandingOrder6Data;
import com.capgemini.psd2.aisp.domain.OBStandingOrder6;
import com.capgemini.psd2.aisp.mongo.db.adapter.domain.AccountStandingOrderCMA2;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountStandingOrdersResponse;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.InternalServerErrorMessage;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.utilities.SandboxValidationUtility;

@Component
public class AccountStandingOrderMongoDbAdapterImpl implements AccountStandingOrdersAdapter {
	
	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(AccountStandingOrderMongoDbAdapterImpl.class);
	
	@Autowired
	private AccountStandingOrderRepository repository; 
	
	@Autowired
	private SandboxValidationUtility utility;
	
	@Override
	public PlatformAccountStandingOrdersResponse retrieveAccountStandingOrders(AccountMapping accountMapping,Map<String, String> params) {

		PlatformAccountStandingOrdersResponse platformAccountStandingOrdersResponse = new PlatformAccountStandingOrdersResponse();
		OBReadStandingOrder6 oBReadStandingOrder = new OBReadStandingOrder6();
		List<AccountStandingOrderCMA2> mockStandingOrder = new ArrayList<>();
		LOG.info("TenantId : " + params.get(PSD2Constants.TENANT_ID));
		
		try {
			if(utility.isValidPsuId(accountMapping.getPsuId())){
				throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,InternalServerErrorMessage.SANDBOX_MOCKED_ERROR));
			}
			mockStandingOrder = repository.findByAccountNumberAndAccountNSC(
					accountMapping.getAccountDetails().get(0).getAccountNumber(),
					accountMapping.getAccountDetails().get(0).getAccountNSC());
		} catch (DataAccessResourceFailureException e) {
			LOG.error(e.getMessage(),e);
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
					InternalServerErrorMessage.CONNECTION_ERROR));
		}
		
		List<OBStandingOrder6> standingOrderList = new ArrayList<>();
		
		if (null != mockStandingOrder && !mockStandingOrder.isEmpty()) {
			for (OBStandingOrder6 standingOrder : mockStandingOrder) {
				standingOrder.setAccountId(accountMapping.getAccountDetails().get(0).getAccountId());
				standingOrderList.add(standingOrder);
			}
		}
     
		OBReadStandingOrder6Data data = new OBReadStandingOrder6Data();
		data.standingOrder(standingOrderList);
		oBReadStandingOrder.setData(data);
		platformAccountStandingOrdersResponse.setoBReadStandingOrder6(oBReadStandingOrder);
		return platformAccountStandingOrdersResponse;
		
	}	
}