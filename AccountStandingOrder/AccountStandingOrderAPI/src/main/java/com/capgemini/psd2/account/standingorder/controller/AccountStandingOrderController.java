package com.capgemini.psd2.account.standingorder.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.capgemini.psd2.account.standingorder.service.AccountStandingOrderService;
import com.capgemini.psd2.aisp.domain.OBReadStandingOrder6;

@RestController
public class AccountStandingOrderController {

@Autowired	
private AccountStandingOrderService service;
	
	@RequestMapping(value ="/accounts/{accountId}/standing-orders", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public OBReadStandingOrder6 retrieveAccountStandingOrders(@PathVariable("accountId") String accountId) {

		return service.retrieveAccountStandingOrders(accountId);
	}
}
