package com.capgemini.psd2.account.standingorder.test.service.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.account.standingorder.service.impl.AccountStandingOrderServiceImpl;
import com.capgemini.psd2.account.standingorder.test.mockdata.AccountStandingOrderMockData;
import com.capgemini.psd2.aisp.account.mapping.adapter.AccountMappingAdapter;
import com.capgemini.psd2.aisp.adapter.AccountStandingOrdersAdapter;
import com.capgemini.psd2.aisp.adapter.AispConsentAdapter;
import com.capgemini.psd2.aisp.domain.OBReadStandingOrder6;
import com.capgemini.psd2.aisp.validation.adapter.impl.AccountStandingOrdersValidatorImpl;
import com.capgemini.psd2.exceptions.OBPSD2ExceptionUtility;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.LoggerUtils;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.response.validator.ResponseValidator;

@RunWith(SpringJUnit4ClassRunner.class)
public class AccountStandingOrderServiceImplTest{

	/** The adapter. */
	@Mock
	private AccountStandingOrdersAdapter adapter;

	/** The consent mapping. */
	@Mock
	private AispConsentAdapter aispConsentAdapter;

	/** The header atttributes. */
	@Mock
	private RequestHeaderAttributes headerAtttributes;

	@Mock
	private AccountMappingAdapter accountMappingAdapter;

	@Mock
	private ResponseValidator responseValidator;

	/** The service. */
	@InjectMocks
	private AccountStandingOrderServiceImpl service = new AccountStandingOrderServiceImpl();

	@Mock
	private LoggerUtils loggerUtils;
	
	@Mock
	private AccountStandingOrdersValidatorImpl accountStandingOrdersValidatorImpl;

	/**
	 * Sets the up.
	 *
	 * @throws Exception
	 *             the exception
	 */
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		when(headerAtttributes.getToken()).thenReturn(AccountStandingOrderMockData.getToken());
		when(aispConsentAdapter.retrieveAccountMapping(anyString()))
				.thenReturn(AccountStandingOrderMockData.getMockAccountMapping());
		Map<String, String> map=new HashMap<>();
		map.put("SIGNATURE", "Something wrong with x-jws-signature request header");
		Map<String, String> specificErrorMessageMap=new HashMap<>();
		specificErrorMessageMap.put("signature_invalid_content", "Invalid content prsent in joseheader of x-jws-signature header");
		specificErrorMessageMap.put("signature_missing", "signature header missing in request");
		OBPSD2ExceptionUtility.genericErrorMessages.putAll(map);
		OBPSD2ExceptionUtility.specificErrorMessages.putAll(specificErrorMessageMap);

	}

	@Test
	public void retrieveAccountStandingOrderWithLinksMetaTest() {
		accountStandingOrdersValidatorImpl.validateUniqueId(anyObject());

		Mockito.when(aispConsentAdapter.validateAndRetrieveConsentByAccountId(anyString(), anyString()))
				.thenReturn(AccountStandingOrderMockData.getMockAispConsent());

		Mockito.when(accountMappingAdapter.retrieveAccountMappingDetails(anyString(), anyObject()))
				.thenReturn(AccountStandingOrderMockData.getMockAccountMapping());

		Mockito.when(adapter.retrieveAccountStandingOrders(anyObject(), anyObject()))
				.thenReturn(AccountStandingOrderMockData.getMockPlatformAccountStandingOrderResponseWithLinksMeta());

		Mockito.when(loggerUtils.populateLoggerData(anyString()))
				.thenReturn(AccountStandingOrderMockData.getMockLoggerData());

		accountStandingOrdersValidatorImpl.validateResponseParams(anyObject());

		OBReadStandingOrder6 actualResponse = service
				.retrieveAccountStandingOrders("f4483fda-81be-4873-b4a6-20375b7f55c1");

		assertEquals(AccountStandingOrderMockData.getMockExpectedAccountStandingOrderResponseWithLinksMeta(),
				actualResponse);

	}

	@Test
	public void retrieveAccountStandingOrderWithoutLinksMetaTest() {
		accountStandingOrdersValidatorImpl.validateUniqueId(anyObject());
		
		Mockito.when(aispConsentAdapter.validateAndRetrieveConsentByAccountId(anyString(), anyString()))
				.thenReturn(AccountStandingOrderMockData.getMockAispConsent());

		Mockito.when(accountMappingAdapter.retrieveAccountMappingDetails(anyString(), anyObject()))
				.thenReturn(AccountStandingOrderMockData.getMockAccountMapping());

		Mockito.when(adapter.retrieveAccountStandingOrders(anyObject(), anyObject()))
				.thenReturn(AccountStandingOrderMockData.getMockPlatformAccountStandingOrderResponseWithoutLinksMeta());

		Mockito.when(loggerUtils.populateLoggerData(anyString()))
				.thenReturn(AccountStandingOrderMockData.getMockLoggerData());

		accountStandingOrdersValidatorImpl.validateResponseParams(anyObject());

		OBReadStandingOrder6 actualResponse = service.retrieveAccountStandingOrders("123");

		assertEquals(AccountStandingOrderMockData.getMockExpectedAccountStandingOrderResponseWithoutLinksMeta(),
				actualResponse);

	}
	
	@Test(expected = PSD2Exception.class)
	public void retrieveAccountStandingOrderWithNullRetrievalStandingOrders() {
		accountStandingOrdersValidatorImpl.validateUniqueId(anyObject());

		Mockito.when(aispConsentAdapter.validateAndRetrieveConsentByAccountId(anyString(), anyString()))
				.thenReturn(AccountStandingOrderMockData.getMockAispConsent());

		Mockito.when(accountMappingAdapter.retrieveAccountMappingDetails(anyString(), anyObject()))
				.thenReturn(AccountStandingOrderMockData.getMockAccountMapping());

		Mockito.when(adapter.retrieveAccountStandingOrders(anyObject(), anyObject()))
				.thenReturn(null);

		Mockito.when(loggerUtils.populateLoggerData(anyString()))
				.thenReturn(AccountStandingOrderMockData.getMockLoggerData());

		accountStandingOrdersValidatorImpl.validateResponseParams(anyObject());

		OBReadStandingOrder6 actualResponse = service
				.retrieveAccountStandingOrders("f4483fda-81be-4873-b4a6-20375b7f55c1");

		assertEquals(AccountStandingOrderMockData.getMockExpectedAccountStandingOrderResponseWithLinksMeta(),
				actualResponse);

	}
	
	@Test(expected = PSD2Exception.class)
	public void retrieveAccountStandingOrderWithNullTppStandingOrderResponse() {
		accountStandingOrdersValidatorImpl.validateUniqueId(anyObject());
		
		Mockito.when(aispConsentAdapter.validateAndRetrieveConsentByAccountId(anyString(), anyString()))
				.thenReturn(AccountStandingOrderMockData.getMockAispConsent());

		Mockito.when(accountMappingAdapter.retrieveAccountMappingDetails(anyString(), anyObject()))
				.thenReturn(AccountStandingOrderMockData.getMockAccountMapping());

		Mockito.when(adapter.retrieveAccountStandingOrders(anyObject(), anyObject()))
				.thenReturn(AccountStandingOrderMockData.getMockPlatformAccountStandingOrderResponseWithoutLinksMeta());
		
		Mockito.when(adapter.retrieveAccountStandingOrders(anyObject(), anyObject()).getoBReadStandingOrder6())
		.thenReturn(null);
		
		Mockito.when(loggerUtils.populateLoggerData(anyString()))
				.thenReturn(AccountStandingOrderMockData.getMockLoggerData());

		accountStandingOrdersValidatorImpl.validateResponseParams(anyObject());

		OBReadStandingOrder6 actualResponse = service.retrieveAccountStandingOrders("123");

		assertEquals(AccountStandingOrderMockData.getMockExpectedAccountStandingOrderResponseWithoutLinksMeta(),
				actualResponse);

	}

	/**
	 * Tear down.
	 *
	 * @throws Exception
	 *             the exception
	 */
	@After
	public void tearDown() throws Exception {
		service = null;
	}

}
