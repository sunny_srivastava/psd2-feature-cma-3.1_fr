package com.capgemini.psd2.account.standingorder.routing.adapter.test.impl;

import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyString;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.capgemini.psd2.account.standingorder.routing.adapter.impl.AccountStandingOrderRoutingAdapter;
import com.capgemini.psd2.account.standingorder.routing.adapter.routing.AccountStandingOrderAdapterFactory;
import com.capgemini.psd2.account.standingorder.routing.adapter.test.adapter.AccountStandingOrderTestRoutingAdapter;
import com.capgemini.psd2.aisp.adapter.AccountStandingOrdersAdapter;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountStandingOrdersResponse;
import com.capgemini.psd2.exceptions.OBPSD2ExceptionUtility;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.token.Token;

public class AccountStandingOrderRoutingAdapterTest {

	/** The account standing order adapter factory. */
	@Mock
	private AccountStandingOrderAdapterFactory accountStandingOrderAdapterFactory;

	@Mock
	private RequestHeaderAttributes reqHeaderAtrributes;

	/** The account standing order routing adapter. */
	@InjectMocks
	private AccountStandingOrdersAdapter accountStandingOrderRoutingAdapter = new AccountStandingOrderRoutingAdapter();

	/**
	 * Sets the up.
	 *
	 * @throws Exception
	 *             the exception
	 */
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		Map<String, String> map=new HashMap<>();
		map.put("SIGNATURE", "Something wrong with x-jws-signature request header");
		Map<String, String> specificErrorMessageMap=new HashMap<>();
		specificErrorMessageMap.put("signature_invalid_content", "Invalid content prsent in joseheader of x-jws-signature header");
		specificErrorMessageMap.put("signature_missing", "signature header missing in request");
		OBPSD2ExceptionUtility.genericErrorMessages.putAll(map);
		OBPSD2ExceptionUtility.specificErrorMessages.putAll(specificErrorMessageMap);
	}

	/**
	 * Retrieve account balancetest.
	 */
	@Test
	public void retrieveAccountStandingOrdertest() {

		AccountStandingOrdersAdapter accountStandingOrdersAdapter = new AccountStandingOrderTestRoutingAdapter();
		Mockito.when(accountStandingOrderAdapterFactory.getAdapterInstance(anyString()))
		.thenReturn(accountStandingOrdersAdapter);
		Token token = new Token();
		Map<String, String> map  = new HashMap<>();
		token.setSeviceParams(map);
		Mockito.when(reqHeaderAtrributes.getToken()).thenReturn(token);
		PlatformAccountStandingOrdersResponse platformAccountStandingOrdersResponse = accountStandingOrderRoutingAdapter
				.retrieveAccountStandingOrders(null, null);

		assertTrue(
				platformAccountStandingOrdersResponse.getoBReadStandingOrder6().getData().getStandingOrder().isEmpty());

	}

}
