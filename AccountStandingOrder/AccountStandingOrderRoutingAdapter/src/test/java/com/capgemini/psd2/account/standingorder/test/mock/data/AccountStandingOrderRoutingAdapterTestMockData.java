package com.capgemini.psd2.account.standingorder.test.mock.data;

import java.util.ArrayList;
import java.util.List;

import com.capgemini.psd2.aisp.domain.OBReadStandingOrder6;
import com.capgemini.psd2.aisp.domain.OBReadStandingOrder6Data;
import com.capgemini.psd2.aisp.domain.OBStandingOrder6;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountStandingOrdersResponse;

public class AccountStandingOrderRoutingAdapterTestMockData {
	
	public static PlatformAccountStandingOrdersResponse getMockStandingOrdersGETResponse() {
		
		PlatformAccountStandingOrdersResponse platformAccountStandingOrdersResponse = new PlatformAccountStandingOrdersResponse();
		OBReadStandingOrder6Data obReadStandingOrderData = new OBReadStandingOrder6Data();
		List<OBStandingOrder6> obStandingOrderList = new ArrayList<>();
		obReadStandingOrderData.setStandingOrder(obStandingOrderList);
		OBReadStandingOrder6 obReadStandingOrder = new OBReadStandingOrder6();
		obReadStandingOrder.setData(obReadStandingOrderData);
		platformAccountStandingOrdersResponse.setoBReadStandingOrder6(obReadStandingOrder);
		return platformAccountStandingOrdersResponse;
	}

}
