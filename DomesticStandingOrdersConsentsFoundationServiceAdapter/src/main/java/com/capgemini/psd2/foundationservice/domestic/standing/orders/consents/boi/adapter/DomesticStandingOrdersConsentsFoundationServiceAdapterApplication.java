package com.capgemini.psd2.foundationservice.domestic.standing.orders.consents.boi.adapter;



import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.foundationservice.domestic.standing.orders.consents.boi.adapter.client.DomesticStandingOrdersConsentsFoundationServiceClient;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.consents.boi.adapter.delegate.DomesticStandingOrdersConsentsFoundationServiceDelegate;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.consents.boi.adapter.raml.domain.StandingOrderInstructionProposal;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.consents.boi.adapter.transformer.DomesticStandingOrdersConsentsFoundationServiceTransformer;
import com.capgemini.psd2.pisp.domain.CustomDStandingOrderConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomDStandingOrderConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.OBExternalConsentStatus1Code;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.pisp.stage.operations.adapter.DomesticStandingOrdersPaymentStagingAdapter;
import com.capgemini.psd2.rest.client.model.RequestInfo;



@Component("domesticStandingOrdersConsentsFoundationServiceAdapter")
public class DomesticStandingOrdersConsentsFoundationServiceAdapterApplication implements DomesticStandingOrdersPaymentStagingAdapter  {

	@Autowired
	private DomesticStandingOrdersConsentsFoundationServiceDelegate domStandingOrdersConsDelegate;

	@Autowired
	private DomesticStandingOrdersConsentsFoundationServiceClient domStandingOrdersConsClient;
	
	@Autowired
	private DomesticStandingOrdersConsentsFoundationServiceTransformer domPayConsTransformer;
	
	@Override
	public CustomDStandingOrderConsentsPOSTResponse processStandingOrderConsents(
			CustomDStandingOrderConsentsPOSTRequest standingOrderConsentsRequest,
			CustomPaymentStageIdentifiers customStageIdentifiers, Map<String, String> params,
			OBExternalConsentStatus1Code successStatus, OBExternalConsentStatus1Code failureStatus) {
		
		RequestInfo requestInfo = new RequestInfo();
		
		HttpHeaders httpHeaders = domStandingOrdersConsDelegate.createPaymentRequestHeadersForProcess(params);
		
		String domesticPaymentPostURL = domStandingOrdersConsDelegate.postPaymentFoundationServiceURL(customStageIdentifiers.getPaymentSetupVersion());
		
		requestInfo.setUrl(domesticPaymentPostURL);
		
		StandingOrderInstructionProposal standingInstructionProposalRequest = domPayConsTransformer.transformDomesticStandingOrdersResponseFromAPIToFDForInsert(standingOrderConsentsRequest,params);
		
		StandingOrderInstructionProposal standingInstructionProposalResponse = domStandingOrdersConsClient.restTransportForDomesticStandingOrdersServicePost(requestInfo, standingInstructionProposalRequest, StandingOrderInstructionProposal.class, httpHeaders);
		
		return domPayConsTransformer.transformDomesticStandingOrdersResponseFromFDToAPIForInsert(standingInstructionProposalResponse);
	}
	
	@Override
	public CustomDStandingOrderConsentsPOSTResponse retrieveStagedDomesticStandingOrdersConsents(
			CustomPaymentStageIdentifiers customPaymentStageIdentifiers, Map<String, String> params) {
		
		RequestInfo requestInfo = new RequestInfo();

		HttpHeaders httpHeaders = domStandingOrdersConsDelegate.createPaymentRequestHeadersForGet(params);
		
		String domesticPaymentURL = domStandingOrdersConsDelegate.getPaymentFoundationServiceURL(customPaymentStageIdentifiers.getPaymentConsentId(),customPaymentStageIdentifiers.getPaymentSetupVersion());
        
		requestInfo.setUrl(domesticPaymentURL);
	
		StandingOrderInstructionProposal standingInsProposal = domStandingOrdersConsClient.restTransportForDomesticStandingOrderServiceGet(requestInfo, StandingOrderInstructionProposal.class, httpHeaders);
		
		return domPayConsTransformer.transformDomesticStandingOrderResponse(standingInsProposal);

	}

}
