package com.capgemini.psd2.foundationservice.domestic.standing.orders.consents.boi.adapter.constants;

public class DomesticStandingOrdersConsentsFoundationServiceConstants {
	public static final String MULE_OUT_OF_BOX_POLICY_ERROR1 = "401";
	public static final String MULE_OUT_OF_BOX_POLICY_ERROR2 = "429";
	public static final String VALIDATION_VIOLATIONS = "validationViolations";
	public static final String Process_API_Code_Post = "PPA_SOIPP_019";
	
	public static final String MULE_OUT_OF_BOX_POLICY_ERROR1_Get = "0401";
	public static final String MULE_OUT_OF_BOX_POLICY_ERROR2_Get = "0429";
	public static final String DYNAMIC_CODE = "FS_SOPSV_001";
}
