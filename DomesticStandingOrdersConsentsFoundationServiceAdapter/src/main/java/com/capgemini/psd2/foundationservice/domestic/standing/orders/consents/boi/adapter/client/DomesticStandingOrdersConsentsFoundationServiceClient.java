package com.capgemini.psd2.foundationservice.domestic.standing.orders.consents.boi.adapter.client;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.capgemini.psd2.adapter.exceptions.AdapterErrorCodeEnum;
import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.adapter.exceptions.AdapterExceptionHandlerImpl;
import com.capgemini.psd2.adapter.exceptions.AdapterOBErrorCodeEnum;
import com.capgemini.psd2.adapter.exceptions.domain.CommonResponse;
import com.capgemini.psd2.adapter.exceptions.domain.ValidationWrapper;
import com.capgemini.psd2.adapter.utility.AdapterUtility;
import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.consents.boi.adapter.constants.DomesticStandingOrdersConsentsFoundationServiceConstants;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.consents.boi.adapter.raml.domain.StandingOrderInstructionProposal;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.RestClientSync;
import com.capgemini.psd2.utilities.NullCheckUtils;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;

@Component
public class DomesticStandingOrdersConsentsFoundationServiceClient {
	
	@Autowired
	@Qualifier("restClientFoundation")
	private RestClientSync restClient;
	
	CommonResponse commonResponse = null;
	
	@Autowired
	private AdapterUtility adapterUtility;
	
	@Autowired
	private RestTemplate restTemplate;
	
	@PostConstruct
	private void init() {
		MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(Include.NON_NULL);
        converter.setObjectMapper(mapper);
        restTemplate.getMessageConverters().add(0,converter);
	}
	
	public StandingOrderInstructionProposal restTransportForDomesticStandingOrdersServicePost(RequestInfo requestInfo,
			StandingOrderInstructionProposal standingInstructionProposalRequest, Class<StandingOrderInstructionProposal> responseType,
			HttpHeaders httpHeaders) {
		try {
			return restClient.callForPost(requestInfo, standingInstructionProposalRequest, responseType, httpHeaders);
		}catch(AdapterException e){
			if((String.valueOf(e.getFoundationError()).trim().equals(DomesticStandingOrdersConsentsFoundationServiceConstants.MULE_OUT_OF_BOX_POLICY_ERROR1))
			||(String.valueOf(e.getFoundationError()).trim().equals(DomesticStandingOrdersConsentsFoundationServiceConstants.MULE_OUT_OF_BOX_POLICY_ERROR2))){
				throw AdapterException.populatePSD2Exception(e.getMessage(), AdapterErrorCodeEnum.BAD_REQUEST,e.getMessage());
			}
			
			commonResponse = (CommonResponse) e.getFoundationError();
			if (commonResponse.getCode().equals(DomesticStandingOrdersConsentsFoundationServiceConstants.Process_API_Code_Post)
					&& commonResponse.getAdditionalInformation().getDetails().toString()
							.contains(DomesticStandingOrdersConsentsFoundationServiceConstants.VALIDATION_VIOLATIONS)) {
				Gson gson = new Gson();
				ValidationWrapper validationViolations = gson.fromJson(
						gson.toJson(commonResponse.getAdditionalInformation().getDetails()),
						ValidationWrapper.class);
				String fsErrorCode = validationViolations.getValidationViolations().getValidationViolation()
						.getErrorCode();
				String ErrorField = validationViolations.getValidationViolations().getValidationViolation().getErrorField();
				String errorMapping = adapterUtility.getThrowableError().get(fsErrorCode);
				if (!NullCheckUtils.isNullOrEmpty(errorMapping)) {
					AdapterOBErrorCodeEnum errorCodeEnum = null;
					if (fsErrorCode.equals(DomesticStandingOrdersConsentsFoundationServiceConstants.DYNAMIC_CODE)) {
						errorCodeEnum = AdapterOBErrorCodeEnum.valueOf(errorMapping);
						errorCodeEnum.setSpecificErrorMessage("Invalid " + ErrorField + " provided.");
					} else
						errorCodeEnum = AdapterOBErrorCodeEnum.valueOf(errorMapping);
					throw AdapterException.populatePSD2Exception(commonResponse.getSummary().trim(), errorCodeEnum,
							commonResponse);
				}
			}
			throw e;
		}
		
	}
	
	public StandingOrderInstructionProposal restTransportForDomesticStandingOrderServiceGet(RequestInfo reqInfo,
			Class<StandingOrderInstructionProposal> responseType, HttpHeaders headers) {
try {
	return restClient.callForGet(reqInfo, responseType, headers);
}catch(AdapterException e) {
	if((String.valueOf(e.getFoundationError()).trim().equals(DomesticStandingOrdersConsentsFoundationServiceConstants.MULE_OUT_OF_BOX_POLICY_ERROR1_Get))
			||(String.valueOf(e.getFoundationError()).trim().equals(DomesticStandingOrdersConsentsFoundationServiceConstants.MULE_OUT_OF_BOX_POLICY_ERROR2_Get))){
				throw AdapterException.populatePSD2Exception(e.getMessage(), AdapterErrorCodeEnum.BAD_REQUEST,e.getMessage());
			}
	throw e;
	
}
		

	}

}
