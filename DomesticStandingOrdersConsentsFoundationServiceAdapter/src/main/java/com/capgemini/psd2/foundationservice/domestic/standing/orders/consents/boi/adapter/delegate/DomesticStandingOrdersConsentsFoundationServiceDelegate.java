package com.capgemini.psd2.foundationservice.domestic.standing.orders.consents.boi.adapter.delegate;

import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import com.capgemini.psd2.adapter.exceptions.AdapterErrorCodeEnum;
import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.consents.boi.adapter.raml.domain.BrandCode3;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.consents.boi.adapter.transformer.DomesticStandingOrdersConsentsFoundationServiceTransformer;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Component
public class DomesticStandingOrdersConsentsFoundationServiceDelegate {
	
	@Autowired
	private DomesticStandingOrdersConsentsFoundationServiceTransformer domesticPaymentConsentsFoundationServiceTransformer;

	@Value("${foundationService.apiChannelCode:#{x-api-channel-code}}")
	private String apiChannelCode;
	
	@Value("${foundationService.sourceUserReqHeader:#{x-api-source-user}}")
	private String sourceUserReqHeader;
	
	@Value("${foundationService.transactioReqHeader:#{x-api-transaction-id}}")
	private String transactioReqHeader;
	
	@Value("${foundationService.correlationMuleReqHeader:#{x-api-correlation-id}}")
	private String correlationMuleReqHeader;
	
	@Value("${foundationService.sourcesystem:#{x-api-source-system}}")
	private String sourcesystem;
	
	@Value("${foundationService.apiChannelCode:#{x-api-party-source-id-number}}")
	private String partySourceId;
	
	@Value("${foundationService.apiChannelBrand:#{X-API-CHANNEL-BRAND}}")
	private String apiChannelBrand;
	
	@Value("${foundationService.systemApiVersion:#{X-SYSTEM-API-VERSION}}")
	private String systemApiVersion;
	
	@Value("${foundationService.domesticStandingOrderConsentBaseURL}")
	private String domesticPaymentConsentBaseURL;
	
	@Value("${foundationService.version}")
	private String version;
	
public HttpHeaders createPaymentRequestHeadersForProcess(Map<String, String> params) {
		
		HttpHeaders httpHeaders = new HttpHeaders();
		
		httpHeaders.add(sourcesystem, "PSD2API");
		if (!NullCheckUtils.isNullOrEmpty(params.get(PSD2Constants.CHANNEL_IN_REQ_HEADER))) {
		httpHeaders.add(apiChannelCode,params.get(PSD2Constants.CHANNEL_IN_REQ_HEADER).toUpperCase());
		}
		
		if (!NullCheckUtils.isNullOrEmpty(params.get(PSD2Constants.USER_IN_REQ_HEADER))) {
		httpHeaders.add(sourceUserReqHeader,params.get(PSD2Constants.USER_IN_REQ_HEADER));
		}
		
		if (!NullCheckUtils.isNullOrEmpty(params.get(PSD2Constants.CORRELATION_REQ_HEADER))) {
		httpHeaders.add(transactioReqHeader,params.get(PSD2Constants.CORRELATION_REQ_HEADER));
		}
		
		httpHeaders.add(correlationMuleReqHeader,"");
		httpHeaders.add("partySourceId","" );		
		httpHeaders.add(systemApiVersion, "3.0");
		if(params.get(PSD2Constants.TENANT_ID).equalsIgnoreCase("BOIUK")) {
			httpHeaders.add(apiChannelBrand, BrandCode3.NIGB.toString());
		}else if(params.get(PSD2Constants.TENANT_ID).equalsIgnoreCase("BOIROI")){
			httpHeaders.add(apiChannelBrand, BrandCode3.ROI.toString());
		}		
		return httpHeaders;
	}

public HttpHeaders createPaymentRequestHeadersForGet(Map<String, String> params) {
	
	HttpHeaders httpHeaders = new HttpHeaders();
	
	httpHeaders.add(sourcesystem, "PSD2API");
	if (!NullCheckUtils.isNullOrEmpty(params.get(PSD2Constants.CHANNEL_IN_REQ_HEADER))) {
	httpHeaders.add(apiChannelCode,params.get(PSD2Constants.CHANNEL_IN_REQ_HEADER).toUpperCase());
	}
	
	if (!NullCheckUtils.isNullOrEmpty(params.get(PSD2Constants.USER_IN_REQ_HEADER))) {
	httpHeaders.add(sourceUserReqHeader,params.get(PSD2Constants.USER_IN_REQ_HEADER));
	}
	
	if (!NullCheckUtils.isNullOrEmpty(params.get(PSD2Constants.CORRELATION_REQ_HEADER))) {
	httpHeaders.add(transactioReqHeader,params.get(PSD2Constants.CORRELATION_REQ_HEADER));
	}
	
	httpHeaders.add(correlationMuleReqHeader,"");
	httpHeaders.add("partySourceId","" );
	
	httpHeaders.add(systemApiVersion, "3.0");
	if(params.get(PSD2Constants.TENANT_ID).equalsIgnoreCase("BOIUK")) {
		httpHeaders.add(apiChannelBrand, BrandCode3.NIGB.toString());
	}else if(params.get(PSD2Constants.TENANT_ID).equalsIgnoreCase("BOIROI")){
		httpHeaders.add(apiChannelBrand, BrandCode3.ROI.toString());
	}		
	
	
	return httpHeaders;
}

public String postPaymentFoundationServiceURL(String setupVersion) {
	
	if (NullCheckUtils.isNullOrEmpty(setupVersion)) {
		throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.NO_ACCOUNT_DATA_FOUND);
	}
	
	return domesticPaymentConsentBaseURL + "/" + version + "/domestic/standing-orders/payment-instruction-proposals";
}

public String getPaymentFoundationServiceURL(String paymentInstuctionProposalId,String setupVersion){
	
	if (NullCheckUtils.isNullOrEmpty(setupVersion)) {
		throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.NO_ACCOUNT_DATA_FOUND);
	}

	if (NullCheckUtils.isNullOrEmpty(paymentInstuctionProposalId)) {
		throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.NO_ACCOUNT_DATA_FOUND);
	}
	
	return domesticPaymentConsentBaseURL + "/" + version + "/" + "domestic/standing-orders/payment-instruction-proposals" + "/" + paymentInstuctionProposalId;
}

}
