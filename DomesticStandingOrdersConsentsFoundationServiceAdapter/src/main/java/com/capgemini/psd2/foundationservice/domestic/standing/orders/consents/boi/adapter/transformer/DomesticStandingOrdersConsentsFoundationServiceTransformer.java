package com.capgemini.psd2.foundationservice.domestic.standing.orders.consents.boi.adapter.transformer;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.adapter.datetime.utility.TimeZoneDateTimeAdapter;
import com.capgemini.psd2.adapter.exceptions.AdapterErrorCodeEnum;
import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.adapter.exceptions.AdapterOBErrorCodeEnum;
import com.capgemini.psd2.adapter.frequency.utility.FrequencyUtil;

import com.capgemini.psd2.foundationservice.domestic.standing.orders.consents.boi.adapter.raml.domain.AccountInformation;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.consents.boi.adapter.raml.domain.Address;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.consents.boi.adapter.raml.domain.AuthorisationType;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.consents.boi.adapter.raml.domain.BrandCode3;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.consents.boi.adapter.raml.domain.Channel;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.consents.boi.adapter.raml.domain.ChannelCode1;
//import com.capgemini.psd2.foundationservice.domestic.standing.orders.consents.boi.adapter.raml.domain.AuthorisingPartyAccount;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.consents.boi.adapter.raml.domain.Country;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.consents.boi.adapter.raml.domain.Currency;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.consents.boi.adapter.raml.domain.FinancialEventAmount;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.consents.boi.adapter.raml.domain.Frequency;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.consents.boi.adapter.raml.domain.PaymentInstrumentRiskFactor;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.consents.boi.adapter.raml.domain.PaymentTransaction;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.consents.boi.adapter.raml.domain.ProposalStatus;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.consents.boi.adapter.raml.domain.ProposingPartyAccount;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.consents.boi.adapter.raml.domain.StandingOrderInstructionProposal;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.pisp.domain.CustomDStandingOrderConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomDStandingOrderConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.OBAuthorisation1;
import com.capgemini.psd2.pisp.domain.OBCashAccountCreditor2;
import com.capgemini.psd2.pisp.domain.OBCashAccountDebtor3;
import com.capgemini.psd2.pisp.domain.OBDomesticStandingOrder1;
import com.capgemini.psd2.pisp.domain.OBDomesticStandingOrder1FirstPaymentAmount;
import com.capgemini.psd2.pisp.domain.OBExternalAuthorisation1Code;
import com.capgemini.psd2.pisp.domain.OBExternalConsentStatus1Code;
import com.capgemini.psd2.pisp.domain.OBExternalPaymentContext1Code;
import com.capgemini.psd2.pisp.domain.OBExternalPermissions2Code;
import com.capgemini.psd2.pisp.domain.OBRisk1;
import com.capgemini.psd2.pisp.domain.OBRisk1DeliveryAddress;
import com.capgemini.psd2.pisp.domain.OBWriteDataDomesticStandingOrderConsentResponse1;
import com.capgemini.psd2.pisp.enums.ProcessConsentStatusEnum;
import com.capgemini.psd2.utilities.NullCheckUtils;
import com.capgemini.psd2.validator.PSD2Validator;

@Component
public class DomesticStandingOrdersConsentsFoundationServiceTransformer {

	@Autowired
	private PSD2Validator psd2Validator;
	@Autowired
	private TimeZoneDateTimeAdapter timeZoneDateTimeAdapter = new TimeZoneDateTimeAdapter();
	@Autowired
	private FrequencyUtil frequencyUtil;

	public StandingOrderInstructionProposal transformDomesticStandingOrdersResponseFromAPIToFDForInsert(
			CustomDStandingOrderConsentsPOSTRequest standingOrderConsentsRequest, Map<String, String> params) {

		StandingOrderInstructionProposal standingOrderInstructionProposal = new StandingOrderInstructionProposal();
		standingOrderInstructionProposal.setPaymentInstructionProposalId("");

		if (!NullCheckUtils.isNullOrEmpty(standingOrderConsentsRequest)) {

			if (!NullCheckUtils.isNullOrEmpty(standingOrderConsentsRequest.getData())) {
				// Permission
				if (!NullCheckUtils.isNullOrEmpty(standingOrderConsentsRequest.getData().getPermission())) {
					standingOrderInstructionProposal
							.setPermission(standingOrderConsentsRequest.getData().getPermission().toString());
				}

				// Initiation
				if (!NullCheckUtils.isNullOrEmpty(standingOrderConsentsRequest.getData().getInitiation())) {
					// Reference
					if (!NullCheckUtils
							.isNullOrEmpty(standingOrderConsentsRequest.getData().getInitiation().getReference())) {
						standingOrderInstructionProposal
								.setReference((standingOrderConsentsRequest.getData().getInitiation().getReference()));
					}

					// Throwing Bad Request if Adapter gets following fields from API:
					// NumberOfPayments,RecurringPaymentDateTime,FinalPaymentDateTime
					Channel channel = new Channel();
					if (!NullCheckUtils
							.isNullOrEmpty(params.get(PSD2Constants.CHANNEL_IN_REQ_HEADER))) {
						
						channel.setChannelCode(ChannelCode1.fromValue((params.get(PSD2Constants.CHANNEL_IN_REQ_HEADER)).toUpperCase()));
					}else {
						channel.setChannelCode(ChannelCode1.API);
					}
					
					if (!NullCheckUtils
							.isNullOrEmpty(params.get(PSD2Constants.TENANT_ID))) {
						
						if(params.get(PSD2Constants.TENANT_ID).equalsIgnoreCase("BOIUK")) {
							channel.setBrandCode(BrandCode3.NIGB);
						}else if(params.get(PSD2Constants.TENANT_ID).equalsIgnoreCase("BOIROI")){
							channel.setBrandCode(BrandCode3.ROI);
						}	
						
					}

					standingOrderInstructionProposal.setChannel(channel);


					
					
					if (!NullCheckUtils.isNullOrEmpty(
							standingOrderConsentsRequest.getData().getInitiation().getNumberOfPayments())) {
						throw AdapterException.populatePSD2Exception("Something wrong with the request body.",
								AdapterOBErrorCodeEnum.OBIE_BAD_REQUEST_NUMBEROFPAYMENTS, "");
					}

					if (!NullCheckUtils.isNullOrEmpty(
							standingOrderConsentsRequest.getData().getInitiation().getRecurringPaymentDateTime())) {

						throw AdapterException.populatePSD2Exception("Something wrong with the request body.",
								AdapterOBErrorCodeEnum.OBIE_BAD_REQUEST_RECURRINGPAYMENTDATETIME, "");
					}
					if (!NullCheckUtils.isNullOrEmpty(
							standingOrderConsentsRequest.getData().getInitiation().getFinalPaymentDateTime())) {

						throw AdapterException.populatePSD2Exception("Something wrong with the request body.",
								AdapterOBErrorCodeEnum.OBIE_BAD_REQUEST_FINALPAYMENTDATETIME, "");

					}

					if (!NullCheckUtils.isNullOrEmpty(
							standingOrderConsentsRequest.getData().getInitiation().getRecurringPaymentAmount())) {
						throw AdapterException.populatePSD2Exception("Something wrong with the request body.",
								AdapterOBErrorCodeEnum.OBIE_BAD_REQUEST_RECURRINGPAYMENTAMOUNT, "");
					}

					if (!NullCheckUtils.isNullOrEmpty(
							standingOrderConsentsRequest.getData().getInitiation().getFinalPaymentAmount())) {
						throw AdapterException.populatePSD2Exception("Something wrong with the request body.",
								AdapterOBErrorCodeEnum.OBIE_BAD_REQUEST_FINALPAYMENTAMOUNT, "");
					}

					// FirstPaymentDateTime
					if (!NullCheckUtils.isNullOrEmpty(
							standingOrderConsentsRequest.getData().getInitiation().getFirstPaymentDateTime())) {
						standingOrderInstructionProposal.setFirstPaymentDateTime(
								standingOrderConsentsRequest.getData().getInitiation().getFirstPaymentDateTime());
					}

					// FirstPaymentAmount
					if (!NullCheckUtils.isNullOrEmpty(
							standingOrderConsentsRequest.getData().getInitiation().getFirstPaymentAmount())) {
						PaymentTransaction firstPaymentAmount = new PaymentTransaction();

						// Amount
						if (!NullCheckUtils.isNullOrEmpty(standingOrderConsentsRequest.getData().getInitiation()
								.getFirstPaymentAmount().getAmount())) {
							FinancialEventAmount financialEventAmount = new FinancialEventAmount();
							financialEventAmount.setTransactionCurrency(Double.parseDouble(standingOrderConsentsRequest
									.getData().getInitiation().getFirstPaymentAmount().getAmount()));
							firstPaymentAmount.setFinancialEventAmount(financialEventAmount);
						}

						// Currency
						if (!NullCheckUtils.isNullOrEmpty(standingOrderConsentsRequest.getData().getInitiation()
								.getFirstPaymentAmount().getCurrency())) {
							Currency transactionCurrency = new Currency();
							transactionCurrency.setIsoAlphaCode(standingOrderConsentsRequest.getData().getInitiation()
									.getFirstPaymentAmount().getCurrency());
							firstPaymentAmount.setTransactionCurrency(transactionCurrency);
						}
						// FirstPaymentAmount
						standingOrderInstructionProposal.setFirstPaymentAmount(firstPaymentAmount);
					}

					// Frequency
					if (!NullCheckUtils
							.isNullOrEmpty(standingOrderConsentsRequest.getData().getInitiation().getFrequency())
							&& !NullCheckUtils.isNullOrEmpty(
									standingOrderConsentsRequest.getData().getInitiation().getFirstPaymentDateTime())) {
						String paymentStartDate = standingOrderConsentsRequest.getData().getInitiation()
								.getFirstPaymentDateTime();
						try {
							SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
							Date firstPaymentDate = simpleDateFormat.parse(paymentStartDate);
							String frequencyCmaToProcessLayer = frequencyUtil.CmaToProcessLayer(
									standingOrderConsentsRequest.getData().getInitiation().getFrequency(),
									firstPaymentDate);
							standingOrderInstructionProposal
									.setFrequency(Frequency.fromValue(frequencyCmaToProcessLayer));
						} catch (ParseException pe) {
							throw AdapterException
									.populatePSD2Exception(AdapterErrorCodeEnum.BAD_REQUEST_PISP_FREQUENCY);
						}
					}

					// DebtorAccount
					if (!NullCheckUtils
							.isNullOrEmpty(standingOrderConsentsRequest.getData().getInitiation().getDebtorAccount())) {
						AccountInformation authorisingPartyAccount = new AccountInformation();
						// SchemeName
						if (!NullCheckUtils.isNullOrEmpty(standingOrderConsentsRequest.getData().getInitiation()
								.getDebtorAccount().getSchemeName())) {
							authorisingPartyAccount.setSchemeName(standingOrderConsentsRequest.getData().getInitiation()
									.getDebtorAccount().getSchemeName());
						}

						// Identification
						if (!NullCheckUtils.isNullOrEmpty(standingOrderConsentsRequest.getData().getInitiation()
								.getDebtorAccount().getIdentification())) {
							authorisingPartyAccount.setAccountIdentification(standingOrderConsentsRequest.getData()
									.getInitiation().getDebtorAccount().getIdentification());
						}

						// Name
						if (!NullCheckUtils.isNullOrEmpty(
								standingOrderConsentsRequest.getData().getInitiation().getDebtorAccount().getName())) {
							authorisingPartyAccount.setAccountName(standingOrderConsentsRequest.getData()
									.getInitiation().getDebtorAccount().getName());
						}

						// SecondaryIdentification
						if (!NullCheckUtils.isNullOrEmpty(standingOrderConsentsRequest.getData().getInitiation()
								.getDebtorAccount().getSecondaryIdentification())) {
							authorisingPartyAccount.setSecondaryIdentification(standingOrderConsentsRequest.getData()
									.getInitiation().getDebtorAccount().getSecondaryIdentification());
						}
						standingOrderInstructionProposal.setAuthorisingPartyAccount(authorisingPartyAccount);
					}

					// CreditorAccount
					if (!NullCheckUtils.isNullOrEmpty(
							standingOrderConsentsRequest.getData().getInitiation().getCreditorAccount())) {
						ProposingPartyAccount proposingPartyAccount = new ProposingPartyAccount();

						// Name
						if (!NullCheckUtils.isNullOrEmpty(standingOrderConsentsRequest.getData().getInitiation()
								.getCreditorAccount().getName())) {
							proposingPartyAccount.setAccountName(standingOrderConsentsRequest.getData().getInitiation()
									.getCreditorAccount().getName());
						}

						// Identification
						if (!NullCheckUtils.isNullOrEmpty(standingOrderConsentsRequest.getData().getInitiation()
								.getCreditorAccount().getIdentification())) {
							proposingPartyAccount.setAccountIdentification(standingOrderConsentsRequest.getData()
									.getInitiation().getCreditorAccount().getIdentification());
						}

						// SchemeName
						if (!NullCheckUtils.isNullOrEmpty(standingOrderConsentsRequest.getData().getInitiation()
								.getCreditorAccount().getSchemeName())) {
							proposingPartyAccount.setSchemeName(standingOrderConsentsRequest.getData().getInitiation()
									.getCreditorAccount().getSchemeName());
						}

						// SecondaryIdentification
						if (!NullCheckUtils.isNullOrEmpty(standingOrderConsentsRequest.getData().getInitiation()
								.getCreditorAccount().getSecondaryIdentification())) {
							proposingPartyAccount.setSecondaryIdentification(standingOrderConsentsRequest.getData()
									.getInitiation().getCreditorAccount().getSecondaryIdentification());
						}
						standingOrderInstructionProposal.setProposingPartyAccount(proposingPartyAccount);
					}
				}

				// Authorisation
				if (!NullCheckUtils.isNullOrEmpty(standingOrderConsentsRequest.getData().getAuthorisation())) {
					// AuthorisationType
					if (!NullCheckUtils.isNullOrEmpty(
							standingOrderConsentsRequest.getData().getAuthorisation().getAuthorisationType())) {
						standingOrderInstructionProposal
								.setAuthorisationType(AuthorisationType.fromValue(standingOrderConsentsRequest.getData()
										.getAuthorisation().getAuthorisationType().toString()));
					}

					// CompletionDateTime
					if (!NullCheckUtils.isNullOrEmpty(
							standingOrderConsentsRequest.getData().getAuthorisation().getCompletionDateTime())) {
						standingOrderInstructionProposal.setAuthorisationDatetime(
								standingOrderConsentsRequest.getData().getAuthorisation().getCompletionDateTime());
					}
				}
			}

			// Risk
			if (!NullCheckUtils.isNullOrEmpty(standingOrderConsentsRequest.getRisk())) {
				PaymentInstrumentRiskFactor paymentInstructionRiskFactorReference = new PaymentInstrumentRiskFactor();
				// PaymentContextCode
				if (!NullCheckUtils.isNullOrEmpty(standingOrderConsentsRequest.getRisk().getPaymentContextCode())) {
					paymentInstructionRiskFactorReference.setPaymentContextCode(
							standingOrderConsentsRequest.getRisk().getPaymentContextCode().toString());
				}

				// MerchantCategoryCode
				if (!NullCheckUtils.isNullOrEmpty(standingOrderConsentsRequest.getRisk().getMerchantCategoryCode())) {
					paymentInstructionRiskFactorReference
							.setMerchantCategoryCode(standingOrderConsentsRequest.getRisk().getMerchantCategoryCode());
				}

				// MerchantCustomerIdentification
				if (!NullCheckUtils
						.isNullOrEmpty(standingOrderConsentsRequest.getRisk().getMerchantCustomerIdentification())) {
					paymentInstructionRiskFactorReference.setMerchantCustomerIdentification(
							standingOrderConsentsRequest.getRisk().getMerchantCustomerIdentification());
				}

				// DeliveryAddress
				if (!NullCheckUtils.isNullOrEmpty(standingOrderConsentsRequest.getRisk().getDeliveryAddress())) {
					Address counterpartyAddress = new Address();
					// AddressLine
					if (!NullCheckUtils.isNullOrEmpty(
							standingOrderConsentsRequest.getRisk().getDeliveryAddress().getAddressLine())) {
						List<String> addline = standingOrderConsentsRequest.getRisk().getDeliveryAddress()
								.getAddressLine();
						if (addline.size() > 0) {
							counterpartyAddress.setFirstAddressLine(addline.get(0));
						}
						if (addline.size() > 1) {
							counterpartyAddress.setSecondAddressLine(addline.get(1));
						}
					}

					// StreetName
					if (!NullCheckUtils.isNullOrEmpty(
							standingOrderConsentsRequest.getRisk().getDeliveryAddress().getStreetName())) {
						counterpartyAddress.setGeoCodeBuildingName(
								standingOrderConsentsRequest.getRisk().getDeliveryAddress().getStreetName());
					}

					// BuildingNumber
					if (!NullCheckUtils.isNullOrEmpty(
							standingOrderConsentsRequest.getRisk().getDeliveryAddress().getBuildingNumber())) {
						counterpartyAddress.setGeoCodeBuildingNumber(
								standingOrderConsentsRequest.getRisk().getDeliveryAddress().getBuildingNumber());
					}

					// PostCode
					if (!NullCheckUtils
							.isNullOrEmpty(standingOrderConsentsRequest.getRisk().getDeliveryAddress().getPostCode())) {
						counterpartyAddress.setPostCodeNumber(
								standingOrderConsentsRequest.getRisk().getDeliveryAddress().getPostCode());
					}

					// TownName
					if (!NullCheckUtils
							.isNullOrEmpty(standingOrderConsentsRequest.getRisk().getDeliveryAddress().getTownName())) {
						counterpartyAddress.setThirdAddressLine(
								standingOrderConsentsRequest.getRisk().getDeliveryAddress().getTownName());
					}

					// CountySubDivision
					if (!NullCheckUtils.isNullOrEmpty(
							standingOrderConsentsRequest.getRisk().getDeliveryAddress().getCountrySubDivision())) {
						counterpartyAddress.setFourthAddressLine(
								standingOrderConsentsRequest.getRisk().getDeliveryAddress().getCountrySubDivision());
					}

					// Country
					if (!NullCheckUtils
							.isNullOrEmpty(standingOrderConsentsRequest.getRisk().getDeliveryAddress().getCountry())) {
						Country addressCountry = new Country();
						addressCountry.setIsoCountryAlphaTwoCode(
								standingOrderConsentsRequest.getRisk().getDeliveryAddress().getCountry());
						counterpartyAddress.setAddressCountry(addressCountry);
					}
					paymentInstructionRiskFactorReference.setCounterPartyAddress(counterpartyAddress);
				}
				standingOrderInstructionProposal
						.setPaymentInstructionRiskFactorReference(paymentInstructionRiskFactorReference);
			}
		}
		return standingOrderInstructionProposal;
	}

	public <T> CustomDStandingOrderConsentsPOSTResponse transformDomesticStandingOrdersResponseFromFDToAPIForInsert(
			T standingOrderInstructionProposalResponse) {
		CustomDStandingOrderConsentsPOSTResponse customDStandingOrderConsentsPOSTResponse = new CustomDStandingOrderConsentsPOSTResponse();
		StandingOrderInstructionProposal standingOrderInstructionProposal = (StandingOrderInstructionProposal) standingOrderInstructionProposalResponse;
		OBWriteDataDomesticStandingOrderConsentResponse1 obWriteStandingOrderConsentRes = new OBWriteDataDomesticStandingOrderConsentResponse1();
		populateFDtoAPIResponse(customDStandingOrderConsentsPOSTResponse, standingOrderInstructionProposal,
				obWriteStandingOrderConsentRes);
		if ((!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getPaymentInstructionProposalId()))
				&& (!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getProposalStatus()))) {

			if ((standingOrderInstructionProposal.getProposalStatus().toString())
					.equalsIgnoreCase(ProposalStatus.AWAITINGAUTHORISATION.toString())) {
				customDStandingOrderConsentsPOSTResponse.setConsentProcessStatus(ProcessConsentStatusEnum.PASS);
			}
		}
		if ((!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getPaymentInstructionProposalId()))
				&& (!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getProposalStatus()))) {

			if ((standingOrderInstructionProposal.getProposalStatus().toString())
					.equalsIgnoreCase(ProposalStatus.REJECTED.toString())) {
				customDStandingOrderConsentsPOSTResponse.setConsentProcessStatus(ProcessConsentStatusEnum.FAIL);
			}
		}

		return customDStandingOrderConsentsPOSTResponse;

	}

	public <T> CustomDStandingOrderConsentsPOSTResponse transformDomesticStandingOrderResponse(
			T standingOrderInstructionProposalResponse) {
		/* Root Element */
		CustomDStandingOrderConsentsPOSTResponse customDStandingOrderConsentsPOSTResponse = new CustomDStandingOrderConsentsPOSTResponse();
		StandingOrderInstructionProposal standingOrderInstructionProposal = (StandingOrderInstructionProposal) standingOrderInstructionProposalResponse;
		OBWriteDataDomesticStandingOrderConsentResponse1 obWriteStandingOrderConsentRes = new OBWriteDataDomesticStandingOrderConsentResponse1();

		populateFDtoAPIResponse(customDStandingOrderConsentsPOSTResponse, standingOrderInstructionProposal,
				obWriteStandingOrderConsentRes);
		return customDStandingOrderConsentsPOSTResponse;

	}

	/**
	 * @param customDStandingOrderConsentsPOSTResponse
	 * @param standingOrderInstructionProposal
	 * @param obWriteStandingOrderConsentRes
	 */
	private void populateFDtoAPIResponse(
			CustomDStandingOrderConsentsPOSTResponse customDStandingOrderConsentsPOSTResponse,
			StandingOrderInstructionProposal standingOrderInstructionProposal,
			OBWriteDataDomesticStandingOrderConsentResponse1 obWriteStandingOrderConsentRes) {
		if (!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal)) {

			// ConsentId
			if (!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getPaymentInstructionProposalId())) {
				obWriteStandingOrderConsentRes
						.setConsentId(standingOrderInstructionProposal.getPaymentInstructionProposalId());
			}

			// CreationDateTime
			if (!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getProposalCreationDatetime())) {
				obWriteStandingOrderConsentRes
						.setCreationDateTime(standingOrderInstructionProposal.getProposalCreationDatetime());
			}

			// Status
			if (!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getProposalStatus())) {
				obWriteStandingOrderConsentRes.setStatus(OBExternalConsentStatus1Code
						.fromValue(standingOrderInstructionProposal.getProposalStatus().toString()));
			}

			// StatusUpdateDateTime
			if (!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getProposalStatusUpdateDatetime())) {
				obWriteStandingOrderConsentRes
						.setStatusUpdateDateTime(standingOrderInstructionProposal.getProposalStatusUpdateDatetime());
			}

			// Permission
			if (!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getPermission())) {
				obWriteStandingOrderConsentRes.setPermission(
						OBExternalPermissions2Code.fromValue(standingOrderInstructionProposal.getPermission()));
			}

			OBDomesticStandingOrder1 obDomesticStandingOrder1 = new OBDomesticStandingOrder1();

			// Reference
			if (!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getReference())) {
				obDomesticStandingOrder1.setReference(standingOrderInstructionProposal.getReference());
			}

			// FirstPaymentDateTime
			if (!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getFirstPaymentDateTime())) {
				obDomesticStandingOrder1
						.setFirstPaymentDateTime(standingOrderInstructionProposal.getFirstPaymentDateTime());
			}

			// FirstPaymentAmount
			if (!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getFirstPaymentAmount())) {
				OBDomesticStandingOrder1FirstPaymentAmount firstPaymentAmount = new OBDomesticStandingOrder1FirstPaymentAmount();
				// Amount
				if (!NullCheckUtils.isNullOrEmpty(
						standingOrderInstructionProposal.getFirstPaymentAmount().getFinancialEventAmount())
						&& !NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getFirstPaymentAmount()
								.getFinancialEventAmount().getTransactionCurrency())) {
					firstPaymentAmount
							.setAmount(
									BigDecimal
											.valueOf(standingOrderInstructionProposal.getFirstPaymentAmount()
													.getFinancialEventAmount().getTransactionCurrency())
											.toPlainString());
				}

				// Currency
				if (!NullCheckUtils.isNullOrEmpty(
						standingOrderInstructionProposal.getFirstPaymentAmount().getTransactionCurrency())
						&& !NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getFirstPaymentAmount()
								.getTransactionCurrency().getIsoAlphaCode())) {
					firstPaymentAmount.setCurrency(standingOrderInstructionProposal.getFirstPaymentAmount()
							.getTransactionCurrency().getIsoAlphaCode());
				}
				obDomesticStandingOrder1.setFirstPaymentAmount(firstPaymentAmount);
			}

			// Frequency
			if (!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getFirstPaymentDateTime())
					&& !NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getFrequency())) {
				String paymentStartDate = standingOrderInstructionProposal.getFirstPaymentDateTime();
				try {
					SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
					Date firstPaymentDate = simpleDateFormat.parse(paymentStartDate);
					String frequencyProcessLayerToCma = frequencyUtil.processLayerToCma(
							standingOrderInstructionProposal.getFrequency().toString(), firstPaymentDate);
					obDomesticStandingOrder1.setFrequency(frequencyProcessLayerToCma);
				} catch (ParseException pe) {
					throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.BAD_REQUEST_PISP_FREQUENCY);
				}
			}

			// DebtorAccount
			if (!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getAuthorisingPartyAccount())) {
				OBCashAccountDebtor3 accountDebtor = new OBCashAccountDebtor3();

				// SchemeName
				if (!NullCheckUtils
						.isNullOrEmpty(standingOrderInstructionProposal.getAuthorisingPartyAccount().getSchemeName())) {
					accountDebtor.setSchemeName(
							standingOrderInstructionProposal.getAuthorisingPartyAccount().getSchemeName());
				}

				// Identification
				if (!NullCheckUtils.isNullOrEmpty(
						standingOrderInstructionProposal.getAuthorisingPartyAccount().getAccountIdentification())) {
					accountDebtor.setIdentification(
							standingOrderInstructionProposal.getAuthorisingPartyAccount().getAccountIdentification());
				}

				// Name
				if (!NullCheckUtils.isNullOrEmpty(
						standingOrderInstructionProposal.getAuthorisingPartyAccount().getAccountName())) {
					accountDebtor
							.setName(standingOrderInstructionProposal.getAuthorisingPartyAccount().getAccountName());
				}

				// SecondaryIdentification
				if (!NullCheckUtils.isNullOrEmpty(
						standingOrderInstructionProposal.getAuthorisingPartyAccount().getSecondaryIdentification())) {
					accountDebtor.setSecondaryIdentification(
							standingOrderInstructionProposal.getAuthorisingPartyAccount().getSecondaryIdentification());
				}
				obDomesticStandingOrder1.setDebtorAccount(accountDebtor);
			}

			// CreditorAccount
			if (!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getProposingPartyAccount())) {
				OBCashAccountCreditor2 accountCreditor = new OBCashAccountCreditor2();

				// SchemeName
				if (!NullCheckUtils
						.isNullOrEmpty(standingOrderInstructionProposal.getProposingPartyAccount().getSchemeName())) {
					accountCreditor
							.setSchemeName(standingOrderInstructionProposal.getProposingPartyAccount().getSchemeName());
				}

				// Identification
				if (!NullCheckUtils.isNullOrEmpty(
						standingOrderInstructionProposal.getProposingPartyAccount().getAccountIdentification())) {
					accountCreditor.setIdentification(
							standingOrderInstructionProposal.getProposingPartyAccount().getAccountIdentification());
				}

				// Name
				if (!NullCheckUtils
						.isNullOrEmpty(standingOrderInstructionProposal.getProposingPartyAccount().getAccountName())) {
					accountCreditor
							.setName(standingOrderInstructionProposal.getProposingPartyAccount().getAccountName());
				}

				// SecondaryIdentification
				if (!NullCheckUtils.isNullOrEmpty(
						standingOrderInstructionProposal.getProposingPartyAccount().getSecondaryIdentification())) {
					accountCreditor.setSecondaryIdentification(
							standingOrderInstructionProposal.getProposingPartyAccount().getSecondaryIdentification());
				}
				obDomesticStandingOrder1.setCreditorAccount(accountCreditor);
			}

			// Authorisation
			OBAuthorisation1 authorisation = null;
			if (!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getAuthorisationType())) {
				authorisation = new OBAuthorisation1();
				// AuthorisationType
				authorisation.setAuthorisationType(OBExternalAuthorisation1Code
						.fromValue(standingOrderInstructionProposal.getAuthorisationType().toString()));

				// CompletionDateTime
				if (!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal.getAuthorisationDatetime())) {
					authorisation.setCompletionDateTime(standingOrderInstructionProposal.getAuthorisationDatetime());
				}
				obWriteStandingOrderConsentRes.setAuthorisation(authorisation);
			}

			// Risk
			OBRisk1 risk = new OBRisk1();
			if (!NullCheckUtils
					.isNullOrEmpty(standingOrderInstructionProposal.getPaymentInstructionRiskFactorReference())) {
				// PaymentContextCode
				if (!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal
						.getPaymentInstructionRiskFactorReference().getPaymentContextCode())) {
					risk.setPaymentContextCode(OBExternalPaymentContext1Code.fromValue(standingOrderInstructionProposal
							.getPaymentInstructionRiskFactorReference().getPaymentContextCode()));
				}

				// MerchantCategoryCode
				if (!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal
						.getPaymentInstructionRiskFactorReference().getMerchantCategoryCode())) {
					risk.setMerchantCategoryCode(standingOrderInstructionProposal
							.getPaymentInstructionRiskFactorReference().getMerchantCategoryCode());
				}

				// MerchantCustomerIdentification
				if (!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal
						.getPaymentInstructionRiskFactorReference().getMerchantCustomerIdentification())) {
					risk.setMerchantCustomerIdentification(standingOrderInstructionProposal
							.getPaymentInstructionRiskFactorReference().getMerchantCustomerIdentification());
				}

				// DeliveryAddress
				OBRisk1DeliveryAddress deliveryAddress = new OBRisk1DeliveryAddress();
				if (!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal
						.getPaymentInstructionRiskFactorReference().getCounterPartyAddress())) {
					// AddressLine
					if (!NullCheckUtils
							.isNullOrEmpty(standingOrderInstructionProposal.getPaymentInstructionRiskFactorReference()
									.getCounterPartyAddress().getFirstAddressLine())) {
						List<String> addressList = new ArrayList<>();
						if (!NullCheckUtils.isNullOrEmpty(
								standingOrderInstructionProposal.getPaymentInstructionRiskFactorReference()
										.getCounterPartyAddress().getSecondAddressLine())) {
							addressList.add(standingOrderInstructionProposal.getPaymentInstructionRiskFactorReference()
									.getCounterPartyAddress().getFirstAddressLine());
							addressList.add(standingOrderInstructionProposal.getPaymentInstructionRiskFactorReference()
									.getCounterPartyAddress().getSecondAddressLine());
						} else {
							addressList.add(standingOrderInstructionProposal.getPaymentInstructionRiskFactorReference()
									.getCounterPartyAddress().getFirstAddressLine());
						}
						deliveryAddress.setAddressLine(addressList);
					}

					// StreetName
					if (!NullCheckUtils
							.isNullOrEmpty(standingOrderInstructionProposal.getPaymentInstructionRiskFactorReference()
									.getCounterPartyAddress().getGeoCodeBuildingName())) {
						deliveryAddress.setStreetName(
								standingOrderInstructionProposal.getPaymentInstructionRiskFactorReference()
										.getCounterPartyAddress().getGeoCodeBuildingName());
					}

					// BuildingNumber
					if (!NullCheckUtils
							.isNullOrEmpty(standingOrderInstructionProposal.getPaymentInstructionRiskFactorReference()
									.getCounterPartyAddress().getGeoCodeBuildingNumber())) {
						deliveryAddress.setBuildingNumber(
								standingOrderInstructionProposal.getPaymentInstructionRiskFactorReference()
										.getCounterPartyAddress().getGeoCodeBuildingNumber());
					}

					// PostCode
					if (!NullCheckUtils.isNullOrEmpty(standingOrderInstructionProposal
							.getPaymentInstructionRiskFactorReference().getCounterPartyAddress().getPostCodeNumber())) {
						deliveryAddress
								.setPostCode(standingOrderInstructionProposal.getPaymentInstructionRiskFactorReference()
										.getCounterPartyAddress().getPostCodeNumber());
					}

					// TownName
					if (!NullCheckUtils
							.isNullOrEmpty(standingOrderInstructionProposal.getPaymentInstructionRiskFactorReference()
									.getCounterPartyAddress().getThirdAddressLine())) {
						deliveryAddress
								.setTownName(standingOrderInstructionProposal.getPaymentInstructionRiskFactorReference()
										.getCounterPartyAddress().getThirdAddressLine());
					}

					// CountySubDivision
					if (!NullCheckUtils
							.isNullOrEmpty(standingOrderInstructionProposal.getPaymentInstructionRiskFactorReference()
									.getCounterPartyAddress().getFourthAddressLine())) {
						String addressLine = null;
						addressLine = standingOrderInstructionProposal.getPaymentInstructionRiskFactorReference()
								.getCounterPartyAddress().getFourthAddressLine();
						deliveryAddress.setCountrySubDivision(addressLine);
					}

					// Country
					if (!NullCheckUtils
							.isNullOrEmpty(standingOrderInstructionProposal.getPaymentInstructionRiskFactorReference()
									.getCounterPartyAddress().getAddressCountry().getIsoCountryAlphaTwoCode())) {
						deliveryAddress
								.setCountry(standingOrderInstructionProposal.getPaymentInstructionRiskFactorReference()
										.getCounterPartyAddress().getAddressCountry().getIsoCountryAlphaTwoCode());
					}
					risk.setDeliveryAddress(deliveryAddress);
				}
			}

			obWriteStandingOrderConsentRes.setInitiation(obDomesticStandingOrder1);
			customDStandingOrderConsentsPOSTResponse.setData(obWriteStandingOrderConsentRes);
			customDStandingOrderConsentsPOSTResponse.setRisk(risk);
		}
	}

}
