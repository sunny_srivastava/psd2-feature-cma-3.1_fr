package com.capgemini.psd2.foundationservice.domestic.standing.orders.consents.boi.adapter.test;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;

import java.lang.reflect.Method;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import com.capgemini.psd2.foundationservice.domestic.standing.orders.consents.boi.adapter.DomesticStandingOrdersConsentsFoundationServiceAdapterApplication;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.consents.boi.adapter.client.DomesticStandingOrdersConsentsFoundationServiceClient;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.consents.boi.adapter.delegate.DomesticStandingOrdersConsentsFoundationServiceDelegate;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.consents.boi.adapter.raml.domain.StandingOrderInstructionProposal;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.consents.boi.adapter.transformer.DomesticStandingOrdersConsentsFoundationServiceTransformer;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.impl.RestClientSyncImpl;

@RunWith(SpringRunner.class)
public class DomesticStandingOrdersConsentsFoundationServiceClientTest {
	
	@InjectMocks
	DomesticStandingOrdersConsentsFoundationServiceClient client;
	
	@Mock
	DomesticStandingOrdersConsentsFoundationServiceAdapterApplication adapter;
	
	@Mock
	DomesticStandingOrdersConsentsFoundationServiceDelegate delegate;
	
	@Mock
	DomesticStandingOrdersConsentsFoundationServiceTransformer transformer;
	
	@Mock
	private RestClientSyncImpl restClient;

	@Mock
	private RestTemplate restTemplate;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		Method method1 = DomesticStandingOrdersConsentsFoundationServiceClient.class.getDeclaredMethod("init", null);
		method1.setAccessible(true);
		method1.invoke(client);
		assertNotNull(method1);

	}
	
	
	

	@Test
	public void contextLoads() {
	}
	
	@Test
	public void testStandingOrdersConsentFoundationServiceClientCreate(){
		RequestInfo reqInfo= new RequestInfo();
		HttpHeaders httpHeaders= new HttpHeaders();
		StandingOrderInstructionProposal standingInstructionProposalRequest1 = new StandingOrderInstructionProposal();
		Mockito.when(restClient.callForPost(any(),any(),any(),any())).thenReturn(new StandingOrderInstructionProposal());
		
		client.restTransportForDomesticStandingOrdersServicePost(reqInfo,standingInstructionProposalRequest1, StandingOrderInstructionProposal.class, httpHeaders);
	}
	
	@Test
	public void testStandingOrdersConsentFoundationServiceClientRetrive(){
		RequestInfo reqInfo= new RequestInfo();
		HttpHeaders headers= new HttpHeaders();
		Mockito.when(restClient.callForGet(any(),any(),any())).thenReturn(new StandingOrderInstructionProposal());
		
		client.restTransportForDomesticStandingOrderServiceGet(reqInfo, StandingOrderInstructionProposal.class, headers);
	}
	
	

}
