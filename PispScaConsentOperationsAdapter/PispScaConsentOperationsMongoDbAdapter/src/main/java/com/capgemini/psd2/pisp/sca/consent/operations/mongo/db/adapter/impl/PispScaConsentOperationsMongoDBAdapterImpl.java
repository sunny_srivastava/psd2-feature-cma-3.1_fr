package com.capgemini.psd2.pisp.sca.consent.operations.mongo.db.adapter.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Conditional;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.aspect.AdapterJSONUtilities;
import com.capgemini.psd2.conditions.MongoDbMockCondition;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.pisp.adapter.FilePaymentConsentsAdapter;
import com.capgemini.psd2.pisp.domain.CustomDPaymentConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.CustomDSPConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.CustomDStandingOrderConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.CustomFilePaymentConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.CustomIPaymentConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.CustomISPConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.OBCashAccountDebtor3;
import com.capgemini.psd2.pisp.domain.OBTransactionIndividualStatus1Code;
import com.capgemini.psd2.pisp.domain.PaymentConsentsValidationResponse;
import com.capgemini.psd2.pisp.enums.PaymentTypeEnum;
import com.capgemini.psd2.pisp.operations.transformer.PaymentStageDataTransformer;
import com.capgemini.psd2.pisp.sca.consent.operations.adapter.PispScaConsentOperationsAdapter;
import com.capgemini.psd2.pisp.stage.domain.AmountDetails;
import com.capgemini.psd2.pisp.stage.domain.CustomConsentAppViewData;
import com.capgemini.psd2.pisp.stage.domain.CustomFraudSystemPaymentData;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageUpdateData;
import com.capgemini.psd2.pisp.stage.domain.CustomPreAuthorizeAdditionalInfo;
import com.capgemini.psd2.pisp.stage.operations.adapter.DomesticPaymentStagingAdapter;
import com.capgemini.psd2.pisp.stage.operations.adapter.DomesticScheduledPaymentStagingAdapter;
import com.capgemini.psd2.pisp.stage.operations.adapter.DomesticStandingOrdersPaymentStagingAdapter;
import com.capgemini.psd2.pisp.stage.operations.adapter.InternationalPaymentStagingAdapter;
import com.capgemini.psd2.pisp.stage.operations.adapter.InternationalScheduledPaymentStagingAdapter;
import com.capgemini.psd2.pisp.status.PaymentConstants;



@Conditional(MongoDbMockCondition.class)
@ConfigurationProperties ( prefix = "app")
@Component("pispStageMongoDBAdapter")
@DependsOn({ "dPaymentConsentsStagingMongoDbAdapter", "dPaymentStageDataTransformer",
	"iPaymentConsentsStagingMongoDbAdapter", "iPaymentStageDataTransformer",
	"dStandingOrderConsentsStagingMongoDbAdapter", "dStandingOrderStageDataTransformer",
	"dScheduledPaymentConsentsStagingMongoDbAdapter", "dsPaymentStageDataTransformer",
"isPaymentStageDataTransformer" }) // list
// of // beans
public class PispScaConsentOperationsMongoDBAdapterImpl implements PispScaConsentOperationsAdapter {

	private static final Logger LOG = LoggerFactory.getLogger(PispScaConsentOperationsMongoDBAdapterImpl.class);
	
	@Autowired
	@Qualifier("dPaymentConsentsStagingMongoDbAdapter")
	private DomesticPaymentStagingAdapter dPaymentStagingAdapter;

	@Autowired
	@Qualifier("iPaymentConsentsStagingMongoDbAdapter")
	private InternationalPaymentStagingAdapter iPaymentStagingAdapter;

	@Autowired
	@Qualifier("dStandingOrderConsentsStagingMongoDbAdapter")
	private DomesticStandingOrdersPaymentStagingAdapter dsoStagingAdapter;

	@Autowired
	@Qualifier("dScheduledPaymentConsentsStagingMongoDbAdapter")
	private DomesticScheduledPaymentStagingAdapter dsStagingAdapter;

	@Autowired
	@Qualifier("iScheduledPaymentConsentsStagingMongoDbAdapter")
	private InternationalScheduledPaymentStagingAdapter isStagingAdapter;

	@Autowired
	@Qualifier("filePaymentConsentsStagingMongoDbAdapter")
	private FilePaymentConsentsAdapter fileStagingAdapter;

	@Autowired
	@Qualifier("dPaymentStageDataTransformer")
	private PaymentStageDataTransformer<CustomDPaymentConsentsPOSTResponse> dPaymentStageDataTransformer;

	@Autowired
	@Qualifier("iPaymentStageDataTransformer")
	private PaymentStageDataTransformer<CustomIPaymentConsentsPOSTResponse> iPaymentStageDataTransformer;

	@Autowired
	@Qualifier("dStandingOrderStageDataTransformer")
	private PaymentStageDataTransformer<CustomDStandingOrderConsentsPOSTResponse> dsoPaymentStageDataTransformer;

	@Autowired
	@Qualifier("dsPaymentStageDataTransformer")
	private PaymentStageDataTransformer<CustomDSPConsentsPOSTResponse> dsPaymentStageDataTransformer;

	@Autowired
	@Qualifier("isPaymentStageDataTransformer")
	private PaymentStageDataTransformer<CustomISPConsentsPOSTResponse> isPaymentStageDataTransformer;

	@Autowired
	@Qualifier("filePaymentStageDataTransformer")
	private PaymentStageDataTransformer<CustomFilePaymentConsentsPOSTResponse> filePaymentStageDataTransformer;

	
	private Map<String, String> baseCurrency;

	public Map<String, String> getBaseCurrency() {
		return baseCurrency;
	}

	public void setBaseCurrency(Map<String, String> baseCurrency) {
		this.baseCurrency = baseCurrency;
	}
	
	private List<PaymentTypeEnum> postAuthorizingSupportedPaymentTypes = new ArrayList<PaymentTypeEnum>(
			Arrays.asList(PaymentTypeEnum.DOMESTIC_ST_ORD, PaymentTypeEnum.INTERNATIONAL_PAY));

	@Override
	public CustomConsentAppViewData retrieveConsentAppStagedViewData(CustomPaymentStageIdentifiers stageIdentifiers,
			Map<String, String> params) {

		CustomConsentAppViewData customConsentAppSetupData = null;
		if (stageIdentifiers.getPaymentTypeEnum() == PaymentTypeEnum.DOMESTIC_PAY) {
			CustomDPaymentConsentsPOSTResponse domesticStageResponse = dPaymentStagingAdapter
					.retrieveStagedDomesticPaymentConsents(stageIdentifiers, params);
			customConsentAppSetupData = dPaymentStageDataTransformer
					.transformDStageToConsentAppViewData(domesticStageResponse);
		} else if (stageIdentifiers.getPaymentTypeEnum() == PaymentTypeEnum.INTERNATIONAL_PAY) {
			CustomIPaymentConsentsPOSTResponse domesticStageResponse = iPaymentStagingAdapter
					.retrieveStagedInternationalPaymentConsents(stageIdentifiers, params);
			customConsentAppSetupData = iPaymentStageDataTransformer
					.transformDStageToConsentAppViewData(domesticStageResponse);
		} else if (stageIdentifiers.getPaymentTypeEnum() == PaymentTypeEnum.DOMESTIC_ST_ORD) {
			CustomDStandingOrderConsentsPOSTResponse domesticStandingOrderStageResponse = dsoStagingAdapter
					.retrieveStagedDomesticStandingOrdersConsents(stageIdentifiers, params);
			customConsentAppSetupData = dsoPaymentStageDataTransformer
					.transformDStageToConsentAppViewData(domesticStandingOrderStageResponse);
		} else if (stageIdentifiers.getPaymentTypeEnum() == PaymentTypeEnum.DOMESTIC_SCH_PAY) {
			CustomDSPConsentsPOSTResponse domesticStageResponse = dsStagingAdapter
					.retrieveStagedDomesticScheduledPaymentConsents(stageIdentifiers, params);
			customConsentAppSetupData = dsPaymentStageDataTransformer
					.transformDStageToConsentAppViewData(domesticStageResponse);
		} else if (stageIdentifiers.getPaymentTypeEnum() == PaymentTypeEnum.INTERNATIONAL_SCH_PAY) {
			CustomISPConsentsPOSTResponse internationalStageResponse = isStagingAdapter
					.retrieveStagedInternationalScheduledPaymentConsents(stageIdentifiers, params);
			customConsentAppSetupData = isPaymentStageDataTransformer
					.transformDStageToConsentAppViewData(internationalStageResponse);
		} else if (stageIdentifiers.getPaymentTypeEnum() == PaymentTypeEnum.FILE_PAY) {
			CustomFilePaymentConsentsPOSTResponse fileStageResponse = fileStagingAdapter
					.fetchFilePaymentMetadata(stageIdentifiers, params);
			customConsentAppSetupData = filePaymentStageDataTransformer
					.transformDStageToConsentAppViewData(fileStageResponse);
		}
		return customConsentAppSetupData;
	}



	@Override
	public CustomFraudSystemPaymentData retrieveFraudSystemPaymentStagedData(
			CustomPaymentStageIdentifiers stageIdentifiers, Map<String, String> params) {

		CustomFraudSystemPaymentData customFraudSystemPaymentData = null;
		if (stageIdentifiers.getPaymentTypeEnum() == (PaymentTypeEnum.DOMESTIC_PAY)) {
			CustomDPaymentConsentsPOSTResponse domesticStageResponse = dPaymentStagingAdapter
					.retrieveStagedDomesticPaymentConsents(stageIdentifiers, params);
			customFraudSystemPaymentData = dPaymentStageDataTransformer
					.transformDStageToFraudData(domesticStageResponse);
		} else if (stageIdentifiers.getPaymentTypeEnum() == (PaymentTypeEnum.INTERNATIONAL_PAY)) {
			CustomIPaymentConsentsPOSTResponse internationalStageResponse = iPaymentStagingAdapter
					.retrieveStagedInternationalPaymentConsents(stageIdentifiers, params);
			customFraudSystemPaymentData = iPaymentStageDataTransformer
					.transformDStageToFraudData(internationalStageResponse);
		} else if (stageIdentifiers.getPaymentTypeEnum() == (PaymentTypeEnum.DOMESTIC_ST_ORD)) {
			CustomDStandingOrderConsentsPOSTResponse domesticStandingOrderStageResponse = dsoStagingAdapter
					.retrieveStagedDomesticStandingOrdersConsents(stageIdentifiers, params);
			customFraudSystemPaymentData = dsoPaymentStageDataTransformer
					.transformDStageToFraudData(domesticStandingOrderStageResponse);
		} else if (stageIdentifiers.getPaymentTypeEnum() == PaymentTypeEnum.DOMESTIC_SCH_PAY) {
			CustomDSPConsentsPOSTResponse domesticStageResponse = dsStagingAdapter
					.retrieveStagedDomesticScheduledPaymentConsents(stageIdentifiers, params);
			customFraudSystemPaymentData = dsPaymentStageDataTransformer
					.transformDStageToFraudData(domesticStageResponse);
		} else if (stageIdentifiers.getPaymentTypeEnum() == PaymentTypeEnum.FILE_PAY) {
			CustomFilePaymentConsentsPOSTResponse fileStageResponse = fileStagingAdapter
					.fetchFilePaymentMetadata(stageIdentifiers, params);
			customFraudSystemPaymentData = filePaymentStageDataTransformer
					.transformDStageToFraudData(fileStageResponse);
		} else if (stageIdentifiers.getPaymentTypeEnum() == PaymentTypeEnum.INTERNATIONAL_SCH_PAY) {
			CustomISPConsentsPOSTResponse internationalStageResponse = isStagingAdapter
					.retrieveStagedInternationalScheduledPaymentConsents(stageIdentifiers, params);
			customFraudSystemPaymentData = isPaymentStageDataTransformer
					.transformDStageToFraudData(internationalStageResponse);
		}

		return customFraudSystemPaymentData;
	}

	@Override
	public PaymentConsentsValidationResponse validatePreAuthorisation(OBCashAccountDebtor3 selectedDebtorDetails,
			CustomPreAuthorizeAdditionalInfo preAuthAdditionalInfo, CustomPaymentStageIdentifiers stageIdentifiers,
			Map<String, String> params) {
		PaymentConsentsValidationResponse response = new PaymentConsentsValidationResponse();
		String currency = null;
		if (stageIdentifiers.getPaymentTypeEnum() == (PaymentTypeEnum.DOMESTIC_PAY)) {
			CustomDPaymentConsentsPOSTResponse domesticStageResponse = dPaymentStagingAdapter
					.retrieveStagedDomesticPaymentConsents(stageIdentifiers, params);
			currency = domesticStageResponse.getData().getInitiation().getInstructedAmount().getCurrency();
		} else if (stageIdentifiers.getPaymentTypeEnum() == (PaymentTypeEnum.INTERNATIONAL_PAY)) {
			CustomIPaymentConsentsPOSTResponse internationalStageResponse = iPaymentStagingAdapter
					.retrieveStagedInternationalPaymentConsents(stageIdentifiers, params);
			currency = internationalStageResponse.getData().getInitiation().getInstructedAmount().getCurrency();
		} else if (stageIdentifiers.getPaymentTypeEnum() == PaymentTypeEnum.DOMESTIC_ST_ORD) {
			CustomDStandingOrderConsentsPOSTResponse domesticStandingOrderStageResponse = dsoStagingAdapter
					.retrieveStagedDomesticStandingOrdersConsents(stageIdentifiers, params);
			currency = domesticStandingOrderStageResponse.getData().getInitiation().getFirstPaymentAmount()
					.getCurrency();
		} else if (stageIdentifiers.getPaymentTypeEnum() == PaymentTypeEnum.DOMESTIC_SCH_PAY) {
			CustomDSPConsentsPOSTResponse dspStageResponse = dsStagingAdapter
					.retrieveStagedDomesticScheduledPaymentConsents(stageIdentifiers, params);
			currency = dspStageResponse.getData().getInitiation().getInstructedAmount().getCurrency();

		} else if (stageIdentifiers.getPaymentTypeEnum() == PaymentTypeEnum.FILE_PAY) {
			//changes once we get from FS
			currency = "GBP";
		} else if (stageIdentifiers.getPaymentTypeEnum() == PaymentTypeEnum.INTERNATIONAL_SCH_PAY) {
			CustomISPConsentsPOSTResponse ispStageResponse = isStagingAdapter
					.retrieveStagedInternationalScheduledPaymentConsents(stageIdentifiers, params);
			currency = ispStageResponse.getData().getInitiation().getInstructedAmount().getCurrency();
		}
		if (currency.equals("INR")) {
			response.setPaymentSetupValidationStatus(OBTransactionIndividualStatus1Code.REJECTED);
		} else {
			response.setPaymentSetupValidationStatus(OBTransactionIndividualStatus1Code.PENDING);
		}
		return response;
	}

	@Override
	public void updatePaymentStageData(CustomPaymentStageIdentifiers stageIdentifiers,
			CustomPaymentStageUpdateData updateData, Map<String, String> params) {
		if (stageIdentifiers.getPaymentTypeEnum() == PaymentTypeEnum.DOMESTIC_PAY) {
			if (stageIdentifiers.getPaymentSetupVersion().equals(PaymentConstants.CMA_FIRST_VERSION)) {

				/*
				 * Fix Scheme name validation failed. In MongoDB producing
				 * exception: can't have. In field names [UK.OBIE.IBAN] while
				 * creating test data. Hence, in mock data, we used "_" instead
				 * of "." character Replacing "_" to "." to convert valid Scheme
				 * Name into mongo db adapter only. there is no impact on CMA
				 * api flow. Its specifc to Sandbox functionality
				 */
				if (updateData.getDebtorDetails() != null && updateData.getDebtorDetails().getSchemeName() != null) {
					String debtorSchemeName = updateData.getDebtorDetails().getSchemeName().toUpperCase();
					updateData.getDebtorDetails().setSchemeName(debtorSchemeName.replace('_', '.'));
				}
				dPaymentStageDataTransformer.updateStagedPaymentConsents(stageIdentifiers, updateData, params);
			} else if (stageIdentifiers.getPaymentTypeEnum() == PaymentTypeEnum.INTERNATIONAL_PAY) {
				iPaymentStageDataTransformer.updateStagedPaymentConsents(stageIdentifiers, updateData, params);
			} else if (stageIdentifiers.getPaymentTypeEnum() == PaymentTypeEnum.DOMESTIC_ST_ORD) {
				dsoPaymentStageDataTransformer.updateStagedPaymentConsents(stageIdentifiers, updateData, params);
			} else if (stageIdentifiers.getPaymentTypeEnum() == PaymentTypeEnum.DOMESTIC_SCH_PAY) {
				dsPaymentStageDataTransformer.updateStagedPaymentConsents(stageIdentifiers, updateData, params);
			} else if (stageIdentifiers.getPaymentTypeEnum() == PaymentTypeEnum.INTERNATIONAL_SCH_PAY) {
				isPaymentStageDataTransformer.updateStagedPaymentConsents(stageIdentifiers, updateData, params);
			} else if (stageIdentifiers.getPaymentTypeEnum() == PaymentTypeEnum.FILE_PAY) {
				filePaymentStageDataTransformer.updateStagedPaymentConsents(stageIdentifiers, updateData, params);
			}
		}
	}

	@Override
	public CustomConsentAppViewData retrieveConsentAppDebtorDetails(CustomPaymentStageIdentifiers stageIdentifiers,
			CustomPaymentStageUpdateData updateData, Map<String, String> paramsMap) {
		CustomConsentAppViewData customConsentAppSetupData = null;
		LOG.info("Entering retrieveConsentAppDebtorDetails()");
		if(!postAuthorizingSupportedPaymentTypes.contains(stageIdentifiers.getPaymentTypeEnum())){
			LOG.info("postAuthorizingSupportedPaymentTypes not found");
			return null;
		}
		
		if (PaymentTypeEnum.DOMESTIC_ST_ORD == stageIdentifiers.getPaymentTypeEnum()) {
			LOG.info("Inside Domestic Standing Order Flow()");
			CustomDStandingOrderConsentsPOSTResponse domesticStandingOrderStageResponse = dsoStagingAdapter
					.retrieveStagedDomesticStandingOrdersConsents(stageIdentifiers, paramsMap);
			LOG.info("domesticStandingOrderStageResponse fetched");
			customConsentAppSetupData = dsoPaymentStageDataTransformer
					.transformDStageToConsentAppViewData(domesticStandingOrderStageResponse);
		} else if (PaymentTypeEnum.INTERNATIONAL_PAY == stageIdentifiers.getPaymentTypeEnum()) {
			LOG.info("Inside International Flow()");
			CustomIPaymentConsentsPOSTResponse domesticStageResponse = iPaymentStagingAdapter
					.retrieveStagedInternationalPaymentConsents(stageIdentifiers, paramsMap);
			LOG.info("domesticStageResponse fetched");
			customConsentAppSetupData = iPaymentStageDataTransformer
					.transformDStageToConsentAppViewData(domesticStageResponse);
		} 		
		LOG.info("Exiting retrieveConsentAppDebtorDetails()");
		return commonTransformation(customConsentAppSetupData, paramsMap);
	}
	private CustomConsentAppViewData commonTransformation(CustomConsentAppViewData customConsentAppSetupData, Map<String, String> paramsMap){
		
		LOG.debug("{\"method\":\"{}\",\"customConsentAppSetupData\":\"{}\",\"paramsMap\":\"{}\",\"tenantId\":\"{}\"}",
				"com.capgemini.psd2.pisp.sca.consent.operations.mongo.db.adapter.impl.PispScaConsentOperationsMongoDBAdapterImpl.commonTransformation()",
				AdapterJSONUtilities.getJSONOutPutFromObject(customConsentAppSetupData), AdapterJSONUtilities.getJSONOutPutFromObject(paramsMap), PSD2Constants.TENANT_ID);
		
		String tenantId = paramsMap.get(PSD2Constants.TENANT_ID);
		String currency =PSD2Constants.CURRENCY_EUR;
		if (!baseCurrency.isEmpty() && tenantId != null && baseCurrency.containsKey(tenantId)) {
			currency = baseCurrency.get(tenantId);
		}
		
		LOG.debug("{\"method\":\"{}\",\"baseCurrency\":\"{}\",\"currency\":\"{}\",\"tenantId\":\"{}\"}",
				"com.capgemini.psd2.pisp.sca.consent.operations.mongo.db.adapter.impl.PispScaConsentOperationsMongoDBAdapterImpl.commonTransformation()",
				AdapterJSONUtilities.getJSONOutPutFromObject(baseCurrency), currency, tenantId);
		
		if((null != customConsentAppSetupData.getExchangeRateDetails()) && currency.equalsIgnoreCase(customConsentAppSetupData.getAmountDetails().getCurrency())){
			customConsentAppSetupData.getExchangeRateDetails().setExchangeRate(new BigDecimal(1));
		}
		
		BigDecimal amount = new BigDecimal(customConsentAppSetupData.getAmountDetails().getAmount());
		AmountDetails baseAmountDetails = new AmountDetails().amount(customConsentAppSetupData.getAmountDetails().getAmount()).currency(currency);
		
		if(null != customConsentAppSetupData.getExchangeRateDetails() && null != customConsentAppSetupData.getExchangeRateDetails().getExchangeRate()){
			amount = amount.multiply(customConsentAppSetupData.getExchangeRateDetails().getExchangeRate());
			baseAmountDetails.setAmount(amount.toString());	
		}
		customConsentAppSetupData.setBaseAmountDetails(baseAmountDetails);
		
		BigDecimal totalAmount = new BigDecimal(amount.toString());
		if(null != customConsentAppSetupData.getChargeDetails() && null != customConsentAppSetupData.getChargeDetails().getChargesLis()){
			for (com.capgemini.psd2.pisp.domain.OBCharge1 charge : customConsentAppSetupData.getChargeDetails().getChargesLis()) {
				totalAmount = totalAmount.add(new BigDecimal(charge.getAmount().getAmount()));
			}
		}
		
		customConsentAppSetupData.setTotalAmountDetails(new AmountDetails().amount(totalAmount.toString()).currency(currency));
		LOG.debug("{\"method\":\"{}\",\"customConsentAppSetupData\":\"{}\"}",
				"com.capgemini.psd2.pisp.sca.consent.operations.mongo.db.adapter.impl.PispScaConsentOperationsMongoDBAdapterImpl.commonTransformation()",
				AdapterJSONUtilities.getJSONOutPutFromObject(customConsentAppSetupData));
		return customConsentAppSetupData;

	}
}
