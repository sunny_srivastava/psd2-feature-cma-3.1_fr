package com.capgemini.psd2.pisp.operations.transformer.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Conditional;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.conditions.MongoDbMockCondition;
import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.pisp.domain.CustomDPaymentConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.OBCashAccountDebtor3;
import com.capgemini.psd2.pisp.domain.OBExternalConsentStatus1Code;
import com.capgemini.psd2.pisp.enums.PaymentTypeEnum;
import com.capgemini.psd2.pisp.operations.transformer.PaymentStageDataTransformer;
import com.capgemini.psd2.pisp.payment.setup.mongo.db.adapter.repository.DPaymentConsentsFoundationRepository;
import com.capgemini.psd2.pisp.payment.setup.mongo.db.cma1.adapter.repository.DPaymentV1SetupFoundationRepository;
import com.capgemini.psd2.pisp.payment.setup.mongo.db.cma1.domain.CustomDomesticPaymentSetupCma1POSTResponse;
import com.capgemini.psd2.pisp.stage.domain.AmountDetails;
import com.capgemini.psd2.pisp.stage.domain.ChargeDetails;
import com.capgemini.psd2.pisp.stage.domain.CreditorDetails;
import com.capgemini.psd2.pisp.stage.domain.CustomConsentAppViewData;
import com.capgemini.psd2.pisp.stage.domain.CustomDebtorDetails;
import com.capgemini.psd2.pisp.stage.domain.CustomFraudSystemPaymentData;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageUpdateData;
import com.capgemini.psd2.pisp.stage.domain.ExchangeRateDetails;
import com.capgemini.psd2.pisp.stage.domain.RemittanceDetails;
import com.capgemini.psd2.pisp.status.PaymentConstants;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Conditional(MongoDbMockCondition.class)
@Component("dPaymentStageDataTransformer")
public class DPaymentStageDataTransformerImpl
		implements PaymentStageDataTransformer<CustomDPaymentConsentsPOSTResponse> {

	@Autowired
	private DPaymentV1SetupFoundationRepository dPaymentSetupBankRepositoryCma1;

	@Autowired
	private DPaymentConsentsFoundationRepository dPaymentConsentsBankRepository;

	@Override
	public CustomConsentAppViewData transformDStageToConsentAppViewData(
			CustomDPaymentConsentsPOSTResponse dPaymentStageResponse) {
		CustomConsentAppViewData response = new CustomConsentAppViewData();

		/* Payment Type needed on consent page */
		response.setPaymentType(PaymentTypeEnum.DOMESTIC_PAY.getPaymentType());

		/* Amount details needed on consent page */
		AmountDetails amountDetails = new AmountDetails();
		if (!NullCheckUtils.isNullOrEmpty(dPaymentStageResponse.getData().getInitiation().getInstructedAmount())) {
			amountDetails.setAmount(dPaymentStageResponse.getData().getInitiation().getInstructedAmount().getAmount());
			amountDetails
					.setCurrency(dPaymentStageResponse.getData().getInitiation().getInstructedAmount().getCurrency());
		}

		/* Debtor details needed on consent page */
		CustomDebtorDetails debtorDetails = null;
		if (!NullCheckUtils.isNullOrEmpty(dPaymentStageResponse.getData().getInitiation().getDebtorAccount())) {
			debtorDetails = new CustomDebtorDetails();
			debtorDetails.setIdentification(
					dPaymentStageResponse.getData().getInitiation().getDebtorAccount().getIdentification());
			debtorDetails.setName(dPaymentStageResponse.getData().getInitiation().getDebtorAccount().getName());
			String stagedDebtorSchemeName = dPaymentStageResponse.getData().getInitiation().getDebtorAccount()
					.getSchemeName();

			// Fix Scheme name validation failed. In MongoDB producing
			// exception: can't have. In field names [UK.OBIE.IBAN] while
			// creating test data. Hence, in mock data, we used "_" instead of
			// "." character
			// Replacing "_" to "." to convert valid Scheme Name into mongo db
			// adapter only. there is no impact on CMA api flow. Its specifc to
			// Sandbox functionality
			debtorDetails.setSchemeName(stagedDebtorSchemeName.replace("_", "."));
			debtorDetails.setSecondaryIdentification(
					dPaymentStageResponse.getData().getInitiation().getDebtorAccount().getSecondaryIdentification());
		}

		/* Creditor details needed on consent page */
		CreditorDetails creditorDetails = new CreditorDetails();
		if (!NullCheckUtils.isNullOrEmpty(dPaymentStageResponse.getData().getInitiation().getCreditorAccount())) {
			creditorDetails.setIdentification(
					dPaymentStageResponse.getData().getInitiation().getCreditorAccount().getIdentification());
			creditorDetails.setName(dPaymentStageResponse.getData().getInitiation().getCreditorAccount().getName());
			creditorDetails.setSchemeName(
					dPaymentStageResponse.getData().getInitiation().getCreditorAccount().getSchemeName());
			creditorDetails.setSecondaryIdentification(
					dPaymentStageResponse.getData().getInitiation().getCreditorAccount().getSecondaryIdentification());
		}

		/* Remittance details needed on consent page */
		RemittanceDetails remittanceDetails = new RemittanceDetails();
		if (!NullCheckUtils.isNullOrEmpty(dPaymentStageResponse.getData().getInitiation().getRemittanceInformation())) {
			remittanceDetails.setReference(
					dPaymentStageResponse.getData().getInitiation().getRemittanceInformation().getReference());
			remittanceDetails.setUnstructured(
					dPaymentStageResponse.getData().getInitiation().getRemittanceInformation().getUnstructured());
		}

		/* Charge details needed on consent page */
		ChargeDetails chargeDetails = new ChargeDetails();
		chargeDetails.setChargesList(dPaymentStageResponse.getData().getCharges());

		/* No Exchange Rate for Domestic Payment on consent page */
		ExchangeRateDetails exchangeRateDetails = null;

		response.setAmountDetails(amountDetails);
		response.setChargeDetails(chargeDetails);
		response.setCreditorDetails(creditorDetails);
		response.setDebtorDetails(debtorDetails);
		response.setRemittanceDetails(remittanceDetails);
		response.setExchangeRateDetails(exchangeRateDetails);
		return response;
	}

	@Override
	public CustomFraudSystemPaymentData transformDStageToFraudData(
			CustomDPaymentConsentsPOSTResponse domesticStageResponse) {
		CustomFraudSystemPaymentData fraudSystemData = new CustomFraudSystemPaymentData();
		fraudSystemData.setPaymentType(PaymentTypeEnum.DOMESTIC_PAY.getPaymentType());
		fraudSystemData
				.setTransferAmount(domesticStageResponse.getData().getInitiation().getInstructedAmount().getAmount());
		fraudSystemData.setTransferCurrency(
				domesticStageResponse.getData().getInitiation().getInstructedAmount().getCurrency());
		if (!NullCheckUtils.isNullOrEmpty(domesticStageResponse.getData().getInitiation().getRemittanceInformation()))
			fraudSystemData.setTransferMemo(
					domesticStageResponse.getData().getInitiation().getRemittanceInformation().getReference());
		fraudSystemData.setTransferTime(domesticStageResponse.getData().getCreationDateTime());
		CreditorDetails fraudSystenCreditorInfo = new CreditorDetails();
		fraudSystenCreditorInfo.setIdentification(
				domesticStageResponse.getData().getInitiation().getCreditorAccount().getIdentification());
		fraudSystemData.setCreditorDetails(fraudSystenCreditorInfo);

		return fraudSystemData;
	}

	@Override
	public void updateStagedPaymentConsents(CustomPaymentStageIdentifiers customStageIdentifiers,
			CustomPaymentStageUpdateData stageUpdateData, Map<String, String> params) {

		try {
			// Update cma1 staged resource
			if (customStageIdentifiers.getPaymentSetupVersion().equals(PaymentConstants.CMA_FIRST_VERSION)) {
				CustomDomesticPaymentSetupCma1POSTResponse updatedStagedResource = dPaymentSetupBankRepositoryCma1
						.findOneByDataPaymentId(customStageIdentifiers.getPaymentConsentId());
				if (stageUpdateData.getDebtorDetailsUpdated() == Boolean.TRUE) {
					
					OBCashAccountDebtor3 debtorDetails = new OBCashAccountDebtor3();
					debtorDetails.setIdentification(stageUpdateData.getDebtorDetails().getIdentification());
					debtorDetails.setName(stageUpdateData.getDebtorDetails().getName());
					if(stageUpdateData.getDebtorDetails().getSchemeName()!= null && stageUpdateData.getDebtorDetails().getSchemeName().startsWith(PSD2Constants.OB_SCHEME_PREFIX)){
						debtorDetails.setSchemeName(stageUpdateData.getDebtorDetails().getSchemeName().split(PSD2Constants.OB_SCHEME_PREFIX)[1]);
					}
					debtorDetails.setSecondaryIdentification(stageUpdateData.getDebtorDetails().getSecondaryIdentification());
					updatedStagedResource.getData().getInitiation().setDebtorAccount(debtorDetails);
				}
				if (stageUpdateData.getFraudScoreUpdated() == Boolean.TRUE) {
					updatedStagedResource.setFraudnetResponse(stageUpdateData.getFraudScore());
				}
				//PISP 1.1 Fix for Status to change to uppercase 
				updatedStagedResource.getData().setStatus(updatedStagedResource.getData().getStatus().toUpperCase());
	
				if (stageUpdateData.getSetupStatusUpdated() == Boolean.TRUE) {
					updatedStagedResource.getData().setStatus(stageUpdateData.getSetupStatus().toString().toUpperCase());
					updatedStagedResource.getData()
							.setStatusUpdateDateTime(stageUpdateData.getSetupStatusUpdateDateTime());
				}
				dPaymentSetupBankRepositoryCma1.save(updatedStagedResource);
			}

			// Update other than cma1 staged resource
			else{
					CustomDPaymentConsentsPOSTResponse updatedStagedResource = dPaymentConsentsBankRepository
						.findOneByDataConsentId(customStageIdentifiers.getPaymentConsentId());
					if (stageUpdateData.getDebtorDetailsUpdated() == Boolean.TRUE) {
						updatedStagedResource.getData().getInitiation()
							.setDebtorAccount(stageUpdateData.getDebtorDetails());
					}
					if (stageUpdateData.getFraudScoreUpdated() == Boolean.TRUE) {
						updatedStagedResource.setFraudScore(stageUpdateData.getFraudScore());
					}
					if (stageUpdateData.getSetupStatusUpdated() == Boolean.TRUE) {
							updatedStagedResource.getData()
								.setStatus(OBExternalConsentStatus1Code.fromValue(stageUpdateData.getSetupStatus()));
							updatedStagedResource.getData()
								.setStatusUpdateDateTime(stageUpdateData.getSetupStatusUpdateDateTime());
					}
					dPaymentConsentsBankRepository.save(updatedStagedResource);
			}
		} catch (DataAccessResourceFailureException exception) {
			throw PSD2Exception.populatePSD2Exception(
					new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR, exception.getMessage()));
		}

	}
}
