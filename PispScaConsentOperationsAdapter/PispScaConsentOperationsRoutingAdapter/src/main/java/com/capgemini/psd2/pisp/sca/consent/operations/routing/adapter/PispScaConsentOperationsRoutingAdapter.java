package com.capgemini.psd2.pisp.sca.consent.operations.routing.adapter;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.logger.LoggerUtils;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.domain.OBCashAccountDebtor3;
import com.capgemini.psd2.pisp.domain.PaymentConsentsValidationResponse;
import com.capgemini.psd2.pisp.enums.PaymentTypeEnum;
import com.capgemini.psd2.pisp.sca.consent.operations.adapter.PispScaConsentOperationsAdapter;
import com.capgemini.psd2.pisp.sca.consent.operations.routing.PispScaConsentOperationsAdapterFactory;
import com.capgemini.psd2.pisp.stage.domain.CustomConsentAppViewData;
import com.capgemini.psd2.pisp.stage.domain.CustomFraudSystemPaymentData;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageUpdateData;
import com.capgemini.psd2.pisp.stage.domain.CustomPreAuthorizeAdditionalInfo;
import com.capgemini.psd2.utilities.JSONUtilities;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Component
public class PispScaConsentOperationsRoutingAdapter implements PispScaConsentOperationsAdapter {

	private static final Logger LOG = LoggerFactory.getLogger(PispScaConsentOperationsRoutingAdapter.class);

	private PispScaConsentOperationsAdapter beanInstance;

	@Autowired
	private LoggerUtils loggerUtils;

	@Autowired
	private PispScaConsentOperationsAdapterFactory pispScaConsentOperationsAdapter;

	@Autowired
	private RequestHeaderAttributes reqHeaderAtrributes;

	@Value("${app.defaultPispStageAdapter}")
	private String defaultAdapterName;

	@Override
	public CustomConsentAppViewData retrieveConsentAppStagedViewData(CustomPaymentStageIdentifiers stageIdentifiers,
			Map<String, String> params) {
		if (beanInstance == null)
			beanInstance = pispScaConsentOperationsAdapter.getAdapterInstance(defaultAdapterName);
		return beanInstance.retrieveConsentAppStagedViewData(stageIdentifiers, getHeaderParams(params));
	}

	@Override
	public CustomFraudSystemPaymentData retrieveFraudSystemPaymentStagedData(
			CustomPaymentStageIdentifiers stageIdentifiers, Map<String, String> params) {
		if (beanInstance == null)
			beanInstance = pispScaConsentOperationsAdapter.getAdapterInstance(defaultAdapterName);
		return beanInstance.retrieveFraudSystemPaymentStagedData(stageIdentifiers, getHeaderParams(params));
	}

	@Override
	public PaymentConsentsValidationResponse validatePreAuthorisation(OBCashAccountDebtor3 selectedDebtorDetails,
			CustomPreAuthorizeAdditionalInfo preAuthAdditionalInfo, CustomPaymentStageIdentifiers stageIdentifiers,
			Map<String, String> params) {
		if (beanInstance == null)
			beanInstance = pispScaConsentOperationsAdapter.getAdapterInstance(defaultAdapterName);
		return beanInstance.validatePreAuthorisation(selectedDebtorDetails, preAuthAdditionalInfo, stageIdentifiers,
				getHeaderParams(params));
	}

	@Override
	public void updatePaymentStageData(CustomPaymentStageIdentifiers stageIdentifiers,
			CustomPaymentStageUpdateData updateData, Map<String, String> params) {
		LOG.info("{\"Enter\":\"{}\",\"{}\"}",
				"com.capgemini.psd2.pisp.sca.consent.operations.routing.adapter.updatePaymentStageData()",
				loggerUtils.populateLoggerData("updatePaymentStageData"));

		if (beanInstance == null)
			beanInstance = pispScaConsentOperationsAdapter.getAdapterInstance(defaultAdapterName);
		beanInstance.updatePaymentStageData(stageIdentifiers, updateData, getHeaderParams(params));

		LOG.info("{\"Exit\":\"{}\",\"{}\",\"tenantId\":{}}",
				"com.capgemini.psd2.pisp.sca.consent.operations.routing.adapter.updatePaymentStageData()",
				loggerUtils.populateLoggerData("updatePaymentStageData"),
				JSONUtilities.getJSONOutPutFromObject(reqHeaderAtrributes.getTenantId()));
	}

	private Map<String, String> getHeaderParams(Map<String, String> params) {
		if (NullCheckUtils.isNullOrEmpty(params))
			params = new HashMap<>();

		params.put(PSD2Constants.CORRELATION_REQ_HEADER, reqHeaderAtrributes.getCorrelationId());

		if (!params.containsKey(PSD2Constants.USER_IN_REQ_HEADER)
				|| params.get(PSD2Constants.USER_IN_REQ_HEADER) == null)
			params.put(PSD2Constants.USER_IN_REQ_HEADER, reqHeaderAtrributes.getPsuId());

		if (!params.containsKey(PSD2Constants.TENANT_ID) || params.get(PSD2Constants.TENANT_ID) == null)
			params.put(PSD2Constants.TENANT_ID, reqHeaderAtrributes.getTenantId());

		if (!params.containsKey(PSD2Constants.CHANNEL_IN_REQ_HEADER)
				|| params.get(PSD2Constants.CHANNEL_IN_REQ_HEADER) == null) {
			String channelId = null;
			String tokenChannelId = null;
			if (null != reqHeaderAtrributes.getToken() && null != reqHeaderAtrributes.getToken().getSeviceParams()) {
				tokenChannelId = reqHeaderAtrributes.getToken().getSeviceParams().get(PSD2Constants.CHANNEL_NAME);
			}
			channelId = (null == tokenChannelId) ? reqHeaderAtrributes.getChannelId() : tokenChannelId;
			params.put(PSD2Constants.CHANNEL_IN_REQ_HEADER, channelId);
		}

		return params;
	}

	@Override
	public CustomConsentAppViewData retrieveConsentAppDebtorDetails(CustomPaymentStageIdentifiers stageIdentifiers,
			CustomPaymentStageUpdateData updateData, Map<String, String> paramsMap) {
		LOG.info("{\"Enter\":\"{}\",\"{}\"}",
				"com.capgemini.psd2.pisp.sca.consent.operations.routing.adapter.retrieveConsentAppDebtorDetails()",
				loggerUtils.populateLoggerData("retrieveConsentAppDebtorDetails"));

		if (beanInstance == null)
			beanInstance = pispScaConsentOperationsAdapter.getAdapterInstance(defaultAdapterName);
		return beanInstance.retrieveConsentAppDebtorDetails(stageIdentifiers, updateData,getHeaderParams(paramsMap));
	}
}