package com.capgemini.psd2.international.payments.submission.mock.foundationservice.service;

import com.capgemini.psd2.international.payments.submission.mock.foundationservice.domain.PaymentInstructionProposalInternational;
import com.capgemini.psd2.international.payments.submission.mock.foundationservice.domain.PaymentInstructionProposalInternationalComposite;
import com.capgemini.psd2.international.payments.submission.mock.foundationservice.exception.handler.RecordNotFoundException;

public interface InternationalPaymentService {
	
	public PaymentInstructionProposalInternationalComposite createInternationalPaymentSubmissionResource(
			PaymentInstructionProposalInternational paymentInstructionProposalInternational,
			PaymentInstructionProposalInternationalComposite paymentInstructionProposalInternationalComposite) ;
	
	public PaymentInstructionProposalInternationalComposite retrievePaymentInstructionProposalInternationalResource(
			String paymentInstructionProposalId) throws RecordNotFoundException ;

}
