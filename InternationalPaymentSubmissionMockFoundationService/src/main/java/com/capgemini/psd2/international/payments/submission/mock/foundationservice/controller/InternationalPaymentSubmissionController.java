package com.capgemini.psd2.international.payments.submission.mock.foundationservice.controller;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import com.capgemini.psd2.foundationservice.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.foundationservice.exceptions.MockFoundationServiceException;
import com.capgemini.psd2.international.payments.submission.mock.foundationservice.domain.PaymentInstructionProposalInternational;
import com.capgemini.psd2.international.payments.submission.mock.foundationservice.domain.PaymentInstructionProposalInternationalComposite;
import com.capgemini.psd2.international.payments.submission.mock.foundationservice.exception.handler.RecordNotFoundException;
import com.capgemini.psd2.international.payments.submission.mock.foundationservice.service.InternationalPaymentService;


@RestController
@RequestMapping("/group-payments/p/payments-service")
public class InternationalPaymentSubmissionController {
	@Autowired
	private InternationalPaymentService internationalPaymentService;	

	// Submission
	@RequestMapping(value = "/v{version}/international/payment-instructions", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
	@ResponseStatus(HttpStatus.OK)
	public PaymentInstructionProposalInternationalComposite executeInternationalPayment(
			@RequestBody PaymentInstructionProposalInternational paymentInstructionProposalInternational,		
			@PathVariable("version") String version,
			@RequestHeader(required = false, value = "X-API-TRANSACTION-ID") String transactionId,
			@RequestHeader(required = false, value = "X-API-CORRELATION-ID") String correlationId,
			@RequestHeader(required = false, value = "X-API-SOURCE-SYSTEM") String sourceSystem,
			@RequestHeader(required = false, value = "X-API-SOURCE-USER") String sourceUserReqHeader,
			@RequestHeader(required = false, value = "X-API-CHANNEL-CODE") String channelcode,
			@RequestHeader(required = false, value = "X-API-CHANNEL-BRAND") String channelBrand,
			@RequestHeader(required = false, value = "X-SYSTEM-API-VERSION") String apiVesrion,
			@RequestHeader(required = false, value = "X-API-PARTY-SOURCE-ID-NUMBER") String partySourceHeader) {		
		
		if (StringUtils.isBlank(sourceSystem) || StringUtils.isBlank(channelBrand) || StringUtils.isBlank(version)) {
			throw MockFoundationServiceException
					.populateMockFoundationServiceException(ErrorCodeEnum.BAD_REQUEST_PPD_PIP);
		}
		PaymentInstructionProposalInternationalComposite paymentInstructionProposalInternationalComposite = new PaymentInstructionProposalInternationalComposite() ;		
		paymentInstructionProposalInternationalComposite=internationalPaymentService.createInternationalPaymentSubmissionResource(paymentInstructionProposalInternational,
				                                             paymentInstructionProposalInternationalComposite);
		return paymentInstructionProposalInternationalComposite;
	}
	
	
	// Retrieve
	@RequestMapping(value = "/v{version}/international/payment-instruction-proposals/{paymentInstructionNumber}",
		method = RequestMethod.GET, produces = "application/json")
	@ResponseStatus(HttpStatus.OK)
	public PaymentInstructionProposalInternationalComposite retriveInternationalPayment(			
			@PathVariable("paymentInstructionNumber") String paymentInstructionNumber,
			@PathVariable("version") String version,
			@RequestHeader(required = false, value = "X-API-TRANSACTION-ID") String transactionId,
			@RequestHeader(required = false, value = "X-API-CORRELATION-ID") String correlationId,
			@RequestHeader(required = false, value = "X-API-SOURCE-SYSTEM") String sourceSystem,
			@RequestHeader(required = false, value = "X-API-SOURCE-USER") String sourceUserReqHeader,
			@RequestHeader(required = false, value = "X-API-CHANNEL-CODE") String channelcode,
			@RequestHeader(required = false, value = "X-API-CHANNEL-BRAND") String channelBrand,
			@RequestHeader(required = false, value = "X-SYSTEM-API-VERSION") String apiVesrion,
			@RequestHeader(required = false, value = "X-API-PARTY-SOURCE-ID-NUMBER") String partySourceHeader) throws RecordNotFoundException {		
		
		if ( StringUtils.isBlank(version) || StringUtils.isBlank(paymentInstructionNumber) 
				|| StringUtils.isBlank(sourceSystem) || StringUtils.isBlank(channelBrand) ) {
			throw MockFoundationServiceException
					.populateMockFoundationServiceException(ErrorCodeEnum.BAD_REQUEST_PPD_PIP);
		}
			return 	internationalPaymentService.retrievePaymentInstructionProposalInternationalResource(paymentInstructionNumber);
	}
	
}
