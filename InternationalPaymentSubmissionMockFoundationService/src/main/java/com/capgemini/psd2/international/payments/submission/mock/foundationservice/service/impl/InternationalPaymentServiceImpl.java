package com.capgemini.psd2.international.payments.submission.mock.foundationservice.service.impl;

import java.util.Calendar;
import java.util.Date;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.capgemini.psd2.foundationservice.datetime.utility.TimeZoneDateTimeAdapter;
import com.capgemini.psd2.international.payments.submission.mock.foundationservice.constants.InternationalPaymentFoundationConstants;
import com.capgemini.psd2.international.payments.submission.mock.foundationservice.domain.PaymentInstruction2;
import com.capgemini.psd2.international.payments.submission.mock.foundationservice.domain.PaymentInstructionProposalInternational;
import com.capgemini.psd2.international.payments.submission.mock.foundationservice.domain.PaymentInstructionProposalInternationalComposite;
import com.capgemini.psd2.international.payments.submission.mock.foundationservice.domain.PaymentInstructionStatusCode;
import com.capgemini.psd2.international.payments.submission.mock.foundationservice.exception.handler.RecordNotFoundException;
import com.capgemini.psd2.international.payments.submission.mock.foundationservice.repository.InternationalPaymentSubmissionRepository;
import com.capgemini.psd2.international.payments.submission.mock.foundationservice.service.InternationalPaymentService;


@Service
public class InternationalPaymentServiceImpl implements InternationalPaymentService {

	@Autowired
	InternationalPaymentSubmissionRepository internationalPaymentSubmissionRepository;	
	
	@Autowired
	private TimeZoneDateTimeAdapter timeZoneDateTimeAdapter;	
	
	@Override
	public PaymentInstructionProposalInternationalComposite createInternationalPaymentSubmissionResource(
			PaymentInstructionProposalInternational paymentInstructionProposalInternational,
			PaymentInstructionProposalInternationalComposite paymentInstructionProposalInternationalComposite)
		 {
		PaymentInstruction2 paymentInstruction = new PaymentInstruction2();
		paymentInstruction.setPaymentInstructionNumber(UUID.randomUUID().toString());
		
		try {
			Date date = Calendar.getInstance().getTime();
			String strDate = timeZoneDateTimeAdapter.parseDateTimeCMA(date);
			paymentInstruction.setInstructionIssueDate(strDate);
			paymentInstruction.setInstructionStatusUpdateDateTime(strDate);
			} catch (Exception e) {
				e.printStackTrace();
			}
		paymentInstruction.setPaymentInstructionStatusCode(PaymentInstructionStatusCode.ACCEPTEDSETTLEMENTINPROCESS);
		paymentInstructionProposalInternationalComposite.setPaymentInstruction(paymentInstruction);
		paymentInstructionProposalInternationalComposite.setPaymentInstructionProposalInternational(paymentInstructionProposalInternational);
		internationalPaymentSubmissionRepository.save(paymentInstructionProposalInternationalComposite);		
		return paymentInstructionProposalInternationalComposite;
	}

	@Override
	public PaymentInstructionProposalInternationalComposite retrievePaymentInstructionProposalInternationalResource(
			String paymentInstructionNumber) throws RecordNotFoundException {
		PaymentInstructionProposalInternationalComposite paymentInstructionProposalInternationalComposite = internationalPaymentSubmissionRepository.findByPaymentInstructionPaymentInstructionNumber(paymentInstructionNumber);
			if(paymentInstructionProposalInternationalComposite == null)
				throw new RecordNotFoundException(InternationalPaymentFoundationConstants.RECORD_NOT_FOUND);			 
		return paymentInstructionProposalInternationalComposite;
	}

	
	
}