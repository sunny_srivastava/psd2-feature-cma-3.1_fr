package com.capgemini.psd2.international.payments.submission.mock.foundationservice.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.capgemini.psd2.international.payments.submission.mock.foundationservice.domain.PaymentInstructionProposalInternationalComposite;

public interface InternationalPaymentSubmissionRepository extends MongoRepository<PaymentInstructionProposalInternationalComposite, String> {
	
	PaymentInstructionProposalInternationalComposite findByPaymentInstructionPaymentInstructionNumber(String paymentInstructionNumber);
	
}
