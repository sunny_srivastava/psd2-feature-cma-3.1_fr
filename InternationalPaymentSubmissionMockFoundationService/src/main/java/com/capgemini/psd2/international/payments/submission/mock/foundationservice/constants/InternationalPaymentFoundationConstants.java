package com.capgemini.psd2.international.payments.submission.mock.foundationservice.constants;

public class InternationalPaymentFoundationConstants {	
	public static final String RECORD_NOT_FOUND = "Payment Instruction Proposal Record Not Found";	
}
