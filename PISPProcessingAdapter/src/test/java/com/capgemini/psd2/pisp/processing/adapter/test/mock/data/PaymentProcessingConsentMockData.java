package com.capgemini.psd2.pisp.processing.adapter.test.mock.data;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.pisp.domain.CustomDPaymentConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomDPaymentConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.CustomDPaymentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomDSPConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomDSPaymentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomDStandingOrderConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomDStandingOrderPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomFilePaymentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomIPaymentConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomIPaymentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomISPConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomISPaymentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomPaymentSetupPlatformDetails;
import com.capgemini.psd2.pisp.domain.OBAuthorisation1;
import com.capgemini.psd2.pisp.domain.OBCashAccountCreditor2;
import com.capgemini.psd2.pisp.domain.OBCharge1;
import com.capgemini.psd2.pisp.domain.OBCharge1Amount;
import com.capgemini.psd2.pisp.domain.OBChargeBearerType1Code;
import com.capgemini.psd2.pisp.domain.OBDomestic1;
import com.capgemini.psd2.pisp.domain.OBDomestic1InstructedAmount;
import com.capgemini.psd2.pisp.domain.OBDomesticScheduled1;
import com.capgemini.psd2.pisp.domain.OBDomesticStandingOrder1;
import com.capgemini.psd2.pisp.domain.OBExternalAuthorisation1Code;
import com.capgemini.psd2.pisp.domain.OBExternalConsentStatus1Code;
import com.capgemini.psd2.pisp.domain.OBExternalPaymentContext1Code;
import com.capgemini.psd2.pisp.domain.OBFile1;
import com.capgemini.psd2.pisp.domain.OBInternational1;
import com.capgemini.psd2.pisp.domain.OBInternationalScheduled1;
import com.capgemini.psd2.pisp.domain.OBRemittanceInformation1;
import com.capgemini.psd2.pisp.domain.OBRisk1;
import com.capgemini.psd2.pisp.domain.OBRisk1DeliveryAddress;
import com.capgemini.psd2.pisp.domain.OBWriteDataDomestic1;
import com.capgemini.psd2.pisp.domain.OBWriteDataDomesticConsent1;
import com.capgemini.psd2.pisp.domain.OBWriteDataDomesticConsentResponse1;
import com.capgemini.psd2.pisp.domain.OBWriteDataDomesticResponse1;
import com.capgemini.psd2.pisp.domain.OBWriteDataDomesticScheduled1;
import com.capgemini.psd2.pisp.domain.OBWriteDataDomesticScheduledConsent1;
import com.capgemini.psd2.pisp.domain.OBWriteDataDomesticStandingOrder1;
import com.capgemini.psd2.pisp.domain.OBWriteDataDomesticStandingOrderConsent1;
import com.capgemini.psd2.pisp.domain.OBWriteDataFile1;
import com.capgemini.psd2.pisp.domain.OBWriteDataInternational1;
import com.capgemini.psd2.pisp.domain.OBWriteDataInternationalConsent1;
import com.capgemini.psd2.pisp.domain.OBWriteDataInternationalScheduled1;
import com.capgemini.psd2.pisp.domain.OBWriteDataInternationalScheduledConsent1;
import com.capgemini.psd2.pisp.domain.PaymentConsentsPlatformResource;
import com.capgemini.psd2.pisp.domain.PaymentDomesticSubmitPOST201Response;
import com.capgemini.psd2.pisp.domain.PaymentResponseInfo;
import com.capgemini.psd2.pisp.domain.PaymentRetrieveGetRequest;
import com.capgemini.psd2.pisp.domain.PaymentsPlatformResource;
import com.capgemini.psd2.pisp.enums.PaymentTypeEnum;
import com.capgemini.psd2.pisp.enums.ProcessConsentStatusEnum;
import com.capgemini.psd2.pisp.status.PaymentConstants;
import com.capgemini.psd2.pisp.utilities.PispUtilities;
import com.capgemini.psd2.token.Token;

public class PaymentProcessingConsentMockData {

	public static PaymentConsentsPlatformResource setupConsentPlatformResource() {
		PaymentConsentsPlatformResource platformResource = new PaymentConsentsPlatformResource();
		platformResource.setCreatedAt(PispUtilities.getCurrentDateInISOFormat());
		platformResource.setIdempotencyKey("idempotency");
		platformResource.setIdempotencyRequest(String.valueOf(Boolean.TRUE));
		platformResource.setPaymentConsentId("e58976e7-605c-48ac-aec5-0999343f6148");
		platformResource.setPaymentType(PaymentTypeEnum.DOMESTIC_PAY.getPaymentType());
		platformResource.setStatus("AwaitingAuthorisation");
		platformResource.setEndToEndIdentification("DEMO USER");
		platformResource.setInstructionIdentification("International");
		platformResource.setStatusUpdateDateTime(PispUtilities.getCurrentDateInISOFormat());
		platformResource.setTppCID("53ZcZkjLM1sXLOAHkwG6DB");
		platformResource.setTppLegalEntityName("BankOfDemo");
		return platformResource;
	}
	
	
	public static PaymentConsentsPlatformResource setupConsentPlatformResource_filepaytype() {
		PaymentConsentsPlatformResource platformResource = new PaymentConsentsPlatformResource();
		platformResource.setCreatedAt(PispUtilities.getCurrentDateInISOFormat());
		platformResource.setIdempotencyKey("idempotency");
		platformResource.setIdempotencyRequest(String.valueOf(Boolean.TRUE));
		platformResource.setPaymentConsentId("e58976e7-605c-48ac-aec5-0999343f6148");
		platformResource.setPaymentType(PaymentTypeEnum.FILE_PAY.getPaymentType());
		platformResource.setStatus("AwaitingAuthorisation");
		platformResource.setEndToEndIdentification("DEMO USER");
		platformResource.setInstructionIdentification("International");
		platformResource.setStatusUpdateDateTime(PispUtilities.getCurrentDateInISOFormat());
		platformResource.setTppCID("53ZcZkjLM1sXLOAHkwG6DB");
		platformResource.setTppLegalEntityName("BankOfDemo");
		return platformResource;
	}

	public static CustomPaymentSetupPlatformDetails setupCustomPlatformDetails() {
		CustomPaymentSetupPlatformDetails customPlatformDetails = new CustomPaymentSetupPlatformDetails();
		customPlatformDetails.setPaymentType(PaymentTypeEnum.DOMESTIC_PAY);
		customPlatformDetails.setSetupCmaVersion(PaymentConstants.CMA_THIRD_VERSION);
		customPlatformDetails.setTppDebtorDetails(String.valueOf(Boolean.FALSE));
		customPlatformDetails.setTppDebtorNameDetails(String.valueOf(Boolean.FALSE));
		return customPlatformDetails;
	}

	public static CustomDPaymentConsentsPOSTRequest getDomesticPaymentConsentsRequest() {
		CustomDPaymentConsentsPOSTRequest request = new CustomDPaymentConsentsPOSTRequest();

		request.setData(getData());
		request.setRisk(getRisk());

		return request;
	}
	
	public static CustomDStandingOrderConsentsPOSTRequest getCustomDStandingOrderConsentsPOSTRequest() {
		CustomDStandingOrderConsentsPOSTRequest request = new CustomDStandingOrderConsentsPOSTRequest();

		request.setData(new OBWriteDataDomesticStandingOrderConsent1());
		request.setRisk(getRisk());

		return request;
	}
	
	
	public static CustomDSPConsentsPOSTRequest getCustomDSPConsentsPOSTRequest() {
		CustomDSPConsentsPOSTRequest request = new CustomDSPConsentsPOSTRequest();

		request.setData(new OBWriteDataDomesticScheduledConsent1());
		request.setRisk(getRisk());

		return request;
	}
	
	public static CustomISPConsentsPOSTRequest getCustomISPConsentsPOSTRequest() {
		CustomISPConsentsPOSTRequest request = new CustomISPConsentsPOSTRequest();

		request.setData(new OBWriteDataInternationalScheduledConsent1());
		request.setRisk(getRisk());

		return request;
	}
	
	public static CustomIPaymentConsentsPOSTRequest getCustomIPaymentConsentsPOSTRequest() {
		CustomIPaymentConsentsPOSTRequest request = new CustomIPaymentConsentsPOSTRequest();

		request.setData(new OBWriteDataInternationalConsent1());
		request.setRisk(getRisk());

		return request;
	}
	

	private static OBWriteDataDomesticConsent1 getData() {
		OBWriteDataDomesticConsent1 data = new OBWriteDataDomesticConsent1();

		data.setAuthorisation(getAuthorisation());
		data.setInitiation(getInitiation());

		return data;
	}

	private static OBAuthorisation1 getAuthorisation() {
		OBAuthorisation1 authorisation = new OBAuthorisation1();

		authorisation.setAuthorisationType(OBExternalAuthorisation1Code.ANY);
		authorisation
				.setCompletionDateTime(PispUtilities.getTemporalAccessorInISOFormat(ZonedDateTime.now().plusDays(1)));
		return authorisation;
	}

	private static OBDomestic1 getInitiation() {
		OBDomestic1 initiation = new OBDomestic1();
		initiation.setCreditorAccount(getCreditorAccount());
		initiation.setInstructionIdentification("International");
		initiation.setEndToEndIdentification("DEMO USER");
		initiation.setInstructedAmount(getInstructedAmount());
		initiation.setRemittanceInformation(getRemittanceInformation());
		return initiation;
	}

	private static OBCashAccountCreditor2 getCreditorAccount() {
		OBCashAccountCreditor2 creditor = new OBCashAccountCreditor2();
		creditor.setIdentification("FR1420041010050500013M02606");
		creditor.setName("Test user");
		creditor.setSchemeName("UK.OBIE.IBAN");
		creditor.setSecondaryIdentification("0002");
		return creditor;
	}

	private static OBDomestic1InstructedAmount getInstructedAmount() {
		OBDomestic1InstructedAmount amount = new OBDomestic1InstructedAmount();
		amount.setAmount("23.22");
		amount.setCurrency("EUR");
		return amount;
	}

	private static OBRemittanceInformation1 getRemittanceInformation() {
		OBRemittanceInformation1 remittanceInformation = new OBRemittanceInformation1();
		remittanceInformation.setReference("FRESCO-101");
		remittanceInformation.setUnstructured("Internal ops code 5120101");
		return remittanceInformation;
	}

	private static OBRisk1 getRisk() {
		OBRisk1 risk = new OBRisk1();
		risk.setPaymentContextCode(OBExternalPaymentContext1Code.ECOMMERCEGOODS);
		risk.setMerchantCategoryCode("5967");
		risk.setMerchantCustomerIdentification("053598653254");
		risk.setDeliveryAddress(getDeliveryAddress());
		return risk;
	}

	private static OBRisk1DeliveryAddress getDeliveryAddress() {
		OBRisk1DeliveryAddress address = new OBRisk1DeliveryAddress();
		address.setAddressLine(new ArrayList<String>());
		address.setBuildingNumber("27");
		address.setCountry("GB");
		address.setPostCode("7U31 2ZZ");
		address.setStreetName("AcaciaAvenue");
		return address;
	}

	public static PaymentRetrieveGetRequest getPaymentRetrieveGetRequest() {
		PaymentRetrieveGetRequest retrieveRequest = new PaymentRetrieveGetRequest();
		retrieveRequest.setPaymentTypeEnum(PaymentTypeEnum.DOMESTIC_PAY);
		retrieveRequest.setConsentId("e58976e7-605c-48ac-aec5-0999343f6148");
		return retrieveRequest;
	}

	public static PaymentResponseInfo getPaymentResponseInfo() {
		PaymentResponseInfo paymentResponseInfo = new PaymentResponseInfo();
		paymentResponseInfo.setConsentProcessStatus(ProcessConsentStatusEnum.PASS);
		paymentResponseInfo.setConsentValidationStatus(OBExternalConsentStatus1Code.AWAITINGAUTHORISATION);
		paymentResponseInfo.setIdempotencyRequest(String.valueOf(Boolean.TRUE));
		paymentResponseInfo.setRequestType(PSD2Constants.PAYMENT_RESOURCE_IDEMPOTENT);
		paymentResponseInfo.setPaymentConsentId("e58976e7-605c-48ac-aec5-0999343f6148");
		return paymentResponseInfo;
	}

	public static CustomDPaymentConsentsPOSTResponse getPaymentConsentResponse() {
		CustomDPaymentConsentsPOSTResponse response = new CustomDPaymentConsentsPOSTResponse();
		response.setData(getResponseData());
		response.setRisk(getRisk());
		return response;
	}

	private static OBWriteDataDomesticConsentResponse1 getResponseData() {
		OBWriteDataDomesticConsentResponse1 responseData = new OBWriteDataDomesticConsentResponse1();
		responseData.setConsentId("e58976e7-605c-48ac-aec5-0999343f6148");
		responseData.setInitiation(getInitiation());
		responseData.setAuthorisation(getAuthorisation());
		ArrayList<OBCharge1> list = new ArrayList<>();
		list.add(getCharges());
		responseData.setCharges(list);
		return responseData;
	}

	private static OBCharge1 getCharges() {
		OBCharge1 charge = new OBCharge1();
		charge.setAmount(getChargedAmount());
		charge.setType("string");
		charge.setChargeBearer(OBChargeBearerType1Code.BORNEBYCREDITOR);
		return charge;
	}

	private static OBCharge1Amount getChargedAmount() {
		OBCharge1Amount amount = new OBCharge1Amount();
		amount.setAmount("23.22");
		amount.setCurrency("EUR");
		return amount;
	}

	public static CustomDPaymentsPOSTRequest getSubmissionRequest() {
		CustomDPaymentsPOSTRequest request = new CustomDPaymentsPOSTRequest();
		request.setData(getSubmissionData());
		request.setRisk(getRisk());
		return request;
	}
	
	public static CustomDSPaymentsPOSTRequest getSubmissionRequestCustomDSPayments() {
		CustomDSPaymentsPOSTRequest request = new CustomDSPaymentsPOSTRequest();
		OBWriteDataDomesticScheduled1 submissionData = new OBWriteDataDomesticScheduled1();
		submissionData.setInitiation(new OBDomesticScheduled1());
		submissionData.setConsentId("e58976e7-605c-48ac-aec5-0999343f6148");
		request.setData(submissionData);
		request.setRisk(getRisk());
		return request;
	}
	
	public static CustomDStandingOrderPOSTRequest getSubmissionRequestCustomDStandingOrderPOSTRequest() {
		CustomDStandingOrderPOSTRequest request = new CustomDStandingOrderPOSTRequest();
		OBWriteDataDomesticStandingOrder1 submissionData = new OBWriteDataDomesticStandingOrder1();
		submissionData.setInitiation(new OBDomesticStandingOrder1());
		submissionData.setConsentId("e58976e7-605c-48ac-aec5-0999343f6148");
		request.setData(submissionData);
		request.setRisk(getRisk());
		return request;
	}
	
	public static CustomIPaymentsPOSTRequest getSubmissionRequestCustomIPaymentsPOSTRequest() {
		CustomIPaymentsPOSTRequest request = new CustomIPaymentsPOSTRequest();
		OBWriteDataInternational1 submissionData = new OBWriteDataInternational1();
		submissionData.setInitiation(new OBInternational1());
		submissionData.setConsentId("e58976e7-605c-48ac-aec5-0999343f6148");
		request.setData(submissionData);
		request.setRisk(getRisk());
		return request;
	}
	
	public static CustomISPaymentsPOSTRequest getSubmissionRequestCustomISPaymentsPOSTRequest() {
		CustomISPaymentsPOSTRequest request = new CustomISPaymentsPOSTRequest();
		OBWriteDataInternationalScheduled1 submissionData = new OBWriteDataInternationalScheduled1();
		submissionData.setInitiation(new OBInternationalScheduled1());
		submissionData.setConsentId("e58976e7-605c-48ac-aec5-0999343f6148");
		request.setData(submissionData);
		request.setRisk(getRisk());
		return request;
	}
	
	
	
	public static CustomFilePaymentsPOSTRequest getSubmissionRequestCustomFilePaymentsPOSTRequest() {
		CustomFilePaymentsPOSTRequest request = new CustomFilePaymentsPOSTRequest();
		OBWriteDataFile1 submissionData = new OBWriteDataFile1();
		submissionData.setInitiation(new OBFile1());
		submissionData.setConsentId("e58976e7-605c-48ac-aec5-0999343f6148");
		request.setData(submissionData);
		//request.setRisk(getRisk());
		return request;
	}
	
	private static OBWriteDataDomestic1 getSubmissionData() {
		OBWriteDataDomestic1 submissionData = new OBWriteDataDomestic1();
		submissionData.setInitiation(getInitiation());
		submissionData.setConsentId("e58976e7-605c-48ac-aec5-0999343f6148");
		return submissionData;
	}

	public static Token getToken() {
		Token token = new Token();
		token.setClient_id("53ZcZkjLM1sXLOAHkwG6DB");
		token.setRequestId("e58976e7-605c-48ac-aec5-0999343f6148");
		return token;
	}
	public static Token getToken1() {
		Token token = new Token();
		token.setClient_id("53ZcZkjLM1sXLOAHkwG6DB");
		token.setRequestId("e58976e7-605c-48ac-aec5-0999343f6148");
		
		Map<String, String> map1 = new HashMap<>();
		map1.put(PSD2Constants.CHANNEL_NAME, "DD");
		token.setSeviceParams(map1);
		return token;
	}

	public static PaymentsPlatformResource getSubmissionPlatformResource() {
		PaymentsPlatformResource resource = new PaymentsPlatformResource();
		resource.setIdempotencyKey("idempotency");
		resource.setTppCID("53ZcZkjLM1sXLOAHkwG6DB");
		resource.setPaymentConsentId("e58976e7-605c-48ac-aec5-0999343f6148");
		resource.setProccessState(PaymentConstants.COMPLETED);
		resource.setPaymentType(PaymentTypeEnum.DOMESTIC_PAY.toString());
		return resource;
	}
	
	public static PaymentsPlatformResource getSubmissionIdPlatformResource() {
		PaymentsPlatformResource resource = new PaymentsPlatformResource();
		resource.setIdempotencyKey("idempotency");
		resource.setTppCID("53ZcZkjLM1sXLOAHkwG6DB");
		resource.setPaymentConsentId("e58976e7-605c-48ac-aec5-0999343f6148");
		resource.setSubmissionId("Test");
		resource.setProccessState(PaymentConstants.COMPLETED);
		resource.setPaymentType(PaymentTypeEnum.DOMESTIC_PAY.toString());
		return resource;
	}


	public static PaymentDomesticSubmitPOST201Response getSubmissionResponse() {
		PaymentDomesticSubmitPOST201Response response = new PaymentDomesticSubmitPOST201Response();
		response.setData(getSubmissionResponseData());
		return response;
	}

	private static OBWriteDataDomesticResponse1 getSubmissionResponseData() {
		OBWriteDataDomesticResponse1 data = new OBWriteDataDomesticResponse1();
		data.setInitiation(getInitiation());
		data.setConsentId("e58976e7-605c-48ac-aec5-0999343f6148");
		data.setDomesticPaymentId("e58976e7-605c-48ac-aec5-0999343f6148");
		return data;
	}
}