package com.capgemini.psd2.pisp.processing.adapter.test.service.impl;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.Map;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpMethod;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.consent.domain.PispConsent;
import com.capgemini.psd2.exceptions.OBPSD2ExceptionUtility;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.LoggerUtils;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.adapter.PispConsentAdapter;
import com.capgemini.psd2.pisp.consent.adapter.repository.PispConsentMongoRepository;
import com.capgemini.psd2.pisp.domain.CustomDPaymentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.OBExternalStatus2Code;
import com.capgemini.psd2.pisp.domain.OBRisk1;
import com.capgemini.psd2.pisp.domain.OBWriteDataDomestic1;
import com.capgemini.psd2.pisp.domain.PaymentConsentsPlatformResource;
import com.capgemini.psd2.pisp.domain.PaymentDomesticSubmitPOST201Response;
import com.capgemini.psd2.pisp.domain.PaymentsPlatformResource;
import com.capgemini.psd2.pisp.enums.PaymentTypeEnum;
import com.capgemini.psd2.pisp.enums.ProcessExecutionStatusEnum;
import com.capgemini.psd2.pisp.payment.setup.platform.adapter.PaymentSetupPlatformAdapter;
import com.capgemini.psd2.pisp.payment.submission.platform.adapter.PaymentsPlatformAdapter;
import com.capgemini.psd2.pisp.processing.adapter.PaymentSubmissionCustomValidator;
import com.capgemini.psd2.pisp.processing.adapter.PaymentSubmissionTransformer;
import com.capgemini.psd2.pisp.processing.adapter.service.impl.PaymentSubmissionProcessingAdapterImpl;
import com.capgemini.psd2.pisp.processing.adapter.test.mock.data.PaymentProcessingConsentMockData;
import com.capgemini.psd2.pisp.status.PaymentConstants;
import com.capgemini.psd2.pisp.status.PaymentStatusEnum;
import com.capgemini.psd2.pisp.utilities.PispUtilities;
import com.capgemini.psd2.token.Token;

@RunWith(SpringJUnit4ClassRunner.class)
public class PaymentSubmissionProcessingAdapterImplTest {

	@InjectMocks
	private PaymentSubmissionProcessingAdapterImpl paymentSubmissionTest = new PaymentSubmissionProcessingAdapterImpl(
			"24H", "24H");

	@Mock
	private PaymentSubmissionCustomValidator<CustomDPaymentsPOSTRequest, PaymentDomesticSubmitPOST201Response> paymentCustomValidator;

	@Mock
	private LoggerUtils loggerUtils;
	
	@Mock
	private RequestHeaderAttributes reqHeaderAtrributes;

	@Mock
	private PaymentSetupPlatformAdapter paymentConsentsPlatformAdapter;

	@Mock
	private PaymentsPlatformAdapter paymentsPlatformAdapter;

	@Mock
	private PispConsentAdapter pispConsentAdapter;

	@Mock
	private PispConsentMongoRepository pispConsentMongoRepository;

	@Mock
	private PaymentSubmissionTransformer transformer;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		Map<String, String> map = new HashMap<>();
		map.put("SIGNATURE", "Something wrong with x-jws-signature request header");
		Map<String, String> specificErrorMessageMap = new HashMap<>();
		specificErrorMessageMap.put("signature_invalid_content",
				"Invalid content prsent in joseheader of x-jws-signature header");
		specificErrorMessageMap.put("signature_missing", "signature header missing in request");
		OBPSD2ExceptionUtility.genericErrorMessages.putAll(map);
		OBPSD2ExceptionUtility.specificErrorMessages.putAll(specificErrorMessageMap);
	}

	@Test
	public void preSubmissionProcessingWithPlatformResource() {
		CustomDPaymentsPOSTRequest request = new CustomDPaymentsPOSTRequest();
		OBWriteDataDomestic1 data = new OBWriteDataDomestic1();
		OBRisk1 risk = new OBRisk1();

		request.setData(data);
		request.setRisk(risk);
		
		PaymentsPlatformResource platformResource=new PaymentsPlatformResource();
		platformResource.setIdempotencyKey("idempotency");
		platformResource.setTppCID("53ZcZkjLM1sXLOAHkwG6DB");
		platformResource.setPaymentConsentId("e58976e7-605c-48ac-aec5-0999343f6148");
		platformResource.setProccessState(PaymentConstants.COMPLETED);
		platformResource.setPaymentType(PaymentTypeEnum.DOMESTIC_PAY.toString());
		
		Mockito.when(reqHeaderAtrributes.getToken()).thenReturn(PaymentProcessingConsentMockData.getToken());
		Mockito.when(paymentConsentsPlatformAdapter.retrievePaymentSetupPlatformResource(anyString()))
				.thenReturn(PaymentProcessingConsentMockData.setupConsentPlatformResource());
		Mockito.when(paymentsPlatformAdapter.getIdempotentPaymentsPlatformResource(anyLong(), anyString()))
				.thenReturn(platformResource);
		Mockito.when(paymentsPlatformAdapter.retrievePaymentsResourceByConsentId(anyString()))
				.thenReturn(PaymentProcessingConsentMockData.getSubmissionPlatformResource());
		paymentSubmissionTest.preSubmissionProcessing(request,
				PaymentProcessingConsentMockData.setupCustomPlatformDetails(), "e58976e7-605c-48ac-aec5-0999343f6148");
	}
	
	
	@Test(expected = PSD2Exception.class)
	public void preSubmissionProcessingWithPlatformResource_Exception1() {
		CustomDPaymentsPOSTRequest request = new CustomDPaymentsPOSTRequest();
		OBWriteDataDomestic1 data = new OBWriteDataDomestic1();
		OBRisk1 risk = new OBRisk1();

		request.setData(data);
		request.setRisk(risk);
		
		PaymentsPlatformResource platformResource=new PaymentsPlatformResource();
		platformResource.setIdempotencyKey("idempotency");
		platformResource.setTppCID("53ZcZkjLM1sXLOAHkwG6DB");
		platformResource.setPaymentConsentId("e58976e7-605c-48ac-aec5-0999343f6148");
		platformResource.setProccessState(PaymentConstants.COMPLETED);
		platformResource.setPaymentType(PaymentTypeEnum.DOMESTIC_PAY.toString());
		
		Mockito.when(reqHeaderAtrributes.getToken()).thenReturn(PaymentProcessingConsentMockData.getToken());
		Mockito.when(paymentConsentsPlatformAdapter.retrievePaymentSetupPlatformResource(anyString()))
				.thenReturn(PaymentProcessingConsentMockData.setupConsentPlatformResource());
		Mockito.when(paymentsPlatformAdapter.getIdempotentPaymentsPlatformResource(anyLong(), anyString()))
				.thenReturn(platformResource);
		Mockito.when(paymentsPlatformAdapter.retrievePaymentsResourceByConsentId(anyString()))
				.thenReturn(PaymentProcessingConsentMockData.getSubmissionPlatformResource());
		paymentSubmissionTest.preSubmissionProcessing(request,
				PaymentProcessingConsentMockData.setupCustomPlatformDetails(), "e58976e7-605c-48ac-aec5-0999343f6149");
	}
	
	
	@Test(expected = PSD2Exception.class)
	public void preSubmissionProcessingWithPlatformResource_Exception2() {
		CustomDPaymentsPOSTRequest request = new CustomDPaymentsPOSTRequest();
		OBWriteDataDomestic1 data = new OBWriteDataDomestic1();
		OBRisk1 risk = new OBRisk1();

		request.setData(data);
		request.setRisk(risk);
		
		PaymentsPlatformResource platformResource=new PaymentsPlatformResource();
		platformResource.setIdempotencyKey("idempotency");
		platformResource.setTppCID("53ZcZkjLM1sXLOAHkwG6DB");
		platformResource.setPaymentConsentId("e58976e7-605c-48ac-aec5-0999343f6148");
		platformResource.setProccessState(PaymentConstants.ABORT);
		platformResource.setPaymentType(PaymentTypeEnum.DOMESTIC_PAY.toString());
		
		Mockito.when(reqHeaderAtrributes.getToken()).thenReturn(PaymentProcessingConsentMockData.getToken());
		Mockito.when(paymentConsentsPlatformAdapter.retrievePaymentSetupPlatformResource(anyString()))
				.thenReturn(PaymentProcessingConsentMockData.setupConsentPlatformResource());
		Mockito.when(paymentsPlatformAdapter.getIdempotentPaymentsPlatformResource(anyLong(), anyString()))
				.thenReturn(platformResource);
		Mockito.when(paymentsPlatformAdapter.retrievePaymentsResourceByConsentId(anyString()))
				.thenReturn(PaymentProcessingConsentMockData.getSubmissionPlatformResource());
		paymentSubmissionTest.preSubmissionProcessing(PaymentProcessingConsentMockData.getSubmissionRequest(),
				PaymentProcessingConsentMockData.setupCustomPlatformDetails(), "e58976e7-605c-48ac-aec5-0999343f6148");
	}
	
	public void preSubmissionProcessingWithOutPlatformResource() {
		PaymentConsentsPlatformResource platformResourceResponse = PaymentProcessingConsentMockData
				.setupConsentPlatformResource();
		platformResourceResponse.setStatus(PaymentStatusEnum.AUTHORISED.getStatusCode());
		Mockito.when(reqHeaderAtrributes.getToken()).thenReturn(PaymentProcessingConsentMockData.getToken());
		Mockito.when(paymentConsentsPlatformAdapter.retrievePaymentSetupPlatformResource(anyString()))
				.thenReturn(platformResourceResponse);
		Mockito.when(paymentsPlatformAdapter.getIdempotentPaymentsPlatformResource(anyLong(), anyString()))
				.thenReturn(PaymentProcessingConsentMockData.getSubmissionPlatformResource());
		Mockito.when(paymentsPlatformAdapter.getIdempotentPaymentsPlatformResource(anyLong(), anyString()))
				.thenReturn(null);
		Mockito.when(paymentsPlatformAdapter.retrievePaymentsResourceByConsentId(anyString())).thenReturn(null);
		paymentSubmissionTest.preSubmissionProcessing(PaymentProcessingConsentMockData.getSubmissionRequest(),
				PaymentProcessingConsentMockData.setupCustomPlatformDetails(), "e58976e7-605c-48ac-aec5-0999343f6148");
	}
	
	public void preSubmissionProcessingWithPlatformResource_Exception3() {
		CustomDPaymentsPOSTRequest request = new CustomDPaymentsPOSTRequest();
		OBWriteDataDomestic1 data = new OBWriteDataDomestic1();
		OBRisk1 risk = new OBRisk1();

		request.setData(data);
		request.setRisk(risk);
		
		PaymentsPlatformResource platformResource=new PaymentsPlatformResource();
		platformResource.setIdempotencyKey("idempotency");
		platformResource.setTppCID("53ZcZkjLM1sXLOAHkwG6DB");
		platformResource.setPaymentConsentId("e58976e7-605c-48ac-aec5-0999343f6148");
		platformResource.setProccessState(PaymentConstants.INCOMPLETE);
		platformResource.setPaymentType(PaymentTypeEnum.DOMESTIC_PAY.toString());
		
		PaymentConsentsPlatformResource platformResourceResponse = PaymentProcessingConsentMockData
				.setupConsentPlatformResource();
		platformResourceResponse.setStatus(PaymentStatusEnum.AUTHORISED.getStatusCode());
		Mockito.when(reqHeaderAtrributes.getToken()).thenReturn(PaymentProcessingConsentMockData.getToken());
		Mockito.when(paymentConsentsPlatformAdapter.retrievePaymentSetupPlatformResource(anyString()))
				.thenReturn(platformResourceResponse);
		Mockito.when(paymentsPlatformAdapter.getIdempotentPaymentsPlatformResource(anyLong(), anyString()))
				.thenReturn(PaymentProcessingConsentMockData.getSubmissionPlatformResource());
		Mockito.when(paymentsPlatformAdapter.getIdempotentPaymentsPlatformResource(anyLong(), anyString()))
				.thenReturn(null);
		Mockito.when(paymentsPlatformAdapter.retrievePaymentsResourceByConsentId(anyString())).thenReturn(null);
		paymentSubmissionTest.preSubmissionProcessing(PaymentProcessingConsentMockData.getSubmissionRequest(),
				PaymentProcessingConsentMockData.setupCustomPlatformDetails(), "e58976e7-605c-48ac-aec5-0999343f6148");
	}

	@Test(expected = PSD2Exception.class)
	public void preSubmissionProcessingExpiredPlatformResource() {
		PaymentConsentsPlatformResource platformResourceResponse = PaymentProcessingConsentMockData
				.setupConsentPlatformResource();
		platformResourceResponse.setStatus(PaymentStatusEnum.AUTHORISED.getStatusCode());
		DateTime currentTimeStamp = new DateTime();
		currentTimeStamp = currentTimeStamp.minusMonths(1);
		platformResourceResponse.setCreatedAt(currentTimeStamp.toString());
		Mockito.when(reqHeaderAtrributes.getToken()).thenReturn(PaymentProcessingConsentMockData.getToken());
		Mockito.when(paymentConsentsPlatformAdapter.retrievePaymentSetupPlatformResource(anyString()))
				.thenReturn(platformResourceResponse);
		Mockito.when(paymentsPlatformAdapter.getIdempotentPaymentsPlatformResource(anyLong(), anyString()))
				.thenReturn(PaymentProcessingConsentMockData.getSubmissionPlatformResource());
		Mockito.when(paymentsPlatformAdapter.getIdempotentPaymentsPlatformResource(anyLong(), anyString()))
				.thenReturn(null);
		Mockito.when(pispConsentAdapter.retrieveConsentByPaymentId(anyString(), any())).thenReturn(new PispConsent());
		Mockito.when(paymentsPlatformAdapter.retrievePaymentsResourceByConsentId(anyString())).thenReturn(null);
		paymentSubmissionTest.preSubmissionProcessing(PaymentProcessingConsentMockData.getSubmissionRequest(),
				PaymentProcessingConsentMockData.setupCustomPlatformDetails(), "e58976e7-605c-48ac-aec5-0999343f6148");
	}
	
	@Test(expected = PSD2Exception.class)
	public void preSubmissionProcessingExpiredPlatformResource_getSubmissionRequestCustomDSPayments() {
		PaymentConsentsPlatformResource platformResourceResponse = PaymentProcessingConsentMockData
				.setupConsentPlatformResource();
		platformResourceResponse.setStatus(PaymentStatusEnum.AUTHORISED.getStatusCode());
		DateTime currentTimeStamp = new DateTime();
		currentTimeStamp = currentTimeStamp.minusMonths(1);
		platformResourceResponse.setCreatedAt(currentTimeStamp.toString());
		Mockito.when(reqHeaderAtrributes.getToken()).thenReturn(PaymentProcessingConsentMockData.getToken());
		Mockito.when(paymentConsentsPlatformAdapter.retrievePaymentSetupPlatformResource(anyString()))
				.thenReturn(platformResourceResponse);
		Mockito.when(paymentsPlatformAdapter.getIdempotentPaymentsPlatformResource(anyLong(), anyString()))
				.thenReturn(PaymentProcessingConsentMockData.getSubmissionPlatformResource());
		Mockito.when(paymentsPlatformAdapter.getIdempotentPaymentsPlatformResource(anyLong(), anyString()))
				.thenReturn(null);
		Mockito.when(pispConsentAdapter.retrieveConsentByPaymentId(anyString(), any())).thenReturn(new PispConsent());
		Mockito.when(paymentsPlatformAdapter.retrievePaymentsResourceByConsentId(anyString())).thenReturn(null);
		paymentSubmissionTest.preSubmissionProcessing(PaymentProcessingConsentMockData.getSubmissionRequestCustomDSPayments(),
				PaymentProcessingConsentMockData.setupCustomPlatformDetails(), "e58976e7-605c-48ac-aec5-0999343f6148");
	}
	
	
	@Test(expected = PSD2Exception.class)
	public void preSubmissionProcessingExpiredPlatformResource_CustomDStandingOrderPOSTRequest() {
		PaymentConsentsPlatformResource platformResourceResponse = PaymentProcessingConsentMockData
				.setupConsentPlatformResource();
		platformResourceResponse.setStatus(PaymentStatusEnum.AUTHORISED.getStatusCode());
		DateTime currentTimeStamp = new DateTime();
		currentTimeStamp = currentTimeStamp.minusMonths(1);
		platformResourceResponse.setCreatedAt(currentTimeStamp.toString());
		Mockito.when(reqHeaderAtrributes.getToken()).thenReturn(PaymentProcessingConsentMockData.getToken());
		Mockito.when(paymentConsentsPlatformAdapter.retrievePaymentSetupPlatformResource(anyString()))
				.thenReturn(platformResourceResponse);
		Mockito.when(paymentsPlatformAdapter.getIdempotentPaymentsPlatformResource(anyLong(), anyString()))
				.thenReturn(PaymentProcessingConsentMockData.getSubmissionPlatformResource());
		Mockito.when(paymentsPlatformAdapter.getIdempotentPaymentsPlatformResource(anyLong(), anyString()))
				.thenReturn(null);
		Mockito.when(pispConsentAdapter.retrieveConsentByPaymentId(anyString(), any())).thenReturn(new PispConsent());
		Mockito.when(paymentsPlatformAdapter.retrievePaymentsResourceByConsentId(anyString())).thenReturn(null);
		paymentSubmissionTest.preSubmissionProcessing(PaymentProcessingConsentMockData.getSubmissionRequestCustomDStandingOrderPOSTRequest(),
				PaymentProcessingConsentMockData.setupCustomPlatformDetails(), "e58976e7-605c-48ac-aec5-0999343f6148");
	}
	
	@Test(expected = PSD2Exception.class)
	public void preSubmissionProcessingExpiredPlatformResource_CustomIPaymentsPOSTRequest() {
		PaymentConsentsPlatformResource platformResourceResponse = PaymentProcessingConsentMockData
				.setupConsentPlatformResource();
		platformResourceResponse.setStatus(PaymentStatusEnum.AUTHORISED.getStatusCode());
		DateTime currentTimeStamp = new DateTime();
		currentTimeStamp = currentTimeStamp.minusMonths(1);
		platformResourceResponse.setCreatedAt(currentTimeStamp.toString());
		Mockito.when(reqHeaderAtrributes.getToken()).thenReturn(PaymentProcessingConsentMockData.getToken());
		Mockito.when(paymentConsentsPlatformAdapter.retrievePaymentSetupPlatformResource(anyString()))
				.thenReturn(platformResourceResponse);
		Mockito.when(paymentsPlatformAdapter.getIdempotentPaymentsPlatformResource(anyLong(), anyString()))
				.thenReturn(PaymentProcessingConsentMockData.getSubmissionPlatformResource());
		Mockito.when(paymentsPlatformAdapter.getIdempotentPaymentsPlatformResource(anyLong(), anyString()))
				.thenReturn(null);
		Mockito.when(pispConsentAdapter.retrieveConsentByPaymentId(anyString(), any())).thenReturn(new PispConsent());
		Mockito.when(paymentsPlatformAdapter.retrievePaymentsResourceByConsentId(anyString())).thenReturn(null);
		paymentSubmissionTest.preSubmissionProcessing(PaymentProcessingConsentMockData.getSubmissionRequestCustomIPaymentsPOSTRequest(),
				PaymentProcessingConsentMockData.setupCustomPlatformDetails(), "e58976e7-605c-48ac-aec5-0999343f6148");
	}

	@Test(expected = PSD2Exception.class)
	public void preSubmissionProcessingExpiredPlatformResource_CustomISPaymentsPOSTRequest() {
		PaymentConsentsPlatformResource platformResourceResponse = PaymentProcessingConsentMockData
				.setupConsentPlatformResource();
		platformResourceResponse.setStatus(PaymentStatusEnum.AUTHORISED.getStatusCode());
		DateTime currentTimeStamp = new DateTime();
		currentTimeStamp = currentTimeStamp.minusMonths(1);
		platformResourceResponse.setCreatedAt(currentTimeStamp.toString());
		Mockito.when(reqHeaderAtrributes.getToken()).thenReturn(PaymentProcessingConsentMockData.getToken());
		Mockito.when(paymentConsentsPlatformAdapter.retrievePaymentSetupPlatformResource(anyString()))
				.thenReturn(platformResourceResponse);
		Mockito.when(paymentsPlatformAdapter.getIdempotentPaymentsPlatformResource(anyLong(), anyString()))
				.thenReturn(PaymentProcessingConsentMockData.getSubmissionPlatformResource());
		Mockito.when(paymentsPlatformAdapter.getIdempotentPaymentsPlatformResource(anyLong(), anyString()))
				.thenReturn(null);
		Mockito.when(pispConsentAdapter.retrieveConsentByPaymentId(anyString(), any())).thenReturn(new PispConsent());
		Mockito.when(paymentsPlatformAdapter.retrievePaymentsResourceByConsentId(anyString())).thenReturn(null);
		paymentSubmissionTest.preSubmissionProcessing(PaymentProcessingConsentMockData.getSubmissionRequestCustomISPaymentsPOSTRequest(),
				PaymentProcessingConsentMockData.setupCustomPlatformDetails(), "e58976e7-605c-48ac-aec5-0999343f6148");
	}
	
	@Test(expected = PSD2Exception.class)
	public void preSubmissionProcessingExpiredPlatformResource_CustomFilePaymentsPOSTRequest() {
		PaymentConsentsPlatformResource platformResourceResponse = PaymentProcessingConsentMockData
				.setupConsentPlatformResource();
		platformResourceResponse.setStatus(PaymentStatusEnum.AUTHORISED.getStatusCode());
		DateTime currentTimeStamp = new DateTime();
		currentTimeStamp = currentTimeStamp.minusMonths(1);
		platformResourceResponse.setCreatedAt(currentTimeStamp.toString());
		Mockito.when(reqHeaderAtrributes.getToken()).thenReturn(PaymentProcessingConsentMockData.getToken());
		Mockito.when(paymentConsentsPlatformAdapter.retrievePaymentSetupPlatformResource(anyString()))
				.thenReturn(platformResourceResponse);
		Mockito.when(paymentsPlatformAdapter.getIdempotentPaymentsPlatformResource(anyLong(), anyString()))
				.thenReturn(PaymentProcessingConsentMockData.getSubmissionPlatformResource());
		Mockito.when(paymentsPlatformAdapter.getIdempotentPaymentsPlatformResource(anyLong(), anyString()))
				.thenReturn(null);
		Mockito.when(pispConsentAdapter.retrieveConsentByPaymentId(anyString(), any())).thenReturn(new PispConsent());
		Mockito.when(paymentsPlatformAdapter.retrievePaymentsResourceByConsentId(anyString())).thenReturn(null);
		paymentSubmissionTest.preSubmissionProcessing(PaymentProcessingConsentMockData.getSubmissionRequestCustomFilePaymentsPOSTRequest(),
				PaymentProcessingConsentMockData.setupCustomPlatformDetails(), "e58976e7-605c-48ac-aec5-0999343f6148");
	}
	
	
	
	@Test
	public void createInitialPaymentsPlatformResource() {
		Mockito.when(paymentsPlatformAdapter.createInitialPaymentsPlatformResource(any(), any()))
				.thenReturn(PaymentProcessingConsentMockData.getSubmissionPlatformResource());
		paymentSubmissionTest.createInitialPaymentsPlatformResource("e58976e7-605c-48ac-aec5-0999343f6148",
				PaymentConstants.CMA_THIRD_VERSION, PaymentTypeEnum.DOMESTIC_PAY);
	}

	@Test(expected = PSD2Exception.class)
	public void prePaymentProcessGETFlows() {
		Mockito.when(paymentsPlatformAdapter.retrievePaymentsPlatformResource(any(), anyString()))
				.thenReturn(PaymentProcessingConsentMockData.getSubmissionPlatformResource());
		Mockito.when(reqHeaderAtrributes.getTppCID()).thenReturn("53ZcZkjLM1sXLOAHkwG6DB");
		paymentSubmissionTest.prePaymentProcessGETFlows("e58976e7-605c-48ac-aec5-0999343f6148", PaymentTypeEnum.INTERNATIONAL_PAY);
	}

	@Test(expected = PSD2Exception.class)
	public void prePaymentProcessWitoutPostFlow() {
		Mockito.when(paymentsPlatformAdapter.retrievePaymentsPlatformResource(any(), anyString())).thenReturn(null);
		Mockito.when(reqHeaderAtrributes.getTppCID()).thenReturn("53ZcZkjLM1sXLOAHkwG6DB");
		paymentSubmissionTest.prePaymentProcessGETFlows("e58976e7-605c-48ac-aec5-0999343f6148", PaymentTypeEnum.INTERNATIONAL_PAY);
	}

	@Test(expected = PSD2Exception.class)
	public void prePaymentProcessWithoutPostNewFlow() {
		Mockito.when(paymentsPlatformAdapter.retrievePaymentsPlatformResource(any(), anyString()))
				.thenReturn(PaymentProcessingConsentMockData.getSubmissionPlatformResource());
		Mockito.when(reqHeaderAtrributes.getTppCID()).thenReturn("53ZcZkjLM1sLOAHkwG6DB");
		paymentSubmissionTest.prePaymentProcessGETFlows("e58976e7-605c-48ac-aec5-0999343f6148", PaymentTypeEnum.INTERNATIONAL_PAY);
	}

	@Test
	public void postPaymentProcessCompletedFlows() {
		Map<String, Object> paymentsPlatformResourceMap = new HashMap<>();
		PaymentConsentsPlatformResource platformResourceResponse = PaymentProcessingConsentMockData
				.setupConsentPlatformResource();
		PaymentsPlatformResource submissionPlatformResource = PaymentProcessingConsentMockData
				.getSubmissionPlatformResource();
		submissionPlatformResource.setProccessState(PaymentConstants.COMPLETED);
		paymentsPlatformResourceMap.put(PaymentConstants.SUBMISSION, submissionPlatformResource);
		paymentsPlatformResourceMap.put(PaymentConstants.CONSENT, platformResourceResponse);
		Mockito.when(transformer.paymentSubmissionResponseTransformer(any(), any(), any()))
				.thenReturn(PaymentProcessingConsentMockData.getSubmissionResponse());
		paymentSubmissionTest.postPaymentProcessFlows(paymentsPlatformResourceMap,
				PaymentProcessingConsentMockData.getSubmissionResponse(), ProcessExecutionStatusEnum.PASS,
				"e58976e7-605c-48ac-aec5-0999343f6148", OBExternalStatus2Code.AUTHORISED, HttpMethod.POST.toString());
	}

	@Test(expected = PSD2Exception.class)
	public void postPaymentProcessCompletedNullFlows() {
		Map<String, Object> paymentsPlatformResourceMap = new HashMap<>();
		PaymentConsentsPlatformResource platformResourceResponse = PaymentProcessingConsentMockData
				.setupConsentPlatformResource();
		PaymentsPlatformResource submissionPlatformResource = PaymentProcessingConsentMockData
				.getSubmissionPlatformResource();
		submissionPlatformResource.setProccessState(PaymentConstants.INCOMPLETE);
		paymentsPlatformResourceMap.put(PaymentConstants.SUBMISSION, submissionPlatformResource);
		paymentsPlatformResourceMap.put(PaymentConstants.CONSENT, platformResourceResponse);
		Mockito.when(transformer.paymentSubmissionResponseTransformer(any(), any(), any()))
				.thenReturn(PaymentProcessingConsentMockData.getSubmissionResponse());
		paymentSubmissionTest.postPaymentProcessFlows(paymentsPlatformResourceMap,
				PaymentProcessingConsentMockData.getSubmissionResponse(), null, "e58976e7-605c-48ac-aec5-0999343f6148",
				OBExternalStatus2Code.AUTHORISED, HttpMethod.POST.toString());
	}

	@Test
	public void postPaymentProcessCompletedFailFlows() {
		Map<String, Object> paymentsPlatformResourceMap = new HashMap<>();
		PaymentConsentsPlatformResource platformResourceResponse = PaymentProcessingConsentMockData
				.setupConsentPlatformResource();
		PaymentsPlatformResource submissionPlatformResource = PaymentProcessingConsentMockData
				.getSubmissionPlatformResource();
		submissionPlatformResource.setProccessState(PaymentConstants.COMPLETED);
		paymentsPlatformResourceMap.put(PaymentConstants.SUBMISSION, submissionPlatformResource);
		paymentsPlatformResourceMap.put(PaymentConstants.CONSENT, platformResourceResponse);
		Mockito.when(reqHeaderAtrributes.getToken()).thenReturn(PaymentProcessingConsentMockData.getToken());
		
		Mockito.when(transformer.paymentSubmissionResponseTransformer(any(), any(), any()))
				.thenReturn(PaymentProcessingConsentMockData.getSubmissionResponse());
		paymentSubmissionTest.postPaymentProcessFlows(paymentsPlatformResourceMap,
				PaymentProcessingConsentMockData.getSubmissionResponse(), ProcessExecutionStatusEnum.FAIL,
				"e58976e7-605c-48ac-aec5-0999343f6148", OBExternalStatus2Code.AUTHORISED, HttpMethod.POST.toString());
	}

	@Test
	public void postFilePaymentProcessCompletedFailFlows() {
		Map<String, Object> paymentsPlatformResourceMap = new HashMap<>();
		PaymentConsentsPlatformResource platformResourceResponse = PaymentProcessingConsentMockData
				.setupConsentPlatformResource();
		PaymentsPlatformResource submissionPlatformResource = PaymentProcessingConsentMockData
				.getSubmissionPlatformResource();
		submissionPlatformResource.setPaymentType(PaymentTypeEnum.FILE_PAY.getPaymentType());
		submissionPlatformResource.setProccessState(PaymentConstants.COMPLETED);
		paymentsPlatformResourceMap.put(PaymentConstants.SUBMISSION, submissionPlatformResource);
		paymentsPlatformResourceMap.put(PaymentConstants.CONSENT, platformResourceResponse);
		Mockito.when(transformer.paymentSubmissionResponseTransformer(any(), any(), any()))
				.thenReturn(PaymentProcessingConsentMockData.getSubmissionResponse());
		paymentSubmissionTest.postPaymentProcessFlows(paymentsPlatformResourceMap,
				PaymentProcessingConsentMockData.getSubmissionResponse(), ProcessExecutionStatusEnum.FAIL,
				"e58976e7-605c-48ac-aec5-0999343f6148", OBExternalStatus2Code.AUTHORISED, HttpMethod.POST.toString());
	}

	@Test(expected = PSD2Exception.class)
	public void postPaymentProcessCompletedFailWithoutSubmissionIdFlows() {
		Map<String, Object> paymentsPlatformResourceMap = new HashMap<>();
		PaymentConsentsPlatformResource platformResourceResponse = PaymentProcessingConsentMockData
				.setupConsentPlatformResource();
		PaymentsPlatformResource submissionPlatformResource = PaymentProcessingConsentMockData
				.getSubmissionPlatformResource();
		submissionPlatformResource.setProccessState(PaymentConstants.INCOMPLETE);
		paymentsPlatformResourceMap.put(PaymentConstants.SUBMISSION, submissionPlatformResource);
		paymentsPlatformResourceMap.put(PaymentConstants.CONSENT, platformResourceResponse);
		Mockito.when(transformer.paymentSubmissionResponseTransformer(any(), any(), any()))
				.thenReturn(PaymentProcessingConsentMockData.getSubmissionResponse());
		paymentSubmissionTest.postPaymentProcessFlows(paymentsPlatformResourceMap,
				PaymentProcessingConsentMockData.getSubmissionResponse(), ProcessExecutionStatusEnum.FAIL, null,
				OBExternalStatus2Code.AUTHORISED, HttpMethod.POST.toString());
	}

	@Test
	public void postPaymentProcessCompletedPassFlows() {
		Map<String, Object> paymentsPlatformResourceMap = new HashMap<>();
		PaymentConsentsPlatformResource platformResourceResponse = PaymentProcessingConsentMockData
				.setupConsentPlatformResource();
		PaymentsPlatformResource submissionPlatformResource = PaymentProcessingConsentMockData
				.getSubmissionPlatformResource();
		submissionPlatformResource.setProccessState(PaymentConstants.COMPLETED);
		paymentsPlatformResourceMap.put(PaymentConstants.SUBMISSION, submissionPlatformResource);
		paymentsPlatformResourceMap.put(PaymentConstants.CONSENT, platformResourceResponse);
		Mockito.when(transformer.paymentSubmissionResponseTransformer(any(), any(), any()))
				.thenReturn(PaymentProcessingConsentMockData.getSubmissionResponse());
		paymentSubmissionTest.postPaymentProcessFlows(paymentsPlatformResourceMap,
				PaymentProcessingConsentMockData.getSubmissionResponse(), ProcessExecutionStatusEnum.PASS,
				"e58976e7-605c-48ac-aec5-0999343f6148", OBExternalStatus2Code.AUTHORISED, HttpMethod.POST.toString());
	}
	
	@Test
	public void postPaymentProcessCompletedPassFlowspass1() {
		
		PaymentConsentsPlatformResource platformResource = new PaymentConsentsPlatformResource();
		platformResource.setCreatedAt(PispUtilities.getCurrentDateInISOFormat());
		platformResource.setIdempotencyKey("idempotency");
		platformResource.setIdempotencyRequest(String.valueOf(Boolean.TRUE));
		platformResource.setPaymentConsentId("e58976e7-605c-48ac-aec5-0999343f6148");
		platformResource.setPaymentType(PaymentTypeEnum.DOMESTIC_PAY.getPaymentType());
		platformResource.setStatus("AwaitingAuthorisation");
		platformResource.setEndToEndIdentification("DEMO USER");
		platformResource.setInstructionIdentification("International");
		platformResource.setStatusUpdateDateTime(PispUtilities.getCurrentDateInISOFormat());
		platformResource.setTppCID("53ZcZkjLM1sXLOAHkwG6DB");
		platformResource.setTppLegalEntityName("BankOfDemo");
		
		Map<String, Object> paymentsPlatformResourceMap = new HashMap<>();
		PaymentConsentsPlatformResource platformResourceResponse =platformResource;
		PaymentsPlatformResource submissionPlatformResource = PaymentProcessingConsentMockData
				.getSubmissionPlatformResource();
		submissionPlatformResource.setProccessState(PaymentConstants.INCOMPLETE);
		Mockito.when(reqHeaderAtrributes.getToken()).thenReturn(PaymentProcessingConsentMockData.getToken1());
		paymentsPlatformResourceMap.put(PaymentConstants.SUBMISSION, submissionPlatformResource);
		paymentsPlatformResourceMap.put(PaymentConstants.CONSENT, platformResourceResponse);
		Mockito.when(transformer.paymentSubmissionResponseTransformer(any(), any(), any()))
				.thenReturn(PaymentProcessingConsentMockData.getSubmissionResponse());
		paymentSubmissionTest.postPaymentProcessFlows(paymentsPlatformResourceMap,
				PaymentProcessingConsentMockData.getSubmissionResponse(), ProcessExecutionStatusEnum.PASS,
				"e58976e7-605c-48ac-aec5-0999343f6148", OBExternalStatus2Code.AUTHORISED, HttpMethod.POST.toString());
	}
	
	@Test
	public void postPaymentProcessCompletedPassFlowspass1_AUTHORISED_FILE_PAY() {
		
		PaymentConsentsPlatformResource platformResource = new PaymentConsentsPlatformResource();
		platformResource.setCreatedAt(PispUtilities.getCurrentDateInISOFormat());
		platformResource.setIdempotencyKey("idempotency");
		platformResource.setIdempotencyRequest(String.valueOf(Boolean.TRUE));
		platformResource.setPaymentConsentId("e58976e7-605c-48ac-aec5-0999343f6148");
		platformResource.setPaymentType(PaymentTypeEnum.FILE_PAY.getPaymentType());
		platformResource.setStatus("AwaitingAuthorisation");
		platformResource.setEndToEndIdentification("DEMO USER");
		platformResource.setInstructionIdentification("International");
		platformResource.setStatusUpdateDateTime(PispUtilities.getCurrentDateInISOFormat());
		platformResource.setTppCID("53ZcZkjLM1sXLOAHkwG6DB");
		platformResource.setTppLegalEntityName("BankOfDemo");
		
		Map<String, Object> paymentsPlatformResourceMap = new HashMap<>();
		PaymentConsentsPlatformResource platformResourceResponse =platformResource;
		PaymentsPlatformResource submissionPlatformResource = PaymentProcessingConsentMockData
				.getSubmissionPlatformResource();
		submissionPlatformResource.setProccessState(PaymentConstants.INCOMPLETE);
		Mockito.when(reqHeaderAtrributes.getToken()).thenReturn(PaymentProcessingConsentMockData.getToken1());
		paymentsPlatformResourceMap.put(PaymentConstants.SUBMISSION, submissionPlatformResource);
		paymentsPlatformResourceMap.put(PaymentConstants.CONSENT, platformResourceResponse);
		Mockito.when(transformer.paymentSubmissionResponseTransformer(any(), any(), any()))
				.thenReturn(PaymentProcessingConsentMockData.getSubmissionResponse());
		paymentSubmissionTest.postPaymentProcessFlows(paymentsPlatformResourceMap,
				PaymentProcessingConsentMockData.getSubmissionResponse(), ProcessExecutionStatusEnum.PASS,
				"e58976e7-605c-48ac-aec5-0999343f6148", OBExternalStatus2Code.AUTHORISED, HttpMethod.POST.toString());
	}
	
	@Test
	public void postPaymentProcessCompletedPassFlowspass1_AUTHORISED_DOMESTIC_ST_ORD() {
		
		PaymentConsentsPlatformResource platformResource = new PaymentConsentsPlatformResource();
		platformResource.setCreatedAt(PispUtilities.getCurrentDateInISOFormat());
		platformResource.setIdempotencyKey("idempotency");
		platformResource.setIdempotencyRequest(String.valueOf(Boolean.TRUE));
		platformResource.setPaymentConsentId("e58976e7-605c-48ac-aec5-0999343f6148");
		platformResource.setPaymentType(PaymentTypeEnum.DOMESTIC_ST_ORD.getPaymentType());
		platformResource.setStatus("AwaitingAuthorisation");
		platformResource.setEndToEndIdentification("DEMO USER");
		platformResource.setInstructionIdentification("International");
		platformResource.setStatusUpdateDateTime(PispUtilities.getCurrentDateInISOFormat());
		platformResource.setTppCID("53ZcZkjLM1sXLOAHkwG6DB");
		platformResource.setTppLegalEntityName("BankOfDemo");
		
		Map<String, Object> paymentsPlatformResourceMap = new HashMap<>();
		PaymentConsentsPlatformResource platformResourceResponse =platformResource;
		PaymentsPlatformResource submissionPlatformResource = PaymentProcessingConsentMockData
				.getSubmissionPlatformResource();
		submissionPlatformResource.setProccessState(PaymentConstants.INCOMPLETE);
		Mockito.when(reqHeaderAtrributes.getToken()).thenReturn(PaymentProcessingConsentMockData.getToken1());
		paymentsPlatformResourceMap.put(PaymentConstants.SUBMISSION, submissionPlatformResource);
		paymentsPlatformResourceMap.put(PaymentConstants.CONSENT, platformResourceResponse);
		Mockito.when(transformer.paymentSubmissionResponseTransformer(any(), any(), any()))
				.thenReturn(PaymentProcessingConsentMockData.getSubmissionResponse());
		paymentSubmissionTest.postPaymentProcessFlows(paymentsPlatformResourceMap,
				PaymentProcessingConsentMockData.getSubmissionResponse(), ProcessExecutionStatusEnum.PASS,
				"e58976e7-605c-48ac-aec5-0999343f6148", OBExternalStatus2Code.AUTHORISED, HttpMethod.POST.toString());
	}
	
	@Test
	public void postPaymentProcessCompletedPassFlowspass1_AUTHORISED_DOMESTIC_SCH_PAY() {
		
		PaymentConsentsPlatformResource platformResource = new PaymentConsentsPlatformResource();
		platformResource.setCreatedAt(PispUtilities.getCurrentDateInISOFormat());
		platformResource.setIdempotencyKey("idempotency");
		platformResource.setIdempotencyRequest(String.valueOf(Boolean.TRUE));
		platformResource.setPaymentConsentId("e58976e7-605c-48ac-aec5-0999343f6148");
		platformResource.setPaymentType(PaymentTypeEnum.DOMESTIC_SCH_PAY.getPaymentType());
		platformResource.setStatus("AwaitingAuthorisation");
		platformResource.setEndToEndIdentification("DEMO USER");
		platformResource.setInstructionIdentification("International");
		platformResource.setStatusUpdateDateTime(PispUtilities.getCurrentDateInISOFormat());
		platformResource.setTppCID("53ZcZkjLM1sXLOAHkwG6DB");
		platformResource.setTppLegalEntityName("BankOfDemo");
		
		Map<String, Object> paymentsPlatformResourceMap = new HashMap<>();
		PaymentConsentsPlatformResource platformResourceResponse =platformResource;
		PaymentsPlatformResource submissionPlatformResource = PaymentProcessingConsentMockData
				.getSubmissionPlatformResource();
		submissionPlatformResource.setProccessState(PaymentConstants.INCOMPLETE);
		Mockito.when(reqHeaderAtrributes.getToken()).thenReturn(PaymentProcessingConsentMockData.getToken1());
		paymentsPlatformResourceMap.put(PaymentConstants.SUBMISSION, submissionPlatformResource);
		paymentsPlatformResourceMap.put(PaymentConstants.CONSENT, platformResourceResponse);
		Mockito.when(transformer.paymentSubmissionResponseTransformer(any(), any(), any()))
				.thenReturn(PaymentProcessingConsentMockData.getSubmissionResponse());
		paymentSubmissionTest.postPaymentProcessFlows(paymentsPlatformResourceMap,
				PaymentProcessingConsentMockData.getSubmissionResponse(), ProcessExecutionStatusEnum.PASS,
				"e58976e7-605c-48ac-aec5-0999343f6148", OBExternalStatus2Code.AUTHORISED, HttpMethod.POST.toString());
	}
	
	
	@Test
	public void postPaymentProcessCompletedPassFlowspass1_AUTHORISED_INTERNATIONAL_SCH_PAY() {
		
		PaymentConsentsPlatformResource platformResource = new PaymentConsentsPlatformResource();
		platformResource.setCreatedAt(PispUtilities.getCurrentDateInISOFormat());
		platformResource.setIdempotencyKey("idempotency");
		platformResource.setIdempotencyRequest(String.valueOf(Boolean.TRUE));
		platformResource.setPaymentConsentId("e58976e7-605c-48ac-aec5-0999343f6148");
		platformResource.setPaymentType(PaymentTypeEnum.INTERNATIONAL_SCH_PAY.getPaymentType());
		platformResource.setStatus("AwaitingAuthorisation");
		platformResource.setEndToEndIdentification("DEMO USER");
		platformResource.setInstructionIdentification("International");
		platformResource.setStatusUpdateDateTime(PispUtilities.getCurrentDateInISOFormat());
		platformResource.setTppCID("53ZcZkjLM1sXLOAHkwG6DB");
		platformResource.setTppLegalEntityName("BankOfDemo");
		
		Map<String, Object> paymentsPlatformResourceMap = new HashMap<>();
		PaymentConsentsPlatformResource platformResourceResponse =platformResource;
		PaymentsPlatformResource submissionPlatformResource = PaymentProcessingConsentMockData
				.getSubmissionPlatformResource();
		submissionPlatformResource.setProccessState(PaymentConstants.INCOMPLETE);
		Mockito.when(reqHeaderAtrributes.getToken()).thenReturn(PaymentProcessingConsentMockData.getToken1());
		paymentsPlatformResourceMap.put(PaymentConstants.SUBMISSION, submissionPlatformResource);
		paymentsPlatformResourceMap.put(PaymentConstants.CONSENT, platformResourceResponse);
		Mockito.when(transformer.paymentSubmissionResponseTransformer(any(), any(), any()))
				.thenReturn(PaymentProcessingConsentMockData.getSubmissionResponse());
		paymentSubmissionTest.postPaymentProcessFlows(paymentsPlatformResourceMap,
				PaymentProcessingConsentMockData.getSubmissionResponse(), ProcessExecutionStatusEnum.PASS,
				"e58976e7-605c-48ac-aec5-0999343f6148", OBExternalStatus2Code.AUTHORISED, HttpMethod.POST.toString());
	}

	@Test
	public void postPaymentProcessCompletedPassFlowspass1_AWAITINGFURTHERAUTHORISATION_FILE_PAY() {
		
		PaymentConsentsPlatformResource platformResource = new PaymentConsentsPlatformResource();
		platformResource.setCreatedAt(PispUtilities.getCurrentDateInISOFormat());
		platformResource.setIdempotencyKey("idempotency");
		platformResource.setIdempotencyRequest(String.valueOf(Boolean.TRUE));
		platformResource.setPaymentConsentId("e58976e7-605c-48ac-aec5-0999343f6148");
		platformResource.setPaymentType(PaymentTypeEnum.FILE_PAY.getPaymentType());
		platformResource.setStatus("AwaitingAuthorisation");
		platformResource.setEndToEndIdentification("DEMO USER");
		platformResource.setInstructionIdentification("International");
		platformResource.setStatusUpdateDateTime(PispUtilities.getCurrentDateInISOFormat());
		platformResource.setTppCID("53ZcZkjLM1sXLOAHkwG6DB");
		platformResource.setTppLegalEntityName("BankOfDemo");
		
		Map<String, Object> paymentsPlatformResourceMap = new HashMap<>();
		PaymentConsentsPlatformResource platformResourceResponse = platformResource;
		PaymentsPlatformResource submissionPlatformResource = PaymentProcessingConsentMockData
				.getSubmissionPlatformResource();
		submissionPlatformResource.setProccessState(PaymentConstants.INCOMPLETE);
		Mockito.when(reqHeaderAtrributes.getToken()).thenReturn(PaymentProcessingConsentMockData.getToken1());		
		paymentsPlatformResourceMap.put(PaymentConstants.SUBMISSION, submissionPlatformResource);
		paymentsPlatformResourceMap.put(PaymentConstants.CONSENT, platformResourceResponse);
		Mockito.when(transformer.paymentSubmissionResponseTransformer(any(), any(), any()))
				.thenReturn(PaymentProcessingConsentMockData.getSubmissionResponse());
		paymentSubmissionTest.postPaymentProcessFlows(paymentsPlatformResourceMap,
				PaymentProcessingConsentMockData.getSubmissionResponse(), ProcessExecutionStatusEnum.PASS,
				"e58976e7-605c-48ac-aec5-0999343f6148", OBExternalStatus2Code.AWAITINGFURTHERAUTHORISATION, HttpMethod.POST.toString());
	}
	
	
	@Test
	public void postPaymentProcessCompletedPassFlowspass1_AWAITINGFURTHERAUTHORISATION_DOMESTIC_ST_ORD() {
		
		PaymentConsentsPlatformResource platformResource = new PaymentConsentsPlatformResource();
		platformResource.setCreatedAt(PispUtilities.getCurrentDateInISOFormat());
		platformResource.setIdempotencyKey("idempotency");
		platformResource.setIdempotencyRequest(String.valueOf(Boolean.TRUE));
		platformResource.setPaymentConsentId("e58976e7-605c-48ac-aec5-0999343f6148");
		platformResource.setPaymentType(PaymentTypeEnum.DOMESTIC_ST_ORD.getPaymentType());
		platformResource.setStatus("AwaitingAuthorisation");
		platformResource.setEndToEndIdentification("DEMO USER");
		platformResource.setInstructionIdentification("International");
		platformResource.setStatusUpdateDateTime(PispUtilities.getCurrentDateInISOFormat());
		platformResource.setTppCID("53ZcZkjLM1sXLOAHkwG6DB");
		platformResource.setTppLegalEntityName("BankOfDemo");
		
		Map<String, Object> paymentsPlatformResourceMap = new HashMap<>();
		PaymentConsentsPlatformResource platformResourceResponse = platformResource;
		PaymentsPlatformResource submissionPlatformResource = PaymentProcessingConsentMockData
				.getSubmissionPlatformResource();
		submissionPlatformResource.setProccessState(PaymentConstants.INCOMPLETE);
		Mockito.when(reqHeaderAtrributes.getToken()).thenReturn(PaymentProcessingConsentMockData.getToken1());		
		paymentsPlatformResourceMap.put(PaymentConstants.SUBMISSION, submissionPlatformResource);
		paymentsPlatformResourceMap.put(PaymentConstants.CONSENT, platformResourceResponse);
		Mockito.when(transformer.paymentSubmissionResponseTransformer(any(), any(), any()))
				.thenReturn(PaymentProcessingConsentMockData.getSubmissionResponse());
		paymentSubmissionTest.postPaymentProcessFlows(paymentsPlatformResourceMap,
				PaymentProcessingConsentMockData.getSubmissionResponse(), ProcessExecutionStatusEnum.PASS,
				"e58976e7-605c-48ac-aec5-0999343f6148", OBExternalStatus2Code.AWAITINGFURTHERAUTHORISATION, HttpMethod.POST.toString());
	}

	
	@Test
	public void postPaymentProcessCompletedPassFlowspass1_AWAITINGFURTHERAUTHORISATION_INTERNATIONAL_SCH_PAY() {
		
		PaymentConsentsPlatformResource platformResource = new PaymentConsentsPlatformResource();
		platformResource.setCreatedAt(PispUtilities.getCurrentDateInISOFormat());
		platformResource.setIdempotencyKey("idempotency");
		platformResource.setIdempotencyRequest(String.valueOf(Boolean.TRUE));
		platformResource.setPaymentConsentId("e58976e7-605c-48ac-aec5-0999343f6148");
		platformResource.setPaymentType(PaymentTypeEnum.INTERNATIONAL_SCH_PAY.getPaymentType());
		platformResource.setStatus("AwaitingAuthorisation");
		platformResource.setEndToEndIdentification("DEMO USER");
		platformResource.setInstructionIdentification("International");
		platformResource.setStatusUpdateDateTime(PispUtilities.getCurrentDateInISOFormat());
		platformResource.setTppCID("53ZcZkjLM1sXLOAHkwG6DB");
		platformResource.setTppLegalEntityName("BankOfDemo");
		
		Map<String, Object> paymentsPlatformResourceMap = new HashMap<>();
		PaymentConsentsPlatformResource platformResourceResponse = platformResource;
		PaymentsPlatformResource submissionPlatformResource = PaymentProcessingConsentMockData
				.getSubmissionPlatformResource();
		submissionPlatformResource.setProccessState(PaymentConstants.INCOMPLETE);
		Mockito.when(reqHeaderAtrributes.getToken()).thenReturn(PaymentProcessingConsentMockData.getToken1());		
		paymentsPlatformResourceMap.put(PaymentConstants.SUBMISSION, submissionPlatformResource);
		paymentsPlatformResourceMap.put(PaymentConstants.CONSENT, platformResourceResponse);
		Mockito.when(transformer.paymentSubmissionResponseTransformer(any(), any(), any()))
				.thenReturn(PaymentProcessingConsentMockData.getSubmissionResponse());
		paymentSubmissionTest.postPaymentProcessFlows(paymentsPlatformResourceMap,
				PaymentProcessingConsentMockData.getSubmissionResponse(), ProcessExecutionStatusEnum.PASS,
				"e58976e7-605c-48ac-aec5-0999343f6148", OBExternalStatus2Code.AWAITINGFURTHERAUTHORISATION, HttpMethod.POST.toString());
	}
	
	
	@Test
	public void postPaymentProcessCompletedPassFlowspass1_AWAITINGFURTHERAUTHORISATION_DOMESTIC_SCH_PAY() {
		
		PaymentConsentsPlatformResource platformResource = new PaymentConsentsPlatformResource();
		platformResource.setCreatedAt(PispUtilities.getCurrentDateInISOFormat());
		platformResource.setIdempotencyKey("idempotency");
		platformResource.setIdempotencyRequest(String.valueOf(Boolean.TRUE));
		platformResource.setPaymentConsentId("e58976e7-605c-48ac-aec5-0999343f6148");
		platformResource.setPaymentType(PaymentTypeEnum.DOMESTIC_SCH_PAY.getPaymentType());
		platformResource.setStatus("AwaitingAuthorisation");
		platformResource.setEndToEndIdentification("DEMO USER");
		platformResource.setInstructionIdentification("International");
		platformResource.setStatusUpdateDateTime(PispUtilities.getCurrentDateInISOFormat());
		platformResource.setTppCID("53ZcZkjLM1sXLOAHkwG6DB");
		platformResource.setTppLegalEntityName("BankOfDemo");
		
		Map<String, Object> paymentsPlatformResourceMap = new HashMap<>();
		PaymentConsentsPlatformResource platformResourceResponse = platformResource;
		PaymentsPlatformResource submissionPlatformResource = PaymentProcessingConsentMockData
				.getSubmissionPlatformResource();
		submissionPlatformResource.setProccessState(PaymentConstants.INCOMPLETE);
		Mockito.when(reqHeaderAtrributes.getToken()).thenReturn(PaymentProcessingConsentMockData.getToken1());		
		paymentsPlatformResourceMap.put(PaymentConstants.SUBMISSION, submissionPlatformResource);
		paymentsPlatformResourceMap.put(PaymentConstants.CONSENT, platformResourceResponse);
		Mockito.when(transformer.paymentSubmissionResponseTransformer(any(), any(), any()))
				.thenReturn(PaymentProcessingConsentMockData.getSubmissionResponse());
		paymentSubmissionTest.postPaymentProcessFlows(paymentsPlatformResourceMap,
				PaymentProcessingConsentMockData.getSubmissionResponse(), ProcessExecutionStatusEnum.PASS,
				"e58976e7-605c-48ac-aec5-0999343f6148", OBExternalStatus2Code.AWAITINGFURTHERAUTHORISATION, HttpMethod.POST.toString());
	}
	
	

	@Test
	public void postPaymentProcessCompletedPassFlowspass1_REJECTED_FILE_PAY() {
		
		PaymentConsentsPlatformResource platformResource = new PaymentConsentsPlatformResource();
		platformResource.setCreatedAt(PispUtilities.getCurrentDateInISOFormat());
		platformResource.setIdempotencyKey("idempotency");
		platformResource.setIdempotencyRequest(String.valueOf(Boolean.TRUE));
		platformResource.setPaymentConsentId("e58976e7-605c-48ac-aec5-0999343f6148");
		platformResource.setPaymentType(PaymentTypeEnum.FILE_PAY.getPaymentType());
		platformResource.setStatus("AwaitingAuthorisation");
		platformResource.setEndToEndIdentification("DEMO USER");
		platformResource.setInstructionIdentification("International");
		platformResource.setStatusUpdateDateTime(PispUtilities.getCurrentDateInISOFormat());
		platformResource.setTppCID("53ZcZkjLM1sXLOAHkwG6DB");
		platformResource.setTppLegalEntityName("BankOfDemo");
		Map<String, Object> paymentsPlatformResourceMap = new HashMap<>();
		PaymentConsentsPlatformResource platformResourceResponse = PaymentProcessingConsentMockData
				.setupConsentPlatformResource();
		PaymentsPlatformResource submissionPlatformResource = PaymentProcessingConsentMockData
				.getSubmissionPlatformResource();
		submissionPlatformResource.setProccessState(PaymentConstants.INCOMPLETE);
		Mockito.when(reqHeaderAtrributes.getToken()).thenReturn(PaymentProcessingConsentMockData.getToken1());	
		paymentsPlatformResourceMap.put(PaymentConstants.SUBMISSION, submissionPlatformResource);
		paymentsPlatformResourceMap.put(PaymentConstants.CONSENT, platformResourceResponse);
		Mockito.when(transformer.paymentSubmissionResponseTransformer(any(), any(), any()))
				.thenReturn(PaymentProcessingConsentMockData.getSubmissionResponse());
		paymentSubmissionTest.postPaymentProcessFlows(paymentsPlatformResourceMap,
				PaymentProcessingConsentMockData.getSubmissionResponse(), ProcessExecutionStatusEnum.PASS,
				"e58976e7-605c-48ac-aec5-0999343f6148", OBExternalStatus2Code.REJECTED, HttpMethod.POST.toString());
	}
	
	@Test
	public void postPaymentProcessCompletedPassFlowspass1_REJECTED_DOMESTIC_ST_ORD() {
		
		PaymentConsentsPlatformResource platformResource = new PaymentConsentsPlatformResource();
		platformResource.setCreatedAt(PispUtilities.getCurrentDateInISOFormat());
		platformResource.setIdempotencyKey("idempotency");
		platformResource.setIdempotencyRequest(String.valueOf(Boolean.TRUE));
		platformResource.setPaymentConsentId("e58976e7-605c-48ac-aec5-0999343f6148");
		platformResource.setPaymentType(PaymentTypeEnum.DOMESTIC_ST_ORD.getPaymentType());
		platformResource.setStatus("AwaitingAuthorisation");
		platformResource.setEndToEndIdentification("DEMO USER");
		platformResource.setInstructionIdentification("International");
		platformResource.setStatusUpdateDateTime(PispUtilities.getCurrentDateInISOFormat());
		platformResource.setTppCID("53ZcZkjLM1sXLOAHkwG6DB");
		platformResource.setTppLegalEntityName("BankOfDemo");
		Map<String, Object> paymentsPlatformResourceMap = new HashMap<>();
		PaymentConsentsPlatformResource platformResourceResponse = PaymentProcessingConsentMockData
				.setupConsentPlatformResource();
		PaymentsPlatformResource submissionPlatformResource = PaymentProcessingConsentMockData
				.getSubmissionPlatformResource();
		submissionPlatformResource.setProccessState(PaymentConstants.INCOMPLETE);
		Mockito.when(reqHeaderAtrributes.getToken()).thenReturn(PaymentProcessingConsentMockData.getToken1());	
		paymentsPlatformResourceMap.put(PaymentConstants.SUBMISSION, submissionPlatformResource);
		paymentsPlatformResourceMap.put(PaymentConstants.CONSENT, platformResourceResponse);
		Mockito.when(transformer.paymentSubmissionResponseTransformer(any(), any(), any()))
				.thenReturn(PaymentProcessingConsentMockData.getSubmissionResponse());
		paymentSubmissionTest.postPaymentProcessFlows(paymentsPlatformResourceMap,
				PaymentProcessingConsentMockData.getSubmissionResponse(), ProcessExecutionStatusEnum.PASS,
				"e58976e7-605c-48ac-aec5-0999343f6148", OBExternalStatus2Code.REJECTED, HttpMethod.POST.toString());
	}
	
	@Test
	public void postPaymentProcessCompletedPassFlowspass1_REJECTED_INTERNATIONAL_SCH_PAY() {
		
		PaymentConsentsPlatformResource platformResource = new PaymentConsentsPlatformResource();
		platformResource.setCreatedAt(PispUtilities.getCurrentDateInISOFormat());
		platformResource.setIdempotencyKey("idempotency");
		platformResource.setIdempotencyRequest(String.valueOf(Boolean.TRUE));
		platformResource.setPaymentConsentId("e58976e7-605c-48ac-aec5-0999343f6148");
		platformResource.setPaymentType(PaymentTypeEnum.INTERNATIONAL_SCH_PAY.getPaymentType());
		platformResource.setStatus("AwaitingAuthorisation");
		platformResource.setEndToEndIdentification("DEMO USER");
		platformResource.setInstructionIdentification("International");
		platformResource.setStatusUpdateDateTime(PispUtilities.getCurrentDateInISOFormat());
		platformResource.setTppCID("53ZcZkjLM1sXLOAHkwG6DB");
		platformResource.setTppLegalEntityName("BankOfDemo");
		Map<String, Object> paymentsPlatformResourceMap = new HashMap<>();
		PaymentConsentsPlatformResource platformResourceResponse = PaymentProcessingConsentMockData
				.setupConsentPlatformResource();
		PaymentsPlatformResource submissionPlatformResource = PaymentProcessingConsentMockData
				.getSubmissionPlatformResource();
		submissionPlatformResource.setProccessState(PaymentConstants.INCOMPLETE);
		Mockito.when(reqHeaderAtrributes.getToken()).thenReturn(PaymentProcessingConsentMockData.getToken1());	
		paymentsPlatformResourceMap.put(PaymentConstants.SUBMISSION, submissionPlatformResource);
		paymentsPlatformResourceMap.put(PaymentConstants.CONSENT, platformResourceResponse);
		Mockito.when(transformer.paymentSubmissionResponseTransformer(any(), any(), any()))
				.thenReturn(PaymentProcessingConsentMockData.getSubmissionResponse());
		paymentSubmissionTest.postPaymentProcessFlows(paymentsPlatformResourceMap,
				PaymentProcessingConsentMockData.getSubmissionResponse(), ProcessExecutionStatusEnum.PASS,
				"e58976e7-605c-48ac-aec5-0999343f6148", OBExternalStatus2Code.REJECTED, HttpMethod.POST.toString());
	}
	
	
	@Test
	public void postPaymentProcessCompletedPassFlowspass1_REJECTED_DOMESTIC_SCH_PAY() {
		
		PaymentConsentsPlatformResource platformResource = new PaymentConsentsPlatformResource();
		platformResource.setCreatedAt(PispUtilities.getCurrentDateInISOFormat());
		platformResource.setIdempotencyKey("idempotency");
		platformResource.setIdempotencyRequest(String.valueOf(Boolean.TRUE));
		platformResource.setPaymentConsentId("e58976e7-605c-48ac-aec5-0999343f6148");
		platformResource.setPaymentType(PaymentTypeEnum.DOMESTIC_SCH_PAY.getPaymentType());
		platformResource.setStatus("AwaitingAuthorisation");
		platformResource.setEndToEndIdentification("DEMO USER");
		platformResource.setInstructionIdentification("International");
		platformResource.setStatusUpdateDateTime(PispUtilities.getCurrentDateInISOFormat());
		platformResource.setTppCID("53ZcZkjLM1sXLOAHkwG6DB");
		platformResource.setTppLegalEntityName("BankOfDemo");
		Map<String, Object> paymentsPlatformResourceMap = new HashMap<>();
		PaymentConsentsPlatformResource platformResourceResponse = PaymentProcessingConsentMockData
				.setupConsentPlatformResource();
		PaymentsPlatformResource submissionPlatformResource = PaymentProcessingConsentMockData
				.getSubmissionPlatformResource();
		submissionPlatformResource.setProccessState(PaymentConstants.INCOMPLETE);
		Mockito.when(reqHeaderAtrributes.getToken()).thenReturn(PaymentProcessingConsentMockData.getToken1());	
		paymentsPlatformResourceMap.put(PaymentConstants.SUBMISSION, submissionPlatformResource);
		paymentsPlatformResourceMap.put(PaymentConstants.CONSENT, platformResourceResponse);
		Mockito.when(transformer.paymentSubmissionResponseTransformer(any(), any(), any()))
				.thenReturn(PaymentProcessingConsentMockData.getSubmissionResponse());
		paymentSubmissionTest.postPaymentProcessFlows(paymentsPlatformResourceMap,
				PaymentProcessingConsentMockData.getSubmissionResponse(), ProcessExecutionStatusEnum.PASS,
				"e58976e7-605c-48ac-aec5-0999343f6148", OBExternalStatus2Code.REJECTED, HttpMethod.POST.toString());
	}
		
	@Test
	public void postPaymentProcessCompletedPassWithMultiAuthAwaitingFlows() {
		Map<String, Object> paymentsPlatformResourceMap = new HashMap<>();
		PaymentConsentsPlatformResource platformResourceResponse = PaymentProcessingConsentMockData
				.setupConsentPlatformResource();
		PaymentsPlatformResource submissionPlatformResource = PaymentProcessingConsentMockData
				.getSubmissionPlatformResource();
		submissionPlatformResource.setProccessState(PaymentConstants.COMPLETED);
		paymentsPlatformResourceMap.put(PaymentConstants.SUBMISSION, submissionPlatformResource);
		paymentsPlatformResourceMap.put(PaymentConstants.CONSENT, platformResourceResponse);
		Mockito.when(transformer.paymentSubmissionResponseTransformer(any(), any(), any()))
				.thenReturn(PaymentProcessingConsentMockData.getSubmissionResponse());
		paymentSubmissionTest.postPaymentProcessFlows(paymentsPlatformResourceMap,
				PaymentProcessingConsentMockData.getSubmissionResponse(), ProcessExecutionStatusEnum.PASS,
				"e58976e7-605c-48ac-aec5-0999343f6148", OBExternalStatus2Code.AWAITINGFURTHERAUTHORISATION,
				HttpMethod.POST.toString());
	}

	@Test
	public void postPaymentProcessCompletedPassWithMultiAuthRejectedFlows() {
		Map<String, Object> paymentsPlatformResourceMap = new HashMap<>();
		PaymentConsentsPlatformResource platformResourceResponse = PaymentProcessingConsentMockData
				.setupConsentPlatformResource();
		PaymentsPlatformResource submissionPlatformResource = PaymentProcessingConsentMockData
				.getSubmissionPlatformResource();
		submissionPlatformResource.setProccessState(PaymentConstants.COMPLETED);
		paymentsPlatformResourceMap.put(PaymentConstants.SUBMISSION, submissionPlatformResource);
		paymentsPlatformResourceMap.put(PaymentConstants.CONSENT, platformResourceResponse);
		Mockito.when(transformer.paymentSubmissionResponseTransformer(any(), any(), any()))
				.thenReturn(PaymentProcessingConsentMockData.getSubmissionResponse());
		paymentSubmissionTest.postPaymentProcessFlows(paymentsPlatformResourceMap,
				PaymentProcessingConsentMockData.getSubmissionResponse(), ProcessExecutionStatusEnum.PASS,
				"e58976e7-605c-48ac-aec5-0999343f6148", OBExternalStatus2Code.REJECTED, HttpMethod.POST.toString());
	}

	@Test(expected = PSD2Exception.class)
	public void postPaymentProcessCompletedPassWithoutSubmissionIdFlows() {
		Map<String, Object> paymentsPlatformResourceMap = new HashMap<>();
		PaymentConsentsPlatformResource platformResourceResponse = PaymentProcessingConsentMockData
				.setupConsentPlatformResource();
		PaymentsPlatformResource submissionPlatformResource = PaymentProcessingConsentMockData
				.getSubmissionPlatformResource();
		submissionPlatformResource.setProccessState(PaymentConstants.INCOMPLETE);
		paymentsPlatformResourceMap.put(PaymentConstants.SUBMISSION, submissionPlatformResource);
		paymentsPlatformResourceMap.put(PaymentConstants.CONSENT, platformResourceResponse);
		Mockito.when(transformer.paymentSubmissionResponseTransformer(any(), any(), any()))
				.thenReturn(PaymentProcessingConsentMockData.getSubmissionResponse());
		paymentSubmissionTest.postPaymentProcessFlows(paymentsPlatformResourceMap,
				PaymentProcessingConsentMockData.getSubmissionResponse(), ProcessExecutionStatusEnum.PASS, null,
				OBExternalStatus2Code.AUTHORISED, HttpMethod.POST.toString());
	}

	@Test
	public void postPaymentProcessCompletedIncompleteFlows() {
		Map<String, Object> paymentsPlatformResourceMap = new HashMap<>();
		PaymentConsentsPlatformResource platformResourceResponse = PaymentProcessingConsentMockData
				.setupConsentPlatformResource();
		PaymentsPlatformResource submissionPlatformResource = PaymentProcessingConsentMockData
				.getSubmissionPlatformResource();
		submissionPlatformResource.setProccessState(PaymentConstants.INCOMPLETE);
		paymentsPlatformResourceMap.put(PaymentConstants.SUBMISSION, submissionPlatformResource);
		paymentsPlatformResourceMap.put(PaymentConstants.CONSENT, platformResourceResponse);
		Mockito.when(reqHeaderAtrributes.getToken()).thenReturn(PaymentProcessingConsentMockData.getToken1());
		Mockito.when(transformer.paymentSubmissionResponseTransformer(any(), any(), any()))
				.thenReturn(PaymentProcessingConsentMockData.getSubmissionResponse());
		paymentSubmissionTest.postPaymentProcessFlows(paymentsPlatformResourceMap,
				PaymentProcessingConsentMockData.getSubmissionResponse(), ProcessExecutionStatusEnum.INCOMPLETE,
				"e58976e7-605c-48ac-aec5-0999343f6148", OBExternalStatus2Code.AUTHORISED, HttpMethod.POST.toString());
	}

	@Test(expected = PSD2Exception.class)
	public void postPaymentProcessCompletedAbortFlows() {
		Map<String, Object> paymentsPlatformResourceMap = new HashMap<>();
		PaymentConsentsPlatformResource platformResourceResponse = PaymentProcessingConsentMockData
				.setupConsentPlatformResource();
		PaymentsPlatformResource submissionPlatformResource = PaymentProcessingConsentMockData
				.getSubmissionPlatformResource();
		submissionPlatformResource.setProccessState(PaymentConstants.ABORT);
		paymentsPlatformResourceMap.put(PaymentConstants.SUBMISSION, submissionPlatformResource);
		paymentsPlatformResourceMap.put(PaymentConstants.CONSENT, platformResourceResponse);
		Mockito.when(transformer.paymentSubmissionResponseTransformer(any(), any(), any()))
				.thenReturn(PaymentProcessingConsentMockData.getSubmissionResponse());
		paymentSubmissionTest.postPaymentProcessFlows(paymentsPlatformResourceMap,
				PaymentProcessingConsentMockData.getSubmissionResponse(), ProcessExecutionStatusEnum.ABORT,
				"e58976e7-605c-48ac-aec5-0999343f6148", OBExternalStatus2Code.AUTHORISED, HttpMethod.POST.toString());
	}
	
	@Test
	public void prePaymentProcessGETFlowsSuccessFlow() {
		String submissionId = null;
		PaymentTypeEnum status = PaymentTypeEnum.DOMESTIC_PAY;
		when(reqHeaderAtrributes.getTppCID()).thenReturn("53ZcZkjLM1sXLOAHkwG6DB");
		Mockito.when(paymentsPlatformAdapter.retrievePaymentsPlatformResourceByPaySubmissionIdAndType(
				anyString(), anyObject())).thenReturn(PaymentProcessingConsentMockData.getSubmissionPlatformResource());
		paymentSubmissionTest.prePaymentProcessGETFlows(submissionId, status );
	}
	
	@Test(expected = PSD2Exception.class)
	public void prePaymentProcessGETFlowsSubmissionResourceNullFlow() {
		String submissionId = null;
		PaymentTypeEnum status = PaymentTypeEnum.INTERNATIONAL_PAY;
		when(reqHeaderAtrributes.getTppCID()).thenReturn("53ZcZkjLM1sXLOAHkwG6DB");
		Mockito.when(paymentsPlatformAdapter.retrievePaymentsPlatformResourceByPaySubmissionIdAndType(
				anyString(), anyObject())).thenReturn(null);
		paymentSubmissionTest.prePaymentProcessGETFlows(submissionId, status );
	}
	
	@Test(expected = PSD2Exception.class)
	public void prePaymentProcessGETFlowsTppCIDMismatchFlow() {
		String submissionId = null;
		PaymentTypeEnum status = PaymentTypeEnum.INTERNATIONAL_PAY;
		when(reqHeaderAtrributes.getTppCID()).thenReturn("53AcZkjLM1sXLOAHkwG6DB");
		Mockito.when(paymentsPlatformAdapter.retrievePaymentsPlatformResourceByPaySubmissionIdAndType(
				anyString(), anyObject())).thenReturn(null);
		paymentSubmissionTest.prePaymentProcessGETFlows(submissionId, status );
	}
	
	@Test
	public void prePaymentProcessGETFlowsDomesticPaymentsSuccessFlow() {
		String submissionId = null;
		PaymentTypeEnum status = PaymentTypeEnum.DOMESTIC_PAY;
		when(reqHeaderAtrributes.getTppCID()).thenReturn("53ZcZkjLM1sXLOAHkwG6DB");
		Mockito.when(paymentsPlatformAdapter.retrievePaymentsPlatformResourceByPaySubmissionIdAndType(
				anyString(), anyObject())).thenReturn(null);
		Mockito.when(paymentsPlatformAdapter.retrievePaymentsPlatformResourceByBackwardSubmissionId(anyString())).thenReturn(PaymentProcessingConsentMockData.getSubmissionPlatformResource());
		paymentSubmissionTest.prePaymentProcessGETFlows(submissionId, status );
	}
	
	@Test
	public void updateConsentResourceWithConsumedTest(){
		
		PaymentConsentsPlatformResource platformResourceResponse = PaymentProcessingConsentMockData
				.setupConsentPlatformResource();
		Token token = new Token();
		Map<String, String> seviceParams = new HashMap<>();
		token.setSeviceParams(seviceParams );
		when(reqHeaderAtrributes.getToken()).thenReturn(token);
		
		paymentSubmissionTest.updateConsentResourceWithConsumed(platformResourceResponse);
	}
	
	@Test(expected=PSD2Exception.class)
	public void preSubmissionProcessingComparePaymentSubmissionHashKeyNull() {

		CustomDPaymentsPOSTRequest request = new CustomDPaymentsPOSTRequest();
		OBWriteDataDomestic1 data = new OBWriteDataDomestic1();
		OBRisk1 risk = new OBRisk1();

		request.setData(data);
		request.setRisk(risk);
		
		PaymentsPlatformResource platformResource=new PaymentsPlatformResource();
		platformResource.setIdempotencyKey("idempotency");
		platformResource.setTppCID("53ZcZkjLM1sXLOAHkwG6DB");
		platformResource.setPaymentConsentId("e58976e7-605c-48ac-aec5-0999343f6148");
		platformResource.setProccessState(PaymentConstants.COMPLETED);
		platformResource.setPaymentType(PaymentTypeEnum.DOMESTIC_PAY.toString());
		Mockito.when(reqHeaderAtrributes.getToken()).thenReturn(PaymentProcessingConsentMockData.getToken());
		Mockito.when(paymentConsentsPlatformAdapter.retrievePaymentSetupPlatformResource(anyString()))
				.thenReturn(PaymentProcessingConsentMockData.setupConsentPlatformResource());
		Mockito.when(paymentsPlatformAdapter.getIdempotentPaymentsPlatformResource(anyLong(), anyString()))
				.thenReturn(null);
		Mockito.when(paymentsPlatformAdapter.retrievePaymentsResourceByConsentId(anyString()))
				.thenReturn(PaymentProcessingConsentMockData.getSubmissionPlatformResource());
		paymentSubmissionTest.preSubmissionProcessing(request,
				PaymentProcessingConsentMockData.setupCustomPlatformDetails(), "e58976e7-605c-48ac-aec5-0999343f6148");
	}
	
	@Test(expected=PSD2Exception.class)
	public void preSubmissionProcessingComparePaymentSubmissionHashKeyCompareSubmissionHashKeyNull() {

		CustomDPaymentsPOSTRequest request = new CustomDPaymentsPOSTRequest();
		OBWriteDataDomestic1 data = new OBWriteDataDomestic1();
		OBRisk1 risk = new OBRisk1();

		request.setData(data);
		request.setRisk(risk);
		
		PaymentsPlatformResource platformResource=new PaymentsPlatformResource();
		platformResource.setIdempotencyKey("idempotency");
		platformResource.setTppCID("53ZcZkjLM1sXLOAHkwG6DB");
		platformResource.setPaymentConsentId("e58976e7-605c-48ac-aec5-0999343f6148");
		platformResource.setProccessState(PaymentConstants.COMPLETED);
		platformResource.setPaymentType(PaymentTypeEnum.DOMESTIC_PAY.toString());
		
		Mockito.when(reqHeaderAtrributes.getToken()).thenReturn(PaymentProcessingConsentMockData.getToken());
		Mockito.when(paymentConsentsPlatformAdapter.retrievePaymentSetupPlatformResource(anyString()))
				.thenReturn(PaymentProcessingConsentMockData.setupConsentPlatformResource());
		Mockito.when(paymentsPlatformAdapter.getIdempotentPaymentsPlatformResource(anyLong(), anyString()))
				.thenReturn(null);
		Mockito.when(paymentsPlatformAdapter.retrievePaymentsResourceByConsentId(anyString()))
				.thenReturn(PaymentProcessingConsentMockData.getSubmissionIdPlatformResource());
		
		paymentSubmissionTest.preSubmissionProcessing(request,
				PaymentProcessingConsentMockData.setupCustomPlatformDetails(), "e58976e7-605c-48ac-aec5-0999343f6148");
	}
	
	@Test(expected=PSD2Exception.class)
	public void preSubmissionProcessingComparePaymentSubmissionHashKeyCompareSubmissionHashKeyNullSuccess() {

		CustomDPaymentsPOSTRequest request = new CustomDPaymentsPOSTRequest();
		OBWriteDataDomestic1 data = new OBWriteDataDomestic1();
		OBRisk1 risk = new OBRisk1();

		request.setData(data);
		request.setRisk(risk);
		
		PaymentsPlatformResource platformResource=new PaymentsPlatformResource();
		platformResource.setIdempotencyKey("idempotency");
		platformResource.setTppCID("53ZcZkjLM1sXLOAHkwG6DB");
		platformResource.setPaymentConsentId("e58976e7-605c-48ac-aec5-0999343f6148");
		platformResource.setProccessState(PaymentConstants.COMPLETED);
		platformResource.setPaymentType(PaymentTypeEnum.DOMESTIC_PAY.toString());
		Mockito.when(reqHeaderAtrributes.getToken()).thenReturn(PaymentProcessingConsentMockData.getToken());
		Mockito.when(paymentConsentsPlatformAdapter.retrievePaymentSetupPlatformResource(anyString()))
				.thenReturn(PaymentProcessingConsentMockData.setupConsentPlatformResource());
		Mockito.when(paymentsPlatformAdapter.getIdempotentPaymentsPlatformResource(anyLong(), anyString()))
				.thenReturn(null);
		Mockito.when(paymentsPlatformAdapter.retrievePaymentsResourceByConsentId(anyString()))
				.thenReturn(PaymentProcessingConsentMockData.getSubmissionIdPlatformResource());
		
		paymentSubmissionTest.preSubmissionProcessing(request,
				PaymentProcessingConsentMockData.setupCustomPlatformDetails(), "e58976e7-605c-48ac-aec5-0999343f6148");
	}



}
