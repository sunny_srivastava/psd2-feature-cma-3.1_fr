package com.capgemini.psd2.pisp.processing.adapter.test.service.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.bind.annotation.RequestMethod;

import com.capgemini.psd2.exceptions.OBPSD2ExceptionUtility;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.domain.CustomDPaymentConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomDPaymentConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.PaymentConsentsPlatformResource;
import com.capgemini.psd2.pisp.domain.PaymentResponseInfo;
import com.capgemini.psd2.pisp.enums.ProcessConsentStatusEnum;
import com.capgemini.psd2.pisp.payment.setup.platform.adapter.PaymentSetupPlatformAdapter;
import com.capgemini.psd2.pisp.processing.adapter.PaymentConsentsCustomValidator;
import com.capgemini.psd2.pisp.processing.adapter.PaymentConsentsTransformer;
import com.capgemini.psd2.pisp.processing.adapter.service.impl.PaymentConsentProcessingAdapterImpl;
import com.capgemini.psd2.pisp.processing.adapter.test.mock.data.PaymentProcessingConsentMockData;

@RunWith(SpringJUnit4ClassRunner.class)
public class PaymentConsentProcessingAdapterImplTest {

	@InjectMocks
	private PaymentConsentProcessingAdapterImpl paymentConsentProcessingAdapterImplTest = new PaymentConsentProcessingAdapterImpl(
			"24H");

	@Mock
	private PaymentSetupPlatformAdapter paymentSetupPlatformAdapter;

	@Mock
	private PaymentConsentsCustomValidator<CustomDPaymentConsentsPOSTRequest, CustomDPaymentConsentsPOSTResponse> paymentCustomValidator;

	@Mock
	private RequestHeaderAttributes reqHeaderAtrributes;

	@Mock
	private PaymentConsentsTransformer transformer;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		Map<String, String> map = new HashMap<>();
		map.put("SIGNATURE", "Something wrong with x-jws-signature request header");
		Map<String, String> specificErrorMessageMap = new HashMap<>();
		specificErrorMessageMap.put("signature_invalid_content",
				"Invalid content prsent in joseheader of x-jws-signature header");
		specificErrorMessageMap.put("signature_missing", "signature header missing in request");
		OBPSD2ExceptionUtility.genericErrorMessages.putAll(map);
		OBPSD2ExceptionUtility.specificErrorMessages.putAll(specificErrorMessageMap);
	}

	@Test
	public void preConsentFlowTestWithoutIdempotentRequest() {
		PaymentConsentsPlatformResource platformResourceResponse = PaymentProcessingConsentMockData
				.setupConsentPlatformResource();
		Mockito.when(paymentCustomValidator
				.validateConsentRequest(PaymentProcessingConsentMockData.getDomesticPaymentConsentsRequest()))
				.thenReturn(Boolean.TRUE);
		Mockito.when(paymentSetupPlatformAdapter.getIdempotentPaymentSetupPlatformResource(any(), anyLong()))
				.thenReturn(null);
		Mockito.when(paymentSetupPlatformAdapter.createPaymentSetupPlatformResource(any(), any()))
				.thenReturn(platformResourceResponse);
		PaymentConsentsPlatformResource platformResourceRequest = paymentConsentProcessingAdapterImplTest
				.preConsentProcessFlows(PaymentProcessingConsentMockData.getDomesticPaymentConsentsRequest(),
						PaymentProcessingConsentMockData.setupCustomPlatformDetails());
		assertEquals(platformResourceRequest, platformResourceResponse);
	}

	@Test
	public void preConsentFlowTestWithIdempotentRequest() {
		PaymentConsentsPlatformResource platformResourceResponse = PaymentProcessingConsentMockData
				.setupConsentPlatformResource();
		Mockito.when(paymentCustomValidator
				.validateConsentRequest(PaymentProcessingConsentMockData.getDomesticPaymentConsentsRequest()))
				.thenReturn(Boolean.TRUE);
		Mockito.when(paymentSetupPlatformAdapter.getIdempotentPaymentSetupPlatformResource(any(), anyLong()))
				.thenReturn(platformResourceResponse);
		Mockito.when(paymentSetupPlatformAdapter.createPaymentSetupPlatformResource(any(), any()))
				.thenReturn(platformResourceResponse);
		PaymentConsentsPlatformResource platformResourceRequest = paymentConsentProcessingAdapterImplTest
				.preConsentProcessFlows(PaymentProcessingConsentMockData.getDomesticPaymentConsentsRequest(),
						PaymentProcessingConsentMockData.setupCustomPlatformDetails());
		assertEquals(platformResourceRequest, platformResourceResponse);
	}

	@Test(expected = PSD2Exception.class)
	public void preConsentFlowTestWithInvalidRequest() {
		PaymentConsentsPlatformResource platformResourceResponse = PaymentProcessingConsentMockData
				.setupConsentPlatformResource();
		Mockito.when(paymentCustomValidator
				.validateConsentRequest(PaymentProcessingConsentMockData.getDomesticPaymentConsentsRequest()))
				.thenThrow(PSD2Exception.class);
		Mockito.when(paymentSetupPlatformAdapter.createPaymentSetupPlatformResource(any(), any()))
				.thenReturn(platformResourceResponse);
		PaymentConsentsPlatformResource platformResourceRequest = paymentConsentProcessingAdapterImplTest
				.preConsentProcessFlows(PaymentProcessingConsentMockData.getDomesticPaymentConsentsRequest(),
						PaymentProcessingConsentMockData.setupCustomPlatformDetails());
		assertEquals(platformResourceRequest, platformResourceResponse);
	}

	@Test
	public void preConsentFlowTestWithoutIdempotentRequest_CustomDStandingOrderConsentsPOSTRequest() {
		PaymentConsentsPlatformResource platformResourceResponse = PaymentProcessingConsentMockData
				.setupConsentPlatformResource();
		Mockito.when(paymentCustomValidator
				.validateConsentRequest(PaymentProcessingConsentMockData.getDomesticPaymentConsentsRequest()))
				.thenReturn(Boolean.TRUE);
		Mockito.when(paymentSetupPlatformAdapter.getIdempotentPaymentSetupPlatformResource(any(), anyLong()))
				.thenReturn(null);
		Mockito.when(paymentSetupPlatformAdapter.createPaymentSetupPlatformResource(any(), any()))
				.thenReturn(platformResourceResponse);
		PaymentConsentsPlatformResource platformResourceRequest = paymentConsentProcessingAdapterImplTest
				.preConsentProcessFlows(PaymentProcessingConsentMockData.getCustomDStandingOrderConsentsPOSTRequest(),
						PaymentProcessingConsentMockData.setupCustomPlatformDetails());
		assertEquals(platformResourceRequest, platformResourceResponse);
	}
	
	@Test
	public void preConsentFlowTestWithoutIdempotentRequest_CustomCustomDSPConsentsPOSTRequest() {
		PaymentConsentsPlatformResource platformResourceResponse = PaymentProcessingConsentMockData
				.setupConsentPlatformResource();
		Mockito.when(paymentCustomValidator
				.validateConsentRequest(PaymentProcessingConsentMockData.getDomesticPaymentConsentsRequest()))
				.thenReturn(Boolean.TRUE);
		Mockito.when(paymentSetupPlatformAdapter.getIdempotentPaymentSetupPlatformResource(any(), anyLong()))
				.thenReturn(null);
		Mockito.when(paymentSetupPlatformAdapter.createPaymentSetupPlatformResource(any(), any()))
				.thenReturn(platformResourceResponse);
		PaymentConsentsPlatformResource platformResourceRequest = paymentConsentProcessingAdapterImplTest
				.preConsentProcessFlows(PaymentProcessingConsentMockData.getCustomDSPConsentsPOSTRequest(),
						PaymentProcessingConsentMockData.setupCustomPlatformDetails());
		assertEquals(platformResourceRequest, platformResourceResponse);
	}
	
	@Test
	public void preConsentFlowTestWithoutIdempotentRequest_CustomgetCustomISPConsentsPOSTRequest() {
		PaymentConsentsPlatformResource platformResourceResponse = PaymentProcessingConsentMockData
				.setupConsentPlatformResource();
		Mockito.when(paymentCustomValidator
				.validateConsentRequest(PaymentProcessingConsentMockData.getDomesticPaymentConsentsRequest()))
				.thenReturn(Boolean.TRUE);
		Mockito.when(paymentSetupPlatformAdapter.getIdempotentPaymentSetupPlatformResource(any(), anyLong()))
				.thenReturn(null);
		Mockito.when(paymentSetupPlatformAdapter.createPaymentSetupPlatformResource(any(), any()))
				.thenReturn(platformResourceResponse);
		PaymentConsentsPlatformResource platformResourceRequest = paymentConsentProcessingAdapterImplTest
				.preConsentProcessFlows(PaymentProcessingConsentMockData.getCustomISPConsentsPOSTRequest(),
						PaymentProcessingConsentMockData.setupCustomPlatformDetails());
		assertEquals(platformResourceRequest, platformResourceResponse);
	}
	
	@Test
	public void preConsentFlowTestWithoutIdempotentRequest_getCustomIPaymentConsentsPOSTRequest() {
		PaymentConsentsPlatformResource platformResourceResponse = PaymentProcessingConsentMockData
				.setupConsentPlatformResource();
		Mockito.when(paymentCustomValidator
				.validateConsentRequest(PaymentProcessingConsentMockData.getDomesticPaymentConsentsRequest()))
				.thenReturn(Boolean.TRUE);
		Mockito.when(paymentSetupPlatformAdapter.getIdempotentPaymentSetupPlatformResource(any(), anyLong()))
				.thenReturn(null);
		Mockito.when(paymentSetupPlatformAdapter.createPaymentSetupPlatformResource(any(), any()))
				.thenReturn(platformResourceResponse);
		PaymentConsentsPlatformResource platformResourceRequest = paymentConsentProcessingAdapterImplTest
				.preConsentProcessFlows(PaymentProcessingConsentMockData.getCustomIPaymentConsentsPOSTRequest(),
						PaymentProcessingConsentMockData.setupCustomPlatformDetails());
		assertEquals(platformResourceRequest, platformResourceResponse);
	}
	
	@Test
	public void preConsentProcessGetSuccessFlow() {
		PaymentConsentsPlatformResource platformResourceResponse = PaymentProcessingConsentMockData
				.setupConsentPlatformResource();
		Mockito.when(paymentCustomValidator
				.validateConsentsGETRequest(PaymentProcessingConsentMockData.getPaymentRetrieveGetRequest()))
				.thenReturn(Boolean.TRUE);
		when(paymentSetupPlatformAdapter.retrievePaymentSetupPlatformResourceByIdAndType(anyString(), any()))
				.thenReturn(platformResourceResponse);
		when(paymentSetupPlatformAdapter.retrievePaymentSetupPlatformResourceByBackwardPaymentId(anyString()))
		.thenReturn(platformResourceResponse);
		Mockito.when(paymentSetupPlatformAdapter.retrievePaymentSetupPlatformResource(anyString()))
				.thenReturn(platformResourceResponse);
		Mockito.when(reqHeaderAtrributes.getTppCID()).thenReturn("53ZcZkjLM1sXLOAHkwG6DB");
		paymentConsentProcessingAdapterImplTest
				.preConsentProcessGETFlow(PaymentProcessingConsentMockData.getPaymentRetrieveGetRequest());
	}

	@Test(expected = PSD2Exception.class)
	public void preConsentProcessGetWitoutPostFlow() {
		PaymentConsentsPlatformResource platformResourceResponse = PaymentProcessingConsentMockData
				.setupConsentPlatformResource();
		Mockito.when(paymentCustomValidator
				.validateConsentsGETRequest(PaymentProcessingConsentMockData.getPaymentRetrieveGetRequest()))
				.thenReturn(Boolean.TRUE);
		when(paymentSetupPlatformAdapter.retrievePaymentSetupPlatformResourceByIdAndType(anyString(), any()))
				.thenReturn(platformResourceResponse);
		when(paymentSetupPlatformAdapter.retrievePaymentSetupPlatformResourceByBackwardPaymentId(anyString()))
				.thenReturn(platformResourceResponse);
		Mockito.when(paymentSetupPlatformAdapter.retrievePaymentSetupPlatformResource(anyString())).thenReturn(null);
		paymentConsentProcessingAdapterImplTest
				.preConsentProcessGETFlow(PaymentProcessingConsentMockData.getPaymentRetrieveGetRequest());
	}
	
	@Test(expected = PSD2Exception.class)
	public void preConsentProcessGetWitoutPostFlow1() {
		PaymentConsentsPlatformResource platformResourceResponse = PaymentProcessingConsentMockData
				.setupConsentPlatformResource();
		Mockito.when(paymentCustomValidator
				.validateConsentsGETRequest(PaymentProcessingConsentMockData.getPaymentRetrieveGetRequest()))
				.thenReturn(Boolean.TRUE);
		when(paymentSetupPlatformAdapter.retrievePaymentSetupPlatformResourceByIdAndType(anyString(), any()))
				.thenReturn(null);
		when(paymentSetupPlatformAdapter.retrievePaymentSetupPlatformResourceByBackwardPaymentId(anyString()))
				.thenReturn(platformResourceResponse);
		Mockito.when(paymentSetupPlatformAdapter.retrievePaymentSetupPlatformResource(anyString())).thenReturn(null);
		paymentConsentProcessingAdapterImplTest
				.preConsentProcessGETFlow(PaymentProcessingConsentMockData.getPaymentRetrieveGetRequest());
	}
	

	@Test(expected = PSD2Exception.class)
	public void preConsentProcessGetTPPMismatchFlow() {
		PaymentConsentsPlatformResource platformResourceResponse = PaymentProcessingConsentMockData
				.setupConsentPlatformResource();
		Mockito.when(paymentCustomValidator
				.validateConsentsGETRequest(PaymentProcessingConsentMockData.getPaymentRetrieveGetRequest()))
				.thenReturn(Boolean.TRUE);
		when(paymentSetupPlatformAdapter.retrievePaymentSetupPlatformResourceByIdAndType(anyString(), any()))
				.thenReturn(platformResourceResponse);
		Mockito.when(paymentSetupPlatformAdapter.retrievePaymentSetupPlatformResource(anyString()))
				.thenReturn(platformResourceResponse);
		when(paymentSetupPlatformAdapter.retrievePaymentSetupPlatformResourceByBackwardPaymentId(anyString()))
		.thenReturn(platformResourceResponse);
		Mockito.when(reqHeaderAtrributes.getTppCID()).thenReturn("cid");
		paymentConsentProcessingAdapterImplTest
				.preConsentProcessGETFlow(PaymentProcessingConsentMockData.getPaymentRetrieveGetRequest());
	}

	@Test
	public void postConsentProcessSuccessFlowsWithIdempotentRequest() {
		PaymentConsentsPlatformResource platformResourceResponse = PaymentProcessingConsentMockData
				.setupConsentPlatformResource();
		CustomDPaymentConsentsPOSTResponse response = PaymentProcessingConsentMockData.getPaymentConsentResponse();
		PaymentResponseInfo stageInfo = PaymentProcessingConsentMockData.getPaymentResponseInfo();
		Mockito.when(paymentSetupPlatformAdapter.updatePaymentSetupPlatformResource(any()))
				.thenReturn(platformResourceResponse);
		Mockito.when(transformer.paymentConsentsResponseTransformer(any(), any(), anyString())).thenReturn(response);
		Mockito.when(paymentCustomValidator.validatePaymentConsentResponse(any())).thenReturn(Boolean.TRUE);
		paymentConsentProcessingAdapterImplTest.postConsentProcessFlows(stageInfo, response, platformResourceResponse,
				RequestMethod.POST.toString());
	}

	@Test
	public void postConsentProcessSuccessFlowsWithNonIdempotentRequest() {
		PaymentConsentsPlatformResource platformResourceResponse = PaymentProcessingConsentMockData
				.setupConsentPlatformResource();
		CustomDPaymentConsentsPOSTResponse response = PaymentProcessingConsentMockData.getPaymentConsentResponse();
		PaymentResponseInfo stageInfo = PaymentProcessingConsentMockData.getPaymentResponseInfo();
		stageInfo.setRequestType(PSD2Constants.PAYMENT_RESOURCE_NON_IDEMPOTENT);
		Mockito.when(paymentSetupPlatformAdapter.updatePaymentSetupPlatformResource(any()))
				.thenReturn(platformResourceResponse);
		Mockito.when(transformer.paymentConsentsResponseTransformer(any(), any(), anyString())).thenReturn(response);
		Mockito.when(paymentCustomValidator.validatePaymentConsentResponse(any())).thenReturn(Boolean.TRUE);
		paymentConsentProcessingAdapterImplTest.postConsentProcessFlows(stageInfo, response, platformResourceResponse,
				RequestMethod.POST.toString());
	}

	@Test
	public void postConsentProcessSuccessFlowsWithNonIdempotentFailRequest() {
		PaymentConsentsPlatformResource platformResourceResponse = PaymentProcessingConsentMockData
				.setupConsentPlatformResource();
		CustomDPaymentConsentsPOSTResponse response = PaymentProcessingConsentMockData.getPaymentConsentResponse();
		PaymentResponseInfo stageInfo = PaymentProcessingConsentMockData.getPaymentResponseInfo();
		stageInfo.setRequestType(PSD2Constants.PAYMENT_RESOURCE_NON_IDEMPOTENT);
		stageInfo.setConsentProcessStatus(ProcessConsentStatusEnum.FAIL);
		Mockito.when(paymentSetupPlatformAdapter.updatePaymentSetupPlatformResource(any()))
				.thenReturn(platformResourceResponse);
		Mockito.when(transformer.paymentConsentsResponseTransformer(any(), any(), anyString())).thenReturn(response);
		Mockito.when(paymentCustomValidator.validatePaymentConsentResponse(any())).thenReturn(Boolean.TRUE);
		paymentConsentProcessingAdapterImplTest.postConsentProcessFlows(stageInfo, response, platformResourceResponse,
				RequestMethod.POST.toString());
	}

	@Test(expected = PSD2Exception.class)
	public void postConsentProcessSuccessFlowsWithInvalidStagingRequest() {
		PaymentConsentsPlatformResource platformResourceResponse = PaymentProcessingConsentMockData
				.setupConsentPlatformResource();
		CustomDPaymentConsentsPOSTResponse response = PaymentProcessingConsentMockData.getPaymentConsentResponse();
		PaymentResponseInfo stageInfo = PaymentProcessingConsentMockData.getPaymentResponseInfo();
		stageInfo.setRequestType(PSD2Constants.PAYMENT_RESOURCE_NON_IDEMPOTENT);
		stageInfo.setPaymentConsentId(null);
		Mockito.when(paymentSetupPlatformAdapter.updatePaymentSetupPlatformResource(any()))
				.thenReturn(platformResourceResponse);
		Mockito.when(transformer.paymentConsentsResponseTransformer(any(), any(), anyString())).thenReturn(response);
		Mockito.when(paymentCustomValidator.validatePaymentConsentResponse(any())).thenReturn(Boolean.TRUE);
		paymentConsentProcessingAdapterImplTest.postConsentProcessFlows(stageInfo, response, platformResourceResponse,
				RequestMethod.POST.toString());
	}
}
