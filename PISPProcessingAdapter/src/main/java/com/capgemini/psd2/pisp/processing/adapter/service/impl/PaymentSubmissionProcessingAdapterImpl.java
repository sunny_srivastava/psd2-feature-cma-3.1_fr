package com.capgemini.psd2.pisp.processing.adapter.service.impl;

import java.util.HashMap;
import java.util.Map;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMethod;

import com.capgemini.psd2.consent.domain.PispConsent;
import com.capgemini.psd2.enums.ConsentStatusEnum;
import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.ErrorMapKeys;
import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.InternalServerErrorMessage;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.LoggerUtils;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.adapter.PispConsentAdapter;
import com.capgemini.psd2.pisp.consent.adapter.repository.PispConsentMongoRepository;
import com.capgemini.psd2.pisp.domain.CustomPaymentSetupPlatformDetails;
import com.capgemini.psd2.pisp.domain.OBExternalConsentStatus1Code;
import com.capgemini.psd2.pisp.domain.OBExternalStatus1Code;
import com.capgemini.psd2.pisp.domain.OBExternalStatus2Code;
import com.capgemini.psd2.pisp.domain.OBTransactionIndividualStatus1Code;
import com.capgemini.psd2.pisp.domain.PaymentConsentsPlatformResource;
import com.capgemini.psd2.pisp.domain.PaymentsPlatformResource;
import com.capgemini.psd2.pisp.enums.PaymentTypeEnum;
import com.capgemini.psd2.pisp.enums.ProcessExecutionStatusEnum;
import com.capgemini.psd2.pisp.payment.setup.platform.adapter.PaymentSetupPlatformAdapter;
import com.capgemini.psd2.pisp.payment.submission.platform.adapter.PaymentsPlatformAdapter;
import com.capgemini.psd2.pisp.processing.adapter.PaymentSubmissionCustomValidator;
import com.capgemini.psd2.pisp.processing.adapter.PaymentSubmissionTransformer;
import com.capgemini.psd2.pisp.processing.adapter.service.PaymentSubmissionProcessingAdapter;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.pisp.status.PaymentConstants;
import com.capgemini.psd2.pisp.status.PaymentStatusEnum;
import com.capgemini.psd2.pisp.utilities.PispUtilities;
import com.capgemini.psd2.utilities.JSONUtilities;

@Component
public class PaymentSubmissionProcessingAdapterImpl<T, U> implements PaymentSubmissionProcessingAdapter<T, U> {

	private static final Logger LOG = LoggerFactory.getLogger(PaymentSubmissionProcessingAdapterImpl.class);

	@Autowired
	private LoggerUtils loggerUtils;
	@Autowired
	@Qualifier("paymentsValidatorRoutingBean")
	private PaymentSubmissionCustomValidator<T, U> paymentCustomValidator;

	@SuppressWarnings("rawtypes")
	@Autowired
	@Qualifier("paymentsResponseTransformRoutingBean")
	private PaymentSubmissionTransformer transformer;

	@Autowired
	private RequestHeaderAttributes reqHeaderAtrributes;
	@Autowired
	private PaymentSetupPlatformAdapter paymentConsentsPlatformAdapter;
	@Autowired
	private PaymentsPlatformAdapter paymentsPlatformAdapter;
	@Autowired
	private MongoTemplate mongoTemplate;

	@Autowired
	private PispConsentAdapter pispConsentAdapter;
	@Autowired
	private PispConsentMongoRepository pispConsentMongoRepository;
	private long idempotencyDuration;
	private long paymentSetupResourceExpiry;

	@Autowired
	public PaymentSubmissionProcessingAdapterImpl(
			@Value("${app.resource.paymentSetupResourceExpiry}") String paymentSetupResourceExpiry,
			@Value("${app.idempotency.durationForPaymentSubmission}") String idempotencyDuration) {
		this.paymentSetupResourceExpiry = PispUtilities.getMilliSeconds(paymentSetupResourceExpiry);
		this.idempotencyDuration = PispUtilities.getMilliSeconds(idempotencyDuration);
	}

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> preSubmissionProcessing(T request,
			CustomPaymentSetupPlatformDetails platformResourceDetails, String consentIdInRequest) {

		Map<String, Object> platformResourcesMap = new HashMap<>();

		/* Validate Request */
		transformer.paymentSubmissionRequestTransformer(request);
		paymentCustomValidator.validatePaymentsPOSTRequest(request);
		

		/*
		 * If requestId of Token,it is a paymentId for which TPP got the
		 * consent, does not match with consentId of submissionRequest then
		 * throw an exception as '403'
		 */
		if (!consentIdInRequest.equalsIgnoreCase(reqHeaderAtrributes.getToken().getRequestId()))
			throw PSD2Exception
					.populatePSD2Exception(ErrorCodeEnum.PISP_TOKEN_PAYMENT_ID_NOT_MATCHED_WITH_SUBMISSION_PAYMENT_ID);

		/*
		 * If no consent created for provided consent id, throw an exception as
		 * 'Bad Request'
		 */
		PaymentConsentsPlatformResource paymentConsentPlatformResource = paymentConsentsPlatformAdapter
				.retrievePaymentSetupPlatformResource(consentIdInRequest);

		if (paymentConsentPlatformResource == null)
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_RESOURCE_NOTFOUND,
					ErrorMapKeys.EXPECTED_PAYMENT_SETUP_ID));

		/* Check for idempotent request */
		PaymentsPlatformResource paymentsPlatformResource = paymentsPlatformAdapter
				.getIdempotentPaymentsPlatformResource(idempotencyDuration,
						platformResourceDetails.getPaymentType().getPaymentType());

		/* Fresh Req */
		if (paymentsPlatformResource == null) {
			
			/*
			 * Checking whether consentId is already used for other payment
			 * initiation or not. If it is already used then return an error to
			 * PISP from submission Platform.
			 */
			PaymentsPlatformResource existedPaymentsPlatformResource = paymentsPlatformAdapter
					.retrievePaymentsResourceByConsentId(consentIdInRequest);
			if (existedPaymentsPlatformResource != null && existedPaymentsPlatformResource.getSubmissionId() != null)
				throw PSD2Exception.populatePSD2Exception(
						new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_RESOURCE_NOTFOUND, ErrorMapKeys.INVALID_PAYMENT_ID));

			paymentConsentPlatformResourceExceptionHandling(paymentConsentPlatformResource, consentIdInRequest);

		}

		/* Idemp Req */
		else {
			/*
			 * If idempotent and request is completed, then continue with
			 * retrieving stage resource and send response to TPP
			 */
			
			idempotentRequestValidations(paymentsPlatformResource, paymentConsentPlatformResource, consentIdInRequest);
		}
		platformResourcesMap.put(PaymentConstants.CONSENT, paymentConsentPlatformResource);
		platformResourcesMap.put(PaymentConstants.SUBMISSION, paymentsPlatformResource);
		return platformResourcesMap;
	}

	private void paymentConsentPlatformResourceExceptionHandling(
			PaymentConsentsPlatformResource paymentConsentsPlatformResource, String consentId) {

		/*
		 * Checking whether payment consent resource's status is 'Authorised' or
		 * not. If status is differ then return an error to PISP.
		 */
		if (!PaymentStatusEnum.AUTHORISED.getStatusCode().equalsIgnoreCase(paymentConsentsPlatformResource.getStatus()))
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(
					OBErrorCodeEnum.UK_OBIE_RESOURCE_INVALIDCONSENTSTATUS, ErrorMapKeys.INVALID_PAYMENT_SETUP_STATUS));

		/*
		 * Before processing this request, checking whether consent resource is
		 * expired, if expired then revoking consent and token
		 */
		if (isStagedDomesticPaymentResourceExpired(paymentConsentsPlatformResource)) {
			/* Revoke Consent and Access token */
			revokeDomesticConsentAndAccessToken(consentId);
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_RESOURCE_NOTFOUND,
					ErrorMapKeys.PAYMENT_SETUP_RESOURCE_EXPIRED));
		}
	}

	private void idempotentRequestValidations(PaymentsPlatformResource paymentsPlatformResource,
			PaymentConsentsPlatformResource paymentConsentsPlatformResource, String consentId) {
		/*
		 * Checking whether existing payments resource has same consentId as
		 * provided in request.
		 */
		if (!consentId.equalsIgnoreCase(paymentsPlatformResource.getPaymentConsentId()))
			throw PSD2Exception.populatePSD2Exception(
					new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_RESOURCE_NOTFOUND, ErrorMapKeys.PAYMENT_ID_MISMATCH));

		/*
		 * If idempotent but request is still incomplete, before processing this
		 * request, checking whether consent resource is expired, if expired
		 * then revoking consent and token
		 */
		if (paymentsPlatformResource.getProccessState().equals(PaymentConstants.INCOMPLETE)
				&& isStagedDomesticPaymentResourceExpired(paymentConsentsPlatformResource)) {
			/* Revoke Consent and Access token */
			revokeDomesticConsentAndAccessToken(consentId);
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_RESOURCE_NOTFOUND,
					ErrorMapKeys.PAYMENT_SETUP_RESOURCE_EXPIRED));
		}

		/*
		 * If idempotent but request is previously Aborted by bank, then throw
		 * 500 exception to TPP
		 */
		if (paymentsPlatformResource.getProccessState().equals(PaymentConstants.ABORT))
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
					InternalServerErrorMessage.TECHNICAL_ERROR_PLATFORM_SUBMISSION_CREATION));
	}

	private void revokeDomesticConsentAndAccessToken(String paymentId) {
		PispConsent pispConsent = pispConsentAdapter.retrieveConsentByPaymentId(paymentId,
				ConsentStatusEnum.AUTHORISED);
		if (pispConsent != null) {

			pispConsent.setStatus(ConsentStatusEnum.EXPIRED);
			pispConsentMongoRepository.save(pispConsent);
		}
	}

	/*
	 * Checking the validity of paymentId. If its expired then throw the
	 * exception
	 */
	private boolean isStagedDomesticPaymentResourceExpired(
			PaymentConsentsPlatformResource paymentConsentsPlatformResource) {

		String paymentResourceCreatedAt = paymentConsentsPlatformResource.getCreatedAt();
		DateTime paymentResourceCreatedAtDate = new DateTime(paymentResourceCreatedAt);
		DateTime currentTimeStamp = new DateTime();

		boolean expiryStatus = Boolean.FALSE;
		if (currentTimeStamp.getMillis() - paymentResourceCreatedAtDate.getMillis() > paymentSetupResourceExpiry)
			expiryStatus = Boolean.TRUE;

		return expiryStatus;
	}

	@Override
	public PaymentsPlatformResource createInitialPaymentsPlatformResource(String paymentConsentId,
			String paymentSetupVersion, PaymentTypeEnum paymentTypeEnum) {
		CustomPaymentStageIdentifiers platformIdentifiers = new CustomPaymentStageIdentifiers();
		platformIdentifiers.setPaymentConsentId(paymentConsentId);
		platformIdentifiers.setPaymentSetupVersion(paymentSetupVersion);
		platformIdentifiers.setPaymentTypeEnum(paymentTypeEnum);
		String creationDateTime = PispUtilities.getCurrentDateInISOFormat();
		return paymentsPlatformAdapter.createInitialPaymentsPlatformResource(platformIdentifiers, creationDateTime);
	}

	@Override
	public U postPaymentProcessFlows(Map<String, Object> paymentsPlatformResourceMap, U paymentsFoundationResponse,
			ProcessExecutionStatusEnum processExecutionStatusEnum, String submissionId,
			OBExternalStatus2Code multiAuthStatus, String methodType) {

		String paymentStatus;
		String processState;

		PaymentsPlatformResource paymentsPlatformResource = (PaymentsPlatformResource) paymentsPlatformResourceMap
				.get(PaymentConstants.SUBMISSION);
		PaymentConsentsPlatformResource paymentConentsPlatformResource = (PaymentConsentsPlatformResource) paymentsPlatformResourceMap
				.get(PaymentConstants.CONSENT);

		if (methodType.equals(RequestMethod.GET.toString())
				|| paymentsPlatformResource.getProccessState().equals(PaymentConstants.COMPLETED)) {
			return transformAndValidate(paymentsFoundationResponse, paymentsPlatformResourceMap, methodType);
		}

		if (processExecutionStatusEnum == null)
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
					InternalServerErrorMessage.TECHNICAL_ERROR));

		if (processExecutionStatusEnum == ProcessExecutionStatusEnum.FAIL) {
			if (submissionId == null)
				throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
						InternalServerErrorMessage.PAYMENT_SUBMISSION_CREATION_FAILED));
			// update sub res and consent res
			paymentStatus = getPaymentStatusInitiationFailed(paymentsPlatformResource);
			processState = PaymentConstants.COMPLETED;
			updatePaymentsResource(paymentsPlatformResource, submissionId, paymentStatus, processState);
			updateConsentResourceWithConsumed(paymentConentsPlatformResource);
		}

		if (processExecutionStatusEnum == ProcessExecutionStatusEnum.PASS) {
			if (submissionId == null)
				throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
						InternalServerErrorMessage.PAYMENT_SUBMISSION_CREATION_FAILED));
			// update sub res and consent res
			processState = PaymentConstants.COMPLETED;
			paymentStatus = getPaymentStatusFromMultiAuthStatus(multiAuthStatus,
					paymentConentsPlatformResource.getPaymentType());
			updatePaymentsResource(paymentsPlatformResource, submissionId, paymentStatus, processState);
			updateConsentResourceWithConsumed(paymentConentsPlatformResource);
		}

		if (processExecutionStatusEnum == ProcessExecutionStatusEnum.INCOMPLETE) {
			if (submissionId == null)
				throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
						InternalServerErrorMessage.PAYMENT_SUBMISSION_CREATION_FAILED));
			// update sub res and throw exception
		    paymentStatus = getPaymentStatusInititationPending(paymentsPlatformResource);
			processState = PaymentConstants.COMPLETED;
			updatePaymentsResource(paymentsPlatformResource, submissionId, paymentStatus, processState);
			updateConsentResourceWithConsumed(paymentConentsPlatformResource);
		}

		if (processExecutionStatusEnum == ProcessExecutionStatusEnum.ABORT) {
			// update sub res and throw exception
			paymentStatus = getPaymentStatusInitiationFailed(paymentsPlatformResource);
			processState = PaymentConstants.ABORT;
			updatePaymentsResource(paymentsPlatformResource, submissionId, paymentStatus, processState);
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
					InternalServerErrorMessage.PAYMENT_SUBMISSION_CREATION_FAILED));
		}
		return transformAndValidate(paymentsFoundationResponse, paymentsPlatformResourceMap, methodType);
	}

	private String getPaymentStatusInititationPending(PaymentsPlatformResource paymentsPlatformResource) {
		String paymentStatus = OBTransactionIndividualStatus1Code.PENDING.toString();
		if (paymentsPlatformResource.getPaymentType().equals(PaymentTypeEnum.FILE_PAY.getPaymentType())
				|| paymentsPlatformResource.getPaymentType().equals(PaymentTypeEnum.DOMESTIC_ST_ORD.getPaymentType())
				|| paymentsPlatformResource.getPaymentType()
						.equals(PaymentTypeEnum.INTERNATIONAL_SCH_PAY.getPaymentType())
				|| paymentsPlatformResource.getPaymentType().equals(PaymentTypeEnum.DOMESTIC_SCH_PAY.getPaymentType()))
			paymentStatus = OBExternalStatus1Code.INITIATIONPENDING.toString();

		return paymentStatus;
	}

	private String getPaymentStatusInitiationFailed(PaymentsPlatformResource paymentsPlatformResource) {
		String paymentStatus = OBTransactionIndividualStatus1Code.REJECTED.toString();
		if (paymentsPlatformResource.getPaymentType().equals(PaymentTypeEnum.FILE_PAY.getPaymentType())
				|| paymentsPlatformResource.getPaymentType().equals(PaymentTypeEnum.DOMESTIC_ST_ORD.getPaymentType())
				|| paymentsPlatformResource.getPaymentType()
						.equals(PaymentTypeEnum.INTERNATIONAL_SCH_PAY.getPaymentType())
				|| paymentsPlatformResource.getPaymentType().equals(PaymentTypeEnum.DOMESTIC_SCH_PAY.getPaymentType()))
			paymentStatus = OBExternalStatus1Code.INITIATIONFAILED.toString();

		return paymentStatus;
	}

	private String getPaymentStatusFromMultiAuthStatus(OBExternalStatus2Code multiAuthStatus, String paymentType) {
		String paymentStatus = null;
		if (multiAuthStatus == OBExternalStatus2Code.AUTHORISED)
			if (paymentType.equals(PaymentTypeEnum.FILE_PAY.getPaymentType())
					|| paymentType.equals(PaymentTypeEnum.DOMESTIC_ST_ORD.getPaymentType())
					|| paymentType.equals(PaymentTypeEnum.DOMESTIC_SCH_PAY.getPaymentType())
					|| paymentType.equals(PaymentTypeEnum.INTERNATIONAL_SCH_PAY.getPaymentType()))
				paymentStatus = OBExternalStatus1Code.INITIATIONCOMPLETED.toString();
			else
				paymentStatus = OBTransactionIndividualStatus1Code.ACCEPTEDSETTLEMENTINPROCESS.toString();
		else if (multiAuthStatus == OBExternalStatus2Code.AWAITINGFURTHERAUTHORISATION)
			if (paymentType.equals(PaymentTypeEnum.FILE_PAY.getPaymentType())
					|| paymentType.equals(PaymentTypeEnum.DOMESTIC_ST_ORD.getPaymentType())
					|| paymentType.equals(PaymentTypeEnum.INTERNATIONAL_SCH_PAY.getPaymentType())
					|| paymentType.equals(PaymentTypeEnum.DOMESTIC_SCH_PAY.getPaymentType()))
				paymentStatus = OBExternalStatus1Code.INITIATIONPENDING.toString();
			else
				paymentStatus = OBTransactionIndividualStatus1Code.PENDING.toString();

		else if (multiAuthStatus == OBExternalStatus2Code.REJECTED)
			if (paymentType.equals(PaymentTypeEnum.FILE_PAY.getPaymentType())
					|| paymentType.equals(PaymentTypeEnum.DOMESTIC_ST_ORD.getPaymentType())
					|| paymentType.equals(PaymentTypeEnum.INTERNATIONAL_SCH_PAY.getPaymentType())
					|| paymentType.equals(PaymentTypeEnum.DOMESTIC_SCH_PAY.getPaymentType()))
				paymentStatus = OBExternalStatus1Code.INITIATIONFAILED.toString();
			else
				paymentStatus = OBTransactionIndividualStatus1Code.REJECTED.toString();
		return paymentStatus;
	}

	private U transformAndValidate(U response, Map<String, Object> paymentsPlatformResourceMap, String methodType) {
		U transformerResponse = (U) transformer.paymentSubmissionResponseTransformer(response, paymentsPlatformResourceMap,
				methodType);
		paymentCustomValidator.validatePaymentsResponse(transformerResponse);
		return transformerResponse;
	}

	/* Log here for reporting : consent platform status change */
	/* Update platform resource with consumed status */
	public void updateConsentResourceWithConsumed(PaymentConsentsPlatformResource consentPlatformResource) {
		LOG.info("{\"Enter\":\"{}\",\"{}\"}",
				"com.capgemini.psd2.pisp.processing.adapter.service.impl.updateConsentResourceWithConsumed()",
				loggerUtils.populateLoggerData("updateConsentResourceWithConsumed"));

		consentPlatformResource.setStatus(OBExternalConsentStatus1Code.CONSUMED.toString());
		consentPlatformResource.setStatusUpdateDateTime(PispUtilities.getCurrentDateInISOFormat());
		paymentConsentsPlatformAdapter.updatePaymentSetupPlatformResource(consentPlatformResource);

		LOG.info("{\"Exit\":\"{}\",\"{}\",\"channelId\":\"{}\",\"updatedConsentPlatformResource\":{}}",
				"com.capgemini.psd2.pisp.processing.adapter.service.impl.updateConsentResourceWithConsumed()",
				loggerUtils.populateLoggerData("updateConsentResourceWithConsumed"),
				reqHeaderAtrributes.getToken().getSeviceParams().get(PSD2Constants.CHANNEL_NAME),
				JSONUtilities.getJSONOutPutFromObject(consentPlatformResource));

	}

	private void updatePaymentsResource(PaymentsPlatformResource paymentsPlatformResource, String submissionId,
			String paymentStatus, String processState) {
		if (submissionId != null)
			paymentsPlatformResource.setSubmissionId(submissionId);
		paymentsPlatformResource.setStatusUpdateDateTime(paymentsPlatformResource.getCreatedAt());
		paymentsPlatformResource.setProccessState(processState);
		paymentsPlatformResource.setStatus(paymentStatus);
		paymentsPlatformAdapter.updatePaymentsPlatformResource(paymentsPlatformResource);
	}

	@Override
	public Map<String, Object> prePaymentProcessGETFlows(String submissionId, PaymentTypeEnum paymentTypeEnum) {

		PaymentsPlatformResource submissionPlatformResource = paymentsPlatformAdapter
				.retrievePaymentsPlatformResourceByPaySubmissionIdAndType(submissionId,
						paymentTypeEnum.getPaymentType());

		/* Check for backward compatible domestic payments resource */
		if (submissionPlatformResource == null && paymentTypeEnum == PaymentTypeEnum.DOMESTIC_PAY) {
			submissionPlatformResource = paymentsPlatformAdapter
					.retrievePaymentsPlatformResourceByBackwardSubmissionId(submissionId);
		}
		if (submissionPlatformResource == null)
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_RESOURCE_NOTFOUND,
					ErrorMapKeys.MISSING_PAYMENT_SUBMISSION_ID));

		if (!reqHeaderAtrributes.getTppCID().equalsIgnoreCase(submissionPlatformResource.getTppCID()))
			throw PSD2Exception
					.populatePSD2Exception(ErrorCodeEnum.PISP_NO_PAYMENT_SUBMISSION_RESOURCES_FOUND_AGAINST_TPP);
		Map<String, Object> platformResourcesMap = new HashMap<>();

		/* Added for fix for 2447 Submission GET */
		PaymentConsentsPlatformResource paymentConsentPlatformResource = paymentConsentsPlatformAdapter
				.retrievePaymentSetupPlatformResource(submissionPlatformResource.getPaymentConsentId());
		platformResourcesMap.put(PaymentConstants.CONSENT, paymentConsentPlatformResource);
		platformResourcesMap.put(PaymentConstants.SUBMISSION, submissionPlatformResource);
		return platformResourcesMap;
	}
}