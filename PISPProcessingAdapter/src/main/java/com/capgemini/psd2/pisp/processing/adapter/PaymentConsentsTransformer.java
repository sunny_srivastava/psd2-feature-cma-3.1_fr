package com.capgemini.psd2.pisp.processing.adapter;

import com.capgemini.psd2.pisp.domain.PaymentConsentsPlatformResource;

public interface PaymentConsentsTransformer<T,U> {

	//Interfaces for Payments Consents Transformer v3.0 API 
	public T paymentConsentsResponseTransformer(T paymentConsentsResponse, PaymentConsentsPlatformResource paymentSetupPlatformResource, String methodType);
	
	// Interfaces for Payments Consents Transformer v3.0 API
	public U paymentConsentRequestTransformer(U paymentConsentRequest);
}
