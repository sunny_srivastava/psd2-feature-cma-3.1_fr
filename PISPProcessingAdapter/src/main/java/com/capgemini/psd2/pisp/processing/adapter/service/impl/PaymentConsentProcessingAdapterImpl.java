package com.capgemini.psd2.pisp.processing.adapter.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.ErrorMapKeys;
import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.InternalServerErrorMessage;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.domain.CustomPaymentSetupPlatformDetails;
import com.capgemini.psd2.pisp.domain.OBExternalConsentStatus1Code;
import com.capgemini.psd2.pisp.domain.PaymentConsentsPlatformResource;
import com.capgemini.psd2.pisp.domain.PaymentResponseInfo;
import com.capgemini.psd2.pisp.domain.PaymentRetrieveGetRequest;
import com.capgemini.psd2.pisp.enums.PaymentTypeEnum;
import com.capgemini.psd2.pisp.enums.ProcessConsentStatusEnum;
import com.capgemini.psd2.pisp.payment.setup.platform.adapter.PaymentSetupPlatformAdapter;
import com.capgemini.psd2.pisp.processing.adapter.PaymentConsentsCustomValidator;
import com.capgemini.psd2.pisp.processing.adapter.PaymentConsentsTransformer;
import com.capgemini.psd2.pisp.processing.adapter.service.PaymentConsentProcessingAdapter;
import com.capgemini.psd2.pisp.utilities.PispUtilities;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Component
public class PaymentConsentProcessingAdapterImpl<T, U> implements PaymentConsentProcessingAdapter<T, U> {

	private static final Logger LOG = LoggerFactory.getLogger(PaymentConsentProcessingAdapterImpl.class);
	
	@SuppressWarnings("rawtypes")
	@Autowired
	@Qualifier("paymentConsentRoutingValidator")
	private PaymentConsentsCustomValidator paymentCustomValidator;

	@SuppressWarnings("rawtypes")
	@Autowired
	@Qualifier("responseTransformRoutingBean")
	private PaymentConsentsTransformer transformer;

	@Autowired
	private PaymentSetupPlatformAdapter paymentSetupPlatformAdapter;

	@Autowired
	private RequestHeaderAttributes reqHeaderAtrributes;

	private long idempotencyDuration;

	@Autowired
	public PaymentConsentProcessingAdapterImpl(
			@Value("${app.idempotency.durationForPaymentConsents}") String idempotencyDuration) {
		this.idempotencyDuration = PispUtilities.getMilliSeconds(idempotencyDuration);
	}

	@SuppressWarnings({ "unchecked" })
	@Override
	public PaymentConsentsPlatformResource preConsentProcessFlows(T request,
			CustomPaymentSetupPlatformDetails customPaymentSetupPlatformDetails) {
		try {
			transformer.paymentConsentRequestTransformer(request);
			paymentCustomValidator.validateConsentRequest(request);
		} catch (PSD2Exception psd2Exception) {
			createInvalidPaymentConsentPlatformResource(customPaymentSetupPlatformDetails);

			throw psd2Exception;
		}
		return performIdempotencyCheckAndSetup(customPaymentSetupPlatformDetails, request);

	}

	private PaymentConsentsPlatformResource performIdempotencyCheckAndSetup(
			CustomPaymentSetupPlatformDetails customPaymentSetupPlatformDetails, T request) {
		PaymentConsentsPlatformResource paymentConsentsPlatformResponse = paymentSetupPlatformAdapter
				.getIdempotentPaymentSetupPlatformResource(
						customPaymentSetupPlatformDetails.getPaymentType().getPaymentType(), idempotencyDuration);

		/* Fresh Request */
		if (paymentConsentsPlatformResponse == null) {
			/* Create platform resource without consent-id */
			return createPlatformResource(customPaymentSetupPlatformDetails);
		}
		/* IdempotentRequest */
		else {
			return paymentConsentsPlatformResponse;
		}
	}

	private PaymentConsentsPlatformResource createPlatformResource(
			CustomPaymentSetupPlatformDetails customPaymentSetupPlatformDetails) {
		String setupCreationDate = PispUtilities.getCurrentDateInISOFormat();
		CustomPaymentSetupPlatformDetails paymentConsentsPlatformDetails = populatePaymentConsentPlatformDetails(
				customPaymentSetupPlatformDetails);

		return paymentSetupPlatformAdapter.createPaymentSetupPlatformResource(paymentConsentsPlatformDetails,
				setupCreationDate);
	}

	private void createInvalidPaymentConsentPlatformResource(
			CustomPaymentSetupPlatformDetails customPaymentSetupPlatformDetails) {
		PaymentResponseInfo validationDetails = new PaymentResponseInfo();
		validationDetails.setConsentValidationStatus(OBExternalConsentStatus1Code.REJECTED);
		validationDetails.setIdempotencyRequest(String.valueOf(Boolean.FALSE));
		CustomPaymentSetupPlatformDetails standingConsentsPlatformDetails = populatePaymentConsentPlatformDetails(
				customPaymentSetupPlatformDetails);
		paymentSetupPlatformAdapter.createInvalidPaymentSetupPlatformResource(standingConsentsPlatformDetails,
				validationDetails, PispUtilities.getCurrentDateInISOFormat());
	}

	private CustomPaymentSetupPlatformDetails populatePaymentConsentPlatformDetails(
			CustomPaymentSetupPlatformDetails customPaymentSetupPlatformDetails) {
		CustomPaymentSetupPlatformDetails domesticConsentsPlatformDetails = new CustomPaymentSetupPlatformDetails();
		String tppDebtorDetailsStatus = String.valueOf(customPaymentSetupPlatformDetails.getTppDebtorDetails());
		String tppDebtorNameDetailsStatus = String.valueOf(customPaymentSetupPlatformDetails.getTppDebtorNameDetails());
		domesticConsentsPlatformDetails.setTppDebtorDetails(tppDebtorDetailsStatus);
		domesticConsentsPlatformDetails.setTppDebtorNameDetails(tppDebtorNameDetailsStatus);
		domesticConsentsPlatformDetails.setPaymentType(customPaymentSetupPlatformDetails.getPaymentType());
		domesticConsentsPlatformDetails.setSetupCmaVersion(customPaymentSetupPlatformDetails.getSetupCmaVersion());
		return domesticConsentsPlatformDetails;
	}

	@SuppressWarnings("unchecked")
	@Override
	public U postConsentProcessFlows(PaymentResponseInfo stageInfo, U response,
			PaymentConsentsPlatformResource paymentConsentsPlatformResponse, String methodType) {

		if (stageInfo.getRequestType().equals(PSD2Constants.PAYMENT_RESOURCE_NON_IDEMPOTENT)) {
			if (NullCheckUtils.isNullOrEmpty(stageInfo.getPaymentConsentId())
					|| NullCheckUtils.isNullOrEmpty(stageInfo.getConsentProcessStatus())) {
				updateAndInvalidatePlatformResourse(paymentConsentsPlatformResponse);
				throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
						InternalServerErrorMessage.PISP_PAYMENT_SETUP_CREATION_FAILED));
			}
			stageInfo.setIdempotencyRequest(String.valueOf(Boolean.TRUE));

			if (stageInfo.getConsentProcessStatus() == ProcessConsentStatusEnum.FAIL) {
				stageInfo.setConsentValidationStatus(OBExternalConsentStatus1Code.REJECTED);
			} else {
				stageInfo.setConsentValidationStatus(OBExternalConsentStatus1Code.AWAITINGAUTHORISATION);
			}

			/* Update Platform resource with consent-id and final status */
			updatePlatformResource(paymentConsentsPlatformResponse, stageInfo);
		}
		U transformerResponse = (U) transformer.paymentConsentsResponseTransformer(response, paymentConsentsPlatformResponse,
				methodType);
		paymentCustomValidator.validatePaymentConsentResponse(transformerResponse);
		return transformerResponse;
	}

	private void updateAndInvalidatePlatformResourse(PaymentConsentsPlatformResource paymentConsentsPlatformResponse) {
		paymentConsentsPlatformResponse.setIdempotencyRequest(String.valueOf(Boolean.FALSE));
		paymentConsentsPlatformResponse.setStatus(OBExternalConsentStatus1Code.REJECTED.name());
		paymentSetupPlatformAdapter.updatePaymentSetupPlatformResource(paymentConsentsPlatformResponse);
	}

	private PaymentConsentsPlatformResource updatePlatformResource(
			PaymentConsentsPlatformResource paymentConsentsPlatformResource, PaymentResponseInfo stageResponse) {

		if (!NullCheckUtils.isNullOrEmpty(stageResponse.getPaymentConsentId())
				&& paymentSetupPlatformAdapter.retrievePaymentSetupPlatformResource(stageResponse.getPaymentConsentId(),
						stageResponse.getPaymentConsentId()) != null)
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
					InternalServerErrorMessage.PISP_DUPLICATE_PAYMENT_ID));

		paymentConsentsPlatformResource.setPaymentConsentId(stageResponse.getPaymentConsentId());
		paymentConsentsPlatformResource.setStatus(stageResponse.getConsentValidationStatus().toString());
		paymentConsentsPlatformResource.setStatusUpdateDateTime(paymentConsentsPlatformResource.getCreatedAt());
		paymentConsentsPlatformResource.setIdempotencyRequest(stageResponse.getIdempotencyRequest());

		try {
			return paymentSetupPlatformAdapter.updatePaymentSetupPlatformResource(paymentConsentsPlatformResource);
		} catch (DataAccessResourceFailureException exception) {
			LOG.error("DataAccessResourceFailureException Occurred: " +exception);
			throw PSD2Exception.populatePSD2Exception(
					new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR, exception.getMessage()));
		}
	}

	@Override
	public PaymentConsentsPlatformResource preConsentProcessGETFlow(PaymentRetrieveGetRequest paymentRetrieveRequest) {
		paymentCustomValidator.validateConsentsGETRequest(paymentRetrieveRequest);

		PaymentConsentsPlatformResource paymentConsentsPlatformResponse = paymentSetupPlatformAdapter.retrievePaymentSetupPlatformResourceByIdAndType(
				paymentRetrieveRequest.getConsentId(), paymentRetrieveRequest.getPaymentTypeEnum().getPaymentType());

		/* For backward compatibility in Domestic Payment Consents */
		if (paymentConsentsPlatformResponse == null
				&& paymentRetrieveRequest.getPaymentTypeEnum() == PaymentTypeEnum.DOMESTIC_PAY) {
			paymentConsentsPlatformResponse = paymentSetupPlatformAdapter
					.retrievePaymentSetupPlatformResourceByBackwardPaymentId(paymentRetrieveRequest.getConsentId());
		}

		if (paymentConsentsPlatformResponse == null)
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_RESOURCE_NOTFOUND,
					ErrorMapKeys.EXPECTED_PAYMENT_SETUP_ID));

		if (!paymentConsentsPlatformResponse.getTppCID().equalsIgnoreCase(reqHeaderAtrributes.getTppCID()))
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.PISP_NO_PAYMENT_SETUP_RESOURCE_FOUND_AGAINST_TPP);

		return paymentConsentsPlatformResponse;
	}
}