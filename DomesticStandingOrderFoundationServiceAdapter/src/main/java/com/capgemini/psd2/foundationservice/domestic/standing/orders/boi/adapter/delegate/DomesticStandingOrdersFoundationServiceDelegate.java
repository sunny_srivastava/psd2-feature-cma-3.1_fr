package com.capgemini.psd2.foundationservice.domestic.standing.orders.boi.adapter.delegate;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.adapter.exceptions.AdapterErrorCodeEnum;
import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.boi.adapter.raml.domain.BrandCode3;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.boi.adapter.raml.domain.StandingOrderInstructionComposite;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.boi.adapter.raml.domain.StandingOrderInstructionProposal;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.boi.adapter.transformer.DomesticStandingOrdersFoundationServiceTransformer;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.pisp.domain.CustomDStandingOrderPOSTRequest;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Component
public class DomesticStandingOrdersFoundationServiceDelegate {

	@Autowired
	private DomesticStandingOrdersFoundationServiceTransformer domesticPaymentConsentsFoundationServiceTransformer;

	@Value("${foundationService.apiChannelCode:#{x-api-channel-code}}")
	private String apiChannelCode;

	@Value("${foundationService.sourceUserReqHeader:#{x-api-source-user}}")
	private String sourceUserReqHeader;

	@Value("${foundationService.transactioReqHeader:#{x-api-transaction-id}}")
	private String transactioReqHeader;

	@Value("${foundationService.correlationMuleReqHeader:#{x-api-correlation-id}}")
	private String correlationMuleReqHeader;

	@Value("${foundationService.sourcesystem:#{x-api-source-system}}")
	private String sourcesystem;

	@Value("${foundationService.apiChannelCode:#{x-api-party-source-id-number}}")
	private String partySourceId;

	// Added for 1.0.66 RAML Upgrade
	@Value("${foundationService.apiChannelBrand:#{X-API-CHANNEL-BRAND}}")
	private String apiChannelBrand;

	// Added for 1.0.66 RAML Upgrade
	@Value("${foundationService.systemApiVersion:#{X-SYSTEM-API-VERSION}}")
	private String systemApiVersion;

	@Value("${foundationService.domesticStandingOrderBaseURL}")
	private String domesticStandingOrderBaseURL;
	
	@Value("${foundationService.domesticStandingOrderEndURL}")
	private String domesticStandingOrderEndURL;

	public HttpHeaders createPaymentRequestHeadersForProcess(Map<String, String> params) {

		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.add(correlationMuleReqHeader, "");
		httpHeaders.add(sourcesystem, "PSD2API");
		if (!NullCheckUtils.isNullOrEmpty(params.get(PSD2Constants.USER_IN_REQ_HEADER))) {
			httpHeaders.add(sourceUserReqHeader, params.get(PSD2Constants.USER_IN_REQ_HEADER));
		}
		if (!NullCheckUtils.isNullOrEmpty(params.get(PSD2Constants.CORRELATION_REQ_HEADER))) {
			httpHeaders.add(transactioReqHeader, params.get(PSD2Constants.CORRELATION_REQ_HEADER));
		}
		if (!NullCheckUtils.isNullOrEmpty(params.get(PSD2Constants.CHANNEL_IN_REQ_HEADER))) {
			httpHeaders.add(apiChannelCode, params.get(PSD2Constants.CHANNEL_IN_REQ_HEADER).toUpperCase());
		}
		if (!NullCheckUtils.isNullOrEmpty(params.get(PSD2Constants.PARTY_IDENTIFIER))) {
			httpHeaders.add(partySourceId, params.get(PSD2Constants.PARTY_IDENTIFIER));

		}
		httpHeaders.add(systemApiVersion, "3.0");
		if (!NullCheckUtils.isNullOrEmpty(params.get(PSD2Constants.TENANT_ID))) {

			if (params.get(PSD2Constants.TENANT_ID).equalsIgnoreCase("BOIUK")) {
				httpHeaders.add(apiChannelBrand, BrandCode3.NIGB.toString());
			} else if (params.get(PSD2Constants.TENANT_ID).equalsIgnoreCase("BOIROI")) {
				httpHeaders.add(apiChannelBrand, BrandCode3.ROI.toString());
			}
		}
		return httpHeaders;
	}

	public HttpHeaders createPaymentRequestHeadersForGet(Map<String, String> params) {

		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.add(correlationMuleReqHeader, "");
		httpHeaders.add(sourcesystem, "PSD2API");
		if (!NullCheckUtils.isNullOrEmpty(params.get(PSD2Constants.USER_IN_REQ_HEADER))) {
			httpHeaders.add(sourceUserReqHeader, params.get(PSD2Constants.USER_IN_REQ_HEADER));
		}
		if (!NullCheckUtils.isNullOrEmpty(params.get(PSD2Constants.CORRELATION_REQ_HEADER))) {
			httpHeaders.add(transactioReqHeader, params.get(PSD2Constants.CORRELATION_REQ_HEADER));
		}
		if (!NullCheckUtils.isNullOrEmpty(params.get(PSD2Constants.CHANNEL_IN_REQ_HEADER))) {
			httpHeaders.add(apiChannelCode, params.get(PSD2Constants.CHANNEL_IN_REQ_HEADER).toUpperCase());
		}
		if (!NullCheckUtils.isNullOrEmpty(params.get(PSD2Constants.PARTY_IDENTIFIER))) {
			httpHeaders.add(partySourceId, params.get(PSD2Constants.PARTY_IDENTIFIER));

		}
		httpHeaders.add(systemApiVersion, "3.0");
		if (!NullCheckUtils.isNullOrEmpty(params.get(PSD2Constants.TENANT_ID))) {

			if (params.get(PSD2Constants.TENANT_ID).equalsIgnoreCase("BOIUK")) {
				httpHeaders.add(apiChannelBrand, BrandCode3.NIGB.toString());
			} else if (params.get(PSD2Constants.TENANT_ID).equalsIgnoreCase("BOIROI")) {
				httpHeaders.add(apiChannelBrand, BrandCode3.ROI.toString());
			}
		}
		return httpHeaders;
	}

	public String postPaymentFoundationServiceURL(String setupVersion) {

		if (NullCheckUtils.isNullOrEmpty(setupVersion)) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.NO_ACCOUNT_DATA_FOUND);
		}

		return domesticStandingOrderBaseURL + "/" + "v" + setupVersion
				+ "/" + domesticStandingOrderEndURL;
	}

	public String getPaymentFoundationServiceURL(String paymentInstuctionProposalId, String setupVersion) {

		if (NullCheckUtils.isNullOrEmpty(setupVersion)) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.NO_ACCOUNT_DATA_FOUND);
		}

		if (NullCheckUtils.isNullOrEmpty(paymentInstuctionProposalId)) {
			throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.NO_ACCOUNT_DATA_FOUND);
		}

		return domesticStandingOrderBaseURL + "/" + "v" + setupVersion + "/"
				+ domesticStandingOrderEndURL + "/" + paymentInstuctionProposalId;
	}

	public StandingOrderInstructionProposal transformDomesticStandingOrdersResponseFromAPIToFDForInsert(
			CustomDStandingOrderPOSTRequest paymentConsentsRequest, Map<String, String> params) {

		return domesticPaymentConsentsFoundationServiceTransformer
				.transformDomesticStandingOrdersResponseFromAPIToFDForInsert(paymentConsentsRequest,params);
	}
}
