package com.capgemini.psd2.foundationservice.domestic.standing.orders.boi.adapter.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.adapter.exceptions.AdapterErrorCodeEnum;
import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.adapter.exceptions.AdapterOBErrorCodeEnum;
import com.capgemini.psd2.adapter.exceptions.domain.CommonResponse;
import com.capgemini.psd2.adapter.exceptions.domain.ValidationWrapper;
import com.capgemini.psd2.adapter.utility.AdapterUtility;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.boi.adapter.constants.DomesticStandingOrdersFoundationServiceConstants;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.boi.adapter.raml.domain.StandingOrderInstructionComposite;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.boi.adapter.raml.domain.StandingOrderInstructionProposal;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.RestClientSync;
import com.capgemini.psd2.utilities.NullCheckUtils;
import com.google.gson.Gson;


@Component
public class DomesticStandingOrdersFoundationServiceClient {
	
	@Autowired
	@Qualifier("restClientFoundation")
	private RestClientSync restClient;
	
	CommonResponse commonResponse = null;

	@Autowired
	private AdapterUtility adapterUtility;

	
	public StandingOrderInstructionComposite restTransportForDomesticStandingOrdersServicePost(RequestInfo requestInfo,
			StandingOrderInstructionProposal standingInstructionProposalRequest, Class<StandingOrderInstructionComposite> responseType,
			HttpHeaders httpHeaders) {
		try {
		return restClient.callForPost(requestInfo, standingInstructionProposalRequest, responseType, httpHeaders);
		
		} catch (AdapterException e) {
			if ((String.valueOf(e.getFoundationError()).trim()
					.equals(DomesticStandingOrdersFoundationServiceConstants.MULE_OUT_OF_BOX_POLICY_ERROR1))
					|| (String.valueOf(e.getFoundationError()).trim()
							.equals(DomesticStandingOrdersFoundationServiceConstants.MULE_OUT_OF_BOX_POLICY_ERROR2))) {
				throw AdapterException.populatePSD2Exception(e.getFoundationError().toString(),
						AdapterOBErrorCodeEnum.OBIE_TECHNICAL_ERROR,e);
			}
			commonResponse = (CommonResponse) e.getFoundationError();
			if (commonResponse.getCode().equals(DomesticStandingOrdersFoundationServiceConstants.Execute_19)
					&& commonResponse.getAdditionalInformation().getDetails().toString()
							.contains(DomesticStandingOrdersFoundationServiceConstants.VALIDATION_VIOLATIONS)) {
				Gson gson = new Gson();
				ValidationWrapper validationViolations = gson.fromJson(
						gson.toJson(commonResponse.getAdditionalInformation().getDetails()),
						ValidationWrapper.class);
				String fsErrorCode = validationViolations.getValidationViolations().getValidationViolation()
						.getErrorCode();
				String errorField = validationViolations.getValidationViolations().getValidationViolation().getErrorField();
				String errorMapping = adapterUtility.getThrowableError().get(fsErrorCode);
				if (!NullCheckUtils.isNullOrEmpty(errorMapping)) {
					AdapterOBErrorCodeEnum errorCodeEnum=null;
					if (fsErrorCode.equals(DomesticStandingOrdersFoundationServiceConstants.dynamic)) {
						errorCodeEnum = AdapterOBErrorCodeEnum.valueOf(errorMapping);
						errorCodeEnum.setSpecificErrorMessage("Invalid " + errorField + " provided.");
					}
					else
						errorCodeEnum = AdapterOBErrorCodeEnum.valueOf(errorMapping);
					throw AdapterException.populatePSD2Exception(commonResponse.getSummary(), errorCodeEnum, commonResponse);
				}
			}
			throw e;
		}
		
	}
	
	public StandingOrderInstructionComposite restTransportForDomesticStandingOrderServiceGet(RequestInfo reqInfo,
			Class<StandingOrderInstructionComposite> responseType, HttpHeaders headers) {

		return restClient.callForGet(reqInfo, responseType, headers);

	}

}
