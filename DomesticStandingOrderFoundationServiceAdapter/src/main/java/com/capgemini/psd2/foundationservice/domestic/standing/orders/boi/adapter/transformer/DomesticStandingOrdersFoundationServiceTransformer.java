package com.capgemini.psd2.foundationservice.domestic.standing.orders.boi.adapter.transformer;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.adapter.exceptions.AdapterErrorCodeEnum;
import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.adapter.frequency.utility.FrequencyUtil;
import com.capgemini.psd2.exceptions.ErrorMapKeys;
import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.boi.adapter.raml.domain.AccountInformation;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.boi.adapter.raml.domain.Address;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.boi.adapter.raml.domain.BrandCode3;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.boi.adapter.raml.domain.Channel;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.boi.adapter.raml.domain.ChannelCode1;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.boi.adapter.raml.domain.Country;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.boi.adapter.raml.domain.Currency;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.boi.adapter.raml.domain.FinancialEventAmount;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.boi.adapter.raml.domain.Frequency;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.boi.adapter.raml.domain.Party2;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.boi.adapter.raml.domain.PartyBasicInformation;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.boi.adapter.raml.domain.PaymentInstructionStatusCode3;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.boi.adapter.raml.domain.PaymentInstrumentRiskFactor;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.boi.adapter.raml.domain.PaymentTransaction;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.boi.adapter.raml.domain.ProposingPartyAccount;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.boi.adapter.raml.domain.StandingOrderInstructionComposite;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.boi.adapter.raml.domain.StandingOrderInstructionProposal;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.pisp.domain.CustomDStandingOrderPOSTRequest;
import com.capgemini.psd2.pisp.domain.DStandingOrderPOST201Response;
import com.capgemini.psd2.pisp.domain.OBCashAccountCreditor2;
import com.capgemini.psd2.pisp.domain.OBCashAccountDebtor3;
import com.capgemini.psd2.pisp.domain.OBCharge1;
import com.capgemini.psd2.pisp.domain.OBCharge1Amount;
import com.capgemini.psd2.pisp.domain.OBDomesticStandingOrder1;
import com.capgemini.psd2.pisp.domain.OBDomesticStandingOrder1FirstPaymentAmount;
import com.capgemini.psd2.pisp.domain.OBExternalStatus1Code;
import com.capgemini.psd2.pisp.domain.OBWriteDataDomesticStandingOrderResponse1;
import com.capgemini.psd2.pisp.enums.ProcessExecutionStatusEnum;
import com.capgemini.psd2.utilities.NullCheckUtils;


@Component
public class DomesticStandingOrdersFoundationServiceTransformer {
	
	public static final String PARTY_NAME = "BOL Customer Id";
	@Autowired
	private FrequencyUtil frequencyUtil;	
	
	public StandingOrderInstructionProposal transformDomesticStandingOrdersResponseFromAPIToFDForInsert(
			CustomDStandingOrderPOSTRequest standingOrderRequest, Map<String, String> params) {
		
		StandingOrderInstructionComposite standingOrderInstructionComposite=new StandingOrderInstructionComposite();
		StandingOrderInstructionProposal standingOrderInstructionProposal= new StandingOrderInstructionProposal();
		
		
		//Initiation
		if (!NullCheckUtils.isNullOrEmpty(standingOrderRequest.getData().getInitiation()))
		{
//			if(!NullCheckUtils.isNullOrEmpty(standingOrderRequest.getData().getInitiation().getNumberOfPayments()))
//			{
//				standingOrderInstructionProposal.setNumberOfPayments(Double.valueOf(standingOrderRequest.getData().getInitiation().getNumberOfPayments()));
//			}
			standingOrderInstructionProposal.setPaymentInstructionProposalId(standingOrderRequest.getData().getConsentId());
			standingOrderInstructionProposal.setReference(standingOrderRequest.getData().getInitiation().getReference());
			standingOrderInstructionProposal.setFirstPaymentDateTime(standingOrderRequest.getData().getInitiation().getFirstPaymentDateTime());
		}
		
		//FirstPaymentAmount
		if (!NullCheckUtils.isNullOrEmpty(standingOrderRequest.getData().getInitiation().getFirstPaymentAmount()))
		{
			PaymentTransaction firstPaymentAmount=new PaymentTransaction();
			FinancialEventAmount financialEventAmount=new FinancialEventAmount();
			Currency transactionCurrency=new Currency();
			
			financialEventAmount.setTransactionCurrency(Double.valueOf(standingOrderRequest.getData().getInitiation().getFirstPaymentAmount().getAmount()));
			transactionCurrency.setIsoAlphaCode(standingOrderRequest.getData().getInitiation().getFirstPaymentAmount().getCurrency());
			
			firstPaymentAmount.setFinancialEventAmount(financialEventAmount);
			firstPaymentAmount.setTransactionCurrency(transactionCurrency);
			
			standingOrderInstructionProposal.setFirstPaymentAmount(firstPaymentAmount);
		}
		
		
		//DebtorAccount
		if (!NullCheckUtils.isNullOrEmpty(standingOrderRequest.getData().getInitiation().getDebtorAccount()))
		{
			AccountInformation authorisingPartyAccount=new AccountInformation();
			authorisingPartyAccount.setSchemeName(standingOrderRequest.getData().getInitiation().getDebtorAccount().getSchemeName());
			authorisingPartyAccount.setAccountIdentification(standingOrderRequest.getData().getInitiation().getDebtorAccount().getIdentification());
			authorisingPartyAccount.setAccountName(standingOrderRequest.getData().getInitiation().getDebtorAccount().getName());
			authorisingPartyAccount.setSecondaryIdentification(standingOrderRequest.getData().getInitiation().getDebtorAccount().getSecondaryIdentification());
			
			standingOrderInstructionProposal.setAuthorisingPartyAccount(authorisingPartyAccount);
		}
		
		//CreditorAccount
		if (!NullCheckUtils.isNullOrEmpty(standingOrderRequest.getData().getInitiation().getCreditorAccount()))
		{
			ProposingPartyAccount proposingPartyAccount=new ProposingPartyAccount();
			proposingPartyAccount.setSchemeName(standingOrderRequest.getData().getInitiation().getCreditorAccount().getSchemeName());
			proposingPartyAccount.setAccountIdentification(standingOrderRequest.getData().getInitiation().getCreditorAccount().getIdentification());
			proposingPartyAccount.setAccountName(standingOrderRequest.getData().getInitiation().getCreditorAccount().getName());
			proposingPartyAccount.setSecondaryIdentification(standingOrderRequest.getData().getInitiation().getCreditorAccount().getSecondaryIdentification());
			
			standingOrderInstructionProposal.setProposingPartyAccount(proposingPartyAccount);
		}
		
		//for channelcode
		
				Channel channel = new Channel();
				 if (!NullCheckUtils
				   .isNullOrEmpty(   params.get(PSD2Constants.CHANNEL_IN_REQ_HEADER))) {
				  
				  channel.setChannelCode(ChannelCode1.fromValue((params.get(PSD2Constants.CHANNEL_IN_REQ_HEADER)).toUpperCase()));
				 }
				 
					if (!NullCheckUtils
							.isNullOrEmpty(params.get(PSD2Constants.TENANT_ID))) {
						
						if(params.get(PSD2Constants.TENANT_ID).equalsIgnoreCase("BOIUK")) {
							channel.setBrandCode(BrandCode3.NIGB);
						}else if(params.get(PSD2Constants.TENANT_ID).equalsIgnoreCase("BOIROI")){
							channel.setBrandCode(BrandCode3.ROI);
						}	
						
					}
				 
					standingOrderInstructionProposal.setChannel(channel);

				if (!NullCheckUtils.isNullOrEmpty(params.get(PSD2Constants.CHANNEL_IN_REQ_HEADER))
						&& params.get(PSD2Constants.CHANNEL_IN_REQ_HEADER).equalsIgnoreCase(ChannelCode1.BOL.toString())) {
					Party2 authorisingParty = new Party2();
					PartyBasicInformation partyInformation = new PartyBasicInformation();
					partyInformation.setPartySourceIdNumber(params.get(PSD2Constants.PARTY_IDENTIFIER));
					partyInformation.setPartyName(PARTY_NAME);
					authorisingParty.setPartyInformation(partyInformation);
					standingOrderInstructionProposal.setAuthorisingParty(authorisingParty);
				}


		
		
		// Frequency
		if (!NullCheckUtils.isNullOrEmpty(standingOrderRequest.getData().getInitiation().getFrequency())
				&& !NullCheckUtils
						.isNullOrEmpty(standingOrderRequest.getData().getInitiation().getFirstPaymentDateTime())) {
			String paymentStartDate = standingOrderRequest.getData().getInitiation().getFirstPaymentDateTime();
			try {
				SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
				Date firstPaymentDate = simpleDateFormat.parse(paymentStartDate);
				String frequencyCmaToProcessLayer = frequencyUtil
						.CmaToProcessLayer(standingOrderRequest.getData().getInitiation().getFrequency(), firstPaymentDate);
				standingOrderInstructionProposal.setFrequency(Frequency.fromValue(frequencyCmaToProcessLayer));
			} catch (ParseException pe) {
				throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.BAD_REQUEST_PISP_FREQUENCY);
			}
		}
				
		//Risk
		PaymentInstrumentRiskFactor paymentInstrumentRiskFactor=new PaymentInstrumentRiskFactor();
		if (!NullCheckUtils.isNullOrEmpty(standingOrderRequest.getRisk()))
		{
			if (!NullCheckUtils.isNullOrEmpty(standingOrderRequest.getRisk().getPaymentContextCode()))
				paymentInstrumentRiskFactor.setPaymentContextCode(standingOrderRequest.getRisk().getPaymentContextCode().toString());
			if (!NullCheckUtils.isNullOrEmpty(standingOrderRequest.getRisk().getMerchantCategoryCode()))
				paymentInstrumentRiskFactor.setMerchantCategoryCode(standingOrderRequest.getRisk().getMerchantCategoryCode());
			if (!NullCheckUtils.isNullOrEmpty(standingOrderRequest.getRisk().getMerchantCustomerIdentification()))
				paymentInstrumentRiskFactor.setMerchantCustomerIdentification(standingOrderRequest.getRisk().getMerchantCustomerIdentification());
			
			standingOrderInstructionProposal.setPaymentInstructionRiskFactorReference(paymentInstrumentRiskFactor);
		
		
		//DeliveryAddress
		if (!NullCheckUtils.isNullOrEmpty(standingOrderRequest.getRisk().getDeliveryAddress())) {
			
			List<String> addline=standingOrderRequest.getRisk().getDeliveryAddress().getAddressLine();
			
			Address counterpartyAddress=new Address();
			
			if(addline.size()>0)
			{
				counterpartyAddress.setFirstAddressLine(addline.get(0));
				
			}
			if(addline.size()>1)
			{
				counterpartyAddress.setSecondAddressLine(addline.get(1));	
			}
			counterpartyAddress.setGeoCodeBuildingName(standingOrderRequest.getRisk().getDeliveryAddress().getStreetName());
			counterpartyAddress.setGeoCodeBuildingNumber(standingOrderRequest.getRisk().getDeliveryAddress().getBuildingNumber());
			counterpartyAddress.setPostCodeNumber(standingOrderRequest.getRisk().getDeliveryAddress().getPostCode());
			counterpartyAddress.setThirdAddressLine(standingOrderRequest.getRisk().getDeliveryAddress().getTownName());
			counterpartyAddress.setFourthAddressLine(standingOrderRequest.getRisk().getDeliveryAddress().getCountrySubDivision());
			if(!NullCheckUtils.isNullOrEmpty(standingOrderRequest.getRisk().getDeliveryAddress().getCountry()))
			{
				Country addressCountry=new Country();
				addressCountry.setIsoCountryAlphaTwoCode(standingOrderRequest.getRisk().getDeliveryAddress().getCountry());
				counterpartyAddress.setAddressCountry(addressCountry);
			}
			
			paymentInstrumentRiskFactor.setCounterPartyAddress(counterpartyAddress);
		}
		
		standingOrderInstructionProposal.setPaymentInstructionRiskFactorReference(paymentInstrumentRiskFactor);
		}
		standingOrderInstructionComposite.setStandingOrderInstructionProposal(standingOrderInstructionProposal);
	
		return standingOrderInstructionProposal;
		}
	
	/********************************************************************
	***** Function Name		:	transformDomesticStandingOrdersResponseFromFDToAPIForInsert
	***** Arguments			:	T
	***** Return Type		:	DStandingOrderPOST201Response
	***** Use of Function	:	It is use to retrieve/Get data from FS for payment-instruction-id
	***** Creation Date		:	10-Feb-2019
	***** Modification Date	:	10-Feb-2019
	*********************************************************************/
	public <T> DStandingOrderPOST201Response transformDomesticStandingOrdersResponseFromFDToAPIForInsert(
			T standingOrderInstructionProposalResponse) {
		
		
		DStandingOrderPOST201Response dstandingOrderPOST201Response=new DStandingOrderPOST201Response();
		
		StandingOrderInstructionComposite standingOrderInstructionComposite = (StandingOrderInstructionComposite) standingOrderInstructionProposalResponse;
		
		OBWriteDataDomesticStandingOrderResponse1 oBWriteDataDomesticStandingOrderResponse1=new OBWriteDataDomesticStandingOrderResponse1();
		populateFDToAPIResponse(standingOrderInstructionComposite, oBWriteDataDomesticStandingOrderResponse1);
		
		PaymentInstructionStatusCode3 status =standingOrderInstructionComposite.getStandingOrderInstruction().getPaymentInstructionStatusCode();
		if(!NullCheckUtils.isNullOrEmpty(status)){
			if( (status.toString().equalsIgnoreCase("InitiationPending")) && (standingOrderInstructionComposite.getStandingOrderInstruction().getPaymentInstructionNumber() != null)) {
				ProcessExecutionStatusEnum consentStatus = ProcessExecutionStatusEnum.INCOMPLETE;
				dstandingOrderPOST201Response.setProcessExecutionStatus(consentStatus);
			}else if( ((status.toString()).equalsIgnoreCase("InitiationCompleted")) && (standingOrderInstructionComposite.getStandingOrderInstruction().getPaymentInstructionNumber()!= null)) {
				ProcessExecutionStatusEnum consentStatus = ProcessExecutionStatusEnum.PASS;
				dstandingOrderPOST201Response.setProcessExecutionStatus(consentStatus);
			}
			else if( ((status.toString()).equalsIgnoreCase("InitiationFailed")) && (standingOrderInstructionComposite.getStandingOrderInstruction().getPaymentInstructionNumber()!= null)) {
				ProcessExecutionStatusEnum consentStatus = ProcessExecutionStatusEnum.FAIL;
				dstandingOrderPOST201Response.setProcessExecutionStatus(consentStatus);
			}
		}
		
		dstandingOrderPOST201Response.setData(oBWriteDataDomesticStandingOrderResponse1);
		
		return dstandingOrderPOST201Response;  
		
		}
	

	/********************************************************************
	***** Function Name		:	transformDomesticStandingOrderResponse
	***** Arguments			:	T
	***** Return Type		:	DStandingOrderPOST201Response
	***** Use of Function	:	It is use to retrieve/Get data from FS for payment-instruction-id
	***** Creation Date		:	10-Feb-2019
	***** Modification Date	:	10-Feb-2019
	*********************************************************************/
	public <T> DStandingOrderPOST201Response transformDomesticStandingOrderResponse(T standingOrderInstructionProposalResponse) 
	{
		DStandingOrderPOST201Response dstandingOrderPOST201Response=new DStandingOrderPOST201Response();
		
		StandingOrderInstructionComposite standingOrderInstructionComposite = (StandingOrderInstructionComposite) standingOrderInstructionProposalResponse;
		
		OBWriteDataDomesticStandingOrderResponse1 oBWriteDataDomesticStandingOrderResponse1=new OBWriteDataDomesticStandingOrderResponse1();
		
		populateFDToAPIResponse(standingOrderInstructionComposite, oBWriteDataDomesticStandingOrderResponse1);
		
		PaymentInstructionStatusCode3 status =standingOrderInstructionComposite.getStandingOrderInstruction().getPaymentInstructionStatusCode();
		if(!NullCheckUtils.isNullOrEmpty(status)){
			if( (status.toString().equalsIgnoreCase("InitiationPending")) && (standingOrderInstructionComposite.getStandingOrderInstruction().getPaymentInstructionNumber() != null)) {
				ProcessExecutionStatusEnum consentStatus = ProcessExecutionStatusEnum.INCOMPLETE;
				dstandingOrderPOST201Response.setProcessExecutionStatus(consentStatus);
			}else if( ((status.toString()).equalsIgnoreCase("InitiationCompleted")) && (standingOrderInstructionComposite.getStandingOrderInstruction().getPaymentInstructionNumber()!= null)) {
				ProcessExecutionStatusEnum consentStatus = ProcessExecutionStatusEnum.PASS;
				dstandingOrderPOST201Response.setProcessExecutionStatus(consentStatus);
			}
			else if( ((status.toString()).equalsIgnoreCase("InitiationFailed")) && (standingOrderInstructionComposite.getStandingOrderInstruction().getPaymentInstructionNumber()!= null)) {
				ProcessExecutionStatusEnum consentStatus = ProcessExecutionStatusEnum.FAIL;
				dstandingOrderPOST201Response.setProcessExecutionStatus(consentStatus);
			}
		}
		
		
		dstandingOrderPOST201Response.setData(oBWriteDataDomesticStandingOrderResponse1);
		
		return dstandingOrderPOST201Response;  
		
		
	}

	/**
	 * @param standingOrderInstructionComposite
	 * @param oBWriteDataDomesticStandingOrderResponse1
	 * @throws ParseException 
	 */
	private void populateFDToAPIResponse(StandingOrderInstructionComposite standingOrderInstructionComposite,
			OBWriteDataDomesticStandingOrderResponse1 oBWriteDataDomesticStandingOrderResponse1)  {
		//Data
		if (!NullCheckUtils.isNullOrEmpty(standingOrderInstructionComposite.getStandingOrderInstruction()))
			{
				oBWriteDataDomesticStandingOrderResponse1.setDomesticStandingOrderId(standingOrderInstructionComposite.getStandingOrderInstruction().getPaymentInstructionNumber());
				oBWriteDataDomesticStandingOrderResponse1.setConsentId(standingOrderInstructionComposite.getStandingOrderInstructionProposal().getPaymentInstructionProposalId());
				try {
					oBWriteDataDomesticStandingOrderResponse1.setCreationDateTime(getISOFormatDateTimeformat((standingOrderInstructionComposite.getStandingOrderInstruction().getInstructionIssueDate())));
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				System.out.println("CreationDateTime in ISO Date format: "+oBWriteDataDomesticStandingOrderResponse1.getCreationDateTime());
				oBWriteDataDomesticStandingOrderResponse1.setStatus(OBExternalStatus1Code.fromValue(String.valueOf(standingOrderInstructionComposite.getStandingOrderInstruction().getPaymentInstructionStatusCode())));
				String dateTime=standingOrderInstructionComposite.getStandingOrderInstruction().getInstructionStatusUpdateDateTime();
					if (!validateDateTime(dateTime)) {
						throw PSD2Exception.populatePSD2Exception(
								new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR, ErrorMapKeys.INVALID_DATE_FORMAT));
					}
				
				try {
					oBWriteDataDomesticStandingOrderResponse1.setStatusUpdateDateTime(getISOFormatDateTimeformat((standingOrderInstructionComposite.getStandingOrderInstruction().getInstructionStatusUpdateDateTime())));
					}catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						throw PSD2Exception.populatePSD2Exception(
								new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR, ErrorMapKeys.INVALID_DATE_FORMAT));
					} 
				System.out.println("StatusUpdateDateTime in ISO Date format: "+oBWriteDataDomesticStandingOrderResponse1.getStatusUpdateDateTime());
			}
		
		if (!NullCheckUtils.isNullOrEmpty(standingOrderInstructionComposite.getStandingOrderInstructionProposal()))
		{
			oBWriteDataDomesticStandingOrderResponse1.setConsentId(standingOrderInstructionComposite.getStandingOrderInstructionProposal().getPaymentInstructionProposalId());
			
			//Charges
			List<OBCharge1> charges = new ArrayList<>();
			OBCharge1 oBCharge1 = new OBCharge1();
			OBCharge1Amount oBCharge1Amount = new OBCharge1Amount();
		//	List<PaymentInstructionCharge> paymentInstructionCharges = new ArrayList<>();
			
		/*	//Amount
			if(!NullCheckUtils.isNullOrEmpty(standingOrderInstructionComposite.getPaymentInstructionProposal().getCharges())){
				
				paymentInstructionCharges = standingOrderInstructionComposite.getPaymentInstructionProposal().getCharges();
				for (PaymentInstructionCharge charge : paymentInstructionCharges) {
					oBCharge1.setChargeBearer(OBChargeBearerType1Code.fromValue(String.valueOf(charge.getChargeBearer())));
					oBCharge1.setType(charge.getType());
					oBCharge1.setAmount(oBCharge1Amount);
					charges.add(oBCharge1);
				}
			}
			
			if(!NullCheckUtils.isNullOrEmpty(standingOrderInstructionComposite.getPaymentInstructionProposal().getCharges())){
				oBCharge1Amount.setAmount(standingOrderInstructionComposite.getPaymentInstructionProposal().getCharges().get(0).getAmount().getTransactionCurrency().toString());
				oBCharge1Amount.setCurrency(standingOrderInstructionComposite.getPaymentInstructionProposal().getCharges().get(0).getCurrency().getIsoAlphaCode());
			}*/
	
			
			
			//oBWriteDataDomesticStandingOrderResponse1.setCharges(charges);
			
			//Initiation
			OBDomesticStandingOrder1 initiation=new OBDomesticStandingOrder1();
//			if(!NullCheckUtils.isNullOrEmpty(standingOrderInstructionComposite.getPaymentInstructionProposal().getNumberOfPayments()))
//			{
//				initiation.setNumberOfPayments(String.valueOf(standingOrderInstructionComposite.getPaymentInstructionProposal().getNumberOfPayments()));
//			}
			initiation.setFrequency(String.valueOf(standingOrderInstructionComposite.getStandingOrderInstructionProposal().getFrequency()));
			initiation.setReference(standingOrderInstructionComposite.getStandingOrderInstructionProposal().getReference());
			initiation.setFirstPaymentDateTime(standingOrderInstructionComposite.getStandingOrderInstructionProposal().getFirstPaymentDateTime());

			//FirstPaymentAmount
			if(!NullCheckUtils.isNullOrEmpty(standingOrderInstructionComposite.getStandingOrderInstructionProposal().getFirstPaymentAmount()))
			{
				OBDomesticStandingOrder1FirstPaymentAmount firstPaymentAmount=new OBDomesticStandingOrder1FirstPaymentAmount();
				firstPaymentAmount.setAmount(BigDecimal.valueOf(standingOrderInstructionComposite.getStandingOrderInstructionProposal().getFirstPaymentAmount().getFinancialEventAmount().getTransactionCurrency()).toPlainString());
				firstPaymentAmount.setCurrency(standingOrderInstructionComposite.getStandingOrderInstructionProposal().getFirstPaymentAmount().getTransactionCurrency().getIsoAlphaCode());
			
				initiation.setFirstPaymentAmount(firstPaymentAmount);
			}
		
			//Frequency
			if (!NullCheckUtils.isNullOrEmpty(standingOrderInstructionComposite.getStandingOrderInstructionProposal().getFirstPaymentDateTime()) && !NullCheckUtils.isNullOrEmpty(standingOrderInstructionComposite.getStandingOrderInstructionProposal().getFrequency())) {	
				String paymentStartDate=standingOrderInstructionComposite.getStandingOrderInstructionProposal().getFirstPaymentDateTime();
				try {
					SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss"); 
					Date firstPaymentDate= simpleDateFormat.parse(paymentStartDate);
					String frequencyProcessLayerToCma=frequencyUtil.processLayerToCma(standingOrderInstructionComposite.getStandingOrderInstructionProposal().getFrequency().toString(), firstPaymentDate);
					initiation.setFrequency(frequencyProcessLayerToCma);
					}catch(ParseException pe) {
						throw AdapterException.populatePSD2Exception(AdapterErrorCodeEnum.BAD_REQUEST_PISP_FREQUENCY);
					}
			}
			//DebtorAccount
			if(!NullCheckUtils.isNullOrEmpty(standingOrderInstructionComposite.getStandingOrderInstructionProposal().getAuthorisingPartyAccount()))
			{
				OBCashAccountDebtor3 accountDebtor3=new OBCashAccountDebtor3();
				accountDebtor3.setSchemeName(standingOrderInstructionComposite.getStandingOrderInstructionProposal().getAuthorisingPartyAccount().getSchemeName());
				accountDebtor3.setIdentification(standingOrderInstructionComposite.getStandingOrderInstructionProposal().getAuthorisingPartyAccount().getAccountIdentification());
				accountDebtor3.setName(standingOrderInstructionComposite.getStandingOrderInstructionProposal().getAuthorisingPartyAccount().getAccountName());
				accountDebtor3.setSecondaryIdentification(standingOrderInstructionComposite.getStandingOrderInstructionProposal().getAuthorisingPartyAccount().getSecondaryIdentification());
				
				initiation.setDebtorAccount(accountDebtor3);
			}
			
			//CreditorAccount
			if(!NullCheckUtils.isNullOrEmpty(standingOrderInstructionComposite.getStandingOrderInstructionProposal().getProposingPartyAccount()))
			{
				OBCashAccountCreditor2 creditorAccount=new OBCashAccountCreditor2();
				creditorAccount.setSchemeName(standingOrderInstructionComposite.getStandingOrderInstructionProposal().getProposingPartyAccount().getSchemeName());
				creditorAccount.setIdentification(standingOrderInstructionComposite.getStandingOrderInstructionProposal().getProposingPartyAccount().getAccountIdentification());
				creditorAccount.setName(standingOrderInstructionComposite.getStandingOrderInstructionProposal().getProposingPartyAccount().getAccountName());
				creditorAccount.setSecondaryIdentification(standingOrderInstructionComposite.getStandingOrderInstructionProposal().getProposingPartyAccount().getSecondaryIdentification());
				
				initiation.setCreditorAccount(creditorAccount);
			}
			oBWriteDataDomesticStandingOrderResponse1.setInitiation(initiation);
		}
	} 
	private String getISOFormatDateTimeformat(String inputdateformat) throws ParseException {
		DateFormat inputformat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
		DateFormat desiredformat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		Date input=new Date();
		StringBuilder builder = new StringBuilder();
		input = inputformat.parse(inputdateformat);
		
		System.out.println("Updated Format: "+desiredformat.format(input));
		
		builder.append(desiredformat.format(input));
		builder.append("+00:00");
		return builder.toString();
	}
	
	private boolean validateDateTime(String dateTime) {

		boolean isValid = false;
		String dateTimeCopy = dateTime;

		if (dateTimeCopy.contains("Z"))
			dateTimeCopy = dateTimeCopy.split("Z")[0] + "+00:00";

		DateTimeFormatter[] formatters = { DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mmXXX"),
				DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ssXXX"),
				DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSXXX"),
				DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SXXX") };

		for (DateTimeFormatter formatter : formatters) {
			try {
				LocalDate.parse(dateTimeCopy, formatter);
				isValid = true;
			} catch (DateTimeParseException | NullPointerException e) {
				continue;
			}
			break;
		}
		return isValid;
	}


}
