package com.capgemini.psd2.foundationservice.domestic.standing.orders.boi.adapter.constants;

public class DomesticStandingOrdersFoundationServiceConstants {
	public static final String MULE_OUT_OF_BOX_POLICY_ERROR1 = "401";
	public static final String MULE_OUT_OF_BOX_POLICY_ERROR2 = "429";
	public static final String VALIDATION_VIOLATIONS = "validationViolations";
	public static final String Execute_19 = "PPA_SOINP_019";
	public static final String dynamic = "FS_SOE_001";
	

}
