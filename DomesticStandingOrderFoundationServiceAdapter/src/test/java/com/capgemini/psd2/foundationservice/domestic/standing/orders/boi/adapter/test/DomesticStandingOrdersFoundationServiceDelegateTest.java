package com.capgemini.psd2.foundationservice.domestic.standing.orders.boi.adapter.test;

import static org.junit.Assert.assertNotNull;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.capgemini.psd2.adapter.datetime.utility.TimeZoneDateTimeAdapter;
import com.capgemini.psd2.adapter.exceptions.AdapterException;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.boi.adapter.DomesticStandingOrdersFoundationServiceAdapterApplication;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.boi.adapter.client.DomesticStandingOrdersFoundationServiceClient;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.boi.adapter.delegate.DomesticStandingOrdersFoundationServiceDelegate;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.boi.adapter.transformer.DomesticStandingOrdersFoundationServiceTransformer;
import com.capgemini.psd2.pisp.domain.CustomDStandingOrderPOSTRequest;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.RestClientSync;
import com.capgemini.psd2.validator.PSD2Validator;

@RunWith(SpringRunner.class)
public class DomesticStandingOrdersFoundationServiceDelegateTest {

	@InjectMocks
	DomesticStandingOrdersFoundationServiceDelegate delegate;
	
	@Mock
	DomesticStandingOrdersFoundationServiceTransformer transformer;
	
	@Mock
	DomesticStandingOrdersFoundationServiceClient client;
	
	@Mock
	DomesticStandingOrdersFoundationServiceAdapterApplication adapter;
	
	@Mock
	private RestClientSync restClient;
	
	@Mock
	private PSD2Validator psd2Validator;

	@Mock
	private TimeZoneDateTimeAdapter timeZoneDateTimeAdapter;
	
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}
	/**
	 * Context loads.
	 */

	@Test
	public void contextLoads() {
	}
	
	@Test
	public void testStandingOrderConsentFoundationServiceDelegateCreate() {
		
		
		HttpHeaders httpHeaders = new HttpHeaders();

		ReflectionTestUtils.setField(delegate, "transactioReqHeader", "transactioReqHeader");
		ReflectionTestUtils.setField(delegate, "correlationMuleReqHeader", "correlation");
		ReflectionTestUtils.setField(delegate, "sourceUserReqHeader", "sorceuser");
		ReflectionTestUtils.setField(delegate, "apiChannelCode", "channel");
		ReflectionTestUtils.setField(delegate, "partySourceId", "partysource");
		ReflectionTestUtils.setField(delegate, "sourcesystem", "source");
		ReflectionTestUtils.setField(delegate, "apiChannelBrand", "brand");
		ReflectionTestUtils.setField(delegate, "systemApiVersion", "apiversion");
		
		RequestInfo requestInfo = new RequestInfo();
		Map<String, String> params = new HashMap<>();

		params.put("X-BOI-USER", "fgrd");
		params.put("X-CORRELATION-ID", "5321iu");
		params.put("X-BOI-CHANNEL", "bol");
		params.put("partyIdentifier", "kjio76876");
		params.put("x-api-channel-code", "Bol");
	//	params.put("x-api-correlation-id", "");
		params.put("tenant_id", "BOIUK");
		params.put("X-SYSTEM-API-VERSION", "3.0");	
	//	params.put("BOL", "BOL");
		
		assertNotNull(params);
		httpHeaders = delegate.createPaymentRequestHeadersForProcess(params);
		params.put("tenant_id", "BOIROI");
		httpHeaders = delegate.createPaymentRequestHeadersForProcess(params);
		params.put("tenant_id", null);
		httpHeaders = delegate.createPaymentRequestHeadersForProcess(params);
		params.put("tenant_id", "AB");
		httpHeaders = delegate.createPaymentRequestHeadersForProcess(params);
	}
	
	@Test
	public void testStandingOrdersConsentFoundationServiceDelegateForUrlCreate() {

		String version = "v2.0";
		delegate.postPaymentFoundationServiceURL(version);
		assertNotNull(delegate);
	}

	@Test(expected = AdapterException.class)
	public void testStandingOrdersConsentFoundationServiceDelegateForUrlCreate1() {

		String version = null;
		delegate.postPaymentFoundationServiceURL(version);

	}
	
	@Test
	public void testStandingOrderConsentFoundationServiceDelegateRetrive() {

		HttpHeaders httpHeaders = new HttpHeaders();
		ReflectionTestUtils.setField(delegate, "transactioReqHeader", "transactioReqHeader");
		ReflectionTestUtils.setField(delegate, "correlationMuleReqHeader", "correlation");
		ReflectionTestUtils.setField(delegate, "sourceUserReqHeader", "sorceuser");
		ReflectionTestUtils.setField(delegate, "apiChannelCode", "channel");
		ReflectionTestUtils.setField(delegate, "partySourceId", "partysource");
		ReflectionTestUtils.setField(delegate, "sourcesystem", "source");
		ReflectionTestUtils.setField(delegate, "apiChannelBrand", "brand");
		ReflectionTestUtils.setField(delegate, "systemApiVersion", "apiversion");

		RequestInfo requestInfo = new RequestInfo();
		Map<String, String> params = new HashMap<>();

		params.put("X-BOI-USER", "fgrd");
		params.put("X-CORRELATION-ID", "5321iu");
		params.put("X-BOI-CHANNEL", "bol");
		params.put("partyIdentifier", "kjio76876");
		params.put("x-api-channel-code", "Bol");
	//	params.put("x-api-correlation-id", "");
		params.put("tenant_id", "BOIROI");
		params.put("X-SYSTEM-API-VERSION", "3.0");	
	//	params.put("BOL", "BOL");

		assertNotNull(params);
		httpHeaders = delegate.createPaymentRequestHeadersForGet(params);
		params.put("tenant_id", "BOIUK");
		httpHeaders = delegate.createPaymentRequestHeadersForGet(params);
		params.put("tenant_id", null);
		httpHeaders = delegate.createPaymentRequestHeadersForGet(params);
		params.put("tenant_id", "AB");
		httpHeaders = delegate.createPaymentRequestHeadersForGet(params);
	}
	
	@Test
	public void testStandingOrderConsentFoundationServiceDelegateRetriveNull() {

		HttpHeaders httpHeaders = new HttpHeaders();
		ReflectionTestUtils.setField(delegate, "transactioReqHeader", "transactioReqHeader");
		ReflectionTestUtils.setField(delegate, "correlationMuleReqHeader", "correlation");
		ReflectionTestUtils.setField(delegate, "sourceUserReqHeader", "sorceuser");
		ReflectionTestUtils.setField(delegate, "apiChannelCode", "channel");
		ReflectionTestUtils.setField(delegate, "partySourceId", "partysource");
		ReflectionTestUtils.setField(delegate, "sourcesystem", "source");
		ReflectionTestUtils.setField(delegate, "apiChannelBrand", "brand");
		ReflectionTestUtils.setField(delegate, "systemApiVersion", "apiversion");

		RequestInfo requestInfo = new RequestInfo();
		Map<String, String> params = new HashMap<>();

		params.put("X-BOI-USER", "");
		params.put("X-CORRELATION-ID", "");
		params.put("X-BOI-CHANNEL", "");
		params.put("partyIdentifier", "");
		params.put("x-api-channel-code", "");
	//	params.put("x-api-correlation-id", "");
		params.put("tenant_id", "BOIUK");
		params.put("X-SYSTEM-API-VERSION", "");	
	//	params.put("BOL", "BOL");

		assertNotNull(params);
		httpHeaders = delegate.createPaymentRequestHeadersForGet(params);
		httpHeaders = delegate.createPaymentRequestHeadersForProcess(params);
	}
	
	
	@Test
	public void testStandingOrdersConsentFoundationServiceDelegateForUrlRetrive() {

		String version = "v2.0";
		String PaymentInstuctionProposalId = "1234";
		delegate.getPaymentFoundationServiceURL(version, PaymentInstuctionProposalId);
		assertNotNull(delegate);
	}

	@Test(expected = AdapterException.class)
	public void testStandingOrdersConsentFoundationServiceDelegateForUrlRetrive1() {

		String version = null;
		String PaymentInstuctionProposalId = "1234";
		delegate.getPaymentFoundationServiceURL(version, PaymentInstuctionProposalId);

	}

	@Test(expected = AdapterException.class)
	public void testStandingOrdersConsentFoundationServiceDelegateForUrlRetrive2() {

		String version = "v2.0";
		String PaymentInstuctionProposalId = null;
		delegate.getPaymentFoundationServiceURL(version, PaymentInstuctionProposalId);

	}
	@Test
	public void testStandingOrdersConsentFoundationServiceDelegateForUrlRetrive3() {

		CustomDStandingOrderPOSTRequest paymentConsentsRequest = new CustomDStandingOrderPOSTRequest();
		 Map<String, String> params=new HashMap<String, String>();
		 params.put("Channel", "B365");
		delegate.transformDomesticStandingOrdersResponseFromAPIToFDForInsert(paymentConsentsRequest,params);

	}
}
