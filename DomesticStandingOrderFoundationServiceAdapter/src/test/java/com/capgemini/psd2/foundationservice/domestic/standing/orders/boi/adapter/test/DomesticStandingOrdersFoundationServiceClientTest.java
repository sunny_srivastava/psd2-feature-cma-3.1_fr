package com.capgemini.psd2.foundationservice.domestic.standing.orders.boi.adapter.test;

import static org.mockito.Matchers.any;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.junit4.SpringRunner;

import com.capgemini.psd2.foundationservice.domestic.standing.orders.boi.adapter.DomesticStandingOrdersFoundationServiceAdapterApplication;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.boi.adapter.client.DomesticStandingOrdersFoundationServiceClient;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.boi.adapter.delegate.DomesticStandingOrdersFoundationServiceDelegate;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.boi.adapter.raml.domain.StandingOrderInstructionComposite;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.boi.adapter.raml.domain.StandingOrderInstructionProposal;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.boi.adapter.transformer.DomesticStandingOrdersFoundationServiceTransformer;
import com.capgemini.psd2.rest.client.model.RequestInfo;
import com.capgemini.psd2.rest.client.sync.RestClientSync;

@RunWith(SpringRunner.class)
public class DomesticStandingOrdersFoundationServiceClientTest {
	
	@InjectMocks
	DomesticStandingOrdersFoundationServiceClient client;
	
	@Mock
	DomesticStandingOrdersFoundationServiceAdapterApplication adapter;
	
	@Mock
	DomesticStandingOrdersFoundationServiceDelegate delegate;
	
	@Mock
	DomesticStandingOrdersFoundationServiceTransformer transformer;
	
	
	@Mock
	private RestClientSync restClient;
	
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}
	/**
	 * Context loads.
	 */

	@Test
	public void contextLoads() {
	}
	
	@Test
	public void testStandingOrdersConsentFoundationServiceClientCreate(){
		RequestInfo reqInfo= new RequestInfo();
		HttpHeaders httpHeaders= new HttpHeaders();
		StandingOrderInstructionProposal standingInstructionProposalRequest1 = new StandingOrderInstructionProposal();
		Mockito.when(restClient.callForPost(any(),any(),any(),any())).thenReturn(new StandingOrderInstructionComposite());
		
		client.restTransportForDomesticStandingOrdersServicePost(reqInfo,standingInstructionProposalRequest1, StandingOrderInstructionComposite.class, httpHeaders);
	}
	
	@Test
	public void testStandingOrdersConsentFoundationServiceClientRetrive(){
		RequestInfo reqInfo= new RequestInfo();
		HttpHeaders headers= new HttpHeaders();
		Mockito.when(restClient.callForGet(any(),any(),any())).thenReturn(new StandingOrderInstructionComposite());
		
		client.restTransportForDomesticStandingOrderServiceGet(reqInfo, StandingOrderInstructionComposite.class, headers);
	}
	
	

}
