package com.capgemini.psd2.foundationservice.domestic.standing.orders.boi.adapter.test;

import static org.mockito.Matchers.any;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.junit4.SpringRunner;

import com.capgemini.psd2.foundationservice.domestic.standing.orders.boi.adapter.DomesticStandingOrdersFoundationServiceAdapterApplication;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.boi.adapter.client.DomesticStandingOrdersFoundationServiceClient;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.boi.adapter.delegate.DomesticStandingOrdersFoundationServiceDelegate;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.boi.adapter.raml.domain.StandingOrderInstructionComposite;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.boi.adapter.raml.domain.StandingOrderInstructionProposal;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.boi.adapter.transformer.DomesticStandingOrdersFoundationServiceTransformer;
import com.capgemini.psd2.pisp.domain.CustomDStandingOrderConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomDStandingOrderConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.CustomDStandingOrderPOSTRequest;
import com.capgemini.psd2.pisp.domain.DStandingOrderPOST201Response;
import com.capgemini.psd2.pisp.domain.OBExternalConsentStatus1Code;
import com.capgemini.psd2.pisp.domain.OBExternalStatus1Code;
import com.capgemini.psd2.pisp.enums.PaymentTypeEnum;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;

@RunWith(SpringRunner.class)
public class DomesticStandingOrdersFoundationServiceAdapterApplicationTest {
	
	@InjectMocks
	DomesticStandingOrdersFoundationServiceAdapterApplication adapter;
	
	@Mock
	DomesticStandingOrdersFoundationServiceDelegate delegate;
	
	@Mock
	DomesticStandingOrdersFoundationServiceClient client;
	
	@Mock
	DomesticStandingOrdersFoundationServiceTransformer transformer;
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	/**
	 * Context loads.
	 */

	@Test
	public void contextLoads() {
	}
	
	@Test
	public void testStandingOrdersConsentFoundationServiceAdapterCreate() {
		Map<String, OBExternalStatus1Code> stageIdentifiers = new HashMap<>();
		Map<String, String> params = new HashMap<>();
		stageIdentifiers.put("user",OBExternalStatus1Code.INITIATIONCOMPLETED);
		CustomDStandingOrderPOSTRequest standingOrderConsentsRequest1 = new CustomDStandingOrderPOSTRequest();
		OBExternalConsentStatus1Code successStatus1 = null;
		OBExternalConsentStatus1Code failureStatus1 = null;
		HttpHeaders httpHeaders = new HttpHeaders();

		params.put("USER_ID", "X-API-SOURCE-USER");
		params.put("PARTY_SOURCE_ID", "X-API-PARTY-SOURCE-ID-NUMBER");
		params.put("TRANSACTION_ID", "X-API-TRANSACTION-ID");

		Mockito.when(delegate.createPaymentRequestHeadersForProcess(any()))
		.thenReturn(httpHeaders);
		Mockito.when(delegate.postPaymentFoundationServiceURL(any()))
		.thenReturn(
				"http://localhost:9093/group-payments/p/payments-service/v3.0/domestic/standing-orders/payment-instruction-proposals");
		Mockito.when(client.restTransportForDomesticStandingOrdersServicePost(any(), any(),
				any(),any())).thenReturn(new StandingOrderInstructionComposite());
		Mockito.when(transformer.transformDomesticStandingOrdersResponseFromAPIToFDForInsert(any(),any()))
		.thenReturn(new StandingOrderInstructionProposal());
		Mockito.when(transformer.transformDomesticStandingOrdersResponseFromFDToAPIForInsert(any()))
		.thenReturn(new DStandingOrderPOST201Response());

		adapter.processDomesticStandingOrder(standingOrderConsentsRequest1,stageIdentifiers, params);

	}

	
	@Test
	public void testStandingOrdersConsentFoundationServiceAdapterRetrive() {
		CustomPaymentStageIdentifiers stageIdentifiers = new CustomPaymentStageIdentifiers();
		Map<String, String> params = new HashMap<>();
		stageIdentifiers.setPaymentConsentId("12345");
		stageIdentifiers.setPaymentSetupVersion("v2.0");
		stageIdentifiers.setPaymentTypeEnum(PaymentTypeEnum.DOMESTIC_PAY);
		HttpHeaders httpHeaders = new HttpHeaders();

		params.put("USER_ID", "X-API-SOURCE-USER");
		params.put("PARTY_SOURCE_ID", "X-API-PARTY-SOURCE-ID-NUMBER");
		params.put("TRANSACTION_ID", "X-API-TRANSACTION-ID");

		Mockito.when(delegate.createPaymentRequestHeadersForGet(any()))
		.thenReturn(httpHeaders);
		Mockito.when(delegate.getPaymentFoundationServiceURL(any(), any()))
		.thenReturn(
				"http://localhost:9093/group-payments/p/payments-service/v3.0/domestic/standing-orders/payment-instruction-proposals/c5e593a0-4e71-4fe6-b453-35e2015e7f58");
		Mockito.when(client.restTransportForDomesticStandingOrderServiceGet(any(), any(),
				any())).thenReturn(new StandingOrderInstructionComposite());
		Mockito.when(transformer.transformDomesticStandingOrderResponse(any()))
		.thenReturn(new DStandingOrderPOST201Response());

		adapter.retrieveStagedDomesticStandingOrder(stageIdentifiers, params);

	}

}

