package com.capgemini.psd2.foundationservice.domestic.standing.orders.boi.adapter.test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringRunner;

import com.capgemini.psd2.adapter.frequency.utility.FrequencyUtil;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.boi.adapter.DomesticStandingOrdersFoundationServiceAdapterApplication;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.boi.adapter.client.DomesticStandingOrdersFoundationServiceClient;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.boi.adapter.delegate.DomesticStandingOrdersFoundationServiceDelegate;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.boi.adapter.raml.domain.AccountInformation;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.boi.adapter.raml.domain.Address;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.boi.adapter.raml.domain.AuthorisationType;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.boi.adapter.raml.domain.Country;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.boi.adapter.raml.domain.Currency;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.boi.adapter.raml.domain.FinancialEventAmount;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.boi.adapter.raml.domain.Frequency;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.boi.adapter.raml.domain.PaymentInstrumentRiskFactor;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.boi.adapter.raml.domain.PaymentTransaction;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.boi.adapter.raml.domain.ProposalStatus;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.boi.adapter.raml.domain.ProposingPartyAccount;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.boi.adapter.raml.domain.StandingOrderInstructionComposite;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.boi.adapter.raml.domain.StandingOrderInstructionProposal;
import com.capgemini.psd2.foundationservice.domestic.standing.orders.boi.adapter.transformer.DomesticStandingOrdersFoundationServiceTransformer;
import com.capgemini.psd2.pisp.domain.CustomDStandingOrderPOSTRequest;
import com.capgemini.psd2.pisp.domain.OBCashAccountCreditor2;
import com.capgemini.psd2.pisp.domain.OBCashAccountDebtor3;
import com.capgemini.psd2.pisp.domain.OBDomesticStandingOrder1;
import com.capgemini.psd2.pisp.domain.OBDomesticStandingOrder1FinalPaymentAmount;
import com.capgemini.psd2.pisp.domain.OBDomesticStandingOrder1FirstPaymentAmount;
import com.capgemini.psd2.pisp.domain.OBDomesticStandingOrder1RecurringPaymentAmount;
import com.capgemini.psd2.pisp.domain.OBExternalPaymentContext1Code;
import com.capgemini.psd2.pisp.domain.OBRisk1;
import com.capgemini.psd2.pisp.domain.OBRisk1DeliveryAddress;
import com.capgemini.psd2.pisp.domain.OBWriteDataDomesticStandingOrder1;
import com.capgemini.psd2.validator.PSD2Validator;

@RunWith(SpringRunner.class)
public class DomesticStandingOrdersFoundationServiceTransformerTest {
	
	
	@InjectMocks
	DomesticStandingOrdersFoundationServiceTransformer transformer;
	
	@Mock
	DomesticStandingOrdersFoundationServiceAdapterApplication adapter;
	
	@Mock
	DomesticStandingOrdersFoundationServiceDelegate delegate;
	
	@Mock
	DomesticStandingOrdersFoundationServiceClient client;
	
	@Mock
	private PSD2Validator psd2Validator;
	
	@Mock
	private FrequencyUtil frequencyUtil;
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}


	@Test
	public void contextLoads() {
	}
/*	@Test
	public void testTransformDomesticStandingOrderResponse(){
		
		StandingOrderInstructionComposite comp = new StandingOrderInstructionComposite();
		StandingOrderInstructionProposal inputBalanceObjStanding = new StandingOrderInstructionProposal();
		PaymentInstructionCharge paymentInstructionCharge = new PaymentInstructionCharge();
		Amount am = new Amount();
		Currency c1 = new Currency();
		
		inputBalanceObjStanding.setPaymentInstructionProposalId("asad");
		inputBalanceObjStanding.setProposalCreationDatetime("Max");
		inputBalanceObjStanding.setProposalStatus(ProposalStatus.AWAITINGAUTHORISATION);
		inputBalanceObjStanding.setProposalStatusUpdateDatetime("Max");
		inputBalanceObjStanding.setPermission("safdf");
		
		paymentInstructionCharge.setChargeBearer(ChargeBearer.BORNEBYCREDITOR);
		paymentInstructionCharge.setType("adfdf");
		paymentInstructionCharge.setAmount(am);
		paymentInstructionCharge.setCurrency(c1);
		am.setTransactionCurrency(16.00);
		c1.setIsoAlphaCode("afsf");
		List<PaymentInstructionCharge> chargeList = new ArrayList<PaymentInstructionCharge>();
		chargeList.add(paymentInstructionCharge);
		
		inputBalanceObjStanding.setFrequency(Frequency.WEEKLY);
		inputBalanceObjStanding.setReference("gghfh");
		inputBalanceObjStanding.setNumberOfPayments(123.00);
		inputBalanceObjStanding.setFirstPaymentDateTime("Max");
		inputBalanceObjStanding.setRecurringPaymentDateTime("Max");
		inputBalanceObjStanding.setFinalPaymentDateTime("Max");
		
		PaymentTransaction payment = new PaymentTransaction();
		FinancialEventAmount amount = new FinancialEventAmount();
		amount.setTransactionCurrency(12.00);
		payment.setFinancialEventAmount(amount);
		Currency c = new Currency();
		c.isoAlphaCode("asdfaf");
		payment.setTransactionCurrency(c);
		inputBalanceObjStanding.setFirstPaymentAmount(payment);
		
		amount.setTransactionCurrency(24.00);
		c.isoAlphaCode("adfsaf");
		inputBalanceObjStanding.setRecurringPaymentAmount(payment);
		
		amount.setTransactionCurrency(24.00);
		c.isoAlphaCode("adfsaf");
		inputBalanceObjStanding.setFinalPaymentAmount(payment);
		
		AuthorisingPartyAccount authorisingPartyAccount = new AuthorisingPartyAccount();
		authorisingPartyAccount.setSchemeName("afsdfa");
		authorisingPartyAccount.setAccountName("dfhhf");
		authorisingPartyAccount.setAccountNumber("dgdggdg");
		authorisingPartyAccount.setSecondaryIdentification("fghfhhj");
		inputBalanceObjStanding.setAuthorisingPartyAccount(authorisingPartyAccount);
		
		ProposingPartyAccount proposingPartyAccount = new ProposingPartyAccount();
		proposingPartyAccount.setSchemeName("afsdfa");
		proposingPartyAccount.setAccountName("dfhhf");
		proposingPartyAccount.setAccountNumber("dgdggdg");
		proposingPartyAccount.setSecondaryIdentification("fghfhhj");
		inputBalanceObjStanding.setProposingPartyAccount(proposingPartyAccount);
		
		inputBalanceObjStanding.authorisationType(AuthorisationType.ANY);
		inputBalanceObjStanding.authorisationDatetime("Max");
		
		PaymentInstrumentRiskFactor paymentInstrumentRiskFactor = new PaymentInstrumentRiskFactor();
		paymentInstrumentRiskFactor.setPaymentContextCode("asfdf");
		paymentInstrumentRiskFactor.setMerchantCategoryCode("aafaff");
		paymentInstrumentRiskFactor.setMerchantCustomerIdentification("dadff");
		
		
		Address address = new Address();
		address.setFirstAddressLine("sdfdf");
		address.setSecondAddressLine("asdfaff");
		address.setGeoCodeBuildingName("asdddf");
		address.setGeoCodeBuildingNumber("asffsaf");
		address.setPostCodeNumber("asdads");
		address.setThirdAddressLine("adfdfa");
		address.setFourthAddressLine("adffd");
		address.setFifthAddressLine("gfsgfs");
		paymentInstrumentRiskFactor.setCounterPartyAddress(address);
		
		Country country = new Country();
		country.setIsoCountryAlphaTwoCode("adadfa");
		address.setAddressCountry(country);
		inputBalanceObjStanding.setPaymentInstructionRiskFactorReference(paymentInstrumentRiskFactor);
		
		List<PaymentInstructionCharge> charges = new ArrayList<PaymentInstructionCharge>();
		PaymentInstructionCharge element = new PaymentInstructionCharge();
		element.setChargeBearer(ChargeBearer.BORNEBYCREDITOR);
		element.setType("454dwd");
		Amount amt = new Amount();
		amt.setTransactionCurrency(12345.2314);
		element.setAmount(amt);
		Currency currency = new Currency();
		currency.setIsoAlphaCode("45dc4d5");
		element.setCurrency(currency);
		charges.add(element);
		inputBalanceObjStanding.setCharges(charges );
		
		
		ScheduledPaymentInstructionProposal2 paymentInstruction = new ScheduledPaymentInstructionProposal2();
		paymentInstruction.setPaymentInstructionStatusCode(PaymentInstructionStatusCode2.INITIATIONFAILED);
		paymentInstruction.setPaymentInstructionNumber("123");
		comp.setPaymentInstruction(paymentInstruction);
		comp.setPaymentInstructionProposal(inputBalanceObjStanding);
		transformer.transformDomesticStandingOrderResponse(comp);
		transformer.transformDomesticStandingOrdersResponseFromFDToAPIForInsert(comp);
	}*/
	
	@Test
	public void testTransformDomesticStandingOrderResponseForNull() {
		/*
		 * StandingOrderInstructionComposite comp = new
		 * StandingOrderInstructionComposite(); StandingOrderInstructionProposal
		 * inputBalanceObjStanding = new StandingOrderInstructionProposal(); //
		 * PaymentInstructionCharge paymentInstructionCharge = new
		 * PaymentInstructionCharge(); // Amount am = new Amount(); Currency c1 = new
		 * Currency(); PaymentTransaction payTrans = new PaymentTransaction();
		 * payTrans.setTransactionCurrency(c1);
		 * 
		 * FinancialEventAmount eventAmount = new FinancialEventAmount();
		 * eventAmount.setAccountCurrency(4.001);
		 * eventAmount.setGroupReportingCurrency(4.88);
		 * eventAmount.setLocalReportingCurrency(4.008);
		 * eventAmount.setTransactionCurrency(4.55);
		 * 
		 * payTrans.setFinancialEventAmount(eventAmount);
		 * 
		 * 
		 * 
		 * inputBalanceObjStanding.setPaymentInstructionProposalId("asad");
		 * inputBalanceObjStanding.setProposalCreationDatetime("Max");
		 * inputBalanceObjStanding.setProposalStatus(ProposalStatus.REJECTED);
		 * inputBalanceObjStanding.setProposalStatusUpdateDatetime("Max");
		 * inputBalanceObjStanding.setPermission("safdf");
		 * inputBalanceObjStanding.setPaymentInstructionProposalId(null);
		 * inputBalanceObjStanding.setProposalCreationDatetime(null);
		 * inputBalanceObjStanding.setProposalStatus(null);
		 * inputBalanceObjStanding.setProposalStatusUpdateDatetime(null);
		 * inputBalanceObjStanding.setPermission(null);
		 * 
		 * 
		 * // paymentInstructionCharge.setChargeBearer(ChargeBearer.BORNEBYCREDITOR); //
		 * paymentInstructionCharge.setType("adfdf"); //
		 * paymentInstructionCharge.setAmount(am); //
		 * paymentInstructionCharge.setCurrency(c1); //
		 * am.setTransactionCurrency(16.00); c1.setIsoAlphaCode("afsf"); //
		 * List<PaymentInstructionCharge> chargeList = new
		 * ArrayList<PaymentInstructionCharge>(); //
		 * chargeList.add(paymentInstructionCharge); //
		 * inputBalanceObjStanding.setFrequency(Frequency.WEEKLY);
		 * inputBalanceObjStanding.setReference("gghfh"); //
		 * inputBalanceObjStanding.setNumberOfPayments(123.00);
		 * inputBalanceObjStanding.setFirstPaymentDateTime("Max");
		 * 
		 * 
		 * inputBalanceObjStanding.setFrequency(null);
		 * inputBalanceObjStanding.setReference(null); //
		 * inputBalanceObjStanding.setNumberOfPayments(null);
		 * inputBalanceObjStanding.setFirstPaymentDateTime(null);
		 * 
		 * 
		 * PaymentTransaction payment = new PaymentTransaction(); FinancialEventAmount
		 * amount = new FinancialEventAmount(); amount.setTransactionCurrency(12.00);
		 * payment.setFinancialEventAmount(amount); Currency c = new Currency();
		 * c.isoAlphaCode("asdfaf"); payment.setTransactionCurrency(c);
		 * inputBalanceObjStanding.setFirstPaymentAmount(payment);
		 * inputBalanceObjStanding.setFirstPaymentAmount(null);
		 * 
		 * 
		 * amount.setTransactionCurrency(24.00); c.isoAlphaCode("adfsaf");
		 * 
		 * amount.setTransactionCurrency(24.00); c.isoAlphaCode("adfsaf");
		 * 
		 * AccountInformation authorisingPartyAccount = new AccountInformation();
		 * authorisingPartyAccount.setSchemeName("afsdfa");
		 * authorisingPartyAccount.setAccountName("dfhhf");
		 * authorisingPartyAccount.setAccountNumber("dgdggdg");
		 * authorisingPartyAccount.setSecondaryIdentification("fghfhhj");
		 * inputBalanceObjStanding.setAuthorisingPartyAccount(authorisingPartyAccount);
		 * inputBalanceObjStanding.setAuthorisingPartyAccount(null);
		 * 
		 * ProposingPartyAccount proposingPartyAccount = new ProposingPartyAccount();
		 * proposingPartyAccount.setSchemeName("afsdfa");
		 * proposingPartyAccount.setAccountName("dfhhf");
		 * proposingPartyAccount.setAccountNumber("dgdggdg");
		 * proposingPartyAccount.setSecondaryIdentification("fghfhhj");
		 * inputBalanceObjStanding.setProposingPartyAccount(proposingPartyAccount);
		 * inputBalanceObjStanding.setProposingPartyAccount(null);
		 * 
		 * inputBalanceObjStanding.authorisationType(AuthorisationType.ANY);
		 * inputBalanceObjStanding.authorisationDatetime("MAX");
		 * inputBalanceObjStanding.authorisationType(null);
		 * inputBalanceObjStanding.authorisationDatetime(null);
		 * 
		 * PaymentInstrumentRiskFactor paymentInstrumentRiskFactor = new
		 * PaymentInstrumentRiskFactor();
		 * paymentInstrumentRiskFactor.setPaymentContextCode("asfdf");
		 * paymentInstrumentRiskFactor.setMerchantCategoryCode("aafaff");
		 * paymentInstrumentRiskFactor.setMerchantCustomerIdentification("dadff");
		 * 
		 * 
		 * Address address = new Address(); address.setFirstAddressLine("sdfdf");
		 * address.setSecondAddressLine("asdfaff");
		 * address.setGeoCodeBuildingName("asdddf");
		 * address.setGeoCodeBuildingNumber("asffsaf");
		 * address.setPostCodeNumber("asdads"); address.setThirdAddressLine("adfdfa");
		 * address.setFourthAddressLine("adffd"); address.setFifthAddressLine("gfsgfs");
		 * paymentInstrumentRiskFactor.setCounterPartyAddress(address);
		 * 
		 * 
		 * Country country = new Country(); country.setIsoCountryAlphaTwoCode("adadfa");
		 * address.setAddressCountry(country);
		 * inputBalanceObjStanding.setPaymentInstructionRiskFactorReference(
		 * paymentInstrumentRiskFactor);
		 * inputBalanceObjStanding.setPaymentInstructionRiskFactorReference(null); //
		 * comp.setPaymentInstructionProposal(inputBalanceObjStanding);
		 * 
		 * 
		 * // ScheduledPaymentInstructionProposal2 paymentInstruction = new
		 * ScheduledPaymentInstructionProposal2(); //
		 * paymentInstruction.setPaymentInstructionStatusCode(
		 * PaymentInstructionStatusCode3.INITIATIONCOMPLETED); //
		 * paymentInstruction.setPaymentInstructionNumber("123"); //
		 * comp.setPaymentInstruction(paymentInstruction);
		 * inputBalanceObjStanding.setPaymentInstructionProposalId(
		 * "12345-2434-21322-31312");
		 * comp.setStandingOrderInstructionProposal(inputBalanceObjStanding);
		 * transformer.transformDomesticStandingOrderResponse(comp);
		 * transformer.transformDomesticStandingOrdersResponseFromFDToAPIForInsert(comp)
		 * ; inputBalanceObjStanding.setFirstPaymentAmount(payTrans); AccountInformation
		 * debtAcount = new AccountInformation(); debtAcount.setAccountCurrency(c1);
		 * debtAcount.setAccountIdentification("IE22445588");
		 * debtAcount.setAccountName("Vivek");
		 * debtAcount.setAccountNumber("55159316482");
		 * debtAcount.setSchemeName("UK.OBIE>IBAN");
		 * debtAcount.setSecondaryIdentification("IE44882255");
		 * inputBalanceObjStanding.setAuthorisingPartyAccount(debtAcount);
		 * 
		 * ProposingPartyAccount credAccount = new ProposingPartyAccount();
		 * credAccount.setAccountIdentification("IE22445588");
		 * 
		 * credAccount.setAccountName("Vivek");
		 * credAccount.setAccountNumber("55159316482");
		 * credAccount.setSchemeName("UK.OBIE>IBAN");
		 * credAccount.setSecondaryIdentification("IE44882255");
		 * inputBalanceObjStanding.setProposingPartyAccount(credAccount);
		 * 
		 * transformer.transformDomesticStandingOrdersResponseFromFDToAPIForInsert(comp)
		 * ; // paymentInstruction.setPaymentInstructionNumber(null);
		 * transformer.transformDomesticStandingOrdersResponseFromFDToAPIForInsert(comp)
		 * ; // paymentInstruction.setPaymentInstructionStatusCode(
		 * PaymentInstructionStatusCode3.INITIATIONFAILED);
		 * transformer.transformDomesticStandingOrdersResponseFromFDToAPIForInsert(comp)
		 * ; // paymentInstruction.setPaymentInstructionNumber("1234"); //
		 * paymentInstruction.setPaymentInstructionStatusCode(
		 * PaymentInstructionStatusCode3.INITIATIONFAILED);
		 * transformer.transformDomesticStandingOrdersResponseFromFDToAPIForInsert(comp)
		 * ;
		 * 
		 */}
	
	@Test
	public void testTransformDomesticStandingOrdersResponseFromAPIToFDForInsert() throws ParseException{
		
		CustomDStandingOrderPOSTRequest standingOrderConsentsRequest = new CustomDStandingOrderPOSTRequest();
		OBWriteDataDomesticStandingOrder1 data = new OBWriteDataDomesticStandingOrder1();
		OBRisk1 risk = new OBRisk1();
		OBCashAccountDebtor3 debtorAccount = new OBCashAccountDebtor3();
		OBRisk1DeliveryAddress deliveryAddress = new OBRisk1DeliveryAddress();
		OBCashAccountCreditor2 creditorAccount = new OBCashAccountCreditor2();
		OBDomesticStandingOrder1 obDomestic1 = new OBDomesticStandingOrder1();
		List<String> addressLine = new ArrayList<String>();
		addressLine.add("fbf");
		addressLine.add("sfsdf");
		
		
		deliveryAddress.setStreetName("abc");
		deliveryAddress.setBuildingNumber("1233");
		deliveryAddress.setPostCode("3111455");
		deliveryAddress.setTownName("pune");
		deliveryAddress.setCountrySubDivision("gfh562");
		deliveryAddress.setCountry("India");
		
		risk.setPaymentContextCode(OBExternalPaymentContext1Code.BILLPAYMENT);
		risk.setMerchantCategoryCode("gfh562");
		risk.setMerchantCustomerIdentification("gfh562");
		risk.setDeliveryAddress(deliveryAddress);
		debtorAccount.setSchemeName("gfh562");
		debtorAccount.setIdentification("gfh562");
		debtorAccount.setName("gfh562");
		debtorAccount.setSecondaryIdentification("gfh562");
		creditorAccount.setSchemeName("gfh562");
		creditorAccount.setIdentification("gfh562");
		creditorAccount.setName("gfh562i");
		creditorAccount.setSecondaryIdentification("gfh562");
		
		obDomestic1.setCreditorAccount(creditorAccount);
		obDomestic1.setDebtorAccount(debtorAccount);
		OBDomesticStandingOrder1FinalPaymentAmount finalPaymentAmount = new OBDomesticStandingOrder1FinalPaymentAmount();
		finalPaymentAmount.setAmount("12345.2314");
		finalPaymentAmount.setCurrency("gfh562");
		obDomestic1.setFinalPaymentAmount(finalPaymentAmount);
		OBDomesticStandingOrder1FirstPaymentAmount firstPaymentAmount = new OBDomesticStandingOrder1FirstPaymentAmount();
		firstPaymentAmount.setAmount("12345.2314");
		firstPaymentAmount.setCurrency("gfh562");
		OBDomesticStandingOrder1RecurringPaymentAmount recurringPaymentAmount = new OBDomesticStandingOrder1RecurringPaymentAmount();
		recurringPaymentAmount.setAmount("12345.2314");
		recurringPaymentAmount.setCurrency("gfh562");
		obDomestic1.setRecurringPaymentAmount(recurringPaymentAmount );
		obDomestic1.setFirstPaymentAmount(firstPaymentAmount );
		obDomestic1.setDebtorAccount(debtorAccount);
		obDomestic1.setCreditorAccount(creditorAccount);
		
		obDomestic1.setReference("gfh562");
		obDomestic1.setNumberOfPayments("12345");
		obDomestic1.setFirstPaymentDateTime("2019-03-01T12:04:56+05:30");
	/*	DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX");
		obDomestic1.setFirstPaymentDateTime(dateFormat.format(new Date()));
		obDomestic1.setRecurringPaymentDateTime(dateFormat.format(new Date()));
		obDomestic1.setFirstPaymentDateTime(dateFormat.format(new Date())); */

		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		Date date=simpleDateFormat.parse(obDomestic1.getFirstPaymentDateTime());
		FrequencyUtil frequencyUtil=new FrequencyUtil();
		String frequencyProcessLayerToCma = frequencyUtil.CmaToProcessLayer("IntrvlMnthDay:01:01",date);	
		obDomestic1.setFrequency(frequencyProcessLayerToCma);
		
		data.setInitiation(obDomestic1);
		//authorisation.completionDateTime(dateFormat.format(new Date()));
		deliveryAddress.setAddressLine(addressLine);
		standingOrderConsentsRequest.setData(data);
		standingOrderConsentsRequest.setRisk(risk);
		 Map<String, String> params=new HashMap<String, String>();
		 params.put("Channel", "B365");
		transformer.transformDomesticStandingOrdersResponseFromAPIToFDForInsert(standingOrderConsentsRequest,params);
		obDomestic1.setFirstPaymentDateTime(null);
		transformer.transformDomesticStandingOrdersResponseFromAPIToFDForInsert(standingOrderConsentsRequest,params);
	}

	
	@Test
	public void testTransformDomesticStandingOrdersResponseFromAPIToFDForInsertForNull(){
		
		CustomDStandingOrderPOSTRequest standingOrderConsentsRequest = new CustomDStandingOrderPOSTRequest();
		
		OBWriteDataDomesticStandingOrder1 data = new OBWriteDataDomesticStandingOrder1();
		OBRisk1 risk = new OBRisk1();
		OBCashAccountDebtor3 debtorAccount = new OBCashAccountDebtor3();
		OBRisk1DeliveryAddress deliveryAddress = new OBRisk1DeliveryAddress();
		OBCashAccountCreditor2 creditorAccount = new OBCashAccountCreditor2();
		OBDomesticStandingOrder1 obDomestic1 = new OBDomesticStandingOrder1();
		List<String> addressLine = new ArrayList<String>();
		addressLine.add("fbf");
		addressLine.add("sfsdf");
		
		
		deliveryAddress.setStreetName("abc");
		
		risk.setPaymentContextCode(OBExternalPaymentContext1Code.BILLPAYMENT);
		risk.setDeliveryAddress(deliveryAddress);
		debtorAccount.setSchemeName("gfh562");
		creditorAccount.setSchemeName("gfh562");

		data.setInitiation(obDomestic1);
		deliveryAddress.setAddressLine(addressLine);
		standingOrderConsentsRequest.setData(data);
		standingOrderConsentsRequest.setRisk(risk);
		
		 Map<String, String> params=new HashMap<String, String>();
		 params.put("Channel", "B365");
		transformer.transformDomesticStandingOrdersResponseFromAPIToFDForInsert(standingOrderConsentsRequest,params);
	}
}