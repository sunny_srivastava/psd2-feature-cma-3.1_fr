package com.capgemini.psd2.pisp.dsp.test.transformer.impl;

import java.util.HashMap;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.bind.annotation.RequestMethod;


import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.domain.CustomDSPaymentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomDSPaymentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.Links;
import com.capgemini.psd2.pisp.domain.Meta;
import com.capgemini.psd2.pisp.domain.OBCashAccountDebtor3;
import com.capgemini.psd2.pisp.domain.OBDomesticScheduled1;
import com.capgemini.psd2.pisp.domain.OBExternalStatus1Code;
import com.capgemini.psd2.pisp.domain.OBExternalStatus2Code;
import com.capgemini.psd2.pisp.domain.OBMultiAuthorisation1;
import com.capgemini.psd2.pisp.domain.OBWriteDataDomesticScheduled1;
import com.capgemini.psd2.pisp.domain.OBWriteDataDomesticScheduledResponse1;
import com.capgemini.psd2.pisp.domain.PaymentConsentsPlatformResource;
import com.capgemini.psd2.pisp.domain.PaymentsPlatformResource;
import com.capgemini.psd2.pisp.dsp.test.transformer.impl.Mock.DomesticScheduledPaymentsTransformerImplMockData;
import com.capgemini.psd2.pisp.dsp.transformer.impl.DSPaymentsResponseTransformerImpl;
import com.capgemini.psd2.pisp.payment.submission.platform.adapter.PaymentsPlatformAdapter;
import com.capgemini.psd2.pisp.status.PaymentConstants;
import com.capgemini.psd2.pisp.utilities.PispDateUtility;

@RunWith(SpringJUnit4ClassRunner.class)
public class DSPaymentsResponseTransformerImplTest {
  
	@Mock
	private RequestHeaderAttributes reqHeaderAtrributes;

	@Mock
	private PaymentsPlatformAdapter paymentsPlatformAdapter;

	@Mock
	private PispDateUtility pispDateUtility;

	@InjectMocks 
	private DSPaymentsResponseTransformerImpl transformer;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void contextLoads() {
	}

	@After
	public void tearDown() {
		reqHeaderAtrributes = null;
		paymentsPlatformAdapter = null;
		pispDateUtility = null;
		transformer = null;
	}
	
	@Test
	public void updateMetaAndLinksTest() {
		CustomDSPaymentsPOSTResponse domesticScheduledPaymentsResponse = new CustomDSPaymentsPOSTResponse();
		Links links = new Links();
		Meta meta = new Meta();
		OBWriteDataDomesticScheduledResponse1 data = new OBWriteDataDomesticScheduledResponse1();
		data.setDomesticScheduledPaymentId("abc123");
		
		domesticScheduledPaymentsResponse.setLinks(links);
		domesticScheduledPaymentsResponse.setMeta(meta);
		domesticScheduledPaymentsResponse.setData(data);
			
		String methodType = RequestMethod.POST.toString();
		
		transformer.updateMetaAndLinks(domesticScheduledPaymentsResponse, methodType);
		
	}
	
	@Test
	public void paymentSubmissionResponseTransformerTest_POST(){
		DomesticScheduledPaymentsTransformerImplMockData domesticScheduledPaymentsTransformerImplMockData = new DomesticScheduledPaymentsTransformerImplMockData();
		
		Map<String, Object> paymentsPlatformResourceMap = new HashMap<>();
		String methodType = RequestMethod.POST.toString();
		
		PaymentsPlatformResource paymentsPlatformResource = new PaymentsPlatformResource();
		paymentsPlatformResource.setCreatedAt("");
		paymentsPlatformResource.setStatus("InitiationCompleted");
		paymentsPlatformResource.setStatusUpdateDateTime("");
		
		CustomDSPaymentsPOSTResponse domesticScheduledPaymentsResponse = new CustomDSPaymentsPOSTResponse();
	
		//Setting Consents
		PaymentConsentsPlatformResource paymentConsentsPlatformResponse = new PaymentConsentsPlatformResource();
		paymentConsentsPlatformResponse.setTppDebtorDetails("false");
				
		domesticScheduledPaymentsResponse.setData(domesticScheduledPaymentsTransformerImplMockData.getOBWriteDataDomesticScheduledResponse1());
		domesticScheduledPaymentsResponse.getData().setMultiAuthorisation(new OBMultiAuthorisation1());
		domesticScheduledPaymentsResponse.getData().getMultiAuthorisation().setStatus(OBExternalStatus2Code.AUTHORISED);
		paymentsPlatformResourceMap.put(PaymentConstants.SUBMISSION, paymentsPlatformResource);
		paymentsPlatformResourceMap.put(PaymentConstants.CONSENT, paymentConsentsPlatformResponse);
	
		domesticScheduledPaymentsResponse.getData().setStatus(OBExternalStatus1Code.INITIATIONCOMPLETED);
		
		domesticScheduledPaymentsResponse.getData().getInitiation().setDebtorAccount(null);
		
		transformer.paymentSubmissionResponseTransformer(domesticScheduledPaymentsResponse,paymentsPlatformResourceMap,methodType);	
		
	}
	
	
	@Test
	public void paymentSubmissionResponseTransformerTest_InitiationCompleted(){
		DomesticScheduledPaymentsTransformerImplMockData domesticScheduledPaymentsTransformerImplMockData = new DomesticScheduledPaymentsTransformerImplMockData();
		
		Map<String, Object> paymentsPlatformResourceMap = new HashMap<>();
		String methodType = RequestMethod.PUT.toString();
		
		PaymentsPlatformResource paymentsPlatformResource = new PaymentsPlatformResource();
		paymentsPlatformResource.setCreatedAt("");
		paymentsPlatformResource.setStatus("InitiationPending");
		paymentsPlatformResource.setStatusUpdateDateTime("");
		
		CustomDSPaymentsPOSTResponse domesticScheduledPaymentsResponse = new CustomDSPaymentsPOSTResponse();
	
		//Setting Consents
		PaymentConsentsPlatformResource paymentConsentsPlatformResponse = new PaymentConsentsPlatformResource();
			
		paymentConsentsPlatformResponse.setTppDebtorDetails("True");
		paymentConsentsPlatformResponse.setTppDebtorNameDetails(null);
		domesticScheduledPaymentsResponse.setData(domesticScheduledPaymentsTransformerImplMockData.getOBWriteDataDomesticScheduledResponse1());
		domesticScheduledPaymentsResponse.getData().getInitiation().setDebtorAccount(new OBCashAccountDebtor3());
		domesticScheduledPaymentsResponse.getData().getInitiation().getDebtorAccount().setSchemeName("IBAN");
		domesticScheduledPaymentsResponse.getData().setMultiAuthorisation(new OBMultiAuthorisation1());
		domesticScheduledPaymentsResponse.getData().getMultiAuthorisation().setStatus(OBExternalStatus2Code.AUTHORISED);
		paymentsPlatformResourceMap.put(PaymentConstants.SUBMISSION, paymentsPlatformResource);
		paymentsPlatformResourceMap.put(PaymentConstants.CONSENT, paymentConsentsPlatformResponse);
	
		domesticScheduledPaymentsResponse.getData().setStatus(OBExternalStatus1Code.INITIATIONCOMPLETED);
		
		transformer.paymentSubmissionResponseTransformer(domesticScheduledPaymentsResponse,paymentsPlatformResourceMap,methodType);	
		
	}
	
	@Test
	public void paymentSubmissionResponseTransformerTest_PendingAuthorized(){
		DomesticScheduledPaymentsTransformerImplMockData domesticScheduledPaymentsTransformerImplMockData = new DomesticScheduledPaymentsTransformerImplMockData();
		
		Map<String, Object> paymentsPlatformResourceMap = new HashMap<>();
		String methodType = RequestMethod.PUT.toString();
		
		PaymentsPlatformResource paymentsPlatformResource = new PaymentsPlatformResource();
		paymentsPlatformResource.setCreatedAt("");
		paymentsPlatformResource.setStatus(OBExternalStatus1Code.INITIATIONPENDING.toString());
		paymentsPlatformResource.setStatusUpdateDateTime("");
		
		CustomDSPaymentsPOSTResponse domesticScheduledPaymentsResponse = new CustomDSPaymentsPOSTResponse();
		
		//Setting Consents
		PaymentConsentsPlatformResource paymentConsentsPlatformResponse = new PaymentConsentsPlatformResource();
		paymentConsentsPlatformResponse.setTppDebtorDetails("false");

		domesticScheduledPaymentsResponse.setData(domesticScheduledPaymentsTransformerImplMockData.getOBWriteDataDomesticScheduledResponse1());
		
		OBMultiAuthorisation1 multiAuth = new OBMultiAuthorisation1();
		multiAuth.setStatus(OBExternalStatus2Code.AUTHORISED);
		domesticScheduledPaymentsResponse.getData().setMultiAuthorisation(multiAuth);

		paymentsPlatformResourceMap.put(PaymentConstants.SUBMISSION, paymentsPlatformResource);
		paymentsPlatformResourceMap.put(PaymentConstants.CONSENT, paymentConsentsPlatformResponse);
	
		domesticScheduledPaymentsResponse.getData().setStatus(OBExternalStatus1Code.INITIATIONPENDING);

		
		domesticScheduledPaymentsResponse.getData().getInitiation().setDebtorAccount(null);
		
		transformer.paymentSubmissionResponseTransformer(domesticScheduledPaymentsResponse,paymentsPlatformResourceMap,methodType);	
		
	}

	@Test
	public void paymentSubmissionResponseTransformerTest_PendingRejected(){
		DomesticScheduledPaymentsTransformerImplMockData domesticScheduledPaymentsTransformerImplMockData = new DomesticScheduledPaymentsTransformerImplMockData();
		
		Map<String, Object> paymentsPlatformResourceMap = new HashMap<>();
		String methodType = RequestMethod.PUT.toString();
		
		PaymentsPlatformResource paymentsPlatformResource = new PaymentsPlatformResource();
		paymentsPlatformResource.setCreatedAt("");
		paymentsPlatformResource.setStatus(OBExternalStatus1Code.INITIATIONPENDING.toString());
		paymentsPlatformResource.setStatusUpdateDateTime("");
		
		CustomDSPaymentsPOSTResponse domesticScheduledPaymentsResponse = new CustomDSPaymentsPOSTResponse();
	
		//Setting Consents
		PaymentConsentsPlatformResource paymentConsentsPlatformResponse = new PaymentConsentsPlatformResource();
		paymentConsentsPlatformResponse.setTppDebtorDetails("false");
	
		domesticScheduledPaymentsResponse.setData(domesticScheduledPaymentsTransformerImplMockData.getOBWriteDataDomesticScheduledResponse1());
		
		OBMultiAuthorisation1 multiAuth = new OBMultiAuthorisation1();
		multiAuth.setStatus(OBExternalStatus2Code.REJECTED);
		domesticScheduledPaymentsResponse.getData().setMultiAuthorisation(multiAuth);

		paymentsPlatformResourceMap.put(PaymentConstants.SUBMISSION, paymentsPlatformResource);
		paymentsPlatformResourceMap.put(PaymentConstants.CONSENT, paymentConsentsPlatformResponse);
		
		domesticScheduledPaymentsResponse.getData().setStatus(OBExternalStatus1Code.INITIATIONPENDING);
		
		domesticScheduledPaymentsResponse.getData().getInitiation().setDebtorAccount(null);
		
		transformer.paymentSubmissionResponseTransformer(domesticScheduledPaymentsResponse,paymentsPlatformResourceMap,methodType);	
		
	}
	
	@Test
	public void paymentSubmissionRequestTransformerTest(){
		CustomDSPaymentsPOSTRequest domesticScheduledpaymentSubmissionRequest = new CustomDSPaymentsPOSTRequest();
		OBWriteDataDomesticScheduled1 data = new OBWriteDataDomesticScheduled1();
		OBDomesticScheduled1 initiation = new OBDomesticScheduled1();
		initiation.requestedExecutionDateTime("2018-05-09T11:10:12.064Z");
		domesticScheduledpaymentSubmissionRequest.setData(data);
		data.setInitiation(initiation);
		transformer.paymentSubmissionRequestTransformer(domesticScheduledpaymentSubmissionRequest);
		
	}

}
