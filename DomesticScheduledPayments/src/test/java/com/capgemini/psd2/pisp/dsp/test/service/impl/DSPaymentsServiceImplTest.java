package com.capgemini.psd2.pisp.dsp.test.service.impl;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.capgemini.psd2.consent.domain.PispConsent;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.adapter.DomesticScheduledPaymentsAdapter;
import com.capgemini.psd2.pisp.adapter.PispConsentAdapter;
import com.capgemini.psd2.pisp.domain.CustomDSPaymentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomDSPaymentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.PaymentsPlatformResource;
import com.capgemini.psd2.pisp.dsp.comparator.DSPaymentsPayloadComparator;
import com.capgemini.psd2.pisp.dsp.service.impl.DSPaymentsServiceImpl;
import com.capgemini.psd2.pisp.dsp.test.models.DSPTestData;
import com.capgemini.psd2.pisp.processing.adapter.service.PaymentSubmissionProcessingAdapter;
import com.capgemini.psd2.pisp.stage.operations.adapter.DomesticScheduledPaymentStagingAdapter;
import com.capgemini.psd2.pisp.status.PaymentConstants;

@RunWith(SpringJUnit4ClassRunner.class)
public class DSPaymentsServiceImplTest {

	@Mock
	private PaymentSubmissionProcessingAdapter<CustomDSPaymentsPOSTRequest, CustomDSPaymentsPOSTResponse> paymentsProcessingAdapter;

	@Mock
	private DomesticScheduledPaymentsAdapter dsPaymentsAdapter;

	@Mock
	private DomesticScheduledPaymentStagingAdapter dsPaymentConsentsAdapter;

	@Mock
	private DSPaymentsPayloadComparator dsPaymentsComparator;
	
	@Mock
	private PispConsentAdapter pispConsentAdapter;
	
	@Mock
	private RequestHeaderAttributes requestHeaderAttr;

	@InjectMocks
	private DSPaymentsServiceImpl service;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void contextLoads() {
	}
	
	@Test
	public void createDSPResourceFreshRequestTest() {
		Map<String, Object> paymentsPlatformResourceMap = new HashMap<>();
		PispConsent pispConsent = new PispConsent();
		pispConsent.setPartyIdentifier("123");
		
		paymentsPlatformResourceMap.put(PaymentConstants.CONSENT, DSPTestData.getConsentPlatformResource());
		Mockito.when(paymentsProcessingAdapter.preSubmissionProcessing(any(), any(), any())).thenReturn(paymentsPlatformResourceMap);
		Mockito.when(dsPaymentConsentsAdapter.retrieveStagedDomesticScheduledPaymentConsents(any(), any())).thenReturn(DSPTestData.getConsentResponse());
		Mockito.when(dsPaymentsComparator.comparePaymentDetails(any(), any(), any())).thenReturn(0);
		Mockito.when(paymentsProcessingAdapter.createInitialPaymentsPlatformResource(any(), any(), any())).thenReturn(DSPTestData.getPlatformResource());
		Mockito.when(dsPaymentsAdapter.processDSPayments(any(), any(), any())).thenReturn(DSPTestData.getGenericResponse());
		Mockito.when(paymentsProcessingAdapter.postPaymentProcessFlows(any(), any(), any(), any(), any(), any())).thenReturn(DSPTestData.getPostResponse());
		Mockito.when(pispConsentAdapter.retrieveConsentByPaymentId(any())).thenReturn(pispConsent);
		
		service.createDomesticScheduledPaymentsResource(DSPTestData.getRequest());
	}
	
	@Test
	public void createDSPResourceFreshWithDebtorRequestTest() {
		Map<String, Object> paymentsPlatformResourceMap = new HashMap<>();
		PispConsent pispConsent = new PispConsent();
		pispConsent.setPartyIdentifier("123");
		paymentsPlatformResourceMap.put(PaymentConstants.CONSENT, DSPTestData.getConsentPlatformResource());
		Mockito.when(paymentsProcessingAdapter.preSubmissionProcessing(any(), any(), any())).thenReturn(paymentsPlatformResourceMap);
		Mockito.when(dsPaymentConsentsAdapter.retrieveStagedDomesticScheduledPaymentConsents(any(), any())).thenReturn(DSPTestData.getConsentResponse());
		Mockito.when(dsPaymentsComparator.comparePaymentDetails(any(), any(), any())).thenReturn(0);
		Mockito.when(paymentsProcessingAdapter.createInitialPaymentsPlatformResource(any(), any(), any())).thenReturn(DSPTestData.getPlatformResource());
		Mockito.when(dsPaymentsAdapter.processDSPayments(any(), any(), any())).thenReturn(DSPTestData.getGenericResponse());
		Mockito.when(paymentsProcessingAdapter.postPaymentProcessFlows(any(), any(), any(), any(), any(), any())).thenReturn(DSPTestData.getPostResponse());
		Mockito.when(pispConsentAdapter.retrieveConsentByPaymentId(any())).thenReturn(pispConsent);
		service.createDomesticScheduledPaymentsResource(DSPTestData.getRequestwithDebtor());
	}
	
	@Test
	public void createDSPResourceOldRequestTest() {
		Map<String, Object> paymentsPlatformResourceMap = new HashMap<>();
		paymentsPlatformResourceMap.put(PaymentConstants.CONSENT, DSPTestData.getConsentPlatformResource());
		PaymentsPlatformResource platformResource = DSPTestData.getPlatformResource();
		platformResource.setProccessState(PaymentConstants.COMPLETED);
		paymentsPlatformResourceMap.put(PaymentConstants.SUBMISSION, platformResource);
		Mockito.when(paymentsProcessingAdapter.preSubmissionProcessing(any(), any(), any())).thenReturn(paymentsPlatformResourceMap);
		Mockito.when(dsPaymentConsentsAdapter.retrieveStagedDomesticScheduledPaymentConsents(any(), any())).thenReturn(DSPTestData.getConsentResponse());
		Mockito.when(dsPaymentsComparator.comparePaymentDetails(any(), any(), any())).thenReturn(0);
		Mockito.when(paymentsProcessingAdapter.createInitialPaymentsPlatformResource(any(), any(), any())).thenReturn(DSPTestData.getPlatformResource());
		Mockito.when(dsPaymentsAdapter.processDSPayments(any(), any(), any())).thenReturn(DSPTestData.getGenericResponse());
		Mockito.when(dsPaymentsAdapter.retrieveStagedDSPaymentsResponse(any(), any())).thenReturn(DSPTestData.getPostResponse());
		Mockito.when(paymentsProcessingAdapter.postPaymentProcessFlows(any(), any(), any(), any(), any(), any())).thenReturn(DSPTestData.getPostResponse());
		service.createDomesticScheduledPaymentsResource(DSPTestData.getRequest());
	}
	
	@Test(expected=PSD2Exception.class)
	public void createDSPResourceOldEmptyRequestTest() {
		Map<String, Object> paymentsPlatformResourceMap = new HashMap<>();
		paymentsPlatformResourceMap.put(PaymentConstants.CONSENT, DSPTestData.getConsentPlatformResource());
		PaymentsPlatformResource platformResource = DSPTestData.getPlatformResource();
		platformResource.setProccessState(PaymentConstants.COMPLETED);
		paymentsPlatformResourceMap.put(PaymentConstants.SUBMISSION, platformResource);
		Mockito.when(paymentsProcessingAdapter.preSubmissionProcessing(any(), any(), any())).thenReturn(paymentsPlatformResourceMap);
		Mockito.when(dsPaymentConsentsAdapter.retrieveStagedDomesticScheduledPaymentConsents(any(), any())).thenReturn(DSPTestData.getConsentResponse());
		Mockito.when(dsPaymentsComparator.comparePaymentDetails(any(), any(), any())).thenReturn(0);
		Mockito.when(paymentsProcessingAdapter.createInitialPaymentsPlatformResource(any(), any(), any())).thenReturn(DSPTestData.getPlatformResource());
		Mockito.when(dsPaymentsAdapter.processDSPayments(any(), any(), any())).thenReturn(DSPTestData.getGenericResponse());
		Mockito.when(dsPaymentsAdapter.retrieveStagedDSPaymentsResponse(any(), any())).thenReturn(null);
		Mockito.when(paymentsProcessingAdapter.postPaymentProcessFlows(any(), any(), any(), any(), any(), any())).thenReturn(DSPTestData.getPostResponse());
		service.createDomesticScheduledPaymentsResource(DSPTestData.getRequest());
	}
	
	@Test
	public void retrieveDSPResponse() {
		Map<String, Object> paymentsPlatformResourceMap = new HashMap<>();
		paymentsPlatformResourceMap.put(PaymentConstants.CONSENT, DSPTestData.getConsentPlatformResource());
		PaymentsPlatformResource platformResource = DSPTestData.getPlatformResource();
		platformResource.setProccessState(PaymentConstants.COMPLETED);
		paymentsPlatformResourceMap.put(PaymentConstants.SUBMISSION, platformResource);
		Mockito.when(paymentsProcessingAdapter.prePaymentProcessGETFlows(any(), any())).thenReturn(paymentsPlatformResourceMap);
		Mockito.when(dsPaymentsAdapter.retrieveStagedDSPaymentsResponse(any(), any())).thenReturn(DSPTestData.getPostResponse());
		Mockito.when(dsPaymentsComparator.comparePaymentDetails(any(), any(), any())).thenReturn(0);
		
		PispConsent consent = new PispConsent();
		consent.setPsuId("12345");
		when(pispConsentAdapter.retrieveConsentByPaymentId(anyObject())).thenReturn(consent);
		
		service.retrieveDomesticScheduledPaymentsResource("12345");
	}
	
	@Test(expected=PSD2Exception.class)
	public void retrieveDSPEmptyResponse() {
		Map<String, Object> paymentsPlatformResourceMap = new HashMap<>();
		paymentsPlatformResourceMap.put(PaymentConstants.CONSENT, DSPTestData.getConsentPlatformResource());
		PaymentsPlatformResource platformResource = DSPTestData.getPlatformResource();
		platformResource.setProccessState(PaymentConstants.COMPLETED);
		paymentsPlatformResourceMap.put(PaymentConstants.SUBMISSION, platformResource);
		Mockito.when(paymentsProcessingAdapter.prePaymentProcessGETFlows(any(), any())).thenReturn(paymentsPlatformResourceMap);
		Mockito.when(dsPaymentsAdapter.retrieveStagedDSPaymentsResponse(any(), any())).thenReturn(null);
		Mockito.when(dsPaymentsComparator.comparePaymentDetails(any(), any(), any())).thenReturn(0);
		service.retrieveDomesticScheduledPaymentsResource("12345");
	}

	@After
	public void tearDown() {
		paymentsProcessingAdapter = null;
		dsPaymentsAdapter = null;
		dsPaymentConsentsAdapter = null;
		dsPaymentsComparator = null;
		service = null;
	}

}