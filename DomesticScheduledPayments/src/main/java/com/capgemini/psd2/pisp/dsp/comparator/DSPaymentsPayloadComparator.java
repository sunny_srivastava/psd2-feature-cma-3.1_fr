package com.capgemini.psd2.pisp.dsp.comparator;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

import org.springframework.stereotype.Component;

import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.pisp.domain.CustomDSPConsentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomDSPConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.CustomDSPaymentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.OBDomestic1InstructedAmount;
import com.capgemini.psd2.pisp.domain.OBDomesticScheduled1;
import com.capgemini.psd2.pisp.domain.OBPostalAddress6;
import com.capgemini.psd2.pisp.domain.OBRemittanceInformation1;
import com.capgemini.psd2.pisp.domain.OBRisk1DeliveryAddress;
import com.capgemini.psd2.pisp.domain.OBWriteDomesticScheduledConsentResponse1;
import com.capgemini.psd2.pisp.domain.PaymentConsentsPlatformResource;
import com.capgemini.psd2.pisp.utilities.PispUtilities;
import com.capgemini.psd2.utilities.JSONUtilities;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Component
public class DSPaymentsPayloadComparator implements Comparator<CustomDSPConsentsPOSTResponse> {

	@Override
	public int compare(CustomDSPConsentsPOSTResponse paymentResponse,
			CustomDSPConsentsPOSTResponse adaptedPaymentResponse) {
		int value = 1;

		if (Double.compare(Double.parseDouble(paymentResponse.getData().getInitiation().getInstructedAmount().getAmount()), Double
				.parseDouble(adaptedPaymentResponse.getData().getInitiation().getInstructedAmount().getAmount()))==0) {
			adaptedPaymentResponse.getData().getInitiation().getInstructedAmount()
					.setAmount(paymentResponse.getData().getInitiation().getInstructedAmount().getAmount());
		}

		if (paymentResponse.getData().getInitiation().equals(adaptedPaymentResponse.getData().getInitiation())
				&& paymentResponse.getRisk().equals(adaptedPaymentResponse.getRisk()))
			value = 0;

		return value;
	}

	public int comparePaymentDetails(Object response, Object request,
			PaymentConsentsPlatformResource paymentSetupPlatformResource) {
		CustomDSPConsentsPOSTResponse paymentSetupResponse = null;
		CustomDSPConsentsPOSTResponse tempPaymentSetupResponse = null;
		CustomDSPConsentsPOSTResponse adaptedPaymentResponse = null;
		CustomDSPConsentsPOSTRequest paymentSetupRequest = null;
		CustomDSPaymentsPOSTRequest paymentSubmissionRequest = null;
		int returnValue = 1;

		if (response instanceof CustomDSPConsentsPOSTResponse) {
			paymentSetupResponse = (CustomDSPConsentsPOSTResponse) response;
			String creationDateTime = paymentSetupResponse.getData().getCreationDateTime();
			paymentSetupResponse.getData().setCreationDateTime(null);
			String strPaymentResponse = JSONUtilities.getJSONOutPutFromObject(paymentSetupResponse);
			tempPaymentSetupResponse = JSONUtilities.getObjectFromJSONString(PispUtilities.getObjectMapper(),
					strPaymentResponse, CustomDSPConsentsPOSTResponse.class);
			paymentSetupResponse.getData().setCreationDateTime(creationDateTime);
		}

		if (request instanceof CustomDSPConsentsPOSTRequest) {
			paymentSetupRequest = (CustomDSPConsentsPOSTRequest) request;
			String strPaymentRequest = JSONUtilities.getJSONOutPutFromObject(paymentSetupRequest);
			adaptedPaymentResponse = JSONUtilities.getObjectFromJSONString(PispUtilities.getObjectMapper(),
					strPaymentRequest, CustomDSPConsentsPOSTResponse.class);
		} else if (request instanceof CustomDSPaymentsPOSTRequest) {
			paymentSubmissionRequest = (CustomDSPaymentsPOSTRequest) request;
			String strSubmissionRequest = JSONUtilities.getJSONOutPutFromObject(paymentSubmissionRequest);
			adaptedPaymentResponse = JSONUtilities.getObjectFromJSONString(PispUtilities.getObjectMapper(),
					strSubmissionRequest, CustomDSPConsentsPOSTResponse.class);
		}

		if (tempPaymentSetupResponse != null && adaptedPaymentResponse != null) {

			if (!validateDebtorDetails(tempPaymentSetupResponse.getData().getInitiation(),
					adaptedPaymentResponse.getData().getInitiation(), paymentSetupPlatformResource))
				return 1;
			compareAmount(tempPaymentSetupResponse.getData().getInitiation().getInstructedAmount(),
					adaptedPaymentResponse.getData().getInitiation().getInstructedAmount());

			/*
			 * Date: 13-Feb-2018 Changes are made for CR-10
			 */
			checkAndModifyEmptyFields(adaptedPaymentResponse, (CustomDSPConsentsPOSTResponse) response);
			// End of CR-10

			returnValue = compare(tempPaymentSetupResponse, adaptedPaymentResponse);
		}
		return returnValue;
	}

	public boolean validateDebtorDetails(OBDomesticScheduled1 responseInitiation,
			OBDomesticScheduled1 requestInitiation, PaymentConsentsPlatformResource paymentSetupPlatformResource) {

		if (Boolean.valueOf(paymentSetupPlatformResource.getTppDebtorDetails())) {

			/*
			 * If DebtorAccount.Name is sent by TPP,tppDebtoNameDetails would be true, then
			 * name will participate in comparison else product will set foundation response
			 * DebtorAccount.Name as null so that it can be passed in comparison. Both
			 * fields would have the value as 'Null'.
			 */
			if (!Boolean.valueOf(paymentSetupPlatformResource.getTppDebtorNameDetails()))
				responseInitiation.getDebtorAccount().setName(null);
			String schemeNameOfResponseInitiation = responseInitiation.getDebtorAccount().getSchemeName();
			String schemeNameOfRequestInitiation = requestInitiation.getDebtorAccount().getSchemeName();
			if(schemeNameOfResponseInitiation!= null && schemeNameOfResponseInitiation.startsWith(PSD2Constants.OB_SCHEME_PREFIX)){
				responseInitiation.getDebtorAccount().setSchemeName(schemeNameOfResponseInitiation.split(PSD2Constants.OB_SCHEME_PREFIX)[1]);
		
			}
			if (schemeNameOfRequestInitiation!= null && schemeNameOfRequestInitiation.startsWith(PSD2Constants.OB_SCHEME_PREFIX)) {
				requestInitiation.getDebtorAccount().setSchemeName(schemeNameOfRequestInitiation.split(PSD2Constants.OB_SCHEME_PREFIX)[1]);
			}
			return Objects.equals(responseInitiation.getDebtorAccount(), requestInitiation.getDebtorAccount());
		}
		if (!Boolean.valueOf(paymentSetupPlatformResource.getTppDebtorDetails())
				&& requestInitiation.getDebtorAccount() != null) {
			return Boolean.FALSE;
		}
		if (!Boolean.valueOf(paymentSetupPlatformResource.getTppDebtorDetails())
				&& responseInitiation.getDebtorAccount() != null) {
			responseInitiation.setDebtorAccount(null);
			return Boolean.TRUE;
		}
		return Boolean.TRUE;
	}

	/*
	 * Date: 12-Jan-2018, SIT defect#956 Comparing two float values received from FS
	 * and TPP. Both values must be treated equal mathematically if values are in
	 * below format. i.e 1.2 == 1.20, 0.20000 == 0.20, 0.2 == 00.20000, 0.2 ==
	 * 0.20000, 001.00 == 1.00 etc. If values are equal then set the amount received
	 * from TPP to the amount object of FS response. By doing this, comparison would
	 * be passed in swagger's java file(compare method of this class) and same
	 * amount would be available to TPP also in response of idempotent request.
	 */
	private void compareAmount(OBDomestic1InstructedAmount responseInstructedAmount,
			OBDomestic1InstructedAmount requestInstructedAmount) {
		if (Double.compare(Double.parseDouble(responseInstructedAmount.getAmount()),
				Double.parseDouble(requestInstructedAmount.getAmount())) == 0)
			responseInstructedAmount.setAmount(requestInstructedAmount.getAmount());
	}

	private void checkAndModifyEmptyFields(OBWriteDomesticScheduledConsentResponse1 adaptedPaymentResponse,
			CustomDSPConsentsPOSTResponse response) {

		checkAndModifyRemittanceInformation(adaptedPaymentResponse.getData().getInitiation(), response);
		checkAndModifyCreditorPostalAddress(adaptedPaymentResponse.getData().getInitiation(), response);

		if (adaptedPaymentResponse.getRisk().getDeliveryAddress() != null) {
			checkAndModifyAddressLine(adaptedPaymentResponse.getRisk().getDeliveryAddress(), response);
		}

		if (adaptedPaymentResponse.getData().getInitiation().getCreditorPostalAddress() != null
				&& adaptedPaymentResponse.getData().getInitiation().getCreditorPostalAddress()
						.getAddressLine() != null) {
			checkAndModifyCreditorPostalAddressLine(
					adaptedPaymentResponse.getData().getInitiation().getCreditorPostalAddress(), response);
		}
	}

	private void checkAndModifyRemittanceInformation(OBDomesticScheduled1 responseInitiation,
			CustomDSPConsentsPOSTResponse response) {

		OBRemittanceInformation1 remittanceInformation = responseInitiation.getRemittanceInformation();
		if (remittanceInformation != null && response.getData().getInitiation().getRemittanceInformation() == null
				&& NullCheckUtils.isNullOrEmpty(remittanceInformation.getReference())
				&& NullCheckUtils.isNullOrEmpty(remittanceInformation.getUnstructured())) {

			responseInitiation.setRemittanceInformation(null);
		}
	}

	/* SIT Defect fix #2272 */
	private void checkAndModifyCreditorPostalAddress(OBDomesticScheduled1 responseInitiation,
			CustomDSPConsentsPOSTResponse response) {

		OBPostalAddress6 crPostaladdInformation = responseInitiation.getCreditorPostalAddress();
		if (crPostaladdInformation != null && response.getData().getInitiation().getCreditorPostalAddress() == null
				&& NullCheckUtils.isNullOrEmpty(crPostaladdInformation.getAddressLine())
				&& NullCheckUtils.isNullOrEmpty(crPostaladdInformation.getAddressType())
				&& NullCheckUtils.isNullOrEmpty(crPostaladdInformation.getDepartment())
				&& NullCheckUtils.isNullOrEmpty(crPostaladdInformation.getSubDepartment())
				&& NullCheckUtils.isNullOrEmpty(crPostaladdInformation.getStreetName())
				&& NullCheckUtils.isNullOrEmpty(crPostaladdInformation.getBuildingNumber())
				&& NullCheckUtils.isNullOrEmpty(crPostaladdInformation.getPostCode())
				&& NullCheckUtils.isNullOrEmpty(crPostaladdInformation.getTownName())
				&& NullCheckUtils.isNullOrEmpty(crPostaladdInformation.getCountrySubDivision())
				&& NullCheckUtils.isNullOrEmpty(crPostaladdInformation.getCountry())) {

			responseInitiation.setCreditorPostalAddress(null);
		}
	}

	private void checkAndModifyAddressLine(OBRisk1DeliveryAddress riskDeliveryAddress,
			CustomDSPConsentsPOSTResponse response) {

		if (riskDeliveryAddress.getAddressLine() != null) {
			List<String> addressLineList = new ArrayList<>();
			for (String addressLine : riskDeliveryAddress.getAddressLine()) {
				if (NullCheckUtils.isNullOrEmpty(addressLine))
					continue;
				addressLineList.add(addressLine);
			}
			if (addressLineList.isEmpty() && response.getRisk().getDeliveryAddress().getAddressLine() == null)
				addressLineList = null;

			riskDeliveryAddress.setAddressLine(addressLineList);
		}
	}

	private void checkAndModifyCreditorPostalAddressLine(OBPostalAddress6 obPostalAddress6,
			CustomDSPConsentsPOSTResponse response) {
		List<String> addressLineList = new ArrayList<>();
		for (String addressLine : (obPostalAddress6.getAddressLine())) {
			if (NullCheckUtils.isNullOrEmpty(addressLine))
				continue;
			addressLineList.add(addressLine);
		}
		if (addressLineList.isEmpty()
				&& response.getData().getInitiation().getCreditorPostalAddress().getAddressLine() == null)
			addressLineList = null;

		obPostalAddress6.setAddressLine(addressLineList);
	}
}
