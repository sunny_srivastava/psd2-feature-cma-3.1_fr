package com.capgemini.psd2.pisp.dsp.service.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.amazonaws.HttpMethod;
import com.capgemini.psd2.consent.domain.PispConsent;
import com.capgemini.psd2.exceptions.ErrorCodeEnum;
import com.capgemini.psd2.exceptions.ErrorMapKeys;
import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.InternalServerErrorMessage;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.adapter.DomesticScheduledPaymentsAdapter;
import com.capgemini.psd2.pisp.adapter.PispConsentAdapter;
import com.capgemini.psd2.pisp.domain.CustomDSPConsentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.CustomDSPaymentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomDSPaymentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.CustomPaymentSetupPlatformDetails;
import com.capgemini.psd2.pisp.domain.GenericPaymentsResponse;
import com.capgemini.psd2.pisp.domain.OBExternalStatus1Code;
import com.capgemini.psd2.pisp.domain.OBExternalStatus2Code;
import com.capgemini.psd2.pisp.domain.OBPostalAddress6;
import com.capgemini.psd2.pisp.domain.OBRemittanceInformation1;
import com.capgemini.psd2.pisp.domain.PaymentConsentsPlatformResource;
import com.capgemini.psd2.pisp.domain.PaymentsPlatformResource;
import com.capgemini.psd2.pisp.dsp.comparator.DSPaymentsPayloadComparator;
import com.capgemini.psd2.pisp.dsp.service.DSPaymentsService;
import com.capgemini.psd2.pisp.enums.PaymentTypeEnum;
import com.capgemini.psd2.pisp.processing.adapter.service.PaymentSubmissionProcessingAdapter;
import com.capgemini.psd2.pisp.stage.domain.CustomPaymentStageIdentifiers;
import com.capgemini.psd2.pisp.stage.operations.adapter.DomesticScheduledPaymentStagingAdapter;
import com.capgemini.psd2.pisp.status.PaymentConstants;
import com.capgemini.psd2.pisp.utilities.PispUtilities;
import com.capgemini.psd2.utilities.JSONUtilities;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Service
public class DSPaymentsServiceImpl implements DSPaymentsService {

	@Autowired
	private PaymentSubmissionProcessingAdapter<CustomDSPaymentsPOSTRequest, CustomDSPaymentsPOSTResponse> paymentsProcessingAdapter;

	@Autowired
	@Qualifier("dsPaymentsRoutingAdapter")
	private DomesticScheduledPaymentsAdapter dsPaymentsAdapter;

	@Autowired
	private PispConsentAdapter pispConsentAdapter;
	
	@Autowired
	private RequestHeaderAttributes requestHeaderAttr;
	
	@Autowired
	@Qualifier("dsPaymentConsentsStagingRoutingAdapter")
	private DomesticScheduledPaymentStagingAdapter dsPaymentConsentsAdapter;

	@Autowired
	private DSPaymentsPayloadComparator dsPaymentsComparator;
	
	@Value("${cmaVersion}")
	private String cmaVersion;

	@Override
	public CustomDSPaymentsPOSTResponse createDomesticScheduledPaymentsResource(
			CustomDSPaymentsPOSTRequest customRequest) {

		CustomDSPaymentsPOSTResponse paymentsFoundationResponse = null;
		CustomDSPConsentsPOSTResponse consentsFoundationResource = null;
		PaymentConsentsPlatformResource paymentConsentPlatformResource = null;
		PaymentsPlatformResource paymentsPlatformResource = null;

		Map<String, Object> paymentsPlatformResourceMap = paymentsProcessingAdapter.preSubmissionProcessing(
				customRequest, populatePlatformDetails(), customRequest.getData().getConsentId().trim());

		paymentConsentPlatformResource = (PaymentConsentsPlatformResource) paymentsPlatformResourceMap
				.get(PaymentConstants.CONSENT);
		paymentsPlatformResource = (PaymentsPlatformResource) paymentsPlatformResourceMap
				.get(PaymentConstants.SUBMISSION);

		consentsFoundationResource = scheduledPayLoadCompare(customRequest, paymentConsentPlatformResource,
				customRequest.getData().getConsentId().trim());

		if (paymentsPlatformResource == null) {
			paymentsPlatformResource = paymentsProcessingAdapter.createInitialPaymentsPlatformResource(
					customRequest.getData().getConsentId(), cmaVersion,
					PaymentTypeEnum.DOMESTIC_SCH_PAY);
			paymentsPlatformResourceMap.put(PaymentConstants.SUBMISSION, paymentsPlatformResource);

		}

		if (paymentsPlatformResource.getProccessState().equals(PaymentConstants.INCOMPLETE)) {
			customRequest = populateScheduledStagedProcessingFields(customRequest, consentsFoundationResource);
			Map<String, OBExternalStatus1Code> paymentStatusMap = new HashMap<>();
			paymentStatusMap.put(PaymentConstants.INITIAL, OBExternalStatus1Code.INITIATIONPENDING);
			paymentStatusMap.put(PaymentConstants.FAIL, OBExternalStatus1Code.INITIATIONFAILED);
			paymentStatusMap.put(PaymentConstants.PASS_M_AUTH, OBExternalStatus1Code.INITIATIONCOMPLETED);
			paymentStatusMap.put(PaymentConstants.PASS_M_AWAIT, OBExternalStatus1Code.INITIATIONPENDING);
			paymentStatusMap.put(PaymentConstants.PASS_M_REJECT, OBExternalStatus1Code.INITIATIONFAILED);
			customRequest.setCreatedOn(paymentsPlatformResource.getCreatedAt());
			PispConsent pispConsent = pispConsentAdapter.retrieveConsentByPaymentId(customRequest.getData().getConsentId());
			String partyIdentifier = pispConsent.getPartyIdentifier();
			Map<String, String> paramsMap = new HashMap<>();
			paramsMap.put(PSD2Constants.PARTY_IDENTIFIER, partyIdentifier);
			paramsMap.put(PSD2Constants.CO_RELATION_ID, requestHeaderAttr.getCorrelationId());
			
			GenericPaymentsResponse paymentsGenericResponse = dsPaymentsAdapter.processDSPayments(customRequest,
					paymentStatusMap, paramsMap);
			paymentsFoundationResponse = transformIntoTppResponse(paymentsGenericResponse, customRequest);
		}

		if (paymentsPlatformResource.getProccessState().equals(PaymentConstants.COMPLETED)) {
			CustomPaymentStageIdentifiers identifiers = new CustomPaymentStageIdentifiers();
			identifiers.setPaymentSetupVersion(cmaVersion);
			identifiers.setPaymentTypeEnum(PaymentTypeEnum.DOMESTIC_SCH_PAY);
			identifiers.setPaymentConsentId(paymentsPlatformResource.getPaymentConsentId());
			identifiers.setPaymentSubmissionId(paymentsPlatformResource.getSubmissionId());
			paymentsFoundationResponse = dsPaymentsAdapter.retrieveStagedDSPaymentsResponse(identifiers, null);

		}

		if (paymentsFoundationResponse == null) {
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
					InternalServerErrorMessage.PISP_NO_PAYMENT_SETUP_RESOURCES_FOUND));
		}

		OBExternalStatus2Code multiAuthStatus = OBExternalStatus2Code.AUTHORISED;
		if (paymentsFoundationResponse.getData().getMultiAuthorisation() != null)
			multiAuthStatus = paymentsFoundationResponse.getData().getMultiAuthorisation().getStatus();

		/* Fix for SIT issue #2272 
		 * Tpp providing Blank remittance info, and FS is returning null,
		 * Thus in such case returning same Blank remittance info to TPP.
		 * 27.02.2019 */
		OBRemittanceInformation1 requestRemittanceInfo = customRequest.getData().getInitiation().getRemittanceInformation();
		OBPostalAddress6 reqCreditorPostalAddress = customRequest.getData().getInitiation().getCreditorPostalAddress();
		paymentsFoundationResponse.getData().getInitiation().setRemittanceInformation(requestRemittanceInfo);
		paymentsFoundationResponse.getData().getInitiation().setCreditorPostalAddress(reqCreditorPostalAddress);		
		
		return paymentsProcessingAdapter.postPaymentProcessFlows(paymentsPlatformResourceMap,
				paymentsFoundationResponse, paymentsFoundationResponse.getProcessExecutionStatus(),
				paymentsFoundationResponse.getData().getDomesticScheduledPaymentId(), multiAuthStatus,
				HttpMethod.POST.toString());
	}

	private CustomDSPaymentsPOSTResponse transformIntoTppResponse(GenericPaymentsResponse paymentsGenericResponse,
			CustomDSPaymentsPOSTRequest customRequest) {
		String paymentFoundationRequest = JSONUtilities.getJSONOutPutFromObject(customRequest);
		CustomDSPaymentsPOSTResponse transformedResponse = JSONUtilities.getObjectFromJSONString(
				PispUtilities.getObjectMapper(), paymentFoundationRequest, CustomDSPaymentsPOSTResponse.class);
		transformedResponse.getData().setCharges(paymentsGenericResponse.getCharges());
		transformedResponse.getData().setDomesticScheduledPaymentId(paymentsGenericResponse.getSubmissionId());
		transformedResponse.getData()
				.setExpectedExecutionDateTime(paymentsGenericResponse.getExpectedExecutionDateTime());
		transformedResponse.getData()
				.setExpectedSettlementDateTime(paymentsGenericResponse.getExpectedSettlementDateTime());
		transformedResponse.getData().setMultiAuthorisation(paymentsGenericResponse.getMultiAuthorisation());
		transformedResponse.setProcessExecutionStatus(paymentsGenericResponse.getProcessExecutionStatusEnum());
		return transformedResponse;
	}

	@Override
	public CustomDSPaymentsPOSTResponse retrieveDomesticScheduledPaymentsResource(String domesticScheduledPaymentId) {

		Map<String, Object> paymentsPlatformResourceMap = paymentsProcessingAdapter
				.prePaymentProcessGETFlows(domesticScheduledPaymentId, PaymentTypeEnum.DOMESTIC_SCH_PAY);

		PaymentsPlatformResource paymentsPlatformResource = (PaymentsPlatformResource) paymentsPlatformResourceMap
				.get(PaymentConstants.SUBMISSION);
		
		/*
		 * Fix for Splunk defect #P001051-37 
		 * Setting psuId to requestHeaderAttr
		 * in order to log it in errors and message.Exit
		 */
		PispConsent consent = pispConsentAdapter.retrieveConsentByPaymentId(paymentsPlatformResource.getPaymentConsentId());
		if(NullCheckUtils.isNullOrEmpty(consent)){
			throw PSD2Exception.populatePSD2Exception(ErrorCodeEnum.NO_CONSENT_FOUND);
		} else {
			requestHeaderAttr.setPsuId(consent.getPsuId());
		}

		CustomPaymentStageIdentifiers identifiers = new CustomPaymentStageIdentifiers();
		identifiers.setPaymentSetupVersion(cmaVersion);
		identifiers.setPaymentTypeEnum(PaymentTypeEnum.DOMESTIC_SCH_PAY);
		identifiers.setPaymentConsentId(paymentsPlatformResource.getPaymentConsentId());
		identifiers.setPaymentSubmissionId(domesticScheduledPaymentId);

		CustomDSPaymentsPOSTResponse paymentsFoundationResponse = dsPaymentsAdapter
				.retrieveStagedDSPaymentsResponse(identifiers, null);

		if (paymentsFoundationResponse == null) {
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,
					InternalServerErrorMessage.PISP_NO_PAYMENT_SETUP_RESOURCES_FOUND));
		}

		OBExternalStatus2Code multiAuthStatus = OBExternalStatus2Code.AUTHORISED;
		if (paymentsFoundationResponse.getData().getMultiAuthorisation() != null)
			multiAuthStatus = paymentsFoundationResponse.getData().getMultiAuthorisation().getStatus();

		return paymentsProcessingAdapter.postPaymentProcessFlows(paymentsPlatformResourceMap,
				paymentsFoundationResponse, paymentsFoundationResponse.getProcessExecutionStatus(),
				paymentsFoundationResponse.getData().getDomesticScheduledPaymentId(), multiAuthStatus,
				HttpMethod.GET.toString());
	}

	private CustomPaymentSetupPlatformDetails populatePlatformDetails() {
		CustomPaymentSetupPlatformDetails customPaymentSetupPlatformDetails = new CustomPaymentSetupPlatformDetails();
		customPaymentSetupPlatformDetails.setSetupCmaVersion(cmaVersion);
		customPaymentSetupPlatformDetails.setPaymentType(PaymentTypeEnum.DOMESTIC_SCH_PAY);
		return customPaymentSetupPlatformDetails;
	}

	private CustomDSPConsentsPOSTResponse scheduledPayLoadCompare(CustomDSPaymentsPOSTRequest customRequest,
			PaymentConsentsPlatformResource paymentConsentPlatformResource, String consentId) {

		CustomPaymentStageIdentifiers identifiers = new CustomPaymentStageIdentifiers();
		identifiers.setPaymentConsentId(consentId);
		identifiers.setPaymentSetupVersion(cmaVersion);
		identifiers.setPaymentTypeEnum(PaymentTypeEnum.DOMESTIC_SCH_PAY);

		CustomDSPConsentsPOSTResponse paymentSetupFoundationResponse = dsPaymentConsentsAdapter
				.retrieveStagedDomesticScheduledPaymentConsents(identifiers, null);

		if (dsPaymentsComparator.comparePaymentDetails(paymentSetupFoundationResponse, customRequest,
				paymentConsentPlatformResource) > 0)
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_RESOURCE_CONSENTMISMATCH,
					ErrorMapKeys.RESOURCE_CONSENT_MISMATCH));
		return paymentSetupFoundationResponse;
	}

	private CustomDSPaymentsPOSTRequest populateScheduledStagedProcessingFields(
			CustomDSPaymentsPOSTRequest customRequest, CustomDSPConsentsPOSTResponse consentsFoundationResource) {
		/*
		 * If TPP has not sent DebtorAccount.Name then product will set the name
		 * details from staging record which is sent by FS
		 */
		if (customRequest.getData().getInitiation().getDebtorAccount() != null
				&& NullCheckUtils.isNullOrEmpty(customRequest.getData().getInitiation().getDebtorAccount().getName())) {

			customRequest.getData().getInitiation().getDebtorAccount()
					.setName(consentsFoundationResource.getData().getInitiation().getDebtorAccount().getName());
		}
		/*
		 * If debtor details are not provided by TPP then Platform will set
		 * debtor details received from FS. Same will be sent to FS for
		 * Pre-SubmissionValidation and Account Permission
		 */

		if (customRequest.getData().getInitiation().getDebtorAccount() == null) {
			customRequest.getData().getInitiation()
					.setDebtorAccount(consentsFoundationResource.getData().getInitiation().getDebtorAccount());
		}
		return customRequest;
	}
}