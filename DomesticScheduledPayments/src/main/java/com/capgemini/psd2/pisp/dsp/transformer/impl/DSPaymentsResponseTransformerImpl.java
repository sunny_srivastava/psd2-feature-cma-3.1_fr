package com.capgemini.psd2.pisp.dsp.transformer.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMethod;

import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;
import com.capgemini.psd2.pisp.domain.CustomDSPaymentsPOSTRequest;
import com.capgemini.psd2.pisp.domain.CustomDSPaymentsPOSTResponse;
import com.capgemini.psd2.pisp.domain.DStandingOrderPOST201Response;
import com.capgemini.psd2.pisp.domain.Links;
import com.capgemini.psd2.pisp.domain.Meta;
import com.capgemini.psd2.pisp.domain.OBCashAccountDebtor3;
import com.capgemini.psd2.pisp.domain.OBExternalStatus1Code;
import com.capgemini.psd2.pisp.domain.OBExternalStatus2Code;
import com.capgemini.psd2.pisp.domain.PaymentConsentsPlatformResource;
import com.capgemini.psd2.pisp.domain.PaymentsPlatformResource;
import com.capgemini.psd2.pisp.payment.submission.platform.adapter.PaymentsPlatformAdapter;
import com.capgemini.psd2.pisp.processing.adapter.PaymentSubmissionTransformer;
import com.capgemini.psd2.pisp.status.PaymentConstants;
import com.capgemini.psd2.pisp.utilities.PispDateUtility;
import com.capgemini.psd2.pisp.utilities.PispUtilities;
import com.capgemini.psd2.pisp.validation.adapter.constants.PaymentSetupConstants;
import com.capgemini.psd2.utilities.NullCheckUtils;

@Component("domesticSchedulePaymentConsentTransformer")
public class DSPaymentsResponseTransformerImpl
		implements PaymentSubmissionTransformer<CustomDSPaymentsPOSTResponse, CustomDSPaymentsPOSTRequest> {

	@Autowired
	private RequestHeaderAttributes reqHeaderAtrributes;

	@Autowired
	private PaymentsPlatformAdapter paymentsPlatformAdapter;

	@Autowired
	private PispDateUtility pispDateUtility;

	@Override
	public CustomDSPaymentsPOSTResponse paymentSubmissionResponseTransformer(
			CustomDSPaymentsPOSTResponse paymentSubmissionResponse, Map<String, Object> paymentsPlatformResourceMap,
			String methodType) {

		PaymentsPlatformResource paymentsPlatformResource = (PaymentsPlatformResource) paymentsPlatformResourceMap
				.get(PaymentConstants.SUBMISSION);
		PaymentConsentsPlatformResource paymentConentsPlatformResource = (PaymentConsentsPlatformResource) paymentsPlatformResourceMap
				.get(PaymentConstants.CONSENT);

		/* Adding Prefix uk.obie. in creditor scheme in response */
		String creditorAccountScheme = paymentSubmissionResponse.getData().getInitiation().getCreditorAccount()
				.getSchemeName();
		if (PaymentSetupConstants.prefixToAddSchemeNameList().contains(creditorAccountScheme)) {
			paymentSubmissionResponse.getData().getInitiation().getCreditorAccount()
					.setSchemeName(PSD2Constants.OB_SCHEME_PREFIX.concat(creditorAccountScheme));
		}

		/* Adding Prefix uk.obie. in debtor scheme in response */
		if (!NullCheckUtils.isNullOrEmpty(paymentSubmissionResponse.getData().getInitiation()
				.getDebtorAccount())) {
			String debtorAccountScheme = paymentSubmissionResponse.getData().getInitiation()
				.getDebtorAccount().getSchemeName();
			if (PaymentSetupConstants.prefixToAddSchemeNameList().contains(debtorAccountScheme)) {
				paymentSubmissionResponse.getData().getInitiation()
				.getDebtorAccount().setSchemeName(PSD2Constants.OB_SCHEME_PREFIX.concat(debtorAccountScheme));
			}
		}

		if (methodType.equals(RequestMethod.POST.toString())) {
			paymentSubmissionResponse.getData().setCreationDateTime(paymentsPlatformResource.getCreatedAt());
			/*
			 * Defect Fix for P000428-1353: If FS changes status from InitiationPending to
			 * InitiationFailed/InitiationCompleted, the status in response should also
			 * reflect the updated FS status during POST call.
			 */
			 updatePaymentsSubmissionResource(paymentsPlatformResource, paymentSubmissionResponse); 
			if (paymentConentsPlatformResource.getTppDebtorDetails().equalsIgnoreCase(Boolean.FALSE.toString())) {
				paymentSubmissionResponse.getData().getInitiation().setDebtorAccount(null);
			}
			if (Boolean.valueOf(paymentConentsPlatformResource.getTppDebtorDetails())
					&& !Boolean.valueOf(paymentConentsPlatformResource.getTppDebtorNameDetails()))
				paymentSubmissionResponse.getData().getInitiation()
				.getDebtorAccount().setName(null);

		} else {
			paymentSubmissionResponse.getData().setCreationDateTime(paymentsPlatformResource.getCreatedAt());
			updatePaymentsSubmissionResource(paymentsPlatformResource, paymentSubmissionResponse); 
			
			/*
			 * SIT Defect #2447 fix Submission GET should not have debtor details if not
			 * provided at time of setup : 04-03-2019
			 */

			/*
			 * If debtorFlag is 'False' then DebtorAccount & DebtorAgent details do not have
			 * to send in response. debtorFlag would be set as Null DebtorAgent is not
			 * available in CMA3
			 */
			if (!Boolean.valueOf(paymentConentsPlatformResource.getTppDebtorDetails()))
				paymentSubmissionResponse.getData().getInitiation().setDebtorAccount(null);

			/*
			 * If tppDebtorNameFlag is 'False' then DebtorAccount.name should not be sent to
			 * TPP.
			 */
			if (Boolean.valueOf(paymentConentsPlatformResource.getTppDebtorDetails())
					&& !Boolean.valueOf(paymentConentsPlatformResource.getTppDebtorNameDetails()))
				paymentSubmissionResponse.getData().getInitiation()
				.getDebtorAccount().setName(null);
		}

		updateMetaAndLinks(paymentSubmissionResponse, methodType);
		return paymentSubmissionResponse;
	}
	
	private void updatePaymentsSubmissionResource(PaymentsPlatformResource paymentsPlatformResource, CustomDSPaymentsPOSTResponse paymentSubmissionResponse) {
		OBExternalStatus1Code status = OBExternalStatus1Code
				.valueOf(paymentsPlatformResource.getStatus().toUpperCase());
		String statusUpdateDateTime = paymentsPlatformResource.getStatusUpdateDateTime();

		if (paymentsPlatformResource.getStatus().equals(OBExternalStatus1Code.INITIATIONPENDING.toString())) {
			if (paymentSubmissionResponse.getData().getStatus() == OBExternalStatus1Code.INITIATIONCOMPLETED) {
				status = paymentSubmissionResponse.getData().getStatus();
				statusUpdateDateTime = paymentSubmissionResponse.getData().getStatusUpdateDateTime();
				updatePaymentsResource(paymentsPlatformResource, status, statusUpdateDateTime);
			}
			else if (paymentSubmissionResponse.getData().getStatus() == OBExternalStatus1Code.INITIATIONFAILED) {
				status = paymentSubmissionResponse.getData().getStatus();
				statusUpdateDateTime = paymentSubmissionResponse.getData().getStatusUpdateDateTime();
				updatePaymentsResource(paymentsPlatformResource, status, statusUpdateDateTime);
			}
			else if (!NullCheckUtils.isNullOrEmpty(paymentSubmissionResponse.getData().getMultiAuthorisation())) {
				OBExternalStatus2Code responseMultiAuthStatus = paymentSubmissionResponse.getData().getMultiAuthorisation()
						.getStatus();
				if (responseMultiAuthStatus == OBExternalStatus2Code.AUTHORISED) {
					status = OBExternalStatus1Code.INITIATIONCOMPLETED;
					statusUpdateDateTime = PispUtilities.getCurrentDateInISOFormat();
					updatePaymentsResource(paymentsPlatformResource, status, statusUpdateDateTime);
				} else if (responseMultiAuthStatus == OBExternalStatus2Code.REJECTED) {
					status = OBExternalStatus1Code.INITIATIONFAILED;
					statusUpdateDateTime = PispUtilities.getCurrentDateInISOFormat();
					updatePaymentsResource(paymentsPlatformResource, status, statusUpdateDateTime);
				}
			}
		}
		paymentSubmissionResponse.getData().setStatus(status);
		paymentSubmissionResponse.getData().setStatusUpdateDateTime(statusUpdateDateTime);		
	}

	public CustomDSPaymentsPOSTResponse updateMetaAndLinks(CustomDSPaymentsPOSTResponse paymentSubmissionResponse,
			String methodType) {
		if (paymentSubmissionResponse.getLinks() == null)
			paymentSubmissionResponse.setLinks(new Links());
		paymentSubmissionResponse.getLinks().setSelf(
				PispUtilities.populateLinks(paymentSubmissionResponse.getData().getDomesticScheduledPaymentId(),
						methodType, reqHeaderAtrributes.getSelfUrl()));

		if (paymentSubmissionResponse.getMeta() == null)
			paymentSubmissionResponse.setMeta(new Meta());
		return paymentSubmissionResponse;
	}

	private void updatePaymentsResource(PaymentsPlatformResource paymentsPlatformResource, OBExternalStatus1Code status,
			String statusUpdateDateTime) {
		paymentsPlatformResource.setStatus(status.toString());
		paymentsPlatformResource.setStatusUpdateDateTime(statusUpdateDateTime);
		paymentsPlatformAdapter.updatePaymentsPlatformResource(paymentsPlatformResource);
	}

	@Override
	public CustomDSPaymentsPOSTRequest paymentSubmissionRequestTransformer(
			CustomDSPaymentsPOSTRequest paymentSubmissionRequest) {
		String submissionRequestExecutionDateTime = paymentSubmissionRequest.getData().getInitiation()
				.getRequestedExecutionDateTime();
		if (!NullCheckUtils.isNullOrEmpty(submissionRequestExecutionDateTime))
			paymentSubmissionRequest.getData().getInitiation().setRequestedExecutionDateTime(
					(pispDateUtility.transformDateTimeInRequest(submissionRequestExecutionDateTime)));
		return paymentSubmissionRequest;
	}
}
