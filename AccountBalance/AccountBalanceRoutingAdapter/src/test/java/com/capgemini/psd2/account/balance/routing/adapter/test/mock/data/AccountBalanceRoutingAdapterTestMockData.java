/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.account.balance.routing.adapter.test.mock.data;

import java.util.ArrayList;
import java.util.List;

import com.capgemini.psd2.aisp.domain.OBReadBalance1;
import com.capgemini.psd2.aisp.domain.OBReadBalance1Data;
import com.capgemini.psd2.aisp.domain.OBReadBalance1DataBalance;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountBalanceResponse;

/**
 * The Class AccountBalanceRoutingAdapterTestMockData.
 */
public class AccountBalanceRoutingAdapterTestMockData {

	public static PlatformAccountBalanceResponse getMockBalanceGETResponse() {
		PlatformAccountBalanceResponse platformAccountBalanceResponse = new PlatformAccountBalanceResponse();
		OBReadBalance1Data data = new OBReadBalance1Data();
		List<OBReadBalance1DataBalance> balance = new ArrayList<>();
		data.setBalance(balance);
		OBReadBalance1 oBReadBalance1 = new OBReadBalance1();
		oBReadBalance1.setData(data);
		platformAccountBalanceResponse.setoBReadBalance1(oBReadBalance1);
		return platformAccountBalanceResponse;
	}
}
