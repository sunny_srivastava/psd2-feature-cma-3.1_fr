/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.account.balance.test.mock.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.capgemini.psd2.aisp.domain.Links;
import com.capgemini.psd2.aisp.domain.Meta;
import com.capgemini.psd2.aisp.domain.OBReadBalance1;
import com.capgemini.psd2.aisp.domain.OBReadBalance1Data;
import com.capgemini.psd2.aisp.domain.OBReadBalance1DataAmount;
import com.capgemini.psd2.aisp.domain.OBReadBalance1DataBalance;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountBalanceResponse;
import com.capgemini.psd2.consent.domain.AccountDetails;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.consent.domain.AispConsent;
import com.capgemini.psd2.enums.ConsentStatusEnum;
import com.capgemini.psd2.logger.LoggerAttribute;
import com.capgemini.psd2.token.ConsentTokenData;
import com.capgemini.psd2.token.Token;

/**
 * The Class AccountBalanceMockData.
 */
public class AccountBalanceMockData {

	/** The mock token. */
	public static Token mockToken;

	/**
	 * Gets the token.
	 *
	 * @return the token
	 */
	public static Token getToken() {
		Map<String,String> map=new HashMap<>();
		mockToken = new Token();
		ConsentTokenData consentTokenData = new ConsentTokenData();
		consentTokenData.setConsentExpiry("1509348259877L");
		consentTokenData.setConsentId("12345");
		mockToken.setConsentTokenData(consentTokenData);
		mockToken.setSeviceParams(map);
		return mockToken;
	}
	
	/**
	 * Gets the mock account mapping.
	 *
	 * @return the mock account mapping
	 */
	public static AccountMapping getMockAccountMapping() {
		AccountMapping mapping = new AccountMapping();
		mapping.setTppCID("6443e15975554bce8099e35b88b40465");
		mapping.setPsuId("88888888");

		List<AccountDetails> selectedAccounts = new ArrayList<>();
		AccountDetails accountRequest = new AccountDetails();

		accountRequest.setAccountId("269c3ff5-d7f8-419b-a3b9-7136c5b4611a");
		accountRequest.setAccountNSC("903779");
		accountRequest.setAccountNumber("76528776");
		selectedAccounts.add(accountRequest);
		mapping.setAccountDetails(selectedAccounts);
		return mapping;
	}

	/**
	 * Gets the consent mock data.
	 *
	 * @return the consent mock data
	 */
	public static AispConsent getMockAispConsent() {
		AispConsent aispConsent = new AispConsent();
		aispConsent.setTppCId("6443e15975554bce8099e35b88b40465");
		aispConsent.setPsuId("88888888");
		aispConsent.setConsentId("39032481");
		aispConsent.setAccountRequestId("ae8c4441-f783-4e80-8810-254241bed98c");
		aispConsent.setChannelId("B365");
		aispConsent.setStartDate("2017-08-09T06:44:31.250Z");
		aispConsent.setEndDate("2018-09-02T00:00:00.875");
		aispConsent.setTransactionFromDateTime("2015-05-03T00:00:00.800");
		aispConsent.setTransactionToDateTime("2018-12-03T00:00:00.345");
		aispConsent.setStatus(ConsentStatusEnum.AUTHORISED);

		List<AccountDetails> selectedAispConsentAccountDetails = new ArrayList<>();
		AccountDetails accountRequest = new AccountDetails();
		
		
		accountRequest.setAccountId("269c3ff5-d7f8-419b-a3b9-7136c5b4611a");
		accountRequest.setAccountNSC("903779");
		accountRequest.setAccountNumber("76528776");
		
		selectedAispConsentAccountDetails.add(accountRequest);
		aispConsent.setAccountDetails(selectedAispConsentAccountDetails);
		
		return aispConsent;
	}


	public static LoggerAttribute getMockLoggerData() {
		LoggerAttribute x = new LoggerAttribute();
		x.setApiId("testApiID");
		return x;
	}
	/**
	 * Gets the balances GET response data.
	 *
	 * @return the balances GET response data
	 */
	public static PlatformAccountBalanceResponse getMockPlatformResponseWithoutLinksMeta() {
		PlatformAccountBalanceResponse platformAccountBalanceResponse = new PlatformAccountBalanceResponse();
		OBReadBalance1Data data = new OBReadBalance1Data();
		List<OBReadBalance1DataBalance> balance = new ArrayList<>();
		OBReadBalance1DataBalance p1 = new OBReadBalance1DataBalance();
		p1.setAccountId("269");
		OBReadBalance1DataAmount oBCashBalance1Amount = new OBReadBalance1DataAmount();
		oBCashBalance1Amount.setAmount("1000.0");
		oBCashBalance1Amount.setCurrency("USD");
		p1.amount(oBCashBalance1Amount);
		balance.add(p1);
		data.setBalance(balance);
		OBReadBalance1 oBReadBalance1 = new OBReadBalance1();
		oBReadBalance1.setData(data);
		
		platformAccountBalanceResponse.setoBReadBalance1(oBReadBalance1);
		return platformAccountBalanceResponse;
	}
	
	public static PlatformAccountBalanceResponse getMockPlatformResponseWithLinksMeta() {
		PlatformAccountBalanceResponse platformAccountBalanceResponse = new PlatformAccountBalanceResponse();
		OBReadBalance1Data data = new OBReadBalance1Data();
		List<OBReadBalance1DataBalance> balance = new ArrayList<>();
		OBReadBalance1DataBalance p1 = new OBReadBalance1DataBalance();
		p1.setAccountId("269c3ff5-d7f8-419b-a3b9-7136c5b4611a");
		OBReadBalance1DataAmount oBCashBalance1Amount = new OBReadBalance1DataAmount();
		oBCashBalance1Amount.setAmount("1000.0");
		oBCashBalance1Amount.setCurrency("USD");
		p1.amount(oBCashBalance1Amount);
		balance.add(p1);
		data.setBalance(balance);
		OBReadBalance1 oBReadBalance1 = new OBReadBalance1();
		oBReadBalance1.setData(data);
		Links links = new Links();
		oBReadBalance1.setLinks(links);
		Meta meta = new Meta();
		oBReadBalance1.setMeta(meta);

		platformAccountBalanceResponse.setoBReadBalance1(oBReadBalance1);
		return platformAccountBalanceResponse;
	}
	
	public static OBReadBalance1 getMockExpectedResponseWithoutLinkMeta() {
		OBReadBalance1 balance1 = new OBReadBalance1();
		OBReadBalance1Data data = new OBReadBalance1Data();
		List<OBReadBalance1DataBalance> balance = new ArrayList<>();
		OBReadBalance1DataBalance p1 = new OBReadBalance1DataBalance();
		p1.setAccountId("269");
		OBReadBalance1DataAmount oBCashBalance1Amount = new OBReadBalance1DataAmount();
		oBCashBalance1Amount.setAmount("1000.0");
		oBCashBalance1Amount.setCurrency("USD");
		p1.amount(oBCashBalance1Amount);
		balance.add(p1);
		data.setBalance(balance);
		balance1.setData(data);
		Links links = new Links();
		Meta meta = new Meta();
		meta.setTotalPages(1);
		balance1.setLinks(links);
		balance1.setMeta(meta);
		return balance1;
	}
	public static OBReadBalance1 getMockExpectedResponseWithLinksMeta() {
	OBReadBalance1 balance1 = new OBReadBalance1();
	OBReadBalance1Data data = new OBReadBalance1Data();
	List<OBReadBalance1DataBalance> balance = new ArrayList<>();
	OBReadBalance1DataBalance p1 = new OBReadBalance1DataBalance();
	p1.setAccountId("269c3ff5-d7f8-419b-a3b9-7136c5b4611a");
	OBReadBalance1DataAmount oBCashBalance1Amount = new OBReadBalance1DataAmount();
	oBCashBalance1Amount.setAmount("1000.0");
	oBCashBalance1Amount.setCurrency("USD");
	p1.amount(oBCashBalance1Amount);
	balance.add(p1);
	data.setBalance(balance);
	balance1.setData(data);
	Links links = new Links();
	Meta meta = new Meta();
	meta.setTotalPages(1);
	balance1.setLinks(links);
	balance1.setMeta(meta);
	return balance1;
}
}