/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.account.balance.service.impl;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.capgemini.psd2.account.balance.service.AccountBalanceService;
import com.capgemini.psd2.aisp.account.mapping.adapter.AccountMappingAdapter;
import com.capgemini.psd2.aisp.adapter.AccountBalanceAdapter;
import com.capgemini.psd2.aisp.adapter.AispConsentAdapter;
import com.capgemini.psd2.aisp.domain.Links;
import com.capgemini.psd2.aisp.domain.Meta;
import com.capgemini.psd2.aisp.domain.OBReadBalance1;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountBalanceResponse;
import com.capgemini.psd2.aisp.validation.adapter.AISPCustomValidator;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.consent.domain.AispConsent;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.logger.RequestHeaderAttributes;

/**
 * The Class AccountBalanceServiceImpl.
 */
@Service
public class AccountBalanceServiceImpl implements AccountBalanceService {

	/** The adapter. */
	@Autowired
	@Qualifier("accountBalanceRoutingAdapter")
	private AccountBalanceAdapter adapter;

	/** The req header atrributes. */
	@Autowired
	private RequestHeaderAttributes reqHeaderAtrributes;

	/** The account mapping adapter. */
	@Autowired
	private AccountMappingAdapter accountMappingAdapter;

	/** The consent mapping adapter. */
	@Autowired
	private AispConsentAdapter aispConsentAdapter;

	@SuppressWarnings("rawtypes")
	@Autowired
	@Qualifier("accountsRoutingValidator")
	private AISPCustomValidator accountsCustomValidator;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.capgemini.psd2.account.balance.service.AccountBalanceService#
	 * retrieveAccountBalance(java.lang.String)
	 */
	@Override
	public OBReadBalance1 retrieveAccountBalance(String accountId) {
		// Get consents by accountId and validate.

		accountsCustomValidator.validateUniqueId(accountId);
		
		AispConsent aispConsent = aispConsentAdapter.validateAndRetrieveConsentByAccountId(accountId,
				reqHeaderAtrributes.getToken().getConsentTokenData().getConsentId());

		// Get account mapping.
		AccountMapping accountMapping = accountMappingAdapter.retrieveAccountMappingDetails(accountId, aispConsent);

		Map<String, String> params = reqHeaderAtrributes.getToken().getSeviceParams();
		params.put(PSD2Constants.CMAVERSION, aispConsent.getCmaVersion());
		params.put(PSD2Constants.CO_RELATION_ID, reqHeaderAtrributes.getCorrelationId());

		// Retrieve account balance.
		PlatformAccountBalanceResponse platformBalanceResponse = adapter.retrieveAccountBalance(accountMapping, params);

		// Get CMA2 product response.
		OBReadBalance1 tppBalanceResponse = platformBalanceResponse.getoBReadBalance1();

		// update platform response.
		updatePlatformResponse(tppBalanceResponse);

		// Apply platform and CMA2-swagger validations.
		accountsCustomValidator.validateResponseParams(tppBalanceResponse);

		return tppBalanceResponse;
	}

	private void updatePlatformResponse(OBReadBalance1 tppBalanceResponse) {
		if (tppBalanceResponse.getLinks() == null)
			tppBalanceResponse.setLinks(new Links());
		tppBalanceResponse.getLinks().setSelf(reqHeaderAtrributes.getSelfUrl());

		if (tppBalanceResponse.getMeta() == null)
			tppBalanceResponse.setMeta(new Meta());
		tppBalanceResponse.getMeta().setTotalPages(1);
	}

}
