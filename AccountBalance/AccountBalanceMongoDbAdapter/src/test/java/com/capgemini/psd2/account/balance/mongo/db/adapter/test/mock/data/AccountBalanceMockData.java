package com.capgemini.psd2.account.balance.mongo.db.adapter.test.mock.data;
/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/

import java.util.ArrayList;
import java.util.List;

import com.capgemini.psd2.aisp.domain.OBReadBalance1;
import com.capgemini.psd2.aisp.domain.OBReadBalance1Data;
import com.capgemini.psd2.aisp.domain.OBReadBalance1DataBalance;
import com.capgemini.psd2.aisp.mongo.db.adapter.domain.AccountBalanceCMA2;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountBalanceResponse;
import com.capgemini.psd2.logger.LoggerAttribute;

/**
 * The Class AccountBalanceAdapterMockData.
 */
public class AccountBalanceMockData {

	public static List<AccountBalanceCMA2> getMockAccountDetails()
	{
		List<AccountBalanceCMA2> balanceList = new ArrayList<>();
		AccountBalanceCMA2 mockBalance = new AccountBalanceCMA2();
		mockBalance.setAccountId("f4483fda-81be-4873-b4a6-20375b7f55c1");
		balanceList.add(mockBalance);
		return balanceList;
	}
	
	public static LoggerAttribute getMockLoggerData() {
		LoggerAttribute x = new LoggerAttribute();
		x.setApiId("testApiID");
		return x;
	}
	
	public static OBReadBalance1 getMockExpectedBalanceresponse()
	{
		OBReadBalance1Data oBReadBalance1Data = new OBReadBalance1Data();
		OBReadBalance1 obreadBalance1 = new OBReadBalance1();
		AccountBalanceCMA2 mockBalance = new AccountBalanceCMA2();
		PlatformAccountBalanceResponse platformAccountBalanceResponse = new PlatformAccountBalanceResponse();
		mockBalance.setAccountId("f4483fda-81be-4873-b4a6-20375b7f55c1");
		
		List<OBReadBalance1DataBalance> balanceList = new ArrayList<>();
		balanceList.add(mockBalance);
		oBReadBalance1Data.setBalance(balanceList);
		obreadBalance1.setData(oBReadBalance1Data);
		platformAccountBalanceResponse.setoBReadBalance1(obreadBalance1);
		return obreadBalance1;
	}
	
}
