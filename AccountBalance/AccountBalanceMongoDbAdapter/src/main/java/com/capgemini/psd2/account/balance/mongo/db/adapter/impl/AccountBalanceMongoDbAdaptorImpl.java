/*******************************************************************************
 * CAPGEMINI CONFIDENTIAL
 * __________________
 * 
 * Copyright (C) 2017 CAPGEMINI GROUP - All Rights Reserved
 *  
 * NOTICE:  All information contained herein is, and remains
 * the property of CAPGEMINI GROUP.
 * The intellectual and technical concepts contained herein
 * are proprietary to CAPGEMINI GROUP and may be covered
 * by patents, patents in process, and are protected by trade secret
 * or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from CAPGEMINI GROUP.
 ******************************************************************************/
package com.capgemini.psd2.account.balance.mongo.db.adapter.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.stereotype.Component;

import com.capgemini.psd2.account.balance.mongo.db.adapter.repository.AccountBalanceRepository;
import com.capgemini.psd2.aisp.adapter.AccountBalanceAdapter;
import com.capgemini.psd2.aisp.domain.OBReadBalance1;
import com.capgemini.psd2.aisp.domain.OBReadBalance1Data;
import com.capgemini.psd2.aisp.domain.OBReadBalance1DataBalance;
import com.capgemini.psd2.aisp.mongo.db.adapter.domain.AccountBalanceCMA2;
import com.capgemini.psd2.aisp.platform.domain.PlatformAccountBalanceResponse;
import com.capgemini.psd2.consent.domain.AccountMapping;
import com.capgemini.psd2.exceptions.ExceptionDTO;
import com.capgemini.psd2.exceptions.InternalServerErrorMessage;
import com.capgemini.psd2.exceptions.OBErrorCodeEnum;
import com.capgemini.psd2.exceptions.PSD2Exception;
import com.capgemini.psd2.logger.PSD2Constants;
import com.capgemini.psd2.utilities.SandboxValidationUtility;

/**
 * The Class AccountBalanceMongoDbAdaptorImpl.
 */
@Component
public class AccountBalanceMongoDbAdaptorImpl implements AccountBalanceAdapter {

	@Autowired
	private AccountBalanceRepository repository;

	@Autowired
	private SandboxValidationUtility utility;
	
	/** The Constant LOG. */
	private static final Logger LOG = LoggerFactory.getLogger(AccountBalanceMongoDbAdaptorImpl.class);

	@Override
	public PlatformAccountBalanceResponse retrieveAccountBalance(AccountMapping accountMapping,
			Map<String, String> params) {

		PlatformAccountBalanceResponse platformAccountBalanceResponse = new PlatformAccountBalanceResponse();
		OBReadBalance1 oBReadBalance1 = new OBReadBalance1();
		List<AccountBalanceCMA2> mockBalanceList = null;
		LOG.info("TenantId : " + params.get(PSD2Constants.TENANT_ID));
		
		try {
				if(utility.isValidPsuId(accountMapping.getPsuId())){
					throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,InternalServerErrorMessage.SANDBOX_MOCKED_ERROR));
				}
			
				mockBalanceList = repository.findByAccountNumberAndAccountNSC(
					accountMapping.getAccountDetails().get(0).getAccountNumber(),
					accountMapping.getAccountDetails().get(0).getAccountNSC());
		} catch (DataAccessResourceFailureException e) {
			LOG.error(e.getMessage(),e);
			throw PSD2Exception.populatePSD2Exception(new ExceptionDTO(OBErrorCodeEnum.UK_OBIE_UNEXPECTEDERROR,InternalServerErrorMessage.CONNECTION_ERROR));
		}

		List<OBReadBalance1DataBalance> balanceList = new ArrayList<>();

		if(mockBalanceList!=null){
			for(AccountBalanceCMA2 balanceData : mockBalanceList){
				balanceData.setAccountId(accountMapping.getAccountDetails().get(0).getAccountId());
				balanceList.add(balanceData);
			}
		}		

		OBReadBalance1Data oBReadBalance1Data =new OBReadBalance1Data();
		oBReadBalance1Data.setBalance(balanceList);
		oBReadBalance1.setData(oBReadBalance1Data);
		platformAccountBalanceResponse.setoBReadBalance1(oBReadBalance1);
		return platformAccountBalanceResponse;
	}
}